/**
 * @file input/Keyboard.h
 * @author Tomas Polasek
 * @brief Keyboard handling class.
 */

#pragma once

#include "engine/input/KeyboardEvent.h"

/// Namespace containing the engine code.
namespace quark
{

/// Input processing.
namespace input
{

/// Keyboard handling class.
class Keyboard : public util::Singleton<Keyboard>
{
public:
    /// Application callback when using keyboard callback.
    using AppCallbackT = void (*)(KeyboardEvent event);

    Keyboard() = default;

    /**
     * Resets the hook, if this keyboard has been used with setCallback.
     */
    ~Keyboard();

    Keyboard(const Keyboard &other) = delete;
    Keyboard(Keyboard &&other) = delete;
    Keyboard &operator=(const Keyboard &other) = delete;
    Keyboard &operator=(Keyboard &&other) = delete;

    /**
     * Set keyboard callback hook. Only one keyboard hook can be used!
     * Does NOT need to be used if messages are passed directly.
     * Parsed events are passed to provided application callback.
     * @param hInstance which application should be hooked.
     * @param appCallback Parsed events are passed to this callback.
     * @throw Throws std::runtime_error if keyboard cannot be hooked.
     */
    void setCallback(HINSTANCE hInstance, AppCallbackT appCallback);

    /**
     * Unhook the current keyboard callback.
     */
    static void resetCallback();

    /**
     * Process given event message.
     * @param msg Message to process.
     * @return Returns a KeyEvent created from the message. Can be tested 
     * as a bool, result is whether the event is valid.
     */
    KeyboardEvent processEvent(const MSG& msg)
    { return processEventInner(msg.message, msg.wParam, msg.lParam); }

    /**
     * Process given event message.
     * @param messageCode Code of the message.
     * @param wParam Parameters of the message.
     * @param lParam Parameters of the message.
     * @return Returns a KeyEvent created from the message. Can be tested 
     * as a bool, result is whether the event is valid.
     */
    KeyboardEvent processEvent(int messageCode, WPARAM wParam, LPARAM lParam)
    { return processEventInner(messageCode, wParam, lParam); }
private:
    /// Keyboard callback function type - KeyboardProc.
    using KeyboardCallbackT = LRESULT (*)(_In_ int code, _In_ WPARAM wParam, _In_ LPARAM lParam) noexcept;

    /// Get callback function usable in SetWindowsHook.
    static KeyboardCallbackT callback()
    { return keyboardCallbackDispatch; }

    /**
     * Callback method called by Windows.
     * Dispatches the call to the selected Keyboard object.
     */
    static LRESULT keyboardCallbackDispatch(_In_ int code, _In_ WPARAM wParam, _In_ LPARAM lParam) noexcept;

    /// Reset the keyboard hook.
    void resetHook();

    /* 
     * Get key action, which can be computed from combination of 2 bits.
     * The result needs to be shifted into the lower 8 bits, since 
     * KeyAction is uint8_t.
     */
    static KeyAction actionFromLParam(LPARAM lParam)
    { return static_cast<KeyAction>(((HIWORD(lParam) & (KF_UP | KF_REPEAT)) >> 8u) + 1u); }

    /**
     * Get hardware dependent scan code from LPARAM value.
     */
    static ScanCode scanCodeFromLParam(LPARAM lParam)
    { return static_cast<ScanCode>(HIWORD(lParam) & 0xff); }

    /**
     * Get current modifiers from the keyboard.
     */
    static KeyMods currentModifiers()
    {
        const auto shift{ GetAsyncKeyState(VK_SHIFT) & 0x8000 };
        const auto control{ GetAsyncKeyState(VK_CONTROL) & 0x8000 };
        const auto alt{ GetAsyncKeyState(VK_MENU) & 0x8000 };
        return (shift ? KeyMods::Shift : KeyMods::None) | 
            (control ? KeyMods::Control : KeyMods::None) | 
            (alt ? KeyMods::Alt : KeyMods::None);
    }

    /**
     * Process given event message.
     * @param messageCode Code of the message.
     * @param wParam Parameters of the message.
     * @param lParam Parameters of the message.
     * @return Returns a KeyEvent created from the message. Can be tested 
     * as a bool, result is whether the event is valid.
     */
    static KeyboardEvent processEventInner(int messageCode, WPARAM wParam, LPARAM lParam);

    /// Currently selected keyboard, which has been set as a callback.
    static Keyboard *sHookKeyboard;

    /// Handle to callback hook.
    HHOOK mCallbackHook{ nullptr };
    /// Callback for passing key events.
    AppCallbackT mCallback{ nullptr };
protected:
}; // class Keyboard

} // namespace input

} // namespace quark
