/**
 * @file helpers/d3d12/D3D12ResourceUpdater.h
 * @author Tomas Polasek
 * @brief Helper class for updating D3D12 resources.
 */

#pragma once

#include "engine/helpers/d3d12/D3D12CommittedAllocator.h"

#include "engine/resources/d3d12/D3D12Device.h"
#include "engine/resources/d3d12/D3D12Resource.h"

/// Namespace containing the engine code.
namespace quark
{

/// Namespace containing helper classes and methods.
namespace helpers
{

/// Direct3D 12 resource management.
namespace d3d12
{

/**
 * Helper class which can be used to update D3D12 resources.
 */
class D3D12ResourceUpdater : public util::PointerType<D3D12ResourceUpdater>
{
public:
    /// Exception thrown when provided allocator does not does not meet the requirements.
    struct IncompatibleAllocatorException : public std::exception
    {
        IncompatibleAllocatorException(const char *msg) :
            std::exception(msg)
        { }
    }; // struct IncompatibleAllocatorException

    /**
     * Initialize resource updater with a committed allocator 
     * created from provided device. This allocator is used for 
     * creating intermediate resources used for updating other 
     * resource.
     * @param device Device used for creation of the allocator.
     */
    static PtrT create(res::d3d12::D3D12Device &device);

    /**
     * Initialize resource updater with given committed allocator 
     * which must meet allocator requirements proposed by this class.
     * @throws Throws IncompatibleAllocatorException if the allocator 
     * is not compatible.
     */
    static PtrT create(D3D12CommittedAllocator::PtrT allocator);

    /// Free the committed allocator.
    ~D3D12ResourceUpdater();

    // Copying and moving is allowed.
    D3D12ResourceUpdater(const D3D12ResourceUpdater &other) = default;
    D3D12ResourceUpdater &operator=(const D3D12ResourceUpdater &other) = default;
    D3D12ResourceUpdater(D3D12ResourceUpdater &&other) = default;
    D3D12ResourceUpdater &operator=(D3D12ResourceUpdater &&other) = default;

    /**
     * Initialize resource updater with a committed allocator 
     * created from provided device. This allocator is used for 
     * creating intermediate resources used for updating other 
     * resource.
     * @param device Device used for creation of the allocator.
     */
    void initialize(res::d3d12::D3D12Device &device);

    /**
     * Update destination resource with provided data.
     * @param data Data to copy to the destination resource.
     * @param sizeInBytes Size of the copied data. This value must 
     * be provided in number of bytes!
     * @param dest Destination of the copy operation.
     * @param cmdList Command list used to record the copy 
     * command.
     * @throws Throws util::winexception on error.
     */
    void copyToResource(const void *data, std::size_t sizeInBytes, 
        res::d3d12::D3D12Resource &dest, res::d3d12::D3D12CommandList &cmdList);

    /**
     * Update destination resource with provided data.
     * @param data Data to copy to the destination resource.
     * @param dest Destination of the copy operation.
     * @param cmdList Command list used to record the copy 
     * command.
     * @throws Throws util::winexception on error.
     */
    template <typename T>
    void copyToResource(const std::vector<T> &data, res::d3d12::D3D12Resource &dest,
        res::d3d12::D3D12CommandList &cmdList);

    /**
     * Update destination resource with provided data.
     * @tparam ItT Type of the iterator, which must be of 
     * random-access iterator type.
     * @param begin Begin iterator for the target data.
     * @param end End iterator for the target data.
     * @param dest Destination of the copy operation.
     * @param cmdList Command list used to record the copy 
     * command.
     * @throws Throws util::winexception on error.
     */
    template <typename ItT>
    typename std::enable_if<
        std::is_same_v<
            typename std::iterator_traits<ItT>::iterator_category,
            std::random_access_iterator_tag>,
        void>::type
    copyToResource(const ItT &begin, const ItT &end, res::d3d12::D3D12Resource &dest,
        res::d3d12::D3D12CommandList &cmdList);

    /// Get heap properties required from the allocator.
    static ::CD3DX12_HEAP_PROPERTIES requiredHeapProps()
    { return ::CD3DX12_HEAP_PROPERTIES(::D3D12_HEAP_TYPE_UPLOAD); }

    /// Get heap flags required from the allocator.
    static ::D3D12_HEAP_FLAGS requiredHeapFlags()
    { return ::D3D12_HEAP_FLAG_NONE; }

    /**
     * Set debug name to the current buffer.
     * @param dbgName Readable debug name string.
     */
    void setDebugName(const wchar_t *dbgName);

    /**
     * Does given allocator meet requirements proposed by the 
     * resource updater?
     * @param allocator Tested allocator.
     * @return Returns true if the allocator does meet the requirements.
     */
    static bool allocatorMeetsRequirements(const D3D12CommittedAllocator &allocator);

    /**
     * Notify resource updater that all of the copy commands it 
     * created have been executed and it can release the intermediate 
     * buffers.
     */
    void commandListExecuted();
private:
    /**
     * Stash provided buffer to keep it allocated until 
     * user allows us to release it.
     * @param buffer Buffer which should be stashed.
     */
    void stashBuffer(res::d3d12::D3D12Resource &buffer);

    /// Allocator used for allocation of intermediate resources.
    D3D12CommittedAllocator::PtrT mAllocator;

    /// Target device.
    res::d3d12::D3D12Device *mDevice{ nullptr };

    /// List of stashed intermediate buffers.
    std::vector<ComPtr<::ID3D12Resource>> mStashedBuffers;
#ifdef _DEBUG
    /// Debug name for the intermediate resources.
    const wchar_t *mDbgName{ nullptr };
#endif
protected:
    /**
     * Create un-initialized resource updater. Before using 
     * it, it must be initialized with target device!
     */
    D3D12ResourceUpdater();

    /**
     * Initialize resource updater with a committed allocator 
     * created from provided device. This allocator is used for 
     * creating intermediate resources used for updating other 
     * resource.
     * @param device Device used for creation of the allocator.
     */
    D3D12ResourceUpdater(res::d3d12::D3D12Device &device);

    /**
     * Initialize resource updater with given committed allocator 
     * which must meet allocator requirements proposed by this class.
     * @throws Throws IncompatibleAllocatorException if the allocator 
     * is not compatible.
     */
    D3D12ResourceUpdater(D3D12CommittedAllocator::PtrT allocator);
}; // class D3D12ResourceUpdater

} // namespace d3d12

} // namespace helpers

} // namespace quark

// Template implementation

namespace quark
{
namespace helpers
{
namespace d3d12
{

template <typename T>
void D3D12ResourceUpdater::copyToResource(const std::vector<T> &data, 
    res::d3d12::D3D12Resource &dest, res::d3d12::D3D12CommandList &cmdList)
{ copyToResource(data.data(), data.size() * sizeof(T), dest, cmdList); }

template <typename ItT>
typename std::enable_if<
    std::is_same_v<
        typename std::iterator_traits<ItT>::iterator_category, 
        std::random_access_iterator_tag>, 
    void>::type
D3D12ResourceUpdater::copyToResource(const ItT &begin, const ItT &end, res::d3d12::D3D12Resource &dest, res::d3d12::D3D12CommandList &cmdList)
{
    using TraitsT = std::iterator_traits<ItT>;

    using ElementT = typename TraitsT::value_type;
    typename TraitsT::pointer data{ &(*begin) };
    const std::size_t sizeInBytes{ (end - begin) * sizeof(ElementT) };

    copyToResource(data, sizeInBytes, dest, cmdList);
}

}
}
}

// Template implementation end
