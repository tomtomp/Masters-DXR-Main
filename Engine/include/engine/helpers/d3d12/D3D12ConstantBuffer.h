/**
 * @file helpers/d3d12/D3D12ConstantBuffer.h
 * @author Tomas Polasek
 * @brief Wrapper around D3D12Resource, allowing to use it 
 * as a constant buffer - CBV.
 */

#pragma once

#include "engine/helpers/d3d12/D3D12UploadBuffer.h"

#include "engine/resources/d3d12/D3D12DescHeap.h"

/// Namespace containing the engine code.
namespace quark
{

/// Namespace containing helper classes and methods.
namespace helpers
{

/// Direct3D 12 resource management.
namespace d3d12
{

/**
 * Wrapper around D3D12Resource, allowing to use it 
 * as a constant buffer - CBV.
 */
class D3D12ConstantBuffer : public helpers::d3d12::D3D12UploadBuffer, public util::InheritPointerType<res::d3d12::D3D12Resource, D3D12ConstantBuffer>
{
public:
    using PtrT = util::PointerType<D3D12ConstantBuffer>::PtrT;
    using ConstPtrT = util::PointerType<D3D12ConstantBuffer>::ConstPtrT;

    /**
     * Create a texture buffer of given size.
     * @tparam AllocT Type of the allocator.
     * @param allocator This allocator will be used to 
     * allocate the constant buffer.
     * @param desc Description of the texture buffer.
     * @param initState Initial state of the buffer.
     * @param name Optional name of the buffer.
     * @throws util::winexception Thrown on error.
     */
    template <typename AllocT>
    static PtrT create(const AllocT &allocator, 
        const ::CD3DX12_RESOURCE_DESC &desc, 
        ::D3D12_RESOURCE_STATES initState = ::D3D12_RESOURCE_STATE_COPY_DEST, 
        const std::wstring &name = L"ConstantBuffer");

    /**
     * Initialize the constant buffer with provided data.
     * Constant buffer will be just large enough to contain provided 
     * data.
     * @tparam AllocT Type of the allocator.
     * @tparam ItT Type of the iterator, which must be of 
     * random-access iterator type.
     * @param begin Begin iterator for the constant data.
     * @param end End iterator for the constant data.
     * @param desc Description of the constant buffer.
     * @param cmdList Command list used to record the copy 
     * command.
     * @param allocator Allocator used to allocate the constant 
     * buffer.
     * @param updater This resource updater will be used to 
     * copy data over to the GPU side.
     * @param name Optional name of the buffer.
     * @throws util::winexception Thrown on error.
     */
    template <typename AllocT, typename ItT>
    static PtrT create(const ItT &begin, const ItT &end,
        const ::CD3DX12_RESOURCE_DESC &desc, 
        res::d3d12::D3D12CommandList &cmdList,
        const AllocT &allocator, D3D12ResourceUpdater &updater, 
        const std::wstring &name = L"ConstantBuffer");

    /// Free any allocated buffers.
    ~D3D12ConstantBuffer();

    // No copying.
    D3D12ConstantBuffer(const D3D12ConstantBuffer &other) = delete;
    D3D12ConstantBuffer &operator=(const D3D12ConstantBuffer &other) = delete;
    // Moving is allowed.
    D3D12ConstantBuffer(D3D12ConstantBuffer &&other) = default;
    D3D12ConstantBuffer &operator=(D3D12ConstantBuffer &&other) = default;

    /**
     * Create view for this constant buffer on provided descriptor 
     * heap.
     * @param descHeap Descriptor heap used to allocate the view 
     * descriptor.
     */
    void createCBV(res::d3d12::D3D12DescHeap &descHeap);

    /// Clear create CBV handles.
    void clearCBV();

    /**
     * Get CPU handle to the constant buffer view descriptor.
     * @return Returns the descriptor handle. Returns zero handle 
     * if creteCBV has not been called yet.
     */
    ::CD3DX12_CPU_DESCRIPTOR_HANDLE cpuCBV() const;
    /**
     * Get GPU handle to the constant buffer view descriptor.
     * @return Returns the descriptor handle. Returns zero handle 
     * if creteCBV has not been called yet.
     */
    ::CD3DX12_GPU_DESCRIPTOR_HANDLE gpuCBV() const;
private:
    /// Cached Constant-Buffer-View CPU handle.
    ::CD3DX12_CPU_DESCRIPTOR_HANDLE mCpuCBV{ };
    /// Cached Constant-Buffer-View GPU handle.
    ::CD3DX12_GPU_DESCRIPTOR_HANDLE mGpuCBV{ };
protected:
    /**
     * Initialize empty constant buffer.
     */
    D3D12ConstantBuffer();

    /**
     * Create a texture buffer of given size.
     * @tparam AllocT Type of the allocator.
     * @param allocator This allocator will be used to 
     * allocate the constant buffer.
     * @param desc Description of the texture buffer.
     * @param initState Initial state of the buffer.
     * @param name Optional name of the buffer.
     * @throws util::winexception Thrown on error.
     */
    template <typename AllocT>
    D3D12ConstantBuffer(const AllocT &allocator, 
        const ::CD3DX12_RESOURCE_DESC &desc, 
        ::D3D12_RESOURCE_STATES initState = ::D3D12_RESOURCE_STATE_COPY_DEST, 
        const std::wstring &name = L"ConstantBuffer");

    /**
     * Initialize the constant buffer with provided data.
     * Constant buffer will be just large enough to contain provided 
     * data.
     * @tparam AllocT Type of the allocator.
     * @tparam ItT Type of the iterator, which must be of 
     * random-access iterator type.
     * @param begin Begin iterator for the constant data.
     * @param end End iterator for the constant data.
     * @param desc Description of the constant buffer.
     * @param cmdList Command list used to record the copy 
     * command.
     * @param allocator Allocator used to allocate the constant 
     * buffer.
     * @param updater This resource updater will be used to 
     * copy data over to the GPU side.
     * @param name Optional name of the buffer.
     * @throws util::winexception Thrown on error.
     */
    template <typename AllocT, typename ItT>
    D3D12ConstantBuffer(const ItT &begin, const ItT &end,
        const ::CD3DX12_RESOURCE_DESC &desc, 
        res::d3d12::D3D12CommandList &cmdList,
        const AllocT &allocator, D3D12ResourceUpdater &updater, 
        const std::wstring &name = L"ConstantBuffer");
}; // class D3D12ConstantBuffer

} // namespace d3d12

} // namespace helpers

} // namespace quark

// Template implementation

namespace quark
{

namespace helpers
{

namespace d3d12
{

template<typename AllocT>
D3D12ConstantBuffer::PtrT D3D12ConstantBuffer::create(const AllocT &allocator, const::CD3DX12_RESOURCE_DESC &desc, 
    ::D3D12_RESOURCE_STATES initState, const std::wstring &name)
{ return PtrT{ new D3D12ConstantBuffer(allocator, desc, initState, name) }; }

template<typename AllocT, typename ItT>
D3D12ConstantBuffer::PtrT D3D12ConstantBuffer::create(const ItT &begin, const ItT &end, const::CD3DX12_RESOURCE_DESC &desc, 
    res::d3d12::D3D12CommandList &cmdList, const AllocT &allocator, D3D12ResourceUpdater &updater, const std::wstring &name)
{ return PtrT{ new D3D12ConstantBuffer(begin, end, desc, cmdList, allocator, updater, name) }; }

template <typename AllocT>
D3D12ConstantBuffer::D3D12ConstantBuffer(const AllocT &allocator, const ::CD3DX12_RESOURCE_DESC &desc,
    ::D3D12_RESOURCE_STATES initState, const std::wstring &name)
{ allocate(allocator, desc, initState, nullptr, name); }

template <typename AllocT, typename ItT>
D3D12ConstantBuffer::D3D12ConstantBuffer(const ItT &begin, const ItT &end, const ::CD3DX12_RESOURCE_DESC &desc,
    res::d3d12::D3D12CommandList &cmdList, const AllocT &allocator, D3D12ResourceUpdater &updater,
    const std::wstring &name)
{ allocateUpload(begin, end, desc, cmdList, allocator, updater, nullptr, name); }

}

}

}

// Template implementation end
