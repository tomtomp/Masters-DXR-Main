/**
 * @file helpers/d3d12/D3D12InstancedBuffer.h
 * @author Tomas Polasek
 * @brief Wrapper around D3D12Resource, providing 
 * interface for staging values in CPU memory and 
 * copying them over to the GPU.
 */

#pragma once

#include "engine/helpers/d3d12/D3D12ConstantBuffer.h"
#include "engine/helpers/d3d12/D3D12ResourceView.h"

#include "engine/renderer/ResourceViewType.h"

#include "engine/util/Math.h"

/// Namespace containing the engine code.
namespace quark
{

/// Namespace containing helper classes and methods.
namespace helpers
{

/// Direct3D 12 resource management.
namespace d3d12
{

/**
 * Wrapper around D3D12Resource, providing 
 * interface for staging values in CPU memory and 
 * copying them over to the GPU.
 * Buffer on the GPU side may contain multiple 
 * values of given type, but there is only a 
 * single staging value.
 * @tparam T Type of the value being staged.
 * @tparam Alignment Alignment of instances within 
 * the final buffer memory.
 */
template <typename T, std::size_t Alignment = D3D12_CONSTANT_BUFFER_DATA_PLACEMENT_ALIGNMENT>
class D3D12InstancedBuffer : public D3D12ConstantBuffer, public util::InheritPointerType<res::d3d12::D3D12Resource, D3D12InstancedBuffer<T, Alignment>>
{
public:
    /**
     * Exception thrown when trying to upload data to the GPU 
     * without reallocation, when reallocation is required.
     */
    struct ReallocationRequiredException : public std::exception
    {
        ReallocationRequiredException(const char *msg) :
            std::exception(msg)
        { }
    }; // struct ReallocationRequiredException

    using PtrT = typename util::PointerType<D3D12InstancedBuffer<T, Alignment>>::PtrT;
    using ConstPtrT = typename util::PointerType<D3D12InstancedBuffer<T, Alignment>>::ConstPtrT;

    /// Create unallocated buffer.
    static PtrT create();

    /// Free resource.
    ~D3D12InstancedBuffer();

    /// No copying, allow moving.
    D3D12InstancedBuffer(const D3D12InstancedBuffer &other) = delete;
    D3D12InstancedBuffer &operator=(const D3D12InstancedBuffer &other) = delete;
    D3D12InstancedBuffer(D3D12InstancedBuffer &&other) = default;
    D3D12InstancedBuffer &operator=(D3D12InstancedBuffer &&other) = default;

    /**
     * Is reallocation of the buffer and creation of 
     * new CBV handle required to copy the new values 
     * onto the GPU?
     * @return Returns true if reallocation of buffer is 
     * required.
     */
    bool allocationRequired() const;

    /**
     * Reallocate the buffer and re-create the CBV handles.
     * No action will be taken if allocationRequired() returns 
     * false.
     * @param allocator Allocator used to allocate the buffer.
     * @param name Optional name of the buffer, used for 
     * debugging purposes.
     * @throws util::winexception Thrown on error.
     */
    template <typename AllocT>
    void reallocate(const AllocT &allocator, 
        const std::wstring &name = L"ListBuffer");

    /**
     * Reallocate the buffer and re-create the CBV handles.
     * No action will be taken if reallocationRequired() returns 
     * false. This version used a CommittedAllocator.
     * @param device Buffer will be allocated on this device.
     * @param name Optional name of the buffer, used for 
     * debugging purposes.
     * @throws util::winexception Thrown on error.
     */
    void reallocate(res::d3d12::D3D12Device &device, 
        const std::wstring &name = L"ListBuffer");

    /**
     * Has the list changed since last upload?
     * @return Returns true if uploading of new data 
     * is required.
     */
    bool uploadRequired() const;

    /**
     * Copy the current list to the GPU.
     * @throws ReallocationRequiredException Thrown when 
     * reallocation is required before the upload.
     */
    void uploadToGPU();

    /**
     * Access instance with given index.
     * If the index is larger than the number of instances 
     * then the list of instances will be grown to the new 
     * size.
     * @param index Index of the instance.
     * @return Returns reference to the instance.
     * @warning This operation automatically sets the data 
     * as dirty!
     */
    T& at(std::size_t index);

    /**
     * Access instance with given index.
     * If the index is larger than the number of instances 
     * then the list of instances will be grown to the new 
     * size.
     * @param index Index of the instance.
     * @return Returns reference to the instance.
     * @warning This operation automatically sets the data 
     * as dirty!
     */
    T& operator[](std::size_t index);

    /**
     * Access instance with given index.
     * If the index is larger than the number of instances 
     * then the list of instances will be grown to the new 
     * size.
     * @param index Index of the instance.
     * @return Returns reference to the instance.
     */
    const T& at(std::size_t index) const;

    /**
     * Access instance with given index.
     * If the index is larger than the number of instances 
     * then the list of instances will be grown to the new 
     * size.
     * @param index Index of the instance.
     * @return Returns reference to the instance.
     */
    const T& operator[](std::size_t index) const;

    /**
     * Assign whole data vector to this buffer.
     * @param data Data to copy.
     * @warning This operation automatically sets the data 
     * as dirty!
     */
    void assignCopy(const std::vector<T> &data);

    /**
     * Get CPU CBV descriptor handle for this buffer.
     * The handle may be invalid if descriptors were not allocated.
     * @return Returns the CPU descriptor handle.
     */
    ::CD3DX12_CPU_DESCRIPTOR_HANDLE cpuHandle() const;

    /**
     * Get CPU CBV descriptor handle for this buffer.
     * The handle may be invalid, if descriptors were not allocated.
     * @return Returns the GPU descriptor handle.
     */
    ::CD3DX12_GPU_DESCRIPTOR_HANDLE gpuHandle() const;

    /// Get GPU address to this buffer.
    ::D3D12_GPU_VIRTUAL_ADDRESS gpuAddress() const;

    /**
     * Reallocate descriptor for this buffer.
     * May decide to allocate a new one, if the descriptor 
     * heap is different than the last one.
     * @param descHeap Descriptor heap used for the allocation.
     * @param 
     */
    void reallocateDescriptor(res::d3d12::D3D12DescAllocatorInterface &descHeap, 
        quark::rndr::ResourceViewType type = quark::rndr::ResourceViewType::ConstantBuffer);

    /// has the buffer been reallocated and descriptors invalidated?
    bool descDirty() const;

    /// Generate a shader resource view description for this resource.
    virtual ::D3D12_CONSTANT_BUFFER_VIEW_DESC cbvDesc() const override;

    /// Generate a render-target view description for this resource.
    virtual ::D3D12_RENDER_TARGET_VIEW_DESC rtvDesc() const override;
private:
    /// Size of a single instance aligned to the required size.
    static constexpr std::size_t alignedInstanceSize();

    /// Calculate total required buffer size including alignment.
    std::size_t totalBufferSize() const;

    /// List of staging values.
    std::vector<T> mStaging{ };

    /// Do we need to copy the data?
    bool mDataDirty{ true };

    /// Do we need to reallocate the buffer?
    bool mReallocationRequired{ true };

    /// Optional resource view for this buffer.
    util::PtrT<D3D12ResourceView> mResourceView{ };
protected:
    /// Create unallocated buffer.
    D3D12InstancedBuffer();
}; // class D3D12InstancedBuffer

} // namespace d3d12

} // namespace helpers

} // namespace quark

// Template implementation

namespace quark
{

namespace helpers
{

namespace d3d12
{

template <typename T, std::size_t Alignment>
::quark::helpers::d3d12::D3D12InstancedBuffer<T, Alignment>::D3D12InstancedBuffer()
{ /* Automatic */ }

template<typename T, std::size_t Alignment>
auto D3D12InstancedBuffer<T, Alignment>::create() -> PtrT
{ return PtrT{ new D3D12InstancedBuffer() }; }

template <typename T, std::size_t Alignment>
D3D12InstancedBuffer<T, Alignment>::~D3D12InstancedBuffer()
{ /* Automatic */ }

template <typename T, std::size_t Alignment>
bool D3D12InstancedBuffer<T, Alignment>::allocationRequired() const
{ return mReallocationRequired; }

template <typename T, std::size_t Alignment>
template <typename AllocT>
void D3D12InstancedBuffer<T, Alignment>::reallocate(
    const AllocT &allocator, const std::wstring &name)
{
    if (!allocationRequired())
    { return; }

    D3D12ConstantBuffer::allocate(allocator, totalBufferSize(), ::D3D12_RESOURCE_STATE_GENERIC_READ, name);

    mReallocationRequired = false;
}

template <typename T, std::size_t Alignment>
void D3D12InstancedBuffer<T, Alignment>::reallocate(
    res::d3d12::D3D12Device &device, const std::wstring &name)
{
    if (!allocationRequired())
    { return; }

    D3D12ConstantBuffer::allocate(device, totalBufferSize(), ::D3D12_RESOURCE_STATE_GENERIC_READ, name);

    mReallocationRequired = false;
}

template <typename T, std::size_t Alignment>
bool D3D12InstancedBuffer<T, Alignment>::uploadRequired() const
{ return mDataDirty; }

template <typename T, std::size_t Alignment>
void D3D12InstancedBuffer<T, Alignment>::uploadToGPU()
{
    if (allocationRequired())
    { throw ReallocationRequiredException("Unable to upload data to the GPU: Reallocation of the constant buffer is required!"); }

    auto *basePtr{ D3D12ConstantBuffer::mapForCpuWrite() };
    { // Copy the data over to the GPU.
        if (Alignment > 1u)
        { // Re-align the data.
            for (std::size_t iii = 0u; iii < mStaging.size(); ++iii)
            {
                auto *dstPtr{ basePtr + iii * alignedInstanceSize() };
                std::memcpy(dstPtr, &mStaging[iii], sizeof(T));
            }
        }
        else
        { // Copy as-is.
            auto *dstPtr{ basePtr };
            std::memcpy(dstPtr, mStaging.data(), mStaging.size() * sizeof(T));
        }
    } D3D12ConstantBuffer::unmapFullRewrite();

    mDataDirty = false;
}

template <typename T, std::size_t Alignment>
T &D3D12InstancedBuffer<T, Alignment>::at(std::size_t index)
{
    if (mStaging.size() <= index)
    { 
        mStaging.resize(index + 1u); 
        mReallocationRequired = true;
    }

    mDataDirty = true;
    return mStaging[index];
}

template <typename T, std::size_t Alignment>
T &D3D12InstancedBuffer<T, Alignment>::operator[](std::size_t index)
{ return at(index); }

template <typename T, std::size_t Alignment>
const T &D3D12InstancedBuffer<T, Alignment>::at(std::size_t index) const
{
    if (mStaging.size() <= index)
    { throw std::exception("Out of bounds access to the constant buffer staging list!"); }
    
    return mStaging[index];
}

template <typename T, std::size_t Alignment>
const T &D3D12InstancedBuffer<T, Alignment>::operator[](std::size_t index) const
{ return at(index); }

template <typename T, std::size_t Alignment>
void D3D12InstancedBuffer<T, Alignment>::assignCopy(const std::vector<T> &data)
{
    mDataDirty = true;
    // TODO - Reallocate even when the new size is smaller to conserve space?
    // We may need to reallocate if not enough space is available.
    mReallocationRequired = data.size() > mStaging.size();

    mStaging = data;

    return;
}

template <typename T, std::size_t Alignment>
::CD3DX12_CPU_DESCRIPTOR_HANDLE D3D12InstancedBuffer<T, Alignment>::cpuHandle() const
{ return mResourceView ? mResourceView->cpuHandle() : ::CD3DX12_CPU_DESCRIPTOR_HANDLE{ }; }

template <typename T, std::size_t Alignment>
::CD3DX12_GPU_DESCRIPTOR_HANDLE D3D12InstancedBuffer<T, Alignment>::gpuHandle() const
{ return mResourceView ? mResourceView->gpuHandle() : ::CD3DX12_GPU_DESCRIPTOR_HANDLE{ }; }

template <typename T, std::size_t Alignment>
::D3D12_GPU_VIRTUAL_ADDRESS D3D12InstancedBuffer<T, Alignment>::gpuAddress() const
{ return get()->GetGPUVirtualAddress(); }

template <typename T, std::size_t Alignment>
constexpr std::size_t D3D12InstancedBuffer<T, Alignment>::alignedInstanceSize()
{ return util::math::alignTo(sizeof(T), Alignment); }

template <typename T, std::size_t Alignment>
std::size_t D3D12InstancedBuffer<T, Alignment>::totalBufferSize() const
{ return util::math::alignTo(mStaging.size() * alignedInstanceSize(), D3D12_CONSTANT_BUFFER_DATA_PLACEMENT_ALIGNMENT); }

template<typename T, std::size_t Alignment>
void D3D12InstancedBuffer<T, Alignment>::reallocateDescriptor(
    res::d3d12::D3D12DescAllocatorInterface &descHeap, quark::rndr::ResourceViewType type)
{
    if (!mResourceView || mResourceView->type() != type)
    { mResourceView = D3D12ResourceView::create(D3D12Resource::shared_from_this(), descHeap, type); }

    mResourceView->reallocateDescriptor(descHeap);
}

template<typename T, std::size_t Alignment>
bool D3D12InstancedBuffer<T, Alignment>::descDirty() const
{ return !mResourceView || !mResourceView->valid(); }

template<typename T, std::size_t Alignment>
::D3D12_CONSTANT_BUFFER_VIEW_DESC D3D12InstancedBuffer<T, Alignment>::cbvDesc() const
{
    // Start with pre-generated description.
    auto result{ D3D12Resource::cbvDesc() };

    // No further changes necessary.
    return result;
}

template<typename T, std::size_t Alignment>
::D3D12_RENDER_TARGET_VIEW_DESC D3D12InstancedBuffer<T, Alignment>::rtvDesc() const
{
    // Start with pre-generated description.
    auto result{ D3D12Resource::rtvDesc() };

    // No further changes necessary.
    return result;
}

}

}

}

// Template implementation end
