/**
 * @file helpers/d3d12/D3D12RTAccelerationBuilder.h
 * @author Tomas Polasek
 * @brief Helper used for building D3D12 acceleration structures 
 * for ray tracing.
 */

#pragma once

#ifdef ENGINE_RAY_TRACING_ENABLED

#include "engine/resources/d3d12/D3D12CommandQueueMgr.h"
#include "engine/resources/d3d12/D3D12RayTracingDevice.h"
#include "engine/resources/render/GpuMemoryMgr.h"
#include "engine/resources/render/MeshMgr.h"

#include "engine/helpers/d3d12/D3D12RTBottomLevelAS.h"
#include "engine/helpers/d3d12/D3D12RTTopLevelAS.h"
#include "engine/helpers/d3d12/D3D12DescTable.h"
#include "engine/helpers/math/Transform.h"

/// Namespace containing the engine code.
namespace quark
{

/// Namespace containing helper classes and methods.
namespace helpers
{

/// Direct3D 12 resource management.
namespace d3d12
{

/**
 * Helper used for building D3D12 acceleration structures 
 * for ray tracing.
 */
class D3D12RTAccelerationBuilder : public util::PointerType<D3D12RTAccelerationBuilder>
{
public:
    /// Exception thrown when trying to use invalid geometry handle.
    struct UnknownGeometryException : public std::exception
    {
        UnknownGeometryException(const char *msg) :
            std::exception(msg)
        { }
    }; // struct UnknownGeometryException

    /// Exception thrown when problems with descriptor heap occur.
    struct DescriptorException : public std::exception
    {
        DescriptorException(const char *msg) :
            std::exception(msg)
        { }
    }; // struct DescriptorException

    /// Handle to added geometry.
    using GeometryHandleT = std::size_t;

    /// Geometry handle pointing to nothing.
    static constexpr GeometryHandleT NullGeometryHandle{ 0u };

    /// Holder for statistical data about the acceleration structures.
    struct Statistics
    {
        /// Initialize the statistics.
        Statistics();

        /// Clear all of the statistics, setting them to default values.
        void clear();

        /// Generate statistics about the current acceleration structures.
        std::string generateStatistics() const;

        /// Add information about new bottom level acceleration structure.
        void addBlasInfo(std::size_t sizeInBytes, std::size_t numTriangles, std::size_t numMeshes);
        /// Add information about new top level acceleration structure.
        void addTlasInfo(std::size_t sizeInBytes, std::size_t numInstances);
        /// Add information about new scratch buffer.
        void addScratchInfo(std::size_t sizeInBytes);

        /// Reset statistics for bottom level acceleration structures.
        void resetBlasInfo();
        /// Reset statistics for bottom level acceleration structures.
        void resetTlasInfo();
        /// Reset statistics for the scratch buffer.
        void resetScratchInfo();

        std::size_t maximumBlasSize{ };
        std::size_t minimumBlasSize{ };
        std::size_t totalBlasSizes{ };
        std::size_t numBlasCreated{ };

        std::size_t maximumBlasTriangles{ };
        std::size_t minimumBlasTriangles{ };
        std::size_t totalBlasTriangles{ };

        std::size_t maximumBlasMeshes{ };
        std::size_t minimumBlasMeshes{ };
        std::size_t totalBlasMeshes{ };

        std::size_t maximumTlasSize{ };
        std::size_t minimumTlasSize{ };
        std::size_t currentTlasSize{ };
        std::size_t totalTlasSizes{ };
        std::size_t numTlasCreated{ };

        std::size_t maximumTlasInstances{ };
        std::size_t minimumTlasInstances{ };
        std::size_t totalTlasInstances{ };

        std::size_t maximumScratchSize{ };
        std::size_t minimumScratchSize{ };
        std::size_t currentScratchSize{ };
        std::size_t totalScratchSizes{ };
        std::size_t numScratchCreated{ };
    }; // struct Statistics

    /**
     * Initialize the ray tracing acceleration structure builder 
     * for given device. 
     * @param device Target device, where ray tracing will occur.
     * @param memoryMgr Memory management system which should be 
     * used for GPU memory allocation for the acceleration structure.
     */
    static PtrT create(res::d3d12::D3D12RayTracingDevice &device, 
        res::rndr::GpuMemoryMgr &memoryMgr);

    /// Free any resources used by the acceleration structure.
    ~D3D12RTAccelerationBuilder();

    // No copying, allow moving.
    D3D12RTAccelerationBuilder(const D3D12RTAccelerationBuilder &other) = delete;
    D3D12RTAccelerationBuilder &operator=(const D3D12RTAccelerationBuilder &other) = delete;
    D3D12RTAccelerationBuilder(D3D12RTAccelerationBuilder &&other) = default;
    D3D12RTAccelerationBuilder &operator=(D3D12RTAccelerationBuilder &&other) = default;

    /// Clear the acceleration structure and any configuration.
    void clear();

    /**
     * Only clear instances and the top level of the acceleration structure.
     * All geometry handles and their acceleration structures remain intact.
     */
    void clearInstances();

    /**
     * Add new geometry to the acceleration structure.
     * @param primitive Primitive representing the geometry.
     * @param name Name of the primitive for debug purposes.
     * @return Returns geometry handle which represents the 
     * primitive. Returns NullGeometryHandle if the primitive 
     * is invalid.
     */
    GeometryHandleT addGeometryNoTransform(const res::rndr::Primitive &primitive, 
        const std::wstring &name = L"");

    /**
     * Add new geometry to the acceleration structure.
     * @param meshHandle Mesh representing the geometry. All 
     * primitives within this mesh will be added under a single 
     * instantiable handle.
     * @param name Name of the primitive for debug purposes.
     * @param fastBuild Set the flag for fast building of AS, which 
     * will not be as optimal.
     * @param noDuplication Don't duplicate already added meshes.
     * @return Returns geometry handle, representing the mesh as 
     * a whole, which can be made of multiple primitives. Returns 
     * NullGeometryHandle if there are not valid primitives.
     */
    GeometryHandleT addGeometryNoTransform(const res::rndr::MeshHandle &meshHandle, 
        const std::wstring &name = L"", bool fastBuild = false, bool noDuplication = true);

    /**
     * Get a unique number associated with given geometry handle.
     * This number can be for example used for indexing meshes and 
     * materials.
     * Hashes start at 0 and go up for each new added geometry by 
     * one. This allows their use in indexing array structures.
     * @param geometry Handle to the geometry.
     * @return Returns unique number, which can be used as a hash.
     * @throws UnknownGeometryException Thrown when provided geometry 
     * handle is not valid.
     */
    std::size_t geometryUniqueHash(const GeometryHandleT &geometry) const;

    /**
     * Get number of primitives within specified geometry.
     * @param geometry Handle to the geometry.
     * @return Returns number of primitives, or geometry 
     * descriptions, within the bottom level acceleration 
     * structure.
     * @throws UnknownGeometryException Thrown when provided geometry 
     * handle is not valid.
     */
    std::size_t geometryPrimitiveCount(const GeometryHandleT &geometry) const;

    /**
     * Add instance of given geometry to the scene.
     * @param geometry Handle specifying which geometry will be used 
     * by this instance.
     * @param transform Transform of the instance within the scene. 
     * Only 3x4 part of the matrix will be used - no projection.
     * @param shaderTableOffset Offset of this instance when choosing 
     * shaders within the bound shader table.
     * @param instanceID Value accessible from ray tracing shaders, 
     * allowing specialization of per-instance data.
     * @param instanceMask 8 bit mask used for ray tracing only some 
     * objects. Set to zero to never include the instance.
     * @param flags Flags used for this instance.
     * @throw UnknownGeometryException Thrown when invalid geometry 
     * handle is provided.
     */
    void addInstance(const GeometryHandleT &geometry, const helpers::math::Transform &transform, 
        ::UINT shaderTableOffset = 0u, ::UINT instanceID = 0u, ::UINT instanceMask = 1u, 
        ::D3D12_RAYTRACING_INSTANCE_FLAGS flags = ::D3D12_RAYTRACING_INSTANCE_FLAG_NONE);

    /**
     * Build current configuration of acceleration structure.
     * @param directCmdList Command list used to record the building 
     * commands.
     * @param buildDescTable Descriptor table used for the build operation. 
     * Size of this table must be at least requiredDescriptors().
     * @warning buildDescHeap must have at least a number of allocatable 
     * descriptors as the number returned by requiredDescriptors(), after 
     * adding all required geometry and instances. This heap must allow 
     * CBV, SRV and UAV records, and must have SHADER_VISIBLE flag enabled.
     * @throw util::winexception Thrown when error occurs.
     * @throw DescriptorException Thrown when given descriptor table is 
     * not bound or is not large enough.
     */
    void buildAccelerationStructure(res::d3d12::D3D12CommandList &directCmdList,
        D3D12DescTable &buildDescTable);

    /**
     * Get number of required descriptors, when building the acceleration 
     * structure. Should be called each time a change is made to the number 
     * of geometry or instances.
     * @return Returns number of required free descriptor handles within 
     * buildDescHeap provided when building the acceleration structure.
     */
    uint32_t requiredDescriptors() const;

    /**
     * Get pointer to the top level acceleration structure.
     * @warning The acceleration structure must be built!
     */
    ::WRAPPED_GPU_POINTER getTopLevelAccelerationStruture() const;

    /// Generate statistics about the current state of the acceleration structures.
    std::string generateStatisticsString() const;

    /// Get current building statistics.
    const Statistics &statistics() const;

    /// Focus the build on fast AS.
    void preferFastRayTracing();

    /// Focus the build on finishing as fast as possible.
    void preferFastBuild();
private:
    /**
     * Wrapper for all data required to build the bottom level 
     * acceleration structure.
     */
    class GeometryHolder
    {
    public:
        /// Wrapper around data for a single instantiable geometry.
        struct GeometryInfo
        {
            /// Acceleration structure wrapper for this geometry.
            D3D12RTBottomLevelAS accelerationStructure{ };
            /// Buffer containing AS for this geometry. May be empty!
            res::d3d12::D3D12Resource::PtrT accelerationStructureBuffer{ };
            /// Wrapped pointer for the acceleration structure buffer.
            ::WRAPPED_GPU_POINTER wrappedPointer{ };
            /// Name of the geometry used for debugging purposes.
            std::wstring name{ };

            /// Has this geometry been already build?
            bool built() const
            { return accelerationStructureBuffer && accelerationStructureBuffer->valid(); }
        }; // struct GeometryInfo

        /**
         * Add new geometry.
         * @param primitive Primitive representing the geometry.
         * @param name Name of the primitive for debug purposes.
         * @param fastBuild Prefer building speed over quality?
         * @return Returns handle to the geometry. Returns 
         * NullGeometryHandle if the primitive is invalid.
         */
        GeometryHandleT addGeometryNoTransform(const res::rndr::Primitive &primitive, 
            const std::wstring &name, bool fastBuild);

        /**
         * Add new geometry.
         * @param meshHandle Mesh representing the geometry.
         * @param name Name of the primitive for debug purposes.
         * @param noDuplication Don't duplicate already created 
         * meshes.
         * @return Returns handle to the geometry. Returns 
         * NullGeometryHandle if there are not valid primitives.
         */
        GeometryHandleT addGeometryNoTransform(const res::rndr::MeshHandle &meshHandle, 
            const std::wstring &name, bool fastBuild, bool noDuplication = true);

        /**
         * Check whether given geometry handle is valid.
         * @param handle Handle to check.
         * @return Returns true if the handle is valid, else 
         * returns false.
         */
        bool checkGeometryHandle(const GeometryHandleT &handle) const;

        /// Get iterable object for all instantiable geometry handles.
        auto foreachGeometry() const
        { return util::Range<GeometryHandleT>(1u, mGeometry.size() + 1u); }

        /// Clear all geometry.
        void clear();

        /**
         * Access geometry using its handle.
         * @param handle Handle to the geometry.
         * @return Returns reference to the geometry information 
         * structure.
         * @throws UnknownGeometryException Thrown when provided handle 
         * is invalid.
         */
        GeometryInfo &getGeometry(const GeometryHandleT &handle);

        /**
         * Access geometry using its handle.
         * @param handle Handle to the geometry.
         * @return Returns reference to the geometry information 
         * structure.
         * @throws UnknownGeometryException Thrown when provided handle 
         * is invalid.
         */
        const GeometryInfo &getGeometry(const GeometryHandleT &handle) const;

        /**
         * Calculate size required for the scratch buffer.
         * Only un-built geometry is taken into consideration.
         * @param device Device used for ray tracing purposes.
         * @return Returns required size of the scratch buffer in bytes.
         */
        std::size_t calculateScratchSize(res::d3d12::D3D12RayTracingDevice &device);

        /**
         * Convert handle to index into the lists.
         * @param handle Handle to geometry.
         * @return Returns index to mGeometry and mBuildInformation lists.
         * @throw UnknownGeometryException Thrown when invalid handle is 
         * provided.
         */
        std::size_t geometryHandleToIndex(const GeometryHandleT &handle) const;

        /**
         * Get number of primitives within specified geometry.
         * @param handle Handle to the geometry.
         * @return Returns number of primitives, or geometry 
         * descriptions, within the bottom level acceleration 
         * structure.
         * @throws UnknownGeometryException Thrown when provided geometry 
         * handle is not valid.
         */
        std::size_t geometryPrimitiveCount(const GeometryHandleT &handle) const;
    private:
        /**
         * Register a mesh, which has been added to this holder.
         * @param meshHandle Handle to the mesh, used to generate unique ID.
         * @param handle Handle to the geometry, made from the mesh.
         */
        void registerMesh(const res::rndr::MeshHandle &meshHandle, const GeometryHandleT &handle);

        /**
         * Attempt to get already added mesh geometry handle, by its mesh 
         * handle.
         * @param meshHandle Handle to the mesh.
         * @return Returns handle to geometry corresponding to the mesh handle 
         * or invalid handle if there is no such mesh added.
         */
        GeometryHandleT getMeshGeometryHandle(const res::rndr::MeshHandle &meshHandle);

        /// List of currently managed geometry.
        std::deque<GeometryInfo> mGeometry{ };

        /// Mapping to already added mesh geometry handles.
        std::map<std::size_t, GeometryHandleT> mMeshToHandle;
    protected:
    }; // class GeometryHolder

    /**
     * Wrapper for all data required to build the top level 
     * acceleration structure.
     */
    class InstanceHolder
    {
    public:
        /// Top level acceleration structure configurator and builder.
        D3D12RTTopLevelAS accelerationStructure{ };
        /// Buffer containing AS for the instances. May be empty!
        res::d3d12::D3D12Resource::PtrT accelerationStructureBuffer{ };
        /// Wrapped pointer for the acceleration structure buffer.
        ::WRAPPED_GPU_POINTER wrappedPointer{ };
        /// Buffer used for storing the instance descriptions.
        res::d3d12::D3D12Resource::PtrT instanceBuffer{ };

        /**
         * Process all added instances, finalizing the process and 
         * setting the correct bottom level acceleration structure 
         * pointers.
         * @param noDuplication Don't duplicate already created 
         * meshes.
         * @param geometry Holder of geometry, which has finished 
         * building of the AS.
         */
        void processInstances(GeometryHolder &geometry, bool fastBuild);

        /// Add instance with provided data.
        void addInstance(const GeometryHandleT &geometry, const helpers::math::Transform &transform, 
            ::UINT shaderTableOffset, ::UINT instanceID, ::UINT instanceMask, 
            ::D3D12_RAYTRACING_INSTANCE_FLAGS flags);

        /// Clear all of the structures.
        void clear();

        /**
         * Calculate size required for the scratch buffer.
         * @param device Device used for ray tracing purposes.
         * @return Returns required size of the scratch buffer in bytes.
         */
        std::size_t calculateScratchSize(res::d3d12::D3D12RayTracingDevice &device);

        /// Have there been instances added since the last buildComplete()?
        bool addedInstances() const;

        /// Reset the added instances flag.
        void buildComplete();
    private:
        /// Helper used for storing instance information.
        struct InstanceRecord
        {
            /// Handle to the target instance.
            D3D12RTTopLevelAS::InstanceHandleT instanceHandle{ };
            /// Appointed geometry handle.
            GeometryHandleT geometry{ };
        }; // struct InstanceRecord

        /// List of instances which need to be added.
        std::vector<InstanceRecord> mInstancesToProcess{ };
        /// Are there any changes to the added instances?
        bool mAddedInstances{ true };
    protected:
    }; // struct InstanceHolder

    /**
     * Check whether given geometry handle is valid.
     * @param handle Handle being checked.
     * @return Returns true if the handle is valid, else 
     * returns false.
     */
    bool checkGeometryHandle(const GeometryHandleT &handle) const;

    /**
     * Create the scratch buffer used for building of the acceleration 
     * structures bottom and top levels.
     * @param sizeInBytes Required size of the buffer in bytes.
     * @return Returns resource which can be used as scratch buffer in 
     * building acceleration structures.
     * @throw util::winexception Thrown when error occurs.
     */
    res::d3d12::D3D12Resource::PtrT createScratchBuffer(std::size_t sizeInBytes);

    /**
     * Make sure, the scratch buffer is at least sizeInBytes large.
     * @param sizeInBytes How many bytes is the minimal accepted size 
     * of the scratch buffer.
     */
    void assureScratchSize(std::size_t sizeInBytes);

    /**
     * Get initial state required by the acceleration structure buffers.
     * @return Returns the state required.
     * @throw util::winexception Thrown on error.
     */
    ::D3D12_RESOURCE_STATES getInitialASBufferState();

    /**
     * Create the bottom level acceleration structure buffer.
     * @param sizeInBytes Required size of the buffer in bytes.
     * @param initialASBufferState Initial state of the buffer, gained by 
     * calling getInitialASBufferState().
     * @param name Name used for debugging purposes.
     * @return Returns the created buffer.
     * @throw util::winexception Thrown on error.
     */
    res::d3d12::D3D12Resource::PtrT createBottomLevelBuffer(
        std::size_t sizeInBytes, 
        ::D3D12_RESOURCE_STATES initialASBufferState, 
        const std::wstring &name = L"");

    /**
     * Create the top level acceleration structure buffer.
     * @param sizeInBytes Required size of the buffer in bytes.
     * @param initialASBufferState Initial state of the buffer, gained by 
     * calling getInitialASBufferState().
     * @return Returns the created buffer.
     * @throw util::winexception Thrown on error.
     */
    res::d3d12::D3D12Resource::PtrT createTopLevelBuffer(
        std::size_t sizeInBytes, 
        ::D3D12_RESOURCE_STATES initialASBufferState);

    /**
     * Create a wrapped pointer to given resource and create a wrapping 
     * descriptor if necessary.
     * @param buildDescTable Descriptor table used for creating the descriptors.
     * @param resource Resource which should be wrapped.
     * @return Returns the wrapped pointer.
     */
    ::WRAPPED_GPU_POINTER createWrapped(
        helpers::d3d12::D3D12DescTable &buildDescTable, 
        res::d3d12::D3D12Resource &resource) const;

    /**
     * Build the bottom level acceleration structures, recording commands on 
     * provided command list. Uses geometry container, which must contain 
     * prepared build information.
     * @param geometry Holder containing the geometry.
     * @param scratchBuffer Scratch buffer used to build the AS.
     * @param directCmdList Command list used for recording of building commands.
     * @param buildDescHeap Descriptor heap used for building.
     * @param initialASBufferState Initial state of the buffer, gained by 
     * calling getInitialASBufferState().
     * @throw util::winexception Thrown on error.
     */
    void buildBottomLevelAccelerationStructures(GeometryHolder &geometry,
        res::d3d12::D3D12Resource &scratchBuffer, 
        res::d3d12::D3D12CommandList &directCmdList, 
        D3D12DescTable &buildDescHeap, ::D3D12_RESOURCE_STATES initialASBufferState);

    /**
     * Create the instance buffer required to build the top level AS.
     * @return Returns the created instance buffer.
     * @throw util::winexception Thrown on error.
     */
    res::d3d12::D3D12Resource::PtrT createInstanceBuffer(std::size_t sizeInBytes);

    /**
     * Build the top level acceleration structure, recording commands on 
     * provided command list. 
     * @param instances Holder containing the instances.
     * @param scratchBuffer Scratch buffer used to build the AS.
     * @param directCmdList Command list used for recording of building commands.
     * @param buildDescTable Descriptor table used for building.
     * @param initialASBufferState Initial state of the buffer, gained by 
     * calling getInitialASBufferState().
     * @throw util::winexception Thrown on error.
     */
    void buildTopLevelAccelerationStructure(InstanceHolder &instances, 
        res::d3d12::D3D12Resource &scratchBuffer, 
        res::d3d12::D3D12CommandList &directCmdList, 
        D3D12DescTable &buildDescTable, 
        ::D3D12_RESOURCE_STATES initialASBufferState);

    /**
     * Finalize building the acceleration structure.
     * @param directCmdList Command list used for recording of building commands.
     * @throw util::winexception Thrown on error.
     */
    void finalizeAccelerationStructure(res::d3d12::D3D12CommandList &directCmdList);

    /// Target device which will use the ray tracing acceleration structure.
    res::d3d12::D3D12RayTracingDevice *mDevice{ nullptr };
    /// Memory management system used for GPU memory allocation.
    res::rndr::GpuMemoryMgr *mMemoryMgr{ nullptr };

    /// Flag specifying whether the structure needs to be re-built.
    bool mDirty{ true };

    /// Statistics about the acceleration structure.
    Statistics mStatistics{ };
    /// Holder of geometry and their bottom level acceleration structures.
    GeometryHolder mGeometryHolder{ };
    /// Holder of instances and their top level acceleration structure.
    InstanceHolder mInstanceHolder{ };

    /// Buffer used when constructing the acceleration structure.
    res::d3d12::D3D12Resource::PtrT mScratchBuffer{ };

    /// Build acceleration structures optimized for fast ray tracing?
    bool mBuildOptimized{ true };
protected:
    /// Uninitialized acceleration structure builder.
    D3D12RTAccelerationBuilder();

    /**
     * Initialize the ray tracing acceleration structure builder 
     * for given device. 
     * @param device Target device, where ray tracing will occur.
     * @param memoryMgr Memory management system which should be 
     * used for GPU memory allocation for the acceleration structure.
     */
    D3D12RTAccelerationBuilder(res::d3d12::D3D12RayTracingDevice &device, 
        res::rndr::GpuMemoryMgr &memoryMgr);
}; // class D3D12RTAccelerationBuilder

} // namespace d3d12

} // namespace helpers

} // namespace quark

// Template implementation

namespace quark
{

namespace helpers
{

namespace d3d12
{

}

}

}

// Template implementation end

#endif 
