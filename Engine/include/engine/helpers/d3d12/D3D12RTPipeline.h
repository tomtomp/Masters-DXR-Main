/**
 * @file helpers/d3d12/D3D12RTPipeline.h
 * @author Tomas Polasek
 * @brief Wrapper around ray tracing pipeline and its state.
 */

#pragma once

#ifdef ENGINE_RAY_TRACING_ENABLED

#include "engine/resources/d3d12/D3D12RayTracingDevice.h"

#include "engine/helpers/d3d12/D3D12RTStateObjectBuilder.h"
#include "engine/helpers/d3d12/D3D12RTShaderLibraryRegistry.h"
#include "engine/helpers/d3d12/D3D12RTRootSignatureRegistry.h"
#include "engine/helpers/d3d12/D3D12RTHitGroupRegistry.h"
#include "engine/helpers/d3d12/D3D12RTAccelerationBuilder.h"

/// Namespace containing the engine code.
namespace quark
{

/// Namespace containing helper classes and methods.
namespace helpers
{

/// Direct3D 12 resource management.
namespace d3d12
{

/**
 * Wrapper around ray tracing pipeline and its state.
 */
class D3D12RTPipeline : public util::PointerType<D3D12RTPipeline>
{
public:
    /// Un-configured ray tracing pipeline.
    static PtrT create();

    /// Free any resources used by the pipeline.
    ~D3D12RTPipeline();

    // No copying, allow moving.
    D3D12RTPipeline(const D3D12RTPipeline &other) = delete;
    D3D12RTPipeline &operator=(const D3D12RTPipeline &other) = delete;
    D3D12RTPipeline(D3D12RTPipeline &&other) = default;
    D3D12RTPipeline &operator=(D3D12RTPipeline &&other) = default;

    /// Reset the state of the pipeline to default state.
    void clear();

    /// Set maximal size of the ray payload.
    void setMaxPayloadSize(std::size_t sizeInBytes);
    /// Set maximal size of the attributes structure.
    void setMaxAttributeSize(std::size_t sizeInBytes);
    /// Set maximal depth of ray tracing recursion.
    void setMaxRecursiveDepth(std::size_t depth);

    /**
     * Associate a shader library with its entry points.
     * Works with unregistered shader libraries, they will just be 
     * registered, before their association.
     * @param byteCode Compiled byte code of the shader library.
     * @param byteCodeLength Length of the byte code.
     * @param identifiers Entry points to be associated with the 
     * shader library.
     */
    void addShaderLibrary(const void *byteCode, std::size_t byteCodeLength, 
        const std::initializer_list<std::wstring> &identifiers);

    /**
     * Add a new hit group under a given name.
     * @param hitGroupName Identifier of the whole hit group.
     * @param closestHitName Identifier of the closest hit 
     * shader.
     * @param anyHitName Identifier of the any hit shader.
     * @param intersectionName Identifier of the intersection 
     * shader.
     * @param type Type of primitives this hit group will be 
     * used for.
     * @throws AlreadyRegisteredException Thrown when a hit 
     * group with given name already exists.
     */
    void addHitGroup(
        const std::wstring &hitGroupName, 
        const std::wstring &closestHitName, 
        const std::wstring &anyHitName = L"", 
        const std::wstring &intersectionName = L"", 
        ::D3D12_HIT_GROUP_TYPE type = ::D3D12_HIT_GROUP_TYPE_TRIANGLES);

    /**
     * Associate a list of shader entry points with provided 
     * local root signature.
     * @param rootSignature Root signature used.
     * @param identifiers List of identifiers which should use this 
     * root signature.
     */
    void useLocalRootSignature(res::d3d12::D3D12RootSignature &rootSignature, 
        const std::initializer_list<std::wstring> &identifiers);

    /**
     * Associate a list of shader entry points with provided 
     * global root signature.
     * @param rootSignature Root signature used.
     * @param identifiers List of identifiers which should use this 
     * root signature.
     */
    void useGlobalRootSignature(res::d3d12::D3D12RootSignature &rootSignature, 
        const std::initializer_list<std::wstring> &identifiers);

    /**
     * Get the pipeline state object, re-building it as necessary.
     * @param device Device used for ray tracing purposes.
     * @return Returns the build pipeline state object.
     */
    res::d3d12::D3D12RayTracingStateObject::PtrT &build(
        res::d3d12::D3D12RayTracingDevice &device);

    /**
     * Access already built pipeline object.
     * @warning The pipeline must have been built before!
     */
    res::d3d12::D3D12RayTracingStateObject::PtrT &get();
private:
    /// Add given libraries into the provided builder.
    void buildShaderLibraries(D3D12RTStateObjectBuilder &builder, 
        D3D12RTShaderLibraryRegistry &libraries);

    /// Add given hit groups into the provided builder.
    void buildHitGroups(D3D12RTStateObjectBuilder &builder, 
        D3D12RTHitGroupRegistry &hitGroups);

    /// Add given local root signature associations in to the provided builder.
    void buildLocalRootSignatures(D3D12RTStateObjectBuilder &builder, 
        D3D12RTRootSignatureRegistry &localRootSignatures);

    /// Add given global root signature associations in to the provided builder.
    void buildGlobalRootSignatures(D3D12RTStateObjectBuilder &builder, 
        D3D12RTRootSignatureRegistry &globalRootSignatures);

    /// Add miscellaneous configurations into the provided builder.
    void buildConfigurations(D3D12RTStateObjectBuilder &builder, 
        UINT maxPayloadSize, UINT maxAttributeSize, UINT maxRayTracingRecursion);

    /// List of known shader libraries and their entry points.
    D3D12RTShaderLibraryRegistry mShaderLibraries{ };

    /// List of added hit groups.
    D3D12RTHitGroupRegistry mHitGroups{ };

    /// Shader association for local root signatures.
    D3D12RTRootSignatureRegistry mLocalRootSignatures{ };
    /// Shader association for global root signatures.
    D3D12RTRootSignatureRegistry mGlobalRootSignatures{ };

    /// Dirty flag used for detecting whether we need to rebuild the pipeline.
    bool mDirty{ true };
    /// Maximal size of the ray payload in bytes.
    ::UINT mMaxPayloadSize{ 0u };
    /// Maximal size of the attributes structure in bytes.
    ::UINT mMaxAttributeSize{ 0u };
    /// Maximal recursive depth of the ray tracing.
    ::UINT mMaxRayTracingRecursion{ 0u };

    /// Compiled pipeline state object. May be invalid if mDirty.
    res::d3d12::D3D12RayTracingStateObject::PtrT mPipeline{ };
protected:
    /// Un-configured ray tracing pipeline.
    D3D12RTPipeline();
}; // class D3D12RTPipeline

} // namespace d3d12

} // namespace helpers

} // namespace quark

// Template implementation

namespace quark
{

namespace helpers
{

namespace d3d12
{

}

}

}

// Template implementation end

#endif 
