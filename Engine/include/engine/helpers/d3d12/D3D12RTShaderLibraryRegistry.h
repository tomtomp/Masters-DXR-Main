/**
 * @file helpers/d3d12/D3D12RTShaderLibraryRegistry.h
 * @author Tomas Polasek
 * @brief Wrapper around a list of shader libraries and their 
 * associated entry points.
 */

#pragma once

#ifdef ENGINE_RAY_TRACING_ENABLED

#include "engine/resources/d3d12/D3D12Shader.h"

/// Namespace containing the engine code.
namespace quark
{

/// Namespace containing helper classes and methods.
namespace helpers
{

/// Direct3D 12 resource management.
namespace d3d12
{

/**
 * Wrapper around a list of shader libraries and their 
 * associated entry points.
 */
class D3D12RTShaderLibraryRegistry
{
public:
    /**
     * Exception thrown when an association is requested 
     * to an unknown library name.
     */
    struct UnknownNameException : public std::exception
    {
        UnknownNameException(const char *msg) :
            std::exception(msg)
        { }
    }; // struct UnknownNameException

    /**
     * Exception thrown when trying to register the same 
     * library with 2 different names. Can also be thrown 
     * when the same name is used twice.
     */
    struct AlreadyRegisteredException : public std::exception
    {
        AlreadyRegisteredException(const char *msg) :
            std::exception(msg)
        { }
    }; // struct AlreadyRegisteredException

    /// Wrapper around data needed for a shader library.
    struct ShaderLibrary
    {
        /// Pointer to the shader byte code.
        const void *byteCode{ nullptr };
        /// Length of the shader byte code in bytes.
        std::size_t byteCodeLength{ 0u };
    }; // struct ShaderLibrary

    /// Empty shader registry.
    D3D12RTShaderLibraryRegistry();

    /// Free any resources used by the registry.
    ~D3D12RTShaderLibraryRegistry();

    // No copying, allow moving.
    D3D12RTShaderLibraryRegistry(const D3D12RTShaderLibraryRegistry &other) = delete;
    D3D12RTShaderLibraryRegistry &operator=(const D3D12RTShaderLibraryRegistry &other) = delete;
    D3D12RTShaderLibraryRegistry(D3D12RTShaderLibraryRegistry &&other) = default;
    D3D12RTShaderLibraryRegistry &operator=(D3D12RTShaderLibraryRegistry &&other) = default;

    /// Reset the associations and the library database.
    void clear();

    /**
     * Register given shader library in this registry.
     * A copy of given byte-code will be made.
     * @param library Shader library to register.
     * @param name Optional name of the shader library, under which it 
     * can later be used. When set to empty string, the association 
     * will not be usable with a name.
     * @throws AlreadyRegisteredException Thrown when trying to register 
     * the same shader library with multiple non-empty names.
     */
    void registerShaderLibrary(const ShaderLibrary &library, 
        const std::wstring &name = L"");

    /**
     * Associate a shader library with its entry points.
     * Works with unregistered shader libraries, they will just be 
     * registered, before their association.
     * @param library Shader library being associated.
     * @param entryPoints Entry points to be associated with the 
     * shader library.
     */
    void associateEntryPoints(const ShaderLibrary &library, 
        const std::initializer_list<std::wstring> &entryPoints);

    /**
     * Associate a shader library with its entry points.
     * @param libraryName Registered name of the shader library.
     * @param entryPoints Entry points to be associated with the 
     * shader library.
     * @throws UnknownNameException Thrown when provided root signature 
     * name has not been registered before.
     */
    void associateEntryPoints(const std::wstring &libraryName, 
        const std::initializer_list<std::wstring> &entryPoints);

    /// Access the registered shader libraries.
    const auto &libraries() const
    { return mRegistry; }
private:
    /**
     * Helper structure for holding information about entry  
     * points associated with a shader library.
     */
    struct ShaderLibraryAssociation
    {
        ShaderLibraryAssociation() = default;

        /// Print debug information about this record.
        void printInfo() const;

        /// Shader library byte code.
        std::unique_ptr<uint8_t[]> byteCode{ };
        /// Length of the shader byte code.
        std::size_t byteCodeLength{ 0u };

        /// Optional name of the shader library.
        std::wstring shaderLibraryName{ };

        /// List of associated entry points.
        std::vector<std::wstring> associatedEntryPoints{ };
    }; // struct ShaderLibraryAssociation

    /**
     * Get already registered shader library, or create it if 
     * it doesn't exist yet.
     * @param library The shader library.
     * @return Returns initialized association structure for 
     * the provided shader library.
     */
    ShaderLibraryAssociation &createOrGet(const ShaderLibrary &library);

    /**
     * Register given shader library name with its association.
     * @param association Association to register.
     * @param shaderLibraryName Name of the shader library.
     * @throws AlreadyRegisteredException Thrown when trying to 
     * associate a name with 2 different shader libraries or if 
     * the association is already registered under different name.
     */
    void registerName(ShaderLibraryAssociation &association, 
        const std::wstring &shaderLibraryName);

    /**
     * Get a shader library by name.
     * @param shaderLibraryName Name of the shader library.
     * @return Returns pointer to its association.
     * @throws UnknownNameException Thrown when provided shader library  
     * name has not been registered before.
     */
    ShaderLibraryAssociation &get(const std::wstring &shaderLibraryName);

    /**
     * Associate a shader library with a list of entry points.
     * @param association Association record.
     * @param entryPoints Entry points to be associated with the 
     * shader library.
     */
    void associateWith(ShaderLibraryAssociation &association, 
        const std::initializer_list<std::wstring> &entryPoints);

    /// Registry of managed shader library associations.
    std::deque<ShaderLibraryAssociation> mRegistry{ };
    /// Mapping from shader library pointers to the registry.
    std::map<const void*, ShaderLibraryAssociation*> mPtrToRegistry{ };
    /// Mapping from root signature names to the registry.
    std::map<std::wstring, ShaderLibraryAssociation*> mNameToRegistry{ };

    /// Mapping from entry point names to their associated shader library.
    std::map<std::wstring, ShaderLibraryAssociation*> mEntryPointToRegistry{ };
protected:
}; // class D3D12RTShaderLibraryRegistry

} // namespace d3d12

} // namespace helpers

} // namespace quark

// Template implementation

namespace quark
{

namespace helpers
{

namespace d3d12
{

}

}

}

// Template implementation end

#endif 
