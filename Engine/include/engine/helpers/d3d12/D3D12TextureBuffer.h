/**
 * @file helpers/d3d12/D3D12TextureBuffer.h
 * @author Tomas Polasek
 * @brief Wrapper around D3D12Resource, providing a texture interface.
 */

#pragma once

#include "engine/helpers/d3d12/D3D12UploadBuffer.h"

#include "engine/resources/d3d12/D3D12DescHeap.h"

#include "engine/renderer/ElementFormat.h"
#include "engine/renderer/ResourceViewType.h"

#include "engine/renderer/platform/d3d12/D3D12ElementFormat.h"

#include "engine/resources/d3d12/D3D12SwapChain.h"

/// Namespace containing the engine code.
namespace quark
{

/// Namespace containing helper classes and methods.
namespace helpers
{

/// Direct3D 12 resource management.
namespace d3d12
{

// Forward declaration.
class D3D12ResourceView;

/**
 * Wrapper around D3D12Resource, providing a texture interface.
 */
class D3D12TextureBuffer : public helpers::d3d12::D3D12UploadBuffer, public util::InheritPointerType<res::d3d12::D3D12Resource, D3D12TextureBuffer>
{
public:
    using PtrT = util::PointerType<D3D12TextureBuffer>::PtrT;
    using ConstPtrT = util::PointerType<D3D12TextureBuffer>::ConstPtrT;

    /**
     * Initialize empty texture buffer.
     */
    D3D12TextureBuffer();

    /**
     * Initialize empty texture buffer.
     */
    static PtrT create();

    /**
     * Create a texture buffer of given size.
     * @tparam AllocPtrT Type of the allocator.
     * @param allocator This allocator will be used to 
     * allocate the texture buffer.
     * @param desc Description of the texture buffer.
     * @param initState Initial state of the buffer.
     * @param clearValue Optimized clear value for this texture.
     * @param name Optional name of the texture.
     * @throws util::winexception Thrown on error.
     */
    template <typename AllocPtrT>
    static PtrT create(AllocPtrT allocator, 
        const ::CD3DX12_RESOURCE_DESC &desc, 
        ::D3D12_RESOURCE_STATES initState = ::D3D12_RESOURCE_STATE_COPY_DEST, 
        const ::D3D12_CLEAR_VALUE *clearValue = nullptr, 
        const std::wstring &name = L"TextureBuffer");

    /**
     * Initialize the texture buffer with provided texture data.
     * Texture buffer will be just large enough to contain provided 
     * data.
     * @tparam AllocPtrT Type of the allocator.
     * @tparam ItT Type of the iterator, which must be of 
     * random-access iterator type.
     * @param begin Begin iterator for the texture data.
     * @param end End iterator for the texture data.
     * @param desc Description of the texture buffer.
     * @param cmdList Command list used to record the copy 
     * command.
     * @param allocator Allocator used to allocate the texture 
     * buffer.
     * @param updater This resource updater will be used to 
     * copy data over to the GPU side.
     * @param clearValue Optimized clear value for this texture.
     * @param name Optional name of the texture.
     * @throws util::winexception Thrown on error.
     */
    template <typename AllocPtrT, typename ItT>
    static PtrT create(const ItT &begin, const ItT &end,
        const ::CD3DX12_RESOURCE_DESC &desc, 
        res::d3d12::D3D12CommandList &cmdList,
        AllocPtrT allocator, D3D12ResourceUpdater &updater, 
        const ::D3D12_CLEAR_VALUE *clearValue = nullptr, 
        const std::wstring &name = L"TextureBuffer");

    /// Free any allocated buffers.
    ~D3D12TextureBuffer();

    // No copying.
    D3D12TextureBuffer(const D3D12TextureBuffer &other) = delete;
    D3D12TextureBuffer &operator=(const D3D12TextureBuffer &other) = delete;
    // Moving is allowed.
    D3D12TextureBuffer(D3D12TextureBuffer &&other) = default;
    D3D12TextureBuffer &operator=(D3D12TextureBuffer &&other) = default;

    using PtrT = util::PointerType<D3D12TextureBuffer>::PtrT;
    using ConstPtrT = util::PointerType<D3D12TextureBuffer>::ConstPtrT;

    /**
     * Create a texture from provided data.
     * @tparam ItT Type of the iterator, which must be of 
     * random-access iterator type.
     * @param begin Begin iterator for the texture data.
     * @param end End iterator for the texture data.
     * @param elementFormat Format of a single texture element.
     * @param width Width of the texture.
     * @param height Height of the texture.
     * @param cmdList Command list used to record the copy 
     * commands.
     * @param device Device used for memory allocation.
     * @param updater This resource updater will be used to 
     * copy data over to the GPU side.
     * @param name Optional name of the texture.
     * @throws util::winexception Thrown on error.
     */
    template <typename ItT>
    void createFromData(const ItT &begin, const ItT &end, 
        const quark::rndr::ElementFormat &elementFormat, uint32_t width, uint32_t height, 
        res::d3d12::D3D12CommandList &cmdList, res::d3d12::D3D12Device &device, 
        helpers::d3d12::D3D12ResourceUpdater &updater, 
        const std::wstring &name = L"TextureBuffer");

    /**
     * Allocate texture buffer on the GPU.
     * @tparam AllocPtrT Type of the allocator.
     * @param desc Description of the texture buffer.
     * @param allocator Allocator which should be used to 
     * allocate the texture buffer.
     * @param initState Initial state of the buffer.
     * @param clearValue Optimized clear value for this texture.
     * @param name Optional name of the texture.
     * @throws util::winexception Thrown on error.
     */
    template <typename AllocPtrT>
    void allocate(AllocPtrT allocator, const ::CD3DX12_RESOURCE_DESC &desc, 
        ::D3D12_RESOURCE_STATES initState = ::D3D12_RESOURCE_STATE_COPY_DEST, 
        const ::D3D12_CLEAR_VALUE *clearValue = nullptr, 
        const std::wstring &name = L"TextureBuffer");

    /**
     * Allocate the texture buffer large enough to hold 
     * provided texture data and upload them to the GPU.
     * @tparam AllocPtrT Type of the allocator.
     * @tparam ItT Type of the iterator, which must be of 
     * random-access iterator type.
     * @param begin Begin iterator for the texture data.
     * @param end End iterator for the texture data.
     * @param desc Description of the texture buffer.
     * @param cmdList Command list used to record the copy 
     * command.
     * @param allocator Allocator used to allocate the texture 
     * buffer.
     * @param updater This resource updater will be used to 
     * copy data over to the GPU side.
     * @param clearValue Optimized clear value for this texture.
     * @param name Optional name of the texture.
     * @throws util::winexception Thrown on error.
     */
    template <typename AllocPtrT, typename ItT>
    void allocateUpload(const ItT &begin, const ItT &end,
        const ::CD3DX12_RESOURCE_DESC &desc, 
        res::d3d12::D3D12CommandList &cmdList,
        AllocPtrT allocator, D3D12ResourceUpdater &updater, 
        const ::D3D12_CLEAR_VALUE *clearValue = nullptr, 
        const std::wstring &name = L"TextureBuffer");

    /**
     * Allocate a texture buffer, with the same properties as 
     * back buffers in provided swap chain.
     * @param swapChain Swap chain to take the properties from.
     * @param allocator Allocator used to allocate the texture 
     * buffer.
     * @param width Hint for width of the viewport. Set to 0 to 
     * auto-detect.
     * @param height Hint for height of the viewport. Set to 0 to 
     * auto-detect.
     * @param initState Initial state of the buffer.
     * @param flags Flags for the resource.
     * @param clearValue Optimized clear value for this texture.
     * @param name Optional name of the texture.
     * @throws util::winexception Thrown on error.
     */
    template <typename AllocPtrT>
    void allocateBackBuffer(
        res::d3d12::D3D12SwapChain &swapChain, AllocPtrT allocator,
        uint32_t width = 0u, uint32_t height = 0u, 
        ::D3D12_RESOURCE_STATES initState = ::D3D12_RESOURCE_STATE_COPY_DEST, 
        ::D3D12_RESOURCE_FLAGS flags = ::D3D12_RESOURCE_FLAG_NONE, 
        const ::D3D12_CLEAR_VALUE *clearValue = nullptr, 
        const std::wstring &name = L"BackBuffer");

    /// has the buffer been reallocated and descriptors invalidated?
    bool descDirty() const;

    /**
     * Get CPU SRV descriptor handle for this buffer.
     * The handle may be invalid, if dirty().
     * @return Returns the CPU descriptor handle.
     */
    ::CD3DX12_CPU_DESCRIPTOR_HANDLE cpuHandle() const;

    /**
     * Get CPU SRV descriptor handle for this buffer.
     * The handle may be invalid, if dirty().
     * @return Returns the GPU descriptor handle.
     */
    ::CD3DX12_GPU_DESCRIPTOR_HANDLE gpuHandle() const;

    /// Get GPU address to this buffer.
    ::D3D12_GPU_VIRTUAL_ADDRESS gpuAddress() const;

    /**
     * Reallocate descriptor for this buffer.
     * May decide to allocate a new one, if the descriptor 
     * heap is different than the last one.
     * @param descHeap Descriptor heap used for the allocation.
     * @param type Type of the view on this resource.
     * @param forceFormat When specified, given format will be 
     * used instead of the format of the texture.
     */
    void reallocateDescriptor(res::d3d12::D3D12DescAllocatorInterface &descHeap, 
        quark::rndr::ResourceViewType type = quark::rndr::ResourceViewType::ShaderResource, 
        ::DXGI_FORMAT forceFormat = ::DXGI_FORMAT_UNKNOWN);

    /// Generate a shader resource view description for this resource.
    virtual ::D3D12_SHADER_RESOURCE_VIEW_DESC srvDesc() const override;

    /// Generate a render-target view description for this resource.
    virtual ::D3D12_RENDER_TARGET_VIEW_DESC rtvDesc() const override;
private:
    /// Optional resource view for this buffer.
    util::PtrT<D3D12ResourceView> mResourceView{ };
protected:
    /**
     * Create a texture buffer of given size.
     * @tparam AllocPtrT Type of the allocator.
     * @param allocator This allocator will be used to 
     * allocate the texture buffer.
     * @param desc Description of the texture buffer.
     * @param initState Initial state of the buffer.
     * @param clearValue Optimized clear value for this texture.
     * @param name Optional name of the texture.
     * @throws util::winexception Thrown on error.
     */
    template <typename AllocPtrT>
    D3D12TextureBuffer(AllocPtrT allocator, 
        const ::CD3DX12_RESOURCE_DESC &desc, 
        ::D3D12_RESOURCE_STATES initState = ::D3D12_RESOURCE_STATE_COPY_DEST, 
        const ::D3D12_CLEAR_VALUE *clearValue = nullptr, 
        const std::wstring &name = L"TextureBuffer");

    /**
     * Initialize the texture buffer with provided texture data.
     * Texture buffer will be just large enough to contain provided 
     * data.
     * @tparam AllocPtrT Type of the allocator.
     * @tparam ItT Type of the iterator, which must be of 
     * random-access iterator type.
     * @param begin Begin iterator for the texture data.
     * @param end End iterator for the texture data.
     * @param desc Description of the texture buffer.
     * @param cmdList Command list used to record the copy 
     * command.
     * @param allocator Allocator used to allocate the texture 
     * buffer.
     * @param updater This resource updater will be used to 
     * copy data over to the GPU side.
     * @param clearValue Optimized clear value for this texture.
     * @param name Optional name of the texture.
     * @throws util::winexception Thrown on error.
     */
    template <typename AllocPtrT, typename ItT>
    D3D12TextureBuffer(const ItT &begin, const ItT &end,
        const ::CD3DX12_RESOURCE_DESC &desc, 
        res::d3d12::D3D12CommandList &cmdList,
        AllocPtrT allocator, D3D12ResourceUpdater &updater, 
        const ::D3D12_CLEAR_VALUE *clearValue = nullptr, 
        const std::wstring &name = L"TextureBuffer");
}; // class D3D12TextureBuffer

} // namespace d3d12

} // namespace helpers

} // namespace quark

// Template implementation

namespace quark
{

namespace helpers
{

namespace d3d12
{

template <typename AllocPtrT>
D3D12TextureBuffer::D3D12TextureBuffer(AllocPtrT allocator, const ::CD3DX12_RESOURCE_DESC &desc,
    ::D3D12_RESOURCE_STATES initState, const ::D3D12_CLEAR_VALUE *clearValue, const std::wstring &name)
{ allocate(allocator, desc, initState, clearValue, name); }

template <typename AllocPtrT, typename ItT>
D3D12TextureBuffer::D3D12TextureBuffer(const ItT &begin, const ItT &end, const ::CD3DX12_RESOURCE_DESC &desc,
    res::d3d12::D3D12CommandList &cmdList, AllocPtrT allocator, D3D12ResourceUpdater &updater,
    const ::D3D12_CLEAR_VALUE *clearValue, const std::wstring &name)
{ allocateUpload(begin, end, desc, cmdList, allocator, updater, clearValue, name); }

template <typename AllocPtrT>
D3D12TextureBuffer::PtrT D3D12TextureBuffer::create(AllocPtrT allocator, const::CD3DX12_RESOURCE_DESC &desc, 
    ::D3D12_RESOURCE_STATES initState, const ::D3D12_CLEAR_VALUE *clearValue, const std::wstring &name)
{ return PtrT{ new D3D12TextureBuffer(allocator, desc, initState, clearValue, name) }; }

template <typename AllocPtrT, typename ItT>
D3D12TextureBuffer::PtrT D3D12TextureBuffer::create(const ItT &begin, const ItT &end, 
    const ::CD3DX12_RESOURCE_DESC &desc, res::d3d12::D3D12CommandList &cmdList, AllocPtrT allocator, 
    D3D12ResourceUpdater &updater, const ::D3D12_CLEAR_VALUE *clearValue, const std::wstring &name)
{ return PtrT{ new D3D12TextureBuffer(begin, end, desc, cmdList, allocator, updater, clearValue, name) }; }

template <typename ItT>
void D3D12TextureBuffer::createFromData(const ItT &begin, const ItT &end, const quark::rndr::ElementFormat &elementFormat,
    uint32_t width, uint32_t height, res::d3d12::D3D12CommandList &cmdList, res::d3d12::D3D12Device &device,
    helpers::d3d12::D3D12ResourceUpdater &updater, const std::wstring &name)
{
    const auto format{ quark::rndr::platform::d3d12::elementFormatToD3D12(elementFormat) };
    const auto alloc{ D3D12CommittedAllocator::create(
        device,
        ::CD3DX12_HEAP_PROPERTIES(::D3D12_HEAP_TYPE_DEFAULT),
        ::D3D12_HEAP_FLAG_NONE) };

    allocateUpload(begin, end, 
        ::CD3DX12_RESOURCE_DESC::Tex2D(format, width, height), 
        cmdList, alloc, updater, nullptr, name);
}

template <typename AllocPtrT>
void D3D12TextureBuffer::allocate(AllocPtrT allocator, const ::CD3DX12_RESOURCE_DESC &desc,
    ::D3D12_RESOURCE_STATES initState, const ::D3D12_CLEAR_VALUE *clearValue, const std::wstring &name)
{
    res::d3d12::D3D12Resource::allocate(desc, initState, clearValue, allocator);

    if (!name.empty())
    { getPtr()->SetName(name.c_str()); }
}

template <typename AllocPtrT, typename ItT>
void D3D12TextureBuffer::allocateUpload(const ItT &begin, const ItT &end, const ::CD3DX12_RESOURCE_DESC &desc,
    res::d3d12::D3D12CommandList &cmdList, AllocPtrT allocator, D3D12ResourceUpdater &updater,
    const ::D3D12_CLEAR_VALUE *clearValue, const std::wstring &name)
{
    using TraitsT = std::iterator_traits<ItT>;

    using ElementT = typename TraitsT::value_type;
    typename TraitsT::pointer data{ &(*begin) };
    const std::size_t sizeInBytes{ (end - begin) * sizeof(ElementT) };

    allocate(allocator, desc, ::D3D12_RESOURCE_STATE_COPY_DEST, clearValue, name);
    uploadData(data, sizeInBytes, cmdList, updater);
}

template<typename AllocPtrT>
void D3D12TextureBuffer::allocateBackBuffer( res::d3d12::D3D12SwapChain &swapChain, 
    AllocPtrT allocator, uint32_t width, uint32_t height, ::D3D12_RESOURCE_STATES initState, 
    ::D3D12_RESOURCE_FLAGS flags, const ::D3D12_CLEAR_VALUE *clearValue, 
    const std::wstring &name)
{
    const auto swapChainWidth{ swapChain.bufferWidth() ? swapChain.bufferWidth() : 1u };
    const auto swapChainHeight{ swapChain.bufferHeight() ? swapChain.bufferHeight() : 1u };

    const auto bufferWidth{ width ? width : swapChainWidth };
    const auto bufferHeight{ height ? height : swapChainHeight };

    // Copy description of swap chain back-buffers.
    const auto desc{ ::CD3DX12_RESOURCE_DESC::Tex2D(
        swapChain.bufferFormat(), 
        bufferWidth, bufferHeight,  
        1u, 1u, 1u, 0u, 
        flags) };

    allocate(allocator, desc, initState, clearValue, name);
}

}

}

}

// Template implementation end
