/**
 * @file helpers/d3d12/D3D12RTTopLevelAS.h
 * @author Tomas Polasek
 * @brief Helper used for building D3D12 top level acceleration 
 * structure for ray tracing.
 */

#pragma once

#ifdef ENGINE_RAY_TRACING_ENABLED

#include "engine/resources/d3d12/D3D12RayTracingDevice.h"
#include "engine/resources/d3d12/D3D12RayTracingCommandList.h"

#include "engine/helpers/math/Transform.h"
#include "engine/helpers/d3d12/D3D12UploadBuffer.h"

/// Namespace containing the engine code.
namespace quark
{

/// Namespace containing helper classes and methods.
namespace helpers
{

/// Direct3D 12 resource management.
namespace d3d12
{

/**
 * Helper used for building D3D12 top level acceleration 
 * structure for ray tracing.
 */
class D3D12RTTopLevelAS
{
public:
    /**
     * Exception thrown when forcing update, but updating  
     * is not allowed.
     */
    struct InvalidUpdateException : public std::exception
    {
        InvalidUpdateException(const char *msg) :
            std::exception(msg)
        { }
    }; // struct InvalidUpdateException

    /**
     * Exception thrown when provided instance handle is 
     * invalid.
     */
    struct UnknownHandleException : public std::exception
    {
        UnknownHandleException(const char *msg) :
            std::exception(msg)
        { }
    }; // struct UnknownHandleException

    /**
     * Structure containing information about required 
     * buffer sizes.
     */
    struct BufferSizeRequirements
    {
        /// Size of the AS build output buffer.
        ::UINT64 outputBufferSize{ 0u };
        /// Size of the AS build scratch buffer.
        ::UINT64 scratchBufferSize{ 0u };
        /// Size of the buffer used for storing instances.
        ::UINT64 instanceBufferSize{ 0u };
    }; // struct BufferSizeRequirements

    /// Handle to an already added instance.
    using InstanceHandleT = std::size_t;

    /**
     * Our default priority for building top level AS is for ray tracing 
     * to be as fast as possible.
     */
    static constexpr ::D3D12_RAYTRACING_ACCELERATION_STRUCTURE_BUILD_FLAGS DEFAULT_BUILD_PRIORITY{ 
        ::D3D12_RAYTRACING_ACCELERATION_STRUCTURE_BUILD_FLAG_PREFER_FAST_TRACE };

    /// Uninitialized acceleration structure builder.
    D3D12RTTopLevelAS();

    /// Free any resources used by the acceleration structure.
    ~D3D12RTTopLevelAS();

    // No copying, allow moving.
    D3D12RTTopLevelAS(const D3D12RTTopLevelAS &other) = delete;
    D3D12RTTopLevelAS &operator=(const D3D12RTTopLevelAS &other) = delete;
    D3D12RTTopLevelAS(D3D12RTTopLevelAS &&other) = default;
    D3D12RTTopLevelAS &operator=(D3D12RTTopLevelAS &&other) = default;

    /// Clear the acceleration structure and any configuration.
    void clear();

    /**
     * Set whether the created top level acceleration structure 
     * can later be updated, without re-creating it from scratch.
     * @param allowUpdating Should updating be allowed?
     */
    void setAllowUpdating(bool allowUpdating);

    /**
     * Set whether to minimize memory used when building the 
     * acceleration structure. May result in slower ray tracing 
     * or slower build times!.
     * @param minimizeMemory Should memory minimization occur?
     */
    void setMinimizeMemory(bool minimizeMemory);

    /**
     * Set whether the compacting of resulting AS should be 
     * allowed.
     * @param allowCompacting Should compacting be allowed?
     */
    void setAllowCompacting(bool allowCompacting);

    /// Focus the build on fast AS.
    void preferFastRayTracing();

    /// Focus the build on finishing as fast as possible.
    void preferFastBuild();

    /**
     * Add instance of given geometry to the scene.
     * @param bottomLevelAS Buffer containing the AS of added geometry.
     * @param transform Transform of the instance within the scene. 
     * Only 3x4 part of the matrix will be used - no projection.
     * @param shaderTableOffset Offset of this instance when choosing 
     * the hit group within the bound shader table.
     * @param instanceID Value accessible from ray tracing shaders, 
     * allowing specialization of per-instance data.
     * @param instanceMask 8 bit mask used for ray tracing only some 
     * objects. Set to 0xFF to never include the instance.
     * @param flags Flags used for this instance.
     * @return Returns handle to the added instance for changes.
     */
    InstanceHandleT addInstance(res::d3d12::D3D12Resource &bottomLevelAS, 
        const helpers::math::Transform &transform, 
        ::UINT shaderTableOffset = 0u, ::UINT instanceID = 0u, 
        ::UINT instanceMask = 0xFF, 
        ::D3D12_RAYTRACING_INSTANCE_FLAGS flags = ::D3D12_RAYTRACING_INSTANCE_FLAG_NONE);

    /**
     * Add instance of given geometry to the scene.
     * @param bottomLevelAS Wrapper of the memory containing the 
     * bottom level acceleration structure.
     * @param transform Transform of the instance within the scene. 
     * Only 3x4 part of the matrix will be used - no projection.
     * @param shaderTableOffset Offset of this instance when choosing 
     * the hit group within the bound shader table.
     * @param instanceID Value accessible from ray tracing shaders, 
     * allowing specialization of per-instance data.
     * @param instanceMask 8 bit mask used for ray tracing only some 
     * objects. Set to 0xFF to never include the instance.
     * @param flags Flags used for this instance.
     * @return Returns handle to the added instance for changes.
     */
    InstanceHandleT addInstance(const ::WRAPPED_GPU_POINTER &bottomLevelAS, 
        const helpers::math::Transform &transform, 
        ::UINT shaderTableOffset = 0u, ::UINT instanceID = 0u, 
        ::UINT instanceMask = 0xFF, 
        ::D3D12_RAYTRACING_INSTANCE_FLAGS flags = ::D3D12_RAYTRACING_INSTANCE_FLAG_NONE);

    /**
     * Add instance of given geometry to the scene.
     * @param transform Transform of the instance within the scene. 
     * Only 3x4 part of the matrix will be used - no projection.
     * @param shaderTableOffset Offset of this instance when choosing 
     * the hit group within the bound shader table.
     * @param instanceID Value accessible from ray tracing shaders, 
     * allowing specialization of per-instance data.
     * @param instanceMask 8 bit mask used for ray tracing only some 
     * objects. Set to 0xFF to never include the instance.
     * @param flags Flags used for this instance.
     * @return Returns handle to the added instance for changes.
     */
    InstanceHandleT addInstance(const helpers::math::Transform &transform, 
        ::UINT shaderTableOffset = 0u, ::UINT instanceID = 0u, 
        ::UINT instanceMask = 0xFF, 
        ::D3D12_RAYTRACING_INSTANCE_FLAGS flags = ::D3D12_RAYTRACING_INSTANCE_FLAG_NONE);

    /**
     * Set bottom level acceleration structure for an already added 
     * instance.
     * @param instanceHandle Handle to the target instance.
     * @param bottomLevelAS Bottom level acceleration structure to use.
     * @throws UnknownHandleException Thrown when provided handle is 
     * invalid.
     */
    void setBottomLevelAS(const InstanceHandleT &instanceHandle, 
        const ::WRAPPED_GPU_POINTER &bottomLevelAS);

    /**
     * Calculate size requirements for AS buffers, for currently 
     * added instances.
     * @param device Device used for ray tracing purposes.
     * @return Returns structure with required sizes.
     */
    BufferSizeRequirements calculateSizeRequirements(
        res::d3d12::D3D12RayTracingDevice &device);

    /**
     * Build current configuration of top acceleration structures.
     * @param device Device used for ray tracing purposes.
     * @return Returns structure with required sizes.
     * @param directCmdList Command list used to record the building 
     * commands.
     * @param instanceBuffer Buffer used for storing the instance 
     * information.
     * @param scratchBuffer Buffer used as scratch space during the 
     * acceleration structure build.
     * @param outputBuffer Resulting acceleration structure will be 
     * stored in this buffer.
     * @throw util::winexception Thrown when error occurs.
     * @warning All of the buffers must be aligned correctly and 
     * have the correct size. Additionally the output buffer must 
     * be in the correct state!
     */
    void build( res::d3d12::D3D12RayTracingDevice &device, 
        res::d3d12::D3D12RayTracingCommandList &directCmdList,
        res::d3d12::D3D12Resource &instanceBuffer,
        res::d3d12::D3D12Resource &scratchBuffer,
        res::d3d12::D3D12Resource &outputBuffer);

    /**
     * Build current configuration of top level acceleration structures.
     * @param device Device used for ray tracing purposes.
     * @param directCmdList Command list used to record the building 
     * commands.
     * @param instanceBuffer Buffer used for storing the instance 
     * information.
     * @param scratchBuffer Buffer used as scratch space during the 
     * acceleration structure build.
     * @param outputBuffer Resulting acceleration structure will be 
     * stored in this buffer.
     * @param pastOutputBuffer Result from the last building of the 
     * acceleration structure. Contents will be used as an update 
     * base, if valid.
     * @param forceUpdate When set to true, the 
     * @throw util::winexception Thrown when error occurs.
     * @throw InvalidUpdateException Thrown when forceUpdate is set 
     * to true but the update cannot be performed.
     * @warning All of the buffers must be aligned correctly and 
     * have the correct size. Additionally the output buffer must 
     * be in the correct state!
     */
    void build(res::d3d12::D3D12RayTracingDevice &device, 
        res::d3d12::D3D12RayTracingCommandList &directCmdList,
        res::d3d12::D3D12Resource &instanceBuffer,
        res::d3d12::D3D12Resource &scratchBuffer,
        res::d3d12::D3D12Resource &outputBuffer, 
        res::d3d12::D3D12Resource &pastOutputBuffer, 
        bool forceUpdate = false);

    /// Get number of instances currently in this acceleration structure.
    uint64_t instanceCount();
private:
    /**
     * Generate instance description with provided information.
     * @param bottomLevelAS Buffer containing the AS of added geometry.
     * @param transform Transform of the instance within the scene. 
     * Only 3x4 part of the matrix will be used - no projection.
     * @param shaderTableOffset Offset of this instance when choosing 
     * the hit group within the bound shader table.
     * @param instanceID Value accessible from ray tracing shaders, 
     * allowing specialization of per-instance data.
     * @param instanceMask 8 bit mask used for ray tracing only some 
     * objects. Set to 0xFF to never include the instance.
     * @param flags Flags used for this instance.
     * @return Returns instance description created from provided 
     * information.
     */
    ::D3D12_RAYTRACING_INSTANCE_DESC generateInstanceDesc( 
        const ::D3D12_GPU_VIRTUAL_ADDRESS &bottomLevelAS, 
        const helpers::math::Transform &transform, 
        ::UINT shaderTableOffset = 0u, ::UINT instanceID = 0u, 
        ::UINT instanceMask = 0xFF, 
        ::D3D12_RAYTRACING_INSTANCE_FLAGS flags = ::D3D12_RAYTRACING_INSTANCE_FLAG_NONE);

    /**
     * Generate instance description with provided information.
     * @param bottomLevelAS Wrapper of the memory containing the 
     * bottom level acceleration structure.
     * @param transform Transform of the instance within the scene. 
     * Only 3x4 part of the matrix will be used - no projection.
     * @param shaderTableOffset Offset of this instance when choosing 
     * the hit group within the bound shader table.
     * @param instanceID Value accessible from ray tracing shaders, 
     * allowing specialization of per-instance data.
     * @param instanceMask 8 bit mask used for ray tracing only some 
     * objects. Set to 0xFF to never include the instance.
     * @param flags Flags used for this instance.
     * @return Returns instance description created from provided 
     * information.
     */
    ::D3D12_RAYTRACING_INSTANCE_DESC generateInstanceDesc( 
        const ::WRAPPED_GPU_POINTER &bottomLevelAS, 
        const helpers::math::Transform &transform, 
        ::UINT shaderTableOffset = 0u, ::UINT instanceID = 0u, 
        ::UINT instanceMask = 0xFF, 
        ::D3D12_RAYTRACING_INSTANCE_FLAGS flags = ::D3D12_RAYTRACING_INSTANCE_FLAG_NONE);

    /**
     * Add given instance description to the list of current 
     * instances.
     * @param desc Description of the instance.
     * @return Returns handle to the new instance.
     */
    InstanceHandleT addInstanceDesc(const ::D3D12_RAYTRACING_INSTANCE_DESC &desc);

    /**
     * Access already added instance by its handle.
     * @param instanceHandle Handle to the instance.
     * @return Returns reference to its description.
     * @throws UnknownHandleException Thrown when given handle 
     * is invalid.
     */
    ::D3D12_RAYTRACING_INSTANCE_DESC &getInstanceDesc(const InstanceHandleT &instanceHandle);

    /**
     * Generate AS building flags from given parameters.
     * @param baseFlags Base building flags, specifying what are 
     * the priorities of the output AS.
     * @param allowUpdates Should the output AS allow updating?
     * @param minimizeMemory Should the output AS take up as small 
     * amount of memory as possible?
     * @param allowCompaction Should the output AS allow compacting 
     * operations, to save memory?
     */
    ::D3D12_RAYTRACING_ACCELERATION_STRUCTURE_BUILD_FLAGS generateBuildFlags(
        ::D3D12_RAYTRACING_ACCELERATION_STRUCTURE_BUILD_FLAGS baseFlags,
        bool allowUpdates, bool minimizeMemory, bool allowCompaction);

    /**
     * Add flag specifying to update the AS to the other provided 
     * flags.
     * @param startFlags Starting flags, the update flag will be 
     * added to them.
     * @return Returns starting flags with update flag included.
     */
    ::D3D12_RAYTRACING_ACCELERATION_STRUCTURE_BUILD_FLAGS addUpdateFlag(
        ::D3D12_RAYTRACING_ACCELERATION_STRUCTURE_BUILD_FLAGS startFlags);

    /**
     * Generate inputs structure for given list of instances.
     * @param instances List of instance descriptions to create the inputs 
     * structure for.
     * @param buildFlags Building flags passed to the AS building command.
     * @return Returns filled build structure for instances with given 
     * list of instances.
     * @throw util::winexception Thrown on error.
     */
    ::D3D12_BUILD_RAYTRACING_ACCELERATION_STRUCTURE_INPUTS generateInstanceInputs(
        const std::vector<::D3D12_RAYTRACING_INSTANCE_DESC> &instances, 
        ::D3D12_RAYTRACING_ACCELERATION_STRUCTURE_BUILD_FLAGS buildFlags);

    /**
     * Get pre-build information for given acceleration structure 
     * building inputs.
     * @param device Device used for ray tracing purposes.
     * @param buildInputs Filled structure with top level build 
     * information.
     * @return Returns filled pre-build structure for given inputs.
     * @throw util::winexception Thrown on error.
     */
    ::D3D12_RAYTRACING_ACCELERATION_STRUCTURE_PREBUILD_INFO getPreBuildInfo(
        res::d3d12::D3D12RayTracingDevice &device, 
        const ::D3D12_BUILD_RAYTRACING_ACCELERATION_STRUCTURE_INPUTS &buildInputs);

    /**
     * Recalculate information structures for the current instances.
     * @param device Device used for ray tracing purposes.
     */
    void recalculateInfoStructures(
        res::d3d12::D3D12RayTracingDevice &device);

    /**
     * Calculate size requirements of the instance buffer.
     * @param instances List of instance descriptions used to calculate 
     * the required size.
     * @return Returns required size of the instance buffer in bytes.
     */
    std::size_t calculateInstanceBufferSize(
        const std::vector<::D3D12_RAYTRACING_INSTANCE_DESC> &instances);

    /**
     * Fill given instance buffer with instance descriptions.
     * @param instances List of instance descriptions.
     * @param instanceBuffer GPU destination buffer for the instance 
     * descriptions.
     */
    void fillInstanceBuffer(
        const std::vector<::D3D12_RAYTRACING_INSTANCE_DESC> &instances,
        res::d3d12::D3D12Resource &instanceBuffer);

    /**
     * Generate build command description for provided parameters.
     * @param instanceInputs Description of the instances.
     * @param scratchBuffer Helper buffer used for building the AS.
     * @param outputBuffer Buffer where the final AS will be stored.
     * @param pastOutputBuffer Optional buffer with AS from the last 
     * build.
     */
    ::D3D12_BUILD_RAYTRACING_ACCELERATION_STRUCTURE_DESC generateBuildDesc(
        const ::D3D12_BUILD_RAYTRACING_ACCELERATION_STRUCTURE_INPUTS &instanceInputs, 
        res::d3d12::D3D12Resource &scratchBuffer,
        res::d3d12::D3D12Resource &outputBuffer, 
        res::d3d12::D3D12Resource *pastOutputBuffer = nullptr);

    /// Has there been a change to the added primitives?
    bool mDirty{ true };
    /// Should updating be allowed?
    bool mAllowUpdating{ false };
    /// Should memory minimization occur?
    bool mMinimizeMemory{ false };
    /// Should compacting of AS be allowed?
    bool mAllowCompacting{ false };
    /// List of instances to build the AS for.
    std::vector<::D3D12_RAYTRACING_INSTANCE_DESC> mInstances{ };
    /// Base building flags, specifying priorities of AS building.
    ::D3D12_RAYTRACING_ACCELERATION_STRUCTURE_BUILD_FLAGS mBaseFlags{ };

    /// Flags used for building the top level AS. May be invalid if mDirty is set to true!
    ::D3D12_RAYTRACING_ACCELERATION_STRUCTURE_BUILD_FLAGS mBuildFlags{ };
    /// Structure containing information about instances. May be invalid if mDirty is set to true!
    ::D3D12_BUILD_RAYTRACING_ACCELERATION_STRUCTURE_INPUTS mInstanceInputs{ };
    /// Calculated pre-build info. May be invalid if mDirty is set to true!
    ::D3D12_RAYTRACING_ACCELERATION_STRUCTURE_PREBUILD_INFO mPreBuildInfo{ };

    /// Current number of instances in this acceleration structure.
    uint64_t mInstanceCount{ 0u };
protected:
}; // class D3D12RTTopLevelAS

} // namespace d3d12

} // namespace helpers

} // namespace quark

// Template implementation

namespace quark
{

namespace helpers
{

namespace d3d12
{

}

}

}

// Template implementation end

#endif 
