/**
 * @file helpers/d3d12/D3D12DescTable.h
 * @author Tomas Polasek
 * @brief Wrapper around D3D12DescHeap and a DescriptorTable, allowing 
 * allocation of descriptors.
 */

#pragma once

#include "engine/resources/d3d12/D3D12DescHeap.h"

/// Namespace containing the engine code.
namespace quark
{

/// Namespace containing helper classes and methods.
namespace helpers
{

/// Direct3D 12 resource management.
namespace d3d12
{

// TODO - Fix this mess? virtual allocateNext(), other methods can be shared.
/**
 * Wrapper around D3D12DescHeap and a DescriptorTable, allowing 
 * allocation of descriptors.
 */
class D3D12DescTable : public res::d3d12::D3D12DescAllocatorInterface
{
public:
    using DescHeapFull = res::d3d12::D3D12DescHeap::DescHeapFull;
    using InvalidDescriptorHandle = res::d3d12::D3D12DescHeap::InvalidDescriptorHandle;
    using InvalidDescriptorTable = res::d3d12::D3D12DescHeap::InvalidDescriptorTable;

    using DescriptorTable = res::d3d12::D3D12DescHeap::DescriptorTable;

    /// Un-initialized descriptor table.
    D3D12DescTable();

    /// Free any allocated buffers.
    virtual ~D3D12DescTable();

    // No copying.
    D3D12DescTable(const D3D12DescTable &other) = delete;
    D3D12DescTable &operator=(const D3D12DescTable &other) = delete;
    // Moving is allowed.
    D3D12DescTable(D3D12DescTable &&other) = default;
    D3D12DescTable &operator=(D3D12DescTable &&other) = default;

    /**
     * Create descriptor table bound to given descriptor heap, using 
     * a descriptor table specification.
     * @param descHeap Bind to this descriptor heap.
     * @param descriptorTable Specification of descriptor table within 
     * the given descriptor heap.
     * @throws InvalidDescriptorTable Thrown when given descriptor table 
     * does not belong to the provided descriptor heap.
     */
    D3D12DescTable(res::d3d12::D3D12DescHeap &descHeap, 
        const DescriptorTable &descriptorTable);

    /**
     * Create SRV handle on this descriptor heap.
     * @param resource Create SRV for this resource.
     * @param desc Description of the SRV.
     * @return Returns the new descriptor handle.
     * @throws DescHeapFull Thrown if the descriptor heap 
     * is already full.
     */
    virtual ::CD3DX12_CPU_DESCRIPTOR_HANDLE createSRV(
        res::d3d12::D3D12Resource &resource, 
        const ::D3D12_SHADER_RESOURCE_VIEW_DESC &desc) override;

    /**
     * Create SRV handle on this descriptor heap.
     * @param resource Create SRV for this resource.
     * @param desc Description of the SRV.
     * @param target Descriptor will be created in place 
     * of this target.
     * @return Returns the new descriptor handle.
     */
    virtual ::CD3DX12_CPU_DESCRIPTOR_HANDLE createSRV(
        res::d3d12::D3D12Resource &resource, 
        const ::D3D12_SHADER_RESOURCE_VIEW_DESC &desc, 
        const ::CD3DX12_CPU_DESCRIPTOR_HANDLE &target) override;

    /**
     * Create RTV handle on this descriptor heap.
     * @param resource Create RTV for this resource.
     * @param desc Description of the RTV.
     * @return Returns the new descriptor handle.
     * @throws DescHeapFull Thrown if the descriptor heap 
     * is already full.
     */
    virtual ::CD3DX12_CPU_DESCRIPTOR_HANDLE createRTV(
        res::d3d12::D3D12Resource &resource, 
        const ::D3D12_RENDER_TARGET_VIEW_DESC &desc) override;

    /**
     * Create RTV handle on this descriptor heap.
     * @param resource Create RTV for this resource.
     * @param desc Description of the RTV.
     * @param target Descriptor will be created in place 
     * of this target.
     * @return Returns the new descriptor handle.
     */
    virtual ::CD3DX12_CPU_DESCRIPTOR_HANDLE createRTV(
        res::d3d12::D3D12Resource &resource, 
        const ::D3D12_RENDER_TARGET_VIEW_DESC &desc, 
        const ::CD3DX12_CPU_DESCRIPTOR_HANDLE &target) override;

    /**
     * Create DSV handle on this descriptor heap.
     * @param resource Create DSV for this resource.
     * @param desc Description of the DSV.
     * @return Returns the new descriptor handle.
     * @throws DescHeapFull Thrown if the descriptor heap 
     * is already full.
     */
    virtual ::CD3DX12_CPU_DESCRIPTOR_HANDLE createDSV(
        res::d3d12::D3D12Resource &resource, 
        const ::D3D12_DEPTH_STENCIL_VIEW_DESC &desc) override;

    /**
     * Create DSV handle on this descriptor heap.
     * @param resource Create DSV for this resource.
     * @param desc Description of the DSV.
     * @param target Descriptor will be created in place 
     * of this target.
     * @return Returns the new descriptor handle.
     */
    virtual ::CD3DX12_CPU_DESCRIPTOR_HANDLE createDSV(
        res::d3d12::D3D12Resource &resource, 
        const ::D3D12_DEPTH_STENCIL_VIEW_DESC &desc, 
        const ::CD3DX12_CPU_DESCRIPTOR_HANDLE &target) override;

    /**
     * Create UAV handle on this descriptor heap.
     * @param resource Create UAV for this resource.
     * @param desc Description of the UAV.
     * @param counterResource Resource used as UAV counter.
     * @return Returns the new descriptor handle.
     * @throws DescHeapFull Thrown if the descriptor heap 
     * is already full.
     */
    virtual ::CD3DX12_CPU_DESCRIPTOR_HANDLE createUAV(
        res::d3d12::D3D12Resource &resource, 
        const ::D3D12_UNORDERED_ACCESS_VIEW_DESC &desc, 
        res::d3d12::D3D12Resource *counterResource = nullptr) override;

    /**
     * Create UAV handle on this descriptor heap.
     * @param resource Create UAV for this resource.
     * @param desc Description of the UAV.
     * @param target Descriptor will be created in place 
     * of this target.
     * @param counterResource Resource used as UAV counter.
     * @return Returns the new descriptor handle.
     */
    virtual ::CD3DX12_CPU_DESCRIPTOR_HANDLE createUAV(
        res::d3d12::D3D12Resource &resource, 
        const ::D3D12_UNORDERED_ACCESS_VIEW_DESC &desc, 
        const ::CD3DX12_CPU_DESCRIPTOR_HANDLE &target, 
        res::d3d12::D3D12Resource *counterResource = nullptr) override;

    /**
     * Create CBV handle on this descriptor heap.
     * @param desc Description of the CBV.
     * @return Returns the new descriptor handle.
     */
    virtual ::CD3DX12_CPU_DESCRIPTOR_HANDLE createCBV(
        const ::D3D12_CONSTANT_BUFFER_VIEW_DESC &desc) override;

    /**
     * Create CBV handle on this descriptor heap.
     * @param desc Description of the CBV.
     * @param target Descriptor will be created in place 
     * of this target.
     * @return Returns the new descriptor handle.
     */
    virtual ::CD3DX12_CPU_DESCRIPTOR_HANDLE createCBV(
        const ::D3D12_CONSTANT_BUFFER_VIEW_DESC &desc,
        const ::CD3DX12_CPU_DESCRIPTOR_HANDLE &target) override;

    /**
     * Create SRV null handle on this descriptor heap.
     * @param dimension Type of the SRV.
     * @return Returns the new descriptor handle.
     * @throws DescHeapFull Thrown if the descriptor heap 
     * is already full.
     */
    virtual ::CD3DX12_CPU_DESCRIPTOR_HANDLE createSrvNullHandle(
        ::D3D12_SRV_DIMENSION dimension) override;

    /**
     * Create SRV null handle on this descriptor heap.
     * @param dimension Type of the SRV.
     * @param target Descriptor will be created in place 
     * of this target.
     * @return Returns the new descriptor handle.
     * @throws DescHeapFull Thrown if the descriptor heap 
     * is already full.
     */
    virtual ::CD3DX12_CPU_DESCRIPTOR_HANDLE createSrvNullHandle(
        ::D3D12_SRV_DIMENSION dimension, 
        const ::CD3DX12_CPU_DESCRIPTOR_HANDLE &target) override;

    /**
     * Create UAV null handle on this descriptor heap.
     * @param dimension Type of the UAV.
     * @return Returns the new descriptor handle.
     * @throws DescHeapFull Thrown if the descriptor heap 
     * is already full.
     */
    virtual ::CD3DX12_CPU_DESCRIPTOR_HANDLE createUavNullHandle(
        ::D3D12_UAV_DIMENSION dimension) override;

    /**
     * Create UAV null handle on this descriptor heap.
     * @param dimension Type of the UAV.
     * @param target Descriptor will be created in place 
     * of this target.
     * @return Returns the new descriptor handle.
     * @throws DescHeapFull Thrown if the descriptor heap 
     * is already full.
     */
    virtual ::CD3DX12_CPU_DESCRIPTOR_HANDLE createUavNullHandle(
        ::D3D12_UAV_DIMENSION dimension, 
        const ::CD3DX12_CPU_DESCRIPTOR_HANDLE &target) override;

    /**
     * Create CBV null handle on this descriptor heap.
     * @return Returns the new descriptor handle.
     * @throws DescHeapFull Thrown if the descriptor heap 
     * is already full.
     */
    virtual ::CD3DX12_CPU_DESCRIPTOR_HANDLE createCbvNullHandle() override;

    /**
     * Create CBV null handle on this descriptor heap.
     * @param target Descriptor will be created in place 
     * of this target.
     * @return Returns the new descriptor handle.
     * @throws DescHeapFull Thrown if the descriptor heap 
     * is already full.
     */
    virtual ::CD3DX12_CPU_DESCRIPTOR_HANDLE createCbvNullHandle(
        const ::CD3DX12_CPU_DESCRIPTOR_HANDLE &target) override;

    /**
     * Create sampler on this descriptor heap.
     * @param desc Description of the sampler.
     * @return Returns the new descriptor handle.
     */
    virtual ::CD3DX12_CPU_DESCRIPTOR_HANDLE createSampler(
        const ::D3D12_SAMPLER_DESC &desc) override;

    /**
     * Create sampler on this descriptor heap.
     * @param desc Description of the sampler.
     * @param target Descriptor will be created in place 
     * of this target.
     * @return Returns the new descriptor handle.
     */
    virtual ::CD3DX12_CPU_DESCRIPTOR_HANDLE createSampler(
        const ::D3D12_SAMPLER_DESC &desc, 
        const ::CD3DX12_CPU_DESCRIPTOR_HANDLE &target) override;

    /**
     * Get offset from the beginning of the descriptor heap.
     * @param cpuHandle Handle to calculate the offset for.
     * @return Returns offset in a number of handles from the 
     * start of the heap.
     */
    virtual std::size_t offsetFromStart(
        const ::CD3DX12_CPU_DESCRIPTOR_HANDLE &cpuHandle) const override;

    /**
     * Get offset from the beginning of this descriptor table.
     * @param cpuHandle Handle to calculate the offset for.
     * @return Returns offset in a number of handles from the 
     * start of this descriptor table.
     */
    std::size_t offsetFromTableStart(
        const ::CD3DX12_CPU_DESCRIPTOR_HANDLE &cpuHandle) const;

    /// Check whether given descriptor handle is on this heap.
    virtual bool isOnThisDescHeap(
        const ::CD3DX12_CPU_DESCRIPTOR_HANDLE &cHandle) const override;

    /// Check whether given descriptor handle is on this heap.
    virtual bool isOnThisDescHeap(
        const ::CD3DX12_GPU_DESCRIPTOR_HANDLE &gHandle) const override;

    /**
     * Get GPU handle for a CPU handle from this descriptor heap.
     * @param cpuHandle CPU handle from this descriptor heap.
     * @return Returns GPU descriptor handle corresponding to the 
     * provided CPU handle.
     */
    virtual ::CD3DX12_GPU_DESCRIPTOR_HANDLE gpuHandle(
        const ::CD3DX12_CPU_DESCRIPTOR_HANDLE &cpuHandle) const override;

    /**
     * Get base handle on the GPU for the start of this table.
     * @return Returns handle to the first descriptor within this 
     * table.
     */
    ::CD3DX12_GPU_DESCRIPTOR_HANDLE baseHandle() const;

    /**
     * Reset the used table, allowing its reuse.
     * This action invalidates all of the descriptor created 
     * in this table.
     */
    void clear();

    /**
     * Initialize all unused descriptors in this table 
     * to null.
     * @param dimension Type of the SRV.
     */
    void initializeSrvNullHandles(::D3D12_SRV_DIMENSION dimension);

    /**
     * Copy as many descriptors as possible from the source descriptor 
     * table.
     * If the source table is not bound, this method returns 0 and 
     * performs no further actions.
     * Descriptors will be copied only if this table contains enough 
     * free space. If there is not enough free descriptors to fit the 
     * whole source table, method returns 0 and performs no further 
     * actions.
     * @param srcTable Source of the copy operation.
     * @return Returns the number of descriptors copied.
     */
    uint32_t copyFrom(const D3D12DescTable &srcTable);

    /**
     * Get the full descriptor table specification, which has been allocated 
     * on the descriptor heap.
     * @return Returns descriptor table which includes unallocated descriptors.
     */
    DescriptorTable totalTable() const;

    /**
     * Get the descriptor table specification, which has already been filled with 
     * allocated descriptors.
     * @return Returns descriptor table which includes only allocated descriptors.
     */
    DescriptorTable allocatedTable() const;

    /**
     * Get the descriptor table specification, which contains only unallocated 
     * descriptors.
     * @return Returns descriptor table which includes only unallocated descriptors.
     */
    DescriptorTable unallocatedTable() const;

    /// Pop the last allocated descriptor from the table.
    void pop();

    /// Is this descriptor table bound to a descriptor heap?
    bool bound() const;

    /// Has the descriptor heap been initialized?
    virtual bool initialized() const override
    { return bound(); }

    /// Is this descriptor table bound to a descriptor heap?
    operator bool() const;

    /// Total number of descriptors in this table.
    uint32_t size() const;
    /// Number of allocated descriptors.
    uint32_t allocated() const;
    /// Number of free, unallocated descriptors.
    uint32_t unallocated() const;

    /// Get the inner descriptor heap.
    auto *get() const
    {
        ASSERT_SLOW(bound());
        return mDescHeap->get();
    }
private:
    /**
     * Allocate next descriptor from the table.
     * @return Returns the new descriptor handle.
     * @throws DescHeapFull Thrown if the descriptor heap 
     * is already full.
     */
    ::CD3DX12_CPU_DESCRIPTOR_HANDLE allocateNext();

    /// Bound descriptor heap.
    res::d3d12::D3D12DescHeap *mDescHeap{ nullptr };
    
    /// Currently used descriptor table.
    DescriptorTable mDescTable{ };
    /// Iterator within the table to the last unused descriptor.
    uint32_t mDescIt{ };
protected:
}; // class D3D12DescTable

} // namespace d3d12

} // namespace helpers

} // namespace quark

// Template implementation

namespace quark
{

namespace helpers
{

namespace d3d12
{

}

}

}

// Template implementation end
