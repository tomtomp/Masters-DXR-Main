/**
 * @file helpers/d3d12/D3D12RTStateObjectSOBuilder.h
 * @author Tomas Polasek
 * @brief Generator of state sub-objects used for 
 * state object configuration.
 */

#pragma once

#ifdef ENGINE_RAY_TRACING_ENABLED

#include "engine/lib/d3dx12.h"

/// Namespace containing the engine code.
namespace quark
{

/// Namespace containing helper classes and methods.
namespace helpers
{

/// Direct3D 12 resource management.
namespace d3d12
{

/// Base sub-object class, used for homogenous access to the sub-objects.
class D3D12StateObjectSubObjectBase
{
public:
    explicit D3D12StateObjectSubObjectBase(::D3D12_STATE_SUBOBJECT &subObject, std::size_t index);
    virtual ~D3D12StateObjectSubObjectBase();

    /// Finalize changes made to this sub-object.
    virtual void finalize() = 0;

    /**
     * Get type of the sub-object contained in this wrapper.
     * Returns ::D3D12_STATE_SUBOBJECT_TYPE_MAX_VALID when the 
     * wrapper is empty.
     */
    virtual ::D3D12_STATE_SUBOBJECT_TYPE type() const noexcept = 0;

    /**
     * Perform any required tasks in order to fix any dangling 
     * pointers.
     * @param subObjects Linear list of sub objects, which can 
     * be accessed using index().
     */
    virtual void finalizePointers(::D3D12_STATE_SUBOBJECT *subObjects) noexcept
	{ UNUSED(subObjects); };

    /// Access the managed sub-object.
    const ::D3D12_STATE_SUBOBJECT &subObj() const
    { return *mSubObj; }

    /// Access the managed sub-object.
    ::D3D12_STATE_SUBOBJECT &subObj() 
    { return *mSubObj; }

    /// Get index of this sub object within the whole configuration.
    std::size_t index() const
    { return mIndex; }
private:
protected:
    /// Sub-object managed by this class.
    ::D3D12_STATE_SUBOBJECT *mSubObj{ nullptr };
    /// Index of this sub object within the whole configuration.
    std::size_t mIndex{ 0u };
}; // class D3D12StateObjectSubObjectBase

/**
 * Helper structure for wrapping state object 
 * sub-objects.
 * Inspired by CD3DX12_PIPELINE_STATE_STREAM_VS.
 * @tparam InnerTT Type of the inner configuration structure.
 * @tparam SubObjTypeT Identifier of the sub-object.
 */
template <typename InnerTT, ::D3D12_STATE_SUBOBJECT_TYPE SubObjTypeT>
class D3D12StateObjectSubObject : public D3D12StateObjectSubObjectBase
{
public:
    /// Type of the sub-object.
    using InnerT = InnerTT;
    /// Identifier of the sub-object.
    static constexpr const auto SubObjType{ SubObjTypeT };

    /// Initialize the sub-object, managin given sub-object state.
    explicit D3D12StateObjectSubObject(::D3D12_STATE_SUBOBJECT &subObject, std::size_t index) :
        D3D12StateObjectSubObjectBase(subObject, index)
    {
        mSubObj->Type = type();
        mSubObj->pDesc = &mInner;
    }
    /// Free resources.
    virtual ~D3D12StateObjectSubObject()
    { /* Automatic */ }

    /// Set the sub-object to provided state.
    D3D12StateObjectSubObject &operator=(const InnerT &inner)
    { mInner = inner; return *this; }

    /// Finalize changes made to this sub-object.
    virtual void finalize() override final
    { /* Nothing to be done */ }

    /**
     * Get type of the sub-object contained in this wrapper.
     * Returns ::D3D12_STATE_SUBOBJECT_TYPE_MAX_VALID when the 
     * wrapper is empty.
     */
    virtual ::D3D12_STATE_SUBOBJECT_TYPE type() const noexcept override final
    { return SubObjType; }

    /// Access the sub-object description.
    InnerT &desc()
    { return mInner; }
private:
    /// Sub-object itself.
    InnerT mInner{ InnerT() };
protected:
}; // class D3D12PipelineStateSubObject

/// Wrapper around DXIL library description.
class D3D12DxilLibrary : public D3D12StateObjectSubObjectBase
{
public:
    /// Initialize the library.
    explicit D3D12DxilLibrary(::D3D12_STATE_SUBOBJECT &subObject, std::size_t index);
    /// Free resources.
    virtual ~D3D12DxilLibrary();

    // No copying or moving.
    D3D12DxilLibrary(const D3D12DxilLibrary &other) = delete;
    D3D12DxilLibrary &operator=(const D3D12DxilLibrary &other) = delete;
    D3D12DxilLibrary(D3D12DxilLibrary &&other) = delete;
    D3D12DxilLibrary &operator=(D3D12DxilLibrary &&other) = delete;

    /// Finalize changes made to this sub-object.
    virtual void finalize() override final;

    /// Set shader code used by this library.
    void setLibrary(::D3D12_SHADER_BYTECODE *code);

    /// Set shader code used by this library.
    void setLibrary(const ::D3D12_SHADER_BYTECODE &code);

    /// Set shader code used by this library.
    void setLibrary(::ID3DBlob *code);

    /**
     * Define symbol exported by this library. The name must 
     * correspond with library code set with setLibrary.
     * @param exportName Name of the exported symbol.
     * @param renameTo Optional name under which the exported 
     * symbol will be known.
     * @param flags Flags used for this export.
     */
    void defineExport(
        const wchar_t *exportName,
        const wchar_t *renameTo = nullptr,
        ::D3D12_EXPORT_FLAGS flags = ::D3D12_EXPORT_FLAG_NONE);

    /**
     * Define all symbols specified withing provided array.
     * Uses default flags and doesn't perform any renaming.
     * @param exportNames Array of names.
     */
    template <std::size_t N>
    void defineExports(const wchar_t (&exportNames)[N])
    { defineExports(exportNames, N); }

    /**
     * Define all symbols specified withing provided array.
     * Uses default flags and doesn't perform any renaming.
     * @param exportNames List of names.
     */
    void defineExports(const std::vector<std::wstring> &exportNames);

    /**
     * Get type of the sub-object contained in this wrapper.
     * Returns ::D3D12_STATE_SUBOBJECT_TYPE_MAX_VALID when the 
     * wrapper is empty.
     */
    virtual ::D3D12_STATE_SUBOBJECT_TYPE type() const noexcept override final
    { return ::D3D12_STATE_SUBOBJECT_TYPE_DXIL_LIBRARY; }
private:
    /**
     * Define all symbols specified withing provided array.
     * Uses default flags and doesn't perform any renaming.
     * @param exportNames Array of names.
     * @param size Size of the array.
     */
    void defineExports(const wchar_t *exportNames[], std::size_t size);

    /// Description of the DXIL library.
    ::D3D12_DXIL_LIBRARY_DESC mDesc{ };
    /// List of symbols exported by this library.
    std::vector<::D3D12_EXPORT_DESC> mExports{ };
    /// Strings used for defining the exports.
    util::StringContainer<std::wstring> mStrings{ };
protected:
}; // class D3D12DxilLibrary

/// Wrapper around existing collection description.
class D3D12ExistingCollection : public D3D12StateObjectSubObjectBase
{
public:
    /// Initialize the collection.
    explicit D3D12ExistingCollection(::D3D12_STATE_SUBOBJECT &subObject, std::size_t index);
    /// Free resources.
    virtual ~D3D12ExistingCollection();

    // No copying or moving.
    D3D12ExistingCollection(const D3D12ExistingCollection &other) = delete;
    D3D12ExistingCollection &operator=(const D3D12ExistingCollection &other) = delete;
    D3D12ExistingCollection(D3D12ExistingCollection &&other) = delete;
    D3D12ExistingCollection &operator=(D3D12ExistingCollection &&other) = delete;

    /// Finalize changes made to this sub-object.
    virtual void finalize() override final;

    /// Set state object containing data used by this collection.
    void setState(::ID3D12StateObject *stateObject);

    /**
     * Define symbol exported by this collection. The name must 
     * be exported by state object set with setState.
     * @param exportName Name of the exported symbol.
     * @param renameTo Optional name under which the exported 
     * symbol will be known.
     * @param flags Flags used for this export.
     */
    void defineExport(
        const wchar_t *exportName,
        const wchar_t *renameTo = nullptr,
        ::D3D12_EXPORT_FLAGS flags = ::D3D12_EXPORT_FLAG_NONE);

    /**
     * Define all symbols specified withing provided array.
     * Uses default flags and doesn't perform any renaming.
     * @param exportNames Array of names.
     */
    template <std::size_t N>
    void defineExports(const wchar_t (&exportNames)[N])
    { defineExports(exportNames, N); }

    /**
     * Define all symbols specified withing provided array.
     * Uses default flags and doesn't perform any renaming.
     * @param exportNames Array of names.
     */
    void defineExports(const std::vector<std::wstring> &exportNames);

    /**
     * Get type of the sub-object contained in this wrapper.
     * Returns ::D3D12_STATE_SUBOBJECT_TYPE_MAX_VALID when the 
     * wrapper is empty.
     */
    virtual ::D3D12_STATE_SUBOBJECT_TYPE type() const noexcept override final
    { return ::D3D12_STATE_SUBOBJECT_TYPE_EXISTING_COLLECTION; }
private:
    /**
     * Define all symbols specified withing provided array.
     * Uses default flags and doesn't perform any renaming.
     * @param exportNames Array of names.
     * @param size Size of the array.
     */
    void defineExports(const wchar_t *exportNames[], std::size_t size);

    /// Description of the existing collection.
    ::D3D12_EXISTING_COLLECTION_DESC mDesc{ };
    /// List of symbols exported by this library.
    std::vector<::D3D12_EXPORT_DESC> mExports{ };
    /// Strings used for defining the exports.
    util::StringContainer<std::wstring> mStrings{ };
protected:
}; // class D3D12ExistingCollection

/// Wrapper around sub-objects to exports association.
class D3D12SubObjectsToExports : public D3D12StateObjectSubObjectBase

{
public:
    /// Initialize the sub-objects to export.
    explicit D3D12SubObjectsToExports(::D3D12_STATE_SUBOBJECT &subObject, std::size_t index);
    /// Free resources.
    virtual ~D3D12SubObjectsToExports();

    // No copying or moving.
    D3D12SubObjectsToExports(const D3D12SubObjectsToExports &other) = delete;
    D3D12SubObjectsToExports &operator=(const D3D12SubObjectsToExports &other) = delete;
    D3D12SubObjectsToExports(D3D12SubObjectsToExports &&other) = delete;
    D3D12SubObjectsToExports &operator=(D3D12SubObjectsToExports &&other) = delete;

    /// Finalize changes made to this sub-object.
    virtual void finalize() override final;

    /// Fix the associated sub object pointer.
    virtual void finalizePointers(::D3D12_STATE_SUBOBJECT *subObjects) noexcept override final;

    /// Set sub-object associated with the exports.
    void setSubObject(const D3D12StateObjectSubObjectBase *subObject);

    /**
     * Add symbol which should be associated with provided 
     * sub-object.
     * @param name Name of the associated symbol.
     */
    void addExport(const wchar_t *name);

    /**
     * Add all symbols specified withing provided array.
     * @param exportNames Array of names.
     */
    template <std::size_t N>
    void addExports(const wchar_t (&exportNames)[N])
    { addExports(exportNames, N); }

    /**
     * Add all symbols specified withing provided array.
     * @param exportNames Array of names.
     */
    void addExports(const std::vector<std::wstring> &exportNames);

    /**
     * Get type of the sub-object contained in this wrapper.
     * Returns ::D3D12_STATE_SUBOBJECT_TYPE_MAX_VALID when the 
     * wrapper is empty.
     */
    virtual ::D3D12_STATE_SUBOBJECT_TYPE type() const noexcept override final
    { return ::D3D12_STATE_SUBOBJECT_TYPE_SUBOBJECT_TO_EXPORTS_ASSOCIATION; }
private:
    /**
     * Add all symbols specified withing provided array.
     * @param exportNames Array of names.
     * @param size Size of the array.
     */
    void addExports(const wchar_t *exportNames[], std::size_t size);

    /// Description of the export association.
    ::D3D12_SUBOBJECT_TO_EXPORTS_ASSOCIATION mDesc{ };
    /// Index of the associated sub object.
    std::size_t mAssociatedSubObjectIndex{ 0u };
    /// List of symbols associated to the sub-object.
    std::vector<const wchar_t*> mExports{ };
    /// Strings used for defining the symbols.
    util::StringContainer<std::wstring> mStrings{ };
protected:
}; // class D3D12SubObjectsToExports

/// Wrapper around DXIL sub-objects to exports association.
class D3D12DxilSubObjectsToExports : public D3D12StateObjectSubObjectBase

{
public:
    /// Initialize the sub-objects to export.
    explicit D3D12DxilSubObjectsToExports(::D3D12_STATE_SUBOBJECT &subObject, std::size_t index);
    /// Free resources.
    virtual ~D3D12DxilSubObjectsToExports();

    // No copying or moving.
    D3D12DxilSubObjectsToExports(const D3D12DxilSubObjectsToExports &other) = delete;
    D3D12DxilSubObjectsToExports &operator=(const D3D12DxilSubObjectsToExports &other) = delete;
    D3D12DxilSubObjectsToExports(D3D12DxilSubObjectsToExports &&other) = delete;
    D3D12DxilSubObjectsToExports &operator=(D3D12DxilSubObjectsToExports &&other) = delete;

    /// Finalize changes made to this sub-object.
    virtual void finalize() override final;

    /// Set sub-object asssociated with the exports.
    void setSubObject(const wchar_t *subObject);

    /**
     * Add symbol which should be associated with provided 
     * sub-object.
     * @param name Name of the associated symbol.
     */
    void addExport(const wchar_t *name);

    /**
     * Add all symbols specified withing provided array.
     * @param exportNames Array of names.
     */
    template <std::size_t N>
    void addExports(const wchar_t (&exportNames)[N])
    { addExports(exportNames, N); }

    /**
     * Add all symbols specified withing provided array.
     * @param exportNames Array of names.
     */
    void addExports(const std::vector<std::wstring> &exportNames);

    /**
     * Get type of the sub-object contained in this wrapper.
     * Returns ::D3D12_STATE_SUBOBJECT_TYPE_MAX_VALID when the 
     * wrapper is empty.
     */
    virtual ::D3D12_STATE_SUBOBJECT_TYPE type() const noexcept override final
    { return ::D3D12_STATE_SUBOBJECT_TYPE_DXIL_SUBOBJECT_TO_EXPORTS_ASSOCIATION; }
private:
    /**
     * Add all symbols specified withing provided array.
     * @param exportNames Array of names.
     * @param size Size of the array.
     */
    void addExports(const wchar_t *exportNames[], std::size_t size);

    /// Description of the export association.
    ::D3D12_DXIL_SUBOBJECT_TO_EXPORTS_ASSOCIATION mDesc{ };
    /// List of symbols associated to the sub-object.
    std::vector<const wchar_t*> mExports{ };
    /// Strings used for defining the symbols.
    util::StringContainer<std::wstring> mStrings{ };
    /// Name of the associated DXIL sub-object.
    std::wstring mSubObjectName{ };
protected:
}; // class D3D12DxilSubObjectsToExports

/// Wrapper around hit group description.
class D3D12HitGroup : public D3D12StateObjectSubObjectBase

{
public:
    /// Initialize the hit group.
    explicit D3D12HitGroup(::D3D12_STATE_SUBOBJECT &subObject, std::size_t index);
    /// Free resources.
    virtual ~D3D12HitGroup();

    // No copying or moving.
    D3D12HitGroup(const D3D12HitGroup &other) = delete;
    D3D12HitGroup &operator=(const D3D12HitGroup &other) = delete;
    D3D12HitGroup(D3D12HitGroup &&other) = delete;
    D3D12HitGroup &operator=(D3D12HitGroup &&other) = delete;

    /// Finalize changes made to this sub-object.
    virtual void finalize() override final;

    /// Set name of this hit group.
    void setName(const wchar_t *name);

    /// Set name of this hit group.
    void setName(const std::wstring &name);

    /// Set type of this hit group
    void setType(::D3D12_HIT_GROUP_TYPE type);

    /// Set name of the any hit shader.
    void setAnyHit(const wchar_t *name);

    /// Set name of the any hit shader.
    void setAnyHit(const std::wstring &name);

    /// Set name of the closest hit shader.
    void setClosestHit(const wchar_t *name);

    /// Set name of the closest hit shader.
    void setClosestHit(const std::wstring &name);

    /// Set name of the intersection shader.
    void setIntersection(const wchar_t *name);

    /// Set name of the intersection shader.
    void setIntersection(const std::wstring &name);

    /**
     * Get type of the sub-object contained in this wrapper.
     * Returns ::D3D12_STATE_SUBOBJECT_TYPE_MAX_VALID when the 
     * wrapper is empty.
     */
    virtual ::D3D12_STATE_SUBOBJECT_TYPE type() const noexcept override final
    { return ::D3D12_STATE_SUBOBJECT_TYPE_HIT_GROUP; }
private:
    /// Description of the hit group.
    ::D3D12_HIT_GROUP_DESC mDesc{ };
    /// Name of the hit group
    std::wstring mHitGroupName{ };
    /// Name of the any hit shader.
    std::wstring mAnyHitShader{ };
    /// Name of the closest hit shader.
    std::wstring mClosestHitShader{ };
    /// Name of the intersection shader.
    std::wstring mIntersectionShader{ };
protected:
}; // class D3D12DxilSubObjectsToExports

/// State object sub-object state types.
namespace sosot
{

using StateObjectConfig = ::quark::helpers::d3d12::D3D12StateObjectSubObject<
    ::D3D12_STATE_OBJECT_CONFIG, 
    ::D3D12_STATE_SUBOBJECT_TYPE_STATE_OBJECT_CONFIG>;
using GlobalRootSignature = ::quark::helpers::d3d12::D3D12StateObjectSubObject<
    ::D3D12_GLOBAL_ROOT_SIGNATURE, 
    ::D3D12_STATE_SUBOBJECT_TYPE_GLOBAL_ROOT_SIGNATURE>;
using LocalRootSignature = ::quark::helpers::d3d12::D3D12StateObjectSubObject<
    ::D3D12_LOCAL_ROOT_SIGNATURE, 
    ::D3D12_STATE_SUBOBJECT_TYPE_LOCAL_ROOT_SIGNATURE>;
using NodeMask = ::quark::helpers::d3d12::D3D12StateObjectSubObject<
    ::D3D12_NODE_MASK, 
    ::D3D12_STATE_SUBOBJECT_TYPE_NODE_MASK>;
using DxilLibrary = D3D12DxilLibrary;
using ExistingCollection = D3D12ExistingCollection;
using SubObjExportsAssociation = D3D12SubObjectsToExports;
using DxilSubObjExportsAssociation = D3D12DxilSubObjectsToExports;
using RayTracingShaderCfg = ::quark::helpers::d3d12::D3D12StateObjectSubObject<
    ::D3D12_RAYTRACING_SHADER_CONFIG, 
    ::D3D12_STATE_SUBOBJECT_TYPE_RAYTRACING_SHADER_CONFIG>;
using RayTracingPipelineCfg = ::quark::helpers::d3d12::D3D12StateObjectSubObject<
    ::D3D12_RAYTRACING_PIPELINE_CONFIG, 
    ::D3D12_STATE_SUBOBJECT_TYPE_RAYTRACING_PIPELINE_CONFIG>;
using HitGroup = D3D12HitGroup;

} // namespace sosot

} // namespace d3d12

} // namespace helpers

} // namespace quark

// Template implementation

namespace quark
{
namespace helpers
{
namespace d3d12
{


}
}
}

// Template implementation end

#endif
