/**
 * @file helpers/d3d12/D3D12ImGuiWrapper.h
 * @author Tomas Polasek
 * @brief Wrapper around ImGui, usable within D3D12.
 */

#pragma once

#include "engine/resources/d3d12/D3D12Device.h"
#include "engine/resources/d3d12/D3D12SwapChain.h"
#include "engine/resources/d3d12/D3D12CommandQueueMgr.h"
#include "engine/resources/d3d12/D3D12DescHeap.h"

/// Namespace containing the engine code.
namespace quark
{

/// Namespace containing helper classes and methods.
namespace helpers
{

/// Direct3D 12 helpers.
namespace d3d12
{

/// Helper class used for initializing and using the ImGui library.
class D3D12ImGuiWrapper
{
public:
    /**
     * Initialize ImGui library on specified device, using provided 
     * swap chain for drawing.
     * @param device Target device.
     * @param swapChain Swap chain used for drawing the GUI.
     */
    D3D12ImGuiWrapper(res::d3d12::D3D12Device &device, res::d3d12::D3D12SwapChain &swapChain);

    /// De-initialize ImGui.
    ~D3D12ImGuiWrapper();

    /**
     * Prepare for rendering of a new frame.
     * Should be called each frame, before rendering.
     */
    void newFrame();

    /**
     * Render ImGui data, posting draw commands on 
     * given command list.
     * @param cmdList Command list used for storing draw 
     * commands.
     */
    void render(res::d3d12::D3D12CommandList &cmdList);

    /// Free D3D12 objects without de-initializing.
    void freeD3D12Objects();
    /// Re-create D3D12 objects after freeing them.
    void createD3D12Objects();

    /// Resize the drawing area.
    void resize(int width, int height);
private:
    /// Required size of the SRV descriptor heap
    static constexpr uint32_t REQUIRED_SRV_DESCRIPTORS{ 1u };

    /**
     * Initialize ImGui library on specified device, using provided 
     * swap chain for drawing.
     * @param device Target device.
     * @param swapChain Swap chain used for drawing the GUI.
     */
    void initialize(res::d3d12::D3D12Device &device, res::d3d12::D3D12SwapChain &swapChain);

    /// De-initialize ImGui.
    void shutdown();

    /**
     * Prepare for rendering of a new frame.
     * Should be called each frame, before rendering.
     */
    void newFrameImpl();

    /**
     * Render ImGui data, posting draw commands on 
     * given command list.
     * @param cmdList Command list used for storing draw 
     * commands.
     */
    void renderImpl(res::d3d12::D3D12CommandList &cmdList);

    /// Free D3D12 objects without de-initializing.
    void freeD3D12ObjectsImpl();
    /// Re-create D3D12 objects after freeing them.
    void createD3D12ObjectsImpl();

    /// Descriptor heap used for storing shader resource views used by ImGui.
    res::d3d12::D3D12DescHeap::PtrT mSrvDescHeap;
protected:
}; // class D3D12ImGuiWrapper

} // namespace d3d12

} // namespace helpers

} // namespace quark
