/**
 * @file helpers/d3d12/D3D12RTBottomLevelAS.h
 * @author Tomas Polasek
 * @brief Helper used for building D3D12 bottom level acceleration 
 * structure for ray tracing.
 */

#pragma once

#ifdef ENGINE_RAY_TRACING_ENABLED

#include "engine/resources/d3d12/D3D12RayTracingDevice.h"
#include "engine/resources/d3d12/D3D12RayTracingCommandList.h"

#include "engine/resources/render/MeshMgr.h"

/// Namespace containing the engine code.
namespace quark
{

/// Namespace containing helper classes and methods.
namespace helpers
{

/// Direct3D 12 resource management.
namespace d3d12
{

/**
 * Helper used for building D3D12 bottom level acceleration 
 * structure for ray tracing.
 */
class D3D12RTBottomLevelAS
{
public:
    /**
     * Exception thrown when forcing update, but updating  
     * is not allowed.
     */
    struct InvalidUpdate : public std::exception
    {
        InvalidUpdate(const char *msg) :
            std::exception(msg)
        { }
    }; // struct InvalidUpdate

    /**
     * Structure containing information about required 
     * buffer sizes.
     */
    struct BufferSizeRequirements
    {
        /// Size of the AS build output buffer.
        ::UINT64 outputBufferSize{ 0u };
        /// Size of the AS build scratch buffer.
        ::UINT64 scratchBufferSize{ 0u };
    }; // struct BufferSizeRequirements

    /**
     * Our default priority for building bottom level AS is for ray tracing 
     * to be as fast as possible.
     */
    static constexpr ::D3D12_RAYTRACING_ACCELERATION_STRUCTURE_BUILD_FLAGS DEFAULT_BUILD_PRIORITY{ 
        ::D3D12_RAYTRACING_ACCELERATION_STRUCTURE_BUILD_FLAG_PREFER_FAST_TRACE };

    /// Uninitialized acceleration structure builder.
    D3D12RTBottomLevelAS();

    /// Free any resources used by the acceleration structure.
    ~D3D12RTBottomLevelAS();

    // No copying, allow moving.
    D3D12RTBottomLevelAS(const D3D12RTBottomLevelAS &other) = delete;
    D3D12RTBottomLevelAS &operator=(const D3D12RTBottomLevelAS &other) = delete;
    D3D12RTBottomLevelAS(D3D12RTBottomLevelAS &&other) = default;
    D3D12RTBottomLevelAS &operator=(D3D12RTBottomLevelAS &&other) = default;

    /// Clear the acceleration structure and any configuration.
    void clear();

    /**
     * Set whether the created bottom level acceleration structure 
     * can later be updated, without re-creating it from scratch.
     * @param allowUpdating Should updating be allowed?
     */
    void setAllowUpdating(bool allowUpdating);

    /**
     * Set whether to minimize memory used when building the 
     * acceleration structure. May result in slower ray tracing 
     * or slower build times!.
     * @param minimizeMemory Should memory minimization occur?
     */
    void setMinimizeMemory(bool minimizeMemory);

    /**
     * Set whether the compacting of resulting AS should be 
     * allowed.
     * @param allowCompacting Should compacting be allowed?
     */
    void setAllowCompacting(bool allowCompacting);

    /// Focus the build on fast AS.
    void preferFastRayTracing();

    /// Focus the build on finishing as fast as possible.
    void preferFastBuild();

    /**
     * Add new geometry to the acceleration structure.
     * @param primitive Primitive representing the geometry.
     */
    void addGeometryNoTransform(const res::rndr::Primitive &primitive);

    /**
     * Add new geometry to the acceleration structure.
     * @param meshHandle Mesh representing the geometry. 
     */
    void addGeometryNoTransform(const res::rndr::MeshHandle &meshHandle);

    /**
     * Calculate size requirements for AS buffers, for currently added 
     * geometry.
     * @param device Device used for ray tracing purposes.
     * @return Returns structure with required sizes.
     */
    BufferSizeRequirements calculateSizeRequirements(
        res::d3d12::D3D12RayTracingDevice &device);

    /// Get number of the currently added geometry triangles.
    uint64_t triangleCount() const;

    /// Get number of the currently added geometry meshes.
    uint64_t meshCount() const;

    /**
     * Build current configuration of bottom acceleration structures.
     * @param device Device used for ray tracing purposes.
     * @return Returns structure with required sizes.
     * @param directCmdList Command list used to record the building 
     * commands.
     * @param scratchBuffer Buffer used as scratch space during the 
     * acceleration structure build.
     * @param outputBuffer Resulting acceleration structure will be 
     * stored in this buffer.
     * @throw util::winexception Thrown when error occurs.
     * @warning All of the buffers must be aligned correctly and 
     * have the correct size. Additionally the output buffer must 
     * be in the correct state!
     */
    void build( res::d3d12::D3D12RayTracingDevice &device, 
        res::d3d12::D3D12RayTracingCommandList &directCmdList,
        res::d3d12::D3D12Resource &scratchBuffer,
        res::d3d12::D3D12Resource &outputBuffer);

    /**
     * Build current configuration of bottom acceleration structures.
     * @param device Device used for ray tracing purposes.
     * @param directCmdList Command list used to record the building 
     * commands.
     * @param scratchBuffer Buffer used as scratch space during the 
     * acceleration structure build.
     * @param outputBuffer Resulting acceleration structure will be 
     * stored in this buffer.
     * @param pastOutputBuffer Result from the last building of the 
     * acceleration structure. Contents will be used as an update 
     * base, if valid.
     * @param forceUpdate When set to true, the 
     * @throw util::winexception Thrown when error occurs.
     * @throw InvalidUpdate Thrown when forceUpdate is set to true 
     * but the update cannot be performed.
     * @warning All of the buffers must be aligned correctly and 
     * have the correct size. Additionally the output buffer must 
     * be in the correct state!
     */
    void build(res::d3d12::D3D12RayTracingDevice &device, 
        res::d3d12::D3D12RayTracingCommandList &directCmdList,
        res::d3d12::D3D12Resource &scratchBuffer,
        res::d3d12::D3D12Resource &outputBuffer, 
        res::d3d12::D3D12Resource &pastOutputBuffer, 
        bool forceUpdate = false);

    /// Get number of descriptions within this bottom level AS.
    std::size_t numDescriptions() const;
private:
    /**
     * Check whether given primitive is a valid geometry and can be 
     * added to this acceleration structure.
     * @param primitive Primitive being tested.
     * @return Returns true if the primitive is valid.
     */
    bool checkPrimitiveValid(const res::rndr::Primitive &primitive) const;

    /**
     * Generate geometry description for provided primitive.
     * @param primitive Primitive representing the geometry.
     * @return Returns geometry description for given primitive.
     */
    ::D3D12_RAYTRACING_GEOMETRY_DESC generateGeometryDesc(
        const res::rndr::Primitive &primitive) const;

    /**
     * Add given geometry description to the list of current 
     * geometry.
     * @param desc Description of the geometry.
     */
    void addGeometryDesc(const ::D3D12_RAYTRACING_GEOMETRY_DESC &desc);

    /**
     * Generate AS building flags from given parameters.
     * @param baseFlags Base building flags, specifying what are 
     * the priorities of the output AS.
     * @param allowUpdates Should the output AS allow updating?
     * @param minimizeMemory Should the output AS take up as small 
     * amount of memory as possible?
     * @param allowCompaction Should the output AS allow compacting 
     * operations, to save memory?
     */
    ::D3D12_RAYTRACING_ACCELERATION_STRUCTURE_BUILD_FLAGS generateBuildFlags(
        ::D3D12_RAYTRACING_ACCELERATION_STRUCTURE_BUILD_FLAGS baseFlags,
        bool allowUpdates, bool minimizeMemory, bool allowCompaction);

    /**
     * Add flag specifying to update the AS to the other provided 
     * flags.
     * @param startFlags Starting flags, the update flag will be 
     * added to them.
     * @return Returns starting flags with update flag included.
     */
    ::D3D12_RAYTRACING_ACCELERATION_STRUCTURE_BUILD_FLAGS addUpdateFlag(
        ::D3D12_RAYTRACING_ACCELERATION_STRUCTURE_BUILD_FLAGS startFlags);

    /**
     * Generate inputs structure for given list of geometry.
     * @param geometry List of geometry descriptions to create the inputs 
     * structure for.
     * @param buildFlags Building flags passed to the AS building command.
     * @return Returns filled build structure for geometry with given 
     * list of geometry.
     * @throw util::winexception Thrown on error.
     */
    ::D3D12_BUILD_RAYTRACING_ACCELERATION_STRUCTURE_INPUTS generateGeometryInputs(
        const std::vector<::D3D12_RAYTRACING_GEOMETRY_DESC> &geometry, 
        ::D3D12_RAYTRACING_ACCELERATION_STRUCTURE_BUILD_FLAGS buildFlags);

    /**
     * Get pre-build information for given acceleration structure 
     * building inputs.
     * @param device Device used for ray tracing purposes.
     * @param buildInputs Filled structure with bottom layer build 
     * information.
     * @return Returns filled pre-build structure for given inputs.
     * @throw util::winexception Thrown on error.
     */
    ::D3D12_RAYTRACING_ACCELERATION_STRUCTURE_PREBUILD_INFO getPreBuildInfo(
        res::d3d12::D3D12RayTracingDevice &device, 
        const ::D3D12_BUILD_RAYTRACING_ACCELERATION_STRUCTURE_INPUTS &buildInputs);

    /**
     * Recalculate information structures for the current geometry.
     * @param device Device used for ray tracing purposes.
     */
    void recalculateInfoStructures(
        res::d3d12::D3D12RayTracingDevice &device);

    /**
     * Generate build command description for provided parameters.
     * @param geometryInputs Description of the geometry.
     * @param scratchBuffer Helper buffer used for building the AS.
     * @param outputBuffer Buffer where the final AS will be stored.
     * @param pastOutputBuffer Optional buffer with AS from the last 
     * build.
     */
    ::D3D12_BUILD_RAYTRACING_ACCELERATION_STRUCTURE_DESC generateBuildDesc(
        const ::D3D12_BUILD_RAYTRACING_ACCELERATION_STRUCTURE_INPUTS &geometryInputs, 
        res::d3d12::D3D12Resource &scratchBuffer,
        res::d3d12::D3D12Resource &outputBuffer, 
        res::d3d12::D3D12Resource *pastOutputBuffer = nullptr);

    /// Has there been a change to the added primitives?
    bool mDirty{ true };
    /// Should updating be allowed?
    bool mAllowUpdating{ false };
    /// Should memory minimization occur?
    bool mMinimizeMemory{ false };
    /// Should compacting of AS be allowed?
    bool mAllowCompacting{ false };
    /// List of geometry to build the AS for.
    std::vector<::D3D12_RAYTRACING_GEOMETRY_DESC> mGeometry{ };
    /// Base building flags, specifying priorities of AS building.
    ::D3D12_RAYTRACING_ACCELERATION_STRUCTURE_BUILD_FLAGS mBaseFlags{ };

    /// Flags used for building the bottom level AS. May be invalid if mDirty is set to true!
    ::D3D12_RAYTRACING_ACCELERATION_STRUCTURE_BUILD_FLAGS mBuildFlags{ };
    /// Structure containing information about geometry. May be invalid if mDirty is set to true!
    ::D3D12_BUILD_RAYTRACING_ACCELERATION_STRUCTURE_INPUTS mGeometryInputs{ };
    /// Calculated pre-build info. May be invalid if mDirty is set to true!
    ::D3D12_RAYTRACING_ACCELERATION_STRUCTURE_PREBUILD_INFO mPreBuildInfo{ };

    /// Number of triangles of the currently added geometry.
    uint64_t mTriangleCount{ 0u };
    /// Number of meshes in the currently added geometry.
    uint64_t mMeshCount{ 0u };
protected:
}; // class D3D12RTBottomLevelAS

} // namespace d3d12

} // namespace helpers

} // namespace quark

// Template implementation

namespace quark
{

namespace helpers
{

namespace d3d12
{

}

}

}

// Template implementation end

#endif 
