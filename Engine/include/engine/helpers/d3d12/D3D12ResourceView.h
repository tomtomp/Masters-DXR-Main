/**
 * @file helpers/d3d12/D3D12ResourceView.h
 * @author Tomas Polasek
 * @brief Wrapper around a D3D12 resource pointer and a resource view.
 */

#pragma once

#include "engine/renderer/ResourceViewType.h"

/// Namespace containing the engine code.
namespace quark
{

// Forward declaration
namespace res
{
namespace d3d12
{

class D3D12Resource;
class D3D12DescAllocatorInterface;

}
}

/// Namespace containing helper classes and methods.
namespace helpers
{

/// Direct3D 12 resource management.
namespace d3d12
{

/**
 * Wrapper around a D3D12 resource pointer and a resource view.
 */
class D3D12ResourceView : public util::PointerType<D3D12ResourceView>
{
public:
    /**
     * Initialize empty resource view.
     */
    static PtrT create();

    /**
     * Create resource view for provided resource.
     * @param resource Resource to create the view for.
     * @param descHeap Descriptor heap used to create the descriptor.
     * @param viewType Type of the view which should be created.
     * @param forceFormat When specified, given format will be 
     * used instead of the format of the texture.
     */
    static PtrT create(util::PtrT<res::d3d12::D3D12Resource> resource, 
        res::d3d12::D3D12DescAllocatorInterface &descHeap, 
        quark::rndr::ResourceViewType viewType, 
        ::DXGI_FORMAT forceFormat = ::DXGI_FORMAT_UNKNOWN);

    /// Destroy the resource shared pointer.
    ~D3D12ResourceView();

    // No copying.
    D3D12ResourceView(const D3D12ResourceView &other) = delete;
    D3D12ResourceView &operator=(const D3D12ResourceView &other) = delete;
    // Moving is allowed.
    D3D12ResourceView(D3D12ResourceView &&other) = default;
    D3D12ResourceView &operator=(D3D12ResourceView &&other) = default;

    /**
     * Get CPU descriptor handle for this resource view.
     * The handle may be invalid, if dirty().
     * @return Returns the CPU descriptor handle.
     */
    ::CD3DX12_CPU_DESCRIPTOR_HANDLE cpuHandle() const;

    /**
     * Get CPU descriptor handle for this resource view.
     * The handle may be invalid, if dirty().
     * @return Returns the GPU descriptor handle.
     */
    ::CD3DX12_GPU_DESCRIPTOR_HANDLE gpuHandle() const;

    /**
     * Reallocate descriptors for a completely different resource.
     * @param resource Resource to create the view for.
     * @param descHeap Descriptor heap used to create the descriptor.
     * @param viewType Type of the view which should be created.
     * @param forceFormat When specified, given format will be 
     * used instead of the format of the texture.
     */
    void reallocateDescriptor(
        util::PtrT<res::d3d12::D3D12Resource> resource, 
        res::d3d12::D3D12DescAllocatorInterface &descHeap, 
        quark::rndr::ResourceViewType viewType, 
        ::DXGI_FORMAT forceFormat = ::DXGI_FORMAT_UNKNOWN);

    /**
     * Reallocate descriptors for this resource view.
     * May decide to allocate a new one, if the descriptor 
     * heap is different than the last one.
     * @param descHeap Descriptor heap used for the allocation.
     * @param forceFormat When specified, given format will be 
     * used instead of the format of the texture.
     */
    void reallocateDescriptor(res::d3d12::D3D12DescAllocatorInterface &descHeap, 
        ::DXGI_FORMAT forceFormat = ::DXGI_FORMAT_UNKNOWN);

    /// Type of this resource view.
    quark::rndr::ResourceViewType type() const;

    /// Is reallocation required?
    bool dirty() const;

    /// Is this view initialized and not dirty?
    bool valid() const;

    /// Is this view valid?
    operator bool() const;
private:
    /// Shared pointer to the target resource of this view.
    util::WeakPtrT<res::d3d12::D3D12Resource> mTargetResource{ };

    /// Type of the currently contained resource view.
    quark::rndr::ResourceViewType mViewType{ quark::rndr::ResourceViewType::Unknown };

    /// Allocated CPU descriptor handle for this buffer.
    ::CD3DX12_CPU_DESCRIPTOR_HANDLE mCpuHandle{ };
    /// Allocated GPU descriptor handle for this buffer.
    ::CD3DX12_GPU_DESCRIPTOR_HANDLE mGpuHandle{ };

    /// Cached pointer to the last resource which is currently viewed.
    ::ID3D12Resource *mViewedResource{ };
protected:
    /**
     * Initialize empty resource view.
     */
    D3D12ResourceView();

    /**
     * Create resource view for provided resource.
     * @param resource Resource to create the view for.
     * @param descHeap Descriptor heap used to create the descriptor.
     * @param viewType Type of the view which should be created.
     * @param forceFormat When specified, given format will be 
     * used instead of the format of the texture.
     */
    D3D12ResourceView(util::PtrT<res::d3d12::D3D12Resource> resource, 
        res::d3d12::D3D12DescAllocatorInterface &descHeap, 
        quark::rndr::ResourceViewType viewType, 
        ::DXGI_FORMAT forceFormat = ::DXGI_FORMAT_UNKNOWN);
}; // class D3D12ResourceView

} // namespace d3d12

} // namespace helpers

} // namespace quark

// Template implementation

namespace quark
{

namespace helpers
{

namespace d3d12
{

}

}

}

// Template implementation end
