/**
 * @file helpers/d3d12/D3D12BaseGpuAllocator.h
 * @author Tomas Polasek
 * @brief Direct3D 12 GPU memory allocator base.
 */

#pragma once

#include "engine/util/SafeD3D12.h"

/// Namespace containing the engine code.
namespace quark
{

/// Namespace containing helper classes and methods.
namespace helpers
{

/// Direct3D 12 resource management.
namespace d3d12
{

/**
 * Base allocator for Direct3D 12 GPU memory.
 */
class D3D12BaseGpuAllocator
{
public:
    /// Initialize allocator resources.
    D3D12BaseGpuAllocator() = default;

    virtual ~D3D12BaseGpuAllocator() = default;

    // Allow copying.
    D3D12BaseGpuAllocator(const D3D12BaseGpuAllocator &other) = default;
    D3D12BaseGpuAllocator &operator=(const D3D12BaseGpuAllocator &other) = default;
    // Allow moving.
    D3D12BaseGpuAllocator(D3D12BaseGpuAllocator &&other) = default;
    D3D12BaseGpuAllocator &operator=(D3D12BaseGpuAllocator &&other) = default;

    /**
     * Allocate resource described in the provided 
     * description structure. After creation, the 
     * resource will have requested initial state and 
     * use optimized clear value.
     * @param desc Description of the resource.
     * @param initState Initial state of the resource 
     * after allocation.
     * @param optimizedClearValue Clear value used by 
     * the resource. Can be nullptr.
     * @param riid Identifier of the resource interface. 
     * Can be gained by using IID_PPV_ARGS macro.
     * @param ptr Pointer to the resource being allocated. 
     * Can be gained by using IID_PPV_ARGS macro.
     * @throws On allocation error throws util::winexception 
     * which contains error description.
     */
    virtual void allocate(const ::D3D12_RESOURCE_DESC &desc,
        ::D3D12_RESOURCE_STATES initState,
        const ::D3D12_CLEAR_VALUE *optimizedClearValue,
        REFIID riid, void **ptr) = 0;

    /**
     * Deallocate given resource.
     * @tparam ResT Type of the target resource.
     * @param riid Identifier of the resource interface. 
     * Can be gained by using IID_PPV_ARGS macro.
     * @param ptr Pointer to the resource being allocated. 
     * Can be gained by using IID_PPV_ARGS macro.
     * @throws On deallocation error throws util::winexception 
     * which contains error description.
     * @warning Only resource allocated by this allocator 
     * should be passed to the deallocate method!
     */
    virtual void deallocate(REFIID riid, void **ptr) = 0;

    /// Is the allocator valid?
    virtual explicit operator bool() const = 0;
private:
protected:
}; // class D3D12BaseGpuAllocator

} // namespace d3d12

} // namespace helpers

} // namespace quark
