/**
 * @file helpers/d3d12/D3D12DummyAllocator.h
 * @author Tomas Polasek
 * @brief Dummy allocator for Direct3D 12 placed 
 * resources, which allows computation of required 
 * size of the target buffer.
 */

#pragma once

#include "engine/helpers/d3d12/D3D12BaseGpuAllocator.h"

#include "engine/resources/d3d12/D3D12Device.h"

/// Namespace containing the engine code.
namespace quark
{

/// Namespace containing helper classes and methods.
namespace helpers
{

/// Direct3D 12 resource management.
namespace d3d12
{

/**
 * Dummy allocator which allows the calculation of 
 * buffer size for a set set of resources.
 */
class D3D12DummyAllocator : public D3D12BaseGpuAllocator, public util::PointerType<D3D12DummyAllocator>
{
public:
    /**
     * Create allocator for specified device.
     * @param device Device which will be used for the 
     * real allocation.
     * @throws Throws util::winexception on error.
     */
    static PtrT create(res::d3d12::D3D12Device &device);

    /// No resources need to be freed.
    virtual ~D3D12DummyAllocator() = default;

    /// Copy allocator and share state with it.
    D3D12DummyAllocator(const D3D12DummyAllocator &other) = default;
    /// Copy allocator and share state with it.
    D3D12DummyAllocator &operator=(const D3D12DummyAllocator &other) = default;
    /// Move allocator and its state.
    D3D12DummyAllocator(D3D12DummyAllocator &&other) = default;
    /// Move allocator and its state.
    D3D12DummyAllocator &operator=(D3D12DummyAllocator &&other) = default;

    /**
     * Allocate resource described in the provided 
     * description structure. 
     * No actual allocation will be performed, but 
     * this allocators counter will be move accordingly.
     * @param desc Description of the resource.
     * @param initState Unused.
     * @param optimizedClearValue Unused.
     * @param riid Unused.
     * @param ptr Unused.
     * @throws On allocation error throws util::winexception 
     * which contains error description.
     */
    virtual void allocate(const D3D12_RESOURCE_DESC &desc,
        D3D12_RESOURCE_STATES initState,
        const D3D12_CLEAR_VALUE *optimizedClearValue,
        REFIID riid, void **ptr) override final;

    /**
     * Performs not operation.
     */
    virtual void deallocate(REFIID riid, void **ptr) override final;

    /// Is this allocator valid?
    virtual explicit operator bool() const override
    { return mShared.get(); }

    /// Get number of bytes allocated using this allocator.
    std::size_t allocated() const noexcept;
private:
    /// Date shared between allocator instances.
    struct SharedAllocatorData
    {
        explicit SharedAllocatorData(res::d3d12::D3D12Device &device) :
            device{ &device }
        { }

        /// Target device for the allocation.
        res::d3d12::D3D12Device *device{ nullptr };
        /// Number of bytes allocated using this allocator.
        std::size_t allocatedSize{ 0u };
        /// Mutex used for locking before accessing the heap.
        std::mutex mtx;
    }; // struct SharedAllocatorDate
    /// Data shared between instances.
    std::shared_ptr<SharedAllocatorData> mShared;
protected:
    /**
     * Create un-initialized allocator. It must be initialized 
     * before further use.
     */
    D3D12DummyAllocator();

    /**
     * Create allocator for specified device.
     * @param device Device which will be used for the 
     * real allocation.
     * @throws Throws util::winexception on error.
     */
    D3D12DummyAllocator(res::d3d12::D3D12Device &device);
}; // class D3D12PlacedAllocator

} // namespace d3d12

} // namespace helpers

} // namespace quark
