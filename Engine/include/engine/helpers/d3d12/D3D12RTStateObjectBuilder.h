/**
 * @file helpers/d3d12/D3D12RTStateObjectBuilder.h
 * @author Tomas Polasek
 * @brief D3D12 state object configurator and builder.
 */

#pragma once

#ifdef ENGINE_RAY_TRACING_ENABLED

#include "engine/resources/d3d12/D3D12Device.h"
#include "engine/resources/d3d12/D3D12RayTracingDevice.h"

#include "engine/resources/d3d12/D3D12StateObject.h"
#include "engine/resources/d3d12/D3D12RayTracingStateObject.h"

#include "engine/helpers/d3d12/D3D12RTStateObjectSOBuilder.h"

/// Namespace containing the engine code.
namespace quark
{

/// Namespace containing helper classes and methods.
namespace helpers
{

/// Direct3D 12 resource management.
namespace d3d12
{

/**
 * D3D12 state object configurator and builder.
 */
class D3D12RTStateObjectBuilder
{
public:
    /**
     * Initialize empty builder.
     * @param type Type of state objects built by this builder.
     */
    D3D12RTStateObjectBuilder(::D3D12_STATE_OBJECT_TYPE type = ::D3D12_STATE_OBJECT_TYPE_RAYTRACING_PIPELINE);
        
    /// Free any allocated buffers.
    ~D3D12RTStateObjectBuilder();

    // Allow copying.
    D3D12RTStateObjectBuilder(const D3D12RTStateObjectBuilder &other) = default;
    D3D12RTStateObjectBuilder &operator=(const D3D12RTStateObjectBuilder &other) = default;
    // Moving is allowed.
    D3D12RTStateObjectBuilder(D3D12RTStateObjectBuilder &&other) = default;
    D3D12RTStateObjectBuilder &operator=(D3D12RTStateObjectBuilder &&other) = default;

    /// Clear the current configuration.
    void clear();

    /**
     * Add provided state object stream sub-object to the 
     * configuration of the current state object.
     * @tparam SubT Sub-object type, must be instance of 
     * the template D3D12StateObjectSubObjectBase.
     * @return Returns pointer to the sub-object.
     * @warning After making changes to the sub-object, 
     * finalize() should always be called!
     */
    template <typename SubT>
    SubT *add();

    /**
     * Build current state object configuration into a 
     * state object and return it.
     * @param device Device to build the state object on.
     * @return Returns state object created from the 
     * current configuration of this builder.
     * @throws Throws util::winexception on error.
     */
    res::d3d12::D3D12StateObject::PtrT build(res::d3d12::D3D12Device &device);

    /**
     * Build current state object configuration into a 
     * state object and return it.
     * This version is used for creating ray tracing state 
     * objects.
     * @param device Device to build the state object on.
     * @return Returns state object created from the 
     * current configuration of this builder.
     * @throws Throws util::winexception on error.
     */
    res::d3d12::D3D12RayTracingStateObject::PtrT build(res::d3d12::D3D12RayTracingDevice &device);
private:
    /**
     * Generate state object description for currently 
     * configured sub objects.
     * @return Returns filled description structure.
     */
    ::D3D12_STATE_OBJECT_DESC generateDesc();

    /// List of currently used sub-object managers.
    std::vector<std::unique_ptr<D3D12StateObjectSubObjectBase>> mSubObjectManagers{ };
    /// List of currently configured sub-object states.
    std::list<::D3D12_STATE_SUBOBJECT> mSubObjects{ };
    /// Sub objects in linear memory.
    std::vector<::D3D12_STATE_SUBOBJECT> mLinearSubObjects{ };

    /// Type of the built state object.
    ::D3D12_STATE_OBJECT_TYPE mType{ };
protected:
}; // class D3D12RTStateObjectBuilder

} // namespace d3d12

} // namespace helpers

} // namespace quark

// Template implementation

namespace quark
{

namespace helpers
{

namespace d3d12
{

template <typename SubT>
SubT *D3D12RTStateObjectBuilder::add()
{
    // Create the sub object.
    mSubObjects.emplace_back();

    // Create manager for the sub object.
    std::unique_ptr<SubT> subObject{ std::make_unique<SubT>(mSubObjects.back(), mSubObjects.size() - 1u) };
    const auto subObjectPtr{ subObject.get() };

    // Register the manager.
    mSubObjectManagers.push_back(std::move(subObject));

    // Return manager to the caller.
    return subObjectPtr;
}

}

}

}

// Template implementation end

#endif 
