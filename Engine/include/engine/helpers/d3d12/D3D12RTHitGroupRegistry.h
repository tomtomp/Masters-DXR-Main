/**
 * @file helpers/d3d12/D3D12RTHitGroupRegistry.h
 * @author Tomas Polasek
 * @brief Registration of hit groups and their shaders.
 */

#pragma once

#ifdef ENGINE_RAY_TRACING_ENABLED

#include "engine/helpers/d3d12/D3D12RTShaderLibraryRegistry.h"

/// Namespace containing the engine code.
namespace quark
{

/// Namespace containing helper classes and methods.
namespace helpers
{

/// Direct3D 12 resource management.
namespace d3d12
{

/**
 * Registration of hit groups and their shaders.
 */
class D3D12RTHitGroupRegistry
{
public:
    /**
     * Exception thrown when trying to use the same name 
     * for 2 hit groups.
     */
    struct AlreadyRegisteredException : public std::exception
    {
        AlreadyRegisteredException(const char *msg) :
            std::exception(msg)
        { }
    }; // struct AlreadyRegisteredException

    /// Empty hit group registry.
    D3D12RTHitGroupRegistry();

    /// Free any resources used by the registry.
    ~D3D12RTHitGroupRegistry();

    // No copying, allow moving.
    D3D12RTHitGroupRegistry(const D3D12RTHitGroupRegistry &other) = delete;
    D3D12RTHitGroupRegistry &operator=(const D3D12RTHitGroupRegistry &other) = delete;
    D3D12RTHitGroupRegistry(D3D12RTHitGroupRegistry &&other) = default;
    D3D12RTHitGroupRegistry &operator=(D3D12RTHitGroupRegistry &&other) = default;

    /// Reset the list of registered hit groups.
    void clear();

    /**
     * Register a new hit group under a given name.
     * @param hitGroupName Identifier of the whole hit group.
     * @param closestHitName Identifier of the closest hit 
     * shader.
     * @param anyHitName Identifier of the any hit shader.
     * @param intersectionName Identifier of the intersection 
     * shader.
     * @param type Type of primitives this group is used for.
     * @throws AlreadyRegisteredException Thrown when a hit 
     * group with given name already exists.
     */
    void registerHitGroup(
        const std::wstring &hitGroupName, 
        const std::wstring &closestHitName, 
        const std::wstring &anyHitName = L"", 
        const std::wstring &intersectionName = L"", 
        ::D3D12_HIT_GROUP_TYPE type = ::D3D12_HIT_GROUP_TYPE_TRIANGLES);

    /// Access the registered hit groups
    const auto &hitGroups() const
    { return mRegistry; }
private:
    /**
     * Helper structure for holding information about a hit 
     * group and its shader identifiers.
     */
    struct HitGroupRecord
    {
        /**
         * Print debug information about this record.
         * @param name Name of the record, taken from map.
         */
        void printInfo(const std::wstring &name) const;

        /// Identifier of the closest hit shader.
        std::wstring closestHitName{ };
        /// Identifier of the any hit shader.
        std::wstring anyHitName{ };
        /// Identifier of the intersection shader.
        std::wstring intersectionName{ };
        /// Type of primitives this group is used for.
        ::D3D12_HIT_GROUP_TYPE type{ };
    }; // struct HitGroupRecord

    /// Registry of hit groups, associated with their names.
    std::map<std::wstring, HitGroupRecord> mRegistry{ };
protected:
}; // class D3D12RTHitGroupRegistry

} // namespace d3d12

} // namespace helpers

} // namespace quark

// Template implementation

namespace quark
{

namespace helpers
{

namespace d3d12
{

}

}

}

// Template implementation end

#endif 
