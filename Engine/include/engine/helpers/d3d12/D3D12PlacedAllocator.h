/**
 * @file helpers/d3d12/D3D12PlacedAllocator.h
 * @author Tomas Polasek
 * @brief Allocator for Direct3D 12 placed resources.
 */

#pragma once

#include "engine/helpers/d3d12/D3D12BaseGpuAllocator.h"

#include "engine/resources/d3d12/D3D12Device.h"
#include "engine/resources/d3d12/D3D12Heap.h"

/// Namespace containing the engine code.
namespace quark
{

/// Namespace containing helper classes and methods.
namespace helpers
{

/// Direct3D 12 resource management.
namespace d3d12
{

/**
 * Simple Direct3D resource allocator, which allows 
 * the allocation of placed resources. 
 * For now the allocator just allows sequential 
 * placing of new resources into a heap created in 
 * the initialization phase.
 * The allocator is thread-safe and allows copying 
 * and moving with shared state.
 */
class D3D12PlacedAllocator : public D3D12BaseGpuAllocator, public util::PointerType<D3D12PlacedAllocator>
{
public:
    /**
     * Create allocator for specified heap. The ownership 
     * of the heap is passing to this allocator.
     * @param device Device the heap lives on.
     * @param heap Heap to use for allocation.
     * @throws Throws util::winexception on error.
     */
    static PtrT create(res::d3d12::D3D12Device &device, res::d3d12::D3D12Heap::PtrT heap);

    /**
     * Create the allocator with heap whose information 
     * is specified within the description structure.
     * @param device Device to allocate on.
     * @param desc Properties of the explicit heap.
     * @throws Throws util::winexception on error.
     */
    static PtrT create(res::d3d12::D3D12Device &device, const ::CD3DX12_HEAP_DESC &desc);

    /**
     * Create the allocator with heap whose information 
     * is specified within the description structure.
     * @param device Device to allocate on.
     * @param desc Properties of the explicit heap.
     * @throws Throws util::winexception on error.
     */
    static PtrT create(res::d3d12::D3D12Device &device, const ::D3D12_HEAP_DESC &desc);

    /// No resources need to be freed.
    virtual ~D3D12PlacedAllocator() = default;

    /// Copy allocator and share state with it.
    D3D12PlacedAllocator(const D3D12PlacedAllocator &other) = default;
    /// Copy allocator and share state with it.
    D3D12PlacedAllocator &operator=(const D3D12PlacedAllocator &other) = default;
    /// Move allocator and its state.
    D3D12PlacedAllocator(D3D12PlacedAllocator &&other) = default;
    /// Move allocator and its state.
    D3D12PlacedAllocator &operator=(D3D12PlacedAllocator &&other) = default;

    /**
     * Allocate resource described in the provided 
     * description structure. After creation, the 
     * resource will have requested initial state and 
     * use optimized clear value.
     * @param desc Description of the resource.
     * @param initState Initial state of the resource 
     * after allocation.
     * @param optimizedClearValue Clear value used by 
     * the resource. Can be nullptr.
     * @param riid Identifier of the resource interface. 
     * Can be gained by using IID_PPV_ARGS macro.
     * @param ptr Pointer to the resource being allocated. 
     * Can be gained by using IID_PPV_ARGS macro.
     * @throws On allocation error throws util::winexception 
     * which contains error description.
     */
    virtual void allocate(const D3D12_RESOURCE_DESC &desc,
        D3D12_RESOURCE_STATES initState,
        const D3D12_CLEAR_VALUE *optimizedClearValue,
        REFIID riid, void **ptr) override final;

    /**
     * Deallocate given resource.
     * @tparam ResT Type of the target resource.
     * @param riid Identifier of the resource interface. 
     * Can be gained by using IID_PPV_ARGS macro.
     * @param ptr Pointer to the resource being allocated. 
     * Can be gained by using IID_PPV_ARGS macro.
     * @throws On deallocation error throws util::winexception 
     * which contains error description.
     * @warning Only resource allocated by this allocator 
     * should be passed to the deallocate method!
     */
    virtual void deallocate(REFIID riid, void **ptr) override final;

    /// Is this allocator valid?
    virtual explicit operator bool() const override
    { return mShared.get(); }
private:
    /// Date shared between allocator instances.
    struct SharedAllocatorData
    {
        /// Create from existing heap.
        SharedAllocatorData(res::d3d12::D3D12Device &dev, res::d3d12::D3D12Heap::PtrT &srcHeap) :
            device{ &dev }, 
            allocationHeap{ srcHeap }
        { }

        /// Create a new heap.
        SharedAllocatorData(res::d3d12::D3D12Device &dev, const ::CD3DX12_HEAP_DESC &desc) :
            device{ &dev }, 
            allocationHeap{ res::d3d12::D3D12Heap::create(dev, desc) }
        { }

        /// Device used for the allocation.
        res::d3d12::D3D12Device *device;
        /// Heap used for allocation of resources.
        res::d3d12::D3D12Heap::PtrT allocationHeap;
        /// Offset of the first unallocated byte on the heap.
        std::size_t allocationOffset{ 0u };
        /// Mutex used for locking before accessing the heap.
        std::mutex mtx;
    }; // struct SharedAllocatorDate

    /// Data shared between instances.
    std::shared_ptr<SharedAllocatorData> mShared;
protected:
    /**
     * Create un-initialized allocator. It must be initialized 
     * before further use.
     */
    D3D12PlacedAllocator();

    /**
     * Create allocator for specified heap. The ownership 
     * of the heap is passing to this allocator.
     * @param device Device the heap lives on.
     * @param heap Heap to use for allocation.
     * @throws Throws util::winexception on error.
     */
    D3D12PlacedAllocator(res::d3d12::D3D12Device &device, res::d3d12::D3D12Heap::PtrT heap);

    /**
     * Create the allocator with heap whose information 
     * is specified within the description structure.
     * @param device Device to allocate on.
     * @param desc Properties of the explicit heap.
     * @throws Throws util::winexception on error.
     */
    D3D12PlacedAllocator(res::d3d12::D3D12Device &device, const ::CD3DX12_HEAP_DESC &desc);

    /**
     * Create the allocator with heap whose information 
     * is specified within the description structure.
     * @param device Device to allocate on.
     * @param desc Properties of the explicit heap.
     * @throws Throws util::winexception on error.
     */
    D3D12PlacedAllocator(res::d3d12::D3D12Device &device, const ::D3D12_HEAP_DESC &desc);
}; // class D3D12PlacedAllocator

} // namespace d3d12

} // namespace helpers

} // namespace quark
