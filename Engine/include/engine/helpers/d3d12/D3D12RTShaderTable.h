/**
 * @file helpers/d3d12/D3D12RTShaderTable.h
 * @author Tomas Polasek
 * @brief Wrapper around ray tracing shader table, which allows 
 * storing the shader records and their local root parameters.
 */

#pragma once

#ifdef ENGINE_RAY_TRACING_ENABLED

#include "engine/resources/d3d12/D3D12RayTracingDevice.h"
#include "engine/resources/d3d12/D3D12RayTracingStateObject.h"

/// Namespace containing the engine code.
namespace quark
{

/// Namespace containing helper classes and methods.
namespace helpers
{

/// Direct3D 12 resource management.
namespace d3d12
{

/**
 * Wrapper around ray tracing shader table, which allows 
 * storing the shader records and their local root parameters.
 */
class D3D12RTShaderTable
{
public:
    /**
     * Exception thrown when provided group name does not exist 
     * in the ray tracing pipeline state object.
     */
    struct UnknownNameException : public std::exception
    {
        UnknownNameException(const char *msg) :
            std::exception(msg)
        { }
    }; // struct UnknownNameException

    /// Initialize empty shader table.
    D3D12RTShaderTable();

    /// Free all resources.
    ~D3D12RTShaderTable();

    // No copying, allow moving.
    D3D12RTShaderTable(const D3D12RTShaderTable &other) = delete;
    D3D12RTShaderTable &operator=(const D3D12RTShaderTable &other) = delete;
    D3D12RTShaderTable(D3D12RTShaderTable &&other) = default;
    D3D12RTShaderTable &operator=(D3D12RTShaderTable &&other) = default;

    /// Clear all added shader records.
    void clear();

    /**
     * Add a ray generation shader, specified by its name used by the ray 
     * tracing pipeline.
     * @param name Name of the ray generation shader. Must be the same as 
     * the one used in the DXIL library, when generating the pipeline.
     * @param rootSignatureArguments list of parameters stored as parameters 
     * for the local root signature. All elements will be aligned to either 
     * 4 bytes - when sizeof() is 4 or less - or 8 bytes - when sizeof() is 
     * more than 4 bytes.
     * @return Returns index of the ray generation group in the final order.
     */
    template <typename... RSArgs>
    std::size_t addRayGenerationGroup(const std::wstring &name, RSArgs... rootSignatureArguments);

    /**
     * Add a miss shader, specified by its name used by the ray tracing pipeline.
     * @param name Name of the miss shader. Must be the same as the one used in 
     * the DXIL library, when generating the pipeline.
     * @param rootSignatureArguments list of parameters stored as parameters 
     * for the local root signature. All elements will be aligned to either 
     * 4 bytes - when sizeof() is 4 or less - or 8 bytes - when sizeof() is 
     * more than 4 bytes.
     * @return Returns index of the miss group in the final order.
     */
    template <typename... RSArgs>
    std::size_t addMissGroup(const std::wstring &name, RSArgs... rootSignatureArguments);

    /**
     * Add a hit group shaders, specified by its name used by the ray tracing 
     * pipeline.
     * @param name Name of the hit group shaders. Must be the same as the one 
     * used in the DXIL library, when generating the pipeline.
     * @param rootSignatureArguments list of parameters stored as parameters 
     * for the local root signature. All elements will be aligned to either 
     * 4 bytes - when sizeof() is 4 or less - or 8 bytes - when sizeof() is 
     * more than 4 bytes.
     * @return Returns index of the hit group in the final order.
     */
    template <typename... RSArgs>
    std::size_t addHitGroup(const std::wstring &name, RSArgs... rootSignatureArguments);

    /// Clean all of the hit group records.
    void cleanHitGroups();

    /**
     * Calculate size requirements for the output buffer, which will 
     * contain the shader table.
     * @param device Device used for ray tracing purposes.
     * @return Returns required size of the output buffer in bytes.
     */
    ::UINT64 calculateSizeRequirements(res::d3d12::D3D12RayTracingDevice &device);

    /**
     * Build the current configuration of this shader table and copy 
     * the resulting structure into provided GPU buffer.
     * @param device Device used for ray tracing purposes.
     * @param pipeline Ray tracing pipeline state, used for translating 
     * the provided shader group names into their identifiers.
     * @param outputBuffer Buffer used to store the resulting data 
     * structure.
     * @throws util::winexception Thrown on d3d12 error.
     * @throws UnknownNameException Thrown when a group name could not 
     * be found in provided ray tracing pipeline.
     * @warning Provided buffer must be at least as large as the 
     * calculateSizeRequirements() specifies!
     */
    void build(res::d3d12::D3D12RayTracingDevice &device, 
        res::d3d12::D3D12RayTracingStateObject &pipeline, 
        res::d3d12::D3D12Resource &outputBuffer);

    /**
     * Get size of a single ray generation record. 
     * @warning Must be called only after building the table!
     */
    ::UINT64 rayGenerationRecordSize() const;

    /**
     * Size of the ray generation part of the shader table. 
     * @warning Must be called only after building the table!
     */
    ::UINT64 rayGenerationPartSize() const;

    /**
     * Offset of the ray generation part from the start of the shader table. 
     * @warning Must be called only after building the table!
     */
    ::UINT64 rayGenerationPartOffset() const;
    
    /**
     * Get size of a single miss record. 
     * @warning Must be called only after building the table!
     */
    ::UINT64 missRecordSize() const;

    /**
     * Size of the miss part of the shader table.
     * @warning Must be called only after building the table!
     */
    ::UINT64 missPartSize() const;

    /**
     * Offset of the miss part from the start of the shader table. 
     * @warning Must be called only after building the table!
     */
    ::UINT64 missPartOffset() const;

    /**
     * Get size of a single hit group record. 
     * @warning Must be called only after building the table!
     */
    ::UINT64 hitGroupRecordSize() const;

    /**
     * Size of the hit group part of the shader table.
     * @warning Must be called only after building the table!
     */
    ::UINT64 hitGroupPartSize() const;

    /**
     * Offset of the hit group part from the start of the shader 
     * table. 
     * @warning Must be called only after building the table!
     */
    ::UINT64 hitGroupPartOffset() const;

    /**
     * Print information about the current layout of the shader 
     * table.
     * @warning Must be called only after building the table, or 
     * after calling calculateSizeRequirements()!
     */
    void printLayoutInfo() const;
private:
    /// Helper structure used for storing shader group information.
    class ShaderRecord
    {
    public:
        ShaderRecord(const std::wstring &name);

        /**
         * Push given data onto the root argument vector, using 
         * given alignment.
         * @param rootSignatureArgument Argument being added to 
         * the list.
         * @param prePadding How many bytes of padding should be 
         * added before pushing the new argument.
         */
        template <typename T>
        void pushRootArgument(T rootSignatureArgument, std::size_t prePadding);

        /// How many bytes do the arguments occupy at the moment?
        std::size_t currentArgumentBytes() const;

        /// Get pointer to the root arguments data.
        const uint8_t *rootArguments() const;

        /// Get the name used by this record.
        const std::wstring &name() const;
    private:
        /// Name used by this record.
        std::wstring mName{ };
        /// Data passed to the root signature.
        std::vector<uint8_t> mRootArguments{ };
    protected:
    }; // class ShaderRecord

    /**
     * Add new shader record with given name, into provided holder.
     * @param name Name of the new shader record.
     * @param holder Add it to this holder.
     * @return Returns reference to the newly added record. It is 
     * valid at least until the next manipulation with the holder.
     */
    ShaderRecord &addShaderRecord(const std::wstring &name, 
        std::vector<ShaderRecord> &holder) const;

    /**
     * Calculate padding for a root argument.
     * @param sizeOfArgument Size of the argument currently being 
     * added.
     * @param currentOffset Current size in bytes of the other root 
     * arguments already on the pile.
     * @return Returns padding required to be put before the new 
     * argument can be put onto the pile.
     */
    std::size_t calculateArgumentPrePadding(std::size_t sizeOfArgument, 
        std::size_t currentOffset) const;

    /**
     * Add all of the root arguments into the record.
     * @param record Add to this record.
     * @param firstArg Start of the arguments.
     * @param restArgs The other arguments.
     */
    template <typename FirstT, typename... RestTs>
    void addRootArguments(ShaderRecord &record, FirstT firstArg, RestTs... restArgs) const;

    /**
     * Add all of the root arguments into the record.
     * This is the end case for the template recursion.
     * @param record Add to this record.
     * @param lastArg Last of the arguments.
     */
    template <typename LastT>
    void addRootArguments(ShaderRecord &record, LastT lastArg) const;

    /// The end case.
    inline void addRootArguments(ShaderRecord &record) const;

    /**
     * Add shader group to given holder.
     * @param name Name of the hit group shaders.
     * @param holder Target holder to which the the record 
     * belongs to.
     * @param rootSignatureArguments list of root parameters.
     * @return Returns index of the group in the final order.
     */
    template <typename... RSArgs>
    std::size_t addGroup(const std::wstring &name, std::vector<ShaderRecord> &holder, 
        RSArgs... rootSignatureArguments);

    /**
     * Calculate total size of given record, when copied into the 
     * shader table. Includes correct alignment!
     * @param record Record to calculate the size of.
     * @param nameSize Size of the name handle.
     * @return Returns size of the record, including its name, root 
     * arguments and the alignment.
     */
    std::size_t calculateRecordSize(const ShaderRecord &record, 
        ::UINT64 nameSize) const;

    /// Get size of a name identifier for given device
    ::UINT64 getNameSize(res::d3d12::D3D12RayTracingDevice &device) const;

    /**
     * Calculate the largest record size of the records in provided 
     * holder.
     * @param records Holder containing the records.
     * @param nameSize Size of a single name identifier.
     * @return Returns the aggregate record size, which can be used 
     * by the whole holder - the maximum.
     */
    ::UINT64 calculateMaxRecordSize(const std::vector<ShaderRecord> &records, 
        ::UINT64 nameSize) const;

    /**
     * Recalculate nameSize and record sizes, if necessary.
     * @param device Device used for ray tracing purposes.
     */
    void recalculateSizes(res::d3d12::D3D12RayTracingDevice &device);

    /**
     * Create a storage which can be used for storing the shader table, before 
     * sending it over to the GPU.
     */
    std::vector<uint8_t> createStagingStorage(::UINT64 sizeInBytes) const;

    /**
     * Copy records from given holder into destination memory.
     * @param holder Holder containing the shader records.
     * @param dst Destination memory.
     * @param pipeline Ray tracing pipeline used for name translation.
     * @param recordSize Unified size for the records.
     * @param nameSize Size of a single name identifier.
     * @return Returns number of bytes written.
     * @throws UnknownNameException Thrown when a group name could not 
     * be found in provided ray tracing pipeline.
     */
    std::size_t copyRecords(const std::vector<ShaderRecord> &holder, uint8_t *dst, 
        res::d3d12::D3D12RayTracingStateObject &pipeline, ::UINT64 recordSize, 
        ::UINT64 nameSize);

    /// List of added ray generation groups.
    std::vector<ShaderRecord> mRayGenerationGroups{ };
    /// List of added miss groups.
    std::vector<ShaderRecord> mMissGroups{ };
    /// List of added hit groups.
    std::vector<ShaderRecord> mHitGroups{ };

    /// Dirty flag used for getting shader group sizes.
    bool mDirty{ true };
    /// Size of the name identifier. May be invalid if mDirty.
    ::UINT64 mNameSize{ 0u };
    /// Size of a single ray generation record. May be invalid if mDirty.
    ::UINT64 mRayGenerationRecordSize{ 0u };
    /// Size of a single miss record. May be invalid if mDirty.
    ::UINT64 mMissRecordSize{ 0u };
    /// Size of a single hit record. May be invalid if mDirty.
    ::UINT64 mHitRecordSize{ 0u };
protected:
}; // class D3D12RTShaderTable

} // namespace d3d12

} // namespace helpers

} // namespace quark

// Template implementation

namespace quark
{

namespace helpers
{

namespace d3d12
{

template <typename ... RSArgs>
std::size_t D3D12RTShaderTable::addRayGenerationGroup(const std::wstring &name, RSArgs... rootSignatureArguments)
{ return addGroup(name, mRayGenerationGroups, std::forward<RSArgs>(rootSignatureArguments)...); }

template <typename ... RSArgs>
std::size_t D3D12RTShaderTable::addMissGroup(const std::wstring &name, RSArgs... rootSignatureArguments)
{ return addGroup(name, mMissGroups, std::forward<RSArgs>(rootSignatureArguments)...); }

template <typename ... RSArgs>
std::size_t D3D12RTShaderTable::addHitGroup(const std::wstring &name, RSArgs... rootSignatureArguments)
{ return addGroup(name, mHitGroups, std::forward<RSArgs>(rootSignatureArguments)...); }

template <typename T>
void D3D12RTShaderTable::ShaderRecord::pushRootArgument(T rootSignatureArgument, 
    std::size_t prePadding)
{
    // First we need to resize the data vector to hold padding + argument.
    const auto currentOffset{ currentArgumentBytes() };
    mRootArguments.resize(currentOffset + prePadding + sizeof(T));

    // Then we can copy the new data onto the pile.
    const auto baseDst{ mRootArguments.data() };
    const auto newOffset{ currentOffset + prePadding };
    std::memcpy(baseDst + newOffset, &rootSignatureArgument, sizeof(T));
}

template <typename FirstT, typename ... RestTs>
void D3D12RTShaderTable::addRootArguments(ShaderRecord &record, 
    FirstT firstArg, RestTs... restArgs) const 
{
    const auto prePadding{ calculateArgumentPrePadding(sizeof(FirstT), record.currentArgumentBytes()) };
    record.pushRootArgument(std::forward<FirstT>(firstArg), prePadding);

    // Continue until no arguments remain.
    addRootArguments(record, std::forward<RestTs>(restArgs)...);
}

template <typename LastT>
void D3D12RTShaderTable::addRootArguments(ShaderRecord &record, LastT lastArg) const
{
    const auto prePadding{ calculateArgumentPrePadding(sizeof(LastT), record.currentArgumentBytes()) };
    record.pushRootArgument(std::forward<LastT>(lastArg), prePadding);

    // No more arguments remain -> we are finished!
}

void D3D12RTShaderTable::addRootArguments(ShaderRecord&) const
{ /* No more arguments remain -> we are finished! */ }

template <typename ... RSArgs>
std::size_t D3D12RTShaderTable::addGroup(const std::wstring &name, std::vector<ShaderRecord> &holder,
    RSArgs... rootSignatureArguments)
{
    const auto groupIndex{ holder.size() };

    auto &record{ addShaderRecord(name, holder) };
    addRootArguments(record, std::forward<RSArgs>(rootSignatureArguments)...);

    mDirty = true;
    
    return groupIndex;
}

}

}

}

// Template implementation end

#endif 
