/**
 * @file helpers/d3d12/D3D12RootSignatureBuilder.h
 * @author Tomas Polasek
 * @brief D3D12 root signature configurator and builder.
 */

#pragma once

#include "engine/resources/d3d12/D3D12RootSignature.h"

/// Namespace containing the engine code.
namespace quark
{

/// Namespace containing helper classes and methods.
namespace helpers
{

/// Direct3D 12 resource management.
namespace d3d12
{

/**
 * D3D12 root signature configurator and builder.
 */
class D3D12RootSignatureBuilder
{
public:
    // Forward declaration.
    class ParameterWrapper;

    /**
     * Exception thrown when trying to access root parameter 
     * by its index, which is either not added already or is 
     * not a new one in order.
     */
    struct IndexOutOfOrderException : public std::exception
    {
        IndexOutOfOrderException(const char *msg):
            std::exception(msg)
        { }
    }; // struct IndexOutOfOrderException

    /**
     * Initialize empty builder.
     */
    D3D12RootSignatureBuilder();

    /// Free any allocated buffers.
    ~D3D12RootSignatureBuilder();

    // Allow copying.
    D3D12RootSignatureBuilder(const D3D12RootSignatureBuilder &other) = default;
    D3D12RootSignatureBuilder &operator=(const D3D12RootSignatureBuilder &other) = default;
    // Moving is allowed.
    D3D12RootSignatureBuilder(D3D12RootSignatureBuilder &&other) = default;
    D3D12RootSignatureBuilder &operator=(D3D12RootSignatureBuilder &&other) = default;

    /**
     * Add given root parameter to the signature.
     * @param parameter New root parameter.
     * @return Returns reference to this object.
     */
    D3D12RootSignatureBuilder &addRootParameter(const ::CD3DX12_ROOT_PARAMETER1 &parameter);
    
    /**
     * Add given root parameter to the signature.
     * @param parameter New root parameter.
     * @return Returns reference to this object.
     */
    D3D12RootSignatureBuilder &operator<<(const ::CD3DX12_ROOT_PARAMETER1 &parameter);

    /**
     * Add new root parameter and return its configuration wrapper.
     * @return Returns reference to the new root parameter, which 
     * allows its configuration.
     * @warning The reference is valid only until another root 
     * parameter is added!
     */
    ParameterWrapper &nextRootParameter();

    /**
     * Access root parameter by its index. 
     * Already added parameters may be accessed and new ones 
     * added - however only the next one in order may be 
     * added by this method.
     * @param index Index of the requested root parameter.
     * @return Returns reference to the root parameter on 
     * requested index.
     * @throws IndexOutOfOrderException Thrown when trying to 
     * access root parameter which is not added yet and its 
     * index is greater than the last one by more than 1.
     * @warning The reference is valid only until another root 
     * parameter is added!
     */
    ParameterWrapper &operator[](std::size_t index);

    /**
     * Access root parameter by its index. 
     * Only already added parameters may be accessed.
     * @param index Index of the requested root parameter.
     * @return Returns reference to the root parameter on 
     * requested index.
     * @throws IndexOutOfOrderException Thrown when trying to 
     * access root parameter which is not added yet.
     * @warning The reference is valid only until another root 
     * parameter is added!
     */
    const ParameterWrapper &operator[](std::size_t index) const;
    
    /**
     * Clear the list of root parameters.
     * @return Returns reference to this object.
     */
    D3D12RootSignatureBuilder &clearRootParameters();

    /**
     * Add given static sampler description to the signature.
     * @param sampler New static sampler.
     * @return Returns reference to this object.
     */
    D3D12RootSignatureBuilder &addStaticSampler(const ::CD3DX12_STATIC_SAMPLER_DESC &sampler);

    /**
     * Add a static sampler to the signature.
     * @return Returns reference to this object.
     */
    D3D12RootSignatureBuilder &addStaticSampler(
        UINT shaderRegister,
        D3D12_FILTER filter = D3D12_FILTER_ANISOTROPIC,
        D3D12_TEXTURE_ADDRESS_MODE addressU = D3D12_TEXTURE_ADDRESS_MODE_WRAP,
        D3D12_TEXTURE_ADDRESS_MODE addressV = D3D12_TEXTURE_ADDRESS_MODE_WRAP,
        D3D12_TEXTURE_ADDRESS_MODE addressW = D3D12_TEXTURE_ADDRESS_MODE_WRAP,
        FLOAT mipLODBias = 0,
        UINT maxAnisotropy = 16,
        D3D12_COMPARISON_FUNC comparisonFunc = D3D12_COMPARISON_FUNC_LESS_EQUAL,
        D3D12_STATIC_BORDER_COLOR borderColor = D3D12_STATIC_BORDER_COLOR_OPAQUE_WHITE,
        FLOAT minLOD = 0.f,
        FLOAT maxLOD = D3D12_FLOAT32_MAX,
        D3D12_SHADER_VISIBILITY shaderVisibility = D3D12_SHADER_VISIBILITY_ALL,
        UINT registerSpace = 0);

    /**
     * Add given static sampler description to the signature.
     * @param sampler New static sampler.
     * @return Returns reference to this object.
     */
    D3D12RootSignatureBuilder &operator<<(const ::CD3DX12_STATIC_SAMPLER_DESC &sampler);
    
    /**
     * Clear the list of static sampler descriptions.
     * @return Returns reference to this object.
     */
    D3D12RootSignatureBuilder &clearStaticSamplers();

    /**
     * Set root signature flags.
     * @param flags Flags of the root signature.
     * @return Returns reference to this object.
     */
    D3D12RootSignatureBuilder &setFlags(::D3D12_ROOT_SIGNATURE_FLAGS flags);

    /**
     * Build the root signature with configuration within this 
     * builder.
     * @param device Device to create the root signature on.
     * @return Returns the root signature object.
     * @throws Throws util::winexception on error.
     */
    res::d3d12::D3D12RootSignature::PtrT build(res::d3d12::D3D12Device &device) const;

#ifdef ENGINE_RAY_TRACING_ENABLED
    /**
     * Build the root signature using configuration within this 
     * builder.
     * This version builds root signature usable in ray tracing.
     * @param device Device to create the root signature on.
     * @return Returns the root signature object.
     * @throws Throws util::winexception on error.
     */
    res::d3d12::D3D12RootSignature::PtrT build(res::d3d12::D3D12RayTracingDevice &device) const;
#endif

    /// Current number of root parameters.
    std::size_t rootParameterCount() const;
    /// Index of the last added root parameter.
    std::size_t lastRootParameterIndex() const;

    /// Current number of static samplers.
    std::size_t staticSamplerCount() const;

    /// Clear all configuration.
    void clear();
    /// Wrapper around a single root parameter.
    class ParameterWrapper : public ::D3D12_ROOT_PARAMETER1
    {
    public:
        /// Empty root parameter.
        ParameterWrapper() = default;
        /// Initialize from root parameter.
        explicit ParameterWrapper(const ::CD3DX12_ROOT_PARAMETER1 &param);

        /**
         * Initialize this root parameter as a descriptor table.
         * @param numDescriptorRanges Number of descriptor ranges used.
         * @param descriptorRanges Pointer to an array of descriptor 
         * ranges with numDescriptorRanges elements. Describes the tables.
         * @param visibility Which stages should be allowed to access 
         * the parameters.
         */
        void asDescriptorTableParameter(
            ::UINT numDescriptorRanges,
            const ::D3D12_DESCRIPTOR_RANGE1 *descriptorRanges,
            ::D3D12_SHADER_VISIBILITY visibility = D3D12_SHADER_VISIBILITY_ALL);
        
        /**
         * Initialize this root parameter as a constant value.
         * @param num32BitValues Number of 4B values passed to the 
         * shader.
         * @param shaderRegister Register used to access the constant 
         * from the shader.
         * @param registerSpace Specification of space used for the 
         * register.
         * @param visibility Which stages should be allowed to access 
         * the constant.
         */
        void asConstantParameter(
            ::UINT num32BitValues, ::UINT shaderRegister, ::UINT registerSpace = 0u,
            ::D3D12_SHADER_VISIBILITY visibility = D3D12_SHADER_VISIBILITY_ALL);

        /**
         * Initialize this root parameter as a constant buffer view.
         * @param shaderRegister Register used to access the constant 
         * from the shader.
         * @param registerSpace Specification of space used for the 
         * register.
         * @param flags Flags specifying what should be done with the 
         * buffer before executing.
         * @param visibility Which stages should be allowed to access 
         * the constant.
         */
        void asConstantBufferViewParameter(
            ::UINT shaderRegister, ::UINT registerSpace = 0u,
            ::D3D12_ROOT_DESCRIPTOR_FLAGS flags = D3D12_ROOT_DESCRIPTOR_FLAG_NONE,
            ::D3D12_SHADER_VISIBILITY visibility = D3D12_SHADER_VISIBILITY_ALL);

        /**
         * Initialize this root parameter as a resource view.
         * @param shaderRegister Register used to access the resource 
         * from the shader.
         * @param registerSpace Specification of space used for the 
         * register.
         * @param flags Flags specifying what should be done with the 
         * resource view before executing.
         * @param visibility Which stages should be allowed to access 
         * the constant.
         */
        void asShaderResourceViewParameter(
            ::UINT shaderRegister, ::UINT registerSpace = 0u,
            ::D3D12_ROOT_DESCRIPTOR_FLAGS flags = D3D12_ROOT_DESCRIPTOR_FLAG_NONE,
            ::D3D12_SHADER_VISIBILITY visibility = D3D12_SHADER_VISIBILITY_ALL);

        /**
         * Initialize this root parameter as an unordered access view.
         * @param shaderRegister Register used to access the resource 
         * from the shader.
         * @param registerSpace Specification of space used for the 
         * register.
         * @param flags Flags specifying what should be done with the 
         * resource before executing.
         * @param visibility Which stages should be allowed to access 
         * the constant.
         */
        void asUnorderedAccessViewParameter(
            ::UINT shaderRegister, ::UINT registerSpace = 0u,
            ::D3D12_ROOT_DESCRIPTOR_FLAGS flags = D3D12_ROOT_DESCRIPTOR_FLAG_NONE,
            ::D3D12_SHADER_VISIBILITY visibility = D3D12_SHADER_VISIBILITY_ALL);
    private:
    protected:
    }; // class ParameterWrapper
private:
    /// List of root parameters.
    std::vector<ParameterWrapper> mRootParameters;
    /// List of static samplers.
    std::vector<::CD3DX12_STATIC_SAMPLER_DESC> mStaticSamplers;

    /// Flags passed to the root signature.
    ::D3D12_ROOT_SIGNATURE_FLAGS mFlags{ };
protected:
}; // class D3D12RootSignatureBuilder

} // namespace d3d12

} // namespace helpers

} // namespace quark

// Template implementation

namespace quark
{
namespace helpers
{
namespace d3d12
{

}
}
}

// Template implementation end
