/**
 * @file helpers/d3d12/D3D12Helpers.h
 * @author Tomas Polasek
 * @brief Helper functions for Direct3D 12.
 */

#pragma once

#include "engine/util/SafeD3D12.h"

/// Namespace containing the engine code.
namespace quark
{

/// Namespace containing helper classes and methods.
namespace helpers
{

/// Direct3D 12 helpers.
namespace d3d12
{

/// Helper functions for Direct3D 12.
class D3D12Helpers
{
public:
    // Only static functions should be here.
    D3D12Helpers() = delete;

    /**
     * Create new waiting event for use with fences.
     * @return Returns handle to the created event.
     * @throws Throws util::winexception on error, which 
     * contains error message specifying what happened.
     */
    static HANDLE createFenceEvent();

    /**
     * Convert command list type to string, usable for debugging.
     * @param type Type of the command list.
     */
    static constexpr const char *cmdListTypeToStr(::D3D12_COMMAND_LIST_TYPE type);

    /**
     * Get size of an element in bytes, specified by its DXGI_FORMAT.
     * Original author: Mike.Popoloski
     * @param format Format of the element.
     * @return Returns size of a single element in bytes or 0 if the 
     * format is unknown.
     */
    static constexpr std::size_t sizeOfFormatElement(::DXGI_FORMAT format);

    /// Is provided format compressed?
    static constexpr bool isFormatCompressed(::DXGI_FORMAT format);

    /**
     * Calculate mip-map size for given mip-map level.
     * Should be called for width and height separately.
     * @param size Width, height or depth of the texture.
     * @param level Level of the mip-map, starting at zero 
     * for the original.
     * @return Returns size of the mip-map. Minimal returned 
     * value is one.
     */
    static constexpr std::size_t mipMapSize(std::size_t size, std::size_t level);

    /**
     * Calculate real number of elements in a given dimension, for 
     * specified format.
     * For example, compressed formats aggregate 4x4 pixels into one 
     * element -> size / 4.
     * @param format Format of an element.
     * @param size Full width, height or depth of the resource, in pixels.
     * @return Returns real width/height/depth in elements.
     */
    static constexpr std::size_t elementCount(::DXGI_FORMAT format, std::size_t size);

    /// Size in elements of a single compressed texture block.
    static constexpr std::size_t COMPRESSED_BLOCK_SIZE{ 4u };

    /**
     * Calculate number of bytes per row, for a given 2D texture resource.
     * @param format Format of a single element in the texture.
     * @param width Width of the texture in pixels.
     * @param level Mip-map level of the texture.
     * @return Returns number of bytes taken up by a single row.
     */
    static constexpr std::size_t rowPitch2D(::DXGI_FORMAT format, std::size_t width, std::size_t level);

    /**
     * Calculate number of rows, for given 2D texture resource.
     * Should be used in concert with rowPitch2D to find the total size.
     * @param format Format of a single element in the texture.
     * @param width Width of the texture in pixels.
     * @param height Height of the texture in pixels.
     * @param level Mip-map level of the texture.
     * @return Returns number of rows in the texture.
     */
    static constexpr std::size_t rowCount2D(::DXGI_FORMAT format, std::size_t height, std::size_t level);
private:
protected:
}; // class D3D12Helpers

} // namespace d3d12

} // namespace helpers

} // namespace quark

// template implementation

namespace quark
{
namespace helpers
{
namespace d3d12
{
constexpr const char *D3D12Helpers::cmdListTypeToStr(::D3D12_COMMAND_LIST_TYPE type)
{
    switch (type)
    {
        case ::D3D12_COMMAND_LIST_TYPE_DIRECT:
            return "Direct";
        case ::D3D12_COMMAND_LIST_TYPE_BUNDLE:
            return "Bundle";
        case ::D3D12_COMMAND_LIST_TYPE_COMPUTE:
            return "Compute";
        case ::D3D12_COMMAND_LIST_TYPE_COPY:
            return "Copy";
        case ::D3D12_COMMAND_LIST_TYPE_VIDEO_DECODE:
            return "VideoDecode";
        case ::D3D12_COMMAND_LIST_TYPE_VIDEO_PROCESS:
            return "VideoProcess";
        default:
            return "Unknown";
    }
}

constexpr std::size_t D3D12Helpers::sizeOfFormatElement(::DXGI_FORMAT format)
{
    switch (format)
    {
        case DXGI_FORMAT_R32G32B32A32_TYPELESS:
        case DXGI_FORMAT_R32G32B32A32_FLOAT:
        case DXGI_FORMAT_R32G32B32A32_UINT:
        case DXGI_FORMAT_R32G32B32A32_SINT:
            return 16u;

        case DXGI_FORMAT_R32G32B32_TYPELESS:
        case DXGI_FORMAT_R32G32B32_FLOAT:
        case DXGI_FORMAT_R32G32B32_UINT:
        case DXGI_FORMAT_R32G32B32_SINT:
            return 12u;

        case DXGI_FORMAT_R16G16B16A16_TYPELESS:
        case DXGI_FORMAT_R16G16B16A16_FLOAT:
        case DXGI_FORMAT_R16G16B16A16_UNORM:
        case DXGI_FORMAT_R16G16B16A16_UINT:
        case DXGI_FORMAT_R16G16B16A16_SNORM:
        case DXGI_FORMAT_R16G16B16A16_SINT:
        case DXGI_FORMAT_R32G32_TYPELESS:
        case DXGI_FORMAT_R32G32_FLOAT:
        case DXGI_FORMAT_R32G32_UINT:
        case DXGI_FORMAT_R32G32_SINT:
        case DXGI_FORMAT_R32G8X24_TYPELESS:
        case DXGI_FORMAT_D32_FLOAT_S8X24_UINT:
        case DXGI_FORMAT_R32_FLOAT_X8X24_TYPELESS:
        case DXGI_FORMAT_X32_TYPELESS_G8X24_UINT:
            return 8u;

        case DXGI_FORMAT_R10G10B10A2_TYPELESS:
        case DXGI_FORMAT_R10G10B10A2_UNORM:
        case DXGI_FORMAT_R10G10B10A2_UINT:
        case DXGI_FORMAT_R11G11B10_FLOAT:
        case DXGI_FORMAT_R8G8B8A8_TYPELESS:
        case DXGI_FORMAT_R8G8B8A8_UNORM:
        case DXGI_FORMAT_R8G8B8A8_UNORM_SRGB:
        case DXGI_FORMAT_R8G8B8A8_UINT:
        case DXGI_FORMAT_R8G8B8A8_SNORM:
        case DXGI_FORMAT_R8G8B8A8_SINT:
        case DXGI_FORMAT_R16G16_TYPELESS:
        case DXGI_FORMAT_R16G16_FLOAT:
        case DXGI_FORMAT_R16G16_UNORM:
        case DXGI_FORMAT_R16G16_UINT:
        case DXGI_FORMAT_R16G16_SNORM:
        case DXGI_FORMAT_R16G16_SINT:
        case DXGI_FORMAT_R32_TYPELESS:
        case DXGI_FORMAT_D32_FLOAT:
        case DXGI_FORMAT_R32_FLOAT:
        case DXGI_FORMAT_R32_UINT:
        case DXGI_FORMAT_R32_SINT:
        case DXGI_FORMAT_R24G8_TYPELESS:
        case DXGI_FORMAT_D24_UNORM_S8_UINT:
        case DXGI_FORMAT_R24_UNORM_X8_TYPELESS:
        case DXGI_FORMAT_X24_TYPELESS_G8_UINT:
        case DXGI_FORMAT_B8G8R8A8_UNORM:
        case DXGI_FORMAT_B8G8R8X8_UNORM:
            return 4u;

        case DXGI_FORMAT_R8G8_TYPELESS:
        case DXGI_FORMAT_R8G8_UNORM:
        case DXGI_FORMAT_R8G8_UINT:
        case DXGI_FORMAT_R8G8_SNORM:
        case DXGI_FORMAT_R8G8_SINT:
        case DXGI_FORMAT_R16_TYPELESS:
        case DXGI_FORMAT_R16_FLOAT:
        case DXGI_FORMAT_D16_UNORM:
        case DXGI_FORMAT_R16_UNORM:
        case DXGI_FORMAT_R16_UINT:
        case DXGI_FORMAT_R16_SNORM:
        case DXGI_FORMAT_R16_SINT:
        case DXGI_FORMAT_B5G6R5_UNORM:
        case DXGI_FORMAT_B5G5R5A1_UNORM:
            return 2u;

        case DXGI_FORMAT_R8_TYPELESS:
        case DXGI_FORMAT_R8_UNORM:
        case DXGI_FORMAT_R8_UINT:
        case DXGI_FORMAT_R8_SNORM:
        case DXGI_FORMAT_R8_SINT:
        case DXGI_FORMAT_A8_UNORM:
            return 1u;

            // Compressed format; http://msdn2.microsoft.com/en-us/library/bb694531(VS.85).aspx
        case DXGI_FORMAT_BC2_TYPELESS:
        case DXGI_FORMAT_BC2_UNORM:
        case DXGI_FORMAT_BC2_UNORM_SRGB:
        case DXGI_FORMAT_BC3_TYPELESS:
        case DXGI_FORMAT_BC3_UNORM:
        case DXGI_FORMAT_BC3_UNORM_SRGB:
        case DXGI_FORMAT_BC5_TYPELESS:
        case DXGI_FORMAT_BC5_UNORM:
        case DXGI_FORMAT_BC5_SNORM:
            return 16u;

            // Compressed format; http://msdn2.microsoft.com/en-us/library/bb694531(VS.85).aspx
        case DXGI_FORMAT_R1_UNORM:
        case DXGI_FORMAT_BC1_TYPELESS:
        case DXGI_FORMAT_BC1_UNORM:
        case DXGI_FORMAT_BC1_UNORM_SRGB:
        case DXGI_FORMAT_BC4_TYPELESS:
        case DXGI_FORMAT_BC4_UNORM:
        case DXGI_FORMAT_BC4_SNORM:
            return 8u;

            // Compressed format; http://msdn2.microsoft.com/en-us/library/bb694531(VS.85).aspx
        case DXGI_FORMAT_R9G9B9E5_SHAREDEXP:
            return 4u;

            // These are compressed, but bit-size information is unclear.
        case DXGI_FORMAT_R8G8_B8G8_UNORM:
        case DXGI_FORMAT_G8R8_G8B8_UNORM:
            return 4u;

        case DXGI_FORMAT_UNKNOWN:
        default:
            return 1u;
    }
}

constexpr bool D3D12Helpers::isFormatCompressed(::DXGI_FORMAT format)
{
    switch (format)
    {
        case DXGI_FORMAT_BC1_TYPELESS:
        case DXGI_FORMAT_BC1_UNORM:
        case DXGI_FORMAT_BC1_UNORM_SRGB:
        case DXGI_FORMAT_BC2_TYPELESS:
        case DXGI_FORMAT_BC2_UNORM:
        case DXGI_FORMAT_BC2_UNORM_SRGB:
        case DXGI_FORMAT_BC3_TYPELESS:
        case DXGI_FORMAT_BC3_UNORM:
        case DXGI_FORMAT_BC3_UNORM_SRGB:
        case DXGI_FORMAT_BC4_TYPELESS:
        case DXGI_FORMAT_BC4_UNORM:
        case DXGI_FORMAT_BC4_SNORM:
        case DXGI_FORMAT_BC5_TYPELESS:
        case DXGI_FORMAT_BC5_UNORM:
        case DXGI_FORMAT_BC5_SNORM:
        case DXGI_FORMAT_BC6H_TYPELESS:
        case DXGI_FORMAT_BC6H_UF16:
        case DXGI_FORMAT_BC6H_SF16:
        case DXGI_FORMAT_BC7_TYPELESS:
        case DXGI_FORMAT_BC7_UNORM:
        case DXGI_FORMAT_BC7_UNORM_SRGB:
        case DXGI_FORMAT_R1_UNORM:
            return true;
        default:
            return false;
    }
}

inline constexpr std::size_t D3D12Helpers::mipMapSize(
    std::size_t size, std::size_t level)
{
    const auto result{ size >> level };
    return result ? result : 1u;
}

inline constexpr std::size_t D3D12Helpers::elementCount(::DXGI_FORMAT format, std::size_t size)
{ 
    return isFormatCompressed(format) ? 
        // Compressed -> divide into blocks, returning 1 block for small inputs.
        (size / COMPRESSED_BLOCK_SIZE + ((size % COMPRESSED_BLOCK_SIZE) ? 1u : 0u)) : 
        // Non-compressed -> return input.
        size; 
}

inline constexpr std::size_t D3D12Helpers::rowPitch2D(
    ::DXGI_FORMAT format, std::size_t width, std::size_t level)
{
    const auto elementWidth{ elementCount(format, mipMapSize(width, level)) };
    const auto elementByteSize{ sizeOfFormatElement(format) };

    //return util::math::alignTo(elementWidth * elementByteSize, D3D12_TEXTURE_DATA_PITCH_ALIGNMENT);
    return elementWidth * elementByteSize;
}

inline constexpr std::size_t D3D12Helpers::rowCount2D(
    ::DXGI_FORMAT format, std::size_t height, std::size_t level)
{
    const auto elementHeight{ elementCount(format, mipMapSize(height, level)) };
    return elementHeight;
}

}
}
}

namespace std
{

// Adapters for standard hashing.

template<>
struct hash<::CD3DX12_STATIC_SAMPLER_DESC>
{
    std::size_t operator()(const ::CD3DX12_STATIC_SAMPLER_DESC &d) const noexcept
    { 
        // TODO - Take only some members?
        return util::hashAll(
            d.Filter, d.AddressU, d.AddressV, d.AddressW, 
            d.MipLODBias, d.MaxAnisotropy, d.ComparisonFunc, 
            d.BorderColor, d.MinLOD, d.MaxLOD, d.ShaderRegister, 
            d.RegisterSpace, d.ShaderVisibility); 
    }
}; // struct hash

template<>
struct hash<::CD3DX12_RESOURCE_DESC>
{
    std::size_t operator()(const ::CD3DX12_RESOURCE_DESC &d) const noexcept
    { 
        // TODO - Take only some members?
        return util::hashAll(
            d.Dimension, d.Alignment, d.Width, d.Height, 
            d.DepthOrArraySize, d.MipLevels, d.Format, 
            d.SampleDesc.Count, d.SampleDesc.Quality, 
            d.Layout, d.Flags);
    }
}; // struct hash

template<>
struct hash<::D3D12_SHADER_RESOURCE_VIEW_DESC>
{
    std::size_t operator()(const ::D3D12_SHADER_RESOURCE_VIEW_DESC &d) const noexcept
    { 
        // TODO - Take only some members?
        return util::hashAll(
            d.Format, d.ViewDimension, d.Shader4ComponentMapping, 
            d.TextureCube.MostDetailedMip);
    }
}; // struct hash

template<>
struct hash<void*>
{
    std::size_t operator()(const void* &p) const noexcept
    { 
        return reinterpret_cast<uint64_t>(p);
    }
}; // struct hash

template<>
struct hash<::DXGI_FORMAT>
{
    std::size_t operator()(const ::DXGI_FORMAT &f) const noexcept
    { 
        return static_cast<uint16_t>(f);
    }
}; // struct hash

}

// template implementation end
