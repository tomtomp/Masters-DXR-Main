/**
 * @file helpers/d3d12/D3D12UploadBuffer.h
 * @author Tomas Polasek
 * @brief Wrapper around D3D12Resource, providing simple 
 * uploading and mapping of contained resource.
 */

#pragma once

#include "engine/helpers/d3d12/D3D12ResourceUpdater.h"
#include "engine/resources/d3d12/D3D12Resource.h"

/// Namespace containing the engine code.
namespace quark
{

/// Namespace containing helper classes and methods.
namespace helpers
{

/// Direct3D 12 resource management.
namespace d3d12
{

/**
 * Wrapper around D3D12Resource, providing simple 
 * uploading and mapping of contained resource.
 */
class D3D12UploadBuffer : public res::d3d12::D3D12Resource, public util::InheritPointerType<res::d3d12::D3D12Resource, D3D12UploadBuffer>
{
public:
    /**
     * Exception thrown when the buffer is too 
     * small to contain provided data.
     */
    struct BufferTooSmallException : public std::exception
    {
        BufferTooSmallException(const char *msg) :
            std::exception(msg)
        { }
    }; // struct BufferTooSmallException

    /**
     * Exception thrown when attempting to map 
     * already mapped buffer with incompatible 
     * mapping type.
     */
    struct BufferMapIncompatibleException : public std::exception
    {
        BufferMapIncompatibleException(const char *msg) :
            std::exception(msg)
        { }
    }; // struct BufferMapIncompatibleException

    using PtrT = util::PointerType<D3D12UploadBuffer>::PtrT;
    using ConstPtrT = util::PointerType<D3D12UploadBuffer>::ConstPtrT;

    /// Create unallocated buffer.
    D3D12UploadBuffer();

    /// Create unallocated buffer.
    static PtrT create();

    /**
     * Create upload buffer wrapper around given buffer.
     * @param buffer Buffer resource to wrap around.
     */
    static PtrT create(res::d3d12::D3D12Resource &buffer);

    /**
     * Initialize the with provided data.
     * Upload buffer will be just large enough to contain 
     * provided data.
     * @tparam AllocPtrT Type of the allocator pointer.
     * @tparam ItT Type of the iterator, which must be of 
     * random-access iterator type.
     * @param begin Begin iterator for the data.
     * @param end End iterator for the data.
     * @param desc Description of the buffer. Width can be set 
     * to 0, for automatic size detection.
     * @param cmdList Command list used to record the copy 
     * command.
     * @param allocator Allocator used to allocate the buffer.
     * @param updater This resource updater will be used to 
     * copy data over to the GPU side.
     * @param initResourceState Initial state of the resource.
     * @param name Optional name of the buffer.
     * @throws util::winexception Thrown on error.
     */
    template <typename AllocPtrT, typename ItT>
    static PtrT create(const ItT &begin, const ItT &end,
        const ::CD3DX12_RESOURCE_DESC &desc, 
        res::d3d12::D3D12CommandList &cmdList,
        AllocPtrT allocator, D3D12ResourceUpdater &updater, 
        ::D3D12_RESOURCE_STATES initResourceState = D3D12_RESOURCE_STATE_GENERIC_READ, 
        const std::wstring &name = L"UploadBuffer");

    /**
     * Initialize the with provided data. This version 
     * uploads the data immediately, through using Map/Unmap.
     * Upload buffer will be just large enough to contain 
     * provided data.
     * @tparam AllocPtrT Type of the allocator pointer.
     * @tparam ItT Type of the iterator, which must be of 
     * random-access iterator type.
     * @param begin Begin iterator for the data.
     * @param end End iterator for the data.
     * @param desc Description of the buffer. Width can be set 
     * to 0, for automatic size detection.
     * @param allocator Allocator used to allocate the buffer.
     * @param initResourceState Initial state of the resource.
     * @param name Optional name of the buffer.
     * @throws util::winexception Thrown on error.
     */
    template <typename AllocPtrT, typename ItT>
    static PtrT create(const ItT &begin, const ItT &end,
        const ::CD3DX12_RESOURCE_DESC &desc, AllocPtrT allocator, 
        ::D3D12_RESOURCE_STATES initResourceState = D3D12_RESOURCE_STATE_GENERIC_READ, 
        const std::wstring &name = L"UploadBuffer");

    /// Unmap and free resource.
    ~D3D12UploadBuffer();

    /// No copying, allow moving.
    D3D12UploadBuffer(const D3D12UploadBuffer &other) = delete;
    D3D12UploadBuffer &operator=(const D3D12UploadBuffer &other) = delete;
    D3D12UploadBuffer(D3D12UploadBuffer &&other) = default;
    D3D12UploadBuffer &operator=(D3D12UploadBuffer &&other) = default;

    /**
     * Allocate the buffer, so it can contain at least 
     * a given number of bytes.
     * @param device Target device where the buffer should 
     * be allocated.
     * @param sizeInBytes Minimal size of the buffer in 
     * bytes.
     * @param initResourceState Initial state of the resource.
     * @param name Optional name of the buffer.
     * @throw util::winexception Thrown on error.
     */
    void allocate(res::d3d12::D3D12Device &device, 
        std::size_t sizeInBytes, 
        ::D3D12_RESOURCE_STATES initResourceState = D3D12_RESOURCE_STATE_GENERIC_READ, 
        const std::wstring &name = L"UploadBuffer");

    /**
     * Allocate the buffer, so it can contain at least 
     * a given number of bytes.
     * @tparam AllocPtrT Type of the allocator pointer.
     * @param allocator Allocator which allows the allocation 
     * of at least sizeInBytes bytes.
     * @param sizeInBytes Minimal size of the buffer in 
     * bytes.
     * @param initResourceState Initial state of the resource.
     * @param name Optional name of the buffer.
     * @throw util::winexception Thrown on error.
     */
    template <typename AllocPtrT>
    void allocate(AllocPtrT allocator, std::size_t sizeInBytes, 
        ::D3D12_RESOURCE_STATES initResourceState = D3D12_RESOURCE_STATE_GENERIC_READ, 
        const std::wstring &name = L"UploadBuffer");

    /**
     * Allocate the buffer, so it can contain at least 
     * a given number of bytes.
     * @tparam AllocPtrT Type of the allocator pointer.
     * @param allocator Allocator which allows the allocation 
     * of at least sizeInBytes bytes.
     * @param desc Description of the buffer.
     * @param initResourceState Initial state of the resource.
     * @param name Optional name of the buffer.
     * @throw util::winexception Thrown on error.
     */
    template <typename AllocPtrT>
    void allocate(AllocPtrT allocator, const ::CD3DX12_RESOURCE_DESC &desc, 
        ::D3D12_RESOURCE_STATES initResourceState = D3D12_RESOURCE_STATE_GENERIC_READ, 
        const std::wstring &name = L"UploadBuffer");

    /**
     * Allocate the buffer large enough to hold 
     * provided data and upload them to the GPU.
     * @tparam AllocPtrT Type of the allocator pointer.
     * @tparam ItT Type of the iterator, which must be of 
     * random-access iterator type.
     * @param begin Begin iterator for the data.
     * @param end End iterator for the data.
     * @param desc Description of the buffer. Width can be set 
     * to 0, for automatic size detection.
     * @param cmdList Command list used to record the copy 
     * command.
     * @param allocator Allocator used to allocate the buffer.
     * @param updater This resource updater will be used to 
     * copy data over to the GPU side.
     * @param initResourceState Initial state of the resource.
     * @param name Optional name of the buffer.
     * @throws util::winexception Thrown on error.
     */
    template <typename AllocPtrT, typename ItT>
    void allocateUpload(const ItT &begin, const ItT &end,
        const ::CD3DX12_RESOURCE_DESC &desc, 
        res::d3d12::D3D12CommandList &cmdList,
        AllocPtrT allocator, D3D12ResourceUpdater &updater, 
        ::D3D12_RESOURCE_STATES initResourceState = D3D12_RESOURCE_STATE_GENERIC_READ, 
        const std::wstring &name = L"UploadBuffer");

    /**
     * Initialize the with provided data. This version uploads the 
     * data immediately, through using Map/Unmap.
     * Upload buffer will be just large enough to contain provided 
     * data.
     * @tparam AllocPtrT Type of the allocator pointer.
     * @tparam ItT Type of the iterator, which must be of 
     * random-access iterator type.
     * @param begin Begin iterator for the data.
     * @param end End iterator for the data.
     * @param desc Description of the buffer. Width can be set 
     * to 0, for automatic size detection.
     * @param allocator Allocator used to allocate the buffer.
     * @param initResourceState Initial state of the resource.
     * @param name Optional name of the buffer.
     * @throws util::winexception Thrown on error.
     */
    template <typename AllocPtrT, typename ItT>
    void allocateUploadImmediate(const ItT &begin, const ItT &end,
        const ::CD3DX12_RESOURCE_DESC &desc, AllocPtrT allocator, 
        ::D3D12_RESOURCE_STATES initResourceState = D3D12_RESOURCE_STATE_GENERIC_READ, 
        const std::wstring &name = L"UploadBuffer");

    /**
     * Upload data to the GPU.
     * @tparam ItT Type of the iterator, which must be of 
     * random-access iterator type.
     * @param begin Begin iterator for the data.
     * @param end End iterator for the data.
     * @param cmdList Command list used to record the copy 
     * command.
     * @param updater This resource updater will be used to 
     * copy data over to the GPU side.
     * @throws util::winexception Thrown on error. 
     * @throws BufferTooSmallException if the allocated buffer 
     * if too small to contain provided data.
     */
    template <typename ItT>
    typename std::enable_if<
        std::is_same_v<
            typename std::iterator_traits<ItT>::iterator_category,
            std::random_access_iterator_tag>,
        void>::type
    uploadData(const ItT &begin, const ItT &end, 
        res::d3d12::D3D12CommandList &cmdList, 
        D3D12ResourceUpdater &updater);

    /**
     * Upload data to the GPU.
     * If the data pointer is nullptr, or sizeInBytes is 0, then 
     * no action will be taken and buffer will not be unmapped.
     * @param data Data to copy to the buffer.
     * @param sizeInBytes Size of the copied data. This value 
     * must be provided in number of bytes!
     * @param cmdList Command list used to record the copy 
     * command.
     * @param updater This resource updater will be used to 
     * copy data over to the GPU side.
     * @throws util::winexception Thrown on error. 
     * @throws BufferTooSmallException if the allocated buffer 
     * if too small to contain provided data.
     */
    void uploadData(const void *data, std::size_t sizeInBytes,  
        res::d3d12::D3D12CommandList &cmdList, D3D12ResourceUpdater &updater);

    /**
     * Upload data to the GPU.
     * If the data pointer is nullptr, or sizeInBytes is 0, then 
     * no action will be taken and buffer will not be unmapped.
     * This version is performed immediately.
     * @param data Data to copy to the buffer.
     * @param sizeInBytes Size of the copied data. This value 
     * must be provided in number of bytes!
     * @throws util::winexception Thrown on error. 
     * @throws BufferTooSmallException Thrown if the allocated 
     * buffer if too small to contain provided data.
     * @warning Rewrites and unmaps the whole texture buffer!
     */
    void uploadDataImmediate(const void *data, std::size_t sizeInBytes);

    /**
     * Map the buffer to CPU memory, allowing the caller to 
     * write into the memory.
     * @return Returns pointer to writable memory, which will 
     * be mirrored into GPU memory.
     * @throw util::winexception Thrown on error.
     */
    uint8_t *mapForCpuWrite();

    /**
     * Map the buffer to CPU memory, allowing the caller to 
     * write and read from the memory.
     * @return Returns pointer to writable memory, which will 
     * be mirrored into GPU memory.
     * @throw BufferMapIncompatibleException Thrown when trying 
     * to map already mapped buffer, which is mapped in 'Write' 
     * only mode.
     * @throw util::winexception Thrown on error.
     */
    uint8_t *mapForCpuReadWrite();

    /**
     * Unmap a mapped upload buffer from the CPU memory.
     * @param writtenRange Which part of the memory has been 
     * changed and should thus be written back to the GPU. 
     * Leave <0, 0> for no write-back.
     */
    void unmap(const ::CD3DX12_RANGE &writtenRange = { 0u, 0u });

    /**
     * Unmap a mapped upload buffer from the CPU memory.
     * This version marks the whole memory as rewritten and 
     * requests write-back to the GPU memory.
     */
    void unmapFullRewrite();
protected:
    /// Possible types of buffer mapping.
    enum class MappingType
    {
        None,
        Write,
        ReadWrite
    }; // enum class MappingType

    /**
     * Check, whether the buffer is already mapped in provided 
     * mode. Also returns true when mapped in compatible way.
     * @param type Check for this mapping type.
     * @return Returns true if the buffer is mapped in compatible 
     * way with provided type.
     * @throw BufferMapIncompatibleException Thrown when buffer is 
     * mapped in 'Write' mode and checking for 'ReadWrite' mode. 
     */
    bool checkMapped(MappingType type);

    /**
     * Allocate the buffer, so it can contain at least 
     * a given number of bytes, as given in the description.
     * @tparam AllocPtrT Type of the allocator pointer.
     * @param allocator Allocator which allows the allocation 
     * of at least the number of bytes for given description.
     * @param desc Description of the buffer.
     * @param initResourceState Initial state of the resource.
     * @param clearValue Optimized clear value for the buffer.
     * @param name Optional name of the buffer.
     * @throw util::winexception Thrown on error.
     */
    template <typename AllocPtrT>
    void allocate(AllocPtrT allocator, const ::CD3DX12_RESOURCE_DESC &desc, 
        const ::D3D12_RESOURCE_STATES &initResourceState, 
        const ::D3D12_CLEAR_VALUE *clearValue, const std::wstring &name);
private:
    /// Currently used mapping address. When not mapped, should be nullptr.
    uint8_t *mMappedAddress{ nullptr };
    /// Contains type of current memory mapping.
    MappingType mMappedType{ MappingType::None };
protected:
    /**
     * Create upload buffer wrapper around given buffer.
     * @param buffer Buffer resource to wrap around.
     */
    D3D12UploadBuffer(res::d3d12::D3D12Resource &buffer);

    /**
     * Initialize the with provided data.
     * Upload buffer will be just large enough to contain 
     * provided data.
     * @tparam AllocPtrT Type of the allocator pointer.
     * @tparam ItT Type of the iterator, which must be of 
     * random-access iterator type.
     * @param begin Begin iterator for the data.
     * @param end End iterator for the data.
     * @param desc Description of the buffer. Width can be set 
     * to 0, for automatic size detection.
     * @param cmdList Command list used to record the copy 
     * command.
     * @param allocator Allocator used to allocate the buffer.
     * @param updater This resource updater will be used to 
     * copy data over to the GPU side.
     * @param initResourceState Initial state of the resource.
     * @param name Optional name of the buffer.
     * @throws util::winexception Thrown on error.
     */
    template <typename AllocPtrT, typename ItT>
    D3D12UploadBuffer(const ItT &begin, const ItT &end,
        const ::CD3DX12_RESOURCE_DESC &desc, 
        res::d3d12::D3D12CommandList &cmdList,
        AllocPtrT allocator, D3D12ResourceUpdater &updater, 
        ::D3D12_RESOURCE_STATES initResourceState = D3D12_RESOURCE_STATE_GENERIC_READ, 
        const std::wstring &name = L"UploadBuffer");

    /**
     * Initialize the with provided data. This version 
     * uploads the data immediately, through using Map/Unmap.
     * Upload buffer will be just large enough to contain 
     * provided data.
     * @tparam AllocPtrT Type of the allocator pointer.
     * @tparam ItT Type of the iterator, which must be of 
     * random-access iterator type.
     * @param begin Begin iterator for the data.
     * @param end End iterator for the data.
     * @param desc Description of the buffer. Width can be set 
     * to 0, for automatic size detection.
     * @param allocator Allocator used to allocate the buffer.
     * @param initResourceState Initial state of the resource.
     * @param name Optional name of the buffer.
     * @throws util::winexception Thrown on error.
     */
    template <typename AllocPtrT, typename ItT>
    D3D12UploadBuffer(const ItT &begin, const ItT &end,
        const ::CD3DX12_RESOURCE_DESC &desc, AllocPtrT allocator, 
        ::D3D12_RESOURCE_STATES initResourceState = D3D12_RESOURCE_STATE_GENERIC_READ, 
        const std::wstring &name = L"UploadBuffer");
}; // class D3D12UploadBuffer

} // namespace d3d12

} // namespace helpers

} // namespace quark

// Template implementation

namespace quark
{

namespace helpers
{

namespace d3d12
{

template <typename AllocPtrT, typename ItT>
D3D12UploadBuffer::D3D12UploadBuffer(const ItT &begin, const ItT &end, 
    const ::CD3DX12_RESOURCE_DESC &desc, res::d3d12::D3D12CommandList &cmdList, 
    AllocPtrT allocator, D3D12ResourceUpdater &updater, 
    ::D3D12_RESOURCE_STATES initResourceState, const std::wstring &name)
{ allocateUpload(begin, end, desc, cmdList, allocator, updater, initResourceState, name); }

template <typename AllocPtrT, typename ItT>
D3D12UploadBuffer::D3D12UploadBuffer(const ItT &begin, const ItT &end, const ::CD3DX12_RESOURCE_DESC &desc,
    AllocPtrT allocator, ::D3D12_RESOURCE_STATES initResourceState, const std::wstring &name)
{ allocateUploadImmediate(begin, end, desc, allocator, initResourceState, name); }

template <typename AllocPtrT, typename ItT>
D3D12UploadBuffer::PtrT D3D12UploadBuffer::create(const ItT &begin, const ItT &end, const::CD3DX12_RESOURCE_DESC &desc, 
    res::d3d12::D3D12CommandList &cmdList, AllocPtrT allocator, D3D12ResourceUpdater &updater, 
    ::D3D12_RESOURCE_STATES initResourceState, const std::wstring &name)
{ return PtrT{ new D3D12UploadBuffer(begin, end, desc, cmdList, allocator, updater, initResourceState, name) }; }

template <typename AllocPtrT, typename ItT>
D3D12UploadBuffer::PtrT D3D12UploadBuffer::create(const ItT &begin, const ItT &end, const::CD3DX12_RESOURCE_DESC &desc, 
    AllocPtrT allocator, ::D3D12_RESOURCE_STATES initResourceState, const std::wstring &name)
{ return PtrT{ new D3D12UploadBuffer(begin, end, desc, allocator, initResourceState, name) }; }

template <typename AllocPtrT>
void D3D12UploadBuffer::allocate(AllocPtrT allocator, std::size_t sizeInBytes,
    ::D3D12_RESOURCE_STATES initResourceState, const std::wstring &name)
{ allocate(allocator, ::CD3DX12_RESOURCE_DESC::Buffer(sizeInBytes), initResourceState, nullptr, name); }

template <typename AllocPtrT>
void D3D12UploadBuffer::allocate(AllocPtrT allocator, const ::CD3DX12_RESOURCE_DESC &desc,
    ::D3D12_RESOURCE_STATES initResourceState, const std::wstring &name)
{ allocate(allocator, desc, initResourceState, nullptr, name); }

template <typename AllocPtrT, typename ItT>
void D3D12UploadBuffer::allocateUpload(const ItT &begin, const ItT &end, const ::CD3DX12_RESOURCE_DESC &desc,
    res::d3d12::D3D12CommandList &cmdList, AllocPtrT allocator, D3D12ResourceUpdater &updater, 
    ::D3D12_RESOURCE_STATES initResourceState, const std::wstring &name)
{
    using TraitsT = std::iterator_traits<ItT>;

    using ElementT = typename TraitsT::value_type;
    typename TraitsT::pointer data{ &(*begin) };
    const std::size_t sizeInBytes{ (end - begin) * sizeof(ElementT) };

    auto finalDesc{ desc };
    if (finalDesc.Width == 0u)
    { // Set for auto-detection.
        finalDesc.Width = sizeInBytes;
    }

    allocate(allocator, finalDesc, initResourceState, nullptr, name);
    uploadData(data, sizeInBytes, cmdList, updater);
}

template <typename AllocPtrT, typename ItT>
void D3D12UploadBuffer::allocateUploadImmediate(const ItT &begin, const ItT &end, const ::CD3DX12_RESOURCE_DESC &desc,
    AllocPtrT allocator, ::D3D12_RESOURCE_STATES initResourceState, const std::wstring &name)
{
    using TraitsT = std::iterator_traits<ItT>;

    using ElementT = typename TraitsT::value_type;
    typename TraitsT::pointer data{ &(*begin) };
    const std::size_t sizeInBytes{ (end - begin) * sizeof(ElementT) };

    auto finalDesc{ desc };
    if (finalDesc.Width == 0u)
    { // Set for auto-detection.
        finalDesc.Width = sizeInBytes;
    }

    allocate(allocator, finalDesc, initResourceState, nullptr, name);
    uploadDataImmediate(data, sizeInBytes);
}

template <typename ItT>
typename std::enable_if<std::is_same_v<typename std::iterator_traits<ItT>::iterator_category, std::random_access_iterator_tag>,
void>::type D3D12UploadBuffer::uploadData(const ItT &begin, const ItT &end, res::d3d12::D3D12CommandList &cmdList,
    D3D12ResourceUpdater &updater)
{
    using TraitsT = std::iterator_traits<ItT>;

    using ElementT = typename TraitsT::value_type;
    typename TraitsT::pointer data{ &(*begin) };
    const std::size_t sizeInBytes{ (end - begin) * sizeof(ElementT) };

    uploadData(data, sizeInBytes, cmdList, updater);
}

template <typename AllocPtrT>
void D3D12UploadBuffer::allocate(AllocPtrT allocator, const ::CD3DX12_RESOURCE_DESC &desc,
    const ::D3D12_RESOURCE_STATES &initResourceState, const ::D3D12_CLEAR_VALUE *clearValue, 
    const std::wstring &name)
{
    res::d3d12::D3D12Resource::allocate(desc, initResourceState, clearValue, allocator);

    if (!name.empty())
    { getPtr()->SetName(name.c_str()); }
}

}

}

}

// Template implementation end
