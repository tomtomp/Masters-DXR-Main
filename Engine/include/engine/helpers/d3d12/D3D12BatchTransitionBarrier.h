/**
 * @file helpers/d3d12/D3D12BatchTransitionBarrier.h
 * @author Tomas Polasek
 * @brief Helper D3D12 class used to batch transition 
 * barriers into one ResourceBarrier command.
 */

#pragma once

#include "engine/resources/d3d12/D3D12Resource.h"
#include "engine/resources/d3d12/D3D12CommandQueueMgr.h"

/// Namespace containing the engine code.
namespace quark
{

/// Namespace containing helper classes and methods.
namespace helpers
{

/// Direct3D 12 helpers.
namespace d3d12
{

/*
 * Helper D3D12 class used to batch transition 
 * barriers into one ResourceBarrier command.
 */
class D3D12BatchTransitionBarrier
{
public:
    /// Helper used for passing resources.
    struct ResourceTransitionRequest
    {
        ResourceTransitionRequest(res::d3d12::D3D12Resource &resource, 
            ::D3D12_RESOURCE_STATES state = ::D3D12_RESOURCE_STATE_COMMON, 
            ::UINT subRes = D3D12_RESOURCE_BARRIER_ALL_SUBRESOURCES,
            ::D3D12_RESOURCE_BARRIER_FLAGS flag = ::D3D12_RESOURCE_BARRIER_FLAG_NONE);
        ResourceTransitionRequest(res::d3d12::D3D12Resource::PtrT &resource, 
            ::D3D12_RESOURCE_STATES state = ::D3D12_RESOURCE_STATE_COMMON, 
            ::UINT subRes = D3D12_RESOURCE_BARRIER_ALL_SUBRESOURCES,
            ::D3D12_RESOURCE_BARRIER_FLAGS flag = ::D3D12_RESOURCE_BARRIER_FLAG_NONE);
        template <typename ResT>
        ResourceTransitionRequest(ResT &resource, 
            ::D3D12_RESOURCE_STATES state = ::D3D12_RESOURCE_STATE_COMMON, 
            ::UINT subRes = D3D12_RESOURCE_BARRIER_ALL_SUBRESOURCES,
            ::D3D12_RESOURCE_BARRIER_FLAGS flag = ::D3D12_RESOURCE_BARRIER_FLAG_NONE);

        /// Resource to transition.
        res::d3d12::D3D12Resource *res{ nullptr };
        /// Target state to transition to.
        ::D3D12_RESOURCE_STATES targetState{ ::D3D12_RESOURCE_STATE_COMMON };
        /// Target sub-resource.
        ::UINT subResource{ D3D12_RESOURCE_BARRIER_ALL_SUBRESOURCES};
        /// Flags for the barrier.
        ::D3D12_RESOURCE_BARRIER_FLAGS flags{ ::D3D12_RESOURCE_BARRIER_FLAG_NONE };
    }; // struct ResourceTransitionRequest

    // Class contains only static methods.
    D3D12BatchTransitionBarrier() = delete;

    /**
     * Transition all resources using the same barrier configuration.
     * Ignores all other values settings in the transition requests.
     * @param targetState Target state to transition all the resources to.
     * @param cmdList Command list used to record the barrier on.
     * @param requests List of resources to transition.
     * @param subResource Target sub-resource to transition.
     * @param flags Flags used for the barrier.
     */
    static void transitionTo(::D3D12_RESOURCE_STATES targetState, res::d3d12::D3D12CommandList &cmdList,
        std::initializer_list<ResourceTransitionRequest> requests, 
        ::UINT subResource = D3D12_RESOURCE_BARRIER_ALL_SUBRESOURCES,
        ::D3D12_RESOURCE_BARRIER_FLAGS flags = ::D3D12_RESOURCE_BARRIER_FLAG_NONE);
private:
protected:
}; // class D3D12BatchTransitionBarrier

} // namespace d3d12

} // namespace helpers

} // namespace quark

// template implementation

namespace quark
{

namespace helpers
{

namespace d3d12
{

template<typename ResT>
D3D12BatchTransitionBarrier::ResourceTransitionRequest::ResourceTransitionRequest(
    ResT &resource, ::D3D12_RESOURCE_STATES state, ::UINT subRes, ::D3D12_RESOURCE_BARRIER_FLAGS flag) : 
    res{ resource.get() }, targetState{ state }, subResource{ subRes }, flags{ flag }
{ }

}

}

}

// template implementation end
