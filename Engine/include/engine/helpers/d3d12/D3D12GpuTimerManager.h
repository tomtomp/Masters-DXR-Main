/**
 * @file helpers/d3d12/D3D12GpuTimerManager.h
 * @author Tomas Polasek
 * @brief Helper class allowing profiling of GPU 
 * performance using timers.
 */

#pragma once

#include "engine/resources/d3d12/D3D12Device.h"
#include "engine/resources/d3d12/D3D12CommandQueueMgr.h"
#include "engine/resources/d3d12/D3D12Resource.h"
#include "engine/resources/d3d12/D3D12QueryHeap.h"

/// Namespace containing the engine code.
namespace quark
{

/// Namespace containing helper classes and methods.
namespace helpers
{

/// Direct3D 12 resource management.
namespace d3d12
{

/**
 * Helper class allowing profiling of GPU 
 * performance using timers.
 */
class D3D12GpuTimerManager : public util::PointerType<D3D12GpuTimerManager>
{
public:
    /// Default number of timers allocated.
    static constexpr std::size_t DEFAULT_NUM_TIMERS{ 128u };

    /// Handle to an allocated timer.
    using TimerHandleT = std::size_t;

    /// Single timer record, as used in the timer buffer.
    struct alignas(uint64_t) TimerValue
    {
        /// Start ticks of the timer.
        uint64_t start;
        /// End ticks of the timer.
        uint64_t end;

        /**
         * Convert handle to a timer into index for its start 
         * time on the query heap.
         * @param handle Handle to the timer.
         * @return Returns index in the query heap which can 
         * be used to access the start time of the timer.
         */
        static ::UINT handleToStartTimer(const TimerHandleT &handle);

        /**
         * Convert handle to a timer into index for its start 
         * time on the query heap.
         * @param handle Handle to the timer.
         * @return Returns index in the query heap which can 
         * be used to access the start time of the timer.
         */
        static ::UINT handleToEndTimer(const TimerHandleT &handle);

        /**
         * Calculate current duration between the start and 
         * end timers in GPU ticks.
         * @return Returns number of ticks between start and 
         * and timers.
         */
        std::size_t ticks() const;

        /**
         * Convert current values of timer start and end in ticks 
         * into a duration in provided units.
         * @tparam DurationT Specification of units in which the 
         * result should be returned as.
         * @param ticksPerSecond How many ticks are there in one 
         * second.
         * @return Returns duration in specified units.
         */
        template <typename DurationT>
        DurationT duration(std::size_t ticksPerSecond) const;
    }; // struct TimerValue

    /**
     * Exception thrown when attempting to read 
     * timer values without mapping them first first. 
     */
    struct TimerNotMappedException : public std::exception
    {
        TimerNotMappedException(const char *msg) :
            std::exception(msg)
        { }
    }; // struct TimerNotMappedException

    /**
     * Exception thrown when no more timer slots are 
     * available for allocation.
     */
    struct TimerNotAvailableException : public std::exception
    {
        TimerNotAvailableException(const char *msg) :
            std::exception(msg)
        { }
    }; // struct TimerNotAvailableException

    /**
     * Initialize the profiler with provided number of 
     * timers.
     * @param device Device which will be profiled.
     * @param directCmdQueue Command queue used for getting 
     * frequency of the device.
     * @param numTimers Maximal number of timers which can 
     * be used at a time.
     * @throws util::winexception Thrown on error.
     */
    static PtrT create(res::d3d12::D3D12Device &device, 
        res::d3d12::D3D12CommandQueueMgr &directCmdQueue, 
        std::size_t numTimers = DEFAULT_NUM_TIMERS);

    /// Free all allocated resources.
    ~D3D12GpuTimerManager();

    // No copying, allow moving.
    D3D12GpuTimerManager(const D3D12GpuTimerManager &other) = delete;
    D3D12GpuTimerManager &operator=(const D3D12GpuTimerManager &other) = delete;
    D3D12GpuTimerManager(D3D12GpuTimerManager &&other) = default;
    D3D12GpuTimerManager &operator=(D3D12GpuTimerManager &&other) = default;

    /**
     * Create a new timer within this profiler and 
     * return a handle to it.
     * @return Returns a handle to the created timer.
     * @throws TimerNotAvailableException Thrown when no 
     * more timer slots are available.
     */
    TimerHandleT allocateTimer();

    /**
     * Free provided timer handle, allowing its reuse.
     * When invalid or un-allocated timer handle is 
     * provided, no actions are taken.
     * @param handle Handle to a timer within this 
     * profiler.
     * @throws TimerNotAvailableException Thrown when 
     * provided timer handle has never been allocated 
     * by this profiler.
     */
    void freeTimer(const TimerHandleT &handle);

    /// Are there any free timers which can be allocated?
    bool hasFreeTimers() const;

    /**
     * Map the result buffer, allowing the reading 
     * of timer values.
     * @throws util::winexception Thrown on mapping error.
     */
    void mapTimerResults();

    /**
     * Unmap the result buffer, after which reading 
     * is no longer possible, until another map call.
     */
    void unmapTimerResults();

    /**
     * Start time measurement in given command list.
     * @param cmdList Command list being measured.
     * @param handle Handle to the timer which should be 
     * used.
     * @throws TimerNotAvailableException Thrown when 
     * attempting to start an unallocated timer.
     */
    void startTimer(res::d3d12::D3D12CommandList &cmdList, 
        const TimerHandleT &handle);

    /**
     * Start time measurement in given command list.
     * @param cmdList Command list being measured.
     * @param handle Handle to the timer which should be 
     * used.
     * @throws TimerNotAvailableException Thrown when 
     * attempting to stop an unallocated timer.
     * @warning Given command list is not checked to be 
     * the same as the one in the startTimer()!
     */
    void stopTimer(res::d3d12::D3D12CommandList &cmdList, 
        const TimerHandleT &handle);

    /**
     * Resolve all timer values into the internal buffer, 
     * making it ready for reading.
     * @param cmdList Command list used for the resolving.
     */
    void resolveTimers(res::d3d12::D3D12CommandList &cmdList);

    /**
     * Get raw values of a timer specified by given timer 
     * handle.
     * @param handle Handle to the timer.
     * @return Returns raw timer values for given timer.
     * @warning mapTimerResults must be called before 
     * using this function!
     * @throws TimerNotMappedException Thrown when 
     * attempting to read timer without first mapping 
     * the buffer.
     * @throws TimerNotAvailableException Thrown when 
     * attempting to read an unallocated timer.
     */
    const TimerValue &readTimerRaw(const TimerHandleT &handle) const;

    /**
     * Get value of a timer specified by given timer 
     * handle.
     * @param handle Handle to the timer.
     * @return Returns accumulated timer value in 
     * ticks.
     * @warning mapTimerResults must be called before 
     * using this function!
     * @throws TimerNotMappedException Thrown when 
     * attempting to read timer without first mapping 
     * the buffer.
     * @throws TimerNotAvailableException Thrown when 
     * attempting to read an unallocated timer.
     */
    std::size_t readTimer(const TimerHandleT &handle) const;

    /**
     * Get value of a timer specified by given timer 
     * handle.
     * @param handle Handle to the timer.
     * @return Returns accumulated timer value in 
     * milliseconds.
     * @warning mapTimerResults must be called before 
     * using this function!
     * @throws TimerNotMappedException Thrown when 
     * attempting to read timer without first mapping 
     * the buffer.
     * @throws TimerNotAvailableException Thrown when 
     * attempting to read an unallocated timer.
     */
    float readTimerMs(const TimerHandleT &handle) const;

    /// Get number of GPU ticks per second.
    std::size_t gpuTicksPerSecond() const;

    /// Dump all of the timer values.
    void debugDumpTimerValues() const;
private:
    /// Initialize the gpu profiler.
    void initialize(res::d3d12::D3D12Device &device,
        res::d3d12::D3D12CommandQueueMgr &directCmdQueue,
        std::size_t numTimers);

    /// Are there any timers which were never allocated?
    bool hasNeverAllocatedTimers() const;

    /// Is given timer handle allocated at the moment?
    bool handleAllocated(const TimerHandleT &handle) const;

    /**
     * Calculate how many bytes in the timer buffer 
     * are currently used by active timers.
     * Fragmentation is counted as used bytes too.
     * @return Returns the number of bytes.
     */
    std::size_t calculateActiveTimerBufferSize() const;

    /**
     * Calculate how many bytes are required to store 
     * maximal number of timers specified.
     * @return Returns the number of bytes.
     */
    std::size_t calculateTotalTimerBufferSize() const;

    /**
     * Calculate how many query timers we need - basically 
     * 2 per each of the allocatable timers.
     * @return Returns number of query timers in the query 
     * heap.
     */
    std::size_t calculateTotalQueryTimers() const;

    /**
     * Attempt to determine the upper bound of allocated 
     * timers and set the mLastNeverAllocated accordingly.
     * @warning mFreeTimerHandles will be changed within 
     * this method!
     */
    void checkFreeTimers();

    /**
     * Convert handle to a timer into index for start of 
     * its TimerValue structure within the timer buffer.
     * @param handle Handle to the timer.
     * @return Returns index in the mapped buffer, which 
     * can be used to access the timer value.
     */
    std::size_t handleToBufferIndex(const TimerHandleT &handle) const;

    /// Number of GPU timer ticks in one second.
    std::size_t mGpuTicksPerSecond{ 0u };
    /// Command queue used for getting current GPU frequency.
    res::d3d12::D3D12CommandQueueMgr::PtrT mCommandQueue{ };
    /// Buffer containing timer results.
    res::d3d12::D3D12Resource::PtrT mTimerBuffer{ };
    /// Pointer to the mapped buffer.
    TimerValue *mMappedBuffer{ nullptr };
    /// Heap containing the query timers.
    res::d3d12::D3D12QueryHeap::PtrT mQueryHeap{ };

    /// Last timer handle which is unallocated.
    TimerHandleT mLastNeverAllocated{ 0u };
    /// Maximal number of timers which can be allocated.
    std::size_t mMaxTimers{ 0u };
    /// Timer handle which were freed and can be reused.
    std::list<TimerHandleT> mFreeTimerHandles{ };
protected:
    /**
     * Create profiler without any timers.
     */
    D3D12GpuTimerManager();

    /**
     * Initialize the profiler with provided number of 
     * timers.
     * @param device Device which will be profiled.
     * @param directCmdQueue Command queue used for getting 
     * frequency of the device.
     * @param numTimers Maximal number of timers which can 
     * be used at a time.
     * @throws util::winexception Thrown on error.
     */
    D3D12GpuTimerManager(res::d3d12::D3D12Device &device, 
        res::d3d12::D3D12CommandQueueMgr &directCmdQueue, 
        std::size_t numTimers = DEFAULT_NUM_TIMERS);
}; // class D3D12GpuTimerManager

} // namespace d3d12

} // namespace helpers

} // namespace quark

// Template implementation

namespace quark
{

namespace helpers
{

namespace d3d12
{

template <typename DurationT>
DurationT D3D12GpuTimerManager::TimerValue::duration(std::size_t ticksPerSecond) const 
{
    if (start >= end)
    { return DurationT{ }; }

    using SecondsT = std::chrono::duration<typename DurationT::rep>;
    const auto seconds{ SecondsT((end - start) / static_cast<double>(ticksPerSecond)) };
    return std::chrono::duration_cast<DurationT>(seconds);
}

}

}

}

// Template implementation end
