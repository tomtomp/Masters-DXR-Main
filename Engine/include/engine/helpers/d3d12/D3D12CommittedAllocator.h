/**
 * @file helpers/d3d12/D3D12CommittedAllocator.h
 * @author Tomas Polasek
 * @brief Allocator for Direct3D 12 committed resources.
 */

#pragma once

#include "engine/helpers/d3d12/D3D12BaseGpuAllocator.h"

#include "engine/resources/d3d12/D3D12Device.h"

/// Namespace containing the engine code.
namespace quark
{

/// Namespace containing helper classes and methods.
namespace helpers
{

/// Direct3D 12 resource management.
namespace d3d12
{

/**
 * Simple Direct3D resource allocator, which allows 
 * the allocation of committed resources. 
 * Committed resources are created with implicit heap 
 * whose properties and flags can be specified.
 */
class D3D12CommittedAllocator : public D3D12BaseGpuAllocator, public util::PointerType<D3D12CommittedAllocator>
{
public:
    /// Unbound allocator, cannot be used to allocate.
    static PtrT create();


    /**
     * Create committed resource allocator on given device.
     * @param device Device to allocate on.
     * @param props Properties of the implicit heaps created 
     * when allocating resources.
     * @param flags Flags of the implicit heaps created 
     * when allocating resources.
     */
    static PtrT create(res::d3d12::D3D12Device &device, 
        const ::CD3DX12_HEAP_PROPERTIES &props, ::D3D12_HEAP_FLAGS flags);

    /**
     * Create committed resource allocator on given device.
     * @param device Device to allocate on.
     * @param props Properties of the implicit heaps created 
     * when allocating resources.
     * @param flags Flags of the implicit heaps created 
     * when allocating resources.
     */
    static PtrT create(res::d3d12::D3D12Device &device, 
        const ::D3D12_HEAP_PROPERTIES &props, ::D3D12_HEAP_FLAGS flags);

    /// No resources need to be freed.
    virtual ~D3D12CommittedAllocator() = default;

    // Allow copying.
    D3D12CommittedAllocator(const D3D12CommittedAllocator &other) = default;
    D3D12CommittedAllocator &operator=(const D3D12CommittedAllocator &other) = default;
    // Allow moving.
    D3D12CommittedAllocator(D3D12CommittedAllocator &&other) = default;
    D3D12CommittedAllocator &operator=(D3D12CommittedAllocator &&other) = default;

    /**
     * Allocate resource described in the provided 
     * description structure. After creation, the 
     * resource will have requested initial state and 
     * use optimized clear value.
     * @param desc Description of the resource.
     * @param initState Initial state of the resource 
     * after allocation.
     * @param optimizedClearValue Clear value used by 
     * the resource. Can be nullptr.
     * @param riid Identifier of the resource interface. 
     * Can be gained by using IID_PPV_ARGS macro.
     * @param ptr Pointer to the resource being allocated. 
     * Can be gained by using IID_PPV_ARGS macro.
     * @throws On allocation error throws util::winexception 
     * which contains error description.
     */
    virtual void allocate(const D3D12_RESOURCE_DESC &desc,
        D3D12_RESOURCE_STATES initState,
        const D3D12_CLEAR_VALUE *optimizedClearValue,
        REFIID riid, void **ptr) override final;

    /**
     * Deallocate given resource.
     * @tparam ResT Type of the target resource.
     * @param riid Identifier of the resource interface. 
     * Can be gained by using IID_PPV_ARGS macro.
     * @param ptr Pointer to the resource being allocated. 
     * Can be gained by using IID_PPV_ARGS macro.
     * @throws On deallocation error throws util::winexception 
     * which contains error description.
     * @warning Only resource allocated by this allocator 
     * should be passed to the deallocate method!
     */
    virtual void deallocate(REFIID riid, void **ptr) override final;

    /// Get heap properties of this allocator.
    const ::CD3DX12_HEAP_PROPERTIES &heapProperties() const
    { return mHeapProps; }

    /// Get heap flags of this allocator.
    const ::D3D12_HEAP_FLAGS &heapFlags() const
    { return mHeapFlags; }

    /// Is this allocator valid?
    virtual explicit operator bool() const override
    { return mDevice; }
private:
    /// Device used by the allocator.
    res::d3d12::D3D12Device *mDevice{ nullptr };
    /// Properties for the implicit resource heaps.
    ::CD3DX12_HEAP_PROPERTIES mHeapProps{ };
    /// Flags for the implicit resource heaps.
    ::D3D12_HEAP_FLAGS mHeapFlags{ };
protected:
    /// Unbound allocator, cannot be used to allocate.
    D3D12CommittedAllocator();

    /**
     * Create committed resource allocator on given device.
     * @param device Device to allocate on.
     * @param props Properties of the implicit heaps created 
     * when allocating resources.
     * @param flags Flags of the implicit heaps created 
     * when allocating resources.
     */
    D3D12CommittedAllocator(res::d3d12::D3D12Device &device, 
        const ::CD3DX12_HEAP_PROPERTIES &props, ::D3D12_HEAP_FLAGS flags);

    /**
     * Create committed resource allocator on given device.
     * @param device Device to allocate on.
     * @param props Properties of the implicit heaps created 
     * when allocating resources.
     * @param flags Flags of the implicit heaps created 
     * when allocating resources.
     */
    D3D12CommittedAllocator(res::d3d12::D3D12Device &device, 
        const ::D3D12_HEAP_PROPERTIES &props, ::D3D12_HEAP_FLAGS flags);
}; // class D3D12CommittedAllocator

} // namespace d3d12

} // namespace helpers

} // namespace quark
