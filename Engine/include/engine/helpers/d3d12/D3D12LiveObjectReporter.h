/**
 * @file helpers/d3d12/D3D12LiveObjectReporter.h
 * @author Tomas Polasek
 * @brief Direct3D 12 helper which reports live 
 * objects on destruction.
 */

#pragma once

#include "engine/util/SafeD3D12.h"

/// Namespace containing the engine code.
namespace quark
{

/// Namespace containing helper classes and methods.
namespace helpers
{

/// Direct3D 12 helpers.
namespace d3d12
{

/**
 * Helper class which reports live D3D12 objects on destruction.
 * It should be constructed as a part of some aggregate D3D12 
 * manager, so that it can be destroyed last and report living 
 * objects for use in debugging memory leaks.
 */
class D3D12LiveObjectReporter
{
public:
    /**
     * Configure the reporter with given parameters.
     * @param doReport Should the reporting be performed?
     * @param debugId What kind of resources should 
     * be reported, e.g. ::DXGI_DEBUG_ALL.
     * @param flags How to report and how much information 
     * should be displayed, e.g. ::DXGI_DEBUG_RLO_IGNORE_INTERNAL.
     */
    D3D12LiveObjectReporter(bool doReport, 
        ::DXGI_DEBUG_ID debugId = ::DXGI_DEBUG_ALL, 
        ::DXGI_DEBUG_RLO_FLAGS flags = ::DXGI_DEBUG_RLO_IGNORE_INTERNAL);

    /// Perform the check and print information.
    ~D3D12LiveObjectReporter();
private:
    /// Should the reporting be performed?
    bool mDoReport{ false };
    /// What should be reported?
    ::GUID mDebugId{ ::DXGI_DEBUG_ALL };
    /// How should they be reported?
    ::DXGI_DEBUG_RLO_FLAGS mFlags{ ::DXGI_DEBUG_RLO_IGNORE_INTERNAL };
protected:
}; // class D3D12LiveObjectReporter.

} // namespace d3d12

} // namespace helpers

} // namespace quark
