/**
 * @file helpers/d3d12/D3D12RTRootSignatureRegistry.h
 * @author Tomas Polasek
 * @brief Wrapper around a list of root signatures and their 
 * associated shader names.
 */

#pragma once

#ifdef ENGINE_RAY_TRACING_ENABLED

#include "engine/resources/d3d12/D3D12RootSignature.h"

/// Namespace containing the engine code.
namespace quark
{

/// Namespace containing helper classes and methods.
namespace helpers
{

/// Direct3D 12 resource management.
namespace d3d12
{

/**
 * Wrapper around a list of root signatures and their 
 * associated shader names.
 */
class D3D12RTRootSignatureRegistry
{
public:
    /// Use the same type of root signatures.
    using D3D12RootSignatureT = res::d3d12::D3D12RootSignature::D3D12RootSignatureT;

    /**
     * Exception thrown when an association is requested 
     * to an unknown root signature name.
     */
    struct UnknownNameException : public std::exception
    {
        UnknownNameException(const char *msg) :
            std::exception(msg)
        { }
    }; // struct UnknownNameException

    /**
     * Exception thrown when trying to register the same 
     * root signature with 2 different names. Can also be 
     * thrown when the same name is used twice.
     */
    struct AlreadyRegisteredException : public std::exception
    {
        AlreadyRegisteredException(const char *msg) :
            std::exception(msg)
        { }
    }; // struct AlreadyRegisteredException

    /// Empty root signature registry.
    D3D12RTRootSignatureRegistry();

    /// Free any resources used by the registry.
    ~D3D12RTRootSignatureRegistry();

    // No copying, allow moving.
    D3D12RTRootSignatureRegistry(const D3D12RTRootSignatureRegistry &other) = delete;
    D3D12RTRootSignatureRegistry &operator=(const D3D12RTRootSignatureRegistry &other) = delete;
    D3D12RTRootSignatureRegistry(D3D12RTRootSignatureRegistry &&other) = default;
    D3D12RTRootSignatureRegistry &operator=(D3D12RTRootSignatureRegistry &&other) = default;

    /// Reset the associations and the root signature database.
    void clear();

    /**
     * Register given root signature in this registry.
     * @param rootSignature Root signature to register.
     * @param name Optional name of the root signature, under which it 
     * can later be used. When set to empty string, the association 
     * will not be usable with a name.
     * @throws AlreadyRegisteredException Thrown when trying to register 
     * the same root signature with multiple non-empty names.
     */
    void registerRootSignature(res::d3d12::D3D12RootSignature &rootSignature, 
        const std::wstring &name = L"");

    /**
     * Associate a root signature with given shader.
     * Works with unregistered root signatures, they will just be 
     * registered, before their association.
     * @param rootSignature Signature being associated.
     * @param shader Shader name to associate with the root signature.
     */
    void associateWith(res::d3d12::D3D12RootSignature &rootSignature, 
        const std::wstring &shader);

    /**
     * Associate a root signature with given shader.
     * Works with unregistered root signatures, they will just be 
     * registered, before their association.
     * @param rootSignature Signature being associated.
     * @param shaders Shader names to associate with the root signature.
     */
    void associateWith(res::d3d12::D3D12RootSignature &rootSignature, 
        const std::initializer_list<std::wstring> &shaders);

    /**
     * Associate a root signature with given shader.
     * Works with unregistered root signatures, they will just be 
     * registered, before their association.
     * @param rootSignature Signature being associated.
     * @param shader Shader name to associate with the root signature.
     */
    void associateWith(ComPtr<D3D12RootSignatureT> &rootSignature, 
        const std::wstring &shader);

    /**
     * Associate a root signature with given shader.
     * @param rootSignatureName Use this name to lookup the root signature.
     * @param shader Shader name to associate with the root signature.
     * @throws UnknownNameException Thrown when provided root signature 
     * name has not been registered before.
     */
    void associateWith(const std::wstring &rootSignatureName, 
        const std::wstring &shader);

    /// Access the root signature associations.
    const auto &associations() const
    { return mRegistry; }
private:
    /**
     * Helper structure for holding information about shaders 
     * associated with a root signature.
     */
    struct RootSignatureAssociation
    {
        /// Print debug information about this record.
        void printInfo() const;

        /// The root signature.
        ComPtr<D3D12RootSignatureT> rootSignature{ };
        /// Optional name of the root signature.
        std::wstring rootSignatureName{ };
        /// List of associated shader names.
        std::vector<std::wstring> associatedShaders{ };
    }; // struct RootSignatureAssociation

    /**
     * Get already registered root signature, or create it if 
     * it doesn't exist yet.
     * @param rootSignature The root signature.
     * @return Returns initialized association structure for 
     * the provided root signature.
     */
    RootSignatureAssociation &createOrGet(D3D12RootSignatureT *rootSignature);

    /**
     * Register given root signature name with its association.
     * @param association Association to register.
     * @param rootSignatureName Name of the root signature.
     * @throws AlreadyRegisteredException Thrown when trying to 
     * associate a name with 2 different root signatures or if 
     * the association is already registered under different name.
     */
    void registerName(RootSignatureAssociation &association, 
        const std::wstring &rootSignatureName);

    /**
     * Get a root signature by name.
     * @param rootSignatureName Name of the root signature.
     * @return Returns pointer to its association.
     * @throws UnknownNameException Thrown when provided root signature 
     * name has not been registered before.
     */
    RootSignatureAssociation &get(const std::wstring &rootSignatureName);

    /**
     * Associate a root signature with given shader.
     * @param association Association record.
     * @param shader Shader name to associate with the root signature.
     */
    void associateWith(RootSignatureAssociation &association, 
        const std::wstring &shader);

    /**
     * Associate a root signature with given shader.
     * @param association Association record.
     * @param shaders Shader names to associate with the root signature.
     */
    void associateWith(RootSignatureAssociation &association, 
        const std::initializer_list<std::wstring> &shaders);

    /// Registry of managed root signature associations.
    std::deque<RootSignatureAssociation> mRegistry{ };
    /// Mapping from root signature pointers to the registry.
    std::map<D3D12RootSignatureT*, RootSignatureAssociation*> mPtrToRegistry{ };
    /// Mapping from root signature names to the registry.
    std::map<std::wstring, RootSignatureAssociation*> mNameToRegistry{ };
protected:
}; // class D3D12RTRootSignatureRegistry

} // namespace d3d12

} // namespace helpers

} // namespace quark

// Template implementation

namespace quark
{

namespace helpers
{

namespace d3d12
{

}

}

}

// Template implementation end

#endif 
