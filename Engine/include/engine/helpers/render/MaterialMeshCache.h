/**
 * @file helpers/render/MaterialMeshCache.h
 * @author Tomas Polasek
 * @brief Cache of materials and meshes, usable when rendering scenes.
 */

#pragma once

#include "engine/resources/render/MeshMgr.h"
#include "engine/resources/render/GpuMemoryMgr.h"

#include "engine/helpers/d3d12/D3D12DescTable.h"

#include "engine/helpers/hlsl/HlslCompatibility.h"

/// Namespace containing the engine code.
namespace quark
{

/// Namespace containing helper classes and methods.
namespace helpers
{

/// Rendering helpers and types.
namespace rndr
{

/**
 * Cache of materials and meshes, usable when rendering scenes.
 */
class MaterialMeshCache : public util::PointerType<MaterialMeshCache>
{
public:
    /**
     * Exception thrown when requested mesh is not registered 
     * or the cache needs to be rebuilt first.
     */
    struct MeshNotReadyException : std::exception
    {
        MeshNotReadyException(const char *msg) :
            std::exception(msg)
        { }
    }; // MeshNotReadyException

    /**
     * Exception thrown when user requests static size of some 
     * descriptor tables and there are more descriptors than 
     * the size of the provided table.
     */
    struct TableLayoutException : std::exception
    {
        TableLayoutException(const char *msg) :
            std::exception(msg)
        { }
    }; // struct TableLayoutException

    /**
     * Exception thrown when a geometry buffer is in invalid 
     * format.
     */
    struct InvalidBufferFormat : std::exception
    {
        InvalidBufferFormat(const char *msg) :
            std::exception(msg)
        { }
    }; // struct InvalidBufferFormat

    /// Range of indices, used for accessing textures and meshes.
    using IndexRange = util::Range<std::size_t>;

    /// Record about added material textures and mesh primitives.
    struct MaterialMeshRecord
    {
        /// Indices of mesh primitives.
        IndexRange primitiveRange;
    }; // struct MaterialMeshRecord

    /// Description of a single mesh, shared with shader code.
    using MeshSpecification = hlsl::structures::MeshSpecification;
    /// Description of a single material, shared with shader code.
    using MaterialSpecification = hlsl::structures::MaterialSpecification;

    /// Instance of mesh specification with invalid buffer handles.
    static constexpr MeshSpecification InvalidMeshSpecification{ 
        HLSL_INVALID_BUFFER_INDEX, HLSL_INVALID_BUFFER_INDEX, HLSL_INVALID_BUFFER_INDEX, 
        HLSL_INVALID_BUFFER_INDEX, HLSL_INVALID_BUFFER_INDEX
    };
    /// Instance of material specification with invalid texture indices.
    static constexpr MaterialSpecification InvalidMaterialSpecification{
        HLSL_INVALID_TEXTURE_INDEX, HLSL_INVALID_TEXTURE_INDEX, HLSL_INVALID_TEXTURE_INDEX
    };

    /// Description of a single primitives mesh and material.
    struct MaterialMeshSpecification
    {
        /// Mesh this primitive is part of.
        res::rndr::MeshHandle meshHandle{ };
        /// Index of the primitive within the mesh.
        std::size_t primitiveIndex{ 0u };

        /// Description of a single primitives mesh.
        MeshSpecification mesh{ };
        /// Description of accompanying material.
        MaterialSpecification material{ };
    }; // struct MaterialMeshSpecification

    /// Helper class which allows iteration over specifications.
    class MaterialMeshSpecificationIterable
    {
    public:
        /// Begin iterator.
        const MaterialMeshSpecification *begin() const;
        /// End iterator.
        const MaterialMeshSpecification *end() const;

        /// Is the range valid?
        bool valid() const;
    private:
        // Access the constructor.
        friend class MaterialMeshCache;

        /// Create the iterable.
        MaterialMeshSpecificationIterable(
            const std::vector<MaterialMeshSpecification> &specifications, 
            const MaterialMeshRecord &record);

        // Allow copying and moving.
        MaterialMeshSpecificationIterable(const MaterialMeshSpecificationIterable &other) = default;
        MaterialMeshSpecificationIterable(MaterialMeshSpecificationIterable &&other) = default;

        /// Vector of specifications used.
        const std::vector<MaterialMeshSpecification> &mSpecifications;
        /// Iterated record.
        const MaterialMeshRecord mRecord;
    protected:
    }; // class MaterialMeshSpecificationIterable

    /// Specification of sizes of the descriptor tables.
    struct LayoutSizeRequirements
    {
        uint32_t textureDescriptors{ 0u };
        uint32_t bufferDescriptors{ 0u };
    }; // struct LayoutSizeRequirements

    /// Empty material/mesh cache.
    static PtrT create();

    /// Free any resources.
    ~MaterialMeshCache();

    // No copying, allow moving.
    MaterialMeshCache(const MaterialMeshCache &other) = delete;
    MaterialMeshCache &operator=(const MaterialMeshCache &other) = delete;
    MaterialMeshCache(MaterialMeshCache &&other) = default;
    MaterialMeshCache &operator=(MaterialMeshCache &&other) = default;

    /**
     * Add given mesh into this cache.
     * If the mesh is already registered, no action will be taken.
     * @param meshHandle Handle to the added mesh.
     */
    void registerMesh(const res::rndr::MeshHandle &meshHandle);

    /**
     * Calculate how many descriptors will be needed with the current configuration.
     * @return Returns filled size specification structure.
     */
    LayoutSizeRequirements calculateSizeRequirements();

    /**
     * Rebuild the material/mesh cache. If the cache is dirty() this 
     * action triggers a re-creation of all structures, which will 
     * invalidate any previous structures/descriptors.
     * @param textureTable Descriptor table used for texture SRV allocation. 
     * @param bufferTable Descriptor table used for buffer SRV allocation. 
     * @throws util::winexception Thrown on d3d12 error.
     * @throws TableLayoutException Thrown when requested table size 
     * constrains could not be satisfied - e.g. there are more 
     * descriptors than the static size of the table allows.
     * @throws InvalidBufferFormat Thrown when one of the meshes has 
     * buffers in incompatible format with their assigned attribute.
     */
    void buildCache(d3d12::D3D12DescTable &textureTable, d3d12::D3D12DescTable &bufferTable);

    /**
     * Rebuild the material/mesh cache. If the cache is dirty() this 
     * action triggers a re-creation of all structures, which will 
     * invalidate any previous structures/descriptors.
     * This version builds only the texture table.
     * @param textureTable Descriptor table used for texture SRV allocation. 
     * @throws util::winexception Thrown on d3d12 error.
     * @throws TableLayoutException Thrown when requested table size 
     * constrains could not be satisfied - e.g. there are more 
     * descriptors than the static size of the table allows.
     * @throws InvalidBufferFormat Thrown when one of the meshes has 
     * buffers in incompatible format with their assigned attribute.
     */
    void buildTextureCache(d3d12::D3D12DescTable &textureTable);

    /**
     * Get record range for given mesh.
     * @param meshHandle Handle to the cached mesh.
     * @return Returns helper structure which allows iteration over 
     * the meshes specification structures.
     * @throws MeshNotReadyException Thrown when provided mesh handle 
     * has not been registered yet, or the cache has not been rebuilt 
     * since the registration.
     */
    MaterialMeshRecord recordForMesh(const res::rndr::MeshHandle &meshHandle) const;

    /**
     * Get iterable object used for accessing primitive specifications for 
     * mesh specified by the mesh handle.
     * @param meshHandle Handle to the cached mesh.
     * @return Returns iterable object used for accessing the specifications.
     * @throws MeshNotReadyException Thrown when provided mesh handle 
     * has not been registered yet, or the cache has not been rebuilt 
     * since the registration.
     */
    MaterialMeshSpecificationIterable iterableForMesh(const res::rndr::MeshHandle &meshHandle) const;

    /**
     * Get iterable object used for accessing primitive specifications for 
     * mesh specified by the record.
     * @param record Record of the target mesh.
     * @return Returns iterable object used for accessing the specifications.
     */
    MaterialMeshSpecificationIterable iterableForRecord(const MaterialMeshRecord &record) const;

    /**
     * Get material/mesh specification for provided mesh and primitive combination.
     * @param meshHandle Complete mesh, which contains the primitive.
     * @param primitiveIndex Index of the primitive within the mesh.
     * @return Returns reference to the specification for requested primitive, within 
     * the mesh.
     * @throws MeshNotReadyException Thrown when provided mesh handle has not been 
     * registered yet, the cache has not been rebuilt since the registration or it 
     * doesn't contain primitive with such index.
     */
    const MaterialMeshSpecification &specificationForPrimitive(const res::rndr::MeshHandle &meshHandle, 
        std::size_t primitiveIndex) const;

    /// Does the cache need rebuilding?
    bool dirty() const;

    /// Clear all configuration.
    void clear();
private:
    /**
     * Look for registered mesh in the registry.
     * @param meshHandle Handle to the mesh.
     * @return Returns pointer to the meshes record or 
     * nullptr if it was not found.
     */
    MaterialMeshRecord *findMesh(const res::rndr::MeshHandle &meshHandle);

    /**
     * Look for registered mesh in the registry.
     * @param meshHandle Handle to the mesh.
     * @return Returns pointer to the meshes record or 
     * nullptr if it was not found.
     */
    const MaterialMeshRecord *findMesh(const res::rndr::MeshHandle &meshHandle) const;

    /**
     * Look for registered mesh in the registry and create it 
     * if it was not registered before.
     * @param meshHandle Handle to the mesh.
     * @return Returns reference to the mesh record, which will 
     * be created if the mesh is not yet registered.
     */
    MaterialMeshRecord &findRegisterMesh(const res::rndr::MeshHandle &meshHandle);

    /**
     * Process the list of new requests and initialize their 
     * records in the mesh registry. Specification for each 
     * of the primitives will be only partially created, without 
     * allocating descriptors.
     * @param requests List of new requests.
     */
    void processNewRequests(std::vector<res::rndr::MeshHandle> &requests);

    /**
     * Calculate required number of descriptors to create views for all 
     * resources within the list of specifications.
     * @param requests List of new requests.
     * @param oldRequirements Size requirements from the last build.
     * @return Returns required number of descriptors.
     */
    LayoutSizeRequirements calculateNewSizeRequirements(const std::vector<res::rndr::MeshHandle> &requests,
        const LayoutSizeRequirements &oldRequirements) const;

    /**
     * Create UAV for given buffer view, in specified descriptor table.
     * @param descTable Table used for allocation.
     * @param bufferView Resource to view.
     */
    ::CD3DX12_CPU_DESCRIPTOR_HANDLE createBufferUav(d3d12::D3D12DescTable &descTable, 
        res::rndr::BufferView &bufferView);

    /**
     * Create SRV for given buffer view, in specified descriptor table.
     * @param descTable Table used for allocation.
     * @param bufferView Resource to view.
     */
    ::CD3DX12_CPU_DESCRIPTOR_HANDLE createBufferSrv(d3d12::D3D12DescTable &descTable, 
        res::rndr::BufferView &bufferView);

    /**
     * Create SRV for given texture, in specified descriptor table.
     * @param descTable Table used for allocation.
     * @param texture Resource to view.
     */
    ::CD3DX12_CPU_DESCRIPTOR_HANDLE createTextureSrv(d3d12::D3D12DescTable &descTable, 
        res::rndr::Texture &texture);

    /**
     * Allocate specification view descriptors from given descriptor heap.
     * @param textureTable Descriptor table used for texture SRV allocation. 
     * May be nullptr to disable texture handles.
     * @param bufferTable Descriptor table used for buffer SRV allocation. 
     * May be nullptr to disable texture handles.
     * @param specifications List of specifications to initialize.
     */
    void allocateDescriptors(std::vector<MaterialMeshSpecification> &specifications,
        d3d12::D3D12DescTable *textureTable, d3d12::D3D12DescTable *bufferTable);

    /**
     * Rebuild the material/mesh cache. If the cache is dirty() this 
     * action triggers a re-creation of all structures, which will 
     * invalidate any previous structures/descriptors.
     * @param textureTable Descriptor table used for texture SRV allocation. 
     * May be nullptr to disable texture handles.
     * @param bufferTable Descriptor table used for buffer SRV allocation. 
     * May be nullptr to disable texture handles.
     * @throws util::winexception Thrown on d3d12 error.
     * @throws TableLayoutException Thrown when requested table size 
     * constrains could not be satisfied - e.g. there are more 
     * descriptors than the static size of the table allows.
     */
    void buildCacheInner(d3d12::D3D12DescTable *textureTable, d3d12::D3D12DescTable *bufferTable);

    /// Does the cache need rebuilding?
    bool mDirty{ true };
    /// Mapping from registered meshes to their records.
    std::map<std::size_t, MaterialMeshRecord> mMeshRegistry{ };
    /// List of material and mesh specifications for each primitive.
    std::vector<MaterialMeshSpecification> mSpecifications{ };
    /// List of requests for mesh registration and caching.
    std::vector<res::rndr::MeshHandle> mMeshRequests{ };

    /// Old size requirements from the last build.
    LayoutSizeRequirements mCachedSizeRequirements{ };

    /// Do the new size requirements need recalculation?
    bool mNewSizeRequirementsDirty{ true };
    /// New size requirements including the new requests.
    LayoutSizeRequirements mNewSizeRequirements{ };
protected:
    /// Empty material/mesh cache.
    MaterialMeshCache();
}; // class MaterialMeshCache

} // namespace rndr

} // namespace helpers

} // namespace quark

// Template implementation

namespace quark
{

namespace helpers
{

namespace rndr
{

}

}

}

// Template implementation end
