/**
 * @file helpers/hlsl/HlslDeferredUser.h
 * @author Tomas Polasek
 * @brief Header containing definitions common to 
 * shaders which use deferred shaders..
 */

#ifndef ENGINE_HLSL_DEFERRED_USER_H
#define ENGINE_HLSL_DEFERRED_USER_H

#ifdef HLSL

#include "HlslCommon.h"

#ifndef DEFERRED_BUFFERS_SPACE
// Register space used by the deferred textures.
#   define DEFERRED_BUFFERS_SPACE space1
#endif // DEFERRED_BUFFERS_SPACE

/// G-Buffer texture containing world-space positions.
Texture2D<float4> gWorldPositionTexture : register(t0, DEFERRED_BUFFERS_SPACE);
/// G-Buffer texture containing world-space normals.
Texture2D<float4> gWorldNormalTexture : register(t1, DEFERRED_BUFFERS_SPACE);
/// G-Buffer texture containing material diffuse colors.
Texture2D<float4> gDiffuseColorTexture : register(t2, DEFERRED_BUFFERS_SPACE);
/// G-Buffer texture containing material properties.
Texture2D<float4> gMaterialPropsTexture : register(t3, DEFERRED_BUFFERS_SPACE);
/// G-Buffer texture containing depths.
Texture2D<float> gDepthTexture : register(t4, DEFERRED_BUFFERS_SPACE);

/**
 * Structure used for passing deferred shading information.
 */
struct DeferredInformation
{
    /// Screen-space UV coordinates.
    float2 ssTexCoord;
    /// Does G-Buffer contain data for this fragment?
    bool hasData;
    /// World-space position.
    float3 wsPosition;
    /// World-space normal.
    float3 wsNormal;
    /// Material diffuse color.
    float4 diffuse;
    /// Material metallic-roughness values.
    float2 metallicRoughness;
    /// Depth of the fragment from camera.
    float depth;
    /// Does the material generate specular reflections?
    float doReflections;
    /// Does the material generate "specular" refraction?
    float doRefractions;
}; // struct ShadingInformation

/**
 * Recover information from the deferred buffers, for the 
 * current dispatch index.
 * @param texCoord Texture coordinates <0.0f, 1.0f> of screen-space.
 * @return Returns filled deferred information structure.
 */
DeferredInformation prepareDeferredInformation(in float2 texCoord, in float3 cameraWsPosition)
{
    DeferredInformation result;

    result.ssTexCoord = texCoord;
    result.hasData = false;
    result.wsPosition = float3(0.0f, 0.0f, 0.0f);
    result.wsNormal = float3(0.0f, 0.0f, 0.0f);
    result.diffuse = float4(0.0f, 0.0f, 0.0f, 0.0f);
    result.metallicRoughness = float2(0.0f, 0.0f);
    result.depth = 0.0f;
    result.doReflections = 0.0f;
    result.doRefractions = 0.0f;

    // Detect whether we have any data for this fragment.
    result.wsPosition = gDecodeWsPosition(gWorldPositionTexture.Sample(gCommonSampler, texCoord), 
        cameraWsPosition, result.hasData);

    if (!result.hasData)
    { // No value written -> clear color.
        return result;
    }

    result.wsNormal = gDecodeWsNormal(gWorldNormalTexture.Sample(gCommonSampler, texCoord));

    result.diffuse = gDecodeDiffuse(gDiffuseColorTexture.Sample(gCommonSampler, texCoord));
    gDecodeMaterialProps(gMaterialPropsTexture.Sample(gCommonSampler, texCoord), 
        result.metallicRoughness, result.doReflections, result.doRefractions);
    result.depth = gDecodeDepth(gDepthTexture.Sample(gCommonSampler, texCoord));

    return result;
}

/**
 * Recover information from the deferred buffers, for the 
 * current dispatch index.
 * @param texCoord Texture coordinates <0.0f, 1.0f> of screen-space.
 * @return Returns filled deferred information structure.
 */
DeferredInformation prepareDeferredInformation(in uint2 index, in float3 cameraWsPosition)
{
    DeferredInformation result;

    result.ssTexCoord = index / textureDimensions(gWorldPositionTexture);
    result.hasData = false;
    result.wsPosition = float3(0.0f, 0.0f, 0.0f);
    result.wsNormal = float3(0.0f, 0.0f, 0.0f);
    result.diffuse = float4(0.0f, 0.0f, 0.0f, 0.0f);
    result.metallicRoughness = float2(0.0f, 0.0f);
    result.depth = 0.0f;
    result.doReflections = 0.0f;
    result.doRefractions = 0.0f;

    // Access the first mip-map.
    int3 textureIndex = int3(index, 0);

    // Detect whether we have any data for this fragment.
    result.wsPosition = gDecodeWsPosition(gWorldPositionTexture.Load(textureIndex), 
        cameraWsPosition, result.hasData);

    if (!result.hasData)
    { // No value written -> clear color.
        return result;
    }

    result.wsNormal = gDecodeWsNormal(gWorldNormalTexture.Load(textureIndex));

    result.diffuse = gDecodeDiffuse(gDiffuseColorTexture.Load(textureIndex));
    gDecodeMaterialProps(gMaterialPropsTexture.Load(textureIndex), 
        result.metallicRoughness, result.doReflections, result.doRefractions);
    result.depth = gDecodeDepth(gDepthTexture.Load(textureIndex));

    return result;
}

#endif // HLSL

#endif // ENGINE_HLSL_DEFERRED_USER_H
