/**
 * @file helpers/hlsl/HlslCommon.h
 * @author Tomas Polasek
 * @brief Header containing miscellaneous methods.
 */

#ifndef ENGINE_HLSL_COMMON_H
#define ENGINE_HLSL_COMMON_H

#ifdef HLSL

/**
 * Get width and height of given texture.
 * Uses level 0 mip-map.
 * @param tex Texture to get the size of.
 * @return Returns width and height of the texture.
 */
float2 textureDimensions(in Texture2D<float> tex)
{
    float width = 0.0f;
    float height = 0.0f;
    tex.GetDimensions(width, height);
    return float2(width, height);
}

/**
 * Get width and height of given texture.
 * Uses level 0 mip-map.
 * @param tex Texture to get the size of.
 * @return Returns width and height of the texture.
 */
float2 textureDimensions(in Texture2D<float3> tex)
{
    float width = 0.0f;
    float height = 0.0f;
    tex.GetDimensions(width, height);
    return float2(width, height);
}

/**
 * Get width and height of given texture.
 * Uses level 0 mip-map.
 * @param tex Texture to get the size of.
 * @return Returns width and height of the texture.
 */
float2 textureDimensions(in Texture2D tex)
{
    float width = 0.0f;
    float height = 0.0f;
    tex.GetDimensions(width, height);
    return float2(width, height);
}

/**
 * Encode world-space position for the G-Buffer.
 */
float4 gEncodeWsPosition(in float3 wsPosition, in float3 cameraWsPosition)
{ return float4(wsPosition - cameraWsPosition, 1.0f); }

/**
 * Decode world-space position from the G-Buffer, also 
 * returns whether there is any data in the fragment.
 */
float3 gDecodeWsPosition(in float4 gbPosition, in float3 cameraWsPosition, out bool hasData)
{
    hasData = gbPosition.w > 0.0f;
    return (gbPosition.xyz + cameraWsPosition) * hasData;
}

/**
 * Encode depth for the G-Buffer.
 */
float gEncodeDepth(in float depth)
{ return depth; }

/**
 * Decode depth from the G-Buffer.
 */
float gDecodeDepth(in float gbDepth)
{ return gbDepth; }

/**
 * Encode world-space normal for the G-Buffer.
 */
float4 gEncodeWsNormal(in float3 wsNormal)
{
    // <-1.0f, 1.0f> -> <0.0f, 1.0f>
    return (normalize(float4(wsNormal, 0.0f)) + 1.0f) / 2.0f;
}

/**
 * Decode world-space normal from the G-Buffer.
 */
float3 gDecodeWsNormal(in float4 gbNormal)
{
    // <0.0f, 1.0f> -> <-1.0f, 1.0f>
    return normalize((gbNormal.xyz * 2.0f) - 1.0f);
}

/**
 * Encode diffuse/albedo for the G-Buffer.
 */
float4 gEncodeDiffuse(in float4 diffuse)
{ return diffuse; }

/**
 * Decode diffuse/albedo from the G-Buffer.
 */
float4 gDecodeDiffuse(in float4 gbDiffuse)
{ return gbDiffuse; }

/**
 * Encode material properties for the G-Buffer.
 */
float4 gEncodeMaterialProps(in float2 metallicRoughness, in float doReflections, in float doRefractions)
{ return float4(metallicRoughness, doReflections, doRefractions); }

/**
 * Decode material properties from the G-Buffer.
 */
void gDecodeMaterialProps(in float4 gbMaterialProps, out float2 metallicRoughness, out float doReflections, out float doRefractions)
{
    metallicRoughness = gbMaterialProps.rg;
    doReflections = gbMaterialProps.b;
    doRefractions = gbMaterialProps.a;
}

/**
 * Generate a camera ray for given dispatch thread.
 * @param ssTexCoord Screen-space texture coordinates.
 * @param screenToWorld Matrix used for transforming from 
 * screen space <-1.0f, 1.0f> to world space of camera.
 * @param worldPosition Position of the camera in the 
 * world.
 * @param rayOrigin Origin of the generated ray.
 * @param rayDirection Direction of the generated ray.
 */
void generateCameraRay(in float2 ssTexCoord, in float4x4 screenToWorld, 
	in float3 worldPosition, out float3 rayOrigin, out float3 rayDirection)
{
    // Calculate screen-space position on the output raster, <-1.0f, 1.0f> in 2D.
    float2 rasterPos = ssTexCoord * 2.0f - 1.0f;
    rasterPos.y = -rasterPos.y;

    // Un-project the pixel back into the world-space.
    float4 worldPos = mul(float4(rasterPos, 0.0f, 1.0f), screenToWorld);
    worldPos.xyz /= worldPos.w;

    // Set the output attributes.
    rayOrigin = worldPosition;
    rayDirection = normalize(worldPos.xyz - rayOrigin);
}

/**
 * Linearize provided depth sample for frustum with 
 * given near and far values.
 * @param depth Non-linear depth sample.
 * @param near Position of the near plane.
 * @param far Position of the far plane.
 */
float linearizeDepth(in float depth, in float zNear = 0.1f, in float zFar = 100.0f)
{ 
    const float linearDepth = (2.0f * zNear) / (zFar + zNear - depth * (zFar - zNear));
    return linearDepth;
    //return zNear * zFar / (zFar + depth * (zNear - zFar)); 
}

#endif // HLSL

#endif // ENGINE_HLSL_COMMON_H
