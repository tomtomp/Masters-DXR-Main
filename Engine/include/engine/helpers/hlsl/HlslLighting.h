/**
 * @file helpers/hlsl/HlslLighting.h
 * @author Tomas Polasek
 * @brief Header containing common methods used for lighting 
 * and shading in all of the shaders.
 */

#ifndef ENGINE_HLSL_LIGHTING_H
#define ENGINE_HLSL_LIGHTING_H

#ifdef HLSL

/**
 * Calculate color for a pixel which missed all geometry.
 * @param viewDirection Direction of view ray from camera 
 * to the point.
 * @param backgroundColor Color of the background.
 * @param sunColor Color of the main directional light.
 * @param sunDirection Direction of the main directional 
 * light.
 * @return Returns the final color.
 */
float4 pbrMissColor(in float3 viewDirection, in float3 backgroundColor, 
    in float3 sunColor, in float3 sunDirection)
{
    const float sunAngle = 0.01f;
    const float3 direction = normalize(viewDirection);

    const float4 skyColor = float4(backgroundColor, 1.0f);
    const float4 lightColor = float4(sunColor, 1.0f);
    const float3 lightDirection = sunDirection;

    float4 result = skyColor;

    const float angleFromSun = 1.0f - saturate(dot(direction, -normalize(lightDirection)));
    if (angleFromSun < sunAngle)
    { result = lerp(lightColor, skyColor, angleFromSun / sunAngle); }

    return result;
}

/// Calculate fresnel coefficients.
float3 fresnelSchlick(in float cosTheta, in float3 f0)
{ return f0 + (1.0f - f0) * pow(1.0f - cosTheta, 5.0f); }

/// Calculate fresnel coefficients, with roughness.
float3 fresnelSchlickRoughness(in float cosTheta, in float3 f0, in float roughness)
{ return f0 + (max((1.0f - roughness), f0) - f0) * pow(1.0f - cosTheta, 5.0f); }

/// Distribution function used by GGX.
float distributionGGX(in float3 N, in float3 H, in float roughness)
{
    float a = roughness * roughness;
    float a2 = a * a;
    float NdotH = max(dot(N, H), 0.0f);
    float NdotH2 = NdotH * NdotH;

    float num = a2;
    float denom = (NdotH2 * (a2 - 1.0f) + 1.0f);
    denom = HLSL_PI * denom * denom;

    return num / denom;
}

/// Geometry term of GGX.
float geometrySchlickGGX(in float NdotV, in float roughness)
{
    float r = (roughness + 1.0f);
    float k = (r * r) / 8.0f;

    float num = NdotV;
    float denom = NdotV * (1.0f - k) + k;

    return num / denom;
}

/// Combination of geometry terms.
float geometrySmith(in float3 N, in float3 V, in float3 L, in float roughness)
{
    float NdotV = max(dot(N, V), 0.0f);
    float NdotL = max(dot(N, L), 0.0f);
    float ggx2 = geometrySchlickGGX(NdotV, roughness);
    float ggx1 = geometrySchlickGGX(NdotL, roughness);

    return ggx1 * ggx2;
}

/**
 * Calculate final light for a given surface-light combination.
 * @param normal World-space normal of the surface.
 * @param view World-space view vector from the eye to the surface.
 * @param light World-space vector from the surface to the light.
 * @param metallicRoughness Metallicity and roughness of the surface.
 * @param albedo Albedo color of the surface.
 * @param lightBaseColor Color of the reflected light.
 */
float3 specularColorGGX(float3 normal, float3 view, float3 light,
    in float2 metallicRoughness, in float3 albedo, in float3 lightBaseColor)
{
    const float3 halfway = normalize(view + light);
    const float metallic = metallicRoughness.x;
    const float roughness = metallicRoughness.y;

    float3 f0 = float3(0.04f, 0.04f, 0.04f);
    f0 = lerp(f0, albedo, metallic);
    const float3 f = fresnelSchlick(max(dot(halfway, view), 0.0f), f0);

    float ndf = distributionGGX(normal, halfway, roughness);
    float g = geometrySmith(normal, view, light, roughness);

    const float3 numerator = ndf * g * f;
    float denominator = 4.0f * max(dot(normal, view), 0.0f) * max(dot(normal, light), 0.0f);
    const float3 specular = numerator / max(denominator, 0.001f);

    float nDotL = max(dot(normal, light), 0.0f);
    const float3 kd = (float3(1.0f, 1.0f, 1.0f) - f) * (1.0f - metallic);
    const float3 lightColor = (kd * albedo / HLSL_PI + specular) * 3.0f * lightBaseColor * nDotL;

    return lightColor;
}

/**
 * Calculate final light for a given surface-light combination.
 * @param normal World-space normal of the surface.
 * @param view World-space view vector from the eye to the surface.
 * @param light World-space vector from the surface to the light.
 * @param metallicRoughness Metallicity and roughness of the surface.
 * @param albedo Albedo color of the surface.
 * @param lightBaseColor Color of the reflected light.
 */
float3 reflectColorGGX(float3 normal, float3 view, float3 light,
    in float2 metallicRoughness, in float3 albedo, in float3 lightBaseColor)
{
    const float3 halfway = normalize(view + light);
    const float metallic = metallicRoughness.x;
    const float roughness = metallicRoughness.y;

    float3 f0 = float3(0.04f, 0.04f, 0.04f);
    f0 = lerp(f0, albedo, metallic);
    const float3 f = fresnelSchlick(max(dot(halfway, view), 0.0f), f0);

    float ndf = distributionGGX(normal, halfway, roughness);
    float g = 1.0f - geometrySmith(normal, view, light, roughness);

    const float3 numerator = ndf * g * f;
    float denominator = 4.0f * max(dot(normal, view), 0.0f) * max(dot(normal, light), 0.0f);
    const float3 refl = numerator / max(denominator, 0.001f);

    //float nDotL = max(dot(normal, light), 0.0f);
    const float3 lightColor = clamp(refl, 0.25f, 0.35f) * lightBaseColor /* nDotL*/;

    return lightColor;
}

/**
 * Calculate the final color for a pixel.
 * @param viewDirection Direction of view ray from camera to the 
 * point.
 * @param wsNormal Normal in world-space.
 * @param pbrMetallicity Metalness of the material.
 * @param pbrRoughness Roughness of the material.
 * @param baseColor Base color - diffuse/albedo - of the material.
 * @param ambientOcclusion How occluded is the position - 1.0 for 
 * maximal occlusion.
 * @param shadowOcclusion How much shadow is on the position - 1.0
 * for maximal shadow.
 * @param sunDirection Direction of the main directional light.
 * @param sunColor color of the main directional light.
 * @param reflectedColor Color seen as the reflection.
 * @return Returns the final color.
 */
float4 pbrHitColor(in float3 viewDirection, in float3 wsNormal, 
    in float pbrMetallicity, in float pbrRoughness, in float4 baseColor, 
    in float ambientOcclusion, in float shadowOcclusion, 
    in float3 sunDirection, in float3 sunColor, 
    in float4 reflectedColor)
{
    // https://learnopengl.com/PBR/Lighting

    const float3 normal = wsNormal;
    const float3 view = -viewDirection;
    const float3 reflectedView = -reflect(view, normal);
    const float3 light = -normalize(sunDirection);
    const float2 metallicRoughness = float2(pbrMetallicity, pbrRoughness);
    const float3 albedo = baseColor.rgb;
    const float3 lightBaseColor = sunColor;

    const float3 lightColor = specularColorGGX(normal, view, light, metallicRoughness, albedo, lightBaseColor);

    const float occlusion = ambientOcclusion;
    const float shadowed = shadowOcclusion;

    float3 color = float3(0.3f, 0.3f, 0.3f) * albedo * (1.0f - occlusion) + 
        lightColor * (1.0f - shadowed) * (1.0f - occlusion);

    if (length(reflectedColor) > HLSL_EPS)
    {
        const float3 reflectionColor = reflectColorGGX(normal, view, reflectedView, metallicRoughness, albedo, reflectedColor.rgb);

        color += reflectionColor;
    }
    else
    {
        float ambientFactor = 0.05f;
        color += lightBaseColor * ambientFactor;
    }

    //color = color / (color + float3(1.0f, 1.0f, 1.0f));
    //color = pow(color, float3(1.0f / 3.2f, 1.0f / 3.2f, 1.0f / 3.2f));

    return float4(color, 1.0f);
}

/**
 * Calculate hemisphere direction, using fibonacci spiral.
 * Source: Petr Smilek
 * @param index Index of the current point.
 * @param totalSamples Total number of sampled points.
 * @param rotOffset Offset of the spiral arms.
 * @return Returns direction on the positive z-axis oriented hemisphere.
 */
float3 hemisphereDirFibonacci(in float index, in float totalSamples, in float rotOffset)
{
    const float offset = 1.0f / totalSamples;
    const float z = 1.0f - (offset * (index + 0.5f));
    const float r = sqrt(1.0f - z * z);
    const float phi = (index + 1.0f) * (HLSL_PI * (3.0f - sqrt(5.0f))) + rotOffset;

    const float2 sc = float2(sin(phi), cos(phi));
    //sincos(phi, sc.x, sc.y);

    return float3(sc.y * r, sc.x * r, z);
}

float3 hemisphereDirFibonacci2(in float index, in float cone, in float totalSamples, in float rotOffset)
{
    const float offset = 1.0f / totalSamples;
    const float z = 1.0f - (offset * (index + 0.5f)) * cone;
    const float r = sqrt(1.0f - z * z);
    const float phi = (index + 1.0f) * (HLSL_PI * (3.0f - sqrt(5.0f))) + rotOffset;

    const float2 sc = float2(sin(phi), cos(phi));
    //sincos(phi, sc.x, sc.y);

    return float3(sc.y * r, sc.x * r, z);
}

#endif // HLSL

#endif // ENGINE_HLSL_LIGHTING_H
