/**
 * @file helpers/hlsl/HlslCompatibility.h
 * @author Tomas Polasek
 * @brief Helper for cross-compatibility between HLSL and C++.
 */

#ifndef ENGINE_HLSL_COMPATIBILITY_H
#define ENGINE_HLSL_COMPATIBILITY_H

#ifndef HLSL

#include "engine/util/Math.h"
#include "engine/lib/dxtk.h"

/// Namespace containing the engine code.
namespace quark
{

/// Namespace containing helper classes and methods.
namespace helpers
{

/// Compatibility with HLSL programing language.
namespace hlsl
{

/// Type translation for C++.
namespace types
{

/// Type translation helper template.
template <typename SrcT>
struct translate_type
{ };

/// float -> float
template <>
struct translate_type<float>
{ using type = float; };

/// float2 -> Vector2
struct float2 { };
template <>
struct translate_type<float2>
{ using type = ::quark::dxtk::math::Vector2; };

/// float3 -> Vector3
struct float3 { };
template <>
struct translate_type<float3>
{ using type = ::quark::dxtk::math::Vector3; };

/// float4 -> Vector4
struct float4 { };
template <>
struct translate_type<float4>
{ using type = ::quark::dxtk::math::Vector4; };

/// float4x4 -> Matrix
struct float4x4 { };
template <>
struct translate_type<float4x4>
{ using type = ::quark::dxtk::math::Matrix; };

/// uint -> uint32_t
struct uint { };
struct alignas(sizeof(float) * 4u) AlignedUint
{
    AlignedUint &operator=(const AlignedUint &other)
    { value = other.value; return *this; }
    AlignedUint &operator=(uint32_t other)
    { value = other; return *this; }
    operator uint32_t() const
    { return value; }
    uint32_t value;
};
template <>
struct translate_type<uint>
{ using type = uint32_t; };

/// uint2 -> UIntVector2
struct uint2 { };
struct UIntVector2 { uint32_t x; uint32_t y; };
template <>
struct translate_type<uint2>
{ using type = UIntVector2; };

/// uint3 -> UIntVector3
struct uint3 { };
struct UIntVector3 { uint32_t x; uint32_t y; uint32_t z; };
template <>
struct translate_type<uint3>
{ using type = UIntVector3; };

/// uint4 -> UIntVector4
struct uint4 { };
struct UIntVector4 { uint32_t x; uint32_t y; uint32_t z; uint32_t w; };
template <>
struct translate_type<uint4>
{ using type = UIntVector4; };

/// Barycentric triangle coordinates -> BarycentricCoordinates
struct BuiltInTriangleIntersectionAttributes { };
struct BarycentricCoordinates { float x; float y; };
template <>
struct translate_type<BuiltInTriangleIntersectionAttributes>
{ using type = BarycentricCoordinates; };

/// ByteAddressBuffer -> Descriptor handle to the buffer
struct ByteAddressBuffer { };
template <>
struct translate_type<ByteAddressBuffer>
{ using type = ::CD3DX12_GPU_DESCRIPTOR_HANDLE; };

/// Translate a type from HLSL->C++.
template <typename T>
using translate_type_t = typename translate_type<T>::type;

// Some basic checks...
static_assert(sizeof(translate_type_t<uint4>) == sizeof(translate_type_t<float4>));
static_assert(alignof(translate_type_t<uint4>) == alignof(translate_type_t<float4>));

} // namespace types

} // namespace hlsl

} // namespace helpers

} // namespace quark

#endif

#ifdef HLSL
/// Helper macro to translate HLSL types for C++.
#   define HLSL_TYPE(X) X
/// Helper macro to translate HLSL types for C++.
#   define HLSL_STRUCTURED_TYPE(X) X

/// Upper bound for the number of textures, so we can debug shaders.
#   define HLSL_TEXTURE_UPPER_BOUND 128
/// Upper bound for the number of buffers, so we can debug shaders.
#   define HLSL_BUFFER_UPPER_BOUND 128
/// Upper bound for the number of instances, so we can debug shaders.
#   define HLSL_INSTANCE_UPPER_BOUND 128

/// Upper bound for the number of shadow maps.
#   define HLSL_SHADOW_MAP_UPPER_BOUND 8

/// The number PI.
#	define HLSL_PI 3.14159265359f
#	define HLSL_2PI 2.0f * HLSL_PI

/// Small number, still higher than zero
#	define HLSL_EPS 0.00001f

/// Golden ratio.
#	define HLSL_PHI 1.61803398875f

/// Maximum float value.
#   define HLSL_FLT_MAX 1.#INF
/// Minimum float value.
#   define HLSL_FLT_MIN -1.#INF
#else
/// Helper macro to translate HLSL types for C++.
#   define HLSL_TYPE(X) quark::helpers::hlsl::types::translate_type_t<quark::helpers::hlsl::types::X>
/// Helper macro to translate HLSL types for C++.
#   define HLSL_STRUCTURED_TYPE(X) quark::helpers::hlsl::structures::X

/// Upper bound for the number of textures, so we can debug shaders.
#   define HLSL_TEXTURE_UPPER_BOUND 128
/// Upper bound for the number of buffers, so we can debug shaders.
#   define HLSL_BUFFER_UPPER_BOUND 128
/// Upper bound for the number of instances, so we can debug shaders.
#   define HLSL_INSTANCE_UPPER_BOUND 128

/// Upper bound for the number of shadow maps.
#   define HLSL_SHADOW_MAP_UPPER_BOUND 8

/// Default number of textures, before the real amount is calculated.
#   define HLSL_DEFAULT_TEXTURE_COUNT 2048
/// Default number of buffers, before the real amount is calculated.
#   define HLSL_DEFAULT_BUFFER_COUNT 8192
/// Default number of instances, before the real amount is calculated.
#   define HLSL_DEFAULT_INSTANCE_COUNT 2048

#endif

#ifndef HLSL
/// Namespace containing the engine code.
namespace quark
{

/// Namespace containing helper classes and methods.
namespace helpers
{

/// Compatibility with HLSL programing language.
namespace hlsl
{

/// Type translation for C++.
namespace structures
{
#endif

/// Invalid index to the texture array, which should not be used.
#define HLSL_INVALID_TEXTURE_INDEX 0xffffffff

/// Description of a single material and the textures it uses.
struct MaterialSpecification
{
    /// Index of the texture containing color information.
    HLSL_TYPE(uint) colorTextureIndex;

    /// Index of the texture containing metallicity and roughness.
    HLSL_TYPE(uint) metallicRoughnessTextureIndex;

    /// Index of the texture containing normals.
    HLSL_TYPE(uint) normalTextureIndex;

    /// Does the material create specular reflections?
    HLSL_TYPE(uint) specularReflections;
    /// Does the material create "specular" refractions?
    HLSL_TYPE(uint) specularRefractions;

    // TODO - Add flag for choosing material type, occlussion texture, emissive factor/texture, alpha props, ... (from the material desc)
}; // struct MaterialSpecification

/// Invalid index to the buffer array, which should not be used.
#define HLSL_INVALID_BUFFER_INDEX 0xffffffff

/// Buffers required to describe a single mesh.
struct MeshSpecification
{
    /// Buffer containing indices - uint32.
    HLSL_TYPE(uint) indices;
    /// Buffer containing positions - float3.
    HLSL_TYPE(uint) positions;
    /// Buffer containing texture coordinates - float2.
    HLSL_TYPE(uint) texCoords;
    /// Buffer containing normals - float3.
    HLSL_TYPE(uint) normals;
    /// Buffer containing tangents - float4.
    HLSL_TYPE(uint) tangents;

    HLSL_TYPE(uint) padding1;
    HLSL_TYPE(uint) padding2;
    HLSL_TYPE(uint) padding3;
}; // struct MeshSpecification

#ifdef HLSL
#   include "HlslCommon.h"
#   include "HlslLighting.h"
#else // HLSL
#   include "engine/helpers/hlsl/HlslCommon.h"
#   include "engine/helpers/hlsl/HlslLighting.h"
#endif // HLSL

#ifndef HLSL
} // namespace structures

} // namespace hlsl

} // namespace helpers

} // namespace quark
#endif

#endif // ENGINE_HLSL_COMPATIBILITY_H
