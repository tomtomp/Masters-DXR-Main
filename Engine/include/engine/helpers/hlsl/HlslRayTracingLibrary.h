/**
 * @file helpers/hlsl/HlslRayTracingLibrary.h
 * @author Tomas Polasek
 * @brief Shader function library used for ray tracing.
 */

#ifndef ENGINE_HLSL_RAY_TRACING_LIBRARY_H
#define ENGINE_HLSL_RAY_TRACING_LIBRARY_H

/// Register space used by ray tracing attributes.
#define HLSL_REGISTER_SPACE 99

#ifndef HLSL

/// Namespace containing the engine code.
namespace quark
{

/// Namespace containing helper classes and methods.
namespace helpers
{

/// Compatibility with HLSL programing language.
namespace hlsl
{

/// Ray tracing attribute indices.
namespace raytracing
{



} // namespace raytracing

} // namespace hlsl

} // namespace helpers

} // namespace quark

#else
#	include "HlslCompatibility.h"

/// Constructed scene acceleration structure.
RaytracingAccelerationStructure gScene : register(t0, space0);

/**
 * Textures used in rendering, contains all of the material 
 * textures as well.
 * Upper bound is set to allow debugging and validation.
 */
Texture2D gTextures[HLSL_TEXTURE_UPPER_BOUND] : register(t1, space0);

// TODO - Array of samplers, specified in the material?
/// Common sampler used for sampling the textures.
SamplerState gCommonSampler : register(s0, space0);

/**
 * Buffers containing information about the geometry.
 */
ByteAddressBuffer gBuffers[HLSL_BUFFER_UPPER_BOUND] : register(t0, space1);

/**
 * Specification of material and mesh of the hit geometry, only available 
 * from hit group shaders.
 */
ConstantBuffer<GeometryConstantBuffer> hgGeometry : register(b0, space1);

#endif

#endif // ENGINE_HLSL_RAY_TRACING_LIBRARY_H
