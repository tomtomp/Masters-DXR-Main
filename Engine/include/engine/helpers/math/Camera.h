/**
 * @file helpers/math/Camera.h
 * @author Tomas Polasek
 * @brief Helper class containing camera 
 * parameters. Can be used to generate 
 * view and projection matrices.
 */

#pragma once

#include "engine/lib/dxtk.h"

#include "engine/helpers/math/Transform.h"

/// Namespace containing the engine code.
namespace quark
{

/// Namespace containing helper classes and methods.
namespace helpers
{

/// Wrappers for mathematical calculations.
namespace math
{

/**
 * Helper class containing camera parameters. 
 * Can be used to generate view and projection 
 * matrices.
 */
class Camera
{
public:
    /**
     * Initialize default projection, which is 
     * a identity matrix.
     */
    Camera() = default;

    /**
     * Set camera projection matrix specifying an orthographic 
     * projection with provided parameters.
     * @param width Width of the viewport.
     * @param height Height of the viewport.
     * @param zNear Near clipping plane.
     * @param zFar Far clipping plane, must be larger 
     * than near clipping plane!
     * @return Returns reference to this.
     */
    Camera &setOrthoProj(float width, float height, float zNear, float zFar);

    /**
     * Set camera projection matrix specifying an orthographic 
     * projection with provided parameters.
     * @param xMag Horizontal magnification of the view.
     * @param yMag Vertical magnification of the view.
     * @param zNear Near clipping plane.
     * @param zFar Far clipping plane, must be larger 
     * than near clipping plane!
     * @return Returns reference to this.
     */
    Camera &setHalfOrthoProj(float xMag, float yMag, float zNear, float zFar);

    /**
     * Set camera projection matrix specifying an perspective 
     * projection with provided parameters.
     * @param aspectRatio Aspect ratio of the viewport - height/width.
     * @param horFov Horizontal FOV angle, full value not 
     * divided by 2.
     * @param zNear Near clipping plane.
     * @param zFar Far clipping plane, must be larger 
     * than near clipping plane!
     * @return Returns reference to this.
     */
    Camera &setFinPerspProj(float aspectRatio, float horFov, float zNear, float zFar);

    /**
     * Set camera projection matrix specifying an perspective 
     * projection with provided parameters.
     * This version creates infinite perspective projection.
     * @param aspectRatio Aspect ratio of the viewport - height/width.
     * @param horFov Horizontal FOV angle, full value not 
     * divided by 2.
     * @param zNear Near clipping plane. Far clipping plane 
     * is infinite.
     * @return Returns reference to this.
     */
    Camera &setInfinPerspProj(float aspectRatio, float horFov, float zNear);

    /**
     * Compute camera view matrix from provided transformation.
     * Only translation and rotation are used!
     * @param transform New transform of the camera.
     * @return Returns reference to this.
     */
    Camera &setViewTransform(const Transform &transform);

    /**
     * Setup transform for camera looking at given point.
     * @param pos Position of the camera.
     * @param lookAt What point should the camera look at.
     * @param up The up vector.
     */
    Camera &setLookAtTransform(
        const dxtk::math::Vector3 &pos, 
        const dxtk::math::Vector3 &lookAt, 
        const dxtk::math::Vector3 &up);

    /// Get the view matrix.
    const dxtk::math::Matrix &v() const;
    /// Get the projection matrix.
    const dxtk::math::Matrix &p() const;
    /**
     * Get the view-projection matrix. The value will 
     * be using the current values, but will not be cached 
     * for later reuse - matrix will stay dirty.
     */
    dxtk::math::Matrix vpNoRecalculation() const;
    /// Get the view-projection matrix, recalculating if necessary.
    const dxtk::math::Matrix &vp();

    /**
     * Calculate and return View-Projection matrix used 
     * in ray tracing.
     * The output is inverse of vp() matrix, which has 
     * been additionally transposed.
     * @warning If the camera is dirty, the new values will 
     * NOT be written to the cache!
     */
    dxtk::math::Matrix rayTracingVP() const;

    /// Get current position of the camera in world-space.
    const dxtk::math::Vector3 &position() const;

    /**
     * Recalculate result matrix if dirty and 
     * set the dirty flag to false.
     * @return Returns true if recalculation was 
     * necessary.
     */
    bool recalculateIfDirty();
private:
    /// Calculate view-projection matrix from current values.
    dxtk::math::Matrix calculateViewProjection() const;

    /// View matrix;
    dxtk::math::Matrix mView{ };
    /// Projection matrix.
    dxtk::math::Matrix mProjection{ };
    /// Combined view and projection matrices.
    dxtk::math::Matrix mViewProjection{ };

    /// Current position of the camera.
    dxtk::math::Vector3 mPosition{ };

    /// Has there been changes made to this transform?
    bool mDirty{ false };
protected:
}; // class Camera

} // namespace math

} // namespace helpers

} // namespace quark

// template implementation

namespace quark
{

namespace helpers
{

namespace math
{

inline Camera &Camera::setOrthoProj(float width, float height, float zNear, float zFar)
{
    mProjection.CreateOrthographic(2.0f * width, 2.0f * height, zNear, zFar);

    mDirty = true;

    return *this;
}

inline Camera &Camera::setHalfOrthoProj(float xMag, float yMag, float zNear, float zFar)
{ return setOrthoProj(2.0f * xMag, 2.0f * yMag, zNear, zFar); }

inline Camera &Camera::setFinPerspProj(float aspectRatio, float horFov, float zNear, float zFar)
{
    //mProjection.CreatePerspectiveFieldOfView(horFov, aspectRatio, zNear, zFar);
    DirectX::XMStoreFloat4x4(&mProjection, DirectX::XMMatrixPerspectiveFovLH(horFov, 1.0f / aspectRatio, zNear, zFar));

    //mProjection = DirectX::XMMatrixPerspectiveFovLH(DirectX::XMConvertToRadians(45.0f), 1024.0f / 768.0f, 1.0f, 125.0f);
    //mProjection = DirectX::XMMatrixPerspectiveFovLH(DirectX::XMConvertToRadians(45.0f), 1024.0f / 768.0f, 1.0f, 125.0f);

    mDirty = true;

    return *this;
}

inline Camera &Camera::setInfinPerspProj(float aspectRatio, float horFov, float zNear)
{
    // Taken from DirectX math sources.
    float fovSin;
    float fovCos;
    DirectX::XMScalarSinCos(&fovSin, &fovCos, 0.5f * horFov);
    const float height{ fovCos / fovSin };
    const float width{ height / aspectRatio };

    mProjection = dxtk::math::Matrix::Identity;
    mProjection.m[0][0] = width;
    mProjection.m[1][1] = height;
    mProjection.m[2][2] = -1.0f;
    mProjection.m[2][3] = -1.0f;
    mProjection.m[3][2] = -1.0f * zNear;

    mDirty = true;

    return *this;
}

inline Camera &Camera::setViewTransform(const Transform &transform)
{
    mView = dxtk::math::Matrix::CreateTranslation(transform.translate()) *
            dxtk::math::Matrix::CreateFromQuaternion(transform.rotate()) * 
            dxtk::math::Matrix::CreateScale(transform.scale());
    mPosition = -transform.translate();

    mDirty = true;

    return *this;
}

inline Camera &Camera::setLookAtTransform(
    const dxtk::math::Vector3 &pos, 
    const dxtk::math::Vector3 &lookAt,
    const dxtk::math::Vector3 &up)
{
    mView = DirectX::XMMatrixLookAtLH(pos, lookAt, up);
    mPosition = pos;

    mDirty = true;

    return *this;
}

inline bool Camera::recalculateIfDirty()
{
    if (mDirty)
    {
        mViewProjection = calculateViewProjection();
        mDirty = false;
        return true;
    }

    return false;
}

inline dxtk::math::Matrix Camera::calculateViewProjection() const
{ return mView * mProjection; }

inline const dxtk::math::Matrix &Camera::v() const
{ return mView; }

inline const dxtk::math::Matrix &Camera::p() const
{ return mProjection; }

inline dxtk::math::Matrix Camera::vpNoRecalculation() const
{ return mDirty ? calculateViewProjection() : mViewProjection; }

inline const dxtk::math::Matrix &Camera::vp() 
{
    recalculateIfDirty();
    return mViewProjection;
}

inline dxtk::math::Matrix Camera::rayTracingVP() const
{
    // TODO - Cache?
    auto result{ vpNoRecalculation().Invert().Transpose() };

    return result;
}

inline const dxtk::math::Vector3 &Camera::position() const
{ return mPosition; }

}

}

}

// template implementation end
