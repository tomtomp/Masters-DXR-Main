/**
 * @file helpers/math/Transform.h
 * @author Tomas Polasek
 * @brief Helper class used for holding and 
 * computing transformations - i.e. model matrices.
 */

#pragma once

#include "engine/lib/dxtk.h"

/// Namespace containing the engine code.
namespace quark
{

/// Namespace containing helper classes and methods.
namespace helpers
{

/// Wrappers for mathematical calculations.
namespace math
{

class Transform
{
public:
    /**
     * Initialize default transformation resulting 
     * in unit matrix.
     */
    Transform() = default;

    /// Set 3D translation.
    void setTranslate(const dxtk::math::Vector3 &t);
    /// Set 3D translation.
    void setTranslate(float x, float y, float z);
    /// Translate from current position by given values.
    void translateBy(float x, float y, float z);
    /// Get current translation.
    const dxtk::math::Vector3 &translate() const;

    /// Set rotation quaternion.
    void setRotate(const dxtk::math::Quaternion &r);
    /// Set rotation quaternion.
    void setRotate(float x, float y, float z, float w);
    /// Set rotation from Euler angles.
    void setEuler(float x, float y, float z);
    /// Set Euler rotation angles from right-handed coordinates
    void setEulerRh(float x, float y, float z);
    /// Yaw, pitch and roll from current rotation by given degrees.
    void yawPitchRollBy(float yaw, float pitch, float roll);
    /// Get rotation quaternion.
    const dxtk::math::Quaternion &rotate() const;

    /// Set 3D scale.
    void setScale(const dxtk::math::Vector3 &s);
    /// Set 3D scale.
    void setScale(float x, float y, float z);
    /// Get current scale.
    const dxtk::math::Vector3 &scale() const;

    /// Set transform matrix, resetting the dirty flag.
    void setMatrix(const dxtk::math::Matrix &m);
    /// Set transform matrix, resetting the dirty flag.
    void setMatrix(
        float v00, float v01, float v02, float v03, 
        float v10, float v11, float v12, float v13, 
        float v20, float v21, float v22, float v23, 
        float m30, float v31, float v32, float v33);
    /// Get transform matrix, without caching recalculated value.
    dxtk::math::Matrix matrixNoRecalculation() const;
    /// Get transform matrix, performing lazy recalculation.
    const dxtk::math::Matrix &matrix();

    /**
     * Calculate and return a matrix used to transform 
     * normals. It is calculated by taking the matrix() 
     * and calculating an inverse-transpose from it.
     * @return Returns matrix usable in normal transformations.
     */
    dxtk::math::Matrix normalMatrix();

    /**
     * Recalculate result matrix if dirty and 
     * set the dirty flag to false.
     * @return Returns true if recalculation was 
     * necessary.
     */
    bool recalculateIfDirty();

    /**
     * Perform row-major copy of the recalculated 
     * matrix into destination buffer. Only 3 rows 
     * will be copied by this operation.
     * @param dest Destination of the copy command. 
     * There should be at least 4*3*sizeof(float) 
     * bytes of space in the buffer!
     */
    void rowMajorCopy4x3(float *dest);

    /**
     * Perform row-major copy of the current 
     * matrix into destination buffer. Only 3 rows 
     * will be copied by this operation.
     * @param dest Destination of the copy command. 
     * There should be at least 4*3*sizeof(float) 
     * bytes of space in the buffer!
     * @warning This version does NOT recompute 
     * the matrix!
     */
    void rowMajorCopy4x3(float *dest) const;
private:
    /// Recalculate the transform matrix with current values.
    dxtk::math::Matrix recalculateMatrix() const;

    /// Translation in 3D space.
    dxtk::math::Vector3 mTranslate{ 0.0f, 0.0f, 0.0f };
    /// Rotation quaternion.
    dxtk::math::Quaternion mRotate{ 0.0f, 0.0f, 0.0f, 1.0f };
    /// Scaling in 3D space.
    dxtk::math::Vector3 mScale{ 1.0f, 1.0f, 1.0f };

    /// Resulting transform matrix.
    dxtk::math::Matrix mMatrix{ };

    /// Has there been changes made to this transform?
    bool mDirty{ false };
protected:
}; // class Transform

} // namespace math

} // namespace helpers

} // namespace quark

// template implementation

namespace quark
{

namespace helpers
{

namespace math
{

inline void Transform::setTranslate(const dxtk::math::Vector3 &t)
{
    mTranslate = t;
    mDirty = true;
}

inline void Transform::setTranslate(float x, float y, float z)
{ setTranslate({ x, y, z }); }

inline void Transform::translateBy(float x, float y, float z)
{ setTranslate(mTranslate + dxtk::math::Vector3(x, y, z)); }

inline const dxtk::math::Vector3 &Transform::translate() const
{ return mTranslate; }

inline void Transform::setRotate(const dxtk::math::Quaternion &r)
{
    mRotate = r;
    mDirty = true;
}

inline void Transform::setRotate(float x, float y, float z, float w)
{ setRotate({ x, y, z, w }); }

inline void Transform::setEuler(float x, float y, float z)
{
    setRotate(dxtk::math::Quaternion::CreateFromYawPitchRoll(y, x, z));
}

inline void Transform::setEulerRh(float x, float y, float z)
{
    const auto xRot{ dxtk::math::Matrix::CreateFromAxisAngle({ 1.0f, 0.0f, 0.0f }, x) };
    const auto yRot{ dxtk::math::Matrix::CreateFromAxisAngle({ 0.0f, 1.0f, 0.0f }, y) };
    const auto zRot{ dxtk::math::Matrix::CreateFromAxisAngle({ 0.0f, 0.0f, 1.0f }, z) };

    setRotate(dxtk::math::Quaternion::CreateFromRotationMatrix(yRot * zRot * xRot));
}

inline void Transform::yawPitchRollBy(float yaw, float pitch, float roll)
{ setRotate(mRotate * dxtk::math::Quaternion::CreateFromYawPitchRoll(yaw, pitch, roll)); }

inline const dxtk::math::Quaternion &Transform::rotate() const
{ return mRotate; }

inline void Transform::setScale(const dxtk::math::Vector3 &s)
{
    mScale = s;
    mDirty = true;
}

inline void Transform::setScale(float x, float y, float z)
{ setScale({ x, y, z }); }

inline const dxtk::math::Vector3 &Transform::scale() const
{ return mScale; }

inline void Transform::setMatrix(const dxtk::math::Matrix &m)
{
    mMatrix = m;
    mDirty = false;
}

inline void Transform::setMatrix(
    float v00, float v01, float v02, float v03, 
    float v10, float v11, float v12, float v13,
    float v20, float v21, float v22, float v23, 
    float m30, float v31, float v32, float v33)
{
    setMatrix({ 
        v00, v01, v02, v03, 
        v10, v11, v12, v13, 
        v20, v21, v22, v23, 
        m30, v31, v32, v33 });
}

inline dxtk::math::Matrix Transform::matrixNoRecalculation() const
{ 
    if (mDirty)
    { return recalculateMatrix(); }
    else
    { return mMatrix; }
}

inline const dxtk::math::Matrix &Transform::matrix() 
{
    recalculateIfDirty();
    return mMatrix;
}

inline dxtk::math::Matrix Transform::normalMatrix()
{ return matrix().Invert().Transpose(); }

inline bool Transform::recalculateIfDirty()
{
    if (mDirty)
    {
        // Transform = Scale * Rotation * Translation
        mMatrix = recalculateMatrix();
        mDirty = false;

        return true;
    }

    return false;
}

inline void Transform::rowMajorCopy4x3(float *dest)
{
    recalculateIfDirty();
    const_cast<const Transform*>(this)->rowMajorCopy4x3(dest);
}

inline void Transform::rowMajorCopy4x3(float *dest) const
{ 
    const auto rowMajorTransform{ matrixNoRecalculation().Transpose() };
    std::memcpy(dest, &rowMajorTransform, 4u * 3u * sizeof(float)); 
}

inline dxtk::math::Matrix Transform::recalculateMatrix() const
{
    return dxtk::math::Matrix::CreateScale(mScale) * 
        dxtk::math::Matrix::CreateFromQuaternion(mRotate) * 
        dxtk::math::Matrix::CreateTranslation(mTranslate);
}

}

}

}

// template implementation end
