/**
 * @file gui/GuiSubSystem.h
 * @author Tomas Polasek
 * @brief Graphical user interface sub-system 
 * based on ImGui.
 */

#pragma once

// Engine configuration: 
#include "EngineRuntimeConfig.h"

#include "engine/gui/VariableMap.h"
#include "engine/gui/GuiModel.h"

#include "engine/application/Events.h"
#include "engine/application/FpsUpdateTimer.h"

/// Namespace containing the engine code.
namespace quark
{

/// Gui classes.
namespace gui
{

/// Graphical user interface sub-system based on ImGui.
class GuiSubSystem : public util::PointerType<GuiSubSystem>
{
public:
    /**
     * Initialize GUI.
     * @param cfg Runtime configuration.
     */
    static PtrT create(const EngineRuntimeConfig &cfg);

    /// Clean-up GUI related resources.
    ~GuiSubSystem();

    // No copying or moving.
    GuiSubSystem(const GuiSubSystem &other) = delete;
    GuiSubSystem &operator=(const GuiSubSystem &other) = delete;
    GuiSubSystem(GuiSubSystem &&other) = delete;
    GuiSubSystem &operator=(GuiSubSystem &&other) = delete;

    /// Process provided keyboard event, returning whether it was captured.
    bool processEvent(const app::events::KeyboardEvent &event);
    /// Process provided mouse button event, returning whether it was captured.
    bool processEvent(const app::events::MouseButtonEvent &event);
    /// Process provided mouse move event, returning whether it was captured.
    bool processEvent(const app::events::MouseMoveEvent &event);
    /// Process provided mouse scroll event, returning whether it was captured.
    bool processEvent(const app::events::MouseScrollEvent &event);
    /// Process provided resize event, never captures.
    bool processEvent(const app::events::ResizeEvent &event);
    /// Process provided exit event, never captures.
    bool processEvent(const app::events::ExitEvent &event);

    /**
     * Update the GUI.
     * Called automatically from the processEvents method in 
     * the application.
     * @param delta Delta time between updates.
     */
    void update(util::HrTimer::MillisecondsF delta);

    /// Is GUI interaction enabled?
    bool enabled() const;
    /// Enable/disable the GUI and its rendering.
    void setEnabled(bool enabled);

    /// Is GUI capturing user inputs?
    bool captureInput() const;
    /// Should user input on GUI be captured?
    void setCaptureInput(bool enabled);

    /**
     * Add or get already existing GUI context.
     * @param name Unique name of the context - e.g. "/", 
     * "/MyVariables/".
     * @param useAsRoot When set to true, this context will be 
     * used for all other contexts created after it.
     * @return Returns reference to the context configurator.
     */
    GuiContext &addGuiContext(const std::string &name, bool useAsRoot = false);

    /// Is there a default context set?
    bool hasRootContext() const;

    /**
     * Add or get already existing GUI variable.
     * @tparam VarT Type of the variable.
     * @param name Unique name of the variable.
     * @param context Location of the variable in the 
     * hierarchy. Must be the identifier of already existing 
     * context. Examples: "/", "/MyVariables/", ...
     * @param initialValue Initial value of the created variable.
     * @return Returns reference to the variable configurator.
     */
    template <typename VarT>
    GuiVariable<VarT> &addGuiVariable(const std::string &name, 
        const std::string &context, const VarT &initialValue);

    /**
     * Add or get already existing variable.
     * @tparam VarT Type of the variable.
     * @param name Unique name of the variable.
     * @param initialValue Initial value of the created variable.
     * @return Returns reference to the value, which does 
     * not change memory position between calls.
     */
    template <typename VarT>
    VarT &addVariable(const std::string &name, const VarT &initialValue);

    /**
     * Get value of created variable.
     * @tparam VarT Type of the variable.
     * @param name Unique name of the variable.
     * @return Returns reference to the value, which does 
     * not change memory position between calls.
     */
    template <typename VarT>
    VarT &getVariable(const std::string &name);

    /**
     * Get value of created variable.
     * @tparam VarT Type of the variable.
     * @param name Unique name of the variable.
     * @return Returns reference to the value, which does 
     * not change memory position between calls.
     */
    template <typename VarT>
    const VarT &getVariable(const std::string &name) const;

    /// Access the model, to allow its rendering.
    const GuiModel &model() const;
private:
    /// Compile-time configuration.
    using Config = EngineConfig;

    /// Initialize the GUI sub-system.
    void initialize();

    /// Destruct the GUI sub-system.
    void destruct();

    /// Should the GUI be displayed?
    bool mEnabled{ true };
    /// Should the GUI capture user input?
    bool mCaptureInput{ true };

    /// Get context name prefixed with the root context.
    std::string rootPrefixedContext(const std::string &contextName);

    /// Container for all GUI variables.
    VariableMap mVariableMap{ };
    /// Model of the current GUI configuration.
    GuiModel mGuiModel{ };

    /// Context name used as a root.
    std::string mRootContext{ GuiModel::ROOT_CONTEXT };
protected:
    /**
     * Initialize GUI.
     * @param cfg Runtime configuration.
     */
    GuiSubSystem(const EngineRuntimeConfig &cfg);
}; // class GuiSubSystem

} // namespace gui

} // namespace quark

// Template implementation.

namespace quark
{

namespace gui
{

template <typename VarT>
GuiVariable<VarT> &GuiSubSystem::addGuiVariable(const std::string &name, 
    const std::string &context, const VarT &initialValue)
{
    auto &varRef{ mVariableMap.getCreateVar<VarT>(name, initialValue) };
    return mGuiModel.addVariable<VarT>(name, varRef, rootPrefixedContext(context));
}

template <typename VarT>
VarT &GuiSubSystem::addVariable(const std::string &name, 
    const VarT &initialValue)
{ return mVariableMap.getCreateVar<VarT>(name, initialValue); }

template <typename VarT>
VarT &GuiSubSystem::getVariable(const std::string &name)
{ return mVariableMap.getVar<VarT>(name); }

template <typename VarT>
const VarT &GuiSubSystem::getVariable(const std::string &name) const
{ return mVariableMap.getVar<VarT>(name); }

}

}

// Template implementation end.