/**
 * @file gui/GuiModel.h
 * @author Tomas Polasek
 * @brief Model representing the GUI.
 */

#pragma once

#include "engine/gui/GuiVariableTypes.h"

/// Namespace containing the engine code.
namespace quark
{

/// Gui classes.
namespace gui
{

/// Types of GUI contexts.
enum class GuiContextType
{
    /// Independent window.
    Window,
    /// Window embedded within another window.
    ChildWindow,
    /// Labeled category within the parent window.
    Category
}; // enum class GuiContextType

/// Information about a variable context.
class GuiContext
{
public:
    /// Create context with given name.
    GuiContext(const std::string name);

    /// Free context resources.
    ~GuiContext();

    /// Set type of this context.
    GuiContext &setType(GuiContextType type);

    /// Set label of this context.
    GuiContext &setLabel(const std::string &label);

    /// Get name of this context.
    const std::string &name() const;

    /// Access the context configuration.
    GuiContextConfig &cfg();
private:
    friend struct GuiContextConfig;
    friend class GuiModel;

    /// Begin using this context.
    void startContext();
    /// End using this context.
    void stopContext();

    /// Whole name of this context.
    std::string mName{ };

    /// Label displayed for the context.
    std::string mLabel{ };

    /// Type of the context.
    GuiContextType mType{ GuiContextType::Category };

    /// Special context configuration.
    GuiContextConfig mConfig{ };
protected:
}; // class GuiContext

/**
 * Base for all GUI variables
 */
class GuiVariableBase
{
public:
    /// Create a new variable.
    GuiVariableBase(const std::string &name, const std::string &context);
    /// Free variable resources.
    virtual ~GuiVariableBase();

    /// Compare names and contexts of 2 variables.
    bool operator==(GuiVariableBase &other) const;
    /// Compare names and contexts of 2 variables.
    bool operator<(GuiVariableBase &other) const;

    /// Get the name of this variable.
    const std::string &name() const;

    /// Get the context path for this variable.
    const std::string &context() const;
protected:
    friend class GuiModel;
    /// Perform actions to update or create the variable.
    virtual void updateCreateVariable() = 0;
private:

    /// Name of this variable
    std::string mName{ };

    /// Location of the variable in the hierarchy.
    std::string mContext{ };
protected:
}; // class GuiVariableBase

/// A single variable visible in the GUI.
template <typename VarT>
class GuiVariable : public GuiVariableBase
{
public:
    /// Create a new variable.
    GuiVariable(const std::string &name, VarT &target, const std::string &context);
    /// Free variable resources.
    virtual ~GuiVariable();

    /// Set label of this variable.
    GuiVariable<VarT> &setLabel(const std::string &label);

    /// Access type-specific configuration.
    GuiVariableConfig<VarT> &cfg();
private:
    friend struct GuiVariableConfig<VarT>;
    friend class GuiModel;

    /// Perform actions to update or create the variable.
    virtual void updateCreateVariable() override final;

    /// Target to which the variable is written.
    VarT &mTarget;

    /// Label displayed with this variable.
    std::string mLabel{ };

    /// Configuration specific to this type.
    GuiVariableConfig<VarT> mConfig{ };
protected:
}; // class GuiVariable

/// Model representing the GUI.
class GuiModel
{
public:
    /// Type used for names.
    using NameT = std::string;

    /// Symbol used for division of contexts.
    static constexpr char CONTEXT_DIVIDER{ '/' };
    /// Starting context path.
    static constexpr const char *ROOT_CONTEXT{ "/" };

    /**
     * Exception thrown when context with given name
     * already exists.
     */
    struct ContextNameException : public std::exception
    {
        ContextNameException(const char *msg) :
            std::exception(msg)
        { }
    }; // struct ContextNameException

    /// Create empty model of the GUI.
    GuiModel();

    /// Clean-up GUI related resources.
    ~GuiModel();

    // Allow moving and copying.
    GuiModel(const GuiModel &other) = default;
    GuiModel &operator=(const GuiModel &other) = default;
    GuiModel(GuiModel &&other) = default;
    GuiModel &operator=(GuiModel &&other) = default;

    /**
     * Create a context as a container for variables.
     * @param name Unique name of the context.
     * @return Returns reference to the context configurator.
     * @throws ContextNameException Thrown when given name \
     * is already used.
     */
    GuiContext &addContext(const NameT &name);

    /**
     * Add a new variable visible in the GUI.
     * @param name Name of the variable, visible in the GUI.
     * @param target Target to which the value will be written.
     * @param context Location of the variable in the 
     * hierarchy. Must be the identifier of already existing 
     * context. Examples: "/", "/MyVariables/", ...
     * @param orderedInsert Insert the variable after the last 
     * one with the same context.
     * @return Returns reference to the variable configurator.
     */
    template <typename VarT>
    GuiVariable<VarT> &addVariable(const NameT &name, VarT &target, 
        const NameT &context = "", bool orderedInsert = false);

    /// Update the GUI.
    void updateGui();
private:
    /// Get to the target context state. Returns whether the context is opened.
    bool processContexts(std::stack<GuiContext*> &contexts, const std::string &targetContext) const;

    /// Mapping of context names to their configurations.
    std::map<NameT, util::UniquePtr<GuiContext>> mContexts;
    /// List of all variables, ordered by their context name.
    std::list<util::UniquePtr<GuiVariableBase>> mVariables;
protected:
}; // class GuiModel

} // namespace gui

} // namespace quark

// Template implementation.

namespace quark
{

namespace gui
{

template <typename VarT>
GuiVariable<VarT>::GuiVariable(const std::string &name, VarT &target, const std::string &context) : 
    GuiVariableBase(name, context), 
    mTarget{ target }
{ }

template <typename VarT>
GuiVariable<VarT>::~GuiVariable()
{ /* Automatic */ }

template <typename VarT>
GuiVariable<VarT> &GuiVariable<VarT>::setLabel(const std::string &label)
{ mLabel = label; return *this; }

template <typename VarT>
GuiVariableConfig<VarT> &GuiVariable<VarT>::cfg()
{ return mConfig; }

template <typename VarT>
void GuiVariable<VarT>::updateCreateVariable()
{ mConfig.updateCreateVariable(*this); }

template <typename VarT>
GuiVariable<VarT> &GuiModel::addVariable(const NameT &name, VarT &target, 
    const NameT &context, bool orderedInsert)
{
    if (mContexts.find(context) == mContexts.end())
    { throw ContextNameException("Context name not found. Context has to be added before its variables!"); }

    auto newVariablePtr{ std::make_unique<GuiVariable<VarT>>(name, target, context) };
    auto &newVariable{ *newVariablePtr };

    if (orderedInsert)
    { // Place the new variable into the ordered list.
        mVariables.emplace(std::lower_bound(mVariables.begin(), mVariables.end(), newVariablePtr, [&](const auto &var1, const auto &var2)
        { return *var1 < *var2; }), std::move(newVariablePtr));
    }
    else
    { mVariables.emplace_back(std::move(newVariablePtr)); }

    return newVariable;
}

}

}

// Template implementation end.
