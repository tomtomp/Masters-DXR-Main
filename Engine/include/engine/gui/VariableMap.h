/**
 * @file gui/VariableMap.h
 * @author Tomas Polasek
 * @brief Named variable storage.
 */

#pragma once

#include "engine/util/Types.h"

/// Namespace containing the engine code.
namespace quark
{

/// Gui classes.
namespace gui
{

/// Named variable storage.
class VariableMap 
{
public:
    /// Key used for naming the variables.
    using KeyT = std::string;

    /**
     * Exception thrown when there is a problem with 
     * provided variable type.
     */
    struct VarTypeException : public std::exception
    {
        VarTypeException(const char *msg) :
            std::exception(msg)
        { }
    }; // struct VarTypeException

    /**
     * Exception thrown when requested variable is 
     * not registered yet.
     */
    struct VarExistenceException : public std::exception
    {
        VarExistenceException(const char *msg) :
            std::exception(msg)
        { }
    }; // struct VarExistenceException

    /// Create empty variable map
    VariableMap();

    /// Clean-up GUI related resources.
    ~VariableMap();

    // Allow copying and moving
    VariableMap(const VariableMap &other) = default;
    VariableMap &operator=(const VariableMap &other) = default;
    VariableMap(VariableMap &&other) = default;
    VariableMap &operator=(VariableMap &&other) = default;

    /**
     * Create a new variable.
     * If a variable with given name already exists, no 
     * action is taken and false is returned.
     * If the variable already exists, but its type is 
     * different, exception is thrown.
     * @tparam VarT Type of the variable.
     * @param name Unique name of the variable.
     * @param initialValue Initial value of the created variable.
     * @return Returns true if the variable has been created.
     * @throws VarTypeException Thrown when variable 
     * already exists, but uses a different type.
     */
    template <typename VarT>
    bool createVar(const KeyT &name, const VarT &initialValue = { });

    /**
     * Is there a variable under provided name already 
     * registered?
     * @tparam VarT Type of the variable.
     * @param name Unique name of the variable.
     * @param forceType Consider variable to exist 
     * only when the type also matches?
     * @return Returns true if the exists. When forceType 
     * is set to true, true is returned only when type 
     * matches as well.
     */
    template <typename VarT>
    bool varExists(const KeyT &name, bool forceType = true) const;

    /**
     * Get current value for given variable name.
     * @tparam VarT Type of the variable.
     * @param name Unique name of the variable.
     * @return Returns a reference to the variable 
     * value, which does not change location in memory.
     * @throws VarTypeException Thrown when variable 
     * exists under different type.
     * @throws VarExistenceException Thrown when variable 
     * under given name does not exist.
     */
    template <typename VarT>
    VarT &getVar(const KeyT &name);

    /**
     * Get current value for given variable name.
     * @tparam VarT Type of the variable.
     * @param name Unique name of the variable.
     * @return Returns a reference to the variable 
     * value, which does not change location in memory.
     * @throws VarTypeException Thrown when variable 
     * exists under different type.
     * @throws VarExistenceException Thrown when variable 
     * under given name does not exist.
     */
    template <typename VarT>
    const VarT &getVar(const KeyT &name) const;

    /**
     * Get current value of a variable or create it 
     * if it does not exist yet.
     * @tparam VarT Type of the variable.
     * @param name Unique name of the variable.
     * @param initialValue Initial value for non-existent 
     * variables.
     * @return Returns a reference to the variable 
     * value, which does not change location in memory.
     * @throws VarTypeException Thrown when variable 
     * exists under different type.
     */
    template <typename VarT>
    VarT &getCreateVar(const KeyT &name, const VarT &initialValue = { });

    /**
     * Serialize currently contained variables, which allows their 
     * later loading.
     * @return Returns serialized variable string.
     */
    std::string serialize() const;

    /**
     * De-serialize provided string and load all variables.
     * @param serialized String containing serialized variables.
     */
    void deserialize(const std::string &serialized);
private:
    /// Representation of a single variable.
    struct VariableRecord
    {
        /// Unique name of the variable.
        std::string name;
        /// Storage for the variable value.
        std::any value;
    }; // struct VariableRecord

    /// Find variable record for given unique name.
    VariableRecord *varFind(const KeyT &name);

    /// Find variable record for given unique name.
    const VariableRecord *varFind(const KeyT &name) const;

    /// Find variable record, throw if it has different type.
    template <typename VarT>
    VariableRecord *varFindThrow(const KeyT &name);

    /// Find variable record, throw if it has different type.
    template <typename VarT>
    const VariableRecord *varFindThrow(const KeyT &name) const;

    /// Create a variable, it must not exist. Returns ptr to the new record.
    template <typename VarT>
    VariableRecord *varCreateInner(const KeyT &name, const VarT &initialValue = { });

    /// Does given variable record have contains variable of given type?
    template <typename T>
    static bool varHasType(const VariableRecord &rec);

    /// List of all registered variables.
    std::deque<VariableRecord> mVariables;
    /// Mapping from variable names to index in mVariables.
    std::map<KeyT, std::size_t> mNameMap;
protected:
}; // class VariableMap

} // namespace gui

} // namespace quark

// Template implementation.

namespace quark
{

namespace gui
{

template <typename VarT>
bool VariableMap::createVar(const KeyT &name, const VarT &initialValue)
{
    // Skip creation if it already exists.
    if (varFindThrow<VarT>(name) != nullptr)
    { return false; }

    const auto varRecord{ varCreateInner<VarT>(name, initialValue) };

    return varRecord != nullptr;
}

template <typename VarT>
bool VariableMap::varExists(const KeyT &name, bool forceType) const
{ return forceType ? (varFindThrow<VarT>(name) != nullptr) : (varFind(name) != nullptr); }

template <typename VarT>
VarT &VariableMap::getVar(const KeyT &name) 
{
    auto varRecord{ varFindThrow<VarT>(name) };
    if (varRecord == nullptr)
    { throw VarExistenceException("There is no variable with that name!"); }

    // Should not thrown, since varFindThrow would need to throw as well.
    return *std::any_cast<VarT>(&varRecord->value);
}

template <typename VarT>
const VarT &VariableMap::getVar(const KeyT &name) const
{
    auto varRecord{ varFindThrow<VarT>(name) };
    if (varRecord == nullptr)
    { throw VarExistenceException("There is no variable with that name!"); }

    // Should not thrown, since varFindThrow would need to throw as well.
    return *std::any_cast<VarT>(&varRecord->value);
}

template <typename VarT>
VarT &VariableMap::getCreateVar(const KeyT &name, const VarT &initialValue)
{
    auto varRecord{ varFindThrow<VarT>(name) };
    if (varRecord == nullptr)
    { varRecord = varCreateInner<VarT>(name, initialValue); }

    // Should not thrown, since varFindThrow would need to throw as well.
    return *std::any_cast<VarT>(&varRecord->value);
}

template <typename VarT>
VariableMap::VariableRecord *VariableMap::varFindThrow(const KeyT &name)
{
    const auto varRecord{ varFind(name) };
    if (varRecord)
    {
        if (!varHasType<VarT>(*varRecord))
        { throw VarTypeException("Variable already exists with different type!"); }
    }

    return varRecord;
}

template <typename VarT>
const VariableMap::VariableRecord *VariableMap::varFindThrow(const KeyT &name) const
{ return const_cast<VariableMap*>(this)->varFindThrow<VarT>(name); }

template <typename VarT>
VariableMap::VariableRecord *VariableMap::varCreateInner(const KeyT &name, const VarT &initialValue)
{
    ASSERT_SLOW(!varExists<VarT>(name, false));

    const auto varIndex{ mVariables.size() };
    mVariables.emplace_back(
        VariableRecord{
            name,
            initialValue
        } );

    mNameMap[name] = varIndex;

    return &mVariables.back();
}

template <typename T>
bool VariableMap::varHasType(const VariableRecord &rec)
{ return rec.value.type() == typeid(T); }

}

}

// Template implementation end.
