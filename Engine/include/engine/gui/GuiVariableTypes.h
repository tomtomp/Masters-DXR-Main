/**
 * @file gui/GuiVariableTypes.h
 * @author Tomas Polasek
 * @brief Definitions of variable types usable 
 * as GUI variables.
 */

#pragma once

#include "engine/util/Types.h"

/// Namespace containing the engine code.
namespace quark
{

// Forward declaration.
namespace dxtk
{
namespace math
{
struct Vector2;
struct Vector3;
struct Vector4;
struct Quaternion;
struct Matrix;
}
}

/// Gui classes.
namespace gui
{

class GuiContext;

/// Helper structure, which allows work with GUI frames.
struct GuiFrame
{
    /// Start a new frame, must be called before any other GUI methods.
    static void newFrame();
    /// End the last frame, must be called before starting a new one.
    static void endFrame();
}; // GuiFrame

/// Configuration of GUI context.
struct GuiContextConfig
{
    /// Set width of the context, 0 for auto.
    GuiContextConfig &setWidth(float val);

    /// Set height of the context, 0 for auto.
    GuiContextConfig &setHeight(float val);

    /// Set whether is this context opened and visible.
    GuiContextConfig &setOpened(bool enable);

    /// Set position of the window on the screen - 0,0 is top-left.
    GuiContextConfig &setPosition(float valPosX, float valPosY);

    /// Width of the context.
    float width{ 0.0f };
    /// Height of the context.
    float height{ 0.0f };

    /// Is the context opened?
    bool opened{ true };

    /// Position of the window in the x-axis.
    float posX{ 0.0f };
    /// Position of the window in the y-axis.
    float posY{ 0.0f };

    /// Should the position of the window be set?
    bool usePosition{ false };
private:
    friend class GuiContext;

    /// Start using this context.
    void startContext(const GuiContext &ctx);
    /// Stop using this context.
    void stopContext(const GuiContext &ctx);
}; // struct GuiContextConfig

template <typename VarT>
class GuiVariable;

/// Configuration of a GUI variable.
template <typename VarT>
struct GuiVariableConfig;

template <typename ValT>
struct BaseGuiValue
{
    /// Alias for the base type.
    using ValueT = ValT;

    /// Initialize the value to default.
    BaseGuiValue() = default;

    /// Initialize from target value.
    BaseGuiValue(ValT v) : 
        value{ v }
    { }

    /// Convert to the target type.
    operator ValueT() const;

    void setValue(const ValT &val)
    { value = val; valueChanged = true; }

    /// Instance of the value.
    ValT value{ };

    /// Was this value changed from the GUI?
    bool valueChanged{ false };
};

/// Helper structure containing color.
struct Color3V
{
    float data[3]{ 0.0f, 0.0f, 0.0f };
    constexpr float &r() { return data[0]; }
    constexpr float &g() { return data[1]; }
    constexpr float &b() { return data[2]; }
}; // struct Color3V

/// Color usable in GUI variables.
struct Color3 : public BaseGuiValue<Color3V>
{
    Color3() = default;

    // Automatic conversion from vector.
    explicit Color3(const dxtk::math::Vector3 &v);
    // Automatic conversion to vector.
    explicit operator dxtk::math::Vector3();
}; // struct Color3

// No special configuration for Color3.
template <>
struct GuiVariableConfig<Color3>
{
private:
    friend class GuiVariable<Color3>;

    /// Perform actions to update or create the variable.
    void updateCreateVariable(GuiVariable<Color3> &var);
}; // struct GuiVariableConfig<Color3>

/// Helper structure containing color.
struct Color4V
{
    float data[4]{ 0.0f, 0.0f, 0.0f, 0.0f };
    constexpr float &r() { return data[0]; }
    constexpr float &g() { return data[1]; }
    constexpr float &b() { return data[2]; }
    constexpr float &a() { return data[3]; }
}; // struct Color4V

/// Color usable in GUI variables.
struct Color4 : public BaseGuiValue<Color4V>
{
    Color4() = default;

    // Automatic conversion from vector.
    explicit Color4(const dxtk::math::Vector4 &v);
    // Automatic conversion to vector.
    explicit operator dxtk::math::Vector4();
}; // struct Color4

// No special configuration for Color4.
template <>
struct GuiVariableConfig<Color4>
{
private:
    friend class GuiVariable<Color4>;

    /// Perform actions to update or create the variable.
    void updateCreateVariable(GuiVariable<Color4> &var);
}; // struct GuiVariableConfig<Color4>

/// Helper structure containing vector.
struct Position2V
{
    float data[2]{ 0.0f, 0.0f };
    constexpr float &x() { return data[0]; }
    constexpr float &y() { return data[1]; }
}; // struct Position2V

/// Position usable in GUI variables.
struct Position2 : public BaseGuiValue<Position2V>
{
    Position2() = default;

    // Automatic conversion from vector.
    explicit Position2(const dxtk::math::Vector2 &v);
    // Automatic conversion to vector.
    explicit operator dxtk::math::Vector2();
}; // struct Position2

// No special configuration for Position2.
template <>
struct GuiVariableConfig<Position2>
{
private:
    friend class GuiVariable<Position2>;

    /// Perform actions to update or create the variable.
    void updateCreateVariable(GuiVariable<Position2> &var);
}; // struct GuiVariableConfig<Position2>

/// Helper structure containing vector.
struct Position3V
{
    float data[3]{ 0.0f, 0.0f, 0.0f };
    constexpr float &x() { return data[0]; }
    constexpr float &y() { return data[1]; }
    constexpr float &z() { return data[2]; }
}; // struct Position3V

/// Position usable in GUI variables.
struct Position3 : public BaseGuiValue<Position3V>
{
    Position3() = default;

    // Automatic conversion from vector.
    explicit Position3(const dxtk::math::Vector3 &v);
    // Automatic conversion to vector.
    explicit operator dxtk::math::Vector3();
}; // struct Position3

// No special configuration for Position3.
template <>
struct GuiVariableConfig<Position3>
{
private:
    friend class GuiVariable<Position3>;

    /// Perform actions to update or create the variable.
    void updateCreateVariable(GuiVariable<Position3> &var);
}; // struct GuiVariableConfig<Position3>

// Special config for displaying float.
template <>
struct GuiVariableConfig<float>
{
    /// Does the label contain formatted position of the float (%f)?
    GuiVariableConfig<float> &setFormatted(bool enable);

    /// Does the label contain formatted position of the float (%f)?
    bool formatted{ false };
private:
    friend class GuiVariable<float>;

    /// Perform actions to update or create the variable.
    void updateCreateVariable(GuiVariable<float> &var);
}; // struct GuiVariableConfig<float>

/// Float usable in GUI variables.
struct InputFloat : public BaseGuiValue<float>
{ 
    InputFloat() = default;

    InputFloat(float v)
    { value = v; }
}; // struct InputFloat

// Special config for InputFloat.
template <>
struct GuiVariableConfig<InputFloat>
{
    GuiVariableConfig<InputFloat> &setMin(float val);
    GuiVariableConfig<InputFloat> &setMax(float val);
    GuiVariableConfig<InputFloat> &setStep(float val);
    GuiVariableConfig<InputFloat> &setEditable(bool enable);

    float min{ std::numeric_limits<float>::min() };
    float max{ std::numeric_limits<float>::max() };
    float step{ 0.1f };

    bool editable{ true };

private:
    friend class GuiVariable<InputFloat>;

    /// Perform actions to update or create the variable.
    void updateCreateVariable(GuiVariable<InputFloat> &var);
}; // struct GuiVariableConfig<InputFloat>

// Special config for displaying integers.
template <>
struct GuiVariableConfig<uint64_t>
{
    /// Does the label contain formatted position of the integer (%d)?
    GuiVariableConfig<uint64_t> &setFormatted(bool enable);

    /// Does the label contain formatted position of the float (%d)?
    bool formatted{ false };
private:
    friend class GuiVariable<uint64_t>;

    /// Perform actions to update or create the variable.
    void updateCreateVariable(GuiVariable<uint64_t> &var);
}; // struct GuiVariableConfig<uint64_t>

/// Integer usable in GUI variables.
struct InputInt : public BaseGuiValue<int>
{ 
    InputInt() = default;

    // Automatic conversion from other integers.
    template <typename IntT>
    InputInt(const IntT &v);
}; // struct InputInt

// Special config for InputInt.
template <>
struct GuiVariableConfig<InputInt>
{
    GuiVariableConfig<InputInt> &setMin(int val);
    GuiVariableConfig<InputInt> &setMax(int val);
    GuiVariableConfig<InputInt> &setStep(int val);
    GuiVariableConfig<InputInt> &setEditable(bool enable);

    int min{ std::numeric_limits<int>::min() };
    int max{ std::numeric_limits<int>::max() };
    int step{ 1 };

    bool editable{ true };
private:
    friend class GuiVariable<InputInt>;

    /// Perform actions to update or create the variable.
    void updateCreateVariable(GuiVariable<InputInt> &var);
}; // struct GuiVariableConfig<InputFloat>

/// Enumeration usable in GUI variables.
struct Enumeration : public BaseGuiValue<int>
{
    Enumeration() = default;

    /// Convert from enum value.
    template <typename EnumT>
    explicit Enumeration(const EnumT &enumVal);

    /// Convert to enum value.
    template <typename EnumT>
    explicit operator EnumT() const;
}; // struct Enumeration

// Special configuration for Enumeration.
template <>
struct GuiVariableConfig<Enumeration>
{
    /**
     * Type of function callback used for getting the labels.
     * Function gets element index (int) and outputs the text 
     * into the outText (const char**). Returns bool representing 
     * whether the conversion completed successfully.
     */
    using ItemGetterFunT = std::function<bool(int, const char **)>;

    /**
     * Set the function used for getting item labels.
     * @param itemCount Number of items in the enum.
     * @param fun The callback used to get item labels.
     */
    GuiVariableConfig<Enumeration> &setItemGetter(std::size_t itemCount, ItemGetterFunT fun);
private:
    friend class GuiVariable<Enumeration>;

    /// Perform actions to update or create the variable.
    void updateCreateVariable(GuiVariable<Enumeration> &var);

    /// Get name for given index.
    const char *getName(int index);

    /// Adapter used for getting enumeration items.
    static bool itemGetterAdapter(void *data, int index, const char **outText);

    /// Number of items in the enum.
    std::size_t mItemCount{ 0u };
    /// Function used for getting the item labels.
    ItemGetterFunT mItemGetter{ };
}; // struct GuiVariableConfig<Enumeration>

// Special configuration for static text.
template <>
struct GuiVariableConfig<std::string>
{
private:
    friend class GuiVariable<std::string>;

    /// Perform actions to update or create the variable.
    void updateCreateVariable(GuiVariable<std::string> &var);
}; // struct GuiVariableConfig<std::string>

/// Dynamic text displayed in the GUI.
struct DynamicText
{
    /// Function type used for getting the text.
    using TextGetterFunT = std::function<std::string()>;

    /// Function used for getting the text.
    TextGetterFunT textGetter{ };
}; // struct Enumeration

// Special configuration for DynamicText.
template <>
struct GuiVariableConfig<DynamicText>
{
private:
    friend class GuiVariable<DynamicText>;

    /// Perform actions to update or create the variable.
    void updateCreateVariable(GuiVariable<DynamicText> &var);
}; // struct GuiVariableConfig<DynamicText>

/// Float graph displayed in the GUI.
struct GraphFloat
{
    /**
     * Push a new data point to the graph, while keeping 
     * only maxValues newest values.
     */
    void pushDataPoint(float point, std::size_t maxValues);

    /// How many times the displayed values should be kept, until clearing.
    static constexpr std::size_t MAX_DATA_LIMIT{ 10u };

    /// Data displayed on the graph.
    std::vector<float> data{ };
    /// Offset of the point added last.
    std::size_t currentOffset{ 0u };
    /// Maximum number of values to display.
    std::size_t displayedValues{ 0u };
}; // struct Enumeration

// Special configuration for GraphFloat.
template <>
struct GuiVariableConfig<GraphFloat>
{
    GuiVariableConfig<GraphFloat> &setMaxValue(float max);
    GuiVariableConfig<GraphFloat> &setMinValue(float min);

    // Maximum value in the graph.
    float maxValue{ std::numeric_limits<float>::max() };
    // Minimum value in the graph.
    float minValue{ std::numeric_limits<float>::min() };
private:
    friend class GuiVariable<GraphFloat>;

    /// Perform actions to update or create the variable.
    void updateCreateVariable(GuiVariable<GraphFloat> &var);
}; // struct GuiVariableConfig<GraphFloat>

/// Button usable in GUI.
struct Button
{
    /**
     * Type of function used as callback when clicking 
     * the button.
     */
    using ButtonCallbackFunT = std::function<void()>;

    /// Callback used when the button is pressed.
    ButtonCallbackFunT callbackFun{ };
}; // struct Button

// Special configuration for Button.
template <>
struct GuiVariableConfig<Button>
{
private:
    friend class GuiVariable<Button>;

    /// Perform actions to update or create the variable.
    void updateCreateVariable(GuiVariable<Button> &var);
}; // struct GuiVariableConfig<Button>

/// Float usable in GUI variables.
struct InputBool : public BaseGuiValue<bool>
{ 
    InputBool() = default;

    InputBool(bool v) : 
        BaseGuiValue<bool>(v)
    { }
}; // struct InputBool

// Special config for InputBool.
template <>
struct GuiVariableConfig<InputBool>
{
    GuiVariableConfig<InputBool> &setEditable(bool enable);

    bool editable{ true };
private:
    friend class GuiVariable<InputBool>;

    /// Perform actions to update or create the variable.
    void updateCreateVariable(GuiVariable<InputBool> &var);
}; // struct GuiVariableConfig<InputBool>

/// String usable in GUI variables.
struct InputString : public BaseGuiValue<std::string>
{ 
    InputString(const char *str)
    { value = str; }
}; // struct InputString

// Special config for InputString.
template <>
struct GuiVariableConfig<InputString>
{
    GuiVariableConfig<InputString> &setEditable(bool enable);
    GuiVariableConfig<InputString> &setMaxSize(std::size_t size);

    bool editable{ true };
private:
    friend class GuiVariable<InputString>;

    /// Starting size of the string buffer.
    static constexpr std::size_t STARTING_MAX_SIZE{ 32u };

    /// Buffer used for string storage.
    //std::vector<char> mBuffer(STARTING_MAX_SIZE, 0u);
    std::vector<char> mBuffer;

    /// Perform actions to update or create the variable.
    void updateCreateVariable(GuiVariable<InputString> &var);
}; // struct GuiVariableConfig<InputFloat>

template <typename GuiVarT, typename VarT>
bool synchronizeVariables(GuiVarT &guiVar, VarT &var);

} // namespace gui

} // namespace quark

// Template implementation.

namespace quark
{

namespace gui
{

template <typename ValT>
BaseGuiValue<ValT>::operator ValT() const
{ return value; }

template <typename EnumT>
Enumeration::Enumeration(const EnumT &enumVal)
{ value = static_cast<int>(enumVal); }

template <typename EnumT>
Enumeration::operator EnumT() const
{ return static_cast<EnumT>(value); }

template<typename IntT>
InputInt::InputInt(const IntT &v)
{ value = static_cast<ValueT>(v); }

template <typename GuiVarT, typename VarT>
bool synchronizeVariables(GuiVarT &guiVar, VarT &var)
{
    VarT guiValue{ static_cast<VarT>(guiVar) };

    if (guiVar.valueChanged)
    {
        var = guiValue;
        return true;
    }
    else if (var != guiValue)
    {
        guiVar.value = static_cast<GuiVarT>(var);
        return true;
    }

    return false;
}

}

}

// Template implementation end.
