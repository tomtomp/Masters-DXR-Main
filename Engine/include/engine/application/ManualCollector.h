/**
 * @file application/ManualCollector.h
 * @author Tomas Polasek
 * @brief Helper class used for collection of any user data.
 */

#pragma once

#include "engine/util/Types.h"

#include "engine/application/FpsUpdateTimer.h"
#include "engine/application/DataCollectorMgr.h"
#include "engine/application/ProfilingAggregator.h"

/// Namespace containing the engine code.
namespace quark
{

/// Namespace containing application interface.
namespace app
{

class ManualCollector : public BaseCollector
{
private:
    /// Base for all manual data streams
    class DataStreamBase
    {
    public:
        /// Free any special data.
        virtual ~DataStreamBase();
    private:
        friend class ManualCollector;

        /// collect any new data from the user callback.
        virtual void collectData() = 0;

        /// Get current size of the data buffer.
        virtual std::size_t size() const = 0;

        /// Clear currently contained data.
        virtual void clear() = 0;

        /// Synchronize length of data buffer to given length.
        virtual void syncLength(std::size_t dataLength, bool collectData) = 0;

        /// Get name of this data stream.
        virtual const std::string &name() const = 0;
        
        /// Copy the current data over to the provided aggregator.
        virtual void aggregateData(ProfilingAggregator &aggregator) = 0;
    protected:
    }; // class DataStreamBase
public:
    template <typename DataPointT>
    class DataStream : public DataStreamBase
    {
    public:
        /// Type of function used as callback for data.
        using DataCallbackT = std::function<DataPointT()>;

        /// Free any special data.
        virtual ~DataStream();

        /// Initialize the data stream with callback for getting data.
        DataStream(DataCallbackT cb);

        /// Set name for this data stream.
        DataStream &setName(const std::string &name);
    private:
        friend class ManualCollector;

        /// collect any new data from the user callback.
        virtual void collectData() override final;

        /// Get current size of the data buffer.
        virtual std::size_t size() const override final;

        /// Clear currently contained data.
        virtual void clear() override final;

        /// Synchronize length of data buffer to given length.
        virtual void syncLength(std::size_t dataLength, bool collectData) override final;

        /// Get name of this data stream.
        virtual const std::string &name() const override final;

        /// Copy the current data over to the provided aggregator.
        virtual void aggregateData(ProfilingAggregator &aggregator) override final;

        /// Callback used for getting new data points.
        DataCallbackT mDataCallback{ };
        /// List of data points.
        std::vector<DataPointT> mData{ };
        /// Name of the series.
        std::string mName{ };
    protected:
    }; // class DataStream

    /// Initialize collector without any data streams.
    ManualCollector();

    /// Free all data.
    ~ManualCollector();

    // Allow copying and moving.
    ManualCollector(const ManualCollector &other) = default;
    ManualCollector &operator=(const ManualCollector &other) = default;
    ManualCollector(ManualCollector &&other) = default;
    ManualCollector &operator=(ManualCollector &&other) = default;

    /// Clear all data.
    virtual void clear() override final;

    /**
     * Called in order to synchronize data lengths 
     * between collectors
     * @param dataLength Length of the data, which should 
     * be used by the collectors.
     */
    virtual void syncLength(std::size_t dataLength) override final;

    /// Add data point from given timer.
    virtual std::size_t collectData() override final;

    /// Copy the current data over to the provided aggregator.
    virtual void aggregateData(ProfilingAggregator &aggregator) override final;

    /// Get the current size in number of data points.
    virtual std::size_t size() const override final;

    /**
     * When set to true, length synchronization will call the 
     * data callback and add returned values, instead of copying 
     * old ones.
     */
    void setCollectInSync(bool enable);

    /**
     * When set, no collection will occur in the collectData.
     * Can be used with setCollectInSync(true), to collect only 
     * in sync.
     */
    void setNoCollection(bool enable);

    /**
     * Add a new data stream to this collector, which allows collection 
     * of data points.
     * @tparam DataPointT Type of a single data point.
     * @param name Name of the data stream.
     * @param cb Callback used for getting data points.
     */
    template <typename DataPointT>
    DataStream<DataPointT> &addDataStream(const std::string &name, 
        typename DataStream<DataPointT>::DataCallbackT cb);
private:
    /// List of currently added data streams.
    std::vector<util::UniquePtr<DataStreamBase>> mDataStreams{ };

    /// Current number of added data points.
    std::size_t mDataSize{ 0u };

    /// Collect data in syncLength, instead of copying old data.
    bool mCollectInSync{ false };
    /// Disable collection of data in collectData, used with mCollectInSync.
    bool mNoCollection{ false };
protected:
}; // class ManualCollector

} // namespace app

} // namespace quark

// Template implementation

namespace quark
{

namespace app
{

template<typename DataPointT>
ManualCollector::DataStream<DataPointT>::DataStream(DataCallbackT cb) : 
    mDataCallback{ cb }
{ }

template<typename DataPointT>
ManualCollector::DataStream<DataPointT> &ManualCollector::DataStream<DataPointT>::setName(
    const std::string &name)
{ mName = name; return *this; }

template<typename DataPointT>
ManualCollector::DataStream<DataPointT>::~DataStream()
{ /* Automatic */ }

template<typename DataPointT>
void ManualCollector::DataStream<DataPointT>::collectData()
{ mData.emplace_back(mDataCallback()); }

template<typename DataPointT>
std::size_t ManualCollector::DataStream<DataPointT>::size() const
{ return mData.size(); }

template<typename DataPointT>
void ManualCollector::DataStream<DataPointT>::clear()
{ mData.clear(); }

template<typename DataPointT>
void ManualCollector::DataStream<DataPointT>::syncLength(
    std::size_t dataLength, bool collectData)
{
    if (mData.size() >= dataLength)
    { // No action necessary.
        return;
    }

    // Else we need to align it: copy the last data or use new data.
    const auto pointCopy{ collectData ? mDataCallback() : (mData.empty() ? DataPointT{ } : mData.back()) };
    const auto toCreate{ dataLength - mData.size() };
    for (std::size_t iii = 0u; iii < toCreate; ++iii)
    { mData.push_back(pointCopy); }
}

template<typename DataPointT>
const std::string &ManualCollector::DataStream<DataPointT>::name() const
{ return mName; }

template<typename DataPointT>
void ManualCollector::DataStream<DataPointT>::aggregateData(ProfilingAggregator &aggregator)
{ aggregator.addDataSeries<DataPointT>().setName(mName).setData(mData); }

template<typename DataPointT>
ManualCollector::DataStream<DataPointT> &ManualCollector::addDataStream(const std::string &name, 
    typename DataStream<DataPointT>::DataCallbackT cb)
{
    auto collectorPtr{ std::make_unique<DataStream<DataPointT>>(cb) };
    collectorPtr->setName(name);
    const auto rawPtr{ collectorPtr.get() };

    mDataStreams.emplace_back(std::move(collectorPtr));

    return *rawPtr;
}

}

}

// Template implementation end
