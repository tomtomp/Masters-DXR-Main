/**
 * @file application/ProfilingCollector.h
 * @author Tomas Polasek
 * @brief Helper class used for collection of profiling data.
 */

#pragma once

#include "engine/util/Types.h"
#include "engine/util/prof/Profiler.h"
#include "engine/util/prof/PrintCrawler.h"

#include "engine/application/DataCollectorMgr.h"
#include "engine/application/ProfilingAggregator.h"

/// Namespace containing the engine code.
namespace quark
{

/// Namespace containing application interface.
namespace app
{

class ProfilingCollector : public BaseCollector
{
public:
    /// Initialize collector without specifying data streams.
    ProfilingCollector();

    /**
     * Initialize collector for collection of specified data streams.
     * @param noDuplication When set to true, no duplicate data entries 
     * will be added.
     * @param streams List of stream names, which are the same as the 
     * ones specified for PROF_*(...).
     */ 
    ProfilingCollector(bool noDuplication, std::initializer_list<const char*> streams);

    /// Free all data.
    ~ProfilingCollector();

    // Allow copying and moving.
    ProfilingCollector(const ProfilingCollector &other) = default;
    ProfilingCollector &operator=(const ProfilingCollector &other) = default;
    ProfilingCollector(ProfilingCollector &&other) = default;
    ProfilingCollector &operator=(ProfilingCollector &&other) = default;

    /// Clear all data but keep the streams.
    virtual void clear() override final;

    /// Clear all data and streams.
    void clearStreams();

    /**
     * Called in order to synchronize data lengths 
     * between collectors
     * @param dataLength Length of the data, which should 
     * be used by the collectors.
     */
    virtual void syncLength(std::size_t dataLength) override final;

    /**
     * Add a data stream name to collect.
     * @param stream Stream name, which is the same as the one specified 
     * for PROF_*(...).
     * @param numberOfNodes Predicted number of nodes to wait for.
     * @param useLastNode Should only the last node be used?
     * @return Returns reference to this.
     * @warning To take effect, initialize must be called afterwards.
     */
    ProfilingCollector &addStream(const char *stream, 
        std::size_t numberOfNodes = 1u, bool useLastNode = true);

    /// Set whether to prevent data duplication.
    ProfilingCollector &setNoDuplication(bool enable);

    /**
     * Initialize the data streams. This method should be called after 
     * adding all of the desired data streams and letting the profiling 
     * system register them at least once.
     */
    void initialize();

    /**
     * Collect current data from the profiling system.
     * @param noDuplication When set to true, no duplicate entries will 
     * be created.
     * @warning Automatically calls initialize until all of the streams 
     * are initialized!
     */ 
    virtual std::size_t collectData() override final;

    /// Copy the current data over to the provided aggregator.
    virtual void aggregateData(ProfilingAggregator &aggregator) override final;

    /// Get the current size in number of data points.
    virtual std::size_t size() const override final;
private:
    friend class CollectorCrawler;

    /// A single data point from a data stream.
    struct DataPoint
    {
        /// Sum of all measurements.
        double total{ 0.0 };
        /// Average of the measurements so far.
        double avg{ 0.0 };
        /// Last measurement taken.
        double last{ 0.0 };
    }; // struct DataPoint

    /// Specification of a data stream.
    struct DataStream
    {
        /// Name and identifier of the stream.
        std::string name{ };

        /// How many nodes should this data stream have.
        std::size_t targetNodeCount{ 0u };
        /// Should only the last node be used?
        bool useLastNode{ true };

        /// List of nodes containing the information for this stream.
        std::vector<util::prof::CallNode*> nodes{ };
        /// Special node chosen for processing, used as temporary storage.
        util::prof::CallNode *chosenNode{ nullptr };

        /// Last sample which has been registered from this stream.
        uint64_t samples{ 0u };

        /// List of collected data points.
        std::vector<DataPoint> data{ };
    }; // struct DataStream

    /// List of registered data streams.
    std::vector<DataStream> mDataStreams{ };

    /// Current number of data points in the initialized data streams.
    std::size_t mDataSize{ 0u };

    /// Should duplicate data be added?
    bool mNoDuplication{ false };
    /// Have all of specified streams been initialized?
    bool mStreamsInitialized{ false };
protected:
}; // class ProfilingCollector

} // namespace app

} // namespace quark

// Template implementation

namespace quark
{

namespace app
{

}

}

// Template implementation end
