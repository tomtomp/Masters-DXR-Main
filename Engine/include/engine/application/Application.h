/**
 * @file application/Application.h
 * @author Tomas Polasek
 * @brief Base application class, extensible by the user.
 */

#pragma once

#include "engine/util/Types.h"
#include "engine/util/prof/Profiler.h"
#include "engine/util/prof/PrintCrawler.h"
#include "engine/util/Timer.h"
#include "engine/util/Template.h"

#include "engine/application/MessageHandler.h"
#include "engine/application/FpsUpdateTimer.h"

#include "engine/renderer/clearPresentRL/ClearPresentRL.h"

#include "engine/gui/GuiSubSystem.h"

#include "EngineRuntimeConfig.h"

/// Namespace containing the engine code.
namespace quark
{

// Forward declaration.
class Engine;

namespace res
{
namespace win
{
class Window;
}
}

namespace scene
{
class SceneSubSystem;
}

namespace app
{
class MessageHandler;
}

/// Namespace containing application interface.
namespace app
{

namespace impl
{

/// Get the main window.
res::win::Window &engineWindow(Engine &engine);

/// Get renderer sub-system.
rndr::RenderSubSystem &engineRenderer(Engine &engine);

/// Get scene management sub-system.
scene::SceneSubSystem &engineScene(Engine &engine);

/// Get message handling sub-system.
app::MessageHandler &engineMsgHandler(Engine &engine);

/// Get gui management sub-system.
gui::GuiSubSystem &engineGui(Engine &engine);

}

/**
 * Main application class, should be inherited by 
 * user of the engine.
 * Contains default implementation of all methods 
 * required by the engine.
 * @tparam AppT Type of the application class inheriting 
 * this base class. Used for static dispatch of 
 * overridden methods.
 * @code
 * struct MyApp : public engine::app::Application<MyApp>
 * {
 *     // Specify runtime configuration type
 *     using ConfigT = MyConfigT;
 *     MyApp(ConfigT &cfg, engine::Engine &engine) : 
 *         app::Application<MyApp>(cfg, engine)
 *     { ... }
 *     
 *     // Available for overriding: 
 *     virtual ~MyApp();
 *     virtual void initialize() override final;
 *     virtual void destroy() override final;
 *     virtual int32_t run() override final;
 *     virtual void update(Milliseconds delta) override final;
 *     virtual void render() override final;
 *     virtual void processEvents() override final;
 *     void onEvent(const quark::app::events::KeyboardEvent &event);
 *     void onEvent(const quark::app::events::MouseButtonEvent &event);
 *     void onEvent(const quark::app::events::MouseMoveEvent &event);
 *     void onEvent(const quark::app::events::MouseScrollEvent &event);
 *     void onEvent(const quark::app::events::ResizeEvent &event);
 *     void onEvent(const quark::app::events::ExitEvent &event);
 * }
 * @endcode
 */
template <typename AppT, typename AppConfigT = EngineRuntimeConfig>
class Application
{
protected:
    /// Milliseconds type used in the Application.
    using Milliseconds = ::util::HrTimer::MillisecondsF;
    /// Seconds type used in the Application.
    using Seconds = ::util::HrTimer::SecondsF;

    // Event name shortcuts: 
    /// Event triggered when keyboard changes state.
    using KeyboardEvent = ::quark::app::events::KeyboardEvent;
    /// Enumeration of keys on the keyboard.
    using Key = ::quark::input::Key;
    /// Available key modifiers.
    using KeyMods = ::quark::input::KeyMods;
    /// Available key actions.
    using KeyAction = ::quark::input::KeyAction;

    /// Event triggered when mouse is moved.
    using MouseMoveEvent = ::quark::app::events::MouseMoveEvent;
    /// Event triggered when mouse button changes state.
    using MouseButtonEvent = ::quark::app::events::MouseButtonEvent;
    /// Event triggered when mouse scroll wheel changes state.
    using MouseScrollEvent = ::quark::app::events::MouseScrollEvent;
    /// Enumeration of mouse buttons.
    using MouseButton = ::quark::input::MouseButton;
    /// Available mouse keyboard modifiers.
    using MouseMods = ::quark::input::MouseMods;
    /// Available button actions.
    using MouseAction = ::quark::input::MouseAction;

    /// Event triggered when the window is resized.
    using ResizeEvent = ::quark::app::events::ResizeEvent;
    /// Event triggered when the window is closed.
    using ExitEvent = ::quark::app::events::ExitEvent;
public:
    /// Runtime configuration type, may be different than the default one.
    using ConfigT = AppConfigT;

    /// Clean up and exit.
    virtual ~Application() = default;

    // No copying or moving is required.
    Application(const Application &other) = delete;
    Application(Application &&other) = delete;
    Application &operator=(const Application &other) = delete;
    Application &operator=(Application &&other) = delete;

    /**
     * Called by the engine one time, before executing the run 
     * method.
     * Default implementation is empty.
     */
    virtual void initialize();

    /**
     * Ran one time, after initialize() has finished.
     * Default implementation contains main loop, running 
     * as long as mRunning is true. From this loop the 
     * update and render methods are called as many times 
     * per second, as defined in the runtime configuration 
     * stored in mCfg.
     * Update function will be called once, with delta 
     * time provided.
     * @return Return value will be returned by the 
     * engine run() method.
     */
    virtual int32_t run();

    /**
     * Method containing code for updating the state of 
     * the application.
     * Default implementation calls only processEvents().
     * Can be used with constant delta time and multiple 
     * calls or one call with higher delta.
     * @param delta How much time has gone through since 
     * the last update.
     */
    virtual void update(Milliseconds delta);

    /**
     * Method containing code for rendering the scene.
     * Default implementation clears the window and presents.
     */
    virtual void render();

    /**
     * Event processing loop.
     * Default implementation runs through all accumulated 
     * events from keyboard, mouseButton, mouseMove, 
     * mouseScroll and windowResize and passes them 
     * to event handlers. Skips any events with un-implemented 
     * handlers.
     * @param delta Delta time between updates.
     */
    virtual void processEvents(Milliseconds delta);

    /**
     * Default handling of resize events.
     * @param event Resize event.
     */
    void defaultResizeHandler(const events::ResizeEvent &event);

    /**
     * Called by the engine, one time before the destructor 
     * is triggered. Engine member is still valid.
     * Default implementation is empty.
     */
    virtual void destroy();

    /// Exit the application
    void exit();
private:
    friend class Engine;
protected:
    /**
     * Constructed by engine and called with runtime 
     * configuration provided by user to the run() 
     * method.
     * Stores the config into member variable mCfg.
     * Stores engine into member variable mEngine.
     * Also performs registration of event readers.
     * @param cfg Runtime configuration provided by 
     * the user.
     * @param engine Engine running this application.
     */
    Application(AppConfigT &cfg, Engine &engine);

    /**
     * Create event reader if there is onEvent accepting 
     * the event in App class.
     * Case for existing onEvent handler.
     * @tparam ReaderT Type of reader to create.
     * @param reader Where to create the reader.
     * @usage createReaderIfHandlerImplemented(myReader, int());
     */
    template <typename ReaderT>
    decltype(std::declval<AppT>().onEvent(std::declval<typename ReaderT::MessageT>()), void()) 
    createReaderIfHandlerImplemented(ReaderT &reader, int);

    /**
     * Create event reader if there is onEvent accepting 
     * the event in App class.
     * Case for non-existent onEvent handler.
     * @tparam ReaderT Type of reader to create.
     * @usage createReaderIfHandlerImplemented(myReader, int());
     */
    template <typename ReaderT>
    void createReaderIfHandlerImplemented(ReaderT &, ...);

    /**
     * Dispatch events received by given reader, if 
     * event handler exists.
     * Case for existing onEvent handler.
     * @tparam ReaderT Type of reader to use.
     * @param reader Which reader should be used.
     * @usage dispatchEvents(myReader, int());
     */
    template <typename ReaderT>
    decltype(std::declval<AppT>().onEvent(std::declval<typename ReaderT::MessageT>()), void()) 
    dispatchEvents(ReaderT &reader, int);

    /**
     * Dispatch events received by given reader, if 
     * event handler exists.
     * Case for non-existent onEvent handler.
     * @tparam ReaderT Type of reader to use.
     * @usage dispatchEvents(myReader, int());
     */
    template <typename ReaderT>
    void dispatchEvents(ReaderT &, ...);

    /**
     * Begin rendering of a new frame. Creates 
     * a new command list and appends to it the 
     * clear command with provided clear color.
     * @param clearColor Clear color used to 
     * clear the backbuffer.
     * @return Returns the prepared command list.
     * @throws Throws util::winexception on error.
     */
    res::d3d12::D3D12CommandList::PtrT clear(dxtk::math::Color clearColor = { 1.0f, 0.0f, 1.0f, 1.0f });

    /**
     * Finalize provided command list and present 
     * the result.
     * @param cmdList Command list returned by the 
     * clear method.
     * @throws Throws util::winexception on error.
     * @warning The provided command list will be 
     * unusable after the call returns.
     */
    void present(res::d3d12::D3D12CommandList::PtrT cmdList);

    /// Get the main window.
    res::win::Window &window()
    { return impl::engineWindow(mEngine); }

    /// Get renderer sub-system.
    rndr::RenderSubSystem &renderer()
    { return impl::engineRenderer(mEngine); }

    /// Get scene management sub-system.
    scene::SceneSubSystem &scene()
    { return impl::engineScene(mEngine); }

    /// Get message handling sub-system.
    app::MessageHandler &msgHandler()
    { return impl::engineMsgHandler(mEngine); }

    /// Get gui management sub-system.
    gui::GuiSubSystem &gui()
    { return impl::engineGui(mEngine); }

    /// Get number of milliseconds per update.
    Milliseconds updateDelta() const
    { return mCfg.appTargetUps ? Milliseconds(1000) / mCfg.appTargetUps : Milliseconds(1); }

    /// Get number of milliseconds per render.
    Milliseconds renderDelta() const
    { return mCfg.appTargetFps ? Milliseconds(1000) / mCfg.appTargetFps : Milliseconds(0); }

    /// Runtime configuration of this application.
    ConfigT &mCfg;
    /// Engine running this application.
    Engine &mEngine;
    /// Flag used by the main loop to test for ending.
    bool mRunning{ false };

    /// Reader for keyboard events.
    KeyboardEventBus::ReaderT mKeyboardEventReader;
    /// Reader for mouse button events.
    MouseButtonEventBus::ReaderT mMouseButtonEventReader;
    /// Reader for mouse move events.
    MouseMoveEventBus::ReaderT mMouseMoveEventReader;
    /// Reader for mouse scroll events.
    MouseScrollEventBus::ReaderT mMouseScrollEventReader;
    /// Reader for resize events.
    ResizeEventBus::ReaderT mResizeEventReader;
    /// Reader for exit events.
    ExitEventBus::ReaderT mExitEventReader;

    /// Rendering layer used for clearing and presenting the swap chain.
    rndr::ClearPresentRL::PtrT mClearPresentRL;
}; // class Application

} // namespace app

} // namespace quark

// Template implementation

namespace quark
{

namespace app
{

template <typename AppT, typename AppConfigT>
void Application<AppT, AppConfigT>::initialize()
{ }

template <typename AppT, typename AppConfigT>
int32_t Application<AppT, AppConfigT>::run()
{
    mRunning = true;

    // Target number of milliseconds between updates.
    const auto updateDeltaMs{ updateDelta() };
    util::HrTimer updateTimer;

    // Target number of milliseconds between renders.
    const auto renderDeltaMs{ renderDelta() };
    util::HrTimer renderTimer;

    updateTimer.reset();
    renderTimer.reset();

    while (mRunning)
    { // Main application loop
        if (updateTimer.elapsed<Milliseconds>() >= updateDeltaMs)
        { // Time to update.
            update(updateTimer.elapsedReset<Milliseconds>());
        }
        if (renderTimer.elapsed<Milliseconds>() >= renderDeltaMs)
        { // Time to render.
            renderTimer.reset();
            render();
        }
    }

    return EXIT_SUCCESS;
}

template <typename AppT, typename AppConfigT>
void Application<AppT, AppConfigT>::update(Milliseconds delta)
{ processEvents(delta); }

template <typename AppT, typename AppConfigT>
void Application<AppT, AppConfigT>::render()
{ present(clear()); }

template <typename AppT, typename AppConfigT>
void Application<AppT, AppConfigT>::processEvents(Milliseconds delta)
{
    dispatchEvents(mKeyboardEventReader, int());
    dispatchEvents(mMouseButtonEventReader, int());
    dispatchEvents(mMouseMoveEventReader, int());
    dispatchEvents(mMouseScrollEventReader, int());
    dispatchEvents(mResizeEventReader, int());
    dispatchEvents(mExitEventReader, int());

    gui().update(delta);
}

template <typename AppT, typename AppConfigT>
void Application<AppT, AppConfigT>::defaultResizeHandler(const events::ResizeEvent &event)
{ mEngine.renderer().resize(event.newWidth, event.newHeight); }

template <typename AppT, typename AppConfigT>
void Application<AppT, AppConfigT>::destroy()
{ }

template <typename AppT, typename AppConfigT>
void Application<AppT, AppConfigT>::exit()
{ mEngine.window().sendExitMessage(); }

template <typename AppT, typename AppConfigT>
Application<AppT, AppConfigT>::Application(AppConfigT &cfg, Engine &engine):
    mCfg{ cfg }, mEngine{ engine },
    mClearPresentRL{ rndr::ClearPresentRL::create(engine.renderer()) }
{
    createReaderIfHandlerImplemented(mKeyboardEventReader, int());
    createReaderIfHandlerImplemented(mMouseButtonEventReader, int());
    createReaderIfHandlerImplemented(mMouseMoveEventReader, int());
    createReaderIfHandlerImplemented(mMouseScrollEventReader, int());
    createReaderIfHandlerImplemented(mResizeEventReader, int());
    createReaderIfHandlerImplemented(mExitEventReader, int());
}

// TODO - Fake warning, return type is void?
template <typename AppT, typename AppConfigT>
template <typename ReaderT>
decltype(std::declval<AppT>().onEvent(std::declval<typename ReaderT::MessageT>()), void()) 
Application<AppT, AppConfigT>::createReaderIfHandlerImplemented(ReaderT &reader, int)
{ reader = std::move(msgHandler().eventReader<typename ReaderT::MessageT>()); }

template <typename AppT, typename AppConfigT>
template <typename ReaderT>
void Application<AppT, AppConfigT>::createReaderIfHandlerImplemented(ReaderT &, ...)
{
    log<Debug>() << "NOT creating event reader for " << ReaderT::MessageT::Name << 
        " since onEvent does not exist!" << std::endl;
}

// TODO - Fake warning, return type is void?
template <typename AppT, typename AppConfigT>
template <typename ReaderT>
decltype(std::declval<AppT>().onEvent(std::declval<typename ReaderT::MessageT>()) , void()) 
Application<AppT, AppConfigT>::dispatchEvents(ReaderT &reader, int)
{
    for (const auto &event : reader.fetchMessages())
    { // For all events in the message box.
        if (gui().processEvent(event))
        { // Captured the input, skip dispatch.
            continue;
        }

        // Static dispatch.
        static_cast<AppT*>(this)->onEvent(event);
    }
}

template <typename AppT, typename AppConfigT>
template <typename ReaderT>
void Application<AppT, AppConfigT>::dispatchEvents(ReaderT &, ...)
{ }

template <typename AppT, typename AppConfigT>
res::d3d12::D3D12CommandList::PtrT Application<AppT, AppConfigT>::clear(dxtk::math::Color clearColor)
{ return mClearPresentRL->clear(clearColor); }

template <typename AppT, typename AppConfigT>
void Application<AppT, AppConfigT>::present(res::d3d12::D3D12CommandList::PtrT cmdList)
{ mClearPresentRL->present((cmdList), mCfg.appUseVSync, mCfg.appUseTearing); }

}

}

// Template implementation end
