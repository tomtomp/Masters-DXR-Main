/**
 * @file application/Events.h
 * @author Tomas Polasek
 * @brief Event structures.
 */

#pragma once

#include "engine/application/MessageBus.h"

#include "engine/input/KeyboardEvent.h"
#include "engine/input/MouseEvent.h"

/// Namespace containing the engine code.
namespace quark
{

/// Namespace containing application interface.
namespace app
{

/// Namespace containing application events.
namespace events
{

// Input event shortcuts.

/// Event triggered when keyboard changes state.
using KeyboardEvent = ::quark::input::KeyboardEvent;
/// Event triggered when mouse button changes state.
using MouseButtonEvent = ::quark::input::MouseButtonEvent;
/// Event triggered when mouse moves.
using MouseMoveEvent = ::quark::input::MouseMoveEvent;
/// Event triggered when mouse scroll wheel is used.
using MouseScrollEvent = ::quark::input::MouseScrollEvent;

/// Event triggered when window changes size.
struct ResizeEvent
{
    /// Name of this event.
    static constexpr const char *Name{ "ResizeEvent" };

    ResizeEvent() = default;

    ResizeEvent(uint32_t width, uint32_t height) :
        newWidth{ width }, newHeight{ height }
    { }

    uint32_t newWidth{ 0u };
    uint32_t newHeight{ 0u };
}; // struct ResizeEvent

/// Event triggered when the main window is closed.
struct ExitEvent
{
    /// Name of this event.
    static constexpr const char *Name{ "ExitEvent" };
}; // struct ExitEvent

} // namespace events

// Extern template declarations.
DECLARE_EXTERN_MESSAGE_BUS(::quark::app::events::KeyboardEvent);
DECLARE_EXTERN_MESSAGE_BUS(::quark::app::events::MouseButtonEvent);
DECLARE_EXTERN_MESSAGE_BUS(::quark::app::events::MouseMoveEvent);
DECLARE_EXTERN_MESSAGE_BUS(::quark::app::events::MouseScrollEvent);
DECLARE_EXTERN_MESSAGE_BUS(::quark::app::events::ResizeEvent);
DECLARE_EXTERN_MESSAGE_BUS(::quark::app::events::ExitEvent);

using KeyboardEventBus = ::quark::app::MessageBus<::quark::app::events::KeyboardEvent>;
using MouseButtonEventBus = ::quark::app::MessageBus<::quark::app::events::MouseButtonEvent>;
using MouseMoveEventBus = ::quark::app::MessageBus<::quark::app::events::MouseMoveEvent>;
using MouseScrollEventBus = ::quark::app::MessageBus<::quark::app::events::MouseScrollEvent>;
using ResizeEventBus = ::quark::app::MessageBus<::quark::app::events::ResizeEvent>;
using ExitEventBus = ::quark::app::MessageBus<::quark::app::events::ExitEvent>;

} // namespace app

} // namespace quark
