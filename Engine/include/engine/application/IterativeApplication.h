/**
 * @file application/IterativeApplication.h
 * @author Tomas Polasek
 * @brief Wrapper around a normal application 
 * which allows iterative running of the main loop.
 */

#pragma once

#include "engine/util/Types.h"

/// Namespace containing the engine code.
namespace quark
{

// Forward declaration
class Engine;

/// Namespace containing application interface.
namespace app
{

/**
 * Wrapper around application instance which allows 
 * its iterative running.
 */
template <typename AppT>
class IterativeApplication : public util::PointerType<IterativeApplication<AppT>> 
{
public:
    /// Free the application instance.
    ~IterativeApplication();

    // No copying or moving.
    IterativeApplication(const IterativeApplication &other) = delete;
    IterativeApplication &operator=(const IterativeApplication &other) = delete;
    IterativeApplication(IterativeApplication &&other) = delete;
    IterativeApplication &operator=(IterativeApplication &&other) = delete;

    /// Access the application instance.
    AppT &app();

    /// Access the application instance.
    const AppT &app() const;
private:
    // Allow construction.
    friend class quark::Engine;

    /// Create an instance of the wrapper.
    template <typename ConfigT>
    static std::shared_ptr<IterativeApplication<AppT>> create(ConfigT &cfg, Engine &engine);

    /// Create application instance.
    template <typename ConfigT>
    IterativeApplication(ConfigT &cfg, Engine &engine);

    /// Initialize the inner application.
    void initialize();

    /// Execute a single iteration of the application.
    int32_t runIteration();

    /// Destroy the inner application.
    void destroy();

    /// Wrapped instance of the application.
    AppT mAppInstance;
protected: 
}; // class IterativeApplication

} // namespace app

} // namespace quark

// Template implementation

namespace quark
{

namespace app
{

template <typename AppT>
IterativeApplication<AppT>::~IterativeApplication()
{ /* Automatic */ }

template<typename AppT>
AppT& IterativeApplication<AppT>::app()
{ return mAppInstance; }

template<typename AppT>
const AppT& IterativeApplication<AppT>::app() const
{ return mAppInstance; }

template <typename AppT>
template <typename ConfigT>
std::shared_ptr<IterativeApplication<AppT>> IterativeApplication<AppT>::create(ConfigT &cfg, Engine &engine)
{ return std::shared_ptr<IterativeApplication<AppT>>{ new IterativeApplication<AppT>(cfg, engine) }; }

template <typename AppT>
template <typename ConfigT>
IterativeApplication<AppT>::IterativeApplication(ConfigT &cfg, Engine &engine) : 
    mAppInstance(cfg, engine)
{ }

template <typename AppT>
void IterativeApplication<AppT>::initialize()
{ mAppInstance.initialize(); }

template<typename AppT>
int32_t IterativeApplication<AppT>::runIteration()
{ return mAppInstance.run(); }

template <typename AppT>
void IterativeApplication<AppT>::destroy()
{ mAppInstance.destroy(); }

}

}

// Template implementation end
