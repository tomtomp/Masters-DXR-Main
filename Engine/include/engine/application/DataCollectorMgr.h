/**
 * @file application/DataCollectorMgr.h
 * @author Tomas Polasek
 * @brief Helper class used for managing multiple 
 * data collectors and their synchronization.
 */

#pragma once

#include "engine/util/Types.h"

#include "engine/application/FpsUpdateTimer.h"
#include "engine/application/ProfilingAggregator.h"

/// Namespace containing the engine code.
namespace quark
{

/// Namespace containing application interface.
namespace app
{

/**
 * Base class for all data collectors.
 */
class BaseCollector
{
public:
    /// Create the collector.
    BaseCollector();

    /// Free all collected data.
    virtual ~BaseCollector();

    /// Clear all collected data.
    virtual void clear() = 0;

    /**
     * Called in order to synchronize data lengths 
     * between collectors
     * @param dataLength Length of the data, which should 
     * be used by the collectors.
     */
    virtual void syncLength(std::size_t dataLength) = 0;

    /**
     * Check if any more data are present and collect them.
     * @return Returns the number of added elements.
     */ 
    virtual std::size_t collectData() = 0;

    /// Copy the current data over to the provided aggregator.
    virtual void aggregateData(ProfilingAggregator &aggregator) = 0;

    /// Get the current size in number of data points.
    virtual std::size_t size() const = 0;
private:
protected:
}; // class BaseCollector

/**
 * Helper class used for managing multiple data collectors.
 */
class DataCollectorMgr
{
public:
    /// Helper for creating aggregations of collectors.
    class Aggregation
    {
    public:
        /// Create the aggregation and name it.
        Aggregation(const std::string &name);

        /**
         * Add a new collector to this aggregation.
         * @tparam CollectorT Type of this collector.
         * @tparam CArgTs Types of the constructor 
         * arguments passed to the collector constructor.
         * @param cArgs constructor arguments passed to the 
         * @return Returns reference to the newly created 
         * collector.
         */
        template <typename CollectorT, typename... CArgTs>
        CollectorT &addCollector(CArgTs&&... cArgs);

        /// Set whether to use vertical tables for this aggregator.
        void setVerticalTable(bool enable);

        /// Set whether to use automatic indexing for this aggregator.
        void setAutoIndexing(bool enable);

        /// Get maximum number of data points in the list of active collectors.
        std::size_t maxDataSize() const;

        /// Clear all collectors.
        void clear();
    private:
        friend class DataCollectorMgr;

        /// Name of the aggregation.
        std::string mName{ };

        /// Print out the tables as vertical?
        bool mVerticalTable{ false };
        /// Print automatic indices?
        bool mAutoIndexing{ false };

        /// List of collectors to put into this aggregation.
        std::vector<util::UniquePtr<BaseCollector>> mCollectors{ };
    protected:
    }; // class Aggregation

    /// Exception thrown when aggregation with given name has not been added.
    struct AggregationNotFound : public std::exception 
    {
        AggregationNotFound(const char *msg) : 
            std::exception(msg)
        { }
    }; // struct AggregationNotFound

    /// Initialize the manager without any collectors.
    DataCollectorMgr();

    /// Free all data.
    ~DataCollectorMgr();

    // Allow copying and moving.
    DataCollectorMgr(const DataCollectorMgr &other) = default;
    DataCollectorMgr &operator=(const DataCollectorMgr &other) = default;
    DataCollectorMgr(DataCollectorMgr &&other) = default;
    DataCollectorMgr &operator=(DataCollectorMgr &&other) = default;

    /// Clear all collectors.
    void clear();

    /// Reset collector manager, removing all collectors.
    void reset();

    /// Check for new data and collect it.
    void collectData();

    /**
     * Create a new aggregation and return its configurator.
     * @param name Name of the aggregator, used as identifier.
     * @return Returns reference to the aggregation configurator 
     * which is valid until the next push.
     */
    Aggregation &pushAggregation(const std::string &name);

    /**
     * Recover already added aggregator.
     * @param name Name of the aggregator, used as identifier.
     * @return Returns reference to the aggregation configurator 
     * which is valid until the next push.
     * @throws AggregationNotFound Thrown when aggregation with given 
     * name does not exist.
     */
    Aggregation &getAggregation(const std::string &name);

    /**
     * Crate output string, containing tables with data.
     * @return Returns the formatted string.
     */
    std::string generateOutputString();
private:
    /// List of currently active aggregators.
    std::vector<Aggregation> mAggregators{ };
protected:
}; // class DataCollectorMgr

} // namespace app

} // namespace quark

// Template implementation start.

namespace quark
{

namespace app
{

template<typename CollectorT, typename... CArgTs>
CollectorT &DataCollectorMgr::Aggregation::addCollector(CArgTs&&... cArgs)
{
    auto collectorPtr{ std::make_unique<CollectorT>(std::forward<CArgTs>(cArgs)...) };
    auto rawPtr{ collectorPtr.get() };

    mCollectors.emplace_back(std::move(collectorPtr));

    return *rawPtr;
}

}

}
// Template implementation end.