/**
 * @file application/FpsCollector.h
 * @author Tomas Polasek
 * @brief Helper class used for collection of fps data.
 */

#pragma once

#include "engine/util/Types.h"

#include "engine/application/FpsUpdateTimer.h"
#include "engine/application/DataCollectorMgr.h"
#include "engine/application/ProfilingAggregator.h"

/// Namespace containing the engine code.
namespace quark
{

/// Namespace containing application interface.
namespace app
{

class FpsCollector : public BaseCollector
{
public:
    /// Initialize collector, providing the target timer.
    FpsCollector(const FpsUpdateTimer &timer);

    /// Free all data.
    ~FpsCollector();

    // Allow copying and moving.
    FpsCollector(const FpsCollector &other) = default;
    FpsCollector &operator=(const FpsCollector &other) = default;
    FpsCollector(FpsCollector &&other) = default;
    FpsCollector &operator=(FpsCollector &&other) = default;

    /// Clear all data.
    virtual void clear() override final;

    /**
     * Called in order to synchronize data lengths 
     * between collectors
     * @param dataLength Length of the data, which should 
     * be used by the collectors.
     */
    virtual void syncLength(std::size_t dataLength) override final;

    /// Add data point from given timer.
    virtual std::size_t collectData() override final;

    /// Copy the current data over to the provided aggregator.
    virtual void aggregateData(ProfilingAggregator &aggregator) override final;

    /// Get the current size in number of data points.
    virtual std::size_t size() const override final;
private:
    /// A single data point.
    struct DataPoint
    {
        /// Frames per second.
        float fps{ 0.0f };
        /// Updates per second.
        float ups{ 0.0f };
        /// Frame time in milliseconds.
        float frameTime{ 0.0f };
        /// GigaRays per second.
        float grps{ 0.0f };
    }; // struct DataPoint

    /// Target timer used for getting the data.
    const FpsUpdateTimer *mTargetTimer{ };

    /// List of collected data.
    std::vector<DataPoint> mData;
protected:
}; // class FpsCollector

} // namespace app

} // namespace quark
