/**
 * @file application/ApplicationGui.h
 * @author Tomas Polasek
 * @brief Container for common GUI variables used by 
 * all applications.
 */

#pragma once

#include "engine/util/Types.h"

#include "engine/application/FpsUpdateTimer.h"
#include "engine/application/Events.h"

#include "EngineRuntimeConfig.h"

/// Namespace containing the engine code.
namespace quark
{

// Forward declaration.
namespace gui
{
class GuiSubSystem;
}

/// Namespace containing application interface.
namespace app
{

/**
 * Container for common GUI variables used by 
 * all applications.
 */
class ApplicationGui
{
public:
    /// Initialize without creating the GUI.
    explicit ApplicationGui(const EngineRuntimeConfig &cfg);

    /// Free all resources.
    ~ApplicationGui();

    /**
     * Create the GUI elements. Also prepares a default 
     * context, which can be used by other variables.
     * @param gui GUI sub-system used for the main GUI.
     */
    void prepareGui(gui::GuiSubSystem &gui);

    /**
     * Update the GUI variables.
     * @param gui GUI sub-system used for the main GUI.
     * @warning GUI must be prepared before calling this 
     * method!
     */
    void updateGui(gui::GuiSubSystem &gui);

    /**
     * Update information about application timing from 
     * provided timer.
     */
    void updateFpsTimer(const FpsUpdateTimer &timer);

    /// Update current resolution, from provided event.
    void updateResolution(const events::ResizeEvent &event);
private:
    /// Number of data points in the performance graph.
    static constexpr std::size_t PERF_GRAPH_DATA_POINTS{ 512u };

    /// Current runtime config of the application.
    const EngineRuntimeConfig &mCfg;

    /// Frames per second.
    uint64_t mFps{ 0u };
    /// Updates per second.
    uint64_t mUps{ 0u };
    /// Time per frame in milliseconds.
    float mFrameTime{ 0.0f };
    /// Current gigarays/s.
    float mGrps{ 0.0f };

    /// Width of the render area.
    uint32_t mRenderWidth{ 0u };
    /// Height of the render area.
    uint32_t mRenderHeight{ 0u };
protected:
}; // class ApplicationGui

} // namespace app

} // namespace quark
