/**
 * @file application/ProfilingAggregator.h
 * @author Tomas Polasek
 * @brief Helper class used for taking multiple data 
 * series and compiling a table from them.
 */

#pragma once

#include "engine/util/Types.h"

#include "engine/resources/File.h"

/// Namespace containing the engine code.
namespace quark
{

/// Namespace containing application interface.
namespace app
{

class ProfilingAggregator
{
private:
    /// Base class for data series.
    class DataSeriesBase
    {
    public:
        /// Free any specialized data.
        virtual ~DataSeriesBase();

        /// Get total number of data points.
        virtual std::size_t dataPoints() const = 0;

        /// Get name of this data stream.
        virtual const std::string &name() const = 0;

        /// Get the current number of data points.
        virtual std::size_t size() const = 0;
    private:
        friend class ProfilingAggregator;

        /// Push data point from given index to the string stream.
        virtual void pushDataPoint(std::stringstream &ss, std::size_t index) const = 0;
    protected:
    }; // class DataSeriesBase
public:
    /// Manipulator for a series of data points.
    template <typename DataPointT>
    class DataSeries : public DataSeriesBase
    {
    public:
        /// Free any specialized data.
        virtual ~DataSeries();

        DataSeries &setName(const std::string &seriesName);
        DataSeries &setData(const std::vector<DataPointT> &seriesData);
        DataSeries &setData(std::vector<DataPointT> &&seriesData);

        /// Get total number of data points.
        virtual std::size_t dataPoints() const override final;

        /// Get name of this data stream.
        virtual const std::string &name() const override final;

        /// Get the current number of data points.
        virtual std::size_t size() const override final;
    private:
        /**
         * Push data point from given index to the string stream.
         * For indices out of bounds, default value is pushed 
         * to the stream instead.
         * @param ss Stream to push the value to.
         * @param index Index of the requested value.
         */ 
        virtual void pushDataPoint(std::stringstream &ss, std::size_t index) const override final;

        /// Name of the series.
        std::string mName{ };
        /// List of data points.
        std::vector<DataPointT> mData{ };
    protected:
    }; // class DataSeries

    /// Orientations of the generated table.
    enum class TableOrientation
    {
        /// Data will be in rows.
        Horizonal, 
        /// Data will be in columns.
        Vertical
    }; // enum class TableOrientation

    /// Initialize aggregator with no data.
    ProfilingAggregator();

    /// Free all data.
    ~ProfilingAggregator();

    // Allow copying and moving.
    ProfilingAggregator(const ProfilingAggregator &other) = default;
    ProfilingAggregator &operator=(const ProfilingAggregator &other) = default;
    ProfilingAggregator(ProfilingAggregator &&other) = default;
    ProfilingAggregator &operator=(ProfilingAggregator &&other) = default;

    /**
     * Add a new data series to the aggregator.
     * @return Returns series manipulator which is valid until 
     * the next operation on the aggregator.
     */ 
    template <typename DataPointT>
    DataSeries<DataPointT> &addDataSeries();

    /// Set column separator, when generating the table.
    void setTableSeparator(const std::string &sep);

    /**
     * Automatic indexing generates unique sequential indices 
     * for all of the data.
     * When enabled a new data series will appear, which starts 
     * at 0 and goes up linearly.
     * @param enable Enable automatic indexing?
     */
    void setAutoIndexing(bool enable);

    /// Generate a string table from the added data and return it.
    std::string generateTable(TableOrientation orientation = TableOrientation::Horizonal);

    /// Save data currently added to the aggregator to given file.
    void saveToFile(const res::File &trackFile, TableOrientation orientation = TableOrientation::Horizonal);
private:
    /// Default separator used in the table.
    static constexpr const char *DEFAULT_SEPARATOR{ "\t,\t" };
    /// Default name of the automatic index data series.
    static constexpr const char *DEFAULT_INDEX_SERIES_NAME{ "Index" };

    /// Generate a horizontal table from the current data.
    std::string generateHorizontalTable();
    /// Generate a vertical table from the current data.
    std::string generateVerticalTable();

    /// List of data series currently in the aggregator.
    std::vector<util::PtrT<DataSeriesBase>> mDataSeriesList;

    /// Separator used for column separation.
    std::string mSeparator{ DEFAULT_SEPARATOR };

    /// Generate data series containing unique indices?
    bool mAutoIndexing{ false };
protected:
}; // class ProfilingAggregator

} // namespace app

} // namespace quark

// Template implementation

namespace quark
{

namespace app
{

template<typename DataPointT>
inline ProfilingAggregator::DataSeries<DataPointT>::~DataSeries()
{ /* Automatic */ }

template <typename DataPointT>
ProfilingAggregator::DataSeries<DataPointT> &ProfilingAggregator::DataSeries<DataPointT>::setName(
    const std::string &seriesName)
{ mName = seriesName; return *this; }

template <typename DataPointT>
ProfilingAggregator::DataSeries<DataPointT> &ProfilingAggregator::DataSeries<DataPointT>::setData(
    const std::vector<DataPointT> &seriesData)
{ mData = seriesData; return *this; }

template <typename DataPointT>
ProfilingAggregator::DataSeries<DataPointT> &ProfilingAggregator::DataSeries<DataPointT>::setData(
    std::vector<DataPointT> &&seriesData)
{ mData = std::move(seriesData); return *this; }

template<typename DataPointT>
std::size_t ProfilingAggregator::DataSeries<DataPointT>::dataPoints() const
{ return mData.size(); }

template<typename DataPointT>
const std::string &ProfilingAggregator::DataSeries<DataPointT>::name() const
{ return mName; }

template<typename DataPointT>
std::size_t ProfilingAggregator::DataSeries<DataPointT>::size() const
{ return mData.size(); }

template<typename DataPointT>
void ProfilingAggregator::DataSeries<DataPointT>::pushDataPoint(
    std::stringstream &ss, std::size_t index) const
{ ss << ((index < mData.size()) ? mData[index] : DataPointT{ }); }

template<typename DataPointT>
ProfilingAggregator::DataSeries<DataPointT> &ProfilingAggregator::addDataSeries()
{
    auto aggregatorPtr{ std::make_unique<DataSeries<DataPointT>>() };
    const auto rawPtr{ aggregatorPtr.get() };

    mDataSeriesList.emplace_back(std::move(aggregatorPtr));

    return *rawPtr;
}

}

}

// Template implementation end
