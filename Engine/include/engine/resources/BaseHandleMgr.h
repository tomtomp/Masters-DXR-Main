/**
 * @file resources/BaseHandleMgr.h
 * @author Tomas Polasek
 * @brief Resource manager specialization, used as a 
 * base for managers which produce handles to their 
 * resources.
 */

#pragma once

#include "engine/resources/BaseResourceMgr.h"
#include "engine/resources/ResourceHandle.h"

#include "engine/util/Asserts.h"
#include "engine/util/Types.h"
#include "engine/util/logging/Logging.h"

/// Namespace containing the engine code.
namespace quark
{

/// Namespace containing resource management helpers.
namespace res
{

/**
 * Resource manager specialization, used as a 
 * base for managers which produce handles to their 
 * resources.
 * @tparam ResTT Type of resource provided by the manager.
 * @tparam HandleIdxTT Type for handle indices, should be 
 * large enough to hold all potential resources at the same 
 * time - e.g. using default uint16_t there may only ever 
 * exist 65536 resources at one time.
 * @tparam IdentifierTT Type of identifier used for unique 
 * identification of resources. For most cases the default 
 * string should be fine.
 * @tparam RefCounterTT Type used for the reference counter 
 * for each resource. Should be able to hold as many values 
 * as there may exist references to one resource - e.g. for 
 * the default uint8_t the max references is 255.
 */
template <typename ResTT, typename HandleIdxTT = uint16_t, typename IdentifierTT = std::string, typename RefCounterTT = uint8_t>
class BaseHandleMgr : BaseResourceMgr
{
public:
    /// Type of resource provided by this manager.
    using ResT = ResTT;
    /// Type of handle index used by this manager.
    using HandleIdxT = HandleIdxTT;
    /// Type for the unique identification of resources.
    using IdentifierT = IdentifierTT;
    /// Type used for the reference counter for each resource.
    using RefCounterT = RefCounterTT;

    /// Type of this manager.
    using ThisMgrT = BaseHandleMgr<ResTT, HandleIdxTT, IdentifierTT, RefCounterTT>;

    /// Wrapper around the resource provided by this manager.
    class ResourceHolder
    {
    public:
        /// Empty resource holder.
        ResourceHolder() = default;

        /**
         * Resource holder for named resource.
         */
        ResourceHolder(const IdentifierT &id) :
            mId{ id }
        { }

        ResourceHolder(const ResourceHolder &other)
        { swap(other); }
        ResourceHolder &operator=(const ResourceHolder &other)
        { swap(other); return *this; }
        ResourceHolder(ResourceHolder &&other) noexcept
        { swap(other); }
        ResourceHolder &operator=(ResourceHolder &&other) noexcept
        { swap(other); return *this; }

        /// Get unique identifier of this resource.
        const IdentifierT &id() const
        { return mId; }

        /// Get number of active references to this resource.
        const RefCounterT &refs() const
        { return mRefs; }

        /// Access the resource.
        ResT &res()
        { return mRes; }

        /// Access the resource.
        ResT &res() const
        { return const_cast<ResourceHolder*>(this)->res(); }
        // The resource can still be changed, even when holder is const!

        /// Has the resource been initialized?
        bool initialized() const
        { return mInitialized; }

        /// Mark this resource as initialized.
        void markInitialized()
        { mInitialized = true; }

        /**
         * Swap resource holders and their 
         * resources.
         */
        void swap(ResourceHolder &other) noexcept
        {
            using std::swap;
            swap(mId, other.mId);
            swap(mRefs, other.mRefs);
            swap(mRes, other.mRes);
            swap(mInitialized, other.mInitialized);
        }

        /// Swap resources between holders.
        friend void swap(ResourceHolder &first, ResourceHolder &second) noexcept
        { first.swap(second); }
    private:
        friend class BaseHandleMgr;

        /// Reset this resource holder to default state.
        void reset()
        {
            mId = IdentifierT();
            mRefs = 0u;
            mRes = ResT();
            mInitialized = false;
        }

        /// Unique identifier of the resource.
        IdentifierT mId{ };
        /// Number of references to this resource.
        RefCounterT mRefs{ 0u };
        /// Resource itself.
        ResT mRes{ };
        /// Has this resource been initialized?
        bool mInitialized{ false };
    protected:
    }; // struct ResourceHolder

    /// Result when getting resource from the handle.
    using HandleResult = ResourceHolder;
    /// Implementation of the resource handle methods.
    struct ResourceHandleImpl
    {
        /// Actual resource type, without holder.
        using ResourceT = ResT;

        /// Type of the holder used for wrapping the resource.
        using HolderT = ResourceHolder;

        /// Check whether the index is identifier for a valid buffer resource.
        static bool valid(ThisMgrT *mgr, const HandleIdxT &index);
        /// Get resource for given identifier or throw if resource does not exist.
        static ResourceT &get(ThisMgrT *mgr, const HandleIdxT &index);
        /// Get resource for given identifier or return default resource if it does not exist.
        static ResourceT &getOrDefault(ThisMgrT *mgr, const HandleIdxT &index);
        /// Get holder used for wrapping the resource.
        static const HolderT &getHolder(ThisMgrT *mgr, const HandleIdxT &index);
        /// Increase reference number of given resource.
        static void refIncr(ThisMgrT *mgr, const HandleIdxT &index);
        /// Decrease reference number of given resource.
        static void refDecr(ThisMgrT *mgr, const HandleIdxT &index);
    }; // struct ResourceHandleImpl
    /// Handle used by this manager, only allows access to const ResourceHolder!
    using ResourceHandle = ResourceHandle<ThisMgrT, ResourceHandleImpl, HandleResult, HandleIdxT>;
    /// Exception thrown when resource could not be found.
    using ResourceNotFoundException = typename ResourceHandle::ResourceNotFoundException;

    /// Exception thrown when trying to create already existing resource.
    struct ResourceAlreadyExistsException : public std::exception
    {
        ResourceAlreadyExistsException(const char *msg) :
            std::exception(msg)
        { }
    }; // struct ResourceAlreadyExistsException

    /// Free all resources.
    virtual ~BaseHandleMgr() = default;

    // No copying.
    BaseHandleMgr(const BaseHandleMgr &other) = delete;
    BaseHandleMgr &operator=(const BaseHandleMgr &other) = delete;
    // No moving.
    BaseHandleMgr(BaseHandleMgr &&other) = delete;
    BaseHandleMgr &operator=(BaseHandleMgr &&other) = delete;

    /**
     * Check whether provided index is index to a valid resource.
     * @param idx Index to be checked.
     * @return Returns true if the resource with such index 
     * already exists.
     */
    bool valid(const HandleIdxT &idx) const;

    /**
     * Check whether resource with given identifier exists.
     * @param id ID of the resource.
     * @return Returns true if resource with such ID exists.
     */
    bool exists(const IdentifierT &id) const;

    /**
     * Increment reference count for resource with provided 
     * index.
     * @param index Index of the resource.
     * @throws ResourceNotFoundException Thrown when resource 
     * with provided index does not exist.
     */
    void refIncr(const HandleIdxT &index);

    /**
     * Decrement reference count for resource with provided 
     * index.
     * @param index Index of the resource.
     * @throws ResourceNotFoundException Thrown when resource 
     * with provided index does not exist.
     */
    void refDecr(const HandleIdxT &index);

    /**
     * Perform book-keeping operation such as: 
     *   Finalize creation of resources.
     *   Free any resources which reached 0 references.
     * Part of update implementation should be a call to 
     * purgeZeroRefResources(), which performs the second 
     * point.
     */
    virtual void update() = 0;
private:
    /// Mapping from resource identifier to its index in the resource list.
    std::map<std::size_t, HandleIdxT> mResourceMap{ };
    /// List of existing resources.
    std::deque<ResourceHolder> mResources{ };
    /// Queue of indices in the resources list which were freed earlier.
    std::queue<HandleIdxT> mFreeIndices{ };
    /// List of resources with zero references since last update.
    std::vector<HandleIdxT> mZeroRefResources{ };

    /// Mutex used for locking structures for access from handles.
    std::mutex mMtx;

    /// Resource returned from the getOrDefault method when resource is not found.
    ResourceHolder mDefaultResource{ };
protected:
    /**
     * Initialize manager structure, creates an empty manager.
     */
    BaseHandleMgr();

    /**
     * Get resource with given index.
     * @return Returns reference to requested resource.
     * @throws ResourceNotFoundException Thrown when 
     * resource with provided index does not exist.
     */
    ResourceHolder &get(const HandleIdxT &idx);

    /**
     * Get resource with given index. If no such resource 
     * exists, this method returns reference to the default 
     * resource instead.
     * @return Returns reference to requested resource or 
     * the default resource if it does not exist.
     */
    ResourceHolder &getOrDefault(const HandleIdxT &idx);

    /**
     * Find index of a resource with given identifier and return it.
     * @param id Identifier of the target resource.
     * @retun Returns resource index or default-constructed handle 
     * index if resource with given identifier does not exist.
     */
    HandleIdxT findResourceIdx(const IdentifierT &id) const;

    /**
     * Find index of a resource with given identifier and return it.
     * @param idHash Hash of the identifier of the target resource.
     * @retun Returns resource index or default-constructed handle 
     * index if resource with given identifier does not exist.
     */
    HandleIdxT findResourceIdx(std::size_t idHash) const;

    /**
     * Find resource with given identifier and return * a pointer 
     * to it.
     * @param id Identifier of the target resource.
     * @return Returns pointer to the resource or nullptr if 
     * resource with given identifier does not exist.
     */
    const ResourceHolder *findResource(const IdentifierT &id) const;

    /**
     * Find resource with given identifier and return * a pointer 
     * to it.
     * @param id Identifier of the target resource.
     * @return Returns pointer to the resource or nullptr if 
     * resource with given identifier does not exist.
     */
    ResourceHolder *findResource(const IdentifierT &id);

    /**
     * Find resource with given identifier and return * a pointer 
     * to it.
     * @param idHash Hash of the identifier of the target resource.
     * @return Returns pointer to the resource or nullptr if 
     * resource with given identifier does not exist.
     */
    const ResourceHolder *findResource(std::size_t idHash) const;

    /**
     * Find resource with given identifier and return * a pointer 
     * to it.
     * @param idHash Hash of the identifier of the target resource.
     * @return Returns pointer to the resource or nullptr if 
     * resource with given identifier does not exist.
     */
    ResourceHolder *findResource(std::size_t idHash);

    /**
     * Get resource with given index.
     * @param idx Index of the requested buffer.
     * @return Returns pointer to the resource or nullptr if 
     * resource with given index does not exist.
     */
    const ResourceHolder *getResource(const HandleIdxT &idx) const;

    /**
     * Get resource with given index.
     * @param idx Index of the requested buffer.
     * @return Returns pointer to the resource or nullptr if 
     * resource with given index does not exist.
     */
    ResourceHolder *getResource(const HandleIdxT &idx);

    /**
     * Create a new resource and return its index.
     * @param id Identifier of the new resource, must be 
     * unique!
     * @return Returns index to the newly created resource.
     * @warning Does NOT check if resource with given 
     * identifier already exists.
     */
    HandleIdxT createResource(const IdentifierT &id);

    /**
     * Create hash from given identifier, usable in accessing 
     * the resource map.
     * @param id ID to create a hash for.
     */
    std::size_t hashFromIdentifier(const IdentifierT &id) const;

    /**
     * Clear all resources with zero references, since the 
     * last call to this method.
     */
    void purgeZeroRefResources();

    /**
     * Create instance of given manager type.
     * @param cArgs Constructor arguments.
     * @return Returns pointer to the new instance.
     */
    template <typename MgrT, typename... CArgTs>
    static std::shared_ptr<MgrT> createInstance(CArgTs... cArgs);

    /// Get begin iterator for all resources.
    auto begin() const
    { return mResources.begin(); }
    /// Get end iterator for all resources.
    auto end() const
    { return mResources.end(); }
}; // class BaseHandlerMgr

} // namespace res

} // namespace quark

// Template implementation.

namespace quark
{

namespace res
{

template <typename ResTT, typename HandleIdxTT, typename IdentifierTT, typename RefCounterTT>
bool BaseHandleMgr<ResTT, HandleIdxTT, IdentifierTT, RefCounterTT>::ResourceHandleImpl::valid(ThisMgrT *mgr,
    const HandleIdxT &index)
{ return mgr->valid(index); }

template <typename ResTT, typename HandleIdxTT, typename IdentifierTT, typename RefCounterTT>
typename BaseHandleMgr<ResTT, HandleIdxTT, IdentifierTT, RefCounterTT>::ResourceHandleImpl::ResourceT &BaseHandleMgr<ResTT,
HandleIdxTT, IdentifierTT, RefCounterTT>::ResourceHandleImpl::get(ThisMgrT *mgr, const HandleIdxT &index)
{ return mgr->get(index).res(); }

template <typename ResTT, typename HandleIdxTT, typename IdentifierTT, typename RefCounterTT>
typename BaseHandleMgr<ResTT, HandleIdxTT, IdentifierTT, RefCounterTT>::ResourceHandleImpl::ResourceT &BaseHandleMgr<ResTT,
HandleIdxTT, IdentifierTT, RefCounterTT>::ResourceHandleImpl::getOrDefault(ThisMgrT *mgr, const HandleIdxT &index)
{ return mgr->getOrDefault(index).res(); }

template <typename ResTT, typename HandleIdxTT, typename IdentifierTT, typename RefCounterTT>
const typename BaseHandleMgr<ResTT, HandleIdxTT, IdentifierTT, RefCounterTT>::ResourceHandleImpl::HolderT &BaseHandleMgr<
ResTT, HandleIdxTT, IdentifierTT, RefCounterTT>::ResourceHandleImpl::getHolder(ThisMgrT *mgr, const HandleIdxT &index)
{ return mgr->get(index); }

template <typename ResTT, typename HandleIdxTT, typename IdentifierTT, typename RefCounterTT>
void BaseHandleMgr<ResTT, HandleIdxTT, IdentifierTT, RefCounterTT>::ResourceHandleImpl::refIncr(ThisMgrT *mgr,
    const HandleIdxT &index)
{ return mgr->refIncr(index); }

template <typename ResTT, typename HandleIdxTT, typename IdentifierTT, typename RefCounterTT>
void BaseHandleMgr<ResTT, HandleIdxTT, IdentifierTT, RefCounterTT>::ResourceHandleImpl::refDecr(ThisMgrT *mgr,
    const HandleIdxT &index)
{ return mgr->refDecr(index); }

template <typename ResTT, typename HandleIdxTT, typename IdentifierTT, typename RefCounterTT>
BaseHandleMgr<ResTT, HandleIdxTT, IdentifierTT, RefCounterTT>::BaseHandleMgr()
{ /* Automatic */ }

template <typename ResTT, typename HandleIdxTT, typename IdentifierTT, typename RefCounterTT>
bool BaseHandleMgr<ResTT, HandleIdxTT, IdentifierTT, RefCounterTT>::valid(const HandleIdxT &idx) const
{
    // Indices are offset by one, since default-constructed (0u) is the invalid index.
    return idx <= mResources.size();
}

template <typename ResTT, typename HandleIdxTT, typename IdentifierTT, typename RefCounterTT>
bool BaseHandleMgr<ResTT, HandleIdxTT, IdentifierTT, RefCounterTT>::exists(const IdentifierT &id) const
{ return findResource(id) != nullptr; }

template <typename ResTT, typename HandleIdxTT, typename IdentifierTT, typename RefCounterTT>
void BaseHandleMgr<ResTT, HandleIdxTT, IdentifierTT, RefCounterTT>::refIncr(const HandleIdxT &index)
{
    const auto resPtr{ getResource(index) };
    if (resPtr == nullptr)
    { throw ResourceNotFoundException("Failed to increment reference counter: Resource with given index does not exist!"); }

    { // Lock the reference counter.
        std::lock_guard<std::mutex> l(mMtx);

        // We don't want to overflow.
        ASSERT_SLOW(resPtr->refs() != std::numeric_limits<RefCounterT>::max());
        ++resPtr->mRefs;
    } // Unlock the reference counter.
}

template <typename ResTT, typename HandleIdxTT, typename IdentifierTT, typename RefCounterTT>
void BaseHandleMgr<ResTT, HandleIdxTT, IdentifierTT, RefCounterTT>::refDecr(const HandleIdxT &index)
{
    const auto resPtr{ getResource(index) };
    if (resPtr == nullptr)
    { throw ResourceNotFoundException("Failed to decrement reference counter: Resource with given index does not exist!"); }

    { // Lock the reference counter.
        std::lock_guard<std::mutex> l(mMtx);

        if (resPtr->refs() != 0u)
        { // Underflow check.
            --resPtr->mRefs;

            if (resPtr->refs() == 0u)
            { // Last reference has been removed.
                // Mark it for later removal.
                mZeroRefResources.push_back(index);
            }
        }
    } // Unlock the reference counter.
}

template <typename ResTT, typename HandleIdxTT, typename IdentifierTT, typename RefCounterTT>
typename BaseHandleMgr<ResTT, HandleIdxTT, IdentifierTT, RefCounterTT>::ResourceHolder &BaseHandleMgr<ResTT,
HandleIdxTT, IdentifierTT, RefCounterTT>::get(const HandleIdxT &idx)
{
    ResourceHolder *resPtr{ getResource(idx) };
    if (resPtr == nullptr)
    { throw ResourceNotFoundException("Failed to get a resource: Resource with such index has not been found!"); }
    return *resPtr;
}

template <typename ResTT, typename HandleIdxTT, typename IdentifierTT, typename RefCounterTT>
typename BaseHandleMgr<ResTT, HandleIdxTT, IdentifierTT, RefCounterTT>::ResourceHolder &BaseHandleMgr<ResTT,
HandleIdxTT, IdentifierTT, RefCounterTT>::getOrDefault(const HandleIdxT &idx)
{
    ResourceHolder *resPtr{ getResource(idx) };
    if (resPtr == nullptr)
    { return mDefaultResource; }
    return *resPtr;
}

template <typename ResTT, typename HandleIdxTT, typename IdentifierTT, typename RefCounterTT>
typename BaseHandleMgr<ResTT, HandleIdxTT, IdentifierTT, RefCounterTT>::HandleIdxT BaseHandleMgr<ResTT, HandleIdxTT,
IdentifierTT, RefCounterTT>::findResourceIdx(const IdentifierT &id) const
{ return findResourceIdx(hashFromIdentifier(id)); }

template <typename ResTT, typename HandleIdxTT, typename IdentifierTT, typename RefCounterTT>
typename BaseHandleMgr<ResTT, HandleIdxTT, IdentifierTT, RefCounterTT>::HandleIdxT BaseHandleMgr<ResTT, HandleIdxTT,
IdentifierTT, RefCounterTT>::findResourceIdx(std::size_t idHash) const
{
    const auto findIt{ mResourceMap.find(idHash) };
    return findIt == mResourceMap.end() ? ResourceHandle::DefaultInvalidIndex : findIt->second;
}

template <typename ResTT, typename HandleIdxTT, typename IdentifierTT, typename RefCounterTT>
const typename BaseHandleMgr<ResTT, HandleIdxTT, IdentifierTT, RefCounterTT>::ResourceHolder *BaseHandleMgr<ResTT,
HandleIdxTT, IdentifierTT, RefCounterTT>::findResource(const IdentifierT &id) const
{ return findResource(hashFromIdentifier(id)); }

template <typename ResTT, typename HandleIdxTT, typename IdentifierTT, typename RefCounterTT>
const typename BaseHandleMgr<ResTT, HandleIdxTT, IdentifierTT, RefCounterTT>::ResourceHolder *BaseHandleMgr<ResTT,
HandleIdxTT, IdentifierTT, RefCounterTT>::findResource(std::size_t idHash) const
{
    const auto findIt{ mResourceMap.find(idHash) };
    if (findIt == mResourceMap.end())
    { // Resource not found...
        return nullptr;
    }
    const auto idx{ findIt->second };

    return getResource(idx);
}

template <typename ResTT, typename HandleIdxTT, typename IdentifierTT, typename RefCounterTT>
typename BaseHandleMgr<ResTT, HandleIdxTT, IdentifierTT, RefCounterTT>::ResourceHolder * BaseHandleMgr<ResTT,
HandleIdxTT, IdentifierTT, RefCounterTT>::findResource(std::size_t idHash)
{
    // Preventing code duplication with const version of this method.
    const ThisMgrT *constThis{ this };
    const ResourceHolder *constResult{ constThis->findResource(idHash) };
    return const_cast<ResourceHolder*>(constResult);
}

template <typename ResTT, typename HandleIdxTT, typename IdentifierTT, typename RefCounterTT>
typename BaseHandleMgr<ResTT, HandleIdxTT, IdentifierTT, RefCounterTT>::ResourceHolder *BaseHandleMgr<ResTT,
HandleIdxTT, IdentifierTT, RefCounterTT>::findResource(const IdentifierT &id)
{
    // Preventing code duplication with const version of this method.
    const ThisMgrT *constThis{ this };
    const ResourceHolder *constResult{ constThis->findResource(id) };
    return const_cast<ResourceHolder*>(constResult);
}

template <typename ResTT, typename HandleIdxTT, typename IdentifierTT, typename RefCounterTT>
const typename BaseHandleMgr<ResTT, HandleIdxTT, IdentifierTT, RefCounterTT>::ResourceHolder *BaseHandleMgr<ResTT,
HandleIdxTT, IdentifierTT, RefCounterTT>::getResource(const HandleIdxT &idx) const
{
    if (!ResourceHandle::idxValid(idx) || idx > mResources.size())
    { // Resource not found...
        return nullptr;
    }

    // Remove the offset when accessing array.
    return &(mResources[idx - 1u]);
}

template <typename ResTT, typename HandleIdxTT, typename IdentifierTT, typename RefCounterTT>
typename BaseHandleMgr<ResTT, HandleIdxTT, IdentifierTT, RefCounterTT>::ResourceHolder *BaseHandleMgr<ResTT,
HandleIdxTT, IdentifierTT, RefCounterTT>::getResource(const HandleIdxT &idx)
{
    // Preventing code duplication with const version of this method.
    const ThisMgrT *constThis{ this };
    const ResourceHolder *constResult{ constThis->getResource(idx) };
    return const_cast<ResourceHolder*>(constResult);
}

template <typename ResTT, typename HandleIdxTT, typename IdentifierTT, typename RefCounterTT>
typename BaseHandleMgr<ResTT, HandleIdxTT, IdentifierTT, RefCounterTT>::HandleIdxT BaseHandleMgr<ResTT, HandleIdxTT,
IdentifierTT, RefCounterTT>::createResource(const IdentifierT &id)
{
    if (!mFreeIndices.empty())
    { // There are free resource indices -> reuse them!
        // Recover the index.
        const auto resIdx{ mFreeIndices.front() };
        // And remove it from the queue.
        mFreeIndices.pop();

        return resIdx;
    }
    // Else: We need to create a new resource.

    // Create the new resource.
    ASSERT_SLOW(mResources.size() < std::numeric_limits<HandleIdxT>::max());
    mResources.emplace_back( ResourceHolder{ id } );
    // Generate the new index, size() is correct since we offset indices by +1.
    auto handleIdx{ static_cast<HandleIdxT>(mResources.size()) };
    // Map resource identifier to the resource.
    mResourceMap.emplace(std::make_pair(hashFromIdentifier(id), handleIdx));
    // Finally - return the index to the new resource.
    return handleIdx;
}

template <typename ResTT, typename HandleIdxTT, typename IdentifierTT, typename RefCounterTT>
std::size_t BaseHandleMgr<ResTT, HandleIdxTT, IdentifierTT, RefCounterTT>::hashFromIdentifier(const IdentifierT &id) const
{ return std::hash<IdentifierT>{}(id); }

template <typename ResTT, typename HandleIdxTT, typename IdentifierTT, typename RefCounterTT>
void BaseHandleMgr<ResTT, HandleIdxTT, IdentifierTT, RefCounterTT>::purgeZeroRefResources()
{
    for (const auto &idx : mZeroRefResources)
    { // For every zero reference resource
        // Get the target resource.
        auto res{ getResource(idx) };
        // Remove its mapping.
        mResourceMap.erase(hashFromIdentifier(res->id()));
        // Reset it 
        getResource(idx)->reset();
        // and allow its later reuse.
        mFreeIndices.push(idx);
    }
    mZeroRefResources.clear();
}

template <typename ResTT, typename HandleIdxTT, typename IdentifierTT, typename RefCounterTT>
template <typename MgrT, typename ... CArgTs>
std::shared_ptr<MgrT> BaseHandleMgr<ResTT, HandleIdxTT, IdentifierTT, RefCounterTT>::createInstance(CArgTs... cArgs)
{
    /*
     * Since only constructor is private and make_shared 
     * does not have access to it, we create a proxy.
     * Based on: https://stackoverflow.com/a/25069711
     */

    struct SharedEnabler : public MgrT { };
    return std::make_shared<SharedEnabler>(std::forward<CArgTs>(cArgs)...);
}

}

}

// Template implementation end.
