/**
 * @file resources/win32/WinFile.h
 * @author Tomas Polasek
 * @brief Windows file abstraction.
 */

#pragma once

#include "engine/resources/BaseFile.h"

/// Namespace containing the engine code.
namespace quark
{

/// Namespace containing resource management helpers.
namespace res
{

/// Windows resource management.
namespace win
{

/// Implementation of win32 specific features.
namespace impl
{

/**
 * Implementation of filesystem specific functions 
 * for Windows.
 */
struct WinFileImpl
{
    /**
     * Check whether a file exists.
     * @param filename Name of the file including path.
     * @return Returns true if the file exists.
     */
    static bool checkExists(const char *filename);

    /**
     * Check whether a file exists.
     * @param filename Name of the file including path.
     * @return Returns true if the file exists.
     */
    static bool checkExists(const wchar_t *filename);
}; // struct WinFileImpl

} // namespace impl

/// Helper type for windows string filename.
using WinFileS = ::quark::res::BaseFile<std::string, ::quark::res::win::impl::WinFileImpl, '\\'>;
/// Helper type for windows wstring filename.
using WinFileW = ::quark::res::BaseFile<std::wstring, ::quark::res::win::impl::WinFileImpl, L'\\'>;

} // namespace win

} // namespace res

} // namespace quark

// Template implementation

namespace quark
{

namespace res
{

namespace win
{


}

}

}

// Template implementation end