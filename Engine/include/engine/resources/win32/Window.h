/**
 * @file resources/win32/Window.h
 * @author Tomas Polasek
 * @brief Windows window handling class.
 */

#pragma once

#include "engine/resources/BaseResource.h"

#include "engine/resources/win32/WindowClass.h"

/// Namespace containing the engine code.
namespace quark
{

/// Namespace containing resource management helpers.
namespace res
{

/// Windows resource management.
namespace win
{

/// Resource wrapper around WIN32 window.
class Window : public BaseResource, public util::PointerType<Window>
{
public:
    /// User-defined messages.
    enum class WindowMessage : uint16_t
    {
        /// Unknown user message.
        Unknown = WM_USER, 
        /// Message requesting the window to be visible.
        Show,
        /// Message requesting the window to be invisible.
        Hide,
        /// Message requesting the window to maximize.
        Maximize,
        /// Message requesting the window to restore from maximization.
        Restore,
        /// Message requesting to resize the window to a new size. wParam = (width, height)
        Resize,
        /// Last user message type.
        Last, 
    }; // enum class WindowMessages

    /// Convert message type to ID.
    static constexpr uint16_t windowMsgToId(WindowMessage msg)
    { return static_cast<uint16_t>(msg); }

    /**
     * Create a new window with provided parameters.
     * @param hInstance Handle to the instance of a module to be associated 
     * with this window.
     * @param width Preferred window area width in pixels.
     * @param height Preferred window area height in pixels.
     * @param name Name of the window, will be displayed as the title text.
     * @param windowClass Registered window class used by this window.
     * @param ptr Pointer passed to the window message handling procedure.
     */
    static PtrT create(HINSTANCE hInstance, uint32_t width, uint32_t height, 
        const std::wstring &name, const WindowClass &windowClass, 
        LPVOID ptr);

    /**
     * Use pre-set parameters and create the window.
     * @warning This should be ran on the same thread 
     * as the potential message pump!
     */
    void initialize();

    /// Free resources, destroy window.
    virtual ~Window()
    { destroy(); }

    Window(const Window &other) = delete;
    Window(Window &&other) = delete;
    Window &operator=(const Window &other) = delete;
    Window &operator=(Window &&other) = delete;

    /// Get handle to the instance of a module associated with this window.
    HINSTANCE hInstance() const
    { return mHInstance; }

    /// Get the raw window handle.
    HWND hwnd() const
    { return mHandle; }

    /// Get the current width of the drawing area of this window.
    uint32_t clientWidth() const
    { return mClientWidth; }

    /// Get the current height of the drawing area of this window.
    uint32_t clientHeight() const
    { return mClientHeight; }

    /// Is the window currently fullscreen?
    bool isFullscreen() const
    { return mFullscreen; }

    /**
     * Request window, running on different thread, to 
     * set fullscreen state to given value.
     * @param value Set to true for fullscreen.
     */
    void sendFullscreenMessage(bool value);

    /**
     * Request window, running on different thread, to 
     * set visibility state to given value.
     * @param value Set to true for visible.
     */
    void sendVisibleMessage(bool value);

    /**
     * Request window, running on different thread, to 
     * set a new window size.
     * @param width New width of the window.
     * @param height New height of the window.
     */
    void sendResizeMessage(uint16_t width, uint16_t height);

    /**
     * Set window to fullscreen or windowed according to 
     * specified value.
     * @param value Set to true for fullscreen or false 
     * for windowed.
     * @warning Should be used only on the thread which 
     * created the window!
     */ 
    void setFullscreen(bool value);

    /**
     * Resize the window without changing its position.
     * @param width New width of the window.
     * @param height New height of the window.
     */
    void resize(uint16_t width, uint16_t height);

    /**
     * Toggle between fullscreen and windowed mode.
     * @warning Should be used only on the thread which 
     * created the window!
     */
    void toggleFullscreen()
    { setFullscreen(!mFullscreen); }

    /// Is the window currently visible?
    bool isVisible() const
    { return mVisible; }

    /**
     * Set the window visible or hidden, depending of 
     * specified value.
     * @param value Set to true for making the window 
     * visible, or false for hiding the window.
     * @warning Should be used only on the thread which 
     * created the window!
     */
    void setVisible(bool value);

    /**
     * Toggle between visible and hidden window.
     * @warning Should be used only on the thread which 
     * created the window!
     */
    void toggleVisible()
    { setVisible(!mVisible); }

    /// Destroy this window.
    void destroy();

    /// Notify the window that it should quit.
    void sendExitMessage();
private:
    /// Window style when using windowed mode.
    static constexpr DWORD WINDOWED_WINDOW_STYLE{ WS_OVERLAPPEDWINDOW };
    /// Window style when using fullscreen mode.
    static constexpr DWORD FULLSCREEN_WINDOW_STYLE{ WS_OVERLAPPED };

    /**
     * Get size of monitor which contains specified window.
     * @param hwnd Handle to the window.
     * @param searchFlag Flag specifying how to find the monitor.
     * @return Returns the size of the monitor, which can be zero 
     * if the fetching fails.
     */
    static RECT getMonitorSize(HWND hwnd, DWORD searchFlag);

    /**
     * Create a new window with provided parameters. The window will be 
     * visible and using the WINDOWED_WINDOW_STYLE.
     * @param hInstance Handle to the instance of a module to be associated 
     * with this window.
     * @param width Preferred window area width in pixels.
     * @param height Preferred window area height in pixels.
     * @param name Name of the window, will be displayed as the title text.
     * @param className Name of the base class for this window, needs to be 
     * already created.
     * @param ptr Pointer passed to the window message handling procedure.
     * @return Returns handle to the newly created window.
     */
    static HWND createWindow(HINSTANCE hInstance, uint32_t width, uint32_t height, 
        const std::wstring &name, const std::wstring &className, 
        LPVOID ptr);

    /**
     * Get window dimensions of this window and store them in 
     * provided variable.
     * @param rect Pointer to the target rectangle, where the 
     * dimensions will be stored.
     */
    void getWindowRect(LPRECT rect) const;

    /// Make the window visible.
    void showWindow() const;

    /// Make the window hidden.
    void hideWindow() const;

    /**
     * Switch the window to windowed mode.
     * @param dimensions Size of the window.
     */
    void makeWindowed(const RECT &dimensions) const;

    /**
     * Switch the window to fullscreen mode.
     * @param dimensions Storage for keeping the 
     * original dimensions of the window.
     */
    void makeFullscreen(RECT &dimensions) const;

    /// Handle to the instance of a module associated with this window.
    HINSTANCE mHInstance{ nullptr };
    /// Width of the drawing area of the window.
    uint32_t mClientWidth{ 0u };
    /// Height of the drawing area of the window.
    uint32_t mClientHeight{ 0u };
    /// Window name.
    std::wstring mName{ L"" };
    /// Name of the window class used.
    std::wstring mClassName{ L"" };
    /// Data passed on window creation.
    LPVOID mDataPtr{ nullptr };

    /// Is the window currently visible?
    bool mVisible{ false };
    /// Is the window currently fullscreen?
    bool mFullscreen{ false };

    /**
     * Dimensions of the window when in windowed mode, used when 
     * switching from and to fullscreen mode to restore the original 
     * size.
     */
    RECT mWindowedRect{ };

    /// Inner handle for the window.
    HWND mHandle{ nullptr };
protected:
    /**
     * Create a new window with provided parameters.
     * @param hInstance Handle to the instance of a module to be associated 
     * with this window.
     * @param width Preferred window area width in pixels.
     * @param height Preferred window area height in pixels.
     * @param name Name of the window, will be displayed as the title text.
     * @param windowClass Registered window class used by this window.
     * @param ptr Pointer passed to the window message handling procedure.
     */
    Window(HINSTANCE hInstance, uint32_t width, uint32_t height, 
        const std::wstring &name, const WindowClass &windowClass, 
        LPVOID ptr);
}; // class Window

} // namespace win

} // namespace res

} // namespace quark
