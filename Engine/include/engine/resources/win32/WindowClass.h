/**
 * @file resources/win32/WindowClass.h
 * @author Tomas Polasek
 * @brief Windows window handling class.
 */

#pragma once

#include "engine/resources/BaseResource.h"

/// Namespace containing the engine code.
namespace quark
{

/// Namespace containing resource management helpers.
namespace res
{

/// Windows resource management.
namespace win
{

/// Window class registrator.
class WindowClass : public BaseResource, public util::PointerType<WindowClass>
{
public:
    /**
     * Register window class with specified information.
     * This version uses default values for missing parameters.
     * @param hInstance Handle to the instance that contains 
     * the window procedure.
     * @param className Name of the window class, must be 
     * unique!
     * @param wndProc Message handling procedure.
     */
    static PtrT create(::HINSTANCE hInstance, const std::wstring &className,
        ::WNDPROC wndProc);

    /**
     * Register window class with specified information.
     * @param hInstance Handle to the instance that contains 
     * the window procedure.
     * @param className Name of the window class, must be 
     * unique!
     * @param wndProc Message handling procedure.
     * @param appIcon Default icon for windows.
     * @param cursor Default cursor for windows.
     * @param background Default background color for windows.
     * @param classIcon Icon for this window class.
     */
    static PtrT create(::HINSTANCE hInstance, const std::wstring &className, 
        ::WNDPROC wndProc, ::HICON appIcon, ::HCURSOR cursor, 
        ::HBRUSH background, ::HICON classIcon);

    // Does NOT unregister the class!
    virtual ~WindowClass() = default;

    // No copying
    WindowClass(const WindowClass &other) = delete;
    WindowClass &operator=(const WindowClass &other) = delete;
    // No moving.
    WindowClass(WindowClass &&other) = delete;
    WindowClass &operator=(WindowClass &&other) = delete;

    /// Get default icon, usable in the constructor.
    static ::HICON defaultIcon(::HINSTANCE hInstance)
    { return ::LoadIcon(hInstance, nullptr); }

    /// Get default cursor usable in the constructor.
    static ::HCURSOR defaultCursor()
    { return ::LoadCursor(nullptr, IDC_ARROW); }

    /// Convert color to brush, usable in the constructor.
    static ::HBRUSH colorToBrush(::COLOR16 color = COLOR_WINDOW)
    { return ::GetSysColorBrush(color); }

    /// Get the class name registered by this object.
    const std::wstring &className() const
    { return mClassName; }
private:
    /// Storage for the class name registered by this object.
    const std::wstring mClassName;
protected:
    /**
     * Register window class with specified information.
     * This version uses default values for missing parameters.
     * @param hInstance Handle to the instance that contains 
     * the window procedure.
     * @param className Name of the window class, must be 
     * unique!
     * @param wndProc Message handling procedure.
     */
    WindowClass(::HINSTANCE hInstance, const std::wstring &className,
        ::WNDPROC wndProc);

    /**
     * Register window class with specified information.
     * @param hInstance Handle to the instance that contains 
     * the window procedure.
     * @param className Name of the window class, must be 
     * unique!
     * @param wndProc Message handling procedure.
     * @param appIcon Default icon for windows.
     * @param cursor Default cursor for windows.
     * @param background Default background color for windows.
     * @param classIcon Icon for this window class.
     */
    WindowClass(::HINSTANCE hInstance, const std::wstring &className, 
        ::WNDPROC wndProc, ::HICON appIcon, ::HCURSOR cursor, 
        ::HBRUSH background, ::HICON classIcon);
}; // class WindowClass

} // namespace win

} // namespace res

} // namespace quark
