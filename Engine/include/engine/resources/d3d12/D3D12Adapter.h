/**
 * @file resources/d3d12/D3D12Adapter.h
 * @author Tomas Polasek
 * @brief Direct3D 12 adapter wrapper.
 */

#pragma once

#include "engine/resources/BaseResource.h"

#include "engine/resources/d3d12/D3D12DXGIFactory.h"

/// Namespace containing the engine code.
namespace quark
{

/// Namespace containing resource management helpers.
namespace res
{

/// Direct3D 12 resource management.
namespace d3d12
{

/// Resource wrapper for Direct3D 12 adapter.
class D3D12Adapter : public BaseResource, public util::PointerType<D3D12Adapter>
{
public:

#ifdef ENGINE_RAY_TRACING_ENABLED
    using D3D12AdapterT = ::IDXGIAdapter4;
#else
    using D3D12AdapterT = ::IDXGIAdapter2;
#endif

    /**
     * Choose a suitable Direct3D 12 adapter. The main parameter for 
     * choosing the adapter is presence of requested features. Second 
     * parameter used for choosing the adapter is available video memory, 
     * where the more memory adapter has the more powerful it probably is. 
     * If useWarp is set to true, then instead of choosing physical 
     * adapter a virtual Windows Advanced Rasterization Platform adapter 
     * is chosen.
     * @param factory Factory used for the creation of objects.
     * @param useWarp Whether Windows Advanced Rasterization Platform 
     * should be used.
     * @param features Required features to be supported by the chosen adapter.
     * @param enableExperimental Whether enabling of experimental features 
     * should be attempted. Enabling these features is prerequisite for 
     * ray tracing!
     * @throws Throws util::winexception containing message about what 
     * went wrong.
     */
    static PtrT create(D3D12DXGIFactory &factory, bool useWarp, 
        ::D3D_FEATURE_LEVEL features, bool enableExperimental);

    /// Free resources used by this object.
    virtual ~D3D12Adapter();

    // No copying.
    D3D12Adapter(const D3D12Adapter &other) = delete;
    D3D12Adapter &operator=(const D3D12Adapter &rhs) = delete;

    // Allow moving.
    D3D12Adapter(D3D12Adapter &&other) = default;
    D3D12Adapter &operator=(D3D12Adapter &&rhs) = default;

    /// Get the inner pointer.
    auto &getPtr() const
    { return mAdapter; }

    /// Get the inner adapter.
    auto *get() const
    { return mAdapter.Get(); }

    /// Access the adapter.
    auto &operator->() const
    { return mAdapter; }

    /// Are experimental features supported by this adapter?
    bool experimentalFeaturesSupported() const
    { return mExperimentalFeaturesSupported; }

    /// Get name of the device.
    std::wstring deviceName() const;

    /// Get size of devices memory in MB.
    std::size_t deviceMemory() const;

    /// Print information about specified adapter.
    static void printAdapterInfo(const ::DXGI_ADAPTER_DESC1 &adapterDesc);
private:
    /**
     * Attempt to enable experimental features for given adapter 
     * and return whether it was successful.
     * @param adapter Adapter to enable the features for.
     */
    bool enableExperimentalFeatures(const ComPtr<D3D12AdapterT> &adapter);

    /// Inner adapter pointer.
    ComPtr<D3D12AdapterT> mAdapter{ nullptr };
    /// Are experimental features supported by this adapter?
    bool mExperimentalFeaturesSupported{ false };
protected:
    /// Empty wrapper.
    D3D12Adapter() = default;

    /**
     * Choose a suitable Direct3D 12 adapter. The main parameter for 
     * choosing the adapter is presence of requested features. Second 
     * parameter used for choosing the adapter is available video memory, 
     * where the more memory adapter has the more powerful it probably is. 
     * If useWarp is set to true, then instead of choosing physical 
     * adapter a virtual Windows Advanced Rasterization Platform adapter 
     * is chosen.
     * @param factory Factory used for the creation of objects.
     * @param useWarp Whether Windows Advanced Rasterization Platform 
     * should be used.
     * @param features Required features to be supported by the chosen adapter.
     * @param enableExperimental Whether enabling of experimental features 
     * should be attempted. Enabling these features is prerequisite for 
     * ray tracing!
     * @throws Throws util::winexception containing message about what 
     * went wrong.
     */
    D3D12Adapter(D3D12DXGIFactory &factory, bool useWarp, 
        ::D3D_FEATURE_LEVEL features, bool enableExperimental);
}; // class D3D12Adapter

} // namespace d3d12

} // namespace res

} // namespace quark
