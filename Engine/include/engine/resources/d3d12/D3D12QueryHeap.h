/**
 * @file resources/d3d12/D3D12QueryHeap.h
 * @author Tomas Polasek
 * @brief Wrapper around Direct3D 12 query heap.
 */

#pragma once

#include "engine/resources/BaseResource.h"

#include "engine/resources/d3d12/D3D12Device.h"

/// Namespace containing the engine code.
namespace quark
{

/// Namespace containing resource management helpers.
namespace res
{

/// Direct3D 12 resource management.
namespace d3d12
{

/**
 * Wrapper around a Direct3D 12 query heap. 
 */
class D3D12QueryHeap : public BaseResource, public util::PointerType<D3D12QueryHeap>
{
public:
    using D3D12QueryHeapT = ::ID3D12QueryHeap;

    /**
     * Create a query heap according to the provided 
     * description structure.
     * @param device Device to create the query heap on.
     * @param desc Description of the requested query
     * heap.
     */
    static PtrT create(D3D12Device &device, const ::D3D12_QUERY_HEAP_DESC &desc);

    /// Query heap is automatically released.
    virtual ~D3D12QueryHeap();

    // No copying, allow moving.
    D3D12QueryHeap(const D3D12QueryHeap &other) = delete;
    D3D12QueryHeap &operator=(const D3D12QueryHeap &other) = delete;
    D3D12QueryHeap(D3D12QueryHeap &&other) = default;
    D3D12QueryHeap &operator=(D3D12QueryHeap &&other) = default;

    /// Get the inner pointer.
    auto &getPtr() const
    { return mQueryHeap; }

    /// Get the inner query heap.
    auto *get() const
    { return mQueryHeap.Get(); }

    /// Access the query heap.
    auto &operator->() const
    { return mQueryHeap; }
private:
    /**
     * Create query heap on given device.
     * @param device Device to create the query heap on.
     * @param desc Requested query heap specification.
     * @throws Throws util::winexception on error.
     */
    void createQueryHeap(D3D12Device &device, 
        const ::D3D12_QUERY_HEAP_DESC &desc);

    /// The inner query heap pointer.
    ComPtr<D3D12QueryHeapT> mQueryHeap;
protected:
    /// Create unallocated query heap.
    D3D12QueryHeap();

    /**
     * Create a query heap according to the provided 
     * description structure.
     * @param device Device to create the query heap on.
     * @param desc Description of the requested query
     * heap.
     */
    D3D12QueryHeap(D3D12Device &device, const ::D3D12_QUERY_HEAP_DESC &desc);
}; // class D3D12Heap

} // namespace d3d12

} // namespace res

} // namespace quark
