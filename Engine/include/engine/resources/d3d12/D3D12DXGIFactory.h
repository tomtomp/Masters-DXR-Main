/**
 * @file resources/d3d12/D3D12DXGIFactory.h
 * @author Tomas Polasek
 * @brief Wrapper around DXGI factory.
 */

#pragma once

#include "engine/resources/BaseResource.h"

#include "engine/util/SafeD3D12.h"

/// Namespace containing the engine code.
namespace quark
{

/// Namespace containing resource management helpers.
namespace res
{

/// Direct3D 12 resource management.
namespace d3d12
{

/// Resource wrapper for DXGI factory.
class D3D12DXGIFactory : public BaseResource, public util::PointerType<D3D12DXGIFactory>
{
public:
#ifdef ENGINE_RAY_TRACING_ENABLED
    using DXGIFactoryT = ::IDXGIFactory6;
#else
    using DXGIFactoryT = ::IDXGIFactory5;
#endif
    /**
     * Create IDXGIFactory, includes debug flags, if _DEBUG macro is set.
     * If _DEBUG is set, this constructor also 
     * @throw Throws util::winexception if any error occurs. The contained 
     * message details what happened.
     */
    static PtrT create();

    /// Free resources used by this object.
    virtual ~D3D12DXGIFactory() = default;

    // No copying.
    D3D12DXGIFactory(const D3D12DXGIFactory &other) = delete;
    D3D12DXGIFactory &operator=(const D3D12DXGIFactory &rhs) = delete;

    // No moving.
    D3D12DXGIFactory(D3D12DXGIFactory &&other) = default;
    D3D12DXGIFactory &operator=(D3D12DXGIFactory &&rhs) = default;

    /**
     * Check driver for a specified feature. 
     * @param feature Which feature should be checked.
     * @return Returns whether the driver supports given feature.
     */
    bool isFeatureSupported(DXGI_FEATURE feature) const;

    /**
     * Check driver for tearing support. Tearing is required for variable 
     * refresh rate displays - FreeSync and G-Sync.
     * @return Returns whether the driver supports tearing.
     */
    bool isTearingSupported() const
    { return isFeatureSupported(DXGI_FEATURE_PRESENT_ALLOW_TEARING); }

    /**
     * Disable some of message handling done by DXGI.
     * @param hwnd Window which will have handling disabled.
     * @param flags Which messages will not be handled by 
     * DXGI - e.g. DXGI_MWA_NO_ALT_ENTER.
     * @throw Throws util::winexception if any error occurs. The contained 
     * message details what happened.
     */
    void setMessageHandling(HWND hwnd, UINT flags);

    /// Get the inner pointer.
    auto &getPtr() const
    { return mFactory; }

    /// Get the inner factory.
    auto *get() const
    { return mFactory.Get(); }

    /// Access the factory.
    auto &operator->() const
    { return mFactory; }
private:
    /// Inner pointer.
    ComPtr<DXGIFactoryT> mFactory{ nullptr };
protected:
    /**
     * Create IDXGIFactory, includes debug flags, if _DEBUG macro is set.
     * If _DEBUG is set, this constructor also 
     * @throw Throws util::winexception if any error occurs. The contained 
     * message details what happened.
     */
    D3D12DXGIFactory();
}; // class D3D12DXGIFactory

} // namespace d3d12

} // namespace res

} // namespace quark
