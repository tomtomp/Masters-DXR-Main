/**
 * @file resources/d3d12/D3D12RayTracingCommandList.h
 * @author Tomas Polasek
 * @brief Wrapper for D3D12 command list usable in ray tracing.
 */

#pragma once

#include "engine/resources/d3d12/D3D12CommandQueueMgr.h"

#ifdef ENGINE_RAY_TRACING_ENABLED
#   include "engine/lib/DxrFallback.h"
#endif

/// Namespace containing the engine code.
namespace quark
{

/// Namespace containing resource management helpers.
namespace res
{

/// Direct3D 12 resource management.
namespace d3d12
{

/// Wrapper for D3D12 command list usable in ray tracing.
class D3D12RayTracingCommandList : public BaseResource, public util::PointerType<D3D12RayTracingCommandList>
{
public:
#ifdef ENGINE_RAY_TRACING_ENABLED
    using D3D12FallbackCommandListT = ::ID3D12RaytracingFallbackCommandList;
#else
    using D3D12FallbackCommandListT = D3D12CommandList::D3D12GraphicsCommandList2T;
#endif

    /**
     * Create ray tracing command list wrapper without using fallback 
     * command list.
     * @param cmdList Normal command list.
     */
    static PtrT create(res::d3d12::D3D12CommandList &cmdList);

#ifdef ENGINE_RAY_TRACING_ENABLED
    /**
     * Create ray tracing command list wrapper using both fallback 
     * command list and the standard one.
     * @param cmdList Normal command list.
     * @param fallbackCmdList The fallback layer command list.
     */
    static PtrT create(res::d3d12::D3D12CommandList &cmdList, 
        ComPtr<D3D12FallbackCommandListT> fallbackCmdList);
#endif

    /// Free the ray tracing command list, leaving the original untouched.
    virtual ~D3D12RayTracingCommandList()
    { /* Automatic */ }

    // Allow copying and moving.
    D3D12RayTracingCommandList(const D3D12RayTracingCommandList &other) = default;
    D3D12RayTracingCommandList &operator=(const D3D12RayTracingCommandList &other) = default;
    D3D12RayTracingCommandList(D3D12RayTracingCommandList &&other) = default;
    D3D12RayTracingCommandList &operator=(D3D12RayTracingCommandList &&other) = default;

    /**
     * Pass currently used command list to the provided generic 
     * lambda.
     * @param fun Generic function/lambda.
     * @return Returns the same value as provided function.
     * @usage apply([](auto *cmdLis){ cmdList->BuildRaytracingAccelerationStructure(...); })
     * @throws util::winexception Thrown on error.
     */
    template <typename FunT>
    typename std::result_of<FunT(D3D12FallbackCommandListT*)>::type apply(FunT fun);

    /**
     * Access the standard command list hidden within.
     * @return Returns reference to the inner command list.
     * @throws std::runtime_error Thrown if no command list is set.
     */
    res::d3d12::D3D12CommandList &cmdList();

#ifdef ENGINE_RAY_TRACING_ENABLED
    /**
     * Access the fallback command list hidden within.
     * @return Returns reference to the inner fallback command list.
     * @throws std::runtime_error Thrown if no command list is set.
     */
    D3D12FallbackCommandListT *fallbackCmdList();

    /**
     * Set the top level acceleration structure for ray tracing 
     * operations.
     * This method automatically chooses whether to use fallback 
     * or hardware.
     * @param rootParameterIndex Index of the root parameter which 
     * is used to access the acceleration structure.
     * @param gpuPointer GPU pointer to the acceleration structure.
     */
    void setTopLevelAccelerationStructure(::UINT rootParameterIndex, 
        const ::WRAPPED_GPU_POINTER &gpuPointer);
#endif
private:
    /// Normal command list
    res::d3d12::D3D12CommandList *mCmdList{ nullptr };
#ifdef ENGINE_RAY_TRACING_ENABLED
    /// Special fallback command list.
    ComPtr<D3D12FallbackCommandListT> mFallbackCmdList{ nullptr };
#endif
protected:
    /// Creates unbound command list.
    D3D12RayTracingCommandList();

    /**
     * Create ray tracing command list wrapper without using fallback 
     * command list.
     * @param cmdList Normal command list.
     */
    D3D12RayTracingCommandList(res::d3d12::D3D12CommandList &cmdList);

#ifdef ENGINE_RAY_TRACING_ENABLED
    /**
     * Create ray tracing command list wrapper using both fallback 
     * command list and the standard one.
     * @param cmdList Normal command list.
     * @param fallbackCmdList The fallback layer command list.
     */
    D3D12RayTracingCommandList(res::d3d12::D3D12CommandList &cmdList, 
        ComPtr<D3D12FallbackCommandListT> fallbackCmdList);
#endif
}; // class D3D12RayTracingCommandList

} // namespace d3d12

} // namespace res

} // namespace quark

// Template implementation.

namespace quark
{

namespace res
{

namespace d3d12
{

template <typename FunT>
typename std::result_of<FunT(D3D12RayTracingCommandList::D3D12FallbackCommandListT*)>::type 
D3D12RayTracingCommandList::apply(FunT fun)
{
#ifdef ENGINE_RAY_TRACING_ENABLED
    if (mFallbackCmdList)
    { return fun(mFallbackCmdList.Get()); }
    if (mCmdList)
    { return fun(mCmdList->list4().Get()); }
#else // ENGINE_RAY_TRACING_ENABLED
    if (mCmdList)
    { return fun(mCmdList->get()); }
#endif // ENGINE_RAY_TRACING_ENABLED
    else
    { throw util::winexception("No command list available!"); }
}

}

}

}

// Template implementation end.
