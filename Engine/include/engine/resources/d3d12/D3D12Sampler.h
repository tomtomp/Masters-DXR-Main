/**
 * @file resources/d3d12/D3D12Sampler.h
 * @author Tomas Polasek
 * @brief Wrapper around a Direct3D 12 sampler.
 */

#pragma once

#include "engine/resources/BaseResource.h"

#include "engine/resources/d3d12/D3D12DescHeap.h"

/// Namespace containing the engine code.
namespace quark
{

/// Namespace containing resource management helpers.
namespace res
{

/// Direct3D 12 resource management.
namespace d3d12
{

/// Wrapper around Direct3D 12 sampler.
class D3D12Sampler : public BaseResource
{
public:
    /// Uninitialized sampler.
    D3D12Sampler();

    /// Free resources.
    virtual ~D3D12Sampler();

    /**
     * Create the sampler using provided device and on 
     * descriptor heap belonging to that device.
     * @param descHeap Descriptor heap to create the 
     * sampler on. 
     * @param desc Description of the requested sampler.
     */
    D3D12Sampler(d3d12::D3D12DescHeap &descHeap, const ::D3D12_SAMPLER_DESC &desc);

    // No copying.
    D3D12Sampler(const D3D12Sampler &other) = delete;
    D3D12Sampler &operator=(const D3D12Sampler &rhs) = delete;

    // Allow moving.
    D3D12Sampler(D3D12Sampler &&other) = default;
    D3D12Sampler &operator=(D3D12Sampler &&rhs) = default;

    /**
     * Create the sampler using provided device and on 
     * descriptor heap belonging to that device.
     * @param descHeap Descriptor heap to create the 
     * sampler on. 
     * @param desc Description of the requested sampler.
     */
    void createSampler(d3d12::D3D12DescHeap &descHeap, const ::D3D12_SAMPLER_DESC &desc);

    /// Check validity of contained resource.
    bool valid() const
    { return operator bool(); }

    /// Check validity of contained resource.
    explicit operator bool() const
    { return mSamplerHandle.ptr != 0u; }
private:
    /// Handle to the inner sampler.
    ::CD3DX12_CPU_DESCRIPTOR_HANDLE mSamplerHandle{ };
protected:
}; // class D3D12Sampler

} // namespace d3d12

} // namespace res

} // namespace quark
