/**
 * @file resources/d3d12/D3D12RootSignature.h
 * @author Tomas Polasek
 * @brief Wrapper around a Direct3D 12 root signature.
 */

#pragma once

#include "engine/resources/BaseResource.h"
#include "engine/resources/d3d12/D3D12Device.h"
#include "engine/resources/d3d12/D3D12RayTracingDevice.h"

#include "engine/helpers/d3d12/D3D12PipelineStateSOBuilder.h"


/// Namespace containing the engine code.
namespace quark
{

/// Namespace containing helper classes and methods.
namespace helpers
{

/// Direct3D 12 resource management.
namespace d3d12
{

// Forward declaration.
class D3D12RootSignatureBuilder;
    
} // namespace d3d12

} // namespace helpers

/// Namespace containing resource management helpers.
namespace res
{

/// Direct3D 12 resource management.
namespace d3d12
{

/// Wrapper around Direct3D 12 root signature.
class D3D12RootSignature : public BaseResource, public util::PointerType<D3D12RootSignature>
{
public:
    using D3D12RootSignatureT = ::ID3D12RootSignature;

    /**
     * Create root signature using specified data.
     * @param rootParameters List of root parameters.
     * @param staticSamplers List of static samplers.
     * @param flags Flags passed to the root signature.
     * @param device Target device.
     * @throws Throws util::winexception on error.
     */
    static PtrT create(const std::vector<::CD3DX12_ROOT_PARAMETER1> &rootParameters, 
        const std::vector<::CD3DX12_STATIC_SAMPLER_DESC> &staticSamplers,
        ::D3D12_ROOT_SIGNATURE_FLAGS flags, D3D12Device &device);

    /**
     * Create root signature using specified description.
     * @param desc Description of the root signature.
     * @param device Target device.
     * @throws Throws util::winexception on error.
     */
    static PtrT create(const ::CD3DX12_VERSIONED_ROOT_SIGNATURE_DESC &desc, 
        D3D12Device &device);

#ifdef ENGINE_RAY_TRACING_ENABLED
    /**
     * Create root signature using specified data.
     * Root signatures created with this constructor are 
     * usable in ray tracing pipelines.
     * @param rootParameters List of root parameters.
     * @param staticSamplers List of static samplers.
     * @param flags Flags passed to the root signature.
     * @param device Target device.
     * @throws Throws util::winexception on error.
     */
    static PtrT create(const std::vector<::CD3DX12_ROOT_PARAMETER1> &rootParameters, 
        const std::vector<::CD3DX12_STATIC_SAMPLER_DESC> &staticSamplers,
        ::D3D12_ROOT_SIGNATURE_FLAGS flags, D3D12RayTracingDevice &device);

    /**
     * Create root signature using specified description.
     * Root signatures created with this constructor are 
     * usable in ray tracing pipelines.
     * @param desc Description of the root signature.
     * @param device Target device.
     * @throws Throws util::winexception on error.
     */
    static PtrT create(const ::CD3DX12_VERSIONED_ROOT_SIGNATURE_DESC &desc, 
        D3D12RayTracingDevice &device);
#endif

    /// Free resources.
    virtual ~D3D12RootSignature();

    // No copying.
    D3D12RootSignature(const D3D12RootSignature &other) = delete;
    D3D12RootSignature &operator=(const D3D12RootSignature &rhs) = delete;

    // Allow moving.
    D3D12RootSignature(D3D12RootSignature &&other) = default;
    D3D12RootSignature &operator=(D3D12RootSignature &&rhs) = default;

    /// Get pipeline state sub-object for root signature.
    helpers::d3d12::psot::RootSignature rsState() const
    { ASSERT_FAST(created()); return mRootSignature.Get(); }

    /// Get the inner pointer.
    auto &getPtr() const
    { return mRootSignature; }

    /// Get the inner root signature.
    auto *get() const
    { return mRootSignature.Get(); }

    /// Access the root signature.
    auto &operator->() const
    { return mRootSignature; }

    /// Is this root signature created?
    bool created() const noexcept
    { return mRootSignature; }

    /// Is this root signature created?
    explicit operator bool() const noexcept
    { return created(); }
private:
    // Allow builder access to the special constructor.
    friend class helpers::d3d12::D3D12RootSignatureBuilder;

    /**
     * Create root signature using specified data.
     * @param rootParameters List of root parameters.
     * @param numRootParameters Number of root parameters 
     * in the list.
     * @param staticSamplers List of static samplers.
     * @param numStaticSamplers Number of static samplers 
     * in the list.
     * @param flags Flags passed to the root signature.
     * @param supportsRS11 Does the device support 
     * root signatures with version 1.1?
     * @param device Target device.
     * @throws util::winexception Thrown on error.
     */
     static PtrT create(const ::CD3DX12_ROOT_PARAMETER1 *rootParameters, std::size_t numRootParameters, 
        const ::CD3DX12_STATIC_SAMPLER_DESC *staticSamplers, std::size_t numStaticSamplers,
        ::D3D12_ROOT_SIGNATURE_FLAGS flags, bool supportsRS11, D3D12Device &device);

    /**
     * Create root signature using specified data.
     * @param rootParameters List of root parameters.
     * @param numRootParameters Number of root parameters 
     * in the list.
     * @param staticSamplers List of static samplers.
     * @param numStaticSamplers Number of static samplers 
     * in the list.
     * @param flags Flags passed to the root signature.
     * @param supportsRS11 Does the device support 
     * root signatures with version 1.1?
     * @param device Target device.
     * @throws util::winexception Thrown on error.
     */
    D3D12RootSignature(const ::CD3DX12_ROOT_PARAMETER1 *rootParameters, std::size_t numRootParameters, 
        const ::CD3DX12_STATIC_SAMPLER_DESC *staticSamplers, std::size_t numStaticSamplers,
        ::D3D12_ROOT_SIGNATURE_FLAGS flags, bool supportsRS11, D3D12Device &device);

    /**
     * Serialize root signature description into provided blob.
     * @param desc Description of the root signature.
     * @param rsBlob Serialized root signature will be stored here.
     * @throws util::winexception Thrown on error.
     */
    void serializeRootSignature(const ::CD3DX12_VERSIONED_ROOT_SIGNATURE_DESC &desc, 
        ComPtr<::ID3DBlob> &rsBlob);

    /**
     * Create root signature using specified description.
     * @param desc Description of the root signature.
     * @param device Target device.
     * @throws Throws util::winexception on error.
     */
    void createRootSignature(const ::CD3DX12_VERSIONED_ROOT_SIGNATURE_DESC &desc, 
        D3D12Device &device);

#ifdef ENGINE_RAY_TRACING_ENABLED
    /**
     * Create root signature using specified data.
     * Root signatures created with this constructor are 
     * usable in ray tracing pipelines.
     */
    static PtrT create(const ::CD3DX12_ROOT_PARAMETER1 *rootParameters, std::size_t numRootParameters, 
        const ::CD3DX12_STATIC_SAMPLER_DESC *staticSamplers, std::size_t numStaticSamplers,
        ::D3D12_ROOT_SIGNATURE_FLAGS flags, bool supportsRS11, D3D12RayTracingDevice &device);

    /**
     * Create root signature using specified data.
     * Root signatures created with this constructor are 
     * usable in ray tracing pipelines.
     */
    D3D12RootSignature(const ::CD3DX12_ROOT_PARAMETER1 *rootParameters, std::size_t numRootParameters, 
        const ::CD3DX12_STATIC_SAMPLER_DESC *staticSamplers, std::size_t numStaticSamplers,
        ::D3D12_ROOT_SIGNATURE_FLAGS flags, bool supportsRS11, D3D12RayTracingDevice &device);

    /**
     * Create root signature using specified description.
     * Root signatures created with this method are 
     * usable in ray tracing pipelines.
     * @param desc Description of the root signature.
     * @param device Target device.
     * @throws Throws util::winexception on error.
     */
    void createRootSignatureFallback(const ::CD3DX12_VERSIONED_ROOT_SIGNATURE_DESC &desc, 
        D3D12RayTracingDevice &device);
#endif

    /// Inner root signature.
    ComPtr<D3D12RootSignatureT> mRootSignature;
protected:
    /// Uninitialized root signature.
    D3D12RootSignature();

    /**
     * Create root signature using specified data.
     * @param rootParameters List of root parameters.
     * @param staticSamplers List of static samplers.
     * @param flags Flags passed to the root signature.
     * @param device Target device.
     * @throws Throws util::winexception on error.
     */
    D3D12RootSignature(const std::vector<::CD3DX12_ROOT_PARAMETER1> &rootParameters, 
        const std::vector<::CD3DX12_STATIC_SAMPLER_DESC> &staticSamplers,
        ::D3D12_ROOT_SIGNATURE_FLAGS flags, D3D12Device &device);

    /**
     * Create root signature using specified description.
     * @param desc Description of the root signature.
     * @param device Target device.
     * @throws Throws util::winexception on error.
     */
    D3D12RootSignature(const ::CD3DX12_VERSIONED_ROOT_SIGNATURE_DESC &desc, 
        D3D12Device &device);

#ifdef ENGINE_RAY_TRACING_ENABLED
    /**
     * Create root signature using specified data.
     * Root signatures created with this constructor are 
     * usable in ray tracing pipelines.
     * @param rootParameters List of root parameters.
     * @param staticSamplers List of static samplers.
     * @param flags Flags passed to the root signature.
     * @param device Target device.
     * @throws Throws util::winexception on error.
     */
    D3D12RootSignature(const std::vector<::CD3DX12_ROOT_PARAMETER1> &rootParameters, 
        const std::vector<::CD3DX12_STATIC_SAMPLER_DESC> &staticSamplers,
        ::D3D12_ROOT_SIGNATURE_FLAGS flags, D3D12RayTracingDevice &device);

    /**
     * Create root signature using specified description.
     * Root signatures created with this constructor are 
     * usable in ray tracing pipelines.
     * @param desc Description of the root signature.
     * @param device Target device.
     * @throws Throws util::winexception on error.
     */
    D3D12RootSignature(const ::CD3DX12_VERSIONED_ROOT_SIGNATURE_DESC &desc, 
        D3D12RayTracingDevice &device);
#endif
}; // class D3D12RootSignature

} // namespace d3d12

} // namespace res

} // namespace quark

// Template implementation

namespace quark
{

namespace res
{

namespace d3d12
{

}

}

}

// Template implementation end
