/**
 * @file resources/d3d12/D3D12PipelineState.h
 * @author Tomas Polasek
 * @brief Wrapper around a Direct3D 12 pipeline state.
 */

#pragma once

#include "engine/resources/BaseResource.h"

#include "engine/resources/d3d12/D3D12Device.h"

/// Namespace containing the engine code.
namespace quark
{

/// Namespace containing helper classes and methods.
namespace helpers
{

/// Direct3D 12 resource management.
namespace d3d12
{

// Forward declaration.
class D3D12PipelineStateBuilder;

} // namespace d3d12

} // namespace helpers

/// Namespace containing resource management helpers.
namespace res
{

/// Direct3D 12 resource management.
namespace d3d12
{

/// Wrapper around Direct3D 12 pipeline state.
class D3D12PipelineState : public BaseResource, public util::PointerType<D3D12PipelineState>
{
public:
    using D3D12PipelineStateT = ::ID3D12PipelineState;

    /// Create uninitialized pipeline state.
    static PtrT create();

    /**
     * Create a graphics pipeline described in provided structure.
     * @param desc Description of the graphics pipeline.
     * @param device Device to create the pipeline on.
     * @throws Throws util::winexception on error.
     */
    static PtrT create(const ::D3D12_GRAPHICS_PIPELINE_STATE_DESC &desc, D3D12Device &device);

    /**
     * Create a compute pipeline described in provided structure.
     * @param desc Description of the compute pipeline.
     * @param device Device to create the pipeline on.
     * @throws Throws util::winexception on error.
     */
    static PtrT create(const ::D3D12_COMPUTE_PIPELINE_STATE_DESC &desc, D3D12Device &device);

    /// Free resources.
    virtual ~D3D12PipelineState();

    // No copying.
    D3D12PipelineState(const D3D12PipelineState &other) = delete;
    D3D12PipelineState &operator=(const D3D12PipelineState &rhs) = delete;

    // Allow moving.
    D3D12PipelineState(D3D12PipelineState &&other) = default;
    D3D12PipelineState &operator=(D3D12PipelineState &&rhs) = default;

    /// Get the inner pointer.
    auto &getPtr() const
    { return mPipelineState; }

    /// Get the inner pipeline state.
    auto *get() const
    { return mPipelineState.Get(); }

    /// Access the pipeline state.
    auto &operator->() const
    { return mPipelineState; }

    /// Check validity of contained resource.
    bool valid() const
    { return operator bool(); }

    /// Check validity of contained resource.
    explicit operator bool() const
    { return mPipelineState; }
private:
    // Allow access to the stream constructor.
    friend class helpers::d3d12::D3D12PipelineStateBuilder;

    /**
     * Create a pipeline described through pipeline state 
     * stream description.
     * This constructor is only available through 
     * D3D12PipelineStateBuilder.
     * @param desc Description of the pipeline state 
     * sub-object stream.
     * @param device Device to create the pipeline on.
     * @throws Throws util::winexception on error.
     */
    static PtrT create(const ::D3D12_PIPELINE_STATE_STREAM_DESC &desc, D3D12Device &device);

    /**
     * Create a pipeline described through pipeline state 
     * stream description.
     * This constructor is only available through 
     * D3D12PipelineStateBuilder.
     * @param desc Description of the pipeline state 
     * sub-object stream.
     * @param device Device to create the pipeline on.
     * @throws Throws util::winexception on error.
     */
    D3D12PipelineState(const ::D3D12_PIPELINE_STATE_STREAM_DESC &desc, D3D12Device &device);

    /// Compiled pipeline state.
    ComPtr<D3D12PipelineStateT> mPipelineState{ nullptr };
protected:
    /// Create uninitialized pipeline state.
    D3D12PipelineState();

    /**
     * Create a graphics pipeline described in provided structure.
     * @param desc Description of the graphics pipeline.
     * @param device Device to create the pipeline on.
     * @throws Throws util::winexception on error.
     */
    D3D12PipelineState(const ::D3D12_GRAPHICS_PIPELINE_STATE_DESC &desc, D3D12Device &device);

    /**
     * Create a compute pipeline described in provided structure.
     * @param desc Description of the compute pipeline.
     * @param device Device to create the pipeline on.
     * @throws Throws util::winexception on error.
     */
    D3D12PipelineState(const ::D3D12_COMPUTE_PIPELINE_STATE_DESC &desc, D3D12Device &device);
}; // class D3D12PipelineState

} // namespace d3d12

} // namespace res

} // namespace quark

// Template implementation

namespace quark
{

namespace res
{

namespace d3d12
{

}

}

}

// Template implementation end
