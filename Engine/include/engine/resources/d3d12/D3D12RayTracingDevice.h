/**
 * @file resources/d3d12/D3D12RayTracingDevice.h
 * @author Tomas Polasek
 * @brief Direct3D 12 device wrapper, allowing for abstraction
 * of using fallback ray tracing layer.
 */

#pragma once

#include "engine/resources/BaseResource.h"
#include "engine/resources/d3d12/D3D12Device.h"
#include "engine/resources/d3d12/D3D12RayTracingCommandList.h"
#include "engine/resources/d3d12/D3D12DescHeap.h"

#ifdef ENGINE_RAY_TRACING_ENABLED
// Ray tracing fallback layer and helpers.
#	include "engine/lib/DxrFallback.h"
#endif

/// Namespace containing the engine code.
namespace quark {

/// Namespace containing resource management helpers.
namespace res {

/// Direct3D 12 resource management.
namespace d3d12 {

// Forward declarations.
class D3D12Resource;
class D3D12DescHeap;

/**
 * Direct3D 12 device wrapper, allowing for abstraction
 * of using fallback ray tracing layer.
 */
class D3D12RayTracingDevice : public D3D12Device
{
public:
#ifdef ENGINE_RAY_TRACING_ENABLED
	using D3D12Device5T = ::ID3D12Device5;
	using D3D12Device4T = ::ID3D12Device4;
	using D3D12FallbackDeviceT = ::ID3D12RaytracingFallbackDevice;
	using D3D12ApplyDeviceT = D3D12FallbackDeviceT;
#else
	using D3D12Device4T = ::ID3D12Device2;
	using D3D12ApplyDeviceT = D3D12DeviceT;
#endif

    using PtrT = util::PointerType<D3D12RayTracingDevice>::PtrT;
    using ConstPtrT = util::PointerType<D3D12RayTracingDevice>::ConstPtrT;

	// Empty wrapper.
	static PtrT create();

	/**
	 * Create ray tracing device from source device.
	 * @param srcDevice Source device used to create the
	 * ray tracing device.
	 * @throw util::winexception Thrown if any error occurs. The contained
	 * message details what happened.
	 */
	static PtrT create(D3D12Device &srcDevice);

#ifdef ENGINE_RAY_TRACING_ENABLED
	/**
	 * Create fallback ray tracing device from source device.
	 * @param srcDevice Source device used to create the
	 * ray tracing device.
	 * @param flags Flags used when creating the device.
	 * @throw util::winexception Thrown if any error occurs. The contained
	 * message details what happened.
	 */
	static PtrT create(D3D12Device &srcDevice, ::CreateRaytracingFallbackDeviceFlags flags);
#endif // ENGINE_RAY_TRACING_ENABLED

	/// Free resources used by this object.
	virtual ~D3D12RayTracingDevice();

	// No copying, allow moving.
	D3D12RayTracingDevice(const D3D12RayTracingDevice& other) = delete;
	D3D12RayTracingDevice& operator=(const D3D12RayTracingDevice& rhs) = delete;
	D3D12RayTracingDevice(D3D12RayTracingDevice&& other) = default;
	D3D12RayTracingDevice& operator=(D3D12RayTracingDevice&& rhs) = default;

	/**
	 * Check for feature support on this device.
	 * @tparam FeatT Type-specific feature structure
	 * type, e.g. ::D3D12_FEATURE_DATA_ROOT_SIGNATURE.
	 * @param features Tested features.
	 * @param featureType Type of the tested
	 * features, e.g. ::D3D12_FEATURE_ROOT_SIGNATURE.
	 * @return Returns true, if the features are supported.
	 */
	template <typename FeatT>
	bool supportsFeatures(FeatT features, ::D3D12_FEATURE featureType)
	{ return !FAILED(get()->CheckFeatureSupport(featureType, &features, sizeof(FeatT))); }

#ifdef ENGINE_RAY_TRACING_ENABLED
	/**
	 * Convert input command list into ray tracing fallback command list.
	 * @param cmdList Input command list.
	 * @return Returns ray tracing fallback command list created from the
	 * input command list.
	 * @throw util::winexception Thrown if fallback device is not used.
	 */
	ComPtr<::ID3D12RaytracingFallbackCommandList> toFallbackCmdList(::ID3D12GraphicsCommandList *cmdList);

	/**
	 * Convert provided command list wrapper to a ray tracing command
	 * list usable in ray tracing.
	 * @param cmdList Base command list.
	 * @return Returns command list usable in ray tracing.
	 * @throw util::winexception Thrown if fallback device is not used.
	 */
	D3D12RayTracingCommandList::PtrT toRayTracingCmdList(D3D12CommandList &cmdList);

	/// Is the fallback ray tracing device currently active?
	bool usingFallbackDevice() const;

	/**
	 * Serialize a root signature using this ray tracing device,
	 * automatically switching between fallback ray tracing
	 * device and physical device depending on the configuration.
	 * @param desc Description of the requested root signature.
	 * @param output Serialized root signature will be stored here.
	 * @throw util::winexception Thrown on error.
	 */
	void serializeRootSignature(const ::CD3DX12_VERSIONED_ROOT_SIGNATURE_DESC& desc, ComPtr<::ID3DBlob>& output);

	/**
	 * Serialize and create a root signature using this ray tracing
	 * device, automatically switching between fallback ray tracing
	 * device and physical device depending on the configuration.
	 * @param desc Description of the requested root signature.
	 * @param riid Used for passing target root signature.
	 * @param targetRs Used for passing target root signature.
	 * @usage createRootSignature(rsDesc, IID_PPV_ARGS(&targetRs))
	 * @throw util::winexception Thrown on error.
	 */
	void serializeAndCreateRootSignature(const ::CD3DX12_VERSIONED_ROOT_SIGNATURE_DESC& desc, const ::IID& riid, void** targetRs);

	/**
	 * Create a state object, automatically choosing between hardware
	 * ray tracing device, or fallback device.
	 * Two state objects must be passed, since the ray tracing may
	 * be using fallback device.
	 * @param desc Description of the state object.
	 * @param nRiid Used for passing target state object.
	 * @param nTargetSo Used for passing target state object.
	 * @param fRiid Used for passing target fallback state object.
	 * @param fTargetSo Used for passing target state object.
	 * @usage createStateObject(soDesc, IID_PPV_ARGS(&targetSo), IID_PPV_ARGS(&fallbackTargetSo))
	 * @throw util::winexception Thrown on error.
	 */
	void createStateObject(const ::D3D12_STATE_OBJECT_DESC& desc, const ::IID& nRiid, void** nTargetSo, const ::IID& fRiid, void** fTargetSo);

	/// Get size of a single shader identifier.
	std::size_t getShaderIdentifierSize();

	/// Get initial resource state for the acceleration structure buffers.
	::D3D12_RESOURCE_STATES getASResourceState() const;

	/**
	 * Create a wrapped memory pointer for given resource.
	 * This can be used for resources containing ray tracing
	 * acceleration structures, so they can be used on
	 * devices without hardware support.
	 * @param descHeap Descriptor heap used for allocation of
	 * the pointer.
	 * @param resource Create wrapped pointer for this resource.
	 * @return Returns the wrapped pointer for given resource.
	 */
	::WRAPPED_GPU_POINTER wrapResource(D3D12DescAllocatorInterface& descHeap, D3D12Resource& resource);

	/// Access the standard device.
	D3D12DeviceT* device()
	{ return get(); }
	/// Access the fallback device.
	D3D12FallbackDeviceT* fallbackDevice()
	{ return mFallbackDevice.Get(); }
#endif

	/**
	 * Pass currently used device to the provided generic
	 * lambda.
	 * @param fun Generic function/lambda.
	 * @return Returns the same value as provided function.
	 * @usage apply([](auto *dev){ dev->CreateRootSignature(...); })
	 * @throws util::winexception Thrown on error.
	 */
	template <typename FunT>
	typename std::result_of<FunT(D3D12ApplyDeviceT*)>::type apply(FunT fun);

private:
	/**
	 * Dummy initialize the device within, when no ray tracing
	 * support is supported.
	 * @param srcDevice Source device copied to the inner device.
	 * @throw util::winexception Thrown if any error occurs. The contained
	 * message details what happened.
	 */
	void copySourceDevice(D3D12Device& srcDevice);

#ifdef ENGINE_RAY_TRACING_ENABLED
	/**
	 * Initialize ray tracing device from source device.
	 * @param srcDevice Source device used to create the
	 * ray tracing device.
	 * @throw util::winexception Thrown if any error occurs. The contained
	 * message details what happened.
	 */
	void initializeRayTracingDevice(D3D12Device& srcDevice);

	/**
	 * Initialize ray tracing device from source device.
	 * @param srcDevice Source device used to create the
	 * ray tracing device.
	 * @param flags Flags used when creating the device.
	 * @throw util::winexception Thrown if any error occurs. The contained
	 * message details what happened.
	 */
	void initializeFallbackRayTracingDevice(D3D12Device& srcDevice, ::CreateRaytracingFallbackDeviceFlags flags);

	/// Device used with the DXR fallback layer.
	ComPtr<D3D12FallbackDeviceT> mFallbackDevice{nullptr};
	/// Are we currently using the fallback device?
	bool mUsingFallbackDevice{false};
#endif
protected:
	// Empty wrapper.
	D3D12RayTracingDevice();

	/**
	 * Create ray tracing device from source device.
	 * @param srcDevice Source device used to create the
	 * ray tracing device.
	 * @throw util::winexception Thrown if any error occurs. The contained
	 * message details what happened.
	 */
	D3D12RayTracingDevice(D3D12Device& srcDevice);

#ifdef ENGINE_RAY_TRACING_ENABLED
	/**
	 * Create fallback ray tracing device from source device.
	 * @param srcDevice Source device used to create the
	 * ray tracing device.
	 * @param flags Flags used when creating the device.
	 * @throw util::winexception Thrown if any error occurs. The contained
	 * message details what happened.
	 */
	D3D12RayTracingDevice(D3D12Device& srcDevice, ::CreateRaytracingFallbackDeviceFlags flags);
#endif // ENGINE_RAY_TRACING_ENABLED
}; // class D3D12Device

} // namespace d3d12

} // namespace res

} // namespace quark

// Template implementation.

namespace quark {

namespace res {

namespace d3d12 {

template <typename FunT>
typename std::result_of<FunT(D3D12RayTracingDevice::D3D12ApplyDeviceT*)>::type D3D12RayTracingDevice::apply(FunT fun)
{
#ifdef ENGINE_RAY_TRACING_ENABLED
	if (mUsingFallbackDevice)
	{ return fun(mFallbackDevice.Get()); }
	if (D3D12Device::device5())
	{ return fun(D3D12Device::device5().Get()); }
#else // ENGINE_RAY_TRACING_ENABLED
	if (D3D12Device::get())
	{ return fun(D3D12Device::get()); }
#endif // ENGINE_RAY_TRACING_ENABLED
	else
	{ throw util::winexception("No device is set!!"); }
}

} // namespace d3d12

} // namespace res

} // namespace quark

// Template implementation end.
