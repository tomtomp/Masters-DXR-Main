/**
 * @file resources/d3d12/D3D12Resource.h
 * @author Tomas Polasek
 * @brief Wrapper around a Direct3D 12 resource.
 */

#pragma once

#include "engine/resources/BaseResource.h"

#include "engine/resources/d3d12/D3D12CommandQueueMgr.h"

#include "engine/helpers/d3d12/D3D12BaseGpuAllocator.h"

/// Namespace containing the engine code.
namespace quark
{

/// Namespace containing helper classes and methods.
namespace helpers
{

/// Direct3D 12 helpers.
namespace d3d12
{

// Forward declaration.
class D3D12BatchTransitionBarrier;

} // namespace d3d12

} // namespace helpers

/// Namespace containing resource management helpers.
namespace res
{

/// Direct3D 12 resource management.
namespace d3d12
{

/// Wrapper around Direct3D 12 resource.
class D3D12Resource : public BaseResource, public util::PointerType<D3D12Resource>
{
public:
    using D3D12ResourceT = ::ID3D12Resource;

    /**
     * Wrap around already existing Direct3D 12 
     * resource.
     * The original owner of the resource must NOT 
     * change the state of the resource, unless using 
     * this object.
     * @param res Resource being wrapped.
     * @param currentState Current state of the 
     * resource, at the time of wrapping.
     */
    static PtrT create(const ComPtr<D3D12ResourceT> &res, ::D3D12_RESOURCE_STATES currentState);

    /**
     * Wrap around already existing Direct3D 12 
     * resource.
     * The original owner of the resource must NOT 
     * change the state of the resource, unless using 
     * this object.
     * @param res Resource being wrapped.
     * @param currentState Current state of the 
     * resource, at the time of wrapping.
     * @param desc Description of the resource.
     */
    static PtrT create(const ComPtr<D3D12ResourceT> &res, ::D3D12_RESOURCE_STATES currentState, 
        const ::CD3DX12_RESOURCE_DESC &desc);

    /**
     * Create a new resource as specified in the description 
     * and allocate it using provided allocator.
     * @param desc Description of the resource.
     * @param initState Initial state of the resource.
     * @param optimizedClearValue Optimized clearing value 
     * for this resource. Can be nullptr.
     * @param allocator Allocator used in allocation of 
     * this resource.
     * @throws Throws util::winexception if any error 
     * occurs.
     */
    template <typename AllocPtrT>
    static PtrT create(const ::CD3DX12_RESOURCE_DESC &desc, ::D3D12_RESOURCE_STATES initState, 
        const ::D3D12_CLEAR_VALUE *optimizedClearValue, AllocPtrT allocator);

    /**
     * Create a new resource as specified in the description 
     * and allocate it using provided allocator.
     * @param desc Description of the resource.
     * @param initState Initial state of the resource.
     * @param optimizedClearValue Optimized clearing value 
     * for this resource. Can be nullptr.
     * @param allocator Allocator used in allocation of 
     * this resource.
     * @throws Throws util::winexception if any error 
     * occurs.
     */
    template <typename AllocPtrT>
    void allocate(const ::CD3DX12_RESOURCE_DESC &desc, ::D3D12_RESOURCE_STATES initState, 
        const ::D3D12_CLEAR_VALUE *optimizedClearValue, AllocPtrT allocator);

    /// Free resources.
    virtual ~D3D12Resource();

    // No copying.
    D3D12Resource(const D3D12Resource &other) = delete;
    D3D12Resource &operator=(const D3D12Resource &rhs) = delete;

    // Allow moving.
    D3D12Resource(D3D12Resource &&other) = default;
    D3D12Resource &operator=(D3D12Resource &&rhs) = default;

    /**
     * Transition resource to given state.
     * Transition command will be recorded 
     * onto the specified command list.
     * @param newState Target state to transition to.
     * @param cmdList List to record the transition 
     * command on.
     * @param subResource Sub-resource selection.
     * @param flags Flags for the transition barrier.
     * @throws Throws util::winexception if any error 
     * occurs. The contained message details what 
     * happened.
     */
    void transitionTo(::D3D12_RESOURCE_STATES newState, D3D12CommandList &cmdList, 
        UINT subResource = D3D12_RESOURCE_BARRIER_ALL_SUBRESOURCES, 
        ::D3D12_RESOURCE_BARRIER_FLAGS flags = ::D3D12_RESOURCE_BARRIER_FLAG_NONE);

    /// Generate a shader resource view description for this resource.
    virtual ::D3D12_SHADER_RESOURCE_VIEW_DESC srvDesc() const;

    /// Generate a unordered access view description for this resource.
    virtual ::D3D12_UNORDERED_ACCESS_VIEW_DESC uavDesc() const;

    /// Generate a constant buffer view description for this resource.
    virtual ::D3D12_CONSTANT_BUFFER_VIEW_DESC cbvDesc() const;

    /// Generate a render-target view description for this resource.
    virtual ::D3D12_RENDER_TARGET_VIEW_DESC rtvDesc() const;

    /// Generate a depth-stencil view description for this resource.
    virtual ::D3D12_DEPTH_STENCIL_VIEW_DESC dsvDesc() const;

    /**
     * Get inner pointer without checking whether 
     * the resource is initialized.
     * @return Returns pointer to the resource.
     * @warning The returned pointer may be 
     * nullptr!
     */
    auto &getPtr() const
    {
        ASSERT_FAST(mResource);
        return mResource;
    }

    /**
     * Get inner pointer without checking whether 
     * the resource is initialized.
     * @return Returns pointer to the resource.
     * @warning The returned pointer may be 
     * nullptr!
     */
    auto *get() const
    {
        ASSERT_FAST(mResource);
        return mResource.Get();
    }

    /**
     * Access the resource.
     * Performs checking, whether the resource 
     * has been initialized.
     * @throws std::runtime_error if the resource 
     * has not been initialized.
     */
    auto &operator->() const
    {
        if (!valid())
        {
            throw std::runtime_error("Accessing uninitialized resource!");
        }
        return mResource;
    }

    /// Description of this resource.
    const ::CD3DX12_RESOURCE_DESC &desc() const
    { return mDesc; }

    /**
     * Current state of this resource, after 
     * executing all transition barriers.
     */
    ::D3D12_RESOURCE_STATES state() const
    { return mState; }

    /// Check validity of contained resource.
    bool valid() const
    { return operator bool(); }

    /// Check validity of contained resource.
    explicit operator bool() const
    { return mResource; }

    /// Get size of the memory allocated on the GPU, in bytes.
    std::size_t allocatedSize() const;
private:
    // Allow access to current state and resource.
    friend class D3D12Device;

    // Allow access to the state.
    friend class helpers::d3d12::D3D12BatchTransitionBarrier;

    /**
     * Allocate resource described by provided structure.
     * @param desc Description of the resource.
     * @param initState Initial state of the resource.
     * @param optimizedClearValue Optimized clearing value 
     * for this resource. Can be nullptr.
     * @param allocator Allocator to allocate with.
     * @return Returns the allocated resource.
     * @throws Throws util::winexception on error.
     */
    ComPtr<D3D12ResourceT> allocateResource(const ::CD3DX12_RESOURCE_DESC &desc, ::D3D12_RESOURCE_STATES initState, 
        const ::D3D12_CLEAR_VALUE *optimizedClearValue, helpers::d3d12::D3D12BaseGpuAllocator &allocator);

    /**
     * Destroy this resource, de-allocating it if 
     * allocator was used.
     */
    void destroy();

    /// Inner pointer.
    ComPtr<D3D12ResourceT> mResource{ nullptr };
    /// Allocator used to allocate this resource.
    util::PtrT<helpers::d3d12::D3D12BaseGpuAllocator> mAllocator{ nullptr };
    /**
     * Current state of the resource, after 
     * executing all transition barriers.
     */
    ::D3D12_RESOURCE_STATES mState{ D3D12_RESOURCE_STATE_COMMON };
    /// Description of the resource.
    ::CD3DX12_RESOURCE_DESC mDesc{ };
protected:
    /// Initialize empty resource.
    D3D12Resource() = default;

    /**
     * Wrap around already existing Direct3D 12 
     * resource.
     * The original owner of the resource must NOT 
     * change the state of the resource, unless using 
     * this object.
     * @param res Resource being wrapped.
     * @param currentState Current state of the 
     * resource, at the time of wrapping.
     */
    D3D12Resource(const ComPtr<D3D12ResourceT> &res, ::D3D12_RESOURCE_STATES currentState);

    /**
     * Wrap around already existing Direct3D 12 
     * resource.
     * The original owner of the resource must NOT 
     * change the state of the resource, unless using 
     * this object.
     * @param res Resource being wrapped.
     * @param currentState Current state of the 
     * resource, at the time of wrapping.
     * @param desc Description of the resource.
     */
    D3D12Resource(const ComPtr<D3D12ResourceT> &res, ::D3D12_RESOURCE_STATES currentState, 
        const ::CD3DX12_RESOURCE_DESC &desc);

    /**
     * Create a new resource as specified in the description 
     * and allocate it using provided allocator.
     * @param desc Description of the resource.
     * @param initState Initial state of the resource.
     * @param optimizedClearValue Optimized clearing value 
     * for this resource. Can be nullptr.
     * @param allocator Allocator used in allocation of 
     * this resource.
     * @throws Throws util::winexception if any error 
     * occurs.
     */
    template <typename AllocPtrT>
    D3D12Resource(const ::CD3DX12_RESOURCE_DESC &desc, ::D3D12_RESOURCE_STATES initState, 
        const ::D3D12_CLEAR_VALUE *optimizedClearValue, AllocPtrT allocator);
}; // class D3D12Resource

} // namespace d3d12

} // namespace res

} // namespace quark

// Template implementation

namespace quark
{

namespace res
{

namespace d3d12
{

template <typename AllocPtrT>
D3D12Resource::D3D12Resource(const ::CD3DX12_RESOURCE_DESC &desc, ::D3D12_RESOURCE_STATES initState,
    const ::D3D12_CLEAR_VALUE *optimizedClearValue, AllocPtrT allocator)
{ allocate(desc, initState, optimizedClearValue, std::move(allocator)); }

template <typename AllocPtrT>
util::PointerType<D3D12Resource>::PtrT D3D12Resource::create(const ::CD3DX12_RESOURCE_DESC &desc,
    ::D3D12_RESOURCE_STATES initState, const ::D3D12_CLEAR_VALUE *optimizedClearValue, AllocPtrT allocator)
{ return PtrT{ new D3D12Resource(desc, initState, optimizedClearValue, allocator) }; }

template <typename AllocPtrT>
void D3D12Resource::allocate(const ::CD3DX12_RESOURCE_DESC &desc, ::D3D12_RESOURCE_STATES initState,
    const ::D3D12_CLEAR_VALUE *optimizedClearValue, AllocPtrT allocator)
{
    mAllocator = allocator;
    mState = initState;
    mDesc = desc;
    mResource = allocateResource(mDesc, initState, optimizedClearValue, *mAllocator);
}

}

}

}

// Template implementation end
