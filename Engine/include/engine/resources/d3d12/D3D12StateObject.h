/**
 * @file resources/d3d12/D3D12StateObject.h
 * @author Tomas Polasek
 * @brief Wrapper around a Direct3D 12 state object.
 */

#pragma once

#include "engine/resources/BaseResource.h"

#include "engine/resources/d3d12/D3D12Device.h"
#include "engine/resources/d3d12/D3D12RayTracingDevice.h"

/// Namespace containing the engine code.
namespace quark
{

/// Namespace containing resource management helpers.
namespace res
{

/// Direct3D 12 resource management.
namespace d3d12
{

/// Wrapper around Direct3D 12 state object.
class D3D12StateObject : public BaseResource, public util::PointerType<D3D12StateObject>
{
public:
    using D3D12StateObjectT = ::ID3D12StateObject;

    /**
     * Create a state object described in provided structure.
     * @param desc Description of the state object.
     * @param device Device to create the state object.
     * @throws Throws util::winexception on error.
     */
    static PtrT create(const ::D3D12_STATE_OBJECT_DESC &desc, D3D12Device &device);

    /// Free resources.
    virtual ~D3D12StateObject();

    // No copying.
    D3D12StateObject(const D3D12StateObject &other) = delete;
    D3D12StateObject &operator=(const D3D12StateObject &rhs) = delete;

    // Allow moving.
    D3D12StateObject(D3D12StateObject &&other) = default;
    D3D12StateObject &operator=(D3D12StateObject &&rhs) = default;

    /// Get the inner pointer.
    auto &getPtr() const
    { return mStateObject; }

    /// Get the inner state object.
    auto *get() const
    { return mStateObject.Get(); }

    /// Access the state object.
    auto &operator->() const
    { return mStateObject; }

    /// Check validity of contained resource.
    bool valid() const
    { return operator bool(); }

    /// Check validity of contained resource.
    explicit operator bool() const
    { return mStateObject; }
private:
    /// Compiled state object.
    ComPtr<D3D12StateObjectT> mStateObject{ nullptr };
protected:
    /// Uninitialized state object.
    D3D12StateObject();

    /**
     * Create a state object described in provided structure.
     * @param desc Description of the state object.
     * @param device Device to create the state object.
     * @throws Throws util::winexception on error.
     */
    D3D12StateObject(const ::D3D12_STATE_OBJECT_DESC &desc, D3D12Device &device);
}; // class D3D12StateObject

} // namespace d3d12

} // namespace res

} // namespace quark

// Template implementation

namespace quark
{

namespace res
{

namespace d3d12
{

}

}

}

// Template implementation end
