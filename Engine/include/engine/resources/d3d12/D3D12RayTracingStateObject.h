/**
* @file resources/d3d12/D3D12RayTracingStateObject.h
* @author Tomas Polasek
* @brief Wrapper around a Direct3D 12 state object used for 
* ray tracing.
*/

#pragma once

#include "engine/resources/BaseResource.h"

#include "engine/resources/d3d12/D3D12Device.h"
#include "engine/resources/d3d12/D3D12RayTracingDevice.h"

/// Namespace containing the engine code.
namespace quark
{

/// Namespace containing resource management helpers.
namespace res
{

/// Direct3D 12 resource management.
namespace d3d12
{

/// Wrapper around Direct3D 12 state object used for ray tracing.
class D3D12RayTracingStateObject : public BaseResource, public util::PointerType<D3D12RayTracingStateObject>
{
public:
    using D3D12StateObjectT = ::ID3D12StateObject;
#ifdef ENGINE_RAY_TRACING_ENABLED
    using D3D12FallbackStateObjectT = ::ID3D12RaytracingFallbackStateObject;
#endif

#ifdef ENGINE_RAY_TRACING_ENABLED
    /**
     * Create a state object described in provided structure.
     * Resulting state object can be used in ray tracing configuration.
     * @param desc Description of the state object.
     * @param device Device to create the state object.
     * @throws Throws util::winexception on error.
     */
    static PtrT create(const ::D3D12_STATE_OBJECT_DESC &desc, D3D12RayTracingDevice &device);
#endif // ENGINE_RAY_TRACING_ENABLED

    /// Free resources.
    virtual ~D3D12RayTracingStateObject();

    // No copying.
    D3D12RayTracingStateObject(const D3D12RayTracingStateObject &other) = delete;
    D3D12RayTracingStateObject &operator=(const D3D12RayTracingStateObject &rhs) = delete;

    // Allow moving.
    D3D12RayTracingStateObject(D3D12RayTracingStateObject &&other) = default;
    D3D12RayTracingStateObject &operator=(D3D12RayTracingStateObject &&rhs) = default;

#ifdef ENGINE_RAY_TRACING_ENABLED
    /**
     * Get shader identifier of a shader within this state object 
     * which has given name.
     * @param name Name of the requested shader.
     * @throw util::winexception Thrown when not using fallback state
     * object and an error occurs.
     */
    void *getShaderIdentifier(const wchar_t *name);

    /**
     * Pass currently used state object to the provided generic 
     * lambda.
     * @param fun Generic function/lambda.
     * @return Returns the same value as provided function.
     * @usage apply([](auto *so){ so->GetShaderIdentifier(...); })
     */
    template <typename FunT>
    typename std::result_of<FunT(D3D12FallbackStateObjectT*)>::type apply(FunT fun);

    /**
     * Set this pipeline as the current state for given command list.
     * @param cmdList Command list used.
     */
    void setPipelineState(D3D12RayTracingCommandList &cmdList);

#endif

    /// Check validity of contained resource.
    bool valid() const
    { return operator bool(); }

    /// Check validity of contained resource.
    explicit operator bool() const
    { return mStateObject; }

    /// Is the fallback ray tracing state object currently active?
    bool usingFallbackStateObject() const
    { 
#ifdef ENGINE_RAY_TRACING_ENABLED
        return mFallbackStateObject; 
#else
        return false;
#endif
    }

    /// Access the standard state object.
    D3D12StateObjectT *stateObject()
    { return mStateObject.Get(); }

#ifdef ENGINE_RAY_TRACING_ENABLED

    /// Access the fallback state object.
    D3D12FallbackStateObjectT *fallbackStateObject()
    { return mFallbackStateObject.Get(); }

#endif
private:
    /// Compiled state object.
    ComPtr<D3D12StateObjectT> mStateObject{ nullptr };

#ifdef ENGINE_RAY_TRACING_ENABLED

    /// Compiled fallback state object.
    ComPtr<D3D12FallbackStateObjectT> mFallbackStateObject{ nullptr };

#endif
protected:
    /// Uninitialized state object.
    D3D12RayTracingStateObject();

#ifdef ENGINE_RAY_TRACING_ENABLED
    /**
     * Create a state object described in provided structure.
     * Resulting state object can be used in ray tracing configuration.
     * @param desc Description of the state object.
     * @param device Device to create the state object.
     * @throws Throws util::winexception on error.
     */
    D3D12RayTracingStateObject(const ::D3D12_STATE_OBJECT_DESC &desc, D3D12RayTracingDevice &device);
#endif // ENGINE_RAY_TRACING_ENABLED
}; // class D3D12RayTracingStateObject

} // namespace d3d12

} // namespace res

} // namespace quark

// Template implementation

namespace quark
{

namespace res
{

namespace d3d12
{

#ifdef ENGINE_RAY_TRACING_ENABLED
    
template <typename FunT>
typename std::result_of<FunT(D3D12RayTracingStateObject::D3D12FallbackStateObjectT *)>::type D3D12RayTracingStateObject::
apply(FunT fun)
{
    if (usingFallbackStateObject())
    { return fun(mFallbackStateObject.Get()); }
    else
    { return fun(mStateObject.Get()); }
}

#endif

}

}

}

// Template implementation end
