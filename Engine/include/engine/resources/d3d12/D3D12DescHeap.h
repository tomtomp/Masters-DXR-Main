/**
 * @file resources/d3d12/D3D12DescHeap.h
 * @author Tomas Polasek
 * @brief Wrapper around Direct3D 12 descriptor heap.
 */

#pragma once

#include "engine/resources/BaseResource.h"

#include "engine/resources/d3d12/D3D12Device.h"
#include "engine/resources/d3d12/D3D12Resource.h"

/// Namespace containing the engine code.
namespace quark
{

/// Namespace containing resource management helpers.
namespace res
{

/// Direct3D 12 helpers.
namespace d3d12
{

// TODO - Fix this mess? virtual allocateNext(), other methods can be shared.
/**
 * Interface shared by all classes which allow allocation of 
 * descriptors.
 */
class D3D12DescAllocatorInterface
{
public:
    /**
     * Create RTV handle on this descriptor heap.
     * @param resource Create RTV for this resource.
     * @param desc Description of the RTV.
     * @return Returns the new descriptor handle.
     * @throws DescHeapFull Thrown if the descriptor heap 
     * is already full.
     */
    virtual ::CD3DX12_CPU_DESCRIPTOR_HANDLE createRTV(D3D12Resource &resource, 
        const ::D3D12_RENDER_TARGET_VIEW_DESC &desc) = 0;

    /**
     * Create RTV handle on this descriptor heap.
     * @param resource Create RTV for this resource.
     * @param desc Description of the RTV.
     * @param target Descriptor will be created in place 
     * of this target.
     * @return Returns the new descriptor handle.
     */
    virtual ::CD3DX12_CPU_DESCRIPTOR_HANDLE createRTV(D3D12Resource &resource, 
        const ::D3D12_RENDER_TARGET_VIEW_DESC &desc, 
        const ::CD3DX12_CPU_DESCRIPTOR_HANDLE &target) = 0;

    /**
     * Create DSV handle on this descriptor heap.
     * @param resource Create DSV for this resource.
     * @param desc Description of the DSV.
     * @return Returns the new descriptor handle.
     * @throws DescHeapFull Thrown if the descriptor heap 
     * is already full.
     */
    virtual ::CD3DX12_CPU_DESCRIPTOR_HANDLE createDSV(D3D12Resource &resource, 
        const ::D3D12_DEPTH_STENCIL_VIEW_DESC &desc) = 0;

    /**
     * Create DSV handle on this descriptor heap.
     * @param resource Create DSV for this resource.
     * @param desc Description of the DSV.
     * @param target Descriptor will be created in place 
     * of this target.
     * @return Returns the new descriptor handle.
     */
    virtual ::CD3DX12_CPU_DESCRIPTOR_HANDLE createDSV(D3D12Resource &resource, 
        const ::D3D12_DEPTH_STENCIL_VIEW_DESC &desc, 
        const ::CD3DX12_CPU_DESCRIPTOR_HANDLE &target) = 0;

    /**
     * Create SRV handle on this descriptor heap.
     * @param resource Create SRV for this resource.
     * @param desc Description of the SRV.
     * @return Returns the new descriptor handle.
     * @throws DescHeapFull Thrown if the descriptor heap 
     * is already full.
     */
    virtual ::CD3DX12_CPU_DESCRIPTOR_HANDLE createSRV(D3D12Resource &resource, 
        const ::D3D12_SHADER_RESOURCE_VIEW_DESC &desc) = 0;

    /**
     * Create SRV handle on this descriptor heap.
     * @param resource Create SRV for this resource.
     * @param desc Description of the SRV.
     * @param target Descriptor will be created in place 
     * of this target.
     * @return Returns the new descriptor handle.
     */
    virtual ::CD3DX12_CPU_DESCRIPTOR_HANDLE createSRV(D3D12Resource &resource, 
        const ::D3D12_SHADER_RESOURCE_VIEW_DESC &desc, 
        const ::CD3DX12_CPU_DESCRIPTOR_HANDLE &target) = 0;

    /**
     * Create UAV handle on this descriptor heap.
     * @param resource Create UAV for this resource.
     * @param desc Description of the UAV.
     * @param counterResource Resource used as UAV counter.
     * @return Returns the new descriptor handle.
     * @throws DescHeapFull Thrown if the descriptor heap 
     * is already full.
     */
    virtual ::CD3DX12_CPU_DESCRIPTOR_HANDLE createUAV(D3D12Resource &resource, 
        const ::D3D12_UNORDERED_ACCESS_VIEW_DESC &desc, 
        D3D12Resource *counterResource = nullptr) = 0;

    /**
     * Create UAV handle on this descriptor heap.
     * @param resource Create UAV for this resource.
     * @param desc Description of the UAV.
     * @param target Descriptor will be created in place 
     * of this target.
     * @param counterResource Resource used as UAV counter.
     * @return Returns the new descriptor handle.
     */
    virtual ::CD3DX12_CPU_DESCRIPTOR_HANDLE createUAV(D3D12Resource &resource, 
        const ::D3D12_UNORDERED_ACCESS_VIEW_DESC &desc, 
        const ::CD3DX12_CPU_DESCRIPTOR_HANDLE &target, 
        D3D12Resource *counterResource = nullptr) = 0;

    /**
     * Create CBV handle on this descriptor heap.
     * @param desc Description of the CBV.
     * @return Returns the new descriptor handle.
     */
    virtual ::CD3DX12_CPU_DESCRIPTOR_HANDLE createCBV(
        const ::D3D12_CONSTANT_BUFFER_VIEW_DESC &desc) = 0;

    /**
     * Create CBV handle on this descriptor heap.
     * @param desc Description of the CBV.
     * @param target Descriptor will be created in place 
     * of this target.
     * @return Returns the new descriptor handle.
     */
    virtual ::CD3DX12_CPU_DESCRIPTOR_HANDLE createCBV(
        const ::D3D12_CONSTANT_BUFFER_VIEW_DESC &desc,
        const ::CD3DX12_CPU_DESCRIPTOR_HANDLE &target) = 0;

    /**
     * Create SRV null handle on this descriptor heap.
     * @param dimension Type of the SRV.
     * @return Returns the new descriptor handle.
     * @throws DescHeapFull Thrown if the descriptor heap 
     * is already full.
     */
    virtual ::CD3DX12_CPU_DESCRIPTOR_HANDLE createSrvNullHandle(
        ::D3D12_SRV_DIMENSION dimension) = 0;

    /**
     * Create SRV null handle on this descriptor heap.
     * @param dimension Type of the SRV.
     * @param target Descriptor will be created in place 
     * of this target.
     * @return Returns the new descriptor handle.
     * @throws DescHeapFull Thrown if the descriptor heap 
     * is already full.
     */
    virtual ::CD3DX12_CPU_DESCRIPTOR_HANDLE createSrvNullHandle(
        ::D3D12_SRV_DIMENSION dimension, 
        const ::CD3DX12_CPU_DESCRIPTOR_HANDLE &target) = 0;

    /**
     * Create UAV null handle on this descriptor heap.
     * @param dimension Type of the UAV.
     * @return Returns the new descriptor handle.
     * @throws DescHeapFull Thrown if the descriptor heap 
     * is already full.
     */
    virtual ::CD3DX12_CPU_DESCRIPTOR_HANDLE createUavNullHandle(
        ::D3D12_UAV_DIMENSION dimension) = 0;

    /**
     * Create UAV null handle on this descriptor heap.
     * @param dimension Type of the UAV.
     * @param target Descriptor will be created in place 
     * of this target.
     * @return Returns the new descriptor handle.
     * @throws DescHeapFull Thrown if the descriptor heap 
     * is already full.
     */
    virtual ::CD3DX12_CPU_DESCRIPTOR_HANDLE createUavNullHandle(
        ::D3D12_UAV_DIMENSION dimension, 
        const ::CD3DX12_CPU_DESCRIPTOR_HANDLE &target) = 0;

    /**
     * Create CBV null handle on this descriptor heap.
     * @return Returns the new descriptor handle.
     * @throws DescHeapFull Thrown if the descriptor heap 
     * is already full.
     */
    virtual ::CD3DX12_CPU_DESCRIPTOR_HANDLE createCbvNullHandle() = 0;

    /**
     * Create CBV null handle on this descriptor heap.
     * @param target Descriptor will be created in place 
     * of this target.
     * @return Returns the new descriptor handle.
     * @throws DescHeapFull Thrown if the descriptor heap 
     * is already full.
     */
    virtual ::CD3DX12_CPU_DESCRIPTOR_HANDLE createCbvNullHandle(
        const ::CD3DX12_CPU_DESCRIPTOR_HANDLE &target) = 0;

    /**
     * Create sampler on this descriptor heap.
     * @param desc Description of the sampler.
     * @return Returns the new descriptor handle.
     */
    virtual ::CD3DX12_CPU_DESCRIPTOR_HANDLE createSampler(
        const ::D3D12_SAMPLER_DESC &desc) = 0;

    /**
     * Create sampler on this descriptor heap.
     * @param desc Description of the sampler.
     * @param target Descriptor will be created in place 
     * of this target.
     * @return Returns the new descriptor handle.
     */
    virtual ::CD3DX12_CPU_DESCRIPTOR_HANDLE createSampler(
        const ::D3D12_SAMPLER_DESC &desc, 
        const ::CD3DX12_CPU_DESCRIPTOR_HANDLE &target) = 0;

    /**
     * Get offset from the beginning of the descriptor heap.
     * @param cpuHandle Handle to calculate the offset for.
     * @return Returns offset in a number of handles from the 
     * start of the heap.
     */
    virtual std::size_t offsetFromStart(
        const ::CD3DX12_CPU_DESCRIPTOR_HANDLE &cpuHandle) const = 0;

    /// Check whether given descriptor handle is on this heap.
    virtual bool isOnThisDescHeap(
        const ::CD3DX12_CPU_DESCRIPTOR_HANDLE &cpuHandle) const = 0;

    /// Check whether given descriptor handle is on this heap.
    virtual bool isOnThisDescHeap(
        const ::CD3DX12_GPU_DESCRIPTOR_HANDLE &gpuHandle) const = 0;

    /**
     * Get GPU handle for a CPU handle from this descriptor heap.
     * @param cpuHandle CPU handle from this descriptor heap.
     * @return Returns GPU descriptor handle corresponding to the 
     * provided CPU handle.
     */
    virtual ::CD3DX12_GPU_DESCRIPTOR_HANDLE gpuHandle(
        const ::CD3DX12_CPU_DESCRIPTOR_HANDLE &cpuHandle) const = 0;

    /// Has the descriptor heap been initialized?
    virtual bool initialized() const = 0;
private:
protected:
    /// Only usable as an interface.
    D3D12DescAllocatorInterface() = default;
}; // struct D3D12DescAllocatorInterface

/// Wrapper around Direct3D 12 descriptor heap.
class D3D12DescHeap : public BaseResource, public D3D12DescAllocatorInterface, public util::PointerType<D3D12DescHeap>
{
public:
#ifdef ENGINE_RAY_TRACING_ENABLED
    using D3D12DescHeapT = ::ID3D12DescriptorHeap;
#else
    using D3D12DescHeapT = ::ID3D12DescriptorHeap;
#endif

    /// Exception thrown when the descriptor heap cannot contain any more handles.
    struct DescHeapFull : public std::exception
    {
        DescHeapFull(const char *msg) :
            std::exception(msg)
        { }
    }; // struct DescHeapFull

    /// Exception thrown when the provided descriptor handle is invalid.
    struct InvalidDescriptorHandle : public std::exception
    {
        InvalidDescriptorHandle(const char *msg) :
            std::exception(msg)
        { }
    }; // struct InvalidDescriptorHandle

    /// Exception thrown when provided descriptor table is invalid.
    struct InvalidDescriptorTable : public std::exception
    {
        InvalidDescriptorTable(const char *msg) :
            std::exception(msg)
        { }
    }; // struct InvalidDescriptorTable

    /// Range of descriptors.
    struct DescriptorTable
    {
    public:
        /// Empty table
        DescriptorTable();

        /// Create the table.
        DescriptorTable(uint32_t beginVal, uint32_t endVal);

        /// Size of this table in a number of descriptor handles.
        uint32_t size() const;

        /// Offset of the starting descriptor.
        uint32_t startOffset() const
        { return begin; }

        /// Offset of the one past end descriptor.
        uint32_t endOffset() const
        { return end; }
    private:
        /// Offset of the first descriptor within the table.
        uint32_t begin;
        /// Offset of one past the last descriptor in the table.
        uint32_t end;
    protected:
    }; // struct DescriptorTable

    /**
     * Create a descriptor heap with provided parameters.
     * @param device Where should the descriptor heap be created.
     * @param type Type of the descriptor heap.
     * @param numDescriptors Maximal number of descriptors create 
     * table on the heap.
     * @param flags Flags for the descriptor heap.
     * @throws Throws util::winexception when error occurs.
     */
    static PtrT create(D3D12Device &device, ::D3D12_DESCRIPTOR_HEAP_TYPE type, 
        uint32_t numDescriptors, D3D12_DESCRIPTOR_HEAP_FLAGS flags);

    /**
     * Create a descriptor heap with provided parameters.
     * @param device Where should the descriptor heap be created.
     * @param desc Description of requested descriptor heap.
     * @throws Throws util::winexception when error occurs.
     */
    static PtrT create(D3D12Device &device, const ::D3D12_DESCRIPTOR_HEAP_DESC &desc);

    /**
     * Create a descriptor heap with provided parameters.
     * @param device Where should the descriptor heap be created.
     * @param desc Description of requested descriptor heap.
     * @throws Throws util::winexception when error occurs.
     */
    void initializeHeap(D3D12Device &device, const ::D3D12_DESCRIPTOR_HEAP_DESC &desc);

    /// Descriptor heap is automatically released.
    virtual ~D3D12DescHeap() = default;

    // No copying.
    D3D12DescHeap(const D3D12DescHeap &other) = delete;
    D3D12DescHeap &operator=(const D3D12DescHeap &other) = delete;

    // Allow moving.
    D3D12DescHeap(D3D12DescHeap &&other) = default;
    D3D12DescHeap &operator=(D3D12DescHeap &&other) = default;

    /// Get the inner pointer.
    auto &getPtr() const
    { return mDescHeap; }

    /// Get the inner descriptor heap.
    auto *get() const
    { return mDescHeap.Get(); }

    /// Access the descriptor heap.
    auto &operator->() const
    { return mDescHeap; }

    /// Get size of a single descriptor handle.
    uint32_t descHandleSize() const
    { return mDescHandleSize; }

    /// Has the descriptor heap been initialized?
    virtual bool initialized() const override
    { return mDescHeap; }

    /// Is this descriptor heap initialized?
    explicit operator bool() const
    { return initialized(); }

    /**
     * Get CPU pointer to the next descriptor handle, which will 
     * become allocated.
     * @return Returns CPU pointer to the next descriptor handle.
     * @throws DescHeapFull Thrown if the descriptor heap doesn't 
     * have any free handles.
     */
    ::CD3DX12_CPU_DESCRIPTOR_HANDLE nextHandle();

    /**
     * Allocate a block of descriptor handles for manual management.
     * @param sizeInHandles Requested size in a number of handles.
     * @return Returns table specification, which should not be 
     * modified and can be used by calling other methods of this 
     * heap.
     * @throws DescHeapFull Thrown if there is not enough space for 
     * allocation of a linear block of handles of specified size.
     */
    DescriptorTable allocateTable(uint32_t sizeInHandles);

    /**
     * Get descriptor handle contained within a descriptor table.
     * @param table Descriptor table used.
     * @param offset Offset within the descriptor table.
     * @return Returns handle to the descriptor on given offset.
     * @throws InvalidDescriptorTable Thrown if the table is not 
     * on this descriptor heap.
     * @throws InvalidDescriptorHandle Thrown if given offset is 
     * out of bounds of the table.
     */
    ::CD3DX12_CPU_DESCRIPTOR_HANDLE handleFromTable(const DescriptorTable &table, 
        uint32_t offset) const;

    /**
     * Get descriptor handle from offset
     * @param offset Offset within the descriptor heap.
     * @return Returns handle to the descriptor on given offset.
     * @throws InvalidDescriptorHandle Thrown if given offset is 
     * out of bounds of the heap.
     */
    ::CD3DX12_CPU_DESCRIPTOR_HANDLE handleFromOffset(uint32_t offset) const;

    /**
     * Create constant-buffer view handle on this descriptor heap.
     * @param buffer Create constant-buffer view for this resource.
     * @param sizeInBytes Size of the buffer in bytes.
     * @return Returns the new descriptor handle.
     * @throws DescHeapFull Thrown if the descriptor heap 
     * is already full.
     */
    ::CD3DX12_CPU_DESCRIPTOR_HANDLE createCBV(D3D12Resource &buffer, 
        ::UINT sizeInBytes);

    /**
     * Create RTV handle on this descriptor heap.
     * @param resource Create RTV for this resource.
     * @param desc Description of the RTV.
     * @return Returns the new descriptor handle.
     * @throws DescHeapFull Thrown if the descriptor heap 
     * is already full.
     */
    virtual ::CD3DX12_CPU_DESCRIPTOR_HANDLE createRTV(D3D12Resource &resource, 
        const ::D3D12_RENDER_TARGET_VIEW_DESC &desc) override;

    /**
     * Create RTV handle on this descriptor heap.
     * @param resource Create RTV for this resource.
     * @param desc Description of the RTV.
     * @param target Descriptor will be created in place 
     * of this target.
     * @return Returns the new descriptor handle.
     */
    virtual ::CD3DX12_CPU_DESCRIPTOR_HANDLE createRTV(D3D12Resource &resource, 
        const ::D3D12_RENDER_TARGET_VIEW_DESC &desc, 
        const ::CD3DX12_CPU_DESCRIPTOR_HANDLE &target) override;

    /**
     * Create DSV handle on this descriptor heap.
     * @param resource Create DSV for this resource.
     * @param desc Description of the DSV.
     * @return Returns the new descriptor handle.
     * @throws DescHeapFull Thrown if the descriptor heap 
     * is already full.
     */
    virtual ::CD3DX12_CPU_DESCRIPTOR_HANDLE createDSV(D3D12Resource &resource, 
        const ::D3D12_DEPTH_STENCIL_VIEW_DESC &desc) override;

    /**
     * Create DSV handle on this descriptor heap.
     * @param resource Create DSV for this resource.
     * @param desc Description of the DSV.
     * @param target Descriptor will be created in place 
     * of this target.
     * @return Returns the new descriptor handle.
     */
    virtual ::CD3DX12_CPU_DESCRIPTOR_HANDLE createDSV(D3D12Resource &resource, 
        const ::D3D12_DEPTH_STENCIL_VIEW_DESC &desc, 
        const ::CD3DX12_CPU_DESCRIPTOR_HANDLE &target) override;

    /**
     * Create SRV handle on this descriptor heap.
     * @param resource Create SRV for this resource.
     * @param desc Description of the SRV.
     * @return Returns the new descriptor handle.
     * @throws DescHeapFull Thrown if the descriptor heap 
     * is already full.
     */
    virtual ::CD3DX12_CPU_DESCRIPTOR_HANDLE createSRV(D3D12Resource &resource, 
        const ::D3D12_SHADER_RESOURCE_VIEW_DESC &desc) override;

    /**
     * Create SRV handle on this descriptor heap.
     * @param resource Create SRV for this resource.
     * @param desc Description of the SRV.
     * @param target Descriptor will be created in place 
     * of this target.
     * @return Returns the new descriptor handle.
     */
    virtual ::CD3DX12_CPU_DESCRIPTOR_HANDLE createSRV(D3D12Resource &resource, 
        const ::D3D12_SHADER_RESOURCE_VIEW_DESC &desc, 
        const ::CD3DX12_CPU_DESCRIPTOR_HANDLE &target) override;

    /**
     * Create UAV handle on this descriptor heap.
     * @param resource Create UAV for this resource.
     * @param desc Description of the UAV.
     * @param counterResource Resource used as UAV counter.
     * @return Returns the new descriptor handle.
     * @throws DescHeapFull Thrown if the descriptor heap 
     * is already full.
     */
    virtual ::CD3DX12_CPU_DESCRIPTOR_HANDLE createUAV(D3D12Resource &resource, 
        const ::D3D12_UNORDERED_ACCESS_VIEW_DESC &desc, 
        D3D12Resource *counterResource = nullptr) override;

    /**
     * Create UAV handle on this descriptor heap.
     * @param resource Create UAV for this resource.
     * @param desc Description of the UAV.
     * @param target Descriptor will be created in place 
     * of this target.
     * @param counterResource Resource used as UAV counter.
     * @return Returns the new descriptor handle.
     */
    virtual ::CD3DX12_CPU_DESCRIPTOR_HANDLE createUAV(D3D12Resource &resource, 
        const ::D3D12_UNORDERED_ACCESS_VIEW_DESC &desc, 
        const ::CD3DX12_CPU_DESCRIPTOR_HANDLE &target, 
        D3D12Resource *counterResource = nullptr) override;

    /**
     * Create CBV handle on this descriptor heap.
     * @param desc Description of the CBV.
     * @return Returns the new descriptor handle.
     * @throws DescHeapFull Thrown if the descriptor heap 
     * is already full.
     */
    virtual ::CD3DX12_CPU_DESCRIPTOR_HANDLE createCBV(
        const ::D3D12_CONSTANT_BUFFER_VIEW_DESC &desc) override;

    /**
     * Create CBV handle on this descriptor heap.
     * @param desc Description of the CBV.
     * @param target Descriptor will be created in place 
     * of this target.
     * @return Returns the new descriptor handle.
     */
    virtual ::CD3DX12_CPU_DESCRIPTOR_HANDLE createCBV(
        const ::D3D12_CONSTANT_BUFFER_VIEW_DESC &desc,
        const ::CD3DX12_CPU_DESCRIPTOR_HANDLE &target) override;

    /**
     * Create SRV null handle on this descriptor heap.
     * @param dimension Type of the SRV.
     * @return Returns the new descriptor handle.
     * @throws DescHeapFull Thrown if the descriptor heap 
     * is already full.
     */
    virtual ::CD3DX12_CPU_DESCRIPTOR_HANDLE createSrvNullHandle(
        ::D3D12_SRV_DIMENSION dimension) override;

    /**
     * Create SRV null handle on this descriptor heap.
     * @param dimension Type of the SRV.
     * @param target Descriptor will be created in place 
     * of this target.
     * @return Returns the new descriptor handle.
     * @throws DescHeapFull Thrown if the descriptor heap 
     * is already full.
     */
    virtual ::CD3DX12_CPU_DESCRIPTOR_HANDLE createSrvNullHandle(
        ::D3D12_SRV_DIMENSION dimension, 
        const ::CD3DX12_CPU_DESCRIPTOR_HANDLE &target) override;

    /**
     * Create UAV null handle on this descriptor heap.
     * @param dimension Type of the UAV.
     * @return Returns the new descriptor handle.
     * @throws DescHeapFull Thrown if the descriptor heap 
     * is already full.
     */
    virtual ::CD3DX12_CPU_DESCRIPTOR_HANDLE createUavNullHandle(
        ::D3D12_UAV_DIMENSION dimension) override;

    /**
     * Create UAV null handle on this descriptor heap.
     * @param dimension Type of the UAV.
     * @param target Descriptor will be created in place 
     * of this target.
     * @return Returns the new descriptor handle.
     * @throws DescHeapFull Thrown if the descriptor heap 
     * is already full.
     */
    virtual ::CD3DX12_CPU_DESCRIPTOR_HANDLE createUavNullHandle(
        ::D3D12_UAV_DIMENSION dimension, 
        const ::CD3DX12_CPU_DESCRIPTOR_HANDLE &target) override;

    /**
     * Create CBV null handle on this descriptor heap.
     * @return Returns the new descriptor handle.
     * @throws DescHeapFull Thrown if the descriptor heap 
     * is already full.
     */
    virtual ::CD3DX12_CPU_DESCRIPTOR_HANDLE createCbvNullHandle() override;

    /**
     * Create CBV null handle on this descriptor heap.
     * @param target Descriptor will be created in place 
     * of this target.
     * @return Returns the new descriptor handle.
     * @throws DescHeapFull Thrown if the descriptor heap 
     * is already full.
     */
    virtual ::CD3DX12_CPU_DESCRIPTOR_HANDLE createCbvNullHandle(
        const ::CD3DX12_CPU_DESCRIPTOR_HANDLE &target) override;

    /**
     * Create sampler on this descriptor heap.
     * @param desc Description of the sampler.
     * @return Returns the new descriptor handle.
     * @throws DescHeapFull Thrown if the descriptor heap 
     * is already full.
     */
    virtual ::CD3DX12_CPU_DESCRIPTOR_HANDLE createSampler(
        const ::D3D12_SAMPLER_DESC &desc) override;

    /**
     * Create sampler on this descriptor heap.
     * @param desc Description of the sampler.
     * @param target Descriptor will be created in place 
     * of this target.
     * @return Returns the new descriptor handle.
     */
    virtual ::CD3DX12_CPU_DESCRIPTOR_HANDLE createSampler(
        const ::D3D12_SAMPLER_DESC &desc, 
        const ::CD3DX12_CPU_DESCRIPTOR_HANDLE &target) override;

    /// Check whether given descriptor handle is on this heap.
    virtual bool isOnThisDescHeap(
        const ::CD3DX12_CPU_DESCRIPTOR_HANDLE &cHandle) const override;

    /// Check whether given descriptor handle is on this heap.
    virtual bool isOnThisDescHeap(
        const ::CD3DX12_GPU_DESCRIPTOR_HANDLE &gHandle) const override;

    /// Check whether all of the descriptors in given table are on this heap.
    bool isOnThisDescHeap(const DescriptorTable &table) const;

    /**
     * Get offset from the beginning of the descriptor heap.
     * @param cpuHandle Handle to calculate the offset for.
     * @return Returns offset in a number of handles from the 
     * start of the heap.
     */
    virtual std::size_t offsetFromStart(
        const ::CD3DX12_CPU_DESCRIPTOR_HANDLE &cpuHandle) const override;

    /**
     * Get base handle on the GPU for the start of this heap.
     * @return Returns handle to the first descriptor within 
     * GPU memory.
     */
    ::CD3DX12_GPU_DESCRIPTOR_HANDLE baseHandle() const;

    /**
     * Get base handle on the GPU for the start of given table.
     * @param table Descriptor table to get the handle for.
     * @return Returns handle to the first descriptor of the table 
     * within GPU memory.
     * @throws InvalidDescriptorTable Thrown if the provided table 
     * is not valid.
     */
    ::CD3DX12_GPU_DESCRIPTOR_HANDLE baseTableHandle(const DescriptorTable &table) const;

    /**
     * Get GPU handle for a CPU handle from this descriptor heap.
     * @param cpuHandle CPU handle from this descriptor heap.
     * @return Returns GPU descriptor handle corresponding to the 
     * provided CPU handle.
     */
    virtual ::CD3DX12_GPU_DESCRIPTOR_HANDLE gpuHandle(
        const ::CD3DX12_CPU_DESCRIPTOR_HANDLE &cpuHandle) const override;

    /**
     * Copy descriptors from source descriptor heap and table into 
     * the destination descriptor heap and table.
     * @param srcHeap Source for the copy operation.
     * @param srcTable Descriptor table from the source heap.
     * @param dstHeap Destination for the copy operation.
     * @param dstTable Descriptor table from the destination heap.
     * @return Returns the number of descriptors copied over.
     * @throws InvalidDescriptorTable Thrown if the source table is 
     * larger than the destination table or if any of them are invalid.
     */
    static uint32_t copyTable(D3D12DescHeap &srcHeap, const DescriptorTable &srcTable, 
        D3D12DescHeap &dstHeap, const DescriptorTable &dstTable);

    /// Maximal number of descriptors which can be created on this heap.
    uint32_t size() const;

    /// How many descriptors on this heap are used up?
    uint32_t usedDescriptors() const;

    /// How many more descriptors can be created on this heap?
    uint32_t freeDescriptors() const;

    /// Clear all allocated descriptor handles, making them invalid.
    void clear();

    /// Type of this descriptor heap.
    ::D3D12_DESCRIPTOR_HEAP_TYPE type() const;
private:
    /// Range of descriptors.
    struct DescriptorBlock
    {
        /// Begin iterator for the descriptor handles.
        ::CD3DX12_CPU_DESCRIPTOR_HANDLE begin{ };
        /// End iterator for the descriptor handles.
        ::CD3DX12_CPU_DESCRIPTOR_HANDLE end{ };

        /// Is given handle in this block?
        bool isInBlock(const ::CD3DX12_CPU_DESCRIPTOR_HANDLE &handle) const;
        /// Is given descriptor table in this block?
        bool isInBlock(const DescriptorTable &table, uint32_t descriptorSize) const;
        /// Is given handle first in this block?
        bool isFirst(const ::CD3DX12_CPU_DESCRIPTOR_HANDLE &handle) const;
        /// Is given handle last in this block?
        bool isLast(const ::CD3DX12_CPU_DESCRIPTOR_HANDLE &handle, uint32_t descriptorSize) const;
        /// Is given handle one before the first in this block?
        bool isOneBefore(const ::CD3DX12_CPU_DESCRIPTOR_HANDLE &handle, uint32_t descriptorSize) const;
        /// Is given handle one past the last handle in this block?
        bool isOnePast(const ::CD3DX12_CPU_DESCRIPTOR_HANDLE &handle) const;
        /// Is this block empty?
        bool isEmpty() const;

        /// Size of this block in a number of handles.
        uint32_t sizeInHandles(uint32_t descriptorSize) const;
    }; // struct DescriptorBlock

    /**
     * Create a descriptor heap with provided parameters.
     * @param device Where should the descriptor heap be created.
     * @param desc Description of requested descriptor heap.
     * @return Returns the created descriptor heap.
     * @throws util::winexception Thrown when error occurs.
     */
    ComPtr<D3D12DescHeapT> createDescHeap(D3D12Device &device, const ::D3D12_DESCRIPTOR_HEAP_DESC &desc);

    /**
     * Get the next free handle.
     * @return Returns the first available free handle.
     * @throws DescHeapFull Thrown if there are no free 
     * handles.
     */
    ::CD3DX12_CPU_DESCRIPTOR_HANDLE nextFreeHandle();

    /**
     * Mark given handle as allocated.
     * @param handle Handle to allocate.
     * @throws DescHeapFull Thrown if the handle is already 
     * allocated.
     * @throws InvalidDescriptorHandle Thrown if the handle 
     * is not part of this heap.
     */
    void allocateHandle(const ::CD3DX12_CPU_DESCRIPTOR_HANDLE &handle);

    /**
     * Check whether the provided handle is between 2 descriptor 
     * handle blocks.
     * @param first First block of handles.
     * @param second Second block of handles.
     * @param handle Handle to check.
     * @return Returns true if the handle is between 2 given blocks.
     */
    static bool isBetweenBlocks(const DescriptorBlock &first, const DescriptorBlock &second, 
        const ::CD3DX12_CPU_DESCRIPTOR_HANDLE &handle);

    /**
     * Check whether 2 blocks can be joined.
     * @param first First block of handles.
     * @param second Second block of handles.
     * @return Returns true if the blocks are 
     * adjacent and can be joined.
     */
    static bool isAdjacent(const DescriptorBlock &first, const DescriptorBlock &second);

    /**
     * Free given handle, allowing its reuse.
     * This operation does nothing for non-allocated handles.
     * @param handle Handle to free.
     */
    void freeHandle(const ::CD3DX12_CPU_DESCRIPTOR_HANDLE &handle);

    /**
     * Allocate a block of descriptor handles for manual management.
     * @param sizeInHandles Requested size in a number of handles.
     * @return Returns table specification, which should not be 
     * modified and can be used by calling other methods of this 
     * heap.
     * @throws DescHeapFull Thrown if there is not enough space for 
     * allocation of a linear block of handles of specified size.
     */
    DescriptorTable findAllocateTable(uint32_t sizeInHandles);

    /**
     * Free whole block of handles, specified by a descriptor table.
     * This operation does nothing if the table is not allocated.
     * @param table Table which should be freed.
     */
    void freeTable(const DescriptorTable &table);

    /// Device where the heap is created.
    d3d12::D3D12Device *mDevice{ nullptr };
    /// Size of a single descriptor handle.
    uint32_t mDescHandleSize{ 0u };
    /// Inner pointer to the descriptor heap.
    ComPtr<D3D12DescHeapT> mDescHeap{ nullptr };
    /// Description of the currently created descriptor heap.
    ::D3D12_DESCRIPTOR_HEAP_DESC mDesc{ };

    /// List of free descriptors, divided into blocks.
    std::list<DescriptorBlock> mFreeBlocks{ };

    /// Total number of descriptors in this descriptor heap.
    uint32_t mTotalDescriptors{ 0u };
    /// Number of already allocated descriptors.
    uint32_t mAllocatedDescriptors{ 0u };

    /// Block representing the whole heap.
    DescriptorBlock mWholeHeapBlock;
protected:
    /// Un-initialized descriptor heap.
    D3D12DescHeap();

    /**
     * Create a descriptor heap with provided parameters.
     * @param device Where should the descriptor heap be created.
     * @param type Type of the descriptor heap.
     * @param numDescriptors Maximal number of descriptors create 
     * table on the heap.
     * @param flags Flags for the descriptor heap.
     * @throws Throws util::winexception when error occurs.
     */
    D3D12DescHeap(D3D12Device &device, ::D3D12_DESCRIPTOR_HEAP_TYPE type, 
        uint32_t numDescriptors, D3D12_DESCRIPTOR_HEAP_FLAGS flags);

    /**
     * Create a descriptor heap with provided parameters.
     * @param device Where should the descriptor heap be created.
     * @param desc Description of requested descriptor heap.
     * @throws Throws util::winexception when error occurs.
     */
    D3D12DescHeap(D3D12Device &device, const ::D3D12_DESCRIPTOR_HEAP_DESC &desc);
}; // class D3D12DescHeap

} // namespace d3d12

} // namespace res

} // namespace quark
