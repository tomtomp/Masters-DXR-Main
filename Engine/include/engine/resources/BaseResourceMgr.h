/**
 * @file resources/BaseResourceMgr.h
 * @author Tomas Polasek
 * @brief Base class for all resource managers.
 */

#pragma once

#include "engine/util/Types.h"

/// Namespace containing the engine code.
namespace quark
{

/// Namespace containing resource management helpers.
namespace res
{

/// Base class for resource managers.
class BaseResourceMgr
{
public:
    BaseResourceMgr() = default;
    virtual ~BaseResourceMgr() = default;

    // Managers cannot be copied.
    BaseResourceMgr(const BaseResourceMgr &other) = delete;
    BaseResourceMgr &operator=(const BaseResourceMgr &rhs) = delete;
    // Allow moving managers.
    BaseResourceMgr(BaseResourceMgr &&other) = default;
    BaseResourceMgr &operator=(BaseResourceMgr &&rhs) = default;
private:
protected:
}; // class BaseResourceMgr

} // namespace res

} // namespace quark
