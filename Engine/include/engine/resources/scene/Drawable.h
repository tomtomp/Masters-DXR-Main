/**
 * @file resources/scene/Drawable.h
 * @author Tomas Polasek
 * @brief Aggregation of data required for drawing 
 * entities.
 */

#pragma once

#include "engine/resources/BaseResource.h"

#include "engine/helpers/math/Transform.h"
#include "engine/resources/render/MeshMgr.h"

/// Namespace containing the engine code.
namespace quark
{

/// Namespace containing resource management helpers.
namespace res
{

/// Resources used for virtual scene representation.
namespace scene
{

/**
 * Structure containing information about an entity 
 * which can be rendered.
 */
struct Drawable : BaseResource
{
    /// Initialize empty drawable.
    Drawable() = default;

    /// Initialize the drawable information.
    Drawable(helpers::math::Transform &model, const res::rndr::MeshHandle &mesh) :
        modelMatrix{ model }, meshHandle{ mesh }
    { }

    /// Model matrix of the entity, transforming it into world space.
    helpers::math::Transform &modelMatrix;

    /// Mesh of the entity.
    res::rndr::MeshHandle meshHandle;
}; // struct Drawable

} // namespace scene

} // namespace res

} // namespace quark
