/**
 * @file resources/scene/SubScene.h
 * @author Tomas Polasek
 * @brief Container for data describing the 
 * content of a sub-scene.
 */

#pragma once

#include "engine/util/SceneGraph.h"
#include "engine/scene/ECS.h"
#include "engine/scene/SceneComponents.h"

/// Namespace containing the engine code.
namespace quark
{

/// Namespace containing resource management helpers.
namespace res
{

/// Resources used for virtual scene representation.
namespace scene
{

/**
 * Container for data describing the content of 
 * a sub-scene.
 */
class SubScene : BaseResource
{
public:
    /// Type of the scene graph used in the sub-scene.
    using SceneGraphT = util::SceneGraph<quark::scene::EntityId, quark::scene::comp::Transform*>;

    /**
     * Create unitialized sub-scene.
     */
    SubScene();

    /// Free resources and delete entities.
    virtual ~SubScene();

    // No copying.
    SubScene(const SubScene &other) = delete;
    SubScene &operator=(const SubScene &other) = delete;
    // Allow moving.
    SubScene(SubScene &&other) noexcept = default;
    SubScene &operator=(SubScene &&other) noexcept = default;

    /**
     * Create entity within this sub-scene.
     * @param sceneId Identifier of the primary scene.
     * @param name Name of the entity.
     * @return Returns handle to the entity.
     */
    quark::scene::EntityHandle createEntity(quark::scene::SceneIdentifier sceneId, 
        const std::string &name = "");

    /**
     * Destroy entity within this sub-scene.
     * @param sceneId Identifier of the primary scene.
     * @param entity Handle to the entity which should be destroyed.
     * @return Returns true if the destruction was successful.
     */
    bool destroyEntity(quark::scene::SceneIdentifier sceneId, 
        quark::scene::EntityHandle &entity);

    /**
     * Set the parent entity as the parent of the given 
     * child entity within the scene-graph of this sub-scene.
     * @param parent The parent entity.
     * @param child The child entity.
     * @return Returns true, if the operation was completed 
     * successfuly.
     */
    bool setEntityAsParentOf(const quark::scene::EntityHandle &parent, 
        const quark::scene::EntityHandle &child);

    /**
     * Refresh the sub-scene structure, recalculating the scene-graph.
     */
    void refresh();

    /**
     * Signal to the scene graph that the transform of provided entity 
     * has changed its value.
     */
    void transformChanged(const quark::scene::EntityHandle &entity);

    /// Get identifier of this sub-scene.
    quark::scene::SceneIdentifier id() const;

    /// Swap sub-scenes.
    void swap(SubScene &other) noexcept;

    /// Swap sub-scenes.
    friend void swap(SubScene &first, SubScene &second) noexcept
    { first.swap(second); }
private:
    // Allow manager access to the private members.
    friend class SubSceneMgr;

    /**
     * Initialize the sub-scene.
     * @param universe Universe where entities within this sub-scene 
     * will be created.
     * @param identifier Unique identifier of this sub-scene, used 
     * for distinguishing between sub-scenes in the universe.
     */
    void initialize(quark::scene::SceneUniverse &universe, quark::scene::SceneIdentifier identifier);

    /**
     * Remove entities from the ECS universe which belong to 
     * this sub-scene.
     */
    void removeSubSceneEntities();

    /**
     * Get iterator in the scene-graph, for provided entity.
     * @param entity Entity to search for.
     */
    SceneGraphT::iterator sceneGraphEntity(const quark::scene::EntityHandle &entity);

    /// Universe containing entities of this sub-scene.
    quark::scene::SceneUniverse *mUniverse{ nullptr };
    /// Unique identifier of this sub-scene.
    quark::scene::SceneIdentifier mIdentifier{ 0u };

    /// Scene graph of this sub-scene.
    SceneGraphT mSceneGraph{ };
protected:
}; // class Scene

} // namespace scene

} // namespace res

} // namespace quark
