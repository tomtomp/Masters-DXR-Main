/**
 * @file resources/scene/Scene.h
 * @author Tomas Polasek
 * @brief Scene data container. Contains resource managers with 
 * data required to render the scene and a scene graph containing 
 * all of the objects.
 */

#pragma once

#include "engine/resources/BaseResource.h"

#include "engine/resources/render/BufferMgr.h"
#include "engine/resources/render/BufferViewMgr.h"
#include "engine/resources/render/DescriptorMgr.h"
#include "engine/resources/render/TextureMgr.h"
#include "engine/resources/render/StaticSamplerMgr.h"
#include "engine/resources/render/MaterialMgr.h"
#include "engine/resources/render/MeshMgr.h"
#include "engine/resources/scene/SubSceneMgr.h"

#include "engine/resources/render/GpuMemoryMgr.h"

#include "engine/renderer/ElementFormat.h"
#include "engine/renderer/SamplerFiltering.h"

#include "engine/scene/ECS.h"
#include "engine/scene/SceneComponents.h"

/// Namespace containing the engine code.
namespace quark
{

/// Namespace containing resource management helpers.
namespace res
{

/// Resources used for virtual scene representation.
namespace scene
{

/**
 * Scene data container. Contains resource managers with 
 * data required to render the scene and a scene graph containing 
 * all of the objects.
 * Each scene may have multiple sub-scenes, which share resources.
 */
class Scene : BaseResource
{
public:
    /**
     * Create un-initialized scene.
     */
    Scene();

    /// Free resources and delete entities.
    virtual ~Scene();

    // No copying.
    Scene(const Scene &other) = delete;
    Scene &operator=(const Scene &other) = delete;
    // Allow moving.
    Scene(Scene &&other) noexcept = default;
    Scene &operator=(Scene &&other) noexcept = default;

    /**
     * Finalize added resources and generate required 
     * structures for scene rendering.
     * @warning In the final stage of buffer creation, the 
     * command list containing copy commands is asynchronously 
     * executed in the provided command queue.
     * @throws util::winexception Thrown on any D3D12 error.
     */
    void finalizeChanges();

    /**
     * Set data which should be held until finalizeChanges() is 
     * finished.
     * This data may include buffer content, which will be copied 
     * to the GPU.
     * @param data Data to keep alive until finalizeChanges() is 
     * finished.
     */
    void setTransientData(std::any &&data);

    /**
     * Create or retrieve already existing buffer.
     * Creation must be finalized by calling finalizeChanges().
     * @param id Identifier of the buffer.
     * @param data Data to load into the buffer. Buffer 
     * must be valid until finalizeChanges() is called!
     * @param sizeInBytes Number of bytes to load from 
     * the data buffer into the GPU-side buffer.
     * @param desc Description of the requested buffer.
     * @return Returns handle to the buffer.
     */
    res::rndr::BufferHandle createBuffer(const std::string &id, const void *data,
        std::size_t sizeInBytes);

    /**
     * Create or retrieve already existing buffer.
     * Creation must be finalized by calling finalizeChanges().
     * This version uses automatically generated identifier.
     * @param data Data to load into the buffer. Buffer 
     * must be valid until finalizeChanges() is called!
     * @param sizeInBytes Number of bytes to load from 
     * the data buffer into the GPU-side buffer.
     * @param desc Description of the requested buffer.
     * @return Returns handle to the buffer.
     */
    res::rndr::BufferHandle createBuffer(const void *data, std::size_t sizeInBytes);

    /**
     * Create or retrieve already existing buffer view.
     * Creation must be finalized by calling finalizeChanges().
     * @param id Identifier of the buffer view.
     * @param targetBuffer Handle to the buffer viewed through 
     * the buffer view. Buffer does not need to be initialized 
     * at this moment. At the time of update(), it should already 
     * be initialized.
     * @param elementFormat Format of a single element in the 
     * buffer view and a number of its components.
     * @param numElements Number of elements in the buffer.
     * @param byteOffset Offset of this view from the start of 
     * the target buffer.
     * @param byteLength Length of the buffer view in bytes. May 
     * be set to 0 for automatic calculation from number of 
     * elements and their stride.
     * @param byteStride Stride of the elements in the buffer. Set 
     * to zero for elements which go right after one another.
     * @return Returns handle to the buffer view.
     */
    res::rndr::BufferViewHandle createBufferView(const std::string &id, 
        const res::rndr::BufferHandle &targetBuffer, 
        quark::rndr::ElementFormat elementFormat, std::size_t numElements, 
        std::size_t byteOffset = 0u, std::size_t byteLength = 0u, std::size_t byteStride = 0u);

    /**
     * Create or retrieve already existing buffer view.
     * Creation must be finalized by calling finalizeChanges().
     * This version uses automatically generated identifier.
     * @param targetBuffer Handle to the buffer viewed through 
     * the buffer view. Buffer does not need to be initialized 
     * at this moment. At the time of update(), it should already 
     * be initialized.
     * @param elementFormat Format of a single element in the 
     * buffer view and a number of its components.
     * @param numElements Number of elements in the buffer.
     * @param byteOffset Offset of this view from the start of 
     * the target buffer.
     * @param byteLength Length of the buffer view in bytes. May 
     * be set to 0 for automatic calculation from number of 
     * elements and their stride.
     * @param byteStride Stride of the elements in the buffer. Set 
     * to zero for elements which go right after one another.
     * @return Returns handle to the buffer view.
     */
    res::rndr::BufferViewHandle createBufferView( 
        const res::rndr::BufferHandle &targetBuffer, 
        quark::rndr::ElementFormat elementFormat, std::size_t numElements, 
        std::size_t byteOffset = 0u, std::size_t byteLength = 0u, std::size_t byteStride = 0u);

    /**
     * Create or retrieve already existing texture.
     * Creation must be finalized by calling finalizeChanges().
     * @param id Identifier of the texture.
     * @param data Data to load into the texture. Texture 
     * must be valid until finalizeChanges() is called!
     * @param sizeInBytes Number of bytes to load from 
     * the texture buffer into the GPU-side texture.
     * @param elementFormat Format of a single texel.
     * @param width Width of the texture in pixels.
     * @param height Height of the texture in pixels.
     * @param mipMapCount Number of mip-maps in this 
     * texture. Specify 0u for just the base texture.
     * @return Returns handle to the texture.
     */
    res::rndr::TextureHandle createTexture(const std::string &id, const void *data,
        std::size_t sizeInBytes, quark::rndr::ElementFormat elementFormat, 
        std::size_t width, std::size_t height, std::size_t mipMapCount);

    /**
     * Create or retrieve already existing texture.
     * Creation must be finalized by calling finalizeChanges().
     * This version uses automatically generated identifier.
     * @param data Data to load into the texture. Texture  
     * must be valid until finalizeChanges() is called!
     * @param sizeInBytes Number of bytes to load from 
     * the data texture into the GPU-side texture.
     * @param elementFormat Format of a single texel.
     * @param width Width of the texture in pixels.
     * @param height Height of the texture in pixels.
     * @param mipMapCount Number of mip-maps in this 
     * texture. Specify 0u for just the base texture.
     * @return Returns handle to the texture.
     */
    res::rndr::TextureHandle createTexture(const void *data, std::size_t sizeInBytes, 
        quark::rndr::ElementFormat elementFormat, std::size_t width, 
        std::size_t height, std::size_t mipMapCount);

    /**
     * Create or retrieve already existing static sampler.
     * @param id Identifier of the static sampler.
     * @param filtering Filtering specification of the sampler.
     * @param edgeFiltering How to sample when out of bounds.
     * @return Returns handle to the static sampler.
     */
    res::rndr::StaticSamplerHandle createSampler(const std::string &id,
        quark::rndr::MinMagMipFilter filtering, quark::rndr::EdgeFilterUVW edgeFiltering);

    /**
     * Create or retrieve already existing static sampler.
     * This version uses automatically generated identifier.
     * @param id Identifier of the buffer view.
     * @param filtering Filtering specification of the sampler.
     * @param edgeFiltering How to sample when out of bounds.
     * @return Returns handle to the static sampler.
     */
    res::rndr::StaticSamplerHandle createSampler(
        quark::rndr::MinMagMipFilter filtering, quark::rndr::EdgeFilterUVW edgeFiltering);

    /**
     * Create or retrieve already existing material.
     * Returns material handle points to a default 
     * initialized material.
     * @param id Identifier of the material.
     * @return Returns handle to the material.
     */
    res::rndr::MaterialHandle createMaterial(const std::string &id);

    /**
     * Create or retrieve already existing mesh.
     * Returns material handle points to a default 
     * initialized mesh.
     * @param id Identifier of the mesh.
     * @return Returns handle to the mesh.
     */
    res::rndr::MeshHandle createMesh(const std::string &id);

    /**
     * Create or retrieve already existing sub-scene.
     * @param id Identifier of the sub-scene.
     * @return Returns handle to the sub-scene.
     */
    res::scene::SubSceneHandle createSubScene(const std::string &id);

    /**
     * Set which sub-scene is currently being displayed.
     * Does not perform any additional operations with the 
     * scene universe!
     * @param subScene Handle to the new current sub-scene.
     */
    void setCurrentSubScene(const res::scene::SubSceneHandle &subScene);

    /// Get total number of scenes currently contained within this scene.
    std::size_t numSubScenes() const;

    /**
     * Get subscene from specified index.
     * Order of scenes is dictated by the order of their 
     * addition to this scene.
     * @param idx Index of the sub-scene.
     * @return Returns handle to the sub-scene.
     */
    res::scene::SubSceneHandle getSubScene(std::size_t idx) const;

    /**
     * Create entity within given sub-scene.
     * @param subScene Sub-scene which should contain the new 
     * entity.
     * @param name Name of the entity.
     * @return Returns handle to the entity.
     */
    quark::scene::EntityHandle createEntity(res::scene::SubSceneHandle &subScene, 
        const std::string &name = "");

    /**
     * Create entity in the current sub-scene.
     * @param name Name of the entity.
     * @return Returns handle to the entity or an invalid one 
     * if no current sub-scene is selected.
     */
    quark::scene::EntityHandle createEntity(const std::string &name = "");

    /**
     * Destroy provided entity, which should be contained in the 
     * provided sub-scene.
     * @param subScene Sub-scene which contains the entity.
     * @param entity Entity which should be destroyed. After calling 
     * this method, it will be invalid.
     * @return Returns true if the destruction was successful.
     */
    bool destroyEntity(res::scene::SubSceneHandle &subScene, 
        quark::scene::EntityHandle &entity);

    /**
     * Destroy provided entity, which should be contained in the 
     * current sub-scene.
     * @param entity Entity which should be destroyed. After calling 
     * this method, it will be invalid.
     * @return Returns true if the destruction was successful.
     */
    bool destroyEntity(quark::scene::EntityHandle &entity);

    /**
     * Set the parent entity as the parent of the given 
     * child entity within the scene-graph of given sub-scene.
     * @param subScene Sub-scene which contains both of the 
     * entities.
     * @param parent The parent entity.
     * @param child The child entity.
     * @return Returns true, if the operation was completed 
     * successfully.
     */
    bool setEntityAsParentOf(res::scene::SubSceneHandle &subScene, 
        const quark::scene::EntityHandle &parent, const quark::scene::EntityHandle &child);

    /// Get specification of a scene for given sub-scene.
    quark::scene::SceneSpecification sceneSpecification(const res::scene::SubSceneHandle &subScene) const;
    /// Get specification of the currently selected sub-scene.
    quark::scene::SceneSpecification currentSceneSpecification() const;

    /**
     * Signal to the scene graph of the currently selected sub-scene 
     * that the transform of provided entity has changed its value.
     */
    void transformChanged(const quark::scene::EntityHandle &entity);

    /**
     * Refresh the currently selected sub-scene and scene structure.
     */
    void refresh();

    /// Has this scene been finalized?
    bool finalized() const;

    /// Swap scenes.
    void swap(Scene &other) noexcept;

    /// Swap scenes.
    friend void swap(Scene &first, Scene &second) noexcept
    { first.swap(second); }
private:
    // Allow manager access to the private members.
    friend class SceneMgr;

    /**
     * Initialize the scene and its resource management.
     * @param gpuMemoryMgr Manager of target GPU memory, used 
     * for storing scene resources.
     * @param universe Universe where entities within this scene 
     * will be created.
     * @param identifier Unique identifier of this scene, used 
     * for distinguishing between scenes in the universe.
     */
    void initialize(rndr::GpuMemoryMgr &gpuMemoryMgr, 
        quark::scene::SceneUniverse &universe, 
        quark::scene::SceneIdentifier identifier);

    /// Universe containing entities of this scene.
    quark::scene::SceneUniverse *mUniverse{ nullptr };
    /// Unique identifier of this scene.
    quark::scene::SceneIdentifier mIdentifier{ 0u };

    /// Management of GPU-side buffers.
    std::shared_ptr<res::rndr::BufferMgr> mBufferMgr{ };
    /// Management of views to GPU-side buffers.
    std::shared_ptr<res::rndr::BufferViewMgr> mBufferViewMgr{ };
    /// Management of GPU-side textures.
    std::shared_ptr<res::rndr::TextureMgr> mTextureMgr{ };
    /// Management of texture samplers.
    std::shared_ptr<res::rndr::StaticSamplerMgr> mSamplerMgr{ };
    /// Management of object materials.
    std::shared_ptr<res::rndr::MaterialMgr> mMaterialMgr{ };
    /// Management of meshes used by objects in this scene.
    std::shared_ptr<res::rndr::MeshMgr> mMeshMgr{ };
    /// Management of sub-scenes, which contain entities sharing scene resources.
    std::shared_ptr<res::scene::SubSceneMgr> mSubSceneMgr{ };

    /// Handle to the currently active sub-scene.
    res::scene::SubSceneHandle mCurrentSubScene{ };

    /// Data which should be kept alive until finalizeChanges() is finished.
    std::any mTransientData{ };
    /// Has this scene been finalized?
    bool mFinalized{ false };
protected:
}; // class Scene

} // namespace scene

} // namespace res

} // namespace quark
