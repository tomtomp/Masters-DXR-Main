/**
 * @file resources/scene/SubSceneMgr.h
 * @author Tomas Polasek
 * @brief Manager of sub-scenes.
 */

#pragma once

#include "engine/resources/BaseHandleMgr.h"

#include "engine/resources/scene/SubScene.h"

/// Namespace containing the engine code.
namespace quark
{

/// Namespace containing resource management helpers.
namespace res
{

/// Resources used for virtual scene representation.
namespace scene
{
    
/**
 * Manager of sub-scenes. Unlike other managers, sub-scenes 
 * are kept referenced, until remove is called.
 */
class SubSceneMgr : public BaseHandleMgr<SubScene, uint16_t, std::string, uint8_t>, public util::PointerType<SubSceneMgr>
{
public:
    /// Renaming resource handle for readability.
    using SubSceneHandle = ThisMgrT::ResourceHandle;

    /**
     * Create instance of this manager.
     * @return Returns pointer to the new instance.
     * @warning Before any more actions are used, this manager 
     * must be initialized!
     */
    static PtrT createMgr()
    { return createInstance<SubSceneMgr>(); }

    /**
     * Initialize manager structure, creates an empty manager.
     * @param universe Universe used by sub-scenes created in this 
     * manager.
     * @param sceneIdentifier Identifier of the primary scene.
     * @throws res::rndr::BufferMgr::WrongCmdQueueTypeException 
     * Thrown when provided command queue is not a copy queue.
     */
    void initialize(quark::scene::SceneUniverse &universe, 
        quark::scene::SceneIdentifier sceneIdentifier);

    /// Free all resources.
    virtual ~SubSceneMgr() = default;

    // No copying.
    SubSceneMgr(const SubSceneMgr &other) = delete;
    SubSceneMgr &operator=(const SubSceneMgr &other) = delete;
    // No moving.
    SubSceneMgr(SubSceneMgr &&other) = delete;
    SubSceneMgr &operator=(SubSceneMgr &&other) = delete;

    /**
     * Create or retrieve already existing sub-scene.
     * @param id Identifier of the sub-scene.
     * @return Returns handle to the sub-scene.
     */
    SubSceneHandle create(const IdentifierT &id);

    /**
     * Create a new sub-scene with automatically generated 
     * identifier.
     * @return Returns handle to the sub-scene.
     */
    SubSceneHandle create();

    /**
     * Remove sub-scene identified by provided identifier 
     * from memory.
     * If no sub-scene with given identifier exists, no 
     * action will be taken.
     * @param id Identifier of the sub-scene.
     * @warning Update method must be called to 
     * finish the removal process.
     */
    void remove(const IdentifierT &id);

    /**
     * Remove sub-scene specified by provided handle 
     * from memory.
     * @param handle Handle to the sub-scene.
     * @warning Update method must be called to 
     * finish the removal process.
     */
    void remove(const SubSceneHandle &handle);

    /**
     * Clear all loaded sub-scenes from memory.
     * @warning Update method must be called to 
     * finish the removal process.
     */
    void clear();

    /// Begin iterator to the list of loaded sub-scenes.
    auto begin() 
    { return mLoadedSubScenes.begin(); }

    /// Begin iterator to the list of loaded sub-scenes.
    auto begin() const
    { return mLoadedSubScenes.cbegin(); }

    /// End iterator to the list of loaded sub-scenes.
    auto end() 
    { return mLoadedSubScenes.end(); }

    /// End iterator to the list of loaded sub-scenes.
    auto end() const
    { return mLoadedSubScenes.cend(); }

    /// Get current number of active sub-scenes.
    std::size_t numSubScenes() const;

    /**
     * Get subscene from specified index.
     * Order of scenes is dictated by the order of their 
     * addition to this scene.
     * @param idx Index of the sub-scene.
     * @return Returns handle to the sub-scene.
     */
    const SubSceneHandle &getSubScene(std::size_t idx) const;

    /**
     * Perform book-keeping operation such as: 
     *   Free any sub-scenes with 0 in the reference counter.
     */
    virtual void update() override final;
private:
    /// Base name of generated sub-scene identifiers.
    static constexpr const char *UNIQUE_ID_BASE{ "SubScene" };

    /**
     * Create or retrieve already existing sub-scene.
     * @param id Identifier of the sub-scene.
     * @return Returns handle to the sub-scene.
     */
    SubSceneHandle createInner(const IdentifierT &id);

    /**
     * Generate unique identifier.
     * @return Returns a unique scene identifier.
     */
    IdentifierT generateUniqueId();

    /**
     * Initialize provided scene with its unique 
     * identifier and a reference to the ECS.
     * @param subScene Sub-scene to initialize.
     */
    void initializeSubScene(SubScene &subScene);

    /// Universe used for storing sub-scene entities.
    quark::scene::SceneUniverse *mUniverse{ nullptr };
    /// Identifier of the primary scene.
    quark::scene::SceneIdentifier mSceneIdentifier{ 0u };
    /// Counter used for generating unique sub-scene indices.
    quark::scene::SceneIdentifier mSubSceneCounter{ 0u };

    /// List of currently loaded sub-scenes.
    std::vector<SubSceneHandle> mLoadedSubScenes{ };
protected:
    /**
     * Initialize manager structure, creates an empty manager.
     * @warning Before any more actions are used, this manager 
     * must be initialized!
     */
    SubSceneMgr();

    /**
     * Initialize manager structure, creates an empty manager.
     * @param universe Universe used by sub-scenes created in this 
     * manager.
     * @param sceneIdentifier Identifier of the primary scene.
     * @throws res::rndr::BufferMgr::WrongCmdQueueTypeException 
     * Thrown when provided command queue is not a copy queue.
     */
    SubSceneMgr(quark::scene::SceneUniverse &universe, 
        quark::scene::SceneIdentifier sceneIdentifier);
}; // class SceneMgr

/// Handle to a sub-scene.
using SubSceneHandle = ::quark::res::scene::SubSceneMgr::SubSceneHandle;

} // namespace scene

} // namespace res

} // namespace quark
