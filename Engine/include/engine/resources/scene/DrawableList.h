/**
 * @file resources/scene/DrawableList.h
 * @author Tomas Polasek
 * @brief List of drawables.
 */

#pragma once

#include "engine/util/VersionedVector.h"
#include "engine/resources/scene/Drawable.h"

/// Namespace containing the engine code.
namespace quark
{

/// Namespace containing resource management helpers.
namespace res
{

/// Resources used for virtual scene representation.
namespace scene
{

// TODO - Change to structure like: 
/*
struct DrawableList
{
    /// List of active lights in the scene.
    std::vector<Light> lights;
    /// Current list of all drawables.
    std::vector<Drawable> drawables;
    /// Indices of newly added drawables.
    // TODO - Change to a single index and all after it were added?
    std::vector<uint64_t> added;
    /// List of indices of entities which changed in some way.
    std::vector<uint64_t> changed;
    /// List of drawables which were removed.
    std::vector<Drawable> removed;
};
*/

/**
 * Data structure containing a list of drawable 
 * entities and a revision number.
 */
using DrawableList = util::VersionedVector<Drawable>;

} // namespace scene

} // namespace res

} // namespace quark
