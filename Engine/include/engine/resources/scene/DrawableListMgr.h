/**
 * @file resources/scene/DrawableListMgr.h
 * @author Tomas Polasek
 * @brief Manager of entity draw lists.
 */

#pragma once

#include "engine/resources/BaseHandleMgr.h"

#include "engine/resources/scene/DrawableList.h"

/// Namespace containing the engine code.
namespace quark
{

/// Namespace containing resource management helpers.
namespace res
{

/// Resources used for virtual scene representation.
namespace scene
{
    
/**
 * Manager of list of drawable entities.
 */
class DrawableListMgr : public BaseHandleMgr<DrawableList, uint16_t, std::string, uint8_t>, public util::PointerType<DrawableListMgr>
{
public:
    /// Renaming resource handle for readability.
    using DrawableListHandle = ThisMgrT::ResourceHandle;

    /**
     * Create instance of this manager.
     * @return Returns pointer to the new instance.
     */
    static PtrT createMgr()
    { return createInstance<DrawableListMgr>(); }

    /// Free all resources.
    virtual ~DrawableListMgr();

    // No copying.
    DrawableListMgr(const DrawableListMgr &other) = delete;
    DrawableListMgr &operator=(const DrawableListMgr &other) = delete;
    // No moving.
    DrawableListMgr(DrawableListMgr &&other) = delete;
    DrawableListMgr &operator=(DrawableListMgr &&other) = delete;

    /**
     * Create or retrieve already existing drawable list.
     * @param id Identifier of the drawable list.
     * @return Returns handle to the drawable list.
     */
    DrawableListHandle create(const IdentifierT &id);

    /**
     * Create a new drawable list with automatically generated 
     * identifier.
     * @return Returns handle to the drawable list.
     */
    DrawableListHandle create();


    /**
     * Perform book-keeping operation such as: 
     *   Free any drawable lists with 0 in the reference counter.
     */
    virtual void update() override final;
private:
    /// Base name of generated drawable lists identifiers.
    static constexpr const char *UNIQUE_ID_BASE{ "DrawList" };

    /**
     * Create or retrieve already existing drawable list.
     * @param id Identifier of the drawable list.
     * @return Returns handle to the drawable list.
     */
    DrawableListHandle createInner(const IdentifierT &id);

    /**
     * Generate unique identifier.
     * @return Returns a unique scene identifier.
     */
    IdentifierT generateUniqueId();

    /// Counter used for generating unique drawable list indices.
    std::size_t mDrawableListCounter{ 0u };
protected:
    /**
     * Initialize empty drawable list manager.
     */
    DrawableListMgr();
}; // class DrawableListMgr

/// Handle to a drawable list.
using DrawableListHandle = ::quark::res::scene::DrawableListMgr::DrawableListHandle;

} // namespace scene

} // namespace res

} // namespace quark
