/**
 * @file resources/scene/SceneMgr.h
 * @author Tomas Polasek
 * @brief Manager of scenes.
 */

#pragma once

#include "engine/resources/BaseHandleMgr.h"

#include "engine/resources/scene/Scene.h"

/// Namespace containing the engine code.
namespace quark
{

/// Namespace containing resource management helpers.
namespace res
{

/// Resources used for virtual scene representation.
namespace scene
{
    
/**
 * Manager of scenes. Unlike other managers, scenes 
 * are kept referenced, until remove is called.
 */
class SceneMgr : public BaseHandleMgr<Scene, uint16_t, std::string, uint8_t>, public util::PointerType<SceneMgr>
{
public:
    /// Renaming resource handle for readability.
    using SceneHandle = ThisMgrT::ResourceHandle;

    /**
     * Create instance of this manager.
     * @return Returns pointer to the new instance.
     */
    static PtrT createMgr()
    { return createInstance<SceneMgr>(); }

    /**
     * Initialize manager structure, creates an empty manager.
     * @param gpuMemoryMgr Manager of target GPU memory, used 
     * for storing scene resources.
     * @param universe Universe where entities within this scene 
     * will be created.
     * @throws res::rndr::BufferMgr::WrongCmdQueueTypeException 
     * Thrown when provided command queue is not a copy queue.
     */
    void initialize(rndr::GpuMemoryMgr &gpuMemoryMgr, 
        quark::scene::SceneUniverse &universe);

    /// Free all resources.
    virtual ~SceneMgr() = default;

    // No copying.
    SceneMgr(const SceneMgr &other) = delete;
    SceneMgr &operator=(const SceneMgr &other) = delete;
    // No moving.
    SceneMgr(SceneMgr &&other) = delete;
    SceneMgr &operator=(SceneMgr &&other) = delete;

    /**
     * Get scene with given identifier or null handle 
     * if no such scene exists.
     * @param id Identifier of the scene.
     * @return Returns handle to the requested scene 
     * or null handle if no such scene exists.
     */
    SceneHandle getOrNull(const IdentifierT &id);

    /**
     * Create or retrieve already existing scene.
     * @param id Identifier of the scene.
     * @return Returns handle to the scene.
     */
    SceneHandle create(const IdentifierT &id);

    /**
     * Create a new scene with automatically generated 
     * identifier.
     * @return Returns handle to the scene.
     */
    SceneHandle create();

    /**
     * Remove scene identified by provided identifier 
     * from memory.
     * If no scene with given identifier exists, no 
     * action will be taken.
     * @param id Identifier of the scene.
     * @warning Update method must be called to 
     * finish the removal process.
     */
    void remove(const IdentifierT &id);

    /**
     * Remove scene specified by provided handle 
     * from memory.
     * @param handle Handle to the scene.
     * @warning Update method must be called to 
     * finish the removal process.
     */
    void remove(const SceneHandle &handle);

    /**
     * Clear all loaded scenes from memory.
     * @warning Update method must be called to 
     * finish the removal process.
     */
    void clear();

    /// Begin iterator to the list of loaded scenes.
    auto begin() const
    { return mLoadedScenes.cbegin(); }

    /// End iterator to the list of loaded scenes.
    auto end() const
    { return mLoadedScenes.cend(); }

    /**
     * Perform book-keeping operation such as: 
     *   Free any scenes with 0 in the reference counter.
     */
    virtual void update() override final;
private:
    /// Base name of generated scene identifiers.
    static constexpr const char *UNIQUE_ID_BASE{ "Scene" };

    /// Helper for unique identification of scenes.
    class SceneCounter
    {
    public:
        SceneCounter() = delete;

        /**
         * Get next unique scene ID between all scenes 
         * and increment the counter.
         */
        static quark::scene::SceneIdentifier nextUniqueSceneId();

        /**
         * Get the current value of the scene counter 
         * without incrementing it.
         */
        static quark::scene::SceneIdentifier currentUniqueSceneId();

        /**
         * Reset the unique ID counter.
         */
        static void resetUniqueSceneIds();
    private:
        /// Counter for unique scene identification within ECS.
        static inline quark::scene::SceneIdentifier sSceneCounter{ 0u };
    protected:
    }; // class SceneCounter

    /**
     * Create or retrieve already existing scene.
     * @param id Identifier of the scene.
     * @return Returns handle to the scene.
     */
    SceneHandle createInner(const IdentifierT &id);

    /**
     * Generate unique identifier.
     * @return Returns a unique scene identifier.
     */
    IdentifierT generateUniqueId();

    /**
     * Initialize provided scene with its unique 
     * identifier and a reference to the ECS.
     * @param scene Scene to initialize.
     */
    void initializeScene(Scene &scene);

    /// Management of buffer memory on the GPU.
    rndr::GpuMemoryMgr *mGpuMemoryMgr{ nullptr };
    /// Universe used for storing scene entities.
    quark::scene::SceneUniverse *mUniverse{ nullptr };

    /// List of currently loaded scenes
    std::vector<SceneHandle> mLoadedScenes{ };
protected:
    /**
     * Initialize manager structure, creates an empty manager.
     * @warning Before any more actions are used, this manager 
     * must be initialized!
     */
    SceneMgr();

    /**
     * Initialize manager structure, creates an empty manager.
     * @param gpuMemoryMgr Manager of target GPU memory, used 
     * for storing scene resources.
     * @param universe Universe used by scenes created in this 
     * manager.
     * @throws res::rndr::BufferMgr::WrongCmdQueueTypeException 
     * Thrown when provided command queue is not a copy queue.
     */
    SceneMgr(rndr::GpuMemoryMgr &gpuMemoryMgr, 
        quark::scene::SceneUniverse &universe);
}; // class SceneMgr

/// Handle to a scene.
using SceneHandle = ::quark::res::scene::SceneMgr::SceneHandle;

} // namespace scene

} // namespace res

} // namespace quark
