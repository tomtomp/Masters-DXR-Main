/**
 * @file resources/BaseFile.h
 * @author Tomas Polasek
 * @brief Base file which can be specialized for any file system.
 */

#pragma once

#include "engine/resources/BaseResource.h"

/// Namespace containing the engine code.
namespace quark
{

/// Namespace containing resource management helpers.
namespace res
{

/**
 * Wrapper around a filename/path.
 * @tparam StrTT Type of string used.
 * @tparam ImplTT Type of filesystem implementation.
 * @tparam Divider Divider used for the path.
 */
template <typename StrTT, typename ImplTT, typename StrTT::value_type Divider = '/'>
class BaseFile : BaseResource
{
public:
    /// Exception thrown when file with given filename/path has not been found.
    struct FileNotFoundException : public std::exception
    {
        FileNotFoundException(const char *msg) :
            std::exception(msg)
        { }
    }; // struct FileNotFoundException

    /// Tag used when specifying that the file should be checked for existence.
    struct CheckExistsTag
    { };

    /// Type of string used.
    using StringT = StrTT;

    /// Initialize default file, with empty filename.
    BaseFile() = default;

    /// Free resources.
    ~BaseFile() = default;

    // Allow copying.
    BaseFile(const BaseFile &other) = default;
    BaseFile &operator=(const BaseFile &other) = default;
    // Allow moving.
    BaseFile(BaseFile &&other) = default;
    BaseFile &operator=(BaseFile &&other) = default;

    /**
     * Create a file with given filename, which may 
     * include a path.
     * @param filename Name of the file, may include 
     * a path.
     * @param checkExists When set to true, this 
     * constructor will check whether the file exists 
     * and throw FileNotFoundException if it does not. 
     * Search is performed from the current working 
     * directory.
     * @throws FileNotFoundException Thrown when 
     * checkExists is set to true and file could not 
     * be found from the current working directory.
     */
    explicit BaseFile(const StringT &filename);

    /**
     * Create a file with given filename, which may 
     * include a path.
     * @param filename Name of the file, may include 
     * a path.
     * @throws FileNotFoundException Thrown when 
     * file could not be found from the current 
     * working directory.
     */
    explicit BaseFile(const StringT &filename, CheckExistsTag);

    /**
     * Create a file with given filename, which may 
     * include a path. This constructor also performs 
     * a search for the file in given list of paths.
     * The final found file, including the path, will 
     * be used.
     * If there are multiple files with requested 
     * filename in the paths provided, the first one 
     * will be chosen.
     * @param filename Name of the file, may include 
     * a path.
     * @param paths List of paths to search through.
     * @throws FileNotFoundException Thrown when 
     * the file was not found in any of the paths.
     */
    explicit BaseFile(const StringT &filename, const std::initializer_list<StringT> &paths);

    /**
     * Create a file with given filename, which may 
     * include a path. This constructor also performs 
     * a search for the file in given list of paths.
     * The final found file, including the path, will 
     * be used.
     * If there are multiple files with requested 
     * filename in the paths provided, the first one 
     * will be chosen.
     * @param filename Name of the file, may include 
     * a path.
     * @param paths List of paths to search through.
     * @throws FileNotFoundException Thrown when 
     * the file was not found in any of the paths.
     */
    explicit BaseFile(const StringT &filename, const std::vector<StringT> &paths);

    /**
     * Get current file, including its path.
     * @return Returns currently held file path.
     */
    const StringT &file() const;

    /**
     * Get extension of the file contained in this wrapper.
     * @return Returns string representation of the 
     * extension, excluding the '.' . If there is no 
     * extension, empty string is returned instead.
     */
    StringT extension() const;

    /**
     * Get filename of the file contained in this wrapper.
     * @return Returns the filename without path for 
     * this file.
     */
    StringT filename() const;

    /**
     * Get path as it is written in the file path.
     * @return Returns the path of the current file. 
     * This path may be relative to the working 
     * directory.
     */
    StringT path() const;

    /**
     * Get full path of the current file.
     * @return Returns the full path of the current 
     * file.
     * TODO - Implement if needed.
     */
    //StringT fullPath() const;
    
    /**
     * Does the currently held file exist?
     * @return Returns true if the file exists.
     */
    bool exists() const;

    /**
     * Is there any filename/path contained?
     * @return Returns true if the filename is 
     * currently empty.
     */
    bool empty() const noexcept
    { return mFilePath.empty(); }

    /**
     * Is there any filename/path contained?
     * @return Returns true if there IS a 
     * filename/path contained.
     */
    explicit operator bool() const noexcept
    { return !empty(); }

    /**
     * Print file to the output stream.
     * @param out Output stream.
     * @param file File to print.
     * @return Returns the original output stream.
     */
    template <typename StrOT, typename ImplOT, typename StrOT::value_type DividerO>
    friend std::ostream &operator<<(std::ostream &out, const BaseFile<StrOT, ImplOT, DividerO> &file);
private:
    /**
     * Initialize the file by searching for given filename 
     * in a list of potential paths.
     * @tparam ItT Type of iterator for the list of paths.
     * @param filename File to search for.
     * @param begin Begin iterator for the list of paths.
     * @param end End iterator for the list of paths.
     * @throws FileNotFoundException Thrown when no path 
     * from the list contained provided file.
     */
    template <typename ItT>
    void initializeFromPaths(const StringT &filename, const ItT &begin, const ItT &end);

    /**
     * Concatenate path and filename without redundant 
     * slashes.
     * @param path Path part of the string.
     * @param filename Filename part of the string.
     * @return Concatenated path and filename without 
     * redundant slashes.
     */
    static StringT concatPathFile(const StringT &path, const StringT &filename);

    /**
     * Does the provided file exist?
     * @param filename Name/path of the file.
     * @return Returns true if the file exists.
     */
    static bool exists(const StringT &filename);

    /// Currently held file path.
    StringT mFilePath{ };
protected:
}; // class File

} // namespace res

} // namespace quark

// Template implementation

namespace quark
{

namespace res
{

template <typename StrTT, typename ImplTT, typename StrTT::value_type Divider>
BaseFile<StrTT, ImplTT, Divider>::BaseFile(const StringT &filename) : 
    mFilePath{ filename }
{ }

template <typename StrTT, typename ImplTT, typename StrTT::value_type Divider>
BaseFile<StrTT, ImplTT, Divider>::BaseFile(const StringT &filename, CheckExistsTag) :
    BaseFile(filename)
{
    if (!exists())
    { throw FileNotFoundException("File existence checking enabled, but the file does not exist!"); }
}

template <typename StrTT, typename ImplTT, typename StrTT::value_type Divider>
BaseFile<StrTT, ImplTT, Divider>::BaseFile(const StringT &filename, const std::initializer_list<StringT> &paths)
{ initializeFromPaths(filename, paths.begin(), paths.end()); }

template <typename StrTT, typename ImplTT, typename StrTT::value_type Divider>
BaseFile<StrTT, ImplTT, Divider>::BaseFile(const StringT &filename, const std::vector<StringT> &paths)
{ initializeFromPaths(filename, paths.begin(), paths.end()); }

template <typename StrTT, typename ImplTT, typename StrTT::value_type Divider>
const typename BaseFile<StrTT, ImplTT, Divider>::StringT &BaseFile<StrTT, ImplTT, Divider>::file() const
{ return mFilePath; }

template <typename StrTT, typename ImplTT, typename StrTT::value_type Divider>
typename BaseFile<StrTT, ImplTT, Divider>::StringT BaseFile<StrTT, ImplTT, Divider>::extension() const
{
    // Extension is the part after the last dot...
    const auto lastDot{ file().find_last_of(".") };
    if (lastDot != StringT::npos)
    { return file().substr(lastDot + 1u); }
    return {};
}

template <typename StrTT, typename ImplTT, typename StrTT::value_type Divider>
typename BaseFile<StrTT, ImplTT, Divider>::StringT BaseFile<StrTT, ImplTT, Divider>::filename() const
{
    // Filename is the part after the last slash (/ or \)...
    const auto lastSlash{ file().find_last_of(Divider) };
    if (lastSlash != StringT::npos)
    { return file().substr(lastSlash + 1u); }
    // Else: the whole string is the filename, since it doesn't have any slashes.
    return file();
}

template <typename StrTT, typename ImplTT, typename StrTT::value_type Divider>
typename BaseFile<StrTT, ImplTT, Divider>::StringT BaseFile<StrTT, ImplTT, Divider>::path() const
{
    // Path is the part before the last slash (/ or \)...
    const auto lastSlash{ file().find_last_of(Divider) };
    if (lastSlash != StringT::npos)
    { return file().substr(0u, lastSlash); }
    return {};
}

template <typename StrTT, typename ImplTT, typename StrTT::value_type Divider>
bool BaseFile<StrTT, ImplTT, Divider>::exists() const
{ return exists(file()); }

template <typename StrTT, typename ImplTT, typename StrTT::value_type Divider>
template <typename ItT>
void BaseFile<StrTT, ImplTT, Divider>::initializeFromPaths(const StringT &filename, const ItT &begin, const ItT &end)
{
    for (auto it = begin; it != end; ++it)
    { // For each of possible paths: 
        // Generate the path: 
        const auto filePath{ concatPathFile(*it, filename) };

        if (exists(filePath))
        { // We found a path which contains the file.
            mFilePath = filePath;
            return;
        }
    }

    throw FileNotFoundException("File has not been found in any of the provided paths!");
}

template <typename StrTT, typename ImplTT, typename StrTT::value_type Divider>
typename BaseFile<StrTT, ImplTT, Divider>::StringT BaseFile<StrTT, ImplTT, Divider>::concatPathFile(const StringT &path,
    const StringT &filename)
{
    if (path.empty())
    { return filename; }
    // Neither have a slash.
    if (path.back() != Divider && filename.front() != Divider)
    { return path + Divider + filename; }
    // Both have a slash.
    if (path.back() == Divider && filename.front() == Divider)
    { return path + filename.substr(1u); }
    // Only one has a slash.
    return path + filename;
}

template <typename StrTT, typename ImplTT, typename StrTT::value_type Divider>
bool BaseFile<StrTT, ImplTT, Divider>::exists(const StringT &filename)
{ return ImplTT::checkExists(filename.c_str()); }

template <typename StrOT, typename ImplOT, typename StrOT::value_type DividerO>
std::ostream & operator<<(std::ostream &out, const BaseFile<StrOT, ImplOT, DividerO> &file)
{
    out << file.file();
    return out;
}

}

}

// Template implementation end
