/**
 * @file resources/ResourceHandle.h
 * @author Tomas Polasek
 * @brief Wrapper around unique identifier and manager used to get a resource.
 */

#pragma once

#include "engine/resources/BaseResourceMgr.h"
#include "engine/resources/BaseResource.h"

/// Namespace containing the engine code.
namespace quark
{

/// Namespace containing resource management helpers.
namespace res
{

/**
 * Indirection for accessing resources. Contains a pointer to manager 
 * containing the target resource and unique index which specifies which 
 * object is being indexed.
 * @tparam MgrTT Type of the manager.
 * @tparam HandleImplTT Container for static implementation of following 
 * types: 
 *   ResourceT - Type of the resource held within.
 *   HolderT - Type of the wrapper around resource, optional.
 * methods: 
 *   bool valid(MgrTT*, const IdxTT&) - Returns whether such index is valid.
 *   ResTT &get(MgrTT*, const IdxTT&) - Returns reference to the resource, if 
 *     the resource does not exist, ResourceNotFound should be thrown.
 *   ResTT &getOrDefault(MgrTT*, const IdxTT&) - Returns reference to the resource, if 
 *     the resource does not exist, reference to a default resource should be returned 
 *     instead.
 *   HolderT &getHolder(MgrTT*, const IdxTT&) - Returns reference to the holder for given 
 *   resource index. if resource does not exist, ResourceNotFound should be thrown. This 
 *   function must exist, HolderT is defined!
 *   void refIncr(MgrTT*, const IdxTT&) - Called when number of references to the resource 
 *     has increased by one.
 *   void refDecr(MgrTT*, const IdxTT&) - Called when number of references to the resource 
 *     has decreased by one.
 * Further, any of the methods can throw InvalidHandleException when called with invalid handle.
 * @tparam ResTT Type of the resource.
 * @tparam IdxTT Type for the unique index specifying target resource. This type has to be 
 * default-constructible, which creates an invalid index and comparable to default-constructed 
 * value, which returns true only if the index is invalid.
 */
template <typename MgrTT, typename HandleImplTT, typename ResTT, typename IdxTT = uint16_t>
class ResourceHandle
{
public:
    /// Exception thrown when get is called on handler to non-existent resource.
    struct ResourceNotFoundException : public std::exception
    {
        ResourceNotFoundException(const char *msg) :
            std::exception(msg)
        { }
    }; // struct ResourceNotFoundException

    /// Exception thrown when invalid handle is used.
    struct InvalidHandleException : public std::exception
    {
        InvalidHandleException(const char *msg) :
            std::exception(msg)
        { }
    }; // struct InvalidHandleException

    /// Type of the manager who holds target resources.
    using MgrT = MgrTT;
    /// Holder for manager reference.
    using MgrHolderT = std::weak_ptr<MgrTT>;

    /// Type of indexing parameter.
    using IdxT = IdxTT;

    /// Container for implementation methods.
    using HandleImplT = HandleImplTT;

    /// Type of target resource.
    using ResT = typename HandleImplT::ResourceT;

private:
    // Helper structure for SFINAE detection of existence of HolderT type.
    template <typename HandleImplTTT>
    struct HolderTypeExtractor
    {
    private:
        template <typename T>
        static typename T::HolderT test(typename T::HolderT*);
        template <typename T>
        static void test(...);
    public:
        /// Type of the holder. If void, then the holder doesn't exist.
        using type = decltype(test<HandleImplTTT>(nullptr));
        /// When given class has a HolderT, this variable is true, else it's false.
        static constexpr bool hasHolder{ !std::is_same_v<type, void> };
    }; // struct HasHolderT
public:

    /// Type of the holder, if any.
    using HolderT = typename HolderTypeExtractor<HandleImplT>::type;
    /// Does the implementation use a holder?
    static constexpr bool HasHolderT{ HolderTypeExtractor<HandleImplT>::hasHolder };

    /// Default-constructed invalid index.
    static constexpr IdxT DefaultInvalidIndex{ };

    /// Create invalid handle.
    ResourceHandle() = default;

    /**
     * Create new handle with pointing to specified resource.
     * If mgr pointer is non-nullptr and index is non-default 
     * constructed then the reference count will be increased.
     * @param mgr Pointer to the manager holding the resource.
     * @param idx Index valid within the provided manager.
     */
    ResourceHandle(const MgrHolderT &mgr, const IdxT &idx);

    /**
     * If this handle contains non-default constructed ID 
     * and non-nullptr manager pointer, then reference 
     * count of the target resource will be decreased. 
     */
    ~ResourceHandle();

    /// Copy handle, increasing reference count if appropriate.
    ResourceHandle(const ResourceHandle &other);
    /// Copy handle, increasing/decreasing reference count if appropriate.
    ResourceHandle &operator=(const ResourceHandle &other);
    /// Move handle, swapping content of the two handles.
    ResourceHandle(ResourceHandle &&other) noexcept;
    /// Move handle, swapping content of the two handles.
    ResourceHandle &operator=(ResourceHandle &&other) noexcept;

    /**
     * Get the unique resource index.
     * If the resource is valid, then the reference 
     * count of target resource will be incremented 
     * by one.
     * User should either manually decrease reference 
     * count through the target manager or assign 
     * it to other resource handle.
     * @return Returns unique ID contained in this 
     * resource handle.
     */
    const IdxT &extractId() const;

    /**
     * Get the internal manager pointer.
     * Useful for creating other resource handles for 
     * the same manager.
     */
    MgrT *extractMgr() const;

    /**
     * Assign given index to this handle.
     * Decreases reference count of previously contained 
     * index if appropriate.
     * @param idx Index previously extracted from a 
     * valid handle.
     * @warning The state of this handle after assigning 
     * a new index may be invalid, checking with valid() 
     * function is highly recommended!
     */
    void assignId(const IdxT &idx);

    /**
     * Check if handle contains some data.
     * @returns Returns true if this handle contains a 
     * non-default index and non-nullptr manager pointer.
     * @warning Does NOT check if the index is valid 
     * within the manager!
     */
    explicit operator bool() const;

    /**
     * Test handle for validity and existence of target 
     * resource.
     * @returns Returns true if this handle is valid and 
     * index points to a valid resource.
     * @warning Also checks validity of unique index pointing 
     * to the target resource. This may result in locking 
     * the resource manager or, depending on manger, throwing 
     * exceptions.
     */
    bool valid() const;

    /**
     * Get the target resource.
     * @throws ResourceNotFoundException Thrown if the resource 
     * could not be found.
     * @throws InvalidHandleException Thrown when manager finds 
     * the index invalid.
     */
    ResT &get();
    ResT &get() const;

    /**
     * Get the target resource. This version always gets a resource 
     * which can be used. When a target resource is not found a 
     * dummy resource is returned in its stead.
     * @return Returns reference to the target resource.
     */
    ResT &getOrDefault();
    ResT &getOrDefault() const;

    /**
     * Target resource accessor.
     * @return Returns pointer to access the resource. For invalid 
     * handles, returns a pointer to a default value.
     */
    ResT *operator->();
    const ResT *operator->() const;

    /**
     * Get the target resource holder.
     * @throws ResourceNotFoundException Thrown if the resource 
     * could not be found.
     * @throws InvalidHandleException Thrown when manager finds 
     * the index invalid.
     */
    const HolderT& getHolder()
    { return HandleImplT::getHolder(mgrPointer(), mIndex); }
    const HolderT& getHolder() const
    { return HandleImplT::getHolder(mgrPointer(), mIndex); }
    // TODO - std::enable_if, conditional on getHolder.

    /**
     * Get hash for this resource handle.
     * @return Returns hash of this resource handle.
     */
    std::size_t hash() const noexcept;

    /// Swap handles.
    void swap(ResourceHandle &other) noexcept;

    /// Swap handles.
    template <typename MgrST, typename HandleImplST, typename ResST, typename IdxST>
    friend void swap(ResourceHandle<MgrST, HandleImplST, ResST, IdxST> &first,
        ResourceHandle<MgrST, HandleImplST, ResST, IdxST> &second) noexcept;

    /// Check if given index is non-default constructed.
    static bool idxValid(const IdxT &idx);

    /**
     * Compare 2 resource handles for equality.
     * @param other Compare to this handle.
     * @return Returns true if both handles "point" 
     * to the same resource.
     */
    bool operator==(const ResourceHandle &other);
private:
    // Creation of new handles should be in hands of the manager.
    friend MgrT;

    /// Check if given manager pointer is non-nullptr.
    static bool mgrValid(const MgrHolderT &mgr);

    /// Get pointer to the manager object.
    MgrT *mgrPointer() const;

    /// Destroy the handle, decreasing reference count if appropriate.
    void destroy();

    /// Create the handle, increasing reference count if appropriate.
    void create(const MgrHolderT &mgr, const IdxT &idx);

    /// Manager containing the target object.
    MgrHolderT mMgr{ };

    /// Unique index specifying the target object.
    IdxT mIndex{ };
protected:
}; // class ResourceHandle

} // namespace res

} // namespace quark

// Template implementation.

namespace std
{

// Adapter for standard hashing.
template <typename MgrTT, typename HandleImplTT, typename ResTT, typename IdxTT>
struct hash<quark::res::ResourceHandle<MgrTT, HandleImplTT, ResTT, IdxTT>>
{
    std::size_t operator()(const quark::res::ResourceHandle<MgrTT, HandleImplTT, ResTT, IdxTT> &h) const noexcept
    { return h.hash(); }
}; // struct hash

}

namespace quark
{

namespace res
{

template <typename MgrTT, typename HandleImplTT, typename ResTT, typename IdxTT>
ResourceHandle<MgrTT, HandleImplTT, ResTT, IdxTT>::ResourceHandle(const MgrHolderT &mgr, const IdxT &idx)
{ create(mgr, idx); }

template <typename MgrTT, typename HandleImplTT, typename ResTT, typename IdxTT>
ResourceHandle<MgrTT, HandleImplTT, ResTT, IdxTT>::~ResourceHandle()
{ destroy(); }

template <typename MgrTT, typename HandleImplTT, typename ResTT, typename IdxTT>
ResourceHandle<MgrTT, HandleImplTT, ResTT, IdxTT>::ResourceHandle(const ResourceHandle &other) :
    ResourceHandle(other.mMgr, other.mIndex)
{ }

template <typename MgrTT, typename HandleImplTT, typename ResTT, typename IdxTT>
ResourceHandle<MgrTT, HandleImplTT, ResTT, IdxTT> &ResourceHandle<MgrTT, HandleImplTT, ResTT, IdxTT>::operator=(
    const ResourceHandle &other)
{
    destroy();
    create(other.mMgr, other.mIndex);
    return *this;
}

template <typename MgrTT, typename HandleImplTT, typename ResTT, typename IdxTT>
ResourceHandle<MgrTT, HandleImplTT, ResTT, IdxTT>::ResourceHandle(ResourceHandle &&other) noexcept :
    ResourceHandle()
{ swap(other); }

template <typename MgrTT, typename HandleImplTT, typename ResTT, typename IdxTT>
ResourceHandle<MgrTT, HandleImplTT, ResTT, IdxTT> &ResourceHandle<MgrTT, HandleImplTT, ResTT, IdxTT>::operator=(
    ResourceHandle &&other) noexcept
{
    swap(other);
    return *this;
}

template <typename MgrTT, typename HandleImplTT, typename ResTT, typename IdxTT>
const typename ResourceHandle<MgrTT, HandleImplTT, ResTT, IdxTT>::IdxT &ResourceHandle<MgrTT, HandleImplTT, ResTT,
IdxTT>::extractId() const
{
    if (operator bool())
    { // Held Id is valid.
        HandleImplT::refIncr(mgrPointer(), mIndex);
    }
    return mIndex;
}

template <typename MgrTT, typename HandleImplTT, typename ResTT, typename IdxTT>
MgrTT *ResourceHandle<MgrTT, HandleImplTT, ResTT, IdxTT>::
extractMgr() const
{ return mgrPointer(); }

template <typename MgrTT, typename HandleImplTT, typename ResTT, typename IdxTT>
void ResourceHandle<MgrTT, HandleImplTT, ResTT, IdxTT>::assignId(const IdxT &idx)
{
    destroy();
    mIndex = idx;
}

template <typename MgrTT, typename HandleImplTT, typename ResTT, typename IdxTT>
ResourceHandle<MgrTT, HandleImplTT, ResTT, IdxTT>::operator bool() const
{ return mgrValid(mMgr) && idxValid(mIndex); }

template <typename MgrTT, typename HandleImplTT, typename ResTT, typename IdxTT>
bool ResourceHandle<MgrTT, HandleImplTT, ResTT, IdxTT>::valid() const
{ return (operator bool()) && HandleImplT::valid(mgrPointer(), mIndex); }

template <typename MgrTT, typename HandleImplTT, typename ResTT, typename IdxTT>
typename ResourceHandle<MgrTT, HandleImplTT, ResTT, IdxTT>::ResT &ResourceHandle<MgrTT, HandleImplTT, ResTT, IdxTT>::
get()
{ return HandleImplT::get(mgrPointer(), mIndex); }

template <typename MgrTT, typename HandleImplTT, typename ResTT, typename IdxTT>
typename ResourceHandle<MgrTT, HandleImplTT, ResTT, IdxTT>::ResT &ResourceHandle<MgrTT, HandleImplTT, ResTT, IdxTT>::
get() const
{ return HandleImplT::get(mgrPointer(), mIndex); }

template <typename MgrTT, typename HandleImplTT, typename ResTT, typename IdxTT>
typename ResourceHandle<MgrTT, HandleImplTT, ResTT, IdxTT>::ResT & ResourceHandle<MgrTT, HandleImplTT, ResTT, IdxTT>::
getOrDefault()
{ return HandleImplT::getOrDefault(mgrPointer(), mIndex); }

template <typename MgrTT, typename HandleImplTT, typename ResTT, typename IdxTT>
typename ResourceHandle<MgrTT, HandleImplTT, ResTT, IdxTT>::ResT &ResourceHandle<MgrTT, HandleImplTT, ResTT, IdxTT>::
getOrDefault() const
{ return HandleImplT::getOrDefault(mgrPointer(), mIndex); }

template <typename MgrTT, typename HandleImplTT, typename ResTT, typename IdxTT>
typename ResourceHandle<MgrTT, HandleImplTT, ResTT, IdxTT>::ResT *ResourceHandle<MgrTT, HandleImplTT, ResTT, IdxTT>::
operator->()
{ return &getOrDefault(); }

template <typename MgrTT, typename HandleImplTT, typename ResTT, typename IdxTT>
const typename ResourceHandle<MgrTT, HandleImplTT, ResTT, IdxTT>::ResT *ResourceHandle<MgrTT, HandleImplTT, ResTT,
IdxTT>::operator->() const
{ return &getOrDefault(); }

template <typename MgrTT, typename HandleImplTT, typename ResTT, typename IdxTT>
std::size_t ResourceHandle<MgrTT, HandleImplTT, ResTT, IdxTT>::hash() const noexcept
{ return util::hashAll(mgrPointer(), mIndex); }

template <typename MgrTT, typename HandleImplTT, typename ResTT, typename IdxTT>
void ResourceHandle<MgrTT, HandleImplTT, ResTT, IdxTT>::swap(ResourceHandle &other) noexcept
{
    using std::swap;
    swap(mMgr, other.mMgr);
    swap(mIndex, other.mIndex);
}

template <typename MgrTT, typename HandleImplTT, typename ResTT, typename IdxTT>
bool ResourceHandle<MgrTT, HandleImplTT, ResTT, IdxTT>::idxValid(const IdxT &idx)
{ return idx != DefaultInvalidIndex; }

template <typename MgrTT, typename HandleImplTT, typename ResTT, typename IdxTT>
bool ResourceHandle<MgrTT, HandleImplTT, ResTT, IdxTT>::operator==(const ResourceHandle &other)
{ return mMgr.lock() == other.mMgr.lock() && mIndex == other.mIndex; }
// TODO - Locking?

template <typename MgrTT, typename HandleImplTT, typename ResTT, typename IdxTT>
bool ResourceHandle<MgrTT, HandleImplTT, ResTT, IdxTT>::mgrValid(const MgrHolderT &mgr)
{ return !mgr.expired(); }

template <typename MgrTT, typename HandleImplTT, typename ResTT, typename IdxTT>
MgrTT *ResourceHandle<MgrTT, HandleImplTT, ResTT, IdxTT>::mgrPointer() const
{ return mMgr.lock().get(); }

template <typename MgrTT, typename HandleImplTT, typename ResTT, typename IdxTT>
void ResourceHandle<MgrTT, HandleImplTT, ResTT, IdxTT>::destroy()
{
    if (operator bool())
    { // Non-nullptr and index is not default-constructed.
        HandleImplT::refDecr(mgrPointer(), mIndex);
    }

    mMgr = MgrHolderT{ };
    mIndex = IdxT();
}

template <typename MgrTT, typename HandleImplTT, typename ResTT, typename IdxTT>
void ResourceHandle<MgrTT, HandleImplTT, ResTT, IdxTT>::create(const MgrHolderT &mgr, const IdxT &idx)
{
    mMgr = mgr;
    mIndex = idx;

    if (operator bool())
    { // Non-nullptr and index is not default-constructed.
        HandleImplT::refIncr(mgrPointer(), mIndex);
    }
}

template <typename MgrST, typename HandleImplST, typename ResST, typename IdxST>
void swap(ResourceHandle<MgrST, HandleImplST, ResST, IdxST> &first,
    ResourceHandle<MgrST, HandleImplST, ResST, IdxST> &second) noexcept
{ first.swap(second); }

}

}

// Template implementation end.