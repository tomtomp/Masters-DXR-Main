/**
 * @file resources/render/GpuMemoryMgr.h
 * @author Tomas Polasek
 * @brief Class used for managing of GPU memory and 
 * uploading of resources.
 */

#pragma once

#include "engine/resources/BaseResourceMgr.h"

#include "engine/resources/d3d12/D3D12Device.h"
#include "engine/resources/d3d12/D3D12CommandQueueMgr.h"
#include "engine/resources/d3d12/D3D12DescHeap.h"

#include "engine/helpers/d3d12/D3D12ResourceUpdater.h"
#include "engine/helpers/d3d12/D3D12PlacedAllocator.h"
#include "engine/helpers/d3d12/D3D12DummyAllocator.h"
#include "engine/helpers/d3d12/D3D12UploadBuffer.h"
#include "engine/helpers/d3d12/D3D12TextureBuffer.h"

/// Namespace containing the engine code.
namespace quark
{

/// Namespace containing resource management helpers.
namespace res
{

/// Rendering resources.
namespace rndr
{

/**
 * Class used for managing of GPU memory and uploading 
 * of resources.
 */
class GpuMemoryMgr : public BaseResourceMgr, public util::PointerType<GpuMemoryMgr>
{
public:
    /**
     * Initialize the memory manager for given device.
     * @param device Target device.
     * @param copyCmdQueueMgr Manager of copy command 
     * lists, used for copying of resources.
     */
    static PtrT create(res::d3d12::D3D12Device &device, 
        res::d3d12::D3D12CommandQueueMgr &copyCmdQueueMgr);

    /// Release any memory used by this manager.
    ~GpuMemoryMgr();

    // No copying.
    GpuMemoryMgr(const GpuMemoryMgr &other) = delete;
    GpuMemoryMgr &operator=(const GpuMemoryMgr &other) = delete;
    // Allow moving.
    GpuMemoryMgr(GpuMemoryMgr &&other) = default;
    GpuMemoryMgr &operator=(GpuMemoryMgr &&other) = default;

    /**
     * Create GPU memory allocator for committed memory.
     * @param heapProps Properties of the implicit heaps created 
     * by the allocator.
     * @param heapFlags Flags for the implicit heaps created by 
     * the allocator.
     * @return Returns allocator with given properties.
     */
    helpers::d3d12::D3D12CommittedAllocator::PtrT committedAllocator(
        const ::CD3DX12_HEAP_PROPERTIES &heapProps = ::CD3DX12_HEAP_PROPERTIES{ ::D3D12_HEAP_TYPE_DEFAULT }, 
        ::D3D12_HEAP_FLAGS heapFlags = ::D3D12_HEAP_FLAG_NONE);

    /**
     * Create GPU memory allocator having at least given number 
     * of bytes of memory available.
     * @param sizeInBytes Size of the allocated memory.
     * @param heapProps Properties of the main heap of the allocator.
     * @param heapFlags Heap flags for the heap used for placing 
     * resources.
     * @return Returns the allocator with at least given number 
     * of bytes of memory available.
     */
    helpers::d3d12::D3D12PlacedAllocator::PtrT placedAllocator(std::size_t sizeInBytes, 
        const ::CD3DX12_HEAP_PROPERTIES &heapProps, ::D3D12_HEAP_FLAGS heapFlags);

    /**
     * Create dummy allocator for this manager.
     * @return Returns dummy allocator which can be used to get 
     * the required number of bytes.
     */
    helpers::d3d12::D3D12DummyAllocator::PtrT dummyAllocator();

    /**
     * Get a command list which can be used for copying of 
     * resources to/from the GPU memory.
     */
    d3d12::D3D12CommandList::PtrT copyCommandList();

    /**
     * Create a descriptor heap on the device and 
     * return a handle to it.
     * @param size Number of descriptor which can be created 
     * on the returned descriptor heap.
     * @param type Type of descriptors which can be created 
     * on the returned descriptor heap.
     * @param flags Flags of the descriptor heap.
     */
    d3d12::D3D12DescHeap::PtrT descHeap(uint32_t size, ::D3D12_DESCRIPTOR_HEAP_TYPE type, 
        ::D3D12_DESCRIPTOR_HEAP_FLAGS flags = ::D3D12_DESCRIPTOR_HEAP_FLAG_NONE);

    /**
     * Upload data in given buffer to the target resource.
     * @param data Data to copy to the target.
     * @param sizeInBytes Size of the data in bytes.
     * @param target Target buffer to copy the data into.
     * @param copyCmdList Command list used to record the 
     * copy commands.
     */
    void uploadData(const void *data, std::size_t sizeInBytes, 
        d3d12::D3D12Resource &target, d3d12::D3D12CommandList &copyCmdList);

    /// Access the resource updater.
    helpers::d3d12::D3D12ResourceUpdater &updater();

    /**
     * Signalize to the resource updater that all of the 
     * copy command lists have been executed and the intermediate 
     * buffers may be released.
     */
    void uploadCmdListsExecuted();
private:
    /// Main rendering device.
    d3d12::D3D12Device *mDevice;
    /// Command queue used for copy commands.
    d3d12::D3D12CommandQueueMgr *mCopyCommandQueue;
    /// Updater used for uploading resources to the GPU.
    helpers::d3d12::D3D12ResourceUpdater::PtrT mResourceUpdater;
protected:
    /**
     * Initialize the memory manager for given device.
     * @param device Target device.
     * @param copyCmdQueueMgr Manager of copy command 
     * lists, used for copying of resources.
     */
    GpuMemoryMgr(res::d3d12::D3D12Device &device, 
        res::d3d12::D3D12CommandQueueMgr &copyCmdQueueMgr);
}; // class RenderSubSystem

} // namespace rndr

} // namespace res

} // namespace quark
