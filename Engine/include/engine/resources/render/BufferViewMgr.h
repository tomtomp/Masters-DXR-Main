/**
 * @file resources/render/BufferViewMgr.h
 * @author Tomas Polasek
 * @brief Manager of vertex/index views to the GPU-side buffers.
 */

#pragma once

#include "engine/resources/render/BufferMgr.h"

#include "engine/resources/render/BufferView.h"

/// Namespace containing the engine code.
namespace quark
{

/// Namespace containing resource management helpers.
namespace res
{

/// Rendering resources.
namespace rndr
{

/// Manager of vertex/index views to the GPU-side buffers.
class BufferViewMgr : public BaseHandleMgr<BufferView, uint16_t, std::string, uint16_t>, public util::PointerType<BufferViewMgr>
{
public:
    /// Renaming resource handle for readability.
    using BufferViewHandle = ThisMgrT::ResourceHandle;

    /**
     * Create instance of this manager.
     * @return Returns pointer to the new instance.
     */
    static PtrT createMgr()
    { return createInstance<BufferViewMgr>(); }

    /// Free all resources.
    virtual ~BufferViewMgr() = default;

    // No copying.
    BufferViewMgr(const BufferViewMgr &other) = delete;
    BufferViewMgr &operator=(const BufferViewMgr &other) = delete;
    // No moving.
    BufferViewMgr(BufferViewMgr &&other) = delete;
    BufferViewMgr &operator=(BufferViewMgr &&other) = delete;

    /**
     * Create or retrieve already existing buffer view.
     * Returned handle points to the correct resource, but 
     * it is not initialized yet. To initialize it, update() 
     * needs to be ran first.
     * @param id Identifier of the buffer view. Empty identifier 
     * is invalid and automatically generated one will be 
     * used instead.
     * @param targetBuffer Handle to the buffer viewed through 
     * the buffer view. Buffer does not need to be initialized 
     * at this moment. At the time of update(), it should already 
     * be initialized.
     * @param elementFormat Format of a single element in the 
     * buffer view
     * @param numElements Number of elements in the buffer view.
     * @param byteOffset Offset of this view from the start of 
     * the target buffer.
     * @param byteLength Length of the buffer view in bytes. May 
     * be set to 0 for automatic calculation from number of 
     * elements and their stride.
     * @param byteStride Stride of the elements in the buffer. Set 
     * to zero for elements which go right after one another.
     * @return Returns handle to the buffer view.
     */
    BufferViewHandle create(const IdentifierT &id, const BufferHandle &targetBuffer,
        ::DXGI_FORMAT elementFormat, std::size_t numElements, std::size_t byteOffset = 0u,
        std::size_t byteLength = 0u, std::size_t byteStride = 0u);

    /**
     * Create or retrieve already existing buffer view.
     * Returned handle points to the correct resource, but 
     * it is not initialized yet. To initialize it, update() 
     * needs to be ran first.
     * This version uses automatically generated 
     * identifier, created from provided parameters.
     * @param targetBuffer Handle to the buffer viewed through 
     * the buffer view. Buffer does not need to be initialized 
     * at this moment. At the time of update(), it should already 
     * be initialized.
     * @param elementFormat Format of a single element in the 
     * buffer view
     * @param numElements Number of elements in the buffer view.
     * @param byteOffset Offset of this view from the start of 
     * the target buffer.
     * @param byteLength Length of the buffer view in bytes. May 
     * be set to 0 for automatic calculation from number of 
     * elements and their stride.
     * @param byteStride Stride of the elements in the buffer. Set 
     * to zero for elements which go right after one another.
     * @return Returns handle to the buffer view.
     */
    BufferViewHandle create(const BufferHandle &targetBuffer, ::DXGI_FORMAT elementFormat, 
        std::size_t numElements, std::size_t byteOffset = 0u, std::size_t byteLength = 0u, 
        std::size_t byteStride = 0u);

    /**
     * Perform book-keeping operation such as: 
     *   Free any buffer views with 0 in the reference counter.
     */
    virtual void update() override final;
private:
    /// Base name of generated buffer view identifiers.
    static constexpr const char *UNIQUE_ID_BASE{ "BuffV" };

    /**
     * Create or retrieve already existing buffer view.
     * Returned handle points to the correct resource, but 
     * it is not initialized yet. To initialize it, update() 
     * needs to be ran first.
     * @return Returns handle to the buffer view.
     */
    BufferViewHandle createInner(const IdentifierT &id, const BufferHandle &targetBuffer,
        ::DXGI_FORMAT elementFormat, std::size_t numElements, std::size_t byteOffset = 0u,
        std::size_t byteLength = 0u, std::size_t byteStride = 0u);

    /**
     * Add the request to initialize buffer view resource.
     * @param res Target resource.
     * @param targetBuffer Handle to the buffer viewed through 
     * the buffer view.
     * @param elementFormat Format of a single element in the 
     * buffer view
     * @param numElements Number of elements in the buffer view.
     * @param byteOffset Offset of this view from the start of 
     * the target buffer.
     * @param byteLength Length of the buffer view in bytes. May 
     * be set to 0 for automatic calculation from number of 
     * elements and their stride.
     * @param byteStride Stride of the elements in the buffer. Set 
     * to zero for elements which go right after one another.
     */
    void appendRequestTicket(ResourceHolder *res, const BufferHandle &targetBuffer,
        ::DXGI_FORMAT elementFormat, std::size_t numElements, std::size_t byteOffset = 0u,
        std::size_t byteLength = 0u, std::size_t byteStride = 0u);

    /**
     * Finalize creation of requested buffer views.
     */
    void processRequests();

    /**
     * Initialize buffer resource as described in provided user 
     * request.
     * @param res Buffer view which should be initialized.
     */
    void initializeBufferView(ResourceHolder *res);

    /**
     * Generate unique identifier for provided parameters.
     * @return Returns identifier unique to given parameters.
     */
    IdentifierT generateUniqueId(const BufferHandle &targetBuffer, ::DXGI_FORMAT elementFormat, 
        std::size_t numElements, std::size_t byteOffset = 0u, std::size_t byteLength = 0u, 
        std::size_t byteStride = 0u) const;

    /// List of requests for buffer view initialization since last update.
    std::vector<ResourceHolder*> mBufferViewRequests{ };
protected:
    /**
     * Initialize manager structure, creates an empty manager.
     */
    BufferViewMgr();
}; // class BufferViewMgr

/// Handle to a GPU-side buffer view.
using BufferViewHandle = ::quark::res::rndr::BufferViewMgr::BufferViewHandle;

} // namespace rndr

} // namespace res

} // namespace quark
