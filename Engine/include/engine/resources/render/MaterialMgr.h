/**
 * @file resources/render/MaterialMgr.h
 * @author Tomas Polasek
 * @brief Manager of materials, each material has its properties and textures.
 */

#pragma once

#include "engine/resources/BaseHandleMgr.h"

#include "engine/resources/render/Material.h"

/// Namespace containing the engine code.
namespace quark
{

/// Namespace containing resource management helpers.
namespace res
{

/// Rendering resources.
namespace rndr
{

/// Manager of materials, each material has its properties and textures.
class MaterialMgr : public BaseHandleMgr<Material, uint16_t, std::string, uint8_t>, public util::PointerType<MaterialMgr>
{
public:
    /// Renaming resource handle for readability.
    using MaterialHandle = ThisMgrT::ResourceHandle;

    /**
     * Create instance of this manager.
     * @return Returns pointer to the new instance.
     */
    static PtrT createMgr()
    { return createInstance<MaterialMgr>(); }

    /// Free all resources.
    virtual ~MaterialMgr();

    // No copying.
    MaterialMgr(const MaterialMgr &other) = delete;
    MaterialMgr &operator=(const MaterialMgr &other) = delete;
    // No moving.
    MaterialMgr(MaterialMgr &&other) = delete;
    MaterialMgr &operator=(MaterialMgr &&other) = delete;

    /**
     * Create or retrieve already existing material.
     * Returns material handle points to a default 
     * initialized material.
     * @param id Identifier of the material. Empty identifier 
     * is invalid and automatically generated one will be 
     * used instead.
     * @return Returns handle to the material.
     */
    MaterialHandle create(const IdentifierT &id);

    /**
     * Perform book-keeping operation such as: 
     *   Free any materials with 0 in the reference counter.
     */
    virtual void update() override final;
private:
    /// Base name of generated material identifiers.
    static constexpr const char *UNIQUE_ID_BASE{ "Mat" };

    /**
     * Create or retrieve already existing material.
     * Returns material handle points to a default 
     * initialized material.
     * @return Returns handle to the material.
     */
    MaterialHandle createInner(const IdentifierT &id);

    /**
     * Generate unique identifier for a material.
     * @return Returns unique material identifier.
     */
    IdentifierT generateUniqueId();

    /// Counter used in generation of unique IDs.
    std::size_t mUniqueIdCounter{ 0u };
protected:
    /**
     * Initialize manager structure, creates an empty manager.
     */
    MaterialMgr();
}; // class MaterialMgr

/// Handle to a material.
using MaterialHandle = ::quark::res::rndr::MaterialMgr::MaterialHandle;

} // namespace rndr

} // namespace res

} // namespace quark
