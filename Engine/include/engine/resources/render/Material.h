/**
 * @file resources/render/Material.h
 * @author Tomas Polasek
 * @brief Wrapper for data describing objects material properties.
 */

#pragma once

#include "engine/resources/BaseResource.h"

#include "engine/resources/render/TextureMgr.h"
#include "engine/scene/SceneDescription.h"

/// Namespace containing the engine code.
namespace quark
{

/// Namespace containing resource management helpers.
namespace res
{

/// Rendering resources.
namespace rndr
{

/**
 * Wrapper for data describing objects material properties.
 * Based on glTF v2 specification of material: 
 *   https://github.com/KhronosGroup/glTF/tree/master/specification/2.0#reference-material
 */
struct Material : BaseResource
{
    /// Release the handles.
    virtual ~Material() = default;

    /// Section for properties which are used in PBR.
    struct PhysicallyBasedProperties
    {
        using PbrModel = scene::desc::PbrModel;

        /// Is this structure describing Metallic-roughness material?
        bool isMetallicRoughness() const
        { return modelUsed == PbrModel::MetallicRoughness; }

        /// Is this structure describing Specular-Glossiness material?
        bool isSpecularGlossiness() const
        { return modelUsed == PbrModel::SpecularGlossiness; }

        /// Specification of which PBR model is used.
        PbrModel modelUsed{ PbrModel::None };

        /**
         * Multiplicative factor of the baseColorTexture.
         * When no texture is specified, this is instead the 
         * RGBA color of the material.
         * Used both in Metallic-Roughness and 
         * Specular-Glossiness (diffuseFactor).
         * This property is valid even when PBR model is None.
         */
        float baseColorFactor[4]{ 1.0f, 1.0f, 1.0f, 1.0f };
        /**
         * Texture containing the color of the material.
         * Each value should be multiplied by the baseColorFactor 
         * before further use.
         * While using Specular-Glossiness, this texture contains 
         * diffuse map.
         * This property is valid even when PBR model is None.
         */
        TextureHandle baseColorTexture{ };

        union
        {
            /**
             * Flat metalness of the material.
             * Only used for Metallic-Roughness model.
             */
            float metallicFactor;
            /**
             * Specular RGB colors of the material
             * Only used for Specular-Glossiness model.
             */
            float specularFactor[3]{ 1.0f, 1.0f, 1.0f };
        } firstFactor;

        union
        {
            /**
             * Flat roughness of the material
             * Only used for Metallic-Roughness model.
             */
            float roughnessFactor;
            /**
             * Flag glossiness/smoothness of the material.
             * Only used for Specular-Glossiness model.
             */
            float glossinessFactor{ 1.0f };
        } secondFactor;

        /**
         * Map containing values of physical properties of 
         * the material. When set, the first and second 
         * factors are unused!
         * For Metallic-Roughness, this texture contains 
         * metalness (B-channel) and roughness (G-channel) 
         * values for each texel.
         * For Specular-Glossiness, this texture contains 
         * specular RGB color (RGB-channels) and glossiness 
         * (A-channel) values for each texel.
         */
        TextureHandle propertyTexture;
    }; // struct PhysicallyBasedProperties

    /// Container for physically-based shading/rendering data.
    PhysicallyBasedProperties pbr{ };

    /**
     * Texture, where each texel (RGB-channels) contains XYZ 
     * components of the normal vector.
     */
    TextureHandle normalTexture;

    /**
     * Texture, which represents how much is the point occluded. 
     * The data is contained within the R-channel and the higher 
     * the value is, the LESS occluded the point is.
     */
    TextureHandle occlusionTexture;

    /**
     * Color emission of the material.
     * If emissiveTexture is set, every texel should be multiplied 
     * by this factor, before any further operations.
     */
    float emissiveFactor[3]{ 0.0f, 0.0f, 0.0f };

    /**
     * Texture containing data for emissive surfaces. Color of 
     * emission is contained within RGB-channels.
     */
    TextureHandle emissiveTexture;

    using AlphaMode = scene::desc::AlphaMode;

    /// How to draw materials alpha values.
    AlphaMode alphaMode{ AlphaMode::Opaque };

    /**
     * When alphaMode is in Mask mode, this value dictates 
     * the boundary between visible/invisible.
     */
    float alphaCutoff{ 0.5f };

    /// Does the material generate specular reflections?
    bool specularReflections{ false };

    /// Does the material generate "specular" refraction?
    bool specularRefractions{ false };
}; // struct Material

} // namespace rndr

} // namespace res

} // namespace quark
