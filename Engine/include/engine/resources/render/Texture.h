/**
 * @file resources/render/Texture.h
 * @author Tomas Polasek
 * @brief Wrapper around data required to use an image in rendering.
 */

#pragma once

#include "engine/resources/BaseResource.h"

#include "engine/resources/render/StaticSamplerMgr.h"

#include "engine/helpers/d3d12/D3D12TextureBuffer.h"

/// Namespace containing the engine code.
namespace quark
{

/// Namespace containing resource management helpers.
namespace res
{

/// Rendering resources.
namespace rndr
{

/// Wrapper around a GPU-side texture.
struct Texture : BaseResource
{
    /// Texture resource itself.
    helpers::d3d12::D3D12TextureBuffer texture;

    /// Optional sampler for the texture.
    StaticSamplerHandle sampler;

    /// Check if this texture is valid.
    bool valid() const noexcept 
    { return texture.valid(); }

    /// Check if this texture is valid
    explicit operator bool() const noexcept 
    { return valid(); }
}; // struct Texture

} // namespace rndr

} // namespace res

} // namespace quark
