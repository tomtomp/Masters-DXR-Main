/**
 * @file resources/render/BufferView.h
 * @author Tomas Polasek
 * @brief Wrapper around data required for viewing a buffer.
 */

#pragma once

#include "engine/resources/BaseResource.h"

#include "engine/resources/render/BufferMgr.h"

/// Namespace containing the engine code.
namespace quark
{

/// Namespace containing resource management helpers.
namespace res
{

/// Rendering resources.
namespace rndr
{

/// Container for data describing a view of a GPU-side buffer.
struct BufferView : BaseResource
{
    /// Release the handle.
    virtual ~BufferView() = default;

    /// Target of this view, keeping the handle for reference counting purposes.
    BufferHandle targetBuffer{ };
    /// Address to the beginning of the target buffer.
    ::D3D12_GPU_VIRTUAL_ADDRESS targetBufferAddress{ 0u };
    /// Offset in bytes, where this view starts in the target buffer.
    std::size_t byteOffset{ 0u };
    /// Size of the buffer view in bytes.
    std::size_t byteLength{ 0u };
    /// Stride in bytes, between two consecutive elements.
    std::size_t byteStride{ 0u };
    /// Format of an element in the buffer view.
    ::DXGI_FORMAT elementFormat{ ::DXGI_FORMAT_UNKNOWN };
    /// Number of elements in the buffer view.
    std::size_t numElements{ 0u };

    /**
     * Generate a vertex buffer view from this view 
     * description.
     */
    ::D3D12_VERTEX_BUFFER_VIEW vbv() const
    { return ::D3D12_VERTEX_BUFFER_VIEW{ address(), static_cast<::UINT>(byteLength), static_cast<::UINT>(byteStride) }; }

    /**
     * Generate a index buffer view from this view 
     * description.
     */
    ::D3D12_INDEX_BUFFER_VIEW ibv() const
    { return ::D3D12_INDEX_BUFFER_VIEW{ address(), static_cast<::UINT>(byteLength), elementFormat }; }

    /// Get address of the buffer in GPU memory.
    ::D3D12_GPU_VIRTUAL_ADDRESS address() const
    { return ::D3D12_GPU_VIRTUAL_ADDRESS{ targetBufferAddress + byteOffset }; }

    /// Get address of the buffer in GPU memory, including the stride.
    ::D3D12_GPU_VIRTUAL_ADDRESS_AND_STRIDE addressAndStride() const
    { return ::D3D12_GPU_VIRTUAL_ADDRESS_AND_STRIDE{ address(), byteStride }; }
}; // struct BufferView

} // namespace rndr

} // namespace res

} // namespace quark
