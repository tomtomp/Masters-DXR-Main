/**
 * @file resources/render/DescriptorMgr.h
 * @author Tomas Polasek
 * @brief Manager of shader resource views - SRVs, UAVs and CBVs.
 */

#pragma once

#include "engine/resources/BaseHandleMgr.h"

#include "engine/resources/render/Descriptor.h"

/// Namespace containing the engine code.
namespace quark
{

/// Namespace containing resource management helpers.
namespace res
{

/// Rendering resources.
namespace rndr
{

/// Manager of shader resource views - SRVs, UAVs and CBVs.
class DescriptorMgr : public BaseHandleMgr<Descriptor, uint16_t, std::string, uint8_t>, public util::PointerType<DescriptorMgr>
{
public:
    /// Renaming resource handle for readability.
    using DescriptorHandle = ThisMgrT::ResourceHandle;

    /// Structure describing request to crate an SRV.
    template <typename DescT>
    struct DescriptorRequest
    {
        /// Target resource to initialize.
        ResourceHolder *target{ nullptr };
        /// Descriptor description.
        DescT desc{ };
    }; // struct DescriptorRequest

    /**
     * Create instance of this manager.
     * @return Returns pointer to the new instance.
     * @warning Before any more actions are used, this manager 
     * must be initialized with the target device!
     */
    static PtrT createMgr()
    { return createInstance<DescriptorMgr>(); }

    /**
     * Initialize manager structure, creates an empty manager.
     * @param gpuMemoryMgr Manager of target GPU memory, used 
     * for storing scene resources.
     */
    void initialize(rndr::GpuMemoryMgr &gpuMemoryMgr);

    /// Free all resources.
    virtual ~DescriptorMgr() = default;

    // No copying.
    DescriptorMgr(const DescriptorMgr &other) = delete;
    DescriptorMgr &operator=(const DescriptorMgr &other) = delete;
    // Now moving.
    DescriptorMgr(DescriptorMgr &&other) = delete;
    DescriptorMgr &operator=(DescriptorMgr &&other) = delete;

    /**
     * Create or retrieve already existing shader resource 
     * view.
     * Returned handle points to the shader resource view, but 
     * it may not be created yet. To finalize the creation 
     * call the update() method.
     * @param id Identifier of the SRV. Empty identifier 
     * is invalid and automatically generated one will be 
     * used instead.
     * @param targetHandle Target of the resource view.
     * @param desc Description of the requested SRV.
     * @return Returns handle to the SRV.
     */
    template <typename HandleT>
    DescriptorHandle createSrv(const IdentifierT &id, 
        const HandleT &targetHandle, 
        const ::D3D12_SHADER_RESOURCE_VIEW_DESC &desc);

    /**
     * Create or retrieve already existing shader resource 
     * view.
     * Returned handle points to the shader resource view, but 
     * it may not be created yet. To finalize the creation 
     * call the update() method.
     * This version uses automatically generated 
     * @param targetHandle Target of the resource view.
     * @param desc Description of the requested SRV.
     * @return Returns handle to the SRV.
     */
    template <typename HandleT>
    DescriptorHandle createSrv(const HandleT &targetHandle, 
        const ::D3D12_SHADER_RESOURCE_VIEW_DESC &desc);

    /**
     * Retrieve already existing shader resource view.
     * @return Returns handle to the shader resource 
     * view. If there is no shader resource view with 
     * given ID, then invalid handle is returned instead.
     */
    DescriptorHandle get(const IdentifierT &id);

    /// Get descriptor heap currently in use by this manager.
    const res::d3d12::D3D12DescHeap &currentDescHeap() const
    { return *mCurrentDescHeap; }

    /**
     * Perform book-keeping operation such as: 
     *   Finalize creation of buffers.
     *   Free any SRV's with 0 in the reference counter.
     * @throws util::winexception Thrown on any D3D12 error.
     */
    virtual void update() override final;
private:
    /// Base name of generated SRV identifiers.
    static constexpr const char *UNIQUE_ID_BASE_SRV{ "SRV" };
    /// Base name of generated UAV identifiers.
    static constexpr const char *UNIQUE_ID_BASE_UAV{ "UAV" };
    /// Base name of generated CBV identifiers.
    static constexpr const char *UNIQUE_ID_BASE_CBV{ "CBV" };

    /// Request for a SRV descriptor.
    struct SrvDescriptorRequest : public DescriptorRequest<::D3D12_SHADER_RESOURCE_VIEW_DESC>
    {
        /// Create the SRV descriptor.
        static ::CD3DX12_CPU_DESCRIPTOR_HANDLE createDescriptor(
            d3d12::D3D12Resource &resource, const ::D3D12_SHADER_RESOURCE_VIEW_DESC &desc, 
            d3d12::D3D12DescHeap &descHeap, const ::CD3DX12_CPU_DESCRIPTOR_HANDLE &target);
    }; // struct SrvDescriptorRequest
    
    /// Request for an UAV descriptor.
    struct UavDescriptorRequest : public DescriptorRequest<::D3D12_UNORDERED_ACCESS_VIEW_DESC>
    {
        /// Create the UAV descriptor.
        static ::CD3DX12_CPU_DESCRIPTOR_HANDLE createDescriptor(
            d3d12::D3D12Resource &resource, const ::D3D12_UNORDERED_ACCESS_VIEW_DESC &desc, 
            d3d12::D3D12DescHeap &descHeap, const ::CD3DX12_CPU_DESCRIPTOR_HANDLE &target);
    }; // struct SrvDescriptorRequest

    /// Request for a CBV descriptor.
    struct CbvDescriptorRequest : public DescriptorRequest<::D3D12_CONSTANT_BUFFER_VIEW_DESC>
    {
        /// Create the CBV descriptor.
        static ::CD3DX12_CPU_DESCRIPTOR_HANDLE createDescriptor(
            d3d12::D3D12Resource &resource, const ::D3D12_CONSTANT_BUFFER_VIEW_DESC &desc, 
            d3d12::D3D12DescHeap &descHeap, const ::CD3DX12_CPU_DESCRIPTOR_HANDLE &target);
    }; // struct SrvDescriptorRequest

    /**
     * Create or retrieve already existing descriptor.
     * Returned handle points to the descriptor, but it 
     * may not be created yet. To finalize the creation 
     * call the update() method.
     * @return Returns handle to the descriptor.
     */
    template <Descriptor::ResourceType Type, typename HandleT, typename DescT, typename DescReqT>
    DescriptorHandle createInner(const IdentifierT &id, const HandleT &targetHandle, 
        const DescT &desc, std::vector<DescReqT> &requests);

    /**
     * Add the request to initialize and fill descriptor 
     * with provided data.
     * @param res Target resource.
     * @param targetHandle Target of the descriptor.
     * @param desc Description of the descriptor.
     * @param requests Vector of requests to append this 
     * request to.
     */
    template <Descriptor::ResourceType Type, typename HandleT, typename DescT, typename DescReqT>
    void appendRequestTicket(ResourceHolder *res, const HandleT &targetHandle, 
        const DescT &desc, std::vector<DescReqT> &requests);

    /**
     * Create descriptor heap with enough space to fir all of 
     * the current descriptors and a given number of new ones.
     * @param newDescriptors Number of new descriptors.
     * @return Returns the new descriptor heap.
     */
    d3d12::D3D12DescHeap::PtrT createDescHeap(std::size_t newDescriptors);

    /**
     * Process requests for new descriptors.
     * @param oldTable Old segment specification.
     * @param oldDescHeap The old descriptor heap, source of the descriptors.
     * @param newDescHeap New descriptor heap, destination of the descriptors.
     * @param requests List containing the requests.
     * @return Returns descriptor table of the new segment.
     */
    template <typename DescReqT>
    d3d12::D3D12DescHeap::DescriptorTable processAnyRequests(
        const d3d12::D3D12DescHeap::DescriptorTable &oldTable, 
        d3d12::D3D12DescHeap &oldDescHeap, d3d12::D3D12DescHeap &newDescHeap, 
        const std::vector<DescReqT> &requests);

    /// Fix already existing descriptors, when changing to new descriptor heap.
    void fixExistingHandles(
        d3d12::D3D12DescHeap &newDescHeap, 
        const d3d12::D3D12DescHeap::DescriptorTable &oldSrvTable, 
        const d3d12::D3D12DescHeap::DescriptorTable &newSrvTable, 
        const d3d12::D3D12DescHeap::DescriptorTable &oldUavTable, 
        const d3d12::D3D12DescHeap::DescriptorTable &newUavTable, 
        const d3d12::D3D12DescHeap::DescriptorTable &oldCbvTable, 
        const d3d12::D3D12DescHeap::DescriptorTable &newCbvTable);

    /**
     * Finalize creation of requested SRVs, UAVs and CBVs.
     * @warning If there are any to process, the old descriptor 
     * heap will be removed and replaced with a new one!
     */
    void processRequests();

    /**
     * Initialize provided descriptor request.
     * @param request Which descriptor should be initialized.
     * @param descHeap Descriptor heap used to create the 
     * descriptors.
     * @param target Target handle used for the descriptor.
     */
    template <typename DescReqT>
    void initializeDescriptor(const DescReqT &request, 
        d3d12::D3D12DescHeap &descHeap, 
        const ::CD3DX12_CPU_DESCRIPTOR_HANDLE &target);

    /**
     * Generate unique descriptor identifier for provided parameters.
     * @return Returns descriptor identifier unique to given parameters.
     */
    template <typename HandleT, typename DescT>
    IdentifierT generateUniqueId(const HandleT &targetHandle, 
        const DescT &desc, const char *baseStr) const;

    /// List of requests for SRV initialization since last update.
    std::vector<SrvDescriptorRequest> mSrvRequests{ };
    /// List of requests for UAV initialization since last update.
    std::vector<UavDescriptorRequest> mUavRequests{ };
    /// List of requests for CBV initialization since last update.
    std::vector<CbvDescriptorRequest> mCbvRequests{ };

    /// Management of buffer memory on the GPU.
    rndr::GpuMemoryMgr *mGpuMemoryMgr{ nullptr };

    /// Currently used descriptor heap.
    res::d3d12::D3D12DescHeap::PtrT mCurrentDescHeap{ };

    /// Table containing the SRV segment of the descriptor heap.
    d3d12::D3D12DescHeap::DescriptorTable mSrvTable{ };
    /// Table containing the UAV segment of the descriptor heap.
    d3d12::D3D12DescHeap::DescriptorTable mUavTable{ };
    /// Table containing the CBV segment of the descriptor heap.
    d3d12::D3D12DescHeap::DescriptorTable mCbvTable{ };
protected:
    /**
     * Initialize manager structure, creates an empty manager.
     * @warning Before any more actions are used, this manager 
     * must be initialized with the target device!
     */
    DescriptorMgr();

    /**
     * Initialize manager structure, creates an empty manager.
     * @param gpuMemoryMgr Manager of target GPU memory, used 
     * for storing scene resources.
     */
    DescriptorMgr(rndr::GpuMemoryMgr &gpuMemoryMgr);
}; // class DescriptorMgr

/// Handle to a shader resource view.
using DescriptorHandle = ::quark::res::rndr::DescriptorMgr::DescriptorHandle;

} // namespace rndr

} // namespace res

} // namespace quark

// Template implementation.

namespace quark
{

namespace res
{

namespace rndr
{

template <typename HandleT>
DescriptorHandle DescriptorMgr::createSrv(const IdentifierT &id, 
    const HandleT &targetHandle, const ::D3D12_SHADER_RESOURCE_VIEW_DESC &desc)
{
    if (id.empty())
    { return createSrv(targetHandle, desc); }
    else
    { return createInner<Descriptor::ResourceType::ShaderResourceView>(id, targetHandle, desc, mSrvRequests); }
}

template <typename HandleT>
DescriptorHandle DescriptorMgr::createSrv(const HandleT &targetHandle,
    const ::D3D12_SHADER_RESOURCE_VIEW_DESC &desc)
{ return createInner<Descriptor::ResourceType::ShaderResourceView>(generateUniqueId(targetHandle, desc, UNIQUE_ID_BASE_SRV), targetHandle, desc, mSrvRequests); }

template <Descriptor::ResourceType Type, typename HandleT, typename DescT, typename DescReqT>
DescriptorHandle DescriptorMgr::createInner(
    const IdentifierT &id, const HandleT &targetHandle, 
    const DescT &desc, std::vector<DescReqT> &requests)
{
    // Check for previous Descriptor creation.
    auto srvIdx{ findResourceIdx(id) };
    if (!DescriptorHandle::idxValid(srvIdx))
    { // Descriptor with such ID has not been created yet.
        log<Info>() << "Creating a new " << Descriptor::resourceTypeToStr(Type) << " descriptor: \"" << id << "\"" << std::endl;
        // Create it.
        srvIdx = createResource(id);
        // Remember provided parameters.
        appendRequestTicket<Type>(getResource(srvIdx), targetHandle, desc, requests);
    }

    return { weak_from_this(), srvIdx };
}

template <Descriptor::ResourceType Type, typename HandleT, typename DescT, typename DescReqT>
void DescriptorMgr::appendRequestTicket(
    ResourceHolder *res, const HandleT &targetHandle, 
    const DescT &desc, std::vector<DescReqT> &requests)
{
    auto &resource{ res->res() };
    resource.resetFor(targetHandle, Type);

    requests.emplace_back(DescReqT{ res, desc });
}

template <typename DescReqT>
d3d12::D3D12DescHeap::DescriptorTable DescriptorMgr::processAnyRequests(
    const d3d12::D3D12DescHeap::DescriptorTable &oldTable, d3d12::D3D12DescHeap &oldDescHeap,
    d3d12::D3D12DescHeap &newDescHeap, const std::vector<DescReqT> &requests)
{
    // Allocate new segment.
    const auto newTable { newDescHeap.allocateTable(static_cast<uint32_t>(oldTable.size() + requests.size())) };
    // Copy over the old descriptors.
    if (oldDescHeap.initialized())
    { d3d12::D3D12DescHeap::copyTable(oldDescHeap, oldTable, newDescHeap, newTable); }
    // Process the requests: 
    log<Info>() << "Processing " << requests.size() << " new requests!" << std::endl;
    auto descriptorOffset{ oldTable.size() };
    for (const auto &request : requests)
    { // Fulfill the requests.
        if (request.target->refs() != 0u)
        { // Only take requests which are still referenced.
            initializeDescriptor(
                request, 
                newDescHeap, 
                newDescHeap.handleFromTable(newTable, descriptorOffset++));
        }
    }

    return newTable;
}

template <typename DescReqT>
void DescriptorMgr::initializeDescriptor(const DescReqT &request, 
    d3d12::D3D12DescHeap &descHeap, const ::CD3DX12_CPU_DESCRIPTOR_HANDLE &target)
{
    // Obtain the target resource.
    ResourceHolder *resourceHolder{ request.target };
    ASSERT_FAST(!resourceHolder->initialized());

    auto &descriptor{ resourceHolder->res() };

    log<Info>() << "Initializing descriptor: \"" << resourceHolder->id() << "\"." << std::endl;

    // Create the descriptor.
    descriptor.cpuHandle = DescReqT::createDescriptor(descriptor.res(), request.desc, descHeap, target);

    // Calculate offset within the heap.
    descriptor.offsetInHeap = static_cast<uint32_t>(
        descHeap.offsetFromStart(descriptor.cpuHandle));

    resourceHolder->markInitialized();
}

template <typename HandleT, typename DescT>
BaseHandleMgr<Descriptor>::IdentifierT DescriptorMgr::generateUniqueId(
    const HandleT &targetHandle, const DescT &desc, const char *baseStr) const
{ return IdentifierT(baseStr) + std::to_string(util::hashAll(targetHandle, desc)); }

}

}

}

// Template implementation end.
