/**
 * @file resources/render/StaticSamplerMgr.h
 * @author Tomas Polasek
 * @brief Manager of static texture samplers.
 */

#pragma once

#include "engine/resources/BaseHandleMgr.h"

#include "engine/resources/d3d12/D3D12Resource.h"

/// Namespace containing the engine code.
namespace quark
{

/// Namespace containing resource management helpers.
namespace res
{

/// Rendering resources.
namespace rndr
{

///  Manager of static texture samplers.
class StaticSamplerMgr : public BaseHandleMgr<::CD3DX12_STATIC_SAMPLER_DESC, uint16_t, std::string, uint8_t>, public util::PointerType<StaticSamplerMgr>
{
public:
    /// Renaming resource handle for readability.
    using StaticSamplerHandle = ThisMgrT::ResourceHandle;

    /**
     * Create instance of this manager.
     * @return Returns pointer to the new instance.
     */
    static PtrT createMgr()
    { return createInstance<StaticSamplerMgr>(); }

    /// Free all resources.
    virtual ~StaticSamplerMgr();

    // No copying.
    StaticSamplerMgr(const StaticSamplerMgr &other) = delete;
    StaticSamplerMgr &operator=(const StaticSamplerMgr &other) = delete;
    // No moving.
    StaticSamplerMgr(StaticSamplerMgr &&other) = delete;
    StaticSamplerMgr &operator=(StaticSamplerMgr &&other) = delete;

    /**
     * Create or retrieve already existing static sampler.
     * @param id Identifier of the static sampler. Empty 
     * identifier is invalid and automatically generated 
     * one will be used instead.
     * @param desc Description of the requested static 
     * sampler.
     * @return Returns handle to the static sampler.
     */
    StaticSamplerHandle create(const IdentifierT &id, 
        const ::CD3DX12_STATIC_SAMPLER_DESC &desc);

    /**
     * Create or retrieve already existing static sampler.
     * This version uses automatically generated 
     * identifier, created from provided parameters.
     * @param desc Description of the requested static sampler.
     * @return Returns handle to the static sampler.
     */
    StaticSamplerHandle create(const ::CD3DX12_STATIC_SAMPLER_DESC &desc);

    /**
     * Perform book-keeping operation such as: 
     *   Finalize creation of buffers.
     */
    virtual void update() override final;
private:
    /// Base name of generated sampler identifiers.
    static constexpr const char *UNIQUE_ID_BASE{ "Buff" };

    /**
     * Create or retrieve already existing static sampler.
     * @return Returns handle to the static sampler.
     */
    StaticSamplerHandle createInner(const IdentifierT &id, 
        const ::CD3DX12_STATIC_SAMPLER_DESC &desc);

    /**
     * Generate unique identifier for provided parameters.
     * @return Returns identifier unique to given parameters.
     */
    IdentifierT generateUniqueId(const ::CD3DX12_STATIC_SAMPLER_DESC &desc) const;
protected:
    /**
     * Initialize manager structure, creates an empty manager.
     */
    StaticSamplerMgr();
}; // class StaticSamplerMgr

/// Handle to a static texture sampler.
using StaticSamplerHandle = ::quark::res::rndr::StaticSamplerMgr::StaticSamplerHandle;

} // namespace rndr

} // namespace res

} // namespace quark
