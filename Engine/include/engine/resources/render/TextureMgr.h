/**
 * @file resources/render/TextureMgr.h
 * @author Tomas Polasek
 * @brief Manager of GPU-side textures.
 */

#pragma once

#include "engine/resources/BaseHandleMgr.h"

#include "engine/resources/d3d12/D3D12CommandQueueMgr.h"

#include "engine/resources/render/GpuMemoryMgr.h"
#include "engine/resources/render/Texture.h"

/// Namespace containing the engine code.
namespace quark
{

/// Namespace containing resource management helpers.
namespace res
{

/// Rendering resources.
namespace rndr
{

/// Manager of GPU-side textures.
class TextureMgr : public BaseHandleMgr<Texture, uint16_t, std::string, uint16_t>, public util::PointerType<TextureMgr>
{
public:
    /// Structure describing request to create and fill a GPU-side texture.
    struct TextureRequest
    {
        /// Target resource to initialize.
        ResourceHolder *target{ nullptr };
        /// Data to load into the texture.
        const void *data{ nullptr }; 
        /// Size of data in bytes.
        std::size_t sizeInBytes{ 0u };
        /// Description of the texture.
        ::CD3DX12_RESOURCE_DESC desc{ };
    }; // struct TextureRequest

    /// Renaming resource handle for readability.
    using TextureHandle = ThisMgrT::ResourceHandle;

    /**
     * Create instance of this manager.
     * @return Returns pointer to the new instance.
     * @warning Before any more actions are used, this manager 
     * must be initialized with the target device!
     */
    static PtrT createMgr()
    { return createInstance<TextureMgr>(); }

    /**
     * Initialize manager structure, creates an empty manager.
     * @param gpuMemoryMgr Manager of target GPU memory, used 
     * for storing resources.
     */
    void initialize(rndr::GpuMemoryMgr &gpuMemoryMgr);

    /// Free all resources.
    virtual ~TextureMgr() = default;

    // No copying.
    TextureMgr(const TextureMgr &other) = delete;
    TextureMgr &operator=(const TextureMgr &other) = delete;
    // Now moving.
    TextureMgr(TextureMgr &&other) = delete;
    TextureMgr &operator=(TextureMgr &&other) = delete;

    /**
     * Get named texture resource.
     * @param id Identifier or name of the texture.
     * @return Returns handle to the named texture resource or 
     * invalid handle if there is no texture with given identifier.
     */
    TextureHandle get(const IdentifierT &id);

    /**
     * Create or retrieve already existing texture. Returned 
     * handle points to the texture, but it may not be created 
     * yet. To finalize the creation call the update() method.
     * @param id Identifier of the texture. Empty identifier 
     * is invalid and automatically generated one will be 
     * used instead.
     * @param data Data to load into the texture.
     * @param sizeInBytes Number of bytes to load from the data 
     * texture into the GPU-side texture.
     * @param desc Description of the requested texture.
     * @return Returns handle to the texture.
     */
    TextureHandle create(const IdentifierT &id, const void *data, 
        std::size_t sizeInBytes, const ::CD3DX12_RESOURCE_DESC &desc);

    /**
     * Create or retrieve already existing texture. Returned 
     * handle points to the texture, but it may not be created 
     * yet. To finalize the creation call the update() method.
     * This version uses automatically generated identifier, 
     * created from provided parameters.
     * @param data Data to load into the texture.
     * @param sizeInBytes Number of bytes to load from the data 
     * texture into the GPU-side texture.
     * @param desc Description of the requested texture.
     * @return Returns handle to the texture.
     */
    TextureHandle create(const void *data, std::size_t sizeInBytes, 
        const ::CD3DX12_RESOURCE_DESC &desc);

    /**
     * Create or retrieve already existing texture.
     * Returned handle points to the texture which is always 
     * initialized immediately.
     * @param id Identifier of the texture. Empty identifier 
     * is invalid and automatically generated one will be 
     * used instead.
     * @param data Data to load into the texture.
     * @param sizeInBytes Number of bytes to load from the data 
     * texture into the GPU-side texture.
     * @param desc Description of the requested texture.
     * @return Returns handle to the texture.
     */
    TextureHandle createImmediate(const IdentifierT &id, const void *data, 
        std::size_t sizeInBytes, const ::CD3DX12_RESOURCE_DESC &desc);

    /**
     * Create or retrieve already existing texture.
     * Returned handle points to the texture which is always 
     * initialized immediately.
     * This version uses automatically generated identifier, 
     * created from provided parameters.
     * @param data Data to load into the texture.
     * @param sizeInBytes Number of bytes to load from the data 
     * texture into the GPU-side texture.
     * @param desc Description of the requested texture.
     * @return Returns handle to the texture.
     */
    TextureHandle createImmediate(const void *data, std::size_t sizeInBytes, 
        const ::CD3DX12_RESOURCE_DESC &desc);

    /**
     * Create or retrieve already existing texture.
     * Returned handle points to the texture which is always 
     * initialized immediately.
     * @param id Identifier of the texture. Empty identifier 
     * is invalid.
     * @param desc Description of the requested texture.
     * @param initialState Requested initial state of the 
     * texture resource. 
     * @param clearValue Optimized clear value used when 
     * clearing the texture resource.
     * @return Returns handle to the texture.
     */
    TextureHandle createEmpty(const IdentifierT &id, const ::CD3DX12_RESOURCE_DESC &desc,
        ::D3D12_RESOURCE_STATES initialState, const ::D3D12_CLEAR_VALUE *clearValue);

    /**
     * Perform book-keeping operation such as: 
     *   Finalize creation of textures.
     *   Free any textures with 0 in the reference counter.
     * @warning In the final stage of buffer creation, the 
     * command list containing copy commands is asynchronously 
     * executed in the provided command queue.
     * @throws util::winexception Thrown on any D3D12 error.
     */
    virtual void update() override final;
private:
    /// Base name of generated texture identifiers.
    static constexpr const char *UNIQUE_ID_BASE{ "Texture" };

    /// Flags used for the texture memory heaps.
    static constexpr ::D3D12_HEAP_FLAGS TEXTURE_HEAP_FLAGS{
        ::D3D12_HEAP_FLAG_DENY_RT_DS_TEXTURES | ::D3D12_HEAP_FLAG_DENY_BUFFERS };

    /**
     * Create or retrieve already existing texture.
     * Returned handle points to the texture, but it 
     * may not be created yet. To finalize the creation 
     * call the update() method.
     * @return Returns handle to the texture.
     */
    TextureHandle createInner(const IdentifierT &id, const void *data, 
        std::size_t sizeInBytes, const ::CD3DX12_RESOURCE_DESC &desc);

    /**
     * Create or retrieve already existing texture.
     * Returned handle points to the texture which is always 
     * initialized immediately.
     * @return Returns handle to the texture.
     */
    TextureHandle createImmediateInner(const IdentifierT &id, const void *data, 
        std::size_t sizeInBytes, const ::CD3DX12_RESOURCE_DESC &desc);

    /**
     * Create request for texture with given parameters.
     * @param res Target resource.
     * @param data Data to load into the texture.
     * @param sizeInBytes Number of bytes to load from the data 
     * texture into the GPU-side texture.
     * @param desc Description of the requested texture.
     */
    TextureRequest createRequestTicket(ResourceHolder *res, const void *data, 
        std::size_t sizeInBytes, const ::CD3DX12_RESOURCE_DESC &desc);

    /**
     * Add the request to initialize and fill texture resource 
     * with provided data.
     * @param res Target resource.
     * @param data Data to load into the texture.
     * @param sizeInBytes Number of bytes to load from the data 
     * texture into the GPU-side texture.
     * @param desc Description of the requested texture.
     */
    void appendRequestTicket(ResourceHolder *res, const void *data, 
        std::size_t sizeInBytes, const ::CD3DX12_RESOURCE_DESC &desc);

    /**
     * Finalize creation of requested texture.
     */
    void processRequests();

    /**
     * Immediately finalize the creation and filling of 
     * given texture.
     * @param id Identifier of the texture.
     * @param data Data to load into the texture.
     * @param sizeInBytes Number of bytes to load from the data 
     * texture into the GPU-side texture.
     * @param desc Description of the requested texture.
     */
    void processImmediateRequest(const HandleIdxT &id, const void *data,
        std::size_t sizeInBytes, const ::CD3DX12_RESOURCE_DESC &desc);

    /**
     * Immediately create the requested texture.
     * @param id Identifier of the texture. Empty identifier 
     * is invalid.
     * @param desc Description of the requested texture.
     * @param initialState Requested initial state of the 
     * texture resource. 
     * @param clearValue Optimized clear value used when 
     * clearing the texture resource.
     * @return Returns handle to the texture.
     */
    void processImmediateEmpty(const HandleIdxT &id, const ::CD3DX12_RESOURCE_DESC &desc,
        ::D3D12_RESOURCE_STATES initialState, const ::D3D12_CLEAR_VALUE *clearValue);

    /**
     * Initialize texture resource as described in provided user 
     * request.
     * @param request Container for description of requested 
     * texture.
     * @param allocator Allocator with memory large enough for 
     * allocation of the requested texture.
     * @param cmdList Copy command list which will be used for 
     * recording the copy commands.
     */
    void initializeTexture(const TextureRequest &request, 
        helpers::d3d12::D3D12PlacedAllocator::PtrT allocator, 
        d3d12::D3D12CommandList &cmdList);

    /**
     * Generate unique identifier for provided parameters.
     * @return Returns identifier unique to given parameters.
     */
    IdentifierT generateUniqueId(const void *data, std::size_t sizeInBytes, 
        const ::CD3DX12_RESOURCE_DESC &desc) const;

    /// List of requests for texture initialization since last update.
    std::vector<TextureRequest> mBufferRequests{ };

    /// Management of texture memory on the GPU.
    rndr::GpuMemoryMgr *mGpuMemoryMgr{ nullptr };
protected:
    /**
     * Initialize manager structure, creates an empty manager.
     * @warning Before any more actions are used, this manager 
     * must be initialized with the target device!
     */
    TextureMgr();

    /**
     * Initialize manager structure, creates an empty manager.
     * @param gpuMemoryMgr Manager of target GPU memory, used 
     * for storing scene resources.
     */
    TextureMgr(rndr::GpuMemoryMgr &gpuMemoryMgr);
}; // class TextureMgr

/// Handle to a GPU-side texture.
using TextureHandle = ::quark::res::rndr::TextureMgr::TextureHandle;

} // namespace rndr

} // namespace res

} // namespace quark
