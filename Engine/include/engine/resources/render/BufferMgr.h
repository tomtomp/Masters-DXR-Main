/**
 * @file resources/render/BufferMgr.h
 * @author Tomas Polasek
 * @brief Manager of GPU-side buffers.
 */

#pragma once

#include "engine/resources/BaseHandleMgr.h"

#include "engine/resources/d3d12/D3D12CommandQueueMgr.h"

#include "engine/resources/render/GpuMemoryMgr.h"
#include "engine/resources/render/Buffer.h"

/// Namespace containing the engine code.
namespace quark
{

/// Namespace containing resource management helpers.
namespace res
{

/// Rendering resources.
namespace rndr
{

/// Manager of GPU-side buffers with pre-defined content.
class BufferMgr : public BaseHandleMgr<Buffer, uint16_t, std::string, uint16_t>, public util::PointerType<BufferMgr>
{
public:
    /// Structure describing request to create and fill a GPU-side buffer.
    struct BufferRequest
    {
        /// Target resource to initialize.
        ResourceHolder *target{ nullptr };
        /// Data to load into the buffer.
        const void *data{ nullptr }; 
        /// Size of data in bytes.
        std::size_t sizeInBytes{ 0u };
        /// Description of the buffer.
        ::CD3DX12_RESOURCE_DESC desc{ };
    }; // struct BufferRequest

    /// Renaming resource handle for readability.
    using BufferHandle = ThisMgrT::ResourceHandle;

    /**
     * Create instance of this manager.
     * @return Returns pointer to the new instance.
     * @warning Before any more actions are used, this manager 
     * must be initialized with the target device!
     */
    static PtrT createMgr()
    { return createInstance<BufferMgr>(); }

    /**
     * Initialize manager structure, creates an empty manager.
     * @param gpuMemoryMgr Manager of target GPU memory, used 
     * for storing resources.
     */
    void initialize(rndr::GpuMemoryMgr &gpuMemoryMgr);

    /// Free all resources.
    virtual ~BufferMgr() = default;

    // No copying.
    BufferMgr(const BufferMgr &other) = delete;
    BufferMgr &operator=(const BufferMgr &other) = delete;
    // Now moving.
    BufferMgr(BufferMgr &&other) = delete;
    BufferMgr &operator=(BufferMgr &&other) = delete;

    /**
     * Get named buffer resource.
     * @param id Identifier or name of the buffer.
     * @return Returns handle to the named buffer resource or 
     * invalid handle if there is no buffer with given identifier.
     */
    BufferHandle get(const IdentifierT &id);

    /**
     * Create or retrieve already existing buffer. Returned 
     * handle points to the buffer, but it may not be created 
     * yet. To finalize the creation call the update() method.
     * @param id Identifier of the buffer. Empty identifier 
     * is invalid and automatically generated one will be 
     * used instead.
     * @param data Data to load into the buffer.
     * @param sizeInBytes Number of bytes to load from the data 
     * buffer into the GPU-side buffer.
     * @param desc Description of the requested buffer.
     * @return Returns handle to the buffer.
     */
    BufferHandle create(const IdentifierT &id, const void *data, 
        std::size_t sizeInBytes, const ::CD3DX12_RESOURCE_DESC &desc);

    /**
     * Create or retrieve already existing buffer. Returned 
     * handle points to the buffer, but it may not be created 
     * yet. To finalize the creation call the update() method.
     * This version uses automatically generated identifier, 
     * created from provided parameters.
     * @param data Data to load into the buffer.
     * @param sizeInBytes Number of bytes to load from the data 
     * buffer into the GPU-side buffer.
     * @param desc Description of the requested buffer.
     * @return Returns handle to the buffer.
     */
    BufferHandle create(const void *data, std::size_t sizeInBytes, 
        const ::CD3DX12_RESOURCE_DESC &desc);

    /**
     * Create or retrieve already existing buffer.
     * Returned handle points to the buffer which is always 
     * initialized immediately.
     * @param id Identifier of the buffer. Empty identifier 
     * is invalid and automatically generated one will be 
     * used instead.
     * @param data Data to load into the buffer.
     * @param sizeInBytes Number of bytes to load from the data 
     * buffer into the GPU-side buffer.
     * @param desc Description of the requested buffer.
     * @return Returns handle to the buffer.
     */
    BufferHandle createImmediate(const IdentifierT &id, const void *data, 
        std::size_t sizeInBytes, const ::CD3DX12_RESOURCE_DESC &desc);

    /**
     * Create or retrieve already existing buffer.
     * Returned handle points to the buffer which is always 
     * initialized immediately.
     * This version uses automatically generated identifier, 
     * created from provided parameters.
     * @param data Data to load into the buffer.
     * @param sizeInBytes Number of bytes to load from the data 
     * buffer into the GPU-side buffer.
     * @param desc Description of the requested buffer.
     * @return Returns handle to the buffer.
     */
    BufferHandle createImmediate(const void *data, std::size_t sizeInBytes, 
        const ::CD3DX12_RESOURCE_DESC &desc);

    /**
     * Perform book-keeping operation such as: 
     *   Finalize creation of buffers.
     *   Free any buffers with 0 in the reference counter.
     * @warning In the final stage of buffer creation, the 
     * command list containing copy commands is asynchronously 
     * executed in the provided command queue.
     * @throws util::winexception Thrown on any D3D12 error.
     */
    virtual void update() override final;
private:
    /// Base name of generated buffer identifiers.
    static constexpr const char *UNIQUE_ID_BASE{ "Buff" };

    /// Flags used for the buffer memory heaps.
    static constexpr ::D3D12_HEAP_FLAGS BUFFER_HEAP_FLAGS{
        ::D3D12_HEAP_FLAG_DENY_NON_RT_DS_TEXTURES | ::D3D12_HEAP_FLAG_DENY_RT_DS_TEXTURES };

    /**
     * Create or retrieve already existing buffer.
     * Returned handle points to the buffer, but it 
     * may not be created yet. To finalize the creation 
     * call the update() method.
     * @return Returns handle to the buffer.
     */
    BufferHandle createInner(const IdentifierT &id, const void *data, 
        std::size_t sizeInBytes, const ::CD3DX12_RESOURCE_DESC &desc);

    /**
     * Create or retrieve already existing buffer.
     * Returned handle points to the buffer which is always 
     * initialized immediately.
     * @return Returns handle to the buffer.
     */
    BufferHandle createImmediateInner(const IdentifierT &id, const void *data, 
        std::size_t sizeInBytes, const ::CD3DX12_RESOURCE_DESC &desc);

    /**
     * Create request for buffer with given parameters.
     * @param res Target resource.
     * @param data Data to load into the buffer.
     * @param sizeInBytes Number of bytes to load from the data 
     * buffer into the GPU-side buffer.
     * @param desc Description of the requested buffer.
     */
    BufferRequest createRequestTicket(ResourceHolder *res, const void *data, 
        std::size_t sizeInBytes, const ::CD3DX12_RESOURCE_DESC &desc);

    /**
     * Add the request to initialize and fill buffer resource 
     * with provided data.
     * @param res Target resource.
     * @param data Data to load into the buffer.
     * @param sizeInBytes Number of bytes to load from the data 
     * buffer into the GPU-side buffer.
     * @param desc Description of the requested buffer.
     */
    void appendRequestTicket(ResourceHolder *res, const void *data, 
        std::size_t sizeInBytes, const ::CD3DX12_RESOURCE_DESC &desc);

    /**
     * Finalize creation of requested buffers.
     */
    void processRequests();

    /**
     * Immediately finalize the creation and filling of 
     * given buffer.
     * @param id Identifier of the buffer.
     * @param data Data to load into the buffer.
     * @param sizeInBytes Number of bytes to load from the data 
     * buffer into the GPU-side buffer.
     * @param desc Description of the requested buffer.
     */
    void processImmediateRequest(const HandleIdxT &id, const void *data,
        std::size_t sizeInBytes, const ::CD3DX12_RESOURCE_DESC &desc);

    /**
     * Initialize buffer resource as described in provided user 
     * request.
     * @param request Container for description of requested 
     * buffer.
     * @param allocator Allocator with memory large enough for 
     * allocation of the requested buffer.
     * @param cmdList Copy command list which will be used for 
     * recording the copy commands.
     */
    void initializeBuffer(const BufferRequest &request, 
        helpers::d3d12::D3D12PlacedAllocator::PtrT allocator, 
        d3d12::D3D12CommandList &cmdList);

    /**
     * Generate unique identifier for provided parameters.
     * @return Returns identifier unique to given parameters.
     */
    IdentifierT generateUniqueId(const void *data, std::size_t sizeInBytes, 
        const ::CD3DX12_RESOURCE_DESC &desc) const;

    /// List of requests for buffer initialization since last update.
    std::vector<BufferRequest> mBufferRequests{ };

    /// Management of buffer memory on the GPU.
    rndr::GpuMemoryMgr *mGpuMemoryMgr{ nullptr };
protected:
    /**
     * Initialize manager structure, creates an empty manager.
     * @warning Before any more actions are used, this manager 
     * must be initialized with the target device!
     */
    BufferMgr();

    /**
     * Initialize manager structure, creates an empty manager.
     * @param gpuMemoryMgr Manager of target GPU memory, used 
     * for storing scene resources.
     */
    BufferMgr(rndr::GpuMemoryMgr &gpuMemoryMgr);
}; // class BufferMgr

/// Handle to a GPU-side buffer.
using BufferHandle = ::quark::res::rndr::BufferMgr::BufferHandle;

} // namespace rndr

} // namespace res

} // namespace quark
