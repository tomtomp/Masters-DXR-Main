/**
 * @file resources/render/Descriptor.h
 * @author Tomas Polasek
 * @brief Wrapper around descriptor.
 */

#pragma once

#include "engine/resources/BaseResource.h"
#include "engine/resources/render/BufferMgr.h"
#include "engine/resources/render/TextureMgr.h"

#include "engine/resources/d3d12/D3D12DescHeap.h"

/// Namespace containing the engine code.
namespace quark
{

/// Namespace containing resource management helpers.
namespace res
{

/// Rendering resources.
namespace rndr
{

/// Wrapper around descriptor - SRV, UAV or CBV.
struct Descriptor : BaseResource
{
    /// Type of shader resource view.
    enum class ResourceType
    {
        ShaderResourceView,
        UnorderedAccessView,
        ConstantBufferView,
        Unknown
    }; // enum class ResourceType

    /// Convert resource type into corresponding string.
    static constexpr const char *resourceTypeToStr(ResourceType type);

    /// Handle to the descriptor on the CPU.
    ::CD3DX12_CPU_DESCRIPTOR_HANDLE cpuHandle{ };
    /// Type of the descriptor.
    ResourceType handleType{ ResourceType::Unknown };
    /// Offset of the handle within the heap.
    uint32_t offsetInHeap{ };

    /// Type of the used buffer.
    enum class BufferType
    {
        GeneralBuffer,
        TextureBuffer,
        Unknown
    }; // enum class BufferType

    /// Used to potentially store the target of this view.
    BufferHandle targetBuffer{ };
    /// Used to potentially store the target of this view.
    TextureHandle targetTexture{ };

    /// Type of the target buffer.
    BufferType targetType{ BufferType::Unknown };

    /**
     * Access the target resource.
     * @throws std::runtime_error When no resource is set.
     */
    inline res::d3d12::D3D12Resource &res();

    /// Reset structure for use by given resource.
    inline void resetFor(const BufferHandle &targetHandle, ResourceType type);
    /// Reset structure for use by given resource.
    inline void resetFor(const TextureHandle &targetHandle, ResourceType type);
private:
    /// Resetting common to all types.
    inline void resetCommon(ResourceType type);
}; // struct Descriptor

} // namespace rndr

} // namespace res

} // namespace quark

// Template implementation.

namespace quark
{

namespace res
{

namespace rndr
{

constexpr const char *Descriptor::resourceTypeToStr(ResourceType type)
{
    switch (type)
    {
        case ResourceType::ShaderResourceView:
            return "SRV";
        case ResourceType::UnorderedAccessView: 
            return "UAV";
        case ResourceType::ConstantBufferView: 
            return "CBV";
        case ResourceType::Unknown: 
        default: 
            return "Unknown";
    }
}

inline res::d3d12::D3D12Resource &Descriptor::res()
{
    switch (targetType)
    {
        case BufferType::GeneralBuffer:
            return targetBuffer.get().buffer;
        case BufferType::TextureBuffer:
            return targetTexture.get().texture;
        default:
            throw std::runtime_error("No resource set.");
    }
}

inline void Descriptor::resetFor(const BufferHandle &targetHandle, ResourceType type)
{
    resetCommon(type);

    targetBuffer = targetHandle;
    targetType = BufferType::GeneralBuffer;
}

inline void Descriptor::resetFor(const TextureHandle &targetHandle, ResourceType type)
{
    resetCommon(type);

    targetTexture = targetHandle;
    targetType = BufferType::TextureBuffer;
}

inline void Descriptor::resetCommon(ResourceType type)
{
    cpuHandle = ::CD3DX12_CPU_DESCRIPTOR_HANDLE{ };
    handleType = type;
    offsetInHeap = 0u;

    targetBuffer = { };
    targetTexture = { };
}

}

}

}

// Template implementation end.

