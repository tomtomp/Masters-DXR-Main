/**
 * @file resources/render/Mesh.h
 * @author Tomas Polasek
 * @brief Wrapper around vertex attributes and material properties.
 */

#pragma once

#include "engine/resources/BaseResource.h"

#include "engine/resources/render/BufferViewMgr.h"
#include "engine/resources/render/MaterialMgr.h"

/// Namespace containing the engine code.
namespace quark
{

/// Namespace containing resource management helpers.
namespace res
{

/// Rendering resources.
namespace rndr
{

/// Wrapper around vertex attributes and material properties.
struct Primitive : BaseResource
{
    /// Types of attributes for which buffer views can be specified
    enum class AttributeType : uint8_t
    {
        /// Vec3 of float XYZ coordinates.
        Position, 
        /// Vec3 of float XYZ normalized vertex normals.
        Normal,
        /// Vec4 of float XYZW vertex tangents.
        Tangent,
        /// Vec2 of float/uint8/uint16 texture coordinates.
        TexCoord,
        /// Vec3/Vec4 of float/uint8/uint16 vertex colors.
        Color,
        /// Last attribute, whose value is the total number of types.
        LastAttributeType
    }; // enum class AttributeType

    using PrimitiveMode = scene::desc::PrimitiveMode;

    /// Total number of attribute types.
    static constexpr std::size_t NUM_ATTRIBUTES_TYPES{ static_cast<uint8_t>(AttributeType::LastAttributeType) };

    /// Release all resource handles.
    virtual ~Primitive() = default;

    /// Set data buffer for given attribute type.
    void setAttributeBuffer(AttributeType type, const BufferViewHandle &bufferView)
    { attributes[static_cast<std::size_t>(type)] = bufferView; }

    /// Set position attribute buffer.
    void setPositionBuffer(const BufferViewHandle &bufferView)
    { attributes[static_cast<std::size_t>(AttributeType::Position)] = bufferView; }

    /// Set normal attribute buffer.
    void setNormalBuffer(const BufferViewHandle &bufferView)
    { attributes[static_cast<std::size_t>(AttributeType::Normal)] = bufferView; }

    /// Set tangent attribute buffer.
    void setTangentBuffer(const BufferViewHandle &bufferView)
    { attributes[static_cast<std::size_t>(AttributeType::Tangent)] = bufferView; }

    /// Set texture coordinate attribute buffer.
    void setTexCoordBuffer(const BufferViewHandle &bufferView)
    { attributes[static_cast<std::size_t>(AttributeType::TexCoord)] = bufferView; }

    /// Set color attribute buffer.
    void setColorBuffer(const BufferViewHandle &bufferView)
    { attributes[static_cast<std::size_t>(AttributeType::Color)] = bufferView; }

    /// Get data buffer for give attribute type.
    const BufferViewHandle &getAttributeBuffer(AttributeType type) const
    { return attributes[static_cast<std::size_t>(type)]; }

    /// Get position attribute buffer.
    const BufferViewHandle &getPositionBuffer() const
    { return attributes[static_cast<std::size_t>(AttributeType::Position)]; }

    /// Get normal attribute buffer.
    const BufferViewHandle &getNormalBuffer() const
    { return attributes[static_cast<std::size_t>(AttributeType::Normal)]; }

    /// Get tangent attribute buffer.
    const BufferViewHandle &getTangentBuffer() const
    { return attributes[static_cast<std::size_t>(AttributeType::Tangent)]; }

    /// Get texture coordinate attribute buffer.
    const BufferViewHandle &getTexCoordBuffer() const
    { return attributes[static_cast<std::size_t>(AttributeType::TexCoord)]; }

    /// Get color attribute buffer.
    const BufferViewHandle &getColorBuffer() const
    { return attributes[static_cast<std::size_t>(AttributeType::Color)]; }

    /// List of vertex attributes and their buffer views.
    std::array<BufferViewHandle, NUM_ATTRIBUTES_TYPES> attributes{ };

    /// Indices into the vertex attribute buffers.
    BufferViewHandle indices{ };

    /// Material of this mesh.
    MaterialHandle material{ };

    /// What kind of primitives does this primitive use?
    PrimitiveMode primitiveMode{ PrimitiveMode::Triangles };
}; // struct Primitive

/// Each mesh can have 1 or more primitives.
struct Mesh : BaseResource
{
    /// Release all resource handles.
    virtual ~Mesh() = default;

    /// List of primitives defining geometry and material.
    std::vector<Primitive> primitives{ };
}; // struct Mesh

} // namespace rndr

} // namespace res

} // namespace quark
