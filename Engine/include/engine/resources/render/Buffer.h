/**
 * @file resources/render/Buffer.h
 * @author Tomas Polasek
 * @brief General GPU-side buffer wrapper.
 */

#pragma once

#include "engine/resources/BaseResource.h"

#include "engine/helpers/d3d12/D3D12UploadBuffer.h"

/// Namespace containing the engine code.
namespace quark
{

/// Namespace containing resource management helpers.
namespace res
{

/// Rendering resources.
namespace rndr
{

/// Wrapper around GPU-side buffer.
struct Buffer : BaseResource
{
    /// Buffer resource itself.
    helpers::d3d12::D3D12UploadBuffer buffer;

    /// Check if this buffer is valid.
    bool valid() const noexcept 
    { return buffer.valid(); }

    /// Check if this buffer is valid
    explicit operator bool() const noexcept 
    { return valid(); }
}; // struct Buffer

} // namespace rndr

} // namespace res

} // namespace quark
