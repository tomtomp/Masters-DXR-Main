/**
 * @file resources/render/DepthStencilBuffer.h
 * @author Tomas Polasek
 * @brief Depth-stencil buffer abstraction, usable in rendering.
 */

#pragma once

#include "engine/resources/BaseResource.h"

#include "engine/resources/d3d12/D3D12DescHeap.h"
#include "engine/resources/d3d12/D3D12CommandQueueMgr.h"
#include "engine/helpers/d3d12/D3D12TextureBuffer.h"
#include "engine/helpers/d3d12/D3D12CommittedAllocator.h"

/// Namespace containing the engine code.
namespace quark
{

/// Namespace containing resource management helpers.
namespace res
{

/// Rendering resources.
namespace rndr
{

/// Depth-stencil buffer abstraction, usable in rendering.
class DepthStencilBuffer : public BaseResource, public util::PointerType<DepthStencilBuffer>
{
public:
    /// Default format of the depth-stencil buffer.
    static constexpr ::DXGI_FORMAT DEFAULT_FORMAT{ ::DXGI_FORMAT_D32_FLOAT };
    /// Default clear value for the depth buffer.
    static constexpr float DEFAULT_DEPTH_CLEAR_VALUE{ 1.0f };
    /// Default clear value for the stencil buffer.
    static constexpr uint8_t DEFAULT_STENCIL_CLEAR_VALUE{ 0u };
    /// Default initial state for the depth-stencil buffer.
    static constexpr ::D3D12_RESOURCE_STATES DEFAULT_INIT_STATE{ ::D3D12_RESOURCE_STATE_DEPTH_WRITE };

    /**
     * Initialize the depth-stencil buffer, freeing the current 
     * buffer.
     * Uses previously configured attributes.
     * @param width Width of the new depth-stencil buffer in 
     * pixels.
     * @param height Height of the new depth-stencil buffer in 
     * pixels.
     * @param device Target device.
     */
    static PtrT create(uint32_t width, uint32_t height, res::d3d12::D3D12Device &device);

    /// Free the depth-stencil buffer.
    virtual ~DepthStencilBuffer();

    /**
     * Record a command on given command list which will 
     * clear this depth-stencil buffer.
     * @param cmdList Command list to record the command on.
     * @param clearFlags Flags specifying what should be 
     * cleared.
     * @throws Throws util::winexception on error.
     * @warning Does nothing if there is no depth-stencil 
     * buffer allocated.
     */
    void clear(res::d3d12::D3D12CommandList &cmdList, ::D3D12_CLEAR_FLAGS clearFlags = ::D3D12_CLEAR_FLAG_DEPTH);

    /// Get the depth-stencil view for this buffer.
    ::CD3DX12_CPU_DESCRIPTOR_HANDLE dsv() const noexcept
    { return mDepthStencilHandle; }

    /// Get pointer to the DSV descriptor heap.
    ::ID3D12DescriptorHeap *descHeap() const
    { return mDescHeap->get(); }

    /// Is the depth-stencil buffer allocated?
    bool allocated() const
    { return mDepthBuffer->valid(); }

    /// Is the depth-stencil buffer allocated?
    explicit operator bool() const
    { return allocated(); }
private:
    /// Format of the depth-stencil buffer.
    ::DXGI_FORMAT mDsFormat{ DEFAULT_FORMAT };
    /// Clear value for the depth component.
    float mDepthClear{ DEFAULT_DEPTH_CLEAR_VALUE };
    /// Clear value for the stencil component.
    uint8_t mStencilClear{ DEFAULT_STENCIL_CLEAR_VALUE };
    /// Initial state of the depth-buffer after creation.
    ::D3D12_RESOURCE_STATES mInitResourceState{ DEFAULT_INIT_STATE };

    /// Clear value used for optimized depth-stencil clearing.
    ::D3D12_CLEAR_VALUE mClearValue{ };

    /// Descriptor heap used for creation of descriptors for this buffer.
    res::d3d12::D3D12DescHeap::PtrT mDescHeap;
    /// Allocated depth-buffer.
    helpers::d3d12::D3D12TextureBuffer::PtrT mDepthBuffer;
    /// Handle for the depth-stencil buffer.
    ::CD3DX12_CPU_DESCRIPTOR_HANDLE mDepthStencilHandle;
    /// Allocator used for the depth-stencil texture buffer.
    helpers::d3d12::D3D12CommittedAllocator::PtrT mAllocator;
protected:
    /// Un-initialized depth-stencil buffer.
    DepthStencilBuffer();

    /**
     * Initialize the depth-stencil buffer, freeing the current 
     * buffer.
     * Uses previously configured attributes.
     * @param width Width of the new depth-stencil buffer in 
     * pixels.
     * @param height Height of the new depth-stencil buffer in 
     * pixels.
     * @param device Target device.
     */
    DepthStencilBuffer(uint32_t width, uint32_t height, res::d3d12::D3D12Device &device);

    /**
     * Initialize the depth-stencil buffer, freeing the current 
     * buffer.
     * Uses previously configured attributes.
     * @param width Width of the new depth-stencil buffer in 
     * pixels.
     * @param height Height of the new depth-stencil buffer in 
     * pixels.
     * @param device Target device.
     */
    void initialize(uint32_t width, uint32_t height, res::d3d12::D3D12Device &device);
}; // class DepthBuffer

} // namespace rndr

} // namespace res

} // namespace quark
