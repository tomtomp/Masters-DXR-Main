/**
 * @file resources/render/MeshMgr.h
 * @author Tomas Polasek
 * @brief Manager of meshes, connection of vertex attributs, indices and material.
 */

#pragma once

#include "engine/resources/BaseHandleMgr.h"

#include "engine/resources/render/Mesh.h"

/// Namespace containing the engine code.
namespace quark
{

/// Namespace containing resource management helpers.
namespace res
{

/// Rendering resources.
namespace rndr
{

/// Manager of meshes, connection of vertex attributes, indices and material.
class MeshMgr : public BaseHandleMgr<Mesh, uint16_t, std::string, uint16_t>, public util::PointerType<MeshMgr>
{
public:
    /// Renaming resource handle for readability.
    using MeshHandle = ThisMgrT::ResourceHandle;

    /**
     * Create instance of this manager.
     * @return Returns pointer to the new instance.
     */
    static PtrT createMgr()
    { return createInstance<MeshMgr>(); }

    /// Free all resources.
    virtual ~MeshMgr();

    // No copying.
    MeshMgr(const MeshMgr &other) = delete;
    MeshMgr &operator=(const MeshMgr &other) = delete;
    // No moving.
    MeshMgr(MeshMgr &&other) = delete;
    MeshMgr &operator=(MeshMgr &&other) = delete;

    /**
     * Create or retrieve already existing mesh.
     * Returns material handle points to a default 
     * initialized mesh.
     * @param id Identifier of the mesh. Empty identifier 
     * is invalid and automatically generated one will be 
     * used instead.
     * @return Returns handle to the mesh.
     */
    MeshHandle create(const IdentifierT &id);

    /**
     * Perform book-keeping operation such as: 
     *   Free any materials with 0 in the reference counter.
     */
    virtual void update() override final;
private:
    /// Base name of generated mesh identifiers.
    static constexpr const char *UNIQUE_ID_BASE{ "Mesh" };

    /**
     * Create or retrieve already existing mesh.
     * Returns material handle points to a default 
     * initialized mesh.
     * @return Returns handle to the mesh.
     */
    MeshHandle createInner(const IdentifierT &id);

    /**
     * Generate unique identifier for a mesh.
     * @return Returns unique mesh identifier.
     */
    IdentifierT generateUniqueId();

    /// Counter used in generation of unique IDs.
    std::size_t mUniqueIdCounter{ 0u };
protected:
    /**
     * Initialize manager structure, creates an empty manager.
     */
    MeshMgr();
}; // class MeshMgr

/// Handle to a mesh.
using MeshHandle = ::quark::res::rndr::MeshMgr::MeshHandle;

} // namespace rndr

} // namespace res

} // namespace quark
