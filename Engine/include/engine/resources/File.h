/**
 * @file resources/File.h
 * @author Tomas Polasek
 * @brief File header containing File types specific to the target OS / filesystem.
 */

#pragma once

// Windows: 
#ifdef _MSC_BUILD

#include "engine/resources/win32/WinFile.h"

/// Namespace containing the engine code.
namespace quark
{

/// Namespace containing resource management helpers.
namespace res
{

using File = ::quark::res::win::WinFileS;
using FileW = ::quark::res::win::WinFileW;

} // namespace res

} // namespace quark

#endif
