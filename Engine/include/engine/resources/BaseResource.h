/**
 * @file resources/BaseResource.h
 * @author Tomas Polasek
 * @brief Base class for all resources.
 */

#pragma once

#include "engine/util/Types.h"

/// Namespace containing the engine code.
namespace quark
{

/// Namespace containing resource management helpers.
namespace res
{

/// Base class for resources.
class BaseResource
{
public:
    BaseResource() = default;
    virtual ~BaseResource() = default;

    // Depending on the resource, they may allow copying/moving.
    BaseResource(const BaseResource &other) = default;
    BaseResource(BaseResource &&other) = default;
    BaseResource &operator=(const BaseResource &rhs) = default;
    BaseResource &operator=(BaseResource &&rhs) = default;
private:
protected:
}; // class BaseResourceMgr

} // namespace res

} // namespace quark
