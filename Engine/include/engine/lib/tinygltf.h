/**
 * @file lib/tinygltf.h
 * @author Tomas Polasek
 * @brief Helper header for including tinyGLTF library headers.
 */

#pragma once

#include "engine/util/StlWrapperBegin.h"

#include "../lib/tinygltf/tiny_gltf.h"

#include "engine/util/StlWrapperEnd.h"
