/**
 * @file lib/imgui.h
 * @author Tomas Polasek
 * @brief Helper header for including ImGUI library headers.
 */

#pragma once

#include "engine/util/StlWrapperBegin.h"

// Main library: 
#define IMGUI_USER_CONFIG "engine/lib/imconfig.h"
#include "../lib/imgui/imgui.h"

#include "engine/util/StlWrapperEnd.h"
