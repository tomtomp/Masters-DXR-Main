/**
 * @file lib/d3dx12.h
 * @author Tomas Polasek
 * @brief Helper header for including DXR fallback 
 * layer and ray tracing helpers.
 */

#pragma once

#include "engine/util/StlWrapperBegin.h"

#ifdef ENGINE_RAY_TRACING_ENABLED
#   include "../lib/DxrFallback/include/D3D12RaytracingFallback.h"
#   include "../lib/DxrFallback/include/D3D12RaytracingHelpers.hpp"
#endif

#include "engine/util/StlWrapperEnd.h"
