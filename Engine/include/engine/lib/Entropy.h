/**
 * @file lib/Entropy.h
 * @author Tomas Polasek
 * @brief Helper header for including Entropy library headers.
 */

#pragma once

#include "engine/util/StlWrapperBegin.h"

#define ENT_ENTITY_EXCEPT
#define ENT_ENTITY_VALID
#include "../lib/Entropy/Entropy.h"

#include "engine/util/StlWrapperEnd.h"
