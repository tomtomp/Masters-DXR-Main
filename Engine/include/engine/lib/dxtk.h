/**
 * @file lib/dxtk.h
 * @author Tomas Polasek
 * @brief Helper header for the DirectX ToolKit library.
 */

#pragma once

#include "engine/util/StlWrapperBegin.h"

#include "../lib/dxtk/SimpleMath.h"

#include "engine/util/StlWrapperEnd.h"
