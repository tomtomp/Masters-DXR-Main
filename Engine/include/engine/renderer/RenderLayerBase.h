/**
 * @file renderer/RenderLayerBase.h
 * @author Tomas Polasek
 * @brief Base class for all rendering layers.
 */

#pragma once

/// Namespace containing the engine code.
namespace quark
{

/// Rendering classes.
namespace rndr
{

// Forward declaration.
class RenderSubSystem;

/**
 * Base class for all rendering layers.
 */
class RenderLayerBase
{
public:
    RenderLayerBase(const RenderLayerBase &other) = delete;
    RenderLayerBase &operator=(const RenderLayerBase &other) = delete;
private:
protected:
    RenderLayerBase() = default;
}; // class RenderLayerBase

} // namespace rndr

} // namespace quark
