/**
 * @file renderer/ElementFormat.h
 * @author Tomas Polasek
 * @brief Enumeration of data formats.
 */

#pragma once

#include "engine/util/Types.h"

/// Namespace containing the engine code.
namespace quark
{

/// Rendering classes.
namespace rndr
{

/// Type of each component.
enum class ComponentFormat : uint16_t
{
    Unknown, 

    // 8 bits: 
    SignedByte, 
    UnsignedByte, 
    SignedNormByte, 
    UnsignedNormByte, 
    // 16 bits: 
    SignedShort, 
    UnsignedShort, 
    SignedNormShort, 
    UnsignedNormShort, 
    Float16, 
    Depth16, 
    // 32 bits: 
    SignedInt, 
    UnsignedInt, 
    Float32, 
    Depth32, 
    // Compressed: 
    CompressedBC1U, 
    CompressedBC1S, 
    CompressedBC2U, 
    CompressedBC2S, 
    CompressedBC3U, 
    CompressedBC3S, 
    CompressedBC4U, 
    CompressedBC4S, 
    CompressedBC5U, 
    CompressedBC5S, 
    CompressedBC6U, 
    CompressedBC6S, 
    CompressedBC7U, 
    CompressedBC7S
}; // enum class BufferElementFormat

/// Type of every element, consisting of multiple components.
struct ElementFormat
{
    /// Maximum number of components per element - mat4 -> 16 components.
    static constexpr std::size_t MAX_COMPONENTS{ 16u };
    /// How many bytes can take one element? Largest component is float.
    static constexpr std::size_t MAX_BYTES_PER_ELEMENT{ MAX_COMPONENTS * sizeof(float) };

    /// Format of each component.
    ComponentFormat format{ ComponentFormat::Unknown };
    /// Number of components per element.
    uint16_t numComponents{ 0u };
}; // struct ElementFormat

} // namespace rndr

} // namespace quark
