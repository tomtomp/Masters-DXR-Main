/**
 * @file renderer/RasterShaderProgram.h
 * @author Tomas Polasek
 * @brief Wrapper around shaders used by 
 * rasterization pipeline.
 */

#pragma once

#include "engine/renderer/RenderContext.h"
#include "engine/renderer/ShaderVariables.h"
#include "engine/renderer/ShaderCache.h"

/// Namespace containing the engine code.
namespace quark
{

/// Rendering classes.
namespace rndr
{

// Forward declaration.
class RenderContext;

/// Types of shaders used in rasterization pipeline.
enum class RasterShaderType
{
    /// Vertex processing shader.
    Vertex = 0u,
    /// Tessellation Hull/Control shader.
    Hull,
    /// Tessellation Domain/Evaluation shader.
    Domain, 
    /// Geometry shader.
    Geometry,
    /// Pixel/Fragment shader.
    Pixel, 

    Last = Pixel
}; // enum class RasterShaderType

/**
 * Wrapper around shaders used by rasterization pipeline.
 */
class RasterShaderProgram : public util::PointerType<RasterShaderProgram> 
{
public:
    /**
     * Helper structure used for passing shader type 
     * and its associated entry-point.
     */
    struct ShaderTypeEntryPoint
    {
        ShaderTypeEntryPoint(RasterShaderType t, const std::string &ep) :
            type{ t }, entryPoint{ ep }
        { }

        /// Type of the shader.
        RasterShaderType type;
        /// Main entry-point for the shader type.
        std::string entryPoint;
    }; // struct ShaderTypeEntryPoint

    /// Free any resources used by this dispatch.
    virtual ~RasterShaderProgram();

    // No copying or moving
    RasterShaderProgram(const RasterShaderProgram &other) = delete;
    RasterShaderProgram &operator=(const RasterShaderProgram &other) = delete;
    RasterShaderProgram(RasterShaderProgram &&other) = delete;
    RasterShaderProgram &operator=(RasterShaderProgram &&other) = delete;

    /**
     * Load multiple shader stages from a file, specifying entry-point name for each 
     * stage loaded.
     * @param filename Name of the file containing all shader source code.
     * @param entryPoints Specification of shader stages and their respective entry-point 
     * names.
     */
    void shadersFromFile(const std::string &filename, 
        const std::initializer_list<ShaderTypeEntryPoint> &entryPoints);

    /**
     * Load vertex shader from file, optionally specifying the entry point.
     * @param filename Name of the file containing shader source code.
     * @param entryPoint Entry-point of the shader.
     */
    void vsFromFile(const std::string &filename, const std::string &entryPoint = "main");

    /**
     * Load vertex shader from pre-compiled blob.
     * @param identifier String identifier of the shader, which is unique 
     * between different blobs.
     * @param ptr Pointer to the binary blob of pre-compiled shader code.
     * @param sizeInBytes Number of bytes in the binary blob.
     */
    void vsfromBlob(const std::string &identifier, const uint8_t *ptr, std::size_t sizeInBytes);

    /**
     * Load tessellation hull/control shader from file, optionally specifying the entry point.
     * @param filename Name of the file containing shader source code.
     * @param entryPoint Entry-point of the shader.
     */
    void hsFromFile(const std::string &filename, const std::string &entryPoint = "main");

    /**
     * Load tessellation hull/control shader from pre-compiled blob.
     * @param identifier String identifier of the shader, which is unique 
     * between different blobs.
     * @param ptr Pointer to the binary blob of pre-compiled shader code.
     * @param sizeInBytes Number of bytes in the binary blob.
     */
    void hsfromBlob(const std::string &identifier, const uint8_t *ptr, std::size_t sizeInBytes);

    /**
     * Load tessellation domain/evaluation shader from file, optionally specifying the entry point.
     * @param filename Name of the file containing shader source code.
     * @param entryPoint Entry-point of the shader.
     */
    void dsFromFile(const std::string &filename, const std::string &entryPoint = "main");

    /**
     * Load tessellation domain/evaluation shader from pre-compiled blob.
     * @param identifier String identifier of the shader, which is unique 
     * between different blobs.
     * @param ptr Pointer to the binary blob of pre-compiled shader code.
     * @param sizeInBytes Number of bytes in the binary blob.
     */
    void dsfromBlob(const std::string &identifier, const uint8_t *ptr, std::size_t sizeInBytes);

    /**
     * Load geometry shader from file, optionally specifying the entry point.
     * @param filename Name of the file containing shader source code.
     * @param entryPoint Entry-point of the shader.
     */
    void gsFromFile(const std::string &filename, const std::string &entryPoint = "main");

    /**
     * Load geometry shader from pre-compiled blob.
     * @param identifier String identifier of the shader, which is unique 
     * between different blobs.
     * @param ptr Pointer to the binary blob of pre-compiled shader code.
     * @param sizeInBytes Number of bytes in the binary blob.
     */
    void gsfromBlob(const std::string &identifier, const uint8_t *ptr, std::size_t sizeInBytes);

    /**
     * Load pixel/fragment shader from file, optionally specifying the entry point.
     * @param filename Name of the file containing shader source code.
     * @param entryPoint Entry-point of the shader.
     */
    void psFromFile(const std::string &filename, const std::string &entryPoint = "main");

    /**
     * Load pixel/fragment shader from pre-compiled blob.
     * @param identifier String identifier of the shader, which is unique 
     * between different blobs.
     * @param ptr Pointer to the binary blob of pre-compiled shader code.
     * @param sizeInBytes Number of bytes in the binary blob.
     */
    void psfromBlob(const std::string &identifier, const uint8_t *ptr, std::size_t sizeInBytes);

    /// Access the shader variable storage.
    ShaderVariables &vars();
    /// Access the shader variable storage.
    const ShaderVariables &vars() const;
private:
protected:
    /// Maximal number of shader stages in the rasterization shader program.
    static constexpr std::size_t SHADER_STAGES{ static_cast<uint32_t>(RasterShaderType::Last) + 1u };
    /// Array type used to store the shader stages.
    using ShaderStageArray = std::array<ShaderRecord::ConstPtrT, SHADER_STAGES>;

    /// Convert rasterization shader stage to corresponding shader type.
    static ShaderType shaderTypeForStage(RasterShaderType stage);

    /// Access shader record for given shader stage.
    static ShaderRecord::ConstPtrT &shaderStage(ShaderStageArray &arr, RasterShaderType stage);

    /**
     * Create a new default configured rasterization dispatch 
     * using the provided rendering context.
     * @param ctx Current rendering context.
     */
    RasterShaderProgram(RenderContext::PtrT ctx);

    /**
     * Make sure all of the shaders are loaded and prepared.
     */
    void finalizeLoadShaders();

    /// Currently used rendering context.
    RenderContext::PtrT mRenderCtx{ };
    /// Shader variable storage for this program.
    ShaderVariables::PtrT mVariables{ };

    /// Array of shaders available to rasterization pipeline.
    std::array<ShaderRecord::ConstPtrT, SHADER_STAGES> mShaders{ };
}; // class RasterShaderProgram

} // namespace rndr

} // namespace quark
