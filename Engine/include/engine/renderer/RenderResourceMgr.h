/**
 * @file renderer/RenderResourceMgr.h
 * @author Tomas Polasek
 * @brief Management of resources used in rendering. These 
 * include render target textures, buffers, samplers, etc.
 */

#pragma once

#include "engine/util/Util.h"

#include "engine/renderer/RenderContext.h"

#include "engine/resources/Render/BufferMgr.h"
#include "engine/resources/Render/TextureMgr.h"
#include "engine/resources/Render/StaticSamplerMgr.h"

/// Namespace containing the engine code.
namespace quark
{

/// Rendering classes.
namespace rndr
{

/**
 * Management of resources used in rendering. These 
 * include render target textures, buffers, samplers, etc.
 */
class RenderResourceMgr : public util::PointerType<RenderResourceMgr>
{
public:
    /// Handle to a render target texture.
    using RenderTargetHandle = res::rndr::TextureHandle;
    /// Handle to a buffer.
    using BufferHandle = res::rndr::BufferHandle;
    /// Handle to a sampler.
    using SamplerHandle = res::rndr::StaticSamplerHandle;

    /**
     * Create and initialize the resource manager.
     * @param renderContext Current rendering context.
     * @return Returns pointer to the created resource manager.
     */
    static PtrT create(RenderContext::PtrT &renderContext);

    /// Free any resources used by the resource manager.
    ~RenderResourceMgr();

    // No copying or moving.
    RenderResourceMgr(const RenderResourceMgr &other) = delete;
    RenderResourceMgr &operator=(const RenderResourceMgr &other) = delete;
    RenderResourceMgr(RenderResourceMgr &&other) = delete;
    RenderResourceMgr &operator=(RenderResourceMgr &&other) = delete;
private:
    /// Pointer to the current rendering context.
    RenderContext::PtrT mRenderCtx{ };

    /// Inner texture/frame buffer manager.
    res::rndr::TextureMgr::PtrT mTextureMgr{ };
    /// Manager of buffers.
    res::rndr::BufferMgr::PtrT mBufferMgr{ };
    /// Manager of samplers.
    res::rndr::StaticSamplerMgr::PtrT mSamplerMgr{ };
protected:
    /**
     * Initialize the resource manager.
     * @param renderContext Current rendering context.
     */
    RenderResourceMgr(RenderContext::PtrT &renderContext);
}; // class RenderResourceMgr

} // namespace rndr

} // namespace quark
