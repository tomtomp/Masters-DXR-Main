/**
 * @file renderer/ResourceViewType.h
 * @author Tomas Polasek
 * @brief Enumeration of resource view types..
 */

#pragma once

#include "engine/util/Types.h"

/// Namespace containing the engine code.
namespace quark
{

/// Rendering classes.
namespace rndr
{

/// Type of each component.
enum class ResourceViewType : uint16_t
{
    Unknown, 

    /// Read-only shader resource.
    ShaderResource,
    /// Write-only shader resource.
    RenderTarget, 
    /// Depth and/or stencil resource.
    DepthStencil, 
    /// Read-only constant buffer.
    ConstantBuffer,
    /// Read-write buffer.
    UnorderedAccess,
}; // enum class BufferElementFormat

} // namespace rndr

} // namespace quark
