/**
 * @file renderer/RayTracingProvider.h
 * @author Tomas Polasek
 * @brief Provider of ray tracing features.
 */

#pragma once

// Engine configuration: 
#include "EngineRuntimeConfig.h"

// D3D12 resources: 
#include "engine/resources/d3d12/D3D12Adapter.h"
#include "engine/resources/d3d12/D3D12Device.h"
#include "engine/resources/d3d12/D3D12RayTracingDevice.h"

// Fallback layer and ray tracing helpers: 
#include "engine/lib/DxrFallback.h"

/// Namespace containing the engine code.
namespace quark
{

/// Rendering classes.
namespace rndr
{

// Forward declaration.
class RenderSubSystem;

/// Provider of ray tracing features.
class RayTracingProvider : public util::PointerType<RayTracingProvider>
{
public:
    /// Specification of ray tracing API.
    enum class RayTracingApi
    {
        /// Not using ray tracing.
        None,
        /// Use fallback layer, which still may use hardware.
        Fallback, 
        /// Use fallback layer but force compute fallback.
        FallbackForceCompute,
        /// Use pure hardware.
        Hardware
    }; // enum class RayTracingApi

    /**
     * Initialize the ray-tracing provider, using provided 
     * runtime configuration and resources of given rendering 
     * sub-system.
     * @param cfg Runtime configuration of the engine.
     * @param renderer Rendering sub-system.
     * @return Returns pointer to the initialized provider.
     */
    static PtrT create(const EngineRuntimeConfig &cfg, RenderSubSystem &renderer);

    /// Free all resources.
    ~RayTracingProvider();

    // No copying or moving.
    RayTracingProvider(const RayTracingProvider &other) = delete;
    RayTracingProvider &operator=(const RayTracingProvider &other) = delete;
    RayTracingProvider(RayTracingProvider &&other) = delete;
    RayTracingProvider &operator=(RayTracingProvider &&other) = delete;

    /// Does currently used adapter/device support ray tracing capabilities?
    bool rayTracingSupported() const;
    /// Does currently used adapter/device support fallback ray tracing capabilities?
    bool computeRayTracingSupported() const;
    /// Does currently used adapter/device support hardware ray tracing capabilities?
    bool hardwareRayTracingSupported() const;

    /// Are we using ray tracing fallback layer?
    bool usingFallbackLayer() const;

    /**
     * Select what kind of API should be used: 
     *   Fallback - Uses the DXR fallback layer, which may still use hardware RT.
     *   FallbackForceCompute - Uses the DXR fallback layer, forcing compute fallback for RT.
     *   Hardware - Uses device driver for hardware accelerated ray tracing.
     * @param capability Choose which api - hardware or fallback - should be used.
     * @return Returns true if the switch to given api has been successful.
     */
    bool selectRayTracingApi(RayTracingApi capability);

    /**
     * Enable ray tracing, automatically choosing the highest performance API which 
     * is available: 
     *   1) Hardware
     *   2) Fallback
     *   3) None
     * @return Returns true, if (1) or (2) has been chosen, else returns false.
     */
    bool enableRayTracingApi();

    /// Device usable for ray tracing operations.
    res::d3d12::D3D12RayTracingDevice &device()
    { return *mRayTracingDevice; }

    /// Which Ray Tracing backend are we using?
    RayTracingApi rayTracingBackend() const;

    /// Readable string representing currently used ray tracing backend.
    std::string rayTracingBackendStr() const;
private:
    /// Compile-time configuration.
    using Config = EngineConfig;

    /**
     * Initialize the ray tracing provider, making it 
     * ready for use.
     * @param cfg Runtime configuration of the engine.
     */
    void initialize(const EngineRuntimeConfig &cfg);

    /**
     * Does given adapter support ray tracing emulation 
     * through the use of compute shaders?
     * @param adapter Adapter to check.
     * @param device Device created from the adapter.
     */
    bool isComputeRayTracingSupported(
        const res::d3d12::D3D12Adapter &adapter, 
        const res::d3d12::D3D12Device &device);

    /**
     * Does given device support ray tracing hardware 
     * features?
     * @param device Device to check.
     */
    bool isHardwareTracingSupported(
        const res::d3d12::D3D12Device &device);

    /**
     * Initialization of the actual ray tracing hardware, available only with 
     * compile time support from system libraries.
     */
    void initializeRayTracing();

    /// Reference to rendering sub-system used by the engine.
    RenderSubSystem &mRenderer;

    /// Is fallback ray tracing supported?
    bool mComputeRayTracingSupported{ false };
    /// Is hardware ray tracing supported?
    bool mHardwareRayTracingSupported{ false };

    /// Currently used ray tracing API.
    RayTracingApi mRayTracingApi{ RayTracingApi::None };

    /// Device usable for ray tracing, abstracts from fallback layer specifics.
    res::d3d12::D3D12RayTracingDevice::PtrT mRayTracingDevice{ };
protected:
    /**
     * Initialize the ray-tracing provider, using provided 
     * runtime configuration and resources of given rendering 
     * sub-system.
     * @param cfg Runtime configuration of the engine.
     * @param renderer Rendering sub-system.
     */
    RayTracingProvider(const EngineRuntimeConfig &cfg, RenderSubSystem &renderer);
}; // class RayTracingProvider

} // namespace rndr

} // namespace quark
