/**
 * @file renderer/RenderSubSystem.h
 * @author Tomas Polasek
 * @brief Rendering system.
 */

#pragma once

// Engine configuration: 
#include "EngineRuntimeConfig.h"

// Windows resources: 
#include "engine/resources/win32/Window.h"

// Direct3D 12 resources: 
#include "engine/resources/d3d12/D3D12Adapter.h"
#include "engine/resources/d3d12/D3D12DXGIFactory.h"
#include "engine/resources/d3d12/D3D12Device.h"
#include "engine/resources/d3d12/D3D12CommandQueueMgr.h"
#include "engine/resources/d3d12/D3D12SwapChain.h"

// Direct3D 12 helpers: 
#include "engine/helpers/d3d12/D3D12LiveObjectReporter.h"
#include "engine/helpers/d3d12/D3D12DebugConfigurator.h"
#include "engine/helpers/d3d12/D3D12ImGuiWrapper.h"
#include "engine/helpers/d3d12/D3D12CommittedAllocator.h"

// Rendering resources: 
#include "engine/resources/render/GpuMemoryMgr.h"

// DirectX ToolKit for math and some helper classes.
#include "engine/lib/dxtk.h"

// GPU profiling helpers.
#include "engine/renderer/GpuProfiling.h"

// Ray tracing features helper.
#include "engine/renderer/RayTracingProvider.h"

/// Namespace containing the engine code.
namespace quark
{

/// Rendering classes.
namespace rndr
{

/// Main interface for the rendering sub-system.
class RenderSubSystem : public util::PointerType<RenderSubSystem>
{
public:
    /**
     * Initialize renderer and default rendering pipeline.
     * @param cfg Runtime configuration.
     * @param window Window which will be used as output for the renderer.
     */
    static PtrT create(const EngineRuntimeConfig &cfg, const res::win::Window &window);

    /// Flush all command queues, free resources and quit.
    ~RenderSubSystem();

    // No copying or moving.
    RenderSubSystem(const RenderSubSystem &other) = delete;
    RenderSubSystem &operator=(const RenderSubSystem &other) = delete;
    RenderSubSystem(RenderSubSystem &&other) = delete;
    RenderSubSystem &operator=(RenderSubSystem &&other) = delete;

    /**
     * Resize the rendering swap chain to given size.
     * @param width New width in pixels.
     * @param height New height in pixels.
     * @warning The values will be clamped to 1, on 
     * the lower end.
     */
    void resize(uint32_t width, uint32_t height);

    /**
     * Flush direct, copy and compute command queues.
     */
    void flush();

    /**
     * Flush direct, bundle, copy and compute command queues.
     */
    void flushAll();

    /**
     * Flush all command queues and free copy buffers.
     */
    void flushCopyCommands();

    /// Access the runtime configuration.
    const EngineRuntimeConfig &cfg() const
    { return mCfg; }

    /// Access the D3D12 debugging interface.
    const helpers::d3d12::D3D12DebugConfigurator &debug() const
    { return mDebugConfigurator; }

    /// Access the D3D12 adapter.
    res::d3d12::D3D12Adapter &adapter()
    { return *mAdapter; }

    /// Access the D3D12 device.
    res::d3d12::D3D12Device &device()
    { return *mDevice; }

    /// Access the main D3D12 swap chain.
    res::d3d12::D3D12SwapChain &swapChain()
    { return *mSwapChain; }

    /// Access the D3D12 command queue which allows direct commands.
    res::d3d12::D3D12CommandQueueMgr &directCmdQueue()
    { return *mDirectCommandQueue; }

    /// Access the D3D12 command queue which allows creation of command bundles.
    res::d3d12::D3D12CommandQueueMgr &bundleCmdQueue()
    { return *mBundleCommandQueue; }

    /// Access the D3D12 command queue which allows compute commands.
    res::d3d12::D3D12CommandQueueMgr &computeCmdQueue()
    { return *mComputeCommandQueue; }

    /// Access the D3D12 command queue which allows copy commands.
    res::d3d12::D3D12CommandQueueMgr &copyCmdQueue()
    { return *mCopyCommandQueue; }

    /// Access the GPU memory manager.
    res::rndr::GpuMemoryMgr &memoryMgr()
    { return *mMemoryMgr; }

    /// Access the gpu profiling helper.
    prof::GpuProfilingAdapter &gpuProfiling()
    { return *mGpuProfilingAdapter; }

    /// Access ray tracing functionality.
    RayTracingProvider &rayTracing()
    { return *mRayTracingProvider; }
private:
    /// Compile-time configuration.
    using Config = EngineConfig;

    /// Reference to the current runtime configuration.
    const EngineRuntimeConfig &mCfg;

    /// Automatic live object reporting on destruction.
    helpers::d3d12::D3D12LiveObjectReporter mLiveObjectReporter;
    /// Automatic configuration of debug mode.
    helpers::d3d12::D3D12DebugConfigurator mDebugConfigurator;
    /// Factory used for Direct3D 12 object construction.
    res::d3d12::D3D12DXGIFactory::PtrT mFactory;
    /// Main rendering adapter.
    res::d3d12::D3D12Adapter::PtrT mAdapter;
    /// Main rendering device.
    res::d3d12::D3D12Device::PtrT mDevice;
    /// Command queue used for direct commands.
    res::d3d12::D3D12CommandQueueMgr::PtrT mDirectCommandQueue;
    /// Command queue used for creation of command bundles.
    res::d3d12::D3D12CommandQueueMgr::PtrT mBundleCommandQueue;
    /// Command queue used for compute commands.
    res::d3d12::D3D12CommandQueueMgr::PtrT mComputeCommandQueue;
    /// Command queue used for copy commands.
    res::d3d12::D3D12CommandQueueMgr::PtrT mCopyCommandQueue;
    /// Main rendering swap chain.
    res::d3d12::D3D12SwapChain::PtrT mSwapChain;

    /// Management of resources on the GPU.
    res::rndr::GpuMemoryMgr::PtrT mMemoryMgr;

    /// GPU profiling adapter for the global profiling system.
    prof::GpuProfilingAdapter::PtrT mGpuProfilingAdapter{ };

    /// Helper for ray tracing features.
    RayTracingProvider::PtrT mRayTracingProvider{ };
protected:
    /**
     * Initialize renderer and default rendering pipeline.
     * @param cfg Runtime configuration.
     * @param window Window which will be used as output for the renderer.
     */
    RenderSubSystem(const EngineRuntimeConfig &cfg, const res::win::Window &window);
}; // class RenderSubSystem

} // namespace rndr

} // namespace quark
