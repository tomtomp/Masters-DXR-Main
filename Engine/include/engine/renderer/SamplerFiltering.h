/**
 * @file renderer/SamplerFiltering.h
 * @author Tomas Polasek
 * @brief Enumeration of modes of sampler filtering.
 */

#pragma once

#include "engine/util/Types.h"

/// Namespace containing the engine code.
namespace quark
{

/// Rendering classes.
namespace rndr
{

/// Filtering rules used by samplers.
enum class SampleFilter
{
    /// Take the value of the nearest texel.
    Nearest, 
    /// Use linear interpolation between nearest texels.
    Linear
}; // enum class SampleFilter

/**
 * Triplet of filtering rules for minification, magnification 
 * and mip-maps.
 */
struct MinMagMipFilter
{
    /// Filtering rule used when far away.
    SampleFilter minFilter{ SampleFilter::Nearest };
    /// Filtering rule used when close up.
    SampleFilter magFilter{ SampleFilter::Nearest };
    /// Filtering rule used between mip-maps.
    SampleFilter mipFilter{ SampleFilter::Nearest };
}; // struct MinMagMipFilter

/// Filtering rules used when sampling out of range.
enum class SampleEdge
{
    /// Clamp to the edge value.
    Clamped, 
    /// Mirror values out of range.
    Mirrored, 
    /// Repeat value out of range.
    Repeat
}; // enum class SampleEdge

/// Specification of filtering rules when out of bounds.
struct EdgeFilterUVW
{
    // How to sample textures indexed out of range in all 3 dimensions.
    SampleEdge edgeFilterU{ rndr::SampleEdge::Clamped };
    SampleEdge edgeFilterV{ rndr::SampleEdge::Clamped };
    SampleEdge edgeFilterW{ rndr::SampleEdge::Clamped };
}; // struct EdgeFilterUVW

} // namespace rndr

} // namespace quark
