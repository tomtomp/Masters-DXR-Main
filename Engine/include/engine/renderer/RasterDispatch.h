/**
 * @file renderer/RasterDispatch.h
 * @author Tomas Polasek
 * @brief Abstraction of rasterization pipeline which 
 * contains all necessary state to draw to output frame 
 * buffers.
 */

#pragma once

#include "engine/renderer/RenderContext.h"

/// Namespace containing the engine code.
namespace quark
{

/// Rendering classes.
namespace rndr
{

// Forward declaration.
class RenderContext;

/**
 * Abstraction of rasterization pipeline which 
 * contains all necessary state to draw to output frame 
 * buffers.
 */
class RasterDispatch : public util::PointerType<RasterDispatch> 
{
public:
    /// Free any resources used by this dispatch.
    virtual ~RasterDispatch();

    // No copying or moving
    RasterDispatch(const RasterDispatch &other) = delete;
    RasterDispatch &operator=(const RasterDispatch &other) = delete;
    RasterDispatch(RasterDispatch &&other) = delete;
    RasterDispatch &operator=(RasterDispatch &&other) = delete;
private:
protected:
    /// Container for rasterization pipeline configuration.
    struct RasterConfig
    {
        
    }; // struct RasterConfig

    /**
     * Create a new default configured rasterization dispatch 
     * using the provided rendering context.
     * @param ctx Current rendering context.
     */
    RasterDispatch(RenderContext::PtrT ctx);

    /// Currently used rendering context.
    RenderContext::PtrT mRenderCtx;
}; // class RasterDispatch

} // namespace rndr

} // namespace quark
