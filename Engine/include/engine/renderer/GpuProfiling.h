/**
 * @file renderer/GpuProfiling.h
 * @author Tomas Polasek
 * @brief GPU profiling macros and management.
 */

#pragma once

#include "engine/renderer/GpuProfilingAdapter.h"

// Profiling macros:
#ifndef NPROFILE
/// Start GPU profiling for a code block.
#   define GPU_PROF_BLOCK(CMD_LIST, NAME) \
        auto PROF_NAME(_gbp, __LINE__){ ::quark::rndr::prof::impl::createBlockProfiler((CMD_LIST), (NAME), true) }
/// End last GPU profiling block for given command list and name.
#   define GPU_PROF_BLOCK_END(CMD_LIST) \
        ::quark::rndr::prof::impl::endLastBlockProfiler((CMD_LIST))
/// Start GPU profiling for a code scope. Automatically ends on end of scope.
#   define GPU_PROF_SCOPE(CMD_LIST, NAME) \
        const auto PROF_NAME(_gsp, __LINE__){ ::quark::rndr::prof::impl::createScopeProfiler((CMD_LIST), (NAME)) }
/// Run before finalizing, but after all profiling commands.
#   define GPU_PROF_RESOLVE(CMD_LIST) \
        ::quark::rndr::prof::impl::resolveProfiler((CMD_LIST))
/// Finalize the GPU profiling, should be ran only after all command lists are done!
#   define GPU_PROF_FINALIZE() \
        ::quark::rndr::prof::impl::finalizeProfiler()
#else
/// Start GPU profiling for a code block.
#   define GPU_PROF_BLOCK(_, __)
/// End last GPU profiling block for given command list and name.
#   define GPU_PROF_BLOCK_END(_)
/// Start GPU profiling for a code scope. Automatically ends on end of scope.
#   define GPU_PROF_SCOPE(_, __)
/// Run before finalizing, but after all profiling commands.
#   define GPU_PROF_RESOLVE(_)
/// Finalize the GPU profiling, should be ran only after all command lists are done!
#   define GPU_PROF_FINALIZE()
#endif

/// Namespace containing the engine code.
namespace quark
{

/// Rendering classes.
namespace rndr
{

/// GPU profiling.
namespace prof
{

/// Rendering implementation details.
namespace impl
{

/**
 * Set the hidden global profiling adapter to provided 
 * value.
 * This adapter will then be automatically used by all 
 * GPU profiling macros until changed.
 * @param adapter The adapter instance.
 */
void setGlobalProfilingAdapter(GpuProfilingAdapter *adapter);

/**
 * Create a GPU code block profiler with given parameters.
 * Uses the global profiling adapter.
 * @param cmdList Which command list should be profiled.
 * @param name Name of the scope.
 * @param manualEnd When set to true, the destructor 
 * will never call end, even when never used.
 */
GpuBlockProfiler createBlockProfiler(res::d3d12::D3D12CommandList &cmdList, 
    const char *name, bool manualEnd = false);

/**
 * End the last used GPU profiler.
 * Uses the global profiling adapter.
 * @param cmdList Which command list should be profiled.
 * @warning Command list must be the same as the one used 
 * in the last block profiler!
 */
void endLastBlockProfiler(res::d3d12::D3D12CommandList &cmdList);

/**
 * Create a GPU scope profiler with given parameters.
 * Uses the global profiling adapter.
 * @param cmdList Which command list should be profiled.
 * @param name Name of the scope.
 */
GpuScopeProfiler createScopeProfiler(res::d3d12::D3D12CommandList &cmdList, const char *name);

/**
 * Resolve the timers, should be ran before finalizing the 
 * profiler.
 * @param cmdList Command list used for resolving.
 */
void resolveProfiler(res::d3d12::D3D12CommandList &cmdList);

/**
 * Finalize the current run of GPU profiling. Should be 
 * ran only after all of the command lists are finished!
 */
void finalizeProfiler();

} // namespace impl

} // namespace prof

} // namespace rndr

} // namespace quark
