/**
 * @file renderer/ShaderVariables.h
 * @author Tomas Polasek
 * @brief Container used for specification of 
 * shader variables and their values.
 */

#pragma once

#include "engine/renderer/RenderContext.h"
#include "engine/renderer/SamplerFiltering.h"

/// Namespace containing the engine code.
namespace quark
{

/// Rendering classes.
namespace rndr
{

// Forward declaration.
class RenderContext;

/// Types of shader variables.
enum class ShaderVariableType
{
    /// This variable is unused.
    None, 
    /// Variable contains constant data.
    Constant, 
    /// Variable is some kind of shader resource.
    Resource, 
    /// Variable is used in unordered access.
    Unordered, 
    /// Variable contains sampler configuration.
    Sampler
}; // enum class ShaderVariableType

/// Hidden implementation classes.
namespace impl
{

class ShaderVariableBase
{
public:
    /// Initialize type of the shader variable.
    ShaderVariableBase(ShaderVariableType type);

    /// Free resources in specializations.
    virtual ~ShaderVariableBase();

    /// Get type of this shader variable.
    ShaderVariableType type() const;
private:
protected:
    /// Type of this shader variable.
    ShaderVariableType mType;
}; // class ShaderVariableBas

// TODO - Change ShaderVariable* to abstract bases, implemented in platform.
/**
 * Base class for shader variables containing constant values.
 */
class ShaderVariableConstant : public ShaderVariableBase, public util::PointerType<ShaderVariableConstant>
{ 
public:
    /// Initialize empty constant, which will not be sent to the GPU.
    static PtrT create();

    /// Create a constant from provided data.
    static PtrT create(const uint8_t *data, std::size_t sizeInBytes);

    /// Free the constant.
    virtual ~ShaderVariableConstant();

    // Allow copying and moving.
    ShaderVariableConstant(const ShaderVariableConstant &other) = default;
    ShaderVariableConstant &operator=(const ShaderVariableConstant &other) = default;
    ShaderVariableConstant(ShaderVariableConstant &&other) = default;
    ShaderVariableConstant &operator=(ShaderVariableConstant &&other) = default;

    /// Update the currently held data with provided bytes.
    void updateData(const uint8_t *data, std::size_t sizeInBytes);

    /// Access the internal data holder.
    const auto &data() const
    { return mData; }
private:
protected:
    /// Initialize empty constant, which will not be sent to the GPU.
    ShaderVariableConstant();

    /// Create a constant from provided data.
    ShaderVariableConstant(const uint8_t *data, std::size_t sizeInBytes);

    /// Holder of the data.
    std::vector<uint8_t> mData{ };
}; // class ShaderVariableConstant

// TODO - Complete...
class ShaderVariableResource
{ };
class ShaderVariableUnordered
{ };

/**
 * Base class for shader variables containing static samplers.
 */
class ShaderVariableSampler : public ShaderVariableBase, public util::PointerType<ShaderVariableSampler>
{ 
public:
    /// Initialize sampler with default parameters.
    static PtrT create();

    /// Create a sampler with provided parameters.
    static PtrT create(const rndr::MinMagMipFilter &filtering, const rndr::EdgeFilterUVW &edgeFiltering = { });

    /// Free all resources.
    virtual ~ShaderVariableSampler();

    // Allow copying and moving.
    ShaderVariableSampler(const ShaderVariableSampler &other) = default;
    ShaderVariableSampler &operator=(const ShaderVariableSampler &other) = default;
    ShaderVariableSampler(ShaderVariableSampler &&other) = default;
    ShaderVariableSampler &operator=(ShaderVariableSampler &&other) = default;

    /// Set new parameters for this sampler.
    void setParameters(const rndr::MinMagMipFilter &filtering, const rndr::EdgeFilterUVW &edgeFiltering = { });
private:
protected:
    /// Initialize sampler with default parameters.
    ShaderVariableSampler();

    /// Create a sampler with provided parameters.
    ShaderVariableSampler(const rndr::MinMagMipFilter &filtering, const rndr::EdgeFilterUVW &edgeFiltering = { });

    /// How does the sampler interpolate between texels.
    rndr::MinMagMipFilter mFiltering{ };
    /// How does the sampler sample on edges.
    rndr::EdgeFilterUVW mEdgeFiltering{ };
}; // class ShaderVariableSampler

}

/**
 * Representation of a single variable in the shader.
 */
class ShaderVariable
{
public:
    /// Create a null type variable with no value.
    ShaderVariable();

    /// Free any internal variable data.
    ~ShaderVariable();

    // Allow copying and moving.
    ShaderVariable(const ShaderVariable &other) = default;
    ShaderVariable &operator=(const ShaderVariable &other) = default;
    ShaderVariable(ShaderVariable &&other) = default;
    ShaderVariable &operator=(ShaderVariable &&other) = default;

    /**
     * Set the location of this shader variable within the 
     * variable space of the target shader.
     * @param varRegister Register within the shader, where the 
     * variable starts.
     * @param varSpace Register space used by the variable.
     */
    void setLocation(uint32_t varRegister, uint32_t varSpace);

    /**
     * Set this variable to contain a static sampler.
     * @param filtering How should the sampler interpolate between 
     * texels.
     * @param edgeFiltering How should sampler filter on edges.
     */
    void setSampler(const rndr::MinMagMipFilter &filtering, 
        const rndr::EdgeFilterUVW &edgeFiltering = { });

    /**
     * Set this variable to contain constant data.
     * @param data Pointer to the start of the data.
     * @param sizeInBytes Size of the data in bytes.
     */
    void setConstant(const uint8_t *data, std::size_t sizeInBytes);

    //TODO - Assignment for samplers, resources and unordered?
    /**
     * Assign provided value to the shader variable.
     * This version allows setting of any data, the shader will 
     * then have access to the values binary representation as 
     * it is on the CPU.
     * @tparam ValT Type of the value copied over.
     * @param value Value to copy over.
     */
    template <typename ValT>
    ShaderVariable &operator=(const ValT &value);

    /**
     * Assign a vector of values to the shader variable.
     * This version allows to set a whole vector of data, alignment 
     * will be the same as the C++ type.
     * @tparam ValT Type of a single element.
     * @param valueVector Vector of values, copied to the variable.
     */
    template <typename ValT>
    ShaderVariable &operator=(const std::vector<ValT> &valueVector);

    /// What type of variable is this?
    ShaderVariableType type() const;

    /**
     * Access the constant data holder of this variable.
     * If this variable does not contain Constant data, this 
     * method returns nullptr.
     * @return Returns pointer to the constant data holder or 
     * nullptr if this variable does not hold constant data.
     */
    const impl::ShaderVariableConstant *constantData() const;

    /**
     * Access the sampler data holder of this variable.
     * If this variable does not contain Sampler data, this 
     * method returns nullptr.
     * @return Returns pointer to the sampler data holder or 
     * nullptr if this variable does not hold sampler data.
     */
    const impl::ShaderVariableSampler *samplerData() const;
private:
    /// Allow access to the dirty flag and shader registers.
    friend class ShaderVariables;

    /**
     * Access shader variable as a given type.
     * @warning Performs no checking.
     */
    template <typename TargetT>
    static TargetT *asDataPointer(const util::PtrT<impl::ShaderVariableBase> &var);

    /// Is this shader variable dirty?
    bool isDirty() const;
    
    /// Set dirty flag of this shader variable.
    void setDirty(bool dirty);

    /// Index where this variable starts at.
    uint32_t mShaderRegister{ 0u };
    /// Register space used by this variable.
    uint32_t mShaderSpace{ 0u };

    /// Holder of the variable data.
    util::PtrT<impl::ShaderVariableBase> mVariable{ nullptr };
    /// Has the variable changed its type or value?
    bool mVariableDirty{ true };
protected:
}; // class ShaderVariable

// TODO - Shader reflection, automatic detection of variables, etc.
/**
 * Container used for specification of 
 * shader variables and their values.
 */
class ShaderVariables : public util::PointerType<ShaderVariables> 
{
public:
    /**
     * Create empty set of shader variables.
     */
    static PtrT create();

    /// Free any resources used by this dispatch.
    virtual ~ShaderVariables();

    // No copying or moving
    ShaderVariables(const ShaderVariables &other) = delete;
    ShaderVariables &operator=(const ShaderVariables &other) = delete;
    ShaderVariables(ShaderVariables &&other) = delete;
    ShaderVariables &operator=(ShaderVariables &&other) = delete;

    /**
     * Access a shader variable by its name.
     * @param varName Name of the variable.
     * @return Returns reference to the requested variable.
     */
    ShaderVariable &operator[](const std::string &varName);
private:
    /// Allow deeper access to the shader variables.
    friend class RasterDispatch;

    /// Mapping from variable names to their data.
    std::map<std::string, ShaderVariable> mVariables;
protected:
    /**
     * Create empty set of shader variables.
     */
    ShaderVariables();

    /**
     * Is given shader variable dirty? This can mean it has 
     * changed its internal type or value.
     * @param var Variable to check.
     * @return Returns true if the variable is dirty.
     */
    static bool isVariableDirty(const ShaderVariable &var);

    /**
     * Set dirty state of given shader variable.
     * @param var Variable to set the dirty status of.
     * @param dirty Value to set the dirty flag to.
     */
    static void setVariableDirty(ShaderVariable &var, bool dirty = true);
}; // class ShaderVariables

} // namespace rndr

} // namespace quark

// Template implementation begin.

namespace quark
{

namespace rndr
{

template<typename ValT>
ShaderVariable &ShaderVariable::operator=(const ValT &value)
{
    setConstant(&value, sizeof(value));
    return *this;
}

template<typename ValT>
inline ShaderVariable &ShaderVariable::operator=(const std::vector<ValT> &valueVector)
{
    setConstant(valueVector.data(), valueVector.size() * sizeof(ValT));
    return *this;
}

template<typename TargetT>
TargetT *ShaderVariable::asDataPointer(const util::PtrT<impl::ShaderVariableBase> &var)
{ return dynamic_cast<TargetT*>(var.get()); }
// TODO - Change dynamic_cast to type() check and a static/reinterpret cast?

}

}

// Template implementation end.
