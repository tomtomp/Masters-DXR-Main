/**
 * @file renderer/RenderPass.h
 * @author Tomas Polasek
 * @brief Base class for render passes, which can be 
 * added to the RenderPipeline.
 */

#pragma once

#include "engine/renderer/RenderResourceMgr.h"
#include "engine/renderer/RenderScene.h"
#include "engine/renderer/RenderContext.h"

/// Namespace containing the engine code.
namespace quark
{

/// Rendering classes.
namespace rndr
{

/**
 * Base class for render passes, which can be 
 * added to the RenderPipeline.
 */
class RenderPass
{
public:
    using PtrT = std::shared_ptr<RenderPass>;
    using ConstPtrT = std::shared_ptr<const RenderPass>;

    /// Free any resources used by the pass.
    virtual ~RenderPass();

    // No copying or moving.
    RenderPass(const RenderPass &other) = delete;
    RenderPass &operator=(const RenderPass &other) = delete;
    RenderPass(RenderPass &&other) = delete;
    RenderPass &operator=(RenderPass &&other) = delete;

    /// Has this render pass been initialized?
    bool initialized() const;
    
    /// Get the name of this render pass.
    const std::string &name() const;
private:
    // Allow calling of methods.
    friend class RenderPipeline;

    /**
     * Wrapper calling the virtual initialize method.
     * @return Returns true if the initialization finished successfully.
     */
    bool initializeWrapper(RenderContext::PtrT ctx, RenderResourceMgr::PtrT resMgr);

    /**
     * Wrapper calling the virtual sceneChanged method.
     */
    void sceneChangedWrapper(RenderScene::PtrT scene);

    /**
     * Wrapper calling the execute method.
     */
    void executeWrapper(RenderContext::PtrT ctx);

    /**
     * Wrapper calling the virtual deinitialize method.
     */
    void deinitializeWrapper();

    /// Has this pass been initialized?
    bool mInitialized{ false };
    /// Name of this pass.
    std::string mName{ };
protected:
    // Allow calling of virtual methods.
    friend class RenderPipeline;

    /**
     * Create the pass and initialize its meta 
     * information.
     * @param name Name of the pass.
     */
    RenderPass(const std::string &name = "Unknown Pass");

    /**
     * Initialization of the rendering pass.
     * This function is called once per pass, before any further use 
     * of the pass. After a call to de-initialize, this method may be 
     * called again.
     * @param ctx Rendering context containing current command lists. The 
     * context is valid only for this call!
     * @param resMgr Manager of resources shared between passes. This 
     * should generally be used only for resources which are shared, or 
     * created with unique IDs so that there is no collision.
     * @return Returns true if the initialization finished successfully.
     */
    virtual bool initialize(RenderContext::PtrT &ctx, RenderResourceMgr::PtrT &resMgr) = 0;

    /**
     * Called every time a target rendering scene changes.
     * Changes within the scene do not necessitate a call!
     * @param scene The new scene, the pointer can be kept by 
     * the rendering pass until the next call to sceneChange.
     */
    virtual void sceneChanged(RenderScene::PtrT scene) = 0;

    /**
     * Execute the rendering pass, recording commands on provided command
     * lists.
     * @param ctx Rendering context containing current command lists. The 
     * context is valid only for this call!
     */
    virtual void execute(RenderContext::PtrT ctx) = 0;

    /**
     * De-initialize the rendering pass, allowing its re-initialization.
     * This method is always called before final call to the destructor.
     */
    virtual void deinitialize() = 0;
}; // class RenderPass

} // namespace rndr

} // namespace quark
