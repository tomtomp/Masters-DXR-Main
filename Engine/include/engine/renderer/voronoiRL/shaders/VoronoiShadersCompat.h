/**
 * @file renderer/voronoiRL/shaders/VoronoiShadersCompat.h
 * @author Tomas Polasek
 * @brief Data structures shared between voronoi shaders.
 */

#ifndef ENGINE_VORONOI_RL_SHADERS_COMPAT_H
#define ENGINE_VORONOI_RL_SHADERS_COMPAT_H

#ifdef HLSL
#   include "HlslCompatibility.h"
#else
#   include "engine/helpers/hlsl/HlslCompatibility.h"
#endif

/// Buffer containing global static data, which don't change very often.
struct StaticConstantBuffer
{
    /// Number of cells in the buffer.
    HLSL_TYPE(uint) cellCount;

    HLSL_TYPE(uint3) padding1;

    /// Direction of the global light.
    HLSL_TYPE(float3) lightDirection;
    /// Color of the global light.
    HLSL_TYPE(float3) lightColor;
    /// Color of the sky.
    HLSL_TYPE(float3) backgroundColor;

    // TODO - Fix this nicely?
#ifdef HLSL
    float padding2;
#endif // HLSL

    /// Whether to do reflections/refractions.
    HLSL_TYPE(uint) doReflections;
    /// Render shadows?
    HLSL_TYPE(uint) enableShadows;
    /// Size of the Percentage-Closer-Filtering kernel.
    HLSL_TYPE(uint) shadowPcfKernelSize;
    /// Render ambient occlusion?
    HLSL_TYPE(uint) enableAo;
}; // struct StaticConstantBuffer

/// Buffer containing global data which may change per frame.
struct DynamicConstantBuffer
{
    /// Transform from world-space to screen-space (vp).
    HLSL_TYPE(float4x4) worldToCamera;
    /// Transform from screen-space to world-space.
    HLSL_TYPE(float4x4) cameraToWorld;
    /// Transform from world-space to light-space.
    HLSL_TYPE(float4x4) worldToLight;
    /// Position of the camera in world coordinates.
    HLSL_TYPE(float3) cameraWorldPosition;

    // TODO - Fix this nicely?
#ifdef HLSL
    float padding2;
#endif // HLSL

    /// Current time, which increases between frames.
    float time;
}; // struct DynamicConstantBuffer

/// Structure representing a single cell center.
struct CellData
{
    /// Position of the center.
    HLSL_TYPE(float4) position;
}; // struct CellData

/// Maximum number of Voronoi cells.
#define MAX_VORONOI_CELLS 4095

/// Structure representing array of cells.
struct CellDataBuffer
{
    /// Array of currently displayed Voronoi cells.
    CellData cells[MAX_VORONOI_CELLS];
}; // struct CellDataBuffer

#ifndef HLSL

/// Helper structure used for passing HLSL register-space pairs.
struct HlslRegisterSpacePair
{
	uint32_t reg{ 0u };
	uint32_t space{ 0u };
}; // struct HlslRegisterSpacePair

/// Slow-changing constants buffer.
static constexpr HlslRegisterSpacePair StaticConstantsSlot{ 0u, 0u };
/// Fast-changing constants buffer.
static constexpr HlslRegisterSpacePair DynamicConstantsSlot{ 1u, 0u };
/// Cell data buffer.
static constexpr HlslRegisterSpacePair CellDataSlot{ 2u, 0u };
/// Common sampler for material textures.
static constexpr HlslRegisterSpacePair SamplerSlot{ 0u, 0u };
/// Shadow sampler for shadow maps.
static constexpr HlslRegisterSpacePair ShadowSamplerSlot{ 1u, 0u };
/// Array of deferred G-Buffer buffers.
static constexpr HlslRegisterSpacePair DeferredBuffersSlot{ 0u, 1u };
/// Array of shadow maps.
static constexpr HlslRegisterSpacePair ShadowMapBuffersSlot{ 0u, 2u };
/// Ambient occlusion texture.
static constexpr HlslRegisterSpacePair AmbientOcclusionSlot{ 0u, 3u };

#else

/// Slow-changing constants.
ConstantBuffer<StaticConstantBuffer> gStatic : register(b0, space0);
/// Fast-changing constants.
ConstantBuffer<DynamicConstantBuffer> gDynamic : register(b1, space0);
/// Cell data.
ConstantBuffer<CellDataBuffer> gCellData : register(b2, space0);

/// Common sampler used for sampling the textures.
SamplerState gCommonSampler : register(s0, space0);
/// Sampler used for sampling the shadow maps.
SamplerComparisonState gShadowSampler : register(s1, space0);

/// Get the deferred helpers.
#define DEFERRED_BUFFERS_SPACE space1
#include "hlslDeferredUser.h"

/// Array of shadow maps.
Texture2D<float> gShadowMaps[HLSL_SHADOW_MAP_UPPER_BOUND] : register(t0, space2);

/// Ambient occlusion texture.
Texture2D<float> gAmbientOcclusion : register(t0, space3);

/**
 * Structure used for passing shading information.
 */
struct ShadingInformation
{
    /// World origin of the ray.
    float3 rayOrigin;
    /// World direction of the ray.
    float3 rayDirection;

    /// Ambient occlusion of the fragment, 1.0f means fully occluded.
    float ambientOcclusion;
    /// Shadow occlusion of the fragment, 1.0f means fully occluded.
    float shadowOcclusion;

    /// Color seen in the reflection.
    float4 reflectionColor;

    /// Direction of the light.
    float3 lightDirection;
    /// Color of the light.
    float3 lightColor;
    /// Color of the sky.
    float3 backgroundColor;
}; // struct ShadingInformation

/// Data layout for input into the vertex shader.
struct VertexData
{
    /// Model-space position.
    float3 position : POSITION;
    /// Vertex identifier used as cell index.
    uint id : SV_VERTEXID;
}; // struct VertexData

/// Data layout for input into the Geometry shader.
struct GeometryData
{
    /// Model-space position.
    float3 position : POSITION;
    /// Index of the currently processed cell.
    uint cellIndex : TEXCOORD0;
}; // struct GeometryData

/// Data layout for input into the pixel shader.
struct FragmentData
{
    /// Screen-space position.
    float4 ssPosition : SV_Position;
    /// World-space position.
    float3 wsPosition : POSITIONT;
    /// World-space normal.
    float3 wsNormal : NORMAL;
    /// Texture coordinate.
    float2 texCoord : TEXCOORD0;

    /// Color of the vertex.
    float3 color : COLOR;
}; // struct FragmentData

/// Data layout for output out of the pixel shader.
struct PixelData
{
    /// Output color.
    float4 color : SV_Target0;
}; // struct PixelData

/// Data layout for input into the vertex shader.
struct VertexData2
{
    /// Vertex identifier used for generation of fullscreen triangle.
    uint id : SV_VERTEXID;
}; // struct VertexData2

/// Data layout for input into the pixel shader.
struct FragmentData2
{
    /// Screen-space position.
    float4 ssPosition : SV_Position;
    /// Texture coordinate.
    float2 texCoord : TEXCOORD0;
}; // struct FragmentData2

/// Data layout for output out of the pixel shader.
struct PixelData2
{
    /// Output color.
    float4 color : SV_Target0;
}; // struct PixelData2

/**
 * Calculate light-space UVs for given world space position.
 * @param wsPosition Position to calculate the light-space position for.
 * @param worldToLight World to light space matrix.
 * @return Returns position in the light-space.
 */
float3 calcLightSpacePosition(in float3 wsPosition, in float4x4 worldToLight)
{
    // Calculate light-space position.
    float4 lsPosition = mul(worldToLight, float4(wsPosition, 1.0f));
    lsPosition.xyz /= lsPosition.w;

    return lsPosition.xyz;
}

/**
 * Sample provided shadow map and compare the calculated depth from 
 * the light.
 * @param shadowMap Texture containing the depth values.
 * @param shadowBias Bias value, when comparing.
 * @param lsUvDepth UV in the shadow map + calculated depth.
 * @return Returns shadow factor {0.0, 1.0}, where 1.0 means 
 * it is in shade and 0.0 is lit.
 */
float calcSampleShadowed(in Texture2D<float> shadowMap, in float shadowBias, in float3 lsUvDepth)
{
    const float calculatedDepth = lsUvDepth.z;

    /*
    const float shadowMapDepth = shadowMap.Sample(gShadowSampler, lsUvDepth.xy);

    //const float shadowMapDepth = shadowMap.Sample(gCommonSampler, lsUvDepth.xy);

    return (shadowMapDepth + shadowBias) < calculatedDepth ? 1.0f : 0.0f;
    */

    return shadowMap.SampleCmpLevelZero(gShadowSampler, lsUvDepth.xy, calculatedDepth - shadowBias);
}

/**
 * Calculate shadow from given position for directional lights.
 * Uses shadow maps.
 * @param lightIndex Index of the light which should be filled in.
 * @param wsPosition Position of the point in world-space.
 * @return Returns shadow factor {0.0, 1.0}, where 1.0 means 
 * it is in shade and 0.0 is lit.
 */
float calcDirectionalShadow(in uint lightIndex, in float shadowBias, 
    in float3 wsPosition)
{
    const float3 lsPosition = calcLightSpacePosition(wsPosition, gDynamic.worldToLight);
    // <-1.0f, 1.0f> -> <0.0f, 1.0f>, flip y-axis.
    const float3 lsUvDepth = float3((lsPosition.xy + float2(1.0f, -1.0f)) * float2(0.5f, -0.5f), lsPosition.z);

    return calcSampleShadowed(gShadowMaps[lightIndex], shadowBias, lsUvDepth);
}

/**
 * Calculate shadow from given position for directional lights.
 * This version samples multiple values around the original value 
 * in order to create fake soft shadows.
 * Uses shadow maps.
 * @param lightIndex Index of the light which should be filled in.
 * @param wsPosition Position of the point in world-space.
 * @return Returns shadow factor {0.0, 1.0}, where 1.0 means 
 * it is in shade and 0.0 is lit.
 */
float calcDirectionalPcfShadow(in uint lightIndex, in float shadowBias, 
    in float3 wsPosition)
{
    const float3 lsPosition = calcLightSpacePosition(wsPosition, gDynamic.worldToLight);
    // <-1.0f, 1.0f> -> <0.0f, 1.0f>, flip y-axis.
    const float3 lsUvDepth = float3((lsPosition.xy + float2(1.0f, -1.0f)) * float2(0.5f, -0.5f), lsPosition.z);

    const float2 texelSize = 1.0f / textureDimensions(gShadowMaps[lightIndex]);

    const float halfKernelSize = gStatic.shadowPcfKernelSize;

    const float baseShadow = calcSampleShadowed(gShadowMaps[lightIndex], shadowBias, lsUvDepth);
    const float baseDepth = gDepthTexture.Sample(gCommonSampler, lsUvDepth.xy);

    float shadowFactor = 0.0f;
    for (float y = -halfKernelSize; y <= halfKernelSize; y += 1.0f)
    {
        for (float x = -halfKernelSize; x <= halfKernelSize; x += 1.0f)
        {
            const float3 lsUvDepthOffset = float3(float2(x, y) * texelSize, 0.0f);
            const float3 lsUvDepthTotal = lsUvDepth + lsUvDepthOffset;

            const float currentDepth = gDepthTexture.Sample(gCommonSampler, lsUvDepthTotal.xy);
            const float depthDifference = abs(baseDepth - currentDepth);
            //const float distanceFactor = 1.0f - step(0.1f, depthDifference);
            //const float distanceFactor = depthDifference > 0.01f ? 0.0f : 1.0f;

            shadowFactor += depthDifference > 0.01f ? baseShadow : 
                calcSampleShadowed(
                gShadowMaps[lightIndex], 
                shadowBias, 
                lsUvDepthTotal);
        }
    }
    const float kernelSize = halfKernelSize + halfKernelSize + 1u;
    shadowFactor /= kernelSize * kernelSize;

    return shadowFactor;
}

/**
 * Fill light information for given light index, into the shading 
 * information structure.
 * @param enabled Should the value be recovered?
 * @param lightIndex Index of the light which should be filled in.
 * @param deferredInfo Filled structure with deferred information.
 * @param shadingInfo Prepared shading information structure, which 
 * is also used as output.
 */
void fillLightInformation(uniform bool enabled, in uint lightIndex, 
    in DeferredInformation deferredInfo, 
    inout ShadingInformation shadingInfo)
{
    // TODO - Use per-light information?
    shadingInfo.lightDirection = gStatic.lightDirection;
    shadingInfo.lightColor = gStatic.lightColor;
    shadingInfo.shadowOcclusion = 0.0f;

    if (enabled)
    {
        const float shadowBias = 0.006f;
        if (gStatic.shadowPcfKernelSize > 0u)
        { shadingInfo.shadowOcclusion = calcDirectionalPcfShadow(lightIndex, shadowBias, deferredInfo.wsPosition); }
        else
        { shadingInfo.shadowOcclusion = calcDirectionalShadow(lightIndex, shadowBias, deferredInfo.wsPosition); }
    }
}

/**
 * Fill ambient occlusion information into the shading information 
 * structure.
 * @param enabled Should the value be recovered?
 * @param deferredInfo Filled structure with deferred information.
 * @param shadingInfo Prepared shading information structure, which 
 * is also used as output.
 */
void fillAmbientOcclusion(uniform bool enabled, 
    in DeferredInformation deferredInfo, 
    inout ShadingInformation shadingInfo)
{
    if (!enabled)
    { return; }

    if (gStatic.shadowPcfKernelSize == 0u)
    {
        shadingInfo.ambientOcclusion = gAmbientOcclusion.Sample(gCommonSampler, deferredInfo.ssTexCoord);
        return;
    }

    const float2 texelSize = 1.0f / textureDimensions(gAmbientOcclusion);

    const float halfKernelSize = gStatic.shadowPcfKernelSize;

    float shadowFactor = 0.0f;
    for (float y = -halfKernelSize; y <= halfKernelSize; y += 1.0f)
    {
        for (float x = -halfKernelSize; x <= halfKernelSize; x += 1.0f)
        {
            // TODO - Add depth check for the filter?
            const float2 uvOffset = float2(x, y) * texelSize;
            const float2 uv = deferredInfo.ssTexCoord + uvOffset;
            shadowFactor += gAmbientOcclusion.Sample(gCommonSampler, uv);
        }
    }
    const float kernelSize = halfKernelSize + halfKernelSize + 1u;
    shadowFactor /= kernelSize * kernelSize;

    shadingInfo.ambientOcclusion = shadowFactor;
}

/**
 * Prepare shading information for the current fragment.
 * @param deferredInfo Filled structure with deferred information.
 * @return Returns filled shading information structure.
 */
ShadingInformation prepareShadingInformation(in DeferredInformation deferredInfo)
{
    ShadingInformation result;

    result.rayOrigin = 0.0f;
    result.rayDirection = 0.0f;

    result.ambientOcclusion = 0.0f;
    result.shadowOcclusion = 0.0f;

    result.reflectionColor = 0.0f;

    result.lightDirection = 0.0f;
    result.lightColor = 0.0f;
    result.backgroundColor = gStatic.backgroundColor;

    float3 rayOrigin = 0.0f;
    float3 rayDirection = 0.0f;

    generateCameraRay(deferredInfo.ssTexCoord, 
        gDynamic.cameraToWorld, gDynamic.cameraWorldPosition, 
        rayOrigin, rayDirection);

    result.rayOrigin = rayOrigin;
    result.rayDirection = rayDirection;

    return result;
}

/**
 * Fill screen-space reflections into the shading information.
 * @param enabled Should the reflection be calculated?
 * @param deferredInfo Filled structure with deferred information.
 * @param shadingInfo Prepared shading information structure, which 
 * is also used as output.
 */
void fillReflection(uniform bool enabled, in DeferredInformation deferredInfo, 
    inout ShadingInformation shadingInfo)
{
    if (!enabled)
    { return; }

    float4 reflectionColor = 0.0f;

    // TODO - Calculate...

    shadingInfo.reflectionColor = reflectionColor;
}

#endif

#endif // ENGINE_VORONOI_RL_SHADERS_COMPAT_H
