/**
 * @file renderer/voronoiRL/VoronoiRL.h
 * @author Tomas Polasek
 * @brief Render layer used for generation of 3D 
 * Voronoi diagrams.
 */

#pragma once

#include "engine/renderer/RenderLayerBase.h"

#include "engine/renderer/RenderSubSystem.h"

#include "engine/renderer/deferredRL/DeferredRL.h"
#include "engine/renderer/shadowMapRL/ShadowMapRL.h"
#include "engine/renderer/ambientOcclusionRL/AmbientOcclusionRL.h"

#include "engine/resources/d3d12/D3D12Shader.h"
#include "engine/resources/d3d12/D3D12RootSignature.h"
#include "engine/resources/d3d12/D3D12InputLayout.h"
#include "engine/resources/d3d12/D3D12PipelineState.h"

#include "engine/resources/render/DepthStencilBuffer.h"
#include "engine/resources/render/DescriptorMgr.h"

#include "engine/resources/scene/DrawableListMgr.h"

#include "engine/helpers/d3d12/D3D12RootSignatureBuilder.h"
#include "engine/helpers/d3d12/D3D12PipelineStateBuilder.h"
#include "engine/helpers/d3d12/D3D12InstancedBuffer.h"
#include "engine/helpers/d3d12/D3D12VertexBuffer.h"

#include "engine/helpers/render/MaterialMeshCache.h"

#include "engine/helpers/math/Transform.h"
#include "engine/helpers/math/Camera.h"

#include "engine/util/Timer.h"

/// Namespace containing the engine code.
namespace quark
{

// Forward declaration.
namespace gui
{
class GuiSubSystem;
}

/// Rendering classes.
namespace rndr
{

/// Structure compatibility with shaders.
namespace compatVoronoi
{

#include "engine/renderer/voronoiRL/shaders/VoronoiShadersCompat.h"

/// Container for descriptor heap indexing.
namespace DescHeap
{

/// Indexing of miscellaneous descriptor table.
namespace MiscDescTable
{

// Indexes of descriptors within the table.
enum 
{
    /// Buffer containing slow-changing constants.
    StaticConstantsBuffer = 0u,
    /// Buffer containing fast-changing constants.
    DynamicConstantsBuffer,
    /// Buffer containing cell data.
    CellDataBuffer,
    /// Ambient occlusion texture.
    AmbientOcclusionTexture, 

    DescHeapSize
};

} // namespace MiscDescTable

/// Indexing of G-Buffer descriptor table.
namespace DeferredDescTable
{

// Indexes of descriptors within the table.
enum 
{
    // G-Buffer: 
    /// Texture containing world-space coordinates.
    WorldPositionTexture = 0, 
    /// Texture containing world-space normals.
    WorldNormalTexture,
    /// Texture containing diffuse color of materials.
    MaterialDiffuseTexture, 
    /// Texture containing metallic-roughness parameters.
    MetallicRoughnessTexture, 
    /// Texture containing depths from camera.
    DepthTexture, 

    DescHeapSize
};

} // namespace DeferredDescTable

/// Indexing of ray tracing descriptor table.
namespace ShadowMapDescTable
{

// Indexes of descriptors within the table.
enum 
{
    // Shadow maps pass: 
    /// Textures containing the shadow maps.
    ShadowMaps = 0, 

    DescHeapSize
};

} // namespace ShadowMapDescTable

} // namespace DescHeap

/// Container for global root signature root parameters.
namespace GlobalRS
{

// Indexes of root parameters within the global root signature.
enum 
{
    /// Slow-changing constants.
    StaticConstantBuffer = 0u,
    /// Fast-changing constants.
    DynamicConstantBuffer,
    /// Cell data.
    CellDataBuffer,

    /// G-Buffer deferred buffers.
    DeferredBuffers, 
    /// Array of shadow maps.
    ShadowMapBuffers, 
    /// Ambient occlusion texture.
    AmbientOcclusionBuffers, 

    Count
};

} // namespace GlobalRS

/// Specification of the input vertex layout.
namespace InputLayout
{

/// Indexes of input layout slots.
enum
{
    /// Float3 position.
    Position = 0u, 

    Count
};

} // namespace InputLayout

} // namespace compatDeferred

/**
 * Render layer used for generation of 3D 
 * Voronoi diagrams.
 */
class VoronoiRL : public RenderLayerBase, public util::PointerType<VoronoiRL>
{
public:
    /**
     * Initialize the layer for given renderer.
     * @param renderer Renderer used for rendering the 
     * objects.
     */
    static PtrT create(RenderSubSystem &renderer);

    /// Free any resources.
    ~VoronoiRL();

    /**
     * Set buffers containing the deferred information.
     * @param buffers Container for the deferred buffers.
     */
    void setDeferredBuffers(DeferredBuffers buffers);

    /**
     * Set the shadow map buffers and light information.
     * @param buffers Buffers containing the shadow maps.
     * @param lights Information about each light.
     */
    void setShadowMapBuffers(ShadowMapBuffers buffers, ShadowMapRL::DirectionalLights lights);

    /**
     * Set buffers containing ambient occlusion information.
     * @param buffers Buffers containing the ambient occlusion.
     */
    void setAmbientOcclusionBuffers(AmbientOcclusionBuffers buffers);

    /**
     * Resize the buffers used by this render layer for given 
     * screen size.
     * Should be called after resizing of the swap chain!
     * @param width New width in pixels.
     * @param height New height in pixels.
     */
    void resize(uint32_t width, uint32_t height);

    /**
     * Check if the parameters changed and perform required 
     * preparation for rendering.
     */
    void prepare();

    /**
     * Create GUI elements used by this render layer.
     * @param gui GUI sub-system used for the main GUI.
     * @warning Default context/window must be set before 
     * calling this method.
     */
    void prepareGui(gui::GuiSubSystem &gui);

    /**
     * Render current configuration of Voronoi.
     * @param camera Camera containing the view-projection 
     * matrix which should be used in rendering.
     * @param targetRTV Target render-target-view which 
     * should contain the resulting image. Used only in 
     * debug rendering mode.
     * @param cmdList Command list which should contain the 
     * rendering commands.
     */
    void render(const helpers::math::Camera &camera, 
        const ::CD3DX12_CPU_DESCRIPTOR_HANDLE &targetRTV, 
        res::d3d12::D3D12CommandList &cmdList);

    /**
     * Update GUI variables used by this render layer.
     * @param gui GUI sub-system used for the main GUI.
     * @warning GUI preparation for this render layer must 
     * already be executed.
     */
    void updateGui(gui::GuiSubSystem &gui);

    /// Get the current global light direction.
    const dxtk::math::Vector3 &getLightDirection() const;
    /// Set new global light direction.
    void setLightDirection(const dxtk::math::Vector3 &lightDir);
private:
    /// Default format used for vertex positions.
    static constexpr ::DXGI_FORMAT DEFAULT_VERTEX_POSITION_FORMAT{ ::DXGI_FORMAT_R32G32B32_FLOAT };

    /// Default primitive topology type used by the main pipeline.
    static constexpr ::D3D12_PRIMITIVE_TOPOLOGY_TYPE DEFAULT_PRIMITIVE_TOPOLOGY_TYPE{ ::D3D12_PRIMITIVE_TOPOLOGY_TYPE_TRIANGLE };
    /// Default primitive topology used by the main pipeline.
    static constexpr ::D3D12_PRIMITIVE_TOPOLOGY DEFAULT_PRIMITIVE_TOPOLOGY{ ::D3D_PRIMITIVE_TOPOLOGY_TRIANGLELIST };

    /// Default render target buffer format.
    static constexpr ::DXGI_FORMAT DEFAULT_RENDER_TARGET_FORMAT{ ::DXGI_FORMAT_R8G8B8A8_UNORM };

    // Default direction of the global light.
    static constexpr float DEFAULT_LIGHT_DIR_X{ -0.058f };
    static constexpr float DEFAULT_LIGHT_DIR_Y{ -0.084f };
    static constexpr float DEFAULT_LIGHT_DIR_Z{ -0.001f };
    // Default color of the global light.
    static constexpr float DEFAULT_LIGHT_COLOR_R{ 1.0f };
    static constexpr float DEFAULT_LIGHT_COLOR_G{ 1.0f };
    static constexpr float DEFAULT_LIGHT_COLOR_B{ 1.0f };
    // Default color of the sky.
    static constexpr float DEFAULT_BACKGROUND_COLOR_R{ 0.53f };
    static constexpr float DEFAULT_BACKGROUND_COLOR_G{ 0.82f };
    static constexpr float DEFAULT_BACKGROUND_COLOR_B{ 0.98f };

    /// Default setting for rendering shadows.
    static constexpr bool DEFAULT_ENABLE_SHADOWS{ true };
    /// Default setting for rendering ambient occlusion.
    static constexpr bool DEFAULT_ENABLE_AO{ true };
    /// Default setting for rendering reflections.
    static constexpr bool DEFAULT_DO_REFLECTIONS{ true };

    /// Default value for the PCF shadows kernel size.
    static constexpr uint32_t DEFAULT_PCF_KERNEL_HALF_SIZE{ 4u };

    //// Default number of cells in the diagram.
    static constexpr uint32_t DEFAULT_VORONOI_CELL_COUNT{ 128u };
    //// Default size of the generated cell centers.
    static constexpr float DEFAULT_VORONOI_CELL_SIZE{ 1.0f };

    /// Shortcut for buffer containing the slow-changing constants.
    using StaticConstantsBuffer = helpers::d3d12::D3D12InstancedBuffer<compatVoronoi::StaticConstantBuffer, 1u>;
    /// Shortcut for buffer containing the fast-changing constants.
    using DynamicConstantsBuffer = helpers::d3d12::D3D12InstancedBuffer<compatVoronoi::DynamicConstantBuffer, 1u>;
    /// Shortcut for buffer containing the cell data.
    using CellDataBuffer = helpers::d3d12::D3D12InstancedBuffer<compatVoronoi::CellData, 1u>;

    /// Create static and dynamic constant buffers.
    void createConstantBuffers();

    /// Initialize rendering resources.
    void initializeResources();

    /// Build pipeline for the current settings.
    void buildPipeline();

    /// Generate the cell data.
    std::vector<compatVoronoi::CellData> createCellData(
        uint32_t cellCount, float cellSize);

    /**
     * Prepare cell centers and create a vertex buffer.
     * @param cellCount Number of cells to create.
     * @param cellSize Range of values used for generating 
     * the cells.
     * @param directCmdList Command list used for data 
     * upload.
     */
    void createCells(uint32_t cellCount, float cellSize, 
        res::d3d12::D3D12CommandList &directCmdList);

    /// Get the number of miscellaneous descriptors
    uint32_t calculateMiscDescriptorCount();

    /// Get the number of deferred descriptors
    uint32_t calculateDeferredDescriptorCount();

    /// Get the number of shadow map descriptors
    uint32_t calculateShadowMapDescriptorCount();

    /// Create SRV/UAV/CBV descriptor heap for specified number of descriptors.
    res::d3d12::D3D12DescHeap::PtrT createDescHeap(std::size_t requiredDescriptors);

    /// Create the descriptor table for miscellaneous descriptors.
    helpers::d3d12::D3D12DescTable createMiscTable(res::d3d12::D3D12DescHeap &descHeap, 
        uint32_t numDescriptors);

    /// Create the descriptor table for deferred descriptors.
    helpers::d3d12::D3D12DescTable createDeferredTable(res::d3d12::D3D12DescHeap &descHeap, 
        uint32_t numDescriptors);

    /// Create the descriptor table for shadow map descriptors.
    helpers::d3d12::D3D12DescTable createShadowMapTable(res::d3d12::D3D12DescHeap &descHeap, 
        uint32_t numDescriptors);

    /**
     * Create the descriptor heap and its tables according to 
     * requirements.
     * @param descHeap Descriptor heap which should be created.
     * @param miscTable Descriptor table for miscellaneous descriptors.
     * @param deferredTable Descriptor table for deferred buffer descriptors.
     * @param shadowMapTable Descriptor table for shadow map buffer descriptors.
     */
    void createDescHeapLayout(res::d3d12::D3D12DescHeap::PtrT &descHeap,
        helpers::d3d12::D3D12DescTable &miscTable,
        helpers::d3d12::D3D12DescTable &deferredTable,
        helpers::d3d12::D3D12DescTable &shadowMapTable);

    /// Fill miscellaneous table with descriptors.
    void fillMiscTable(helpers::d3d12::D3D12DescTable &miscTable);

    /**
     * Update the constant buffers with new camera values.
     * @param camera Currently used camera.
     */
    void updateCamera(const helpers::math::Camera &camera);

    /**
     * Update the constant buffers with new values.
     */
    void updateConstants();

    /**
     * Update the constant buffers with new values.
     * This will trigger re-upload of buffers to the GPU if 
     * necessary.
     */
    void refreshConstantBuffers();

    /**
     * If necessary, update the cell data and re-create any 
     * buffers.
     * @param directCmdList Command list used for uploading 
     * the cell data.
     */
    void updateCellData(res::d3d12::D3D12CommandList &directCmdList);

    /// Check and reload the shader programs, if requested.
    void checkReloadShaders();

    /// Render using this renderer.
    RenderSubSystem *mRenderer;

    /// Timer used for animation.
    util::HrTimer mTimer;

    /// Is deferred rendering ready to render current drawables?
    bool mPrepared{ false };

    /// Rendering resources.
    struct
    {
        /// Scissor rectangle for the viewport.
        ::CD3DX12_RECT scissorRect{ };
        /// Target viewport dimensions.
        ::CD3DX12_VIEWPORT viewport{ };

        /// Root signature of the main pass.
        res::d3d12::D3D12RootSignature::PtrT rootSig{ };
        /// Input layout for the main pass.
        res::d3d12::D3D12InputLayout inputLayout{ };
        /// Main vertex shader.
        res::d3d12::D3D12Shader::PtrT vShader{ };
        /// Main geometry shader.
        res::d3d12::D3D12Shader::PtrT gShader{ };
        /// Main pixel shader.
        res::d3d12::D3D12Shader::PtrT pShader{ };
        /// Pipeline for the main pass.
        res::d3d12::D3D12PipelineState::PtrT pipelineState{ };

        /// Currently used deferred buffers.
        DeferredBuffers deferredBuffers{ };
        /// Views to the deferred buffers.
        DeferredBufferViews deferredViews{ };
        /// Currently used shadow map buffers.
        ShadowMapBuffers shadowMapBuffers{ };
        /// Views to the shadow map buffers.
        ShadowMapBufferViews shadowMapViews{ };
        /// Currently used ambient occlusion buffers.
        AmbientOcclusionBuffers ambientOcclusionBuffers{ };
        /// Views to the ambient occlusion buffers.
        AmbientOcclusionBufferViews ambientOcclusionViews{ };

        /// Depth-stencil buffer used for the main pass.
        res::rndr::DepthStencilBuffer::PtrT depthStencilTexture{ };

        /// Slow-changing constant data.
        compatVoronoi::StaticConstantBuffer staticConstants{ };
        /// Dirty flag for the static constant data.
        bool staticConstantsChanged{ true };
        /// Slow-changing constant data buffer.
        StaticConstantsBuffer::PtrT staticConstantsBuffer{ };

        /// Fast-changing constant data.
        compatVoronoi::DynamicConstantBuffer dynamicConstants{ };
        /// Dirty flag for the dynamic constant data.
        bool dynamicConstantsChanged{ true };
        /// Fast-changing constant data buffer.
        DynamicConstantsBuffer::PtrT dynamicConstantsBuffer{ };

        /// Vertex buffer containing the cell centers.
        helpers::d3d12::D3D12VertexBuffer::PtrT cellVertexBuffer{ };
        /// Fast-changing constant data buffer.
        CellDataBuffer::PtrT cellDataBuffer{ };
        /// Should new cell data be generated?
        bool generateCellData{ true };

        /// Main descriptor heap used for storing SRVs, UAVs and CBVs.
        res::d3d12::D3D12DescHeap::PtrT descHeap{ };
        /// Descriptor table used for misc descriptors.
        helpers::d3d12::D3D12DescTable miscTable{ };
        /// Descriptor table used for deferred descriptors.
        helpers::d3d12::D3D12DescTable deferredTable{ };
        /// Descriptor table used for shadow map descriptors.
        helpers::d3d12::D3D12DescTable shadowMapTable{ };

        /// Should shader reload be attempted?
        bool reloadShaders{ false };
    } mR;

    /// Configuration of rendering
    struct
    {
        /// Information about lights in the scene.
        ShadowMapRL::DirectionalLights lights{ };

        /// Number of cells in the diagram.
        uint32_t cellCount{ DEFAULT_VORONOI_CELL_COUNT };

        /// Size of the generated cells.
        float cellSize{ DEFAULT_VORONOI_CELL_SIZE };
    } mC;
protected:
    /**
     * Initialize the layer for given renderer.
     * @param renderer Renderer used for rendering the 
     * objects.
     */
    VoronoiRL(RenderSubSystem &renderer);
}; // class VoronoiRL

} // namespace rndr

} // namespace quark
