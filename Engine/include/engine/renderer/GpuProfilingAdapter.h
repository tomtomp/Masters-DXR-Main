/**
 * @file renderer/GpuProfilingAdapter.h
 * @author Tomas Polasek
 * @brief Helper class connecting the profiling 
 * system with GPU timers.
 */

#pragma once

// Switch for putting PIX markers when using GPU profiling.
#define ENGINE_USE_PIX_MARKERS 0

// Engine configuration: 
#include "EngineRuntimeConfig.h"

// Adapter uses GPU timers to measure time on the GPU.
#include "engine/helpers/d3d12/D3D12GpuTimerManager.h"

// Adapter to this profiling system.
#include "engine/util/prof/Profiler.h"

/// Namespace containing the engine code.
namespace quark
{

/// Rendering classes.
namespace rndr
{

// Forward declaration.
class RenderSubSystem;

/// GPU profiling.
namespace prof
{

/**
 * Helper class connecting the profiling 
 * system with GPU timers.
 */
class GpuProfilingAdapter : public util::PointerType<GpuProfilingAdapter>
{
public:
    /// Thrown when there are no more profiling timers available.
    struct NoMoreTimersException : public std::exception
    {
        NoMoreTimersException(const char *msg) :
            std::exception(msg)
        { }
    }; // struct NoMoreTimersException

    /**
     * Initialize the GPU profiler, using provided 
     * runtime configuration and resources of given 
     * rendering sub-system.
     * @param cfg Runtime configuration of the engine.
     * @param renderer Rendering sub-system.
     * @return Returns pointer to the profiler.
     */
    static PtrT create(const EngineRuntimeConfig &cfg, RenderSubSystem &renderer);

    /// Free all resources
    ~GpuProfilingAdapter();

    // No copying.
    GpuProfilingAdapter(const GpuProfilingAdapter &other) = delete;
    GpuProfilingAdapter &operator=(const GpuProfilingAdapter &other) = delete;
    // Allow moving.
    GpuProfilingAdapter(GpuProfilingAdapter &&other) = default;
    GpuProfilingAdapter &operator=(GpuProfilingAdapter &&other) = default;

    /**
     * Enter a new scope in the GPU profiling.
     * @param cmdList Command list being profiled.
     * @param name Name of the scope.
     * @warning Name must be passed by a unique pointer.
     * @throw NoMoreTimersException Thrown when there are 
     * no more profiling timers available.
     */
    void enterScope(res::d3d12::D3D12CommandList &cmdList, const char *name);

    /**
     * Exit the current scope in the GPU profiling.
     * @param cmdList Command list being profiled.
     */
    void exitScope(res::d3d12::D3D12CommandList &cmdList);

    /**
     * Called after all timing command are queued, but before 
     * synchronizing.
     * @param cmdList Command list used for the resolving.
     */
    void resolve(res::d3d12::D3D12CommandList &cmdList);

    /**
     * Called after all of the currently profiled command lists 
     * are finished. Synchronizes the new GPU timer values with 
     * the profiling system.
     */
    void synchronize();
private:
    /// Shortcut for timer handle type.
    using TimerHandleT = helpers::d3d12::D3D12GpuTimerManager::TimerHandleT;

    /**
     * Check if there is a timer assigned to the provided 
     * hash and create it if there is none.
     * @param scopeHash Unique hash of the scope.
     * @throw NoMoreTimersException Thrown when there are 
     * no more profiling timers available.
     */
    TimerHandleT getCreateGpuTimer(std::size_t scopeHash);

    /// Management of timers used for profiling.
    helpers::d3d12::D3D12GpuTimerManager::PtrT mTimerManager{ };
    /// Context used for simulated profiling.
    util::prof::SimulatedProfilingContext mProfilingContext{ };
    /// Mapping from scope unique hash code to its GPU timer.
    std::map<std::size_t, TimerHandleT> mScopeTimerMap{ };
    /// List of scope simulators for the current run of the profiler.
    std::vector<util::prof::SimulatedProfilingContext::LateScopeSimulator> mScopeSimulators{ };
protected:
    /**
     * Initialize the GPU profiler, using provided 
     * runtime configuration and resources of given 
     * rendering sub-system.
     * @param cfg Runtime configuration of the engine.
     * @param renderer Rendering sub-system.
     */
    GpuProfilingAdapter(const EngineRuntimeConfig &cfg, RenderSubSystem &renderer);
}; // class GpuProfilingAdapter

/// GPU profiler for a single scope.
class GpuScopeProfiler
{
public:
    /**
     * Create GPU scope profiler for given parameters.
     * @param profilingAdapter Adapter used for getting 
     * the results.
     * @param cmdList Target command list to profile.
     * @param name Name or description of the scope.
     * @throw GpuProfilingAdapter::NoMoreTimersException 
     * Thrown when there are no more profiling timers 
     * available.
     */
    GpuScopeProfiler(GpuProfilingAdapter &profilingAdapter, 
        res::d3d12::D3D12CommandList &cmdList, const char *name);

    /// Exit the scope and stop the timer.
    ~GpuScopeProfiler();

    GpuScopeProfiler(const GpuScopeProfiler &other) = delete;
    GpuScopeProfiler(GpuScopeProfiler &&other) = default;
    GpuScopeProfiler &operator=(const GpuScopeProfiler &other) = delete;
    GpuScopeProfiler &operator=(GpuScopeProfiler &&other) = default;
private:
    // Parameters of the scope: 
    GpuProfilingAdapter &mProfilingAdapter;
    res::d3d12::D3D12CommandList &mCmdList;
    const char *mName;
protected:
}; // class GpuScopeProfiler

/// GPU profiler for a single code block.
class GpuBlockProfiler
{
public:
    /**
     * Create GPU block profiler for given parameters.
     * @param profilingAdapter Adapter used for getting 
     * the results.
     * @param cmdList Target command list to profile.
     * @param name Name or description of the scope.
     * @param manualEnd When set to true, the destructor 
     * will never call end, even when never used.
     * @throw GpuProfilingAdapter::NoMoreTimersException 
     * Thrown when there are no more profiling timers 
     * available.
     */
    GpuBlockProfiler(GpuProfilingAdapter &profilingAdapter, 
        res::d3d12::D3D12CommandList &cmdList, const char *name, 
        bool manualEnd = false);

    /// Exit the scope and stop the timer, performed only if not ended.
    ~GpuBlockProfiler();

    GpuBlockProfiler(const GpuBlockProfiler &other) = delete;
    GpuBlockProfiler(GpuBlockProfiler &&other) = default;
    GpuBlockProfiler &operator=(const GpuBlockProfiler &other) = delete;
    GpuBlockProfiler &operator=(GpuBlockProfiler &&other) = default;

    /// Exit the scope and stop the timer, performed only if not ended.
    void end();
private:
    /// has this profiler been ended already?
    bool mEnded{ false };
    // Parameters of the block: 
    GpuProfilingAdapter &mProfilingAdapter;
    res::d3d12::D3D12CommandList &mCmdList;
    const char *mName;
protected:
}; // class BlockProfiler

} // namespace prof

} // namespace rndr

} // namespace quark
