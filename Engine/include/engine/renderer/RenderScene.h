/**
 * @file renderer/RenderScene.h
 * @author Tomas Polasek
 * @brief Wrapper around information needed for rendering 
 * of a scene - lists of drawables, camera information, etc.
 */

#pragma once

#include "engine/resources/scene/DrawableListMgr.h"
#include "engine/scene/systems/CameraSys.h"

/// Namespace containing the engine code.
namespace quark
{

/// Rendering classes.
namespace rndr
{

/**
 * Wrapper around information needed for rendering 
 * of a scene - lists of drawables, camera information, etc.
 */
class RenderScene : public util::PointerType<RenderScene>
{
public:
    /**
     * Create and initialize the scene.
     * @param drawables List of drawables contained in the scene.
     * @param cameraSys Manager of camera for this scene.
     * @return Returns pointer to the created rendering pass.
     */
    static PtrT create(res::scene::DrawableListHandle &drawables, scene::sys::CameraSys &cameraSystem);

    /// Free any resources used by the pass.
    virtual ~RenderScene();

    // No copying or moving.
    RenderScene(const RenderScene &other) = delete;
    RenderScene &operator=(const RenderScene &other) = delete;
    RenderScene(RenderScene &&other) = delete;
    RenderScene &operator=(RenderScene &&other) = delete;
private:
    /// List of drawable objects in this scene.
    res::scene::DrawableListHandle mDrawables{ };
    /// Camera manager.
    scene::sys::CameraSys *mCameraSystem{ nullptr };

    // TODO - Drawable list manager, added, removed, changed entities.
    // TODO - Add flags for which lists are required, fill only those.
    // TODO - Add specification of input layout of the vertex data (hard-coded for now?).
protected:
    /**
     * Create and initialize the scene.
     * @param drawables List of drawables contained in the scene.
     * @param cameraSys Manager of camera for this scene.
     */
    RenderScene(res::scene::DrawableListHandle &drawables, scene::sys::CameraSys &cameraSystem);
}; // class RenderScene

} // namespace rndr

} // namespace quark
