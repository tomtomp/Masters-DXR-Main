/**
 * @file renderer/platform/d3d12/D3D12SamplerFiltering.h
 * @author Tomas Polasek
 * @brief Conversion of sampler filtering options into D3D12 format.
 */

#pragma once

#include "engine/renderer/SamplerFiltering.h"

#include "engine/util/SafeD3D12.h"

/// Namespace containing the engine code.
namespace quark
{

/// Rendering classes.
namespace rndr
{

/// Platform specific rendering classes.
namespace platform
{ 

/// D3D12 implementations.
namespace d3d12
{ 

/**
 * Convert engine sampler filtering option into the D3D12 representation.
 * @param minFilter Minification sampler filter.
 * @param minFilter Magnification sampler filter.
 * @param mipFilter Filtering used for mip-maps.
 * @return Returns D3D12 representation of the same filtering option or 
 * ::D3D12_FILTER_MIN_MAG_MIP_POINT if the conversion is not possible.
 */
::D3D12_FILTER sampleFilterToD3D12(SampleFilter minFilter, SampleFilter magFilter, SampleFilter mipFilter);

/**
 * Convert engine sampler filtering option into the D3D12 representation.
 * @param edgeFiltering Bundle of min/mag/mip filtering.
 * @return Returns D3D12 representation of the same filtering option or 
 * ::D3D12_FILTER_MIN_MAG_MIP_POINT if the conversion is not possible.
 */
::D3D12_FILTER sampleFilterToD3D12(MinMagMipFilter edgeFiltering);

/**
 * Convert engine sampler edge filtering option into the D3D12 representation.
 * @param edgeFilter Edge filtering option.
 * @return Returns D3D12 representation of the same edge filtering option or 
 * ::D3D12_TEXTURE_ADDRESS_MODE_WRAP if the conversion is not possible.
 */
::D3D12_TEXTURE_ADDRESS_MODE sampleEdgeToD3D12(SampleEdge edgeFilter);

} // namespace d3d12

} // namespace platform

} // namespace rndr

} // namespace quark
