/**
 * @file renderer/platform/d3d12/D3D12RasterDispatch.h
 * @author Tomas Polasek
 * @brief Implementation of raster dispatch for D3D12.
 */

#pragma once

#include "engine/renderer/RasterDispatch.h"

/// Namespace containing the engine code.
namespace quark
{

/// Rendering classes.
namespace rndr
{

/// Platform specific rendering classes.
namespace platform
{ 

/// D3D12 implementations.
namespace d3d12
{ 

/**
 * Implementation of raster dispatch for D3D12.
 */
class D3D12RasterDispatch : public RasterDispatch
{
public:
    /**
     * Create a new default configured rasterization dispatch 
     * using the provided rendering context.
     * @param ctx Current rendering context.
     * @return Returns pointer to the new dispatch.
     */
    static PtrT create(RenderContext::PtrT &ctx);

    /// Free any resources used by this dispatch.
    virtual ~D3D12RasterDispatch();
private:
    /**
     * Create a new default configured rasterization dispatch 
     * using the provided rendering context.
     * @param ctx Current rendering context.
     */
    D3D12RasterDispatch(RenderContext::PtrT ctx);
protected:
}; // class D3D12RasterDispatch

} // namespace d3d12

} // namespace platform

} // namespace rndr

} // namespace quark
