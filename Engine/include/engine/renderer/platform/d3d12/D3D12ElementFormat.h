/**
 * @file renderer/platform/d3d12/D3D12ElementFormat.h
 * @author Tomas Polasek
 * @brief Conversion of element format into D3D12 DXGI_FORMAT.
 */

#pragma once

#include "engine/renderer/ElementFormat.h"

#include "engine/util/SafeD3D12.h"

/// Namespace containing the engine code.
namespace quark
{

/// Rendering classes.
namespace rndr
{

/// Platform specific rendering classes.
namespace platform
{ 

/// D3D12 implementations.
namespace d3d12
{ 

/**
 * Convert engine element format into the D3D12 representation.
 * @param elementFormat Format of the element.
 * @return Returns D3D12 representation of the same format or 
 * ::DXGI_FORMAT_UNKNOWN if the conversion is not possible.
 */
::DXGI_FORMAT elementFormatToD3D12(ElementFormat elementFormat);

} // namespace d3d12

} // namespace platform

} // namespace rndr

} // namespace quark
