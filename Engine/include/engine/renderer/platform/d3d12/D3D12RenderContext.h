/**
 * @file renderer/platform/d3d12/D3D12RenderContext.h
 * @author Tomas Polasek
 * @brief Specialization of render context for environment 
 * using D3D12.
 */

#pragma once

#include "engine/renderer/RenderContext.h"

#include "engine/util/SafeD3D12.h"

/// Namespace containing the engine code.
namespace quark
{

/// Rendering classes.
namespace rndr
{

/// Platform specific rendering classes.
namespace platform
{ 

/// D3D12 implementations.
namespace d3d12
{ 

// TODO - ...

} // namespace d3d12

} // namespace platform

} // namespace rndr

} // namespace quark
