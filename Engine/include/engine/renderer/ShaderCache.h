/**
 * @file renderer/ShaderCache.h
 * @author Tomas Polasek
 * @brief System used for shader caching and compilation.
 */

#pragma once

#include "engine/util/Types.h"

namespace quark
{

namespace rndr
{

// Forward declaration.
class RenderContext;

/**
 * Enumeration of all types of shaders known 
 * by the shader cache.
 */
enum class ShaderType
{
    // Rasterization shaders: 

    /// Vertex processing shader.
    Vertex,
    /// Tessellation Hull/Control shader.
    Hull,
    /// Tessellation Domain/Evaluation shader.
    Domain, 
    /// Geometry shader.
    Geometry,
    /// Pixel/Fragment shader.
    Pixel, 

    // Compute shaders: 

    /// Compute/Generic shader.
    Compute, 

    // Ray Tracing shaders: 

    /// Generation of rays.
    RayGeneration, 
    /// Intersection with primitives.
    Intersection, 
    /// Deeper testing of primitives.
    AnyHit, 
    /// Geometry hit.
    ClosestHit, 
    /// Geometry missed.
    Miss
}; // enum class ShaderType

/**
 * Record of a single shader within the shader cache.
 * Specialized for each platform.
 */
class ShaderRecord : public util::PointerType<ShaderRecord>
{
public:
    /// Free allocated shader.
    virtual ~ShaderRecord();

    // No copying or moving.
    ShaderRecord(const ShaderRecord &other) = delete;
    ShaderRecord &operator=(const ShaderRecord &other) = delete;
    ShaderRecord(ShaderRecord &&other) = delete;
    ShaderRecord &operator=(ShaderRecord &&other) = delete;

    /// Has this shader been prepared?
    bool prepared() const;

    /**
     * Request reload of this shader, makes sense only for shaders 
     * loaded from file.
     */ 
    void requestReload();
private:
    // Allow access to internal methods.
    friend class ShaderCache;
protected:
    /// Initialize empty shader record.
    ShaderRecord();

    /**
     * Request preparation of this shader from its source code.
     * @param type Type of the shader contained within provided 
     * source code.
     * @param sourceCode Un-compiled source code of the shader.
     * @param entryPoint Entry-point identification for the shader.
     */
    void requestFromSource(ShaderType type, const std::string &sourceCode, 
        const std::string &entryPoint);

    /**
     * Request preparation of this shader from pre-compiled byte code.
     * @param type Type of the shader contained within provided 
     * byte code.
     * @param ptr Pointer to the beginning of the byte-code.
     * @param sizeInBytes Size of the byte-code in bytes.
     */
    void requestFromBlob(ShaderType type, const uint8_t *ptr, 
        std::size_t sizeInBytes);

    /**
     * Perform any steps necessary to prepare the shader 
     * for further usage.
     * @param ctx Current rendering context.
     * @warning Performs initialization only if 
     * prepared() == false;
     */
    void initialize(util::PtrT<RenderContext> ctx);

    /**
     * Implementation of the initialize method, which has 
     * to be specialized for each platform.
     * @param ctx Current rendering context.
     */
    virtual bool initializeInternal(util::PtrT<RenderContext> ctx) = 0;

    /// Stage/type of this shader.
    ShaderType mType{ };

    /// Un-compiled source code of this shader.
    std::string mSourceCode{ };
    /// Pre-compiled byte code of this shader.
    std::vector<uint8_t> mByteCode{ };

    /// Has the shader been compiled/prepared for usage?
    bool mPrepared{ false };
}; // class ShaderRecord

/**
 * System used for shader caching and compilation.
 * Specialized for each platform.
 */
class ShaderCache : public util::PointerType<ShaderCache>
{
public:
    /// Free all shader resources.
    virtual ~ShaderCache();

    // No copying or moving.
    ShaderCache(const ShaderCache &other) = delete;
    ShaderCache &operator=(const ShaderCache &other) = delete;
    ShaderCache(ShaderCache &&other) = delete;
    ShaderCache &operator=(ShaderCache &&other) = delete;

    /**
     * Load a shader stage from file, optionally specifying the entry point.
     * The combination of type + filename + entry-point is used as unique identifier.
     * @param type Type of the shader - e.g. Vertex, Pixel, etc.
     * @param filename Name of the file containing shader source code.
     * @param entryPoint Entry-point of the shader.
     */
    ShaderRecord::PtrT loadFromFile(ShaderType type, const std::string &filename, 
        const std::string &entryPoint = "main");

    /**
     * Load a shader stage from pre-compiled blob.
     * Identifier and type are used as unique identifier.
     * @param type Type of the shader to load.
     * @param identifier String identifier of the shader, which is unique 
     * between different blobs.
     * @param ptr Pointer to the binary blob of pre-compiled shader code.
     * @param sizeInBytes Number of bytes in the binary blob.
     */
    void loadFromBlob(ShaderType type, const std::string &identifier, 
        const uint8_t *ptr, std::size_t sizeInBytes);

    /**
     * Compile and finalize a single shader.
     * @param record Target shader to finalize.
     */
    void finalizeRequestedShader(ShaderRecord &record);

    /**
     * Compile and finalize all shaders which were requested since 
     * last finalization.
     */
    void finalizeRequestedShaders();
private:
    /**
     * Create an instance of shader record specific to the 
     * platform.
     * @return Returns pointer to the created shader record.
     */
    virtual util::PtrT<ShaderRecord> createShaderRecord() = 0;
protected:
    /// Initialize empty shader cache.
    ShaderCache(util::PtrT<RenderContext> ctx);
}; // class ShaderCache

}

}
