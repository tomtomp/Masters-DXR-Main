/**
 * @file renderer/shadowMapRL/shaders/ShadowMapShadersCompat.h
 * @author Tomas Polasek
 * @brief Data structures shared between shadow map rendering shaders.
 */

#ifndef ENGINE_BASIC_SHADOW_MAP_RL_SHADERS_COMPAT_H
#define ENGINE_BASIC_SHADOW_MAP_RL_SHADERS_COMPAT_H

#ifdef HLSL
#   include "HlslCompatibility.h"
#else
#   include "engine/helpers/hlsl/HlslCompatibility.h"
#endif

/// Buffer containing per-light constants.
struct PerLightBuffer
{
    /// Transform from world-space to light-space (vp).
    HLSL_TYPE(float4x4) worldToLight;
}; // struct PerLightBuffer

/// Buffer containing dynamic per-object data.
struct InstanceConstantBuffer
{
    /// Transform from model-space to world-space.
    HLSL_TYPE(float4x4) modelToWorld;
}; // struct DynamicConstantBuffer

#ifndef HLSL

/// Helper structure used for passing HLSL register-space pairs.
struct HlslRegisterSpacePair
{
	uint32_t reg{ 0u };
	uint32_t space{ 0u };
}; // struct HlslRegisterSpacePair

/// Per-light constants buffer.
static constexpr HlslRegisterSpacePair PerLightSlot{ 0u, 0u };
/// Per-object dynamic constants buffer.
static constexpr HlslRegisterSpacePair InstanceConstantsSlot{ 1u, 0u };

#else

/// Per-light constants.
ConstantBuffer<PerLightBuffer> gPerLight : register(b0, space0);
/// Per-object dynamic data.
ConstantBuffer<InstanceConstantBuffer> gInstance : register(b1, space0);


/// Data layout for input into the vertex shader.
struct VertexData
{
    /// Model-space position.
    float3 position : POSITION;

    uint id : SV_VERTEXID;
}; // struct VertexData

/// Data layout for input into the pixel shader.
struct FragmentData
{
    /// Screen-space position.
    float4 ssPosition : SV_Position;
}; // struct FragmentData

/// Data layout for output out of the pixel shader.
struct PixelData
{
    /// Depth of the fragment from the light.
    float depth : SV_Depth;
}; // struct PixelData

#endif

#endif // ENGINE_BASIC_SHADOW_MAP_RL_SHADERS_COMPAT_H
