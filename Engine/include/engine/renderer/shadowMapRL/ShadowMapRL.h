/**
 * @file renderer/shadowMapRL/ShadowMapRL.h
 * @author Tomas Polasek
 * @brief Rendering layer used to generate 
 * shadow map depth textures.
 */

#pragma once

#include "engine/renderer/RenderLayerBase.h"

#include "engine/renderer/RenderSubSystem.h"

#include "engine/resources/d3d12/D3D12Shader.h"
#include "engine/resources/d3d12/D3D12RootSignature.h"
#include "engine/resources/d3d12/D3D12InputLayout.h"
#include "engine/resources/d3d12/D3D12PipelineState.h"

#include "engine/resources/render/DepthStencilBuffer.h"
#include "engine/resources/render/DescriptorMgr.h"

#include "engine/resources/scene/DrawableListMgr.h"

#include "engine/helpers/d3d12/D3D12RootSignatureBuilder.h"
#include "engine/helpers/d3d12/D3D12PipelineStateBuilder.h"
#include "engine/helpers/d3d12/D3D12InstancedBuffer.h"

#include "engine/helpers/render/MaterialMeshCache.h"

#include "engine/helpers/math/Transform.h"
#include "engine/helpers/math/Camera.h"

/// Namespace containing the engine code.
namespace quark
{

// Forward declaration.
namespace gui
{
class GuiSubSystem;
}

/// Rendering classes.
namespace rndr
{

/// Structure compatibility with shaders.
namespace compatShadowMap
{

#include "engine/renderer/shadowMapRL/shaders/ShadowMapShadersCompat.h"

/// Container for descriptor heap indexing.
namespace DescHeap
{

/// Indexing of miscellaneous descriptor table.
namespace MiscDescTable
{

// Indexes of descriptors within the table.
enum 
{
    /// Buffer changing for each light.
    PerLightBuffer = 0u,

    DescHeapSize
};

} // namespace MiscDescTable

} // namespace DescHeap

/// Container for descriptor heap indexing.
namespace RtvDescHeap
{

/// Indexing of RTV descriptor table.
namespace RtvDescTable
{

// Indexes of descriptors within the table.
enum 
{
    /// Output depth texture, as visible from the light.
    ShadowMapTexture = 0, 

    DescHeapSize
};

} // namespace RtvDescTable

} // namespace RtvDescHeap

/// Container for global root signature root parameters.
namespace GlobalRS
{

// Indexes of root parameters within the global root signature.
enum 
{
    /// Buffer changing for each light.
    PerLightBuffer = 0u,
    /// Per-instance constants.
    InstanceConstantBuffer,

    Count
};

} // namespace GlobalRS

/// Specification of the input vertex layout.
namespace InputLayout
{

/// Indexes of input layout slots.
enum
{
    /// Float3 position.
    Position = 0u, 

    Count
};

} // namespace InputLayout

} // namespace compatDeferred

/// Helper structure used for passing shadow map buffers.
struct ShadowMapBuffers
{
    /// Transition all buffers to given state.
    void transitionResourcesTo(res::d3d12::D3D12CommandList &cmdList, ::D3D12_RESOURCE_STATES state);

    /// Current resolution of created shadow maps.
    uint32_t resolution{ 0u };

    /// Shadow map textures.
    std::vector<helpers::d3d12::D3D12TextureBuffer::PtrT> shadowMaps{ };
}; // struct ShadowMapBuffers

/// Helper structure used for creating views to the shadow map buffers.
struct ShadowMapBufferViews
{
    /// Create all resource views for given buffers.
    void createViews(const ShadowMapBuffers &buffers, 
        res::d3d12::D3D12DescAllocatorInterface &descHeap, 
        ResourceViewType type);

    /// Type of the shadow map view.
    static constexpr ::DXGI_FORMAT VIEW_FORMAT{ ::DXGI_FORMAT_R32_FLOAT };

    /// Views of the shadow maps.
    std::vector<helpers::d3d12::D3D12ResourceView::PtrT> shadowMapViews{ };
}; // struct ShadowMapBufferViews

/**
 * Rendering layer used to generate 
 * shadow map depth textures.
 */
class ShadowMapRL : public RenderLayerBase, public util::PointerType<ShadowMapRL>
{
public:
    /// Holder of information about directional lights.
    struct DirectionalLights
    {
        /// Specification of directional light.
        struct DirectionalLight
        {
            /// Direction from which the light shines.
            dxtk::math::Vector3 direction{ };
            /// Transform from world-space into light-space.
            dxtk::math::Matrix lightTransform{ };
        }; // struct DirectionalLight
        
        /// Clear all lights.
        DirectionalLights &clear();

        /// Push a new light, which shines from given direction.
        DirectionalLights &pushLight(const dxtk::math::Vector3 &direction);
        /// Push a new light, which shines from given direction.
        DirectionalLights &pushLight(float x, float y, float z);

        /// List of currently displayed lights.
        std::vector<DirectionalLight> lights{ };
    }; // struct DirectionalLights

    /**
     * Initialize the layer for given renderer.
     * @param renderer Renderer used for rendering the 
     * objects.
     */
    static PtrT create(RenderSubSystem &renderer);

    /// Free any resources.
    ~ShadowMapRL();

    /**
     * Set a list of drawables which should be rendered by 
     * this render layer.
     * @param drawables Versioned list of drawables which will 
     * be used for rendering.
     */
    void setDrawablesList(const res::scene::DrawableListHandle &drawables);

    /// Access the holder of lights.
    DirectionalLights &lights();

    /// Get the shadow maps currently used by this pass.
    ShadowMapBuffers shadowMapBuffers() const;

    /**
     * Check if the drawables list changed and perform required 
     * preparation for rendering if necessary.
     */
    void prepare();

    /**
     * Create GUI elements used by this render layer.
     * @param gui GUI sub-system used for the main GUI.
     * @warning Default context/window must be set before 
     * calling this method.
     */
    void prepareGui(gui::GuiSubSystem &gui);

    /**
     * Update GUI variables used by this render layer.
     * @param gui GUI sub-system used for the main GUI.
     * @warning GUI preparation for this render layer must 
     * already be executed.
     */
    void updateGui(gui::GuiSubSystem &gui);

    /**
     * Render current list of drawables into the G-Buffer.
     * @param camera Camera containing the view-projection 
     * matrix which should be used in rendering.
     * @param cmdList Command list which should contain the 
     * rendering commands.
     */
    void render(const helpers::math::Camera &camera, 
        res::d3d12::D3D12CommandList &cmdList);

    /// Should the layer render ambient occlusion?
    void setEnabled(bool enabled);
private:
    /// Default format used for vertex positions.
    static constexpr ::DXGI_FORMAT DEFAULT_VERTEX_POSITION_FORMAT{ ::DXGI_FORMAT_R32G32B32_FLOAT };

    /// Default primitive topology type used by the main pipeline.
    static constexpr ::D3D12_PRIMITIVE_TOPOLOGY_TYPE DEFAULT_PRIMITIVE_TOPOLOGY_TYPE{ ::D3D12_PRIMITIVE_TOPOLOGY_TYPE_TRIANGLE };
    /// Default primitive topology used by the main pipeline.
    static constexpr ::D3D12_PRIMITIVE_TOPOLOGY DEFAULT_PRIMITIVE_TOPOLOGY{ ::D3D_PRIMITIVE_TOPOLOGY_TRIANGLELIST };

    /// Format of the shadow map render target.
    static constexpr ::DXGI_FORMAT DEFAULT_SHADOW_MAP_RT_FORMAT{ ::DXGI_FORMAT_R32_TYPELESS };
    /// Format of the DSV of the shadow maps.
    static constexpr ::DXGI_FORMAT DEFAULT_SHADOW_MAP_DSV_FORMAT{ ::DXGI_FORMAT_D32_FLOAT };

    /// We are clearing everything to this value.
    static constexpr float DEFAULT_CLEAR_VALUE{ 1.0f };

    /// Resolution of the shadow maps in pixels.
    static constexpr uint32_t DEFAULT_SHADOW_MAP_RESOLUTION{ 4096u };

    /// Width and height of the ortho viewport for light camera.
    static constexpr float DEFAULT_LIGHT_VIEWPORT_SIZE{ 40.0f };
    /// Position of the near plane for the light camera.
    static constexpr float DEFAULT_LIGHT_NEAR{ -10.0f };
    /// Position of the far plane for the light camera.
    static constexpr float DEFAULT_LIGHT_FAR{ 20.0f };

    /// Shortcut for buffer containing the light constants.
    using PerLightBuffer = helpers::d3d12::D3D12InstancedBuffer<compatShadowMap::PerLightBuffer>;

    /// Initialize rendering resources.
    void initializeResources();

    /// Build pipeline using the current settings.
    void buildPipeline();

    /// Calculate the number of required descriptors, in the SRV desc heap.
    uint32_t calculateRequiredDescriptors() const;

    /**
     * Prepare commands used for rendering drawables into 
     * provided draw bundle.
     * @param drawables List of drawables to prepare.
     * @param drawBundle Draw bundle which should contain 
     * all of the draw commands.
     */
    void prepareDrawBundle(res::scene::DrawableList &drawables, 
        res::d3d12::D3D12CommandList &drawBundle);

    /**
     * Create a new command list which can be used as a drawing 
     * command bundle.
     * @param renderer Currently used renderer.
     * @return Returns pointer to the new command bundle.
     */
    res::d3d12::D3D12CommandList::PtrT createDrawBundle(RenderSubSystem &renderer);

    /**
     * Prepare given list of drawables for next render().
     * @param drawables List of drawable to prepare.
     */
    void prepareForRendering(res::scene::DrawableList &drawables);

    /// Clear all shadow map textures to given value.
    void clearShadowMapTextures(res::d3d12::D3D12CommandList &cmdList, 
        ShadowMapBuffers &buffers, float clearValue);

    /// Prepare descriptor heap/table combination for given lights.
    void prepareDsvDescHeap(res::d3d12::D3D12DescHeap::PtrT &dsvDescHeap,
        helpers::d3d12::D3D12DescTable &dsvTable, const DirectionalLights &lights);

    /**
     * Prepare all necessary textures for given list of lights.
     * @param buffers Shadow map texture holder.
     * @param resolution Resolution of each shadow map.
     * @param lights Holder of all lights, which should have 
     * shadow maps.
     */
    void prepareShadowMapTextures(ShadowMapBuffers &buffers, uint32_t resolution, 
        const DirectionalLights &lights);

    /**
     * Create and fill the data into the per-light constant buffer.
     * @param camera Main world camera.
     * @param buffer Buffer passed to the shadow map shader.
     * @param lights Specification of lights. Light matrices will 
     * be automatically recalculated.
     */
    void prepareShadowMapConstantBuffers(
        const helpers::math::Camera &camera, 
        PerLightBuffer::PtrT &buffer, DirectionalLights &lights);

    /**
     * Calculate the view-projection matrix for directional light.
     * @param camera Main world camera.
     * @param direction Direction from which the light shines.
     * @param viewportSize Width and height of the camera viewport.
     * @param nearPos Near plane position.
     * @param farPos Far plane position.
     */
    dxtk::math::Matrix calculateDirectionalLightTransform(
        const helpers::math::Camera &camera, const dxtk::math::Vector3 &direction,
        float viewportSize, float nearPos, float farPos);

    /// Check and reload the shader programs, if requested.
    void checkReloadShaders();

    /// Render using this renderer.
    RenderSubSystem *mRenderer;

    /// Mutable list of drawables which are rendered by this render layer.
    res::scene::DrawableListHandle mDrawables{ };
    /// Which last version of drawables has been prepared.
    res::scene::DrawableList::VersionT mLastPreparedVersion{ };

    /// Is deferred rendering ready to render current drawables?
    bool mPrepared{ false };

    /// Rendering resources.
    struct
    {
        /// Scissor rectangle for the viewport.
        ::CD3DX12_RECT scissorRect{ };
        /// Target viewport dimensions.
        ::CD3DX12_VIEWPORT viewport{ };

        /// Root signature of the main pass.
        res::d3d12::D3D12RootSignature::PtrT rootSig{ };
        /// Input layout for the main pass.
        res::d3d12::D3D12InputLayout inputLayout{ };
        /// Main vertex shader.
        res::d3d12::D3D12Shader::PtrT vShader{ };
        /// Main pixel shader.
        res::d3d12::D3D12Shader::PtrT pShader{ };
        /// Pipeline for the main pass.
        res::d3d12::D3D12PipelineState::PtrT pipelineState{ };

        /// Textures containing the shadow maps.
        ShadowMapBuffers shadowMapBuffers{ };

        /// Per-light constant data buffer.
        PerLightBuffer::PtrT perLightConstantsBuffer{ };

        /**
         * Command bundle containing rendering commands for the 
         * currently prepared drawables.
         */
        res::d3d12::D3D12CommandList::PtrT renderBundle{ };

        /// Main descriptor table, used for SRVs.
        res::d3d12::D3D12DescHeap::PtrT descHeap{ };

        /// Descriptor heap used for shadow map DSVs.
        res::d3d12::D3D12DescHeap::PtrT dsvDescHeap{ };
        /// Descriptor table used for shadow map DSVs.
        helpers::d3d12::D3D12DescTable dsvTable{ };

        /// Should shader reload be attempted?
        bool reloadShaders{ false };
    } mR;

    /// Configuration.
    struct
    {
        /// Holder of currently displayed lights.
        DirectionalLights lights;
        /// Resolution of each shadow map in pixels.
        uint32_t shadowMapResolution{ DEFAULT_SHADOW_MAP_RESOLUTION };

        /// Should the shadows be rendered?
        bool enabled{ true };
    } mC; 
protected:
    /**
     * Initialize the layer for given renderer.
     * @param renderer Renderer used for rendering the 
     * objects.
     */
    ShadowMapRL(RenderSubSystem &renderer);
}; // class ShadowMapRL

} // namespace rndr

} // namespace quark
