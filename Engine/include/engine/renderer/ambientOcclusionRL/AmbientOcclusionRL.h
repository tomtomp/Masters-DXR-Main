/**
 * @file renderer/ambientOcclusionRL/AmbientOcclusionRL.h
 * @author Tomas Polasek
 * @brief Rendering layer used for generation of ambient 
 * occlusion buffers.
 */

#pragma once

#include "engine/renderer/RenderLayerBase.h"
#include "engine/renderer/RenderSubSystem.h"

#include "engine/renderer/deferredRL/DeferredRL.h"

#include "engine/helpers/d3d12/D3D12InstancedBuffer.h"

/// Namespace containing the engine code.
namespace quark
{

// Forward declaration.
namespace gui
{
class GuiSubSystem;
}

/// Rendering classes.
namespace rndr
{

/// Structure compatibility with shaders.
namespace compatAmbientOcclusion
{

#include "engine/renderer/ambientOcclusionRL/shaders/AmbientOcclusionShadersCompat.h"

/// Container for descriptor heap indexing.
namespace DescHeap
{

/// Indexing of miscellaneous descriptor table.
namespace MiscDescTable
{

// Indexes of descriptors within the table.
enum 
{
    /// Texture used as output for ambient occlusion.
    AmbientOcclusionOutput = 0u,
    /// Buffer containing slow-changing constants.
    StaticConstantsBuffer,
    /// Buffer containing fast-changing constants.
    DynamicConstantsBuffer,
    /// Texture containing float3 noise values.
    NoiseTexture, 
    /// Texture containing float3 AO kernel values.
    KernelTexture, 

    DescHeapSize
};

} // namespace MiscDescTable

} // namespace DescHeap

/// Container for global root signature root parameters.
namespace GlobalRS
{

// Indexes of root parameters within the global root signature.
enum 
{
    /// Slow-changing constants.
    StaticConstantBuffer = 0u,
    /// Fast-changing constants.
    DynamicConstantBuffer,
    /// Texture containing float3 noise values.
    NoiseTexture, 
    /// Texture containing float3 AO kernel values.
    KernelTexture, 

    /// G-Buffer deferred buffers.
    DeferredBuffers, 

    Count
};

} // namespace GlobalRS

} // namespace compat

/// Helper structure used for passing ambient occlusion buffers.
struct AmbientOcclusionBuffers
{
    /// Transition all buffers to given state.
    void transitionResourcesTo(res::d3d12::D3D12CommandList &cmdList, ::D3D12_RESOURCE_STATES state);

    /// Texture containing ambient occlusion results - 1.0 means fully occluded.
    helpers::d3d12::D3D12TextureBuffer::PtrT ambientOcclusion{ };

    /// Total number of buffers.
    static constexpr uint32_t BUFFER_COUNT{ 1u };
}; // struct AmbientOcclusionBuffers

/// Helper structure used for creating views to the deferred buffers.
struct AmbientOcclusionBufferViews
{
    /// Create all resource views for given buffers.
    void createViews(const AmbientOcclusionBuffers &buffers, 
        res::d3d12::D3D12DescAllocatorInterface &descHeap, 
        ResourceViewType type);

    /// View for ambient occlusion texture.
    helpers::d3d12::D3D12ResourceView::PtrT ambientOcclusion{ };
}; // struct AmbientOcclusionBufferViews

/**
 * Rendering layer used for generation of ambient 
 * occlusion buffers.
 */
class AmbientOcclusionRL : public RenderLayerBase, public util::PointerType<AmbientOcclusionRL>
{
public:
    /**
     * Initialize the layer for given renderer.
     * @param renderer Renderer used for rendering the 
     * objects.
     */
    static PtrT create(RenderSubSystem &renderer);

    /// Free any resources.
    ~AmbientOcclusionRL();

    /**
     * Set buffers containing the deferred information.
     * @param buffers Container for the deferred buffers.
     */
    void setDeferredBuffers(DeferredBuffers buffers);

    /**
     * Resize the buffers used by this render layer for given 
     * screen size.
     * Should be called after resizing of the swap chain!
     * @param width New width in pixels.
     * @param height New height in pixels.
     */
    void resize(uint32_t width, uint32_t height);

    /**
     * Create GUI elements used by this render layer.
     * @param gui GUI sub-system used for the main GUI.
     * @warning Default context/window must be set before 
     * calling this method.
     */
    void prepareGui(gui::GuiSubSystem &gui);

    /**
     * Render current list of drawables into the output buffers.
     * @param camera Camera containing the view-projection 
     * matrix which should be used in rendering.
     * @param cmdList Command list which should contain the 
     * rendering commands.
     */
    void render(const helpers::math::Camera &camera, 
        res::d3d12::D3D12CommandList &cmdList);

    /**
     * Update GUI variables used by this render layer.
     * @param gui GUI sub-system used for the main GUI.
     * @warning GUI preparation for this render layer must 
     * already be executed.
     */
    void updateGui(gui::GuiSubSystem &gui);

    /// Get the current ambient occlusion buffers.
    AmbientOcclusionBuffers getAmbientOcclusionBuffers();

    /// Should the layer render ambient occlusion?
    void setEnabled(bool enabled);
private:
    /// Width and height of the ambient occlusion kernel.
    static constexpr std::size_t DEFAULT_AO_KERNEL_SIZE{ 16u };

    /// Width and height of the noise random texture.
    static constexpr std::size_t DEFAULT_AO_NOISE_TEXTURE_SIZE{ DEFAULT_AO_KERNEL_SIZE };

    /// Format of the output ambient occlusion texture.
    static constexpr ::DXGI_FORMAT DEFAULT_AMBIENT_OCCLUSION_RT_FORMAT{ ::DXGI_FORMAT_R8_UNORM };
    /// Default input assembler primitive topology.
    static constexpr ::D3D12_PRIMITIVE_TOPOLOGY DEFAULT_PRIMITIVE_TOPOLOGY{ ::D3D_PRIMITIVE_TOPOLOGY_TRIANGLELIST };
    /// Default pipeline primitive topology type.
    static constexpr ::D3D12_PRIMITIVE_TOPOLOGY_TYPE DEFAULT_PRIMITIVE_TOPOLOGY_TYPE{ ::D3D12_PRIMITIVE_TOPOLOGY_TYPE_TRIANGLE };
    
    /// Shortcut for buffer containing the slow-changing constants.
    using StaticConstantsBuffer = helpers::d3d12::D3D12InstancedBuffer<compatAmbientOcclusion::StaticConstantBuffer>;
    /// Shortcut for buffer containing the fast-changing constants.
    using DynamicConstantsBuffer = helpers::d3d12::D3D12InstancedBuffer<compatAmbientOcclusion::DynamicConstantBuffer>;

    /// Create descriptor heaps used for passing parameters to the shaders.
    res::d3d12::D3D12DescHeap::PtrT createDescHeap(std::size_t requiredDescriptors);

    /// Create descriptor heaps used for storing RTVs.
    res::d3d12::D3D12DescHeap::PtrT createRtvDescHeap(std::size_t requiredDescriptors);

    /// Create output textures used for storing ray tracing output.
    void createResizeTextures(uint32_t width = 1u, uint32_t height = 1u);

    /// Create the static and dynamic constant buffers.
    void createConstantBuffers();

    /**
     * Create the noise texture (float3) with random values. 
     * Only performed if the texture is not created yet.
     */
    void createNoiseTexture(uint32_t width, uint32_t height, 
        res::d3d12::D3D12CommandList &directCmdList);

    /**
     * Create the kernel texture (float3) with random values. 
     * Only performed if the texture is not created yet.
     */
    void createKernelTexture(uint32_t width, uint32_t height, 
        res::d3d12::D3D12CommandList &directCmdList);

    /// Initialize rendering resources.
    void initializeResources();

    /// Build pipeline using the current settings.
    void buildPipeline();

    /// Calculate number of descriptors needed in the miscellaneous descriptor table.
    uint32_t calculateMiscDescriptorCount();
    /// Create the miscellaneous descriptors table.
    helpers::d3d12::D3D12DescTable createMiscTable(res::d3d12::D3D12DescHeap &descHeap, 
        uint32_t numDescriptors);

    /// Calculate number of descriptors needed in the deferred descriptor table.
    uint32_t calculateDeferredDescriptorCount();
    /// Create the deferred descriptors table.
    helpers::d3d12::D3D12DescTable createDeferredTable(res::d3d12::D3D12DescHeap &descHeap, 
        uint32_t numDescriptors);

    /**
     * Create main descriptor heap and divide it into tables.
     */
    void createDescHeapLayout();

    /// Create the required descriptors in the descriptor table.
    void fillMiscTable(helpers::d3d12::D3D12DescTable &miscTable,
        StaticConstantsBuffer &staticConstants,
        DynamicConstantsBuffer &dynamicConstants,
        helpers::d3d12::D3D12TextureBuffer &noiseTexture,
        helpers::d3d12::D3D12TextureBuffer &kernelTexture);

    /**
     * Update the constant buffers with new camera values.
     * @param camera Currently used camera.
     */
    void updateCamera(const helpers::math::Camera &camera);

    /**
     * Update the constant buffers with new values.
     * This will trigger re-upload of buffers to the GPU if 
     * necessary.
     */
    void refreshConstantBuffers();

    /// Check and reload the shader programs, if requested.
    void checkReloadShaders();

    /// Render using this renderer.
    RenderSubSystem *mRenderer;

    /// Are resources ready for ray tracing?
    bool mPrepared{ false };

    /// Rendering resources.
    struct
    {
        /// Root signature of the main pass.
        res::d3d12::D3D12RootSignature::PtrT rootSig{ };

        /// Scissor rectangle for the viewport.
        ::CD3DX12_RECT scissorRect{ };
        /// Target viewport dimensions.
        ::CD3DX12_VIEWPORT viewport{ };

        /// Main vertex shader.
        res::d3d12::D3D12Shader::PtrT vShader{ };
        /// Main pixel shader.
        res::d3d12::D3D12Shader::PtrT pShader{ };

        /// Pipeline for the main pass.
        res::d3d12::D3D12PipelineState::PtrT pipelineState{ };

        /// Descriptor heap used for storing common shader parameters.
        res::d3d12::D3D12DescHeap::PtrT descHeap{ };

        /// Descriptor table used for storing miscellaneous descriptors.
        helpers::d3d12::D3D12DescTable miscTable{ };
        /// Descriptor table used for deferred descriptors.
        helpers::d3d12::D3D12DescTable deferredTable{ };

        /// Descriptor heap used for storing RTVs.
        res::d3d12::D3D12DescHeap::PtrT rtvDescHeap{ };

        /// Ambient occlusion rendering buffers.
        AmbientOcclusionBuffers ambientOcclusionBuffers{ };

        /// Slow-changing constant data.
        compatAmbientOcclusion::StaticConstantBuffer staticConstants{ };
        /// Dirty flag for the static constant data.
        bool staticConstantsChanged{ true };
        /// Slow-changing constant data buffer.
        StaticConstantsBuffer::PtrT staticConstantsBuffer{ };

        /// Fast-changing constant data.
        compatAmbientOcclusion::DynamicConstantBuffer dynamicConstants{ };
        /// Dirty flag for the dynamic constant data.
        bool dynamicConstantsChanged{ true };
        /// Fast-changing constant data buffer.
        DynamicConstantsBuffer::PtrT dynamicConstantsBuffer{ };

        /// Texture containing noise values.
        helpers::d3d12::D3D12TextureBuffer::PtrT noiseTexture{ };
        /// Texture containing kernel values.
        helpers::d3d12::D3D12TextureBuffer::PtrT kernelTexture{ };

        /// Currently used deferred buffers.
        DeferredBuffers deferredBuffers{ };
        /// Views to the deferred buffers.
        DeferredBufferViews deferredViews{ };

        /// Should shader reload be attempted?
        bool reloadShaders{ false };
    } mR;

    /// Configuration of rendering
    struct
    {
        /// Current width of the render target.
        float width{ 1.0f };
        /// Current height of the render target.
        float height{ 1.0f };

        /// Size of the AO kernel.
        uint32_t kernelSize{ DEFAULT_AO_KERNEL_SIZE };
        /// Size of the AO noise texture.
        uint32_t noiseSize{ DEFAULT_AO_NOISE_TEXTURE_SIZE };

        /// Should the ambient occlusion be rendered?
        bool enabled{ true };
    } mC;
protected:
    /**
     * Initialize the layer for given renderer.
     * @param renderer Renderer used for rendering the 
     * objects.
     */
    AmbientOcclusionRL(RenderSubSystem &renderer);
}; // class AmbientOcclusionRL

} // namespace rndr

} // namespace quark
