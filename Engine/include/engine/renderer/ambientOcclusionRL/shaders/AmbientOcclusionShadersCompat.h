/**
 * @file renderer/ambientOcclusionRL/shaders/AmbientOcclusionShadersCompat.h
 * @author Tomas Polasek
 * @brief Data structures shared between ambient occlusion pass shaders.
 */

#ifndef ENGINE_BASIC_AMBIENT_OCCLUSION_RL_SHADERS_COMPAT_H
#define ENGINE_BASIC_AMBIENT_OCCLUSION_RL_SHADERS_COMPAT_H

#ifdef HLSL
#   include "HlslCompatibility.h"
#else
#   include "engine/helpers/hlsl/HlslCompatibility.h"
#endif

/// Buffer containing global static data, which don't change very often.
struct StaticConstantBuffer
{
    HLSL_TYPE(uint) padding;
}; // struct StaticConstantBuffer

/// Buffer containing global data which may change per frame.
struct DynamicConstantBuffer
{
    /// Transform from world-space to camera space.
    HLSL_TYPE(float4x4) worldToCamera;
    /// Position of the camera in world coordinates.
    HLSL_TYPE(float3) cameraWorldPosition;
    /// Resolution of the window/canvas.
    HLSL_TYPE(float2) canvasResolution;
    /// Size of the noise texture.
    HLSL_TYPE(float2) noiseResolution;
    /// Size of the kernel texture.
    HLSL_TYPE(float2) kernelResolution;
}; // struct DynamicConstantBuffer

#ifndef HLSL

/// Helper structure used for passing HLSL register-space pairs.
struct HlslRegisterSpacePair
{
	uint32_t reg{ 0u };
	uint32_t space{ 0u };
}; // struct HlslRegisterSpacePair

/// Slow-changing constants buffer.
static constexpr HlslRegisterSpacePair StaticConstantsSlot{ 0u, 0u };
/// Fast-changing constants buffer.
static constexpr HlslRegisterSpacePair DynamicConstantsSlot{ 1u, 0u };
/// Common sampler used for sampling textures.
static constexpr HlslRegisterSpacePair SamplerSlot{ 0u, 0u };
/// Random noise texture.
static constexpr HlslRegisterSpacePair NoiseSlot{ 0u, 0u };
/// Kernel texture.
static constexpr HlslRegisterSpacePair KernelSlot{ 1u, 0u };
/// Array of deferred G-Buffer buffers.
static constexpr HlslRegisterSpacePair DeferredBuffersSlot{ 0u, 1u };

#else

/// Slow-changing constants.
ConstantBuffer<StaticConstantBuffer> gStatic : register(b0, space0);
/// Fast-changing constants.
ConstantBuffer<DynamicConstantBuffer> gDynamic : register(b1, space0);

/// Common sampler used for sampling the textures.
SamplerState gCommonSampler : register(s0, space0);

/// Texture containing noise vectors.
Texture2D<float3> gNoiseTexture : register(t0, space0);
/// Texture containing AO sampling kernel.
Texture2D<float3> gKernelTexture : register(t1, space0);

/// Get the deferred helpers.
#define DEFERRED_BUFFERS_SPACE space1
#include "hlslDeferredUser.h"

/// Data layout for input into the vertex shader.
struct VertexData
{
    /// Vertex identifier used for generation of fullscreen triangle.
    uint id : SV_VERTEXID;
}; // struct VertexData

/// Data layout for input into the pixel shader.
struct FragmentData
{
    /// Screen-space position.
    float4 ssPosition : SV_Position;
    /// Texture coordinate.
    float2 texCoord : TEXCOORD0;
}; // struct FragmentData

/// Data layout for output out of the pixel shader.
struct PixelData
{
    /// Output ambient occlusion.
    float ambientOcclusion : SV_Target0;
}; // struct PixelData

#endif

#endif // ENGINE_BASIC_AMBIENT_OCCLUSION_RL_SHADERS_COMPAT_H
