/**
 * @file renderer/deferredRL/DeferredRL.h
 * @author Tomas Polasek
 * @brief Rendering layer allowing the creation of 
 * G-Buffer and its use in deferred shading/rendering.
 */

#pragma once

#include "engine/renderer/RenderLayerBase.h"

#include "engine/renderer/RenderSubSystem.h"

#include "engine/resources/d3d12/D3D12Shader.h"
#include "engine/resources/d3d12/D3D12RootSignature.h"
#include "engine/resources/d3d12/D3D12InputLayout.h"
#include "engine/resources/d3d12/D3D12PipelineState.h"

#include "engine/resources/render/DepthStencilBuffer.h"
#include "engine/resources/render/DescriptorMgr.h"

#include "engine/resources/scene/DrawableListMgr.h"

#include "engine/helpers/d3d12/D3D12RootSignatureBuilder.h"
#include "engine/helpers/d3d12/D3D12PipelineStateBuilder.h"
#include "engine/helpers/d3d12/D3D12InstancedBuffer.h"

#include "engine/helpers/render/MaterialMeshCache.h"

#include "engine/helpers/math/Transform.h"
#include "engine/helpers/math/Camera.h"

/// Namespace containing the engine code.
namespace quark
{

// Forward declaration.
namespace gui
{
class GuiSubSystem;
}

/// Rendering classes.
namespace rndr
{

/// Structure compatibility with shaders.
namespace compatDeferred
{

#include "engine/renderer/deferredRL/shaders/DeferredShadersCompat.h"

/// Container for descriptor heap indexing.
namespace DescHeap
{

/// Indexing of miscellaneous descriptor table.
namespace MiscDescTable
{

// Indexes of descriptors within the table.
enum 
{
    /// Buffer containing slow-changing constants.
    StaticConstantsBuffer = 0u,
    /// Buffer containing fast-changing constants.
    DynamicConstantsBuffer,

    DescHeapSize
};

} // namespace MiscDescTable

} // namespace DescHeap

/// Container for descriptor heap indexing.
namespace RtvDescHeap
{

/// Indexing of RTV descriptor table.
namespace RtvDescTable
{

// Indexes of descriptors within the table.
enum 
{
    // G-Buffer: 
    /// Texture containing world-space coordinates.
    WorldPositionTexture = 0u, 
    /// Texture containing world-space normals.
    WorldNormalTexture,
    /// Texture containing diffuse color of materials.
    MaterialDiffuseTexture, 
    /// Texture containing metallic-roughness parameters.
    MetallicRoughnessTexture, 
    /// Texture containing depths from camera.
    DepthTexture, 

    DescHeapSize
};

} // namespace RtvDescTable

} // namespace RtvDescHeap

/// Container for global root signature root parameters.
namespace GlobalRS
{

// Indexes of root parameters within the global root signature.
enum 
{
    /// Slow-changing constants.
    StaticConstantBuffer = 0u,
    /// Fast-changing constants.
    DynamicConstantBuffer,
    /// Per-instance constants.
    InstanceConstantBuffer,
    /// List of material textures.
    TextureArray,
    /// Common sampler used for texture sampling.
    CommonSampler, 

    // G-Buffer: 
    /// Texture containing world-space coordinates.
    WorldPositionTexture, 
    /// Texture containing world-space normals.
    WorldNormalTexture,
    /// Texture containing diffuse color of materials.
    MaterialDiffuseTexture, 
    /// Texture containing metallic-roughness parameters.
    MetallicRoughnessTexture, 
    /// Texture containing depths from camera.
    DepthTexture, 

    Count
};

} // namespace GlobalRS

/// Specification of the input vertex layout.
namespace InputLayout
{

/// Indexes of input layout slots.
enum
{
    /// Float3 position.
    Position = 0u, 
    /// Float3 normal.
    Normal, 
    /// Float4 tangent.
    Tangent, 
    /// Float2 texture coordinate.
    TexCoord, 

    Count
};

} // namespace InputLayout

} // namespace compatDeferred

/// Helper structure used for passing deferred buffers.
struct DeferredBuffers
{
    /// Transition all buffers to given state.
    void transitionResourcesTo(res::d3d12::D3D12CommandList &cmdList, ::D3D12_RESOURCE_STATES state);

    /// Texture containing world-space positions.
    helpers::d3d12::D3D12TextureBuffer::PtrT worldPositionTexture{ };
    /// Texture containing world-space normals.
    helpers::d3d12::D3D12TextureBuffer::PtrT worldNormalTexture{ };
    /// Texture containing diffuse color of materials.
    helpers::d3d12::D3D12TextureBuffer::PtrT materialDiffuseTexture{ }; 
    /// Texture containing metallic-roughness parameters.
    helpers::d3d12::D3D12TextureBuffer::PtrT metallicRoughnessTexture{ }; 
    /// Texture containing depths from camera.
    helpers::d3d12::D3D12TextureBuffer::PtrT depthTexture{ };
}; // struct DeferredBuffers

/// Helper structure used for creating views to the deferred buffers.
struct DeferredBufferViews
{
    /// Create all resource views for given buffers.
    void createViews(const DeferredBuffers &buffers, 
        res::d3d12::D3D12DescAllocatorInterface &descHeap, 
        ResourceViewType type);

    /// View for world-space positions.
    helpers::d3d12::D3D12ResourceView::PtrT worldPositionView{ };
    /// View for world-space normals.
    helpers::d3d12::D3D12ResourceView::PtrT worldNormalView{ };
    /// View for diffuse color of materials.
    helpers::d3d12::D3D12ResourceView::PtrT materialDiffuseView{ }; 
    /// View for metallic-roughness parameters.
    helpers::d3d12::D3D12ResourceView::PtrT metallicRoughnessView{ }; 
    /// View for depths from camera.
    helpers::d3d12::D3D12ResourceView::PtrT depthView{ };

    /// Total number of deferred buffers.
    static constexpr uint32_t DEFERRED_BUFFER_COUNT{ 5u };
}; // struct DeferredBufferViews

/**
 * Rendering layer allowing the creation of 
 * G-Buffer and its use in deferred shading/rendering.
 */
class DeferredRL : public RenderLayerBase, public util::PointerType<DeferredRL>
{
public:
    /**
     * Initialize the layer for given renderer.
     * @param renderer Renderer used for rendering the 
     * objects.
     */
    static PtrT create(RenderSubSystem &renderer);

    /// Free any resources.
    ~DeferredRL();

    /**
     * Set a list of drawables which should be rendered by 
     * this render layer.
     * @param drawables Versioned list of drawables which will 
     * be used for rendering.
     */
    void setDrawablesList(const res::scene::DrawableListHandle &drawables);

    /**
     * Resize the buffers used by this render layer for given 
     * screen size.
     * Should be called after resizing of the swap chain!
     * @param width New width in pixels.
     * @param height New height in pixels.
     */
    void resize(uint32_t width, uint32_t height);

    /**
     * Check if the drawables list changed and perform required 
     * preparation for rendering if necessary.
     * @param sceneChanged Has the loaded scene changed?
     */
    void prepare(bool sceneChanged = false);

    /**
     * Create GUI elements used by this render layer.
     * @param gui GUI sub-system used for the main GUI.
     * @warning Default context/window must be set before 
     * calling this method.
     */
    void prepareGui(gui::GuiSubSystem &gui);

    /**
     * Render current list of drawables into the G-Buffer.
     * @param camera Camera containing the view-projection 
     * matrix which should be used in rendering.
     * @param cmdList Command list which should contain the 
     * rendering commands.
     */
    void render(const helpers::math::Camera &camera, 
        res::d3d12::D3D12CommandList &cmdList);

    /**
     * Update GUI variables used by this render layer.
     * @param gui GUI sub-system used for the main GUI.
     * @warning GUI preparation for this render layer must 
     * already be executed.
     */
    void updateGui(gui::GuiSubSystem &gui);

    /// Get currently used deferred buffers.
    DeferredBuffers getDeferredBuffers() const;

    /// Should fragments with transparency be discarded?
    void setDiscardTransparent(bool enabled);
private:
    /// Default format used for vertex positions.
    static constexpr ::DXGI_FORMAT DEFAULT_VERTEX_POSITION_FORMAT{ ::DXGI_FORMAT_R32G32B32_FLOAT };
    /// Default format used for vertex normals.
    static constexpr ::DXGI_FORMAT DEFAULT_VERTEX_NORMAL_FORMAT{ ::DXGI_FORMAT_R32G32B32_FLOAT };
    /// Default format used for vertex tangents.
    static constexpr ::DXGI_FORMAT DEFAULT_VERTEX_TANGENT_FORMAT{ ::DXGI_FORMAT_R32G32B32A32_FLOAT };
    /// Default format used for texture coordinates.
    static constexpr ::DXGI_FORMAT DEFAULT_VERTEX_TEXCOORD_FORMAT{ ::DXGI_FORMAT_R32G32_FLOAT };

    /// Default primitive topology type used by the main pipeline.
    static constexpr ::D3D12_PRIMITIVE_TOPOLOGY_TYPE DEFAULT_PRIMITIVE_TOPOLOGY_TYPE{ ::D3D12_PRIMITIVE_TOPOLOGY_TYPE_TRIANGLE };
    /// Default primitive topology used by the main pipeline.
    static constexpr ::D3D12_PRIMITIVE_TOPOLOGY DEFAULT_PRIMITIVE_TOPOLOGY{ ::D3D_PRIMITIVE_TOPOLOGY_TRIANGLELIST };

    /// Format of the world position render target.
    static constexpr ::DXGI_FORMAT DEFAULT_WORLD_POSITION_RT_FORMAT{ ::DXGI_FORMAT_R32G32B32A32_FLOAT };
    /// Format of the world normal render target.
    static constexpr ::DXGI_FORMAT DEFAULT_WORLD_NORMAL_RT_FORMAT{ ::DXGI_FORMAT_R32G32B32A32_FLOAT };
    /// Format of the material diffuse render target.
    static constexpr ::DXGI_FORMAT DEFAULT_MATERIAL_DIFFUSE_RT_FORMAT{ ::DXGI_FORMAT_R32G32B32A32_FLOAT };
    /// Format of the material properties render target.
    static constexpr ::DXGI_FORMAT DEFAULT_MATERIAL_PROPS_RT_FORMAT{ ::DXGI_FORMAT_R32G32B32A32_FLOAT };
    /// Format of the material depth render target.
    static constexpr ::DXGI_FORMAT DEFAULT_DEPTH_RT_FORMAT{ ::DXGI_FORMAT_R32_FLOAT };

    /// We are clearing everything to this value.
    static constexpr float DEFAULT_CLEAR_VALUE{ 0.0f };
    /// Depth clearing value.
    static constexpr float DEFAULT_DEPTH_CLEAR_VALUE{ 1.0f };

    /// Should transparent fragments be discarded?
    static constexpr bool DEFAULT_DISCARD_TRANSPARENT{ true };

    /// Shortcut for buffer containing the slow-changing constants.
    using StaticConstantsBuffer = helpers::d3d12::D3D12InstancedBuffer<compatDeferred::StaticConstantBuffer>;
    /// Shortcut for buffer containing the fast-changing constants.
    using DynamicConstantsBuffer = helpers::d3d12::D3D12InstancedBuffer<compatDeferred::DynamicConstantBuffer>;

    /// Create static and dynamic constant buffers.
    void createConstantBuffers();

    /// Initialize rendering resources.
    void initializeResources(uint32_t requiredTextures = 0u);

    /// Create rasterization pipeline for given texture count.
    void createPipeline(uint32_t requiredTextures);

    /// Build the pipeline with current settings.
    void buildPipeline();

    /**
     * Prepare materials used in given list of drawables for 
     * next render().
     * @param materialMeshCache Cache to use.
     * @param drawables List of drawables to prepare.
     */
    void prepareMaterials(helpers::rndr::MaterialMeshCache &materialMeshCache, 
        res::scene::DrawableList &drawables);

    /**
     * Prepare commands used for rendering drawables into 
     * provided draw bundle.
     * @param drawables List of drawables to prepare.
     * @param drawBundle Draw bundle which should contain 
     * all of the draw commands.
     */
    void prepareDrawBundle(res::scene::DrawableList &drawables, 
        res::d3d12::D3D12CommandList &drawBundle);

    /**
     * Create a new command list which can be used as a drawing 
     * command bundle.
     * @param renderer Currently used renderer.
     * @return Returns pointer to the new command bundle.
     */
    res::d3d12::D3D12CommandList::PtrT createDrawBundle(RenderSubSystem &renderer);

    /// Get the number of miscellaneous descriptors
    uint32_t calculateMiscDescriptorCount();

    /// Get the number of texture descriptors.
    uint32_t calculateTextureDescriptorCount(helpers::rndr::MaterialMeshCache &cache);

    /// Create SRV/UAV/CBV descriptor heap for specified number of descriptors.
    res::d3d12::D3D12DescHeap::PtrT createDescHeap(std::size_t requiredDescriptors);

    /// Create the descriptor table for miscellaneous descriptors.
    helpers::d3d12::D3D12DescTable createMiscTable(res::d3d12::D3D12DescHeap &descHeap, 
        uint32_t numDescriptors);

    /// Create the descriptor table for texture descriptors.
    helpers::d3d12::D3D12DescTable createTextureTable(res::d3d12::D3D12DescHeap &descHeap, 
        uint32_t numDescriptors);

    /**
     * Create the descriptor heap and its tables according to 
     * requirements.
     * @param descHeap Descriptor heap which should be created.
     * @param textureTable Descriptor table which will contain 
     * material texture descriptors.
     * @param miscTable Descriptor table for miscellaneous descriptors.
     * @param cache Filled cache used for materials.
     */
    void createDescHeapLayout(res::d3d12::D3D12DescHeap::PtrT &descHeap, 
        helpers::d3d12::D3D12DescTable &textureTable, 
        helpers::d3d12::D3D12DescTable &miscTable, 
        helpers::rndr::MaterialMeshCache &cache);

    /// Fill miscellaneous table with descriptors.
    void fillMiscTable(helpers::d3d12::D3D12DescTable &miscTable);

    /**
     * Build the material/mesh cache and prepare material 
     * texture descriptors.
     * @param cache Material cache used.
     * @param textureTable Table which should contain the 
     * texture descriptors.
     */
    void buildMaterialMeshCache(helpers::rndr::MaterialMeshCache &cache, 
        helpers::d3d12::D3D12DescTable &textureTable);

    /// Calculate the number of required RTV descriptors.
    uint32_t calculateRtvDescriptorCount();

    /// Create a new RTV descriptor heap with requested number of descriptors.
    res::d3d12::D3D12DescHeap::PtrT createRtvDescHeap( uint32_t requiredDescriptors);

    /// Create descriptor table for RTV descriptors.
    helpers::d3d12::D3D12DescTable createRtvTable(res::d3d12::D3D12DescHeap &rtvDescHeap, 
        uint32_t numDescriptors);

    /**
     * Create descriptor heap and table for RTVs.
     * @param rtvDescHeap Current descriptor heap pointer which 
     * will be rewritten if necessary.
     * @param rtvTable Descriptor table for RTVs.
     */
    void createRtvDescHeapAndLayout(
        res::d3d12::D3D12DescHeap::PtrT &rtvDescHeap, 
        helpers::d3d12::D3D12DescTable &rtvTable);

    /// Fill the provided table with required RTV descriptors.
    void fillRtvTable(helpers::d3d12::D3D12DescTable &rtvTable);

    /**
     * Prepare given list of drawables for next render().
     * @param drawables List of drawable to prepare.
     * @param sceneChanged Has the loaded scene changed?
     */
    void prepareForRendering(res::scene::DrawableList &drawables, 
        bool sceneChanged);

    /**
     * Create or resize already existing G-Buffer textures and 
     * the depth buffer.
     * @param width New width of the viewport.
     * @param height New height of the viewport.
     */
    void createResizeTextures(uint32_t width, uint32_t height);

    /**
     * Create or reallocate descriptors to already allocated G-Buffer 
     * textures.
     */
    void createTextureDescriptors();

    /**
     * Update the constant buffers with new camera values.
     * @param camera Currently used camera.
     */
    void updateCamera(const helpers::math::Camera &camera);

    /**
     * Update the constant buffers with new values.
     */
    void updateConstants();

    /**
     * Update the constant buffers with new values.
     * This will trigger re-upload of buffers to the GPU if 
     * necessary.
     */
    void refreshConstantBuffers();

    /// Check and reload the shader programs, if requested.
    void checkReloadShaders();

    /// Clear the G-Buffers to the default values.
    void clearDeferredBuffers(res::d3d12::D3D12CommandList &cmdList);

    /**
     * Set all descriptors heap required for rendering.
     * @param cmdList Command list to set the descriptor heaps 
     * on.
     */
    void setDescHeaps(res::d3d12::D3D12CommandList &cmdList);

    /**
     * Set all required root signature parameters.
     * @param cmdList Command list to set the root signature 
     * parameters on.
     */
    void setRootSignatureParameters(res::d3d12::D3D12CommandList &cmdList);

    /// Render using this renderer.
    RenderSubSystem *mRenderer;

    /// Mutable list of drawables which are rendered by this render layer.
    res::scene::DrawableListHandle mDrawables{ };
    /// Which last version of drawables has been prepared.
    res::scene::DrawableList::VersionT mLastPreparedVersion{ };

    /// Is deferred rendering ready to render current drawables?
    bool mPrepared{ false };

    /// Rendering resources.
    struct
    {
        /// Scissor rectangle for the viewport.
        ::CD3DX12_RECT scissorRect{ };
        /// Target viewport dimensions.
        ::CD3DX12_VIEWPORT viewport{ };

        /// Root signature of the main pass.
        res::d3d12::D3D12RootSignature::PtrT rootSig{ };
        /// Input layout for the main pass.
        res::d3d12::D3D12InputLayout inputLayout{ };
        /// Main vertex shader.
        res::d3d12::D3D12Shader::PtrT vShader{ };
        /// Main pixel shader.
        res::d3d12::D3D12Shader::PtrT pShader{ };
        /// Pipeline for the main pass.
        res::d3d12::D3D12PipelineState::PtrT pipelineState{ };

        /// Depth-stencil buffer used for the main pass.
        res::rndr::DepthStencilBuffer::PtrT depthStencilTexture{ };

        /// Deferred rendering buffers.
        DeferredBuffers deferredBuffers{ };

        /// Slow-changing constant data.
        compatDeferred::StaticConstantBuffer staticConstants{ };
        /// Dirty flag for the static constant data.
        bool staticConstantsChanged{ true };
        /// Slow-changing constant data buffer.
        StaticConstantsBuffer::PtrT staticConstantsBuffer{ };

        /// Fast-changing constant data.
        compatDeferred::DynamicConstantBuffer dynamicConstants{ };
        /// Dirty flag for the dynamic constant data.
        bool dynamicConstantsChanged{ true };
        /// Fast-changing constant data buffer.
        DynamicConstantsBuffer::PtrT dynamicConstantsBuffer{ };

        /**
         * Command bundle containing rendering commands for the 
         * currently prepared drawables.
         */
        res::d3d12::D3D12CommandList::PtrT renderBundle{ };

        /// Main descriptor heap used for storing SRVs, UAVs and CBVs.
        res::d3d12::D3D12DescHeap::PtrT descHeap{ };
        /// Descriptor table used for material cache textures.
        helpers::d3d12::D3D12DescTable textureTable{ };
        /// Descriptor table used for misc descriptors.
        helpers::d3d12::D3D12DescTable miscTable{ };

        /// Descriptor heap used for RTVs.
        res::d3d12::D3D12DescHeap::PtrT rtvDescHeap{ };
        /// Descriptor table used for G-Buffer textures and others.
        helpers::d3d12::D3D12DescTable rtvTable{ };

        /// Cache of materials/meshes for the current list of drawable.
        helpers::rndr::MaterialMeshCache::PtrT materialMeshCache;

        /// Should shader reload be attempted?
        bool reloadShaders{ false };
    } mR;
protected:
    /**
     * Initialize the layer for given renderer.
     * @param renderer Renderer used for rendering the 
     * objects.
     */
    DeferredRL(RenderSubSystem &renderer);
}; // class DeferredRL

} // namespace rndr

} // namespace quark
