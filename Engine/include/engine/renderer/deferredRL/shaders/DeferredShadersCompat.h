/**
 * @file renderer/deferredRL/shaders/DeferredShadersCompat.h
 * @author Tomas Polasek
 * @brief Data structures shared between deferred rendering shaders.
 */

#ifndef ENGINE_BASIC_DEFERRED_RL_SHADERS_COMPAT_H
#define ENGINE_BASIC_DEFERRED_RL_SHADERS_COMPAT_H

#ifdef HLSL
#   include "HlslCompatibility.h"
#else
#   include "engine/helpers/hlsl/HlslCompatibility.h"
#endif

/// Buffer containing global static data, which don't change very often.
struct StaticConstantBuffer
{
    /// Switch used to change the performed function.
    HLSL_TYPE(uint) mode;
    /// Should fragments with transparency be discarded?
    HLSL_TYPE(uint) discardTransparent;
}; // struct StaticConstantBuffer

/// Buffer containing global data which may change per frame.
struct DynamicConstantBuffer
{
    /// Transform from world-space to screen-space (vp).
    HLSL_TYPE(float4x4) worldToCamera;
    /// Position of the camera in world coordinates.
    HLSL_TYPE(float3) cameraWorldPosition;
}; // struct DynamicConstantBuffer

/// Buffer containing dynamic per-object data.
struct InstanceConstantBuffer
{
    /// Transform from model-space to world-space.
    HLSL_TYPE(float4x4) modelToWorld;
    /// Transform from model-space to world-space, usable for normals.
    HLSL_TYPE(float4x4) normalToWorld;
    /// Index of the diffuse texture within the main descriptor table.
    HLSL_TYPE(uint) diffuseIdx;
    /// Index of the normal texture within the main descriptor table.
    HLSL_TYPE(uint) normalIdx;
    /// Index of the material texture within the main descriptor table.
    HLSL_TYPE(uint) materialIdx;
    /// Does the material generate specular reflections?
    HLSL_TYPE(uint) specularReflections;
    /// Does the material generate "specular" refraction?
    HLSL_TYPE(uint) specularRefractions;
}; // struct DynamicConstantBuffer

#ifndef HLSL

/// Helper structure used for passing HLSL register-space pairs.
struct HlslRegisterSpacePair
{
	uint32_t reg{ 0u };
	uint32_t space{ 0u };
}; // struct HlslRegisterSpacePair

/// Slow-changing constants buffer.
static constexpr HlslRegisterSpacePair StaticConstantsSlot{ 0u, 0u };
/// Fast-changing constants buffer.
static constexpr HlslRegisterSpacePair DynamicConstantsSlot{ 1u, 0u };
/// Per-object dynamic constants buffer.
static constexpr HlslRegisterSpacePair InstanceConstantsSlot{ 2u, 0u };
/// Array of material textures.
static constexpr HlslRegisterSpacePair TextureArraySlot{ 0u, 0u };
/// Common sampler for material textures.
static constexpr HlslRegisterSpacePair SamplerSlot{ 0u, 0u };

#else

/// Slow-changing constants.
ConstantBuffer<StaticConstantBuffer> gStatic : register(b0, space0);
/// Fast-changing constants.
ConstantBuffer<DynamicConstantBuffer> gDynamic : register(b1, space0);
/// Per-object dynamic data.
ConstantBuffer<InstanceConstantBuffer> gInstance : register(b2, space0);

/**
 * Textures used in rendering, contains all of the material 
 * textures as well.
 * Upper bound is set to allow debugging and validation.
 */
Texture2D gTextures[HLSL_TEXTURE_UPPER_BOUND] : register(t0, space0);

/// Common sampler used for sampling the textures.
SamplerState gCommonSampler : register(s0, space0);

/// Data layout for input into the vertex shader.
struct VertexData
{
    /// Model-space position.
    float3 position : POSITION;
    /// Model-space normal.
    float3 normal : NORMAL;
    /// Model-space tangent.
    float3 tangent : TANGENT;
    /// Texture coordinate.
    float2 texCoord : TEXCOORD;
}; // struct VertexData

/// Data layout for input into the pixel shader.
struct FragmentData
{
    /// Model-space normal.
    float3 mNormal : NORMAL0;
    /// Model-space tangent.
    float3 mTangent : TANGENT0;
    /// Screen-space position.
    float4 ssPosition : SV_Position;
    /// World-space position.
    float3 wsPosition : POSITION;
    /// World-space normal.
    float3 wsNormal : NORMAL1;
    /// Texture coordinate.
    float2 texCoord : TEXCOORD0;
    /// Screen-space position for depth.
    float4 ssDepthPosition : TEXCOORD1;
}; // struct FragmentData

/// Data layout for output out of the pixel shader.
struct PixelData
{
    /// World-space position.
    float4 wsPosition : SV_Target0;
    /// World-space normal.
    float4 wsNormal : SV_Target1;
    /// Material diffuse color.
    float4 diffuse : SV_Target2;
    /// Material metallic-roughness values + other material properties.
    float4 materialProps : SV_Target3;
    /// Depth of the fragment from camera.
    float depth : SV_Target4;
}; // struct PixelData

#endif

#endif // ENGINE_BASIC_DEFERRED_RL_SHADERS_COMPAT_H
