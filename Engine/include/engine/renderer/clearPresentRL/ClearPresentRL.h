/**
 * @file renderer/ClearPresentRL/ClearPresentRL.h
 * @author Tomas Polasek
 * @brief Rendering layer allowing clearing and presenting buffers in 
 * a flip-discard swap chain.
 */

#pragma once

#include "engine/renderer/RenderLayerBase.h"

#include "engine/resources/d3d12/D3D12CommandQueueMgr.h"
#include "engine/lib/dxtk.h"

/// Namespace containing the engine code.
namespace quark
{

/// Rendering classes.
namespace rndr
{

/**
 * Basic rendering layer allowing clearing and presenting buffers 
 * in a flip-discard swap chain.
 */
class ClearPresentRL : public RenderLayerBase, public util::PointerType<ClearPresentRL>
{
public:
    /// Create clear-present rendering layer for given renderer.
    static PtrT create(RenderSubSystem &renderer);

    /// No resource to free.
    ~ClearPresentRL() = default;

    /**
     * Clear the current backbuffer in the main swap chain.
     * @param clearColor Which color should be used for 
     * clear background.
     * @return Returns direct command list containing the clear 
     * command.
     * @throws Throws util::winexception on error.
     */
    res::d3d12::D3D12CommandList::PtrT clear(dxtk::math::Color clearColor = { 1.0f, 0.0f, 1.0f, 1.0f });

    /**
     * Present the current backbuffer in the main swap chain.
     * @param cmdList Command list to execute before presenting.
     * @param useVSync Should vSync be used?
     * @param useTearing Should tearing be enabled? Only works 
     * when vsync is also disabled.
     * @throws Throws util::winexception on error.
     */
    void present(res::d3d12::D3D12CommandList::PtrT cmdList, bool useVSync, bool useTearing);
private:
    /**
     * Set fence value for current backbuffer index.
     * @param value Value of the fence.
     */
    void setCurrentBackbufferFence(uint64_t value);

    /**
     * Get fence value for current backbuffer index.
     * @return Returns fence value for the current 
     * backbuffer index.
     */
    uint64_t getCurrentBackbufferFence();

    /// Using this renderer.
    RenderSubSystem &mRenderer;
    /**
     * Each element contains a fence value which must 
     * be reached before reusing swap-chain backbuffer 
     * with corresponding index.
     */
    std::vector<uint64_t> mFrameFenceValues;
protected:
    /// Create clear-present rendering layer for given renderer.
    ClearPresentRL(RenderSubSystem &renderer);
}; // class ClearPresentRL

} // namespace rndr

} // namespace quark
