/**
 * @file renderer/ImGuiRL/ImGuiRL.h
 * @author Tomas Polasek
 * @brief Rendering layer for drawing GUI using ImGUI.
 */

#pragma once

#include "engine/renderer/RenderLayerBase.h"

#include "engine/gui/GuiModel.h"

#include "engine/helpers/d3d12/D3D12ImGuiWrapper.h"

/// Namespace containing the engine code.
namespace quark
{

/// Rendering classes.
namespace rndr
{

/**
 * Rendering layer for drawing GUI using ImGUI.
 */
class ImGuiRL : public RenderLayerBase, public util::PointerType<ImGuiRL>
{
public:
    /// Create ImGUI rendering layer for given renderer.
    static PtrT create(RenderSubSystem &renderer);

    /// Free ImGUI resources.
    ~ImGuiRL();

    /**
     * Render ImGui frame and store draw commands on 
     * provided command list.
     * @param targetRTV Render target where the gui is rendered.
     * @param model Model of the GUI, which should be rendered.
     * @param cmdList Command list to draw with.
     */
    void render(const ::CD3DX12_CPU_DESCRIPTOR_HANDLE &targetRTV,
        const gui::GuiModel &model, res::d3d12::D3D12CommandList &cmdList);

    /**
     * Resize the rendering buffers used by ImGUI.
     * Should be called before resizing of the swap chain!
     * @param width New width in pixels.
     * @param height New height in pixels.
     */
    void resizeBeforeSwapChain(uint32_t width, uint32_t height);

    /**
     * Finalize resizing of the ImGUI buffers.
     * Should be called after resizing of the swap chain!
     */
    void resizeAfterSwapChain();
private:
    /// Using this renderer.
    RenderSubSystem &mRenderer;
    /// ImGui helper object, used for drawing GUI.
    helpers::d3d12::D3D12ImGuiWrapper mImGui;

    // Rendering configuration.
    struct
    {
        /// Currently used viewport width.
        uint32_t width{ 0u };
        /// Currently used viewport height.
        uint32_t height{ 0u };
    } mC;
protected:
    /// Create ImGUI rendering layer for given renderer.
    ImGuiRL(RenderSubSystem &renderer);
}; // class ImGuiRL

} // namespace rndr

} // namespace quark
