/**
 * @file renderer/basicRayTracing/BasicTexturedRL.h
 * @author Tomas Polasek
 * @brief Rendering layer allowing basic hardware 
 * accelerated ray tracing.
 */

#pragma once

#ifdef ENGINE_RAY_TRACING_ENABLED

#include "engine/renderer/RenderLayerBase.h"
#include "engine/renderer/RenderSubSystem.h"

#include "engine/renderer/deferredRL/DeferredRL.h"

#include "engine/resources/scene/DrawableListMgr.h"

#include "engine/helpers/d3d12/D3D12RTAccelerationBuilder.h"
#include "engine/helpers/d3d12/D3D12RTPipeline.h"
#include "engine/helpers/d3d12/D3D12RTShaderTable.h"
#include "engine/helpers/d3d12/D3D12InstancedBuffer.h"

#include "engine/helpers/render/MaterialMeshCache.h"

#include "engine/helpers/math/Camera.h"


/// Namespace containing the engine code.
namespace quark
{

// Forward declaration.
namespace gui
{
class GuiSubSystem;
}

/// Rendering classes.
namespace rndr
{

/// Structure compatibility with shaders.
namespace compatBasicRt
{

#include "engine/renderer/basicRayTracingRL/shaders/RayTracingShadersCompat.h"

/// Container for descriptor heap indexing.
namespace DescHeap
{

/// Indexing of miscellaneous descriptor table.
namespace MiscDescTable
{

// Indexes of descriptors within the table.
enum 
{
    /// Texture used as output for ambient occlusion.
    AmbientOcclusionOutput = 0u,
    /// Texture used as output for shadow occlusion.
    ShadowOcclusionOutput,
    /// Texture used as output for lighting.
    LightingOutput,
    /// Buffer containing slow-changing constants.
    StaticConstantsBuffer,
    /// Buffer containing fast-changing constants.
    DynamicConstantsBuffer,
    /// Texture containing float3 noise values.
    NoiseTexture, 

    DescHeapSize
};

} // namespace MiscDescTable

} // namespace DescHeap

/// Container for global root signature root parameters.
namespace GlobalRS
{

// Indexes of root parameters within the global root signature.
enum 
{
    /// Array of output of the ray tracing.
    OutputTextures = 0u,
    /// Current scene AS.
    AccelerationStructureSlot,
    /// Slow-changing constants.
    StaticConstantBuffer,
    /// Fast-changing constants.
    DynamicConstantBuffer,
    /// Per-instance constants.
    InstanceConstantBuffer,
    /// List of material textures.
    TextureArray,
    /// List of geometry buffers.
    BufferArray,
    /// Texture containing float3 noise values.
    NoiseTexture, 

    /// G-Buffer deferred buffers.
    DeferredBuffers, 

    Count
};

} // namespace GlobalRS

/// Container for hit-group local root signature root parameters.
namespace HitGroupLocalRS
{

// Indexes of root parameters within the hit-group local root signature.
enum 
{
    /// Specification of material and mesh used by the geometry.
    GeometryConstantBuffer, 

    Count
};

} // namespace HitGroupLocalRS

} // namespace compat

/// Helper structure used for passing ray tracing buffers.
struct RayTracingBuffers
{
    /// Transition all buffers to given state.
    void transitionResourcesTo(res::d3d12::D3D12CommandList &cmdList, ::D3D12_RESOURCE_STATES state);

    /// Texture containing ambient occlusion results - 1.0 means fully occluded.
    helpers::d3d12::D3D12TextureBuffer::PtrT ambientOcclusion{ };
    /// Texture containing shadow results - 1.0 means fully occluded.
    helpers::d3d12::D3D12TextureBuffer::PtrT shadowOcclusion{ };
    /// Texture containing lighting color results.
    helpers::d3d12::D3D12TextureBuffer::PtrT lightingColor{ };
}; // struct RayTracingBuffers

/// Helper structure used for creating views to the deferred buffers.
struct RayTracingBufferViews
{
    /// Create all resource views for given buffers.
    void createViews(const RayTracingBuffers &buffers, 
        res::d3d12::D3D12DescAllocatorInterface &descHeap, 
        ResourceViewType type);

    /// View for ambient occlusion texture.
    helpers::d3d12::D3D12ResourceView::PtrT ambientOcclusion{ };
    /// View for shadow texture.
    helpers::d3d12::D3D12ResourceView::PtrT shadowOcclusion{ };
    /// View for lighting color texture.
    helpers::d3d12::D3D12ResourceView::PtrT lightingColor{ };

    /// Total number of deferred buffers.
    static constexpr uint32_t RAY_TRACING_BUFFER_COUNT{ 3u };
}; // struct RayTracingBufferViews

/**
 * Rendering layer allowing basic hardware 
 * accelerated ray tracing.
 */
class BasicRayTracingRL : public RenderLayerBase, public util::PointerType<BasicRayTracingRL>
{
public:
    /// Debugging modes.
    enum class DebugMode
    {
        /// Display the final color.
        FinalColor = 0, 
        /// Display the final color without hybrid rendering.
        FinalColorWithPrimary, 

        /// Display the base color from the texture.
        DebugTextureBaseColor,
        /// Display the PBR properties texture.
        DebugTexturePbrProperties,
        /// Display the normals texture.
        DebugTextureNormals,
        /// Display normals.
        DebugNormal, 
        /// Display world-space normals.
        DebugWorldNormal, 
        /// Display tangents.
        DebugTangent,
        /// Display barycentric coordinates.
        DebugBarycentrics,
        /// Display reflection-refraction.
        DebugReflectionRefraction,
        /// Display reflection color.
        DebugReflections,

        Count
    }; // enum class DisplayModes

    /**
     * Initialize the layer for given renderer.
     * @param renderer Renderer used for rendering the 
     * objects.
     */
    static PtrT create(RenderSubSystem &renderer);

    /// Free any resources.
    ~BasicRayTracingRL();

    /**
     * Set a list of drawables which should be rendered by 
     * this render layer.
     * @param drawables Versioned list of drawables which will 
     * be used for rendering.
     */
    void setDrawablesList(const res::scene::DrawableListHandle &drawables);

    /**
     * Set buffers containing the deferred information.
     * @param buffers Container for the deferred buffers.
     */
    void setDeferredBuffers(DeferredBuffers buffers);

    /**
     * Resize the buffers used by this render layer for given 
     * screen size.
     * Should be called after resizing of the swap chain!
     * @param width New width in pixels.
     * @param height New height in pixels.
     */
    void resize(uint32_t width, uint32_t height);

    /**
     * Check if the drawables list changed and perform required 
     * preparation for rendering if necessary.
     * @param directCmdList Command list used to record the building 
     * commands. This command list has to be executed before running 
     * any ray tracing dispatches.
     * @param sceneChanged Has the loaded scene changed?
     */
    void prepare(res::d3d12::D3D12CommandList &directCmdList, 
        bool sceneChanged = false);

    /**
     * Create GUI elements used by this render layer.
     * @param gui GUI sub-system used for the main GUI.
     * @warning Default context/window must be set before 
     * calling this method.
     */
    void prepareGui(gui::GuiSubSystem &gui);

    /**
     * Render current list of drawables into the output buffers.
     * @param camera Camera containing the view-projection 
     * matrix which should be used in rendering.
     * @param output Output used when using pure ray tracing.
     * @param cmdList Command list which should contain the 
     * rendering commands.
     */
    void render(const helpers::math::Camera &camera, 
        res::d3d12::D3D12Resource &output, 
        res::d3d12::D3D12CommandList &cmdList);

    /**
     * Update GUI variables used by this render layer.
     * @param gui GUI sub-system used for the main GUI.
     * @warning GUI preparation for this render layer must 
     * already be executed.
     */
    void updateGui(gui::GuiSubSystem &gui);

    /**
     * Set the debugging mode, which changes the output in 
     * the lightingColor texture.
     * @param mode The debug mode to set.
     */
    void setDebugMode(DebugMode mode);

    /// Get the current global light direction.
    const dxtk::math::Vector3 &getLightDirection() const;
    /// Set new global light direction.
    void setLightDirection(const dxtk::math::Vector3 &lightDir);

    /// Get the current global light color.
    const dxtk::math::Vector3 &getLightColor() const;
    /// Set new global light color.
    void setLightColor(const dxtk::math::Vector3 &lightColor);

    /// Get the current global background color.
    const dxtk::math::Vector3 &getBackgroundColor() const;

    /// Get the current number of rays cast per frame.
    uint32_t raysPerFrame() const;

    /// Are the G-Buffers used in the current mode?
    bool deferredBuffersRequired() const;

    /// Doe we need to use resolve pass?
    bool resolveRequired() const;

    /// Get number of rays cast per pixel, for current display mode
    uint32_t raysPerPixel() const;

    /// Get number of rays cast per pixel, for given display mode
    static uint32_t raysPerPixel(DebugMode mode, uint32_t shadowRays, 
        uint32_t aoRays, bool doReflections);

    /// Get currently used output buffers.
    RayTracingBuffers getRayTracingBuffers() const;

    /// Get statistics about the current acceleration structures.
    std::string accelerationStructureStatisticsString() const;

    /// Get statistics about the current acceleration structures.
    const helpers::d3d12::D3D12RTAccelerationBuilder::Statistics &accelerationStructureStatistics() const;

    /// Should bottom level acceleration structures be duplicated for same meshes?
    void setBlasDuplication(bool enabled);

    /// Should the acceleration structure builder build more optimized structures?
    void setBuildOptimized(bool enabled);
private:
#ifdef ENGINE_PROFILE_RAY_TRACING
    /// Name of the file containing the compiled shader code.
    static constexpr const wchar_t *SHADER_FILENAME{ L"BasicRayTracingProf.cso" };
#else // ENGINE_PROFILE_RAY_TRACING
    /// Name of the file containing the compiled shader code.
    static constexpr const wchar_t *SHADER_FILENAME{ L"BasicRayTracing.cso" };
#endif // ENGINE_PROFILE_RAY_TRACING

    /// Name of the main ray generation shader.
    static constexpr const wchar_t *MAIN_RAY_GEN_NAME{ L"BasicRayGenShader" };
    /// Name of the main miss shader.
    static constexpr const wchar_t *MAIN_MISS_NAME{ L"BasicMissShader" };
    /// Name of the main closest hit shader.
    static constexpr const wchar_t *MAIN_CLOSEST_HIT_NAME{ L"BasicClosestHitShader" };
    /// Name of the main hit group.
    static constexpr const wchar_t *MAIN_HIT_GROUP_NAME{ L"BasicHitGroup" };

    /// Name of the AO/shadow miss shader.
    static constexpr const wchar_t *SHADOW_MISS_NAME{ L"ShadowMissShader" };

#ifndef ENGINE_PROFILE_RAY_TRACING
    /// Name of the distance closest hit shader.
    static constexpr const wchar_t *DISTANCE_CLOSEST_HIT_NAME{ L"DistanceClosestHitShader" };
    /// Name of the distance miss shader.
    static constexpr const wchar_t *DISTANCE_MISS_NAME{ L"DistanceMissShader" };
    /// Name of the distance hit group.
    static constexpr const wchar_t *DISTANCE_HIT_GROUP_NAME{ L"DistanceHitGroup" };
#endif // !ENGINE_PROFILE_RAY_TRACING

    /// Size of the ray payload: 4 floats for color RGBA.
    static constexpr std::size_t MAIN_MAX_PAYLOAD_SIZE{ sizeof(compatBasicRt::RayPayload) };
    /// Size of the ray attributes: 2 floats for barycentric coordinates UV.
    static constexpr std::size_t MAIN_MAX_ATTRIBUTES_SIZE{ sizeof(compatBasicRt::MyTriangleAttributes) };

    /// Maximum recursion depth: 1 for primary rays only.
    static constexpr std::size_t MAIN_MAX_RECURSION_DEPTH{ 4u };

    /// Width and height of the noise random texture.
    static constexpr std::size_t NOISE_TEXTURE_SIZE{ 4u };

    // Default direction of the global light.
    static constexpr float DEFAULT_LIGHT_DIR_X{ -0.058f };
    static constexpr float DEFAULT_LIGHT_DIR_Y{ -0.084f };
    static constexpr float DEFAULT_LIGHT_DIR_Z{ -0.001f };
    // Default color of the global light.
    static constexpr float DEFAULT_LIGHT_COLOR_R{ 1.0f };
    static constexpr float DEFAULT_LIGHT_COLOR_G{ 1.0f };
    static constexpr float DEFAULT_LIGHT_COLOR_B{ 1.0f };
    // Default color of the sky.
    static constexpr float DEFAULT_BACKGROUND_COLOR_R{ 0.53f };
    static constexpr float DEFAULT_BACKGROUND_COLOR_G{ 0.82f };
    static constexpr float DEFAULT_BACKGROUND_COLOR_B{ 0.98f };

    /// Default number of ambient occlusion rays.
    static constexpr uint32_t DEFAULT_AO_RAY_COUNT{ 4u };
    /// Should AO use distance rays for smoother effect?
    static constexpr bool DEFAULT_AO_USE_SMOOTH{ true };
    /// Default number of shadow rays.
    static constexpr uint32_t DEFAULT_SHADOW_RAY_COUNT{ 4u };
    /// Default secondary shadow ray jitter.
    static constexpr float DEFAULT_SHADOW_RAY_JITTER{ 0.0003f };
    /// Default ambient occlusion ray range.
    static constexpr float DEFAULT_AO_RAY_RANGE{ 0.5f };
    /// Default value for rendering of reflections.
    static constexpr bool DEFAULT_DO_REFLECTIONS{ true };
    /// Default distribution value used for generating rays.
    static constexpr float DEFAULT_RAY_DISTRIBUTION{ 0.01f };

    /// Format of the output ambient occlusion texture.
    static constexpr ::DXGI_FORMAT DEFAULT_AMBIENT_OCCLUSION_RT_FORMAT{ ::DXGI_FORMAT_R8_UNORM };
    /// Format of the output shadow texture.
    static constexpr ::DXGI_FORMAT DEFAULT_SHADOW_OCCLUSION_RT_FORMAT{ ::DXGI_FORMAT_R8_UNORM };
    /// Format of the output lighting color texture.
    static constexpr ::DXGI_FORMAT DEFAULT_LIGHTING_COLOR_RT_FORMAT{ ::DXGI_FORMAT_R8G8B8A8_UNORM };
    
    /// Shortcut for buffer containing the slow-changing constants.
    using StaticConstantsBuffer = helpers::d3d12::D3D12InstancedBuffer<compatBasicRt::StaticConstantBuffer>;
    /// Shortcut for buffer containing the fast-changing constants.
    using DynamicConstantsBuffer = helpers::d3d12::D3D12InstancedBuffer<compatBasicRt::DynamicConstantBuffer>;
    /// Shortcut for buffer containing the per-instance constants.
    using InstanceConstantsBuffer = helpers::d3d12::D3D12InstancedBuffer<compatBasicRt::PerInstanceData, 1u>;

    /// Create root signatures used by the ray tracing shaders.
    void createRootSignatures(uint32_t requiredTextures = 0u, uint32_t requiredBuffers = 0u);

    // Load the default pre-compiled shaders.
    void loadDefaultShaders();

    /// Create the main ray tracing pipeline.
    void createPipeline();

    /// Create descriptor heaps used for passing parameters to the shaders.
    res::d3d12::D3D12DescHeap::PtrT createDescHeap(std::size_t requiredDescriptors);

    /**
     * Create a buffer used for storing the shader table data.
     * @param sizeInBytes Required size of the buffer.
     */
    res::d3d12::D3D12Resource::PtrT createShaderTableBuffer(std::size_t sizeInBytes);

    /// Create output textures used for storing ray tracing output.
    void createResizeTextures(uint32_t width = 1u, uint32_t height = 1u);

    /// Create descriptors for the ray tracing output textures.
    void createTextureDescriptors();

    /// Create the static and dynamic constant buffers.
    void createConstantBuffers();

    /**
     * Create the noise texture (float3) with random values. 
     * Only performed if the texture is not created yet.
     */
    void createNoiseTexture(uint32_t width, uint32_t height, 
        res::d3d12::D3D12CommandList &directCmdList);

    /// Initialize rendering resources.
    void initializeResources(uint32_t requiredTextures = 0u, uint32_t requiredBuffers = 0u);

    /**
     * Add all drawables from provided list into the acceleration structure 
     * builder, preparing it for building the AS.
     * @param builder Builder of the AS, will be automatically create if null.
     * @param drawables List of drawables to prepare.
     * @param blasDuplication Should bottom level acceleration structures be duplicated?
     */
    void prepareAccelerationStructures(helpers::d3d12::D3D12RTAccelerationBuilder::PtrT &builder, 
        const res::scene::DrawableList &drawables, bool blasDuplication);

    /**
     * Add all materials and meshes from current list of drawables to provided 
     * material/mesh cache.
     * @param cache Structure used for caching, will be automatically created if null.
     * @param drawables List of drawables to prepare.
     */
    void prepareMaterialMeshCache(helpers::rndr::MaterialMeshCache::PtrT &cache, 
        const res::scene::DrawableList &drawables);

    /// Calculate number of descriptors needed in the miscellaneous descriptor table.
    uint32_t calculateMiscDescriptorCount();
    /// Create the miscellaneous descriptors table.
    helpers::d3d12::D3D12DescTable createMiscTable(res::d3d12::D3D12DescHeap &descHeap, 
        uint32_t numDescriptors);

    /// Calculate number of descriptors needed in the deferred descriptor table.
    uint32_t calculateDeferredDescriptorCount();
    /// Create the deferred descriptors table.
    helpers::d3d12::D3D12DescTable createDeferredTable(res::d3d12::D3D12DescHeap &descHeap, 
        uint32_t numDescriptors);

    /// Calculate number of descriptors needed in the texture descriptor table.
    uint32_t calculateTextureDescriptorCount(
        helpers::rndr::MaterialMeshCache &cache);
    /// Create the texture descriptors table.
    helpers::d3d12::D3D12DescTable createTextureTable(res::d3d12::D3D12DescHeap &descHeap, 
        uint32_t numDescriptors);

    /// Calculate number of descriptors needed in the buffer descriptor table.
    uint32_t calculateBufferDescriptorCount(
        helpers::rndr::MaterialMeshCache &cache);
    /// Create the buffer descriptors table.
    helpers::d3d12::D3D12DescTable createBufferTable(res::d3d12::D3D12DescHeap &descHeap, 
        uint32_t numDescriptors);

    /// Calculate number of descriptors needed in the build descriptor table.
    uint32_t calculateBuildDescriptorCount(
        const helpers::d3d12::D3D12RTAccelerationBuilder &builder);
    /// Create the build descriptors table.
    helpers::d3d12::D3D12DescTable createBuildTable(res::d3d12::D3D12DescHeap &descHeap, 
        uint32_t numDescriptors);

    /**
     * Create main descriptor heap and divide it into tables.
     * @param builder AS builder, which needs its own building table.
     * @param cache Material/mesh cache, using texture and buffer tables.
     */
    void createDescHeapLayout(const helpers::d3d12::D3D12RTAccelerationBuilder &builder, 
        helpers::rndr::MaterialMeshCache &cache);

    /// Create the required descriptors in the descriptor table.
    void fillMiscTable(helpers::d3d12::D3D12DescTable &miscTable,
        RayTracingBuffers &buffers, 
        StaticConstantsBuffer &staticConstants,
        DynamicConstantsBuffer &dynamicConstants,
        helpers::d3d12::D3D12TextureBuffer &noiseTexture);

    /// Build the material/mesh cache, depositing descriptors into tables.
    void buildMaterialMeshCache(helpers::rndr::MaterialMeshCache &cache, 
        helpers::d3d12::D3D12DescTable &textureTable, helpers::d3d12::D3D12DescTable &bufferTable);

    /// Build the acceleration structure, returning main wrapped pointer.
    ::WRAPPED_GPU_POINTER buildAccelerationStructures(helpers::d3d12::D3D12RTAccelerationBuilder &builder, 
        helpers::d3d12::D3D12DescTable &buildTable, res::d3d12::D3D12CommandList &cmdList);

    /// Create shader tables used when ray tracing.
    res::d3d12::D3D12Resource::PtrT createShaderTables(helpers::d3d12::D3D12RTShaderTable &shaderTable, 
        const helpers::rndr::MaterialMeshCache &cache, helpers::d3d12::D3D12RTPipeline &pipeline, 
        const res::scene::DrawableList &drawables);

    /**
     * Prepare per-instance data based on the list of drawables.
     * Each index from the list of drawables will have its own record.
     * @param drawables List of drawables to prepare.
     */
    std::vector<compatBasicRt::PerInstanceData> preparePerInstanceData(
        const res::scene::DrawableList &drawables);

    /**
     * Prepare given list of drawables for next render().
     * @param drawables List of drawable to prepare.
     * @param directCmdList Command list used to record the building 
     * commands.
     * @param sceneChanged Has the loaded scene changed?
     */
    void prepareForRendering(res::scene::DrawableList &drawables, 
        res::d3d12::D3D12CommandList &directCmdList, bool sceneChanged);

    /**
     * Update the constant buffers with new camera values.
     * @param camera Currently used camera.
     */
    void updateCamera(const helpers::math::Camera &camera);

    /**
     * Update the constant buffers with new values.
     */
    void updateConstants();

    /**
     * Update the constant buffers with new values.
     * This will trigger re-upload of buffers to the GPU if 
     * necessary.
     */
    void refreshConstantBuffers();

    /// Prepare current deferred buffer views.
    void prepareDeferredBuffers();

    /**
     * Upload the per-instance buffer with contents of 
     * instanceConstants vector.
     */
    void uploadPerInstanceData(const std::vector<compatBasicRt::PerInstanceData> &instanceConstants);

    /**
     * Is the ray tracing actually supported?
     * @return Returns true if the ray tracing rendering layer 
     * can be used.
     */
    bool rayTracingSupported() const;

    /**
     * Generate ray tracing dispatch description from the current 
     * configuration.
     */
    ::D3D12_DISPATCH_RAYS_DESC generateDispatchDesc();

    /// Print information about given dispatch configuration.
    void printDispatchInfo(const ::D3D12_DISPATCH_RAYS_DESC &desc);

    /// Check and reload the shader programs, if requested.
    void checkReloadShaders();

    /// Convert the debug mode into the shader value.
    static uint32_t debugModeToShaderMode(DebugMode mode);

    /// Convert debug mode enum to readable string.
    static const char *debugModeName(DebugMode mode);

    /// Render using this renderer.
    RenderSubSystem &mRenderer;

    /// Is ray tracing supported?
    bool mRayTracingSupported{ false };

    /// Are resources ready for ray tracing?
    bool mPrepared{ false };

    /// Mutable list of drawables which are rendered by this render layer.
    res::scene::DrawableListHandle mDrawables{ };
    /// Which last version of drawables has been prepared.
    res::scene::DrawableList::VersionT mLastPreparedVersion{ };

    /// Builder of acceleration structure levels.
    helpers::d3d12::D3D12RTAccelerationBuilder::PtrT mAccelerationBuilder{ };

    /// Rendering resources.
    struct
    {
        /// Root signature used for passing common data.
        res::d3d12::D3D12RootSignature::PtrT globalRs{ };
        /// Root signature used for hit group shaders parameters.
        res::d3d12::D3D12RootSignature::PtrT hitGroupLocalRs{ };
        /// Empty local root signature used for shaders which don't need any.
        res::d3d12::D3D12RootSignature::PtrT emptyLocalRs{ };

        /// Code blob for the ray tracing shaders.
        res::d3d12::D3D12Shader::PtrT rtShaders{ };

        /// The main Ray tracing pipeline.
        helpers::d3d12::D3D12RTPipeline::PtrT pipeline{ };

        // Fix for dangling Ray Tracing state object in Nvidia driver.
        helpers::d3d12::D3D12RTPipeline::PtrT pipelineBck{ };

        /// Descriptor heap used for storing common shader parameters.
        res::d3d12::D3D12DescHeap::PtrT descHeap{ };

        /// Cache used for preparing materials and meshes for rendering.
        helpers::rndr::MaterialMeshCache::PtrT materialMeshCache{ };

        /// Descriptor table used for storing miscellaneous descriptors.
        helpers::d3d12::D3D12DescTable miscTable{ };
        /// Descriptor table used for storing deferred buffer descriptors.
        helpers::d3d12::D3D12DescTable deferredTable{ };
        /// Descriptor table used for texture SRVs.
        helpers::d3d12::D3D12DescTable textureTable{ };
        /// Descriptor table used for buffer SRVs.
        helpers::d3d12::D3D12DescTable bufferTable{ };
        /// Descriptor table used for AS descriptors.
        helpers::d3d12::D3D12DescTable buildTable{ };

        /// RayTracing rendering buffers.
        RayTracingBuffers rayTracingBuffers{ };

        /// Slow-changing constant data.
        compatBasicRt::StaticConstantBuffer staticConstants{ };
        /// Dirty flag for the static constant data.
        bool staticConstantsChanged{ true };
        /// Slow-changing constant data buffer.
        StaticConstantsBuffer::PtrT staticConstantsBuffer{ };

        /// Fast-changing constant data.
        compatBasicRt::DynamicConstantBuffer dynamicConstants{ };
        /// Dirty flag for the dynamic constant data.
        bool dynamicConstantsChanged{ true };
        /// Fast-changing constant data buffer.
        DynamicConstantsBuffer::PtrT dynamicConstantsBuffer{ };

        /// Per-instance constant data.
        std::vector<compatBasicRt::PerInstanceData> instanceConstants{ };
        /// Per-instance constant data buffer.
        InstanceConstantsBuffer::PtrT instanceConstantsBuffer{ };

        /// Texture containing noise values.
        helpers::d3d12::D3D12TextureBuffer::PtrT noiseTexture{ };

        /// Shader table used with the main Ray Tracing pipeline.
        helpers::d3d12::D3D12RTShaderTable shaderTable{ };
        /// GPU buffer containing the shader table data.
        res::d3d12::D3D12Resource::PtrT shaderTableBuffer{ };

        /// Pointer to the top level acceleration structure.
        ::WRAPPED_GPU_POINTER accelerationStructure{ };

        /// Currently used deferred buffers.
        DeferredBuffers deferredBuffers{ };
        /// Views to the deferred buffers.
        DeferredBufferViews deferredViews{ };

        /// Should shader reload be attempted?
        bool reloadShaders{ false };
    } mR;

    /// Configuration of rendering
    struct
    {
        /// Currently used debugging mode.
        DebugMode debugMode{ DebugMode::FinalColor };

        /// Current width of the render target.
        uint32_t width{ 0u };
        /// Current height of the render target.
        uint32_t height{ 0u };

        /// Should the bottom level acceleration structures be duplicated for same meshes?
        bool blasDuplication{ false };
        /// Should the acceleration structure build optimized structures?
        bool buildOptimized{ true };

        /// Whether the deferred buffer generation should be frozen.
        bool freezeDeferred{ false };
    } mC;
protected:
    /**
     * Initialize the layer for given renderer.
     * @param renderer Renderer used for rendering the 
     * objects.
     */
    BasicRayTracingRL(RenderSubSystem &renderer);
}; // class BasicRayTracingRL

} // namespace rndr

} // namespace quark

#endif // ENGINE_RAY_TRACING_ENABLED
