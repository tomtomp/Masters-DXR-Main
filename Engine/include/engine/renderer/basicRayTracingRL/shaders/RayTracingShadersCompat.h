/**
 * @file renderer/basicRayTracingRL/shaders/RayTracingShadersCompat.h
 * @author Tomas Polasek
 * @brief Data structures shared between ray tracing shaders and the 
 * basic ray tracing render layer.
 */

#ifndef ENGINE_BASIC_RAY_TRACING_RL_SHADERS_COMPAT_H
#define ENGINE_BASIC_RAY_TRACING_RL_SHADERS_COMPAT_H

#ifdef HLSL
#   include "HlslCompatibility.h"
#else
#   include "engine/helpers/hlsl/HlslCompatibility.h"
#endif

/// Buffer containing global static data, which don't change very often.
struct StaticConstantBuffer
{
    /// Variable used for different display modes.
    HLSL_TYPE(uint) mode;

    HLSL_TYPE(uint3) padding;

    /// Direction of the global light.
    HLSL_TYPE(float3) lightDirection;
    /// Color of the global light.
    HLSL_TYPE(float3) lightColor;
    /// Color of the sky.
    HLSL_TYPE(float3) backgroundColor;

    // TODO - Fix this nicely?
#ifdef HLSL
    float padding2;
#endif // HLSL

    /// Whether to do reflections/refractions.
    HLSL_TYPE(uint) doReflections;
    /// Number of ambient occlusion rays.
    HLSL_TYPE(uint) aoRayCount;
    /// Set to true for smooth AO using distance rays.
    HLSL_TYPE(uint) aoDoSmooth;
    /// Number of shadow rays.
    HLSL_TYPE(uint) shadowRayCount;
    /// Jittering of the primary shadow ray.
    float shadowRayJitter;
    /// Range of the ambient occlusion rays.
    float aoRayRange;
    /// Distribution of the random rays.
    float rayDistribution;
}; // struct StaticConstantBuffer

/// Buffer containing global data which may change per frame.
struct DynamicConstantBuffer
{
    /// Transform from screen space into world space.
    HLSL_TYPE(float4x4) cameraToWorld;
    /// Position of the camera in world coordinates.
    HLSL_TYPE(float3) cameraWorldPosition;
    /// Resolution of the window/canvas.
    HLSL_TYPE(float2) canvasResolution;
}; // struct DynamicConstantBuffer

/**
 * Buffer containing static information for one geometry 
 * in the bottom level acceleration structure.
 */
struct GeometryConstantBuffer
{
    /// Mesh description.
    HLSL_STRUCTURED_TYPE(MeshSpecification) mesh;
    /// Material used by the mesh.
    HLSL_STRUCTURED_TYPE(MaterialSpecification) material;
}; // struct GeometryConstantBuffer

/// Data structure sent with each ray.
struct RayPayload
{
    /// Current "color" of the ray.
    HLSL_TYPE(float4) color;
    /// Ambient occlusion and shadow of the hit-point.
    HLSL_TYPE(float3) ambientOcclusionShadow;
}; // struct RayPayload

/// Data structure sent with each AO/shadow ray.
struct ShadowRayPayload
{
    /// Did the ray hit the scene?
    bool hitScene;
}; // struct ShadowRayPayload

/// Data structure sent with distance rays.
struct DistanceRayPayload
{
    /// Distance of the hit geometry. Negative when missed.
    float distance;
}; // struct DistancePayload

/// Using default triangle attributes.
typedef HLSL_TYPE(BuiltInTriangleIntersectionAttributes) MyTriangleAttributes;

/// Data which need to be specified on per-instance basis.
struct PerInstanceData
{
    /// Transform for model normals to world normals.
    HLSL_TYPE(float4x4) normalToWorld;
}; // struct PerInstanceData

/// Number of Hit Group records for each geometry.
#define HLSL_HIT_GROUP_STRIDE 1u

#ifndef HLSL

/// Helper structure used for passing HLSL register-space pairs.
struct HlslRegisterSpacePair
{
	uint32_t reg{ 0u };
	uint32_t space{ 0u };
}; // struct HlslRegisterSpacePair

// TODO - HLSL space variable?

/// Target ray tracing scene.
static constexpr HlslRegisterSpacePair AccelerationStructureSlot{ 0u, 99u };
/// Array of textures - materials etc.
static constexpr HlslRegisterSpacePair TexturesSlot{ 1u, 99u };
/// Array (TODO - single for now) of samplers.
static constexpr HlslRegisterSpacePair SamplersSlot{ 0u, 99u };
/// Array of buffers - vertex data etc.
static constexpr HlslRegisterSpacePair BuffersSlot{ 0u, 100u };
/// Array of geometry specifications.
static constexpr HlslRegisterSpacePair GeometrySlot{ 0u, 100u };
/// Array of deferred G-Buffer buffers.
static constexpr HlslRegisterSpacePair DeferredBuffersSlot{ 0u, 98u };

#else

// Display modes: 
#define HLSL_RAY_TRACING_MODE_FINAL_COLOR 0
#define HLSL_RAY_TRACING_MODE_FINAL_COLOR_WITH_PRIMARY 1
#define HLSL_RAY_TRACING_MODE_BASE_TEXTURE 2
#define HLSL_RAY_TRACING_MODE_PROPERTIES_TEXTURE 3
#define HLSL_RAY_TRACING_MODE_NORMAL_TEXTURE 4
#define HLSL_RAY_TRACING_MODE_NORMAL 5
#define HLSL_RAY_TRACING_MODE_WORLD_NORMAL 6
#define HLSL_RAY_TRACING_MODE_TANGENT 7
#define HLSL_RAY_TRACING_MODE_BARYCENTRINCS 8
#define HLSL_RAY_TRACING_MODE_RR 9
#define HLSL_RAY_TRACING_MODE_REFLECTIONS 10

/// Offset in the miss shaders table for normal rays.
#define HLSL_NORMAL_MISS_OFFSET 0u

/// Offset in the miss shaders table for shadow rays.
#define HLSL_SHADOW_MISS_OFFSET 1u

/// Offset in the miss shaders table for distance rays.
#define HLSL_DISTANCE_MISS_OFFSET 2u

/// Offset in the hit group table for normal rays.
#define HLSL_NORMAL_CLOSEST_HIT_OFFSET 1u

/// Offset in the hit group table for shadow rays.
#define HLSL_SHADOW_CLOSEST_HIT_OFFSET 0u

/// Offset in the hit group table for distance rays.
#define HLSL_DISTANCE_CLOSEST_HIT_OFFSET 0u

/// Starting ray time, which is large enough to prevent self-shadowing.
#define RAY_T_MIN 0.0001f
/// Ending ray time, which is large enough to encompass the whole scene.
#define RAY_T_MAX 1000000.0f

/// Offset of the shadow sampling location from the original position.
#define SHADOW_RAY_OFFSET 0.021f

/// Default maximum time for AO ray.
#define AO_RAY_RANGE 0.05f
/// Offset of the AO sampling location from the original position.
#define AO_RAY_OFFSET 0.0001f

/// Constructed scene acceleration structure.
RaytracingAccelerationStructure gScene : register(t0, space99);

/**
 * Textures used in rendering, contains all of the material 
 * textures as well.
 * Upper bound is set to allow debugging and validation.
 */
Texture2D gTextures[HLSL_TEXTURE_UPPER_BOUND] : register(t1, space99);

// TODO - Array of samplers, specified in the material?
/// Common sampler used for sampling the textures.
SamplerState gCommonSampler : register(s0, space99);

/// Get the deferred helpers.
#define DEFERRED_BUFFERS_SPACE space98
#include "hlslDeferredUser.h"

/**
 * Buffers containing information about the geometry.
 */
ByteAddressBuffer gBuffers[HLSL_BUFFER_UPPER_BOUND] : register(t0, space100);

/**
 * Specification of material and mesh of the hit geometry, only available 
 * from hit group shaders.
 */
ConstantBuffer<GeometryConstantBuffer> hgGeometry : register(b0, space100);

/// Target buffer, where ambient occlusion is stored.
RWTexture2D<float> gAmbientOcclusionTarget : register(u0, space0);
/// Target buffer, where shadows are stored.
RWTexture2D<float> gShadowOcclusionTarget : register(u1, space0);
/// Target buffer, where lighting is stored.
RWTexture2D<float4> gLightingTarget : register(u2, space0);

/// Slow-changing constant data.
ConstantBuffer<StaticConstantBuffer> gStatic : register(b0, space0);
/// Fast-changing constant data.
ConstantBuffer<DynamicConstantBuffer> gDynamic : register(b1, space0);

/// Constants required on per-instance basis.
struct InstanceConstantBuffer
{
    PerInstanceData data[HLSL_INSTANCE_UPPER_BOUND];
}; // struct InstanceConstantBuffer

/// Per-instance constant data.
ConstantBuffer<InstanceConstantBuffer> gInstanceData : register(b2, space0);

/**
 * Get per-instance data for given instance.
 * @param instanceId Identifier of the instance, InstanceID().
 * @return Returns the PerInstanceData structure.
 */
PerInstanceData perparePerInstanceData(in uint instanceId)
{ 
    // TODO - Check bounds, etc. ?
    return gInstanceData.data[instanceId]; 
}

/// Texture containing random noise.
Texture2D<float3> gNoiseTexture : register(t0, space1);

/**
 * Information about hit geometry.
 */
struct GeometryInformation
{
    /// Barycentric coordinates within the triangle.
    float3 barycentrics;

    /// Position of the hit in world coordinate system.
    float3 worldPos;

    /// World origin of the ray.
    float3 rayOrigin;
    /// World direction of the ray.
    float3 rayDirection;

    /**
     * Position of the hit in model coordinate system, interpolated 
     * from vertex buffer.
     */
    float3 modelPos;

    /// Interpolated texture coordinates.
    float2 texCoords;
    /// Change of texture coordinates in x-axis.
    float2 texDdx;
    /// Change of texture coordinates in y-axis.
    float2 texDdy;

    /// Interpolated normal.
    float3 normal;
    /// Interpolated tangent.
    float4 tangent;
}; // struct GeometryInformation

/**
 * Structure used for passing shading information.
 */
struct ShadingInformation
{
    /// Base color of the material.
    float4 baseColor;
    /// Model specific parameters, e.g. Metallicity-Roughness.
    float2 properties;
    /// Normal gained from the normal texture.
    float3 normal;
    /// Normal in the world-space.
    float3 wsNormal;

    /// Does the material generate specular reflections?
    float doReflections;
    /// Does the material generate "specular" refraction?
    float doRefractions;

    /// Ambient occlusion, 1.0f means fully occluded. Must be manually set!
    float ambientOcclusion;
    /// Shadow occlusion, 1.0f means fully occluded. Must be manually set!
    float shadowOcclusion;
    
    /// Color seen in the reflection.
    float4 reflectionColor;

    /// Final color;
    float4 finalColor;
}; // struct ShadingInformation

/**
 * Load 3 indices of a single triangle.
 * @param primitiveIndex Index of the triangle.
 * @param buffer Buffer containing index data.
 */
uint3 loadVertexIndices(in uint primitiveIndex, in ByteAddressBuffer buffer)
{
    // 3 Indices per triangle.
    const uint indexOffset = primitiveIndex * 4u * 3u;

    // Load the 3 indices.
    return buffer.Load3(indexOffset);
}

/**
 * Calculate geometry data based on the primitive index and 
 * barycentric coordinates from the hit triangle.
 * @param primitiveIndex Index of the hit primitive, PrimitiveIndex().
 * @param attributes Barycentric coordinates of the hit triangle.
 */
GeometryInformation prepareGeometryInformation(
    in uint primitiveIndex, in MyTriangleAttributes attributes)
{
    GeometryInformation result;

    // Calculate the current position of the ray.
    float3 hitWorldPosition = WorldRayOrigin() + RayTCurrent() * WorldRayDirection();

    // Clear the data.
    result.worldPos = hitWorldPosition;
    result.rayOrigin = WorldRayOrigin();
    result.rayDirection = WorldRayDirection();
    result.barycentrics = 0;
    result.modelPos = 0;
    result.texCoords = 0;
    result.normal = 0;
    result.tangent = 0;
    result.texDdx = 0;
    result.texDdy = 0;

    // Calculate complete barycentrics.
    float3 barycentrics = float3(
        1.0f - attributes.barycentrics.x - attributes.barycentrics.y, 
        attributes.barycentrics.x, attributes.barycentrics.y);
    result.barycentrics = barycentrics;

    // Fetch indices of the 3 vertexes in the hit triangle primitive.
    const uint3 indices = loadVertexIndices(primitiveIndex, gBuffers[hgGeometry.mesh.indices]);

    [unroll]
    for (int iii = 0; iii < 3; ++iii)
    { // Accumulate increments for each vertex.
        result.modelPos     += asfloat(gBuffers[hgGeometry.mesh.positions].Load3(indices[iii] * 4u * 3u)) * barycentrics[iii];
        result.texCoords    += asfloat(gBuffers[hgGeometry.mesh.texCoords].Load2(indices[iii] * 4u * 2u)) * barycentrics[iii];
        result.normal       += asfloat(gBuffers[hgGeometry.mesh.normals  ].Load3(indices[iii] * 4u * 3u)) * barycentrics[iii];
        result.tangent      += asfloat(gBuffers[hgGeometry.mesh.tangents ].Load4(indices[iii] * 4u * 4u)) * barycentrics[iii];
    }

    // TODO - Calculate texture gradients.
    // For now, just use static texture gradients.
    result.texDdx = float2(0.001f, 0.0f);
    result.texDdy = float2(0.0f, 0.001f);

    return result;
}

/**
 * Calculate world-space normal.
 * @param modelNormal Normal as taken from normal 
 * texture or vertex data.
 * @param instanceData Data for this instance.
 */
float3 worldNormal(in float3 modelNormal, in PerInstanceData instanceData)
{ return normalize(mul(float4(modelNormal, 0.0f), instanceData.normalToWorld).xyz); }

/**
 * Prepare shading information, based on geometry information.
 * @param geometry Pre-filled geometry information, used for accessing the 
 * textures.
 * @param instance Per-filled instance specific data, used for calculating 
 * world-space normal.
 * @return Returns filled shading information structure, final color is 
 * left empty.
 */
ShadingInformation prepareShadingInformation(
    in GeometryInformation geometry, in PerInstanceData instance)
{
    ShadingInformation result;

    // Clear the data.
    result.baseColor = 0.0f;
    result.properties = 0.0f;
    result.normal = 0.0f;
    result.wsNormal = 0.0f;
    result.doReflections = 0.0f;
    result.doRefractions = 0.0f;
    result.ambientOcclusion = 0.0f;
    result.shadowOcclusion = 0.0f;
    result.reflectionColor = 0.0f;
    result.finalColor = 0.0f;

    // Load the UV coordinates for addressing textures.
    float2 texCoords = geometry.texCoords;

    if (hgGeometry.material.colorTextureIndex != HLSL_INVALID_TEXTURE_INDEX)
    { // Load the base color.
        result.baseColor = gTextures[hgGeometry.material.colorTextureIndex].SampleGrad(
            gCommonSampler, texCoords, geometry.texDdx, geometry.texDdy);
    }

    if (hgGeometry.material.metallicRoughnessTextureIndex != HLSL_INVALID_TEXTURE_INDEX)
    { // Load the metallic-roughness PBR texture.
        result.properties = gTextures[hgGeometry.material.metallicRoughnessTextureIndex].SampleGrad(
            gCommonSampler, texCoords, geometry.texDdx, geometry.texDdy).xy;
    }

    if (hgGeometry.material.normalTextureIndex != HLSL_INVALID_TEXTURE_INDEX)
    { // Load the normal texture.
        result.normal = gTextures[hgGeometry.material.normalTextureIndex].SampleGrad(
            gCommonSampler, texCoords, geometry.texDdx, geometry.texDdy).xyz;
        result.wsNormal = worldNormal(result.normal, instance);
    }
    else
    { // Fallback to the mesh normal.
        result.normal = geometry.normal;
        result.wsNormal = worldNormal(result.normal, instance);
    }

    result.doReflections = hgGeometry.material.specularReflections;
    result.doRefractions = hgGeometry.material.specularRefractions;

    return result;
}

/**
 * Prepare shading information, based on deferred buffer information.
 * @param deferred Pre-filled deferred information.
 * @return Returns filled shading information structure, final color is 
 * left empty.
 * @warning Normal will be left unset - only wsNormal is specified!
 */
ShadingInformation prepareDeferredShadingInformation(in DeferredInformation deferred)
{
    ShadingInformation result;

    // Clear the data.
    result.baseColor = deferred.diffuse.rgba;
    result.properties = deferred.metallicRoughness.rg;
    result.normal = 0.0f;
    result.wsNormal = deferred.wsNormal.xyz;
    result.doReflections = deferred.doReflections;
    result.doRefractions = deferred.doRefractions;
    result.ambientOcclusion = 0.0f;
    result.shadowOcclusion = 0.0f;
    result.reflectionColor = 0.0f;
    result.finalColor = 0.0f;

    return result;
}

/**
 * Generate a camera ray for given dispatch thread.
 * @param index Index of this thread.
 * @param dimensions Dimensions of the whole dispatch.
 * @param screenToWorld Matrix used for transforming from 
 * screen space <-1.0f, 1.0f> to world space of camera.
 * @param worldPosition Position of the camera in the 
 * world.
 * @param rayOrigin Origin of the generated ray.
 * @param rayDirection Direction of the generated ray.
 */
void generateCameraRay(in uint2 index, in uint2 dimensions, in float4x4 screenToWorld, 
	in float3 worldPosition, out float3 rayOrigin, out float3 rayDirection)
{
    // Calculate center of pixel assigned to this thread.
    float2 pixelCenter = index + 0.5f; 
    // Calculate screen-space position on the output raster, <-1.0f, 1.0f> in 2D.
    float2 rasterPos = pixelCenter / dimensions * 2.0f - 1.0f;
    // Screen-space has y axis going down.
    rasterPos.y = -rasterPos.y;

    // Un-project the pixel back into the world-space.
    float4 worldPos = mul(float4(rasterPos, 0.0f, 1.0f), screenToWorld);
    worldPos.xyz /= worldPos.w;

    // Set the output attributes.
    rayOrigin = worldPosition;
    rayDirection = normalize(worldPos.xyz - rayOrigin);
}

/**
 * Helper for pseudo-random uniform hemisphere sampling
 * Source: http://www.rorydriscoll.com/2009/01/07/better-sampling/
 * @param rnd Base for randomness, both values should be 
 * in (0.0f, 1.0f>.
 * @return Returns a random value on a one radius hemisphere. 
 * The point will always be situated on the +z hemisphere.
 */
float3 hemisphereUniformSample(in float2 rnd)
{
    const float r = sqrt(1.0f - rnd.x * rnd.x);
    const float phi = HLSL_2PI * rnd.y;

    return float3(cos(phi) * r, sin(phi) * r, rnd.x);
}

/**
 * Helper for pseudo-random cosine hemisphere sampling
 * Source: http://www.rorydriscoll.com/2009/01/07/better-sampling/
 * @param rnd Base for randomness, both values should be 
 * in (0.0f, 1.0f>.
 * @return Returns a random value on a one radius hemisphere. 
 * The point will always be situated on the +z hemisphere.
 */
float3 hemisphereCosineSample(in float2 rnd)
{
	const float r = sqrt(rnd.x);
	const float theta = HLSL_2PI * rnd.y;

	const float x = r * cos(theta);
	const float y = r * sin(theta);

	return float3(x, y, sqrt(max(0.0f, 1.0f - rnd.x)));
}

/**
 * Create a random direction vector in a hemisphere above 
 * provided normal.
 * @param normal Normal pointing in the direction of the 
 * hemisphere.
 * @param rnd Random value which is used to generate the 
 * direction <0.0f, 1.0f>.
 */
float3 lambertNoTangent(in float3 normal, in float2 rnd)
{
    // Source: http://www.amietia.com/lambertnotangent.html

    float2 uv = rnd;
    float theta = HLSL_2PI * uv.x;
    uv.y = 2.0f * uv.y - 1.0f;
    float3 spherePoint = float3(sqrt(1.0f - uv.y * uv.y) * float2(cos(theta), sin(theta)), uv.y);
    return normalize(normal + spherePoint);
}

/**
 * Generate a camera ray for given dispatch thread.
 * @param index Index of this thread.
 * @param dimensions Dimensions of the whole dispatch.
 * @param rayOrigin Origin of the generated ray.
 * @param rayDirection Direction of the generated ray.
 */
void generateCameraRay(in uint2 index, in uint2 dimensions, out float3 rayOrigin, out float3 rayDirection)
{ generateCameraRay(index, dimensions, gDynamic.cameraToWorld, gDynamic.cameraWorldPosition, rayOrigin, rayDirection); }

/**
 * Generate the ray description structure for thread with 
 * given index.
 * @param index Index of the thread.
 * @return Returns filled ray description structure.
 */
RayDesc generateCameraRay(in uint2 index)
{
    // Calculate origin and direction.
    float3 rayOrigin;
    float3 rayDirection;
    generateCameraRay(index, DispatchRaysDimensions().xy, rayOrigin, rayDirection);

    // Create and fill the structure.
    RayDesc desc;
    desc.Origin = rayOrigin;
    desc.Direction = rayDirection;
    // Starting time is non-zero to prevent flickering.
    desc.TMin = RAY_T_MIN;
    // Maximum time is arbitrarily chosen large floating point.
    desc.TMax = RAY_T_MAX;

    return desc;
}

/**
 * Cast distance ray from origin in given direction.
 * @param origin Origin of the ray.
 * @param direction Direction of the ray.
 * @param beginT Starting time of the ray collision detection.
 * @param endT Ending time of the ray collision detection.
 * @return Returns distance of the ray when it hit the scene. 
 * If returned value is negative, no geometry has been hit.
 */
float castDistanceRay(in float3 origin, in float3 direction, in float beginT = RAY_T_MIN, in float endT = RAY_T_MAX)
{
	RayDesc distanceRay;
	distanceRay.Origin = origin;
	distanceRay.Direction = direction;
	distanceRay.TMin = beginT;
    distanceRay.TMax = endT;

    // Distance payload contains only distance to hit.
	DistanceRayPayload distancePayload = {
        // Initialize to not hit.
        -1.0f
	};

	// Dispatch the distance ray.
	TraceRay(
		// Choose the primary scene.
		gScene,
        // Use only first hit.
		RAY_FLAG_ACCEPT_FIRST_HIT_AND_END_SEARCH,
		// Trace against all geometry, no masking.
		0xFF,
        // Use distance ray shaders.
        HLSL_DISTANCE_CLOSEST_HIT_OFFSET, 0u, HLSL_DISTANCE_MISS_OFFSET, 
		// Use the generated ray.
		distanceRay,
		// Starting payload, which will contain the result.
		distancePayload);

    return distancePayload.distance;
}

/**
 * Cast shadow ray from origin in given direction.
 * @param origin Origin of the ray.
 * @param direction Direction of the light, in which the 
 * shadow ray will be cast.
 * @param beginT Starting time of the ray collision detection.
 * @param endT Ending time of the ray collision detection.
 * @return Returns true if the shadow ray hit the scene.
 */
bool castShadowRay(in float3 origin, in float3 direction, in float beginT = RAY_T_MIN, in float endT = RAY_T_MAX)
{
	RayDesc shadowRay;
	shadowRay.Origin = origin;
	shadowRay.Direction = direction;
	shadowRay.TMin = beginT;
    shadowRay.TMax = endT;

	// Shadow rays use the same payload as ambient occlusion.
	ShadowRayPayload shadowPayload = {
		// Presume we hit the scene
		true
	};

	// Dispatch the ambient occlusion rays!
	TraceRay(
		// Choose the primary scene.
		gScene,
        // Skip running the closest hit for better performance.
		RAY_FLAG_ACCEPT_FIRST_HIT_AND_END_SEARCH | RAY_FLAG_SKIP_CLOSEST_HIT_SHADER,
		// Trace against all geometry, no masking.
		0xFF,
        // Use shadow ray shaders.
        HLSL_SHADOW_CLOSEST_HIT_OFFSET, 0u, HLSL_SHADOW_MISS_OFFSET, 
		// Use generated light ray.
		shadowRay,
		// Starting payload, which will contain the result.
		shadowPayload);

    return shadowPayload.hitScene;
}

/**
 * Cast short-range shadow ray from origin in given direction.
 * @param origin Origin of the ray.
 * @param direction Direction of the light, in which the 
 * shadow ray will be cast.
 * @param beginT Starting time of the ray collision detection.
 * @param endT Ending time of the ray collision detection.
 * @return Returns true if the shadow ray hit the scene.
 */
bool castAoRay(in float3 origin, in float3 direction, in float range = AO_RAY_RANGE)
{ return castShadowRay(origin, direction, RAY_T_MIN, range); }

/**
 * Calculate ambient occlusion of given point.
 * Shoots out AO_RAY_COUNT ambient occlusion rays.
 * This version uses more expensive distance rays.
 * @param worldPosition Position which is being tested.
 * @param worldNormal Normal at the position, used to generate 
 * the rays in hemisphere around it. It must be in world-space.
 * @param texCoords Texture coordinates of the fragment.
 * @param rayCount Number of AO rays.
 * @return Returns occlusion factor <0.0, 1.0>, where 1.0 
 * means fully occluded and 0.0 is fully unoccluded.
 */
float calcAmbientOcclusionDistance(in float3 worldPosition, 
    in float3 worldNormal, in float2 texCoords, uniform uint rayCount)
{ 
    if (rayCount == 0u)
    { return 0.0f; }

    // Offset the position to prevent self-shadowing.
    const float3 offsetPosition = worldPosition + worldNormal * (AO_RAY_OFFSET + 0.02f);

    // Get a 3D pseudo-random vector.
    const float2 noiseTexCoordMult = gDynamic.canvasResolution / float2(4.0f, 4.0f);
    const float3 randVec = normalize(gNoiseTexture.SampleLevel(gCommonSampler, texCoords * noiseTexCoordMult, 0.0f).xyz);
    const float3 randSpaceVec = randVec * 2.0f - 1.0f;

    // Calculate a pseudo-random tangent and bitangent for provided normal.
    //const float3 worldTangent = normalize(randSpaceVec - worldNormal * dot(randSpaceVec, worldNormal));
    float3 worldTangent = normalize(randSpaceVec - worldNormal * dot(randSpaceVec, worldNormal));
    const float3 worldBitangent = cross(worldNormal, worldTangent);

    // Create transform matrix which will orient hemisphere by the provided normal.
    float3x3 sphereTransform = {
        worldTangent, 
        worldBitangent, 
        worldNormal, 
    };

    // Counter for number of samples which were occluded.
    float occludedSamples = 0.0f;

    for (uint iii = 0u; iii < rayCount; ++iii)
    { // Cast AO rays...
        // Calculate pseudo-random sampling direction.
        const float3 rayRand = hemisphereDirFibonacci(iii, rayCount, gStatic.rayDistribution);
        const float3 orientedDirection = mul(rayRand, sphereTransform);

        // Cast a distance ray to determine check occlusion.
        const float aoRayLength = castDistanceRay(offsetPosition, orientedDirection, RAY_T_MIN, gStatic.aoRayRange);
        // Are we occluded from the direction?
        const float occluded = aoRayLength >= 0.0f;
        // Attenuate the occlusion over distance.
        const float attenuation = smoothstep(1.0f, 0.0f, aoRayLength / gStatic.aoRayRange);

        // Accumulate occluded samples.
        occludedSamples += occluded * attenuation;
    }

    // Resulting occlusion factor is occluded / total_rays;
    return occludedSamples / rayCount;
}

/**
 * Calculate ambient occlusion of given point.
 * Shoots out AO_RAY_COUNT ambient occlusion rays.
 * This version uses more cheaper shadow rays.
 * @param worldPosition Position which is being tested.
 * @param worldNormal Normal at the position, used to generate 
 * the rays in hemisphere around it. It must be in world-space.
 * @param texCoords Texture coordinates of the fragment.
 * @param rayCount Number of AO rays.
 * @return Returns occlusion factor <0.0, 1.0>, where 1.0 
 * means fully occluded and 0.0 is fully unoccluded.
 */
float calcAmbientOcclusionShadow(in float3 worldPosition, 
    in float3 worldNormal, in float2 texCoords, uniform uint rayCount)
{ 
    if (rayCount == 0u)
    { return 0.0f; }

    // Offset the position to prevent self-shadowing.
    const float3 offsetPosition = worldPosition + worldNormal * AO_RAY_OFFSET;

    // Get a 3D pseudo-random vector.
    const float2 noiseTexCoordMult = gDynamic.canvasResolution / float2(4.0f, 4.0f);
    const float3 randVec = normalize(gNoiseTexture.SampleLevel(gCommonSampler, texCoords * noiseTexCoordMult, 0.0f).xyz);
    const float3 randSpaceVec = randVec * 2.0f - 1.0f;

    // Calculate a pseudo-random tangent and bitangent for provided normal.
    //const float3 worldTangent = normalize(randSpaceVec - worldNormal * dot(randSpaceVec, worldNormal));
    float3 worldTangent = normalize(randSpaceVec - worldNormal * dot(randSpaceVec, worldNormal));
    const float3 worldBitangent = cross(worldNormal, worldTangent);

    // Create transform matrix which will orient hemisphere by the provided normal.
    float3x3 sphereTransform = {
        worldTangent, 
        worldBitangent, 
        worldNormal, 
    };

    // Counter for number of samples which were occluded.
    float occludedSamples = 0.0f;

    for (uint iii = 0u; iii < rayCount; ++iii)
    { // Cast AO rays...
        // Calculate pseudo-random sampling direction.
        const float3 rayRand = hemisphereDirFibonacci(iii, rayCount, gStatic.rayDistribution);
        const float3 orientedDirection = mul(rayRand, sphereTransform);

        // Are we occluded from the direction?
        const bool occluded = castAoRay(offsetPosition, orientedDirection, gStatic.aoRayRange);

        // Accumulate occluded samples.
        occludedSamples += float(occluded);
    }

    // Resulting occlusion factor is occluded / total_rays;
    return occludedSamples / rayCount;
}

/**
 * Calculate ambient occlusion of given point.
 * Shoots out AO_RAY_COUNT ambient occlusion rays.
 * @param worldPosition Position which is being tested.
 * @param worldNormal Normal at the position, used to generate 
 * the rays in hemisphere around it. It must be in world-space.
 * @param texCoords Texture coordinates of the fragment.
 * @param rayCount Number of AO rays.
 * @return Returns occlusion factor <0.0, 1.0>, where 1.0 
 * means fully occluded and 0.0 is fully unoccluded.
 */
float calcAmbientOcclusion(in float3 worldPosition, 
    in float3 worldNormal, in float2 texCoords, uniform uint rayCount)
{ 
    if (gStatic.aoDoSmooth)
    { return calcAmbientOcclusionDistance(worldPosition, worldNormal, texCoords, rayCount); }
    else
    { return calcAmbientOcclusionShadow(worldPosition, worldNormal, texCoords, rayCount); }
}

/**
 * Calculate ambient occlusion of given point.
 * Shoots out AO_RAY_COUNT ambient occlusion rays.
 * @param geometryInfo Information about the fragment, which 
 * the ambient occlusion should be calculated for.
 * @param instanceData Information about the current object 
 * instance.
 * @return Returns occlusion factor <0.0, 1.0>, where 1.0 
 * means fully occluded and 0.0 is fully unoccluded.
 */
float calcAmbientOcclusion(in GeometryInformation geometryInfo, in PerInstanceData instanceData)
{
    return calcAmbientOcclusion(geometryInfo.worldPos, 
        worldNormal(geometryInfo.normal, instanceData), 
        frac(geometryInfo.worldPos.xz), gStatic.aoRayCount);
}

/**
 * Calculate shadow from given position.
 * Shoots out a single shadow ray.
 * @param worldPosition Position which is being tested.
 * @param worldNormal Normal at the position, used to offset 
 * the position a bit in order to prevent self-shadowing. 
 * @param lightDirection Direction from the position to the light.
 * @param texCoords Texture coordinates of the fragment.
 * @param rayCount number of cast shadow rays. One for primary 
 * only.
 * @param rayJitter How much should the secondary rays be jittered?
 * @return Returns shadow factor {0.0, 1.0}, where 1.0 means 
 * it is in shade and 0.0 is lit.
 */
float calcShadow(in float3 worldPosition, in float3 worldNormal, 
    in float3 lightDirection, in float2 texCoords, 
    uniform uint rayCount, uniform float rayJitter)
{
    if (rayCount == 0u)
    { return 0.0f; }

    // Offset the position to prevent self-shadowing.
    const float3 offsetPosition = worldPosition + worldNormal * SHADOW_RAY_OFFSET;

    // Calculate the primary ray.
    float primarySampleShadowed = castShadowRay(offsetPosition, lightDirection);
    //float primarySampleShadowed = 0.0f;

    // Multiplier for noise texture tiling.
    const float2 noiseTexCoordMult = gDynamic.canvasResolution / float2(4.0f, 4.0f);
    // Get a 3D pseudo-random vector.
    const float3 randVec = normalize(gNoiseTexture.SampleLevel(gCommonSampler, texCoords * noiseTexCoordMult, 0.0f).xyz);
    const float3 randSpaceVec = randVec * 2.0f - 1.0f;

    // Calculate a pseudo-random tangent and bitangent for provided normal.
    //const float3 baseDirection = worldNormal;
    const float3 baseDirection = lightDirection;
    const float3 worldTangent = normalize(randSpaceVec - baseDirection * dot(randSpaceVec, baseDirection));
    const float3 worldBitangent = cross(baseDirection, worldTangent);

    // Create transform matrix which will orient hemisphere by the provided normal.
    float3x3 sphereTransform = {
        worldTangent, 
        worldBitangent, 
        baseDirection, 
    };

    // Counter for number of samples which were shadowed.
    float shadowedSamples = primarySampleShadowed;

    // How much to jitter the primary ray.
    const float jitterMult = rayJitter;
    for (uint iii = 1u; iii < rayCount; ++iii)
    { // Cast jittered shadow rays
        // Calculate pseudo-random sampling direction.
        //const float2 rayRand = saturate(gNoiseTexture.SampleLevel(gCommonSampler, texCoords * (iii + 1) * noiseTexCoordMult, 0.0f).xy);
        //const float3 orientedDirection = normalize(lightDirection + lambertNoTangent(lightDirection, rayRand) * jitterMult);
        //const float3 rayRand = hemisphereDirFibonacci(iii, rayCount, 0.01f);

        const float3 rayRand = hemisphereDirFibonacci(iii, rayCount, gStatic.rayDistribution);
        const float3 orientedDirection = normalize(lightDirection + mul(rayRand, sphereTransform) * jitterMult);

        //const float3 rayRand = hemisphereDirFibonacci2(iii, 0.002f, rayCount, gStatic.rayDistribution);
        //const float3 orientedDirection = normalize(mul(rayRand, sphereTransform) * jitterMult);

        // Are we occluded from the direction?
        const bool shadowed = castShadowRay(offsetPosition, orientedDirection);

        // Accumulate occluded samples.
        shadowedSamples += float(shadowed);
    }

    shadowedSamples /= rayCount;

    // Calculate resulting shadow factor.
    return shadowedSamples;
}

/**
 * Calculate shadow from given position for point lights.
 * Shoots out a single shadow ray.
 * @param geometryInfo Information about the fragment, which 
 * the shadow should be calculated for.
 * @param instanceData Information about the current object 
 * instance.
 * @param worldLightPos Position of the light.
 * @return Returns shadow factor {0.0, 1.0}, where 1.0 means 
 * it is in shade and 0.0 is lit.
 */
float calcPointShadow(in GeometryInformation geometryInfo, in PerInstanceData instanceData, in float3 worldLightPos)
{
    const float3 lightDirection = normalize(worldLightPos - geometryInfo.worldPos);
    return calcShadow(geometryInfo.worldPos, 
        worldNormal(geometryInfo.normal, instanceData), 
        lightDirection, frac(geometryInfo.worldPos.xz), 
        gStatic.shadowRayCount, gStatic.shadowRayJitter);
}

/**
 * Calculate shadow from given position for directional lights.
 * Shoots out a single shadow ray.
 * @param wsPosition Position of the point in the scene.
 * @param wsNormal Normal at the position.
 * @param lightDirection Direction from which the light shines.
 * @param texCoords Texture coordinates of the fragment.
 * @param rayCount number of cast shadow rays. One for primary 
 * only.
 * @param rayJitter How much should the secondary rays be jittered?
 * @return Returns shadow factor {0.0, 1.0}, where 1.0 means 
 * it is in shade and 0.0 is lit.
 */
float calcDirectionalShadow(in float3 wsPosition, in float3 wsNormal, 
    in float3 lightDirection, in float2 texCoords, uniform uint rayCount, 
    uniform float rayJitter)
{ return calcShadow(wsPosition, wsNormal, -lightDirection, texCoords, rayCount, rayJitter); }

/**
 * Calculate shadow from given position for directional lights.
 * Shoots out a single shadow ray.
 * @param geometryInfo Information about the fragment, which 
 * the shadow should be calculated for.
 * @param instanceData Information about the current object 
 * instance.
 * @param lightDiretion Direction from which the light shines.
 * @return Returns shadow factor {0.0, 1.0}, where 1.0 means 
 * it is in shade and 0.0 is lit.
 */
float calcDirectionalShadow(in GeometryInformation geometryInfo, in PerInstanceData instanceData, in float3 lightDirection)
{
    return calcDirectionalShadow(geometryInfo.worldPos, 
        worldNormal(geometryInfo.normal, instanceData), 
        lightDirection, geometryInfo.worldPos.xz, 
        gStatic.shadowRayCount, gStatic.shadowRayJitter);
}

/**
 * Calculate the reflection color for given position and normal.
 * @param wsPosition World-space position of the point.
 * @param wsNormal World-space normal at that point.
 * @param wsView View vector in world-space.
 * @param doReflection Whether to perform the calculation.
 * @return Returns the reflected color, including alpha.
 */
float4 calcReflectionColor(in float3 wsPosition, in float3 wsNormal, 
    in float3 wsView, in float doReflections)
{
    if (doReflections * gStatic.doReflections < HLSL_EPS)
    { return 0.0f; }

    const float3 wsReflectedView = reflect(wsView, wsNormal);

    RayDesc reflectionRay;
    reflectionRay.Origin = wsPosition;
    reflectionRay.Direction = wsReflectedView;
    reflectionRay.TMin = RAY_T_MIN;
    reflectionRay.TMax = RAY_T_MAX;

    RayPayload reflectionPayload = { 
        // Starting color.
        float4(1.0f, 1.0f, 1.0f, 0.0f), 
        // Use only shadowing.
        float3(0.0f, 1.0f, 0.0f)
    };

    // Dispatch the rays!
    TraceRay(
        // Choose the primary scene.
        gScene,
        // Skip back-facing triangles from triggering hit.
        //RAY_FLAG_CULL_BACK_FACING_TRIANGLES, 
        RAY_FLAG_NONE, 
        // Trace against all geometry, no masking.
        0xFF,
        // Use normal ray shaders.
        HLSL_NORMAL_CLOSEST_HIT_OFFSET, HLSL_HIT_GROUP_STRIDE, HLSL_NORMAL_MISS_OFFSET, 
        // Use generated camera ray.
        reflectionRay,
        // Starting payload, which will contain the result.
        reflectionPayload);

    return reflectionPayload.color.rgba;
}

/**
 * Calculate the reflection color for given position and normal.
 * @param geometryInfo Geometry information at that point.
 * @param shadingInfo Shading information at that point.
 * @param instanceData Data for this instance.
 * @return Returns the reflected color, including alpha.
 */
float4 calcReflectionColor(in GeometryInformation geometryInfo, 
    in ShadingInformation shadingInfo, in PerInstanceData instanceData)
{
    return calcReflectionColor(
        geometryInfo.worldPos,
        worldNormal(geometryInfo.normal, instanceData),
        geometryInfo.rayDirection, 
        shadingInfo.doReflections);
}

#endif

#endif // ENGINE_BASIC_RAY_TRACING_RL_SHADERS_COMPAT_H
