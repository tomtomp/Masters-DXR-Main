/**
 * @file renderer/basicTexturedRL/BasicTexturedRL.h
 * @author Tomas Polasek
 * @brief Rendering layer allowing basic display of 
 * textured meshes.
 */

#pragma once

#include "engine/renderer/RenderLayerBase.h"

#include "engine/renderer/RenderSubSystem.h"

#include "engine/resources/d3d12/D3D12Shader.h"
#include "engine/resources/d3d12/D3D12RootSignature.h"
#include "engine/resources/d3d12/D3D12InputLayout.h"
#include "engine/resources/d3d12/D3D12PipelineState.h"

#include "engine/resources/render/DepthStencilBuffer.h"
#include "engine/resources/render/DescriptorMgr.h"

#include "engine/resources/scene/DrawableListMgr.h"

#include "engine/helpers/d3d12/D3D12RootSignatureBuilder.h"
#include "engine/helpers/d3d12/D3D12PipelineStateBuilder.h"

#include "engine/helpers/render/MaterialMeshCache.h"

#include "engine/helpers/math/Transform.h"
#include "engine/helpers/math/Camera.h"

/// Namespace containing the engine code.
namespace quark
{

// Forward declaration.
namespace gui
{
class GuiSubSystem;
}

/// Rendering classes.
namespace rndr
{

/**
* Rendering layer allowing basic display of 
* textured meshes.
 */
class BasicTexturedRL : public RenderLayerBase, public util::PointerType<BasicTexturedRL>
{
public:
    /**
     * Initialize the layer for given renderer.
     * @param renderer Renderer used for rendering the 
     * objects.
     */
    static PtrT create(RenderSubSystem &renderer);

    /// Free any resources.
    ~BasicTexturedRL();

    /**
     * Set a list of drawables which should be rendered by 
     * this render layer.
     * @param drawables Versioned list of drawables which will 
     * be used for rendering.
     */
    void setDrawablesList(const res::scene::DrawableListHandle &drawables);

    /**
     * Resize the buffers used by this render layer for given 
     * screen size.
     * Should be called after resizing of the swap chain!
     * @param width New width in pixels.
     * @param height New height in pixels.
     */
    void resize(uint32_t width, uint32_t height);

    /**
     * Check if the drawables list changed and perform required 
     * preparation for rendering if necessary.
     * @param sceneChanged Has the scene completely changed?
     */
    void prepare(bool sceneChanged = false);

    /**
     * Create GUI elements used by this render layer.
     * @param gui GUI sub-system used for the main GUI.
     * @warning Default context/window must be set before 
     * calling this method.
     */
    void prepareGui(gui::GuiSubSystem &gui);

    /**
     * Render current list of drawables.
     * @param camera Camera containing the view-projection 
     * matrix which should be used in rendering.
     * @param targetRTV Target render-target-view which 
     * should contain the resulting image.
     * @param cmdList Command list which should contain the 
     * rendering commands.
     */
    void render(const helpers::math::Camera &camera, 
        const ::CD3DX12_CPU_DESCRIPTOR_HANDLE &targetRTV, 
        res::d3d12::D3D12CommandList &cmdList);

    /**
     * Update GUI variables used by this render layer.
     * @param gui GUI sub-system used for the main GUI.
     * @warning GUI preparation for this render layer must 
     * already be executed.
     */
    void updateGui(gui::GuiSubSystem &gui);
private:
    /// Index of the root parameter which should contain per-draw constants.
    static constexpr ::UINT ROOT_PARAMETER_IDX_PER_DRAW{ 0u };
    /// Index of the root parameter which should contain per-model constants.
    static constexpr ::UINT ROOT_PARAMETER_IDX_PER_MODEL{ 1u };
    /// Index of the root parameter which should contain material constants.
    static constexpr ::UINT ROOT_PARAMETER_IDX_MATERIAL{ 2u };
    /// Index of the root parameter which contains the texture table.
    static constexpr ::UINT ROOT_PARAMETER_IDX_SRV_TABLE{ 3u };
    /// Index of the root parameter where the commonly used sampler is located at.
    static constexpr ::UINT ROOT_PARAMETER_IDX_COMMON_SAMPLER{ 0u };

    /// Default format used for vertex positions.
    static constexpr ::DXGI_FORMAT DEFAULT_VERTEX_POSITION_FORMAT{ ::DXGI_FORMAT_R32G32B32_FLOAT };
    /// Default input slot for vertex position data.
    static constexpr ::UINT DEFAULT_VERTEX_POSITION_SLOT{ 0u };
    /// Default format used for texture coordinates.
    static constexpr ::DXGI_FORMAT DEFAULT_VERTEX_TEXCOORD_FORMAT{ ::DXGI_FORMAT_R32G32_FLOAT };
    /// Default input slot for texture coordinates data.
    static constexpr ::UINT DEFAULT_VERTEX_TEXCOORD_SLOT{ 1u };

    /// Default primitive topology type used by the main pipeline.
    static constexpr ::D3D12_PRIMITIVE_TOPOLOGY_TYPE DEFAULT_PRIMITIVE_TOPOLOGY_TYPE{ ::D3D12_PRIMITIVE_TOPOLOGY_TYPE_TRIANGLE };
    /// Default primitive topology used by the main pipeline.
    static constexpr ::D3D12_PRIMITIVE_TOPOLOGY DEFAULT_PRIMITIVE_TOPOLOGY{ ::D3D_PRIMITIVE_TOPOLOGY_TRIANGLELIST };

    /// Default render target buffer format.
    static constexpr ::DXGI_FORMAT DEFAULT_RENDER_TARGET_FORMAT{ ::DXGI_FORMAT_R8G8B8A8_UNORM };

    /// Data sent to the GPU on per-draw basis.
    struct PerDrawData
    {
        /// Size of this structure in 32b values.
        static constexpr uint32_t sizeIn32bValues()
        { return sizeof(PerDrawData) / 4u; }

        /// The view-projection transform matrix.
        dxtk::math::Matrix vp;
    }; // struct PerDrawData

    /// Data sent to the GPU on per-model basis.
    struct PerModelData
    {
        /// Size of this structure in 32b values.
        static constexpr uint32_t sizeIn32bValues()
        { return sizeof(PerModelData) / 4u; }

        /// The model transform matrix.
        dxtk::math::Matrix m;
    }; // struct PerModelData

    /// Data sent to the GPU containing information about current material.
    struct MaterialData
    {
        /// Size of this structure in 32b values.
        static constexpr uint32_t sizeIn32bValues()
        { return sizeof(MaterialData) / 4u; }

        /// Index of the diffuse texture within the main descriptor table.
        uint32_t diffuseIdx;
        /// Index of the material texture within the main descriptor table.
        uint32_t materialIdx;
    }; // struct MaterialData

    /// Initialize rendering resources.
    void initializeResources(uint32_t requiredTextures = 0u);

    /// Create pipeline for required number of textures.
    void createPipeline(uint32_t requiredTextures);

    /**
     * Prepare materials used in given list of drawables for 
     * next render().
     * @param drawables List of drawables to prepare.
     */
    void prepareMaterials(res::scene::DrawableList &drawables);

    /**
     * Prepare commands used for rendering drawables into 
     * provided draw bundle.
     * @param drawables List of drawables to prepare.
     * @param drawBundle Draw bundle which should contain 
     * all of the draw commands.
     */
    void prepareDrawBundle(res::scene::DrawableList &drawables, 
        res::d3d12::D3D12CommandList &drawBundle);

    /// Get the number of texture descriptors.
    uint32_t calculateTextureDescriptorCount(helpers::rndr::MaterialMeshCache &cache);

    /// Create the descriptor table for texture descriptors.
    helpers::d3d12::D3D12DescTable createTextureTable(res::d3d12::D3D12DescHeap &descHeap, 
        uint32_t numDescriptors);

    /// Create SRV/UAV/CBV descriptor heap for specified number of descriptors.
    res::d3d12::D3D12DescHeap::PtrT createDescHeap(std::size_t requiredDescriptors);

    /**
     * Create the descriptor heap and its tables according to 
     * requirements.
     * @param descHeap Descriptor heap which should be created.
     * @param textureTable Descriptor table which will contain 
     * material texture descriptors.
     * @param cache Filled cache used for materials.
     */
    void createDescHeapLayout(res::d3d12::D3D12DescHeap::PtrT &descHeap, 
        helpers::d3d12::D3D12DescTable &textureTable, 
        helpers::rndr::MaterialMeshCache &cache);

    /**
     * Prepare given list of drawables for next render().
     * @param drawables List of drawable to prepare.
     * @param sceneChanged Has the loaded scene changed?
     */
    void prepareForRendering(res::scene::DrawableList &drawables, 
        bool sceneChanged);

    /**
     * Set current descriptor heaps for given command list.
     * @param cmdList command list which should have the 
     * descriptor heaps set.
     */
    void setDescHeaps(res::d3d12::D3D12CommandList &cmdList);

    /// Render using this renderer.
    RenderSubSystem *mRenderer;

    /// Mutable list of drawables which are rendered by this render layer.
    res::scene::DrawableListHandle mDrawables{ };
    /// Which last version of drawables has been prepared.
    res::scene::DrawableList::VersionT mLastPreparedVersion{ };

    /// Is rendering layer prepared for rendering?
    bool mPrepared{ false };

    /// Rendering resources.
    struct
    {
        /// Format of the vertex position data.
        ::DXGI_FORMAT vertexPosFormat{ DEFAULT_VERTEX_POSITION_FORMAT };
        /// Input slot for the vertex position data.
        ::UINT vertexPosSlot{ DEFAULT_VERTEX_POSITION_SLOT };
        /// Format of the texture coordinate data.
        ::DXGI_FORMAT textureCoordFormat{ DEFAULT_VERTEX_TEXCOORD_FORMAT };
        /// Input slot for the texture coordinates data.
        ::UINT textureCoordSlot{ DEFAULT_VERTEX_TEXCOORD_SLOT };

        /// Primitive topology type for the main pipeline.
        ::D3D12_PRIMITIVE_TOPOLOGY_TYPE primitiveTopologyType{ DEFAULT_PRIMITIVE_TOPOLOGY_TYPE };
        /// Primitive topology for the main pipeline.
        ::D3D12_PRIMITIVE_TOPOLOGY primitiveTopology{ DEFAULT_PRIMITIVE_TOPOLOGY};

        /// Format of the render target buffer.
        ::DXGI_FORMAT renderTargetFormat{ DEFAULT_RENDER_TARGET_FORMAT };

        /// Scissor rectangle for the viewport.
        ::CD3DX12_RECT scissorRect{ };
        /// Target viewport dimensions.
        ::CD3DX12_VIEWPORT viewport{ };

        /// Root signature of the main pass.
        res::d3d12::D3D12RootSignature::PtrT rootSig{ };
        /// Input layout for the main pass.
        res::d3d12::D3D12InputLayout inputLayout{ };
        /// Main vertex shader.
        res::d3d12::D3D12Shader::PtrT vShader{ };
        /// Main pixel shader.
        res::d3d12::D3D12Shader::PtrT pShader{ };
        /// Pipeline for the main pass.
        res::d3d12::D3D12PipelineState::PtrT pipelineState{ };

        /// Depth-stencil buffer used for the main pass.
        res::rndr::DepthStencilBuffer::PtrT depthStencilBuffer{ };

        /**
         * Command bundle containing rendering commands for the 
         * currently prepared drawables.
         */
        res::d3d12::D3D12CommandList::PtrT renderBundle{ };

        /// Main descriptor heap used for storing SRVs, UAVs and CBVs.
        res::d3d12::D3D12DescHeap::PtrT mainDescHeap{ };

        /// Texture table created from the material cache.
        helpers::d3d12::D3D12DescTable textureTable{ };

        /// Cache of materials/meshes for the current list of drawable.
        helpers::rndr::MaterialMeshCache::PtrT materialMeshCache;
    } mR;
protected:
    /**
     * Initialize the layer for given renderer.
     * @param renderer Renderer used for rendering the 
     * objects.
     */
    BasicTexturedRL(RenderSubSystem &renderer);
}; // class BasicTexturedRL

} // namespace rndr

} // namespace quark
