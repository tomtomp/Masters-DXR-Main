/**
 * @file renderer/resolveRL/shaders/ResolveShadersCompat.h
 * @author Tomas Polasek
 * @brief Data structures shared between resolve pass shaders.
 */

#ifndef ENGINE_BASIC_RESOLVE_RL_SHADERS_COMPAT_H
#define ENGINE_BASIC_RESOLVE_RL_SHADERS_COMPAT_H

#ifdef HLSL
#   include "HlslCompatibility.h"
#else
#   include "engine/helpers/hlsl/HlslCompatibility.h"
#endif

/// Buffer containing global static data, which don't change very often.
struct StaticConstantBuffer
{
    /// Switch used to change the performed function.
    HLSL_TYPE(uint) mode;

    HLSL_TYPE(uint3) padding1;

    /// Direction of the global light.
    HLSL_TYPE(float3) lightDirection;
    /// Color of the global light.
    HLSL_TYPE(float3) lightColor;
    /// Color of the sky.
    HLSL_TYPE(float3) backgroundColor;

    // TODO - Fix this nicely?
#ifdef HLSL
    float padding2;
#endif // HLSL

    /// Whether to do reflections/refractions.
    HLSL_TYPE(uint) enableReflections;
    /// Render shadows?
    HLSL_TYPE(uint) enableShadows;
    /// Half-size of the shadow/AO kernel.
    HLSL_TYPE(uint) kernelSize;
    /// Render ambient occlusion?
    HLSL_TYPE(uint) enableAo;
}; // struct StaticConstantBuffer

/// Buffer containing global data which may change per frame.
struct DynamicConstantBuffer
{
    /// Transform from screen-space to world-space.
    HLSL_TYPE(float4x4) cameraToWorld;
    /// Position of the camera in world coordinates.
    HLSL_TYPE(float3) cameraWorldPosition;
}; // struct DynamicConstantBuffer

#ifndef HLSL

/// Helper structure used for passing HLSL register-space pairs.
struct HlslRegisterSpacePair
{
	uint32_t reg{ 0u };
	uint32_t space{ 0u };
}; // struct HlslRegisterSpacePair

/// Slow-changing constants buffer.
static constexpr HlslRegisterSpacePair StaticConstantsSlot{ 0u, 0u };
/// Fast-changing constants buffer.
static constexpr HlslRegisterSpacePair DynamicConstantsSlot{ 1u, 0u };
/// Resolve output texture.
static constexpr HlslRegisterSpacePair ResolveOutputSlot{ 0u, 0u };
/// Common sampler for input textures.
static constexpr HlslRegisterSpacePair SamplerSlot{ 0u, 0u };
/// Array of deferred G-Buffer buffers.
static constexpr HlslRegisterSpacePair DeferredBuffersSlot{ 0u, 1u };
/// Array of ray tracing output buffers.
static constexpr HlslRegisterSpacePair RayTracingBuffersSlot{ 0u, 2u };

#else

// Display modes: 
#define HLSL_RAY_TRACING_MODE_FINAL_COLOR 0
#define HLSL_RAY_TRACING_MODE_RT_OUT 1
#define HLSL_RAY_TRACING_MODE_SHADOW 2
#define HLSL_RAY_TRACING_MODE_OCCLUSION 3
#define HLSL_RAY_TRACING_MODE_DEFERRED_POSITION 4
#define HLSL_RAY_TRACING_MODE_DEFERRED_NORMAL 5
#define HLSL_RAY_TRACING_MODE_DEFERRED_DIFFUSE 6
#define HLSL_RAY_TRACING_MODE_DEFERRED_MR 7
#define HLSL_RAY_TRACING_MODE_DEFERRED_RR 8
#define HLSL_RAY_TRACING_MODE_DEFERRED_DEPTH 9

/// Slow-changing constants.
ConstantBuffer<StaticConstantBuffer> gStatic : register(b0, space0);
/// Fast-changing constants.
ConstantBuffer<DynamicConstantBuffer> gDynamic : register(b1, space0);

/// Target buffer, where rendering output is placed.
RWTexture2D<float4> gRenderTarget : register(u0, space0);

/// Common sampler used for sampling the textures.
SamplerState gCommonSampler : register(s0, space0);

/// Get the deferred helpers.
#define DEFERRED_BUFFERS_SPACE space1
#include "hlslDeferredUser.h"

/// Ray tracing ambient occlusion output.
Texture2D<float> gAmbientOcclusionTexture : register(t0, space2);
/// Ray tracing shadow occlusion output.
Texture2D<float> gShadowOcclusionTexture : register(t1, space2);
/// Ray tracing lighting color output.
Texture2D<float4> gLightingColorTexture : register(t2, space2);

/**
 * Structure used for passing ray tracing information.
 */
struct RayTracingInformation
{
    /// World origin of the ray.
    float3 rayOrigin;
    /// World direction of the ray.
    float3 rayDirection;

    /// Ambient occlusion of the fragment, 1.0f means fully occluded.
    float ambientOcclusion;
    /// Shadow occlusion of the fragment, 1.0f means fully occluded.
    float shadowOcclusion;
    /// Lighting color of the fragment.
    float4 lightingColor;
}; // struct ShadingInformation

/// Data layout for input into the vertex shader.
struct VertexData
{
    /// Vertex identifier used for generation of fullscreen triangle.
    uint id : SV_VERTEXID;
}; // struct VertexData

/// Data layout for input into the pixel shader.
struct FragmentData
{
    /// Screen-space position.
    float4 ssPosition : SV_Position;
    /// Texture coordinate.
    float2 texCoord : TEXCOORD0;
}; // struct FragmentData

/// Data layout for output out of the pixel shader.
struct PixelData
{
    /// Output color.
    float4 color : SV_Target0;
}; // struct PixelData

/**
 * Recover information from the ray tracing buffers, for the 
 * current dispatch index.
 * @param index Index of the thread.
 * @return Returns filled ray tracing information structure.
 */
RayTracingInformation prepareRayTracingInformation(in uint2 index)
{
    RayTracingInformation result;

    // Access the first mip-map.
    int3 textureIndex = int3(index, 0);

    result.ambientOcclusion = gAmbientOcclusionTexture.Load(textureIndex);
    result.shadowOcclusion = gShadowOcclusionTexture.Load(textureIndex);
    result.lightingColor = gLightingColorTexture.Load(textureIndex);

    return result;
}

/**
 * Calculate ambient occlusion coefficient.
 * @param enable Enable AO?
 * @param texCoord Screen-space texture coordinates.
 * @return Returns ambient occlusion.
 */
float calcAmbientOcclusion(uniform bool enable, in float2 texCoord)
{
    if (!enable)
    { return 0.0f; }

    if (gStatic.kernelSize == 0u)
    { return gAmbientOcclusionTexture.Sample(gCommonSampler, texCoord); }
    // Else: Doing AO blurring.

    const float2 texelSize = 1.0f / textureDimensions(gAmbientOcclusionTexture);

    const float halfKernelSize = gStatic.kernelSize;

    float shadowFactor = 0.0f;
    for (float y = -halfKernelSize; y <= halfKernelSize; y += 1.0f)
    {
        for (float x = -halfKernelSize; x <= halfKernelSize; x += 1.0f)
        {
            const float2 uvOffset = float2(x, y) * texelSize;
            const float2 uv = texCoord + uvOffset;
            shadowFactor += gAmbientOcclusionTexture.Sample(gCommonSampler, uv);
        }
    }
    const float kernelSize = halfKernelSize + halfKernelSize + 1u;
    shadowFactor /= kernelSize * kernelSize;

    return shadowFactor;
}

/**
 * Calculate shadow occlusion coefficient.
 * @param enable Enable shadows?
 * @param texCoord Screen-space texture coordinates.
 * @return Returns shadow occlusion.
 */
float calcShadows(uniform bool enable, in float2 texCoord)
{
    if (!enable)
    { return 0.0f; }

    const float baseShadow = gShadowOcclusionTexture.Sample(gCommonSampler, texCoord);

    if (gStatic.kernelSize == 0u)
    { return baseShadow; }
    // Else: Doing shadow blurring.

    const float halfKernelSize = gStatic.kernelSize;
    const float baseDepth = gDepthTexture.Sample(gCommonSampler, texCoord);

    const float texelSizeDepthMult = saturate((1.0f - baseDepth) * 5.0f);
    const float2 texelSize = 1.0f / textureDimensions(gShadowOcclusionTexture) * texelSizeDepthMult;

    float shadowFactor = 0.0f;
    for (float y = -halfKernelSize; y <= halfKernelSize; y += 1.0f)
    {
        for (float x = -halfKernelSize; x <= halfKernelSize; x += 1.0f)
        {
            const float2 uvOffset = float2(x, y) * texelSize;
            const float2 uv = texCoord + uvOffset;

            const float currentDepth = gDepthTexture.Sample(gCommonSampler, uv);
            const float depthDifference = abs(baseDepth - currentDepth);
            //const float distanceFactor = 1.0f - step(0.1f, depthDifference);
            //const float distanceFactor = depthDifference > 0.01f ? 0.0f : 1.0f;

            shadowFactor += depthDifference > 0.001f ? baseShadow : gShadowOcclusionTexture.Sample(gCommonSampler, uv);
        }
    }
    const float kernelSize = halfKernelSize + halfKernelSize + 1u;
    shadowFactor /= kernelSize * kernelSize;

    return shadowFactor;
}

/**
 * Calculate reflection color.
 * @param enable Enable reflections?
 * @param texCoord Screen-space texture coordinates.
 * @return Returns reflection color.
 */
float4 calcReflections(uniform bool enable, in float2 texCoord)
{
    if (!enable)
    { return 0.0f; }

    return gLightingColorTexture.Sample(gCommonSampler, texCoord);
}

/**
 * Recover information from the ray tracing buffers, for the 
 * current dispatch index.
 * @param texCoords Texture coordinates <0.0f, 1.0f>.
 * @return Returns filled ray tracing information structure.
 */
RayTracingInformation prepareRayTracingInformationUv(in float2 texCoord)
{
    RayTracingInformation result;

    // Initialize.
    result.rayOrigin = 0.0f;
    result.rayDirection = 0.0f;
    result.ambientOcclusion = 0.0f;
    result.shadowOcclusion = 0.0f;
    result.lightingColor = 0.0f;

    float3 rayOrigin = 0.0f;
    float3 rayDirection = 0.0f;

    generateCameraRay(texCoord, 
        gDynamic.cameraToWorld, gDynamic.cameraWorldPosition, 
        rayOrigin, rayDirection);

    result.rayOrigin = rayOrigin;
    result.rayDirection = rayDirection;

    result.ambientOcclusion = calcAmbientOcclusion(gStatic.enableAo, texCoord);
    result.shadowOcclusion = calcShadows(gStatic.enableShadows, texCoord);
    result.lightingColor = calcReflections(gStatic.enableReflections, texCoord);

    return result;
}

#endif

#endif // ENGINE_BASIC_RESOLVE_RL_SHADERS_COMPAT_H
