/**
 * @file renderer/RenderPipeline.h
 * @author Tomas Polasek
 * @brief Abstraction of the whole rendering pipeline, which 
 * may include multiple passes of different type - Raster, 
 * RayTrace or Compute.
 */

#pragma once

#include "engine/renderer/RenderResourceMgr.h"
#include "engine/renderer/RenderPass.h"
#include "engine/renderer/RenderScene.h"

/// Namespace containing the engine code.
namespace quark
{

/// Rendering classes.
namespace rndr
{

// Forward declaration.
class RenderSubSystem;

/**
 * Abstraction of the whole rendering pipeline, which 
 * may include multiple passes of different type - Raster, 
 * RayTrace or Compute.
 */
class RenderPipeline : public util::PointerType<RenderPipeline>
{
public:
    /**
     * Create and initialize the pipeline.
     * @param renderer Renderer used by the pipeline.
     * @return Returns pointer to the created pipeline.
     */
    static PtrT create(util::PtrT<RenderSubSystem> renderer);

    /// Free any resources used by the pipeline.
    ~RenderPipeline();

    // No copying or moving.
    RenderPipeline(const RenderPipeline &other) = delete;
    RenderPipeline &operator=(const RenderPipeline &other) = delete;
    RenderPipeline(RenderPipeline &&other) = delete;
    RenderPipeline &operator=(RenderPipeline &&other) = delete;

    /**
     * Set given scene as current for this pipeline.
     * @param scene The scene to display.
     */
    void setScene(RenderScene::PtrT scene);

    /// Get index for the next pass in line.
    uint32_t nextPassIndex() const;

    /**
     * Add a new render pass to the pipeline.
     * The pass must inherit from RenderPass base class and 
     * PointerType.
     * The RenderPassT will be constructed using a ::create static 
     * method which accepts the constructor arguments.
     * @param index Index where the new pass should sit at.
     * @param cArgs Constructor arguments passed to the ::create 
     * function of the RenderPassT.
     */
    template <typename RenderPassT, typename... CArgTs>
    std::enable_if_t<std::is_base_of_v<RenderPass, RenderPassT>, typename RenderPassT::PtrT>
        setRenderPass(uint32_t index, CArgTs... cArgs);

    /**
     * Run all preparations necessary, without actually executing.
     * The next executes will not run prepare() any more.
     */
    void prepare();

    /**
     * Execute the pipeline
     */
    void execute();
private:
    /**
     * Add a new render pass to the pipeline.
     * @param index Index where the new pass should sit at. 
     * @param passPtr Pointer to the new pass.
     */
    void setRenderPass(uint32_t index, RenderPass::PtrT passPtr);

    /**
     * Initialize all uninitialized passes.
     */
    void initializePasses();

    /**
     * Update scene for all active passes.
     */
    void updatePassScene();

    /**
     * Clear all passes, calling their uninitialized method and 
     * destructing their pointers.
     */
    void clearPasses();

    /**
     * Create a new rendering context with current information.
     */
    RenderContext::PtrT createRenderContext();

    /// Pointer to the current scene.
    RenderScene::PtrT mScene{ nullptr };

    /// Renderer used by this pipeline.
    util::PtrT<RenderSubSystem> mRenderer{ };

    /// Currently used rendering context.
    RenderContext::PtrT mRenderContext{ };

    /// Manager of resources shared between passes.
    RenderResourceMgr::PtrT mResourceMgr{ nullptr };

    /// Currently used scene.
    RenderScene::PtrT mCurrentScene{ nullptr };
    /// Has the current scene changed since last prepare?
    bool mCurrentSceneDirty{ true };

    /// List of passes used by this pipeline.
    std::vector<RenderPass::PtrT> mRenderPasses;
    /// Are there changes in the list of passes since last prepare?
    bool mRenderPassesDirty{ true };
protected:
    /**
     * Create and initialize the pipeline.
     * @param renderer Renderer used by the pipeline.
     */
    RenderPipeline(util::PtrT<RenderSubSystem> renderer);
}; // class RenderPipeline

} // namespace rndr

} // namespace quark

// Template implementation

namespace quark
{

namespace rndr
{

template<typename RenderPassT, typename... CArgTs>
inline std::enable_if_t<std::is_base_of_v<RenderPass, RenderPassT>, typename RenderPassT::PtrT> 
    RenderPipeline::setRenderPass(uint32_t index, CArgTs... cArgs)
{ 
    const auto passPtr{ RenderPassT::create(std::forward<CArgTs>(cArgs)...) };
    setRenderPass(index, passPtr); 

    return passPtr;
}

}

}

// Template implementation end
