/**
 * @file renderer/RenderContext.h
 * @author Tomas Polasek
 * @brief Rendering context containing information about current 
 * device, command lists and other resources.
 */

#pragma once

#include "engine/util/Util.h"

/// Namespace containing the engine code.
namespace quark
{

/// Rendering classes.
namespace rndr
{

// Forward declarations.
class RenderSubSystem;
class ShaderCache;

// TODO - Implement platform specialized contexts.
/**
 * Rendering context containing information about current 
 * device, command lists and other resources.
 */
class RenderContext : public util::PointerType<RenderContext>
{
public:
    // Free any allocated resources.
    ~RenderContext();

    // No copying or moving.
    RenderContext(const RenderContext &other) = delete;
    RenderContext &operator=(const RenderContext &other) = delete;
    RenderContext(RenderContext &&other) = delete;
    RenderContext &operator=(RenderContext &&other) = delete;

    /// Create a new rendering context for given renderer.
    static PtrT create(util::PtrT<RenderSubSystem> renderer);
    // TODO - Add direct/copy/compute command lists.

    /// Access the renderer.
    RenderSubSystem &rndr();

    /// Access the shader cache.
    ShaderCache &shaderCache();
private:
    /**
     * Create a new rendering context for given renderer.
     * @param renderer Currently used renderer.
     */
    RenderContext(util::PtrT<RenderSubSystem> renderer);

    /// Currently used renderer.
    util::PtrT<RenderSubSystem> mRenderer;
    /// Shader cache used for shader compilation.
    util::PtrT<ShaderCache> mShaderCache;
protected:
}; // class RenderContext

} // namespace rndr

} // namespace quark
