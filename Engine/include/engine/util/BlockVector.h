/**
 * @file util/BlockVector.h
 * @author Tomas Polasek
 * @brief Vector of memory persistent blocks.
 */

#pragma once

#include "engine/util/Types.h"

/// Utilities and helpers.
namespace util
{

/**
 * TODO - Turn into standard-compliant container?
 * Vector which contains its elements in blocks. These 
 * blocks always stay at same memory location even when 
 * resizing occurs.
 * @tparam ElementTT Type of element, stored in the memory 
 * blocks.
 * @tparam BlockSizeT Size of a single block.
 * @tparam AllocatorTT Allocator used for allocation of the 
 * blocks.
 */
template <typename ElementTT, std::size_t BlockSizeT = 10u, typename AllocatorTT = std::allocator<ElementTT>>
class BlockVector
{
public:
    /// Type of a single element.
    using ElementT = ElementTT;
    /// Number of elements in one allocated memory block.
    static constexpr auto BlockSize{ BlockSizeT };
private:
    /// Container for a single memory block allocated by the vector.
    struct MemoryBlock
    {
        /**
         * Bit-set which contains a '1' when corresponding 
         * element is already used.
         */
        std::bitset<BlockSize> occupancy;
        /// Array of elements.
        std::array<ElementT, BlockSize> elements;
    }; // struct MemoryBlock
public:
    /// Allocator used for allocation of blocks.
    using AllocatorT = typename std::allocator_traits<AllocatorTT>::template rebind_alloc<MemoryBlock>;

    /// Non-const iterator type.
    using IterT = ElementT*;
    /// Const iterator type.
    using ConstIterT = const ElementT*;

    /// Create empty block vector.
    BlockVector(const AllocatorT &alloc = AllocatorT());

    ~Blockvector();

    /// Copy other block vector.
    BlockVector(const BlockVector &other);
    /// Copy other block vector.
    BlockVector &operator=(BlockVector other);
    /// Move from other block vector.
    BlockVector(BlockVector &&other);
    /// Move from other block vector.
    BlockVector &operator=(BlockVector &&other);

    /**
     * Swap content with other block vector.
     * @param other The other block vector.
     */
    void swap(BlockVector &other) noexcept;

    /**
     * Swap 2 block vectors.
     * @param first First block vector, swapped with the second one.
     * @param second Second block vector, swapped with the first one.
     */
    template <typename ElementST, std::size_t BlockSizeS, typename AllocatorST>
    friend void swap(BlockVector<ElementST, BlockSizeS, AllocatorST> &first, 
        BlockVector<ElementST, BlockSizeS, AllocatorST> &second) noexcept
    { first.swap(second); }

    /**
     * Push new element to the back of the block vector.
     * @param val Value to push into the block vector.
     * @return Returns iterator to the new element. This 
     * iterator is guaranteed to be valid until the 
     * element is erased.
     * @throws May throw allocator exceptions.
     */
    IterT push_back(const ElementT &val);

    /**
     * Emplace a new element to the back of the block vector.
     * @tparam ArgTs Types of arguments passed to the constructor.
     * @param args Arguments passed to the constructor.
     * @return Returns iterator to the new element. This 
     * iterator is guaranteed to be valid until the 
     * element is erased.
     * @throws May throw allocator exceptions.
     */
    template <typename... ArgTs>
    IterT emplace_back(ArgTs... args);
private:
    /// List of allocated memory blocks in order.
    std::vector<std::unique_ptr<MemoryBlock>, AllocatorTT> mMemoryBlocks;
    /// List of allocated blocks which are not full.
    std::vector<MemoryBlock*, AllocatorTT> mNonFullBlocks;
    /// Allocator used for allocation of memory blocks.
    AllocatorT mAllocator;
protected:
}; // class BlockVector

} // namespace util

// Template implementation. 

namespace util
{



}

// Template implementation end. 
