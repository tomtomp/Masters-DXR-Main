/**
 * @file util/Types.h
 * @author Tomas Polasek
 * @brief Common types used in the application.
 */

#pragma once

#include "engine/util/StlWrapperBegin.h"

#include <stdexcept>
#include <cstdint>
#include <cstdlib>
#include <iostream>
#include <iomanip>
#include <fstream>
#include <cassert>
#include <chrono>
#include <memory>
#include <random>

#include <algorithm>
#include <numeric>
#include <functional>
#include <optional>

#include <type_traits>

#include <string>
#include <array>
#include <map>
#include <vector>
#include <queue>
#include <deque>
#include <stack>
#include <set>
#include <list>
#include <bitset>
#include <any>

#include <mutex>
#include <shared_mutex>
#include <atomic>
#include <future>

#include "engine/util/Util.h"
#include "engine/util/Math.h"
#include "engine/util/logging/MessageOutput.h"
#include "engine/util/wexception.h"
#include "engine/util/winexception.h"
#include "engine/util/Asserts.h"
#include "engine/util/logging/Logging.h"

#include <wrl.h>
#include <math.h>

/// Shortcut for Microsoft ComPtr.
template <typename T>
using ComPtr = Microsoft::WRL::ComPtr<T>;

#define M_E        2.71828182845904523536   // e
#define M_LOG2E    1.44269504088896340736   // log2(e)
#define M_LOG10E   0.434294481903251827651  // log10(e)
#define M_LN2      0.693147180559945309417  // ln(2)
#define M_LN10     2.30258509299404568402   // ln(10)
#define M_PI       3.14159265358979323846   // pi
#define M_PI_2     1.57079632679489661923   // pi/2
#define M_PI_4     0.785398163397448309616  // pi/4
#define M_1_PI     0.318309886183790671538  // 1/pi
#define M_2_PI     0.636619772367581343076  // 2/pi
#define M_2_SQRTPI 1.12837916709551257390   // 2/sqrt(pi)
#define M_SQRT2    1.41421356237309504880   // sqrt(2)
#define M_SQRT1_2  0.707106781186547524401  // 1/sqrt(2)

#define M_FE        2.718281828f   // e
#define M_FLOG2E    1.442695040f   // log2(e)
#define M_FLOG10E   0.434294481f   // log10(e)
#define M_FLN2      0.693147180f   // ln(2)
#define M_FLN10     2.302585092f   // ln(10)
#define M_FPI       3.141592653f   // pi
#define M_FPI_2     1.570796326f   // pi/2
#define M_FPI_4     0.785398163f   // pi/4
#define M_F1_PI     0.318309886f   // 1/pi
#define M_F2_PI     0.636619772f   // 2/pi
#define M_F2_SQRTPI 1.128379167f   // 2/sqrt(pi)
#define M_FSQRT2    1.414213562f   // sqrt(2)
#define M_FSQRT1_2  0.707106781f   // 1/sqrt(2)

#include "engine/util/StlWrapperEnd.h"
