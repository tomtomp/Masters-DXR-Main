/**
 * @file util/logging/LoggingLevel.h
 * @author Tomas Polasek
 * @brief Specification of logging levels.
 */

#pragma once

#include "engine/util/logging/LoggingStreams.h"

/// Utilities and helpers.
namespace util
{

/// Logging classes and utilities.
namespace logging
{

/// Implementation details for logging.
namespace detail
{

enum class LoggingLevel
{
    /// Dummy level, signifying all levels are disabled.
    Disable,
    /// Fatal errors.
    Fatal,
    /// Non-fatal/recoverable errors.
    Error,
    /// Warnings.
    Warning,
    /// Verbose app information.
    Info,
    /// Profiling information.
    Prof,
    /// Debugging messages.
    Debug,
    /// All messages enabled.
    All
}; // enum class LoggingLevels

} // namespace detail

#define REGISTER_LOGGING_LEVEL(LEVEL, PREFIX)                                           \
    struct LEVEL                                                                        \
    {                                                                                   \
        static constexpr bool wide{ false };                                            \
        static constexpr detail::LoggingLevel level{ detail::LoggingLevel::LEVEL };     \
        static constexpr const char *prefix{ PREFIX };                                  \
    };                                                                                  \
    struct W##LEVEL                                                                     \
    {                                                                                   \
        static constexpr bool wide{ true };                                             \
        static constexpr detail::LoggingLevel level{ detail::LoggingLevel::LEVEL };     \
        static constexpr const char *prefix{ PREFIX };                                  \
    }

REGISTER_LOGGING_LEVEL(Fatal,   "[FATAL] ");
REGISTER_LOGGING_LEVEL(Error,   "[ERROR] ");
REGISTER_LOGGING_LEVEL(Warning, "[WARN] ");
REGISTER_LOGGING_LEVEL(Info,    "[INFO] ");
REGISTER_LOGGING_LEVEL(Prof,    "[PROF] ");
REGISTER_LOGGING_LEVEL(Debug,   "[DEBUG] ");

#undef REGISTER_LOGGING_LEVEL

#define SET_LOGGING_LEVEL(LEVEL)                                                \
    namespace detail                                                            \
    {                                                                           \
    static auto MaxLogLevel = ::util::logging::detail::LoggingLevel::LEVEL;     \
    }

#define SET_DEFAULT_LOG_LEVEL(LEVEL)                                            \
    namespace detail                                                            \
    {                                                                           \
    using DefaultLevel = ::util::logging::LEVEL;                                \
    }

/// Default logging stream.
using DefaultLogStream = ThreadSafeLogStream<std::string>;
/// Default wide logging stream.
using DefaultWLogStream = ThreadSafeLogStream<std::wstring>;

/// Set the default logging stream.
template <typename TLevel>
struct StreamHelper
{
    using BaseStream = typename std::conditional<TLevel::wide, DefaultWLogStream, DefaultLogStream>::type;
};

#define SET_LEVEL_LOG_STREAM(LEVEL, LOG_STREAM)                         \
    template <>                                                         \
    struct StreamHelper<LEVEL>                                          \
    {                                                                   \
        using BaseStream = LOG_STREAM;                                  \
    };

} // namespace logging

} // namespace util
