/**
 * @file util/logging/MessageOutput.h
 * @author Tomas Polasek
 * @brief Tools for redirecting standard console output to WIN32 messages.
 */

#pragma once

#include "engine/util/Util.h"

#include "engine/util/logging/StringBuffer.h"

/// Utilities and helpers.
namespace util
{

/// Debug output stream.
extern std::ostream dout;
/// Wide character debug output stream.
extern std::wostream wdout;

// Debug message stream.
extern std::ostream mout;
/// Wide character debug output stream.
extern std::wostream wmout;
// Wide character debug message stream.

/// No operation syncer.
template <typename T>
struct VoidSyncer
{ };

/// List of message priorities.
enum class MessagePriority
{
    Info, 
    Warning, 
    Error
}; // enum class MessageType

/// Static configurator for logging.
class MessageOutputConfig
{
public:
    /// Get instance of the configuration.
    static MessageOutputConfig &instance()
    {
        static MessageOutputConfig cfg;
        return cfg;
    }

    /**
     * Force send the logging output to the console, independently on 
     * the debugger being present.
     */
    void forceConsoleOutput()
    { mDebuggerPresent = false; }

    /// Is the debugger currently attached to this application?
    bool isDebuggerPresent() const
    { return mDebuggerPresent; }
private:
    /// Initialize the members.
    MessageOutputConfig() : 
        mDebuggerPresent{ static_cast<bool>(::IsDebuggerPresent()) }
    { }

    /// Is the debugger currently attached to this application?
    bool mDebuggerPresent{ false };
protected:
}; // class MessageOutputConfig

/**
 * Message redirect configurator.
 * @tparam UseMessages When set to true, the standard 
 * streams will be redirected into windows message 
 * boxes, else they will go to console output.
 * @tparam SpecialSyncerT When specified, this syncer 
 * will be used for all messages. UseMessages will 
 * be ignored in this case. On construction, this 
 * object will be passed name of the stream and 
 * message priority.
 */
template <
    bool UseMessages, 
    template <typename> typename SpecialSyncerT = VoidSyncer>
class MessageOutput
{
public:
    /**
     * Configure output streams.
     * Redirects standard console outputs to WIN32 messages and 
     * configures the util::dout/util::wdout debug streams.
     * If Debugger is not present, then also configures the 
     * console output window.
     */
    MessageOutput();

    /// Wait for user input if console has been allocated.
    ~MessageOutput();
private:
    /// Configure console outputs for Windows.
    void configureConsole();

    /**
     * Rebind standard outputs - cout, cerr, clog and 
     * wcout, wcerr, wclog to work under Windows.
     */
    static void rebindStandardOutputs();

    /// Did we have to allocate console?
    bool mAllocatedConsole{ false };
protected:
}; // class MessageOutput

/**
 * Runtime chooser for debug printing 
 * buffer / console printing buffer.
 * @tparam StringTT Type of string.
 */
template <typename StringTT, 
          typename DebugBufferTT = StringBuffer<syncers::DebugStringSyncer<StringTT>>, 
          typename ConsoleBufferTT = StringBuffer<syncers::WriteConsoleSyncer<StringTT>>, 
          typename BaseStreamBufferTT = typename DebugBufferTT::BaseStreamBufferT, 
          typename RuntimeChooserTT = RuntimeChooser<DebugBufferTT, ConsoleBufferTT, BaseStreamBufferTT>>
struct DebugBufferChooser : public RuntimeChooserTT
{
    /// String type used by this chooser.
    using StringT = StringTT;
    /// Type of debug buffer.
    using DebugBufferT = DebugBufferTT;
    /// Type of console buffer.
    using ConsoleBufferT = ConsoleBufferTT;
    /// Type of the base stream buffer.
    using BaseStreamBufferT = BaseStreamBufferTT;

    /**
     * Initialize debug buffer or console 
     * buffer, based on return value from 
     * ::IsDebuggerPresent().
     */
    DebugBufferChooser() :
        RuntimeChooserTT(MessageOutputConfig::instance().isDebuggerPresent())
    { }
}; // struct DebugBufferChooser

/**
 * Helper used for rebinding standard outputs based 
 * on the template parameters.
 */
template <
    bool UseMessages,
    template <typename> typename SpecialSyncerT = VoidSyncer>
struct RebindHelper;

template <>
struct RebindHelper<true, VoidSyncer>
{
    /// Standard outputs -> MessageBox.
    static void rebindStandardOutputs();
}; // struct RebindHelper

template <>
struct RebindHelper<false, VoidSyncer>
{
    /// Standard outputs -> DebugString / WriteConsole.
    static void rebindStandardOutputs();
}; // struct RebindHelper

template <template <typename> typename SST>
struct RebindHelper<false, SST>
{
    /// Standard outputs -> SpecialSyncer.
    static void rebindStandardOutputs();
}; // struct RebindHelper

} // namespace util

// Template implementation

namespace util
{

template <bool UseMessages, template <typename> typename SpecialSyncerT>
MessageOutput<UseMessages, SpecialSyncerT>::MessageOutput()
{
    configureConsole();
    rebindStandardOutputs();
}

template <bool UseMessages, template <typename> typename SpecialSyncerT>
MessageOutput<UseMessages, SpecialSyncerT>::~MessageOutput()
{
    if (mAllocatedConsole)
    { // We should wait for user input, so that the console does not disappear.
        // TODO - Is this really the best way (probably not)?
        system("pause");
    }
}

template <bool UseMessages, template <typename> typename SpecialSyncerT>
void MessageOutput<UseMessages, SpecialSyncerT>::configureConsole()
{
    if (!IsDebuggerPresent())
    { // We are not in debugger.
        if (!AttachConsole(ATTACH_PARENT_PROCESS))
        { // We are probably not running in console -> allocate our own.
            mAllocatedConsole = AllocConsole();
        }

        FILE *newStdin{ nullptr };
        FILE *newStdout{ nullptr };
        FILE *newStderr{ nullptr };

        // Re-open standard handles just in case.
        freopen_s(&newStdin, "CONIN$", "rb", stdin);
        freopen_s(&newStdout, "CONOUT$", "wb", stdout);
        freopen_s(&newStderr, "CONOUT$", "wb", stderr);

        fputc('\n', stdout);
    }
}

template <bool UseMessages, template <typename> typename SpecialSyncerT>
void MessageOutput<UseMessages, SpecialSyncerT>::rebindStandardOutputs()
{ RebindHelper<UseMessages, SpecialSyncerT>::rebindStandardOutputs(); }

inline void RebindHelper<true, VoidSyncer>::rebindStandardOutputs()
{
    // TODO - Ugly...
    static StringBuffer<syncers::MessageBoxSyncer<std::string>> coutBuffer("std::cout");
    static StringBuffer<syncers::MessageBoxSyncer<std::string>> cerrBuffer("std::cerr");
    static StringBuffer<syncers::MessageBoxSyncer<std::string>> clogBuffer("std::clog");

    static StringBuffer<syncers::MessageBoxSyncer<std::wstring>> wCoutBuffer(L"std::wcout");
    static StringBuffer<syncers::MessageBoxSyncer<std::wstring>> wCerrBuffer(L"std::wcerr");
    static StringBuffer<syncers::MessageBoxSyncer<std::wstring>> wClogBuffer(L"std::wclog"); 

    std::cout.rdbuf(&coutBuffer);
    std::cerr.rdbuf(&cerrBuffer);
    std::clog.rdbuf(&clogBuffer);

    std::wcout.rdbuf(&wCoutBuffer);
    std::wcerr.rdbuf(&wCerrBuffer);
    std::wclog.rdbuf(&wClogBuffer);
}

inline void RebindHelper<false, VoidSyncer>::rebindStandardOutputs()
{
    // TODO - Ugly...
    static DebugBufferChooser<std::string> coutBuffer;
    static DebugBufferChooser<std::string> cerrBuffer;
    static DebugBufferChooser<std::string> clogBuffer;

    static DebugBufferChooser<std::wstring> wCoutBuffer;
    static DebugBufferChooser<std::wstring> wCerrBuffer;
    static DebugBufferChooser<std::wstring> wClogBuffer; 

    std::cout.rdbuf(coutBuffer.ptr());
    std::cerr.rdbuf(cerrBuffer.ptr());
    std::clog.rdbuf(clogBuffer.ptr());

    std::wcout.rdbuf(wCoutBuffer.ptr());
    std::wcerr.rdbuf(wCerrBuffer.ptr());
    std::wclog.rdbuf(wClogBuffer.ptr());
}

template <template <typename> typename SST>
inline void RebindHelper<false, SST>::rebindStandardOutputs()
{
    // TODO - Ugly...
    static StringBuffer<SST<std::string>> coutBuffer("std::cout", MessagePriority::Info);
    static StringBuffer<SST<std::string>> cerrBuffer("std::cerr", MessagePriority::Error);
    static StringBuffer<SST<std::string>> clogBuffer("std::clog", MessagePriority::Info);

    static StringBuffer<SST<std::wstring>> wCoutBuffer(L"std::wcout", MessagePriority::Info);
    static StringBuffer<SST<std::wstring>> wCerrBuffer(L"std::wcerr", MessagePriority::Error);
    static StringBuffer<SST<std::wstring>> wClogBuffer(L"std::wclog", MessagePriority::Info); 

    std::cout.rdbuf(&coutBuffer);
    std::cerr.rdbuf(&cerrBuffer);
    std::clog.rdbuf(&clogBuffer);

    std::wcout.rdbuf(&wCoutBuffer);
    std::wcerr.rdbuf(&wCerrBuffer);
    std::wclog.rdbuf(&wClogBuffer);
}

}

// Template implementation end
