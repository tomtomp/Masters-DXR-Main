/**
 * @file util/logging/LoggingLogger.h
 * @author Tomas Polasek
 * @brief Logger class and functions.
 */

#pragma once

/// Utilities and helpers.
namespace util
{

/// Logging classes and utilities.
namespace logging
{

/// Implementation details for logging.
namespace detail
{

// Undefined user configuration macros.
#undef SET_LOGGING_LEVEL
#undef SET_DEFAULT_LOG_LEVEL
#undef SET_LEVEL_LOG_STREAM

/// Choose void stream, if the logging level is low enough.
template <typename TLevel>
struct LoggerDisableHelper
{
    /// Choose stream based on level of logging.
    using StreamType = std::conditional <TLevel::level < MaxLogLevel,
        typename StreamHelper<TLevel>::BaseStream,  // Level is enabled.
        VoidLogStream>;                             // Level is disabled.
};

/// Choose logging stream based on chosen log level.
template <typename TLevel, typename TStream = typename StreamHelper<TLevel>::BaseStream>
class Logger
{
public:
    using StreamType = decltype(TStream::instance());

    static inline StreamType &stream()
    { return TStream::instance() << TLevel::prefix; }

    static inline StreamType &streamNoPrefix()
    { return TStream::instance(); }
private:
protected:
}; // class Logger

} // namespace detail

/**
 * Get stream used for logging given logging level.
 * @tparam TLevel Which logging level should be used.
 * @return Returns stream which can be used similarly as std::cout.
 */
template <typename TLevel>
static inline typename detail::Logger<TLevel>::StreamType &log() 
{ return detail::Logger<TLevel>::stream(); }

/**
 * Get stream used for logging given logging level.
 * This variant does not print prefix before the message.
 * @tparam TLevel Which logging level should be used.
 * @return Returns stream which can be used similarly as std::cout.
 */
template <typename TLevel>
static inline typename detail::Logger<TLevel>::StreamType &lognp() 
{ return detail::Logger<TLevel>::streamNoPrefix(); }

/**
 * Get stream used for logging the default logging level.
 * @return Returns stream which can be used similarly as std::cout.
 */
static inline detail::Logger<detail::DefaultLevel>::StreamType &log() 
{ return log<detail::DefaultLevel>(); }

/**
 * Get stream used for logging the default logging level.
 * This variant does not print prefix before the message.
 * @return Returns stream which can be used similarly as std::cout.
 */
static inline detail::Logger<detail::DefaultLevel>::StreamType &lognp() 
{ return lognp<detail::DefaultLevel>(); }

/**
 * Get mutex for logging on given level.
 * @tparam TLevel Which logging level should be used.
 * @return Returns reference to the mutex for given 
 * logging level.
 */
template <typename TLevel>
static inline std::mutex &logMtx()
{
    static std::mutex mtx;
    return mtx;
}

} // namespace logging

} // namespace util
