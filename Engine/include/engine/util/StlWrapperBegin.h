/**
* @file util/StlWrapperBegin.h
* @author Tomas Polasek
* @brief Start of section wrapping STL/standard library includes.
*/

#ifdef _MSC_VER
// Disable some warnings: 
#	pragma warning(push)

//#   pragma warning(disable: 4625 4626 4619 4127 4512 4555 4548 4100 4350 4610 4510 4702 6295 4242 4244 4574 4702 4365 4265)

// Potentially throwing C functions.
#	pragma warning(disable : 5039)
// Virtual functions but destructor not virtual.
#	pragma warning(disable : 4265)
// 'this' used in initializer list.
#	pragma warning(disable : 4355)
// Allow implicit calls to destructor -> std::optional, etc.
#	pragma warning(disable : 4582)
#	pragma warning(disable : 4583)
// Unreferenced formal parameter.
#	pragma warning(disable : 4100)
// Local variable initialized but not referenced.
#	pragma warning(disable : 4189)
// Unreachable code.
#	pragma warning(disable : 4702)
#   pragma warning(disable : 4437)
#   pragma warning(disable : 4996)

// Should be fine, using codecvt, until replacement is standardized...
#define _SILENCE_CXX17_CODECVT_HEADER_DEPRECATION_WARNING

#endif
