/**
 * @file util/prof/PrintCrawler.h
 * @author Tomas Polasek
 * @brief Printing class for the profiler.
 */

#pragma once

#include "engine/util/prof/Profiler.h"
#include "engine/util/logging/Logging.h"

/// Utilities and helpers.
namespace util
{

/// Profiler and tools used by it.
namespace prof
{

/**
 * Crawler which prints information about the call stack.
 */
class PrintCrawler : public CallStackCrawler
{
public:
    /// Default crawler.
    PrintCrawler() = default;

    /**
     * Crawler which contains specification whether to 
     * print inactive nodes.
     * @param skipInactive When set to true, the crawler 
     * will skip nodes which were not active in the last 
     * run of the timers.
     */
    PrintCrawler(bool skipInactive) :
        mSkipInactive{ skipInactive }
    { }

    /**
     * Function is called by the ProfilingManager.
     * @param root The root node of the profiling manager.
     */
    virtual void crawl(prof::ThreadNode *root) override
    {
        auto &mgr = ProfilingManager::instance();

        // Print stats
        log<Prof>() << "Profiling statistics: "
            "\n\tScopes entered : " << mgr.getNumScopeEnter() <<
            "\n\tScopes exited : " << mgr.getNumScopeExit() <<
            "\n\tOverhead per scope [ticks] : " << mgr.getScopeOverhead() <<
            "\n\tThreads entered : " << mgr.getNumThreadEnter() <<
            "\n\tThreads exited : " << mgr.getNumThreadExit() <<
            "\n\t============================" <<
            "\n\t\t         Sum[Mt]   Parent[%]  Samples[num]       Avg[Mt]   Last/Curr[Mt]        Self[Mt]     Self[%] \t Name" << 
            std::endl;

        ForeachPrintThread threadPrinter(mSkipInactive);
        root->foreachnn(threadPrinter);

        lognp<Prof>() << "\n\t============================" << std::endl;
    }
private:
    /// Helper functor to print each threads information.
    class ForeachPrintThread
    {
    public:
        ForeachPrintThread(bool skipInactive) :
            mSkipInactive{ skipInactive }
        { }

        void operator()(prof::ThreadNode *node) 
        {
            lognp<Prof>() << "\t" << node->name() << ":" << std::endl;

            prof::ForeachPrint printer(mSkipInactive, 0u, 0.0);

            {
#ifdef PROFILE_LOCK
                std::lock_guard l(node->data().getLock());
#endif
                printer(node->data().getRoot(), true);
            }
        }
    private:
    /// Should inactive scopes be skiped?
        bool mSkipInactive{ false };
    protected:
    }; // class ForeachPrintThread

    /// Should inactive scopes be skipped?
    bool mSkipInactive{ false };
protected:
}; // class PrintCrawler

} // namespace prof

} // namespace util
