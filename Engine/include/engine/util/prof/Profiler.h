/**
 * @file util/prof/Profiler.h
 * @author Tomas Polasek
 * @brief Code profiler.
 */

#pragma once

#include "engine/util/StlWrapperBegin.h"

#include "engine/util/Asserts.h"
#include "engine/util/Threading.h"
#include "engine/util/prof/HashNode.h"

#include <atomic>
#include <thread>
#include <limits>

#ifdef _WIN32
#	include <intrin.h>
#endif

// Profiling macros:
#ifndef NPROFILE
#   define PROF_INITIALIZE() ::util::prof::ProfilingManager::initialize()
#   define PROF_CONCAT(FIRST, SECOND) \
        FIRST ## SECOND
#   define PROF_NAME(FIRST, SECOND) \
        PROF_CONCAT(FIRST, SECOND)
/// Start a profiling block with given name.
#   define PROF_BLOCK(NAME) \
        ::util::prof::BlockProfiler PROF_NAME(_bp, __LINE__)(NAME)
/// End the last profiling block.
#   define PROF_BLOCK_END() \
        ::util::prof::BlockProfiler::endCurrent()
/// Start a scoped profiling with given name. Automatically ends on end of scope.
#   define PROF_SCOPE(NAME) \
        ::util::prof::ScopeProfiler PROF_NAME(_sp, __LINE__)(NAME)
/// Dump current profiling information using given crawler.
#   define PROF_DUMP(CRAWLER) \
        ::util::prof::ProfilingManager::instance().useCrawler(CRAWLER)
/// Specify name for current thread within the profiler.
#   define PROF_THREAD(NAME) \
        ::util::prof::ThreadProfiler PROF_NAME(_tp, __LINE__)(NAME)
#else
#   define PROF_CONCAT(F, S)
#   define PROF_NAME(F, S)
/// Start a profiling block with given name.
#   define PROF_BLOCK(_)
/// End the last profiling block.
#   define PROF_BLOCK_END(_)
/// Start a scoped profiling with given name. Automatically ends on end of scope.
#   define PROF_SCOPE(_)
/// Dump current profiling information using given crawler.
#   define PROF_DUMP(_)
/// Specify name for current thread within the profiler.
#   define PROF_THREAD(_)
#endif

/// Utilities and helpers.
namespace util
{

/// Profiler and tools used by it.
namespace prof
{

/**
 * Timer class, using rdtsc instruction.
 */
class Timer
{
public:
    static constexpr double TICKS_TO_MEGACYCLES{ 1000000.0 };

    Timer()
    { reset(); }

    /**
     * Reset this timer.
     */
    void reset()
    {
        mNumSamples = 0u;
        mRunning = false;
        mStart = 0u;
        mTicks = 0u;
        mLastTicks = 0u;
    }

    /**
     * Get the current number of ticks taken by active timer.
     * Returns 0, if the timer is not active.
     * @return Returns the actual number of ticks from active timer.
     */
    uint64_t runningTicks() const
    {
        if (mRunning)
        { return getTicks() - mStart; }

        return 0;
    }

    /**
     * Get the current number of megacycles from running timer.
     * Returns 0.0, if the timer is not running.
     * @return Returns the number of megacycles, from the start of this timer.
     */
    double runningMegacycles() const
    { return ticksToMegacycles(runningTicks()); }

    /**
     * Convert ticks to megacycles.
     * @param ticks Number of ticks.
     * @return Number of megacycles.
     */
    static double ticksToMegacycles(uint64_t ticks)
    { return ticks / TICKS_TO_MEGACYCLES; }

    /**
     * Convert megacycles to ticks.
     * @param megacycles Amount of megacycles.
     * @return Amount of ticks.
     */
    static uint64_t megacyclesToTicks(double megacycles)
    { return static_cast<uint64_t>(megacycles * TICKS_TO_MEGACYCLES); }

    /**
     * Add given number of ticks to the internal counter.
     * @param ticks Number of ticks to add.
     */
    void addTicks(uint64_t ticks)
    { mTicks += ticks; }

    /**
     * Start the timer.
     */
    void start()
    {
        mNumSamples++;
        mRunning = true;
        mStart = getTicks();
    }

    /**
     * Stop the timer and calculate the number of ticks
     * passed between now and start.
     */
    void stop()
    {
        mLastTicks = (getTicks() - mStart);
        mLastRun = true;
        mTicks += mLastTicks;
        mRunning = false;
    }

    /**
     * Simulate the running of this timer using provided 
     * start and stop values.
     * @param start Timer start time in absolute ticks.
     * @param stop Timer stop time in absolute ticks.
     */
    void simulatedRuntime(uint64_t start, uint64_t stop)
    {
        mNumSamples++;
        mLastTicks = (stop - start);
        mLastRun = true;
        mTicks += mLastTicks;
        mRunning = false;
    }

    /**
     * Stop the timer, only if it is running.
     */
    void condStop()
    {
        if (mRunning)
        { stop(); }
    }

    /**
     * Get the number of ticks counted by this timer.
     * @return The number of ticks.
     */
    uint64_t ticks() const
    { return mTicks; }

    /**
     * Get the actual number of ticks.
     * If the timer is currently running, count the ticks
     * from the start.
     * @return The actual number of ticks.
     */
    uint64_t actualTicks() const
    {
        if (mRunning)
        { return mTicks + (getTicks() - mStart); }
        else
        { return mTicks; }
    }

    /**
     * Set the number of megacycles to given number.
     * @param megacycles Number of megacycles.
     */
    void setMegacycles(double megacycles)
    { mTicks = megacyclesToTicks(megacycles); }

    /**
     * Get the number of megacycles counted by this timer.
     * @return Returns the number of ticks divided by 10^6
     */
    double megacycles() const
    { return ticksToMegacycles(mTicks); }

    /**
     * Get the actual number of megacycles counted by this timer.
     * @return Returns the actual number of ticks divided by 10^6
     */
    double actualMegacycles() const
    { return ticksToMegacycles(actualTicks()); }

    /**
     * Get the average number of megacycles.
     * @return Get the average number of megacycles per sample
     */
    double avg() const
    { return megacycles() / mNumSamples; }

    /**
     * Get the average number of megacycles.
     * @return Get the average number of megacycles per sample
     */
    double actualAvg() const
    { return actualMegacycles() / mNumSamples; }

    /**
     * Get the average number of ticks.
     * @return Get the average number of ticks per sample
     */
    double avgTicks() const
    { return static_cast<double>(mTicks) / mNumSamples; }

    /// Get the number of samples taken by this timer.
    uint64_t numSamples() const
    { return mNumSamples; }

    /// Get the actual number of samples, including the running one.
    uint64_t actualNumSamples() const
    { return mNumSamples; }

    /// Reset the last tick number and last run flags.
    void resetLastTicks()
    {
        mLastTicks = 0u;
        mLastRun = false;
    }

    /// Get the number of ticks from the last run of this timer.
    uint64_t lastTicks() const
    { return mLastTicks; }

    /// Get the number of megacycles from the last run of this timer.
    double lastMegacycles() const
    { return ticksToMegacycles(lastTicks()); }

    /// Did this timer run in the last iteration?
    bool didRun() const
    { return mLastRun; }

    /// Is this timer currently running?
    bool isRunning() const
    { return mRunning; }

    /**
     * Get current number of ticks passed since reset.
     * Uses rdtsc instruction.
    * @return The number of ticks passed.
    */
    static uint64_t getTicks()
    {
#ifndef PROF_USE_TICKS
        return
            std::chrono::duration_cast<std::chrono::nanoseconds>
            (std::chrono::high_resolution_clock::now().time_since_epoch()).count();
#else // !PROF_USE_TICKS
#ifdef _WIN32
        return __rdtsc();
#else // _WIN32
#if 1
        u32 lo;
        u32 hi;
        __asm__ __volatile__("rdtsc" : "=a" (lo), "=d" (hi));
        return ((u64)hi << 32) | lo;
#elif 0
        timespec t;
        clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &t);
        return static_cast<u64>(t.tv_nsec);
#else
        return
            std::chrono::duration_cast<std::chrono::nanoseconds>
            (std::chrono::high_resolution_clock::now().time_since_epoch()).count();
#endif
#endif // _WIN32
#endif // !PROF_USE_TICKS
    }
private:
protected:
    /// Start time in ticks.
    uint64_t mStart{ 0u };
    /// Current time passed in ticks.
    uint64_t mTicks{ 0u };
    /// Number of samples taken with this timer.
    uint64_t mNumSamples{ 0u };
    /// Number of ticks from the last run of this timer.
    uint64_t mLastTicks{ 0u };
    /// Did this timer run in the last iteration?
    bool mLastRun{ false };
    /// Is the timer currently running?
    bool mRunning{ false };
}; // class Timer

/**
 * Scope timer. Uses already existing Timer to measure ticks taken
 * for a single scope.
 */
class ScopeTimer
{
public:
    explicit ScopeTimer(Timer &timer) :
        mTimer(timer)
    { mTimer.start(); }

    ScopeTimer(const ScopeTimer &other) = delete;
    ScopeTimer(ScopeTimer &&other) = delete;
    ScopeTimer &operator=(const ScopeTimer &rhs) = delete;
    ScopeTimer &operator=(ScopeTimer &&rhs) = delete;

    ~ScopeTimer()
    { mTimer.stop(); }
private:
protected:
    /// Holds reference to the used timer object.
    Timer &mTimer;
}; // class ScopeTimer

/**
 * Measurement data for each node.
 */
struct CallData
{
public:
    /// Start the timer.
    void startTimer()
    { mTimer.start(); }

    /// Stop the timer.
    void stopTimer()
    { mTimer.stop(); }

    /// Get the current megacycles from the timer.
    double getActualMegacycles() const
    { return mTimer.actualMegacycles(); }

    /// Get the current average megacycles per run.
    double getActualAvg() const
    { return mTimer.actualAvg(); }

    /// Get the number of samples taken by the timer.
    uint64_t getActualNumSamples() const
    { return mTimer.actualNumSamples(); }

    /**
     * Get the number of megacycles in the last run 
     * of this node.
     */
    double getLastMegacycles() const
    {
        const auto result{ mTimer.isRunning() ? 
            mTimer.runningMegacycles() : 
            mTimer.lastMegacycles() };

        // We may have reset the timer, recover the last value.
        return result > 0.0 ? result : mCachedLast;
    }

    /// Did this scope run in the last iteration?
    bool lastRun() const
    { return mTimer.didRun() || mTimer.isRunning(); }

    /**
     * Get the number of megacycles in the last run 
     * of this node and reset the counter.
     */
    double getLastMegacyclesAndReset() 
    {
        if (mTimer.isRunning())
        { // When running, just get the current running time.
            return mTimer.runningMegacycles();
        }
        else
        { // Else we get the runtime of last iteration and reset.
            const auto result{ mTimer.lastMegacycles() };
            mTimer.resetLastTicks();
            mCachedLast = result;

            return result;
        }
    }

    /// Add given number of ticks to the timer.
    void addTicks(uint64_t ticks)
    { mTimer.addTicks(ticks); }

    /**
     * Simulate the running of the timer using provided 
     * start and stop values.
     * @param start Timer start time in absolute ticks.
     * @param stop Timer stop time in absolute ticks.
     */
    void simulatedRuntime(uint64_t start, uint64_t stop)
    { mTimer.simulatedRuntime(start, stop); }
private:
protected:
    /// Timer used for timing this node.
    Timer mTimer;

    /// Cached value of the last timer result.
    double mCachedLast{ 0.0 };
}; // class CallData

using CallNode = util::HashNode<CallData>;

/**
 * Data for each thread name.
 */
struct ThreadData
{
public:
    ThreadData() :
        mRoot(new CallNode("/", nullptr))
    { }

    ThreadData(const ThreadData &other) = delete;
    ThreadData(ThreadData &&other) = delete;
    ThreadData &operator=(const ThreadData &rhs) = delete;
    ThreadData &operator=(ThreadData &&rhs) = delete;

    ~ThreadData()
    { delete mRoot; }

    /**
     * Try to occupy this thread call stack.
     * @return Returns true, if the occupation succeeded, else
     *  returns false.
     */
    bool occupy()
    { return mOccupied.tryLock(); }

    /**
     * Free this call stack.
     */
    void freeOccupation()
    { mOccupied.unlock(); }

    /// Get lock for this thread.
    std::mutex &getLock()
    { return mLock; }

    /// Get the root of threads call stack.
    CallNode *getRoot()
    { return mRoot; }
private:
protected:
    /// Used for locking the call stack.
    std::mutex mLock;

    /// Root node of the call stack.
    CallNode *mRoot;

    /// Only one thread is allowed in each thread call stack.
    thread::SpinLock mOccupied;
}; // class ThreadData

using ThreadNode = util::HashNode<ThreadData>;

/**
 * Print information about each node.
 */
class ForeachPrint
{
public:
    /// Initialize printer parameters.
    ForeachPrint(bool skipInactive = false, uint32_t indent = 0, double parentMegacycles = 0);

    /// Run the printer, skip inactive will exclude scopes with 0 in the last run.
    void operator()(CallNode *node, bool rootLevel = false) const;
protected:
    /// Level of indentation.
    uint32_t mIndentLevel{ 0 };
    /// Megacycles of parent node.
    double mParentMegacycles{ 0.0 };
    /// Should the inactive scopes be skipped?
    bool mSkipInactive{ false };
}; // class ForeachPrint

/**
 * Sum megacycles of all children
 */
class ForeachSumMegacycles
{
public:
    /// Run the summer.
    void operator()(CallNode *node);

    /**
     * Sum getter.
     * @return Returns the sum.
     */
    double sum() const
    { return mSum; }

    /**
     * Number of samples getter.
     * @return Number of samples.
     */
    uint64_t numSamples() const
    { return mNumSamples; }
protected:
    /// Sum of megacycles.
    double mSum{ 0.0 };
    /// Number of samples taken.
    uint64_t mNumSamples{ 0 };
}; // class ForeachSumMegacycles

/**
 * Contains information about threads call stack.
 */
class ThreadStatus
{
public:
    /// Ptr to the current node in the call stack.
    //CallNode *mCurNode{nullptr};
    CallNode *mCurNode;
    /// Ptr to the root node of the call stack.
    //CallNode *mRoot{nullptr};
    CallNode *mRoot;
    /// Ptr to the current thread node.
    //ThreadNode *mThreadNode{nullptr};
    ThreadNode *mThreadNode;
private:
protected:
}; // class ThreadStatus

/**
 * Abstract call stack crawler.
 */
class CallStackCrawler
{
public:
    /**
     * Function is called by the ProfilingManager.
     * @param root The root node of the profiling manager.
     */
    virtual void crawl(ThreadNode *root) = 0;

    CallStackCrawler() = default;

    CallStackCrawler(const CallStackCrawler &other) = delete;
    CallStackCrawler(CallStackCrawler &&other) = delete;
    CallStackCrawler &operator=(const CallStackCrawler &other) = delete;
    CallStackCrawler &operator=(CallStackCrawler &&other) = delete;

    virtual ~CallStackCrawler() = default;
private:
protected:
}; // class CallStackCrawler

/**
 * Used for simulating threads and scopes, when the profiling 
 * data is not taken through the standard timers.
 */
class SimulatedProfilingContext
{
public:
    class LateScopeSimulator
    {
    public:
        /// Empty simulator.
        LateScopeSimulator() = default;

        /**
         * Simulate the running of the current scope.
         * @param start Timer start time in absolute ticks.
         * @param stop Timer stop time in absolute ticks.
         */
        void simulateScope(uint64_t start, uint64_t stop);

        /**
         * Get hash value unique to the current scope.
         * Usable for mapping of user values to scopes.
         */
        std::size_t scopeHash() const;
    private:
        friend class SimulatedProfilingContext;

        /// Simulator for given thread-scope combination.
        LateScopeSimulator(ThreadNode *thread, CallNode *scope);

        /// Thread used by this simulator.
        ThreadNode *mThread{ nullptr };
        /// Scope simulated.
        CallNode *mScope{ nullptr };
    protected:
    }; // class LateScopeSimulator

    /// Empty context.
    SimulatedProfilingContext() = default;

    // Allow users to move the context.
    SimulatedProfilingContext(const SimulatedProfilingContext &other) = delete;
    SimulatedProfilingContext &operator=(const SimulatedProfilingContext &other) = delete;
    SimulatedProfilingContext(SimulatedProfilingContext &&other) = default;
    SimulatedProfilingContext &operator=(SimulatedProfilingContext &&other) = default;

    /// Exits the simulated thread.
    ~SimulatedProfilingContext();

    /**
     * Simulate entering scope with given name, without 
     * starting the timer.
     * @param name Name of the scope.
     */
    void enterScope(const char *name);

    /**
     * Simulate the running of the current scope.
     * @param start Timer start time in absolute ticks.
     * @param stop Timer stop time in absolute ticks.
     */
    void simulateScopeRuntime(uint64_t start, uint64_t stop);

    /**
     * Get simulator object, which can be used for later 
     * simulation of scope runtime.
     * @return Returns the simulator object.
     */
    LateScopeSimulator lateSimulatorForCurrentScope();

    /**
     * Simulate exiting the current scope, without stopping 
     * the timer.
     * If the current scope is the root scope, no actions are 
     * taken.
     * @return Return true if the scope has been exitted, else 
     * returns false -- in case when already at the root scope.
     */
    bool exitScope();

    /**
     * Simulate the running of the simulated thread.
     * Has the same effect as using simulateScopeRuntime 
     * while at the root scope.
     * @param start Timer start time in absolute ticks.
     * @param stop Timer stop time in absolute ticks.
     */
    void simulateThreadRuntime(uint64_t start, uint64_t stop);

    /**
     * Get hash value unique to the current scope.
     * Usable for mapping of user values to scopes.
     */
    std::size_t currentScopeHash();

    /// Has the context been initialized?
    bool initialized() const;
private:
    // Allow access to the context data.
    friend class ProfilingManager;

    /// Context for given simulated thread.
    SimulatedProfilingContext(ThreadNode *simulatedThread, CallNode *rootNode);

    /// Current profiling thread.
    ThreadNode *mSimulatedThread{ nullptr };
    /// Root node of the simulated profiling thread.
    CallNode *mSimulatedThreadRoot{ nullptr };
    /// Current profiling scope.
    CallNode *mSimulatedScope{ nullptr };
protected:
}; // class SimulatedProfilingContext

/**
 * Profiling manager keeps information about each thread
 * and its location in the call stack.
 * It also contains the interface to all profiling functions.
 */
class ProfilingManager
{
public:
    /// Initialize profiling structures.
    ProfilingManager();

    ProfilingManager(const ProfilingManager &other) = delete;
    ProfilingManager(ProfilingManager &&other) = delete;
    ProfilingManager &operator=(const ProfilingManager &other) = delete;
    ProfilingManager &operator=(ProfilingManager &&other) = delete;

    /// Free resources and clean up.
    ~ProfilingManager();

    /**
     * Enter a scope with given name.
     * @param name Identifier of the scope.
     * @return Returns ptr to the scope node.
     */
    CallNode *enterScope(const char *name);

    /**
     * Exit the current scope and go to the
     * parent scope.
     * If there is no parent scope, returns nullptr.
     * @return Returns ptr to the parent, or nullptr.
     */
    CallNode *exitScope();

    /**
     * Enter a thread with given name.
     * @param name Identifier of the thread.
     * @return Returns ptr to the thread node.
     */
    ThreadNode *enterThread(const char *name);

    /**
     * Exit the current thread.
     */
    void exitThread();

    /**
     * Start simulation of profiling for the thread 
     * with given name.
     * @param name Name of the thread.
     */
    SimulatedProfilingContext enterSimulatedThread(const char *name);

    /**
     * Get the current scope node.
     * @return Returns ptr to the current node,
     * should never return nullptr.
     */
    CallNode *getCurrentScope();

    /**
     * Use given crawler object to crawl through
     * the call stack.
     * @param crawler Reference to the crawler object.
     */
    void useCrawler(CallStackCrawler &crawler);

    /**
     * Get the overhead of entering and exiting scope.
     * @return The overhead.
     */
    double getScopeOverhead() const
    { return mScopeOverhead; }

    // Statistics getters
    uint64_t getNumScopeEnter() const
    { return mNumScopeEnter; }

    uint64_t getNumScopeExit() const
    { return mNumScopeExit; }

    uint64_t getNumThreadEnter() const
    { return mNumThreadEnter; }

    uint64_t getNumThreadExit() const
    { return mNumThreadExit; }

    /**
     * Initialize the profiler with current thread being the 
     * main execution thread.
     */
    static void initialize();

    /**
     * Get instance of the profiling manager.
     * @return Singleton instance of the profiling manager.
     */
    static ProfilingManager &instance();
private:
    // Allow access to the private simulation methods.
    friend class SimulatedProfilingContext;

    /// Reset profiling into default - empty - state.
    void reset();

    /**
     * Simulate entering scope in given context.
     * @param ctx Context used in this simulation.
     * @param scopeName Name of the scope being entered.
     */
    void enterSimulatedScope(SimulatedProfilingContext *ctx, const char *scopeName);

    /**
     * Simulate exitting scope in given context.
     * Does not exit scope when in the root scope!
     * @param ctx Context used in this simulation.
     * @return Returns true if the exit operation has been 
     * actually performed.
     */
    bool exitSimulatedScope(SimulatedProfilingContext *ctx);

    /**
     * Stop simulating profiling for given context and its 
     * thread.
     * @param ctx Context used in this simulation.
     */
    void exitSimulatedThread(SimulatedProfilingContext *ctx);
protected:
    /// Root node of the profiling tree.
    ThreadNode * mRoot{ nullptr };
    /// Global lock for adding threads to the ProfilingManager.
    thread::SpinLock mGlobalLock;
    /// Thread local storage containing call stack information.
    thread_local static ThreadStatus mThreadStatus;
    // Statistics
    uint64_t mNumScopeEnter{ 0u };
    uint64_t mNumScopeExit{ 0u };
    uint64_t mNumThreadEnter{ 0u };
    uint64_t mNumThreadExit{ 0u };
    /// Overhead per enter/exit scope.
    double mScopeOverhead{ 0.0 };

    /// Used for checking, that only one construction occurred.
    static uint64_t sConstructions;
}; // class ProfilingManager

/// Profiler for a single scope.
class ScopeProfiler
{
public:
    /**
     * Create a scope profiler, add it to the global structure
     * (if necessary) and start the timer.
     * @param desc Description for this profiler.
     */
    ScopeProfiler(const char *desc);

    ScopeProfiler(const ScopeProfiler &other) = delete;
    ScopeProfiler(ScopeProfiler &&other) = delete;
    ScopeProfiler &operator=(const ScopeProfiler &other) = delete;
    ScopeProfiler &operator=(ScopeProfiler &&other) = delete;

    /**
     * Exit the scope and stop the timer.
     */
    ~ScopeProfiler();
private:
protected:
}; // class ScopeProfiler

/// Profiler for a single code block.
class BlockProfiler
{
public:
    /**
     * Create a block profiler, add it to the global structure
     * (if necessary) and start the timer.
     * @param desc Description for this profiler.
     */
    BlockProfiler(const char *desc);

    BlockProfiler(const BlockProfiler &other) = delete;
    BlockProfiler(BlockProfiler &&other) = delete;
    BlockProfiler &operator=(const BlockProfiler &other) = delete;
    BlockProfiler &operator=(BlockProfiler &&other) = delete;

    /**
     * Does NOT complete the profiling block!
     */
    ~BlockProfiler() = default;

    /**
     * Exit the scope and stop the timer.
     */
    void end();

    /**
     * Exit the current scope and stop the timer.
     */
    static void endCurrent();
private:
    /// has this profiler been ended already?
    bool mEnded{ false };
protected:
}; // class BlockProfiler

/// Profiler for a single thread.
class ThreadProfiler
{
public:
    /**
     * Create a thread profiler.
     * @param desc Description for this profiler.
     */
    ThreadProfiler(const char *desc);

    ThreadProfiler(const ThreadProfiler &other) = delete;
    ThreadProfiler(ThreadProfiler &&other) = delete;
    ThreadProfiler &operator=(const ThreadProfiler &other) = delete;
    ThreadProfiler &operator=(ThreadProfiler &&other) = delete;

    /**
     * End the thread profiler.
     */
    ~ThreadProfiler();
private:
protected:
}; // class ThreadProfiler

/**
 * Start simulation of profiling for the thread 
 * with given name.
 * Uses the global profiling system.
 * @param name Name of the thread.
 */
SimulatedProfilingContext enterSimulatedThread(const char *name);

} // namespace prof

} // namespace util

#include "engine/util/StlWrapperEnd.h"

