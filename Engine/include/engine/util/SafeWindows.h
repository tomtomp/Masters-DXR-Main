/**
 * @file util/SafeWindows.h
 * @author Tomas Polasek
 * @brief Safe way to include windows headers, including:
 *   <windows.h>
 *   <shellapi.h>
 *   <wrl.h>
 */

#pragma once

#include "engine/util/StlWrapperBegin.h"

#ifndef WIN32_LEAN_AND_MEAN
// Exclude rarely-used functions from the windows header.
#	define WIN32_LEAN_AND_MEAN
#	define DEFINED_WIN32_LEAN_AND_MEAN_DEFINED
#endif

// Windows Header Files:
#include <windows.h>

// Fix for missing shortcuts in the ATL.
#ifndef DeleteFile
#   define DeleteFile DeleteFileA
#endif // DeleteFile

#ifndef MoveFile
#   define MoveFile MoveFileA
#endif // MoveFile

#ifndef GetMessage
#   define GetMessage GetMessageW
#endif // GetMessage

#ifndef FindResource
#   define FindResource FindResourceA
#endif // FindResource

#include <WinBase.h>
#include <WinUser.h>
#include <shellapi.h>
#include <wrl.h>

// Disable windows min/max and use c++ algorithm ones.
#ifdef min
#	undef min
#endif

#ifdef max
#	undef max
#endif

#ifdef DEFINED_WIN32_LEAN_AND_MEAN_DEFINED
#	undef WIN32_LEAN_AND_MEAN
#	undef DEFINED_WIN32_LEAN_AND_MEAN_DEFINED
#endif

#include "engine/util/StlWrapperEnd.h"
