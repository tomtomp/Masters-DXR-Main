/**
 * @file util/Util.h
 * @author Tomas Polasek
 * @brief Utilities and helper functions/classes.
 */

#pragma once

#include "engine/util/SafeWindows.h"

#include "engine/util/StlWrapperBegin.h"

#include <string>
#include <iomanip>
#include <sstream>
#include <optional>
#include <list>
#include <locale>
#include <codecvt>

/// Utilities and helpers.
namespace util
{

/**
 * Transforming printf format and parameters into std::string.
 * Inspired by https://stackoverflow.com/questions/69738/c-how-to-get-fprintf-results-as-a-stdstring-w-o-sprintf#69911
 * @param fmt Format used by printf.
 * @param ... Parameters used in the formatting string.
 */
std::string format(const char *fmt, ...);

/**
 * Transforming printf format and parameters into std::string.
 * Inspired by https://stackoverflow.com/questions/69738/c-how-to-get-fprintf-results-as-a-stdstring-w-o-sprintf#69911
 * @param fmt Format used by printf.
 * @param ap Parameters used in the formatting string.
 */
std::string vFormat(const char *fmt, va_list ap);

/// Get number of elements of given array.
template <typename T, std::size_t N>
static constexpr std::size_t count(const T (&arr)[N])
{ return N; }

/// Get number of elements of given array, convert to type.
template <typename TargetT, typename T, std::size_t N>
static constexpr TargetT count(const T (&arr)[N])
{ return static_cast<TargetT>(::util::count(arr)); }

/**
 * Get windows error as a string message.
 * Source: https://stackoverflow.com/a/17387176 .
 * @param error Error to transform into a string.
 * @return Returns string representation of provided error code.
 */
std::string getErrorAsString(DWORD error);

/**
 * Get windows error as a wide string message.
 * Source: https://stackoverflow.com/a/17387176 .
 * @param error Error to transform into a string.
 * @return Returns string representation of provided error code.
 */
std::wstring getErrorAsWString(DWORD error);

/**
 * Attempt to convert wide string into string.
 * @param original Original wide string.
 * @return Returns original string converted into a string.
 */
std::string toString(const std::wstring &original);

/**
 * Convert given string to wide character string..
 * @param original Original string.
 * @return Returns original string converted into a wide string.
 */
std::wstring toWString(const std::string &original);

/**
 * Automatically convert input string to output string.
 * @tparam ToStrT Destination string type.
 * @tparam FromStrT Source string type.
 * @param original Source string.
 * @return String in destination string type.
 */
template <typename ToStrT, typename FromStrT>
ToStrT toAnyString(const FromStrT &original);

/**
 * Get last windows error as a string message.
 * @return Returns string representation of the ::GetLastError().
 */
std::string getLastErrorAsString();

/**
 * Get last windows error as a wstring message.
 * @return Returns string representation of the ::GetLastError().
 */
std::wstring getLastErrorAsWString();

/**
 * Convert windows result code into a string.
 * @param hr Result code.
 * @return Returns string representing the result code.
 */
std::string hrToString(::HRESULT hr);

/**
 * Convert windows result code into a wide string.
 * @param hr Result code.
 * @return Returns string representing the result code.
 */
std::wstring hrToWString(::HRESULT hr);

/**
 * Remove trailing characters from target string.
 * If no such characters are present, no action will 
 * be taken.
 * @param target Target string to change.
 * @param c Character to eliminate from suffix.
 */
void removeTrailingChar(std::string &target, char c);

/**
 * Get the longest common prefix of two strings.
 * @param first First of the input strings.
 * @param second Second of the input strings.
 * @return Returns number of characters they have in 
 * common, starting from the beginning.
 */
std::size_t longestCommonPrefix(const std::string &first, const std::string &second);

/**
 * Base case for hash combination. 
 * Based on: https://stackoverflow.com/a/38140932
 * @param hash Hash accumulator.
 */
inline void hashCombine(std::size_t &hash) noexcept;

/**
 * Accumulate hashable values into a combined 
 * hash value.
 * Based on: https://stackoverflow.com/a/38140932
 * @tparam T Type of the value to hash in this step.
 * @tparam RestTs Type of the rest of the values 
 * to hash in further steps.
 * @param hash Hash accumulator.
 * @param val Current value.
 * @param rest The rest of the values.
 * @usage Specify hash accumulator, which will contain 
 * the result and list all of the required components.
 */
template <typename T, typename... RestTs>
inline void hashCombine(std::size_t &hash, const T &val, RestTs... rest) noexcept;

/**
 * Create agregate hash for all given parameters.
 * @tparam ValTs Types of values to aggregate 
 * into hash.
 * @param vals Values used in creation of the 
 * aggregate hash.
 */
template <typename... ValTs>
inline std::size_t hashAll(ValTs... vals) noexcept;

/**
 * Generate a date and time string, which can be 
 * used to uniquely identify files.
 * @return Returns the string containing date and time.
 */
inline std::string generateDateTimeString(const char *format = "%Y_%m_%d_%H-%M-%S");

template <typename T>
struct Point2
{
    Point2() = default;
    ~Point2() = default;

    Point2(const T &val) :
        x{ val }, y{ val }
    { }
    Point2(const T &xx, const T&yy) :
        x{ xx }, y{ yy }
    { }

    Point2(const Point2 &other) = default;
    Point2(Point2 &&other) = default;
    Point2 &operator=(const Point2 &other) = default;
    Point2 &operator=(Point2 &&other) = default;

    T x{ };
    T y{ };
}; // struct Point2

template <typename T>
std::ostream &operator<<(std::ostream &out, const Point2<T> &p)
{
    out << p.x << " " << p.y;
    return out;
}

/**
 * Color holder, where colour are stored 
 * in contiguous array, ordered RGBA.
 * @tparam FT Which type should be used 
 * for color values.
 */
template <typename FT>
struct ColorRGBA
{
    /// Number of color channels.
    static constexpr uint32_t NUM_CHANNELS{ 4u };
    /// Create black, opaque color.
    constexpr ColorRGBA() = default;

    /// Create specified color
    constexpr ColorRGBA(FT red, FT green, FT blue, FT alpha)
    {
        r = red;
        g = green;
        b = blue;
        a = alpha;
    }

    /// Create specified RGB color, which is fully opaque.
    constexpr ColorRGBA(FT red, FT green, FT blue) :
        ColorRGBA(red, green, blue, 1.0f)
    { }

    /// Contiguous array of colors.
    std::array<FT, NUM_CHANNELS> channels{ 0.0f, 0.0f, 0.0f, 1.0f };

    /// Red channel.
    FT &r{ channels[0] };
    /// Green channel.
    FT &g{ channels[1] };
    /// Blue channel.
    FT &b{ channels[2] };
    /// Alpha channel.
    FT &a{ channels[3] };
}; // struct ColorRGBA

using ColorRGBAF = ColorRGBA<float>;

/**
 * Singleton base class using CRTP.
 */
template <typename T>
class Singleton
{
public:
    Singleton(const Singleton &) = delete;
    Singleton(Singleton &&) = delete;
    Singleton &operator=(const Singleton &) = delete;
    Singleton &operator=(Singleton &&) = delete;

    static T &get()
    {
        static T instance;
        return instance;
    }
private:
protected:
    Singleton() = default;
    ~Singleton() = default;
}; // class Singleton

/**
 * Runtime chooser between two objects.
 * @tparam FirstT Type of the first object.
 * @tparam SecondT Type of the second object.
 * @tparam AsT As what type should these 
 * objects be accessed?
 */
template <typename FirstT, typename SecondT, typename AsT>
struct RuntimeChooser
{
    static_assert(std::is_base_of_v<AsT, FirstT> && std::is_base_of_v<AsT, SecondT>, 
        "Both types must have the target type as a base!");

    /**
     * Initialize an object, based on 
     * the provided flags.
     * @param initFirst When set to 
     * true, the first object will 
     * be initialized. Else the second 
     * object will be initialized.
     */
    explicit RuntimeChooser(bool initFirst)
    {
        if (initFirst)
        {
            mFirst.emplace();
        }
        else
        {
            mSecond.emplace();
        }
    }

    /// Get reference to the chosen object.
    AsT &ref()
    { return ((mFirst.has_value()) ? static_cast<AsT&>(mFirst.value()) : static_cast<AsT&>(mSecond.value())); }

    /// Get reference to the chosen object.
    const AsT &ref() const
    { return ((mFirst.has_value()) ? static_cast<const AsT&>(mFirst.value()) : static_cast<const AsT&>(mSecond.value())); }

    /// Get pointer to the chosen object.
    AsT *ptr()
    { return &ref(); }

    /// Get pointer to the chosen object.
    const AsT *ptr() const
    { return &ref(); }

    /// Access the chosen object.
    AsT &operator*()
    { return ref(); }

    /// Access the chosen object.
    const AsT &operator*() const
    { return ref(); }

    /// Access the chosen object.
    AsT *operator->() 
    { return ptr(); }

    /// Access the chosen object.
    const AsT *operator->() const
    { return ptr(); }
private:
    /// Object of the first type.
    std::optional<FirstT> mFirst;
    /// Object of the second type.
    std::optional<SecondT> mSecond;
}; // struct RuntimeChooser

/**
 * Minimal wrapper for automatic ranged loop.
 * @tparam ItT Type of the iterator.
 */
template <typename ItT>
class Iterable
{
public:
    /**
     * Iterable object over <beginIt, endIt).
     * @param beginIt Beginning iterator.
     * @param endIt Ending iterator.
     */
    Iterable(ItT beginIt, ItT endIt) :
        mBegin{ beginIt }, mEnd{ endIt }
    { }

    /// Get begin iterator.
    ItT begin()
    { return mBegin; }
    /// Get begin iterator.
    ItT begin() const
    { return mBegin; }

    /// Get end iterator.
    ItT end()
    { return mEnd; }
    /// Get end iterator.
    ItT end() const
    { return mEnd; }
private:
    /// Beginning iterator.
    ItT mBegin;
    /// Ending iterator.
    ItT mEnd;
protected:
}; // Iterable

template <typename StrT>
class StringContainer
{
public:
    /// Inner string character type.
    using RawCharT = typename StrT::value_type;

    /// Empty string container.
    StringContainer() = default;

    /// Free all stored string, invalidating pointers returned.
    ~StringContainer() = default;

    /**
     * Store string into this container.
     * @param str String to store.
     * @return Returns pointer to the raw string, which 
     * is memory stable - i.e. it is valid until this object 
     * is destroyed.
     */
    const RawCharT *store(const StrT &str);

    /**
     * Store string into this container.
     * @param str String to store. When nullptr is provided 
     * no storing is performed and nullptr is returned.
     * @return Returns pointer to the raw string, which 
     * is memory stable - i.e. it is valid until this object 
     * is destroyed.
     */
    const RawCharT *store(const RawCharT *str);
private:
    /// List of stored strings.
    std::list<StrT> mStrings;
protected:
}; // class StringContainer

/**
 * Iterator-esque wrapper around a single value.
 * @tparam ItTT Type of the value.
 */
template <typename ItTT>
class IterableWrapper
{
public:
    using value_type = ItTT;
    using reference = ItTT&;
    using const_reference = const ItTT&;
    using pointer = ItTT*;
    using const_pointer = const ItTT*;
    using iterator = IterableWrapper;

    IterableWrapper() = default;
    ~IterableWrapper() = default;
    IterableWrapper(const IterableWrapper &other) = default;
    IterableWrapper &operator=(const IterableWrapper &other) = default;
    IterableWrapper(IterableWrapper &&other) = default;
    IterableWrapper &operator=(IterableWrapper &&other) = default;

    IterableWrapper(const value_type &val) :
        mValue{ val }
    { }

    const_reference operator*() const
    { return mValue; }
    const_pointer operator->() const
    { return &mValue; }

    iterator &operator++()
    { ++mValue; return *this; }
    iterator &operator++(int)
    { const auto temp{ *this }; this->operator++(); return temp; }

    iterator &operator--()
    { --mValue; return *this; }
    iterator &operator--(int)
    { const auto temp{ *this }; this->operator--(); return temp; }

    bool operator==(const iterator &other) const
    { return mValue == other.mValue; }
    bool operator!=(const iterator &other) const
    { return !(this->operator==(other)); }

    operator value_type() const
    { return mValue; }
private:
    /// Current value of the iterator.
    value_type mValue{ };
protected:
}; // class IterableWrapper

/**
 * Class wrapping begin and end values in an iterable 
 * fashion. Allows to create ranges and iterate over 
 * them using range based for.
 * @tparam IterableTT Type of the begin and end values.
 */
template <typename IterableTT>
class Range
{
public:
    using value_type = IterableTT;
    using iterator = IterableWrapper<value_type>;

    /// Empty range with default values.
    Range() = default;

    /**
     * Create a range specifying <begin, end) - including 
     * the begin, excluding end.
     * @param begin Beginning value, inclusive.
     * @param end Ending value, not within the range.
     */
    Range(const value_type &begin, const value_type &end) :
        mBegin{ begin }, mEnd{ end }
    { }

    /**
     * Create a range specifying <begin, end) - including 
     * the begin, excluding end.
     * @param range Pair containing begin and end in this 
     * order..
     */
    Range(std::pair<value_type, value_type> range) :
        mBegin{ range.first }, mEnd{ range.end }
    { }

    Range(const Range &other) = default;
    Range &operator=(const Range &other) = default;
    Range(Range &&other) = default;
    Range &operator=(Range &&other) = default;

    /// Get beginning iterator.
    auto begin() const
    { return iterator(mBegin); }
    /// Get ending iterator.
    auto end() const
    { return iterator(mEnd); }

    /// Get number of items in the range.
    auto size() const
    { return mEnd - mBegin; }
private:
    value_type mBegin{ };
    value_type mEnd{ };
protected:
}; // class Range

/// Implementation details.
namespace impl
{

/// Helper for multiple enable_shared_from_this
class MultipleInheritableEnableSharedFromThis : public std::enable_shared_from_this<MultipleInheritableEnableSharedFromThis>
{
public:
    virtual ~MultipleInheritableEnableSharedFromThis()
    {}
private:
protected:
}; // class MultipleInheritableEnableSharedFromThis

} // namespace impl

/// Helper for multiple enable_shared_from_this
template <class T>
class inheritable_enable_shared_from_this : virtual public impl::MultipleInheritableEnableSharedFromThis
{
public:
    // Found on stack overflow: https://stackoverflow.com/questions/16082785/use-of-enable-shared-from-this-with-multiple-inheritance

    std::shared_ptr<T> shared_from_this()
    { return std::dynamic_pointer_cast<T>(MultipleInheritableEnableSharedFromThis::shared_from_this()); }

    std::shared_ptr<const T> shared_from_this() const 
    { return std::dynamic_pointer_cast<const T>(MultipleInheritableEnableSharedFromThis::shared_from_this()); }

    std::weak_ptr<T> weak_from_this() 
    { return shared_from_this(); }

    std::weak_ptr<const T> weak_from_this() const 
    { return shared_from_this(); }
private:
protected:
}; // class inheritable_enable_shared_from_this

/// Helper class, which should be inherited by types which only construct by pointer.
template <typename T>
class PointerType : public std::enable_shared_from_this<T>
{
public:
    /// Type representing a pointer to inheritor.
    using PtrT = std::shared_ptr<T>;
    /// Type representing a pointer to const inheritor.
    using ConstPtrT = std::shared_ptr<const T>;

    /// Type representing a weak pointer to inheritor.
    using WeakPtrT = std::weak_ptr<T>;
    /// Type representing a weak pointer to const inheritor.
    using ConstWeakPtrT = std::weak_ptr<const T>;

    /// Type representing a unique pointer to inheritor.
    using UniquePtrT = std::unique_ptr<T>;
    /// Type representing a unique pointer to const inheritor.
    using ConstUniquePtrT = std::unique_ptr<const T>;
private:
protected:
}; // class PointerType

/**
 * Helper for creating pointer types, which already inherit 
 * from a pointer type.
 * @tparam BaseT The original pointer type which is inherited.
 * @tparam TargetT Type of the new pointer type.
 */
template <typename BaseT, typename TargetT>
class InheritPointerType
{
public:
    std::shared_ptr<TargetT> shared_from_this()
    {
        BaseT* base{ static_cast<TargetT*>(this) };
        std::shared_ptr<BaseT> basePtr{ base->shared_from_this() };
        return std::static_pointer_cast<TargetT>(basePtr);
    }

    std::shared_ptr<const TargetT> shared_from_this() const
    {
        BaseT* base{ static_cast<TargetT*>(this) };
        std::shared_ptr<BaseT> basePtr{ base->shared_from_this() };
        return std::static_pointer_cast<TargetT>(basePtr);
    }
private:
protected:
}; // class PointerType

/// Shortcut for getting pointer to a type.
template <typename T>
using PtrT = typename PointerType<T>::PtrT;

/// Shortcut for getting const pointer to a type.
template <typename T>
using ConstPtrT = typename PointerType<T>::ConstPtrT;

/// Shortcut for getting weak pointer to a type.
template <typename T>
using WeakPtrT = typename PointerType<T>::WeakPtrT;

/// Shortcut for getting weak const pointer to a type.
template <typename T>
using ConstWeakPtrT = typename PointerType<T>::ConstWeakPtrT;

/// Shortcut for getting unique pointer to a type.
template <typename T>
using UniquePtr = typename PointerType<T>::UniquePtrT;

/// Shortcut for getting unique const pointer to a type.
template <typename T>
using ConsUniquePtrT = typename PointerType<T>::ConstUniquePtrT;

/// Convert wrapper pointer to raw pointer.
template <typename T>
T *rawPtrFromPtrT(std::shared_ptr<T> &ptr);
/// Convert wrapper pointer to raw pointer.
template <typename T>
const T *rawPtrFromPtrT(std::shared_ptr<const T> &ptr);

} // namespace util

// Template implementation.

namespace util
{

inline void hashCombine(std::size_t&) noexcept
{ /* Nothing to be done, we are finished...*/ }

std::string generateDateTimeString(const char *format)
{
    const auto t{ std::time(nullptr) };
    const auto tm{ *std::localtime(&t) };

    std::stringstream ss;
    ss << std::put_time(&tm, format);

    return ss.str();
}

namespace impl
{

template <typename ToStrT, typename FromStrT>
struct AnyStringHelper;

template <>
struct AnyStringHelper<std::string, std::string>
{
    static std::string convert(const std::string &original)
    { return original; }
};

template <>
struct AnyStringHelper<std::wstring, std::wstring>
{
    static std::wstring convert(const std::wstring &original)
    { return original; }
};

template <>
struct AnyStringHelper<std::string, std::wstring>
{
    static std::string convert(const std::wstring &original)
    { return toString(original); }
};

template <>
struct AnyStringHelper<std::wstring, std::string>
{
    static std::wstring convert(const std::string &original)
    { return toWString(original); }
};

}

template <typename ToStrT, typename FromStrT>
ToStrT toAnyString(const FromStrT &original)
{ return impl::AnyStringHelper<ToStrT, FromStrT>::convert(original); }

template <typename T, typename... RestTs>
inline void hashCombine(std::size_t &hash, const T &val, RestTs... rest) noexcept
{
    std::hash<T> hasher;
    // "I don't understand how the standards committee declined something so obvious." � Neil G
    hash ^= hasher(val) + 0x9e3779b9 + (hash << 6) + (hash >> 2);
    hashCombine(hash, rest...);
}

template <typename... ValTs>
std::size_t hashAll(ValTs... vals) noexcept
{
    std::size_t result{ 0u };
    hashCombine(result, vals...);
    return result;
}


template <typename StrT>
const typename StringContainer<StrT>::RawCharT *StringContainer<StrT>::store(const StrT &str)
{
    mStrings.emplace_back(str);
    return mStrings.back().c_str();
}

template <typename StrT>
const typename StringContainer<StrT>::RawCharT *StringContainer<StrT>::store(const RawCharT *str)
{
    if (!str)
    { return nullptr; }

    return store(StrT(str));
}

template <typename T>
T *rawPtrFromPtrT(std::shared_ptr<T> &ptr)
{ return &(*ptr); }

template <typename T>
const T *rawPtrFromPtrT(std::shared_ptr<const T> &ptr)
{ return &(*ptr); }

}

// Template implementation end.

#include "engine/util/StlWrapperEnd.h"