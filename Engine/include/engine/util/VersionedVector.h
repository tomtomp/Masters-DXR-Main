/**
 * @file util/VersionedVector.h
 * @author Tomas Polasek
 * @brief Vector which contains a revision number.
 */

#pragma once

#include "engine/util/Types.h"

/// Utilities and helpers.
namespace util
{

/**
 * Vector which contains additional information of 
 * revision number.
 * @tparam ElementTT Type of the element within the 
 * vector.
 * @tparam VersionTT Type of the versioning information.
 */
template <typename ElementTT, typename VersionTT = uint64_t>
class VersionedVector
{
public:
    /// Type of a single element within the vector.
    using ElementT = ElementTT;
    /// Type of the versioning information.
    using VersionT = VersionTT;
    /// Type of the inner vector.
    using VectorT = std::vector<ElementT>;

    /// Empty vector, default constructed version information.
    VersionedVector();

    /**
     * Initialize the inner vector from given vector.
     * @param data Vector containing the initial data.
     */
    VersionedVector(VectorT &&data);

    // Allow copying.
    VersionedVector(const VersionedVector &other) = default;
    VersionedVector &operator=(const VersionedVector &other) = default;
    // Allow moving.
    VersionedVector(VersionedVector &&other) = default;
    VersionedVector &operator=(VersionedVector &&other) = default;

    /**
     * Take data from given vector and move them into 
     * the inner vector.
     * @param data Data to move to the inner vector.
     * @warning Automatically increments the revision 
     * number of this versioned vector!
     */
    VersionedVector &operator=(VectorT &&data);

    /// Increment the revision number within this versioned vector.
    const VersionT &incrementRevision();

    /// Get the current revision number.
    const VersionT &revision() const;

    /// Swap content of this versioned vector with the other one.
    void swap(VersionedVector &other);

    /// Swap two versioned vectors.
    friend void swap(VersionedVector &first, VersionedVector &second)
    { first.swap(second); }

    /// Access to the begin iterator of the inner vector.
    auto begin()
    { return mData.begin(); }
    auto begin() const
    { return mData.begin(); }
    auto cbegin() const
    { return mData.cbegin(); }

    /// Access to the end iterator of the inner vector.
    auto end()
    { return mData.end(); }
    auto end() const
    { return mData.end(); }
    auto cend() const
    { return mData.cend(); }

    /// Access to the data of the inner vector.
    auto data()
    { return mData.data(); }
    auto data() const
    { return mData.data(); }

    /// Size of the inner vector.
    auto size() const
    { return mData.size(); }

    /// Is the inner vector empty?
    auto empty() const
    { return mData.empty(); }

    /// Clear the inner vector.
    void clear()
    { mData.clear(); }
private:
protected:
    /// Inner vector
    VectorT mData{ };
    /// Information about revision of this vector.
    VersionT mRevision{ };
}; // class VersionedVector

} // namespace util

// Template implementation.

namespace util
{

template <typename ElementTT, typename VersionTT>
VersionedVector<ElementTT, VersionTT>::VersionedVector()
{ /* Automatic */ }

template <typename ElementTT, typename VersionTT>
VersionedVector<ElementTT, VersionTT>::VersionedVector(VectorT &&data) :
    mData{ std::move(data) }
{ }

template <typename ElementTT, typename VersionTT>
VersionedVector<ElementTT, VersionTT> &VersionedVector<ElementTT, VersionTT>::operator=(VectorT &&data)
{
    mData = std::move(data);
    incrementRevision();
    return *this;
}

template <typename ElementTT, typename VersionTT>
const typename VersionedVector<ElementTT, VersionTT>::VersionT &VersionedVector<ElementTT, VersionTT>::
incrementRevision()
{ return ++mRevision; }

template <typename ElementTT, typename VersionTT>
const typename VersionedVector<ElementTT, VersionTT>::VersionT &VersionedVector<ElementTT, VersionTT>::revision() const
{ return mRevision; }

template <typename ElementTT, typename VersionTT>
void VersionedVector<ElementTT, VersionTT>::swap(VersionedVector &other)
{
    using std::swap;

    swap(mData, other.mData);
    swap(mRevision, other.mRevision);
}

}

// Template implementation end.
