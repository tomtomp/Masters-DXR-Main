/**
 * @file util/Math.h
 * @author Tomas Polasek
 * @brief Mathematical helper functions.
 */

#pragma once

/// Utilities and helpers.
namespace util
{

/// Mathematical helper functions.
namespace math
{

/// Float value for one Pi.
static constexpr float QF_PI{ 3.1415926f };
/// Float value for Pi/2.
static constexpr float QF_PI2{ 1.5707963f };
/// Float value for Pi/4.
static constexpr float QF_PI4{ 0.7853981f };

/**
 * Calculate smallest power of 2 number, which
 * is larger than specified value.
 * @param value Non-zero value, <= to the returned number.
 * @return Power of 2 number, >= to the value.
 */
static inline uint32_t pow2RoundUp(uint32_t value)
{
    --value;
    value |= value >> 0b1;
    value |= value >> 0b10;
    value |= value >> 0b100;
    value |= value >> 0b1000;
    value |= value >> 0b10000;

    return value + 1;
}

/**
 * Calculate smallest power of 2 number, which
 * is larger than specified value.
 * @param value Non-zero value, <= to the returned number.
 * @return Power of 2 number, >= to the value.
 */
static inline uint64_t pow2RoundUp(uint64_t value)
{
    --value;
    value |= value >> 0b1;
    value |= value >> 0b10;
    value |= value >> 0b100;
    value |= value >> 0b1000;
    value |= value >> 0b10000;
    value |= value >> 0b100000;

    return value + 1;
}

/**
 * Align provided value to the closes multiple 
 * of alignTo value.
 * @param val Starting value.
 * @param alignTo To what value should the value be 
 * aligned to.
 * @return Returns the aligned value.
 */
/*
static inline std::size_t alignTo(std::size_t val, std::size_t alignTo)
{
    if (!val || !alignTo)
    {
        return val;
    }

    const auto remainder{ val % alignTo };
    return remainder ? val + (alignTo - remainder) : val;
}
*/

/**
 * Align provided value to the closes multiple of alignTo value.
 * @param val Starting value.
 * @param alignTo To what value should the value be aligned to.
 * @return Returns the aligned value.
 */
static constexpr std::size_t alignTo(std::size_t val, std::size_t alignTo)
{
    if (!val || !alignTo)
    {
        return val;
    }

    const auto remainder{ val % alignTo };
    return remainder ? val + (alignTo - remainder) : val;
}

/**
 * Clamp given value to interval <min, max>
 * @tparam T Type of the values.
 * @param val Value to clamp.
 * @param min Minimal value to return.
 * @param max Maximal value to return.
 * @return Returns the clamped value.
 */
template <typename T>
static constexpr T clamp(const T &val, const T &min, const T &max)
{ return std::min(std::max(val, min), max); }

} // namespace math

} // namespace util

