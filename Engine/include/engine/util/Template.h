/**
 * @file util/Template.h
 * @author Tomas Polasek
 * @brief Template, constexpr and SFINAE helpers.
 */

#pragma once

#include "engine/util/Types.h"

/// Utilities and helpers.
namespace util
{

/// Implementation details of isValidExpr
namespace IsValidExprImpl
{

/**
 * SFINAE expression tester. Uses TestingFunT 
 * to test provided types validity in given 
 * function.
 * Inspired by https://jguegant.github.io/blogs/tech/sfinae-introduction.html
 * @tparam TestingFunT Type of the testing 
 * function.
 * @usage decltype(Tester<TestFunT>()(std::declval<TestedType>()))::value
 */
template <typename TestingFunT> 
class Tester
{
public:
    /**
     * Test types provided as arguments against the 
     * testing function.
     * @tparam TestedTs Types of the tested objects.
     * @return Return type depends, if the objects are 
     * valid within the testing function. Return type is 
     * true_type if valid, or false_type if invalid.
     */
    template <typename... TestedTs> 
    constexpr auto operator()(const TestedTs& ...)
    { return testTs<TestedTs...>(int()); }
private:
    /**
     * Try putting provided types into testing function and test, if 
     * the expression compiles.
     * This case is for valid expressions.
     * @tparam TestedTs Types being tested.
     * @return Expression is valid, returns true_type.
     */
    template <typename... TestedTs> 
    constexpr auto testTs(int)
        //          Use testing function        on provided types             returns true_type
        -> decltype(std::declval<TestingFunT>()(std::declval<TestedTs>()...), std::true_type())
    { return std::true_type(); }

    /**
     * Try putting provided types into testing function and test, if 
     * the expression compiles.
     * This case is for invalid expressions.
     * @tparam TestedTs Types being tested.
     * @return Expression is invalid, returns false_type.
     */
    template <typename... TestedTs> 
    constexpr std::false_type testTs(...)
    { return std::false_type(); }
protected:
}; // class Tester

} // namespace IsValidExprImpl

/**
 * Check, whether given expression is valid.
 * @param t Expression to check.
 * @usage auto hasFun = util::isValidExpr([](auto &&x) -> decltype(x.fun()) {});
 * constexpr bool AHasFun{ decltype(hasFun(std::declval<A>()))::value };
 */
template <typename T>
constexpr auto isValidExpr(const T &t)
{ return IsValidExprImpl::Tester<T>(); }

/// Is given type dereferencable?
template <typename T>
struct isDereferencable
{
private:
    template <typename R> 
    static constexpr auto tester(int) -> decltype(*std::declval<R>(), std::declval<std::true_type>());
    template <typename R> 
    static constexpr auto tester(...) -> std::true_type;
public:
    static constexpr bool value{ decltype(tester<T>(int(0)))::value };
}; // struct isDereferencable

/// Is given type dereferencable?
template <typename T>
static constexpr bool isDereferencableV{ isDereferencable<T>::value };

/// Is given type comparable to nullptr?
template <typename T>
struct isComparableToNullptr
{
private:
    template <typename R> 
    static constexpr auto tester(int) -> decltype(std::declval<R>() == nullptr, std::declval<std::true_type>());
    template <typename R> 
    static constexpr auto tester(...) -> std::false_type;
public:
    static constexpr bool value{ decltype(tester<T>(int(0)))::value };
}; // struct isComparableToNullptr

/// Is given type comparable to nullptr?
template <typename T>
static constexpr bool isComparableToNullptrV{ isComparableToNullptr<T>::value };

} // namespace util