/**
* @file util/StlWrapperEnd.h
* @author Tomas Polasek
* @brief End of section wrapping STL/standard library includes.
*/

#ifdef _MSC_VER

#undef _SILENCE_CXX17_CODECVT_HEADER_DEPRECATION_WARNING

#	pragma warning(pop)

#endif
