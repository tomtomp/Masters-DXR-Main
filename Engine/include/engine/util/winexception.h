/**
 * @file winexception.h
 * @author Tomas Polasek
 * @brief Exception incorporating windows error message.
 */

#pragma once

#include "engine/util/Util.h"
#include "engine/util/wexception.h"

/// Utilities and helpers.
namespace util
{

/// Wrapper around std::runtime_error, which adds windows error message to the end.
class winexception : public std::runtime_error
{
public:
	explicit winexception(const std::string &message, ::HRESULT hr = 0u) : 
		runtime_error(generateMessage(message, hr))
    { }

	explicit winexception(const char *message, ::HRESULT hr = 0u) : 
		runtime_error(generateMessage(message, hr))
    { }
private:
    /// Generate a message from user message string and current error state.
    std::string generateMessage(const std::string &message, ::HRESULT hr)
    {
        const auto result{ message + "\n\t (" + util::hrToString(hr) + "): " + util::getLastErrorAsString() };
        return result;
    }
protected:
}; // class win_runtime_error

/// Wrapper around util::wruntime_error which adds windows error message to the end.
class wwinexception : public util::wruntime_error
{
public:
	explicit wwinexception(const std::wstring &message, ::HRESULT hr = 0u) : 
		wruntime_error(generateMessage(message, hr))
    { }

	explicit wwinexception(const wchar_t *message, ::HRESULT hr = 0u) : 
		wruntime_error(generateMessage(message, hr))
    { }

	explicit wwinexception(const std::string &message, ::HRESULT hr = 0u) : 
        wruntime_error(generateMessage(message, hr))
    { }

	explicit wwinexception(const char *message, ::HRESULT hr = 0u) : 
        wruntime_error(generateMessage(message, hr))
    { }
private:
    /// Generate a message from user message string and current error state.
    std::string generateMessage(const std::string &message, ::HRESULT hr)
    {
        const auto result{ message + "\n\t (" + util::hrToString(hr) + "): " + util::getLastErrorAsString() };
        return result;
    }

    /// Generate a message from user message string and current error state.
    std::wstring generateMessage(const std::wstring &message, ::HRESULT hr)
    {
        const auto result{ message + L"\n\t (" + util::hrToWString(hr) + L"): " + util::getLastErrorAsWString() };
        return result;
    }
protected:
}; // class win_runtime_error

/**
 * Throw exception if the given result fails.
 * Source: https://github.com/Microsoft/DirectX-Graphics-Samples
 * @param hr Result to check.
 * @param msg Message if the exception is thrown.
 * @throws Throws std::exception if result fails.
 */
inline void throwIfFailed(HRESULT hr, const char *msg)
{
    if (FAILED(hr))
    { throw winexception(msg, hr); }
}

/**
 * Throw exception if the given result fails.
 * Source: https://github.com/Microsoft/DirectX-Graphics-Samples
 * @param hr Result to check.
 * @param msg Message if the exception is thrown.
 * @throws Throws std::exception if result fails.
 */
inline void throwIfFailed(HRESULT hr, const wchar_t *msg)
{
    if (FAILED(hr))
    { throw util::wwinexception(msg, hr); }
}

/**
 * Get size of given type in uint32s, rounded up so 
 * that whole object can fit into returned number of 
 * uint32s. 
 * @tparam T Type to calculate the size for.
 * @return Returns size of the provided type in uint32s.
 */
template <typename T>
static constexpr std::size_t sizeInUint32()
{ return (sizeof(T) + sizeof(uint32_t) - 1u) / sizeof(uint32_t); }

} // namespace util
