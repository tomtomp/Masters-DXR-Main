/**
 * @file util/SafeD3D12.h
 * @author Tomas Polasek
 * @brief Safe way to include D3D12 headers.
 */

#pragma once

#include "engine/util/StlWrapperBegin.h"

#include <d3d12.h>
#include <d3dcompiler.h>
#include <d3dcommon.h>
#include <d3d12shader.h>

#include <dxgidebug.h>
#ifdef ENGINE_RAY_TRACING_ENABLED
#   include <dxgi1_6.h>
#else
// DXGI 1.5 is required for CheckFeatureSupport.
#   include <dxgi1_5.h>
#endif

#include <DirectXMath.h>

#include "engine/util/StlWrapperEnd.h"
