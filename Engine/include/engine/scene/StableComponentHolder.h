/**
 * @file scene/StableComponentHolder.h
 * @author Tomas Polasek
 * @brief Components used in the scene Entity-Component-System.
 */

#pragma once

#include "engine/util/Asserts.h"
#include "engine/util/Types.h"
#include "engine/lib/Entropy.h"

/// Namespace containing the engine code.
namespace quark
{

/// Virtual scene types.
namespace scene
{

/**
 * Component holder which never invalidates references 
 * to already existing components.
 * Should be used only for components which are present 
 * for almost all of the entities!
 * @tparam ComponentT Type of the Component.
 */
template <typename ComponentT>
class StableComponentHolder final : public ent::BaseComponentHolder<ComponentT>
{
public:
    /// Initialize empty holder.
    StableComponentHolder();

    /// Free all components, calling their destructor.
    virtual ~StableComponentHolder();

    /**
     * Add Component for given EntityId, if the Component
     * already exists, nothing happens.
     * @param id Id of the Entity.
     * @return Returns pointer to the Component.
     */
    virtual inline ComponentT *add(ent::EntityId id) noexcept override;

    /**
     * Add/replace Component of given Entity with a copy of given Component.
     * @param id ID of the Entity.
     * @param comp Component to copy.
     * @return Return ptr to the Component.
     */
    virtual inline ComponentT *replace(ent::EntityId id, const ComponentT &comp) noexcept override;

    /**
     * Add Component for given EntityId, if the Component
     * already exists, It will be overwritten with element
     * constructed with given constructor parameters.
     * Pass constructor parameters to the Component on
     * construction.
     * @tparam CArgTs Component constructor argument types.
     * @param id ID of the Entity.
     * @param cArgs Component constructor arguments.
     * @return Returns pointer to the Component.
     */
    template <typename... CArgTs>
    inline ComponentT *add(ent::EntityId id, CArgTs... cArgs) noexcept;

    /**
     * Get Component belonging to given EntityId.
     * @param id Id of the Entity.
     * @return Returns pointer to the Component, or nullptr, if it does not exist.
     */
    virtual inline ComponentT *get(ent::EntityId id) noexcept override;
    /**
     * Get Component belonging to given EntityId.
     * @param id Id of the Entity.
     * @return Returns pointer to the Component, or nullptr, if it does not exist.
     */
    virtual inline const ComponentT *get(ent::EntityId id) const noexcept override;

    /**
     * Remove Component for given Entity. If the Entity does not have
     * Component associated with it, nothing happens.
     * @param id Id of the Entity.
     * @return Returns true, if there are no more Components of this type
     *   for given Entity.
     */
    virtual inline bool remove(ent::EntityId id) noexcept override;

    /**
     * Refresh the Component holder.
     * Called during the Universe refresh.
     */
    virtual inline void refresh() noexcept override;
private:
    /**
     * Ensure, that the component list has at least 
     * size elements.
     * @param size After returning, the components 
     * list will contain at least size elements.
     * @throws Any exceptions thrown by component 
     * constructor.
     */
    void ensureAtLeast(std::size_t size);

    /// List containing the components.
    std::deque<ComponentT> mComponents{ };
protected:
}; // ComponentHolderMapList

} // namespace scene

} // namespace quark

// Template implementation.

namespace quark
{

namespace scene
{

template <typename ComponentT>
StableComponentHolder<ComponentT>::StableComponentHolder()
{ /* Automatic */ }

template <typename ComponentT>
StableComponentHolder<ComponentT>::~StableComponentHolder()
{ /* Automatic */ }

template <typename ComponentT>
ComponentT *StableComponentHolder<ComponentT>::add(ent::EntityId id) noexcept
{
    try {
        ensureAtLeast(id.index());
    } catch(...) {
        return nullptr;
    }

    return &mComponents[id.index()];
}

template <typename ComponentT>
ComponentT *StableComponentHolder<ComponentT>::replace(ent::EntityId id, const ComponentT &comp) noexcept
{
    try {
        ensureAtLeast(id.index());
    } catch(...) {
        return nullptr;
    }

    ComponentT *result{ &mComponents[id.index()] };
    *result = comp;

    return result;
}

template <typename ComponentT>
template <typename... CArgTs>
ComponentT *StableComponentHolder<ComponentT>::add(ent::EntityId id, CArgTs... cArgs) noexcept
{
    try {
        ensureAtLeast(id.index());
    } catch(...) {
        return nullptr;
    }

    ComponentT *result{ &mComponents[id.index()] };
    *result = CT(std::forward<CArgTs>(cArgs)...);

    return result;
}

template <typename ComponentT>
ComponentT *StableComponentHolder<ComponentT>::get(ent::EntityId id) noexcept
{ return id.index() < mComponents.size() ? &mComponents[id.index()] : nullptr; }

template <typename ComponentT>
const ComponentT *StableComponentHolder<ComponentT>::get(ent::EntityId id) const noexcept
{ return const_cast<StableComponentHolder*>(this)->get(id); }

template <typename ComponentT>
bool StableComponentHolder<ComponentT>::remove(ent::EntityId id) noexcept
{
    UNUSED(id);
    return true;
}

template <typename ComponentT>
void StableComponentHolder<ComponentT>::refresh() noexcept
{ /* Nothing to do... */ }

template <typename ComponentT>
void StableComponentHolder<ComponentT>::ensureAtLeast(std::size_t size)
{
    if (size >= mComponents.size())
    { mComponents.resize(size + 1); }
}

}

}

// Template implementation end.
