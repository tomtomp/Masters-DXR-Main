/**
 * @file resources/scene/SceneDescription.h
 * @author Tomas Polasek
 * @brief Container for a description of a scene, its
 * resources and sub-scenes.
 */

#pragma once

#include "engine/util/Types.h"

#include "engine/helpers/math/Transform.h"

#include "engine/renderer/ElementFormat.h"
#include "engine/renderer/SamplerFiltering.h"

/// Namespace containing the engine code.
namespace quark
{

/// Virtual scene types.
namespace scene
{

/// Structures used for scene description.
namespace desc
{

/// Base type used for descriptor handles.
using DescHandleBase = uint32_t;
/// Invalid/unset handle value.
static constexpr DescHandleBase NULL_HANDLE_VALUE{ 0u };

/**
 * Helper structure used for holding a list of resources 
 * and mapping from their names.
 */ 
template <typename DescT, typename HashT>
struct ResourceHolder
{
    /// List of resource descriptions.
    std::vector<DescT> resources{ };
    /// Mapping from resource names to their handles.
    std::map<HashT, DescHandleBase> nameMap{ };
}; // struct ResourceHolder

struct Buffer
{
    /// Name used for debug identification.
    std::string name{ };

    /// Byte data of the buffer.
    std::vector<uint8_t> data{ };
}; // struct Buffer
using BufferHandle = DescHandleBase;

struct BufferView
{
    /// Name used for debug identification.
    std::string name{ };

    /// Index within the buffer array.
    BufferHandle buffer{ NULL_HANDLE_VALUE };

    /// Format of a single element within the buffer.
    rndr::ElementFormat elementFormat{ };

    /// Number of elements in the buffer.
    std::size_t elementCount{ 0u };

    // Optional memory layout information.
    std::size_t byteOffset{ 0u };
    std::size_t byteLength{ 0u };
    std::size_t byteStride{ 0u };

    /**
     * Minimal value within this view. The byte array must be 
     * interpreted according to the element format!
     */ 
    uint8_t minValue[rndr::ElementFormat::MAX_BYTES_PER_ELEMENT]{ 0u, };
    /**
     * Maximal value within this view. The byte array must be 
     * interpreted according to the element format!
     */ 
    uint8_t maxValue[rndr::ElementFormat::MAX_BYTES_PER_ELEMENT]{ 0u, };
}; // struct BufferView
using BufferViewHandle = DescHandleBase;

struct Sampler
{
    /// Name used for debug identification.
    std::string name{ };

    /// Filtering rules used by the sampler.
    rndr::MinMagMipFilter filtering{ };
    /// Filtering rules used by the sampler when out of bounds.
    rndr::EdgeFilterUVW edgeFiltering{ };
}; // struct Sampler
using SamplerHandle = DescHandleBase;

struct Texture
{
    /// Name used for debug identification.
    std::string name{ };

    /// Byte data of the texture.
    std::vector<uint8_t> data{ };

    /// Type of a single channel in the texture
    rndr::ElementFormat elementFormat{ };

    /// Optional index of the sampler used for this texture.
    SamplerHandle sampler{ NULL_HANDLE_VALUE };

    // TODO - Support other dimensions than 2D?
    /// Width of the texture in texels.
    std::size_t width{ 0u };
    /// Height of the texture in texels.
    std::size_t height{ 0u };

    /// Number of mip-maps in this texture, 0u for just the base texture.
    std::size_t mipMapCount{ 0u };

    /// Should mip-maps be automatically generated from the source image?
    bool generateMipMaps{ false };
}; // struct Texture
using TextureHandle = DescHandleBase;

/// Type of PBR models which can be specified.
enum class PbrModel
{
    /// Physical properties are not used.
    None, 
    /// Metallic-roughness model is used.
    MetallicRoughness,
    /// Specular-glossiness model is used.
    SpecularGlossiness
}; // enum class PbrModel

/// Modes of operation with the material color alpha values.
enum class AlphaMode
{
    /// Ignore alpha channel.
    Opaque,
    /// Alpha channel specifies binary visible/invisible.
    Mask,
    /// Standard alpha blending.
    Blend
}; // enum class AlphaMode

struct Material
{
    /// Name used for debug identification.
    std::string name{ };

    /**
     * Texture, where each texel (RGB-channels) contains XYZ 
     * components of the normal vector.
     */
    TextureHandle normalTexture{ NULL_HANDLE_VALUE };

    /**
     * Texture, which represents how much is the point occluded. 
     * The data is contained within the R-channel and the higher 
     * the value is, the LESS occluded the point is.
     */
    TextureHandle occlusionTexture{ NULL_HANDLE_VALUE };

    /**
     * Color emission of the material.
     * If emissiveTexture is set, every texel should be multiplied 
     * by this factor, before any further operations.
     */
    float emissiveFactor[3]{ 0.0f, 0.0f, 0.0f };

    /**
     * Texture containing data for emissive surfaces. Color of 
     * emission is contained within RGB-channels.
     */
    TextureHandle emissiveTexture{ NULL_HANDLE_VALUE };

    /// How to draw materials alpha values.
    AlphaMode alphaMode{ AlphaMode::Opaque };

    /**
     * When alphaMode is in Mask mode, this value dictates 
     * the boundary between visible/invisible.
     */
    float alphaCutoff{ 0.5f };

    /// Does the material generate specular reflections?
    bool specularReflections{ false };

    /// Does the material generate "specular" refraction?
    bool specularRefractions{ false };

    /**
     * Setup material which is just a flat diffuse texture.
     * @param diffuseTexture Color texture used.
     */
    void setupNoPbr(TextureHandle diffuseTexture)
    {
        pbr.reset();

        pbr.modelUsed = PbrModel::None;
        pbr.baseColorTexture = diffuseTexture;
    }

    /**
     * Setup metallic-roughness material.
     * @param baseColorTexture Base color of the material.
     * @param propertyTexture Metallic-roughness texture.
     */
    void setupMetallicRoughness(TextureHandle baseColorTexture, TextureHandle propertyTexture)
    {
        pbr.reset();

        pbr.modelUsed = PbrModel::MetallicRoughness;
        pbr.baseColorTexture = baseColorTexture;
        pbr.propertyTexture = propertyTexture;
    }

    /// Section for properties which are used in PBR.
    struct PhysicallyBasedProperties
    {
        /// Is this structure describing Metallic-roughness material?
        bool isMetallicRoughness() const
        { return modelUsed == PbrModel::MetallicRoughness; }

        /// Is this structure describing Specular-Glossiness material?
        bool isSpecularGlossiness() const
        { return modelUsed == PbrModel::SpecularGlossiness; }

        /// Reset values to default state.
        void reset()
        { *this = { }; }

        /// Specification of which PBR model is used.
        PbrModel modelUsed{ PbrModel::None };

        /**
         * Multiplicative factor of the baseColorTexture.
         * When no texture is specified, this is instead the 
         * RGBA color of the material.
         * Used both in Metallic-Roughness and 
         * Specular-Glossiness (diffuseFactor).
         * This property is valid even when PBR model is None.
         */
        float baseColorFactor[4]{ 1.0f, 1.0f, 1.0f, 1.0f };
        /**
         * Texture containing the color of the material.
         * Each value should be multiplied by the baseColorFactor 
         * before further use.
         * While using Specular-Glossiness, this texture contains 
         * diffuse map.
         * This property is valid even when PBR model is None.
         */
        TextureHandle baseColorTexture{ NULL_HANDLE_VALUE };

        union
        {
            /**
             * Flat metalness of the material.
             * Only used for Metallic-Roughness model.
             */
            float metallicFactor;
            /**
             * Specular RGB colors of the material
             * Only used for Specular-Glossiness model.
             */
            float specularFactor[3]{ 1.0f, 1.0f, 1.0f };
        } firstFactor;

        union
        {
            /**
             * Flat roughness of the material
             * Only used for Metallic-Roughness model.
             */
            float roughnessFactor;
            /**
             * Flag glossiness/smoothness of the material.
             * Only used for Specular-Glossiness model.
             */
            float glossinessFactor{ 1.0f };
        } secondFactor;

        /**
         * Map containing values of physical properties of 
         * the material. When set, the first and second 
         * factors are unused!
         * For Metallic-Roughness, this texture contains 
         * metalness (B-channel) and roughness (G-channel) 
         * values for each texel.
         * For Specular-Glossiness, this texture contains 
         * specular RGB color (RGB-channels) and glossiness 
         * (A-channel) values for each texel.
         */
        TextureHandle propertyTexture{ NULL_HANDLE_VALUE };
    }; // struct PhysicallyBasedProperties

    /// Physically based properties of the material.
    PhysicallyBasedProperties pbr{ };
}; // struct Material
using MaterialHandle = DescHandleBase;

/// Primitive types, used when rendering the mesh.
enum class PrimitiveMode
{
    Points,
    Lines,
    LineLoop,
    LineString,
    Triangles,
    TriangleStrip,
    TriangleFan
}; // enum class PrimitiveMode

struct Primitive
{
    /// Name used for debug identification.
    std::string name{ };

    /// Indices used for indexing vertex data.
    BufferViewHandle indiceBuffer{ NULL_HANDLE_VALUE };

    // Buffer views containing the vertex properties.
    BufferViewHandle positionBuffer{ NULL_HANDLE_VALUE };
    BufferViewHandle normalBuffer{ NULL_HANDLE_VALUE };
    BufferViewHandle tangentBuffer{ NULL_HANDLE_VALUE };
    BufferViewHandle texCoordBuffer{ NULL_HANDLE_VALUE };
    BufferViewHandle colorBuffer{ NULL_HANDLE_VALUE };

    /// Material used by this primitive.
    MaterialHandle material{ NULL_HANDLE_VALUE };

    /// Mode of rendering the vertex data.
    PrimitiveMode primitiveMode{ PrimitiveMode::Triangles };
}; // struct Primitive
using PrimitiveHandle = DescHandleBase;

struct Mesh
{
    /// Name used for debug identification.
    std::string name{ };

    /// List of primitives making up this mesh.
    std::vector<PrimitiveHandle> primitives{ };
}; // struct Mesh
using MeshHandle = DescHandleBase;

/// Available camera types.
enum class CameraType
{
    Orthographic,
    Perspective,
    Unknown
}; // enum class CameraType

using CameraHandle = DescHandleBase;
struct Camera
{
    /// Name used for debug identification.
    std::string name{ };

    /// Type of the camera.
    CameraType type{ CameraType::Unknown };

    union
    {
        /// Width used for orthographic projection, can be zero for automatic.
        float width{ 0.0f };
        /// Aspect ratio used for perspective projection, can be zero for automatic.
        float aspectRatio;
    } firstParam;

    union
    {
        /// Height used for orthographic projection, can be zero for automatic.
        float height{ 0.0f };
        /// Horizontal FOV used for perspective projection, can be zero for automatic.
        float horFov;
    } secondParam;

    /// Near plane location.
    float zNear{ 0.0f };

    /**
     * Far plane location, can be 0.0f for perspective 
     * projection for infinite perspective projection.
     */
    float zFar{ 0.0f };
}; // struct Camera

using EntityHandle = DescHandleBase;
struct Entity
{
    /// Name used for debug identification.
    std::string name{ };

    /// Index of the mesh representing the entity.
    MeshHandle mesh{ NULL_HANDLE_VALUE };

    /// Transform of this entity.
    helpers::math::Transform transform{ };

    /// Index of the parent, default for no parents.
    EntityHandle parent{ NULL_HANDLE_VALUE };

    /// Handle to the camera which this entity contains.
    CameraHandle camera{ NULL_HANDLE_VALUE };
}; // struct Entity

struct SubScene
{
    /// Name used for debug identification.
    std::string name{ };

    /// Holder of entities within the scene.
    ResourceHolder<desc::Entity, uint64_t> entities{ };
}; // struct SubScene
using SubSceneHandle = DescHandleBase;

} // namespace desc

/**
 * Container for a description of a scene, its
 * resources and sub-scenes.
 */
class SceneDescription : public util::PointerType<SceneDescription>
{
public:
    /// Exception thrown when user provided description is invalid.
    struct InvalidDescException : public std::exception 
    {
        InvalidDescException(const char *msg) : 
            std::exception(msg)
        { }
    }; // struct InvalidDescException

    /// Create a new empty scene description.
    static PtrT create();

    /// Free any resources used.
    ~SceneDescription();

    /// Set name for this scene.
    void setName(const std::string &name);

    /// Get name of this scene
    const std::string &name() const;

    /// Add the buffer and return a handle to it.
    desc::BufferHandle createBuffer(desc::Buffer &&bufferDesc);
    /// Add the buffer and return a handle to it. Does not consider empty buffer as an error.
    desc::BufferHandle createEmptyBuffer(desc::Buffer &&bufferDesc);
    /// Get already created buffer handle by name. 
    desc::BufferHandle getBufferByName(const std::string &name) const;
    /// Add the buffer view and return a handle to it.
    desc::BufferViewHandle createBufferView(desc::BufferView &&bufferViewDesc);
    /// Get already created buffer view handle by name. 
    desc::BufferViewHandle getBufferViewByName(const std::string &name) const;
    /// Add the sampler and return a handle to it.
    desc::SamplerHandle createSampler(desc::Sampler &&samplerDesc);
    /// Get already created sampler handle by name. 
    desc::SamplerHandle getSamplerByName(const std::string &name) const;
    /// Add the texture and return a handle to it.
    desc::TextureHandle createTexture(desc::Texture &&textureDesc);
    /// Get already created texture handle by name. 
    desc::TextureHandle getTextureByName(const std::string &name) const;
    /// Add the material and return a handle to it.
    desc::MaterialHandle createMaterial(desc::Material &&materialDesc);
    /// Get already created material handle by name. 
    desc::MaterialHandle getMaterialByName(const std::string &name) const;
    /// Add the primitive and return a handle to it.
    desc::PrimitiveHandle createPrimitive(desc::Primitive &&primitiveDesc);
    /// Get already created primitive handle by name. 
    desc::PrimitiveHandle getPrimitiveByName(const std::string &name) const;
    /// Add the mesh and return a handle to it.
    desc::MeshHandle createMesh(desc::Mesh &&meshDesc);
    /// Get already created mesh handle by name. 
    desc::MeshHandle getMeshByName(const std::string &name) const;
    /// Add the camera and return a handle to it.
    desc::CameraHandle createCamera(desc::Camera &&cameraDesc);
    /**
     * Add the camera and return a handle to it. If a camera 
     * with the same name already exists, replace it.
     */
    desc::CameraHandle amendCamera(desc::Camera &&cameraDesc);
    /// Get already created camera handle by name. 
    desc::CameraHandle getCameraByName(const std::string &name) const;

    /**
     * Add the entity to the current sub-scene and return a handle to it.
     * @warning Scene must be active at the time of calling!
     */ 
    desc::EntityHandle createEntity(desc::Entity &&entityDesc);
    /**
     * Add the entity to the current sub-scene and return a handle to it.
     * If entity with the same name already exists, replace it.
     * @warning Scene must be active at the time of calling!
     */ 
    desc::EntityHandle amendEntity(desc::Entity &&entityDesc);
    /// Get already created entity handle by name.
    desc::EntityHandle getEntityByName(const std::string &name) const;
    /// Clear all of the entities within the current sub-scene.
    void clearEntities();

    /// Create a new sub-scene, without activating it.
    desc::SubSceneHandle createSubScene(const std::string &name);
    /// Get already created sub-scene handle by name. 
    desc::SubSceneHandle getSubSceneByName(const std::string &name) const;

    /// Make the sub-scene current.
    void activateSubScene(desc::SubSceneHandle subScene);

    /// Get index of the currently active sub-scene.
    std::size_t activeSubScene() const;

    /// Set properties of the default sampler.
    void setDefaultSampler(desc::Sampler &&samplerDesc);

    /// Check whether given handle is a Null handle.
    static bool handleNotNull(desc::DescHandleBase handle);

    /**
     * Convert given handle to an index.
     * @return Returns an index representing the handle.
     * @warning Provided handle must not be a Null handle.
     */
    static std::size_t handleToIndex(desc::DescHandleBase handle);

    // Holder accessors: 
    const auto &buffers() const
    { return mBuffers.resources; }
    const auto &bufferViews() const
    { return mBufferViews.resources; }
    const auto &samplers() const
    { return mSamplers.resources; }
    const auto &textures() const
    { return mTextures.resources; }
    const auto &materials() const
    { return mMaterials.resources; }
    const auto &primitives() const
    { return mPrimitives.resources; }
    const auto &meshes() const
    { return mMeshes.resources; }
    const auto &cameras() const 
    { return mCameras.resources; }
    const auto &subScenes() const
    { return mSubScenes.resources; }

    /**
     * Get the default sampler which should be used 
     * when no other is specified.
     */ 
    const auto &defaultSampler() const 
    { return mDefaultSampler; }
private:
    /// Hash type used for tables mapping names to handles.
    using HashType = uint64_t;

    /**
     * Should the methods throw InvalidDescException (true), or just 
     * log the message and return a Null handle (false)?
     */ 
    static constexpr bool THROW_BAD_DESC{ true };

    /**
     * Convert given index to a handle.
     * @return Returns a handle representing the index.
     */
    static desc::DescHandleBase indexToHandle(std::size_t index);

    /**
     * Check that provided handle is valid for the holder.
     * Handle is valid if it is Null handle or if it points 
     * to a valid element of the holder.
     * @param handle Handle to check.
     * @param holder Holder referenced by the handle.
     * @return Returns whether the handle is valid.
     * @warning Even when returning true, the handle may still 
     * be Null handle!
     */
    template <typename DescT>
    static bool handleValid(desc::DescHandleBase handle, const desc::ResourceHolder<DescT, HashType> &holder);

    /**
     * Check that provided handle is valid for the holder.
     * @param handle Handle to check.
     * @param holder Holder referenced by the handle.
     * @return Returns whether the handle is valid.
     */
    template <typename DescT>
    static bool handleValidNotNull(desc::DescHandleBase handle, const desc::ResourceHolder<DescT, HashType> &holder);

    /**
     * Push given description and return a handle to it.
     * @param desc Description which should be pushed.
     * @param holder Holder used to store descriptions of 
     * this type.
     * @return Returns handle to the newly created description.
     */ 
    template <typename DescT>
    static desc::DescHandleBase pushDesc(DescT &&desc, desc::ResourceHolder<DescT, HashType> &holder);

    /**
     * Push given description and return a handle to it.
     * If the name is already registered, then the original is replaced 
     * with this new one.
     * @param desc Description which should be pushed.
     * @param holder Holder used to store descriptions of 
     * this type.
     * @return Returns handle to the newly created description.
     */ 
    template <typename DescT>
    static desc::DescHandleBase pushReplaceDesc(DescT &&desc, desc::ResourceHolder<DescT, HashType> &holder);

    /**
     * Attempt to get a handle for named description contained 
     * within provided holder.
     * @param name Name being searched.
     * @param holder Holder containing the descriptions.
     * @return Returns handle corresponding to the name. If no 
     * such description is registered, returns NULL_HANDLE_VALUE 
     * instead.
     */
    template <typename DescT>
    static desc::DescHandleBase getHandleFromName(const std::string &name, 
        const desc::ResourceHolder<DescT, HashType> &holder);

    /**
     * Get description from provided holder.
     * @param handle Valid handle to the descriptor.
     * @param holder Holder referenced by the descriptor.
     * @return Returns reference to the descriptor.
     */
    template <typename DescT>
    static DescT &getDesc(desc::DescHandleBase handle, 
        desc::ResourceHolder<DescT, HashType> &holder);

    /**
     * Get description from provided holder.
     * @param handle Valid handle to the descriptor.
     * @param holder Holder referenced by the descriptor.
     * @return Returns reference to the descriptor.
     */
    template <typename DescT>
    static const DescT &getDesc(desc::DescHandleBase handle, 
        const desc::ResourceHolder<DescT, HashType> &holder);

    /**
     * Clear all descriptions from the provided holder.
     * @param holder Holder used to clear descriptions from.
     */ 
    template <typename DescT>
    static void clearDescs(desc::ResourceHolder<DescT, HashType> &holder);

    /**
     * Depending on configuration this method either throws 
     * InvalidDescException or just prints error message and 
     * returns Null handle.
     */
    static desc::DescHandleBase throwLogBadDesc(const char *msg);

    /// Create and initialize empty scene.
    SceneDescription();

    /// Name of this scene.
    std::string mName{ };

    // Resource lists: 
    desc::ResourceHolder<desc::Buffer, HashType> mBuffers{ };
    desc::ResourceHolder<desc::BufferView, HashType> mBufferViews{ };
    desc::ResourceHolder<desc::Sampler, HashType> mSamplers{ };
    desc::ResourceHolder<desc::Texture, HashType> mTextures{ };
    desc::ResourceHolder<desc::Material, HashType> mMaterials{ };
    desc::ResourceHolder<desc::Primitive, HashType> mPrimitives{ };
    desc::ResourceHolder<desc::Mesh, HashType> mMeshes{ };
    desc::ResourceHolder<desc::Camera, HashType> mCameras{ };

    /// List of sub-scenes currently in the scene.
    desc::ResourceHolder<desc::SubScene, HashType> mSubScenes{ };
    /// Currently selected sub-scene.
    desc::SubSceneHandle mCurrentSubScene{ desc::NULL_HANDLE_VALUE };

    /// Default texture sampler used when no other is specified.
    desc::Sampler mDefaultSampler{ };
protected:
}; // class SceneDescription

} // namespace scene

} // namespace quark

// Template implementation begin.

namespace quark
{

namespace scene
{

template<typename DescT>
bool SceneDescription::handleValid(desc::DescHandleBase handle, const desc::ResourceHolder<DescT, HashType> &holder)
{ return handle == desc::NULL_HANDLE_VALUE || handleToIndex(handle) < holder.resources.size(); }

template<typename DescT>
bool SceneDescription::handleValidNotNull(desc::DescHandleBase handle, const desc::ResourceHolder<DescT, HashType> &holder)
{ return  handle != desc::NULL_HANDLE_VALUE && handleToIndex(handle) < holder.resources.size(); }

template<typename DescT>
desc::DescHandleBase SceneDescription::pushDesc(DescT &&desc, desc::ResourceHolder<DescT, HashType> &holder)
{
    // New description is always added to the end.
    const auto handle{ indexToHandle(holder.resources.size()) };

    // Register its name if specified.
    if (!desc.name.empty())
    { 
        const auto nameHash{ std::hash<std::string>()(desc.name) };
        ASSERT_SLOW(holder.nameMap.find(nameHash) == holder.nameMap.end());
        holder.nameMap.emplace(nameHash, handle); 
    }

    // Add the description.
    holder.resources.emplace_back(std::move(desc));

    return handle;
}

template<typename DescT>
desc::DescHandleBase SceneDescription::pushReplaceDesc(DescT &&desc, desc::ResourceHolder<DescT, HashType> &holder)
{
    const auto nameHash{ std::hash<std::string>()(desc.name) };
    const auto findIt{ holder.nameMap.find(nameHash) };
    if (desc.name.empty() || findIt == holder.nameMap.end())
    { // Never added -> use the standard one.
        return pushDesc(std::move(desc), holder);
    }
    // Else, amend already existing one.

    // Retrieve handle.
    const auto originalHandle{ findIt->second };
    // Replace description.
    auto &originalDesc{ getDesc(originalHandle, holder) };
    originalDesc = std::move(desc);

    return originalHandle;
}

template<typename DescT>
desc::DescHandleBase SceneDescription::getHandleFromName(const std::string &name, 
    const desc::ResourceHolder<DescT, HashType> &holder) 
{
    if (name.empty())
    { return desc::NULL_HANDLE_VALUE; }

    const auto findIt{ holder.nameMap.find(std::hash<std::string>()(name)) };
    // Return NULL handle if not found.
    return findIt != holder.nameMap.end() ? findIt->second : desc::NULL_HANDLE_VALUE;
}

template<typename DescT>
DescT &SceneDescription::getDesc(desc::DescHandleBase handle, 
    desc::ResourceHolder<DescT, HashType> &holder)
{
    ASSERT_FAST(handleNotNull(handle));
    return holder.resources[handleToIndex(handle)];
}

template<typename DescT>
const DescT &SceneDescription::getDesc(desc::DescHandleBase handle, 
    const desc::ResourceHolder<DescT, HashType> &holder)
{
    ASSERT_FAST(handleNotNull(handle));
    return holder.resources[handleToIndex(handle)];
}

template<typename DescT>
void SceneDescription::clearDescs(desc::ResourceHolder<DescT, HashType> &holder)
{ holder.resources.clear(); holder.nameMap.clear(); }

} 

} 

// Template implementation end.
