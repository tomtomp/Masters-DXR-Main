/**
 * @file scene/logic/FreeLookController.h
 * @author Tomas Polasek
 * @brief Entity controller which allows a control of 
 * camera in the 3D space.
 */

#pragma once

#include "engine/scene/ECS.h"

#include "engine/scene/SceneComponents.h"

#include "engine/scene/systems/CameraSys.h"

/// Namespace containing the engine code.
namespace quark
{

/// Virtual scene types.
namespace scene
{

/// Scene logic and controllers.
namespace logic
{

/**
 * Entity controller which allows a control of 
 * camera in the 3D space.
 * The camera has 6 degrees of freedom: 
 *   Yaw - turning left/right, 
 *   Pitch - turning up and down, 
 *   Roll - clockwise/anti-clockwise rotation, 
 *   and translation in XYZ
 * Also contains helper functions for getting view and 
 * projection matrices.
 * All of the manipulator methods do nothing when called 
 * on controller without a bound camera.
 */
class FreeLookController
{
public:
    /**
     * Initialize the controller without binding it to a 
     * camera system.
     */
    FreeLookController();

    /// Stop controlling the camera.
    ~FreeLookController();

    /**
     * Initialize the controller using given camera system 
     * for choosing the controlled camera.
     */
    FreeLookController(sys::CameraSys *cameraSystem);

    // Allow copying and moving.
    FreeLookController(const FreeLookController &other) = default;
    FreeLookController &operator=(const FreeLookController &other) = default;
    FreeLookController(FreeLookController &&other) = default;
    FreeLookController &operator=(FreeLookController &&other) = default;

    /**
     * Calculate new rotation quaternion from mouse position change.
     * Takes into account the current rotation, projection the mouse 
     * delta onto a sphere.
     * @param deltaX Mouse change in x-axis, positive value is to the 
     * right.
     * @param deltaY Mouse change in y-axis, positiove vallue is up.
     * @return Returns the new rotation after applying the change.
     */
    dxtk::math::Quaternion rotationFromMouseDelta(float deltaX, float deltaY);

    /// Set rotation of the camera.
    void setRotation(const dxtk::math::Quaternion &rot);
    /// Yaw the camera by a number of degrees.
    void yawBy(float degrees);
    /// Set yaw delta in degrees per one time unit.
    void yawDelta(float deltaDegrees);
    /// Pitch the camera by a number of degrees.
    void pitchBy(float degrees);
    /// Set pitch delta in degrees per one time unit.
    void pitchDelta(float deltaDegrees);
    /// Roll the camera by a number of degrees.
    void rollBy(float degrees);
    /// Set roll delta in degrees per one time unit.
    void rollDelta(float deltaDegrees);
    /// Yaw, pitch and roll camera by a number of degrees.
    void yawPitchRollBy(const dxtk::math::Vector3 &degrees);
    /// Yaw, pitch and roll camera by a number of degrees.
    void yawPitchRollBy(float yawDegrees, float pitchDegrees, float rollDegrees);
    /// Set yaw, pitch and roll in degrees per one time unit.
    void yawPitchRollDelta(float yawDeltaDegrees, float pitchDeltaDegrees, float rollDeltaDegrees);

    /// Get position of the camera.
    dxtk::math::Vector3 position() const;
    /// Set position of the camera.
    void position(const dxtk::math::Vector3 &pos);
    /// Move the camera by given number of units in the x-axis.
    void xMoveBy(float units);
    /// Move the camera by given number of units in the y-axis.
    void yMoveBy(float units);
    /// Move the camera by given number of units in the z-axis.
    void zMoveBy(float units);
    /// Move the camera by given number of units.
    void xyzMoveBy(const dxtk::math::Vector3 &units);
    /// Move the camera by given number of units.
    void xyzMoveBy(float xUnits, float yUnits, float zUnits);

    /// Calculate movement vector in units, for current facing of the camera.
    dxtk::math::Vector3 movementForView(float xUnits, float yUnits, float zUnits);

    /// Calculate movement vector in units, for current facing of the camera.
    dxtk::math::Vector3 movementForView(const dxtk::math::Vector3 &units);

    /// Apply movement forward to the camera in units per one time unit.
    void forward(float units);
    /// Apply movement backward to the camera in units per one time unit.
    void backward(float units);
    /// Apply movement left to the camera in units per one time unit.
    void left(float units);
    /// Apply movement right to the camera in units per one time unit.
    void right(float units);
    /// Apply movement up to the camera in units per one time unit.
    void up(float units);
    /// Apply movement down to the camera in units per one time unit.
    void down(float units);

    /// Set multiplier for the movement speed.
    void speedModifier(float unitMultiplier);

    /// Set viewport dimensions for the cameras.
    void setViewportSize(float width, float height);

    /// Set horizontal FOV (in degrees) of perspective cameras.
    void setFov(float horFov);

    /**
     * Set camera system used by the controller.
     * @param cameraSystem System which will be used for 
     * getting the current controlled camera.
     */
    void setCameraSystem(sys::CameraSys &cameraSystem);

    /**
     * Check the current camera and update it if the camera system 
     * has been changed. If current camera changed, all of the delta 
     * values are set to zero!
     * Perform updates to the camera position/rotation using the 
     * delta values.
     * @param deltaTime Time delta in time units.
     */
    void update(float deltaTime);

    /// Reset the delta values applied with each update.
    void resetDeltas();

    /**
     * Apply delta values to the currently selected 
     * camera.
     * @param deltaTime Time delta in time units.
     */
    void applyDeltas(float deltaTime);

    /// Is there any camera bound to this controller?
    bool cameraBound() const;
private:
    /// Change in degrees for yaw, pitch and roll per one delta time unit.
    dxtk::math::Vector3 mYawPitchRollDeltas{ };
    /// Movement in the direction of the view vector, per one delta time unit.
    dxtk::math::Vector3 mViewMovement{ };

    /// Multiplier for the movement speed.
    float mSpeedMultiplier{ 1.0f };

    /// Camera system used by this controller.
    sys::CameraSys *mCameraSystem{ nullptr };
protected:
}; // class FreeLookController

} // namespace logic

} // namespace scene

} // namespace quark
