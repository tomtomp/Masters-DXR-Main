/**
 * @file scene/logic/ScriptedCameraController.h
 * @author Tomas Polasek
 * @brief Camera controller which automatically controls 
 * the camera according to predefined route.
 */

#pragma once

#include "engine/scene/ECS.h"
#include "engine/scene/SceneComponents.h"
#include "engine/scene/systems/CameraSys.h"

#include "engine/resources/File.h"

#include "engine/util/Timer.h"

/// Namespace containing the engine code.
namespace quark
{

/// Virtual scene types.
namespace scene
{

// Forward declaration.
namespace sys
{
class CameraSys;
}

/// Scene logic and controllers.
namespace logic
{

/**
 * Automation track for camera properties.
 */
class ScriptedCameraTrack
{
public:
    /**
     * Container for information about a single key-point.
     */
    struct KeyPoint
    {
        /// Conversion from seconds to milliseconds.
        static constexpr float sToMs(float seconds)
        { return seconds * 1000.0f; }

        /// Setup a relative translation from the last key-point.
        KeyPoint &setRelativeTranslation(const dxtk::math::Vector3 &t);
        /// Setup absolute translation from the last key-point.
        KeyPoint &setAbsoluteTranslation(const dxtk::math::Vector3 &t);

        /// Setup a relative rotation from the last key-point.
        KeyPoint &setRelativeRotation(const dxtk::math::Quaternion &r);
        /// Setup absolute rotation from the last key-point.
        KeyPoint &setAbsoluteRotation(const dxtk::math::Quaternion &r);

        /// Setup a relative rotation from the last key-point, yaw-pitch-roll in radians.
        KeyPoint &setRelativeYawPitchRoll(const dxtk::math::Vector3 &ypr);
        /// Setup absolute rotation from the last key-point, yaw-pitch-roll in radians.
        KeyPoint &setAbsoluteYawPitchRoll(const dxtk::math::Vector3 &ypr);

        /// Setup a relative rotation from the last key-point, Euler in radians.
        KeyPoint &setRelativeEuler(const dxtk::math::Vector3 &xyz);
        /// Setup absolute rotation from the last key-point, Euler in radians.
        KeyPoint &setAbsoluteEuler(const dxtk::math::Vector3 &xyz);

        /// Set how long should the 
        KeyPoint &setDuration(float seconds);

        /// Translation target or relative change from the last translation.
        dxtk::math::Vector3 translation{ 0.0f, 0.0f, 0.0f };
        /// Should the translation be added to the last one, or is it absolute?
        bool translationRelative{ true };

        /// Rotation target or relative change from the last rotation.
        dxtk::math::Quaternion rotation{ dxtk::math::Quaternion::Identity };
        /// Should the rotation be added to the last one, or is it absolute?
        bool rotationRelative{ true };

        /// How long should the interpolation to the next key-point take?
        float durationInMilliseconds{ 0.0f };
    }; // struct KeyPoint

    /// Function used for key point transformation.
    using TransformFunT = std::function<void(KeyPoint&)>;

    /// Initialize an empty camera track.
    ScriptedCameraTrack();

    /// Destroy the track.
    ~ScriptedCameraTrack();

    // Allow copying and moving.
    ScriptedCameraTrack(const ScriptedCameraTrack &other) = default;
    ScriptedCameraTrack &operator=(const ScriptedCameraTrack &other) = default;
    ScriptedCameraTrack(ScriptedCameraTrack &&other) = default;
    ScriptedCameraTrack &operator=(ScriptedCameraTrack &&other) = default;

    /**
     * Add a new key-point to the end of key-point list.
     * @return Returns key-point configurator, which is valid 
     * until push/clear operation.
     */
    KeyPoint &pushKeyPoint();

    /// Clear all key-points.
    void clearKeyPoints();

    /// Load automatic track from given file.
    void loadFromFile(const res::File &trackFile);
    /// Save current automation track to given file.
    void saveToFile(const res::File &trackFile);

    /**
     * Call provided function on all key points in the 
     * current track.
     * @param fun Function to perform.
     */
    void transformKeyPoints(TransformFunT fun);

    /// Access the list of key-points.
    const auto &keyPoints() const
    { return mKeyPoints; }
private:
    /// Read key-point from file and return whether the operation was successful.
    static bool readKeyPointFromFile(std::ifstream &file, KeyPoint &outKeyPoint);
    /// Write key-point to file and return whether the operation was successful.
    static bool writeKeyPointToFile(std::ofstream &file, const KeyPoint &inKeyPoint);

    /// List of key-points, creating the track.
    std::vector<KeyPoint> mKeyPoints{ };
protected:
}; // class ScriptedCameraTrack

/**
 * Player for scripted camera tracks.
 */
class ScriptedCameraPlayer
{
public:
    /// Structure used for passing camera state.
    struct CameraState
    {
        /// Absolute translation of the camera in world-space.
        dxtk::math::Vector3 translation{ 0.0f, 0.0f, 0.0f };
        /// Absolute rotation of the camera in world-space.
        dxtk::math::Quaternion rotation{ dxtk::math::Quaternion::Identity };
    }; // struct CameraState

    /// Initialize player without a track.
    ScriptedCameraPlayer();

    /// Destroy the track.
    ~ScriptedCameraPlayer();

    // Allow copying and moving.
    ScriptedCameraPlayer(const ScriptedCameraPlayer &other) = default;
    ScriptedCameraPlayer &operator=(const ScriptedCameraPlayer &other) = default;
    ScriptedCameraPlayer(ScriptedCameraPlayer &&other) = default;
    ScriptedCameraPlayer &operator=(ScriptedCameraPlayer &&other) = default;

    /**
     * Load provided track for playback. Automatically starts 
     * the track.
     * @param track Track to load.
     */
    void loadTrack(const ScriptedCameraTrack &track);

    /// Should the track keep looping, or end?
    void setLoopTrack(bool enable);

    /// Start playing the current track from the beginning.
    void restartTrack();

    /// Stop running the current track.
    void stopTrack();

    /**
     * Update the current state, moving it forward by provided 
     * number of milliseconds.
     * @delta Delta time from the last update.
     */
    void updatePlayback(float deltaMs);

    /// Access the current state of the camera.
    const CameraState &currentState() const;

    /// Is the track currently running?
    bool running() const;
private:
    /// Shortcut typedef.
    using KeyPoint = ScriptedCameraTrack::KeyPoint;

    /**
     * Get key-point for the given index.
     * @param track Track used.
     * @param index Index of the current key-point.
     * @param loop Whether the last key-point index should loop 
     * back to the first one.
     * @return Returns Returns the next key-point for specified index.
     */
    static const KeyPoint &getCurrentKeyPoint(const ScriptedCameraTrack &track, std::size_t index);

    /**
     * Get the next key-point for given key-point index.
     * @param track Track used.
     * @param index Index of the current key-point.
     * @param loop Whether the last key-point index should loop 
     * back to the first one.
     * @return Returns Returns the next key-point for specified index.
     */
    static const KeyPoint &getNextKeyPoint(const ScriptedCameraTrack &track, std::size_t index, bool loop);

    /// Automation track which is currently running.
    ScriptedCameraTrack mTrack{ };

    /// Current updated state of the camera.
    CameraState mCameraState{ };

    /// Should the track keep looping?
    bool mLoop{ false };
    /// Is the player currently running?
    bool mRunning{ false };

    /// Index of the current key-point, which is being interpolated.
    std::size_t mKeyPointIndex{ 0u };
    /// Accumulated delta from current key-point.
    float mDeltaMs{ 0.0f };
protected:
}; // class ScriptedCameraPlayer

/**
 * Camera controller which automatically controls 
 * the camera according to predefined route.
 *
 * The path is made up of key-points, each of 
 * them can specify relative and absolute changes in 
 * rotation, translation and other camera properties.
 * Each key-point also has a time specified, which defines 
 * how long the transition into the next key-point takes.
 *
 * Each track - consisting of zero or more key-points - can 
 * be set to loop or to end after one iteration.
 *
 * Secondary functionality allows to record the tracks, manually 
 * adding the key-points. This track can then be saved into file 
 * and loaded later.
 * 
 * All of the manipulator methods do nothing when called 
 * on controller without a bound camera.
 */
class ScriptedCameraController
{
public:
    /**
     * Initialize the controller without binding it to a 
     * camera system.
     */
    ScriptedCameraController();

    /// Stop controlling the camera.
    ~ScriptedCameraController();

    /**
     * Initialize the controller using given camera system 
     * for choosing the controlled camera.
     */
    ScriptedCameraController(sys::CameraSys &cameraSystem);

    // Allow copying and moving.
    ScriptedCameraController(const ScriptedCameraController &other) = default;
    ScriptedCameraController &operator=(const ScriptedCameraController &other) = default;
    ScriptedCameraController(ScriptedCameraController &&other) = default;
    ScriptedCameraController &operator=(ScriptedCameraController &&other) = default;

    /**
     * Set camera system used by the controller.
     * @param cameraSystem System which will be used for 
     * getting the current controlled camera.
     */
    void setCameraSystem(sys::CameraSys &cameraSystem);

    /// Start running the current track.
    void startTrack();

    /// Stop running the current track.
    void stopTrack();

    /// Access the track, which can be ran by this controller.
    ScriptedCameraTrack &track();

    /// Should the track keep looping, or end?
    void setLoopTrack(bool enable);

    /**
     * Check the current camera and update its properties according 
     * to the running track.
     * @param deltaTime Time delta in milliseconds.
     */
    void update(float deltaMs);

    /// Is there any camera bound to this controller?
    bool cameraBound() const;

    /// Is track currently running?
    bool running() const;
private:
    /// Current camera track.
    ScriptedCameraTrack mTrack{ };
    /// Automated track player.
    ScriptedCameraPlayer mPlayer{ };

    /// Camera system used by this controller.
    sys::CameraSys *mCameraSystem{ nullptr };
protected:
}; // class ScriptedCameraController

/// Helper class used for adding track points.
class ScriptedCameraTrackMaker
{
public:
    /// Push a new key-point in the position of the camera.
    void pushCameraPoint(const sys::CameraSys &cameraSystem, ScriptedCameraTrack &track);

    /// Finish the currently recorded track.
    void finishPoints(ScriptedCameraTrack &track);
private:
    /// Last added key-point, which needs to be finished.
    ScriptedCameraTrack::KeyPoint *mLastKeyPoint{ nullptr };
    /// Timer used for timing the key-points.
    util::HrTimer mTimer{ };
protected:
}; // class ScriptedCameraTrackMaker

} // namespace logic

} // namespace scene

} // namespace quark
