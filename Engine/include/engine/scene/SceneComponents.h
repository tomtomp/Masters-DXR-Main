/**
 * @file scene/SceneComponents.h
 * @author Tomas Polasek
 * @brief Components used in the scene Entity-Component-System.
 */

#pragma once

#include "engine/util/SceneGraph.h"
#include "engine/scene/StableComponentHolder.h"
#include "engine/helpers/math/Transform.h"

#include "engine/resources/render/BufferMgr.h"
#include "engine/resources/render/BufferViewMgr.h"
#include "engine/resources/render/StaticSamplerMgr.h"
#include "engine/resources/render/MaterialMgr.h"
#include "engine/resources/render/MeshMgr.h"

#include "engine/scene/SceneDescription.h"

/// Namespace containing the engine code.
namespace quark
{

/// Virtual scene types.
namespace scene
{

/// ECS component types.
namespace comp
{

/**
 * Component containing information about entity's 
 * position and transformation within the scene.
 */
struct Transform : public util::DownPropagateBase
{
    using HolderT = StableComponentHolder<Transform>;

    /**
     * Propagate transformation through the scene-graph.
     * @param t Parents transform component.
     */
    void propagateFrom(const Transform &t)
    {
        // Use provided parent transform and local transform to compute the final transform.
        globalTransform.setMatrix(
            t.globalTransform.matrixNoRecalculation() * localTransform.matrix());

        dirty = false;
    }

    // Default propagation for root nodes.
    void defaultPropagate()
    {
        // Just copy the local transform into the final transform.
        globalTransform = localTransform;

        dirty = false;
    }

    /// Model transform matrix.
    helpers::math::Transform localTransform{ };
    /// Transform including the parents frame of reference.
    helpers::math::Transform globalTransform{ };

    /// Has the local transform been changed?
    bool dirty{ false };
}; // struct Transform

/// Identifier used to uniquely distinguish between scenes.
using SceneIdentifier = std::size_t;

/**
 * Component containing information about which 
 * scene does the entity belong to.
 */
struct Scene
{
    /// Max length of the name component.
    static constexpr std::size_t MAX_NAME_LENGTH{ 16u };

    Scene() = default;
    Scene(SceneIdentifier sceneIdentifier, SceneIdentifier subSceneIdentifier, const std::string &entName) :
        sceneSpec{ sceneIdentifier, subSceneIdentifier }
    { setName(entName); }

    /**
     * Set name of the entity from string.
     * @param entName New name of the entity. At most 
     * MAX_NAME_LENGTH - 1u characters will be used!
     */
    void setName(const std::string &entName)
    {
        if (entName.empty())
        { return; }
#ifdef _MSC_VER
        strncpy_s(name, entName.c_str(), MAX_NAME_LENGTH - 1u);
#else
        std::strncpy(name, entName.c_str(), MAX_NAME_LENGTH - 1u);
#endif
        name[MAX_NAME_LENGTH - 1u] = '\0';
    }

    /// Name of the entity, within the scene.
    char name[MAX_NAME_LENGTH]{ 0u };

    /// Specification of which scene/sub-scene contains this entity.
    SceneSpecification sceneSpec{ };
}; // struct Scene

/**
 * Component containing information about rendering 
 * of the entity.
 */
struct Renderable
{
    /// Handle to the mesh for this entity.
    res::rndr::MeshHandle meshHandle;
}; // struct Renderable

/**
 * When entity contains a camera component, it may 
 * be used to look into the scene with parameters 
 * contained within.
 */
struct Camera
{
    using CameraType = scene::desc::CameraType;
    using HolderT = StableComponentHolder<Transform>;

    /// Copy settings from camera description into this component.
    void fromCameraDesc(const scene::desc::Camera &desc)
    {
        type = desc.type;

        // Assigning the largest union member, should be fine...
        firstParam.width = desc.firstParam.width;
        secondParam.height = desc.secondParam.height;

        zNear = desc.zNear;
        zFar = desc.zFar;
    }

    /// Type of the camera.
    CameraType type{ CameraType::Unknown };

    union
    {
        /// Width used for orthographic projection, can be zero for automatic.
        float width{ 0.0f };
        /// Aspect ratio used for perspective projection, can be zero for automatic.
        float aspectRatio;
    } firstParam;

    union
    {
        /// Height used for orthographic projection, can be zero for automatic.
        float height{ 0.0f };
        /// Horizontal FOV used for perspective projection, can be zero for automatic.
        float horFov;
    } secondParam;

    /// Near plane location.
    float zNear{ 0.0f };

    /**
     * Far plane location, can be 0.0f for perspective 
     * projection for infinite perspective projection.
     */
    float zFar{ 0.0f };
}; // struct Camera

} // namespace comp

} // namespace scene

} // namespace quark
