/**
 * @file scene/ECS.h
 * @author Tomas Polasek
 * @brief Definition of Entity-Component-System universe.
 */

#pragma once

#include "engine/lib/Entropy.h"

/// Namespace containing the engine code.
namespace quark
{

/// Virtual scene types.
namespace scene
{

// Forward declaration for extern template.
class SceneUniverse;
// Template is defined in the cpp source file.
extern template ent::Universe<SceneUniverse>;

/// Identifier used to uniquely distinguish between scenes/sub-scenes.
using SceneIdentifier = std::size_t;

/// Specification of sub-scene within a primary scene.
struct SceneSpecification
{
    /// Compare scene specifications, returning true if they reference the same scene.
    bool operator==(const SceneSpecification &other) const
    { return primary == other.primary && secondary == other.secondary; }
    /// Compare scene specifications, returning false if they reference the same scene.
    bool operator!=(const SceneSpecification &other) const
    { return !operator==(other); }

    /// Identifier of the primary scene.
    SceneIdentifier primary{ 0u };
    /// Identifier of the sub-scene.
    SceneIdentifier secondary{ 0u };
}; // struct SceneSpecification

/**
 * Entity-Component-System universe used for storing scene entities.
 */
class SceneUniverse : public ent::Universe<SceneUniverse>
{
public:
private:
    // Scene manager need to be able to get the instance.
    friend class SceneSubSystem;

    // Only one instance should ever exist -> singleton.
    SceneUniverse() = default;
    ~SceneUniverse() = default;
    SceneUniverse(const SceneUniverse& other) = delete;
    SceneUniverse &operator=(const SceneUniverse& other) = delete;

    /**
     * Register components used for scene entities.
     */
    void registerSceneComponents();
protected:
}; // class SceneUniverse

/// Type of an entity in the scene.
using EntityHandle = SceneUniverse::EntityT;
/// Type of the inner entity identifier contained within entity handle.
using EntityId = ent::EntityId;
/// Base type for all system working in the ECS.
using UniverseSystem = SceneUniverse::SystemT;

} // namespace scene

} // namespace quark
