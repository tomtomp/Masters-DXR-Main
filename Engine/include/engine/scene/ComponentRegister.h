/**
 * @file scene/ComponentRegister.h
 * @author Tomas Polasek
 * @brief Helper class used for registering components for the ECS.
 */

#pragma once

#include "engine/scene/ECS.h"

/// Namespace containing the engine code.
namespace quark
{

/// Virtual scene types.
namespace scene
{

/**
 * Helper class used for registering components for the ECS.
 */
class ComponentRegister
{
public:
    /**
     * Register a component and pass given constructor arguments to its holder 
     * on construction.
     * @tparam CompT Type of the component.
     * @tparam HolderCArgTs Types of constructor arguments of the holder.
     * @param holderCArgs Constructor arguments which must be still valid when 
     * the engine is initialized. They may be copied and moved in the process!
     */
    template <typename CompT, typename... HolderCArgTs>
    static void registerComponent(HolderCArgTs... holderCArgs);

    /**
     * Perform the registration of components on provided universe.
     * @param universe The universe which should have its components registered.
     */
    static void performRegistration(SceneUniverse &universe);
private:
    /// Pure static class.
    ComponentRegister() = delete;
    
    /// Type of the function which will be used for registration.
    using RegisterFunctionT = std::function<void(SceneUniverse&)>;

    /// List of functions for registering the components.
    static inline std::vector<RegisterFunctionT> sRegisterFunctions{ };
protected:
}; // class ComponentRegister

} // namespace scene

} // namespace quark

// Template implementation.

namespace quark
{

namespace scene
{

template<typename CompT, typename... HolderCArgTs>
void ComponentRegister::registerComponent(HolderCArgTs... holderCArgs)
{
    // Create the registration function.
    sRegisterFunctions.emplace_back([holderCArgs...] (SceneUniverse &universe) {
        universe.registerComponent<CompT>(std::forward<HolderCArgTs>(holderCArgs)...); 
    });
}

inline void ComponentRegister::performRegistration(SceneUniverse &universe)
{
    // Perform the registration for all registered components.
    for (auto &registerFunction : sRegisterFunctions)
    { registerFunction(universe); }
}

}

}

// Template implementation end.
