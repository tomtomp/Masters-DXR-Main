/**
 * @file scene/loaders/BaseSceneLoader.h
 * @author Tomas Polasek
 * @brief Base class for all scene loaders.
 */

#pragma once

#include "engine/resources/BaseFile.h"
#include "engine/resources/File.h"
#include "engine/resources/scene/SceneMgr.h"

/// Namespace containing the engine code.
namespace quark
{

/// Virtual scene types.
namespace scene
{

/// Scene resource loaders.
namespace loaders
{

/**
 * Base class for all scene loaders.
 */
class BaseSceneLoader
{
public:
    /// Exception thrown when error occurrs while loading a scene.
    struct SceneLoadException : std::exception
    {
        SceneLoadException(const char *msg) :
            std::exception(msg)
        { }
    }; // struct SceneLoadException

    /// Exception thrown when specified scene file has not been found.
    struct SceneFileNotFoundException : std::exception
    {
        SceneFileNotFoundException(const char *msg) :
            std::exception(msg)
        { }
    }; // struct SceneFileNotFoundException

    /// Initialize the loader without loading any scenes.
    BaseSceneLoader() = default;

    /// Free all resources, already created scenes must still be valid.
    virtual ~BaseSceneLoader() = default;

    // No copying.
    BaseSceneLoader(const BaseSceneLoader &other) = delete;
    BaseSceneLoader &operator=(const BaseSceneLoader &other) = delete;
    // Allow moving.
    BaseSceneLoader(BaseSceneLoader &&other) = default;
    BaseSceneLoader &operator=(BaseSceneLoader &&other) = default;
private:
protected:
}; // class BaseSceneLoader

} // namespace loaders

} // namespace scene

} // namespace quark
