/**
 * @file scene/loaders/GlTFLoader.h
 * @author Tomas Polasek
 * @brief Wrapper around tinyglTF library, used for loading glTF files.
 */

#pragma once

#include "engine/scene/loaders/BaseSceneLoader.h"

#include "engine/scene/SceneDescription.h"

/// Namespace containing the engine code.
namespace quark
{

/// Virtual scene types.
namespace scene
{

/// Scene resource loaders.
namespace loaders
{

/**
 * Wrapper around tinyglTF library, used for loading glTF files.
 */
class GlTFLoader : public BaseSceneLoader
{
public:
    /// Initialize the loader without loading any scenes.
    GlTFLoader() = default;

    /// Free all resources, already created scenes must still be valid.
    virtual ~GlTFLoader() = default;

    // No copying.
    GlTFLoader(const GlTFLoader &other) = delete;
    GlTFLoader &operator=(const BaseSceneLoader &other) = delete;
    // Allow moving.
    GlTFLoader(GlTFLoader &&other) = default;
    GlTFLoader &operator=(GlTFLoader &&other) = default;

    /**
     * Load a scene or multiple scenes contained within provided 
     * file.
     * Currently only some of the glTF sub-formats are supported. 
     * Specific type of glTF file will be chosen by extension of 
     * the file: 
     *   ".gltf" -> ASCII
     *   ".glb"  -> Binary
     * @param sceneFile Specification of the file which contains 
     * the requested scene data.
     * @param mgr Target scene manager to load the scenes into.
     * @return Returns handle to the primary scene, loaded from 
     * provided file.
     * @throws SceneLoadException Thrown when any error occurs 
     * while loading the scene data.
     * @throws SceneFileNotFoundException Thrown when provided 
     * scene file does not exist, or is otherwise inaccessible.
     * @throws Any other exceptions which were thrown by the 
     * resource manager, while creating the scene.
     */
    res::scene::SceneHandle loadFromFile(const res::File &sceneFile, 
        res::scene::SceneMgr &mgr);

    /**
     * Load a scene or multiple scenes contained within provided 
     * file and store the information into a scene description.
     * Currently only some of the glTF sub-formats are supported. 
     * Specific type of glTF file will be chosen by extension of 
     * the file: 
     *   ".gltf" -> ASCII
     *   ".glb"  -> Binary
     * @param sceneFile Specification of the file which contains 
     * the requested scene data.
     * @return Returns pointer to the scene description.
     * @throws SceneLoadException Thrown when any error occurs 
     * while loading the scene data.
     * @throws SceneFileNotFoundException Thrown when provided 
     * scene file does not exist, or is otherwise inaccessible.
     * @throws Any other exceptions which were thrown by the 
     * resource manager, while creating the scene.
     */
    scene::SceneDescription::PtrT loadFromFileToSceneDesc(const res::File &sceneFile);
private:
protected:
}; // class GlTFLoader

} // namespace loaders

} // namespace scene

} // namespace quark
