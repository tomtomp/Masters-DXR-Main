/**
 * @file scene/loaders/SceneDescLoader.h
 * @author Tomas Polasek
 * @brief Loader which allows scene loading from SceneDescription objects.
 */

#pragma once

#include "engine/scene/loaders/BaseSceneLoader.h"

#include "engine/scene/SceneDescription.h"

/// Namespace containing the engine code.
namespace quark
{

/// Virtual scene types.
namespace scene
{

/// Scene resource loaders.
namespace loaders
{

/**
 * Loader which allows scene loading from SceneDescription objects.
 */
class SceneDescLoader : public BaseSceneLoader
{
public:
    /// Initialize the loader without loading any scenes.
    SceneDescLoader() = default;

    /// Free all resources, already created scenes remain valid.
    virtual ~SceneDescLoader() = default;

    // No copying.
    SceneDescLoader(const SceneDescLoader &other) = delete;
    SceneDescLoader &operator=(const BaseSceneLoader &other) = delete;
    // Allow moving.
    SceneDescLoader(SceneDescLoader &&other) = default;
    SceneDescLoader &operator=(SceneDescLoader &&other) = default;

    /**
     * Load a scene or multiple scenes contained within provided
     * scene description
     * @param sceneDesc Description of requested scene.
     * @param mgr Target scene manager to load the scenes into.
     * @return Returns handle to the primary scene, loaded from 
     * provided file.
     * @throws SceneLoadException Thrown when any error occurs 
     * while loading the scene data.
     */
    res::scene::SceneHandle loadFromDesc(const SceneDescription::ConstPtrT sceneDesc, 
        res::scene::SceneMgr &mgr);

    /**
     * Load a default testing scene and return a handle to it.
     * @param mgr Target scene manager to load the scenes into.
     * @return Returns handle to the primary scene, loaded from 
     * provided file.
     * @throws SceneLoadException Thrown when any error occurs 
     * while loading the scene data.
     */
    res::scene::SceneHandle loadTestingScene(res::scene::SceneMgr &mgr);
private:
	/// Name of the default sampler, used for textures without sampler specified.
	static constexpr const char* DEFAULT_SAMPLER_NAME{"DefaultSampler"};

    /// Create resources and scenes as described in the scene.
    res::scene::SceneHandle createScenes(const SceneDescription::ConstPtrT sceneDesc,
        res::scene::SceneMgr &mgr);

    /// Add specified buffer as a scene resource.
    void createBuffer(const SceneDescription &sceneDesc, res::scene::Scene &primaryScene, 
        const scene::desc::Buffer &bufferDesc);
    /// Add specified buffer view as a scene resource.
    void createBufferView(const SceneDescription &sceneDesc, res::scene::Scene &primaryScene, 
        const scene::desc::BufferView &bufferViewDesc);
    /// Add specified sampler as a scene resource.
    void createSampler(const SceneDescription &sceneDesc, res::scene::Scene &primaryScene, 
        const scene::desc::Sampler &samplerDesc);
    /// Create the default sampler as specified by the scene.
    void createDefaultSampler(const SceneDescription &sceneDesc, res::scene::Scene &primaryScene);
    /// Add specified texture as a scene resource.
    void createTexture(const SceneDescription &sceneDesc, res::scene::Scene &primaryScene, 
        const scene::desc::Texture &textureDesc);
    /// Add specified material as a scene resource.
    void createMaterial(const SceneDescription &sceneDesc, res::scene::Scene &primaryScene, 
        const scene::desc::Material &materialDesc);
    /// Add specified primitive to the mesh.
    void createMeshPrimitive(res::rndr::Mesh &meshVal, const scene::desc::Primitive &primitiveDesc);
    /// Add specified mesh as a scene resource.
    void createMesh(const SceneDescription &sceneDesc, res::scene::Scene &primaryScene, 
        const scene::desc::Mesh &meshDesc);
    /// Setup entity components according to the description.
    void createEntityComponents(const SceneDescription &sceneDesc, EntityHandle &entity, 
        const scene::desc::Entity &entityDesc);
    /// Add specified sub-scene and all of its entities.
    void createSubSceneEntities(const SceneDescription &sceneDesc, res::scene::Scene &primaryScene, 
        const scene::desc::SubScene &subSceneDesc, res::scene::SubSceneHandle &subScene);
    /// Add specified sub-scene and all of its entities.
    void createSubScene(const SceneDescription &sceneDesc, res::scene::Scene &primaryScene, 
        const scene::desc::SubScene &subSceneDesc);
    /// Create a default camera for given sub-scene.
    void createDefaultCamera(res::scene::Scene &primaryScene, res::scene::SubSceneHandle &subScene);

    /// Create all resources required by the scene.
    void createSceneResources(const SceneDescription &sceneDesc, 
        res::scene::Scene &primaryScene);
    /// Create sub-scenes and entities.
    void createSceneEntities(const SceneDescription &sceneDesc, 
        res::scene::Scene &primaryScene);

	/// Buffer resources created for the primary scene.
	std::vector<res::rndr::BufferHandle> mBuffers;
	/// Buffer view resources created for the primary scene.
	std::vector<res::rndr::BufferViewHandle> mBufferViews;
	/// Sampler resources created for the primary scene.
	std::vector<res::rndr::StaticSamplerHandle> mSamplers;
	/// Texture resources created for the primary scene.
	std::vector<res::rndr::TextureHandle> mTextures;
	/// Material resources created for the primary scene.
	std::vector<res::rndr::MaterialHandle> mMaterials;
	/// Mesh resources created for the primary scene.
	std::vector<res::rndr::MeshHandle> mMeshes;
protected:
}; // class SceneDescLoader

} // namespace loaders

} // namespace scene

} // namespace quark
