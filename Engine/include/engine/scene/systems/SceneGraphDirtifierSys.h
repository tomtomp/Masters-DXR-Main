/**
 * @file scene/systems/SceneGraphDirtifierSys.h
 * @author Tomas Polasek
 * @brief System for the main scene universe, which marks entities 
 * dirty within the current sub-scene scene-graph. 
 */

#pragma once

#include "engine/scene/ECS.h"

#include "engine/scene/SceneComponents.h"

#include "engine/resources/scene/SceneMgr.h"

/// Namespace containing the engine code.
namespace quark
{

/// Virtual scene types.
namespace scene
{

/// Scene universe systems.
namespace sys
{

/**
 * System for the main scene universe, which marks entities 
 * dirty within the current sub-scene scene-graph. 
 */
class SceneGraphDirtifierSys : public UniverseSystem
{
public:
    // Only take entities which contain transform to dirtify.
    using Require = ent::Require<comp::Transform>;
    using Reject = ent::Reject<>;
    // Only take active entities, which should be the ones in the chosen scene.
    using Activity = ent::activity::Active;

    /**
     * Initialize the dirtifier, without processing any entities.
     */
    SceneGraphDirtifierSys();

    /**
     * Dirtify entities, which have a dirty transform. The result is 
     * passed to the currently selected scene/sub-scene combination 
     * and their scene-graph.
     * @param chosenScene Scene which is currently chosen.
     */
    void refresh(const res::scene::SceneHandle &chosenScene);
private:
protected:
}; // class SceneGraphDirtifierSys

} // namespace sys

} // namespace scene

} // namespace quark
