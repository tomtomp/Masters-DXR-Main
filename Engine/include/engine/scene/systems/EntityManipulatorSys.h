/**
 * @file scene/systems/EntityManipulatorSys.h
 * @author Tomas Polasek
 * @brief System which allows manipulation of entities 
 * and ways of their creation, deletion and other changes.
 */

#pragma once

#include "engine/scene/ECS.h"

#include "engine/scene/SceneComponents.h"

/// Namespace containing the engine code.
namespace quark
{

/// Virtual scene types.
namespace scene
{

/// Scene universe systems.
namespace sys
{

/**
 * System which allows manipulation of entities 
 * and ways of their creation, deletion and other changes.
 */
class EntityManipulatorSys : public UniverseSystem
{
public:
    /// Exception thrown when requested component is not found.
    struct ComponentNotAvailable : public std::exception 
    {
        ComponentNotAvailable(const char *msg) : 
            std::exception(msg)
        { }
    }; // struct ComponentNotAvailable

    // Take all available entities.
    using Require = ent::Require<>;
    using Reject = ent::Reject<>;
    // Only entities in the current scene are active.
    using Activity = ent::activity::Active;

    /**
     * Initialize the manipulator without affecting the entities.
     */
    EntityManipulatorSys();

    /**
     * Get iterable object used for iterating trough Entities within the group.
     * @return Returns iterable for iterating through Entities withing the group.
     */
    using UniverseSystem::foreach;

    /**
     * Get iterable object used for iterating trough Entities which were added since the last refresh.
     * @return Returns iterable for iterating through Entities which were added since the last refresh.
     */
    using UniverseSystem::foreachAdded;

    /**
     * Get iterable object used for iterating trough Entities which were removed since the last refresh.
     * @return Returns iterable for iterating through Entities which were removed since the last refresh.
     * @warning The Entities withing this Group may not exist anymore!!
     */
    using UniverseSystem::foreachRemoved;

    /**
     * Duplicate the input entity including all of its components defined in the template type list.
     * @tparam CompTs Component types which should be copied.
     * @param source Source entity from which to copy the component.
     * @param desc Destination entity to add the component to.
     * @throws ComponentNotAvailable Thrown when one of the requested components is not present.
     */
    template <typename... CompTs>
    static void duplicate(const EntityHandle &source, EntityHandle &dest);

    /**
     * Duplicate the input entity including all of its components defined in the template type list.
     * This version skips any unavailable component types.
     * @tparam CompTs Component types which should be copied.
     * @param source Source entity from which to copy the component.
     * @param desc Destination entity to add the component to.
     */
    template <typename... CompTs>
    static void duplicateSkipUnavailable(const EntityHandle &source, EntityHandle &dest);
private:
    /**
     * Duplicate a component type from the source entity into the destination entity.
     * Duplicated component will have the same value as the source.
     * @tparam CompT Component type to duplicate.
     * @param source Source entity from which to copy the component.
     * @param desc Destination entity to add the component to.
     * @param requirePresent When set to false, the components not available from the 
     * source entity will be skipped.
     * @throws ComponentNotAvailable Thrown when requirePresent is set to true but the component is 
     * not available.
     */
    template <typename CompT>
    static void duplicateComponent(const EntityHandle &source, EntityHandle &dest, bool requirePresent);

    template <typename... CompTs>
    struct DuplicationHelper;

    /**
     * Helper for duplicating the components.
     * @tparam CompTs Component types which should be copied.
     */
    template <typename FirstCompT, typename... CompTs>
    struct DuplicationHelper<FirstCompT, CompTs...>
    {
        /**
         * Duplicate all specified components from the source entity to the destination entity.
         * @param source Source entity from which to copy the component.
         * @param desc Destination entity to add the component to.
         * @param requirePresent When set to false, the components not available from the 
         * source entity will be skipped.
         * @throws ComponentNotAvailable Thrown when requirePresent is set to true but the component is 
         * not available.
         */
        static void duplicateComponents(const EntityHandle &source, EntityHandle &dest, bool requirePresent);
    };

    template <typename LastCompT>
    struct DuplicationHelper<LastCompT>
    {
        /**
         * Duplicate all specified components from the source entity to the destination entity.
         * @param source Source entity from which to copy the component.
         * @param desc Destination entity to add the component to.
         * @param requirePresent When set to false, the components not available from the 
         * source entity will be skipped.
         * @throws ComponentNotAvailable Thrown when requirePresent is set to true but the component is 
         * not available.
         */
        static void duplicateComponents(const EntityHandle &source, EntityHandle &dest, bool requirePresent);
    };
protected:
}; // class EntityManipulatorSys

} // namespace sys

} // namespace scene

} // namespace quark

// Template implementation start.

namespace quark
{

namespace scene
{

namespace sys
{

template<typename... CompTs>
void EntityManipulatorSys::duplicate(const EntityHandle &source, EntityHandle &dest)
{ DuplicationHelper<CompTs...>::duplicateComponents(source, dest, true); }

template<typename... CompTs>
void EntityManipulatorSys::duplicateSkipUnavailable(const EntityHandle &source, EntityHandle &dest)
{ DuplicationHelper<CompTs...>::duplicateComponents(source, dest, false); }

template<typename CompT>
void EntityManipulatorSys::duplicateComponent(const EntityHandle &source, EntityHandle &dest, bool requirePresent)
{
    // Get the source component.
    const CompT *sourceComp{ nullptr };
    if (source.has<CompT>())
    { // We have the requested component -> duplicate it!
        sourceComp = source.get<CompT>();
    }
    else if (requirePresent)
    { // Component should be present -> throw exception.
        throw ComponentNotAvailable("Unable to duplicate component: Requested component is not present!");
    }
    else
    { /* Nothing to be done. */ }

    // Get the destination component.
    CompT *destComp{ nullptr };
    if (dest.has<CompT>())
    { // Component type is already present -> use it.
        destComp = dest.get<CompT>(); 
    }
    else
    { // Component not present -> create it.
        destComp = dest.add<CompT>(); 
    }

    // Copy it over.
    if (sourceComp && destComp)
    { *destComp = *sourceComp; }
}

template <typename FirstCompT, typename... RestCompTs>
void EntityManipulatorSys::DuplicationHelper<FirstCompT, RestCompTs...>::duplicateComponents(const EntityHandle &source, EntityHandle &dest, bool requirePresent)
{
    // Call through to the implementation.
    duplicateComponent<FirstCompT>(source, dest, requirePresent);
    // Continue in the component list.
    DuplicationHelper<RestCompTs...>::duplicateComponents(source, dest, requirePresent);
}

template <typename LastCompT>
void EntityManipulatorSys::DuplicationHelper<LastCompT>::duplicateComponents(const EntityHandle &source, EntityHandle &dest, bool requirePresent)
{
    // Call through to the implementation.
    duplicateComponent<LastCompT>(source, dest, requirePresent);
    // End now...
}

}

}

}
// Template implementation end.