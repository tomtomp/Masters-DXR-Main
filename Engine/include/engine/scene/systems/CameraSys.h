/**
 * @file scene/systems/CameraSys.h
 * @author Tomas Polasek
 * @brief System which allows choosing a camera from 
 * within the current scene and allows access to its transform.
 */

#pragma once

#include "engine/scene/ECS.h"

#include "engine/scene/SceneComponents.h"

#include "engine/helpers/math/Camera.h"

/// Namespace containing the engine code.
namespace quark
{

/// Virtual scene types.
namespace scene
{

/// Scene universe systems.
namespace sys
{

/**
 * System which allows choosing a camera from within the 
 * current scene and allows access to its transform.
 */
class CameraSys : public UniverseSystem
{
public:
    // Only take entities which are usable as cameras.
    using Require = ent::Require<comp::Camera, comp::Transform>;
    using Reject = ent::Reject<>;
    using Activity = ent::activity::Active;

    /// Exception thrown when no cameras are available.
    struct NoCamerasException : public std::exception
    {
        NoCamerasException(const char *msg) :
            std::exception(msg)
        { }
    }; // struct NoCamerasException

    /// Helpre structure for passing information about camera.
    struct CameraInfo
    {
        /// Name of the camera, may be empty.
        std::string name;
        /// Physical parameters of the camera.
        comp::Camera parameters;
    }; // struct CameraInfo

    /**
     * Initialize the camera system without choosin any cameras.
     */
    CameraSys();

    /**
     * Refresh the list of available cameras, possibly removing 
     * the currently selected camera.
     * If the chosen camera is removed, then a random one will 
     * be chosen instead.
     * @throws NoCamerasException If current camera has been 
     * removed and no other cameras are available.
     */
    void refresh();

    /**
     * Get current number of available cameras.
     * @return Returns count of currently available 
     * cameras.
     */
    std::size_t numAvailableCameras();

    /**
     * Get information about camera with given index.
     * @param idx Index of the camera, must be smaller 
     * than the value returned by numAvailableCameras()!
     * @return Returns information about camera under 
     * given index.
     * @throws NoCamerasException If given index is out 
     * of range of the cameras vector.
     */
    CameraInfo cameraInfo(std::size_t idx);

    /**
     * Chose camera with given index.
     * @param idx Index of the camera, must be smaller 
     * than the value returned by numAvailableCameras()!
     * @throws NoCamerasException If given index is out 
     * of range of the cameras vector.
     */
    void chooseCamera(std::size_t idx);

    /**
     * Access transform of the chosen camera.
     * @throws NoCamerasException If no camera has been 
     * chosen yet.
     */
    comp::Transform &chosenCameraTransform();

    /**
     * Access transform of the chosen camera.
     * @throws NoCamerasException If no camera has been 
     * chosen yet.
     */
    const comp::Transform &chosenCameraTransform() const;

    /// Mark chosen camera transform as dirty.
    void chosenCameraTransformChanged();

    /**
     * Access physical parameters of the chosen camera.
     * @throws NoCamerasException If no camera has been 
     * chosen yet.
     */
    comp::Camera &chosenCameraParameters();

    /// Is there a chosen camera?
    bool cameraChosen() const;

    /// Has the camera changed since last refresh?
    bool cameraChanged() const;

    /// Set viewport dimensions for the cameras.
    void setViewportSize(float width, float height);

    /// Set horizontal FOV of perspective cameras.
    void setFov(float horFov);

    /// Calculate the current view matrix and return it.
    const dxtk::math::Matrix &viewMatrix();
    /// Calculate the current projection matrix and return it.
    const dxtk::math::Matrix &projectionMatrix();
    /// Calculate the view-projection matrix and return it.
    const dxtk::math::Matrix &viewProjectionMatrix();

    /// Get the currently used camera.
    const helpers::math::Camera &camera() const;

    /**
     * Generate camera matrices from currently selected camera.
     */
    void generateCameraMatrices();
private:
    /**
     * Check whether the currently chosen camera 
     * has been removed.
     * @return Returns true if it has been removed, else 
     * returns false.
     */
    bool checkChosenCamera();

    /// Handle to the currently chosen camera entity.
    EntityHandle mChosenCamera{ };

    /// Has the camera changed since last refresh?
    bool mCameraChanged{ false };

    /// Width of the viewport.
    float mWidth{ 0.0f };
    /// Height of the viewport.
    float mHeight{ 0.0f };

    /// Horizontal FOV used for perspective cameras.
    float mHorizontalFov{ 0.0f };

    /// Flag specifying whether the camera parameters changed.
    bool mCameraDirty{ true };

    /// Helper for camera matrix calculations.
    helpers::math::Camera mCamera{ };
protected:
}; // class CameraSys

} // namespace sys

} // namespace scene

} // namespace quark
