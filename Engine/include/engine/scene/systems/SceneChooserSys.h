/**
 * @file scene/systems/SceneChooserSys.h
 * @author Tomas Polasek
 * @brief System for the main scene universe, which activates only 
 * entities within the chosen scene.
 */

#pragma once

#include "engine/scene/ECS.h"

#include "engine/scene/SceneComponents.h"

/// Namespace containing the engine code.
namespace quark
{

/// Virtual scene types.
namespace scene
{

/// Scene universe systems.
namespace sys
{

/**
 * System for the main scene universe, which activates only 
 * entities within the chosen scene.
 */
class SceneChooserSys : public UniverseSystem
{
public:
    // Only take entities which are assigned to a scene.
    using Require = ent::Require<comp::Scene>;
    using Reject = ent::Reject<>;
    /*
     * We need to iterate through active and inactive 
     * entities when choosing a scene.
     */
    using Activity = ent::activity::Any;

    /**
     * Initialize the system without selecting scene.
     */
    SceneChooserSys();

    /**
     * Set which scene is currently active.
     * @param scene Specification of the selected scene.
     */
    void setActiveScene(SceneSpecification scene);

    /**
     * Activate entities which are in the chosen scene and 
     * deactivate any others.
     */
    void refresh();
private:
    /**
     * Process all entities, enabling the ones which are within 
     * chosen scene and disabling all others.
     */
    void processAll();

    /**
     * Process added entities, enabling the ones which are within 
     * chosen scene and disabling all others.
     */
    void processChanges();

    /// Identifier of the currently selected scene.
    SceneSpecification mChosenScene{ };
    /// Has the selected scene been changed?
    bool mChangedScenes{ false };
protected:
}; // class SceneDrawSys

} // namespace sys

} // namespace scene

} // namespace quark
