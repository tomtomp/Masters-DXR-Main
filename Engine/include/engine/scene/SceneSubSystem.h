/**
 * @file scene/SceneSubSystem.h
 * @author Tomas Polasek
 * @brief Helper class for managing scenes - loading, unloading, activating, etc.
 */

#pragma once

// Connection to rendering: 
#include "engine/renderer/RenderSubSystem.h"

// Entity-Component-System elements: 
#include "engine/scene/ECS.h"
#include "engine/scene/SceneComponents.h"
#include "engine/scene/ComponentRegister.h"
#include "engine/scene/systems/SceneChooserSys.h"
#include "engine/scene/systems/SceneDrawSys.h"
#include "engine/scene/systems/SceneGraphDirtifierSys.h"
#include "engine/scene/systems/EntityManipulatorSys.h"

// Resources and their managers: 
#include "engine/resources/File.h"
#include "engine/resources/scene/SceneMgr.h"
#include "engine/resources/scene/DrawableListMgr.h"

/// Namespace containing the engine code.
namespace quark
{

/// Virtual scene types.
namespace scene
{

/**
 * Helper class used for managing scenes and 
 * manipulating entities in them.
 */
class SceneSubSystem : public util::PointerType<SceneSubSystem>
{
public:
    /// Exception thrown when setting unprepared scene as current.
    struct SceneNotPreparedException : public std::exception
    {
        SceneNotPreparedException(const char *msg) :
            std::exception(msg)
        { }
    }; // struct SceneNotPreparedException

    /**
     * Initialize scene management and ECS universe.
     * @param cfg Runtime configuration.
     * @param renderSubSystem Rendering sub-system used 
     * for rendering the scenes.
     */
    static PtrT create(const EngineRuntimeConfig &cfg, rndr::RenderSubSystem &renderSubSystem);

    // No copying or moving.
    SceneSubSystem(const SceneSubSystem &other) = delete;
    SceneSubSystem &operator=(const SceneSubSystem &other) = delete;
    SceneSubSystem(SceneSubSystem &&other) = delete;
    SceneSubSystem &operator=(SceneSubSystem &&other) = delete;

    /**
     * Load a scene in given file.
     * The scene will be first loaded into SceneDescription and then processed.
     * Currently only supported formats are glTF ASCII 
     * and Binary. Specific type of glTF file will be 
     * chosen by extension of the file: 
     *   ".gltf" -> ASCII
     *   ".glb"  -> Binary
     * @param sceneFile Name of the file containing 
     * the scene. Can contain full path to the file.
     * @return Returns handle to the primary scene.
     * @throws BaseSceneLoader::SceneFileNotFoundException Thrown when provided 
     * scene file could not be found.
     * @throws BaseSceneLoader::SceneLoadException Thrown when tinyGlTF fails 
     * to load provided scene.
     */
    res::scene::SceneHandle loadGlTFScene(const res::File &sceneFile);

    /**
     * Load a scene in given file.
     * The scene will be directly loaded through the use of glTF loader.
     * Currently only supported formats are glTF ASCII 
     * and Binary. Specific type of glTF file will be 
     * chosen by extension of the file: 
     *   ".gltf" -> ASCII
     *   ".glb"  -> Binary
     * @param sceneFile Name of the file containing 
     * the scene. Can contain full path to the file.
     * @return Returns handle to the primary scene.
     * @throws BaseSceneLoader::SceneFileNotFoundException Thrown when provided 
     * scene file could not be found.
     * @throws BaseSceneLoader::SceneLoadException Thrown when tinyGlTF fails 
     * to load provided scene.
     */
    res::scene::SceneHandle loadGlTFSceneDirect(const res::File &sceneFile);

    /**
     * Load a scene from scene description.
     * @param sceneDesc Pointer to the scene description.
     * @return Returns handle to the primary scene.
     * @throws BaseSceneLoader::SceneLoadException Thrown when any error occurs 
     * while loading the scene data.
     */
    res::scene::SceneHandle loadSceneDescription(SceneDescription::ConstPtrT sceneDesc);

    /**
     * Load a testing scene.
     * @return Returns handle to the testing scene.
     */
    res::scene::SceneHandle loadTestingScene();

    /**
     * Load a default (empty) scene with a simple camera.
     * @return Returns handle to the default scene.
     */
    res::scene::SceneHandle loadEmptyScene();

    /**
     * Prepare provided scene for use.
     * @param sceneHandle Handle to the target scene.
     */
    void prepareScene(const res::scene::SceneHandle &sceneHandle);

    /**
     * Refresh the scene universe and perform operations for the currently 
     * selected scene.
     */
    void refresh();

    /**
     * Switch to provided scene.
     * @param sceneHandle The new scene to use.
     * @throws SceneNotPreparedException Thrown when given scene has not 
     * been prepared.
     */
    void setCurrentScene(const res::scene::SceneHandle &sceneHandle);

    /**
     * Get list of entities which should be rendered.
     * Should be called after refreshing.
     */
    res::scene::DrawableListHandle getToDraw();

    /**
     * Add or get already existing system for the main scene universe.
     * @tparam SysT Type of the system.
     * @tparam CArgTs Types of constructor arguments passed to the system.
     * @param cArgs Arguments passed to the system constructor, if it 
     * was not constructed yet.
     * @return Returns pointer to the newly constructed system, or pointer 
     * to the instance of already created system, if it was already created 
     * in the past.
     */
    template <
        typename SysT,
        typename... CArgTs>
    SysT *addGetSystem(CArgTs... cArgs);

    /**
     * Get iterable object used for iterating trough active Entities within the currently 
     * active scene.
     * @return Returns iterable for iterating through Entities withing the group.
     */
    auto foreachSceneEntity()
    { return mEntityManipulatorSys->foreach(); }
    /**
     * Get iterable object used for iterating trough active Entities which were added since the last refresh.
     * @return Returns iterable for iterating through Entities which were added since the last refresh.
     */
    auto foreachSceneEntityAdded()
    { return mEntityManipulatorSys->foreachAdded(); }
    /**
     * Get iterable object used for iterating trough active Entities which were removed since the last refresh.
     * @return Returns iterable for iterating through Entities which were removed since the last refresh.
     */
    auto foreachSceneEntityRemoved()
    { return mEntityManipulatorSys->foreachRemoved(); }

    /**
     * Create a new entity in the current scene.
     * @return Returns handle to the newly created entity.
     */
    EntityHandle createEntity(const std::string &name = "");

    /**
     * Destroy provided entity, which should be contained in the 
     * current scene.
     * @param entity Entity which should be destroyed. After calling 
     * this method, it will be invalid.
     */
    void destroyEntity(EntityHandle &entity);

    /**
     * Duplicate entity and all of the specified component types.
     * @tparam CompTs Component types which should be dulicated, comp::scene will be included automatically.
     * @param entity Source entity which should be duplicated.
     * @return Returns a handle to the duplicated entity.
     */
    template <typename... CompTs>
    EntityHandle duplicateSceneEntity(const EntityHandle &entity);
private:
    /// Name of the empty scene which can be loaded by default.
    static constexpr const char *DEFAULT_EMPTY_SCENE_NAME{ "EmptyScene" };

    /**
     * Initialize the scene universe.
     */
    void initializeUniverse();

    /// ECS universe used for storing scene entities.
    SceneUniverse mUniverse;

    /// Manager of scenes.
    std::shared_ptr<res::scene::SceneMgr> mSceneMgr{ };
    /// Manager of lists of drawable entities.
    std::shared_ptr<res::scene::DrawableListMgr> mDrawableListMgr{ };
    /// Handle to the current scene.
    res::scene::SceneHandle mCurrentScene{ };
    /// System used for choosing only entities within the current scene.
    sys::SceneChooserSys *mSceneChooserSys{ nullptr };
    /// System used for getting the entities which should be rendered.
    sys::SceneDrawSys *mSceneDrawSys{ nullptr };
    /// System used for passing dirty transforms to their scene-graphs.
    sys::SceneGraphDirtifierSys *mSceneGraphDirtifierSys{ nullptr };
    /// System used for entity manipulation.
    sys::EntityManipulatorSys *mEntityManipulatorSys{ nullptr };
protected:
    /**
     * Initialize scene management and ECS universe.
     * @param cfg Runtime configuration.
     * @param renderSubSystem Rendering sub-system used 
     * for rendering the scenes.
     */
    SceneSubSystem(const EngineRuntimeConfig &cfg, rndr::RenderSubSystem &renderSubSystem);
}; // class SceneSubSystem

} // namespace scene

} // namespace quark

// Template implementation.

namespace quark
{

namespace scene
{

template <typename SysT, typename ... CArgTs>
SysT *SceneSubSystem::addGetSystem(CArgTs... cArgs)
{ return mUniverse.addSystem<SysT>(std::forward<CArgTs>(cArgs)...); }

template<typename... CompTs>
EntityHandle SceneSubSystem::duplicateSceneEntity(const EntityHandle &sourceEntity)
{ 
    // TODO - Set name similar to the input entity?
    auto newEntity{ mCurrentScene->createEntity() };
    mEntityManipulatorSys->duplicate<comp::Scene, CompTs...>(sourceEntity, newEntity); 

    return newEntity;
}

}

}

// Template implementation end.
