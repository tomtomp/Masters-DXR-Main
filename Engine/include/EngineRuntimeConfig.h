/**
 * @file EngineRuntimeConfig.h
 * @author Tomas Polasek
 * @brief Runtime configuration of the engine.
 */

#pragma once

#include "EngineConfig.h"

#include "engine/util/OptionParser.h"

/// Namespace containing the engine code.
namespace quark
{

/// Runtime engine configuration, provided on engine initialization.
class EngineRuntimeConfig
{
    /// Compile-time configuration.
    using Config = EngineConfig;
public:
    /// Initialize with default settings and configure command line parser.
    EngineRuntimeConfig();

    /**
     * Get command line options and parse them. Options will be 
     * set accordingly.
     * Options will be fetched from command line gained 
     * by calling ::GetCommandLineW().
     * @throws util::OptionParser::ParsingException on error.
     */
    void setParseCmdArgs();

    /**
     * Set options according to provided argument vector.
     * @param argv Argument vector containing command line options.
     * @param argc Number of arguments in the argument vector.
     */
    void setParseCmdArgs(const wchar_t * const * const argv, int argc);

    // Start of options section

    /**
     * Use Windows Advanced Rendering Platform 
     * adapter, instead of physical one.
     */
    bool adapterUseWarp{ Config::RUNTIME_DEFAULT_ADAPTER_USE_WARP };

    /// Get application help message.
    std::wstring getHelp() const
    { return mParser.generateHelp(); }

    /// Command line parameters which were parsed.
    std::string commandLineParameters{ };

    /// Default window width for the main window.
    uint32_t windowWidth{ Config::RUNTIME_DEFAULT_WINDOW_WIDTH };
    /// Default window height for the main window.
    uint32_t windowHeight{ Config::RUNTIME_DEFAULT_WINDOW_HEIGHT };
    /// Main window title.
    std::wstring windowTitle{ Config::RUNTIME_DEFAULT_WINDOW_TITLE };

    /// Number of back buffers in the main swap chain - including the currently displayed one.
    uint32_t swapChainNumBackBuffers{ Config::RUNTIME_DEFAULT_SWAP_CHAIN_NUM_BACKBUFFERS };

    /// Which message categories should be disabled when debugging is enabled?
    std::set<::D3D12_MESSAGE_CATEGORY> deviceDisableMsgCategories{ 
        Config::RUNTIME_DEFAULT_DEVICE_DISABLE_MSG_CATEGORIES };
    /// Which message severities should be disabled when debugging is enabled?
    std::set<::D3D12_MESSAGE_SEVERITY> deviceDisableMsgSeverities{ 
        Config::RUNTIME_DEFAULT_DEVICE_DISABLE_MSG_SEVERITIES };
    /// Which message IDs should be disabled when debugging is enabled?
    std::set<::D3D12_MESSAGE_ID> deviceDisableMsgIds{ 
        Config::RUNTIME_DEFAULT_DEVICE_DISABLE_MSG_IDS };

    /// Should the triangles be filled or wire-frame?
    bool renderRasterizerFill{ Config::RUNTIME_DEFAULT_RENDER_RASTERIZER_FILL };
    /// Are front-facing triangles clock-wise, or counter clock-wise?
    bool renderRasterizerFrontClockwise{ Config::RUNTIME_DEFAULT_RENDER_RASTERIZER_FRONT_CLOCKWISE };
    /// Prefer fast acceleration structure build times, or ray tracing performance?
    bool renderRayTracingFastBuildAS{ Config::RUNTIME_DEFAULT_RENDER_RAY_TRACING_FAST_BUILD_AS };

    /// Target frames per second for the application.
    uint32_t appTargetFps{ Config::RUNTIME_DEFAULT_APPLICATION_TARGET_FPS };
    /// Target updates per second for the application.
    uint32_t appTargetUps{ Config::RUNTIME_DEFAULT_APPLICATION_TARGET_UPS };
    /// Should the main swap chain synchronize to vblank?
    bool appUseVSync{ Config::RUNTIME_DEFAULT_APPLICATION_USE_VSYNC };
    /// Should the main swap chain use tearing, if supported?
    bool appUseTearing{ Config::RUNTIME_DEFAULT_APPLICATION_USE_TEARING };

    /// Should the help be displayed?
    bool metaDisplayHelp{ Config::RUNTIME_DEFAULT_META_DISPLAY_HELP };

    /// Should the GUI be displayed when the application starts?
    bool guiEnable{ Config::RUNTIME_DEFAULT_GUI_ENABLE };
    /// Should user input on GUI be captured?
    bool guiCaptureInput{ Config::RUNTIME_DEFAULT_GUI_CAPTURE_INPUT };
    /// Width of the main GUI window in pixels.
    float guiWidth{ Config::RUNTIME_DEFAULT_GUI_WIDTH };

    /// Should the debugging layer be enabled?
    bool debugEnableLayer{ Config::RUNTIME_DEFAULT_DEBUG_ENABLE_LAYER };
    /// Should the GPU-based validation be enabled?
    bool debugGpuValidation{ Config::RUNTIME_DEFAULT_DEBUG_GPU_VALIDATION };
    /// Should live object be reported when shutting down the application?
    bool debugReportLiveObjects{ Config::RUNTIME_DEFAULT_DEBUG_REPORT_LIVE };

    /// Enabling GPU profiling allows the use of GPU timers for measurement.
    bool debugGpuProfiling{ Config::RUNTIME_DEFAULT_DEBUG_GPU_PROFILING };

    // End of options section
private:
protected:
    /// Parser used when parsing command line options.
    util::OptionParser mParser;
}; // class EngineRuntimeConfig

} // namespace quark

