/**
 * @file Quark.h
 * @author Tomas Polasek
 * @brief Helper header which can be used for including 
 * all necessary headers for the creation of application class.
 */

#pragma once

#include "Engine.h"
#include "engine/application/Application.h"
