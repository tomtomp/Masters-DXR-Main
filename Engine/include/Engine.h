/**
 * @file Engine.h
 * @author Tomas Polasek
 * @brief Main engine class, connects resource manager, renderer and application.
 */

#pragma once

#include "EngineRuntimeConfig.h"

// Resources: 
#include "engine/resources/win32/WindowClass.h"
#include "engine/resources/win32/Window.h"
#include "engine/resources/ThreadPool.h"

// Utilities: 
#include "engine/application/MessageHandler.h"
#include "engine/application/MessageBus.h"
#include "engine/application/Events.h"
#include "engine/application/WindowRunner.h"
#include "engine/application/IterativeApplication.h"
#include "engine/util/prof/Profiler.h"

// Sub-systems: 
#include "engine/renderer/RenderSubSystem.h"
#include "engine/scene/SceneSubSystem.h"
#include "engine/gui/GuiSubSystem.h"

#include "engine/lib/dxtk.h"

/// Namespace containing the engine code.
namespace quark
{

/// Main engine class, used for running an application.
class Engine
{
public:
    /**
     * Initialize engine and its subsystems.
     * @param cfg Runtime engine configuration.
     * @param hInstance Handle to main module.
     */
    Engine(const EngineRuntimeConfig &cfg, HINSTANCE hInstance);

    /// Destroy all subsystems and quit.
    ~Engine();

    /**
     * Run specified application in this engine.
     * @tparam AppT Application type.
     * @tparam ConfigT Runtime configuration type.
     * @param cfg Application configuration.
     * @return Returns code returned by the 
     * application run function.
     */
    template <typename AppT, typename ConfigT>
    int32_t run(ConfigT &cfg);

    /**
     * Prepare instance of given application for iterative 
     * running.
     * @tparam AppT Application type. The run() function should 
     * return after one iteration.
     * @tparam ConfigT Runtime configuration type used by the 
     * application.
     * @param cfg Application configuration.
     * @return Returns handle to the application, which can be 
     * further used with Engine methods.
     */
    template <typename AppT, typename ConfigT>
    typename app::IterativeApplication<AppT>::PtrT prepareIterativeRun(ConfigT &cfg);

    /**
     * Perform a single iterative run of provided application 
     * instance.
     * @tparam AppT Application type. The run() function should 
     * return after one iteration.
     * @param appPtr Pointer to the application instance.
     * @return Returns the same code as the application run().
     */
    template <typename AppT>
    int32_t performIterativeRun(typename app::IterativeApplication<AppT>::PtrT appPtr);

    /**
     * Destroy instance of iterative application and clean up.
     * @tparam AppT Application type. 
     * @param appPtr Pointer to the application instance.
     * @return Returns the same code as the application run().
     */
    template <typename AppT>
    void destroyIterativeRun(typename app::IterativeApplication<AppT>::PtrT appPtr);

    /// Get the main window.
    res::win::Window &window()
    { return *mWindow; }

    /// Get the main window.
    const res::win::Window &window() const
    { return *mWindow; }

    /// Get renderer sub-system.
    rndr::RenderSubSystem &renderer()
    { return *mRenderer; }

    /// Get renderer sub-system.
    const rndr::RenderSubSystem &renderer() const
    { return *mRenderer; }

    /// Get scene management sub-system.
    scene::SceneSubSystem &scene()
    { return *mSceneSubSystem; }

    /// Get scene management sub-system.
    const scene::SceneSubSystem &scene() const
    { return *mSceneSubSystem; }

    /// Get message handling sub-system.
    app::MessageHandler &msgHandler()
    { return *mMessageHandler; }

    /// Get message handling sub-system.
    const app::MessageHandler &msgHandler() const 
    { return *mMessageHandler; }

    /// Get gui management sub-system.
    gui::GuiSubSystem &gui()
    { return *mGuiSubSystem; }

    /// Get gui management sub-system.
    gui::GuiSubSystem &gui() const
    { return *mGuiSubSystem; }
private:
    /// Compile-time configuration.
    using Config = EngineConfig;

    /// Handle to the main module.
    HINSTANCE mHInstance;
    /// Main thread pool containing the worker threads.
    res::thread::ThreadPool::PtrT mThreadPool;
    /// Message handling sub-system.
    app::MessageHandler::PtrT mMessageHandler;
    /// Main window class registrator.
    res::win::WindowClass::PtrT mWindowClass;
    /// Main window.
    res::win::Window::PtrT mWindow;
    /// Message pump runner.
    app::WindowRunner::PtrT mWindowRunner;
    /// Rendering sub-system.
    rndr::RenderSubSystem::PtrT mRenderer;
    /// Scene management sub-system.
    scene::SceneSubSystem::PtrT mSceneSubSystem;
    /// Gui management sub-system.
    gui::GuiSubSystem::PtrT mGuiSubSystem;
protected:
}; // class Engine

} // namespace quark

// Template implementation

namespace quark
{

template <typename AppT, typename ConfigT>
int32_t Engine::run(ConfigT &cfg)
{
    //mWindow.setVisible(true);

    int32_t result{ 0 };

    AppT app(cfg, *this);

    app.initialize();
    {
        mWindowRunner->runMessagePump(*mMessageHandler);
        mWindow->sendVisibleMessage(true);
        result = app.run();
    }
    app.destroy();

    return result;
}

template <typename AppT, typename ConfigT>
typename app::IterativeApplication<AppT>::PtrT Engine::prepareIterativeRun(ConfigT &cfg)
{ 
    auto appPtr{ app::IterativeApplication<AppT>::template create(cfg, *this) };
    appPtr->initialize();

    mWindowRunner->runMessagePump(*mMessageHandler);
    mWindow->sendVisibleMessage(true);

    return appPtr;
}

template <typename AppT>
int32_t Engine::performIterativeRun(typename app::IterativeApplication<AppT>::PtrT appPtr)
{
    // TODO - Instead of message pump, do message processing here?
    // Run message pump until no messages remain.
    /*
    bool messageProcessed{ true };
    while (messageProcessed)
    { messageProcessed = mMessageHandler.processMessage(); }
    */

    // Perform a one run of application run function.
    return appPtr->runIteration();
}

template <typename AppT>
void Engine::destroyIterativeRun(typename app::IterativeApplication<AppT>::PtrT appPtr)
{
    mWindow->sendVisibleMessage(false);
    mWindowRunner->pauseMessagePump();

    appPtr->destroy();
}

}

// Template implementation end
