/**
 * @file EngineConfig.h
 * @author Tomas Polasek
 * @brief Engine configuration static settings can be changed 
 * in this file. The runtime config is provided by the user when 
 * initializing the engine.
 */

#pragma once

#include "engine/util/Types.h"
#include "engine/util/SafeD3D12.h"

/// Namespace containing the engine code.
namespace quark
{

/// Compile-time engine configuration, can be changed in this file.
class EngineConfig
{
public:
    /// Name of the main window class.
    static constexpr const wchar_t *WINDOW_CLASS_MAIN_NAME{ L"MainWindowClassName" };

    /// Required Direct3D feature level.
    static constexpr ::D3D_FEATURE_LEVEL ADAPTER_FEATURE_LEVEL{ ::D3D_FEATURE_LEVEL_11_0 };

    /// Debug name for the main device.
    static constexpr const wchar_t *DEVICE_DEBUG_NAME{ L"MainDevice" };

    /// Debug name for the main direct command queue.
    static constexpr const wchar_t *DIRECT_QUEUE_DEBUG_NAME{ L"MainDirectQueue" };
    /// Debug name for the main command bundle queue.
    static constexpr const wchar_t *BUNDLE_QUEUE_DEBUG_NAME{ L"MainBundleQueue" };
    /// Debug name for the main compute command queue.
    static constexpr const wchar_t *COMPUTE_QUEUE_DEBUG_NAME{ L"MainComputeQueue" };
    /// Debug name for the main copy command queue.
    static constexpr const wchar_t *COPY_QUEUE_DEBUG_NAME{ L"MainCopyQueue" };

    /// Number of back buffers in the default swap chain.
    static constexpr uint64_t SWAP_CHAIN_NUM_BUFFERS{ 3u };
    /// Usage specification for buffers in the main swap chain.
    static constexpr ::DXGI_USAGE SWAP_CHAIN_USAGE{ DXGI_USAGE_RENDER_TARGET_OUTPUT };
    /// Format of buffers in the main swap chain.
    static constexpr ::DXGI_FORMAT SWAP_CHAIN_FORMAT{ ::DXGI_FORMAT_R8G8B8A8_UNORM };
    /// Scaling behavior for buffers in the main swap chain.
    static constexpr ::DXGI_SCALING SWAP_CHAIN_SCALING{ ::DXGI_SCALING_STRETCH };
    /// Alpha value behavior for buffers in the main swap chain.
    static constexpr ::DXGI_ALPHA_MODE SWAP_CHAIN_ALPHA_MODE{ ::DXGI_ALPHA_MODE_UNSPECIFIED };

    /// What should be reported by the live object reporter.
    static inline const ::DXGI_DEBUG_ID LIVE_REPORTER_DEBUG_ID{ ::DXGI_DEBUG_ALL };
    /// How should the live objects be reported.
    static constexpr ::DXGI_DEBUG_RLO_FLAGS LIVE_REPORTER_FLAGS{ ::DXGI_DEBUG_RLO_ALL };

#ifdef ENGINE_RAY_TRACING_ENABLED
    /// Should ray tracing features be enabled?
    static constexpr bool RAY_TRACING_ENABLED{ true };
#else
    /// Should ray tracing features be enabled?
    static constexpr bool RAY_TRACING_ENABLED{ false };
#endif

    // Default values for the runtime configuration: 

    // TODO - MSVC bug, inline const not working with incomplete types?
    /// Default value for disabled debugging message categories.
    static const std::initializer_list<::D3D12_MESSAGE_CATEGORY>
        RUNTIME_DEFAULT_DEVICE_DISABLE_MSG_CATEGORIES;
    /// Default value for disabled debugging message severities.
    static const std::initializer_list<::D3D12_MESSAGE_SEVERITY>
        RUNTIME_DEFAULT_DEVICE_DISABLE_MSG_SEVERITIES;
    /// Default value for disabled debugging message IDs.
    static const std::initializer_list<::D3D12_MESSAGE_ID>
        RUNTIME_DEFAULT_DEVICE_DISABLE_MSG_IDS;

    /// Default value for filling the triangles.
    static constexpr bool RUNTIME_DEFAULT_RENDER_RASTERIZER_FILL{ true };
    /// Default value for triangle front facing order.
    static constexpr bool RUNTIME_DEFAULT_RENDER_RASTERIZER_FRONT_CLOCKWISE{ true };
    /// Default value for fast building ray tracing acceleration structures.
    static constexpr bool RUNTIME_DEFAULT_RENDER_RAY_TRACING_FAST_BUILD_AS{ false };

    /**
     * Maximal number of GPU profiling timers. this value essentially 
     * specifies how many different scopes can be profiled on the GPU.
     */
    static constexpr std::size_t GPU_PROFILING_MAX_TIMERS{ 32u };
    /// Name of the GPU "thread" within the profiling system.
    static constexpr const char *GPU_PROFILING_THREAD_NAME{ "GpuThread" };

    /// Default value for runtime option adapter use warp.
    static constexpr bool RUNTIME_DEFAULT_ADAPTER_USE_WARP{ false };
    /// Default value for runtime option window width.
    static constexpr uint32_t RUNTIME_DEFAULT_WINDOW_WIDTH{ 1024u };
    /// Default value for runtime option window height.
    static constexpr uint32_t RUNTIME_DEFAULT_WINDOW_HEIGHT{ 768u };
    /// Default value for runtime option window title.
    static constexpr const wchar_t *RUNTIME_DEFAULT_WINDOW_TITLE{ L"Direct3D App" };

    /// Default value for number of back buffers in the main swap chain.
    static constexpr uint32_t RUNTIME_DEFAULT_SWAP_CHAIN_NUM_BACKBUFFERS{ 3u };

    /// Default value for target frames per second for the application.
    static constexpr uint32_t RUNTIME_DEFAULT_APPLICATION_TARGET_FPS{ 60u };
    /// Default value for target updates per second for the application.
    static constexpr uint32_t RUNTIME_DEFAULT_APPLICATION_TARGET_UPS{ 60u };
    /// Default value for vsync settings.
    static constexpr bool RUNTIME_DEFAULT_APPLICATION_USE_VSYNC{ false };
    /// Default value for using tearing if supported.
    static constexpr bool RUNTIME_DEFAULT_APPLICATION_USE_TEARING{ false };

    /// Default value for displaying the help menu.
    static constexpr bool RUNTIME_DEFAULT_META_DISPLAY_HELP{ false };

    /// Should the GUI be displayed when the application starts?
    static constexpr bool RUNTIME_DEFAULT_GUI_ENABLE{ true };
    /// Should user input on GUI be captured?
    static constexpr bool RUNTIME_DEFAULT_GUI_CAPTURE_INPUT{ true };
    /// Width of the main GUI window in pixels.
    static constexpr float RUNTIME_DEFAULT_GUI_WIDTH{ 350.0f };

#ifdef _DEBUG
    /// Default value for enabling the debug layer.
    static constexpr bool RUNTIME_DEFAULT_DEBUG_ENABLE_LAYER{ true };
    /// Default value for enabling the GPU-based validation.
    static constexpr bool RUNTIME_DEFAULT_DEBUG_GPU_VALIDATION{ true };
    /// Default value for enabling the live object reporting.
    static constexpr bool RUNTIME_DEFAULT_DEBUG_REPORT_LIVE{ true };
    /// Default value for enabling the GPU profiling.
    static constexpr bool RUNTIME_DEFAULT_DEBUG_GPU_PROFILING{ true };
#else
    /// Default value for enabling the debug layer.
    static constexpr bool RUNTIME_DEFAULT_DEBUG_ENABLE_LAYER{ false };
    /// Default value for enabling the GPU-based validation.
    static constexpr bool RUNTIME_DEFAULT_DEBUG_GPU_VALIDATION{ false };
    /// Default value for enabling the live object reporting.
    static constexpr bool RUNTIME_DEFAULT_DEBUG_REPORT_LIVE{ false };
    /// Default value for enabling the GPU profiling.
    static constexpr bool RUNTIME_DEFAULT_DEBUG_GPU_PROFILING{ true };
#endif
private:
protected:
public:
    EngineConfig() = delete;
    ~EngineConfig() = delete;
    EngineConfig(const EngineConfig &other) = delete;
    EngineConfig(EngineConfig &&other) = delete;
    EngineConfig &operator=(const EngineConfig &other) = delete;
    EngineConfig &operator=(EngineConfig &&other) = delete;
}; // class EngineConfig

} // namespace quark
