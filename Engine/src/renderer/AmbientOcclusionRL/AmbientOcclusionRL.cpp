/**
 * @file renderer/ambientOcclusionRL/AmbientOcclusionRL.cpp
 * @author Tomas Polasek
 * @brief Rendering layer used for generation of ambient 
 * occlusion buffers.
 */

#include "stdafx.h"

#include "engine/renderer/ambientOcclusionRL/AmbientOcclusionRL.h"

#include "engine/renderer/ambientOcclusionRL/shaders/AmbientOcclusionVS.h"
#include "engine/renderer/ambientOcclusionRL/shaders/AmbientOcclusionPS.h"

#include "engine/gui/GuiSubSystem.h"

namespace quark
{

namespace rndr
{

void AmbientOcclusionBuffers::transitionResourcesTo(
    res::d3d12::D3D12CommandList &cmdList, ::D3D12_RESOURCE_STATES state)
{
    if (ambientOcclusion)
    { ambientOcclusion->transitionTo(state, cmdList); }
}

void AmbientOcclusionBufferViews::createViews(const AmbientOcclusionBuffers &buffers, 
    res::d3d12::D3D12DescAllocatorInterface &descHeap, ResourceViewType type)
{
    if (!ambientOcclusion)
    { ambientOcclusion = helpers::d3d12::D3D12ResourceView::create(); }
    if (buffers.ambientOcclusion)
    { ambientOcclusion->reallocateDescriptor(buffers.ambientOcclusion, descHeap, type); }
}

AmbientOcclusionRL::AmbientOcclusionRL(RenderSubSystem &renderer) :
    mRenderer{ &renderer }
{ initializeResources(); }

util::PointerType<AmbientOcclusionRL>::PtrT AmbientOcclusionRL::create(RenderSubSystem &renderer)
{ return PtrT{ new AmbientOcclusionRL(renderer) }; }

AmbientOcclusionRL::~AmbientOcclusionRL()
{ /* Automatic */ }

void AmbientOcclusionRL::setDeferredBuffers(DeferredBuffers buffers)
{ mR.deferredBuffers = buffers; }

void AmbientOcclusionRL::resize(uint32_t width, uint32_t height)
{
    // Create the output textures.
    createResizeTextures(width, height);
}

void AmbientOcclusionRL::prepareGui(gui::GuiSubSystem &gui)
{
    gui.addGuiContext("/AmbientOcclusionRL/")
        .setType(gui::GuiContextType::Category)
        .setLabel("Ambient Occlusion Pass");

    gui.addGuiVariable<gui::InputInt>("AmbientOcclusionRlKernelSize", "/AmbientOcclusionRL/", { mC.kernelSize })
        .setLabel("Kernel Size")
        .cfg().setMin(1);

    gui.addGuiVariable<gui::InputBool>("AmbientOcclusionRlEnabled", "/AmbientOcclusionRL/", { mC.enabled })
        .setLabel("Rendering Enabled")
        .cfg().setEditable(false);

    gui.addGuiVariable<gui::Button>("AmbientOcclusionRlReloadShaders", "/AmbientOcclusionRL/", { [&] ()
    { mR.reloadShaders = true; } }).setLabel("Reload Shaders");
}

void AmbientOcclusionRL::render(const helpers::math::Camera &camera, 
    res::d3d12::D3D12CommandList &cmdList)
{
    // Skip if we didn't initialize the size.
    if (!mPrepared)
    { return; }

    // Check if shader reloading is required.
    checkReloadShaders();

    // Update other constants.
    updateCamera(camera);
    // Re-upload constant buffers.
    refreshConstantBuffers();

    // Create random textures, if not created yet.
    createNoiseTexture(mC.noiseSize, mC.noiseSize, cmdList);
    createKernelTexture(mC.kernelSize, mC.kernelSize, cmdList);

    // Fill table with descriptors.
    fillMiscTable(mR.miscTable, 
        *mR.staticConstantsBuffer, *mR.dynamicConstantsBuffer, 
        *mR.noiseTexture, *mR.kernelTexture);

    // Stop after creating the textures, if not enabled.
    if (!mC.enabled)
    { return; }

    GPU_PROF_SCOPE(cmdList, "AmbientOcclusionRLGpu");

    // Use the prepared root signature and pipeline state.
    cmdList->SetGraphicsRootSignature(mR.rootSig->get());
    cmdList->SetPipelineState(mR.pipelineState->get());

    // Using no vertex data, generated ad-hoc in vertex shader.
    cmdList->IASetPrimitiveTopology(DEFAULT_PRIMITIVE_TOPOLOGY);
    cmdList->IASetVertexBuffers(0u, 0u, nullptr);
    cmdList->IASetIndexBuffer(nullptr);

    // Setup viewport.
    cmdList->RSSetViewports(1u, &mR.viewport);
    cmdList->RSSetScissorRects(1u, &mR.scissorRect);

    // Return the buffers into the original state.
    mR.ambientOcclusionBuffers.transitionResourcesTo(cmdList, ::D3D12_RESOURCE_STATE_RENDER_TARGET);

    // We are going to read the G-Buffers from compute shaders.
    mR.deferredViews.createViews(mR.deferredBuffers, mR.deferredTable, ResourceViewType::ShaderResource);
    mR.deferredBuffers.transitionResourcesTo(cmdList, ::D3D12_RESOURCE_STATE_NON_PIXEL_SHADER_RESOURCE | ::D3D12_RESOURCE_STATE_PIXEL_SHADER_RESOURCE);

    const ::D3D12_CPU_DESCRIPTOR_HANDLE rtv[]{ mR.ambientOcclusionBuffers.ambientOcclusion->cpuHandle() };
    cmdList->OMSetRenderTargets(util::count<::UINT>(rtv), rtv, false, nullptr);

    // Descriptor heap containing all of the descriptors used.
    ::ID3D12DescriptorHeap *descriptorHeaps[] = { mR.descHeap->get() };
    cmdList->SetDescriptorHeaps(util::count<::UINT>(descriptorHeaps), descriptorHeaps);

    // Set the slow-changing constants.
    cmdList->SetGraphicsRootConstantBufferView(
        compatAmbientOcclusion::GlobalRS::StaticConstantBuffer, 
        mR.staticConstantsBuffer->gpuAddress());

    // Set the fast-changing constants.
    cmdList->SetGraphicsRootConstantBufferView(
        compatAmbientOcclusion::GlobalRS::DynamicConstantBuffer, 
        mR.dynamicConstantsBuffer->gpuAddress());

    // Array of G-Buffer textures.
    cmdList->SetGraphicsRootDescriptorTable(
        compatAmbientOcclusion::GlobalRS::DeferredBuffers, 
        mR.deferredTable.baseHandle());

    // Noise texture.
    cmdList->SetGraphicsRootDescriptorTable(
        compatAmbientOcclusion::GlobalRS::NoiseTexture, 
        mR.noiseTexture->gpuHandle());

    // Kernel texture.
    cmdList->SetGraphicsRootDescriptorTable(
        compatAmbientOcclusion::GlobalRS::KernelTexture, 
        mR.kernelTexture->gpuHandle());

    // Render 3 vertices for full-screen triangle -> quad.
    cmdList->DrawInstanced(3u, 1u, 0u, 0u);
}

void AmbientOcclusionRL::updateGui(gui::GuiSubSystem &gui)
{
    auto &kernelSizeVar{ gui.getVariable<gui::InputInt>("AmbientOcclusionRlKernelSize") };
    gui::synchronizeVariables(kernelSizeVar, mC.kernelSize);
    mC.noiseSize = mC.kernelSize;

    auto &enabledVar{ gui.getVariable<gui::InputBool>("AmbientOcclusionRlEnabled") };
    enabledVar.value = mC.enabled;
}

AmbientOcclusionBuffers AmbientOcclusionRL::getAmbientOcclusionBuffers()
{ return mR.ambientOcclusionBuffers; }

void AmbientOcclusionRL::setEnabled(bool enabled)
{ mC.enabled = enabled; }

res::d3d12::D3D12DescHeap::PtrT AmbientOcclusionRL::createDescHeap(std::size_t requiredDescriptors)
{
    return res::d3d12::D3D12DescHeap::create(mRenderer->device(), 
        ::D3D12_DESCRIPTOR_HEAP_TYPE_CBV_SRV_UAV,
        static_cast<::uint32_t>(requiredDescriptors),
        ::D3D12_DESCRIPTOR_HEAP_FLAG_SHADER_VISIBLE);
}

res::d3d12::D3D12DescHeap::PtrT AmbientOcclusionRL::createRtvDescHeap(std::size_t requiredDescriptors)
{
    return res::d3d12::D3D12DescHeap::create(mRenderer->device(), 
        ::D3D12_DESCRIPTOR_HEAP_TYPE_RTV,
        static_cast<::uint32_t>(requiredDescriptors),
        ::D3D12_DESCRIPTOR_HEAP_FLAG_NONE);
}

void AmbientOcclusionRL::createResizeTextures(uint32_t width, uint32_t height)
{
    mR.scissorRect = ::CD3DX12_RECT(
        0u, 0u, 
        width, height);
    mR.viewport = ::CD3DX12_VIEWPORT{ 
        0.0f, 0.0f, 
        static_cast<float>(width), static_cast<float>(height) };

    auto alloc{ helpers::d3d12::D3D12CommittedAllocator::create(
        mRenderer->device(),
        ::CD3DX12_HEAP_PROPERTIES(::D3D12_HEAP_TYPE_DEFAULT),
        ::D3D12_HEAP_FLAG_NONE) };

    auto textureDesc{ ::CD3DX12_RESOURCE_DESC::Tex2D(::DXGI_FORMAT_UNKNOWN, width, height) };
    textureDesc.Flags = ::D3D12_RESOURCE_FLAG_ALLOW_RENDER_TARGET;

    if (!mR.ambientOcclusionBuffers.ambientOcclusion)
    { mR.ambientOcclusionBuffers.ambientOcclusion = helpers::d3d12::D3D12TextureBuffer::create(); }
    textureDesc.Format = DEFAULT_AMBIENT_OCCLUSION_RT_FORMAT;
    mR.ambientOcclusionBuffers.ambientOcclusion->allocate(alloc, textureDesc, 
        ::D3D12_RESOURCE_STATE_RENDER_TARGET, nullptr, L"AmbientOcclusionOutput");
    mR.ambientOcclusionBuffers.ambientOcclusion->reallocateDescriptor(
        *mR.rtvDescHeap, ResourceViewType::RenderTarget);

    mC.width = static_cast<float>(width);
    mC.height = static_cast<float>(height);
}

void AmbientOcclusionRL::createConstantBuffers()
{
    mR.staticConstantsBuffer = StaticConstantsBuffer::create();
    mR.staticConstantsBuffer->at(0u) = mR.staticConstants;
    mR.staticConstantsBuffer->reallocate(mRenderer->device(), L"StaticConstantsBuffer");

    mR.dynamicConstantsBuffer = DynamicConstantsBuffer::create();
    mR.dynamicConstants.canvasResolution = dxtk::math::Vector2{
        mC.width, mC.height };
    mR.dynamicConstants.noiseResolution = dxtk::math::Vector2{
        static_cast<float>(mC.noiseSize), static_cast<float>(mC.noiseSize) };
    mR.dynamicConstants.kernelResolution = dxtk::math::Vector2{
        static_cast<float>(mC.kernelSize), static_cast<float>(mC.kernelSize) };
    mR.dynamicConstantsBuffer->at(0u) = mR.dynamicConstants;
    mR.dynamicConstantsBuffer->reallocate(mRenderer->device(), L"DynamicConstantsBuffer");
}

void AmbientOcclusionRL::createNoiseTexture(uint32_t width, uint32_t height, 
    res::d3d12::D3D12CommandList &directCmdList)
{
    // Perform only once.
    if (mR.noiseTexture &&
        mR.noiseTexture->desc().Width == width && 
        mR.noiseTexture->desc().Height == height)
    { return; }

    // float3 -> 3 components.
    static constexpr std::size_t COMPONENTS_PER_ELEMENT{ 3u };

    // Initialize the random number generation.
    std::random_device randomDev;
    std::mt19937 randomEngine(randomDev());
    std::uniform_real_distribution<float> randomDist(-1.0f, 1.0f);

    // Initialize holder of texture data.
    std::vector<float> textureData;
    const auto totalElements{ width * height};
    textureData.resize(totalElements * COMPONENTS_PER_ELEMENT );

    // Initialize random texture values.
    for (auto &val : textureData)
    { val = randomDist(randomEngine); }

    // Upload the texture to the GPU.
    if (!mR.noiseTexture)
    { mR.noiseTexture = helpers::d3d12::D3D12TextureBuffer::create(); }
    mR.noiseTexture->createFromData(
        textureData.begin(), textureData.end(), 
        rndr::ElementFormat{
            rndr::ComponentFormat::Float32, 
            COMPONENTS_PER_ELEMENT 
        }, 
        width, height,
        directCmdList, 
        mRenderer->device(), 
        mRenderer->memoryMgr().updater(), 
        L"NoiseTexture");
    mR.noiseTexture->transitionTo(::D3D12_RESOURCE_STATE_NON_PIXEL_SHADER_RESOURCE | ::D3D12_RESOURCE_STATE_PIXEL_SHADER_RESOURCE, directCmdList);
}

void AmbientOcclusionRL::createKernelTexture(uint32_t width, uint32_t height,
    res::d3d12::D3D12CommandList &directCmdList)
{
    // Perform only once.
    if (mR.kernelTexture && 
        mR.kernelTexture->desc().Width == width && 
        mR.kernelTexture->desc().Height == height)
    { return; }

    // float3 -> 3 components.
    static constexpr std::size_t COMPONENTS_PER_ELEMENT{ 3u };

    // Initialize the random number generation.
    std::random_device randomDev;
    std::mt19937 randomEngine(randomDev());
    std::uniform_real_distribution<float> randomDist1(-0.5f, 0.5f);
    std::uniform_real_distribution<float> randomDist2(0.3f, 1.0f);

    // Initialize holder of texture data.
    std::vector<float> textureData;
    textureData.resize(width * height * COMPONENTS_PER_ELEMENT);

    // Initialize random texture values.
    for (uint32_t iii = 0u; iii < width * height; ++iii)
    {
        dxtk::math::Vector3 dir{
            randomDist1(randomEngine),
            randomDist1(randomEngine),
            randomDist2(randomEngine) };
        dir.Normalize();

        textureData[iii * COMPONENTS_PER_ELEMENT + 0u] = dir.x;
        textureData[iii * COMPONENTS_PER_ELEMENT + 1u] = dir.y;
        textureData[iii * COMPONENTS_PER_ELEMENT + 2u] = dir.z;
    }

    // Upload the texture to the GPU.
    if (!mR.kernelTexture)
    { mR.kernelTexture = helpers::d3d12::D3D12TextureBuffer::create(); }
    mR.kernelTexture->createFromData(
        textureData.begin(), textureData.end(), 
        rndr::ElementFormat{
            rndr::ComponentFormat::Float32, 
            COMPONENTS_PER_ELEMENT 
        }, 
        width, height,
        directCmdList, 
        mRenderer->device(), 
        mRenderer->memoryMgr().updater(), 
        L"KernelTexture");
    mR.kernelTexture->transitionTo(::D3D12_RESOURCE_STATE_NON_PIXEL_SHADER_RESOURCE | ::D3D12_RESOURCE_STATE_PIXEL_SHADER_RESOURCE, directCmdList);
}

void AmbientOcclusionRL::initializeResources()
{
    PROF_SCOPE("AmbientOcclusionRLInitialization");

    // Prepare root signature.
    quark::helpers::d3d12::D3D12RootSignatureBuilder rsb;

    // Static, slow-changing constants, register(b0, space0).
    rsb[compatAmbientOcclusion::GlobalRS::StaticConstantBuffer].asConstantBufferViewParameter(
        compatAmbientOcclusion::StaticConstantsSlot.reg, compatAmbientOcclusion::StaticConstantsSlot.space);
    // Dynamic, fast-changing constants, register(b1, space0).
    rsb[compatAmbientOcclusion::GlobalRS::DynamicConstantBuffer].asConstantBufferViewParameter(
        compatAmbientOcclusion::DynamicConstantsSlot.reg, compatAmbientOcclusion::DynamicConstantsSlot.space);

    // Noise texture used for random values, register(t0, space1).
    const ::CD3DX12_DESCRIPTOR_RANGE1 noiseTextureRange[]{
        { // Texture array SRVs: 
            ::D3D12_DESCRIPTOR_RANGE_TYPE_SRV, 
            1u, 
            // register(t0, space0)
            compatAmbientOcclusion::NoiseSlot.reg, compatAmbientOcclusion::NoiseSlot.space,
            ::D3D12_DESCRIPTOR_RANGE_FLAG_DATA_STATIC
        }
    };
    rsb[compatAmbientOcclusion::GlobalRS::NoiseTexture].asDescriptorTableParameter(
        util::count<::UINT>(noiseTextureRange), noiseTextureRange);

    // Kernel texture, register(t0, space1).
    const ::CD3DX12_DESCRIPTOR_RANGE1 kernelTextureRange[]{
        { // Texture array SRVs: 
            ::D3D12_DESCRIPTOR_RANGE_TYPE_SRV, 
            1u, 
            // register(t1, space0)
            compatAmbientOcclusion::KernelSlot.reg, compatAmbientOcclusion::KernelSlot.space,
            ::D3D12_DESCRIPTOR_RANGE_FLAG_DATA_STATIC
        }
    };
    rsb[compatAmbientOcclusion::GlobalRS::KernelTexture].asDescriptorTableParameter(
        util::count<::UINT>(kernelTextureRange), kernelTextureRange);

    // G-Buffer textures, generated by the deferred pass, starting at register(t0, space1).
    const ::CD3DX12_DESCRIPTOR_RANGE1 deferredTextureRange[]{
        { // Texture array SRVs: 
            ::D3D12_DESCRIPTOR_RANGE_TYPE_SRV, 
            DeferredBufferViews::DEFERRED_BUFFER_COUNT, 
            // register(t0, space1)
            compatAmbientOcclusion::DeferredBuffersSlot.reg, compatAmbientOcclusion::DeferredBuffersSlot.space, 
            ::D3D12_DESCRIPTOR_RANGE_FLAG_DATA_STATIC
        }
    };
    rsb[compatAmbientOcclusion::GlobalRS::DeferredBuffers].asDescriptorTableParameter(
        util::count<::UINT>(deferredTextureRange), deferredTextureRange);

    // Common sampler used for sampling material textures, register(s0, space0).
    const ::CD3DX12_STATIC_SAMPLER_DESC commonSampler{ 
        compatAmbientOcclusion::SamplerSlot.reg, 
        //::D3D12_FILTER_MIN_MAG_MIP_POINT, 
        ::D3D12_FILTER_MIN_MAG_MIP_LINEAR, 
        ::D3D12_TEXTURE_ADDRESS_MODE_BORDER, 
        ::D3D12_TEXTURE_ADDRESS_MODE_BORDER, 
        ::D3D12_TEXTURE_ADDRESS_MODE_BORDER, 
        0.0f, 16u,
        ::D3D12_COMPARISON_FUNC_LESS_EQUAL,
        ::D3D12_STATIC_BORDER_COLOR_OPAQUE_WHITE,
        0.0f, D3D12_FLOAT32_MAX,
        ::D3D12_SHADER_VISIBILITY_ALL,
        compatAmbientOcclusion::SamplerSlot.space
    };
    rsb.addStaticSampler(commonSampler);

    mR.rootSig = rsb.build(mRenderer->device());

    // Load resolve shaders.
    mR.vShader = res::d3d12::D3D12Shader::create(gAmbientOcclusionVs, sizeof(gAmbientOcclusionVs));
    mR.pShader = res::d3d12::D3D12Shader::create(gAmbientOcclusionPs, sizeof(gAmbientOcclusionPs));

    // Build the pipeline state.
    buildPipeline();

    // Create the descriptor heap and tables.
    createDescHeapLayout();

    // Prepare render target.
    mR.rtvDescHeap = createRtvDescHeap(AmbientOcclusionBuffers::BUFFER_COUNT);

    // Create output texture used for storing ray tracing output.
    createResizeTextures();

    // Create static and dynamic constant buffers.
    createConstantBuffers();

    mPrepared = true;
}

void AmbientOcclusionRL::buildPipeline()
{
    quark::helpers::d3d12::D3D12PipelineStateBuilder psb;
    quark::helpers::d3d12::D3D12PipelineStateSOBuilder pssob;
    psb <<
        mR.rootSig->rsState() <<
        pssob.primitiveTopology(DEFAULT_PRIMITIVE_TOPOLOGY_TYPE) <<
        mR.vShader->vsState() <<
        mR.pShader->psState() <<
        pssob.renderTargetFormats({ DEFAULT_AMBIENT_OCCLUSION_RT_FORMAT });
    mR.pipelineState = psb.build(mRenderer->device());
}

uint32_t AmbientOcclusionRL::calculateMiscDescriptorCount()
{
    auto result{ 0u };

    // Output buffer: 
    result += 1u;
    // Slow-changing constants buffer: 
    result += 1u;
    // Fast-changing constants buffer: 
    result += 1u;
    // Random noise texture: 
    result += 1u;
    // Kernel texture: 
    result += 1u;

    return result;
}

helpers::d3d12::D3D12DescTable AmbientOcclusionRL::createMiscTable(
    res::d3d12::D3D12DescHeap &descHeap, uint32_t numDescriptors)
{
    return helpers::d3d12::D3D12DescTable(
        descHeap, descHeap.allocateTable(numDescriptors));
}

uint32_t AmbientOcclusionRL::calculateDeferredDescriptorCount()
{ return DeferredBufferViews::DEFERRED_BUFFER_COUNT; }

helpers::d3d12::D3D12DescTable AmbientOcclusionRL::createDeferredTable(
    res::d3d12::D3D12DescHeap &descHeap, uint32_t numDescriptors)
{
    return helpers::d3d12::D3D12DescTable(
        descHeap, descHeap.allocateTable(numDescriptors));
}

void AmbientOcclusionRL::createDescHeapLayout()
{
    // Calculate descriptor requirements.
    const auto miscDescriptors{ calculateMiscDescriptorCount() };
    const auto deferredDescriptors{ calculateDeferredDescriptorCount() };
    const auto totalDescriptors{ 
        miscDescriptors + deferredDescriptors };

    // Check whether we need to create a new descriptor heap.
    bool newDescHeapRequired{ false };
    // TODO - Fix on GTX 970.
    //newDescHeapRequired |= true;
    newDescHeapRequired |= miscDescriptors != mR.miscTable.size();
    newDescHeapRequired |= deferredDescriptors != mR.deferredTable.size();

    if (!newDescHeapRequired)
    { // No changed -> we don't need a new descriptor heap.
        return;
    }

    // Create the main descriptor heap, which will be used for all descriptors.
    auto newDescHeap{ createDescHeap(totalDescriptors) };

    // Create descriptor table for miscellaneous descriptors.
    auto miscTable{ createMiscTable(*newDescHeap, miscDescriptors) };
    // Create descriptor table for deferred descriptors.
    auto deferredTable{ createMiscTable(*newDescHeap, deferredDescriptors) };

    // Change over to the new descriptor heap.
    mR.descHeap = newDescHeap;
    mR.miscTable = std::move(miscTable);
    mR.deferredTable = std::move(deferredTable);
}

void AmbientOcclusionRL::fillMiscTable(
    helpers::d3d12::D3D12DescTable &miscTable, 
    StaticConstantsBuffer &staticConstants,
    DynamicConstantsBuffer &dynamicConstants,
    helpers::d3d12::D3D12TextureBuffer &noiseTexture, 
    helpers::d3d12::D3D12TextureBuffer &kernelTexture)
{
    staticConstants.reallocateDescriptor(miscTable);
    dynamicConstants.reallocateDescriptor(miscTable);
    noiseTexture.reallocateDescriptor(miscTable);
    kernelTexture.reallocateDescriptor(miscTable);
}

void AmbientOcclusionRL::updateCamera(const helpers::math::Camera &camera)
{
    //mR.dynamicConstants.cameraToWorld = camera.vpNoRecalculation().Invert();
    //mR.dynamicConstants.cameraToWorld = camera.rayTracingVP();
    mR.dynamicConstants.worldToCamera = camera.vpNoRecalculation();
    mR.dynamicConstants.cameraWorldPosition = camera.position();
    mR.dynamicConstants.canvasResolution = dxtk::math::Vector2{
        mC.width, mC.height };
    mR.dynamicConstants.noiseResolution = dxtk::math::Vector2{
        static_cast<float>(mC.noiseSize), static_cast<float>(mC.noiseSize) };
    mR.dynamicConstants.kernelResolution = dxtk::math::Vector2{
        static_cast<float>(mC.kernelSize), static_cast<float>(mC.kernelSize) };

    mR.dynamicConstantsChanged = true;
}

void AmbientOcclusionRL::refreshConstantBuffers()
{
    if (mR.staticConstantsChanged)
    {
        mR.staticConstantsBuffer->at(0u) = mR.staticConstants;
        mR.staticConstantsBuffer->uploadToGPU();
        mR.staticConstantsChanged = false;
    }

    if (mR.dynamicConstantsChanged)
    {
        mR.dynamicConstantsBuffer->at(0u) = mR.dynamicConstants;
        mR.dynamicConstantsBuffer->uploadToGPU();
        mR.dynamicConstantsChanged = false;
    }
}

void AmbientOcclusionRL::checkReloadShaders()
{
    if (!mR.reloadShaders)
    { return; }

    log<Info>() << "Attempting to reload Ambient Occlusion shaders!" << std::endl;

    // Load shaders.
    res::d3d12::D3D12Shader::PtrT vShader{ };
    res::d3d12::D3D12Shader::PtrT pShader{ };
    try
    {
        vShader = res::d3d12::D3D12Shader::create(L"AmbientOcclusionVS.cso");
        pShader = res::d3d12::D3D12Shader::create(L"AmbientOcclusionPS.cso");
    }
    catch (const std::exception &err)
    {
        log<Error>() << "Failed to reload Ambient Occlusion shaders! : " << err.what() << std::endl;
        mR.reloadShaders = false;
        return;
    }
    catch (...)
    {
        log<Error>() << "Failed to reload Ambient Occlusion shaders!" << std::endl;
        mR.reloadShaders = false;
        return;
    }

    mR.vShader = vShader;
    mR.pShader = pShader;

    // Build the pipeline state.
    buildPipeline();

    // Done...
    mR.reloadShaders = false;
}

}

}
