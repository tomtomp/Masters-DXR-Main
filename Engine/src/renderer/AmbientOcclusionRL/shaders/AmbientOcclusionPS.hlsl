/**
 * @file renderer/ambientOcclusionRL/shaders/AmbientOcclusionPS.hlsl
 * @author Tomas Polasek
 * @brief Vertex shader used in ambient occlusion generation.
 */

#ifndef ENGINE_AMBIENT_OCCLUSION_RL_RESOLVE_PS_HLSL
#define ENGINE_AMBIENT_OCCLUSION_RL_RESOLVE_PS_HLSL

#include "AmbientOcclusionShadersCompat.h"

/**
 * Create a random direction vector in a hemisphere above 
 * provided normal.
 * @param normal Normal pointing in the direction of the 
 * hemisphere.
 * @param rnd Random value which is used to generate the 
 * direction <0.0f, 1.0f>.
 */
float3 lambertNoTangent(in float3 normal, in float2 rnd)
{
    // Source: http://www.amietia.com/lambertnotangent.html

    float2 uv = rnd;
    float theta = HLSL_2PI * uv.x;
    uv.y = 2.0f * uv.y - 1.0f;
    float3 spherePoint = float3(sqrt(1.0f - uv.y * uv.y) * float2(cos(theta), sin(theta)), uv.y);
    return normalize(normal + spherePoint);
}

/**
 * Calculate ambient occlusion for each fragment in 
 * the input deferred G-buffers.
 */
PixelData main(FragmentData input)
{
    PixelData output;

    DeferredInformation deferredInfo = prepareDeferredInformation(input.texCoord, gDynamic.cameraWorldPosition);

    // Implementation based on: https://john-chapman-graphics.blogspot.com/2013/01/ssao-tutorial.html

    // Inputs: 
    const float2 ssTexCoord = deferredInfo.ssTexCoord;
    const float3 wsNormal = deferredInfo.wsNormal;
    const float3 wsPosition = deferredInfo.wsPosition;
    const float3 wsCameraPosition = gDynamic.cameraWorldPosition;
    // Radius of the ambient occlusion kernel.
    const float radius = 0.05f;

    const float2 inputDimension = gDynamic.canvasResolution;
    //const float2 noiseDimension = gDynamic.noiseResolution;
    //const float2 kernelDimension = gDynamic.kernelResolution;
    const float2 noiseDimension = textureDimensions(gNoiseTexture);
    const float2 kernelDimension = textureDimensions(gKernelTexture);

    // Get a 3D pseudo-random vector, tiled over the screen.
    const float2 noiseTexCoordMult = inputDimension / noiseDimension;
    const float3 randVec = normalize(gNoiseTexture.Sample(gCommonSampler, ssTexCoord * noiseTexCoordMult).xyz);
    //const float3 randSpaceVec = randVec * 2.0f - 1.0f;
    const float3 randSpaceVec = randVec;

    // Calculate a pseudo-random tangent and bitangent for provided normal.
    const float3 wsTangent = normalize(randSpaceVec - wsNormal * dot(randSpaceVec, wsNormal));
    const float3 wsBitangent = cross(wsNormal, wsTangent);

    // Create transform matrix which will orient hemisphere by the provided normal.
    float3x3 sphereTransform = {
        wsTangent, 
        wsBitangent, 
        wsNormal, 
    };

    const float kernelElementCount = kernelDimension.x * kernelDimension.y;

    float ambientOcclusion = 0.0f;
    for (int yyy = 0; yyy < kernelDimension.y; ++yyy)
    {
        for (int xxx = 0; xxx < kernelDimension.x; ++xxx)
        {
            // Recover the sampling position from the kernel
            /*
            const int3 kernelPos = int3(xxx, yyy, 0);
            float3 wsKernelSample = gKernelTexture.Load(kernelPos);
            // Transform it to the sampled point.
            wsKernelSample = normalize(mul(sphereTransform, wsKernelSample));
            //const float2 rayRand = saturate(gNoiseTexture.SampleLevel(gCommonSampler, ssTexCoord * (xxx + yyy + 1) * noiseTexCoordMult, 0.0f).xy);
            //float3 wsKernelSample = lambertNoTangent(wsNormal, rayRand);
            wsKernelSample = wsKernelSample * radius + wsPosition;
            */

            const float currentElement = yyy * kernelDimension.x + xxx;
            const float randRadius = saturate(gNoiseTexture.SampleLevel(gCommonSampler, ssTexCoord * (currentElement + 1u) * noiseTexCoordMult, 0.0f).x);
            const float3 rayRand = hemisphereDirFibonacci(currentElement, kernelElementCount, radius);
            const float3 wsKernelSample = mul(rayRand, sphereTransform) * radius * randRadius + wsPosition;

            // Re-project to screen-space.
            const float4 ssSample = mul(gDynamic.worldToCamera, float4(wsKernelSample, 1.0f));
            // Perspective division and <-1.0f, 1.0f> -> <0.0f, 1.0f>, while flipping y axis.
            const float2 ssUv = ((ssSample.xy / ssSample.w + float2(1.0f, -1.0f)) * float2(0.5f, -0.5f));

            // Sample depth at that position.
            const float sampleDepth = gDepthTexture.Sample(gCommonSampler, ssUv).x;
            // Calculate depth of the position.
            const float realDepth = ssSample.z / ssSample.w;

            const float depthDiff = realDepth - sampleDepth;

            //const float rangeCheck = length(wsKernelSample - wsPosition) < radius ? 1.0f : 0.0f;
            const float rangeCheck = 1.0f - step(radius, depthDiff);
            const float occlusion = step(0.0f, depthDiff);
            const float attenuation = 1.0f / (1.0f + randRadius);
            //const float rangeCheck = 1.0f;
            //ambientOcclusion += ((sampleDepth <= realDepth) ? 1.0f : 0.0f) * rangeCheck;
            //ambientOcclusion += step(0.0f, depthDiff) * rangeCheck;
            //ambientOcclusion += smoothstep(0.0f, radius, depthDiff) * rangeCheck;
            ambientOcclusion +=  occlusion * attenuation * rangeCheck;
        }
    }
    ambientOcclusion /= (kernelElementCount);

    output.ambientOcclusion = saturate(ambientOcclusion);

    if (!deferredInfo.hasData)
    {
        output.ambientOcclusion = 0.0f;
        return output;
    }

    return output;
}

#endif // ENGINE_AMBIENT_OCCLUSION_RL_RESOLVE_PS_HLSL
