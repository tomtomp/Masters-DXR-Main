/**
 * @file renderer/ambientOcclusionRL/shaders/AmbientOcclusionVS.hlsl
 * @author Tomas Polasek
 * @brief Vertex shader used in ambient occlusion generation.
 */

#ifndef ENGINE_AMBIENT_OCCLUSION_RL_RESOLVE_VS_HLSL
#define ENGINE_AMBIENT_OCCLUSION_RL_RESOLVE_VS_HLSL

#include "AmbientOcclusionShadersCompat.h"

/**
 * Generate a fullscreen triangle, with texture coordinates.
 * Source: Vertex Shader Tricks by Bill Bilodeau - AMD at GDC14
 */
FragmentData main(VertexData input)
{
    FragmentData output;

    output.ssPosition = float4(
        // x = <-1.0f, 3.0f>
        ((float)(input.id / 2)) * 4.0f - 1.0f, 
        // y = <-1.0f, 3.0f>
        ((float)(input.id % 2)) * 4.0f - 1.0f, 
        0.0f, 1.0f
    );

    output.texCoord = float2(
        // u = <0.0f, 2.0f> -> <0.0f, 1.0f> in quad.
        ((float)(input.id / 2)) * 2.0f, 
        // v = <0.0f, 1.0f> -> <0.0f, 1.0f> in quad.
        1.0f - ((float)(input.id % 2)) * 2.0f
    );

    return output;
}

#endif // ENGINE_AMBIENT_OCCLUSION_RL_RESOLVE_VS_HLSL
