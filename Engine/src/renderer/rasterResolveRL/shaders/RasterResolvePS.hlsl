/**
 * @file renderer/rasterResolveRl/shaders/RasterResolvePS.hlsl
 * @author Tomas Polasek
 * @brief Pixel shader used in resolving the final image.
 */

#ifndef ENGINE_RASTER_RESOLVE_RL_RESOLVE_PS_HLSL
#define ENGINE_RASTER_RESOLVE_RL_RESOLVE_PS_HLSL

#include "RasterResolveShadersCompat.h"

/**
 * Calculate the final color for current fragment.
 * @param shadingInfo Filled structure with shading information.
 * @return Returns the final color.
 */
float4 calcMissColor(in ShadingInformation shadingInfo)
{
    return pbrMissColor(shadingInfo.rayDirection, shadingInfo.backgroundColor, 
        shadingInfo.lightColor, shadingInfo.lightDirection);
}

/**
 * Calculate the final color for current ray.
 * @param deferredInfo Filled structure with deferred information.
 * @param shadingInfo Filled structure with shading information.
 * @return Returns the final color.
 */
float4 calcFinalColor(in DeferredInformation deferredInfo, in ShadingInformation shadingInfo)
{
    return pbrHitColor(shadingInfo.rayDirection, deferredInfo.wsNormal,
        deferredInfo.metallicRoughness.r, deferredInfo.metallicRoughness.g, deferredInfo.diffuse,
        shadingInfo.ambientOcclusion, shadingInfo.shadowOcclusion,
        shadingInfo.lightDirection, shadingInfo.lightColor, shadingInfo.reflectionColor);
}

/**
 * Generate the requested debug output.
 * @param mode Debugging mode.
 * @param deferredInfo Filled deferred information structure.
 * @param shadingInfo Filled shading information structure.
 */
float4 generateDebugOutput(uint mode, 
    DeferredInformation deferredInfo, 
    ShadingInformation shadingInfo)
{
    switch (mode)
    {
        case HLSL_RASTERIZATION_MODE_SHADOW_MAP:
        { 
            const float near = 0.1f;
            const float far = 50.0f;
            const float depth = gShadowMaps[0].Sample(gCommonSampler, deferredInfo.ssTexCoord);
            const float linearDepth = (2.0f * near) / (far + near - depth * (far - near));
            const float3 depthColor = float3(linearDepth, linearDepth, linearDepth);
            if (depth == 0.0f)
            { return float4(1.0f, 1.0f, 1.0f, 1.0f); }
            else
            { return float4(depthColor, 1.0f); }
        }
        case HLSL_RASTERIZATION_MODE_SHADOW:
        { 
            const float3 dimmedColor = deferredInfo.diffuse.rgb * 0.5f;
            const float3 intenseColor = saturate(deferredInfo.diffuse.rgb * 3.0f);
            return float4(lerp(dimmedColor, intenseColor, (1.0f - shadingInfo.shadowOcclusion)), 1.0f); 
        }
        case HLSL_RASTERIZATION_MODE_OCCLUSION:
        { 
            const float ao = 1.0f - shadingInfo.ambientOcclusion;
            return float4(ao, ao, ao, 1.0f);
            const float3 dimmedColor = deferredInfo.diffuse.rgb * 0.5f;
            const float3 intenseColor = saturate(deferredInfo.diffuse.rgb * 5.0f);
            return float4(lerp(dimmedColor, intenseColor, shadingInfo.ambientOcclusion), 1.0f); 
        }
        case HLSL_RASTERIZATION_MODE_DEFERRED_POSITION: 
        { return float4(abs(deferredInfo.wsPosition.xyz) / 15.0f, 1.0f); }
        case HLSL_RASTERIZATION_MODE_DEFERRED_NORMAL: 
        { return float4(abs(deferredInfo.wsNormal.xyz), 1.0f); }
        case HLSL_RASTERIZATION_MODE_DEFERRED_DIFFUSE: 
        { return float4(deferredInfo.diffuse.rgba); }
        case HLSL_RASTERIZATION_MODE_DEFERRED_MR: 
        { return float4(deferredInfo.metallicRoughness, 0.0f, 1.0f); }
        case HLSL_RASTERIZATION_MODE_DEFERRED_RR: 
        { 
            const float3 dimmedColor = deferredInfo.diffuse.rgb * 0.5f;
            const float3 intenseColor = saturate(dimmedColor + float3(deferredInfo.doReflections, deferredInfo.doRefractions, 0.0f));
            return float4(intenseColor, 1.0f); 
        }
        case HLSL_RASTERIZATION_MODE_DEFERRED_DEPTH:
        { 
            const float near = 0.1f;
            const float far = 50.0f;
            const float depth = deferredInfo.depth;
            const float linearDepth = (2.0f * near) / (far + near - depth * (far - near));
            const float3 depthColor = float3(linearDepth, linearDepth, linearDepth);
            if (depth == 0.0f)
            { return float4(1.0f, 1.0f, 1.0f, 1.0f); }
            else
            { return float4(depthColor, 1.0f); }
        }
        default:
        { return float4(1.0f, 0.0f, 1.0f, 1.0f); }
    }
}

/**
 * Resolve the final image.
 */
PixelData main(FragmentData input)
{
    PixelData output;

    DeferredInformation deferredInfo = prepareDeferredInformation(input.texCoord, gDynamic.cameraWorldPosition);
    ShadingInformation shadingInfo = prepareShadingInformation(deferredInfo);

    // Using the first light, which is the "sun".
    fillLightInformation(gStatic.enableShadows, 0u, deferredInfo, shadingInfo);
    // Calculate ambient occlusion in screen space.
    fillAmbientOcclusion(gStatic.enableAo, deferredInfo, shadingInfo);
    // Calculate screen-space reflections.
    fillReflection(gStatic.doReflections, deferredInfo, shadingInfo);

    if (gStatic.mode != HLSL_RASTERIZATION_MODE_FINAL_COLOR)
    { // For debug modes, just output the requested texture.
        output.color = generateDebugOutput(gStatic.mode, deferredInfo, shadingInfo);
        return output;
    }

    float4 finalColor = 0.0f;
    if (deferredInfo.hasData)
    { finalColor = calcFinalColor(deferredInfo, shadingInfo); }
    else
    { finalColor = calcMissColor(shadingInfo); }

    output.color = finalColor;
    return output;
}

#endif // ENGINE_RASTER_RESOLVE_RL_RESOLVE_PS_HLSL
