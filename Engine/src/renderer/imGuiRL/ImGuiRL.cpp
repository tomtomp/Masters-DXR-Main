/**
 * @file renderer/ImGuiRL.cpp
 * @author Tomas Polasek
 * @brief Rendering layer for drawing GUI using ImGUI.
 */

#include "stdafx.h"

#include "engine/renderer/imGuiRL/ImGuiRL.h"

#include "engine/renderer/RenderSubSystem.h"

#include "engine/lib/imgui.h"

/// Namespace containing the engine code.
namespace quark
{

/// Rendering classes.
namespace rndr
{

ImGuiRL::ImGuiRL(RenderSubSystem &renderer) :
    mRenderer{ renderer },
    mImGui(mRenderer.device(), mRenderer.swapChain())
{ }

util::PointerType<ImGuiRL>::PtrT ImGuiRL::create(RenderSubSystem &renderer)
{ return PtrT{ new ImGuiRL(renderer) }; }

ImGuiRL::~ImGuiRL()
{ /* Automatic */ }

void ImGuiRL::render(const ::CD3DX12_CPU_DESCRIPTOR_HANDLE &targetRTV, const gui::GuiModel &model,
    res::d3d12::D3D12CommandList &cmdList)
{
    UNUSED(model);

    if (mC.width == 0u || mC.height == 0u)
    { return; }

    const ::D3D12_CPU_DESCRIPTOR_HANDLE rtvs[]{ targetRTV };
    cmdList->OMSetRenderTargets(util::count<::UINT>(rtvs), rtvs, false, nullptr);

    mImGui.render(cmdList);
}

void ImGuiRL::resizeBeforeSwapChain(uint32_t width, uint32_t height)
{
    mImGui.resize(width, height);
    mImGui.freeD3D12Objects();

    mC.width = width;
    mC.height = height;
}

void ImGuiRL::resizeAfterSwapChain()
{
    mImGui.createD3D12Objects();
}

} // namespace rndr

} // namespace quark
