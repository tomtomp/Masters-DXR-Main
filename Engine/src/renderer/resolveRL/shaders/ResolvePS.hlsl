/**
 * @file renderer/resolveRl/shaders/ResolvePS.hlsl
 * @author Tomas Polasek
 * @brief Pixel shader used in resolving the final image.
 */

#ifndef ENGINE_RESOLVE_RL_RESOLVE_PS_HLSL
#define ENGINE_RESOLVE_RL_RESOLVE_PS_HLSL

#include "ResolveShadersCompat.h"

/**
 * Generate the requested debug output.
 * @param mode Debugging mode.
 * @param deferredInfo Filled deferred information structure.
 * @param rayTracingInfo Filled ray tracing information structure.
 */
float4 generateDebugOutput(uint mode, 
    DeferredInformation deferredInfo, 
    RayTracingInformation rayTracingInfo)
{
    switch (mode)
    {
        case HLSL_RAY_TRACING_MODE_RT_OUT:
        { return rayTracingInfo.lightingColor; }
        case HLSL_RAY_TRACING_MODE_SHADOW:
        { 
            const float3 dimmedColor = deferredInfo.diffuse.rgb * 0.5f;
            const float3 intenseColor = saturate(deferredInfo.diffuse.rgb * 3.0f);
            return float4(lerp(dimmedColor, intenseColor, (1.0f - rayTracingInfo.shadowOcclusion)), 1.0f); 
        }
        case HLSL_RAY_TRACING_MODE_OCCLUSION:
        { 
            const float ao = 1.0f - rayTracingInfo.ambientOcclusion;
            return float4(ao, ao, ao, 1.0f);
            const float3 dimmedColor = deferredInfo.diffuse.rgb * 0.5f;
            const float3 intenseColor = saturate(deferredInfo.diffuse.rgb * 5.0f);
            return float4(lerp(dimmedColor, intenseColor, rayTracingInfo.ambientOcclusion), 1.0f); 
        }
        case HLSL_RAY_TRACING_MODE_DEFERRED_POSITION: 
        { return float4(abs(deferredInfo.wsPosition.xyz) / 15.0f, 1.0f); }
        case HLSL_RAY_TRACING_MODE_DEFERRED_NORMAL: 
        { return float4(abs(deferredInfo.wsNormal.xyz), 1.0f); }
        case HLSL_RAY_TRACING_MODE_DEFERRED_DIFFUSE: 
        { return float4(deferredInfo.diffuse.rgba); }
        case HLSL_RAY_TRACING_MODE_DEFERRED_MR: 
        { return float4(deferredInfo.metallicRoughness, 0.0f, 1.0f); }
        case HLSL_RAY_TRACING_MODE_DEFERRED_RR: 
        { 
            const float3 dimmedColor = deferredInfo.diffuse.rgb * 0.5f;
            const float3 intenseColor = saturate(dimmedColor + float3(deferredInfo.doReflections, deferredInfo.doRefractions, 0.0f));
            return float4(intenseColor, 1.0f); 
        }
        case HLSL_RAY_TRACING_MODE_DEFERRED_DEPTH:
        { 
            const float near = 0.1f;
            const float far = 50.0f;
            const float depth = deferredInfo.depth;
            const float linearDepth = (2.0f * near) / (far + near - depth * (far - near));
            const float3 depthColor = float3(linearDepth, linearDepth, linearDepth);
            if (depth == 0.0f)
            { return float4(1.0f, 1.0f, 1.0f, 1.0f); }
            else
            { return float4(depthColor, 1.0f); }
        }
        default:
        { return float4(1.0f, 0.0f, 1.0f, 1.0f); }
    }
}

/**
 * Calculate the final color for current ray.
 * @param deferredInfo Filled structure with deferred information.
 * @param rayTracingInfo Filled structure with ray tracing information.
 * @return Returns the final color.
 */
float4 calcFinalColor(in DeferredInformation deferredInfo, in RayTracingInformation rayTracingInfo)
{
    return pbrHitColor(rayTracingInfo.rayDirection, deferredInfo.wsNormal,
        deferredInfo.metallicRoughness.x, deferredInfo.metallicRoughness.y, deferredInfo.diffuse,
        rayTracingInfo.ambientOcclusion, rayTracingInfo.shadowOcclusion,
        gStatic.lightDirection, gStatic.lightColor, rayTracingInfo.lightingColor);
}

/**
 * Calculate the miss color for current ray.
 * @param deferredInfo Filled structure with deferred information.
 * @param rayTracingInfo Filled structure with ray tracing information.
 * @return Returns the miss color.
 */
float4 calcMissColor(in DeferredInformation deferredInfo, in RayTracingInformation rayTracingInfo)
{ return pbrMissColor(rayTracingInfo.rayDirection, gStatic.backgroundColor, gStatic.lightColor, gStatic.lightDirection); }

/**
 * Resolve the final image.
 */
PixelData main(FragmentData input)
{
    PixelData output;

    DeferredInformation deferredInfo = prepareDeferredInformation(input.texCoord, gDynamic.cameraWorldPosition);
    RayTracingInformation rayTracingInfo = prepareRayTracingInformationUv(input.texCoord);

    if (gStatic.mode != HLSL_RAY_TRACING_MODE_FINAL_COLOR)
    { // For debug modes, just output the requested texture.
        output.color = generateDebugOutput(gStatic.mode, deferredInfo, rayTracingInfo);
        return output;
    }

    float4 finalColor = 0.0f;
    if (deferredInfo.hasData)
    { finalColor = calcFinalColor(deferredInfo, rayTracingInfo); }
    else
    { finalColor = calcMissColor(deferredInfo, rayTracingInfo); }

    output.color = finalColor;
    return output;
}

#endif // ENGINE_RESOLVE_RL_RESOLVE_PS_HLSL
