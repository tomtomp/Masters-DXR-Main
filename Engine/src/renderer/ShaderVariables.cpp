/**
 * @file renderer/ShaderVariables.cpp
 * @author Tomas Polasek
 * @brief Container used for specification of 
 * shader variables and their values.
 */

#include "stdafx.h"

#if 0
#include "engine/renderer/ShaderVariables.h"

namespace quark
{

namespace rndr
{

namespace impl
{

ShaderVariableBase::ShaderVariableBase(ShaderVariableType type) : 
    mType{ type }
{ }

ShaderVariableBase::~ShaderVariableBase()
{ /* Automatic */ }

ShaderVariableType ShaderVariableBase::type() const
{ return mType; }

ShaderVariableConstant::PtrT ShaderVariableConstant::create()
{ return PtrT{ new ShaderVariableConstant() }; }

ShaderVariableConstant::PtrT ShaderVariableConstant::create(const uint8_t *data, std::size_t sizeInBytes)
{ return PtrT{ new ShaderVariableConstant(data, sizeInBytes) }; }

ShaderVariableConstant::~ShaderVariableConstant()
{ /* Automatic */ }

void ShaderVariableConstant::updateData(const uint8_t *data, std::size_t sizeInBytes)
{
    mData.resize(sizeInBytes);
    mData.assign(data, data + sizeInBytes);
}

ShaderVariableConstant::ShaderVariableConstant() : 
    ShaderVariableBase(ShaderVariableType::Constant)
{ }

ShaderVariableConstant::ShaderVariableConstant(const uint8_t *data, std::size_t sizeInBytes) : 
    ShaderVariableBase(ShaderVariableType::Constant)
{ updateData(data, sizeInBytes); }

ShaderVariableSampler::PtrT ShaderVariableSampler::create()
{ return PtrT{ new ShaderVariableSampler() }; }

ShaderVariableSampler::PtrT ShaderVariableSampler::create(const rndr::MinMagMipFilter &filtering, 
    const rndr::EdgeFilterUVW &edgeFiltering)
{ return PtrT{ new ShaderVariableSampler(filtering, edgeFiltering) }; }

ShaderVariableSampler::~ShaderVariableSampler()
{ /* Automatic */ }

void ShaderVariableSampler::setParameters(const rndr::MinMagMipFilter &filtering, 
    const rndr::EdgeFilterUVW &edgeFiltering)
{
    mFiltering = filtering;
    mEdgeFiltering = edgeFiltering;
}

ShaderVariableSampler::ShaderVariableSampler() : 
    ShaderVariableBase(ShaderVariableType::Sampler)
{ }

ShaderVariableSampler::ShaderVariableSampler(const rndr::MinMagMipFilter &filtering, 
    const rndr::EdgeFilterUVW &edgeFiltering) : 
    ShaderVariableBase(ShaderVariableType::Sampler)
{ setParameters(filtering, edgeFiltering); }

}

ShaderVariable::ShaderVariable()
{ /* Automatic */ }

ShaderVariable::~ShaderVariable()
{ /* Automatic */ }

void ShaderVariable::setLocation(uint32_t varRegister, uint32_t varSpace)
{
    mShaderRegister = varRegister;
    mShaderSpace = varSpace;

    setDirty(true);
}

void ShaderVariable::setSampler(const rndr::MinMagMipFilter &filtering, 
    const rndr::EdgeFilterUVW &edgeFiltering)
{
    if (type() != ShaderVariableType::Sampler)
    { // Create the data holder.
        mVariable = impl::ShaderVariableSampler::create();
    }

    // Set the requested data.
    asDataPointer<impl::ShaderVariableSampler>(mVariable)->setParameters(filtering, edgeFiltering);
}

void ShaderVariable::setConstant(const uint8_t *data, std::size_t sizeInBytes)
{
    if (type() != ShaderVariableType::Constant)
    { // Create the data holder.
        mVariable = impl::ShaderVariableConstant::create();
    }

    // Set the requested data.
    asDataPointer<impl::ShaderVariableConstant>(mVariable)->updateData(data, sizeInBytes);
}

ShaderVariableType ShaderVariable::type() const
{ return mVariable ? mVariable->type() : ShaderVariableType::None; }

const impl::ShaderVariableConstant *ShaderVariable::constantData() const
{ return type() == ShaderVariableType::Constant ? asDataPointer<impl::ShaderVariableConstant>(mVariable) : nullptr; }

const impl::ShaderVariableSampler *ShaderVariable::samplerData() const
{ return type() == ShaderVariableType::Sampler ? asDataPointer<impl::ShaderVariableSampler>(mVariable) : nullptr; }

bool ShaderVariable::isDirty() const
{ return mVariableDirty; }

void ShaderVariable::setDirty(bool dirty)
{ mVariableDirty = dirty; }

ShaderVariables::PtrT ShaderVariables::create()
{ return PtrT{ new ShaderVariables() }; }

ShaderVariables::~ShaderVariables()
{ /* Automatic */ }

ShaderVariable &ShaderVariables::operator[](const std::string &varName)
{ return mVariables[varName]; }

ShaderVariables::ShaderVariables()
{ /* Automatic */ }

bool ShaderVariables::isVariableDirty(const ShaderVariable &var)
{ return var.isDirty(); }

void ShaderVariables::setVariableDirty(ShaderVariable &var, bool dirty)
{ var.setDirty(dirty); }

}

}

#endif // 0

