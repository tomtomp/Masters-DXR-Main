/**
 * @file renderer/basicTexturedRL/shaders/TexturedVS.hlsl
 * @author Tomas Polasek
 * @brief Vertex shader for simple textured render layer.
 */

//#include "HlslCompatibility.h"

struct PerDrawData
{
    row_major matrix vp;
};
ConstantBuffer<PerDrawData> gPerDrawData : register(b0);

struct PerModelData
{
    row_major matrix m;
};
ConstantBuffer<PerModelData> gPerModelData : register(b1);

struct VertexData
{
    float3 position : POSITION;
    float2 texCoord : TEXCOORD;
};

struct VertexShaderOutput
{
    float4 position : SV_Position;
    float2 texCoord : TEXCOORD;
};

VertexShaderOutput main(VertexData input)
{
    VertexShaderOutput output;

    output.position = mul(float4(input.position, 1.0f), mul(gPerModelData.m, gPerDrawData.vp));
    output.texCoord = input.texCoord;

    return output;
}
