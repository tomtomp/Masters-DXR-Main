/**
 * @file renderer/basicTexturedRL/shaders/TexturedPS.hlsl
 * @author Tomas Polasek
 * @brief Pixel shader for simple textured render layer.
 */

#include "HlslCompatibility.h"

Texture2D gTextures[HLSL_TEXTURE_UPPER_BOUND] : register(t3);
SamplerState gCommonSampler : register(s0);

struct MaterialData
{
    uint diffuseIdx;
    uint materialIdx;
};
ConstantBuffer<MaterialData> gMaterialData : register(b2);

struct PixelShaderInput
{
    float4 position : SV_Position;
    float2 texCoord : TEXCOORD;
};

float4 main(PixelShaderInput input) : SV_Target
{
    float4 result = gTextures[gMaterialData.diffuseIdx].Sample(gCommonSampler, input.texCoord);
    return result;
}
