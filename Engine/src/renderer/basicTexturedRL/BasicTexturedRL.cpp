/**
 * @file renderer/basicTexturedRL/BasicTexturedRL.cpp
 * @author Tomas Polasek
 * @brief Rendering layer allowing basic display of 
 * textured meshes.
 */

#include "stdafx.h"

#include "engine/renderer/basicTexturedRL/BasicTexturedRL.h"

#include "engine/renderer/basicTexturedRL/shaders/TexturedVS.h"
#include "engine/renderer/basicTexturedRL/shaders/TexturedPS.h"

#include "engine/helpers/hlsl/HlslCompatibility.h"

namespace quark
{

namespace rndr
{

BasicTexturedRL::BasicTexturedRL(RenderSubSystem &renderer) :
    mRenderer{ &renderer }
{ initializeResources(); }

BasicTexturedRL::PtrT BasicTexturedRL::create(RenderSubSystem &renderer)
{ return PtrT{ new BasicTexturedRL(renderer) }; }

BasicTexturedRL::~BasicTexturedRL()
{ /* Automatic */ }

void BasicTexturedRL::setDrawablesList(const res::scene::DrawableListHandle &drawables)
{
    mDrawables = drawables;
    mLastPreparedVersion = { };
}

void BasicTexturedRL::resize(uint32_t width, uint32_t height)
{
    mR.scissorRect = ::CD3DX12_RECT(
        0u, 0u, 
        width, height);
    mR.viewport = ::CD3DX12_VIEWPORT{ 
        0.0f, 0.0f, 
        static_cast<float>(width), static_cast<float>(height) };
    mR.depthStencilBuffer = res::rndr::DepthStencilBuffer::create(width, height, mRenderer->device());

    mPrepared = true;
}

void BasicTexturedRL::prepare(bool sceneChanged)
{
    PROF_SCOPE("TexturedRLPrep");

    auto &drawables{ mDrawables.get() };
    if (drawables.revision() != mLastPreparedVersion)
    { // Current version of the list is not prepared!
        prepareForRendering(drawables, sceneChanged);
        mLastPreparedVersion = drawables.revision();
    }
}

void BasicTexturedRL::prepareGui(gui::GuiSubSystem &gui)
{ UNUSED(gui); }

void BasicTexturedRL::render(const helpers::math::Camera &camera, 
    const ::CD3DX12_CPU_DESCRIPTOR_HANDLE &targetRTV,
    res::d3d12::D3D12CommandList &cmdList)
{
    if (!mPrepared)
    { return; }

    GPU_PROF_SCOPE(cmdList, "TexturedRLGpu");

    // Clear the depth-stencil buffer.
    mR.depthStencilBuffer->clear(cmdList);

    // Prepare shared GPU state:
    cmdList->RSSetViewports(1u, &mR.viewport);
    cmdList->RSSetScissorRects(1u, &mR.scissorRect);

    cmdList->SetGraphicsRootSignature(mR.rootSig->get());

    const auto rtv{ targetRTV };
    const auto dsv{ mR.depthStencilBuffer->dsv() };
    cmdList->OMSetRenderTargets(1u, &rtv, false, &dsv);

    // Set the user specified camera matrix.
    PerDrawData pdd{ camera.vpNoRecalculation() };
    cmdList->SetGraphicsRoot32BitConstants(
        ROOT_PARAMETER_IDX_PER_DRAW, 
        PerDrawData::sizeIn32bValues(), &pdd,
        0u);

    // Use the prepared descriptor heaps.
    setDescHeaps(cmdList);

    // Execute the current drawing bundle.
    cmdList->ExecuteBundle(mR.renderBundle->get());
}

void BasicTexturedRL::updateGui(gui::GuiSubSystem &gui)
{ UNUSED(gui); }

void BasicTexturedRL::initializeResources(uint32_t requiredTextures)
{
    PROF_SCOPE("TexturedRLInitialization");

    // Create the root signatures, input layout and build pipeline.
    createPipeline(requiredTextures);

    // Create temporary depth-stencil buffer, until we create the real one.
    mR.depthStencilBuffer = res::rndr::DepthStencilBuffer::create(1u, 1u, mRenderer->device());

    /*
    // Descriptor heap used for storing the SRV descriptors.
    mR.mainDescHeap = mRenderer->memoryMgr().descHeap(
        std::max<uint32_t>(requiredTextures, HLSL_TEXTURE_UPPER_BOUND), 
        ::D3D12_DESCRIPTOR_HEAP_TYPE_CBV_SRV_UAV,
        ::D3D12_DESCRIPTOR_HEAP_FLAG_SHADER_VISIBLE);

    // One large texture table.
    mR.textureTable = helpers::d3d12::D3D12DescTable(
        *mR.mainDescHeap, mR.mainDescHeap->allocateTable(
            std::max<uint32_t>(requiredTextures, HLSL_TEXTURE_UPPER_BOUND)));
    mR.textureTable.initializeSrvNullHandles(
        ::D3D12_SRV_DIMENSION_TEXTURE2D);
        */

    // Create a material cache.
    if (!mR.materialMeshCache)
    { mR.materialMeshCache = helpers::rndr::MaterialMeshCache::create(); }
}

void BasicTexturedRL::createPipeline(uint32_t requiredTextures)
{
    // No scissoring.
    mR.scissorRect = ::CD3DX12_RECT(0, 0, LONG_MAX, LONG_MAX);

    // Range specification for the SRV descriptor table.
    ::CD3DX12_DESCRIPTOR_RANGE1 srvRange{ 
        ::D3D12_DESCRIPTOR_RANGE_TYPE_SRV, 
        std::max<uint32_t>(requiredTextures, HLSL_TEXTURE_UPPER_BOUND), 
        ROOT_PARAMETER_IDX_SRV_TABLE, 0u };
    const ::CD3DX12_STATIC_SAMPLER_DESC commonSampler{ 
        ROOT_PARAMETER_IDX_COMMON_SAMPLER, 
        ::D3D12_FILTER_ANISOTROPIC, 
        ::D3D12_TEXTURE_ADDRESS_MODE_WRAP, 
        ::D3D12_TEXTURE_ADDRESS_MODE_WRAP, 
        ::D3D12_TEXTURE_ADDRESS_MODE_WRAP, 
        0.0f, 16u,
        ::D3D12_COMPARISON_FUNC_LESS_EQUAL,
        ::D3D12_STATIC_BORDER_COLOR_OPAQUE_WHITE,
        0.0f, D3D12_FLOAT32_MAX,
        ::D3D12_SHADER_VISIBILITY_ALL,
        0u
    };
    // Prepare root signature, which allows passing a single matrix - projection matrix.
    quark::helpers::d3d12::D3D12RootSignatureBuilder rsBuilder;
    rsBuilder.nextRootParameter().asConstantParameter(PerDrawData::sizeIn32bValues(), ROOT_PARAMETER_IDX_PER_DRAW,
        0u, ::D3D12_SHADER_VISIBILITY_VERTEX);
        rsBuilder.nextRootParameter().asConstantParameter(PerModelData::sizeIn32bValues(), ROOT_PARAMETER_IDX_PER_MODEL,
            0u, ::D3D12_SHADER_VISIBILITY_VERTEX);
        rsBuilder.nextRootParameter().asConstantParameter(MaterialData::sizeIn32bValues(), ROOT_PARAMETER_IDX_MATERIAL,
            0u, ::D3D12_SHADER_VISIBILITY_PIXEL);
        rsBuilder.nextRootParameter().asDescriptorTableParameter(1u, &srvRange, ::D3D12_SHADER_VISIBILITY_PIXEL);
    rsBuilder.addStaticSampler(commonSampler);
    rsBuilder.setFlags(
        D3D12_ROOT_SIGNATURE_FLAG_ALLOW_INPUT_ASSEMBLER_INPUT_LAYOUT |
        D3D12_ROOT_SIGNATURE_FLAG_DENY_HULL_SHADER_ROOT_ACCESS |
        D3D12_ROOT_SIGNATURE_FLAG_DENY_DOMAIN_SHADER_ROOT_ACCESS |
        D3D12_ROOT_SIGNATURE_FLAG_DENY_GEOMETRY_SHADER_ROOT_ACCESS);

    mR.rootSig = rsBuilder.build(mRenderer->device());

    mR.inputLayout.clear();

    // Vertex position.
    mR.inputLayout.addElement(
        "POSITION", 0u, 
        mR.vertexPosFormat, mR.vertexPosSlot, 
        D3D12_APPEND_ALIGNED_ELEMENT, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0u);
    // Vertex texture coordinate.
    mR.inputLayout.addElement(
        "TEXCOORD", 0u, 
        mR.textureCoordFormat, mR.textureCoordSlot, 
        D3D12_APPEND_ALIGNED_ELEMENT, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0u);

    // Load texturing shaders.
    mR.vShader = res::d3d12::D3D12Shader::create(gTexturedVs, sizeof(gTexturedVs));
    mR.pShader = res::d3d12::D3D12Shader::create(gTexturedPs, sizeof(gTexturedPs));

    // Build the pipeline state.
    quark::helpers::d3d12::D3D12PipelineStateSOBuilder pssob;
    quark::helpers::d3d12::D3D12PipelineStateBuilder psb;
    psb << 
        mR.rootSig->rsState() <<
        mR.inputLayout.ilState() <<
        pssob.rasterizer(::D3D12_CULL_MODE_BACK, 
            !mRenderer->cfg().renderRasterizerFrontClockwise, 
            mRenderer->cfg().renderRasterizerFill) << 
        pssob.primitiveTopology(mR.primitiveTopologyType) << 
        mR.vShader->vsState() << 
        mR.pShader->psState() << 
        pssob.depthStencilFormat(::DXGI_FORMAT_D32_FLOAT) << 
        pssob.renderTargetFormats({ mR.renderTargetFormat });
    mR.pipelineState = psb.build(mRenderer->device());

}

void BasicTexturedRL::prepareMaterials(res::scene::DrawableList &drawables)
{
    PROF_SCOPE("PrepareMaterials");

    for (const auto &drawable : drawables)
    { // Process all drawables.
        mR.materialMeshCache->registerMesh(drawable.meshHandle);
    }
}

void BasicTexturedRL::prepareDrawBundle(res::scene::DrawableList &drawables, 
    res::d3d12::D3D12CommandList &drawBundle)
{
    PROF_SCOPE("PrepareDrawBundle");

    // Common to all drawables: 
    drawBundle->SetGraphicsRootSignature(mR.rootSig->get());
    drawBundle->SetPipelineState(mR.pipelineState->get());
    drawBundle->IASetPrimitiveTopology(mR.primitiveTopology);
    setDescHeaps(drawBundle);
    drawBundle->SetGraphicsRootDescriptorTable(
        ROOT_PARAMETER_IDX_SRV_TABLE, mR.textureTable.baseHandle());

    for (const auto &drawable : drawables)
    { // Process all drawables.

        // One mesh can contain multiple primitives.
        const auto &mesh{ drawable.meshHandle.get() };

        // Use the specified model matrix.
        PerModelData pmd{ drawable.modelMatrix.matrix() };
        drawBundle->SetGraphicsRoot32BitConstants(
            ROOT_PARAMETER_IDX_PER_MODEL, 
            PerModelData::sizeIn32bValues(), &pmd, 
            0u);

        for (std::size_t iii = 0; iii < mesh.primitives.size(); ++iii)
        { // Render the primitives.
            const auto &primitive{ mesh.primitives[iii] };

            // Get all used attributes.
            const auto &positionBuffer{ primitive.getPositionBuffer() };
            const auto &textureCoordBuffer{ primitive.getTexCoordBuffer() };

            // Access the material/mesh cache.
            const auto primitiveSpecification{ mR.materialMeshCache->specificationForPrimitive(drawable.meshHandle, iii) };

            // Indices for diffuse and material textures.
            const auto diffuseIndex{ primitiveSpecification.material.colorTextureIndex };
            const auto materialIndex{ primitiveSpecification.material.metallicRoughnessTextureIndex };

            // Specify which material textures should be used.
            MaterialData md{ diffuseIndex, materialIndex };
            drawBundle->SetGraphicsRoot32BitConstants(
                ROOT_PARAMETER_IDX_MATERIAL, 
                MaterialData::sizeIn32bValues(), &md, 
                0u);

            const ::D3D12_VERTEX_BUFFER_VIEW vbvs[] = {
                positionBuffer->vbv(), 
                textureCoordBuffer->vbv(), 
            };

            drawBundle->IASetVertexBuffers(0u, static_cast<::UINT>(util::count(vbvs)), vbvs);

            const auto &indexBuffer{ primitive.indices };
            const auto ibw = indexBuffer->ibv();
            drawBundle->IASetIndexBuffer(&ibw);

            drawBundle->DrawIndexedInstanced(
                static_cast<::UINT>(indexBuffer->numElements), 
                // Only one instance.
                1u, 0u, 0u, 0u);
        }
    }
}

uint32_t BasicTexturedRL::calculateTextureDescriptorCount(
    helpers::rndr::MaterialMeshCache &cache)
{ return std::max<uint32_t>(cache.calculateSizeRequirements().textureDescriptors, HLSL_TEXTURE_UPPER_BOUND); }

helpers::d3d12::D3D12DescTable BasicTexturedRL::createTextureTable(
    res::d3d12::D3D12DescHeap &descHeap, uint32_t numDescriptors)
{
    // Create the table and initialize null handles, so we can debug.
    helpers::d3d12::D3D12DescTable result(
        descHeap, descHeap.allocateTable(numDescriptors));
    result.initializeSrvNullHandles(::D3D12_SRV_DIMENSION_TEXTURE2D);

    return result;
}

res::d3d12::D3D12DescHeap::PtrT BasicTexturedRL::createDescHeap(std::size_t requiredDescriptors)
{
    return res::d3d12::D3D12DescHeap::create(mRenderer->device(), 
        ::D3D12_DESCRIPTOR_HEAP_TYPE_CBV_SRV_UAV,
        static_cast<::uint32_t>(requiredDescriptors),
        ::D3D12_DESCRIPTOR_HEAP_FLAG_SHADER_VISIBLE);
}

void BasicTexturedRL::createDescHeapLayout(res::d3d12::D3D12DescHeap::PtrT &descHeap, 
    helpers::d3d12::D3D12DescTable &textureTable, helpers::rndr::MaterialMeshCache &cache)
{
    // Calculate descriptor requirements.
    const auto textureDescriptors{ calculateTextureDescriptorCount(cache) };
    const auto totalDescriptors{ textureDescriptors };

    // Check whether we need to create a new descriptor heap.
    bool newDescHeapRequired{ false };
    newDescHeapRequired |= textureDescriptors != textureTable.size();

    if (!newDescHeapRequired)
    { // No changed -> we don't need a new descriptor heap.
        return;
    }

    // Create the main descriptor heap, which will be used for all descriptors.
    auto newDescHeap{ createDescHeap(totalDescriptors) };

    // Create descriptor table for texture SRV descriptors.
    auto newTextureTable{ createTextureTable(*newDescHeap, textureDescriptors) };

    // Copy over the original descriptor tables: 
    //newTextureTable.copyFrom(textureTable);

    // Change over to the new descriptor heap.
    descHeap = newDescHeap;
    textureTable = std::move(newTextureTable);
}

void BasicTexturedRL::prepareForRendering(
    res::scene::DrawableList &drawables, bool sceneChanged)
{
    PROF_SCOPE("PrepareForRendering");

    // Create the reusable command bundle: 
    res::d3d12::D3D12CommandList::PtrT drawBundle{ mRenderer->bundleCmdQueue().getCommandList() };

    if (drawables.empty())
    { // Nothing to be done...
        mR.renderBundle = drawBundle;
        mR.renderBundle->get()->Close();
        return;
    }

    if (sceneChanged)
    { // Purge old materials.
        mR.materialMeshCache->clear();
    }

    // Get materials into usable state.
    prepareMaterials(drawables);

    // Create pipeline supporting required number of textures.
    createPipeline(mR.materialMeshCache->calculateSizeRequirements().textureDescriptors);

    // Prepare descriptor heap for texture descriptors.
    createDescHeapLayout(mR.mainDescHeap, mR.textureTable, *mR.materialMeshCache);

    // Fill the descriptor table with required texture descriptors.
    mR.materialMeshCache->buildTextureCache(mR.textureTable);

    // Prepare drawing commands for all drawables.
    prepareDrawBundle(drawables, *drawBundle);

    mR.renderBundle = drawBundle;
    mR.renderBundle->get()->Close();
}

void BasicTexturedRL::setDescHeaps(res::d3d12::D3D12CommandList &cmdList)
{
    if (mR.mainDescHeap->initialized())
    { // May not be created, e.g. when there are no drawables.
        ::ID3D12DescriptorHeap *heaps[] = {
            mR.mainDescHeap->get()
        };
        cmdList->SetDescriptorHeaps(static_cast<::UINT>(util::count(heaps)), heaps);
    }
}

}

}
