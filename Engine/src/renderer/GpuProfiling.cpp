/**
 * @file renderer/GpuProfiling.cpp
 * @author Tomas Polasek
 * @brief GPU profiling macros and management.
 */

#include "stdafx.h"

#include "engine/renderer/GpuProfiling.h"

namespace quark
{

namespace rndr
{

namespace prof
{

namespace impl
{

/// Profiling adapter used by all GPU profiling macros.
GpuProfilingAdapter *gHiddenProfilingAdapter{ nullptr };

void setGlobalProfilingAdapter(GpuProfilingAdapter *adapter)
{ gHiddenProfilingAdapter = adapter; }

GpuBlockProfiler createBlockProfiler(res::d3d12::D3D12CommandList &cmdList, const char *name, bool manualEnd)
{
    ASSERT_SLOW(gHiddenProfilingAdapter);
    return GpuBlockProfiler(*gHiddenProfilingAdapter, cmdList, name, manualEnd);
}

void endLastBlockProfiler(res::d3d12::D3D12CommandList &cmdList)
{
    ASSERT_SLOW(gHiddenProfilingAdapter);
    gHiddenProfilingAdapter->exitScope(cmdList);
}

GpuScopeProfiler createScopeProfiler(res::d3d12::D3D12CommandList &cmdList, const char *name)
{
    ASSERT_SLOW(gHiddenProfilingAdapter);
    return GpuScopeProfiler(*gHiddenProfilingAdapter, cmdList, name);
}

void resolveProfiler(res::d3d12::D3D12CommandList &cmdList)
{
    ASSERT_SLOW(gHiddenProfilingAdapter);
    gHiddenProfilingAdapter->resolve(cmdList);
}

void finalizeProfiler()
{
    ASSERT_SLOW(gHiddenProfilingAdapter);
    gHiddenProfilingAdapter->synchronize();
}

}

}

}

}
