/**
 * @file renderer/RenderResourceMgr.cpp
 * @author Tomas Polasek
 * @brief Management of resources used in rendering. These 
 * include render target textures, buffers, samplers, etc.
 */

#include "stdafx.h"

#include "engine/renderer/RenderResourceMgr.h"

#include "engine/renderer/RenderSubSystem.h"

namespace quark
{

namespace rndr
{

RenderResourceMgr::PtrT RenderResourceMgr::create(RenderContext::PtrT &renderContext)
{ return PtrT(new RenderResourceMgr(renderContext)); }

RenderResourceMgr::~RenderResourceMgr()
{ /* Automatic */ }

RenderResourceMgr::RenderResourceMgr(RenderContext::PtrT &renderContext) : 
    mRenderCtx{ renderContext }, 
    mTextureMgr{ res::rndr::TextureMgr::createMgr() }, 
    mBufferMgr{ res::rndr::BufferMgr::createMgr() }, 
    mSamplerMgr{ res::rndr::StaticSamplerMgr::createMgr() }
{
    mTextureMgr->initialize(mRenderCtx->rndr().memoryMgr());
    mBufferMgr->initialize(mRenderCtx->rndr().memoryMgr());
}

}

}
