/**
 * @file renderer/ShaderCache.cpp
 * @author Tomas Polasek
 * @brief System used for shader caching and compilation.
 */

#include "stdafx.h"

#if 0

#include "engine/renderer/ShaderCache.h"

namespace quark
{

namespace rndr
{

ShaderRecord::~ShaderRecord()
{
}

bool ShaderRecord::prepared() const
{
    return false;
}

void ShaderRecord::requestReload()
{
}

ShaderRecord::ShaderRecord()
{
}

void ShaderRecord::requestFromSource(ShaderType type, const std::string & sourceCode, const std::string & entryPoint)
{
}

void ShaderRecord::requestFromBlob(ShaderType type, const uint8_t * ptr, std::size_t sizeInBytes)
{
}

void ShaderRecord::initialize(util::PtrT<RenderContext> ctx)
{
}

}

}

#endif // 0
