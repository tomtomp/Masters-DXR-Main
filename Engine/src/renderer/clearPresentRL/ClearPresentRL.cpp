/**
 * @file renderer/ClearPresentRL.cpp
 * @author Tomas Polasek
 * @brief Rendering layer allowing clearing and presenting buffers in 
 * a flip-discard swap chain.
 */

#include "stdafx.h"

#include "engine/renderer/ClearPresentRL/ClearPresentRL.h"

#include "engine/renderer/RenderSubSystem.h"

namespace quark
{

namespace rndr
{

ClearPresentRL::ClearPresentRL(RenderSubSystem &renderer) :
    mRenderer{ renderer },
    mFrameFenceValues(renderer.swapChain().numBuffers(), 0u)
{ }

util::PointerType<ClearPresentRL>::PtrT ClearPresentRL::create(RenderSubSystem &renderer)
{ return PtrT{ new ClearPresentRL(renderer) }; }

res::d3d12::D3D12CommandList::PtrT ClearPresentRL::clear(dxtk::math::Color clearColor)
{
    auto cmdList{ mRenderer.directCmdQueue().getCommandList() };
    { // Command list.
        // Transition current backbuffer, so we can use it for rendering.
        mRenderer.swapChain().currentBackbuffer().transitionTo(::D3D12_RESOURCE_STATE_RENDER_TARGET, *cmdList);
        // Clear the backbuffer with provided color.
        cmdList->get()->ClearRenderTargetView(mRenderer.swapChain().currentBackbufferRtv(), clearColor, 0u, nullptr);
    }

    return cmdList;
}

void ClearPresentRL::present(res::d3d12::D3D12CommandList::PtrT cmdList, bool useVSync, bool useTearing)
{
    PROF_SCOPE("Present");
    GPU_PROF_BLOCK(*cmdList, "Present");

    // Transition current backbuffer, so we can present it.
    mRenderer.swapChain().currentBackbuffer().transitionTo(::D3D12_RESOURCE_STATE_PRESENT, *cmdList);

    /* 
     * Choose how to synchronize presentation - 0 for immediate, 
     * 1-4 for after n-th vertical blank
     */
    const uint32_t syncInterval{ useVSync ? 1u : 0u };
    /// Enable tearing, if requested, supported and vSync is disabled.
    const uint32_t presentFlags{ useTearing && mRenderer.swapChain().tearingSupported() && !useVSync ? 
        DXGI_PRESENT_ALLOW_TEARING : 0u };

    GPU_PROF_BLOCK_END(*cmdList);

    // Resolve all timers in the GPU profiler.
    GPU_PROF_RESOLVE(*cmdList);

    // Post fence, so we know when we can reuse the back-buffer.
    const auto fenceValue{ cmdList->executeNoWait() };
    setCurrentBackbufferFence(fenceValue);
    
    mRenderer.swapChain().present(syncInterval, presentFlags);

    //mRenderer.directCmdQueue().fenceWaitFor(fenceValue);
    //mRenderer.directCmdQueue().flush();
    // TODO - Should this wait be before rendering the next frame?
    // Wait for the fence value of the next back-buffer, so it is safe to use it again.
    //mRenderer.directCmdQueue().fenceWaitFor(getCurrentBackbufferFence());
    mRenderer.directCmdQueue().fenceWaitFor(fenceValue);

    // Read the timer values and use them in the profiler.
    GPU_PROF_FINALIZE();
}

void ClearPresentRL::setCurrentBackbufferFence(uint64_t value)
{
    const auto currentBackBufferIdx{ mRenderer.swapChain().currentBackbufferIndex() };
    ASSERT_SLOW(currentBackBufferIdx < mFrameFenceValues.size());
    mFrameFenceValues[currentBackBufferIdx] = value;
}

uint64_t ClearPresentRL::getCurrentBackbufferFence()
{
    const auto currentBackBufferIdx{ mRenderer.swapChain().currentBackbufferIndex() };
    ASSERT_SLOW(currentBackBufferIdx < mFrameFenceValues.size());
    return mFrameFenceValues[currentBackBufferIdx];
}

}

}
