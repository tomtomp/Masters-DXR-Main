/**
 * @file renderer/RenderPass.cpp
 * @author Tomas Polasek
 * @brief Base class for render passes, which can be 
 * added to the RenderPipeline.
 */

#include "stdafx.h"

#include "engine/renderer/RenderPass.h"

namespace quark
{

namespace rndr
{

bool RenderPass::initialized() const
{ return mInitialized; }

const std::string &RenderPass::name() const
{ return mName; }

bool RenderPass::initializeWrapper(RenderContext::PtrT ctx, RenderResourceMgr::PtrT resMgr)
{
    // Call the implementation.
    return this->initialize(ctx, resMgr);
}

void RenderPass::sceneChangedWrapper(RenderScene::PtrT scene)
{
    // Call the implementation.
    this->sceneChanged(scene);
}

void RenderPass::executeWrapper(RenderContext::PtrT ctx)
{
    // Call the implementation.
    this->execute(ctx);
}

void RenderPass::deinitializeWrapper()
{
    // Call the implementation.
    this->deinitialize();
}

RenderPass::RenderPass(const std::string &name) : 
    mName{ name }
{ } 

RenderPass::~RenderPass()
{ /* Automatic */ }

}

}
