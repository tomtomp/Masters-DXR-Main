/**
 * @file renderer/basicRayTracing/BasicTexturedRL.cpp
 * @author Tomas Polasek
 * @brief Rendering layer allowing basic hardware 
 * accelerated ray tracing.
 */

#include "stdafx.h"

#ifdef ENGINE_RAY_TRACING_ENABLED

#include "engine/renderer/basicRayTracingRL/BasicRayTracingRL.h"

#include "engine/renderer/basicRayTracingRL/shaders/BasicRayTracing.h"
#include "engine/renderer/basicRayTracingRL/shaders/BasicRayTracingProf.h"

#include "engine/helpers/d3d12/D3D12RootSignatureBuilder.h"
#include "engine/helpers/d3d12/D3D12BatchTransitionBarrier.h"

#include "engine/gui/GuiSubSystem.h"

namespace quark
{

namespace rndr
{

void RayTracingBuffers::transitionResourcesTo(
    res::d3d12::D3D12CommandList &cmdList, ::D3D12_RESOURCE_STATES state)
{
    /*
    if (ambientOcclusion)
    { ambientOcclusion->transitionTo(state, cmdList); }
    if (shadowOcclusion)
    { shadowOcclusion->transitionTo(state, cmdList); }
    if (lightingColor)
    { lightingColor->transitionTo(state, cmdList); }
    */

    helpers::d3d12::D3D12BatchTransitionBarrier::transitionTo(state, cmdList,
        {
            ambientOcclusion, 
            shadowOcclusion, 
            lightingColor
        });
}

void RayTracingBufferViews::createViews(const RayTracingBuffers &buffers, 
    res::d3d12::D3D12DescAllocatorInterface &descHeap, ResourceViewType type)
{
    if (!ambientOcclusion)
    { ambientOcclusion = helpers::d3d12::D3D12ResourceView::create(); }
    if (buffers.ambientOcclusion)
    { ambientOcclusion->reallocateDescriptor(buffers.ambientOcclusion, descHeap, type); }

    if (!shadowOcclusion)
    { shadowOcclusion = helpers::d3d12::D3D12ResourceView::create(); }
    if (buffers.shadowOcclusion)
    { shadowOcclusion->reallocateDescriptor(buffers.shadowOcclusion, descHeap, type); }

    if (!lightingColor)
    { lightingColor = helpers::d3d12::D3D12ResourceView::create(); }
    if (buffers.lightingColor)
    { lightingColor->reallocateDescriptor(buffers.lightingColor, descHeap, type); }
}

BasicRayTracingRL::BasicRayTracingRL(RenderSubSystem &renderer) :
    mRenderer{ renderer }, 
    mAccelerationBuilder{ 
        helpers::d3d12::D3D12RTAccelerationBuilder::create(
            mRenderer.rayTracing().device(), 
            mRenderer.memoryMgr()) }
{ 
    initializeResources(); 

    if (mRenderer.cfg().renderRayTracingFastBuildAS)
    { mAccelerationBuilder->preferFastBuild(); }
    else
    { mAccelerationBuilder->preferFastRayTracing(); }
}

util::PointerType<BasicRayTracingRL>::PtrT BasicRayTracingRL::create(RenderSubSystem &renderer)
{ return PtrT{ new BasicRayTracingRL(renderer) }; }

BasicRayTracingRL::~BasicRayTracingRL()
{ /* Automatic */ }

void BasicRayTracingRL::setDrawablesList(const res::scene::DrawableListHandle &drawables)
{
    mDrawables = drawables;
    mLastPreparedVersion = { };
}

void BasicRayTracingRL::setDeferredBuffers(DeferredBuffers buffers)
{ mR.deferredBuffers = buffers; }

void BasicRayTracingRL::resize(uint32_t width, uint32_t height)
{
    // Create the output textures.
    createResizeTextures(width, height);

    // End here, if we are not going to ray trace.
    if (!mRayTracingSupported || !mPrepared)
    { return; }

    // Create descriptors for output textures.
    createTextureDescriptors();
}

void BasicRayTracingRL::prepare(
    res::d3d12::D3D12CommandList &directCmdList, bool sceneChanged)
{
    PROF_SCOPE("RayTracingRLPrep");

    if (!mRayTracingSupported)
    { return; }

    auto &drawables{ mDrawables.get() };
    if (drawables.revision() != mLastPreparedVersion)
    { // Current version of the list is not prepared!
        prepareForRendering(drawables, directCmdList, sceneChanged);
        mLastPreparedVersion = drawables.revision();
    }
}

void BasicRayTracingRL::prepareGui(gui::GuiSubSystem &gui)
{
    gui.addGuiContext("/RayTracingRL/")
        .setType(gui::GuiContextType::Category)
        .setLabel("RayTracing Pass");

    gui.addGuiVariable<gui::Enumeration>("RayTracingRlMode", "/RayTracingRL/", { })
        .setLabel("Mode")
        .cfg().setItemGetter(static_cast<std::size_t>(DebugMode::Count), 
            [] (int index, const char **out) 
    {
        *out = debugModeName(static_cast<DebugMode>(index));
        return out != nullptr;
    });

    gui.addGuiVariable<gui::Position3>("RayTracingRlSunDirection", "/RayTracingRL/", { })
        .setLabel("Sun Direction");
    gui.addGuiVariable<gui::Color3>("RayTracingRlSunColor", "/RayTracingRL/", { })
        .setLabel("Sun Color");
    gui.addGuiVariable<gui::Color3>("RayTracingRlSkyColor", "/RayTracingRL/", { })
        .setLabel("Sky Color");

    gui.addGuiVariable<gui::InputInt>("RayTracingRlShadowRays", "/RayTracingRL/", { })
        .setLabel("Shadow Rays")
        .cfg().setMin(0);
    gui.addGuiVariable<gui::InputFloat>("RayTracingRlShadowRayJitter", "/RayTracingRL/", { })
        .setLabel("Shadow Jitter")
        .cfg().setStep(0.0001f);

    gui.addGuiVariable<gui::InputInt>("RayTracingRlAoRays", "/RayTracingRL/", { })
        .setLabel("AO Rays")
        .cfg().setMin(0);
    gui.addGuiVariable<gui::InputFloat>("RayTracingRlAoRayRange", "/RayTracingRL/", { })
        .setLabel("AO Range")
        .cfg()
            .setMin(0.0001f)
            .setStep(0.001f);
    gui.addGuiVariable<gui::InputBool>("RayTracingRlAoDoSmooth", "/RayTracingRL/", { })
        .setLabel("Smooth AO");

    gui.addGuiVariable<gui::InputFloat>("RayTracingRlRayDistribution", "/RayTracingRL/", { })
        .setLabel("Ray Distribution")
        .cfg()
            .setMin(0.0001f)
            .setStep(0.01f);

    gui.addGuiVariable<gui::InputBool>("RayTracingRlEnableReflections", "/RayTracingRL/", { })
        .setLabel("Enable Reflections");

    gui.addGuiVariable<gui::InputBool>("RayTracingRlFreezeDeferred", "/RayTracingRL/", { })
        .setLabel("Freeze Deferred");

    gui.addGuiVariable<gui::Button>("RayTracingRlReloadShaders", "/RayTracingRL/", { [&] ()
    { mR.reloadShaders = true; } }).setLabel("Reload Shaders");
}

void BasicRayTracingRL::render(
    const helpers::math::Camera &camera, 
    res::d3d12::D3D12Resource &output, 
    res::d3d12::D3D12CommandList &cmdList)
{
    if (!mRayTracingSupported)
    { return; }

    GPU_PROF_SCOPE(cmdList, "RayTracingRLGpu");

    // Check if shader reloading is required.
    checkReloadShaders();

    // Generate the ray tracing dispatch.
    const auto dispatchDesc{ generateDispatchDesc() };

    // Get command list allowing ray tracing commands.
    auto rtCmdList{ mRenderer.rayTracing().device().toRayTracingCmdList(cmdList) };

    // Update the constant buffers with new camera.
    updateCamera(camera);
    // Update other constants.
    updateConstants();
    // Re-upload constant buffers.
    refreshConstantBuffers();

    // Create views for the currently used deferred buffers.
    prepareDeferredBuffers();

#ifndef ENGINE_PROFILE_RAY_TRACING
#ifdef ENGINE_RESOLVE_USE_COMPUTE
    // We are going to read the buffers from compute shaders.
    mR.deferredBuffers.transitionResourcesTo(cmdList, ::D3D12_RESOURCE_STATE_NON_PIXEL_SHADER_RESOURCE);
#else // ENGINE_RESOLVE_USE_COMPUTE
    // We are going to read the buffers from compute shaders.
    mR.deferredBuffers.transitionResourcesTo(cmdList, ::D3D12_RESOURCE_STATE_NON_PIXEL_SHADER_RESOURCE | ::D3D12_RESOURCE_STATE_PIXEL_SHADER_RESOURCE);
    // Return the output buffers into the original state.
    mR.rayTracingBuffers.transitionResourcesTo(cmdList, ::D3D12_RESOURCE_STATE_UNORDERED_ACCESS);
#endif // ENGINE_RESOLVE_USE_COMPUTE
#endif // !ENGINE_PROFILE_RAY_TRACING
    
    rtCmdList->apply([&] (auto *anyCmdList)
    { // Some actions need to be filed through the fallback command list.
        // Setup the main global root signature common to all shaders.
        cmdList->SetComputeRootSignature(mR.globalRs->get());

        // Descriptor heap containing all of the descriptors used.
        ::ID3D12DescriptorHeap *descriptorHeaps[] = { mR.descHeap->get() };
        anyCmdList->SetDescriptorHeaps(util::count<::UINT>(descriptorHeaps), descriptorHeaps);

        // Set the output buffer.
        cmdList->SetComputeRootDescriptorTable(
            compatBasicRt::GlobalRS::OutputTextures, 
            mR.miscTable.baseHandle());

        // Set the acceleration structure.
        rtCmdList->setTopLevelAccelerationStructure(
            compatBasicRt::GlobalRS::AccelerationStructureSlot, 
            mR.accelerationStructure);

        // Set the slow-changing constants.
        cmdList->SetComputeRootConstantBufferView(
            compatBasicRt::GlobalRS::StaticConstantBuffer, 
            mR.staticConstantsBuffer->gpuAddress());

        // Set the fast-changing constants.
        cmdList->SetComputeRootConstantBufferView(
            compatBasicRt::GlobalRS::DynamicConstantBuffer, 
            mR.dynamicConstantsBuffer->gpuAddress());

        // Set the per-instance constants.
        cmdList->SetComputeRootConstantBufferView(
            compatBasicRt::GlobalRS::InstanceConstantBuffer, 
            mR.instanceConstantsBuffer->gpuAddress());

        // Set the table of material textures.
        cmdList->SetComputeRootDescriptorTable(
            compatBasicRt::GlobalRS::TextureArray, 
            mR.textureTable.baseHandle());

        // Set the table of geometry buffers.
        cmdList->SetComputeRootDescriptorTable(
            compatBasicRt::GlobalRS::BufferArray, 
            mR.bufferTable.baseHandle());

        // Set the noise texture.
        cmdList->SetComputeRootDescriptorTable(
            compatBasicRt::GlobalRS::NoiseTexture, 
            mR.noiseTexture->gpuHandle());

#ifndef ENGINE_PROFILE_RAY_TRACING
        // Set the deferred world-position texture.
        cmdList->SetComputeRootDescriptorTable(
            compatBasicRt::GlobalRS::DeferredBuffers,
            mR.deferredTable.baseHandle());
#endif // !ENGINE_PROFILE_RAY_TRACING

        // Set the correct pipeline state.
        mR.pipeline->get()->setPipelineState(*rtCmdList);

        // Dispatch rays.
        anyCmdList->DispatchRays(&dispatchDesc);
    });

    if (mC.debugMode != DebugMode::FinalColor)
    {
        // Copy the ray tracing result into result buffer.
        mR.rayTracingBuffers.lightingColor->transitionTo(::D3D12_RESOURCE_STATE_COPY_SOURCE, cmdList);
        output.transitionTo(::D3D12_RESOURCE_STATE_COPY_DEST, cmdList);
        cmdList->CopyResource(output.get(), mR.rayTracingBuffers.lightingColor->get());
        mR.rayTracingBuffers.lightingColor->transitionTo(::D3D12_RESOURCE_STATE_UNORDERED_ACCESS, cmdList);
        output.transitionTo(::D3D12_RESOURCE_STATE_RENDER_TARGET, cmdList);
    }
}

void BasicRayTracingRL::updateGui(gui::GuiSubSystem &gui)
{
    auto &modeIndex{ gui.getVariable<gui::Enumeration>("RayTracingRlMode") };
    gui::synchronizeVariables(modeIndex, mC.debugMode);

    auto &sunDirection{ gui.getVariable<gui::Position3>("RayTracingRlSunDirection") };
    mR.staticConstantsChanged |= gui::synchronizeVariables(sunDirection, mR.staticConstants.lightDirection);
    auto &sunColor{ gui.getVariable<gui::Color3>("RayTracingRlSunColor") };
    mR.staticConstantsChanged |= gui::synchronizeVariables(sunColor, mR.staticConstants.lightColor);
    auto &skyColor{ gui.getVariable<gui::Color3>("RayTracingRlSkyColor") };
    mR.staticConstantsChanged |= gui::synchronizeVariables(skyColor, mR.staticConstants.backgroundColor);

    auto &shadowRays{ gui.getVariable<gui::InputInt>("RayTracingRlShadowRays") };
    mR.staticConstantsChanged |= gui::synchronizeVariables(shadowRays, mR.staticConstants.shadowRayCount);
    auto &shadowRayJitter{ gui.getVariable<gui::InputFloat>("RayTracingRlShadowRayJitter") };
    mR.staticConstantsChanged |= gui::synchronizeVariables(shadowRayJitter, mR.staticConstants.shadowRayJitter);
    auto &aoRays{ gui.getVariable<gui::InputInt>("RayTracingRlAoRays") };
    mR.staticConstantsChanged |= gui::synchronizeVariables(aoRays, mR.staticConstants.aoRayCount);
    auto &aoRayRange{ gui.getVariable<gui::InputFloat>("RayTracingRlAoRayRange") };
    mR.staticConstantsChanged |= gui::synchronizeVariables(aoRayRange, mR.staticConstants.aoRayRange);
    auto &aoDoSmooth{ gui.getVariable<gui::InputBool>("RayTracingRlAoDoSmooth") };
    mR.staticConstantsChanged |= gui::synchronizeVariables(aoDoSmooth, mR.staticConstants.aoDoSmooth);
    auto &rayDistribution{ gui.getVariable<gui::InputFloat>("RayTracingRlRayDistribution") };
    mR.staticConstantsChanged |= gui::synchronizeVariables(rayDistribution, mR.staticConstants.rayDistribution);

    auto &enableReflections{ gui.getVariable<gui::InputBool>("RayTracingRlEnableReflections") };
    mR.staticConstantsChanged |= gui::synchronizeVariables(enableReflections, mR.staticConstants.doReflections);

    auto &freezeDeferred{ gui.getVariable<gui::InputBool>("RayTracingRlFreezeDeferred") };
    gui::synchronizeVariables(freezeDeferred, mC.freezeDeferred);
}

void BasicRayTracingRL::setDebugMode(DebugMode mode)
{ mC.debugMode = mode; }

const dxtk::math::Vector3 &BasicRayTracingRL::getLightDirection() const
{ return mR.staticConstants.lightDirection; }

void BasicRayTracingRL::setLightDirection(const dxtk::math::Vector3 &lightDir) 
{
    mR.staticConstants.lightDirection = lightDir;
    mR.staticConstantsChanged = true;
}

const dxtk::math::Vector3 &BasicRayTracingRL::getLightColor() const
{ return mR.staticConstants.lightColor; }

void BasicRayTracingRL::setLightColor(const dxtk::math::Vector3 &lightColor)
{
    mR.staticConstants.lightColor = lightColor;
    mR.staticConstantsChanged = true;
}

const dxtk::math::Vector3 &BasicRayTracingRL::getBackgroundColor() const
{ return mR.staticConstants.backgroundColor; }

uint32_t BasicRayTracingRL::raysPerFrame() const
{
    return raysPerPixel(mC.debugMode, mR.staticConstants.shadowRayCount, 
        mR.staticConstants.aoRayCount, mR.staticConstants.doReflections) * 
        mC.width * mC.height;
}

bool BasicRayTracingRL::deferredBuffersRequired() const
{ return mC.debugMode == DebugMode::FinalColor && !mC.freezeDeferred; }

bool BasicRayTracingRL::resolveRequired() const
{ return mC.debugMode == DebugMode::FinalColor; }

uint32_t BasicRayTracingRL::raysPerPixel() const
{
    return raysPerPixel(mC.debugMode, mR.staticConstants.shadowRayCount, 
        mR.staticConstants.aoRayCount, mR.staticConstants.doReflections);
}

uint32_t BasicRayTracingRL::raysPerPixel(DebugMode mode, uint32_t shadowRays, uint32_t aoRays, bool doReflections)
{
    UNUSED(mode);

    uint32_t result{ 0u };

    // Primary ray only when not using hybrid.
    if (mode != DebugMode::FinalColor)
    { result += 1u; }

    // Shadow rays.
    result += (1u + doReflections) * shadowRays;

    // Reflection ray.
    result += doReflections ? 1u : 0u;

    // Ambient occlusion rays.
    result += aoRays;

    return result;
}

RayTracingBuffers BasicRayTracingRL::getRayTracingBuffers() const
{ return mR.rayTracingBuffers; }

std::string BasicRayTracingRL::accelerationStructureStatisticsString() const
{ return mAccelerationBuilder ? mAccelerationBuilder->generateStatisticsString() : ""; }

const helpers::d3d12::D3D12RTAccelerationBuilder::Statistics &BasicRayTracingRL::accelerationStructureStatistics() const
{ ASSERT_SLOW(mAccelerationBuilder); return mAccelerationBuilder->statistics(); } 

void BasicRayTracingRL::setBlasDuplication(bool enabled)
{ mC.blasDuplication = enabled; }

void BasicRayTracingRL::setBuildOptimized(bool enabled)
{ mC.buildOptimized = enabled; }

void BasicRayTracingRL::createRootSignatures(uint32_t requiredTextures, uint32_t requiredBuffers)
{
    helpers::d3d12::D3D12RootSignatureBuilder rsb;

    { // Root signature used for global shader parameters.
        rsb.clear();

        // Binding for output render targets, starting at register(u0, space0).
        const ::CD3DX12_DESCRIPTOR_RANGE1 rtvRange[]{
            { // Render-target-view: 
                ::D3D12_DESCRIPTOR_RANGE_TYPE_UAV, 
                RayTracingBufferViews::RAY_TRACING_BUFFER_COUNT, 
                // Starting at register(u0, space0)
                0u, 0u, 
                ::D3D12_DESCRIPTOR_RANGE_FLAG_DESCRIPTORS_VOLATILE
            }
        };
        rsb[compatBasicRt::GlobalRS::OutputTextures].asDescriptorTableParameter(
            util::count<::UINT>(rtvRange), rtvRange);

        // Single slot for acceleration structure.
        rsb[compatBasicRt::GlobalRS::AccelerationStructureSlot].asShaderResourceViewParameter(
			compatBasicRt::AccelerationStructureSlot.reg, compatBasicRt::AccelerationStructureSlot.space);

        // Static, slow-changing constants, register(b0, space0).
        rsb[compatBasicRt::GlobalRS::StaticConstantBuffer].asConstantBufferViewParameter(0u, 0u);
        // Dynamic, fast-changing constants, register(b1, space0).
        rsb[compatBasicRt::GlobalRS::DynamicConstantBuffer].asConstantBufferViewParameter(1u, 0u);

        // Per-instance data, register(b2, space0);
        rsb[compatBasicRt::GlobalRS::InstanceConstantBuffer].asConstantBufferViewParameter(2u, 0u);

        // Array of material textures.
        const ::CD3DX12_DESCRIPTOR_RANGE1 textureRange[]{
            { // Texture array SRVs: 
                ::D3D12_DESCRIPTOR_RANGE_TYPE_SRV, 
                // Bounded number of descriptors
                std::max<uint32_t>(requiredTextures, HLSL_TEXTURE_UPPER_BOUND), 
				compatBasicRt::TexturesSlot.reg, compatBasicRt::TexturesSlot.space, 
                ::D3D12_DESCRIPTOR_RANGE_FLAG_DATA_STATIC
            }
        };
        rsb[compatBasicRt::GlobalRS::TextureArray].asDescriptorTableParameter(
            util::count<::UINT>(textureRange), textureRange);

        // Array of geometry buffers.
        const ::CD3DX12_DESCRIPTOR_RANGE1 bufferRange[]{
            { // Buffer array SRVs: 
                ::D3D12_DESCRIPTOR_RANGE_TYPE_SRV, 
                // Bounded number of descriptors
                std::max<uint32_t>(requiredBuffers, HLSL_BUFFER_UPPER_BOUND), 
				compatBasicRt::BuffersSlot.reg, compatBasicRt::BuffersSlot.space, 
                ::D3D12_DESCRIPTOR_RANGE_FLAG_DATA_STATIC
            }
        };
        rsb[compatBasicRt::GlobalRS::BufferArray].asDescriptorTableParameter(
            util::count<::UINT>(bufferRange), bufferRange);

        // Common sampler used for sampling material textures.
        const ::CD3DX12_STATIC_SAMPLER_DESC commonSampler{ 
            compatBasicRt::SamplersSlot.reg, 
			//::D3D12_FILTER_ANISOTROPIC, 
			::D3D12_FILTER_MIN_MAG_MIP_LINEAR, 
            ::D3D12_TEXTURE_ADDRESS_MODE_WRAP, 
            ::D3D12_TEXTURE_ADDRESS_MODE_WRAP, 
            ::D3D12_TEXTURE_ADDRESS_MODE_WRAP, 
            0.0f, 16u,
            ::D3D12_COMPARISON_FUNC_LESS_EQUAL,
            ::D3D12_STATIC_BORDER_COLOR_OPAQUE_WHITE,
            0.0f, D3D12_FLOAT32_MAX,
            ::D3D12_SHADER_VISIBILITY_ALL,
            compatBasicRt::SamplersSlot.space
        };
        rsb.addStaticSampler(commonSampler);

        // Noise texture used for random values, register(t0, space1).
        const ::CD3DX12_DESCRIPTOR_RANGE1 noiseTextureRange[]{
            { // Texture array SRVs: 
                ::D3D12_DESCRIPTOR_RANGE_TYPE_SRV, 
                1u, 
                // register(t0, space1)
                0u, 1u, 
                ::D3D12_DESCRIPTOR_RANGE_FLAG_DATA_STATIC
            }
        };
        rsb[compatBasicRt::GlobalRS::NoiseTexture].asDescriptorTableParameter(
            util::count<::UINT>(noiseTextureRange), noiseTextureRange);

        // Deferred textures, starting at register(t0, space98).
#ifndef ENGINE_PROFILE_RAY_TRACING
        const ::CD3DX12_DESCRIPTOR_RANGE1 deferredTextureRange[]{
            { // Texture array SRVs: 
                ::D3D12_DESCRIPTOR_RANGE_TYPE_SRV, 
                DeferredBufferViews::DEFERRED_BUFFER_COUNT, 
                // register(t0, space98)
				compatBasicRt::DeferredBuffersSlot.reg, compatBasicRt::DeferredBuffersSlot.space, 
                ::D3D12_DESCRIPTOR_RANGE_FLAG_DATA_STATIC
            }
        };
        rsb[compatBasicRt::GlobalRS::DeferredBuffers].asDescriptorTableParameter(
            util::count<::UINT>(deferredTextureRange), deferredTextureRange);
#endif // !ENGINE_PROFILE_RAY_TRACING

        // Finalize and build the global root signature.
        mR.globalRs = rsb.build(mRenderer.rayTracing().device());
    }

    { // Root signature used for hit group specific parameters.
        rsb.clear();

        // Constant buffer used for specification of material and mesh, register(b0, space100).
        rsb[compatBasicRt::HitGroupLocalRS::GeometryConstantBuffer].asConstantParameter(
            static_cast<::UINT>(util::sizeInUint32<compatBasicRt::GeometryConstantBuffer>()),
            compatBasicRt::GeometrySlot.reg, compatBasicRt::GeometrySlot.space);

        // Notify D3D12 this is a local root signature.
        rsb.setFlags(::D3D12_ROOT_SIGNATURE_FLAG_LOCAL_ROOT_SIGNATURE);

        // Finalize and build the local root signature.
        mR.hitGroupLocalRs = rsb.build(mRenderer.rayTracing().device());
    }

    { // Empty local root signature.
        rsb.clear();

        // Notify D3D12 this is a local root signature.
        rsb.setFlags(::D3D12_ROOT_SIGNATURE_FLAG_LOCAL_ROOT_SIGNATURE);

        // Finalize and build the local root signature.
        mR.emptyLocalRs = rsb.build(mRenderer.rayTracing().device());
    }
}

void BasicRayTracingRL::loadDefaultShaders()
{
#ifdef ENGINE_PROFILE_RAY_TRACING
    mR.rtShaders = res::d3d12::D3D12Shader::create(gBasicRayTracingProf, util::count(gBasicRayTracingProf));
#else // ENGINE_PROFILE_RAY_TRACING
    mR.rtShaders = res::d3d12::D3D12Shader::create(gBasicRayTracing, util::count(gBasicRayTracing));
#endif // ENGINE_PROFILE_RAY_TRACING
}

void BasicRayTracingRL::createPipeline()
{
    PROF_SCOPE("PipelineBuild");

    // Clean up any previous configuration.
    mR.pipeline = helpers::d3d12::D3D12RTPipeline::create();

#ifdef ENGINE_PROFILE_RAY_TRACING
    // Add ray generation, miss and closest hit shaders.
    mR.pipeline->addShaderLibrary(mR.rtShaders->blob()->GetBufferPointer(), mR.rtShaders->blob()->GetBufferSize(), 
        { MAIN_RAY_GEN_NAME, MAIN_MISS_NAME, MAIN_CLOSEST_HIT_NAME, SHADOW_MISS_NAME });
#else // ENGINE_PROFILE_RAY_TRACING
    // Add ray generation, miss and closest hit shaders.
    mR.pipeline->addShaderLibrary(mR.rtShaders->blob()->GetBufferPointer(), mR.rtShaders->blob()->GetBufferSize(), 
        { 
            MAIN_RAY_GEN_NAME, MAIN_MISS_NAME, MAIN_CLOSEST_HIT_NAME, 
            SHADOW_MISS_NAME, 
            DISTANCE_CLOSEST_HIT_NAME, DISTANCE_MISS_NAME 
        });
#endif // ENGINE_PROFILE_RAY_TRACING

    // Add the main hit group, containing only a closest hit shader.
    mR.pipeline->addHitGroup(MAIN_HIT_GROUP_NAME, MAIN_CLOSEST_HIT_NAME);

    // Use the main global root signature for all shader stages.
    mR.pipeline->useGlobalRootSignature(*mR.globalRs, 
        { MAIN_RAY_GEN_NAME, MAIN_MISS_NAME, MAIN_CLOSEST_HIT_NAME, SHADOW_MISS_NAME });
    // Use the hit group local root signature to pass geometry information.
    mR.pipeline->useLocalRootSignature(*mR.hitGroupLocalRs, 
        { MAIN_CLOSEST_HIT_NAME });
    // Use empty local root signature for the rest.
    mR.pipeline->useLocalRootSignature(*mR.emptyLocalRs, 
        { MAIN_RAY_GEN_NAME, MAIN_MISS_NAME, SHADOW_MISS_NAME });

#ifndef ENGINE_PROFILE_RAY_TRACING
    // Add the distance Hit Group.
    mR.pipeline->addHitGroup(DISTANCE_HIT_GROUP_NAME, DISTANCE_CLOSEST_HIT_NAME);

    // Use the main global root signature for distance shaders.
    mR.pipeline->useGlobalRootSignature(*mR.globalRs, 
        { DISTANCE_CLOSEST_HIT_NAME, DISTANCE_MISS_NAME });

    // Use empty local root signature, since we don't need any more data.
    mR.pipeline->useLocalRootSignature(*mR.emptyLocalRs, 
        { DISTANCE_CLOSEST_HIT_NAME, DISTANCE_MISS_NAME });
#endif // !ENGINE_PROFILE_RAY_TRACING

    // Ray payload is RGBA -> 4 * float.
    mR.pipeline->setMaxPayloadSize(MAIN_MAX_PAYLOAD_SIZE);
    // Attributes are 2 of the barycentric coordinates -> 2 * float.
    mR.pipeline->setMaxAttributeSize(MAIN_MAX_ATTRIBUTES_SIZE);
    // No recursion, only primary rays.
    mR.pipeline->setMaxRecursiveDepth(MAIN_MAX_RECURSION_DEPTH);

    // Finalize and build the pipeline.
    mR.pipeline->build(mRenderer.rayTracing().device());
}

res::d3d12::D3D12DescHeap::PtrT BasicRayTracingRL::createDescHeap(std::size_t requiredDescriptors)
{
    return res::d3d12::D3D12DescHeap::create(mRenderer.device(), 
        ::D3D12_DESCRIPTOR_HEAP_TYPE_CBV_SRV_UAV,
        static_cast<::uint32_t>(requiredDescriptors),
        ::D3D12_DESCRIPTOR_HEAP_FLAG_SHADER_VISIBLE);
}

res::d3d12::D3D12Resource::PtrT BasicRayTracingRL::createShaderTableBuffer(std::size_t sizeInBytes)
{
    auto buffer{ helpers::d3d12::D3D12UploadBuffer::create() };
    buffer->allocate(mRenderer.device(), sizeInBytes, 
        ::D3D12_RESOURCE_STATE_GENERIC_READ, L"ShaderTable");

    return buffer;
}

void BasicRayTracingRL::createResizeTextures(uint32_t width, uint32_t height)
{
    auto alloc{ helpers::d3d12::D3D12CommittedAllocator::create(
        mRenderer.device(),
        ::CD3DX12_HEAP_PROPERTIES(::D3D12_HEAP_TYPE_DEFAULT),
        ::D3D12_HEAP_FLAG_NONE) };

    auto textureDesc{ ::CD3DX12_RESOURCE_DESC::Tex2D(::DXGI_FORMAT_UNKNOWN, width, height, 1u, 1u) };
    textureDesc.Flags = ::D3D12_RESOURCE_FLAG_ALLOW_UNORDERED_ACCESS;

    if (!mR.rayTracingBuffers.ambientOcclusion)
    { mR.rayTracingBuffers.ambientOcclusion = helpers::d3d12::D3D12TextureBuffer::create(); }
    textureDesc.Format = DEFAULT_AMBIENT_OCCLUSION_RT_FORMAT;
    mR.rayTracingBuffers.ambientOcclusion->allocate(alloc, textureDesc, 
        ::D3D12_RESOURCE_STATE_UNORDERED_ACCESS, nullptr, L"RayTracingAo");

    if (!mR.rayTracingBuffers.shadowOcclusion)
    { mR.rayTracingBuffers.shadowOcclusion = helpers::d3d12::D3D12TextureBuffer::create(); }
    textureDesc.Format = DEFAULT_SHADOW_OCCLUSION_RT_FORMAT;
    mR.rayTracingBuffers.shadowOcclusion->allocate(alloc, textureDesc, 
        ::D3D12_RESOURCE_STATE_UNORDERED_ACCESS, nullptr, L"RayTracingShadows");

    if (!mR.rayTracingBuffers.lightingColor)
    { mR.rayTracingBuffers.lightingColor = helpers::d3d12::D3D12TextureBuffer::create(); }
    textureDesc.Format = DEFAULT_LIGHTING_COLOR_RT_FORMAT;
    mR.rayTracingBuffers.lightingColor->allocate(alloc, textureDesc, 
        ::D3D12_RESOURCE_STATE_UNORDERED_ACCESS, nullptr, L"RayTracingLighting");

    mC.width = width;
    mC.height = height;
}

void BasicRayTracingRL::createTextureDescriptors()
{ fillMiscTable(mR.miscTable, mR.rayTracingBuffers, *mR.staticConstantsBuffer, *mR.dynamicConstantsBuffer, *mR.noiseTexture); }

void BasicRayTracingRL::createConstantBuffers()
{
    mR.staticConstantsBuffer = StaticConstantsBuffer::create();
    mR.staticConstants.lightDirection = dxtk::math::Vector3{ 
        DEFAULT_LIGHT_DIR_X, DEFAULT_LIGHT_DIR_Y, DEFAULT_LIGHT_DIR_Z };
    mR.staticConstants.lightColor = dxtk::math::Vector3{ 
        DEFAULT_LIGHT_COLOR_R, DEFAULT_LIGHT_COLOR_G, DEFAULT_LIGHT_COLOR_B };
    mR.staticConstants.backgroundColor = dxtk::math::Vector3{ 
        DEFAULT_BACKGROUND_COLOR_R, DEFAULT_BACKGROUND_COLOR_G, DEFAULT_BACKGROUND_COLOR_B };
    mR.staticConstants.aoRayCount = DEFAULT_AO_RAY_COUNT;
    mR.staticConstants.shadowRayCount = DEFAULT_SHADOW_RAY_COUNT;
    mR.staticConstants.shadowRayJitter = DEFAULT_SHADOW_RAY_JITTER;
    mR.staticConstants.aoRayRange = DEFAULT_AO_RAY_RANGE;
    mR.staticConstants.aoDoSmooth = DEFAULT_AO_USE_SMOOTH;
    mR.staticConstants.rayDistribution = DEFAULT_RAY_DISTRIBUTION;
    mR.staticConstants.doReflections = DEFAULT_DO_REFLECTIONS;
    mR.staticConstantsBuffer->at(0u) = mR.staticConstants;
    mR.staticConstantsBuffer->reallocate(mRenderer.device(), L"StaticConstantsBuffer");

    mR.dynamicConstantsBuffer = DynamicConstantsBuffer::create();
    mR.dynamicConstantsBuffer->at(0u) = mR.dynamicConstants;
    mR.dynamicConstantsBuffer->reallocate(mRenderer.device(), L"DynamicConstantsBuffer");

    // Dummy data...
    mR.instanceConstantsBuffer = InstanceConstantsBuffer::create();
    mR.instanceConstantsBuffer->at(0u) = { };
    mR.instanceConstantsBuffer->reallocate(mRenderer.device(), L"InstanceConstantsBuffer");
}

void BasicRayTracingRL::createNoiseTexture(uint32_t width, uint32_t height, 
    res::d3d12::D3D12CommandList &directCmdList)
{
    // Perform only once.
    if (mR.noiseTexture)
    { return; }

    // float3 -> 3 components.
    static constexpr std::size_t COMPONENTS_PER_ELEMENT{ 3u };

    // Initialize the random number generation.
    std::random_device randomDev;
    std::mt19937 randomEngine(randomDev());
    std::uniform_real_distribution<float> randomDist(0.0f, 1.0f);

    // Initialize holder of texture data.
    std::vector<float> textureData;
    textureData.resize(width * height * COMPONENTS_PER_ELEMENT);

    // Initialize random texture values.
    for (auto &val : textureData)
    { val = randomDist(randomEngine); }

    // Upload the texture to the GPU.
    mR.noiseTexture = helpers::d3d12::D3D12TextureBuffer::create();
    mR.noiseTexture->createFromData(
        textureData.begin(), textureData.end(), 
        rndr::ElementFormat{
            rndr::ComponentFormat::Float32, 
            COMPONENTS_PER_ELEMENT 
        }, 
        width, height,
        directCmdList, 
        mRenderer.device(), 
        mRenderer.memoryMgr().updater(), 
        L"NoiseTexture");
    mR.noiseTexture->transitionTo(::D3D12_RESOURCE_STATE_NON_PIXEL_SHADER_RESOURCE, directCmdList);
}

void BasicRayTracingRL::initializeResources(uint32_t requiredTextures, uint32_t requiredBuffers)
{
    PROF_SCOPE("RayTracingRLInitialization");

    if (!mRenderer.rayTracing().rayTracingSupported())
    {
        log<Warning>() << "Unable to initialize ray tracing rendering layer, "
            "since Ray Tracing is not supported!" << std::endl;
        mRayTracingSupported = false;
        return;
    }

    // Create root signatures used by ray tracing shaders.
    createRootSignatures(requiredTextures, requiredBuffers);

    // Load the default pre-compiled shaders.
    loadDefaultShaders();

    // Create the main ray tracing pipeline.
    createPipeline();

    // Create output texture used for storing ray tracing output.
    createResizeTextures();

    // Create static and dynamic constant buffers.
    createConstantBuffers();

    mRayTracingSupported = true;
}

void BasicRayTracingRL::prepareAccelerationStructures(
    helpers::d3d12::D3D12RTAccelerationBuilder::PtrT &builder,
    const res::scene::DrawableList &drawables, bool blasDuplication)
{
    PROF_SCOPE("PrepareAccelerationStructures");

    // Clean-up the last build.
    if (builder)
    { 
        // Clear instances, if we are not profiling AS.
        if (mC.blasDuplication)
        { builder->clear(); }
        else
        { builder->clearInstances(); }
    }
    else
    { // First time -> create the builder
        builder = helpers::d3d12::D3D12RTAccelerationBuilder::create(
            mRenderer.rayTracing().device(), mRenderer.memoryMgr());

        if (mRenderer.cfg().renderRayTracingFastBuildAS)
        { builder->preferFastBuild(); }
        else
        { builder->preferFastRayTracing(); }
    }

    // Primitive index counter, through all meshes.
    ::UINT primitiveIndex{ 0u };
    // Index of the instance, used for per-instance parameters.
    ::UINT instanceIndex{ 0u };

    for (auto &drawable : drawables)
    {
        // Add or get already added geometry handle for current mesh.
        const auto geometryHandle{ builder->addGeometryNoTransform(
            drawable.meshHandle, 
            util::toWString(drawable.meshHandle.getHolder().id()), 
            mRenderer.cfg().renderRayTracingFastBuildAS, 
            !blasDuplication) };

        // Add an instance of the mesh to the scene.
        builder->addInstance(geometryHandle, drawable.modelMatrix, primitiveIndex, instanceIndex);

        // TODO - Change from per-instance to per-geometry indexing.
        // Added given amount of primitives.
        primitiveIndex += static_cast<::UINT>(drawable.meshHandle->primitives.size()) * HLSL_HIT_GROUP_STRIDE;

        // TODO - Find a better solution?
        // Distance hit group.
        primitiveIndex += 1u;

        // Move to the next instance.
        instanceIndex++;
    }
}

void BasicRayTracingRL::prepareMaterialMeshCache(helpers::rndr::MaterialMeshCache::PtrT &cache,
    const res::scene::DrawableList &drawables)
{
    PROF_SCOPE("PrepareCache");

    if (!cache)
    { // First time -> create it!
        cache = helpers::rndr::MaterialMeshCache::create();
    }
    
    // TODO - Fix on GTX 970.
    //cache->clear();

    for (const auto &drawable : drawables)
    { // Process all drawables.
        cache->registerMesh(drawable.meshHandle);
    }
}

uint32_t BasicRayTracingRL::calculateMiscDescriptorCount()
{
    auto result{ 0u };

    // Ray tracing output ambient occlusion: 
    result += 1u;
    // Ray tracing output shadow occlusion: 
    result += 1u;
    // Ray tracing output lighting color: 
    result += 1u;
    // Slow-changing constants buffer: 
    result += 1u;
    // Fast-changing constants buffer: 
    result += 1u;
    // Random noise texture: 
    result += 1u;

    ASSERT_FAST(result == compatBasicRt::DescHeap::MiscDescTable::DescHeapSize);

    return result;
}

helpers::d3d12::D3D12DescTable BasicRayTracingRL::createMiscTable(
    res::d3d12::D3D12DescHeap &descHeap, uint32_t numDescriptors)
{
    return helpers::d3d12::D3D12DescTable(
        descHeap, descHeap.allocateTable(numDescriptors));
}

uint32_t BasicRayTracingRL::calculateDeferredDescriptorCount()
{ return DeferredBufferViews::DEFERRED_BUFFER_COUNT; }

helpers::d3d12::D3D12DescTable BasicRayTracingRL::createDeferredTable(
    res::d3d12::D3D12DescHeap &descHeap, uint32_t numDescriptors)
{
    return helpers::d3d12::D3D12DescTable(
        descHeap, descHeap.allocateTable(numDescriptors));
}

uint32_t BasicRayTracingRL::calculateTextureDescriptorCount(
    helpers::rndr::MaterialMeshCache &cache)
{
    /*
	UNUSED(cache);
    ASSERT_FAST(cache.calculateSizeRequirements().textureDescriptors <= HLSL_TEXTURE_UPPER_BOUND);
    return HLSL_TEXTURE_UPPER_BOUND;
    */
    return std::max<uint32_t>(cache.calculateSizeRequirements().textureDescriptors, HLSL_TEXTURE_UPPER_BOUND);
}

helpers::d3d12::D3D12DescTable BasicRayTracingRL::createTextureTable(
    res::d3d12::D3D12DescHeap &descHeap, uint32_t numDescriptors)
{
    // Create the table and initialize null handles, so we can debug.
    helpers::d3d12::D3D12DescTable result(
        descHeap, descHeap.allocateTable(numDescriptors));
    result.initializeSrvNullHandles(::D3D12_SRV_DIMENSION_TEXTURE2D);

    return result;
}

uint32_t BasicRayTracingRL::calculateBufferDescriptorCount(
    helpers::rndr::MaterialMeshCache &cache)
{
    /*
	UNUSED(cache);
    ASSERT_FAST(cache.calculateSizeRequirements().bufferDescriptors <= HLSL_BUFFER_UPPER_BOUND);
    return HLSL_BUFFER_UPPER_BOUND;
    */
    return std::max<uint32_t>(cache.calculateSizeRequirements().bufferDescriptors, HLSL_BUFFER_UPPER_BOUND);
}

helpers::d3d12::D3D12DescTable BasicRayTracingRL::createBufferTable(
    res::d3d12::D3D12DescHeap &descHeap, uint32_t numDescriptors)
{
    // Create the table and initialize null handles, so we can debug.
    helpers::d3d12::D3D12DescTable result(
        descHeap, descHeap.allocateTable(numDescriptors));
    result.initializeSrvNullHandles(::D3D12_SRV_DIMENSION_BUFFER);

    return result;
}

uint32_t BasicRayTracingRL::calculateBuildDescriptorCount(
    const helpers::d3d12::D3D12RTAccelerationBuilder &builder)
{ return builder.requiredDescriptors(); }

helpers::d3d12::D3D12DescTable BasicRayTracingRL::createBuildTable(
    res::d3d12::D3D12DescHeap &descHeap, uint32_t numDescriptors)
{ return helpers::d3d12::D3D12DescTable(descHeap, descHeap.allocateTable(numDescriptors)); }

void BasicRayTracingRL::createDescHeapLayout(
    const helpers::d3d12::D3D12RTAccelerationBuilder &builder, helpers::rndr::MaterialMeshCache &cache)
{
    // Calculate descriptor requirements.
    const auto miscDescriptors{ calculateMiscDescriptorCount() };
    const auto deferredDescriptors{ calculateDeferredDescriptorCount() };
    const auto textureDescriptors{ calculateTextureDescriptorCount(cache) };
    const auto bufferDescriptors{ calculateBufferDescriptorCount(cache) };
    const auto buildDescriptors{ calculateBuildDescriptorCount(builder) };
    const auto totalDescriptors{ 
        miscDescriptors + deferredDescriptors + textureDescriptors + bufferDescriptors + buildDescriptors };

    // Check whether we need to create a new descriptor heap.
    bool newDescHeapRequired{ false };
    // TODO - Fix on GTX 970.
    //newDescHeapRequired |= true;
    newDescHeapRequired |= miscDescriptors != mR.miscTable.size();
    newDescHeapRequired |= deferredDescriptors != mR.deferredTable.size();
    newDescHeapRequired |= textureDescriptors != mR.textureTable.size();
    newDescHeapRequired |= bufferDescriptors != mR.bufferTable.size();
    newDescHeapRequired |= buildDescriptors != mR.buildTable.size();

    if (!newDescHeapRequired)
    { // No changed -> we don't need a new descriptor heap.
        return;
    }

    // Create the main descriptor heap, which will be used for all descriptors.
    auto newDescHeap{ createDescHeap(totalDescriptors) };

    // Create descriptor table for miscellaneous descriptors.
    auto miscTable{ createMiscTable(*newDescHeap, miscDescriptors) };
    // Create descriptor table for deferred descriptors.
    auto deferredTable{ createDeferredTable(*newDescHeap, deferredDescriptors) };
    // Create descriptor table for texture SRV descriptors.
    auto textureTable{ createTextureTable(*newDescHeap, textureDescriptors) };
    // Create descriptor table for buffer SRV descriptors.
    auto bufferTable{ createBufferTable(*newDescHeap, bufferDescriptors) };
    // Create descriptor table for AS building.
    auto buildTable{ createBuildTable(*newDescHeap, buildDescriptors) };

    // Copy over the original descriptor tables: 
    // Misc/deferred tables do not need to be copied, since we use reallocateDescriptor().
    // TODO - Fix on GTX 970.
    //textureTable.copyFrom(mR.textureTable);
    //bufferTable.copyFrom(mR.bufferTable);
    //buildTable.copyFrom(mR.buildTable);

    // Change over to the new descriptor heap.
    mR.descHeap = newDescHeap;
    mR.miscTable = std::move(miscTable);
    mR.deferredTable = std::move(deferredTable);
    mR.textureTable = std::move(textureTable);
    mR.bufferTable = std::move(bufferTable);
    mR.buildTable = std::move(buildTable);
}

void BasicRayTracingRL::fillMiscTable(
    helpers::d3d12::D3D12DescTable &miscTable, RayTracingBuffers &buffers, 
    StaticConstantsBuffer &staticConstants, DynamicConstantsBuffer &dynamicConstants, 
    helpers::d3d12::D3D12TextureBuffer &noiseTexture)
{
    buffers.ambientOcclusion->reallocateDescriptor(miscTable);
    buffers.shadowOcclusion->reallocateDescriptor(miscTable);
    buffers.lightingColor->reallocateDescriptor(miscTable);

    staticConstants.reallocateDescriptor(miscTable);
    dynamicConstants.reallocateDescriptor(miscTable);
    noiseTexture.reallocateDescriptor(miscTable);
}

void BasicRayTracingRL::buildMaterialMeshCache(helpers::rndr::MaterialMeshCache &cache,
    helpers::d3d12::D3D12DescTable &textureTable, helpers::d3d12::D3D12DescTable &bufferTable)
{
    PROF_SCOPE("BuildCache");
    cache.buildCache(textureTable, bufferTable);
}

::WRAPPED_GPU_POINTER BasicRayTracingRL::buildAccelerationStructures(
    helpers::d3d12::D3D12RTAccelerationBuilder &builder, helpers::d3d12::D3D12DescTable &buildTable,
    res::d3d12::D3D12CommandList &cmdList)
{
    PROF_SCOPE("BuildAccelerationStructures");
    builder.buildAccelerationStructure(cmdList, buildTable);
    return builder.getTopLevelAccelerationStruture();
}

res::d3d12::D3D12Resource::PtrT BasicRayTracingRL::createShaderTables(
    helpers::d3d12::D3D12RTShaderTable &shaderTable, const helpers::rndr::MaterialMeshCache &cache, 
    helpers::d3d12::D3D12RTPipeline &pipeline, const res::scene::DrawableList &drawables)
{
    PROF_SCOPE("BuildShaderTables");

    // Clean up any previous configuration.
    shaderTable.clear();

    // Create the ray generation record, no additional data required.
    shaderTable.addRayGenerationGroup(MAIN_RAY_GEN_NAME);

    // Miss record, no additional data required.
    shaderTable.addMissGroup(MAIN_MISS_NAME);

    // Shadow miss record.
    shaderTable.addMissGroup(SHADOW_MISS_NAME);

#ifndef ENGINE_PROFILE_RAY_TRACING
    // Distance miss record.
    shaderTable.addMissGroup(DISTANCE_MISS_NAME);
#endif // !ENGINE_PROFILE_RAY_TRACING

    // Create Hit Group records, which require material and mesh specification.
    for (const auto &drawable : drawables)
    { // Go through all of the drawables.
        const auto &meshHandle{ drawable.meshHandle };

#ifndef ENGINE_PROFILE_RAY_TRACING
        // Distance Hit Group.
        shaderTable.addHitGroup(DISTANCE_HIT_GROUP_NAME);
#endif // !ENGINE_PROFILE_RAY_TRACING

        for (const auto &materialMeshSpec : cache.iterableForMesh(meshHandle))
        { // Each primitive gets its own record.
            compatBasicRt::GeometryConstantBuffer specification{ };

            // Specify buffers.
            specification.mesh.indices = materialMeshSpec.mesh.indices;
            specification.mesh.positions = materialMeshSpec.mesh.positions;
            specification.mesh.texCoords = materialMeshSpec.mesh.texCoords;
            specification.mesh.normals = materialMeshSpec.mesh.normals;
            specification.mesh.tangents = materialMeshSpec.mesh.tangents;

            // Specify textures.
            specification.material.colorTextureIndex = materialMeshSpec.material.colorTextureIndex;
            specification.material.metallicRoughnessTextureIndex = materialMeshSpec.material.metallicRoughnessTextureIndex;
            specification.material.normalTextureIndex = materialMeshSpec.material.normalTextureIndex;
            specification.material.specularReflections = materialMeshSpec.material.specularReflections;
            specification.material.specularRefractions = materialMeshSpec.material.specularRefractions;

            // Create the record, specifying material and mesh.
            shaderTable.addHitGroup(MAIN_HIT_GROUP_NAME, specification);
        }
    }

    // Create GPU buffer which will hold the shader table data.
    const auto shaderTableSize{ 
        shaderTable.calculateSizeRequirements(mRenderer.rayTracing().device()) };
    auto shaderTableBuffer{ createShaderTableBuffer(shaderTableSize) };

    // Finalize and build the shader table.
    shaderTable.build(mRenderer.rayTracing().device(), 
        *pipeline.get(), *shaderTableBuffer);

    return shaderTableBuffer;
}

std::vector<compatBasicRt::PerInstanceData> BasicRayTracingRL::preparePerInstanceData(
    const res::scene::DrawableList &drawables)
{
    std::vector<compatBasicRt::PerInstanceData> result{ };

    for (const auto &drawable : drawables)
    { // Each drawable gets its own record.
        compatBasicRt::PerInstanceData data{ };

        // Calculate normal transform matrix.
        data.normalToWorld = drawable.modelMatrix.normalMatrix();

        result.emplace_back(data);
    }

    return result;
}

void BasicRayTracingRL::prepareForRendering(res::scene::DrawableList &drawables,
    res::d3d12::D3D12CommandList &directCmdList, bool sceneChanged)
{
    if (sceneChanged)
    { // Throwing away the current scene.
        if (mR.materialMeshCache)
        { // Purge materials and meshes.
            mR.materialMeshCache->clear();
            mR.textureTable.clear();
            mR.bufferTable.clear();
        }

        if (mAccelerationBuilder)
        { // Purge acceleration structures.
            mAccelerationBuilder->clear();
            mR.buildTable.clear();
        }
    }

    // Prepare the builder for building AS for new list of drawables.
    prepareAccelerationStructures(mAccelerationBuilder, drawables, mC.blasDuplication);

    // Prepare all materials and meshes used for rendering.
    prepareMaterialMeshCache(mR.materialMeshCache, drawables);

    // Create new root signature with current texture and buffer counts.
    createRootSignatures(
        mR.materialMeshCache->calculateSizeRequirements().textureDescriptors, 
        mR.materialMeshCache->calculateSizeRequirements().bufferDescriptors);

    // Fix for dangling Ray Tracing state object in Nvidia driver.
    mR.pipelineBck = mR.pipeline;

    // Create the main ray tracing pipeline, using the new root signature.
    createPipeline();

    // Create random noise texture used for random number generation.
    createNoiseTexture(NOISE_TEXTURE_SIZE, NOISE_TEXTURE_SIZE, directCmdList);

    // Create the main descriptor heap and divide it into tables.
    createDescHeapLayout(*mAccelerationBuilder, *mR.materialMeshCache);

    // Create descriptors in the miscellaneous descriptor table.
    fillMiscTable(mR.miscTable, 
        mR.rayTracingBuffers, 
        *mR.staticConstantsBuffer, *mR.dynamicConstantsBuffer, 
        *mR.noiseTexture);

    // Finalize and create material/mesh descriptors.
    buildMaterialMeshCache(*mR.materialMeshCache, mR.textureTable, mR.bufferTable);

    // Finalize building of the acceleration structure.
    mR.accelerationStructure = buildAccelerationStructures(*mAccelerationBuilder, mR.buildTable, directCmdList);

    // Create shader tables used when ray tracing.
    mR.shaderTableBuffer = createShaderTables(mR.shaderTable, *mR.materialMeshCache, *mR.pipeline, drawables);

    // Prepare per-instance data for the current list of drawables.
    mR.instanceConstants = preparePerInstanceData(drawables);
    // And upload them to the GPU.
    uploadPerInstanceData(mR.instanceConstants);

    // We have prepared the resources for rendering.
    mPrepared = true;

    // Print a sample of dispatch description.
    log<Info>() << "Sample of Ray Tracing Dispatch: " << std::endl;
    printDispatchInfo(generateDispatchDesc());
}

void BasicRayTracingRL::updateCamera(const helpers::math::Camera &camera)
{
    mR.dynamicConstants.cameraToWorld = camera.rayTracingVP();
    mR.dynamicConstants.cameraWorldPosition = camera.position();

    mR.dynamicConstants.canvasResolution.x = static_cast<float>(mC.width);
    mR.dynamicConstants.canvasResolution.y = static_cast<float>(mC.height);

    mR.dynamicConstantsChanged = true;
}

void BasicRayTracingRL::updateConstants()
{
    mR.staticConstants.mode = debugModeToShaderMode(mC.debugMode);

    mR.staticConstantsChanged = true;
}

void BasicRayTracingRL::refreshConstantBuffers()
{
    if (mR.staticConstantsChanged)
    {
        mR.staticConstantsBuffer->at(0u) = mR.staticConstants;
        mR.staticConstantsBuffer->uploadToGPU();
        mR.staticConstantsChanged = false;
    }

    if (mR.dynamicConstantsChanged)
    {
        mR.dynamicConstantsBuffer->at(0u) = mR.dynamicConstants;
        mR.dynamicConstantsBuffer->uploadToGPU();
        mR.dynamicConstantsChanged = false;
    }
}

void BasicRayTracingRL::prepareDeferredBuffers()
{ mR.deferredViews.createViews(mR.deferredBuffers, mR.deferredTable, ResourceViewType::ShaderResource); }

void BasicRayTracingRL::uploadPerInstanceData(const std::vector<compatBasicRt::PerInstanceData> &instanceConstants)
{
    mR.instanceConstantsBuffer->assignCopy(instanceConstants);
    mR.instanceConstantsBuffer->reallocate(mRenderer.device(), L"InstanceConstantsBuffer");
    mR.instanceConstantsBuffer->uploadToGPU();
}

bool BasicRayTracingRL::rayTracingSupported() const
{ return mRayTracingSupported; }

::D3D12_DISPATCH_RAYS_DESC BasicRayTracingRL::generateDispatchDesc()
{
    ::D3D12_DISPATCH_RAYS_DESC desc{ };

    // Start of the GPU memory, where the shader table is located.
    const auto shaderTableAddress{ mR.shaderTableBuffer->get()->GetGPUVirtualAddress() };

    // Set all of the shader records.

    // Use the first of the ray generation shaders.
    desc.RayGenerationShaderRecord.StartAddress = shaderTableAddress + mR.shaderTable.rayGenerationPartOffset();
    desc.RayGenerationShaderRecord.SizeInBytes = mR.shaderTable.rayGenerationPartSize();
    ASSERT_FAST(desc.RayGenerationShaderRecord.SizeInBytes);

    // Allow use of all of the miss shaders.
    desc.MissShaderTable.StartAddress = shaderTableAddress + mR.shaderTable.missPartOffset();
    desc.MissShaderTable.SizeInBytes = mR.shaderTable.missPartSize();
    desc.MissShaderTable.StrideInBytes = mR.shaderTable.missRecordSize();
    ASSERT_FAST(desc.MissShaderTable.SizeInBytes);

    // Allow use of all of the hit group shaders.
    desc.HitGroupTable.StartAddress = shaderTableAddress + mR.shaderTable.hitGroupPartOffset();
    desc.HitGroupTable.SizeInBytes = mR.shaderTable.hitGroupPartSize();
    desc.HitGroupTable.StrideInBytes = mR.shaderTable.hitGroupRecordSize();
    // TODO - Can be zero, if no instances are provided?
    //ASSERT_FAST(desc.HitGroupTable.SizeInBytes);

    // No callable shaders.
    desc.CallableShaderTable.StartAddress = 0u;
    desc.CallableShaderTable.SizeInBytes = 0u;
    desc.CallableShaderTable.StrideInBytes = 0u;

    // Dispatch one ray per pixel.
    desc.Width = mC.width;
    desc.Height = mC.height;
    desc.Depth = 1u;

    return desc;
}

void BasicRayTracingRL::printDispatchInfo(const ::D3D12_DISPATCH_RAYS_DESC &desc)
{
    log<Info>() << "Ray Tracing Dispatch: "
        "\n\tRay Generation Shader Table: "
        "\n\t\tStart Address [ptr]: " << desc.RayGenerationShaderRecord.StartAddress <<
        "\n\t\tSize [B]: " << desc.RayGenerationShaderRecord.SizeInBytes <<
        "\n\tMiss Shader Table: "
        "\n\t\tStart Address [ptr]: " << desc.MissShaderTable.StartAddress <<
        "\n\t\tSize [B]: " << desc.MissShaderTable.SizeInBytes <<
        "\n\t\tStride [B]: " << desc.MissShaderTable.StrideInBytes <<
        "\n\tHit Group Shader Table: "
        "\n\t\tStart Address [ptr]: " << desc.HitGroupTable.StartAddress <<
        "\n\t\tSize [B]: " << desc.HitGroupTable.SizeInBytes <<
        "\n\t\tStride [B]: " << desc.HitGroupTable.StrideInBytes <<
        "\n\tCallable Shader Table: "
        "\n\t\tStart Address [ptr]: " << desc.CallableShaderTable.StartAddress <<
        "\n\t\tSize [B]: " << desc.CallableShaderTable.SizeInBytes <<
        "\n\t\tStride [B]: " << desc.CallableShaderTable.StrideInBytes <<
        "\n\tDispatch Dimensions: "
        "\n\t\tWidth [n]: " << desc.Width <<
        "\n\t\tHeight [n]: " << desc.Height <<
        "\n\t\tDepth [n]: " << desc.Depth << std::endl;

}

void BasicRayTracingRL::checkReloadShaders()
{
    if (!mR.reloadShaders || !mR.shaderTableBuffer)
    { return; }

    log<Info>() << "Attempting to reload Ray Tracing shaders!" << std::endl;

    // Load shaders.
    res::d3d12::D3D12Shader::PtrT rtShaders{ };
    try
    {
        rtShaders = res::d3d12::D3D12Shader::create(SHADER_FILENAME);
    }
    catch (const std::exception &err)
    {
        log<Error>() << "Failed to reload Ray Tracing shaders! : " << err.what() << std::endl;
        mR.reloadShaders = false;
        return;
    }
    catch (...)
    {
        log<Error>() << "Failed to reload Ray Tracing shaders!" << std::endl;
        mR.reloadShaders = false;
        return;
    }

    mR.rtShaders = rtShaders;

    // Build the pipeline state.
    createPipeline();
    // Build the shader table.
    mR.shaderTable.build(mRenderer.rayTracing().device(), 
        *mR.pipeline->get(), *mR.shaderTableBuffer);

    // Done...
    mR.reloadShaders = false;
}

uint32_t BasicRayTracingRL::debugModeToShaderMode(DebugMode mode)
{ return static_cast<uint32_t>(mode); }

const char *BasicRayTracingRL::debugModeName(DebugMode mode)
{
    switch (mode)
    {
    default:
    case DebugMode::FinalColor:
        return "Final Hybrid Color";
    case DebugMode::FinalColorWithPrimary:
        return "Final Full RT Color";
    case DebugMode::DebugTextureBaseColor: 
        return "Base Color";
    case DebugMode::DebugTexturePbrProperties: 
        return "PBR Properties";
    case DebugMode::DebugTextureNormals: 
        return "Texture Normals";
    case DebugMode::DebugNormal: 
        return "Mesh Normals";
    case DebugMode::DebugWorldNormal: 
        return "World Normals";
    case DebugMode::DebugTangent: 
        return "Tangents";
    case DebugMode::DebugReflectionRefraction: 
        return "Reflection-Refraction";
    case DebugMode::DebugBarycentrics: 
        return "Barycentrics";
    case DebugMode::DebugReflections: 
        return "Reflections";
    }
}

}

}

#endif // ENGINE_RAY_TRACING_ENABLED
