/**
 * @file renderer/basicRayTracing/shaders/BasicRayTracing.hlsl
 * @author Tomas Polasek
 * @brief Shader used for basic ray tracing. Inspired by 
 * D3D12RayTracingHelloWorld project.
 */

#ifndef ENGINE_BASIC_RAY_TRACING_RL_BASIC_RAY_TRACING_HLSL
#define ENGINE_BASIC_RAY_TRACING_RL_BASIC_RAY_TRACING_HLSL

#include "RayTracingShadersCompat.h"

/**
 * Main function for profiling the raw capabilities of ray tracing 
 * cores.
 * @return Returns the final color.
 */
float4 profRawRayCasts(uint3 rayIndex)
{
    // Generate the camera ray for this thread.
    RayDesc ray = generateCameraRay(rayIndex.xy);

    RayPayload payload = { 
        float4(ray.Direction, 1.0f), 
        float2(1.0f, 1.0f),
        true 
    };

    // Dispatch the rays!
    TraceRay(
        // Choose the primary scene.
        gScene,
        // Skip back-facing triangles from triggering hit.
        RAY_FLAG_CULL_BACK_FACING_TRIANGLES, 
        /*
        RAY_FLAG_CULL_BACK_FACING_TRIANGLES |
        RAY_FLAG_ACCEPT_FIRST_HIT_AND_END_SEARCH |
        RAY_FLAG_SKIP_CLOSEST_HIT_SHADER,
        */
        // Trace against all geometry, no masking.
        0xFF,
        // No additional shader table offset.
        0u,
        // Each primitive offsets the hit group index by 1.
        1u,
        // Use the first miss shader.
        0u,
        // Use generated camera ray.
        ray,
        // Starting payload, which will contain the result.
        payload);

    return payload.color;
}

/// Shader which generates rays as specified by the camera.
[shader("raygeneration")]
void BasicRayGenShader()
{
    // Get index of this ray, within the whole dispatch.
    uint3 rayIndex = DispatchRaysIndex();

    gLightingTarget[rayIndex.xy] = profRawRayCasts(rayIndex);
}

/// Hit shader, executed on hitting a triangle.
[shader("closesthit")]
void BasicClosestHitShader(inout RayPayload payload, in MyTriangleAttributes attr)
{
    GeometryInformation geometryInfo = prepareGeometryInformation(PrimitiveIndex(), attr);
    PerInstanceData instanceData = perparePerInstanceData(InstanceID());
    ShadingInformation shadingInfo = prepareShadingInformation(geometryInfo, instanceData);

    shadingInfo.finalColor = float4(shadingInfo.baseColor.rgba);

    float4 rayColor = float4(1.0f, 0.0f, 1.0f, 1.0f);

    const float occlusion = calcAmbientOcclusion(geometryInfo, instanceData);
    const float shadowed = calcDirectionalShadow(geometryInfo, instanceData, gStatic.lightDirection);

    // https://learnopengl.com/PBR/Lighting

    const float3 normal = worldNormal(geometryInfo.normal, instanceData);
    const float3 view = -payload.color.rgb;
    const float3 light = -normalize(gStatic.lightDirection);
    const float3 halfway = normalize(view + light);
    const float metallic = shadingInfo.properties.x;
    const float roughness = shadingInfo.properties.y;
    const float3 albedo = shadingInfo.baseColor.rgb;
    const float3 lightBaseColor = float3(1.0f, 1.0f, 1.0f);

    float3 f0 = float3(0.04f, 0.04f, 0.04f);
    f0 = lerp(f0, albedo, metallic);
    float3 f = fresnelSchlick(max(dot(halfway, view), 0.0f), f0);

    float ndf = distributionGGX(normal, halfway, roughness);
    float g = geometrySmith(normal, view, light, roughness);

    float3 numerator = ndf * g * f;
    float denominator = 4.0f * max(dot(normal, view), 0.0f) * max(dot(normal, light), 0.0f);
    float3 specular = numerator / max(denominator, 0.001f);

    float nDotL = max(dot(normal, light), 0.0f);
    float3 kd = (float3(1.0f, 1.0f, 1.0f) - f) * (1.0f - metallic);
    float3 lightColor = (kd * albedo / HLSL_PI + specular) * 5.0f * lightBaseColor * nDotL;

    //float3 color = float3(0.5f, 0.5f, 0.5f) * albedo * (1.0f - occlusion) + lightColor * (1.0f - shadowed);
    float3 color = albedo * (1.0f - occlusion);
    rayColor = float4(color, shadingInfo.baseColor.a);

    //const float shadowFactor = (shadowed + occlusion) / 2.0f;
    //rayColor = float4(shadingInfo.finalColor.rgb * (1.0f - shadowFactor), shadingInfo.finalColor.a); 

    //rayColor = float4(occlusion, shadowed, 0.0f, 1.0f);

    if (payload.ambientOcclusionShadow.z && metallic >= 0.9f)
    {
        float3 reflected = reflect(-view, normal);

        float3 reflectFactor = fresnelSchlick(max(dot(reflected, normal), 0.0f), 0.9f);

        RayDesc reflectionRay;
        reflectionRay.Origin = geometryInfo.worldPos;
        reflectionRay.Direction = reflect(-view, normal);
        reflectionRay.TMin = RAY_T_MIN;
        reflectionRay.TMax = RAY_T_MAX;

        RayPayload reflectionPayload = { 
            float4(reflectionRay.Direction, 1.0f), 
            float2(1.0f, 1.0f), 
            false 
        };

        // Dispatch the rays!
        TraceRay(
            // Choose the primary scene.
            gScene,
            // Skip back-facing triangles from triggering hit.
            RAY_FLAG_CULL_BACK_FACING_TRIANGLES, 
            // Trace against all geometry, no masking.
            0xFF,
            // No additional shader table offset.
            0u,
            // Each primitive offsets the hit group index by 1.
            1u,
            // Use the first miss shader.
            0u,
            // Use generated camera ray.
            reflectionRay,
            // Starting payload, which will contain the result.
            reflectionPayload);

        rayColor.rgb = (rayColor.rgb * (1.0f - reflectFactor)) + (reflectionPayload.color.rgb * reflectFactor);
    }

    payload.color = rayColor;
}

/// Miss shader, executed on missing all geometry.
[shader("miss")]
void BasicMissShader(inout RayPayload payload)
{
    payload.color = float4(0.3f, 0.3f, 0.3f, 1.0f);
}

/// Miss shader used for AO/shadow rays, sets hitScene to false.
[shader("miss")]
void ShadowMissShader(inout ShadowRayPayload payload)
{
    // No hit...
    payload.hitScene = false;
}

#endif // ENGINE_BASIC_RAY_TRACING_RL_BASIC_RAY_TRACING_HLSL
