/**
 * @file renderer/basicRayTracing/shaders/BasicRayTracing.hlsl
 * @author Tomas Polasek
 * @brief Shader used for basic ray tracing. Inspired by 
 * D3D12RayTracingHelloWorld project.
 */

#ifndef ENGINE_BASIC_RAY_TRACING_RL_BASIC_RAY_TRACING_HLSL
#define ENGINE_BASIC_RAY_TRACING_RL_BASIC_RAY_TRACING_HLSL

#include "RayTracingShadersCompat.h"

/**
 * Calculate the final color for current ray.
 * Other parameters are taken from the static constants buffer.
 * @param viewDirection Direction of view ray from camera to the 
 * point.
 * @return Returns the final color.
 */
float4 calcMissColor(in float3 viewDirection)
{
    return pbrMissColor(viewDirection, gStatic.backgroundColor, 
        gStatic.lightColor, gStatic.lightDirection);
}

/**
 * Calculate the final color for current ray.
 * @param shadingInfo Filled structure with shading information.
 * @param viewDirection Direction of view ray from camera to the 
 * point.
 * @return Returns the final color.
 */
float4 calcFinalColor(in ShadingInformation shadingInfo, in float3 viewDirection)
{
    return pbrHitColor(viewDirection, shadingInfo.wsNormal,
        shadingInfo.properties.x, shadingInfo.properties.y, shadingInfo.baseColor,
        shadingInfo.ambientOcclusion, shadingInfo.shadowOcclusion,
        gStatic.lightDirection, gStatic.lightColor, shadingInfo.reflectionColor);
}

/// Main function of the ray generation shader with primary rays.
inline void rayGenWithPrimary()
{
    // Index of this ray within the whole dispatch.
    uint3 rayIndex = DispatchRaysIndex();

    // Get information from the deferred pass.
    //DeferredInformation deferredInfo = prepareDeferredInformation(rayIndex.xy);

    // Generate the camera ray for this thread.
    RayDesc ray = generateCameraRay(rayIndex.xy);

    // Payload starts with no color.
    RayPayload payload = { 
        // Starting color.
        float4(1.0f, 0.0f, 1.0f, 0.0f), 
        // Use occlusion, shadowing, and reflections.
        float3(1.0f, 1.0f, 1.0f)
    };

    // Dispatch the rays!
    TraceRay(
        // Choose the primary scene.
        gScene, 
        // Skip back-facing triangles from triggering hit.
        //RAY_FLAG_CULL_BACK_FACING_TRIANGLES, 
        RAY_FLAG_NONE, 
        // Trace against all geometry, no masking.
        0xFF, 
        // Use normal ray shaders.
        HLSL_NORMAL_CLOSEST_HIT_OFFSET, HLSL_HIT_GROUP_STRIDE, HLSL_NORMAL_MISS_OFFSET, 
        // Use generated camera ray.
        ray, 
        // Starting payload, which will contain the result.
        payload);

    // Write the raytraced values to the output textures.
    gLightingTarget[rayIndex.xy] = payload.color;
    gAmbientOcclusionTarget[rayIndex.xy] = payload.ambientOcclusionShadow.x;
    gShadowOcclusionTarget[rayIndex.xy] = payload.ambientOcclusionShadow.y;
}

/// Main function of the ray generation shader without primary rays.
inline void rayGenNoPrimary()
{
    // Index of this ray within the whole dispatch.
    uint3 rayIndex = DispatchRaysIndex();

    // Get information from the deferred pass.
    DeferredInformation deferredInfo = prepareDeferredInformation(rayIndex.xy, gDynamic.cameraWorldPosition);
    // Generate shading info from deferred buffers.
    ShadingInformation shadingInfo = prepareDeferredShadingInformation(deferredInfo);

    // Generate the camera ray for this thread.
    RayDesc ray = generateCameraRay(rayIndex.xy);
    float4 rayColor = float4(1.0f, 0.0f, 1.0f, 1.0f);

    if (!deferredInfo.hasData)
    { // No geometry -> miss!
        rayColor = calcMissColor(ray.Direction);

        gLightingTarget[rayIndex.xy] = rayColor;
        gAmbientOcclusionTarget[rayIndex.xy] = 0.0f;
        gShadowOcclusionTarget[rayIndex.xy] = 0.0f;

        return;
    }
    // Else we hit a geometry in the G-Buffer!

    // We are also using shadows and AO.
    shadingInfo.ambientOcclusion = calcAmbientOcclusion(
        deferredInfo.wsPosition, deferredInfo.wsNormal, 
        deferredInfo.ssTexCoord, gStatic.aoRayCount);
    shadingInfo.shadowOcclusion = calcDirectionalShadow(
        deferredInfo.wsPosition, deferredInfo.wsNormal, 
        gStatic.lightDirection, deferredInfo.ssTexCoord,  
        //gStatic.lightDirection, deferredInfo.wsPosition.xz * 16.0f, 
        gStatic.shadowRayCount, gStatic.shadowRayJitter);
    shadingInfo.reflectionColor = calcReflectionColor(
        deferredInfo.wsPosition, deferredInfo.wsNormal,
        ray.Direction, deferredInfo.doReflections);

    // Calculate lighting for the hit-point.
    //rayColor = calcFinalColor(shadingInfo, ray.Direction);
    rayColor = shadingInfo.reflectionColor;

    // Write the raytraced values to the output textures.
    gLightingTarget[rayIndex.xy] = rayColor;
    gAmbientOcclusionTarget[rayIndex.xy] = shadingInfo.ambientOcclusion;
    gShadowOcclusionTarget[rayIndex.xy] = shadingInfo.shadowOcclusion;
}

/// Shader which generates rays as specified by the camera.
[shader("raygeneration")]
void BasicRayGenShader()
{
    if (gStatic.mode == HLSL_RAY_TRACING_MODE_FINAL_COLOR)
    { rayGenNoPrimary(); }
    else
    { rayGenWithPrimary(); }
}

/// Shortcut for calculating the final color.
float4 calcFinalColor(in GeometryInformation geometryInfo, 
    in ShadingInformation shadingInfo)
{
    const float3 viewDirection = geometryInfo.rayDirection;
    return calcFinalColor(shadingInfo, viewDirection);
}

/**
 * Calculate debug color for given mode.
 * @return Returns the final color.
 */
float4 generateDebugOutput(uint mode, GeometryInformation geometryInfo, 
    ShadingInformation shadingInfo, PerInstanceData instanceData)
{
    switch (mode)
    {
        case HLSL_RAY_TRACING_MODE_BASE_TEXTURE:
        { return shadingInfo.baseColor; }
        case HLSL_RAY_TRACING_MODE_PROPERTIES_TEXTURE:
        { return float4(shadingInfo.properties, 0.0f, 1.0f); }
        case HLSL_RAY_TRACING_MODE_NORMAL_TEXTURE:
        { return float4(abs(shadingInfo.normal), 1.0f); }
        case HLSL_RAY_TRACING_MODE_NORMAL:
        { return float4(abs(geometryInfo.normal), 1.0f); }
        case HLSL_RAY_TRACING_MODE_WORLD_NORMAL:
        { return float4(abs(shadingInfo.wsNormal), 1.0f); }
        case HLSL_RAY_TRACING_MODE_TANGENT:
        { return float4(abs(geometryInfo.tangent.xyz), 1.0f); }
        case HLSL_RAY_TRACING_MODE_BARYCENTRINCS:
        { return float4(geometryInfo.barycentrics, 1.0f); }
        case HLSL_RAY_TRACING_MODE_RR: 
        { 
            const float3 dimmedColor = shadingInfo.baseColor.rgb * 0.5f;
            const float3 intenseColor = saturate(dimmedColor + float3(shadingInfo.doReflections, shadingInfo.doRefractions, 0.0f));
            return float4(intenseColor, 1.0f); 
        }
        case HLSL_RAY_TRACING_MODE_REFLECTIONS:
        { return float4(shadingInfo.reflectionColor.rgb, 1.0f); }
        default:
        { return float4(1.0f, 0.0f, 1.0f, 1.0f); }
    }
}

/// Hit shader, executed on hitting a triangle.
[shader("closesthit")]
void BasicClosestHitShader(inout RayPayload payload, in MyTriangleAttributes attr)
{
    // Index of this ray within the whole dispatch.
    uint3 rayIndex = DispatchRaysIndex();

    // Calculate necessary information.
    GeometryInformation geometryInfo = prepareGeometryInformation(PrimitiveIndex(), attr);
    PerInstanceData instanceData = perparePerInstanceData(InstanceID());
    ShadingInformation shadingInfo = prepareShadingInformation(geometryInfo, instanceData);

    if (payload.ambientOcclusionShadow.x > 0.0f)
    { // We are using AO.
        shadingInfo.ambientOcclusion = calcAmbientOcclusion(geometryInfo, instanceData);
    }

    if (payload.ambientOcclusionShadow.y > 0.0f)
    { // We are using shadows.
        shadingInfo.shadowOcclusion = calcDirectionalShadow(geometryInfo, instanceData, gStatic.lightDirection);
    }

    if (payload.ambientOcclusionShadow.z > 0.0f)
    { // We are using reflections.
        shadingInfo.reflectionColor = calcReflectionColor(geometryInfo, shadingInfo, instanceData);
    }

    payload.ambientOcclusionShadow.x = shadingInfo.ambientOcclusion;
    payload.ambientOcclusionShadow.y = shadingInfo.shadowOcclusion;

    if (gStatic.mode != HLSL_RAY_TRACING_MODE_FINAL_COLOR_WITH_PRIMARY && 
        gStatic.mode != HLSL_RAY_TRACING_MODE_FINAL_COLOR && 
        // Skip for reflection debugging.
        (gStatic.mode != HLSL_RAY_TRACING_MODE_REFLECTIONS || 
            payload.ambientOcclusionShadow.z > 0.0f))
    { // For debug modes, just output the requested texture.
        payload.color = generateDebugOutput(gStatic.mode, geometryInfo, shadingInfo, instanceData);
        return;
    }

    // Calculate lighting for the hit-point.
    float4 rayColor = 0.0f;
    if (payload.ambientOcclusionShadow.z > 0.0f && 
        gStatic.mode != HLSL_RAY_TRACING_MODE_FINAL_COLOR_WITH_PRIMARY)
    { // Calculating reflection.
        rayColor = shadingInfo.reflectionColor;
    }
    else
    { // Calculate final color for reflected point.
        rayColor = calcFinalColor(geometryInfo, shadingInfo);
    }

    // Fill the output buffers.
    payload.color = rayColor;
}

/// Hit shader used for getting distance to hit.
[shader("closesthit")]
void DistanceClosestHitShader(inout DistanceRayPayload payload, in MyTriangleAttributes attr)
{
    // Store length of the ray.
    payload.distance = RayTCurrent();
}

/// Miss shader, executed on missing all geometry.
[shader("miss")]
void BasicMissShader(inout RayPayload payload)
{
    payload.color = calcMissColor(WorldRayDirection());
}

/// Miss shader used for AO/shadow rays, sets hitScene to false.
[shader("miss")]
void ShadowMissShader(inout ShadowRayPayload payload)
{
    // No hit...
    payload.hitScene = false;
}

/// Miss shader used for getting distance to hit.
[shader("miss")]
void DistanceMissShader(inout DistanceRayPayload payload)
{
    // No hit, set noticable value.
    payload.distance = -1.0f;
}

#endif // ENGINE_BASIC_RAY_TRACING_RL_BASIC_RAY_TRACING_HLSL
