/**
 * @file renderer/shadowMapRl/shaders/ShadowMapPS.hlsl
 * @author Tomas Polasek
 * @brief Pixel shader used in generation of shadow maps
 */

#ifndef ENGINE_SHADOW_MAP_RL_DEFERRED_PS_HLSL
#define ENGINE_SHADOW_MAP_RL_DEFERRED_PS_HLSL

#include "ShadowmapShadersCompat.h"

/**
 * Process the input fragment and store it into the shadow map.
 */
/*
PixelData main(FragmentData input)
{
    PixelData output;

    output.depth = 0.5f;

    return output;
}
*/
void main(FragmentData input)
{ }

#endif // ENGINE_SHADOW_MAP_RL_DEFERRED_PS_HLSL
