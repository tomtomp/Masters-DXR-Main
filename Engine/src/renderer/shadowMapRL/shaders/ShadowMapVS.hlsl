/**
 * @file renderer/shadowMapRl/shaders/ShadowMapVS.hlsl
 * @author Tomas Polasek
 * @brief Vertex shader used in generation of shadow maps
 */

#ifndef ENGINE_SHADOW_MAP_RL_DEFERRED_VS_HLSL
#define ENGINE_SHADOW_MAP_RL_DEFERRED_VS_HLSL

#include "ShadowmapShadersCompat.h"

/**
 * Process the input vertex and transform it into the light space.
 */
FragmentData main(VertexData input)
{
    FragmentData output;

    output.ssPosition = mul(gPerLight.worldToLight, mul(gInstance.modelToWorld, float4(input.position, 1.0f)));

    return output;
}

#endif // ENGINE_SHADOW_MAP_RL_DEFERRED_VS_HLSL
