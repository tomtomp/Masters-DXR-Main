/**
 * @file renderer/shadowMapRL/ShadowMapRL.cpp
 * @author Tomas Polasek
 * @brief Rendering layer used to generate 
 * shadow map depth textures.
 */

#include "stdafx.h"

#include "engine/renderer/shadowMapRL/ShadowMapRL.h"

#include "engine/renderer/shadowMapRL/shaders/ShadowMapVS.h"
#include "engine/renderer/shadowMapRL/shaders/ShadowMapPS.h"

#include "engine/gui/GuiSubSystem.h"

namespace quark
{

namespace rndr
{

void ShadowMapBuffers::transitionResourcesTo(res::d3d12::D3D12CommandList &cmdList, 
    ::D3D12_RESOURCE_STATES state)
{
    for (auto &shadowMap : shadowMaps)
    {
        if (shadowMap)
        { shadowMap->transitionTo(state, cmdList); }
    }
}

void ShadowMapBufferViews::createViews(const ShadowMapBuffers &buffers,
    res::d3d12::D3D12DescAllocatorInterface &descHeap, ResourceViewType type)
{
    shadowMapViews.resize(buffers.shadowMaps.size());

    for (uint32_t iii = 0u; iii < buffers.shadowMaps.size(); ++iii)
    {
        if (!shadowMapViews[iii])
        { shadowMapViews[iii] = helpers::d3d12::D3D12ResourceView::create(); }
        if (buffers.shadowMaps[iii])
        { shadowMapViews[iii]->reallocateDescriptor(buffers.shadowMaps[iii], descHeap, type, VIEW_FORMAT); }
    }
}

ShadowMapRL::DirectionalLights &ShadowMapRL::DirectionalLights::clear()
{ lights.clear(); return *this; }

ShadowMapRL::DirectionalLights &ShadowMapRL::DirectionalLights::pushLight(const dxtk::math::Vector3 &direction)
{
    // Prevent pushing a zero vector.
    if (direction.Length() < FLT_EPSILON)
    { lights.emplace_back(DirectionalLight{ dxtk::math::Vector3{ FLT_EPSILON } }); }
    else
    { lights.emplace_back(DirectionalLight{ direction }); }

    return *this;
}

ShadowMapRL::DirectionalLights &ShadowMapRL::DirectionalLights::pushLight(float x, float y, float z)
{ return pushLight(dxtk::math::Vector3{ x, y, z }); }

util::PointerType<ShadowMapRL>::PtrT ShadowMapRL::create(RenderSubSystem &renderer)
{ return PtrT{ new ShadowMapRL(renderer) }; }

ShadowMapRL::~ShadowMapRL()
{ /* Automatic */ }

void ShadowMapRL::setDrawablesList(const res::scene::DrawableListHandle &drawables)
{
    mDrawables = drawables;
    mLastPreparedVersion = { };
}

ShadowMapRL::DirectionalLights &ShadowMapRL::lights()
{ return mC.lights; }

ShadowMapBuffers ShadowMapRL::shadowMapBuffers() const
{ return mR.shadowMapBuffers; }

void ShadowMapRL::prepare()
{
    PROF_SCOPE("ShadowMapRLPrep");

    auto &drawables{ mDrawables.get() };
    if (drawables.revision() != mLastPreparedVersion)
    { // Current version of the list is not prepared!
        prepareForRendering(drawables);
        mLastPreparedVersion = drawables.revision();
    }

    mPrepared = true;
}

void ShadowMapRL::prepareGui(gui::GuiSubSystem &gui)
{
    gui.addGuiContext("/ShadowMapRL/")
        .setType(gui::GuiContextType::Category)
        .setLabel("ShadowMap Pass");

    gui.addGuiVariable<gui::InputInt>("ShadowMapRlResolution", "/ShadowMapRL/", { mC.shadowMapResolution })
        .setLabel("Resolution");

    gui.addGuiVariable<gui::InputBool>("ShadowMapRlEnabled", "/ShadowMapRL/", { mC.enabled })
        .setLabel("Rendering Enabled")
        .cfg().setEditable(false);

    gui.addGuiVariable<gui::Button>("ShadowMapRlReloadShaders", "/ShadowMapRL/", { [&] ()
    { mR.reloadShaders = true; } }).setLabel("Reload Shaders");
}

void ShadowMapRL::updateGui(gui::GuiSubSystem &gui)
{
    auto &shadowMapResolutionVar{ gui.getVariable<gui::InputInt>("ShadowMapRlResolution") };
    gui::synchronizeVariables(shadowMapResolutionVar, mC.shadowMapResolution);

    auto &enabledVar{ gui.getVariable<gui::InputBool>("ShadowMapRlEnabled") };
    enabledVar.value = mC.enabled;
}

void ShadowMapRL::render(const helpers::math::Camera &camera, 
    res::d3d12::D3D12CommandList &cmdList)
{
    // Skip if we didn't initialize the size.
    if (!mPrepared)
    { return; }

    // Check if shader reloading is required.
    checkReloadShaders();

    // Reflect any changes to the shadow maps.
    prepareShadowMapTextures(mR.shadowMapBuffers, mC.shadowMapResolution, mC.lights);
    // Prepare information passed to the shaders.
    prepareShadowMapConstantBuffers(camera, mR.perLightConstantsBuffer, mC.lights);

    // Stop after creating the textures, if not enabled.
    if (!mC.enabled)
    { return; }

    GPU_PROF_SCOPE(cmdList, "ShadowMapRLGpu");

    mR.viewport = ::CD3DX12_VIEWPORT(0.0f, 0.0f, 
        static_cast<float>(mC.shadowMapResolution), 
        static_cast<float>(mC.shadowMapResolution));

    // Prepare shared GPU state:
    cmdList->RSSetViewports(1u, &mR.viewport);
    cmdList->RSSetScissorRects(1u, &mR.scissorRect);

    cmdList->SetGraphicsRootSignature(mR.rootSig->get());

    // Return the buffers into the original state.
    mR.shadowMapBuffers.transitionResourcesTo(cmdList, ::D3D12_RESOURCE_STATE_DEPTH_WRITE);
    // Reset shadow maps to default value.
    clearShadowMapTextures(cmdList, mR.shadowMapBuffers, DEFAULT_CLEAR_VALUE);

    for (auto &shadowMap : mR.shadowMapBuffers.shadowMaps)
    { // Generate shadow maps.
        // No color RTVs, just the DSV.
        const auto shadowMapHandle{ shadowMap->cpuHandle() };
        cmdList->OMSetRenderTargets(0u, nullptr, false, &shadowMapHandle);

        // Set the per-light constants.
        cmdList->SetGraphicsRootConstantBufferView(
            compatShadowMap::GlobalRS::PerLightBuffer, 
            mR.perLightConstantsBuffer->gpuAddress());

        /// Execute draw bundle with all of the objects.
        cmdList->ExecuteBundle(mR.renderBundle->get());
    }
}

void ShadowMapRL::setEnabled(bool enabled)
{ mC.enabled = enabled; }

void ShadowMapRL::initializeResources()
{
    PROF_SCOPE("ShadowMapRLInitialization");

    // No scissoring.
    mR.scissorRect = ::CD3DX12_RECT(0, 0, LONG_MAX, LONG_MAX);

    // Prepare root signature.
    quark::helpers::d3d12::D3D12RootSignatureBuilder rsb;

    // Static, slow-changing constants, register(b0, space0).
    rsb[compatShadowMap::GlobalRS::PerLightBuffer].asConstantBufferViewParameter(
        compatShadowMap::PerLightSlot.reg, compatShadowMap::PerLightSlot.space);
    // Per-instance constants, straight in root signature, register(b1, space0).
    rsb[compatShadowMap::GlobalRS::InstanceConstantBuffer].asConstantParameter(
        static_cast<::UINT>(util::sizeInUint32<compatShadowMap::InstanceConstantBuffer>()), 
        compatShadowMap::InstanceConstantsSlot.reg, compatShadowMap::InstanceConstantsSlot.space);

    // Enable stages which are used in deferred pipeline.
    rsb.setFlags(
        D3D12_ROOT_SIGNATURE_FLAG_ALLOW_INPUT_ASSEMBLER_INPUT_LAYOUT |
        D3D12_ROOT_SIGNATURE_FLAG_DENY_HULL_SHADER_ROOT_ACCESS |
        D3D12_ROOT_SIGNATURE_FLAG_DENY_DOMAIN_SHADER_ROOT_ACCESS |
        D3D12_ROOT_SIGNATURE_FLAG_DENY_GEOMETRY_SHADER_ROOT_ACCESS);

    mR.rootSig = rsb.build(mRenderer->device());

    mR.inputLayout.clear();

    // Vertex position.
    mR.inputLayout.addElement(
        "POSITION", 0u, 
        DEFAULT_VERTEX_POSITION_FORMAT, compatShadowMap::InputLayout::Position, 
        D3D12_APPEND_ALIGNED_ELEMENT, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0u);

    // Load shadow map shaders.
    mR.vShader = res::d3d12::D3D12Shader::create(gShadowMapVs, sizeof(gShadowMapVs));
    mR.pShader = res::d3d12::D3D12Shader::create(gShadowMapPs, sizeof(gShadowMapPs));

    // Build the pipeline state.
    buildPipeline();

    mR.descHeap = res::d3d12::D3D12DescHeap::create(mRenderer->device(),
        ::D3D12_DESCRIPTOR_HEAP_TYPE_CBV_SRV_UAV, calculateRequiredDescriptors(),
        ::D3D12_DESCRIPTOR_HEAP_FLAG_SHADER_VISIBLE);
}

void ShadowMapRL::buildPipeline()
{
    quark::helpers::d3d12::D3D12PipelineStateSOBuilder pssob;
    quark::helpers::d3d12::D3D12PipelineStateBuilder psb;
    psb <<
        mR.rootSig->rsState() <<
        mR.inputLayout.ilState() <<
        pssob.rasterizer(::D3D12_CULL_MODE_BACK,
            !mRenderer->cfg().renderRasterizerFrontClockwise,
            mRenderer->cfg().renderRasterizerFill) <<
        pssob.primitiveTopology(DEFAULT_PRIMITIVE_TOPOLOGY_TYPE) <<
        mR.vShader->vsState() <<
        mR.pShader->psState() <<
        pssob.depthStencilFormat(DEFAULT_SHADOW_MAP_DSV_FORMAT);
    mR.pipelineState = psb.build(mRenderer->device());
}

uint32_t ShadowMapRL::calculateRequiredDescriptors() const
{
    uint32_t result{ 0u };

    // Pre-light constant buffer.
    result += 1u;

    return result;
}

void ShadowMapRL::prepareDrawBundle(res::scene::DrawableList &drawables, 
    res::d3d12::D3D12CommandList &drawBundle)
{
    PROF_SCOPE("PrepareDrawBundle");

    // Common to all drawables: 
    drawBundle->SetGraphicsRootSignature(mR.rootSig->get());
    drawBundle->SetPipelineState(mR.pipelineState->get());
    drawBundle->IASetPrimitiveTopology(DEFAULT_PRIMITIVE_TOPOLOGY);

    // Prepared array for input layout vertex buffers.
    ::D3D12_VERTEX_BUFFER_VIEW vbvs[compatShadowMap::InputLayout::Count]{ };

    for (const auto &drawable : drawables)
    { // Process all drawables.

        // One mesh can contain multiple primitives.
        const auto &mesh{ drawable.meshHandle.get() };

        for (std::size_t iii = 0; iii < mesh.primitives.size(); ++iii)
        { // Render the primitives.
            const auto &primitive{ mesh.primitives[iii] };

            // Specify which material textures should be used.
            compatShadowMap::InstanceConstantBuffer instanceData{ };
            instanceData.modelToWorld = drawable.modelMatrix.matrix();

            drawBundle->SetGraphicsRoot32BitConstants(
                compatShadowMap::GlobalRS::InstanceConstantBuffer, 
                static_cast<::UINT>(util::sizeInUint32<compatShadowMap::InstanceConstantBuffer>()), 
                &instanceData, 0u);

            // Specify vertex data.
            vbvs[compatShadowMap::InputLayout::Position] = primitive.getPositionBuffer()->vbv();

            drawBundle->IASetVertexBuffers(0u, static_cast<::UINT>(util::count(vbvs)), vbvs);

            // Specify index data.
            const auto &indexBuffer{ primitive.indices };
            const auto ibw = indexBuffer->ibv();
            drawBundle->IASetIndexBuffer(&ibw);

            // Perform the draw.
            drawBundle->DrawIndexedInstanced(
                static_cast<::UINT>(indexBuffer->numElements), 
                // Only one instance.
                1u, 0u, 0u, 0u);
        }
    }
}

res::d3d12::D3D12CommandList::PtrT ShadowMapRL::createDrawBundle(
    RenderSubSystem &renderer)
{
    // Create the reusable command bundle: 
    return renderer.bundleCmdQueue().getCommandList();
}

void ShadowMapRL::prepareForRendering(res::scene::DrawableList &drawables)
{
    PROF_SCOPE("PrepareForRendering");

    if (drawables.empty())
    { // Nothing to be done...
        mR.renderBundle = nullptr;
        return;
    }

    // Prepare drawing commands for all drawables.
    const auto drawBundle{ createDrawBundle(*mRenderer) };
    prepareDrawBundle(drawables, *drawBundle);
    drawBundle->get()->Close();
    mR.renderBundle = drawBundle;
}

void ShadowMapRL::clearShadowMapTextures(res::d3d12::D3D12CommandList &cmdList, 
    ShadowMapBuffers &buffers, float clearValue)
{
    const float clearColor[] = { clearValue, clearValue, clearValue, clearValue };
    
    for (auto &shadowMap : buffers.shadowMaps)
    { 
        cmdList->ClearDepthStencilView(shadowMap->cpuHandle(), 
            ::D3D12_CLEAR_FLAG_DEPTH, 
            clearValue, static_cast<::UINT8>(clearValue), 
            0u, nullptr); 
    }
}

void ShadowMapRL::prepareDsvDescHeap(res::d3d12::D3D12DescHeap::PtrT &dsvDescHeap,
    helpers::d3d12::D3D12DescTable &dsvTable, const DirectionalLights &lights)
{
    const auto requiredDescriptors{ lights.lights.size() };

    dsvDescHeap = res::d3d12::D3D12DescHeap::create(mRenderer->device(), 
        ::D3D12_DESCRIPTOR_HEAP_TYPE_DSV, static_cast<uint32_t>(requiredDescriptors), 
        ::D3D12_DESCRIPTOR_HEAP_FLAG_NONE);

    dsvTable = helpers::d3d12::D3D12DescTable(
        *dsvDescHeap, 
        dsvDescHeap->allocateTable(static_cast<uint32_t>(requiredDescriptors)));
}

void ShadowMapRL::prepareShadowMapTextures(ShadowMapBuffers &buffers, 
    uint32_t resolution, const DirectionalLights &lights)
{
    if (buffers.shadowMaps.size() == lights.lights.size() && 
        buffers.resolution == resolution)
    { // We are done...
        return;
    }

    /// Create the descriptor heap for the shadow map textures.
    prepareDsvDescHeap(mR.dsvDescHeap, mR.dsvTable, lights);

    auto allocator{ mRenderer->memoryMgr().committedAllocator(
        ::CD3DX12_HEAP_PROPERTIES{
            ::D3D12_HEAP_TYPE_DEFAULT
        },
        ::D3D12_HEAP_FLAG_NONE) };

    auto textureDesc{ ::CD3DX12_RESOURCE_DESC::Tex2D(::DXGI_FORMAT_UNKNOWN, resolution, resolution) };
    textureDesc.Flags = ::D3D12_RESOURCE_FLAG_ALLOW_DEPTH_STENCIL;
    textureDesc.Format = DEFAULT_SHADOW_MAP_RT_FORMAT;

    ::D3D12_CLEAR_VALUE clearValue{ };
    clearValue.Color[0u] = DEFAULT_CLEAR_VALUE;
    clearValue.Color[1u] = DEFAULT_CLEAR_VALUE;
    clearValue.Color[2u] = DEFAULT_CLEAR_VALUE;
    clearValue.Color[3u] = DEFAULT_CLEAR_VALUE;
    clearValue.Format = DEFAULT_SHADOW_MAP_DSV_FORMAT;

    // Create the shadow map textures.
    buffers.shadowMaps.resize(lights.lights.size());
    for (auto &shadowMap : buffers.shadowMaps)
    {
        if (!shadowMap)
        { shadowMap = helpers::d3d12::D3D12TextureBuffer::create(); }
        shadowMap->allocate(allocator, textureDesc, ::D3D12_RESOURCE_STATE_DEPTH_WRITE, 
            &clearValue, L"ShadowMap");
        shadowMap->reallocateDescriptor(mR.dsvTable, 
            rndr::ResourceViewType::DepthStencil, 
            DEFAULT_SHADOW_MAP_DSV_FORMAT);
    }

    buffers.resolution = resolution;
}

void ShadowMapRL::prepareShadowMapConstantBuffers(
    const helpers::math::Camera &camera, 
    PerLightBuffer::PtrT &buffer, DirectionalLights &lights)
{
    if (!buffer)
    { buffer = PerLightBuffer::create(); }

    for (uint32_t iii = 0; iii < lights.lights.size(); ++iii)
    {
        auto &light{ lights.lights[iii] };
        light.lightTransform = calculateDirectionalLightTransform(camera, light.direction, 
            DEFAULT_LIGHT_VIEWPORT_SIZE, DEFAULT_LIGHT_NEAR, DEFAULT_LIGHT_FAR);

        compatShadowMap::PerLightBuffer constants{ };
        constants.worldToLight = light.lightTransform;
        
        buffer->at(iii) = constants;
    }

    buffer->reallocate(mRenderer->device(), L"PerLightConstantsBuffer");
    buffer->reallocateDescriptor(*mR.descHeap, rndr::ResourceViewType::ConstantBuffer);
    buffer->uploadToGPU();
}

dxtk::math::Matrix ShadowMapRL::calculateDirectionalLightTransform(
    const helpers::math::Camera &camera, 
    const dxtk::math::Vector3 &direction,
    float viewportSize, float nearPos, float farPos)
{
    //const auto projection{ dxtk::math::Matrix::CreateOrthographic(viewportSize, viewportSize, nearPos, farPos).Transpose() };
    //const auto view{ dxtk::math::Matrix::CreateLookAt(-direction, { 0.0f, 0.0f, 0.0 }, { 0.0f, 1.0f, 0.0f }).Transpose() };
    //const auto view{ dxtk::math::Matrix::CreateLookAt({ 0.0f, 0.0f, 0.0 }, direction, { 0.0f, 1.0f, 0.0f }).Transpose() };
    //const auto projection{ dxtk::math::Matrix::CreateOrthographic(viewportSize, viewportSize, nearPos, farPos) };
    //const auto view{ dxtk::math::Matrix::CreateLookAt({ 0.0f, 0.0f, 0.0 }, direction, { 0.0f, 1.0f, 0.0f }) };

    auto projection{ dxtk::math::Matrix() };
    DirectX::XMStoreFloat4x4(&projection, DirectX::XMMatrixOrthographicLH(viewportSize, viewportSize, nearPos, farPos));
    
    auto view{ dxtk::math::Matrix() };
    auto dir{ direction };
    dir.Normalize();
    const auto basePos{ camera.position() };
    //const auto basePos{ dxtk::math::Vector3() };
    const auto eye{ basePos + dxtk::math::Vector3{ -dir } };
    //const auto target{ dxtk::math::Vector3{ 0.0f, 0.0f, 0.0f } };
    const auto target{ dxtk::math::Vector3{ basePos } };
    const auto up{ dxtk::math::Vector3{ 0.0f, 1.0f, 0.0f } };
    DirectX::XMVECTOR eyev = DirectX::XMLoadFloat3(&eye);
    DirectX::XMVECTOR targetv = DirectX::XMLoadFloat3(&target);
    DirectX::XMVECTOR upv = DirectX::XMLoadFloat3(&up);
    DirectX::XMStoreFloat4x4(&view, DirectX::XMMatrixLookAtLH(eyev, targetv, upv));

    return view * projection;
}

void ShadowMapRL::checkReloadShaders()
{
    if (!mR.reloadShaders)
    { return; }

    log<Info>() << "Attempting to reload Shadow Map shaders!" << std::endl;

    // Load shaders.
    res::d3d12::D3D12Shader::PtrT vShader{ };
    res::d3d12::D3D12Shader::PtrT pShader{ };
    try
    {
        vShader = res::d3d12::D3D12Shader::create(L"ShadowMapVS.cso");
        pShader = res::d3d12::D3D12Shader::create(L"ShadowMapPS.cso");
    }
    catch (const std::exception &err)
    {
        log<Error>() << "Failed to reload Shadow Map shaders! : " << err.what() << std::endl;
        mR.reloadShaders = false;
        return;
    }
    catch (...)
    {
        log<Error>() << "Failed to reload Shadow Map shaders!" << std::endl;
        mR.reloadShaders = false;
        return;
    }

    mR.vShader = vShader;
    mR.pShader = pShader;

    // Build the pipeline state.
    buildPipeline();
    prepareForRendering(mDrawables.get());

    // Done...
    mR.reloadShaders = false;
}

ShadowMapRL::ShadowMapRL(RenderSubSystem &renderer) :
    mRenderer{ &renderer }
{ initializeResources(); }

}

}
