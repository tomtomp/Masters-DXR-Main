/**
 * @file renderer/voronoiRL/shaders/VoronoiGS.hlsl
 * @author Tomas Polasek
 * @brief Geometry shader used in Voronoi shaders.
 */

#ifndef ENGINE_VORONOI_RL_VORONOI_GS_HLSL
#define ENGINE_VORONOI_RL_VORONOI_GS_HLSL

#include "VoronoiShadersCompat.h"

// Comment to disable debug rendering.
#define DBG_RENDER

/// Width of the debug line.
#define DBG_LINE_WIDTH 0.005f
#define DBG_LINE_COLOR float3(0.3f, 0.1f, 0.85f)
/// Width and height of the debug quad.
#define DBG_QUAD_SIZE 0.05f

/// Number of closest cell centers to search for.
#define NUM_CLOSEST_CELLS 4

/// Invalid cell index for uint.
#define INVALID_CELL_IDX 0xffffffff

/// Maximum number of vertices in the cell.
#define MAX_VERTICES 64
/// Maximum number of neighbors for a single vertex.
#define MAX_NEIGHBORS 6

/// Calculate distance of 2 cell centers.
float cellDistance(in float3 first, in float3 second)
{ return distance(first, second); }

/// Get cell center position for cell with given index.
float3 cellPosition(in uint cellIndex)
{ return gCellData.cells[cellIndex].position.xyz; }

/// Draw a quad line from first point to the second one.
void drawLine(in float3 first, in float3 second, in float3 camDir, in float3 color, inout TriangleStream<FragmentData> outputStream)
{
    const float lineWidth = DBG_LINE_WIDTH;
    const float3 lineDir = normalize(first - second);
    const float3 lineNormal = cross(lineDir, camDir);

    FragmentData output;
    output.color = color;

    output.wsPosition = first - lineWidth * lineNormal;
    output.ssPosition = mul(gDynamic.worldToCamera, float4(output.wsPosition, 1.0f));
    output.wsNormal = float3(0.0f, 0.0f, 1.0f);
    output.texCoord = float2(0.0f, 0.0f);
    outputStream.Append(output);

    output.wsPosition = second - lineWidth * lineNormal;
    output.ssPosition = mul(gDynamic.worldToCamera, float4(output.wsPosition, 1.0f));
    output.wsNormal = float3(0.0f, 0.0f, 1.0f);
    output.texCoord = float2(0.0f, 1.0f);
    outputStream.Append(output);

    output.wsPosition = first + lineWidth * lineNormal;
    output.ssPosition = mul(gDynamic.worldToCamera, float4(output.wsPosition, 1.0f));
    output.wsNormal = float3(0.0f, 0.0f, 1.0f);
    output.texCoord = float2(1.0f, 0.0f);
    outputStream.Append(output);

    output.wsPosition = second + lineWidth * lineNormal;
    output.ssPosition = mul(gDynamic.worldToCamera, float4(output.wsPosition, 1.0f));
    output.wsNormal = float3(0.0f, 0.0f, 1.0f);
    output.texCoord = float2(1.0f, 1.0f);
    outputStream.Append(output);

    outputStream.RestartStrip();
}

/// Draw a quad centered on the first point.
void drawQuad(in float3 first, in float3 camDir, in float3 color, inout TriangleStream<FragmentData> outputStream)
{
    const float billboardSize = DBG_QUAD_SIZE;
    const float3 baseWsPosition = first;
    const float3 up = float3(0.0f, 1.0f, 0.0f);
    const float3 right = cross(up, camDir);

    FragmentData output;
    output.color = color;

    //output.wsPosition = baseWsPosition + float3(-billboardSize, -billboardSize, 0.0f);
    output.wsPosition = baseWsPosition - billboardSize * right - billboardSize * up;
    output.ssPosition = mul(gDynamic.worldToCamera, float4(output.wsPosition, 1.0f));
    output.wsNormal = float3(0.0f, 0.0f, 1.0f);
    output.texCoord = float2(0.0f, 0.0f);
    outputStream.Append(output);

    //output.wsPosition = baseWsPosition + float3(-billboardSize, billboardSize, 0.0f);
    output.wsPosition = baseWsPosition - billboardSize * right + billboardSize * up;
    output.ssPosition = mul(gDynamic.worldToCamera, float4(output.wsPosition, 1.0f));
    output.wsNormal = float3(0.0f, 0.0f, 1.0f);
    output.texCoord = float2(0.0f, 1.0f);
    outputStream.Append(output);

    //output.wsPosition = baseWsPosition + float3(billboardSize, -billboardSize, 0.0f);
    output.wsPosition = baseWsPosition + billboardSize * right - billboardSize * up;
    output.ssPosition = mul(gDynamic.worldToCamera, float4(output.wsPosition, 1.0f));
    output.wsNormal = float3(0.0f, 0.0f, 1.0f);
    output.texCoord = float2(1.0f, 0.0f);
    outputStream.Append(output);

    //output.wsPosition = baseWsPosition + float3(billboardSize, billboardSize, 0.0f);
    output.wsPosition = baseWsPosition + billboardSize * right + billboardSize * up;
    output.ssPosition = mul(gDynamic.worldToCamera, float4(output.wsPosition, 1.0f));
    output.wsNormal = float3(0.0f, 0.0f, 1.0f);
    output.texCoord = float2(1.0f, 1.0f);
    outputStream.Append(output);

    outputStream.RestartStrip();
}

/// Description of a plane and its vertices.
struct PlaneDesc
{
    /// Vertices comprising this plane.
    float3 vertices[MAX_VERTICES];
    /// Line specification of this plane, 2 vertices and the next line.
    uint4 lines[MAX_VERTICES / 2];
    /// Number of lines which are actualy used.
    uint linesUsed;
}; // struct PlaneDesc

/// Initialize plane description for a plane with given parameters.
//void initializePlane(inout PlaneDesc planeDesc, in float3 planePosition, in float3 planeNormal, in float startSize)
void initializePlane(out float3 vertices[MAX_VERTICES], out uint4 lines[MAX_VERTICES / 2], 
    out uint linesUsed, in float3 planePosition, in float3 planeNormal, in float startSize)
{
    const float3 normal = planeNormal;
    float3 tangent = cross(normal, float3(1.0f, 0.0f, 0.0f));
    if (length(tangent) < HLSL_EPS)
    { tangent = cross(planeNormal, float3(0.0f, 1.0f, 0.0f)); }
    float3 bitangent = cross(normal, tangent);

    const float3 p1 = planePosition - startSize * tangent - startSize * bitangent;
    const float3 p2 = planePosition + startSize * tangent - startSize * bitangent;
    const float3 p3 = planePosition + startSize * tangent + startSize * bitangent;
    const float3 p4 = planePosition - startSize * tangent + startSize * bitangent;

    vertices[0] = p1;
    vertices[1] = p2;
    vertices[2] = p3;
    vertices[3] = p4;

    lines[0].x = 0u;
    lines[0].y = 1u;
    lines[0].z = 1u;
    lines[0].w = 3u;

    lines[1].x = 1u;
    lines[1].y = 2u;
    lines[1].z = 2u;
    lines[1].w = 0u;

    lines[2].x = 2u;
    lines[2].y = 3u;
    lines[2].z = 3u;
    lines[2].w = 1u;
    
    lines[3].x = 3u;
    lines[3].y = 0u;
    lines[3].z = 0u;
    lines[3].w = 2u;

    linesUsed = 4u;
}

/// Draw a plane using provided description.
void drawQuad(in PlaneDesc planeDesc, in float3 color, inout TriangleStream<FragmentData> outputStream)
{
    FragmentData output;
    output.color = color;
    output.wsNormal = float3(0.0f, 0.0f, 1.0f);
    output.texCoord = float2(0.0f, 0.0f);

    uint lineIndexPrev = planeDesc.lines[0u].z;
    uint lineIndexNext = planeDesc.lines[0u].w;
    const uint4 firstLineSpec = planeDesc.lines[0u];

    output.wsPosition = planeDesc.vertices[firstLineSpec.y];
    output.ssPosition = mul(gDynamic.worldToCamera, float4(output.wsPosition, 1.0f));
    outputStream.Append(output);
    output.wsPosition = planeDesc.vertices[firstLineSpec.x];
    output.ssPosition = mul(gDynamic.worldToCamera, float4(output.wsPosition, 1.0f));
    outputStream.Append(output);

    //for (uint iii = 0u; iii < planeDesc.linesUsed; ++iii)
    //while (planeDesc.lines[lineIndex].z != 0u)
    while (lineIndexPrev != 0u && lineIndexNext != 0u)
    {
        uint4 lineSpec = planeDesc.lines[lineIndexPrev];
        float3 vertexPos = planeDesc.vertices[lineSpec.y];
        lineIndexPrev = lineSpec.w;

        output.wsPosition = vertexPos;
        output.ssPosition = mul(gDynamic.worldToCamera, float4(output.wsPosition, 1.0f));
        outputStream.Append(output);

        lineSpec = planeDesc.lines[lineIndexNext];
        vertexPos = planeDesc.vertices[lineSpec.x];
        lineIndexNext = lineSpec.z;

        output.wsPosition = vertexPos;
        output.ssPosition = mul(gDynamic.worldToCamera, float4(output.wsPosition, 1.0f));
        outputStream.Append(output);
    }

    outputStream.RestartStrip();
}

/**
 * Perform Voronoi diagram generation and pass 
 * the vertices to rasterizer.
 */
[maxvertexcount(MAX_VERTICES)]
void main(in point GeometryData input[1], inout TriangleStream<FragmentData> outputStream)
{
    FragmentData output;

    // Current cell data.
    const uint currentCellIdx = input[0].cellIndex;
    const float3 currentCellPosition = input[0].position;
    const float3 wsCurrentCellPosition = currentCellPosition;
    // Total number of cells.
    const uint cellCount = gStatic.cellCount;

    // Initialize the search algorithm.
    uint closestCellIdxs[NUM_CLOSEST_CELLS] = {
        INVALID_CELL_IDX, INVALID_CELL_IDX, 
        INVALID_CELL_IDX, INVALID_CELL_IDX
    };

    for (uint iii = 0u; iii < cellCount; ++iii)
    { // Go through all other cells
        if (iii == currentCellIdx)
        { continue; }

        const float3 testedCellPosition = cellPosition(iii);
        //float bestDistance = HLSL_FLT_MAX;
        uint bestIdx = INVALID_CELL_IDX;

        for (uint jjj = 0u; jjj < NUM_CLOSEST_CELLS; ++jjj)
        {
            const uint targetCellIdx = closestCellIdxs[jjj];
            if (targetCellIdx == INVALID_CELL_IDX)
            { 
                bestIdx = jjj; 
                break;
            }

            const float3 targetCellPosition = cellPosition(targetCellIdx);

            const float currentTestedDistance = cellDistance(currentCellPosition, testedCellPosition);
            const float currentTargetDistance = cellDistance(currentCellPosition, targetCellPosition);

            if (currentTestedDistance < currentTargetDistance)
                //currentTestedDistance < bestDistance)
            { // Found a candidate for replacing.
                //bestDistance = currentTestedDistance;
                bestIdx = jjj;
                break;
            }
        }

        if (bestIdx != INVALID_CELL_IDX)
        { // We found a better neighbor.
            for (uint jjj = NUM_CLOSEST_CELLS - 1u; jjj > bestIdx; --jjj)
            { // Move the others by one.
                closestCellIdxs[jjj] = closestCellIdxs[jjj - 1];
            }
            closestCellIdxs[bestIdx] = iii; 
        }
    }

#ifdef DBG_RENDER
    const float3 dbgColor = float3(
        (closestCellIdxs[0] + 1u) / ((float)cellCount), 
        (closestCellIdxs[1] + 1u) / ((float)cellCount), 
        (closestCellIdxs[2] + 1u) / ((float)cellCount));

    const float3 camDir = normalize(wsCurrentCellPosition - gDynamic.cameraWorldPosition);
    drawLine(currentCellPosition, cellPosition(closestCellIdxs[0]), camDir, 0.2f * DBG_LINE_COLOR, outputStream);
    drawLine(currentCellPosition, cellPosition(closestCellIdxs[1]), camDir, 0.4f * DBG_LINE_COLOR, outputStream);
    drawLine(currentCellPosition, cellPosition(closestCellIdxs[2]), camDir, 0.6f * DBG_LINE_COLOR, outputStream);
    drawLine(currentCellPosition, cellPosition(closestCellIdxs[3]), camDir, 0.8f * DBG_LINE_COLOR, outputStream);
    //drawLine(float3(0.0f, 0.0f, 0.0f), float3(1.0f, 0.0f, 0.0f), camDir, DBG_LINE_COLOR, outputStream);

    drawQuad(wsCurrentCellPosition, camDir, dbgColor, outputStream);
#endif

    PlaneDesc planeDesc;

    /*
    for (uint iii = 0u; iii < NUM_CLOSEST_CELLS; ++iii)
    {
        const uint targetCellIdx = closestCellIdxs[iii];
        if (targetCellIdx == INVALID_CELL_IDX)
        { break; }
        const float3 targetCellPosition = cellPosition(targetCellIdx);

        const float3 planePosition = (currentCellPosition + targetCellPosition) / 2.0f;
        const float3 planeNormal = normalize(targetCellPosition - currentCellPosition);
        initializePlane(planeDesc.vertices, planeDesc.lines, planeDesc.linesUsed, planePosition, planeNormal, 0.05f);

        //drawQuad(planeDesc, iii / ((float)NUM_CLOSEST_CELLS), outputStream);
        drawQuad(planeDesc, 1.0f, outputStream);
    }
    */
}

#endif // ENGINE_VORONOI_RL_VORONOI_GS_HLSL
