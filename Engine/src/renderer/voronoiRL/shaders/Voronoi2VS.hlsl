/**
 * @file renderer/voronoiRL/shaders/VoronoiVS2.hlsl
 * @author Tomas Polasek
 * @brief Vertex shader used in Voronoi shaders.
 */

#ifndef ENGINE_VORONOI_RL_VORONOI_VS2_HLSL
#define ENGINE_VORONOI_RL_VORONOI_VS2_HLSL

#include "VoronoiShadersCompat.h"

/**
 * Transform input vertices for 3D Voronoi.
 */
FragmentData2 main(VertexData2 input)
{
    FragmentData2 output;

    output.ssPosition = float4(
        // x = <-1.0f, 3.0f>
        ((float)(input.id / 2)) * 4.0f - 1.0f, 
        // y = <-1.0f, 3.0f>
        ((float)(input.id % 2)) * 4.0f - 1.0f, 
        0.0f, 1.0f
    );

    output.texCoord = float2(
        // u = <0.0f, 2.0f> -> <0.0f, 1.0f> in quad.
        ((float)(input.id / 2)) * 2.0f, 
        // v = <0.0f, 1.0f> -> <0.0f, 1.0f> in quad.
        1.0f - ((float)(input.id % 2)) * 2.0f
    );

    return output;
}

#endif // ENGINE_VORONOI_RL_VORONOI_VS2_HLSL
