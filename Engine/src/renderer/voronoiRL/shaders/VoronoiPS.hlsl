/**
 * @file renderer/voronoiRL/shaders/VoronoiPS.hlsl
 * @author Tomas Polasek
 * @brief Pixel shader used in Voronoi shaders.
 */

#ifndef ENGINE_VORONOI_RL_VORONOI_PS_HLSL
#define ENGINE_VORONOI_RL_VORONOI_PS_HLSL

#include "VoronoiShadersCompat.h"

/**
 * Process fragments and colorize them.
 */
PixelData main(FragmentData input)
{
    PixelData output;

    output.color = float4(1.0f, 0.0f, 1.0f, 1.0f);
    output.color = float4(input.texCoord, 0.0f, 1.0f);
    output.color = float4(input.color, 1.0f);

    return output;
}

#endif // ENGINE_VORONOI_RL_VORONOI_PS_HLSL
