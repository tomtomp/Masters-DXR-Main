/**
 * @file renderer/voronoiRL/shaders/VoronoiVS.hlsl
 * @author Tomas Polasek
 * @brief Vertex shader used in Voronoi shaders.
 */

#ifndef ENGINE_VORONOI_RL_VORONOI_VS_HLSL
#define ENGINE_VORONOI_RL_VORONOI_VS_HLSL

#include "VoronoiShadersCompat.h"

/**
 * Transform input vertices for Voronoi generation in 
 * geometry shader.
 */
GeometryData main(VertexData input)
{
    GeometryData output;

    output.position = input.position;
    output.cellIndex = input.id;

    return output;
}

#endif // ENGINE_VORONOI_RL_VORONOI_VS_HLSL
