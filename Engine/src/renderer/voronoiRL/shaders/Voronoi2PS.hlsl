/**
 * @file renderer/voronoiRL/shaders/VoronoiPS2.hlsl
 * @author Tomas Polasek
 * @brief Pixel shader used in Voronoi shaders.
 */

#ifndef ENGINE_VORONOI_RL_VORONOI_PS_HLSL
#define ENGINE_VORONOI_RL_VORONOI_PS_HLSL

#include "VoronoiShadersCompat.h"

/**
 * Calculate the final color for current fragment.
 * @param shadingInfo Filled structure with shading information.
 * @return Returns the final color.
 */
float4 calcMissColor(in ShadingInformation shadingInfo)
{
    return pbrMissColor(shadingInfo.rayDirection, shadingInfo.backgroundColor, 
        shadingInfo.lightColor, shadingInfo.lightDirection);
}

/**
 * Calculate the final color for current ray.
 * @param deferredInfo Filled structure with deferred information.
 * @param shadingInfo Filled structure with shading information.
 * @return Returns the final color.
 */
float4 calcFinalColor(in DeferredInformation deferredInfo, in ShadingInformation shadingInfo)
{
    return pbrHitColor(shadingInfo.rayDirection, deferredInfo.wsNormal,
        deferredInfo.metallicRoughness.r, deferredInfo.metallicRoughness.g, deferredInfo.diffuse,
        shadingInfo.ambientOcclusion, shadingInfo.shadowOcclusion,
        shadingInfo.lightDirection, shadingInfo.lightColor, shadingInfo.reflectionColor);
}

float2 random2(in float2 p) 
{ return frac(sin(float2(dot(p, float2(127.1f, 311.7f)), dot(p, float2(269.5f, 183.3f)))) * 43758.5453f); }

float3 random3(in float p) 
{ 
    return frac(sin(float3(
        dot(float3(p, p, p), float3(127.1f, 311.7f, 137.3f)), 
        dot(float3(p, p, p), float3(269.5f, 183.3f, 137.1f)), 
        dot(float3(p, p, p), float3(183.3f, 311.7f, 736.3f))
        )) * 43758.5453f); 
}

/// Calculate color of the current Voronoi cell.
float4 calcCellColor1(in DeferredInformation deferredInfo, in ShadingInformation shadingInfo)
{
    float2 position = deferredInfo.ssTexCoord;
    position *= 6.0f;

    float2 positionFloor = floor(position);
    float2 positionFract = frac(position);

    float minDistance = HLSL_FLT_MAX;

    for (int y = -1; y <= 1; y++) 
    {
        for (int x = -1; x <= 1; x++) 
        {
            const float2 neighbor = float2(x, y);
            const float2 pBase = random2(positionFloor + neighbor);
            const float2 p = 0.5f + 0.5f * sin(gDynamic.time + 6.2831f * pBase);
            const float2 diff = neighbor + p - positionFract;
            const float dist = length(diff);
            minDistance = min(minDistance, dist);
        }
    }

    return float4(minDistance, minDistance, minDistance, 1.0f);
}

/// Calculate distance of 2 cell centers.
float cellDistance(in float3 first, in float3 second)
{ return distance(first, second); }

/// Get cell center position for cell with given index.
float3 cellPosition(in uint cellIndex)
{ 
    return gCellData.cells[cellIndex].position.xyz * 
        sin(random3(frac(cellIndex / ((float)gStatic.cellCount))) * 
            gDynamic.time + float3(137.0f, 317.3f, 237.1f)); 
}

/// Source: https://thebookofshaders.com/06/
float3 rgb2hsb(in float3 c) 
{
    float4 K = float4(0.0f, -1.0f / 3.0f, 2.0f / 3.0f, -1.0f);
    float4 p = lerp(float4(c.bg, K.wz),
        float4(c.gb, K.xy),
        step(c.b, c.g));
    float4 q = lerp(float4(p.xyw, c.r),
        float4(c.r, p.yzx),
        step(p.x, c.r));
    float d = q.x - min(q.w, q.y);
    float e = 1.0e-10;
    return float3(abs(q.z + (q.w - q.y) / (6.0f * d + e)),
        d / (q.x + e),
        q.x);
}

/// Source: https://www.shadertoy.com/view/MsS3Wc
float3 hsb2rgb(in float3 c) {
    float3 rgb = clamp(
        abs(
            fmod(c.x * 6.0f + float3(0.0f, 4.0f, 2.0f), 
                6.0f) - 
            3.0f) - 
        1.0f, 0.0f, 1.0f);
    rgb = rgb * rgb * (3.0f - 2.0f * rgb);
    return c.z * lerp(float3(1.0f, 1.0f, 1.0f), rgb, c.y);
}

/// Calculate color of the current Voronoi cell.
float4 calcCellColor2(in DeferredInformation deferredInfo, in ShadingInformation shadingInfo)
{
    const uint cellCount = gStatic.cellCount;

    const float3 currentPosition = deferredInfo.wsPosition;

    uint minCellIndex = 0u;
    float minCellDistance = HLSL_FLT_MAX;

    for (uint iii = 0u; iii < cellCount; ++iii)
    {
        const float3 testedCellPosition = cellPosition(iii);

        const float testedDistance = cellDistance(currentPosition, testedCellPosition);

        if (testedDistance < minCellDistance)
        {
            minCellIndex = iii;
            minCellDistance = testedDistance;
        }
    }

    const float3 currentCellPosition = cellPosition(minCellIndex);

    const float colorGradient = 1.0f - smoothstep(0.0f, 1.0f, minCellDistance);
    const float borderGradient = smoothstep(0.0f, 0.7f, minCellDistance);

    const float3 color = hsb2rgb(float3(minCellIndex / ((float)cellCount), 1.0f, colorGradient));
    const float3 borderColor = float3(0.0f, 0.0f, 0.0f);
    const float3 finalColor = lerp(color, borderColor, borderGradient);
    //const float3 finalColor = colorGradient * color;

    const float3 viewDirection = shadingInfo.rayDirection;
    const float3 cellDirection = normalize(currentCellPosition - gDynamic.cameraWorldPosition);

    const float cellCenterDistance = 1.0f - abs(dot(viewDirection, cellDirection));

    //return float4(float3(cellCenterDistance, cellCenterDistance, cellCenterDistance), 1.0f);
    //return float4(float3(minCellDistance, minCellDistance, minCellDistance) * 2.0f + (1.0f - step(0.0001f, cellCenterDistance)) * float3(0.8, 0.3, 0.3), 1.0f);
    //return float4(float3(minCellDistance, minCellDistance, minCellDistance), 1.0f);
    return float4(finalColor, 1.0f);
    //return float4(float3(colorGradient, colorGradient, colorGradient), 1.0f);
    //return float4(float3(borderGradient, borderGradient, borderGradient), 1.0f);
}

/**
 * Process fragments and colorize them.
 */
PixelData2 main(FragmentData2 input)
{
    PixelData2 output;

    DeferredInformation deferredInfo = prepareDeferredInformation(input.texCoord, gDynamic.cameraWorldPosition);
    ShadingInformation shadingInfo = prepareShadingInformation(deferredInfo);

    // Using the first light, which is the "sun".
    fillLightInformation(gStatic.enableShadows, 0u, deferredInfo, shadingInfo);
    // Calculate ambient occlusion in screen space.
    fillAmbientOcclusion(gStatic.enableAo, deferredInfo, shadingInfo);
    // Calculate screen-space reflections.
    fillReflection(gStatic.doReflections, deferredInfo, shadingInfo);

    deferredInfo.diffuse = calcCellColor2(deferredInfo, shadingInfo);

    float4 finalColor = 0.0f;
    if (deferredInfo.hasData)
    //{ finalColor = calcFinalColor(deferredInfo, shadingInfo); }
    { finalColor = deferredInfo.diffuse * (1.0f - shadingInfo.ambientOcclusion); }
    else
    { finalColor = calcMissColor(shadingInfo); }

    output.color = finalColor;
    return output;
}

#endif // ENGINE_VORONOI_RL_VORONOI_PS_HLSL
