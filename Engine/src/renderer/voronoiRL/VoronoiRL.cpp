/**
 * @file renderer/voronoiRL/VoronoiRL.cpp
 * @author Tomas Polasek
 * @brief Render layer used for generation of 3D 
 * Voronoi diagrams.
 */

#include "stdafx.h"

#include "engine/renderer/voronoiRL/VoronoiRL.h"

#include "engine/renderer/voronoiRL/shaders/VoronoiVS.h"
#include "engine/renderer/voronoiRL/shaders/VoronoiGS.h"
#include "engine/renderer/voronoiRL/shaders/VoronoiPS.h"

#include "engine/renderer/voronoiRL/shaders/Voronoi2VS.h"
#include "engine/renderer/voronoiRL/shaders/Voronoi2PS.h"

#include "engine/gui/GuiSubSystem.h"

namespace quark
{

namespace rndr
{

VoronoiRL::VoronoiRL(RenderSubSystem &renderer) :
    mRenderer{ &renderer }
{ initializeResources(); }

VoronoiRL::PtrT VoronoiRL::create(RenderSubSystem &renderer)
{ return PtrT{ new VoronoiRL(renderer) }; }

VoronoiRL::~VoronoiRL()
{ /* Automatic */ }

void VoronoiRL::setDeferredBuffers(DeferredBuffers buffers)
{ mR.deferredBuffers = buffers; }

void VoronoiRL::setShadowMapBuffers(ShadowMapBuffers buffers, ShadowMapRL::DirectionalLights lights)
{
    mR.shadowMapBuffers = buffers;
    mC.lights = lights;
}

void VoronoiRL::setAmbientOcclusionBuffers(AmbientOcclusionBuffers buffers)
{ mR.ambientOcclusionBuffers = buffers; }

void VoronoiRL::resize(uint32_t width, uint32_t height)
{
    mR.scissorRect = ::CD3DX12_RECT(
        0u, 0u, 
        width, height);
    mR.viewport = ::CD3DX12_VIEWPORT{ 
        0.0f, 0.0f, 
        static_cast<float>(width), static_cast<float>(height) };
    mR.depthStencilTexture = res::rndr::DepthStencilBuffer::create(width, height, mRenderer->device());
}

void VoronoiRL::prepare()
{
    PROF_SCOPE("VoronoiRLPrep");

    mPrepared = true;
}

void VoronoiRL::prepareGui(gui::GuiSubSystem &gui)
{
    gui.addGuiContext("/VoronoiRL/")
        .setType(gui::GuiContextType::Category)
        .setLabel("Voronoi Pass");

    gui.addGuiVariable<gui::InputInt>("VoronoiRlCellCount", "/VoronoiRL/", { })
        .setLabel("Cell Count")
        .cfg()
            .setMin(0)
            .setMax(MAX_VORONOI_CELLS);

    gui.addGuiVariable<gui::InputFloat>("VoronoiRlCellSize", "/VoronoiRL/", { })
        .setLabel("Cell Size")
        .cfg()
            .setMin(0.001f);

    gui.addGuiVariable<gui::Button>("VoronoiRlReloadShaders", "/VoronoiRL/", { [&] ()
    { mR.reloadShaders = true; } }).setLabel("Reload Shaders");

    gui.addGuiVariable<gui::Position3>("VoronoiRlSunDirection", "/VoronoiRL/", { })
        .setLabel("Sun Direction");
    gui.addGuiVariable<gui::Color3>("VoronoiRlSunColor", "/VoronoiRL/", { })
        .setLabel("Sun Color");
    gui.addGuiVariable<gui::Color3>("VoronoiRlSkyColor", "/VoronoiRL/", { })
        .setLabel("Sky Color");
    gui.addGuiVariable<gui::InputBool>("VoronoiRlEnableShadows", "/VoronoiRL/", { })
        .setLabel("Enable Shadows");
    gui.addGuiVariable<gui::InputInt>("VoronoiRlPcfKernelHalfSize", "/VoronoiRL/", { })
        .setLabel("PCF Half-Size")
        .cfg().setMin(0);

    gui.addGuiVariable<gui::InputBool>("VoronoiRlEnableAO", "/VoronoiRL/", { })
        .setLabel("Enable AO");

    gui.addGuiVariable<gui::InputBool>("VoronoiRlEnableReflections", "/VoronoiRL/", { })
        .setLabel("Enable Reflections");
}

void VoronoiRL::render(const helpers::math::Camera &camera, 
    const ::CD3DX12_CPU_DESCRIPTOR_HANDLE &targetRTV, 
    res::d3d12::D3D12CommandList &cmdList)
{
    // Skip if we didn't initialize the size.
    if (!mPrepared)
    { return; }

    GPU_PROF_SCOPE(cmdList, "VoronoiRLGpu");

    // Check if shader reloading is required.
    checkReloadShaders();

    // Update the constant buffers with new camera.
    updateCamera(camera);
    // Update other constants.
    updateConstants();
    // Re-upload constant buffers.
    refreshConstantBuffers();

    // Check if cell data needs updating and update them.
    updateCellData(cmdList);

    // Clear the depth-stencil buffer.
    mR.depthStencilTexture->clear(cmdList);

    // Prepare GPU state:
    cmdList->SetPipelineState(mR.pipelineState->get());
    cmdList->RSSetViewports(1u, &mR.viewport);
    cmdList->RSSetScissorRects(1u, &mR.scissorRect);
    cmdList->SetGraphicsRootSignature(mR.rootSig->get());

    // Set the cell vertex data.
    cmdList->IASetPrimitiveTopology(DEFAULT_PRIMITIVE_TOPOLOGY);
    //cmdList->IASetVertexBuffers(0u, 1u, mR.cellVertexBuffer->vbw());
    cmdList->IASetVertexBuffers(0u, 0u, nullptr);
    cmdList->IASetIndexBuffer(nullptr);

    // Bind all of the G-Buffer render-targets.
    const ::D3D12_CPU_DESCRIPTOR_HANDLE rtvs[] {
        targetRTV
    };
    const auto dsv{ mR.depthStencilTexture->dsv() };
    cmdList->OMSetRenderTargets(util::count<::UINT>(rtvs), rtvs, false, &dsv);

    // We are going to read the G-Buffers from compute shaders.
    mR.deferredViews.createViews(mR.deferredBuffers, mR.deferredTable, ResourceViewType::ShaderResource);
    mR.deferredBuffers.transitionResourcesTo(cmdList, ::D3D12_RESOURCE_STATE_NON_PIXEL_SHADER_RESOURCE | ::D3D12_RESOURCE_STATE_PIXEL_SHADER_RESOURCE);
    // We are going to read the shadow map buffers from shaders.
    mR.shadowMapViews.createViews(mR.shadowMapBuffers, mR.shadowMapTable, ResourceViewType::ShaderResource);
    mR.shadowMapBuffers.transitionResourcesTo(cmdList, ::D3D12_RESOURCE_STATE_NON_PIXEL_SHADER_RESOURCE | ::D3D12_RESOURCE_STATE_PIXEL_SHADER_RESOURCE);
    // Using the ambient occlusion.
    mR.ambientOcclusionViews.createViews(mR.ambientOcclusionBuffers, mR.miscTable, ResourceViewType::ShaderResource);
    mR.ambientOcclusionBuffers.transitionResourcesTo(cmdList, ::D3D12_RESOURCE_STATE_NON_PIXEL_SHADER_RESOURCE | ::D3D12_RESOURCE_STATE_PIXEL_SHADER_RESOURCE);

    ::ID3D12DescriptorHeap *heaps[] = {
        mR.descHeap->get()
    };
    cmdList->SetDescriptorHeaps(static_cast<::UINT>(util::count(heaps)), heaps);

    // Set the slow-changing constants.
    cmdList->SetGraphicsRootConstantBufferView(
        compatVoronoi::GlobalRS::StaticConstantBuffer, 
        mR.staticConstantsBuffer->gpuAddress());

    // Set the fast-changing constants.
    cmdList->SetGraphicsRootConstantBufferView(
        compatVoronoi::GlobalRS::DynamicConstantBuffer, 
        mR.dynamicConstantsBuffer->gpuAddress());

    // Set the cell data.
    cmdList->SetGraphicsRootConstantBufferView(
        compatVoronoi::GlobalRS::CellDataBuffer, 
        mR.cellDataBuffer->gpuAddress());

    // Array of G-Buffer textures.
    cmdList->SetGraphicsRootDescriptorTable(
        compatVoronoi::GlobalRS::DeferredBuffers, 
        mR.deferredTable.baseHandle());

    // Shadow map textures.
    cmdList->SetGraphicsRootDescriptorTable(
        compatVoronoi::GlobalRS::ShadowMapBuffers, 
        mR.shadowMapTable.baseHandle());

    // Ambient occlusion textures.
    cmdList->SetGraphicsRootDescriptorTable(
        compatVoronoi::GlobalRS::AmbientOcclusionBuffers, 
        mR.ambientOcclusionViews.ambientOcclusion->gpuHandle());

    // One point for each cell center.
    //cmdList->DrawInstanced(mC.cellCount, 1u, 0u, 0);
    // Render 3 vertices for full-screen triangle -> quad.
    cmdList->DrawInstanced(3u, 1u, 0u, 0u);
}

void VoronoiRL::updateGui(gui::GuiSubSystem &gui)
{
    auto &cellCount{ gui.getVariable<gui::InputInt>("VoronoiRlCellCount") };
    mR.generateCellData |= gui::synchronizeVariables(cellCount, mC.cellCount);
    auto &cellSize{ gui.getVariable<gui::InputFloat>("VoronoiRlCellSize") };
    mR.generateCellData |= gui::synchronizeVariables(cellSize, mC.cellSize);

    auto &sunDirection{ gui.getVariable<gui::Position3>("VoronoiRlSunDirection") };
    mR.staticConstantsChanged |= gui::synchronizeVariables(sunDirection, mR.staticConstants.lightDirection);
    auto &sunColor{ gui.getVariable<gui::Color3>("VoronoiRlSunColor") };
    mR.staticConstantsChanged |= gui::synchronizeVariables(sunColor, mR.staticConstants.lightColor);
    auto &skyColor{ gui.getVariable<gui::Color3>("VoronoiRlSkyColor") };
    mR.staticConstantsChanged |= gui::synchronizeVariables(skyColor, mR.staticConstants.backgroundColor);
    auto &enableShadows{ gui.getVariable<gui::InputBool>("VoronoiRlEnableShadows") };
    mR.staticConstantsChanged |= gui::synchronizeVariables(enableShadows, mR.staticConstants.enableShadows);
    auto &pcfHalfSize{ gui.getVariable<gui::InputInt>("VoronoiRlPcfKernelHalfSize") };
    mR.staticConstantsChanged |= gui::synchronizeVariables(pcfHalfSize, mR.staticConstants.shadowPcfKernelSize);

    auto &enableAo{ gui.getVariable<gui::InputBool>("VoronoiRlEnableAO") };
    mR.staticConstantsChanged |= gui::synchronizeVariables(enableAo, mR.staticConstants.enableAo);

    auto &enableReflections{ gui.getVariable<gui::InputBool>("VoronoiRlEnableReflections") };
    mR.staticConstantsChanged |= gui::synchronizeVariables(enableReflections, mR.staticConstants.doReflections);
}

const dxtk::math::Vector3 &VoronoiRL::getLightDirection() const
{ return mR.staticConstants.lightDirection; }

void VoronoiRL::setLightDirection(const dxtk::math::Vector3 &lightDir)
{
    mR.staticConstants.lightDirection = lightDir;
    mR.staticConstantsChanged = true;
}

void VoronoiRL::createConstantBuffers()
{
    mR.staticConstantsBuffer = StaticConstantsBuffer::create();
    mR.staticConstants.lightDirection = dxtk::math::Vector3{ 
        DEFAULT_LIGHT_DIR_X, DEFAULT_LIGHT_DIR_Y, DEFAULT_LIGHT_DIR_Z };
    mR.staticConstants.lightColor = dxtk::math::Vector3{ 
        DEFAULT_LIGHT_COLOR_R, DEFAULT_LIGHT_COLOR_G, DEFAULT_LIGHT_COLOR_B };
    mR.staticConstants.backgroundColor = dxtk::math::Vector3{ 
        DEFAULT_BACKGROUND_COLOR_R, DEFAULT_BACKGROUND_COLOR_G, DEFAULT_BACKGROUND_COLOR_B };
    mR.staticConstants.enableShadows = DEFAULT_ENABLE_SHADOWS;
    mR.staticConstants.shadowPcfKernelSize = DEFAULT_PCF_KERNEL_HALF_SIZE;
    mR.staticConstants.enableAo = DEFAULT_ENABLE_AO;
    mR.staticConstants.doReflections = DEFAULT_DO_REFLECTIONS;
    mR.staticConstants.cellCount = mC.cellCount;
    mR.staticConstantsBuffer->at(0u) = mR.staticConstants;
    mR.staticConstantsBuffer->reallocate(mRenderer->device(), L"StaticConstantsBuffer");

    mR.dynamicConstantsBuffer = DynamicConstantsBuffer::create();
    mR.dynamicConstantsBuffer->at(0u) = mR.dynamicConstants;
    mR.dynamicConstantsBuffer->reallocate(mRenderer->device(), L"DynamicConstantsBuffer");
}

void VoronoiRL::initializeResources()
{
    PROF_SCOPE("VoronoiRLInitialization");

    // Prepare root signature.
    quark::helpers::d3d12::D3D12RootSignatureBuilder rsb;

    // Static, slow-changing constants, register(b0, space0).
    rsb[compatVoronoi::GlobalRS::StaticConstantBuffer].asConstantBufferViewParameter(
        compatVoronoi::StaticConstantsSlot.reg, compatVoronoi::StaticConstantsSlot.space);
    // Dynamic, fast-changing constants, register(b1, space0).
    rsb[compatVoronoi::GlobalRS::DynamicConstantBuffer].asConstantBufferViewParameter(
        compatVoronoi::DynamicConstantsSlot.reg, compatVoronoi::DynamicConstantsSlot.space);
    // Cell data, register(b2, space0).
    rsb[compatVoronoi::GlobalRS::CellDataBuffer].asConstantBufferViewParameter(
        compatVoronoi::CellDataSlot.reg, compatVoronoi::CellDataSlot.space);

    // G-Buffer textures, generated by the deferred pass, starting at register(t0, space1).
    const ::CD3DX12_DESCRIPTOR_RANGE1 deferredTextureRange[]{
        { // Texture array SRVs: 
            ::D3D12_DESCRIPTOR_RANGE_TYPE_SRV, 
            DeferredBufferViews::DEFERRED_BUFFER_COUNT, 
            // register(t0, space1)
            compatVoronoi::DeferredBuffersSlot.reg, compatVoronoi::DeferredBuffersSlot.space, 
            ::D3D12_DESCRIPTOR_RANGE_FLAG_DATA_STATIC
        }
    };
    rsb[compatVoronoi::GlobalRS::DeferredBuffers].asDescriptorTableParameter(
        util::count<::UINT>(deferredTextureRange), deferredTextureRange);

    // Shadow maps, generated by the shadow map render layer, starting at register(t0, space2).
    const ::CD3DX12_DESCRIPTOR_RANGE1 shadowMapTextureRange[]{
        { // Texture array SRVs: 
            ::D3D12_DESCRIPTOR_RANGE_TYPE_SRV, 
            // Bounded number of descriptors
            HLSL_SHADOW_MAP_UPPER_BOUND, 
            // register(t0, space2)
            compatVoronoi::ShadowMapBuffersSlot.reg, compatVoronoi::ShadowMapBuffersSlot.space, 
            ::D3D12_DESCRIPTOR_RANGE_FLAG_DATA_STATIC
        }
    };
    rsb[compatVoronoi::GlobalRS::ShadowMapBuffers].asDescriptorTableParameter(
        util::count<::UINT>(shadowMapTextureRange), shadowMapTextureRange);

    // Ambient occlusion, generated by the ambient occlusion render layer, starting at register(t0, space3).
    const ::CD3DX12_DESCRIPTOR_RANGE1 ambientOcclusionTextureRange[]{
        { // Texture array SRVs: 
            ::D3D12_DESCRIPTOR_RANGE_TYPE_SRV, 
            // Only the ambient occlusion output.
            1u, 
            // register(t0, space3)
            compatVoronoi::AmbientOcclusionSlot.reg, compatVoronoi::AmbientOcclusionSlot.space, 
            ::D3D12_DESCRIPTOR_RANGE_FLAG_DATA_STATIC
        }
    };
    rsb[compatVoronoi::GlobalRS::AmbientOcclusionBuffers].asDescriptorTableParameter(
        util::count<::UINT>(ambientOcclusionTextureRange), ambientOcclusionTextureRange);

    // Common sampler used for sampling material textures, register(s0, space0).
    const ::CD3DX12_STATIC_SAMPLER_DESC commonSampler{ 
        compatVoronoi::SamplerSlot.reg, 
        ::D3D12_FILTER_MIN_MAG_MIP_POINT, 
        //::D3D12_FILTER_MIN_MAG_MIP_LINEAR, 
        ::D3D12_TEXTURE_ADDRESS_MODE_BORDER, 
        ::D3D12_TEXTURE_ADDRESS_MODE_BORDER, 
        ::D3D12_TEXTURE_ADDRESS_MODE_BORDER, 
        0.0f, 16u,
        ::D3D12_COMPARISON_FUNC_LESS,
        ::D3D12_STATIC_BORDER_COLOR_OPAQUE_BLACK,
        0.0f, D3D12_FLOAT32_MAX,
        ::D3D12_SHADER_VISIBILITY_ALL,
        compatVoronoi::SamplerSlot.space
    };
    rsb.addStaticSampler(commonSampler);

    // Linear shadow map sampler, register(s1, space0).
    const ::CD3DX12_STATIC_SAMPLER_DESC shadowMapSampler{ 
        compatVoronoi::ShadowSamplerSlot.reg, 
        //::D3D12_FILTER_MIN_MAG_MIP_POINT, 
        ::D3D12_FILTER_COMPARISON_MIN_MAG_MIP_LINEAR, 
        ::D3D12_TEXTURE_ADDRESS_MODE_BORDER, 
        ::D3D12_TEXTURE_ADDRESS_MODE_BORDER, 
        ::D3D12_TEXTURE_ADDRESS_MODE_BORDER, 
        0.0f, 16u,
        ::D3D12_COMPARISON_FUNC_GREATER_EQUAL,
        ::D3D12_STATIC_BORDER_COLOR_OPAQUE_WHITE,
        0.0f, D3D12_FLOAT32_MAX,
        ::D3D12_SHADER_VISIBILITY_ALL,
        compatVoronoi::ShadowSamplerSlot.space
    };
    rsb.addStaticSampler(shadowMapSampler);

    // Enable stages which are used in deferred pipeline.
    rsb.setFlags(
        D3D12_ROOT_SIGNATURE_FLAG_ALLOW_INPUT_ASSEMBLER_INPUT_LAYOUT |
        D3D12_ROOT_SIGNATURE_FLAG_DENY_HULL_SHADER_ROOT_ACCESS |
        D3D12_ROOT_SIGNATURE_FLAG_DENY_DOMAIN_SHADER_ROOT_ACCESS);
    // TODO - Add ALLOW_STREAM_OUTPUT ^^

    mR.rootSig = rsb.build(mRenderer->device());

    mR.inputLayout.clear();

    // Vertex position.
    mR.inputLayout.addElement(
        "POSITION", 0u, 
        DEFAULT_VERTEX_POSITION_FORMAT, compatVoronoi::InputLayout::Position, 
        D3D12_APPEND_ALIGNED_ELEMENT, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0u);

    // Load shaders.
    mR.vShader = res::d3d12::D3D12Shader::create(gVoronoi2Vs, sizeof(gVoronoi2Vs));
    //mR.gShader = res::d3d12::D3D12Shader::create(gVoronoiGs, sizeof(gVoronoiGs));
    mR.pShader = res::d3d12::D3D12Shader::create(gVoronoi2Ps, sizeof(gVoronoi2Ps));

    // Build the pipeline state.
    buildPipeline();

    // Initialize to some default values...
    resize(1u, 1u);

    // Create static and dynamic constant buffers.
    createConstantBuffers();

    // Create descriptor heaps for SRVs, CBVs and UAVs.
    createDescHeapLayout(mR.descHeap, mR.miscTable, mR.deferredTable, mR.shadowMapTable);

    // Create descriptors in the miscellaneous descriptor table.
    fillMiscTable(mR.miscTable);
}

void VoronoiRL::buildPipeline()
{
    quark::helpers::d3d12::D3D12PipelineStateSOBuilder pssob;
    quark::helpers::d3d12::D3D12PipelineStateBuilder psb;
    psb << 
        mR.rootSig->rsState() <<
        mR.inputLayout.ilState() <<
        //pssob.rasterizer(::D3D12_CULL_MODE_BACK, 
        pssob.rasterizer(::D3D12_CULL_MODE_NONE, 
            !mRenderer->cfg().renderRasterizerFrontClockwise, 
            mRenderer->cfg().renderRasterizerFill) << 
        pssob.primitiveTopology(DEFAULT_PRIMITIVE_TOPOLOGY_TYPE) << 
        mR.vShader->vsState() << 
        //mR.gShader->gsState() << 
        mR.pShader->psState() << 
        pssob.depthStencilFormat(
            res::rndr::DepthStencilBuffer::DEFAULT_FORMAT) << 
        pssob.renderTargetFormats({ 
            DEFAULT_RENDER_TARGET_FORMAT });
    mR.pipelineState = psb.build(mRenderer->device());
}

std::vector<compatVoronoi::CellData> VoronoiRL::createCellData(
    uint32_t cellCount, float cellSize)
{
    // Initialize holder of cell data.
    std::vector<compatVoronoi::CellData> cellData;
    cellData.resize(cellCount);

    // Initialize the random number generation.
    std::random_device randomDev;
    std::mt19937 randomEngine(randomDev());
    std::uniform_real_distribution<float> randomDist(-cellSize, cellSize);

    // Initialize random texture values.
    for (auto &cell : cellData)
    {
        cell.position.x = randomDist(randomEngine);
        cell.position.y = randomDist(randomEngine);
        cell.position.z = randomDist(randomEngine);
    }

    return cellData;
}

void VoronoiRL::createCells(uint32_t cellCount, float cellSize, 
    res::d3d12::D3D12CommandList &directCmdList)
{
    /// Generate random cell data.
    const auto cellData = createCellData(cellCount, cellSize);

    const auto allocator{ mRenderer->memoryMgr().committedAllocator() };
    auto &updater{ mRenderer->memoryMgr().updater() };

    /// Upload cells for use as vertices.
    if (!mR.cellVertexBuffer)
    {
        mR.cellVertexBuffer = helpers::d3d12::D3D12VertexBuffer::create();
        mR.cellVertexBuffer->setDebugName(L"CellVertexBuffer");
    }
    mR.cellVertexBuffer->allocateUpload(
        cellData.begin(), cellData.end(), directCmdList, allocator, updater);
    mR.cellVertexBuffer->transitionTo(::D3D12_RESOURCE_STATE_VERTEX_AND_CONSTANT_BUFFER, directCmdList);

    /// Upload cells for access from the shaders.
    if (!mR.cellDataBuffer)
    { mR.cellDataBuffer = CellDataBuffer::create(); }
    mR.cellDataBuffer->assignCopy(cellData);
    mR.cellDataBuffer->reallocate(mRenderer->device(), L"CellDataBuffer");
    mR.cellDataBuffer->uploadToGPU();

    mR.generateCellData = false;
}

uint32_t VoronoiRL::calculateMiscDescriptorCount()
{
    uint32_t result{ 0u };

    // Static constant buffer CBV: 
    result += 1u;
    // Dynamic constant buffer CBV: 
    result += 1u;
    // Cell data buffer CBV: 
    result += 1u;
    // Ambient occlusion texture: 
    result += 1u;

    return result;
}

uint32_t VoronoiRL::calculateDeferredDescriptorCount()
{ return DeferredBufferViews::DEFERRED_BUFFER_COUNT; }

uint32_t VoronoiRL::calculateShadowMapDescriptorCount()
{ return HLSL_SHADOW_MAP_UPPER_BOUND; }

res::d3d12::D3D12DescHeap::PtrT VoronoiRL::createDescHeap(std::size_t requiredDescriptors)
{
    return res::d3d12::D3D12DescHeap::create(mRenderer->device(), 
        ::D3D12_DESCRIPTOR_HEAP_TYPE_CBV_SRV_UAV,
        static_cast<::uint32_t>(requiredDescriptors),
        ::D3D12_DESCRIPTOR_HEAP_FLAG_SHADER_VISIBLE);
}

helpers::d3d12::D3D12DescTable VoronoiRL::createMiscTable(
    res::d3d12::D3D12DescHeap &descHeap, uint32_t numDescriptors)
{
    return helpers::d3d12::D3D12DescTable(
        descHeap, descHeap.allocateTable(numDescriptors));
}

helpers::d3d12::D3D12DescTable VoronoiRL::createDeferredTable(res::d3d12::D3D12DescHeap &descHeap,
    uint32_t numDescriptors)
{
    return helpers::d3d12::D3D12DescTable(
        descHeap, descHeap.allocateTable(numDescriptors));
}

helpers::d3d12::D3D12DescTable VoronoiRL::createShadowMapTable(res::d3d12::D3D12DescHeap &descHeap,
    uint32_t numDescriptors)
{
    // Create the table and initialize null handles, so we can debug.
    helpers::d3d12::D3D12DescTable result(
        descHeap, descHeap.allocateTable(numDescriptors));
    result.initializeSrvNullHandles(::D3D12_SRV_DIMENSION_TEXTURE2D);

    return result;
}

void VoronoiRL::createDescHeapLayout(
    res::d3d12::D3D12DescHeap::PtrT &descHeap,
    helpers::d3d12::D3D12DescTable &miscTable, 
    helpers::d3d12::D3D12DescTable &deferredTable,
    helpers::d3d12::D3D12DescTable &shadowMapTable)
{
    // Calculate descriptor requirements.
    const auto miscDescriptors{ calculateMiscDescriptorCount() };
    const auto deferredDescriptors{ calculateDeferredDescriptorCount() };
    const auto rayTracingDescriptors{ calculateShadowMapDescriptorCount() };
    const auto totalDescriptors{ miscDescriptors + deferredDescriptors + rayTracingDescriptors };

    // Check whether we need to create a new descriptor heap.
    bool newDescHeapRequired{ false };
    newDescHeapRequired |= miscDescriptors != miscTable.size();
    newDescHeapRequired |= deferredDescriptors != deferredTable.size();
    newDescHeapRequired |= rayTracingDescriptors != shadowMapTable.size();

    if (!newDescHeapRequired)
    { // No changed -> we don't need a new descriptor heap.
        return;
    }

    // Create the main descriptor heap, which will be used for all descriptors.
    auto newDescHeap{ createDescHeap(totalDescriptors) };

    // Create descriptor table for miscellaneous descriptors.
    auto newMiscTable{ createMiscTable(*newDescHeap, miscDescriptors) };
    // Create descriptor table for deferred buffers.
    auto newDeferredTable{ createDeferredTable(*newDescHeap, deferredDescriptors) };
    // Create descriptor table for ray tracing buffers.
    auto newRayTracingTable{ createShadowMapTable(*newDescHeap, rayTracingDescriptors) };

    // Change over to the new descriptor heap.
    descHeap = newDescHeap;
    miscTable = std::move(newMiscTable);
    deferredTable = std::move(newDeferredTable);
    shadowMapTable = std::move(newRayTracingTable);
}

void VoronoiRL::fillMiscTable(helpers::d3d12::D3D12DescTable &miscTable)
{
    mR.staticConstantsBuffer->reallocateDescriptor(miscTable, ResourceViewType::ConstantBuffer);
    mR.dynamicConstantsBuffer->reallocateDescriptor(miscTable, ResourceViewType::ConstantBuffer);
}

void VoronoiRL::updateCamera(const helpers::math::Camera &camera)
{
    mR.dynamicConstants.worldToCamera = camera.vpNoRecalculation();
    mR.dynamicConstants.cameraToWorld = camera.rayTracingVP();
    mR.dynamicConstants.cameraWorldPosition = camera.position();
    mR.dynamicConstants.worldToLight = mC.lights.lights[0].lightTransform;

    mR.dynamicConstantsChanged = true;
}

void VoronoiRL::updateConstants()
{
    mR.staticConstantsChanged |= mR.staticConstants.cellCount != mC.cellCount;
    mR.staticConstants.cellCount = mC.cellCount;

    mR.dynamicConstants.time = mTimer.elapsed<util::HrTimer::SecondsF>().count();
    mR.dynamicConstantsChanged = true;
}

void VoronoiRL::refreshConstantBuffers()
{
    if (mR.staticConstantsChanged)
    {
        mR.staticConstantsBuffer->at(0u) = mR.staticConstants;
        mR.staticConstantsBuffer->uploadToGPU();
        mR.staticConstantsChanged = false;
    }

    if (mR.dynamicConstantsChanged)
    {
        mR.dynamicConstantsBuffer->at(0u) = mR.dynamicConstants;
        mR.dynamicConstantsBuffer->uploadToGPU();
        mR.dynamicConstantsChanged = false;
    }
}

void VoronoiRL::updateCellData(res::d3d12::D3D12CommandList &directCmdList)
{
    if (mR.cellDataBuffer && mR.cellVertexBuffer && !mR.generateCellData)
    { return; }

    createCells(mC.cellCount, mC.cellSize, directCmdList);
    mR.cellDataBuffer->reallocateDescriptor(mR.miscTable, ResourceViewType::ConstantBuffer);
}

void VoronoiRL::checkReloadShaders()
{
    if (!mR.reloadShaders)
    { return; }

    log<Info>() << "Attempting to reload Voronoi shaders!" << std::endl;

    // Load shaders.
    res::d3d12::D3D12Shader::PtrT vShader{ };
    //res::d3d12::D3D12Shader::PtrT gShader{ };
    res::d3d12::D3D12Shader::PtrT pShader{ };
    try
    {
        vShader = res::d3d12::D3D12Shader::create(L"Voronoi2VS.cso");
        //gShader = res::d3d12::D3D12Shader::create(L"VoronoiGS.cso");
        pShader = res::d3d12::D3D12Shader::create(L"Voronoi2PS.cso");
    }
    catch (const std::exception &err)
    {
        log<Error>() << "Failed to reload Voronoi shaders! : " << err.what() << std::endl;
        mR.reloadShaders = false;
        return;
    }
    catch (...)
    {
        log<Error>() << "Failed to reload Voronoi shaders!" << std::endl;
        mR.reloadShaders = false;
        return;
    }

    mR.vShader = vShader;
    //mR.gShader = gShader;
    mR.pShader = pShader;

    // Build the pipeline state.
    buildPipeline();

    // Done...
    mR.reloadShaders = false;
}

}

}
