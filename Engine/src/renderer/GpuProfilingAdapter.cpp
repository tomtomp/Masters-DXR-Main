/**
 * @file renderer/GpuProfilingAdapter.cpp
 * @author Tomas Polasek
 * @brief Helper class connecting the profiling 
 * system with GPU timers.
 */

#include "stdafx.h"

#include "engine/renderer/GpuProfilingAdapter.h"

#include "engine/renderer/RenderSubSystem.h"

#if ENGINE_USE_PIX_MARKERS
#   include <pix.h>
#endif // ENGINE_USE_PIX_MARKERS

namespace quark
{

namespace rndr
{

namespace prof 
{

GpuProfilingAdapter::GpuProfilingAdapter(const EngineRuntimeConfig& cfg, RenderSubSystem& renderer) :
    mTimerManager{ 
        helpers::d3d12::D3D12GpuTimerManager::create(
            renderer.device(), 
            renderer.directCmdQueue(), 
            EngineConfig::GPU_PROFILING_MAX_TIMERS) },
    mProfilingContext{ util::prof::enterSimulatedThread(EngineConfig::GPU_PROFILING_THREAD_NAME) }
{ UNUSED(cfg); }

GpuProfilingAdapter::PtrT GpuProfilingAdapter::create(const EngineRuntimeConfig& cfg, RenderSubSystem& renderer)
{ return PtrT(new GpuProfilingAdapter(cfg, renderer)); }

GpuProfilingAdapter::~GpuProfilingAdapter()
{ /* Automatic */ }

void GpuProfilingAdapter::enterScope(res::d3d12::D3D12CommandList &cmdList, const char *name)
{
    mProfilingContext.enterScope(name);
    mScopeSimulators.emplace_back(mProfilingContext.lateSimulatorForCurrentScope());

    const auto scopeGpuTimer{ getCreateGpuTimer(mProfilingContext.currentScopeHash()) };
    mTimerManager->startTimer(cmdList, scopeGpuTimer);
}

void GpuProfilingAdapter::exitScope(res::d3d12::D3D12CommandList &cmdList)
{
    const auto scopeGpuTimer{ getCreateGpuTimer(mProfilingContext.currentScopeHash()) };
    mTimerManager->stopTimer(cmdList, scopeGpuTimer);

    mProfilingContext.exitScope();
}

void GpuProfilingAdapter::resolve(res::d3d12::D3D12CommandList &cmdList)
{ mTimerManager->resolveTimers(cmdList); }

void GpuProfilingAdapter::synchronize()
{
    // Map the GPU timers into CPU memory.
    mTimerManager->mapTimerResults();

    //mTimerManager.debugDumpTimerValues();

    // Remember minimal and maximal values, which are used later.
    auto minTicks{ std::numeric_limits<std::size_t>::max() };
    auto maxTicks{ std::numeric_limits<std::size_t>::min() };

#ifndef PROF_USE_TICKS
    const auto ticksPerNanosecond{ mTimerManager->gpuTicksPerSecond() / std::nano::den };
#endif // !PROF_USE_TICKS

    for (auto &scopeSimulator : mScopeSimulators)
    { // Iterate through all entered scopes.
        const auto scopeGpuTimer{ getCreateGpuTimer(scopeSimulator.scopeHash()) };
        // Recover the GPU timer value.
        const auto &timerValue{ mTimerManager->readTimerRaw(scopeGpuTimer) };

        // Get start and end times for this timer.
        auto timerStart{ timerValue.start };
        auto timerEnd{ timerValue.end };

#ifndef PROF_USE_TICKS
        // Convert to nanoseconds.
        timerStart /= ticksPerNanosecond;
        timerEnd /= ticksPerNanosecond;
#endif // !PROF_USE_TICKS

        // Remember max and min...
        minTicks = std::min(minTicks, timerStart);
        maxTicks = std::max(maxTicks, timerEnd);

        // Simulate the runtime of the target scope.
        scopeSimulator.simulateScope(timerStart, timerEnd);
    }

    // Add runtime of the GPU thread.
    mProfilingContext.simulateThreadRuntime(minTicks, maxTicks);

    // Clear entered scopes, preparing for the next iteration.
    mScopeSimulators.clear();

    // Unmap GPU timers from CPU memory.
    mTimerManager->unmapTimerResults();
}

GpuProfilingAdapter::TimerHandleT GpuProfilingAdapter::getCreateGpuTimer(std::size_t scopeHash)
{
    const auto findIt{ mScopeTimerMap.find(scopeHash) };

    TimerHandleT result;

    if (findIt == mScopeTimerMap.end())
    { // No timer has been assigned yet.
        if (!mTimerManager->hasFreeTimers())
        { throw NoMoreTimersException("Failed to create new GPU timer: No more timers are available!"); }

        // Create the timer and assign it to the scope.
        const auto scopeGpuTimer{ mTimerManager->allocateTimer() };
        mScopeTimerMap.emplace(scopeHash, scopeGpuTimer);

        result = scopeGpuTimer;
    }
    else
    { result = findIt->second; }

    return result;
}

GpuScopeProfiler::GpuScopeProfiler( GpuProfilingAdapter &profilingAdapter,
    res::d3d12::D3D12CommandList &cmdList, const char *name) :
    mProfilingAdapter{ profilingAdapter }, mCmdList{ cmdList },
    mName{ name }
{
#if ENGINE_USE_PIX_MARKERS
    PIXBeginEvent(PIX_COLOR(255, 0, 0), name);
#endif // ENGINE_USE_PIX_MARKERS

    mProfilingAdapter.enterScope(mCmdList, mName);
}

GpuScopeProfiler::~GpuScopeProfiler()
{
    mProfilingAdapter.exitScope(mCmdList);

#if ENGINE_USE_PIX_MARKERS
    PIXEndEvent(mCmdList.get());
#endif // ENGINE_USE_PIX_MARKERS
}

GpuBlockProfiler::GpuBlockProfiler( GpuProfilingAdapter &profilingAdapter, 
    res::d3d12::D3D12CommandList &cmdList, const char *name, bool manualEnd) : 
    mProfilingAdapter{ profilingAdapter }, mCmdList{ cmdList },
    mName{ name }, mEnded{ manualEnd }
{
#if ENGINE_USE_PIX_MARKERS
    PIXBeginEvent(PIX_COLOR(255, 0, 0), name);
#endif // ENGINE_USE_PIX_MARKERS

    mProfilingAdapter.enterScope(mCmdList, mName);
}

GpuBlockProfiler::~GpuBlockProfiler()
{ end(); }

void GpuBlockProfiler::end()
{
    if (mEnded)
    { return; }

    mProfilingAdapter.exitScope(mCmdList);

#if ENGINE_USE_PIX_MARKERS
    PIXEndEvent(mCmdList.get());
#endif // ENGINE_USE_PIX_MARKERS

    mEnded = true;
}

}

}

}
