/**
 * @file renderer/RenderContext.cpp
 * @author Tomas Polasek
 * @brief Rendering context containing information about current 
 * device, command lists and other resources.
 */

#include "stdafx.h"

#include "engine/renderer/RenderContext.h"

namespace quark
{

namespace rndr
{

RenderContext::~RenderContext()
{ /* Automatic */ }

RenderContext::PtrT RenderContext::create(util::PtrT<RenderSubSystem> renderer)
{ return PtrT(new RenderContext(renderer)); }

RenderSubSystem &RenderContext::rndr()
{ return *mRenderer; }

RenderContext::RenderContext(util::PtrT<RenderSubSystem> renderer) : 
    mRenderer{ renderer }
{ }

}

}
