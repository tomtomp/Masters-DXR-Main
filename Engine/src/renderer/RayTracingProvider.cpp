/**
 * @file renderer/RayTracingProvider.cpp
 * @author Tomas Polasek
 * @brief Provider of ray tracing features.
 */

#include "stdafx.h"

#include "engine/renderer/RayTracingProvider.h"

#include "engine/renderer/RenderSubSystem.h"

namespace quark {

namespace rndr {

RayTracingProvider::RayTracingProvider(const EngineRuntimeConfig& cfg, RenderSubSystem& renderer)
	: mRenderer{renderer}
{ initialize(cfg); }

RayTracingProvider::PtrT RayTracingProvider::create(const EngineRuntimeConfig& cfg, RenderSubSystem& renderer)
{ return PtrT(new RayTracingProvider(cfg, renderer)); }

RayTracingProvider::~RayTracingProvider()
{ /* Automatic */ }

bool RayTracingProvider::rayTracingSupported() const
{
	return mComputeRayTracingSupported || mHardwareRayTracingSupported;
}

bool RayTracingProvider::computeRayTracingSupported() const
{
	return mComputeRayTracingSupported;
}

bool RayTracingProvider::hardwareRayTracingSupported() const
{
#ifdef ENGINE_WITH_WIN_1809
	return mHardwareRayTracingSupported;
#else
	return false;
#endif
}

bool RayTracingProvider::usingFallbackLayer() const
{
	return mRayTracingApi == RayTracingApi::Fallback || mRayTracingApi == RayTracingApi::FallbackForceCompute;
}

bool RayTracingProvider::selectRayTracingApi(RayTracingApi capability)
{
	switch (capability)
	{
		case RayTracingApi::Fallback:
		case RayTracingApi::FallbackForceCompute:
		{
			if (!computeRayTracingSupported())
			{ return false; }

			mRayTracingApi = capability;
			initializeRayTracing();

			return true;
		}
		case RayTracingApi::Hardware:
		{
			if (!hardwareRayTracingSupported())
			{ return false; }

			mRayTracingApi = capability;
			initializeRayTracing();

			return true;
		}
		case RayTracingApi::None:
		{
			mRayTracingApi = capability;
			mRayTracingDevice = res::d3d12::D3D12RayTracingDevice::create();

			return true;
		}
		default:
		{
			return false;
		}
	}
}

bool RayTracingProvider::enableRayTracingApi()
{
	if (hardwareRayTracingSupported())
	{
		return selectRayTracingApi(RayTracingApi::Hardware);
	}
	else if (computeRayTracingSupported())
	{
		return selectRayTracingApi(RayTracingApi::FallbackForceCompute);
	}
	else
	{
		return false;
	}
}

RayTracingProvider::RayTracingApi RayTracingProvider::rayTracingBackend() const
{ return mRayTracingApi; }

std::string RayTracingProvider::rayTracingBackendStr() const
{
    switch (rayTracingBackend())
    {
    case RayTracingProvider::RayTracingApi::None:
        return "None";
    case RayTracingProvider::RayTracingApi::Fallback:
        return "FallbackCompute";
    case RayTracingProvider::RayTracingApi::FallbackForceCompute:
        return "FallbackCompute";
    case RayTracingProvider::RayTracingApi::Hardware:
        return "Hardware";
    default:
        return "Unknown";
    }
}

void RayTracingProvider::initialize(const EngineRuntimeConfig &cfg)
{
    // For now...
	UNUSED(cfg);

	mComputeRayTracingSupported = isComputeRayTracingSupported(mRenderer.adapter(), mRenderer.device());
	mHardwareRayTracingSupported = isHardwareTracingSupported(mRenderer.device());
	log<Info>() << "Compute Ray Tracing [" << (mComputeRayTracingSupported ? 'X' : ' ') << "] Hardware Ray Tracing ["
				<< (mHardwareRayTracingSupported ? 'X' : ' ') << "]" << std::endl;

	// Ray tracing should be initialized only when specified at compilation time.
#ifdef ENGINE_RAY_TRACING_ENABLED
	// Automatically choose a valid ray tracing API.
	enableRayTracingApi();
#endif // ENGINE_RAY_TRACING_ENABLED
}

bool RayTracingProvider::isComputeRayTracingSupported(const res::d3d12::D3D12Adapter& adapter, const res::d3d12::D3D12Device& device)
{
#ifdef ENGINE_RAY_TRACING_ENABLED
	::D3D12_FEATURE_DATA_D3D12_OPTIONS features{};
	const auto hr{device->CheckFeatureSupport(::D3D12_FEATURE_D3D12_OPTIONS, &features, sizeof(::D3D12_FEATURE_DATA_D3D12_OPTIONS))};
	return SUCCEEDED(hr) && adapter.experimentalFeaturesSupported() && features.ResourceBindingTier >= ::D3D12_RESOURCE_BINDING_TIER_3;
#else
	UNUSED(adapter);
	UNUSED(device);

	return false;
#endif
}

bool RayTracingProvider::isHardwareTracingSupported(const res::d3d12::D3D12Device& device)
{
#ifdef ENGINE_RAY_TRACING_ENABLED
	D3D12_FEATURE_DATA_D3D12_OPTIONS5 featureSupportData = {};

	// Querry supported features of our device.
	const auto checkResult{device->CheckFeatureSupport(D3D12_FEATURE_D3D12_OPTIONS5, &featureSupportData, sizeof(featureSupportData))};

	// Check that the device supports any kinds of hardware ray tracing.
	return SUCCEEDED(checkResult) && featureSupportData.RaytracingTier != D3D12_RAYTRACING_TIER_NOT_SUPPORTED;
#else
	UNUSED(device);

	return false;
#endif
}

void RayTracingProvider::initializeRayTracing()
{
#ifdef ENGINE_RAY_TRACING_ENABLED
	switch (mRayTracingApi)
	{
		case RayTracingApi::Fallback:
		case RayTracingApi::FallbackForceCompute:
		{
			const auto createDeviceFlags{mRayTracingApi == RayTracingApi::FallbackForceCompute
											 ? CreateRaytracingFallbackDeviceFlags::ForceComputeFallback
											 : CreateRaytracingFallbackDeviceFlags::None};

			mRayTracingDevice = res::d3d12::D3D12RayTracingDevice::create(mRenderer.device(), createDeviceFlags);

			log<Info>() << "Initialized fallback ray tracing!" << std::endl;

			break;
		}
		case RayTracingApi::Hardware:
		{
			mRayTracingDevice = res::d3d12::D3D12RayTracingDevice::create(mRenderer.device());

			log<Info>() << "Initialized hardware ray tracing!" << std::endl;

			break;
		}
		case RayTracingApi::None:
		default:
		{
			log<Warning>() << "Unable to initialize ray tracing: No ray tracing API has been selected!" << std::endl;
			break;
		}
	}
#else
	log<Error>() << "Unable to initialize ray tracing: Engine not compiled with ray tracing support!" << std::endl;
#endif
}

} // namespace rndr

} // namespace quark
