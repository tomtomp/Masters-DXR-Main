/**
 * @file renderer/RenderPipeline.cpp
 * @author Tomas Polasek
 * @brief Abstraction of the whole rendering pipeline, which 
 * may include multiple passes of different type - Raster, 
 * RayTrace or Compute.
 */

#include "stdafx.h"

#include "engine/renderer/RenderPipeline.h"

namespace quark
{

namespace rndr
{

RenderPipeline::PtrT RenderPipeline::create(util::PtrT<RenderSubSystem> renderer)
{ return PtrT(new RenderPipeline(renderer)); }

RenderPipeline::~RenderPipeline()
{ clearPasses(); }

void RenderPipeline::setScene(RenderScene::PtrT scene)
{
    if (mCurrentScene == scene)
    { return; }

    mCurrentScene = scene;
    mCurrentSceneDirty = true;
}

uint32_t RenderPipeline::nextPassIndex() const
{ return static_cast<uint32_t>(mRenderPasses.size()); }

void RenderPipeline::prepare()
{
    if (mRenderPassesDirty)
    { // We may need to initialize new passes!
        initializePasses(); 
        mRenderPassesDirty = false;
    }

    if (mCurrentSceneDirty)
    { // We should update scene used by the passes.
        updatePassScene();
        mCurrentSceneDirty = false;
    }
}

void RenderPipeline::execute()
{
    // Prepare for execution by updating pass configuration.
    prepare();

    // Create a rendering context for this execution.
    auto ctx{ createRenderContext() };

    for (auto &renderPass : mRenderPasses)
    { // Go through all passes and execute them in correct order.
        if (renderPass)
        { renderPass->executeWrapper(ctx); }
    }
}

void RenderPipeline::setRenderPass(uint32_t index, RenderPass::PtrT passPtr)
{
    if (mRenderPasses.size() <= index)
    { mRenderPasses.resize(index + 1u); }

    mRenderPasses[index] = passPtr;
    mRenderPassesDirty = true;
}

void RenderPipeline::initializePasses()
{
    for (auto &renderPass : mRenderPasses)
    { // Initialize all uninitilized passes.
        if (renderPass && !renderPass->initialized())
        { renderPass->initializeWrapper(mRenderContext, mResourceMgr); }
    }
}

void RenderPipeline::updatePassScene()
{
    if (!mCurrentSceneDirty)
    { return; }

    for (auto &renderPass : mRenderPasses)
    {
        if (renderPass)
        { renderPass->sceneChanged(mCurrentScene); }
    }
}

void RenderPipeline::clearPasses()
{
    for (auto &renderPass : mRenderPasses)
    {
        if (renderPass && renderPass->initialized())
        { renderPass->deinitializeWrapper(); }
    }

    mRenderPasses.clear();
}

RenderContext::PtrT RenderPipeline::createRenderContext()
{
    auto result{ RenderContext::create(mRenderer) };

    // TODO - Fill information.

    return result;
}

RenderPipeline::RenderPipeline(util::PtrT<RenderSubSystem> renderer) :
    mRenderer{ renderer }, 
    mRenderContext{ createRenderContext() }
{ }

}

}
