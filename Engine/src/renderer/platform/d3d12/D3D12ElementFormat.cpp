/**
 * @file renderer/platform/d3d12/D3D12ElementFormat.cpp
 * @author Tomas Polasek
 * @brief Conversion of element format into D3D12 DXGI_FORMAT.
 */

#include "stdafx.h"

#include "engine/renderer/platform/d3d12/D3D12ElementFormat.h"

namespace quark
{

namespace rndr
{

namespace platform
{ 

namespace d3d12
{

::DXGI_FORMAT elementFormatToD3D12(ElementFormat elementFormat)
{
    switch (elementFormat.format)
    {
        case ComponentFormat::SignedByte:
			switch (elementFormat.numComponents)
			{
				case 1u:
					return ::DXGI_FORMAT_R8_SINT;
				case 2u:
					return ::DXGI_FORMAT_R8G8_SINT;
				case 3u:
					return ::DXGI_FORMAT_UNKNOWN;
				case 4u:
					return ::DXGI_FORMAT_R8G8B8A8_SINT;
				default:
					return ::DXGI_FORMAT_UNKNOWN;
			}
        case ComponentFormat::UnsignedByte:
			switch (elementFormat.numComponents)
			{
				case 1u:
					return ::DXGI_FORMAT_R8_UINT;
				case 2u:
					return ::DXGI_FORMAT_R8G8_UINT;
				case 3u:
					return ::DXGI_FORMAT_UNKNOWN;
				case 4u:
					return ::DXGI_FORMAT_R8G8B8A8_UINT;
				default:
					return ::DXGI_FORMAT_UNKNOWN;
			}
        case ComponentFormat::SignedNormByte:
			switch (elementFormat.numComponents)
			{
				case 1u:
					return ::DXGI_FORMAT_R8_SNORM;
				case 2u:
					return ::DXGI_FORMAT_R8G8_SNORM;
				case 3u:
					return ::DXGI_FORMAT_UNKNOWN;
				case 4u:
					return ::DXGI_FORMAT_R8G8B8A8_SNORM;
				default:
					return ::DXGI_FORMAT_UNKNOWN;
			}
        case ComponentFormat::UnsignedNormByte:
			switch (elementFormat.numComponents)
			{
				case 1u:
					return ::DXGI_FORMAT_R8_UNORM;
				case 2u:
					return ::DXGI_FORMAT_R8G8_UNORM;
				case 3u:
					return ::DXGI_FORMAT_UNKNOWN;
				case 4u:
					return ::DXGI_FORMAT_R8G8B8A8_UNORM;
				default:
					return ::DXGI_FORMAT_UNKNOWN;
			}
        case ComponentFormat::SignedShort:
			switch (elementFormat.numComponents)
			{
				case 1u:
					return ::DXGI_FORMAT_R16_SINT;
				case 2u:
					return ::DXGI_FORMAT_R16G16_SINT;
				case 3u:
					return ::DXGI_FORMAT_UNKNOWN;
				case 4u:
					return ::DXGI_FORMAT_R16G16B16A16_SINT;
				default:
					return ::DXGI_FORMAT_UNKNOWN;
			}
        case ComponentFormat::UnsignedShort:
			switch (elementFormat.numComponents)
			{
				case 1u:
					return ::DXGI_FORMAT_R16_UINT;
				case 2u:
					return ::DXGI_FORMAT_R16G16_UINT;
				case 3u:
					return ::DXGI_FORMAT_UNKNOWN;
				case 4u:
					return ::DXGI_FORMAT_R16G16B16A16_UINT;
				default:
					return ::DXGI_FORMAT_UNKNOWN;
			}
        case ComponentFormat::SignedNormShort:
			switch (elementFormat.numComponents)
			{
				case 1u:
					return ::DXGI_FORMAT_R16_SNORM;
				case 2u:
					return ::DXGI_FORMAT_R16G16_SNORM;
				case 3u:
					return ::DXGI_FORMAT_UNKNOWN;
				case 4u:
					return ::DXGI_FORMAT_R16G16B16A16_SNORM;
				default:
					return ::DXGI_FORMAT_UNKNOWN;
			}
        case ComponentFormat::UnsignedNormShort:
			switch (elementFormat.numComponents)
			{
				case 1u:
					return ::DXGI_FORMAT_R16_UNORM;
				case 2u:
					return ::DXGI_FORMAT_R16G16_UNORM;
				case 3u:
					return ::DXGI_FORMAT_UNKNOWN;
				case 4u:
					return ::DXGI_FORMAT_R16G16B16A16_UNORM;
				default:
					return ::DXGI_FORMAT_UNKNOWN;
			}
        case ComponentFormat::Float16:
			switch (elementFormat.numComponents)
			{
				case 1u:
					return ::DXGI_FORMAT_R16_FLOAT;
				case 2u:
					return ::DXGI_FORMAT_R16G16_FLOAT;
				case 3u:
					return ::DXGI_FORMAT_UNKNOWN;
				case 4u:
					return ::DXGI_FORMAT_R16G16B16A16_FLOAT;
				default:
					return ::DXGI_FORMAT_UNKNOWN;
			}
        case ComponentFormat::Depth16:
			switch (elementFormat.numComponents)
			{
				case 1u:
					return ::DXGI_FORMAT_D16_UNORM;
				default:
					return ::DXGI_FORMAT_UNKNOWN;
			}
        case ComponentFormat::SignedInt:
			switch (elementFormat.numComponents)
			{
				case 1u:
					return ::DXGI_FORMAT_R32_SINT;
				case 2u:
					return ::DXGI_FORMAT_R32G32_SINT;
				case 3u:
					return ::DXGI_FORMAT_UNKNOWN;
				case 4u:
					return ::DXGI_FORMAT_R32G32B32A32_SINT;
				default:
					return ::DXGI_FORMAT_UNKNOWN;
			}
        case ComponentFormat::UnsignedInt:
			switch (elementFormat.numComponents)
			{
				case 1u:
					return ::DXGI_FORMAT_R32_UINT;
				case 2u:
					return ::DXGI_FORMAT_R32G32_UINT;
				case 3u:
					return ::DXGI_FORMAT_UNKNOWN;
				case 4u:
					return ::DXGI_FORMAT_R32G32B32A32_UINT;
				default:
					return ::DXGI_FORMAT_UNKNOWN;
			}
        case ComponentFormat::Float32:
			switch (elementFormat.numComponents)
			{
				case 1u:
					return ::DXGI_FORMAT_R32_FLOAT;
				case 2u:
					return ::DXGI_FORMAT_R32G32_FLOAT;
				case 3u:
					return ::DXGI_FORMAT_R32G32B32_FLOAT;
				case 4u:
					return ::DXGI_FORMAT_R32G32B32A32_FLOAT;
				default:
					return ::DXGI_FORMAT_UNKNOWN;
			}
        case ComponentFormat::Depth32:
			switch (elementFormat.numComponents)
			{
				case 1u:
					return ::DXGI_FORMAT_D32_FLOAT;
				default:
					return ::DXGI_FORMAT_UNKNOWN;
			}
        case ComponentFormat::CompressedBC1U:
			switch (elementFormat.numComponents)
			{
				case 4u:
					return ::DXGI_FORMAT_BC1_UNORM;
				default:
					return ::DXGI_FORMAT_UNKNOWN;
			}
        case ComponentFormat::CompressedBC1S:
			switch (elementFormat.numComponents)
			{
				case 4u:
					return ::DXGI_FORMAT_BC1_UNORM_SRGB;
				default:
					return ::DXGI_FORMAT_UNKNOWN;
			}
        case ComponentFormat::CompressedBC2U:
			switch (elementFormat.numComponents)
			{
				case 4u:
					return ::DXGI_FORMAT_BC2_UNORM;
				default:
					return ::DXGI_FORMAT_UNKNOWN;
			}
        case ComponentFormat::CompressedBC2S:
			switch (elementFormat.numComponents)
			{
				case 4u:
					return ::DXGI_FORMAT_BC2_UNORM_SRGB;
				default:
					return ::DXGI_FORMAT_UNKNOWN;
			}
        case ComponentFormat::CompressedBC3U:
			switch (elementFormat.numComponents)
			{
				case 4u:
					return ::DXGI_FORMAT_BC3_UNORM;
				default:
					return ::DXGI_FORMAT_UNKNOWN;
			}
        case ComponentFormat::CompressedBC3S:
			switch (elementFormat.numComponents)
			{
				case 4u:
					return ::DXGI_FORMAT_BC3_UNORM_SRGB;
				default:
					return ::DXGI_FORMAT_UNKNOWN;
			}
        case ComponentFormat::CompressedBC4U:
			switch (elementFormat.numComponents)
			{
				case 1u:
					return ::DXGI_FORMAT_BC4_UNORM;
				default:
					return ::DXGI_FORMAT_UNKNOWN;
			}
        case ComponentFormat::CompressedBC4S:
			switch (elementFormat.numComponents)
			{
				case 1u:
					return ::DXGI_FORMAT_BC4_SNORM;
				default:
					return ::DXGI_FORMAT_UNKNOWN;
			}
        case ComponentFormat::CompressedBC5U:
			switch (elementFormat.numComponents)
			{
				case 2u:
					return ::DXGI_FORMAT_BC5_UNORM;
				default:
					return ::DXGI_FORMAT_UNKNOWN;
			}
        case ComponentFormat::CompressedBC5S:
			switch (elementFormat.numComponents)
			{
				case 2u:
					return ::DXGI_FORMAT_BC5_SNORM;
				default:
					return ::DXGI_FORMAT_UNKNOWN;
			}
        case ComponentFormat::CompressedBC6U:
			switch (elementFormat.numComponents)
			{
				case 3u:
					return ::DXGI_FORMAT_BC6H_UF16;
				default:
					return ::DXGI_FORMAT_UNKNOWN;
			}
        case ComponentFormat::CompressedBC6S:
			switch (elementFormat.numComponents)
			{
				case 3u:
					return ::DXGI_FORMAT_BC6H_SF16;
				default:
					return ::DXGI_FORMAT_UNKNOWN;
			}
        case ComponentFormat::CompressedBC7U:
			switch (elementFormat.numComponents)
			{
				case 4u:
					return ::DXGI_FORMAT_BC7_UNORM;
				default:
					return ::DXGI_FORMAT_UNKNOWN;
			}
        case ComponentFormat::CompressedBC7S:
			switch (elementFormat.numComponents)
			{
				case 4u:
					return ::DXGI_FORMAT_BC7_UNORM_SRGB;
				default:
					return ::DXGI_FORMAT_UNKNOWN;
			}
        default:
        { break; }
    }

    return ::DXGI_FORMAT_UNKNOWN;
}

}

}

}

}
