/**
 * @file renderer/platform/d3d12/D3D12SamplerFiltering.cpp
 * @author Tomas Polasek
 * @brief Conversion of sampler filtering options into D3D12 format.
 */

#include "stdafx.h"

#include "engine/renderer/platform/d3d12/D3D12SamplerFiltering.h"

namespace quark
{

namespace rndr
{

namespace platform
{ 

namespace d3d12
{

::D3D12_FILTER sampleFilterToD3D12(SampleFilter minFilter, SampleFilter magFilter, SampleFilter mipFilter)
{
	return D3D12_ENCODE_BASIC_FILTER(
        (minFilter == SampleFilter::Nearest ? ::D3D12_FILTER_TYPE_POINT : ::D3D12_FILTER_TYPE_LINEAR),
        (magFilter == SampleFilter::Nearest ? ::D3D12_FILTER_TYPE_POINT : ::D3D12_FILTER_TYPE_LINEAR),
        (mipFilter == SampleFilter::Nearest ? ::D3D12_FILTER_TYPE_POINT : ::D3D12_FILTER_TYPE_LINEAR),
        ::D3D12_FILTER_REDUCTION_TYPE_STANDARD);
}

::D3D12_FILTER sampleFilterToD3D12(MinMagMipFilter edgeFiltering)
{ return sampleFilterToD3D12(edgeFiltering.minFilter, edgeFiltering.magFilter, edgeFiltering.mipFilter); }

::D3D12_TEXTURE_ADDRESS_MODE sampleEdgeToD3D12(SampleEdge edgeFilter)
{
    switch (edgeFilter)
    {
        case SampleEdge::Clamped:
        { return ::D3D12_TEXTURE_ADDRESS_MODE_CLAMP; }
        case SampleEdge::Mirrored:
        { return ::D3D12_TEXTURE_ADDRESS_MODE_MIRROR; }
        case SampleEdge::Repeat:
        { return ::D3D12_TEXTURE_ADDRESS_MODE_WRAP; }
        default:
        { return ::D3D12_TEXTURE_ADDRESS_MODE_WRAP; }
    }
}

}

}

}

}
