/**
 * @file renderer/deferredRL/DeferredRL.cpp
 * @author Tomas Polasek
 * @brief Rendering layer allowing the creation of 
 * G-Buffer and its use in deferred shading/rendering.
 */

#include "stdafx.h"

#include "engine/renderer/deferredRL/deferredRL.h"

#include "engine/renderer/deferredRL/shaders/DeferredVS.h"
#include "engine/renderer/deferredRL/shaders/DeferredPS.h"

#include "engine/helpers/d3d12/D3D12BatchTransitionBarrier.h"

#include "engine/gui/GuiSubSystem.h"

namespace quark
{

namespace rndr
{

void DeferredBuffers::transitionResourcesTo(
    res::d3d12::D3D12CommandList &cmdList, ::D3D12_RESOURCE_STATES state)
{
    /*
    if (worldPositionTexture)
    { worldPositionTexture->transitionTo(state, cmdList); }
    if (worldNormalTexture)
    { worldNormalTexture->transitionTo(state, cmdList); }
    if (materialDiffuseTexture)
    { materialDiffuseTexture->transitionTo(state, cmdList); }
    if (metallicRoughnessTexture)
    { metallicRoughnessTexture->transitionTo(state, cmdList); }
    if (depthTexture)
    { depthTexture->transitionTo(state, cmdList); }
    */

    helpers::d3d12::D3D12BatchTransitionBarrier::transitionTo(state, cmdList,
        {
            worldPositionTexture,
            worldNormalTexture,
            materialDiffuseTexture,
            metallicRoughnessTexture,
            depthTexture
        });
}

void DeferredBufferViews::createViews(const DeferredBuffers &buffers, 
    res::d3d12::D3D12DescAllocatorInterface &descHeap, ResourceViewType type)
{
    if (!worldPositionView)
    { worldPositionView = helpers::d3d12::D3D12ResourceView::create(); }
    if (buffers.worldPositionTexture)
    { worldPositionView->reallocateDescriptor(buffers.worldPositionTexture, descHeap, type); }

    if (!worldNormalView)
    { worldNormalView = helpers::d3d12::D3D12ResourceView::create(); }
    if (buffers.worldNormalTexture)
    { worldNormalView->reallocateDescriptor(buffers.worldNormalTexture, descHeap, type); }

    if (!materialDiffuseView)
    { materialDiffuseView = helpers::d3d12::D3D12ResourceView::create(); }
    if (buffers.materialDiffuseTexture)
    { materialDiffuseView->reallocateDescriptor(buffers.materialDiffuseTexture, descHeap, type); }

    if (!metallicRoughnessView)
    { metallicRoughnessView = helpers::d3d12::D3D12ResourceView::create(); }
    if (buffers.metallicRoughnessTexture)
    { metallicRoughnessView->reallocateDescriptor(buffers.metallicRoughnessTexture, descHeap, type); }

    if (!depthView)
    { depthView = helpers::d3d12::D3D12ResourceView::create(); }
    if (buffers.depthTexture)
    { depthView->reallocateDescriptor(buffers.depthTexture, descHeap, type); }
}

DeferredRL::DeferredRL(RenderSubSystem &renderer) :
    mRenderer{ &renderer }
{ initializeResources(); }

DeferredRL::PtrT DeferredRL::create(RenderSubSystem &renderer)
{ return PtrT{ new DeferredRL(renderer) }; }

DeferredRL::~DeferredRL()
{ /* Automatic */ }

void DeferredRL::setDrawablesList(const res::scene::DrawableListHandle &drawables)
{
    mDrawables = drawables;
    mLastPreparedVersion = { };
}

void DeferredRL::resize(uint32_t width, uint32_t height)
{
    if (!mPrepared)
    { return; }

    mR.scissorRect = ::CD3DX12_RECT(
        0u, 0u, 
        width, height);
    mR.viewport = ::CD3DX12_VIEWPORT{ 
        0.0f, 0.0f, 
        static_cast<float>(width), static_cast<float>(height) };

    // Create or resize already existing G-Buffer textures and depth buffer.
    createResizeTextures(width, height);

    // Create texture descriptors for the G-Buffer textures.
    createTextureDescriptors();
}

void DeferredRL::prepare(bool sceneChanged)
{
    PROF_SCOPE("DeferredRLPrep");

    auto &drawables{ mDrawables.get() };
    if (drawables.revision() != mLastPreparedVersion)
    { // Current version of the list is not prepared!
        prepareForRendering(drawables, sceneChanged);
        mLastPreparedVersion = drawables.revision();
    }

    mPrepared = true;
}

void DeferredRL::prepareGui(gui::GuiSubSystem &gui)
{
    gui.addGuiContext("/DeferredRL/")
        .setType(gui::GuiContextType::Category)
        .setLabel("Deferred Pass");

    gui.addGuiVariable<gui::InputBool>("DeferredRlDiscardTransparent", "/DeferredRL/", { })
        .setLabel("Discard Transparent");

    gui.addGuiVariable<gui::Button>("DeferredRlReloadShaders", "/DeferredRL/", { [&] ()
    { mR.reloadShaders = true; } }).setLabel("Reload Shaders");
}

void DeferredRL::render(const helpers::math::Camera &camera, 
    res::d3d12::D3D12CommandList &cmdList)
{
    // Skip if we didn't initialize the size.
    if (!mPrepared)
    { return; }

    GPU_PROF_SCOPE(cmdList, "DeferredRLGpu");

    // Check if shader reloading is required.
    checkReloadShaders();

    // Update the constant buffers with new camera.
    updateCamera(camera);
    // Update other constants.
    updateConstants();
    // Re-upload constant buffers.
    refreshConstantBuffers();

    // Clear the depth-stencil buffer.
    mR.depthStencilTexture->clear(cmdList);

    // Prepare shared GPU state:
    cmdList->RSSetViewports(1u, &mR.viewport);
    cmdList->RSSetScissorRects(1u, &mR.scissorRect);

    cmdList->SetGraphicsRootSignature(mR.rootSig->get());

    // Return the buffers into the original state.
    mR.deferredBuffers.transitionResourcesTo(cmdList, ::D3D12_RESOURCE_STATE_RENDER_TARGET);

    // Clear the G-Buffers to default values.
    clearDeferredBuffers(cmdList);

    // Bind all of the G-Buffer render-targets.
    const ::D3D12_CPU_DESCRIPTOR_HANDLE rtvs[] {
        mR.deferredBuffers.worldPositionTexture->cpuHandle(), 
        mR.deferredBuffers.worldNormalTexture->cpuHandle(), 
        mR.deferredBuffers.materialDiffuseTexture->cpuHandle(), 
        mR.deferredBuffers.metallicRoughnessTexture->cpuHandle(), 
        mR.deferredBuffers.depthTexture->cpuHandle()
    };
    const auto dsv{ mR.depthStencilTexture->dsv() };
    cmdList->OMSetRenderTargets(util::count<::UINT>(rtvs), rtvs, false, &dsv);

    // Use the prepared descriptor heaps.
    setDescHeaps(cmdList);

    // Fill descriptors into the root signature.
    setRootSignatureParameters(cmdList);

    // Execute the current drawing bundle.
    cmdList->ExecuteBundle(mR.renderBundle->get());
}

void DeferredRL::updateGui(gui::GuiSubSystem &gui)
{ 
    auto &discardTransparent{ gui.getVariable<gui::InputBool>("DeferredRlDiscardTransparent") };
    mR.staticConstantsChanged |= gui::synchronizeVariables(discardTransparent, mR.staticConstants.discardTransparent);
}

DeferredBuffers DeferredRL::getDeferredBuffers() const
{ return mR.deferredBuffers; }

void DeferredRL::setDiscardTransparent(bool enabled)
{
    mR.staticConstants.discardTransparent = enabled;
    mR.staticConstantsChanged = true;
}

void DeferredRL::createConstantBuffers()
{
    mR.staticConstantsBuffer = StaticConstantsBuffer::create();
    mR.staticConstants.mode = 0u;
    mR.staticConstants.discardTransparent = DEFAULT_DISCARD_TRANSPARENT;
    mR.staticConstantsBuffer->at(0u) = mR.staticConstants;
    mR.staticConstantsBuffer->reallocate(mRenderer->device(), L"StaticConstantsBuffer");

    mR.dynamicConstantsBuffer = DynamicConstantsBuffer::create();
    mR.dynamicConstantsBuffer->at(0u) = mR.dynamicConstants;
    mR.dynamicConstantsBuffer->reallocate(mRenderer->device(), L"DynamicConstantsBuffer");
}

void DeferredRL::initializeResources(uint32_t requiredTextures)
{
    PROF_SCOPE("DeferredRLInitialization");

    // Create root signatures, input layout and build the pipeline.
    createPipeline(requiredTextures);

    // Create temporary depth-stencil buffer, until we create the real one.
    mR.depthStencilTexture = res::rndr::DepthStencilBuffer::create(1u, 1u, mRenderer->device());

    // Create static and dynamic constant buffers.
    createConstantBuffers();

    // Create dummy textures until we resize.
    createResizeTextures(1u, 1u);

    // Create a material cache.
    if (!mR.materialMeshCache)
    { mR.materialMeshCache = helpers::rndr::MaterialMeshCache::create(); }
}

void DeferredRL::createPipeline(uint32_t requiredTextures)
{
    // No scissoring.
    mR.scissorRect = ::CD3DX12_RECT(0, 0, LONG_MAX, LONG_MAX);

    // Prepare root signature.
    quark::helpers::d3d12::D3D12RootSignatureBuilder rsb;

    // Static, slow-changing constants, register(b0, space0).
    rsb[compatDeferred::GlobalRS::StaticConstantBuffer].asConstantBufferViewParameter(
        compatDeferred::StaticConstantsSlot.reg, compatDeferred::StaticConstantsSlot.space);
    // Dynamic, fast-changing constants, register(b1, space0).
    rsb[compatDeferred::GlobalRS::DynamicConstantBuffer].asConstantBufferViewParameter(
        compatDeferred::DynamicConstantsSlot.reg, compatDeferred::DynamicConstantsSlot.space);
    // Per-instance constants, straight in root signature, register(b2, space0).
    rsb[compatDeferred::GlobalRS::InstanceConstantBuffer].asConstantParameter(
        static_cast<::UINT>(util::sizeInUint32<compatDeferred::InstanceConstantBuffer>()), 
        compatDeferred::InstanceConstantsSlot.reg, compatDeferred::InstanceConstantsSlot.space);
    // Array of material textures.
    const ::CD3DX12_DESCRIPTOR_RANGE1 textureRange[]{
        { // Texture array SRVs: 
            ::D3D12_DESCRIPTOR_RANGE_TYPE_SRV, 
            // Bounded number of descriptors
            std::max<uint32_t>(requiredTextures, HLSL_TEXTURE_UPPER_BOUND), 
            // Starting at register(t0, space0).
            compatDeferred::TextureArraySlot.reg, compatDeferred::TextureArraySlot.space, 
            ::D3D12_DESCRIPTOR_RANGE_FLAG_DATA_STATIC
        }
    };
    rsb[compatDeferred::GlobalRS::TextureArray].asDescriptorTableParameter(
        util::count<::UINT>(textureRange), textureRange);

    // Common sampler used for sampling material textures, register(s0, space0).
    const ::CD3DX12_STATIC_SAMPLER_DESC commonSampler{ 
        compatDeferred::SamplerSlot.reg, 
        ::D3D12_FILTER_ANISOTROPIC, 
        ::D3D12_TEXTURE_ADDRESS_MODE_WRAP, 
        ::D3D12_TEXTURE_ADDRESS_MODE_WRAP, 
        ::D3D12_TEXTURE_ADDRESS_MODE_WRAP, 
        0.0f, 16u,
        ::D3D12_COMPARISON_FUNC_LESS_EQUAL,
        ::D3D12_STATIC_BORDER_COLOR_OPAQUE_WHITE,
        0.0f, D3D12_FLOAT32_MAX,
        ::D3D12_SHADER_VISIBILITY_ALL,
        compatDeferred::SamplerSlot.space
    };
    rsb.addStaticSampler(commonSampler);

    // Enable stages which are used in deferred pipeline.
    rsb.setFlags(
        D3D12_ROOT_SIGNATURE_FLAG_ALLOW_INPUT_ASSEMBLER_INPUT_LAYOUT |
        D3D12_ROOT_SIGNATURE_FLAG_DENY_HULL_SHADER_ROOT_ACCESS |
        D3D12_ROOT_SIGNATURE_FLAG_DENY_DOMAIN_SHADER_ROOT_ACCESS |
        D3D12_ROOT_SIGNATURE_FLAG_DENY_GEOMETRY_SHADER_ROOT_ACCESS);

    mR.rootSig = rsb.build(mRenderer->device());

    mR.inputLayout.clear();

    // Vertex position.
    mR.inputLayout.addElement(
        "POSITION", 0u, 
        DEFAULT_VERTEX_POSITION_FORMAT, compatDeferred::InputLayout::Position, 
        D3D12_APPEND_ALIGNED_ELEMENT, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0u);
    // Vertex normal.
    mR.inputLayout.addElement(
        "NORMAL", 0u, 
        DEFAULT_VERTEX_NORMAL_FORMAT, compatDeferred::InputLayout::Normal, 
        D3D12_APPEND_ALIGNED_ELEMENT, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0u);
    // Vertex tangent.
    mR.inputLayout.addElement(
        "TANGENT", 0u, 
        DEFAULT_VERTEX_TANGENT_FORMAT, compatDeferred::InputLayout::Tangent, 
        D3D12_APPEND_ALIGNED_ELEMENT, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0u);
    // Vertex texture coordinate.
    mR.inputLayout.addElement(
        "TEXCOORD", 0u, 
        DEFAULT_VERTEX_TEXCOORD_FORMAT,  compatDeferred::InputLayout::TexCoord, 
        D3D12_APPEND_ALIGNED_ELEMENT, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0u);

    // Load deferred shaders.
    mR.vShader = res::d3d12::D3D12Shader::create(gDeferredVs, sizeof(gDeferredVs));
    mR.pShader = res::d3d12::D3D12Shader::create(gDeferredPs, sizeof(gDeferredPs));

    // Build the pipeline state.
    buildPipeline();
}

void DeferredRL::buildPipeline()
{
    quark::helpers::d3d12::D3D12PipelineStateSOBuilder pssob;
    quark::helpers::d3d12::D3D12PipelineStateBuilder psb;
    psb << 
        mR.rootSig->rsState() <<
        mR.inputLayout.ilState() <<
        pssob.rasterizer(::D3D12_CULL_MODE_BACK, 
            !mRenderer->cfg().renderRasterizerFrontClockwise, 
            mRenderer->cfg().renderRasterizerFill) << 
        pssob.primitiveTopology(DEFAULT_PRIMITIVE_TOPOLOGY_TYPE) << 
        mR.vShader->vsState() << 
        mR.pShader->psState() << 
        pssob.depthStencilFormat(::DXGI_FORMAT_D32_FLOAT) << 
        pssob.renderTargetFormats({ 
            DEFAULT_WORLD_POSITION_RT_FORMAT,
            DEFAULT_WORLD_NORMAL_RT_FORMAT,
            DEFAULT_MATERIAL_DIFFUSE_RT_FORMAT,
            DEFAULT_MATERIAL_PROPS_RT_FORMAT,
            DEFAULT_DEPTH_RT_FORMAT
        });
    mR.pipelineState = psb.build(mRenderer->device());
}

void DeferredRL::prepareMaterials(helpers::rndr::MaterialMeshCache &materialMeshCache, 
    res::scene::DrawableList &drawables)
{
    PROF_SCOPE("PrepareMaterials");

    for (const auto &drawable : drawables)
    { // Process all drawables.
        materialMeshCache.registerMesh(drawable.meshHandle);
    }
}

res::d3d12::D3D12CommandList::PtrT DeferredRL::createDrawBundle(
    RenderSubSystem &renderer)
{
    // Create the reusable command bundle: 
    return renderer.bundleCmdQueue().getCommandList();
}

uint32_t DeferredRL::calculateMiscDescriptorCount()
{
    uint32_t result{ 0u };

    // Static constant buffer CBV: 
    result += 1u;
    // Dynamic constant buffer CBV: 
    result += 1u;

    return result;
}

uint32_t DeferredRL::calculateTextureDescriptorCount(helpers::rndr::MaterialMeshCache &cache)
{
    /*
	UNUSED(cache);
    ASSERT_FAST(cache.calculateSizeRequirements().textureDescriptors <= HLSL_TEXTURE_UPPER_BOUND);
    return HLSL_TEXTURE_UPPER_BOUND;
    */
    return std::max<uint32_t>(cache.calculateSizeRequirements().textureDescriptors, HLSL_TEXTURE_UPPER_BOUND);
}

res::d3d12::D3D12DescHeap::PtrT DeferredRL::createDescHeap(std::size_t requiredDescriptors)
{
    return res::d3d12::D3D12DescHeap::create(mRenderer->device(), 
        ::D3D12_DESCRIPTOR_HEAP_TYPE_CBV_SRV_UAV,
        static_cast<::uint32_t>(requiredDescriptors),
        ::D3D12_DESCRIPTOR_HEAP_FLAG_SHADER_VISIBLE);
}

helpers::d3d12::D3D12DescTable DeferredRL::createMiscTable(
    res::d3d12::D3D12DescHeap &descHeap, uint32_t numDescriptors)
{
    return helpers::d3d12::D3D12DescTable(
        descHeap, descHeap.allocateTable(numDescriptors));
}

helpers::d3d12::D3D12DescTable DeferredRL::createTextureTable(
    res::d3d12::D3D12DescHeap &descHeap, uint32_t numDescriptors)
{
    // Create the table and initialize null handles, so we can debug.
    helpers::d3d12::D3D12DescTable result(
        descHeap, descHeap.allocateTable(numDescriptors));
    result.initializeSrvNullHandles(::D3D12_SRV_DIMENSION_TEXTURE2D);

    return result;
}

void DeferredRL::createDescHeapLayout(res::d3d12::D3D12DescHeap::PtrT &descHeap, 
    helpers::d3d12::D3D12DescTable &textureTable, helpers::d3d12::D3D12DescTable &miscTable, 
    helpers::rndr::MaterialMeshCache &cache)
{
    // Calculate descriptor requirements.
    const auto miscDescriptors{ calculateMiscDescriptorCount() };
    const auto textureDescriptors{ calculateTextureDescriptorCount(cache) };
    const auto totalDescriptors{ miscDescriptors + textureDescriptors };

    // Check whether we need to create a new descriptor heap.
    bool newDescHeapRequired{ false };
    newDescHeapRequired |= miscDescriptors != miscTable.size();
    newDescHeapRequired |= textureDescriptors != textureTable.size();

    if (!newDescHeapRequired)
    { // No changed -> we don't need a new descriptor heap.
        return;
    }

    // Create the main descriptor heap, which will be used for all descriptors.
    auto newDescHeap{ createDescHeap(totalDescriptors) };

    // Create descriptor table for miscellaneous descriptors.
    auto newMiscTable{ createMiscTable(*newDescHeap, miscDescriptors) };
    // Create descriptor table for texture SRV descriptors.
    auto newTextureTable{ createTextureTable(*newDescHeap, textureDescriptors) };

    // Copy over the original descriptor tables: 
    // Misc table does not need to be copied, since we use reallocateDescriptor().
    //textureTable.copyFrom(textureTable);

    // Change over to the new descriptor heap.
    descHeap = newDescHeap;
    miscTable = std::move(newMiscTable);
    textureTable = std::move(newTextureTable);
}

void DeferredRL::fillMiscTable(helpers::d3d12::D3D12DescTable &miscTable)
{
    mR.staticConstantsBuffer->reallocateDescriptor(miscTable, ResourceViewType::ConstantBuffer);
    mR.dynamicConstantsBuffer->reallocateDescriptor(miscTable, ResourceViewType::ConstantBuffer);
}

void DeferredRL::buildMaterialMeshCache(helpers::rndr::MaterialMeshCache &cache,
    helpers::d3d12::D3D12DescTable &textureTable)
{
    PROF_SCOPE("BuildMaterialCache");

    cache.buildTextureCache(textureTable);
}

uint32_t DeferredRL::calculateRtvDescriptorCount()
{
    uint32_t result{ 0u };

    // World Position G-Buffer: 
    result += 1u;
    // World Normal G-Buffer: 
    result += 1u;
    // Material Diffuse G-Buffer: 
    result += 1u;
    // Metallic Roughness G-Buffer: 
    result += 1u;
    // Depth G-Buffer: 
    result += 1u;

    return result;
}

res::d3d12::D3D12DescHeap::PtrT DeferredRL::createRtvDescHeap(uint32_t requiredDescriptors)
{
    return res::d3d12::D3D12DescHeap::create(mRenderer->device(), 
        ::D3D12_DESCRIPTOR_HEAP_TYPE_RTV,
        static_cast<::uint32_t>(requiredDescriptors),
        ::D3D12_DESCRIPTOR_HEAP_FLAG_NONE);
}

helpers::d3d12::D3D12DescTable DeferredRL::createRtvTable(
    res::d3d12::D3D12DescHeap &rtvDescHeap, uint32_t numDescriptors)
{
    return helpers::d3d12::D3D12DescTable(
        rtvDescHeap, rtvDescHeap.allocateTable(numDescriptors));
}

void DeferredRL::createRtvDescHeapAndLayout(res::d3d12::D3D12DescHeap::PtrT &rtvDescHeap,
    helpers::d3d12::D3D12DescTable &rtvTable)
{
    // Calculate descriptor requirements.
    const auto rtvDescriptors{ calculateRtvDescriptorCount() };

    // Check whether we need to create a new descriptor heap.
    bool newDescHeapRequired{ false };
    newDescHeapRequired |= rtvDescriptors != rtvTable.size();

    if (!newDescHeapRequired)
    { // No changed -> we don't need a new descriptor heap.
        return;
    }

    // Create the main descriptor heap, which will be used for all descriptors.
    auto newRtvDescHeap{ createRtvDescHeap(rtvDescriptors) };

    // Create descriptor table for miscellaneous descriptors.
    auto newRtvTable{ createRtvTable(*newRtvDescHeap, rtvDescriptors) };

    // Change over to the new descriptor heap.
    rtvDescHeap = newRtvDescHeap;
    rtvTable = std::move(newRtvTable);
}

void DeferredRL::fillRtvTable(helpers::d3d12::D3D12DescTable &rtvTable)
{
    mR.deferredBuffers.worldPositionTexture->reallocateDescriptor(rtvTable, rndr::ResourceViewType::RenderTarget);
    mR.deferredBuffers.worldNormalTexture->reallocateDescriptor(rtvTable, rndr::ResourceViewType::RenderTarget);
    mR.deferredBuffers.materialDiffuseTexture->reallocateDescriptor(rtvTable, rndr::ResourceViewType::RenderTarget);
    mR.deferredBuffers.metallicRoughnessTexture->reallocateDescriptor(rtvTable, rndr::ResourceViewType::RenderTarget);
    mR.deferredBuffers.depthTexture->reallocateDescriptor(rtvTable, rndr::ResourceViewType::RenderTarget);
}

void DeferredRL::prepareDrawBundle(res::scene::DrawableList &drawables, 
    res::d3d12::D3D12CommandList &drawBundle)
{
    PROF_SCOPE("PrepareDrawBundle");

    // Common to all drawables: 
    drawBundle->SetGraphicsRootSignature(mR.rootSig->get());
    drawBundle->SetPipelineState(mR.pipelineState->get());
    drawBundle->IASetPrimitiveTopology(DEFAULT_PRIMITIVE_TOPOLOGY);

    // Use the prepared descriptor heaps.
    setDescHeaps(drawBundle);
    // Fill descriptors into the root signature.
    setRootSignatureParameters(drawBundle);

    // Prepared array for input layout vertex buffers.
    ::D3D12_VERTEX_BUFFER_VIEW vbvs[compatDeferred::InputLayout::Count]{ };

    for (const auto &drawable : drawables)
    { // Process all drawables.

        // One mesh can contain multiple primitives.
        const auto &mesh{ drawable.meshHandle.get() };

        for (std::size_t iii = 0; iii < mesh.primitives.size(); ++iii)
        { // Render the primitives.
            const auto &primitive{ mesh.primitives[iii] };

            // Access the material/mesh cache.
            const auto primitiveSpecification{ mR.materialMeshCache->specificationForPrimitive(drawable.meshHandle, iii) };

            // Indices for diffuse, normal and material textures.
            const auto diffuseIndex{ primitiveSpecification.material.colorTextureIndex };
            const auto materialIndex{ primitiveSpecification.material.metallicRoughnessTextureIndex };
            const auto normalIndex{ primitiveSpecification.material.normalTextureIndex };

            // Specify which material textures should be used.
            compatDeferred::InstanceConstantBuffer instanceData{ };
            instanceData.modelToWorld = drawable.modelMatrix.matrix();
            instanceData.normalToWorld = drawable.modelMatrix.normalMatrix();
            instanceData.diffuseIdx = diffuseIndex;
            instanceData.normalIdx = normalIndex;
            instanceData.materialIdx = materialIndex;
            instanceData.specularReflections = primitiveSpecification.material.specularReflections;
            instanceData.specularRefractions = primitiveSpecification.material.specularRefractions;

            drawBundle->SetGraphicsRoot32BitConstants(
                compatDeferred::GlobalRS::InstanceConstantBuffer, 
                static_cast<::UINT>(util::sizeInUint32<compatDeferred::InstanceConstantBuffer>()), 
                &instanceData, 0u);

            // Specify vertex data - positions and texture coordinates.
            vbvs[compatDeferred::InputLayout::Position] = primitive.getPositionBuffer()->vbv();
            vbvs[compatDeferred::InputLayout::Normal] = primitive.getNormalBuffer()->vbv();
            vbvs[compatDeferred::InputLayout::TexCoord] = primitive.getTexCoordBuffer()->vbv();

            drawBundle->IASetVertexBuffers(0u, static_cast<::UINT>(util::count(vbvs)), vbvs);

            // Specify index data.
            const auto &indexBuffer{ primitive.indices };
            const auto ibw = indexBuffer->ibv();
            drawBundle->IASetIndexBuffer(&ibw);

            // Perform the draw.
            drawBundle->DrawIndexedInstanced(
                static_cast<::UINT>(indexBuffer->numElements), 
                // Only one instance.
                1u, 0u, 0u, 0u);
        }
    }
}

void DeferredRL::prepareForRendering(
    res::scene::DrawableList &drawables, bool sceneChanged)
{
    PROF_SCOPE("PrepareForRendering");

    if (drawables.empty())
    { // Nothing to be done...
        mR.renderBundle = nullptr;
        return;
    }

    if (sceneChanged)
    { // Purge old materials.
        mR.materialMeshCache->clear();
        mR.textureTable.clear();
    }

    // Get materials into usable state.
    prepareMaterials(*mR.materialMeshCache, drawables);

    // Create descriptor heaps for SRVs, CBVs and UAVs.
    createDescHeapLayout(mR.descHeap, mR.textureTable, mR.miscTable, *mR.materialMeshCache);

    // Create descriptors in the miscellaneous descriptor table.
    fillMiscTable(mR.miscTable);

    // Finalize and create material/mesh descriptors.
    buildMaterialMeshCache(*mR.materialMeshCache, mR.textureTable);

    // Create pipeline with required number of texture slots.
    createPipeline(mR.materialMeshCache->calculateSizeRequirements().textureDescriptors);

    // Create descriptor heap for RTVs.
    createRtvDescHeapAndLayout(mR.rtvDescHeap, mR.rtvTable);

    // Fill descriptor table with current RTVs.
    fillRtvTable(mR.rtvTable);

    // Prepare drawing commands for all drawables.
    const auto drawBundle{ createDrawBundle(*mRenderer) };
    prepareDrawBundle(drawables, *drawBundle);
    drawBundle->get()->Close();
    mR.renderBundle = drawBundle;
}

void DeferredRL::createResizeTextures(uint32_t width, uint32_t height)
{
    mR.depthStencilTexture = res::rndr::DepthStencilBuffer::create(width, height, mRenderer->device());

    auto allocator{ mRenderer->memoryMgr().committedAllocator(
        ::CD3DX12_HEAP_PROPERTIES{
            ::D3D12_HEAP_TYPE_DEFAULT
        },
        ::D3D12_HEAP_FLAG_NONE) };

    // TODO - Optimized clear values?

    auto textureDesc{ ::CD3DX12_RESOURCE_DESC::Tex2D(::DXGI_FORMAT_UNKNOWN, width, height) };
    textureDesc.Flags = ::D3D12_RESOURCE_FLAG_ALLOW_RENDER_TARGET;

    ::D3D12_CLEAR_VALUE clearValue{ };
    clearValue.Color[0u] = DEFAULT_CLEAR_VALUE;
    clearValue.Color[1u] = DEFAULT_CLEAR_VALUE;
    clearValue.Color[2u] = DEFAULT_CLEAR_VALUE;
    clearValue.Color[3u] = DEFAULT_CLEAR_VALUE;

    if (!mR.deferredBuffers.worldPositionTexture)
    { mR.deferredBuffers.worldPositionTexture = helpers::d3d12::D3D12TextureBuffer::create(); }
    textureDesc.Format = DEFAULT_WORLD_POSITION_RT_FORMAT;
    clearValue.Format = DEFAULT_WORLD_POSITION_RT_FORMAT;
    mR.deferredBuffers.worldPositionTexture->allocate(allocator, textureDesc, 
        ::D3D12_RESOURCE_STATE_RENDER_TARGET, &clearValue, L"GWorldPosition");

    if (!mR.deferredBuffers.worldNormalTexture)
    { mR.deferredBuffers.worldNormalTexture = helpers::d3d12::D3D12TextureBuffer::create(); }
    textureDesc.Format = DEFAULT_WORLD_NORMAL_RT_FORMAT;
    clearValue.Format = DEFAULT_WORLD_NORMAL_RT_FORMAT;
    mR.deferredBuffers.worldNormalTexture->allocate(allocator, textureDesc, 
        ::D3D12_RESOURCE_STATE_RENDER_TARGET, &clearValue, L"GWorldNormal");

    if (!mR.deferredBuffers.materialDiffuseTexture)
    { mR.deferredBuffers.materialDiffuseTexture = helpers::d3d12::D3D12TextureBuffer::create(); }
    textureDesc.Format = DEFAULT_MATERIAL_DIFFUSE_RT_FORMAT;
    clearValue.Format = DEFAULT_MATERIAL_DIFFUSE_RT_FORMAT;
    mR.deferredBuffers.materialDiffuseTexture->allocate(allocator, textureDesc, 
        ::D3D12_RESOURCE_STATE_RENDER_TARGET, &clearValue, L"GMaterialDiffuse");

    if (!mR.deferredBuffers.metallicRoughnessTexture)
    { mR.deferredBuffers.metallicRoughnessTexture = helpers::d3d12::D3D12TextureBuffer::create(); }
    textureDesc.Format = DEFAULT_MATERIAL_PROPS_RT_FORMAT;
    clearValue.Format = DEFAULT_MATERIAL_PROPS_RT_FORMAT;
    mR.deferredBuffers.metallicRoughnessTexture->allocate(allocator, textureDesc, 
        ::D3D12_RESOURCE_STATE_RENDER_TARGET, &clearValue, L"GMaterialMR");

    if (!mR.deferredBuffers.depthTexture)
    { mR.deferredBuffers.depthTexture = helpers::d3d12::D3D12TextureBuffer::create(); }
    textureDesc.Format = DEFAULT_DEPTH_RT_FORMAT;
    clearValue.Format = DEFAULT_DEPTH_RT_FORMAT;
    clearValue.Color[0u] = DEFAULT_DEPTH_CLEAR_VALUE;
    clearValue.Color[1u] = DEFAULT_DEPTH_CLEAR_VALUE;
    clearValue.Color[2u] = DEFAULT_DEPTH_CLEAR_VALUE;
    clearValue.Color[3u] = DEFAULT_DEPTH_CLEAR_VALUE;
    mR.deferredBuffers.depthTexture->allocate(allocator, textureDesc, 
        ::D3D12_RESOURCE_STATE_RENDER_TARGET, &clearValue, L"GDepth");
}

void DeferredRL::createTextureDescriptors()
{ fillRtvTable(mR.rtvTable); }

void DeferredRL::updateCamera(const helpers::math::Camera &camera)
{
    mR.dynamicConstants.worldToCamera = camera.vpNoRecalculation();
    mR.dynamicConstants.cameraWorldPosition = camera.position();

    mR.dynamicConstantsChanged = true;
}

void DeferredRL::updateConstants()
{
    // TODO - Do something with mode...
    //mR.staticConstants.mode = 0;
}

void DeferredRL::refreshConstantBuffers()
{
    if (mR.staticConstantsChanged)
    {
        mR.staticConstantsBuffer->at(0u) = mR.staticConstants;
        mR.staticConstantsBuffer->uploadToGPU();
        mR.staticConstantsChanged = false;
    }

    if (mR.dynamicConstantsChanged)
    {
        mR.dynamicConstantsBuffer->at(0u) = mR.dynamicConstants;
        mR.dynamicConstantsBuffer->uploadToGPU();
        mR.dynamicConstantsChanged = false;
    }
}

void DeferredRL::checkReloadShaders()
{
    if (!mR.reloadShaders)
    { return; }

    log<Info>() << "Attempting to reload Deferred shaders!" << std::endl;

    // Load shaders.
    res::d3d12::D3D12Shader::PtrT vShader{ };
    res::d3d12::D3D12Shader::PtrT pShader{ };
    try
    {
        vShader = res::d3d12::D3D12Shader::create(L"DeferredVS.cso");
        pShader = res::d3d12::D3D12Shader::create(L"DeferredPS.cso");
    }
    catch (const std::exception &err)
    {
        log<Error>() << "Failed to reload Deferred shaders! : " << err.what() << std::endl;
        mR.reloadShaders = false;
        return;
    }
    catch (...)
    {
        log<Error>() << "Failed to reload Deferred shaders!" << std::endl;
        mR.reloadShaders = false;
        return;
    }

    mR.vShader = vShader;
    mR.pShader = pShader;

    // Build the pipeline state.
    buildPipeline();
    // Prepare drawing commands for all drawables.
    const auto drawBundle{ createDrawBundle(*mRenderer) };
    prepareDrawBundle(mDrawables.get(), *drawBundle);
    drawBundle->get()->Close();
    mR.renderBundle = drawBundle;

    // Done...
    mR.reloadShaders = false;
}

void DeferredRL::clearDeferredBuffers(res::d3d12::D3D12CommandList &cmdList)
{
    const float clearColor[] = { DEFAULT_CLEAR_VALUE, DEFAULT_CLEAR_VALUE, DEFAULT_CLEAR_VALUE, DEFAULT_CLEAR_VALUE };
    const float depthClearColor[] = { DEFAULT_DEPTH_CLEAR_VALUE, DEFAULT_DEPTH_CLEAR_VALUE, DEFAULT_DEPTH_CLEAR_VALUE, DEFAULT_DEPTH_CLEAR_VALUE };
    
    cmdList->ClearRenderTargetView(mR.deferredBuffers.worldPositionTexture->cpuHandle(), clearColor, 0u, nullptr);
    cmdList->ClearRenderTargetView(mR.deferredBuffers.worldNormalTexture->cpuHandle(), clearColor, 0u, nullptr);
    cmdList->ClearRenderTargetView(mR.deferredBuffers.materialDiffuseTexture->cpuHandle(), clearColor, 0u, nullptr);
    cmdList->ClearRenderTargetView(mR.deferredBuffers.metallicRoughnessTexture->cpuHandle(), clearColor, 0u, nullptr);
    cmdList->ClearRenderTargetView(mR.deferredBuffers.depthTexture->cpuHandle(), depthClearColor, 0u, nullptr);
}

void DeferredRL::setDescHeaps(res::d3d12::D3D12CommandList &cmdList)
{
    ::ID3D12DescriptorHeap *descriptorHeaps[] = { 
        mR.descHeap->get()
        //mR.rtvDescHeap->get(), 
        //mR.depthStencilTexture->descHeap()
    };
    cmdList->SetDescriptorHeaps(util::count<::UINT>(descriptorHeaps), descriptorHeaps);
}

void DeferredRL::setRootSignatureParameters(res::d3d12::D3D12CommandList &cmdList)
{
    // Set the slow-changing constants.
    cmdList->SetGraphicsRootConstantBufferView(
        compatDeferred::GlobalRS::StaticConstantBuffer, 
        mR.staticConstantsBuffer->gpuAddress());

    // Set the fast-changing constants.
    cmdList->SetGraphicsRootConstantBufferView(
        compatDeferred::GlobalRS::DynamicConstantBuffer, 
        mR.dynamicConstantsBuffer->gpuAddress());

    // Array of material textures.
    cmdList->SetGraphicsRootDescriptorTable(
        compatDeferred::GlobalRS::TextureArray, 
        mR.textureTable.baseHandle());
}

}

}
