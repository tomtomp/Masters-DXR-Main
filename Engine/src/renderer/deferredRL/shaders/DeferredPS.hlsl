/**
 * @file renderer/deferredRl/shaders/DeferredPS.hlsl
 * @author Tomas Polasek
 * @brief Pixel shader used in generation of deferred 
 * G-Buffer.
 */

#ifndef ENGINE_DEFERRED_RL_DEFERRED_PS_HLSL
#define ENGINE_DEFERRED_RL_DEFERRED_PS_HLSL

#include "DeferredShadersCompat.h"

/**
 * Process fragment data and store them in the G-Buffers.
 */
PixelData main(FragmentData input)
{
    PixelData output;

    output.wsPosition = gEncodeWsPosition(input.wsPosition, gDynamic.cameraWorldPosition);
    output.depth = gEncodeDepth(input.ssDepthPosition.z / input.ssDepthPosition.w);

    // Default to vertex normal, but use texture if possible.
    float3 wsNormal = input.wsNormal;
    // TODO - tengent-bitangent-normal transform...
    if (gInstance.normalIdx != HLSL_INVALID_TEXTURE_INDEX)
    { 
        const float3 bitangent = cross(input.mNormal, input.mTangent);
        float3x3 tbn = {
            input.mTangent, 
            bitangent, 
            input.mNormal, 
        };

        const float3 textureNormal = normalize(gTextures[gInstance.normalIdx].Sample(gCommonSampler, input.texCoord).rgb * 2.0f - 1.0f); 
        wsNormal = mul(gInstance.normalToWorld, float4(mul(textureNormal, tbn), 0.0f)).xyz;
    }
    output.wsNormal = gEncodeWsNormal(wsNormal);

    float4 diffuse = 0.0f;
    float2 metallicRoughness = 0.0f;

    if (gInstance.diffuseIdx != HLSL_INVALID_TEXTURE_INDEX)
    { diffuse = gTextures[gInstance.diffuseIdx].Sample(gCommonSampler, input.texCoord).rgba; }
    if (gInstance.materialIdx != HLSL_INVALID_TEXTURE_INDEX)
    { metallicRoughness = gTextures[gInstance.materialIdx].Sample(gCommonSampler, input.texCoord).rg; }

    output.diffuse = gEncodeDiffuse(diffuse);
    output.materialProps = gEncodeMaterialProps(metallicRoughness, gInstance.specularReflections, gInstance.specularRefractions);

    if (gStatic.discardTransparent && output.diffuse.a < 0.5f)
    { discard; }

    return output;
}

#endif // ENGINE_DEFERRED_RL_DEFERRED_PS_HLSL