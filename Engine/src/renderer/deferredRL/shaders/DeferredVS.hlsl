/**
 * @file renderer/deferredRl/shaders/DeferredVS.hlsl
 * @author Tomas Polasek
 * @brief Vertex shader used in generation of deferred 
 * G-Buffer.
 */

#ifndef ENGINE_DEFERRED_RL_DEFERRED_VS_HLSL
#define ENGINE_DEFERRED_RL_DEFERRED_VS_HLSL

#include "DeferredShadersCompat.h"

/**
 * Tranform vertex data and pass them to the pixel shader.
 */
FragmentData main(VertexData input)
{
    FragmentData output;

    output.mNormal = input.normal;
    output.mTangent = input.tangent;
    output.wsPosition = mul(gInstance.modelToWorld, float4(input.position, 1.0f)).xyz;
    output.ssPosition = mul(gDynamic.worldToCamera, float4(output.wsPosition, 1.0f));
    output.wsNormal = mul(gInstance.normalToWorld, float4(input.normal, 0.0f)).xyz;
    output.texCoord = input.texCoord;
    output.ssDepthPosition = output.ssPosition;

    return output;
}

#endif // ENGINE_DEFERRED_RL_DEFERRED_VS_HLSL
