/**
 * @file renderer/RasterDispatch.cpp
 * @author Tomas Polasek
 * @brief Abstraction of rasterization pipeline which 
 * contains all necessary state to draw to output frame 
 * buffers.
 */

#include "stdafx.h"

#include "engine/renderer/RasterDispatch.h"

#include "engine/renderer/RenderSubSystem.h"

namespace quark
{

namespace rndr
{

RasterDispatch::~RasterDispatch()
{ /* Automatic */ }

RasterDispatch::RasterDispatch(RenderContext::PtrT ctx) : 
    mRenderCtx{ ctx }
{ /* Automatic */ }

}

}
