/**
 * @file renderer/RenderScene.cpp
 * @author Tomas Polasek
 * @brief Wrapper around information needed for rendering 
 * of a scene - lists of drawables, camera information, etc.
 */

#include "stdafx.h"

#include "engine/renderer/RenderScene.h"

namespace quark
{

namespace rndr
{

RenderScene::PtrT RenderScene::create(res::scene::DrawableListHandle &drawables, scene::sys::CameraSys &cameraSystem)
{ return PtrT(new RenderScene(drawables, cameraSystem)); }

RenderScene::~RenderScene()
{ /* Automatic */ }

RenderScene::RenderScene(res::scene::DrawableListHandle &drawables, scene::sys::CameraSys &cameraSystem) : 
    mDrawables{ drawables }, mCameraSystem{ &cameraSystem }
{ } 

}

}
