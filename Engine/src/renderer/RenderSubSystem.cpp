/**
 * @file renderer/RenderSubSystem.h
 * @author Tomas Polasek
 * @brief Rendering system.
 */

#include "stdafx.h"

#include "engine/renderer/RenderSubSystem.h"

namespace quark
{

namespace rndr
{

RenderSubSystem::RenderSubSystem(const EngineRuntimeConfig &cfg, const res::win::Window &window) :
    mCfg{ cfg }, 
    mLiveObjectReporter(
        cfg.debugReportLiveObjects, 
        Config::LIVE_REPORTER_DEBUG_ID, 
        Config::LIVE_REPORTER_FLAGS), 
    mDebugConfigurator(
        cfg.debugEnableLayer, 
        cfg.debugGpuValidation), 
    mFactory{ res::d3d12::D3D12DXGIFactory::create() },
    mAdapter{ 
        res::d3d12::D3D12Adapter::create(
            *mFactory, 
            cfg.adapterUseWarp, 
            Config::ADAPTER_FEATURE_LEVEL, 
            Config::RAY_TRACING_ENABLED) },
    mDevice{ 
        res::d3d12::D3D12Device::create(
            *mAdapter, 
            Config::ADAPTER_FEATURE_LEVEL,
            cfg.deviceDisableMsgCategories,
            cfg.deviceDisableMsgSeverities,
            cfg.deviceDisableMsgIds,
            Config::DEVICE_DEBUG_NAME) },
    mDirectCommandQueue{
        res::d3d12::D3D12CommandQueueMgr::create(
            *mDevice,
            D3D12_COMMAND_LIST_TYPE_DIRECT,
            Config::DIRECT_QUEUE_DEBUG_NAME) },
    mBundleCommandQueue{
        res::d3d12::D3D12CommandQueueMgr::create(
            *mDevice,
            D3D12_COMMAND_LIST_TYPE_BUNDLE,
            Config::BUNDLE_QUEUE_DEBUG_NAME) },
    mComputeCommandQueue{
        res::d3d12::D3D12CommandQueueMgr::create(
            *mDevice,
            D3D12_COMMAND_LIST_TYPE_COMPUTE,
            Config::COMPUTE_QUEUE_DEBUG_NAME) },
    mCopyCommandQueue{
        res::d3d12::D3D12CommandQueueMgr::create(
            *mDevice,
            D3D12_COMMAND_LIST_TYPE_COPY,
            Config::COPY_QUEUE_DEBUG_NAME) },
    mSwapChain{
        res::d3d12::D3D12SwapChain::create(
            window, *mDirectCommandQueue, *mFactory, 
            cfg.swapChainNumBackBuffers, 
            Config::SWAP_CHAIN_USAGE,
            Config::SWAP_CHAIN_FORMAT, 
            Config::SWAP_CHAIN_SCALING, 
            Config::SWAP_CHAIN_ALPHA_MODE) },
    mMemoryMgr{
        res::rndr::GpuMemoryMgr::create(
            *mDevice, *mCopyCommandQueue) }
{
	mGpuProfilingAdapter = prof::GpuProfilingAdapter::create(cfg, *this);
    prof::impl::setGlobalProfilingAdapter(mGpuProfilingAdapter.get());

	mRayTracingProvider = RayTracingProvider::create(cfg, *this);

    log<Info>() << "Initialized rendering sub-system." << std::endl;
}

RenderSubSystem::PtrT RenderSubSystem::create(const EngineRuntimeConfig &cfg, const res::win::Window &window)
{ return PtrT{ new RenderSubSystem(cfg, window) }; }

RenderSubSystem::~RenderSubSystem()
{
    flush();
    prof::impl::setGlobalProfilingAdapter(nullptr);
}

void RenderSubSystem::resize(uint32_t width, uint32_t height)
{
    flush();

    mSwapChain->resize(width, height);
}

void RenderSubSystem::flush()
{
    mDirectCommandQueue->flush();
    mComputeCommandQueue->flush();
    mCopyCommandQueue->flush();
}

void RenderSubSystem::flushAll()
{
    flush();
    mBundleCommandQueue->flush();
}

void RenderSubSystem::flushCopyCommands()
{
    flushAll();
    mMemoryMgr->uploadCmdListsExecuted();
}

}

}
