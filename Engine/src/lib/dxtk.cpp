/**
 * @file lib/dxtk.cpp
 * @author Tomas Polasek
 * @brief Helper source for the DirectX ToolKit library.
 */

#include "engine/util/StlWrapperBegin.h"

#include "../lib/dxtk/SimpleMath.cpp"

#include "engine/util/StlWrapperEnd.h"
