/**
 * @file lib/imgui.cpp
 * @author Tomas Polasek
 * @brief Sources of the ImGUI library.
 */

#include "engine/util/StlWrapperBegin.h"

// Main library: 
#include "../lib/imgui/imgui.cpp"
#include "../lib/imgui/imgui_draw.cpp"
#include "../lib/imgui/imgui_widgets.cpp"
//#include "../lib/imgui/imgui_demo.cpp"

#include "engine/util/StlWrapperEnd.h"
