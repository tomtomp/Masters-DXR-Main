/**
 * @file scene/logic/ScriptedCameraController.cpp
 * @author Tomas Polasek
 * @brief Camera controller which automatically controls 
 * the camera according to predefined route.
 */

#include "stdafx.h"

#include "engine/scene/logic/ScriptedCameraController.h"

#include "engine/scene/systems/CameraSys.h"

namespace quark
{

namespace scene
{

namespace logic
{

ScriptedCameraController::ScriptedCameraController()
{ /* Automatic */ }

ScriptedCameraController::~ScriptedCameraController()
{ /* Automatic */ }

ScriptedCameraController::ScriptedCameraController(sys::CameraSys &cameraSystem) 
{ setCameraSystem(cameraSystem); }

void ScriptedCameraController::setCameraSystem(sys::CameraSys &cameraSystem)
{ mCameraSystem = &cameraSystem; }

void ScriptedCameraController::startTrack()
{
    mPlayer.loadTrack(track());
    mPlayer.restartTrack();
}

void ScriptedCameraController::stopTrack()
{ mPlayer.stopTrack(); }

quark::scene::logic::ScriptedCameraTrack &ScriptedCameraController::track()
{ return mTrack; }

void ScriptedCameraController::setLoopTrack(bool enable)
{ mPlayer.setLoopTrack(enable); }

void ScriptedCameraController::update(float deltaMs)
{
    if (!cameraBound() || !mPlayer.running())
    { return; }

    mPlayer.updatePlayback(deltaMs);

    auto cameraState{ mPlayer.currentState() };
    auto &transform{ mCameraSystem->chosenCameraTransform() };

    transform.localTransform.setTranslate(cameraState.translation);
    transform.localTransform.setRotate(cameraState.rotation);

    mCameraSystem->chosenCameraTransformChanged();
}

bool ScriptedCameraController::cameraBound() const
{ return mCameraSystem && mCameraSystem->cameraChosen(); }

bool ScriptedCameraController::running() const
{ return mPlayer.running(); }

ScriptedCameraTrack::KeyPoint &ScriptedCameraTrack::KeyPoint::setRelativeTranslation(const dxtk::math::Vector3 &t)
{ translation = t; translationRelative = true; return *this; }

ScriptedCameraTrack::KeyPoint &ScriptedCameraTrack::KeyPoint::setAbsoluteTranslation(const dxtk::math::Vector3 &t)
{ translation = t; translationRelative = false; return *this; }

ScriptedCameraTrack::KeyPoint &ScriptedCameraTrack::KeyPoint::setRelativeRotation(const dxtk::math::Quaternion &r)
{ rotation = r; rotationRelative = true; return *this; }

ScriptedCameraTrack::KeyPoint &ScriptedCameraTrack::KeyPoint::setAbsoluteRotation(const dxtk::math::Quaternion &r)
{ rotation = r; rotationRelative = false; return *this; }

ScriptedCameraTrack::KeyPoint &ScriptedCameraTrack::KeyPoint::setRelativeYawPitchRoll(const dxtk::math::Vector3 &degrees)
{ 
    rotation = dxtk::math::Quaternion::CreateFromYawPitchRoll(degrees.x, degrees.y, degrees.z); 
    rotationRelative = true; 
    return *this; 
}

ScriptedCameraTrack::KeyPoint &ScriptedCameraTrack::KeyPoint::setAbsoluteYawPitchRoll(const dxtk::math::Vector3 & degrees)
{
    rotation = dxtk::math::Quaternion::CreateFromYawPitchRoll(degrees.x, degrees.y, degrees.z); 
    rotationRelative = false; 
    return *this; 
}

ScriptedCameraTrack::KeyPoint &ScriptedCameraTrack::KeyPoint::setRelativeEuler(const dxtk::math::Vector3 &degrees)
{ return setRelativeYawPitchRoll({ degrees.y, degrees.x, degrees.z }); }

ScriptedCameraTrack::KeyPoint &ScriptedCameraTrack::KeyPoint::setAbsoluteEuler(const dxtk::math::Vector3 &degrees)
{ return setAbsoluteYawPitchRoll({ degrees.y, degrees.x, degrees.z }); }

ScriptedCameraTrack::KeyPoint &ScriptedCameraTrack::KeyPoint::setDuration(float seconds)
{ durationInMilliseconds = sToMs(seconds); return *this; }

ScriptedCameraTrack::ScriptedCameraTrack()
{ /* Automatic */ }

ScriptedCameraTrack::~ScriptedCameraTrack()
{ /* Automatic */ }

ScriptedCameraTrack::KeyPoint &ScriptedCameraTrack::pushKeyPoint()
{ mKeyPoints.push_back({ }); return mKeyPoints.back(); }

void ScriptedCameraTrack::clearKeyPoints()
{ mKeyPoints.clear(); }

void ScriptedCameraTrack::loadFromFile(const res::File &trackFile)
{
    clearKeyPoints();

    if (!trackFile.exists())
    { log<Warning>() << "Camera track load failed: Provided camera track file does not exist!" << std::endl; }

    std::ifstream inputFile(trackFile.file());
    KeyPoint keyPoint;

    while (readKeyPointFromFile(inputFile, keyPoint))
    { mKeyPoints.push_back(keyPoint); }
}

void ScriptedCameraTrack::saveToFile(const res::File &trackFile)
{
    std::ofstream outputFile(trackFile.file());

    for (const auto &keyPoint : mKeyPoints)
    { writeKeyPointToFile(outputFile, keyPoint); }
}

void ScriptedCameraTrack::transformKeyPoints(TransformFunT fun)
{
    for (auto &keyPoint : mKeyPoints)
    { fun(keyPoint); }
}

bool ScriptedCameraTrack::readKeyPointFromFile(std::ifstream &file, KeyPoint &outKeyPoint)
{
    bool result{ true };

    result &= (file >> outKeyPoint.translation.x >> outKeyPoint.translation.y >> outKeyPoint.translation.z).good();
    result &= (file >> outKeyPoint.translationRelative).good();

    result &= (file >> outKeyPoint.rotation.x >> outKeyPoint.rotation.y >> outKeyPoint.rotation.z >> outKeyPoint.rotation.w).good();
    result &= (file >> outKeyPoint.rotationRelative).good();

    result &= (file >> outKeyPoint.durationInMilliseconds).good();

    return result;
}

bool ScriptedCameraTrack::writeKeyPointToFile(std::ofstream &file, const KeyPoint &inKeyPoint)
{
    bool result{ true };

    result &= (file << inKeyPoint.translation.x << " " << inKeyPoint.translation.y << " " << inKeyPoint.translation.z << " ").good();
    result &= (file << inKeyPoint.translationRelative << " ").good();

    result &= (file << inKeyPoint.rotation.x << " " << inKeyPoint.rotation.y << " " << inKeyPoint.rotation.z << " " << inKeyPoint.rotation.w << " ").good();
    result &= (file << inKeyPoint.rotationRelative << " ").good();

    result &= (file << inKeyPoint.durationInMilliseconds << "\n").good();

    return result;
}

ScriptedCameraPlayer::ScriptedCameraPlayer()
{ /* Automatic */ }

ScriptedCameraPlayer::~ScriptedCameraPlayer()
{ /* Automatic */ }

void ScriptedCameraPlayer::loadTrack(const ScriptedCameraTrack &track)
{
    mTrack = track;
    restartTrack();
}

void ScriptedCameraPlayer::setLoopTrack(bool enable)
{ mLoop = enable; }

void ScriptedCameraPlayer::restartTrack()
{
    mCameraState = { };
    mKeyPointIndex = 0u;
    mDeltaMs = 0.0f;
    mRunning = true;
}

void ScriptedCameraPlayer::stopTrack()
{ mRunning = false; }

void ScriptedCameraPlayer::updatePlayback(float deltaMs)
{
    if (!running())
    { return; }

    mDeltaMs += deltaMs;

    auto currentKeyPoint{ getCurrentKeyPoint(mTrack, mKeyPointIndex) };

    auto keyPoint{ getCurrentKeyPoint(mTrack, mKeyPointIndex) };

    while (keyPoint.durationInMilliseconds <= mDeltaMs && running())
    {
        mDeltaMs -= keyPoint.durationInMilliseconds;
        mKeyPointIndex++;

        if (mLoop)
        { mKeyPointIndex %= mTrack.keyPoints().size(); }

        if (keyPoint.translationRelative)
        { mCameraState.translation += keyPoint.translation; }
        else
        { mCameraState.translation = keyPoint.translation; }
        if (keyPoint.rotationRelative)
        { mCameraState.rotation *= keyPoint.rotation; }
        else
        { mCameraState.rotation = keyPoint.rotation; }

        keyPoint = getCurrentKeyPoint(mTrack, mKeyPointIndex);
    }

    if (running())
    { // Interpolate...
        const auto &currentKeyPoint{ getCurrentKeyPoint(mTrack, mKeyPointIndex) };
        const auto &nextKeyPoint{ getNextKeyPoint(mTrack, mKeyPointIndex, mLoop) };

        const auto currentTranslation{ currentKeyPoint.translationRelative ? 
            currentKeyPoint.translation + mCameraState.translation : currentKeyPoint.translation};
        const auto currentRotation{ currentKeyPoint.rotationRelative ? 
            currentKeyPoint.rotation * mCameraState.rotation : currentKeyPoint.rotation};

        const auto nextTranslation{ nextKeyPoint.translationRelative ? 
            nextKeyPoint.translation + currentTranslation : nextKeyPoint.translation};
        const auto nextRotation{ nextKeyPoint.rotationRelative ? 
            nextKeyPoint.rotation * currentRotation : nextKeyPoint.rotation};

        const auto interpolator{ mDeltaMs / currentKeyPoint.durationInMilliseconds };

        mCameraState.translation = dxtk::math::Vector3::Lerp(currentTranslation, nextTranslation, interpolator);
        mCameraState.rotation = dxtk::math::Quaternion::Slerp(currentRotation, nextRotation, interpolator);
    }
}

const ScriptedCameraPlayer::CameraState &ScriptedCameraPlayer::currentState() const
{ return mCameraState; }

bool ScriptedCameraPlayer::running() const
{ return mRunning && mKeyPointIndex < mTrack.keyPoints().size(); }

const ScriptedCameraPlayer::KeyPoint &ScriptedCameraPlayer::getCurrentKeyPoint(
    const ScriptedCameraTrack &track, std::size_t index)
{
    if (index >= track.keyPoints().size())
    { return track.keyPoints().back(); }

    return track.keyPoints()[index];
}

const ScriptedCameraPlayer::KeyPoint &ScriptedCameraPlayer::getNextKeyPoint(
    const ScriptedCameraTrack &track, std::size_t index, bool loop)
{
    const auto numKeyPoints{ track.keyPoints().size() };

    if (index >= numKeyPoints)
    { return track.keyPoints().back(); }

    const auto nextIndex{ loop ? ((index + 1u) % numKeyPoints) : std::min<std::size_t>(index + 1u, numKeyPoints - 1u) };

    return track.keyPoints()[nextIndex];
}

void ScriptedCameraTrackMaker::pushCameraPoint(
    const sys::CameraSys &cameraSystem, ScriptedCameraTrack &track)
{
    if (!cameraSystem.cameraChosen())
    { return; }

    finishPoints(track);

    const auto &transform{ cameraSystem.chosenCameraTransform() };

    mLastKeyPoint = &track.pushKeyPoint();
    mLastKeyPoint->setAbsoluteTranslation(transform.localTransform.translate());
    mLastKeyPoint->setAbsoluteRotation(transform.localTransform.rotate());
}

void ScriptedCameraTrackMaker::finishPoints(ScriptedCameraTrack &track)
{
    UNUSED(track);
    const auto elapsed{ mTimer.elapsedReset<util::HrTimer::SecondsF>() };
    if (mLastKeyPoint)
    { // Finalize the last key-point.
        mLastKeyPoint->setDuration(elapsed.count());
    }
}

}

}

}
