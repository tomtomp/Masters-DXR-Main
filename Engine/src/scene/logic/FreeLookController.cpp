/**
 * @file scene/logic/FreeLookController.cpp
 * @author Tomas Polasek
 * @brief Entity controller which allows a control of 
 * camera in the 3D space.
 */

#include "stdafx.h"

#include "engine/scene/logic/FreeLookController.h"

namespace quark
{

namespace scene
{

namespace logic
{

FreeLookController::FreeLookController()
{ /* Automatic */ }

FreeLookController::~FreeLookController()
{ /* Automatic */ }

FreeLookController::FreeLookController(sys::CameraSys *cameraSystem) :
    mCameraSystem{ cameraSystem }
{ }

dxtk::math::Quaternion FreeLookController::rotationFromMouseDelta(float deltaX, float deltaY)
{
    if (!cameraBound())
    { // Not controlling any camera!
        return dxtk::math::Quaternion{ };
    }

    // Get rotation of the current camera.
    const auto currentRotation{ mCameraSystem->chosenCameraTransform().localTransform.rotate() };
    // Calculate changes based on the deltas.
    const auto changeRotationYaw{ dxtk::math::Quaternion::CreateFromYawPitchRoll(-deltaX, 0.0f, 0.0f) };
    const auto changeRotationPitch{ dxtk::math::Quaternion::CreateFromYawPitchRoll(0.0f, -deltaY, 0.0f) };

    auto result{ changeRotationYaw * currentRotation * changeRotationPitch };
    result.Normalize();

    // Return rotated quaternion.
    return result;
}

void FreeLookController::setRotation(const dxtk::math::Quaternion &rot)
{
    if (!cameraBound())
    { return; }

    mCameraSystem->chosenCameraTransform().localTransform.setRotate(rot);
    mCameraSystem->chosenCameraTransformChanged();
}

void FreeLookController::yawBy(float degrees)
{
    if (!cameraBound())
    { return; }

    mCameraSystem->chosenCameraTransform().localTransform.yawPitchRollBy(degrees, 0.0f, 0.0f);
    mCameraSystem->chosenCameraTransformChanged();
}

void FreeLookController::yawDelta(float deltaDegrees)
{ mYawPitchRollDeltas.x = deltaDegrees; }

void FreeLookController::pitchBy(float degrees)
{
    if (!cameraBound())
    { return; }

    mCameraSystem->chosenCameraTransform().localTransform.yawPitchRollBy(0.0f, degrees, 0.0f);
    mCameraSystem->chosenCameraTransformChanged();
}

void FreeLookController::pitchDelta(float deltaDegrees)
{ mYawPitchRollDeltas.y = deltaDegrees; }

void FreeLookController::rollBy(float degrees)
{
    if (!cameraBound())
    { return; }

    mCameraSystem->chosenCameraTransform().localTransform.yawPitchRollBy(0.0f, 0.0f, degrees);
    mCameraSystem->chosenCameraTransformChanged();
}

void FreeLookController::rollDelta(float deltaDegrees)
{ mYawPitchRollDeltas.z = deltaDegrees; }

void FreeLookController::yawPitchRollBy(const dxtk::math::Vector3 &degrees)
{
    if (!cameraBound())
    { return; }

    mCameraSystem->chosenCameraTransform().localTransform.yawPitchRollBy(degrees.x, degrees.y, degrees.z);
    mCameraSystem->chosenCameraTransformChanged();
}

void FreeLookController::yawPitchRollBy(float yawDegrees, float pitchDegrees, float rollDegrees)
{
    if (!cameraBound())
    { return; }

    mCameraSystem->chosenCameraTransform().localTransform.yawPitchRollBy(yawDegrees, pitchDegrees, rollDegrees);
    mCameraSystem->chosenCameraTransformChanged();
}

void FreeLookController::yawPitchRollDelta(float yawDeltaDegrees, float pitchDeltaDegrees, float rollDeltaDegrees)
{ mYawPitchRollDeltas = dxtk::math::Vector3(yawDeltaDegrees, pitchDeltaDegrees, rollDeltaDegrees); }

dxtk::math::Vector3 FreeLookController::position() const
{
    if (!cameraBound())
    { return { }; }

    return mCameraSystem->chosenCameraTransform().localTransform.translate();
}

void FreeLookController::position(const dxtk::math::Vector3 &pos)
{
    if (!cameraBound())
    { return; }

    mCameraSystem->chosenCameraTransform().localTransform.setTranslate(pos);
    mCameraSystem->chosenCameraTransformChanged();
}

void FreeLookController::xMoveBy(float units)
{
    if (!cameraBound())
    { return; }

    mCameraSystem->chosenCameraTransform().localTransform.translateBy(units, 0.0f, 0.0f);
    mCameraSystem->chosenCameraTransformChanged();
}

void FreeLookController::yMoveBy(float units)
{
    if (!cameraBound())
    { return; }

    mCameraSystem->chosenCameraTransform().localTransform.translateBy(0.0f, units, 0.0f);
    mCameraSystem->chosenCameraTransformChanged();
}

void FreeLookController::zMoveBy(float units)
{
    if (!cameraBound())
    { return; }

    mCameraSystem->chosenCameraTransform().localTransform.translateBy(0.0f, 0.0f, units);
    mCameraSystem->chosenCameraTransformChanged();
}

void FreeLookController::xyzMoveBy(const dxtk::math::Vector3 &units)
{
    if (!cameraBound())
    { return; }

    mCameraSystem->chosenCameraTransform().localTransform.translateBy(units.x, units.y, units.z);
    mCameraSystem->chosenCameraTransformChanged();
}

void FreeLookController::xyzMoveBy(float xUnits, float yUnits, float zUnits)
{
    if (!cameraBound())
    { return; }

    mCameraSystem->chosenCameraTransform().localTransform.translateBy(xUnits, yUnits, zUnits);
    mCameraSystem->chosenCameraTransformChanged();
}

dxtk::math::Vector3 FreeLookController::movementForView(float xUnits, float yUnits, float zUnits)
{ return movementForView(dxtk::math::Vector3(xUnits, yUnits, zUnits)); }

dxtk::math::Vector3 FreeLookController::movementForView(const dxtk::math::Vector3 &units)
{
    if (!cameraBound())
    { return dxtk::math::Vector3::Zero; }

    auto rotation{ -mCameraSystem->chosenCameraTransform().localTransform.rotate() };
    rotation.Conjugate();
    rotation.Normalize();

    const auto viewMovement{ 
        dxtk::math::Vector3::Transform(
            units, 
            rotation) };

    return viewMovement;
}

void FreeLookController::forward(float units)
{ mViewMovement.z = -units; }

void FreeLookController::backward(float units)
{ mViewMovement.z = units; }

void FreeLookController::left(float units)
{ mViewMovement.x = units; }

void FreeLookController::right(float units)
{ mViewMovement.x = -units; }

void FreeLookController::up(float units)
{ mViewMovement.y = -units; }

void FreeLookController::down(float units)
{ mViewMovement.y = units; }

void FreeLookController::speedModifier(float unitMultiplier)
{ mSpeedMultiplier = unitMultiplier; }

void FreeLookController::setViewportSize(float width, float height)
{
    if (!mCameraSystem)
    { return; }

    mCameraSystem->setViewportSize(width, height);
}

void FreeLookController::setFov(float horFov)
{
    if (!mCameraSystem)
    { return; }

    mCameraSystem->setFov(DirectX::XMConvertToRadians(horFov));
}

void FreeLookController::setCameraSystem(sys::CameraSys &cameraSystem)
{
    if (mCameraSystem != &cameraSystem)
    {
        mCameraSystem = &cameraSystem;
        resetDeltas();
    }
}

void FreeLookController::update(float deltaTime)
{
    if (!cameraBound())
    { return; }

    if (mCameraSystem->cameraChanged())
    { resetDeltas(); }
    else
    { applyDeltas(deltaTime); }

    mCameraSystem->chosenCameraTransformChanged();
    mCameraSystem->generateCameraMatrices();
}

void FreeLookController::resetDeltas()
{
    mYawPitchRollDeltas = dxtk::math::Vector3{ };
    mViewMovement = dxtk::math::Vector3{ };
}

void FreeLookController::applyDeltas(float deltaTime)
{
    if (mViewMovement.Length() > 0.0f)
    { xyzMoveBy(movementForView(mViewMovement) * deltaTime * mSpeedMultiplier); }
    if (mYawPitchRollDeltas.Length() > 0.0f)
    { yawPitchRollBy(mYawPitchRollDeltas * deltaTime); }
}

bool FreeLookController::cameraBound() const
{ return mCameraSystem && mCameraSystem->cameraChosen(); }

}

}

}
