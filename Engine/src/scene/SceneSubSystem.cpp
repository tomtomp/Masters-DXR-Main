/**
 * @file scene/SceneManager.h
 * @author Tomas Polasek
 * @brief Helper class for managing scenes - loading, unloading, activating, etc.
 */

#include "stdafx.h"

#include "engine/scene/SceneSubSystem.h"

#include "engine/scene/loaders/GlTFLoader.h"
#include "engine/scene/loaders/SceneDescLoader.h"

namespace quark
{

namespace scene
{

SceneSubSystem::SceneSubSystem(const EngineRuntimeConfig &cfg, rndr::RenderSubSystem &renderSubSystem) : 
    //mUniverse{ SceneUniverse::instance() },
    mSceneMgr{ res::scene::SceneMgr::createMgr() },
    mDrawableListMgr{ res::scene::DrawableListMgr::createMgr() }
{
	UNUSED(cfg);
    mSceneMgr->initialize(renderSubSystem.memoryMgr(), mUniverse);

    initializeUniverse();
}

SceneSubSystem::PtrT SceneSubSystem::create(const EngineRuntimeConfig &cfg, rndr::RenderSubSystem &renderSubSystem)
{ return PtrT{ new SceneSubSystem(cfg, renderSubSystem) }; }

res::scene::SceneHandle SceneSubSystem::loadGlTFScene(const res::File &sceneFile)
{
    log<Info>() << "Loading a scene from: " << sceneFile << std::endl;

    // Check if we already loaded it before.
    const auto primarySceneHandle{ mSceneMgr->getOrNull(sceneFile.filename()) };
    if (primarySceneHandle)
    { return primarySceneHandle; }

    loaders::GlTFLoader glTFLoader;
    loaders::SceneDescLoader sceneDescLoader;

    return sceneDescLoader.loadFromDesc(glTFLoader.loadFromFileToSceneDesc(sceneFile), *mSceneMgr);
}

res::scene::SceneHandle SceneSubSystem::loadGlTFSceneDirect(const res::File &sceneFile)
{
    log<Info>() << "Loading a scene directly from: " << sceneFile << std::endl;

    // Check if we already loaded it before.
    const auto primarySceneHandle{ mSceneMgr->getOrNull(sceneFile.filename()) };
    if (primarySceneHandle)
    { return primarySceneHandle; }

    loaders::GlTFLoader loader;

    return loader.loadFromFile(sceneFile, *mSceneMgr);
}

res::scene::SceneHandle SceneSubSystem::loadSceneDescription(SceneDescription::ConstPtrT sceneDesc)
{
    log<Info>() << "Loading a scene from scene description: " << sceneDesc->name() << std::endl;

    loaders::SceneDescLoader sceneDescLoader;

    return sceneDescLoader.loadFromDesc(sceneDesc, *mSceneMgr);
}

res::scene::SceneHandle SceneSubSystem::loadTestingScene()
{
    log<Info>() << "Loading the testing scene!" << std::endl;;

    loaders::SceneDescLoader sceneDescLoader;

    return sceneDescLoader.loadTestingScene(*mSceneMgr);
}

res::scene::SceneHandle SceneSubSystem::loadEmptyScene()
{
    log<Info>() << "Loading a default empty scene!" << std::endl;
 
    // Create the default scene.
    auto sceneHandle{ mSceneMgr->create(DEFAULT_EMPTY_SCENE_NAME) };
    auto &scene{ sceneHandle.get() };

    // Create one sub-scene containing a camera.
    auto subSceneHandle{ scene.createSubScene("Default") };
    scene.setCurrentSubScene(subSceneHandle);

    // Create the camera entity and component.
    auto cameraEntity{ scene.createEntity(subSceneHandle, "PrimaryCamera") };
    auto camera{ cameraEntity.add<comp::Camera>() };
    // Setup perspective camera with variable aspect ratio.
    camera->type = comp::Camera::CameraType::Perspective;
    camera->firstParam.aspectRatio = 0.0f;
    camera->secondParam.horFov = 90.0f;
    camera->zNear = 0.1f;
    camera->zFar = 100.0f;

    return sceneHandle;
}

void SceneSubSystem::prepareScene(const res::scene::SceneHandle &sceneHandle)
{
    auto &scene{ sceneHandle.get() };
    if (scene.finalized())
    { // Already prepared...
        return;
    }

    scene.finalizeChanges();
}

void SceneSubSystem::refresh()
{
    mUniverse.refresh();
    mSceneChooserSys->refresh();
    mUniverse.refresh();
    mSceneGraphDirtifierSys->refresh(mCurrentScene);
    if (mCurrentScene)
    { mCurrentScene->refresh(); }
    mSceneDrawSys->refreshToDraw();
}

void SceneSubSystem::setCurrentScene(const res::scene::SceneHandle &sceneHandle)
{
    const auto &scene{ sceneHandle.get() };

    if (!scene.finalized())
    { throw SceneNotPreparedException("Cannot set scene as current: Scene is not prepared!"); }

    // Remember which scene is running.
    mCurrentScene = sceneHandle;

    // Prepare to activate entities within the chosen scene.
    mSceneChooserSys->setActiveScene(scene.currentSceneSpecification());
}

res::scene::DrawableListHandle SceneSubSystem::getToDraw()
{ return mSceneDrawSys->getToDraw(); }

EntityHandle SceneSubSystem::createEntity(const std::string &name)
{ return mCurrentScene->createEntity(name); }

void SceneSubSystem::destroyEntity(EntityHandle &entity)
{
    const auto result{ mCurrentScene->destroyEntity(entity) };
    ASSERT_SLOW(result);
}

void SceneSubSystem::initializeUniverse()
{
    mUniverse.registerSceneComponents();
    mUniverse.init();

    mSceneChooserSys = mUniverse.addSystem<sys::SceneChooserSys>();
    mSceneDrawSys = mUniverse.addSystem<sys::SceneDrawSys>(mDrawableListMgr);
    mSceneGraphDirtifierSys = mUniverse.addSystem<sys::SceneGraphDirtifierSys>();
    mEntityManipulatorSys = mUniverse.addSystem<sys::EntityManipulatorSys>();

    mUniverse.refresh();
}

}

}
