/**
 * @file scene/loaders/GlTFLoader.cpp
 * @author Tomas Polasek
 * @brief Wrapper around tinyglTF library, used for loading glTF files.
 */

#include "stdafx.h"

#include "engine/scene/loaders/GlTFLoader.h"

#include "engine/renderer/ElementFormat.h"
#include "engine/lib/tinygltf.h"
#include "engine/util/prof/Profiler.h"

namespace quark {

namespace scene {

namespace loaders {

/**
 * Wrapper used for passing glTF data.
 * Instances must be created using make_shared!
 */
class GlTFModelWrapper : public std::enable_shared_from_this<GlTFModelWrapper>
{
public:
    /// glTF extension used for ASCII file format.
    static constexpr const char* GLTF_ASCII_FILE_EXT{"gltf"};
    /// glTF extension used for binary file format.
    static constexpr const char* GLTF_BINARY_FILE_EXT{"glb"};

    // Material key values:
    /// Standard normal texture.
    static constexpr const char* GLTF_MAT_NORMAL_TEXTURE{"normalTexture"};
    /// Occlusion baking texture.
    static constexpr const char* GLTF_MAT_OCCLUSION_TEXTURE{"occlusionTexture"};
    /// Light emission texture.
    static constexpr const char* GLTF_MAT_EMISSIVE_TEXTURE{"emissiveTexture"};
    /// Light emission factor.
    static constexpr const char* GLTF_MAT_EMISSIVE_FACTOR{"emissiveFactor"};
    /// Alpha rendering mode.
    static constexpr const char* GLTF_MAT_ALPHA_MODE{"alphaMode"};
    /// Alpha cutoff for alpha masking.
    static constexpr const char* GLTF_MAT_ALPHA_CUTOFF{"alphaCutoff"};
    /// Is the material visible from both sides?
    static constexpr const char* GLTF_MAT_DOUBLE_SIDED{"doubleSided"};

    /// Enumeration of possible keys found in the material definition.
    enum class GlTFMaterialKey
    {
        NormalTexture,
        OcclusionTexture,
        EmissiveTexture,
        EmissiveFactor,
        AlphaMode,
        AlphaCutoff,
        DoubleSided,
        Unknown
    }; // enum class GlTFMaterialKey

    /**
     * Convert string representation of the key found in glTF material
     * definition.
     * @param strKey String representation of the key.
     * @return Returns corresponding enum value.
     */
    static GlTFMaterialKey matStrKeyToKey(const std::string& strKey);

    /// Fully opaque material.
    static constexpr const char* GLTF_MAT_ALPHA_MODE_OPAQUE{"OPAQUE"};
    /// Binary transparency, based on alpha cutoff..
    static constexpr const char* GLTF_MAT_ALPHA_MODE_MASK{"MASK"};
    /// Standard alpha blending.
    static constexpr const char* GLTF_MAT_ALPHA_MODE_BLEND{"BLEND"};

    /// Enumeration of possible alpha modes.
    enum class GlTFMaterialAlphaMode
    {
        Opaque,
        Mask,
        Blend,
        Unknown
    }; // enum class GlTFMaterialAlphaMode

    /**
     * Convert string representation of the alpha mode found in glTF material
     * definition.
     * @param modeStr String representation of alpha mode.
     * @return Returns corresponding enum value.
     */
    static GlTFMaterialAlphaMode matAlphaStrToAlphaMode(const std::string& modeStr);

    // Material PBR key values:
    /// Diffuse color factor.
    static constexpr const char* GLTF_MAT_PBR_BASE_COLOR_FACTOR{"baseColorFactor"};
    /// Diffuse color texture.
    static constexpr const char* GLTF_MAT_PBR_BASE_COLOR_TEXTURE{"baseColorTexture"};
    /// Metallicity of the material.
    static constexpr const char* GLTF_MAT_PBR_METALLIC_FACTOR{"metallicFactor"};
    /// Roughness of the material.
    static constexpr const char* GLTF_MAT_PBR_ROUGHNESS_FACTOR{"roughnessFactor"};
    /// Metallicity-Roughness texture.
    static constexpr const char* GLTF_MAT_PBR_MET_ROUGH_TEXTURE{"metallicRoughnessTexture"};

    /// Enumeration of possible keys found in PBR part of the material definition.
    enum class GlTFPbrMaterialKey
    {
        BaseColorFactor,
        BaseColorTexture,
        MetallicFactor,
        RoughnessFactor,
        MetallicRoughnessTexture,
        Unknown
    }; // enum class GlTFPbrMaterialKey

    /**
     * Convert string representation of the key found in glTF PBR material
     * definition.
     * @param strKey String representation of the key.
     * @return Returns corresponding enum value.
     */
    static GlTFPbrMaterialKey matPbrStrKeyToKey(const std::string& strKey);

    /// Primitive attribute containing position vertex data.
    static constexpr const char* GLTF_PRIMITIVE_ATTRIBUTE_POSITION{"POSITION"};
    /// Primitive attribute containing normal vertex data.
    static constexpr const char* GLTF_PRIMITIVE_ATTRIBUTE_NORMAL{"NORMAL"};
    /// Primitive attribute containing tangent vertex data.
    static constexpr const char* GLTF_PRIMITIVE_ATTRIBUTE_TANGENT{"TANGENT"};
    /// Primitive attribute containing first texture coordinate data.
    static constexpr const char* GLTF_PRIMITIVE_ATTRIBUTE_TEXCOORD0{"TEXCOORD_0"};
    /// Primitive attribute containing second texture coordinate data.
    static constexpr const char* GLTF_PRIMITIVE_ATTRIBUTE_TEXCOORD1{"TEXCOORD_1"};
    /// Primitive attribute containing color data.
    static constexpr const char* GLTF_PRIMITIVE_ATTRIBUTE_COLOR{"COLOR_0"};
    /// Primitive attribute containing joint data.
    static constexpr const char* GLTF_PRIMITIVE_ATTRIBUTE_JOINT{"JOINTS_0"};
    /// Primitive attribute containing joint weight data.
    static constexpr const char* GLTF_PRIMITIVE_ATTRIBUTE_WEIGHT{"WEIGHTS_0"};

    /// Attributes found in glTF primitive specification.
    enum class GlTFPrimAttribute
    {
        /// Vec3, float.
        Position,
        /// Vec3, float.
        Normal,
        /// Vec4, float.
        Tangent,
        /// Vec2, float/unorm8/unorm16.
        TexCoord0,
        /// Vec2, float/unorm8/unorm16.
        TexCoord1,
        /// Vec2, float/unorm8/unorm16.
        Color,
        /// Vec2, /uint8/uint16.
        Joint,
        /// Vec2, float/unorm8/unorm16.
        Weight,
        Unknown
    }; // enum class GlTFPrimAttribute

    /**
     * Convert string representation of the attribute found
     * in glTF primitive definition.
     * @param strKey String representation of the attribute.
     * @return Returns corresponding enum value.
     */
    static GlTFPrimAttribute primStrAttrToAttr(const std::string& strAttr);

    /// Camera type string representing the perspective camera.
    static constexpr const char* TINYGLTF_CAMERA_TYPE_PERSPECTIVE{"perspective"};
    /// Camera type string representing the orthographic camera.
    static constexpr const char* TINYGLTF_CAMERA_TYPE_ORTHOGRAPHIC{"orthographic"};

    /// glTF camera types.
    enum class GlTFCameraType
    {
        Perspective,
        Orthographic,
        Unknown
    }; // enum class GlTFCameraType

    /**
     * Convert string representation of the attribute found
     * in glTF primitive definition.
     * @param strType String representation of the camera type.
     * @return Returns corresponding enum value.
     */
    static GlTFCameraType cameraTypeStrToType(const std::string& strType);

    /// Name of the default sampler, used for textures without sampler specified.
    static constexpr const char* DEFAULT_SAMPLER_NAME{"DefaultSampler"};

    /// Listing of file formats used by glTF.
    enum class GlTFFileFormat
    {
        Ascii,
        Binary,
        Unknown
    }; // enum class GlTFFileFormat

    /// Create instance of the loader.
    static std::shared_ptr<GlTFModelWrapper> create();

    ~GlTFModelWrapper() = default;

    // No copying or moving.
    GlTFModelWrapper(const GlTFModelWrapper& other) = delete;
    GlTFModelWrapper& operator=(const GlTFModelWrapper& other) = delete;
    GlTFModelWrapper(GlTFModelWrapper&& other) = delete;
    GlTFModelWrapper& operator=(GlTFModelWrapper&& other) = delete;

    /**
     * Load glTF scenes from provided file.
     */
    void loadScenesFromFile(const res::File& sceneFile);

    /**
     * Get glTF file format from extension string.
     * @param ext Extension of the file.
     * @return Returns glTF file format.
     */
    static GlTFFileFormat formatFromStr(const std::string& ext);

    /**
     * Create scenes from loaded data.
     * @param mgr Scene manager used to manage the scenes.
     * @param primaryName Name of the primary scene.
     * @return Returns handle to the primary scene.
     */
    res::scene::SceneHandle createScenes(res::scene::SceneMgr& mgr, const std::string& primaryName);

private:
    GlTFModelWrapper() = default;

    /// Helper method for profiling TinyGLTF image loading.
    static bool profilingLoadImageWrapper(tinygltf::Image* image,
        int imageIdx, std::string* err, std::string* warn,
        int reqWidth, int reqHeight, const unsigned char* bytes,
        int size, void* ptr);

    /**
     * Create buffer according to the glTF original.
     * @param primaryScene Scene using the material.
     * @param buffer Original glTF buffer definition.
     */
    void createBuffer(res::scene::Scene& primaryScene, const tinygltf::Buffer& buffer);

    /**
     * Create image according to the glTF original.
     * @param primaryScene Scene using the material.
     * @param image Original glTF image definition.
     */
    void createImage(res::scene::Scene& primaryScene, const tinygltf::Image& image);

    /**
     * Create buffer view according to the glTF original.
     * @param primaryScene Scene using the material.
     * @param accessor Original glTF buffer view definition.
     */
    void createBufferView(res::scene::Scene& primaryScene, const tinygltf::Accessor& accessor);

    /**
     * Create sampler according to the glTF original.
     * @param primaryScene Scene using the material.
     * @param sampler Original glTF sampler definition.
     */
    void createSampler(res::scene::Scene& primaryScene, const tinygltf::Sampler& sampler);

    /**
     * Create the default sampler used by glTF texture
     * without specified sampler.
     * @param primaryScene Scene using the material.
     */
    void createDefaultSampler(res::scene::Scene& primaryScene);

    /**
     * Create texture according to the glTF original.
     * @param primaryScene Scene using the texture.
     * @param texture Original glTF texture definition.
     */
    void createTexture(res::scene::Scene& primaryScene, const tinygltf::Texture& texture);

    /**
     * Create material according to the glTF original.
     * @param primaryScene Scene using the material.
     * @param material Original glTF material definition.
     */
    void createMaterial(res::scene::Scene& primaryScene, const tinygltf::Material& material);

    /**
     * Process a single material parameter and set its
     * value into given material.
     * @param material Change values of this material
     * according to the parameter
     * @param parameter Parameter of the material gained
     * from glTF.
     */
    void processMaterialParameter(res::rndr::Material& material, const std::pair<std::string, tinygltf::Parameter>& parameter);

    /**
     * Perform parsing of material alpha mode parameter.
     * @param material Target material.
     * @param parameter Alpha-mode parameter.
     */
    void processMaterialAlphaMode(res::rndr::Material& material, const std::pair<std::string, tinygltf::Parameter>& parameter);

    /**
     * Process a single material parameter and set its
     * value into given material.
     * This method should be used for material parameters
     * common for all materials.
     * @param material Change values of this material
     * according to the parameter
     * @param parameter Parameter of the material gained
     * from glTF.
     */
    void processCommonMaterialParameter(res::rndr::Material& material, const std::pair<std::string, tinygltf::Parameter>& parameter);

    /**
     * Process a single primitive attribute and set its
     * value into given primitive
     * @param primitive Target primitive.
     * @param attribute Primitive attribute to process.
     */
    void processPrimitiveAttribute(res::rndr::Primitive& primitive, const std::pair<std::string, int>& attribute);

    /**
     * Create primitive according to the glTF original.
     * @param mesh Mesh which contains the primitive.
     * @param primitive Original glTF primitive definition.
     */
    void createMeshPrimitive(res::rndr::Mesh& mesh, const tinygltf::Primitive& primitive);

    /**
     * Create mesh according to the glTF original.
     * @param primaryScene Scene using the mesh.
     * @param mesh Original glTF mesh definition.
     */
    void createMesh(res::scene::Scene& primaryScene, const tinygltf::Mesh& mesh);

    /// Helper record for storing information about required conversions.
    struct ConversionRequest
    {
        /// Convert from this type.
        rndr::ElementFormat sourceFormat{ };
        /// Convert to this type.
        rndr::ElementFormat targetFormat{ };
        /// Target GlTF component type.
        int targetComponentType;
        /// Target GlTF type.
        int targetType;
    }; // struct ConversionRequest

    /**
     * Find a list of required conversions.
     * @return Returns a map from buffer view index to conversion
     * request structure.
     */
    std::map<std::size_t, ConversionRequest> findRequiredConversions(const tinygltf::Model& model);

    /// Helper structure, associating buffer view with its index.
    struct IndexedBufferView
    {
        /// Original index of the buffer view.
        std::size_t originalIndex{0u};
        /// Content of the structure.
        tinygltf::BufferView bufferView{};
    }; // struct IndexedBufferView

    /**
     * Predict how many bytes will be required to perform the copy
     * or conversion of buffer view data.
     * @param view Source buffer view.
     * @param request Conversion request, may be nullptr.
     */
    std::size_t calculateConvertCopyBytes(const IndexedBufferView& view, const ConversionRequest* request);

    /**
     * Convert or just copy data specified by the view into
     * the output buffer.
     * @param model Loaded model.
     * @param dst Destination of the copy.
     * @param view Source buffer view.
     * @param requiredBytes Number of bytes required.
     * @param request Conversion request, may be nullptr.
     */
    void convertCopyData(const tinygltf::Model& model,
                         uint8_t* dst,
                         const IndexedBufferView& view,
                         std::size_t requiredBytes,
                         const ConversionRequest* request);

    /// Find conversion request for given buffer view.
    const ConversionRequest* findConversionRequest(const IndexedBufferView& view, const std::map<std::size_t, ConversionRequest>& requests);

    /**
     * Go through all attribute buffers and index
     * buffers and convert them to application supported
     * types.
     * @param model Loaded model.
     */
    void convertToSupportedTypes(tinygltf::Model& model);

    /**
     * Create resources required by all of the sub-scenes.
     * @param primaryScene Primary scene holder.
     */
    void createSceneResources(res::scene::Scene& primaryScene);

    /**
     * Initialize perspective camera from glTF camera
     * specification.
     * @param camera Original glTF camera.
     * @param cameraComp Target camera component.
     */
    void initializeNodeEntityPerspCamera(const tinygltf::Camera& camera, comp::Camera& cameraComp);

    /**
     * Initialize perspective camera from glTF camera
     * specification.
     * @param camera Original glTF camera.
     * @param cameraComp Target camera component.
     */
    void initializeNodeEntityOrthoCamera(const tinygltf::Camera& camera, comp::Camera& cameraComp);

    /**
     * Initilize entity's camera component according to the
     * glTF camera properties.
     * @param camera Original glTF camera.
     * @param entity The target entity.
     */
    void initializeNodeEntityCamera(const tinygltf::Camera& camera, EntityHandle& entity);

    /**
     * Initialize entity's rendering component according to the
     * glTF mesh properties.
     * @param mesh Handle to the mesh.
     * @param entity The target entity.
     */
    void initializeNodeEntityMesh(const res::rndr::MeshHandle& mesh, EntityHandle& entity);

    /**
     * Initialize entity's transform component according to the
     * glTF node specification.
     * @param node Original glTF node.
     * @param entity The target entity.
     */
    void initializeNodeEntityTransform(const tinygltf::Node& node, EntityHandle& entity);

    /**
     * Initialize entity according to the glTF node specification.
     * @param node Original glTF node.
     * @param entity The target entity.
     */
    void initializeNodeEntity(const tinygltf::Node& node, EntityHandle& entity);

    /**
     * Add sub-scene entities starting with the root node
     * to provided sub-scene.
     * @param primaryScene Primary scene holder.
     * @param subScene Sub-scene which should contain the entities.
     * @param rootNodeIdx Index of the root node within the glTF
     * nodes list.
     */
    void createSubSceneEntities(res::scene::Scene& primaryScene, res::scene::SubSceneHandle& subScene, std::size_t rootNodeIdx);

    /**
     * Create a default camera, which can be used to peer into the
     * scene.
     * @param primaryScene Primary scene holder.
     * @param subScene Sub-scene which should contain the camera.
     */
    void createDefaultCamera(res::scene::Scene& primaryScene, res::scene::SubSceneHandle& subScene);

    /**
     * Create sub-scene according to the glTF original.
     * @param primaryScene Primary scene holder.
     * @param scene Original glTF scene definition.
     */
    void createSubScene(res::scene::Scene& primaryScene, const tinygltf::Scene& scene);

    /**
     * Create entities within the sub-scenes.
     * @param primaryScene Primary scene holder.
     */
    void createSceneEntities(res::scene::Scene& primaryScene);

    /**
     * Get format for a given number of components.
     * @param numComponents Number of components for each pixel.
     * @return Returns pixel format.
     */
    rndr::ElementFormat numComponentsToFormat(std::size_t numComponents);

    /**
     * Convert minification and magnification filtering options
     * into one filter value.
     * @param minFilter Minification glTF filtering option.
     * @param magFilter Magnification glTF filtering option.
     * @return Returns combination of min and mag into one value.
     */
    rndr::MinMagMipFilter minMagToFilter(int minFilter, int magFilter);

    /**
     * Convert glTF wrap mode into texture addressing mode.
     * @param wrapS Wrapping mode.
     * @return Returns texture addressing mode.
     */
    rndr::SampleEdge wrapToEdgeFilter(int wrap);

    /**
     * Convert glTF wrap mode into texture addressing mode.
     * @param wrapS Wrapping mode in first coordinate.
     * @param wrapT Wrapping mode in second coordinate.
     * @param wrapR Wrapping mode in third coordinate.
     * @return Returns texture addressing mode.
     */
    rndr::EdgeFilterUVW wrapToEdgeFilters(int wrapS, int wrapT, int wrapR);

    /**
     * Convert component type and type into element format.
     * @param componentType Type of a single component.
     * @param elementClass Class of a single element.
     * @return Returns the element format.
     */
    rndr::ElementFormat componentTypeToElementFormat(int componentType, int elementClass);

    /// GlTF file loader.
    tinygltf::TinyGLTF mLoader;
    /// Model of the loaded glTF scene.
    tinygltf::Model mModel;

    /// Buffer resources created for the primary scene.
    std::vector<res::rndr::BufferHandle> mBuffers;
    /// Buffer view resources created for the primary scene.
    std::vector<res::rndr::BufferViewHandle> mBufferViews;
    /// Sampler resources created for the primary scene.
    std::vector<res::rndr::StaticSamplerHandle> mSamplers;
    /// Information about image buffers.
    std::vector<const tinygltf::Image*> mImageInfo;
    /// Texture resources created for the primary scene.
    std::vector<res::rndr::TextureHandle> mTextures;
    /// Material resources created for the primary scene.
    std::vector<res::rndr::MaterialHandle> mMaterials;
    /// Mesh resources created for the primary scene.
    std::vector<res::rndr::MeshHandle> mMeshes;

protected:
}; // class GlTFModelWrapper

} // namespace loaders

} // namespace scene

} // namespace quark

// Implementation begin.

namespace quark {

namespace scene {

namespace loaders {

GlTFModelWrapper::GlTFMaterialKey GlTFModelWrapper::matStrKeyToKey(const std::string& strKey)
{
    if (strKey == GLTF_MAT_NORMAL_TEXTURE)
    {
        return GlTFMaterialKey::NormalTexture;
    }
    if (strKey == GLTF_MAT_OCCLUSION_TEXTURE)
    {
        return GlTFMaterialKey::OcclusionTexture;
    }
    if (strKey == GLTF_MAT_EMISSIVE_TEXTURE)
    {
        return GlTFMaterialKey::EmissiveTexture;
    }
    if (strKey == GLTF_MAT_EMISSIVE_FACTOR)
    {
        return GlTFMaterialKey::EmissiveFactor;
    }
    if (strKey == GLTF_MAT_ALPHA_MODE)
    {
        return GlTFMaterialKey::AlphaMode;
    }
    if (strKey == GLTF_MAT_ALPHA_CUTOFF)
    {
        return GlTFMaterialKey::AlphaCutoff;
    }
    if (strKey == GLTF_MAT_DOUBLE_SIDED)
    {
        return GlTFMaterialKey::DoubleSided;
    }

    return GlTFMaterialKey::Unknown;
}

GlTFModelWrapper::GlTFMaterialAlphaMode GlTFModelWrapper::matAlphaStrToAlphaMode(const std::string& modeStr)
{
    if (modeStr == GLTF_MAT_ALPHA_MODE_OPAQUE)
        return GlTFMaterialAlphaMode::Opaque;
    if (modeStr == GLTF_MAT_ALPHA_MODE_MASK)
        return GlTFMaterialAlphaMode::Mask;
    if (modeStr == GLTF_MAT_ALPHA_MODE_BLEND)
        return GlTFMaterialAlphaMode::Blend;

    return GlTFMaterialAlphaMode::Unknown;
}

GlTFModelWrapper::GlTFPbrMaterialKey GlTFModelWrapper::matPbrStrKeyToKey(const std::string& strKey)
{
    if (strKey == GLTF_MAT_PBR_BASE_COLOR_FACTOR)
    {
        return GlTFPbrMaterialKey::BaseColorFactor;
    }
    if (strKey == GLTF_MAT_PBR_BASE_COLOR_TEXTURE)
    {
        return GlTFPbrMaterialKey::BaseColorTexture;
    }
    if (strKey == GLTF_MAT_PBR_METALLIC_FACTOR)
    {
        return GlTFPbrMaterialKey::MetallicFactor;
    }
    if (strKey == GLTF_MAT_PBR_ROUGHNESS_FACTOR)
    {
        return GlTFPbrMaterialKey::RoughnessFactor;
    }
    if (strKey == GLTF_MAT_PBR_MET_ROUGH_TEXTURE)
    {
        return GlTFPbrMaterialKey::MetallicRoughnessTexture;
    }

    return GlTFPbrMaterialKey::Unknown;
}

GlTFModelWrapper::GlTFPrimAttribute GlTFModelWrapper::primStrAttrToAttr(const std::string& strAttr)
{
    if (strAttr == "POSITION")
    {
        return GlTFPrimAttribute::Position;
    }
    if (strAttr == "NORMAL")
    {
        return GlTFPrimAttribute::Normal;
    }
    if (strAttr == "TANGENT")
    {
        return GlTFPrimAttribute::Tangent;
    }
    if (strAttr == "TEXCOORD_0")
    {
        return GlTFPrimAttribute::TexCoord0;
    }
    if (strAttr == "TEXCOORD_1")
    {
        return GlTFPrimAttribute::TexCoord1;
    }
    if (strAttr == "COLOR_0")
    {
        return GlTFPrimAttribute::Color;
    }
    if (strAttr == "JOINTS_0")
    {
        return GlTFPrimAttribute::Joint;
    }
    if (strAttr == "WEIGHTS_0")
    {
        return GlTFPrimAttribute::Weight;
    }

    return GlTFPrimAttribute::Unknown;
}

GlTFModelWrapper::GlTFCameraType GlTFModelWrapper::cameraTypeStrToType(const std::string& strType)
{
    if (strType == TINYGLTF_CAMERA_TYPE_PERSPECTIVE)
    {
        return GlTFCameraType::Perspective;
    }
    if (strType == TINYGLTF_CAMERA_TYPE_ORTHOGRAPHIC)
    {
        return GlTFCameraType::Orthographic;
    }

    return GlTFCameraType::Unknown;
}

std::shared_ptr<GlTFModelWrapper> GlTFModelWrapper::create()
{
    /*
     * Since only constructor is private and make_shared
     * does not have access to it, we create a proxy.
     * Based on: https://stackoverflow.com/a/25069711
     */

    struct SharedEnabler : public GlTFModelWrapper
    {
    };
    return std::make_shared<SharedEnabler>();
}

bool GlTFModelWrapper::profilingLoadImageWrapper(tinygltf::Image* image,
    int imageIdx, std::string* err, std::string* warn, 
    int reqWidth, int reqHeight, const unsigned char* bytes, 
    int size, void* ptr)
{
    PROF_SCOPE("glTF-LoadImageData");
    return tinygltf::LoadImageData(image, imageIdx, err, warn, reqWidth, reqHeight, bytes, size, ptr);
}

void GlTFModelWrapper::loadScenesFromFile(const res::File& sceneFile)
{
    std::string errMessages;
    std::string warnMessages;

    if (!sceneFile.exists())
    {
        throw GlTFLoader::SceneFileNotFoundException("GlTF scene file could not be loaded: File does not exist!");
    }

    mLoader.SetImageLoader(&GlTFModelWrapper::profilingLoadImageWrapper, nullptr);

    const auto format = formatFromStr(sceneFile.extension());
    switch (format)
    {
        case GlTFFileFormat::Ascii:
        { // Readable JSON file:
            log<Info>() << "Scene file is using ASCII format of glTF!" << std::endl;
            mLoader.LoadASCIIFromFile(&mModel, &errMessages, &warnMessages, sceneFile.file());
            break;
        }

        case GlTFFileFormat::Binary:
        { // Binary JSON file:
            log<Info>() << "Scene file is using binary format of glTF!" << std::endl;
            mLoader.LoadBinaryFromFile(&mModel, &errMessages, &warnMessages, sceneFile.file());
            break;
        }

        default:
        {
            log<Debug>() << "Unknown scene file extension!" << std::endl;
            throw GlTFLoader::SceneLoadException("GlTF scene file could not be loaded: Provided scene file has unknown extension!");
        }
    }

    if (!errMessages.empty())
    { // Loading failed.
        std::stringstream ss;
        ss << "GlTF scene file could not be loaded: Scene loading failed with: \n Err: \n" << errMessages << "\nWarn: \n" << warnMessages;
        throw GlTFLoader::SceneLoadException(ss.str().c_str());
    }

    if (!warnMessages.empty())
    { // TinyGlTF returned some warnings which are not critical.
        log<Warning>() << "Scene loading ended with warnings: \n" << warnMessages << std::endl;
    }
}

GlTFModelWrapper::GlTFFileFormat GlTFModelWrapper::formatFromStr(const std::string& ext)
{
    if (ext == GLTF_ASCII_FILE_EXT)
    {
        return GlTFFileFormat::Ascii;
    }
    if (ext == GLTF_BINARY_FILE_EXT)
    {
        return GlTFFileFormat::Binary;
    }

    return GlTFFileFormat::Unknown;
}

res::scene::SceneHandle GlTFModelWrapper::createScenes(res::scene::SceneMgr& mgr, const std::string& primaryName)
{
    // Create the primary scene, which will hold all of the sub-scenes.
    auto primarySceneHandle{mgr.create(primaryName)};
    res::scene::Scene& primaryScene{primarySceneHandle.get()};

    // Create the scene according to the scene model:
    createSceneResources(primaryScene);
    createSceneEntities(primaryScene);

    // Set transient data, which needs to be held until finalization.
    primaryScene.setTransientData(this->shared_from_this());

    // Return back the primary scene of this model.
    return primarySceneHandle;
}

void GlTFModelWrapper::createBuffer(res::scene::Scene& primaryScene, const tinygltf::Buffer& buffer)
{
    if (buffer.uri.empty())
    {
        throw GlTFLoader::SceneLoadException("GlTF scene file could not be loaded: Buffer has no data!");
    }

    const auto sizeInBytes{buffer.data.size() * sizeof(buffer.data[0u])};
    mBuffers.emplace_back(
        primaryScene.createBuffer(
            buffer.name, 
            buffer.data.data(), sizeInBytes));
}

void GlTFModelWrapper::createImage(res::scene::Scene& primaryScene, const tinygltf::Image& image)
{
    UNUSED(primaryScene);

    if (image.uri.empty())
    {
        throw GlTFLoader::SceneLoadException("GlTF scene file could not be loaded: Image has no data!");
    }
    if (image.component < 1)
    {
        throw GlTFLoader::SceneLoadException("GlTF scene file could not be loaded: Image has no pixel components!");
    }

    mImageInfo.emplace_back(&image);
}

void GlTFModelWrapper::createBufferView(res::scene::Scene& primaryScene, const tinygltf::Accessor& accessor)
{
    if (accessor.bufferView < 0)
    {
        throw GlTFLoader::SceneLoadException("GlTF scene file could not be loaded: Accessor has no buffer view!");
    }

    ASSERT_FAST(accessor.bufferView < mModel.bufferViews.size());
    const auto& bufferView{mModel.bufferViews[accessor.bufferView]};

    if (bufferView.buffer >= static_cast<int64_t>(mBuffers.size()))
    {
        throw GlTFLoader::SceneLoadException("GlTF scene file could not be loaded: Buffer view target buffer is out of bounds!");
    }

    const auto& targetBuffer{mModel.buffers[bufferView.buffer].data};
    if (targetBuffer.size() < accessor.byteOffset + bufferView.byteOffset + bufferView.byteLength)
    {
        throw GlTFLoader::SceneLoadException("GlTF scene file could not be loaded: Buffer view is referencing data out of bounds!");
    }

    mBufferViews.emplace_back(
        primaryScene.createBufferView(
            bufferView.name,
            mBuffers[bufferView.buffer],
            componentTypeToElementFormat(accessor.componentType, accessor.type),
            accessor.count,
            accessor.byteOffset + bufferView.byteOffset,
            bufferView.byteLength,
            accessor.ByteStride(bufferView)));
}

void GlTFModelWrapper::createSampler(res::scene::Scene& primaryScene, const tinygltf::Sampler& sampler)
{
    mSamplers.emplace_back(
        primaryScene.createSampler(
            sampler.name,
            minMagToFilter(sampler.minFilter, sampler.magFilter),
            wrapToEdgeFilters(sampler.wrapS, sampler.wrapT, sampler.wrapR)));
}

void GlTFModelWrapper::createDefaultSampler(res::scene::Scene& primaryScene)
{
    rndr::MinMagMipFilter filtering{ };
    filtering.minFilter = rndr::SampleFilter::Linear;
    filtering.magFilter = rndr::SampleFilter::Linear;
    filtering.mipFilter = rndr::SampleFilter::Linear;

    rndr::EdgeFilterUVW edgeFiltering{ };
    edgeFiltering.edgeFilterU = rndr::SampleEdge::Clamped;
    edgeFiltering.edgeFilterV = rndr::SampleEdge::Clamped;
    edgeFiltering.edgeFilterW = rndr::SampleEdge::Clamped;

    mSamplers.emplace_back(
        primaryScene.createSampler(
            DEFAULT_SAMPLER_NAME,
            filtering, edgeFiltering));
}

void GlTFModelWrapper::createTexture(res::scene::Scene& primaryScene, const tinygltf::Texture& texture)
{
    if (texture.source < 0)
    {
        throw GlTFLoader::SceneLoadException("GlTF scene file could not be loaded: Texture has no source!");
    }
    if (texture.source >= static_cast<int64_t>(mImageInfo.size()))
    {
        throw GlTFLoader::SceneLoadException("GlTF scene file could not be loaded: Texture source index is out of bounds!");
    }
    if (texture.sampler >= 0 && texture.sampler >= static_cast<int64_t>(mSamplers.size()) - 1)
    {
        throw GlTFLoader::SceneLoadException("GlTF scene file could not be loaded: Sampler source index is out of bounds!");
    }

    // Take provided sampler or the default one.
    const auto samplerIdx{texture.sampler >= 0 ? texture.sampler : mSamplers.size() - 1};
    // Index of the image source of this texture.
    const auto sourceIdx{texture.source};

    // Recover information about the source image.
    const auto& imageInfo{*mImageInfo[sourceIdx]};

    // Calculate size of the texture image.
    const auto sizeInBytes{imageInfo.image.size() * sizeof(imageInfo.image[0u])};
    // Choose a name - whichever is not empty...
    const auto textureName{texture.name.empty() ? (imageInfo.name.empty() ? imageInfo.uri : imageInfo.name) : texture.name};

    // Create, allocate and configure GPU texture.
    auto textureHandle{
        primaryScene.createTexture(
            textureName,
            imageInfo.image.data(), sizeInBytes,
            numComponentsToFormat(imageInfo.component),
            imageInfo.width, imageInfo.height, 
            0u) };
    textureHandle->sampler = mSamplers[samplerIdx];

    // Remember for later reference by glTF.
    mTextures.emplace_back(textureHandle);
}

void GlTFModelWrapper::createMaterial(res::scene::Scene& primaryScene, const tinygltf::Material& material)
{
    // Create the new material.
    auto matHandle{primaryScene.createMaterial(material.name)};
    // Get the instance, so we can set its values.
    auto& matVal{matHandle.get()};

    for (const auto& param : material.values)
    {
        processMaterialParameter(matVal, param);
    }

    // TODO - Add detection of other models.
    matVal.pbr.modelUsed = res::rndr::Material::PhysicallyBasedProperties::PbrModel::MetallicRoughness;

    for (const auto& param : material.additionalValues)
    { // Process common material parameters:
        processCommonMaterialParameter(matVal, param);
    }

    mMaterials.emplace_back(matHandle);
}

void GlTFModelWrapper::processMaterialParameter(res::rndr::Material& material, const std::pair<std::string, tinygltf::Parameter>& parameter)
{
    switch (matPbrStrKeyToKey(parameter.first))
    {
        case GlTFPbrMaterialKey::BaseColorFactor:
        {
            ASSERT_FAST(util::count(material.pbr.baseColorFactor) <= parameter.second.number_array.size());
            for (std::size_t iii = 0u; iii < util::count(material.pbr.baseColorFactor); ++iii)
            {
                material.pbr.baseColorFactor[iii] = static_cast<float>(parameter.second.number_array[iii]);
            }

            break;
        }
        case GlTFPbrMaterialKey::BaseColorTexture:
        {
            const auto textureIdx{parameter.second.TextureIndex()};

            ASSERT_FAST(textureIdx < mTextures.size());
            material.pbr.baseColorTexture = mTextures[textureIdx];

            break;
        }
        case GlTFPbrMaterialKey::MetallicFactor:
        {
            material.pbr.firstFactor.metallicFactor = static_cast<float>(parameter.second.Factor());

            break;
        }
        case GlTFPbrMaterialKey::RoughnessFactor:
        {
            material.pbr.secondFactor.roughnessFactor = static_cast<float>(parameter.second.Factor());

            break;
        }
        case GlTFPbrMaterialKey::MetallicRoughnessTexture:
        {
            const auto textureIdx{parameter.second.TextureIndex()};

            ASSERT_FAST(textureIdx < mTextures.size());
            material.pbr.propertyTexture = mTextures[textureIdx];

            break;
        }
        case GlTFPbrMaterialKey::Unknown:
        default:
        {
            log<Info>() << "Unknown GlTF material key: \"" << parameter.first << "\" !" << std::endl;
            break;
        }
    }
}

void GlTFModelWrapper::processCommonMaterialParameter(res::rndr::Material& material, const std::pair<std::string, tinygltf::Parameter>& parameter)
{
    switch (matStrKeyToKey(parameter.first))
    {
        case GlTFMaterialKey::NormalTexture:
        {
            const auto textureIdx{parameter.second.TextureIndex()};

            ASSERT_FAST(textureIdx < mTextures.size());
            material.normalTexture = mTextures[textureIdx];

            break;
        }
        case GlTFMaterialKey::OcclusionTexture:
        {
            const auto textureIdx{parameter.second.TextureIndex()};

            ASSERT_FAST(textureIdx < mTextures.size());
            material.occlusionTexture = mTextures[textureIdx];

            break;
        }
        case GlTFMaterialKey::EmissiveTexture:
        {
            const auto textureIdx{parameter.second.TextureIndex()};

            ASSERT_FAST(textureIdx < mTextures.size());
            material.emissiveTexture = mTextures[textureIdx];

            break;
        }
        case GlTFMaterialKey::EmissiveFactor:
        {
            ASSERT_FAST(util::count(material.emissiveFactor) <= parameter.second.number_array.size());
            for (std::size_t iii = 0u; iii < util::count(material.emissiveFactor); ++iii)
            {
                material.emissiveFactor[iii] = static_cast<float>(parameter.second.number_array[iii]);
            }

            break;
        }
        case GlTFMaterialKey::AlphaMode:
        {
            processMaterialAlphaMode(material, parameter);
            break;
        }
        case GlTFMaterialKey::AlphaCutoff:
        {
            material.alphaCutoff = static_cast<float>(parameter.second.number_value);

            break;
        }
        case GlTFMaterialKey::DoubleSided:
        {
            log<Info>() << "Skipping glTF material parameter for double-sidedness!" << std::endl;

            break;
        }
        case GlTFMaterialKey::Unknown:
        default:
        {
            log<Info>() << "Unknown GlTF material key: \"" << parameter.first << "\" !" << std::endl;
            break;
        }
    }
}

void GlTFModelWrapper::processMaterialAlphaMode(res::rndr::Material& material, const std::pair<std::string, tinygltf::Parameter>& parameter)
{
    switch (matAlphaStrToAlphaMode(parameter.second.string_value))
    {
        case GlTFMaterialAlphaMode::Opaque:
        {
            material.alphaMode = res::rndr::Material::AlphaMode::Opaque;

            break;
        }
        case GlTFMaterialAlphaMode::Mask:
        {
            material.alphaMode = res::rndr::Material::AlphaMode::Mask;

            break;
        }
        case GlTFMaterialAlphaMode::Blend:
        {
            material.alphaMode = res::rndr::Material::AlphaMode::Blend;

            break;
        }
        case GlTFMaterialAlphaMode::Unknown:
        default:
        {
            log<Info>() << "Unknown GlTF alpha mode: \"" << parameter.second.string_value << "\" !" << std::endl;

            break;
        }
    }
}

void GlTFModelWrapper::processPrimitiveAttribute(res::rndr::Primitive& primitive, const std::pair<std::string, int>& attribute)
{
    switch (primStrAttrToAttr(attribute.first))
    {
        case GlTFPrimAttribute::Position:
        {
            ASSERT_FAST(attribute.second < mBufferViews.size());

            primitive.setPositionBuffer(mBufferViews[attribute.second]);

            break;
        }
        case GlTFPrimAttribute::Normal:
        {
            ASSERT_FAST(attribute.second < mBufferViews.size());

            primitive.setNormalBuffer(mBufferViews[attribute.second]);

            break;
        }
        case GlTFPrimAttribute::Tangent:
        {
            ASSERT_FAST(attribute.second < mBufferViews.size());

            primitive.setTangentBuffer(mBufferViews[attribute.second]);

            break;
        }
        case GlTFPrimAttribute::TexCoord0:
        {
            ASSERT_FAST(attribute.second < mBufferViews.size());

            primitive.setTexCoordBuffer(mBufferViews[attribute.second]);

            break;
        }
        case GlTFPrimAttribute::TexCoord1:
        {
            log<Info>() << "Skipping glTF second texture coordinate attribute!" << std::endl;

            break;
        }
        case GlTFPrimAttribute::Color:
        {
            ASSERT_FAST(attribute.second < mBufferViews.size());

            primitive.setColorBuffer(mBufferViews[attribute.second]);

            break;
        }
        case GlTFPrimAttribute::Joint:
        {
            log<Info>() << "Skipping glTF joint attribute!" << std::endl;

            break;
        }
        case GlTFPrimAttribute::Weight:
        {
            log<Info>() << "Skipping glTF weight attribute!" << std::endl;

            break;
        }
        case GlTFPrimAttribute::Unknown:
        default:
        {
            log<Info>() << "Unknown GlTF attribute key: \"" << attribute.first << "\" !" << std::endl;
            break;
        }
    }
}

void GlTFModelWrapper::createMeshPrimitive(res::rndr::Mesh& mesh, const tinygltf::Primitive& primitive)
{
    res::rndr::Primitive meshPrimitive;

    for (const auto& attribute : primitive.attributes)
    { // Process primitive attributes:
        processPrimitiveAttribute(meshPrimitive, attribute);
    }

    if (primitive.indices >= 0)
    {
        ASSERT_FAST(primitive.indices < mBufferViews.size());
        meshPrimitive.indices = mBufferViews[primitive.indices];
    }

    if (primitive.material >= 0)
    {
        ASSERT_FAST(primitive.material < mMaterials.size());
        meshPrimitive.material = mMaterials[primitive.material];
    }

    switch (primitive.mode)
    {
        case TINYGLTF_MODE_POINTS:
        {
            meshPrimitive.primitiveMode = res::rndr::Primitive::PrimitiveMode::Points;

            break;
        }
        case TINYGLTF_MODE_LINE:
        {
            meshPrimitive.primitiveMode = res::rndr::Primitive::PrimitiveMode::Lines;

            break;
        }
        case TINYGLTF_MODE_LINE_LOOP:
        {
            meshPrimitive.primitiveMode = res::rndr::Primitive::PrimitiveMode::LineLoop;

            break;
        }
        case TINYGLTF_MODE_TRIANGLES:
        {
            meshPrimitive.primitiveMode = res::rndr::Primitive::PrimitiveMode::Triangles;

            break;
        }
        case TINYGLTF_MODE_TRIANGLE_STRIP:
        {
            meshPrimitive.primitiveMode = res::rndr::Primitive::PrimitiveMode::TriangleStrip;

            break;
        }
        case TINYGLTF_MODE_TRIANGLE_FAN:
        {
            meshPrimitive.primitiveMode = res::rndr::Primitive::PrimitiveMode::TriangleFan;

            break;
        }
        default:
        {
            log<Info>() << "Unknown glTF primitive mode!" << std::endl;

            break;
        }
    }

    mesh.primitives.emplace_back(meshPrimitive);
}

void GlTFModelWrapper::createMesh(res::scene::Scene& primaryScene, const tinygltf::Mesh& mesh)
{
    // Create the new mesh.
    auto meshHandle{primaryScene.createMesh(mesh.name)};
    // Get the instance, so we can set its values.
    auto& meshVal{meshHandle.get()};

    for (const auto& primitive : mesh.primitives)
    { // Create all of meshes primitives:
        createMeshPrimitive(meshVal, primitive);
    }

    mMeshes.emplace_back(meshHandle);
}

std::map<std::size_t, GlTFModelWrapper::ConversionRequest> GlTFModelWrapper::findRequiredConversions(const tinygltf::Model& model)
{
    // Vector to contain the requests.
    std::map<std::size_t, ConversionRequest> requests{};

    for (const auto& mesh : model.meshes)
    { // Accumulate attributes which need conversion.
        for (const auto& primitive : mesh.primitives)
        {
            if (primitive.indices >= 0)
            { // All valid index buffers should be uint32_t.
                ASSERT_FAST(primitive.indices < model.accessors.size());
                const auto& accessor{model.accessors[primitive.indices]};
                const auto elementFormat{componentTypeToElementFormat(accessor.componentType, accessor.type)};
                ASSERT_FAST(accessor.bufferView < model.bufferViews.size());
                if (elementFormat.format != rndr::ComponentFormat::UnsignedInt)
                { // All indices must be uint32_t.
                    requests.emplace(
                        accessor.bufferView,
                        ConversionRequest{
                            elementFormat, 
                            rndr::ElementFormat{ rndr::ComponentFormat::UnsignedInt, 1u }, 
                            TINYGLTF_COMPONENT_TYPE_UNSIGNED_INT, 
                            TINYGLTF_TYPE_SCALAR});
                }
            }
            for (const auto& attribute : primitive.attributes)
            {
                // Skip attributes with invalid buffer views.
                if (attribute.second < 0)
                {
                    continue;
                }

                switch (primStrAttrToAttr(attribute.first))
                {
                    case GlTFPrimAttribute::Position:
                    case GlTFPrimAttribute::Normal:
                    case GlTFPrimAttribute::Tangent:
                    { // Only float type is allowed.
                        break;
                    }
                    case GlTFPrimAttribute::TexCoord0:
                    { // Should always be float2
                        // TODO - Always convert to float2?
                        break;
                    }
                    case GlTFPrimAttribute::Color:
                    case GlTFPrimAttribute::TexCoord1:
                    case GlTFPrimAttribute::Joint:
                    case GlTFPrimAttribute::Weight:
                    case GlTFPrimAttribute::Unknown:
                    default:
                    { // Skip
                        break;
                    }
                }
            }
        }
    }

    return requests;
}

std::size_t GlTFModelWrapper::calculateConvertCopyBytes(const IndexedBufferView& view, const ConversionRequest* request)
{
    std::size_t result{0u};

    if (request)
    { // Conversion is requested -> size will be different.
        // TODO - Implement more as required?
        if (request->sourceFormat.format == rndr::ComponentFormat::UnsignedShort && 
            request->targetFormat.format == rndr::ComponentFormat::UnsignedInt)
        {
            result = (view.bufferView.byteLength / sizeof(uint16_t)) * sizeof(uint32_t);
        }
        else
        {
            throw GlTFLoader::SceneLoadException("Unknown conversion request: Not implemented!");
        }
    }
    else
    { // No conversion, use the old size.
        result = view.bufferView.byteLength;
    }

    return result;
}

void GlTFModelWrapper::convertCopyData(
    const tinygltf::Model& model, uint8_t* dst,
    const IndexedBufferView& view, std::size_t requiredBytes,
    const ConversionRequest* request)
{
    if (request)
    { // Conversion is requested -> conver-copy the data.
        // TODO - Implement more as required?
        if (request->sourceFormat.format == rndr::ComponentFormat::UnsignedShort && 
            request->targetFormat.format == rndr::ComponentFormat::UnsignedInt)
        {
            const auto* sourcePtr{reinterpret_cast<const uint16_t*>(model.buffers[view.bufferView.buffer].data.data() + view.bufferView.byteOffset)};
            auto* dstPtr{reinterpret_cast<uint32_t*>(dst)};
            const auto numElements{view.bufferView.byteLength / sizeof(uint16_t)};

            for (std::size_t iii = 0; iii < numElements; ++iii)
            {
                dstPtr[iii] = static_cast<uint32_t>(sourcePtr[iii]);
            }
        }
        else
        {
            throw GlTFLoader::SceneLoadException("Unknown conversion request: Not implemented!");
        }
    }
    else
    { // No conversion, just copy.
        const auto* src{model.buffers[view.bufferView.buffer].data.data() + view.bufferView.byteOffset};
        std::memcpy(dst, src, requiredBytes);
    }
}

const GlTFModelWrapper::ConversionRequest* GlTFModelWrapper::findConversionRequest(const IndexedBufferView& view,
                                                                                   const std::map<std::size_t, ConversionRequest>& requests)
{
    const auto findIt{requests.find(view.originalIndex)};
    return findIt == requests.end() ? nullptr : &findIt->second;
}

void GlTFModelWrapper::convertToSupportedTypes(tinygltf::Model& model)
{
    if (model.buffers.size() == 0u)
    {
        return;
    }

    // Which views should have its types converted?
    const auto& conversionRequests{findRequiredConversions(model)};

    std::vector<IndexedBufferView> sortedViews{};
    for (std::size_t iii = 0; iii < model.bufferViews.size(); ++iii)
    { // Accumulate the buffer views.
        sortedViews.emplace_back(IndexedBufferView{iii, model.bufferViews[iii]});
    }
    // Then sort them.
    std::sort(sortedViews.begin(),
              sortedViews.end(),
              [](auto& first, auto& second) { // Sort primarily by buffer index, secondarily by offset within that buffer.
                  return first.bufferView.buffer < second.bufferView.buffer ||
                         (first.bufferView.buffer == second.bufferView.buffer && first.bufferView.byteOffset < second.bufferView.byteOffset);
              });

    // Storage of the converted buffer data.
    std::vector<uint8_t> convertedData{};
    // Offset within the current output buffer.
    std::size_t bufferOffset{0u};
    // Index of the currently converted buffer.
    std::size_t currentBufferIndex{0u};
    // To what value should start of each view be aligned?
    static constexpr std::size_t VIEW_BYTE_ALIGNMENT{4 * sizeof(uint32_t)};
    // How many more bytes should we expect after conversion?
    static constexpr std::size_t BUFFER_OVER_ALLOCATE{1024u};
    // The new buffer will have at least the same size.
    convertedData.reserve(model.buffers[currentBufferIndex].data.size() + BUFFER_OVER_ALLOCATE);

    for (const auto& sortedView : sortedViews)
    { // Convert and align all the views.
        if (currentBufferIndex != static_cast<std::size_t>(sortedView.bufferView.buffer))
        { // Move to the next buffer.
            if (bufferOffset != 0u)
            { // Only replace if we actually copied something.
                model.buffers[currentBufferIndex].data = std::move(convertedData);
            }

            currentBufferIndex = static_cast<std::size_t>(sortedView.bufferView.buffer);
            bufferOffset = 0u;
            convertedData.clear();
            convertedData.reserve(model.buffers[currentBufferIndex].data.size() + BUFFER_OVER_ALLOCATE);
        }

        const auto conversionRequest{findConversionRequest(sortedView, conversionRequests)};

        // Align the start of the new view.
        const auto alignedOffset{util::math::alignTo(bufferOffset, VIEW_BYTE_ALIGNMENT)};
        // Predict how many bytes we will require.
        const auto requiredBytes{calculateConvertCopyBytes(sortedView, conversionRequest)};
        // Resize the output buffer accordingly.
        convertedData.resize(alignedOffset + requiredBytes);

        // Convert or just copy the data.
        convertCopyData(model, convertedData.data() + alignedOffset, sortedView, requiredBytes, conversionRequest);

        // Fix the buffer view and its accessor.
        auto& originalBufferView{model.bufferViews[sortedView.originalIndex]};
        originalBufferView.byteOffset = alignedOffset;
        originalBufferView.byteLength = requiredBytes;
        if (conversionRequest)
        {
            for (auto &originalAccessor : model.accessors)
            { // Fix all accessors using this buffer view.
                if (originalAccessor.bufferView != static_cast<int>(sortedView.originalIndex))
                { continue; }
                originalAccessor.componentType = conversionRequest->targetComponentType;
                originalAccessor.type = conversionRequest->targetType;
            }
        }

        bufferOffset = alignedOffset + requiredBytes;
    }

    if (bufferOffset != 0u)
    { // Only replace if we actually copied something.
        model.buffers[currentBufferIndex].data = std::move(convertedData);
    }
}

void GlTFModelWrapper::createSceneResources(res::scene::Scene& primaryScene)
{
    // Go through attribute buffers and convert to supported types.
    convertToSupportedTypes(mModel);

    // Create vertex data buffers.
    for (const auto& buffer : mModel.buffers)
    {
        createBuffer(primaryScene, buffer);
    }

    // Create texture data buffers.
    for (const auto& image : mModel.images)
    {
        createImage(primaryScene, image);
    }

    // Create buffer views.
    for (const auto& accessor : mModel.accessors)
    {
        createBufferView(primaryScene, accessor);
    }

    // Create texture samplers.
    for (const auto& sampler : mModel.samplers)
    {
        createSampler(primaryScene, sampler);
    }
    // Create default sampler for textures with undefined sampler.
    createDefaultSampler(primaryScene);

    // Create textures.
    for (const auto& texture : mModel.textures)
    {
        createTexture(primaryScene, texture);
    }

    // Create materials.
    for (const auto& material : mModel.materials)
    {
        createMaterial(primaryScene, material);
    }

    // Create meshes.
    for (const auto& mesh : mModel.meshes)
    {
        createMesh(primaryScene, mesh);
    }
}

void GlTFModelWrapper::initializeNodeEntityPerspCamera(const tinygltf::Camera& camera, comp::Camera& cameraComp)
{
    cameraComp.type = comp::Camera::CameraType::Perspective;
    cameraComp.firstParam.aspectRatio = static_cast<float>(camera.perspective.aspectRatio);
    cameraComp.secondParam.horFov = static_cast<float>(camera.perspective.yfov);
    cameraComp.zNear = static_cast<float>(camera.perspective.znear);
    cameraComp.zFar = static_cast<float>(camera.perspective.zfar);
}

void GlTFModelWrapper::initializeNodeEntityOrthoCamera(const tinygltf::Camera& camera, comp::Camera& cameraComp)
{
    cameraComp.type = comp::Camera::CameraType::Orthographic;
    cameraComp.firstParam.width = 2.0f * static_cast<float>(camera.orthographic.xmag);
    cameraComp.secondParam.height = 2.0f * static_cast<float>(camera.orthographic.ymag);
    cameraComp.zNear = static_cast<float>(camera.orthographic.znear);
    cameraComp.zFar = static_cast<float>(camera.orthographic.zfar);
}

void GlTFModelWrapper::initializeNodeEntityCamera(const tinygltf::Camera& camera, EntityHandle& entity)
{
    const auto cameraComp{entity.add<comp::Camera>()};
    ASSERT_FAST(cameraComp != nullptr);

    switch (cameraTypeStrToType(camera.type))
    {
        case GlTFCameraType::Perspective:
        {
            initializeNodeEntityPerspCamera(camera, *cameraComp);

            break;
        }
        case GlTFCameraType::Orthographic:
        {
            initializeNodeEntityOrthoCamera(camera, *cameraComp);

            break;
        }
        case GlTFCameraType::Unknown:
        default:
        {
            log<Info>() << "Unknown glTF camera type!" << std::endl;
            cameraComp->type = comp::Camera::CameraType::Unknown;

            break;
        }
    }
}

void GlTFModelWrapper::initializeNodeEntityMesh(const res::rndr::MeshHandle& mesh, EntityHandle& entity)
{
    const auto renderableComp{entity.add<comp::Renderable>()};
    ASSERT_FAST(renderableComp != nullptr);

    renderableComp->meshHandle = mesh;
}

void GlTFModelWrapper::initializeNodeEntityTransform(const tinygltf::Node& node, EntityHandle& entity)
{
    const auto transformComponent{entity.get<comp::Transform>()};
    ASSERT_FAST(transformComponent != nullptr);

    if (node.rotation.size() != 0u)
    { // We have a rotation.
        // 4 elements for a rotation quaternion.
        static constexpr std::size_t ROTATION_ELEMENTS{4u};
        ASSERT_FAST(node.rotation.size() == ROTATION_ELEMENTS);

        // Make a floating precision copy:
        float rotation[ROTATION_ELEMENTS];
        for (std::size_t iii = 0; iii < ROTATION_ELEMENTS; ++iii)
        {
            rotation[iii] = static_cast<float>(node.rotation[iii]);
        }

        transformComponent->localTransform.setRotate(dxtk::math::Quaternion(rotation));
        transformComponent->dirty = true;
    }

    if (node.scale.size() != 0u)
    { // We have a scale.
        // 3 elements for a scale.
        static constexpr std::size_t SCALE_ELEMENTS{3u};
        ASSERT_FAST(node.scale.size() == SCALE_ELEMENTS);

        // Make a floating precision copy:
        float scale[SCALE_ELEMENTS];
        for (std::size_t iii = 0; iii < SCALE_ELEMENTS; ++iii)
        {
            scale[iii] = static_cast<float>(node.scale[iii]);
        }

        transformComponent->localTransform.setScale(dxtk::math::Vector3(scale));
        transformComponent->dirty = true;
    }

    if (node.translation.size() != 0u)
    { // We have a translation.
        // 3 elements for a translation in 3D.
        static constexpr std::size_t TRANSLATION_ELEMENTS{3u};
        ASSERT_FAST(node.translation.size() == 3u);

        // Make a floating precision copy:
        float translation[TRANSLATION_ELEMENTS];
        for (std::size_t iii = 0; iii < TRANSLATION_ELEMENTS; ++iii)
        {
            translation[iii] = static_cast<float>(node.translation[iii]);
        }

        transformComponent->localTransform.setTranslate(dxtk::math::Vector3(translation));
        transformComponent->dirty = true;
    }

    if (node.matrix.size() != 0u)
    { // We have a complex transform matrix.
        // 16 elements for a 4x4 matrix.
        static constexpr std::size_t MATRIX_ELEMENTS{16u};
        ASSERT_FAST(node.matrix.size() == MATRIX_ELEMENTS);

        // Make a floating precision copy:
        float matrix[MATRIX_ELEMENTS];
        for (std::size_t iii = 0; iii < MATRIX_ELEMENTS; ++iii)
        {
            matrix[iii] = static_cast<float>(node.matrix[iii]);
        }

        dxtk::math::Matrix m;
        // Copy the column-major matrix.
        std::memcpy(m.m, matrix, 16u * sizeof(float));
        // Transpose to correctly match the row-major matix in DXTK.
        transformComponent->localTransform.setMatrix(m.Transpose());
        transformComponent->dirty = true;
    }
}

void GlTFModelWrapper::initializeNodeEntity(const tinygltf::Node& node, EntityHandle& entity)
{
    if (node.camera >= 0)
    {
        if (node.camera > static_cast<int64_t>(mModel.cameras.size()))
        {
            throw BaseSceneLoader::SceneLoadException("GlTF scene file could not be loaded: Node camera index is out of bounds!");
        }

        initializeNodeEntityCamera(mModel.cameras[node.camera], entity);
    }

    if (node.mesh >= 0)
    {
        if (node.mesh > static_cast<int64_t>(mMeshes.size()))
        {
            throw BaseSceneLoader::SceneLoadException("GlTF scene file could not be loaded: Node mesh index is out of bounds!");
        }

        initializeNodeEntityMesh(mMeshes[node.mesh], entity);
    }

    initializeNodeEntityTransform(node, entity);
}

void GlTFModelWrapper::createSubSceneEntities(res::scene::Scene& primaryScene, res::scene::SubSceneHandle& subScene, std::size_t rootNodeIdx)
{
    ASSERT_FAST(rootNodeIdx < mModel.nodes.size());

    // List of entities are yet to be initialized, but are already created.
    std::vector<std::pair<std::size_t, scene::EntityHandle>> entitiesToInitialize;

    // Initialize the sub-tree creation.
    const auto rootEntity{primaryScene.createEntity(subScene, mModel.nodes[rootNodeIdx].name)};
    entitiesToInitialize.emplace_back(std::make_pair(rootNodeIdx, rootEntity));

    while (!entitiesToInitialize.empty())
    { // Until the entity sub-tree is completely created.
        // Move to the next node:
        const auto currentNodeIdx{entitiesToInitialize.back().first};
        auto currentEntity{entitiesToInitialize.back().second};
        entitiesToInitialize.pop_back();
        const auto& currentNode{mModel.nodes[currentNodeIdx]};

        // Initialize the current node with its components.
        initializeNodeEntity(currentNode, currentEntity);

        // Create children:
        for (const auto& childNodeIdx : currentNode.children)
        {
            if (childNodeIdx < 0 || childNodeIdx >= static_cast<int64_t>(mModel.nodes.size()))
            {
                throw BaseSceneLoader::SceneLoadException("GlTF scene file could not be loaded: Child node index is out of bounds!");
            }

            const auto childNode{mModel.nodes[childNodeIdx]};
            const auto childEntity{primaryScene.createEntity(subScene, childNode.name)};

            // Form the scene-graph connections.
            primaryScene.setEntityAsParentOf(subScene, currentEntity, childEntity);

            // Stash the entity for later initialization and processing.
            entitiesToInitialize.emplace_back(std::make_pair(static_cast<std::size_t>(childNodeIdx), childEntity));
        }
    }
}

void GlTFModelWrapper::createDefaultCamera(res::scene::Scene& primaryScene, res::scene::SubSceneHandle& subScene)
{
    auto cameraEntity{primaryScene.createEntity(subScene, "DefaultCamera")};
    auto cameraComp{cameraEntity.add<comp::Camera>()};

    // Create a default perspective camera, without forcing too many parameters.
    cameraComp->type = comp::Camera::CameraType::Perspective;
    cameraComp->firstParam.aspectRatio = 0.0f;
    cameraComp->secondParam.horFov = 0.0f;
    cameraComp->zNear = 0.1f;
    cameraComp->zFar = 100.0f;
}

void GlTFModelWrapper::createSubScene(res::scene::Scene& primaryScene, const tinygltf::Scene& scene)
{
    // Create a new sub-scene for the glTF scene.
    auto subSceneHandle{primaryScene.createSubScene(scene.name)};

    for (const auto& node : scene.nodes)
    { // For every root node in the sub-scene.
        if (node < 0 || node >= static_cast<int64_t>(scene.nodes.size()))
        {
            throw BaseSceneLoader::SceneLoadException("GlTF scene file could not be loaded: Root node index is out of bounds!");
        }

        createSubSceneEntities(primaryScene, subSceneHandle, static_cast<std::size_t>(node));
    }

    // Create a default camera for looking through the scene.
    createDefaultCamera(primaryScene, subSceneHandle);
}

void GlTFModelWrapper::createSceneEntities(res::scene::Scene& primaryScene)
{
    // Create sub-scenes and their entities.
    for (const auto& scene : mModel.scenes)
    {
        createSubScene(primaryScene, scene);
    }

    // If there are no scenes, create a default one.
    if (primaryScene.numSubScenes() <= 0u)
    {
        auto defaultSubScene{primaryScene.createSubScene("Default")};
        // Create a default camera for looking through the scene.
        createDefaultCamera(primaryScene, defaultSubScene);
    }

    // Default scene is either the specified one, or the first one.
    const std::size_t defaultSceneIdx{mModel.defaultScene >= 0 ? mModel.defaultScene : 0u};
    // Specify which sub-scene should be active at the start.
    primaryScene.setCurrentSubScene(primaryScene.getSubScene(defaultSceneIdx));
}

rndr::ElementFormat GlTFModelWrapper::numComponentsToFormat(std::size_t numComponents)
{
    rndr::ElementFormat elementFormat{ };

    // Since we are using stb::image, we will always get 8bit values.
    elementFormat.format = rndr::ComponentFormat::UnsignedNormByte;
    elementFormat.numComponents = static_cast<uint16_t>(numComponents);

    return elementFormat;
}

rndr::MinMagMipFilter GlTFModelWrapper::minMagToFilter(int minFilter, int magFilter)
{
    rndr::MinMagMipFilter edgeFiltering{ };

    const auto minFilterNearest{ 
        minFilter == TINYGLTF_TEXTURE_FILTER_NEAREST || 
        minFilter == TINYGLTF_TEXTURE_FILTER_NEAREST_MIPMAP_LINEAR || 
        minFilter == TINYGLTF_TEXTURE_FILTER_NEAREST_MIPMAP_NEAREST};
    edgeFiltering.minFilter = minFilterNearest ? rndr::SampleFilter::Nearest : rndr::SampleFilter::Linear;

    const auto mipFilterNearest{
        minFilter == TINYGLTF_TEXTURE_FILTER_NEAREST_MIPMAP_NEAREST || 
        minFilter == TINYGLTF_TEXTURE_FILTER_LINEAR_MIPMAP_NEAREST};
    edgeFiltering.mipFilter = mipFilterNearest ? rndr::SampleFilter::Nearest : rndr::SampleFilter::Linear;

    const auto magFilterNearest{magFilter == TINYGLTF_TEXTURE_FILTER_NEAREST};
    edgeFiltering.magFilter = magFilterNearest ? rndr::SampleFilter::Nearest : rndr::SampleFilter::Linear;

    return edgeFiltering;
}

rndr::SampleEdge GlTFModelWrapper::wrapToEdgeFilter(int wrap)
{
    switch (wrap)
    {
        case TINYGLTF_TEXTURE_WRAP_CLAMP_TO_EDGE:
            { return rndr::SampleEdge::Clamped; }
        case TINYGLTF_TEXTURE_WRAP_MIRRORED_REPEAT:
            { return rndr::SampleEdge::Mirrored; }
        case TINYGLTF_TEXTURE_WRAP_REPEAT:
        default:
            { return rndr::SampleEdge::Repeat; }
    }
}

rndr::EdgeFilterUVW GlTFModelWrapper::wrapToEdgeFilters(int wrapS, int wrapT, int wrapR)
{
    return {
        wrapToEdgeFilter(wrapS), 
        wrapToEdgeFilter(wrapT), 
        wrapToEdgeFilter(wrapR)
    };
}

rndr::ElementFormat GlTFModelWrapper::componentTypeToElementFormat(int componentType, int elementClass)
{
    rndr::ElementFormat elementFormat{ };

    switch (elementClass)
    {
        case TINYGLTF_TYPE_SCALAR:
        {
            elementFormat.numComponents = 1u;
            break;
        }
        case TINYGLTF_TYPE_VEC2:
        {
            elementFormat.numComponents = 2u;
            break;
        }
        case TINYGLTF_TYPE_VEC3:
        {
            elementFormat.numComponents = 3u;
            break;
        }
        case TINYGLTF_TYPE_VEC4:
        {
            elementFormat.numComponents = 4u;
            break;
        }
        default:
        {
            elementFormat.numComponents = 0u;
            break;
        }
    }

    switch (componentType)
    {
        case TINYGLTF_COMPONENT_TYPE_BYTE:
        {
            elementFormat.format = rndr::ComponentFormat::SignedByte;
            break;
        }
        case TINYGLTF_COMPONENT_TYPE_FLOAT:
        {
            elementFormat.format = rndr::ComponentFormat::Float32;
            break;
        }
        case TINYGLTF_COMPONENT_TYPE_SHORT:
        {
            elementFormat.format = rndr::ComponentFormat::SignedShort;
            break;
        }
        case TINYGLTF_COMPONENT_TYPE_UNSIGNED_BYTE:
        {
            elementFormat.format = rndr::ComponentFormat::UnsignedByte;
            break;
        }
        case TINYGLTF_COMPONENT_TYPE_UNSIGNED_INT:
        {
            elementFormat.format = rndr::ComponentFormat::UnsignedInt;
            break;
        }
        case TINYGLTF_COMPONENT_TYPE_UNSIGNED_SHORT:
        {
            elementFormat.format = rndr::ComponentFormat::UnsignedShort;
            break;
        }
        default:
        {
            elementFormat.format = rndr::ComponentFormat::Unknown;
            break;
        }
    }

    return elementFormat;
}

res::scene::SceneHandle GlTFLoader::loadFromFile(const res::File& sceneFile, res::scene::SceneMgr& mgr)
{
    // Make model wrapper, so we can later pass it to the scene as transient data.
    auto modelWrapper{GlTFModelWrapper::create()};

    {
        PROF_SCOPE("glTF-LoadScene");
        // Load scene data from user provided file.
        modelWrapper->loadScenesFromFile(sceneFile);
    }

    {
        PROF_SCOPE("glTF-CreateScene");
        // Parse scene data and transfer it into the internal representation.
        return modelWrapper->createScenes(mgr, sceneFile.filename());
    }
}

} // namespace loaders

} // namespace scene

} // namespace quark

// Implementation begin end.
