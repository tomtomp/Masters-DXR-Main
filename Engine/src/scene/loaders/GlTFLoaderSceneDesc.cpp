/**
 * @file scene/loaders/GlTFLoaderSceneDesc.cpp
 * @author Tomas Polasek
 * @brief Wrapper around tinyglTF library, used for loading glTF files.
 * This file contains definition of the method loadFromFileToSceneDesc.
 */

#include "stdafx.h"

#include "engine/scene/loaders/GlTFLoader.h"

#include "engine/renderer/ElementFormat.h"
#include "engine/lib/tinygltf.h"
#include "engine/util/prof/Profiler.h"

namespace quark {

namespace scene {

namespace loaders {

/**
 * Wrapper used for passing glTF data.
 * Instances must be created using make_shared!
 */
class GlTFModelWrapperSceneDesc
{
public:
    GlTFModelWrapperSceneDesc() = default;
    ~GlTFModelWrapperSceneDesc() = default;

    // No copying or moving.
    GlTFModelWrapperSceneDesc(const GlTFModelWrapperSceneDesc &other) = delete;
    GlTFModelWrapperSceneDesc &operator=(const GlTFModelWrapperSceneDesc &other) = delete;
    GlTFModelWrapperSceneDesc(GlTFModelWrapperSceneDesc &&other) = delete;
    GlTFModelWrapperSceneDesc &operator=(GlTFModelWrapperSceneDesc &&other) = delete;

    /**
     * Load glTF scenes from provided file.
     */
    void loadScenesFromFile(const res::File &sceneFile);

    /**
     * Create scene description from loaded data.
     * @param primaryName Name of the primary scene.
     * @return Returns pointer to the scene description.
     */
    scene::SceneDescription::PtrT createSceneDesc(const std::string &primaryName);
private:
    /// glTF extension used for ASCII file format.
    static constexpr const char *GLTF_ASCII_FILE_EXT{ "gltf" };
    /// glTF extension used for binary file format.
    static constexpr const char *GLTF_BINARY_FILE_EXT{ "glb" };

    /// Name of the special material extension.
    static constexpr const char *DXR_MATERIALS_SPEC_EXT_NAME{ "DXR_materials_spec" };

    /// Name of the extension property which specifies whether the material creates specular reflections.
    static constexpr const char *DXR_MATERIALS_SPEC_EXT_REFLECTIONS_NAME{ "specularReflections" };
    /// Name of the extension property which specifies whether the material creates specular refractions.
    static constexpr const char *DXR_MATERIALS_SPEC_EXT_REFRACTIONS_NAME{ "specularRefractions" };

    // Material key values:
    /// Standard normal texture.
    static constexpr const char *GLTF_MAT_NORMAL_TEXTURE{"normalTexture"};
    /// Occlusion baking texture.
    static constexpr const char *GLTF_MAT_OCCLUSION_TEXTURE{"occlusionTexture"};
    /// Light emission texture.
    static constexpr const char *GLTF_MAT_EMISSIVE_TEXTURE{"emissiveTexture"};
    /// Light emission factor.
    static constexpr const char *GLTF_MAT_EMISSIVE_FACTOR{"emissiveFactor"};
    /// Alpha rendering mode.
    static constexpr const char *GLTF_MAT_ALPHA_MODE{"alphaMode"};
    /// Alpha cutoff for alpha masking.
    static constexpr const char *GLTF_MAT_ALPHA_CUTOFF{"alphaCutoff"};
    /// Is the material visible from both sides?
    static constexpr const char *GLTF_MAT_DOUBLE_SIDED{"doubleSided"};

    /// Enumeration of possible keys found in the material definition.
    enum class GlTFMaterialKey
    {
        NormalTexture,
        OcclusionTexture,
        EmissiveTexture,
        EmissiveFactor,
        AlphaMode,
        AlphaCutoff,
        DoubleSided,
        Unknown
    }; // enum class GlTFMaterialKey

    /**
     * Convert string representation of the key found in glTF material
     * definition.
     * @param strKey String representation of the key.
     * @return Returns corresponding enum value.
     */
    static GlTFMaterialKey matStrKeyToKey(const std::string &strKey);

    /// Fully opaque material.
    static constexpr const char *GLTF_MAT_ALPHA_MODE_OPAQUE{"OPAQUE"};
    /// Binary transparency, based on alpha cutoff..
    static constexpr const char *GLTF_MAT_ALPHA_MODE_MASK{"MASK"};
    /// Standard alpha blending.
    static constexpr const char *GLTF_MAT_ALPHA_MODE_BLEND{"BLEND"};

    /// Enumeration of possible alpha modes.
    enum class GlTFMaterialAlphaMode
    {
        Opaque,
        Mask,
        Blend,
        Unknown
    }; // enum class GlTFMaterialAlphaMode

    /**
     * Convert string representation of the alpha mode found in glTF material
     * definition.
     * @param modeStr String representation of alpha mode.
     * @return Returns corresponding enum value.
     */
    static GlTFMaterialAlphaMode matAlphaStrToAlphaMode(const std::string &modeStr);

    // Material PBR key values:
    /// Diffuse color factor.
    static constexpr const char *GLTF_MAT_PBR_BASE_COLOR_FACTOR{"baseColorFactor"};
    /// Diffuse color texture.
    static constexpr const char *GLTF_MAT_PBR_BASE_COLOR_TEXTURE{"baseColorTexture"};
    /// Metallicity of the material.
    static constexpr const char *GLTF_MAT_PBR_METALLIC_FACTOR{"metallicFactor"};
    /// Roughness of the material.
    static constexpr const char *GLTF_MAT_PBR_ROUGHNESS_FACTOR{"roughnessFactor"};
    /// Metallicity-Roughness texture.
    static constexpr const char *GLTF_MAT_PBR_MET_ROUGH_TEXTURE{"metallicRoughnessTexture"};

    /// Enumeration of possible keys found in PBR part of the material definition.
    enum class GlTFPbrMaterialKey
    {
        BaseColorFactor,
        BaseColorTexture,
        MetallicFactor,
        RoughnessFactor,
        MetallicRoughnessTexture,
        Unknown
    }; // enum class GlTFPbrMaterialKey

    /**
     * Convert string representation of the key found in glTF PBR material
     * definition.
     * @param strKey String representation of the key.
     * @return Returns corresponding enum value.
     */
    static GlTFPbrMaterialKey matPbrStrKeyToKey(const std::string &strKey);

    /// Primitive attribute containing position vertex data.
    static constexpr const char *GLTF_PRIMITIVE_ATTRIBUTE_POSITION{"POSITION"};
    /// Primitive attribute containing normal vertex data.
    static constexpr const char *GLTF_PRIMITIVE_ATTRIBUTE_NORMAL{"NORMAL"};
    /// Primitive attribute containing tangent vertex data.
    static constexpr const char *GLTF_PRIMITIVE_ATTRIBUTE_TANGENT{"TANGENT"};
    /// Primitive attribute containing first texture coordinate data.
    static constexpr const char *GLTF_PRIMITIVE_ATTRIBUTE_TEXCOORD0{"TEXCOORD_0"};
    /// Primitive attribute containing second texture coordinate data.
    static constexpr const char *GLTF_PRIMITIVE_ATTRIBUTE_TEXCOORD1{"TEXCOORD_1"};
    /// Primitive attribute containing color data.
    static constexpr const char *GLTF_PRIMITIVE_ATTRIBUTE_COLOR{"COLOR_0"};
    /// Primitive attribute containing joint data.
    static constexpr const char *GLTF_PRIMITIVE_ATTRIBUTE_JOINT{"JOINTS_0"};
    /// Primitive attribute containing joint weight data.
    static constexpr const char *GLTF_PRIMITIVE_ATTRIBUTE_WEIGHT{"WEIGHTS_0"};

    /// Attributes found in glTF primitive specification.
    enum class GlTFPrimAttribute
    {
        /// Vec3, float.
        Position,
        /// Vec3, float.
        Normal,
        /// Vec4, float.
        Tangent,
        /// Vec2, float/unorm8/unorm16.
        TexCoord0,
        /// Vec2, float/unorm8/unorm16.
        TexCoord1,
        /// Vec2, float/unorm8/unorm16.
        Color,
        /// Vec2, /uint8/uint16.
        Joint,
        /// Vec2, float/unorm8/unorm16.
        Weight,
        Unknown
    }; // enum class GlTFPrimAttribute

    /**
     * Convert string representation of the attribute found
     * in glTF primitive definition.
     * @param strKey String representation of the attribute.
     * @return Returns corresponding enum value.
     */
    static GlTFPrimAttribute primStrAttrToAttr(const std::string &strAttr);

    /// Camera type string representing the perspective camera.
    static constexpr const char *TINYGLTF_CAMERA_TYPE_PERSPECTIVE{"perspective"};
    /// Camera type string representing the orthographic camera.
    static constexpr const char *TINYGLTF_CAMERA_TYPE_ORTHOGRAPHIC{"orthographic"};

    /// glTF camera types.
    enum class GlTFCameraType
    {
        Perspective,
        Orthographic,
        Unknown
    }; // enum class GlTFCameraType

    /**
     * Convert string representation of the attribute found
     * in glTF primitive definition.
     * @param strType String representation of the camera type.
     * @return Returns corresponding enum value.
     */
    static GlTFCameraType cameraTypeStrToType(const std::string &strType);

    /// Name of the default sampler, used for textures without sampler specified.
    static constexpr const char *DEFAULT_SAMPLER_NAME{"DefaultSampler"};

    /// Listing of file formats used by glTF.
    enum class GlTFFileFormat
    {
        Ascii,
        Binary,
        Unknown
    }; // enum class GlTFFileFormat

    /**
     * Get glTF file format from extension string.
     * @param ext Extension of the file.
     * @return Returns glTF file format.
     */
    static GlTFFileFormat formatFromStr(const std::string &ext);

    /// Helper method for profiling TinyGLTF image loading.
    static bool profilingLoadImageWrapper(tinygltf::Image* image,
        int imageIdx, std::string* err, std::string* warn,
        int reqWidth, int reqHeight, const unsigned char* bytes,
        int size, void* ptr);

    /**
     * Create buffer according to the glTF original.
     * @param sceneDesc Description of the scene
     * @param buffer Original glTF buffer definition.
     */
    void createBuffer(scene::SceneDescription::PtrT sceneDesc, const tinygltf::Buffer &buffer);

    /**
     * Create image according to the glTF original.
     * @param sceneDesc Description of the scene
     * @param image Original glTF image definition.
     */
    void createImage(scene::SceneDescription::PtrT sceneDesc, const tinygltf::Image &image);

    /**
     * Create buffer view according to the glTF original.
     * @param sceneDesc Description of the scene
     * @param accessor Original glTF buffer view definition.
     */
    void createBufferView(scene::SceneDescription::PtrT sceneDesc, const tinygltf::Accessor &accessor);

    /**
     * Create sampler according to the glTF original.
     * @param sceneDesc Description of the scene
     * @param sampler Original glTF sampler definition.
     */
    void createSampler(scene::SceneDescription::PtrT sceneDesc, const tinygltf::Sampler &sampler);

    /**
     * Create the default sampler used by glTF texture
     * without specified sampler.
     * @param sceneDesc Description of the scene
     */
    void createDefaultSampler(scene::SceneDescription::PtrT sceneDesc);

    /**
     * Create texture according to the glTF original.
     * @param sceneDesc Scene using the texture.
     * @param texture Original glTF texture definition.
     */
    void createTexture(scene::SceneDescription::PtrT sceneDesc, const tinygltf::Texture &texture);

    /**
     * Create material according to the glTF original.
     * @param sceneDesc Description of the scene
     * @param material Original glTF material definition.
     */
    void createMaterial(scene::SceneDescription::PtrT sceneDesc, const tinygltf::Material &material);

    /**
     * Process a single material parameter and set its
     * value into given material.
     * @param materialDesc Change values of this material
     * according to the parameter
     * @param parameter Parameter of the material gained
     * from glTF.
     */
    void processMaterialParameter(scene::desc::Material &materialDesc, const std::pair<std::string, tinygltf::Parameter> &parameter);

    /**
     * Perform parsing of material alpha mode parameter.
     * @param materialDesc Target material.
     * @param parameter Alpha-mode parameter.
     */
    void processMaterialAlphaMode(scene::desc::Material &materialDesc, const std::pair<std::string, tinygltf::Parameter> &parameter);

    /**
     * Process a single material parameter and set its
     * value into given material.
     * This method should be used for material parameters
     * common for all materials.
     * @param materialDesc Change values of this material
     * according to the parameter
     * @param parameter Parameter of the material gained
     * from glTF.
     */
    void processCommonMaterialParameter(scene::desc::Material &materialDesc, const std::pair<std::string, tinygltf::Parameter> &parameter);

    /**
     * Process the material specification extension.
     * @param materialDesc Output material description.
     * @param extensions Extensions map.
     */
    void processMaterialSpecs(scene::desc::Material &materialDesc, const tinygltf::ExtensionMap &extensions);

    /**
     * Process a single primitive attribute and set its
     * value into given primitive
     * @param primitiveDesc Target primitive.
     * @param attribute Primitive attribute to process.
     */
    void processPrimitiveAttribute(scene::desc::Primitive &primitiveDesc, const std::pair<std::string, int> &attribute);

    /**
     * Create primitive according to the glTF original.
     * @param sceneDesc Description of the scene
     * @param meshDesc Mesh which contains the primitive.
     * @param primitive Original glTF primitive definition.
     */
    void createMeshPrimitive(scene::SceneDescription::PtrT sceneDesc, scene::desc::Mesh &meshDesc, const tinygltf::Primitive &primitive);

    /**
     * Create mesh according to the glTF original.
     * @param sceneDesc Scene using the mesh.
     * @param mesh Original glTF mesh definition.
     */
    void createMesh(scene::SceneDescription::PtrT sceneDesc, const tinygltf::Mesh &mesh);

    /// Helper record for storing information about required conversions.
    struct ConversionRequest
    {
        /// Convert from this type.
        rndr::ElementFormat sourceFormat{ };
        /// Convert to this type.
        rndr::ElementFormat targetFormat{ };
        /// Target GlTF component type.
        int targetComponentType;
        /// Target GlTF type.
        int targetType;
    }; // struct ConversionRequest

    /**
     * Find a list of required conversions.
     * @return Returns a map from buffer view index to conversion
     * request structure.
     */
    std::map<std::size_t, ConversionRequest> findRequiredConversions(const tinygltf::Model &model);

    /// Helper structure, associating buffer view with its index.
    struct IndexedBufferView
    {
        /// Original index of the buffer view.
        std::size_t originalIndex{0u};
        /// Content of the structure.
        tinygltf::BufferView bufferView{};
    }; // struct IndexedBufferView

    /**
     * Predict how many bytes will be required to perform the copy
     * or conversion of buffer view data.
     * @param view Source buffer view.
     * @param request Conversion request, may be nullptr.
     */
    std::size_t calculateConvertCopyBytes(const IndexedBufferView &view, const ConversionRequest *request);

    /**
     * Convert or just copy data specified by the view into
     * the output buffer.
     * @param model Loaded model.
     * @param dst Destination of the copy.
     * @param view Source buffer view.
     * @param requiredBytes Number of bytes required.
     * @param request Conversion request, may be nullptr.
     */
    void convertCopyData(const tinygltf::Model &model, uint8_t *dst, const IndexedBufferView &view, 
        std::size_t requiredBytes, const ConversionRequest *request); 
    /// Find conversion request for given buffer view.
    const ConversionRequest *findConversionRequest(const IndexedBufferView &view, const std::map<std::size_t, ConversionRequest> &requests);

    /**
     * Go through all attribute buffers and index
     * buffers and convert them to application supported
     * types.
     * @param model Loaded model.
     */
    void convertToSupportedTypes(tinygltf::Model &model);

    /**
     * Create resources required by all of the sub-scenes.
     * @param sceneDesc Primary scene holder.
     */
    void createSceneResources(scene::SceneDescription::PtrT sceneDesc);

    /**
     * Initialize perspective camera from glTF camera
     * specification.
     * @param camera Original glTF camera.
     * @param cameraDesc Target camera description.
     */
    void initializeNodeEntityPerspCamera(const tinygltf::Camera &camera, scene::desc::Camera &cameraDesc);

    /**
     * Initialize perspective camera from glTF camera
     * specification.
     * @param camera Original glTF camera.
     * @param cameraDesc Target camera descriptions.
     */
    void initializeNodeEntityOrthoCamera(const tinygltf::Camera &camera, scene::desc::Camera &cameraDesc);

    /**
     * Initialize entity's camera component according to the
     * glTF camera properties.
     * @param camera Original glTF camera.
     * @param cameraDesc Target camera descriptions.
     */
    void initializeNodeEntityCamera(const tinygltf::Camera &camera, scene::desc::Camera &cameraDesc);

    /**
     * Initialize entity's rendering component according to the
     * glTF mesh properties.
     * @param mesh Handle to the mesh.
     * @param entityDesc The target entity.
     */
    void initializeNodeEntityMesh(scene::desc::MeshHandle meshHandle, scene::desc::Entity &entityDesc);

    /**
     * Initialize entity's transform component according to the
     * glTF node specification.
     * @param node Original glTF node.
     * @param entityDesc The target entity.
     */
    void initializeNodeEntityTransform(const tinygltf::Node &node, scene::desc::Entity &entityDesc);

    /**
     * Initialize entity according to the glTF node specification.
     * @param sceneDesc Primary scene holder.
     * @param node Original glTF node.
     * @param entityDesc The target entity.
     */
    void initializeNodeEntity(scene::SceneDescription::PtrT sceneDesc, const tinygltf::Node &node, scene::desc::Entity &entityDesc);

    /**
     * Add sub-scene entities starting with the root node
     * to provided sub-scene.
     * @param sceneDesc Primary scene holder.
     * @param rootNodeIdx Index of the root node within the glTF
     * nodes list.
     */
    void createSubSceneEntities(scene::SceneDescription::PtrT sceneDesc, std::size_t rootNodeIdx);

    /**
     * Create a default camera, which can be used to peer into the
     * scene.
     * @param sceneDesc Primary scene holder.
     */
    void createDefaultCamera(scene::SceneDescription::PtrT sceneDesc);

    /**
     * Create sub-scene according to the glTF original.
     * @param sceneDesc Primary scene holder.
     * @param scene Original glTF scene definition.
     * @return Returns handle to the sub-scene.
     */
    scene::desc::SubSceneHandle createSubScene(scene::SceneDescription::PtrT sceneDesc, const tinygltf::Scene &scene);

    /**
     * Create entities within the sub-scenes.
     * @param sceneDesc Primary scene holder.
     */
    void createSceneEntities(scene::SceneDescription::PtrT sceneDesc);

    /**
     * Get format for a given number of components.
     * @param numComponents Number of components for each pixel.
     * @return Returns pixel format.
     */
    rndr::ElementFormat numComponentsToFormat(std::size_t numComponents);

    /**
     * Convert minification and magnification filtering options
     * into one filter value.
     * @param minFilter Minification glTF filtering option.
     * @param magFilter Magnification glTF filtering option.
     * @return Returns combination of min and mag into one value.
     */
    rndr::MinMagMipFilter minMagToFilter(int minFilter, int magFilter);

    /**
     * Convert glTF wrap mode into texture addressing mode.
     * @param wrapS Wrapping mode.
     * @return Returns texture addressing mode.
     */
    rndr::SampleEdge wrapToEdgeFilter(int wrap);

    /**
     * Convert glTF wrap mode into texture addressing mode.
     * @param wrapS Wrapping mode in first coordinate.
     * @param wrapT Wrapping mode in second coordinate.
     * @param wrapR Wrapping mode in third coordinate.
     * @return Returns texture addressing mode.
     */
    rndr::EdgeFilterUVW wrapToEdgeFilters(int wrapS, int wrapT, int wrapR);

    /**
     * Convert component type and type into element format.
     * @param componentType Type of a single component.
     * @param elementClass Class of a single element.
     * @return Returns the element format.
     */
    rndr::ElementFormat componentTypeToElementFormat(int componentType, int elementClass);

    /// GlTF file loader.
    tinygltf::TinyGLTF mLoader;
    /// Model of the loaded glTF scene.
    tinygltf::Model mModel;

    /// Buffer resources created for the primary scene.
    std::vector<scene::desc::BufferHandle> mBuffers;
    /// Information about image buffers.
    std::vector<const tinygltf::Image*> mImageInfo;
    /// Buffer view resources created for the primary scene.
    std::vector<scene::desc::BufferViewHandle> mBufferViews;
    /// Sampler resources created for the primary scene.
    std::vector<scene::desc::SamplerHandle> mSamplers;
    /// Texture resources created for the primary scene.
    std::vector<scene::desc::TextureHandle> mTextures;
    /// Material resources created for the primary scene.
    std::vector<scene::desc::MaterialHandle> mMaterials;
    /// Mesh resources created for the primary scene.
    std::vector<scene::desc::MeshHandle> mMeshes;
protected:
}; // class GlTFModelWrapperSceneDesc

} // namespace loaders

} // namespace scene

} // namespace quark

// Implementation begin.

namespace quark {

namespace scene {

namespace loaders {

GlTFModelWrapperSceneDesc::GlTFMaterialKey GlTFModelWrapperSceneDesc::matStrKeyToKey(const std::string &strKey)
{
    if (strKey == GLTF_MAT_NORMAL_TEXTURE)
    { return GlTFMaterialKey::NormalTexture; }
    if (strKey == GLTF_MAT_OCCLUSION_TEXTURE)
    { return GlTFMaterialKey::OcclusionTexture; }
    if (strKey == GLTF_MAT_EMISSIVE_TEXTURE)
    { return GlTFMaterialKey::EmissiveTexture; }
    if (strKey == GLTF_MAT_EMISSIVE_FACTOR)
    { return GlTFMaterialKey::EmissiveFactor; }
    if (strKey == GLTF_MAT_ALPHA_MODE)
    { return GlTFMaterialKey::AlphaMode; }
    if (strKey == GLTF_MAT_ALPHA_CUTOFF)
    { return GlTFMaterialKey::AlphaCutoff; }
    if (strKey == GLTF_MAT_DOUBLE_SIDED)
    { return GlTFMaterialKey::DoubleSided; }

    return GlTFMaterialKey::Unknown;
}

GlTFModelWrapperSceneDesc::GlTFMaterialAlphaMode GlTFModelWrapperSceneDesc::matAlphaStrToAlphaMode(const std::string &modeStr)
{
    if (modeStr == GLTF_MAT_ALPHA_MODE_OPAQUE)
    { return GlTFMaterialAlphaMode::Opaque; }
    if (modeStr == GLTF_MAT_ALPHA_MODE_MASK)
    { return GlTFMaterialAlphaMode::Mask; }
    if (modeStr == GLTF_MAT_ALPHA_MODE_BLEND)
    { return GlTFMaterialAlphaMode::Blend; }

    return GlTFMaterialAlphaMode::Unknown;
}

GlTFModelWrapperSceneDesc::GlTFPbrMaterialKey GlTFModelWrapperSceneDesc::matPbrStrKeyToKey(const std::string &strKey)
{
    if (strKey == GLTF_MAT_PBR_BASE_COLOR_FACTOR)
    { return GlTFPbrMaterialKey::BaseColorFactor; }
    if (strKey == GLTF_MAT_PBR_BASE_COLOR_TEXTURE)
    { return GlTFPbrMaterialKey::BaseColorTexture; }
    if (strKey == GLTF_MAT_PBR_METALLIC_FACTOR)
    { return GlTFPbrMaterialKey::MetallicFactor; }
    if (strKey == GLTF_MAT_PBR_ROUGHNESS_FACTOR)
    { return GlTFPbrMaterialKey::RoughnessFactor; }
    if (strKey == GLTF_MAT_PBR_MET_ROUGH_TEXTURE)
    { return GlTFPbrMaterialKey::MetallicRoughnessTexture; }

    return GlTFPbrMaterialKey::Unknown;
}

GlTFModelWrapperSceneDesc::GlTFPrimAttribute GlTFModelWrapperSceneDesc::primStrAttrToAttr(const std::string &strAttr)
{
    if (strAttr == "POSITION")
    { return GlTFPrimAttribute::Position; }
    if (strAttr == "NORMAL")
    { return GlTFPrimAttribute::Normal; }
    if (strAttr == "TANGENT")
    { return GlTFPrimAttribute::Tangent; }
    if (strAttr == "TEXCOORD_0")
    { return GlTFPrimAttribute::TexCoord0; }
    if (strAttr == "TEXCOORD_1")
    { return GlTFPrimAttribute::TexCoord1; }
    if (strAttr == "COLOR_0")
    { return GlTFPrimAttribute::Color; }
    if (strAttr == "JOINTS_0")
    { return GlTFPrimAttribute::Joint; }
    if (strAttr == "WEIGHTS_0")
    { return GlTFPrimAttribute::Weight; }

    return GlTFPrimAttribute::Unknown;
}

GlTFModelWrapperSceneDesc::GlTFCameraType GlTFModelWrapperSceneDesc::cameraTypeStrToType(const std::string &strType)
{
    if (strType == TINYGLTF_CAMERA_TYPE_PERSPECTIVE)
    { return GlTFCameraType::Perspective; }
    if (strType == TINYGLTF_CAMERA_TYPE_ORTHOGRAPHIC)
    { return GlTFCameraType::Orthographic; }

    return GlTFCameraType::Unknown;
}

bool GlTFModelWrapperSceneDesc::profilingLoadImageWrapper(tinygltf::Image *image,
    int imageIdx, std::string *err, std::string *warn, 
    int reqWidth, int reqHeight, const unsigned char *bytes, 
    int size, void *ptr)
{
    PROF_SCOPE("glTF-LoadImageData");
    return tinygltf::LoadImageData(image, imageIdx, err, warn, reqWidth, reqHeight, bytes, size, ptr);
}

void GlTFModelWrapperSceneDesc::loadScenesFromFile(const res::File &sceneFile)
{
    std::string errMessages;
    std::string warnMessages;

    if (!sceneFile.exists())
    { throw GlTFLoader::SceneFileNotFoundException("GlTF scene file could not be loaded: File does not exist!"); }

    mLoader.SetImageLoader(&GlTFModelWrapperSceneDesc::profilingLoadImageWrapper, nullptr);

    const auto format = formatFromStr(sceneFile.extension());
    switch (format)
    {
        case GlTFFileFormat::Ascii:
        { // Readable JSON file:
            log<Info>() << "Scene file is using ASCII format of glTF!" << std::endl;
            mLoader.LoadASCIIFromFile(&mModel, &errMessages, &warnMessages, sceneFile.file());
            break;
        }

        case GlTFFileFormat::Binary:
        { // Binary JSON file:
            log<Info>() << "Scene file is using binary format of glTF!" << std::endl;
            mLoader.LoadBinaryFromFile(&mModel, &errMessages, &warnMessages, sceneFile.file());
            break;
        }

        default:
        {
            log<Debug>() << "Unknown scene file extension!" << std::endl;
            throw GlTFLoader::SceneLoadException("GlTF scene file could not be loaded: Provided scene file has unknown extension!");
        }
    }

    if (!errMessages.empty())
    { // Loading failed.
        std::stringstream ss;
        ss << "GlTF scene file could not be loaded: Scene loading failed with: \n Err: \n" << errMessages << "\nWarn: \n" << warnMessages;
        throw GlTFLoader::SceneLoadException(ss.str().c_str());
    }

    if (!warnMessages.empty())
    { // TinyGlTF returned some warnings which are not critical.
        log<Warning>() << "Scene loading ended with warnings: \n" << warnMessages << std::endl;
    }
}

GlTFModelWrapperSceneDesc::GlTFFileFormat GlTFModelWrapperSceneDesc::formatFromStr(const std::string &ext)
{
    if (ext == GLTF_ASCII_FILE_EXT)
    { return GlTFFileFormat::Ascii; }
    if (ext == GLTF_BINARY_FILE_EXT)
    { return GlTFFileFormat::Binary; }

    return GlTFFileFormat::Unknown;
}

scene::SceneDescription::PtrT GlTFModelWrapperSceneDesc::createSceneDesc(const std::string &primaryName)
{
    // Create the scene description, which will hold all of the sub-scenes.
    auto sceneDesc{ scene::SceneDescription::create() };
    sceneDesc->setName(primaryName);

    // Create the scene according to the scene model:
    createSceneResources(sceneDesc);
    createSceneEntities(sceneDesc);

    // Return back the scene description of this model.
    return sceneDesc;
}

void GlTFModelWrapperSceneDesc::createBuffer(scene::SceneDescription::PtrT sceneDesc, const tinygltf::Buffer &buffer)
{
    if (buffer.data.empty())
    { throw GlTFLoader::SceneLoadException("GlTF scene file could not be loaded: Buffer has no data!"); }

    mBuffers.emplace_back(
        sceneDesc->createBuffer(
            scene::desc::Buffer{
                (buffer.name.empty() ? (std::to_string(mBuffers.size()) + "buffer") : buffer.name),
                std::move(buffer.data) }));
}

void GlTFModelWrapperSceneDesc::createImage(scene::SceneDescription::PtrT sceneDesc, const tinygltf::Image &image)
{
    UNUSED(sceneDesc);

    //if (image.uri.empty())
    //{ throw GlTFLoader::SceneLoadException("GlTF scene file could not be loaded: Image has no data!"); }
    if (image.component < 1)
    { throw GlTFLoader::SceneLoadException("GlTF scene file could not be loaded: Image has no pixel components!"); }

    mImageInfo.emplace_back(&image);
}

void GlTFModelWrapperSceneDesc::createBufferView(scene::SceneDescription::PtrT sceneDesc, const tinygltf::Accessor &accessor)
{
    if (accessor.bufferView < 0)
    { throw GlTFLoader::SceneLoadException("GlTF scene file could not be loaded: Accessor has no buffer view!"); }

    ASSERT_FAST(accessor.bufferView < mModel.bufferViews.size());
    const auto& bufferView{ mModel.bufferViews[accessor.bufferView] };

    if (bufferView.buffer >= static_cast<int64_t>(mBuffers.size()))
    { throw GlTFLoader::SceneLoadException("GlTF scene file could not be loaded: Buffer view target buffer is out of bounds!"); }

    const auto& targetBuffer{mModel.buffers[bufferView.buffer].data};
    if (targetBuffer.size() < accessor.byteOffset + bufferView.byteOffset + bufferView.byteLength)
    { throw GlTFLoader::SceneLoadException("GlTF scene file could not be loaded: Buffer view is referencing data out of bounds!"); }

    mBufferViews.emplace_back(
        sceneDesc->createBufferView(
            scene::desc::BufferView{
                bufferView.name, 
                mBuffers[bufferView.buffer], 
                componentTypeToElementFormat(accessor.componentType, accessor.type),
                accessor.count,
                accessor.byteOffset + bufferView.byteOffset,
                bufferView.byteLength,
                static_cast<std::size_t>(accessor.ByteStride(bufferView)) }));
}

void GlTFModelWrapperSceneDesc::createSampler(scene::SceneDescription::PtrT sceneDesc, const tinygltf::Sampler &sampler)
{
    mSamplers.emplace_back(
        sceneDesc->createSampler(
            scene::desc::Sampler{
                sampler.name,
                minMagToFilter(sampler.minFilter, sampler.magFilter),
                wrapToEdgeFilters(sampler.wrapS, sampler.wrapT, sampler.wrapR) }));
}

void GlTFModelWrapperSceneDesc::createDefaultSampler(scene::SceneDescription::PtrT sceneDesc)
{
    rndr::MinMagMipFilter filtering{ };
    filtering.minFilter = rndr::SampleFilter::Linear;
    filtering.magFilter = rndr::SampleFilter::Linear;
    filtering.mipFilter = rndr::SampleFilter::Linear;

    rndr::EdgeFilterUVW edgeFiltering{ };
    edgeFiltering.edgeFilterU = rndr::SampleEdge::Clamped;
    edgeFiltering.edgeFilterV = rndr::SampleEdge::Clamped;
    edgeFiltering.edgeFilterW = rndr::SampleEdge::Clamped;

    mSamplers.emplace_back(
        sceneDesc->createSampler(
            scene::desc::Sampler{
                DEFAULT_SAMPLER_NAME,
                filtering, edgeFiltering }));
}

void GlTFModelWrapperSceneDesc::createTexture(scene::SceneDescription::PtrT sceneDesc, const tinygltf::Texture &texture)
{
    if (texture.source < 0)
    { throw GlTFLoader::SceneLoadException("GlTF scene file could not be loaded: Texture has no source!"); }
    if (texture.source >= static_cast<int64_t>(mImageInfo.size()))
    { throw GlTFLoader::SceneLoadException("GlTF scene file could not be loaded: Texture source index is out of bounds!"); }
    if (texture.sampler >= 0 && texture.sampler >= static_cast<int64_t>(mSamplers.size()) - 1)
    { throw GlTFLoader::SceneLoadException("GlTF scene file could not be loaded: Sampler source index is out of bounds!"); }

    // Take provided sampler or the default one.
    const auto samplerIdx{ texture.sampler >= 0 ? texture.sampler : mSamplers.size() - 1 };
    // Index of the image source of this texture.
    const auto sourceIdx{ texture.source };

    // Recover information about the source image.
    const auto& imageInfo{ *mImageInfo[sourceIdx] };

    // Calculate size of the texture image.
    const auto sizeInBytes{ imageInfo.image.size() * sizeof(imageInfo.image[0u]) };
    // Choose a name - whichever is not empty...
    const auto textureName{ texture.name.empty() ? (imageInfo.name.empty() ? imageInfo.uri : imageInfo.name) : texture.name };

    mTextures.emplace_back(
        sceneDesc->createTexture(
            scene::desc::Texture{
                textureName,
                // TODO - Can we move the data? It could be used by multiple textures.
                imageInfo.image, 
                numComponentsToFormat(imageInfo.component),
                mSamplers[samplerIdx], 
                static_cast<std::size_t>(imageInfo.width), 
                static_cast<std::size_t>(imageInfo.height) }));
}

void GlTFModelWrapperSceneDesc::createMaterial(scene::SceneDescription::PtrT sceneDesc, const tinygltf::Material &material)
{
    scene::desc::Material materialDesc{ };
    materialDesc.name = material.name;

    for (const auto& param : material.values)
    { processMaterialParameter(materialDesc, param); }

    // TODO - Add detection of other models.
    materialDesc.pbr.modelUsed = res::rndr::Material::PhysicallyBasedProperties::PbrModel::MetallicRoughness;

    for (const auto& param : material.additionalValues)
    { // Process common material parameters:
        processCommonMaterialParameter(materialDesc, param);
    }

    // Process the special material attributes.
    processMaterialSpecs(materialDesc, material.extensions);

    mMaterials.emplace_back(
        sceneDesc->createMaterial(
            std::move(materialDesc)));
}

void GlTFModelWrapperSceneDesc::processMaterialParameter(scene::desc::Material &materialDesc, const std::pair<std::string, tinygltf::Parameter> &parameter)
{
    switch (matPbrStrKeyToKey(parameter.first))
    {
        case GlTFPbrMaterialKey::BaseColorFactor:
        {
            ASSERT_FAST(util::count(materialDesc.pbr.baseColorFactor) <= parameter.second.number_array.size());
            for (std::size_t iii = 0u; iii < util::count(materialDesc.pbr.baseColorFactor); ++iii)
            {
                materialDesc.pbr.baseColorFactor[iii] = static_cast<float>(parameter.second.number_array[iii]);
            }

            break;
        }
        case GlTFPbrMaterialKey::BaseColorTexture:
        {
            const auto textureIdx{ parameter.second.TextureIndex() };

            ASSERT_FAST(textureIdx < mTextures.size());
            materialDesc.pbr.baseColorTexture = mTextures[textureIdx];

            break;
        }
        case GlTFPbrMaterialKey::MetallicFactor:
        {
            materialDesc.pbr.firstFactor.metallicFactor = static_cast<float>(parameter.second.Factor());

            break;
        }
        case GlTFPbrMaterialKey::RoughnessFactor:
        {
            materialDesc.pbr.secondFactor.roughnessFactor = static_cast<float>(parameter.second.Factor());

            break;
        }
        case GlTFPbrMaterialKey::MetallicRoughnessTexture:
        {
            const auto textureIdx{ parameter.second.TextureIndex() };

            ASSERT_FAST(textureIdx < mTextures.size());
            materialDesc.pbr.propertyTexture = mTextures[textureIdx];

            break;
        }
        case GlTFPbrMaterialKey::Unknown:
        default:
        {
            log<Info>() << "Unknown GlTF material key: \"" << parameter.first << "\" !" << std::endl;
            break;
        }
    }
}

void GlTFModelWrapperSceneDesc::processCommonMaterialParameter(scene::desc::Material &materialDesc, const std::pair<std::string, tinygltf::Parameter> &parameter)
{
    switch (matStrKeyToKey(parameter.first))
    {
        case GlTFMaterialKey::NormalTexture:
        {
            const auto textureIdx{ parameter.second.TextureIndex() };

            ASSERT_FAST(textureIdx < mTextures.size());
            materialDesc.normalTexture = mTextures[textureIdx];

            break;
        }
        case GlTFMaterialKey::OcclusionTexture:
        {
            const auto textureIdx{ parameter.second.TextureIndex() };

            ASSERT_FAST(textureIdx < mTextures.size());
            materialDesc.occlusionTexture = mTextures[textureIdx];

            break;
        }
        case GlTFMaterialKey::EmissiveTexture:
        {
            const auto textureIdx{ parameter.second.TextureIndex() };

            ASSERT_FAST(textureIdx < mTextures.size());
            materialDesc.emissiveTexture = mTextures[textureIdx];

            break;
        }
        case GlTFMaterialKey::EmissiveFactor:
        {
            ASSERT_FAST(util::count(materialDesc.emissiveFactor) <= parameter.second.number_array.size());
            for (std::size_t iii = 0u; iii < util::count(materialDesc.emissiveFactor); ++iii)
            { materialDesc.emissiveFactor[iii] = static_cast<float>(parameter.second.number_array[iii]); }

            break;
        }
        case GlTFMaterialKey::AlphaMode:
        {
            processMaterialAlphaMode(materialDesc, parameter);
            
            break;
        }
        case GlTFMaterialKey::AlphaCutoff:
        {
            materialDesc.alphaCutoff = static_cast<float>(parameter.second.number_value);

            break;
        }
        case GlTFMaterialKey::DoubleSided:
        {
            log<Info>() << "Skipping glTF material parameter for double-sidedness!" << std::endl;

            break;
        }
        case GlTFMaterialKey::Unknown:
        default:
        {
            log<Info>() << "Unknown GlTF material key: \"" << parameter.first << "\" !" << std::endl;
            break;
        }
    }
}

void GlTFModelWrapperSceneDesc::processMaterialSpecs(scene::desc::Material &materialDesc,
    const tinygltf::ExtensionMap &extensions)
{
    const auto materialSpec{ extensions.find(DXR_MATERIALS_SPEC_EXT_NAME) };

    if (materialSpec == extensions.end())
    { return; }

    const auto materialKeys{ materialSpec->second.Keys() };

    for (const auto &key : materialKeys)
    {
        const auto prop{ materialSpec->second.Get(key) };

        if (key == DXR_MATERIALS_SPEC_EXT_REFLECTIONS_NAME)
        { materialDesc.specularReflections = prop.Get<bool>(); }
        else if (key == DXR_MATERIALS_SPEC_EXT_REFRACTIONS_NAME)
        { materialDesc.specularRefractions = prop.Get<bool>(); }
    }
}

void GlTFModelWrapperSceneDesc::processMaterialAlphaMode(scene::desc::Material &materialDesc, const std::pair<std::string, tinygltf::Parameter> &parameter)
{
    switch (matAlphaStrToAlphaMode(parameter.second.string_value))
    {
        case GlTFMaterialAlphaMode::Opaque:
        {
            materialDesc.alphaMode = res::rndr::Material::AlphaMode::Opaque;

            break;
        }
        case GlTFMaterialAlphaMode::Mask:
        {
            materialDesc.alphaMode = res::rndr::Material::AlphaMode::Mask;

            break;
        }
        case GlTFMaterialAlphaMode::Blend:
        {
            materialDesc.alphaMode = res::rndr::Material::AlphaMode::Blend;

            break;
        }
        case GlTFMaterialAlphaMode::Unknown:
        default:
        {
            log<Info>() << "Unknown GlTF alpha mode: \"" << parameter.second.string_value << "\" !" << std::endl;

            break;
        }
    }
}

void GlTFModelWrapperSceneDesc::processPrimitiveAttribute(scene::desc::Primitive &primitiveDesc, const std::pair<std::string, int> &attribute)
{
    switch (primStrAttrToAttr(attribute.first))
    {
        case GlTFPrimAttribute::Position:
        {
            ASSERT_FAST(attribute.second < mBufferViews.size());

            primitiveDesc.positionBuffer = mBufferViews[attribute.second];

            break;
        }
        case GlTFPrimAttribute::Normal:
        {
            ASSERT_FAST(attribute.second < mBufferViews.size());

            primitiveDesc.normalBuffer = mBufferViews[attribute.second];

            break;
        }
        case GlTFPrimAttribute::Tangent:
        {
            ASSERT_FAST(attribute.second < mBufferViews.size());

            primitiveDesc.tangentBuffer = mBufferViews[attribute.second];

            break;
        }
        case GlTFPrimAttribute::TexCoord0:
        {
            ASSERT_FAST(attribute.second < mBufferViews.size());

            primitiveDesc.texCoordBuffer = mBufferViews[attribute.second];

            break;
        }
        case GlTFPrimAttribute::TexCoord1:
        {
            log<Info>() << "Skipping glTF second texture coordinate attribute!" << std::endl;

            break;
        }
        case GlTFPrimAttribute::Color:
        {
            ASSERT_FAST(attribute.second < mBufferViews.size());

            primitiveDesc.colorBuffer = mBufferViews[attribute.second];

            break;
        }
        case GlTFPrimAttribute::Joint:
        {
            log<Info>() << "Skipping glTF joint attribute!" << std::endl;

            break;
        }
        case GlTFPrimAttribute::Weight:
        {
            log<Info>() << "Skipping glTF weight attribute!" << std::endl;

            break;
        }
        case GlTFPrimAttribute::Unknown:
        default:
        {
            log<Info>() << "Unknown GlTF attribute key: \"" << attribute.first << "\" !" << std::endl;
            break;
        }
    }
}

void GlTFModelWrapperSceneDesc::createMeshPrimitive(scene::SceneDescription::PtrT sceneDesc, scene::desc::Mesh &meshDesc, 
    const tinygltf::Primitive &primitive)
{
    scene::desc::Primitive primitiveDesc{ };

    for (const auto& attribute : primitive.attributes)
    { // Process primitive attributes:
        processPrimitiveAttribute(primitiveDesc, attribute);
    }

    if (primitive.indices >= 0)
    {
        ASSERT_FAST(primitive.indices < mBufferViews.size());
        primitiveDesc.indiceBuffer = mBufferViews[primitive.indices];
    }

    if (primitive.material >= 0)
    {
        ASSERT_FAST(primitive.material < mMaterials.size());
        primitiveDesc.material = mMaterials[primitive.material];
    }

    switch (primitive.mode)
    {
        case TINYGLTF_MODE_POINTS:
        {
            primitiveDesc.primitiveMode = scene::desc::PrimitiveMode::Points;

            break;
        }
        case TINYGLTF_MODE_LINE:
        {
            primitiveDesc.primitiveMode = scene::desc::PrimitiveMode::Lines;

            break;
        }
        case TINYGLTF_MODE_LINE_LOOP:
        {
            primitiveDesc.primitiveMode = scene::desc::PrimitiveMode::LineLoop;

            break;
        }
        case TINYGLTF_MODE_TRIANGLES:
        {
            primitiveDesc.primitiveMode = scene::desc::PrimitiveMode::Triangles;

            break;
        }
        case TINYGLTF_MODE_TRIANGLE_STRIP:
        {
            primitiveDesc.primitiveMode = scene::desc::PrimitiveMode::TriangleStrip;

            break;
        }
        case TINYGLTF_MODE_TRIANGLE_FAN:
        {
            primitiveDesc.primitiveMode = scene::desc::PrimitiveMode::TriangleFan;

            break;
        }
        default:
        {
            log<Info>() << "Unknown glTF primitive mode!" << std::endl;

            break;
        }
    }

    meshDesc.primitives.emplace_back(
        sceneDesc->createPrimitive(
            std::move(primitiveDesc)));
}

void GlTFModelWrapperSceneDesc::createMesh(scene::SceneDescription::PtrT sceneDesc, const tinygltf::Mesh &mesh)
{
    // Create the mesh.
    scene::desc::Mesh meshDesc{ };
    meshDesc.name = mesh.name;

    for (const auto& primitive : mesh.primitives)
    { // Create all of meshes primitives:
        createMeshPrimitive(sceneDesc, meshDesc, primitive);
    }

    mMeshes.emplace_back(
        sceneDesc->createMesh(
            std::move(meshDesc)));
}

std::map<std::size_t, GlTFModelWrapperSceneDesc::ConversionRequest> GlTFModelWrapperSceneDesc::findRequiredConversions(const tinygltf::Model &model)
{
    // Vector to contain the requests.
    std::map<std::size_t, ConversionRequest> requests{};

    for (const auto& mesh : model.meshes)
    { // Accumulate attributes which need conversion.
        for (const auto& primitive : mesh.primitives)
        {
            if (primitive.indices >= 0)
            { // All valid index buffers should be uint32_t.
                ASSERT_FAST(primitive.indices < model.accessors.size());
                const auto& accessor{ model.accessors[primitive.indices] };
                const auto elementFormat{ componentTypeToElementFormat(accessor.componentType, accessor.type) };
                ASSERT_FAST(accessor.bufferView < model.bufferViews.size());
                if (elementFormat.format != rndr::ComponentFormat::UnsignedInt)
                { // All indices must be uint32_t.
                    requests.emplace(
                        accessor.bufferView,
                        ConversionRequest{
                            elementFormat, 
                            rndr::ElementFormat{ rndr::ComponentFormat::UnsignedInt, 1u }, 
                            TINYGLTF_COMPONENT_TYPE_UNSIGNED_INT, 
                            TINYGLTF_TYPE_SCALAR});
                }
            }
            for (const auto& attribute : primitive.attributes)
            {
                // Skip attributes with invalid buffer views.
                if (attribute.second < 0)
                {
                    continue;
                }

                switch (primStrAttrToAttr(attribute.first))
                {
                    case GlTFPrimAttribute::Position:
                    case GlTFPrimAttribute::Normal:
                    case GlTFPrimAttribute::Tangent:
                    { // Only float type is allowed.
                        break;
                    }
                    case GlTFPrimAttribute::TexCoord0:
                    { // Should always be float2
                        // TODO - Always convert to float2?
                        break;
                    }
                    case GlTFPrimAttribute::Color:
                    case GlTFPrimAttribute::TexCoord1:
                    case GlTFPrimAttribute::Joint:
                    case GlTFPrimAttribute::Weight:
                    case GlTFPrimAttribute::Unknown:
                    default:
                    { // Skip
                        break;
                    }
                }
            }
        }
    }

    return requests;
}

std::size_t GlTFModelWrapperSceneDesc::calculateConvertCopyBytes(const IndexedBufferView &view, const ConversionRequest *request)
{
    std::size_t result{0u};

    if (request)
    { // Conversion is requested -> size will be different.
        // TODO - Implement more as required?
        if (request->sourceFormat.format == rndr::ComponentFormat::UnsignedShort && 
            request->targetFormat.format == rndr::ComponentFormat::UnsignedInt)
        {
            result = (view.bufferView.byteLength / sizeof(uint16_t)) * sizeof(uint32_t);
        }
        else
        {
            throw GlTFLoader::SceneLoadException("Unknown conversion request: Not implemented!");
        }
    }
    else
    { // No conversion, use the old size.
        result = view.bufferView.byteLength;
    }

    return result;
}

void GlTFModelWrapperSceneDesc::convertCopyData(
    const tinygltf::Model &model, uint8_t *dst,
    const IndexedBufferView &view, std::size_t requiredBytes,
    const ConversionRequest *request)
{
    if (request)
    { // Conversion is requested -> conver-copy the data.
        // TODO - Implement more as required?
        if (request->sourceFormat.format == rndr::ComponentFormat::UnsignedShort && 
            request->targetFormat.format == rndr::ComponentFormat::UnsignedInt)
        {
            const auto* sourcePtr{ reinterpret_cast<const uint16_t*>(model.buffers[view.bufferView.buffer].data.data() + view.bufferView.byteOffset) };
            auto* dstPtr{reinterpret_cast<uint32_t*>(dst)};
            const auto numElements{view.bufferView.byteLength / sizeof(uint16_t)};

            for (std::size_t iii = 0; iii < numElements; ++iii)
            {
                dstPtr[iii] = static_cast<uint32_t>(sourcePtr[iii]);
            }
        }
        else
        {
            throw GlTFLoader::SceneLoadException("Unknown conversion request: Not implemented!");
        }
    }
    else
    { // No conversion, just copy.
        const auto* src{ model.buffers[view.bufferView.buffer].data.data() + view.bufferView.byteOffset };
        std::memcpy(dst, src, requiredBytes);
    }
}

const GlTFModelWrapperSceneDesc::ConversionRequest *GlTFModelWrapperSceneDesc::findConversionRequest(const IndexedBufferView &view, 
    const std::map<std::size_t, ConversionRequest> &requests)
{
    const auto findIt{requests.find(view.originalIndex)};
    return findIt == requests.end() ? nullptr : &findIt->second;
}

void GlTFModelWrapperSceneDesc::convertToSupportedTypes(tinygltf::Model &model)
{
    if (model.buffers.size() == 0u)
    { return; }

    // Which views should have its types converted?
    const auto& conversionRequests{ findRequiredConversions(model) };

    std::vector<IndexedBufferView> sortedViews{};
    for (std::size_t iii = 0; iii < model.bufferViews.size(); ++iii)
    { // Accumulate the buffer views.
        sortedViews.emplace_back(IndexedBufferView{ iii, model.bufferViews[iii] });
    }
    // Then sort them.
    std::sort(sortedViews.begin(),
              sortedViews.end(),
              [](auto& first, auto& second) { // Sort primarily by buffer index, secondarily by offset within that buffer.
                  return first.bufferView.buffer < second.bufferView.buffer ||
                         (first.bufferView.buffer == second.bufferView.buffer && first.bufferView.byteOffset < second.bufferView.byteOffset);
              });

    // Storage of the converted buffer data.
    std::vector<uint8_t> convertedData{};
    // Offset within the current output buffer.
    std::size_t bufferOffset{ 0u };
    // Index of the currently converted buffer.
    std::size_t currentBufferIndex{ 0u };
    // To what value should start of each view be aligned?
    static constexpr std::size_t VIEW_BYTE_ALIGNMENT{ 4 * sizeof(uint32_t) };
    // How many more bytes should we expect after conversion?
    static constexpr std::size_t BUFFER_OVER_ALLOCATE{ 1024u };
    // The new buffer will have at least the same size.
    convertedData.reserve(model.buffers[currentBufferIndex].data.size() + BUFFER_OVER_ALLOCATE);

    for (const auto& sortedView : sortedViews)
    { // Convert and align all the views.
        if (currentBufferIndex != static_cast<std::size_t>(sortedView.bufferView.buffer))
        { // Move to the next buffer.
            if (bufferOffset != 0u)
            { // Only replace if we actually copied something.
                model.buffers[currentBufferIndex].data = std::move(convertedData);
            }

            currentBufferIndex = static_cast<std::size_t>(sortedView.bufferView.buffer);
            bufferOffset = 0u;
            convertedData.clear();
            convertedData.reserve(model.buffers[currentBufferIndex].data.size() + BUFFER_OVER_ALLOCATE);
        }

        const auto conversionRequest{ findConversionRequest(sortedView, conversionRequests) };

        // Align the start of the new view.
        const auto alignedOffset{ util::math::alignTo(bufferOffset, VIEW_BYTE_ALIGNMENT) };
        // Predict how many bytes we will require.
        const auto requiredBytes{ calculateConvertCopyBytes(sortedView, conversionRequest) };
        // Resize the output buffer accordingly.
        convertedData.resize(alignedOffset + requiredBytes);

        // Convert or just copy the data.
        convertCopyData(model, convertedData.data() + alignedOffset, sortedView, requiredBytes, conversionRequest);

        // Fix the buffer view and its accessor.
        auto& originalBufferView{ model.bufferViews[sortedView.originalIndex] };
        originalBufferView.byteOffset = alignedOffset;
        originalBufferView.byteLength = requiredBytes;
        if (conversionRequest)
        {
            for (auto &originalAccessor : model.accessors)
            { // Fix all accessors using this buffer view.
                if (originalAccessor.bufferView != static_cast<int>(sortedView.originalIndex))
                { continue; }
                originalAccessor.componentType = conversionRequest->targetComponentType;
                originalAccessor.type = conversionRequest->targetType;
            }
        }

        bufferOffset = alignedOffset + requiredBytes;
    }

    if (bufferOffset != 0u)
    { // Only replace if we actually copied something.
        model.buffers[currentBufferIndex].data = std::move(convertedData);
    }
}

void GlTFModelWrapperSceneDesc::createSceneResources(scene::SceneDescription::PtrT sceneDesc)
{
    // Go through attribute buffers and convert to supported types.
    convertToSupportedTypes(mModel);

    // Create vertex data buffers.
    for (const auto& buffer : mModel.buffers)
    { createBuffer(sceneDesc, buffer); }

    // Create texture data buffers.
    for (const auto& image : mModel.images)
    { createImage(sceneDesc, image); }

    // Create buffer views.
    for (const auto& accessor : mModel.accessors)
    { createBufferView(sceneDesc, accessor); }

    // Create texture samplers.
    for (const auto& sampler : mModel.samplers)
    { createSampler(sceneDesc, sampler); }
    // Create default sampler for textures with undefined sampler.
    createDefaultSampler(sceneDesc);

    // Create textures.
    for (const auto& texture : mModel.textures)
    { createTexture(sceneDesc, texture); }

    // Create materials.
    for (const auto& material : mModel.materials)
    { createMaterial(sceneDesc, material); }

    // Create meshes.
    for (const auto& mesh : mModel.meshes)
    { createMesh(sceneDesc, mesh); }
}

void GlTFModelWrapperSceneDesc::initializeNodeEntityPerspCamera(const tinygltf::Camera &camera, scene::desc::Camera &cameraDesc)
{
    cameraDesc.type = scene::desc::CameraType::Perspective;
    cameraDesc.firstParam.aspectRatio = static_cast<float>(camera.perspective.aspectRatio);
    cameraDesc.secondParam.horFov = static_cast<float>(camera.perspective.yfov);
    cameraDesc.zNear = static_cast<float>(camera.perspective.znear);
    cameraDesc.zFar = static_cast<float>(camera.perspective.zfar);
}

void GlTFModelWrapperSceneDesc::initializeNodeEntityOrthoCamera(const tinygltf::Camera &camera, scene::desc::Camera &cameraDesc)
{
    cameraDesc.type = scene::desc::CameraType::Perspective;
    cameraDesc.firstParam.width = 2.0f * static_cast<float>(camera.orthographic.xmag);
    cameraDesc.secondParam.height = 2.0f * static_cast<float>(camera.orthographic.ymag);
    cameraDesc.zNear = static_cast<float>(camera.orthographic.znear);
    cameraDesc.zFar = static_cast<float>(camera.orthographic.zfar);
}

void GlTFModelWrapperSceneDesc::initializeNodeEntityCamera(const tinygltf::Camera &camera, scene::desc::Camera &cameraDesc)
{
    switch (cameraTypeStrToType(camera.type))
    {
        case GlTFCameraType::Perspective:
        {
            initializeNodeEntityPerspCamera(camera, cameraDesc);

            break;
        }
        case GlTFCameraType::Orthographic:
        {
            initializeNodeEntityOrthoCamera(camera, cameraDesc);

            break;
        }
        case GlTFCameraType::Unknown:
        default:
        {
            log<Info>() << "Unknown glTF camera type!" << std::endl;
            cameraDesc.type = scene::desc::CameraType::Unknown;

            break;
        }
    }
}

void GlTFModelWrapperSceneDesc::initializeNodeEntityMesh(scene::desc::MeshHandle mesh, scene::desc::Entity &entityDesc)
{ entityDesc.mesh = mesh; }

void GlTFModelWrapperSceneDesc::initializeNodeEntityTransform(const tinygltf::Node &node, scene::desc::Entity &entityDesc)
{
    if (node.rotation.size() != 0u)
    { // We have a rotation.
        // 4 elements for a rotation quaternion.
        static constexpr std::size_t ROTATION_ELEMENTS{ 4u };
        ASSERT_FAST(node.rotation.size() == ROTATION_ELEMENTS);

        // Make a floating precision copy:
        float rotation[ROTATION_ELEMENTS];
        for (std::size_t iii = 0; iii < ROTATION_ELEMENTS; ++iii)
        {
            rotation[iii] = static_cast<float>(node.rotation[iii]);
        }

        entityDesc.transform.setRotate(dxtk::math::Quaternion(rotation));
    }

    if (node.scale.size() != 0u)
    { // We have a scale.
        // 3 elements for a scale.
        static constexpr std::size_t SCALE_ELEMENTS{ 3u };
        ASSERT_FAST(node.scale.size() == SCALE_ELEMENTS);

        // Make a floating precision copy:
        float scale[SCALE_ELEMENTS];
        for (std::size_t iii = 0; iii < SCALE_ELEMENTS; ++iii)
        {
            scale[iii] = static_cast<float>(node.scale[iii]);
        }

        entityDesc.transform.setScale(dxtk::math::Vector3(scale));
    }

    if (node.translation.size() != 0u)
    { // We have a translation.
        // 3 elements for a translation in 3D.
        static constexpr std::size_t TRANSLATION_ELEMENTS{ 3u };
        ASSERT_FAST(node.translation.size() == 3u);

        // Make a floating precision copy:
        float translation[TRANSLATION_ELEMENTS];
        for (std::size_t iii = 0; iii < TRANSLATION_ELEMENTS; ++iii)
        {
            translation[iii] = static_cast<float>(node.translation[iii]);
        }

        entityDesc.transform.setTranslate(dxtk::math::Vector3(translation));
    }

    if (node.matrix.size() != 0u)
    { // We have a complex transform matrix.
        // 16 elements for a 4x4 matrix.
        static constexpr std::size_t MATRIX_ELEMENTS{ 16u };
        ASSERT_FAST(node.matrix.size() == MATRIX_ELEMENTS);

        // Make a floating precision copy:
        float matrix[MATRIX_ELEMENTS];
        for (std::size_t iii = 0; iii < MATRIX_ELEMENTS; ++iii)
        {
            matrix[iii] = static_cast<float>(node.matrix[iii]);
        }

        dxtk::math::Matrix m;
        // Copy the column-major matrix.
        std::memcpy(m.m, matrix, 16u * sizeof(float));
        // Transpose to correctly match the row-major matrix in DXTK.
        entityDesc.transform.setMatrix(m.Transpose());
    }
}

void GlTFModelWrapperSceneDesc::initializeNodeEntity(scene::SceneDescription::PtrT sceneDesc, const tinygltf::Node &node, 
    scene::desc::Entity &entityDesc)
{
    if (node.camera >= 0)
    {
        if (node.camera > static_cast<int64_t>(mModel.cameras.size()))
        { throw BaseSceneLoader::SceneLoadException("GlTF scene file could not be loaded: Node camera index is out of bounds!"); }

        scene::desc::Camera cameraDesc{ };
        initializeNodeEntityCamera(mModel.cameras[node.camera], cameraDesc);
        entityDesc.camera = sceneDesc->createCamera(std::move(cameraDesc));
    }

    if (node.mesh >= 0)
    {
        if (node.mesh > static_cast<int64_t>(mMeshes.size()))
        { throw BaseSceneLoader::SceneLoadException("GlTF scene file could not be loaded: Node mesh index is out of bounds!"); }

        initializeNodeEntityMesh(mMeshes[node.mesh], entityDesc);
    }

    initializeNodeEntityTransform(node, entityDesc);
}

void GlTFModelWrapperSceneDesc::createSubSceneEntities(scene::SceneDescription::PtrT sceneDesc, std::size_t rootNodeIdx)
{
    ASSERT_FAST(rootNodeIdx < mModel.nodes.size());

    // List of entities are yet to be created and their parents.
    std::vector<std::pair<std::size_t, scene::desc::EntityHandle>> entitiesToCreate;

    // Initialize the sub-tree creation.
    entitiesToCreate.emplace_back(std::make_pair(rootNodeIdx, scene::desc::NULL_HANDLE_VALUE));

    while (!entitiesToCreate.empty())
    { // Until the entity sub-tree is completely created.
        // Move to the next node:
        const auto currentNodeIdx{ entitiesToCreate.back().first };
        const auto currentNodeParent{ entitiesToCreate.back().second };
        entitiesToCreate.pop_back();
        const auto& currentNode{ mModel.nodes[currentNodeIdx] };

        scene::desc::Entity entityDesc{ };
        entityDesc.name = currentNode.name;

        // Initialize the current node with its components.
        initializeNodeEntity(sceneDesc, currentNode, entityDesc);

        // Create the current entity.
        const auto entityHandle{ sceneDesc->createEntity(std::move(entityDesc)) };

        // Create children:
        for (const auto& childNodeIdx : currentNode.children)
        {
            if (childNodeIdx < 0 || childNodeIdx >= static_cast<int64_t>(mModel.nodes.size()))
            { throw BaseSceneLoader::SceneLoadException("GlTF scene file could not be loaded: Child node index is out of bounds!"); }

            // Mark the child index and the parent handle.
            entitiesToCreate.emplace_back(std::make_pair(static_cast<std::size_t>(childNodeIdx), entityHandle));
        }
    }
}

void GlTFModelWrapperSceneDesc::createDefaultCamera(scene::SceneDescription::PtrT sceneDesc)
{
    // Create a default perspective camera, without forcing too many parameters.
    scene::desc::Camera cameraDesc{ };
    cameraDesc.type = scene::desc::CameraType::Perspective;
    cameraDesc.firstParam.aspectRatio = 0.0f;
    cameraDesc.secondParam.horFov = 0.0f;
    cameraDesc.zNear = 0.1f;
    cameraDesc.zFar = 100.0f;

    // Create the entity which will hold the camera
    scene::desc::Entity cameraEntityDesc{ };
    cameraEntityDesc.name = "DefaultCamera";
    cameraEntityDesc.camera = sceneDesc->createCamera(std::move(cameraDesc));
    sceneDesc->createEntity(std::move(cameraEntityDesc));
}

scene::desc::SubSceneHandle GlTFModelWrapperSceneDesc::createSubScene(scene::SceneDescription::PtrT sceneDesc, const tinygltf::Scene &scene)
{
    // Create a new sub-scene for the glTF scene.
    auto subSceneHandle{ sceneDesc->createSubScene(scene.name) };
    sceneDesc->activateSubScene(subSceneHandle);

    for (const auto& node : scene.nodes)
    { // Create every root entity in the scene and their children.
        //if (node < 0 || node >= static_cast<int64_t>(scene.nodes.size()))
        //{ throw BaseSceneLoader::SceneLoadException("GlTF scene file could not be loaded: Root node index is out of bounds!"); }

        createSubSceneEntities(sceneDesc, static_cast<std::size_t>(node));
    }

    // Create a default camera for looking through the scene.
    createDefaultCamera(sceneDesc);

    return subSceneHandle;
}

void GlTFModelWrapperSceneDesc::createSceneEntities(scene::SceneDescription::PtrT sceneDesc)
{
    // Holder for the default sub-scene.
    scene::desc::SubSceneHandle defaultSubSceneHandle{ scene::desc::NULL_HANDLE_VALUE };
    const auto defaultSceneIndex{ mModel.defaultScene >= 0 ? static_cast<std::size_t>(mModel.defaultScene) : 0u };

    // Create sub-scenes and their entities.
    for (std::size_t iii = 0; iii < mModel.scenes.size(); ++iii)
    { 
        const auto subSceneHandle{ createSubScene(sceneDesc, mModel.scenes[iii]) };
        if (iii == defaultSceneIndex)
        { defaultSubSceneHandle = subSceneHandle; }
    }

    // If there are no scenes, create a default one.
    if (mModel.scenes.size() <= 0u)
    {
        // Create the default sub-scene
        auto subSceneHandle{ sceneDesc->createSubScene("Default sub-scene") };
        sceneDesc->activateSubScene(subSceneHandle);
        // Create a default camera for looking through the scene.
        createDefaultCamera(sceneDesc);
    }

    if (defaultSubSceneHandle != scene::desc::NULL_HANDLE_VALUE)
    { 
        // Specify which sub-scene should be active at the start.
        sceneDesc->activateSubScene(defaultSubSceneHandle);
    }
    else
    { /* If no other sub-scene is set as primary, keep the default one. */ }
}

rndr::ElementFormat GlTFModelWrapperSceneDesc::numComponentsToFormat(std::size_t numComponents)
{
    rndr::ElementFormat elementFormat{ };

    // Since we are using stb::image, we will always get 8bit values.
    elementFormat.format = rndr::ComponentFormat::UnsignedNormByte;
    elementFormat.numComponents = static_cast<uint16_t>(numComponents);

    return elementFormat;
}

rndr::MinMagMipFilter GlTFModelWrapperSceneDesc::minMagToFilter(int minFilter, int magFilter)
{
    rndr::MinMagMipFilter edgeFiltering{ };

    const auto minFilterNearest{ 
        minFilter == TINYGLTF_TEXTURE_FILTER_NEAREST || 
        minFilter == TINYGLTF_TEXTURE_FILTER_NEAREST_MIPMAP_LINEAR || 
        minFilter == TINYGLTF_TEXTURE_FILTER_NEAREST_MIPMAP_NEAREST};
    edgeFiltering.minFilter = minFilterNearest ? rndr::SampleFilter::Nearest : rndr::SampleFilter::Linear;

    const auto mipFilterNearest{
        minFilter == TINYGLTF_TEXTURE_FILTER_NEAREST_MIPMAP_NEAREST || 
        minFilter == TINYGLTF_TEXTURE_FILTER_LINEAR_MIPMAP_NEAREST};
    edgeFiltering.mipFilter = mipFilterNearest ? rndr::SampleFilter::Nearest : rndr::SampleFilter::Linear;

    const auto magFilterNearest{magFilter == TINYGLTF_TEXTURE_FILTER_NEAREST};
    edgeFiltering.magFilter = magFilterNearest ? rndr::SampleFilter::Nearest : rndr::SampleFilter::Linear;

    return edgeFiltering;
}

rndr::SampleEdge GlTFModelWrapperSceneDesc::wrapToEdgeFilter(int wrap)
{
    switch (wrap)
    {
        case TINYGLTF_TEXTURE_WRAP_CLAMP_TO_EDGE:
            { return rndr::SampleEdge::Clamped; }
        case TINYGLTF_TEXTURE_WRAP_MIRRORED_REPEAT:
            { return rndr::SampleEdge::Mirrored; }
        case TINYGLTF_TEXTURE_WRAP_REPEAT:
        default:
            { return rndr::SampleEdge::Repeat; }
    }
}

rndr::EdgeFilterUVW GlTFModelWrapperSceneDesc::wrapToEdgeFilters(int wrapS, int wrapT, int wrapR)
{
    return {
        wrapToEdgeFilter(wrapS), 
        wrapToEdgeFilter(wrapT), 
        wrapToEdgeFilter(wrapR)
    };
}

rndr::ElementFormat GlTFModelWrapperSceneDesc::componentTypeToElementFormat(int componentType, int elementClass)
{
    rndr::ElementFormat elementFormat{ };

    switch (elementClass)
    {
        case TINYGLTF_TYPE_SCALAR:
        {
            elementFormat.numComponents = 1u;
            break;
        }
        case TINYGLTF_TYPE_VEC2:
        {
            elementFormat.numComponents = 2u;
            break;
        }
        case TINYGLTF_TYPE_VEC3:
        {
            elementFormat.numComponents = 3u;
            break;
        }
        case TINYGLTF_TYPE_VEC4:
        {
            elementFormat.numComponents = 4u;
            break;
        }
        default:
        {
            elementFormat.numComponents = 0u;
            break;
        }
    }

    switch (componentType)
    {
        case TINYGLTF_COMPONENT_TYPE_BYTE:
        {
            elementFormat.format = rndr::ComponentFormat::SignedByte;
            break;
        }
        case TINYGLTF_COMPONENT_TYPE_FLOAT:
        {
            elementFormat.format = rndr::ComponentFormat::Float32;
            break;
        }
        case TINYGLTF_COMPONENT_TYPE_SHORT:
        {
            elementFormat.format = rndr::ComponentFormat::SignedShort;
            break;
        }
        case TINYGLTF_COMPONENT_TYPE_UNSIGNED_BYTE:
        {
            elementFormat.format = rndr::ComponentFormat::UnsignedByte;
            break;
        }
        case TINYGLTF_COMPONENT_TYPE_UNSIGNED_INT:
        {
            elementFormat.format = rndr::ComponentFormat::UnsignedInt;
            break;
        }
        case TINYGLTF_COMPONENT_TYPE_UNSIGNED_SHORT:
        {
            elementFormat.format = rndr::ComponentFormat::UnsignedShort;
            break;
        }
        default:
        {
            elementFormat.format = rndr::ComponentFormat::Unknown;
            break;
        }
    }

    return elementFormat;
}

scene::SceneDescription::PtrT GlTFLoader::loadFromFileToSceneDesc(const res::File &sceneFile)
{
    // Make model wrapper which contains the processing methods.
    GlTFModelWrapperSceneDesc modelWrapper;

    {
        PROF_SCOPE("glTF-LoadScene");
        // Load scene data from user provided file.
        modelWrapper.loadScenesFromFile(sceneFile);
    }

    {
        PROF_SCOPE("glTF-CreateScene");
        // Parse scene data and transfer it into the internal representation.
        return modelWrapper.createSceneDesc(sceneFile.filename());
    }
}

} // namespace loaders

} // namespace scene

} // namespace quark

// Implementation begin end.
