/**
 * @file scene/loaders/SceneDescLoader.cpp
 * @author Tomas Polasek
 * @brief Loader which allows scene loading from SceneDescription objects.
 */

#include "stdafx.h"

#include "engine/scene/loaders/SceneDescLoader.h"

#include "engine/util/prof/Profiler.h"

namespace quark
{

namespace scene
{

namespace loaders
{

res::scene::SceneHandle SceneDescLoader::loadFromDesc(
    const SceneDescription::ConstPtrT sceneDesc, res::scene::SceneMgr &mgr)
{
    if (!sceneDesc)
    { 
        log<Error>() << "Failed to load scene description: Provided sceneDesc pointer is not valid!" << std::endl;

        return { }; 
    }

    PROF_SCOPE("SceneDesc-CreateScene");
    return createScenes(sceneDesc, mgr);
}

res::scene::SceneHandle SceneDescLoader::loadTestingScene(res::scene::SceneMgr & mgr)
{
    const auto sceneDesc{ quark::scene::SceneDescription::create() };

    sceneDesc->setName("TestScene");

    const auto subScene{ sceneDesc->createSubScene("TestSubScene") };
    sceneDesc->activateSubScene(subScene);

    quark::scene::desc::Buffer bufferDesc{ };
    bufferDesc.name = "AllBuffer";
    const float positions[]{
        -1.0f, -1.0f, 0.0f, 
        1.0f, 1.0f, 0.0f, 
        1.0f, -1.0f, 0.0f, 

        1.0f, 1.0f, 0.0f, 
        -1.0f, -1.0f, 0.0f, 
        -1.0f, 1.0f, 0.0f, 
    };
    const float normals[]{
        0.0f, 0.0f, 1.0f, 
        0.0f, 0.0f, 1.0f, 
        0.0f, 0.0f, 1.0f, 

        0.0f, 0.0f, 1.0f, 
        0.0f, 0.0f, 1.0f, 
        0.0f, 0.0f, 1.0f
    };
    const float texCoords[]{
        0.0f, 0.0f, 
        10.0f, 10.0f, 
        10.0f, 0.0f, 

        10.0f, 10.0f, 
        0.0f, 0.0f, 
        0.0f, 10.0f, 
    };
    const uint32_t indices[]{
        0u, 1u, 2u, 
        3u, 4u, 5u, 
    };

    const auto attributeAlignment{ sizeof(float) * 4u };

    const auto posStart{ 0u };
    const auto posSize{ sizeof(positions) };
    const auto posEnd{ util::math::alignTo(posSize, attributeAlignment) };
    const auto posStride{ sizeof(float) * 3u };
    const auto posCount{ util::count(positions) / 3u };

    const auto normStart{ posEnd };
    const auto normSize{ sizeof(normals) };
    const auto normEnd{ util::math::alignTo(normSize, attributeAlignment) };
    const auto normStride{ sizeof(float) * 3u };
    const auto normCount{ util::count(normals) / 3u };

    const auto texStart{ normEnd };
    const auto texSize{ sizeof(texCoords) };
    const auto texEnd{ util::math::alignTo(texStart + texSize, attributeAlignment) };
    const auto texStride{ sizeof(float) * 2u };
    const auto texCount{ util::count(texCoords) / 2u };

    const auto indStart{ texEnd };
    const auto indSize{ sizeof(indices) };
    const auto indEnd{ util::math::alignTo(indStart + indSize, attributeAlignment) };
    const auto indStride{ sizeof(uint32_t) };
    const auto indCount{ util::count(indices) };

    bufferDesc.data.resize(indEnd);
    const auto dataPtr{ bufferDesc.data.data() };
    std::memcpy(dataPtr + posStart, positions, posSize);
    std::memcpy(dataPtr + normStart, normals, normSize);
    std::memcpy(dataPtr + texStart, texCoords, texSize);
    std::memcpy(dataPtr + indStart, indices, indSize);

    const auto bufferHandle{ sceneDesc->createBuffer(std::move(bufferDesc)) };

    const auto posBufferViewHandle{
        sceneDesc->createBufferView(
            quark::scene::desc::BufferView{
                "Positions",
                bufferHandle,
                quark::rndr::ElementFormat{
                    quark::rndr::ComponentFormat::Float32,
                    3u },
                posCount,
                posStart,
                posSize,
                posStride }) };

    const auto normBufferViewHandle{
        sceneDesc->createBufferView(
            quark::scene::desc::BufferView{
                "Normals",
                bufferHandle,
                quark::rndr::ElementFormat{
                    quark::rndr::ComponentFormat::Float32,
                    3u },
                normCount,
                normStart,
                normSize,
                normStride }) };

    const auto texBufferViewHandle{
        sceneDesc->createBufferView(
            quark::scene::desc::BufferView{
                "TexCoords",
                bufferHandle,
                quark::rndr::ElementFormat{
                    quark::rndr::ComponentFormat::Float32,
                    2u },
                texCount,
                texStart,
                texSize,
                texStride }) };

    const auto indBufferViewHandle{
        sceneDesc->createBufferView(
            quark::scene::desc::BufferView{
                "Indices",
                bufferHandle,
                quark::rndr::ElementFormat{
                    quark::rndr::ComponentFormat::UnsignedInt,
                    1u },
                indCount,
                indStart,
                indSize,
                indStride }) };

    quark::scene::desc::Texture textureDesc{ };
    textureDesc.name = "TestTexture";
    const uint8_t textureData[]{
        // Base
        255u, 0u, 0u, 255u, 
        255u, 0u, 0u, 255u, 
        0u, 255u, 0u, 255u, 
        0u, 255u, 0u, 255u, 

        255u, 0u, 0u, 255u, 
        255u, 0u, 0u, 255u, 
        0u, 255u, 0u, 255u, 
        0u, 255u, 0u, 255u, 

        0u, 255u, 0u, 255u, 
        0u, 255u, 0u, 255u, 
        255u, 0u, 0u, 255u, 
        255u, 0u, 0u, 255u, 

        0u, 255u, 0u, 255u, 
        0u, 255u, 0u, 255u, 
        255u, 0u, 0u, 255u, 
        255u, 0u, 0u, 255u, 

        // Mip-map 1
        0u, 255u, 0u, 255u, 
        0u, 0u, 255u, 255u, 

        0u, 0u, 255u, 255u, 
        0u, 255u, 0u, 255u, 

        // Mip-map 2
        255u, 255u, 255u, 255u, 
    };
    textureDesc.data.assign(std::begin(textureData), std::end(textureData));
    textureDesc.elementFormat.format = quark::rndr::ComponentFormat::UnsignedNormByte;
    textureDesc.elementFormat.numComponents = 4u;
    textureDesc.width = 4u;
    textureDesc.height = 4u;
    textureDesc.mipMapCount = 2u;
    const auto textureHandle{ sceneDesc->createTexture(std::move(textureDesc)) };

    quark::scene::desc::Material materialDesc{ };
    materialDesc.name = "TestMaterial";
    materialDesc.setupNoPbr(textureHandle);
    materialDesc.normalTexture = textureHandle;
    const auto materialHandle{ sceneDesc->createMaterial(std::move(materialDesc)) };

    quark::scene::desc::Mesh meshDesc{ };
    meshDesc.name = "TestMesh";

    quark::scene::desc::Primitive primitiveDesc{ };
    primitiveDesc.name = "TestPrimitive";
    primitiveDesc.indiceBuffer = indBufferViewHandle;
    primitiveDesc.positionBuffer = posBufferViewHandle;
    primitiveDesc.normalBuffer = normBufferViewHandle;
    primitiveDesc.texCoordBuffer = texBufferViewHandle;
    primitiveDesc.material = materialHandle;
    const auto primitiveHandle{ sceneDesc->createPrimitive(std::move(primitiveDesc)) };

    meshDesc.primitives.emplace_back(primitiveHandle);
    const auto meshHandle{ sceneDesc->createMesh(std::move(meshDesc)) };

    quark::scene::desc::Entity entityDesc{ };
    entityDesc.name = "TestEntity";
    entityDesc.mesh = meshHandle;
    const auto entityHandle{ sceneDesc->createEntity(std::move(entityDesc)) };

    return loadFromDesc(sceneDesc, mgr);
}

res::scene::SceneHandle SceneDescLoader::createScenes(
    const SceneDescription::ConstPtrT sceneDesc, res::scene::SceneMgr &mgr)
{
    // Check if we have already loaded the target scene.
    auto primarySceneHandle{ mgr.getOrNull(sceneDesc->name()) };
    if (primarySceneHandle)
    { return primarySceneHandle; }

	// Create the primary scene, which will hold all of the sub-scenes.
    //auto primarySceneHandle{ mgr.create(sceneDesc->name()) };
    primarySceneHandle = mgr.create(sceneDesc->name());
    res::scene::Scene &primaryScene{ primarySceneHandle.get() };

	// Create the scene according to the scene description:
	createSceneResources(*sceneDesc, primaryScene);
	createSceneEntities(*sceneDesc, primaryScene);

	// Set transient data, which needs to be held until finalization.
	primaryScene.setTransientData(sceneDesc->shared_from_this());

	// Return back the primary scene of this model.
	return primarySceneHandle;
}

void SceneDescLoader::createBuffer(
    const SceneDescription &sceneDesc, res::scene::Scene &primaryScene, 
    const scene::desc::Buffer &bufferDesc)
{
    UNUSED(sceneDesc);

	const auto sizeInBytes{bufferDesc.data.size() * sizeof(bufferDesc.data[0u])};
    mBuffers.emplace_back(
        primaryScene.createBuffer(
            bufferDesc.name, 
            bufferDesc.data.data(), sizeInBytes));
}

void SceneDescLoader::createBufferView(
    const SceneDescription &sceneDesc, res::scene::Scene &primaryScene, 
    const scene::desc::BufferView &bufferViewDesc)
{
    UNUSED(sceneDesc);

    mBufferViews.emplace_back(
        primaryScene.createBufferView(
            bufferViewDesc.name, 
            mBuffers[SceneDescription::handleToIndex(bufferViewDesc.buffer)], 
            bufferViewDesc.elementFormat, 
            bufferViewDesc.elementCount, 
            bufferViewDesc.byteOffset, 
            bufferViewDesc.byteLength, 
            bufferViewDesc.byteStride));
}

void SceneDescLoader::createSampler(
    const SceneDescription &sceneDesc, res::scene::Scene &primaryScene, 
    const scene::desc::Sampler &samplerDesc)
{
    UNUSED(sceneDesc);

    mSamplers.emplace_back(
        primaryScene.createSampler(
            samplerDesc.name,
            samplerDesc.filtering,
            samplerDesc.edgeFiltering));
}

void SceneDescLoader::createDefaultSampler(
    const SceneDescription &sceneDesc, res::scene::Scene &primaryScene)
{
    UNUSED(sceneDesc);

    rndr::MinMagMipFilter filtering{ };
    filtering.minFilter = rndr::SampleFilter::Linear;
    filtering.magFilter = rndr::SampleFilter::Linear;
    filtering.mipFilter = rndr::SampleFilter::Linear;

    rndr::EdgeFilterUVW edgeFiltering{ };
    edgeFiltering.edgeFilterU = rndr::SampleEdge::Clamped;
    edgeFiltering.edgeFilterV = rndr::SampleEdge::Clamped;
    edgeFiltering.edgeFilterW = rndr::SampleEdge::Clamped;

    mSamplers.emplace_back(
        primaryScene.createSampler(
            DEFAULT_SAMPLER_NAME,
            filtering, edgeFiltering));
}

void SceneDescLoader::createTexture(
    const SceneDescription &sceneDesc, res::scene::Scene &primaryScene, 
    const scene::desc::Texture &textureDesc)
{
    UNUSED(sceneDesc);

    const auto sizeInBytes{ textureDesc.data.size() * sizeof(textureDesc.data[0u]) };
    mTextures.emplace_back(
        primaryScene.createTexture(
            textureDesc.name, 
            textureDesc.data.data(), sizeInBytes, 
            textureDesc.elementFormat, 
            textureDesc.width, textureDesc.height, 
            textureDesc.mipMapCount));
}

void SceneDescLoader::createMaterial(
    const SceneDescription &sceneDesc, res::scene::Scene &primaryScene, 
    const scene::desc::Material &materialDesc)
{
    UNUSED(sceneDesc);

	// Create the new material.
    auto matHandle{ primaryScene.createMaterial(materialDesc.name) };
	// Get the instance, so we can set its values.
    auto& matVal{ matHandle.get() };

	// TODO - Add other models.
    // TODO - Fix ugliness?
	matVal.pbr.modelUsed = materialDesc.pbr.modelUsed;

    if (SceneDescription::handleNotNull(materialDesc.normalTexture))
    { matVal.normalTexture = mTextures[SceneDescription::handleToIndex(materialDesc.normalTexture)]; }

    if (SceneDescription::handleNotNull(materialDesc.occlusionTexture))
    { matVal.occlusionTexture = mTextures[SceneDescription::handleToIndex(materialDesc.occlusionTexture)]; }

    std::copy(std::begin(materialDesc.emissiveFactor), std::end(materialDesc.emissiveFactor), matVal.emissiveFactor);

    if (SceneDescription::handleNotNull(materialDesc.emissiveTexture))
    { matVal.emissiveTexture = mTextures[SceneDescription::handleToIndex(materialDesc.emissiveTexture)]; }

    matVal.alphaCutoff = materialDesc.alphaCutoff;
    matVal.alphaMode = materialDesc.alphaMode;
    std::copy(std::begin(materialDesc.pbr.baseColorFactor), std::end(materialDesc.pbr.baseColorFactor), matVal.pbr.baseColorFactor);

    if (SceneDescription::handleNotNull(materialDesc.pbr.baseColorTexture))
    { matVal.pbr.baseColorTexture = mTextures[SceneDescription::handleToIndex(materialDesc.pbr.baseColorTexture)]; }

    std::copy(std::begin(materialDesc.pbr.firstFactor.specularFactor), std::end(materialDesc.pbr.firstFactor.specularFactor), matVal.pbr.firstFactor.specularFactor);
    matVal.pbr.secondFactor.glossinessFactor = materialDesc.pbr.secondFactor.glossinessFactor;

    if (SceneDescription::handleNotNull(materialDesc.pbr.propertyTexture))
    { matVal.pbr.propertyTexture = mTextures[SceneDescription::handleToIndex(materialDesc.pbr.propertyTexture)]; }

    matVal.specularReflections = materialDesc.specularReflections;
    matVal.specularRefractions = materialDesc.specularRefractions;

	mMaterials.emplace_back(matHandle);
}

void SceneDescLoader::createMeshPrimitive(
    res::rndr::Mesh &meshVal, const scene::desc::Primitive &primitiveDesc)
{
    // Create the primitive.
    res::rndr::Primitive meshPrimitive;

    meshPrimitive.indices = mBufferViews[SceneDescription::handleToIndex(primitiveDesc.indiceBuffer)];

    if (SceneDescription::handleNotNull(primitiveDesc.positionBuffer))
    { meshPrimitive.setPositionBuffer(mBufferViews[SceneDescription::handleToIndex(primitiveDesc.positionBuffer)]); }
    if (SceneDescription::handleNotNull(primitiveDesc.normalBuffer))
    { meshPrimitive.setNormalBuffer(mBufferViews[SceneDescription::handleToIndex(primitiveDesc.normalBuffer)]); }
    if (SceneDescription::handleNotNull(primitiveDesc.tangentBuffer))
    { meshPrimitive.setTangentBuffer(mBufferViews[SceneDescription::handleToIndex(primitiveDesc.tangentBuffer)]); }
    if (SceneDescription::handleNotNull(primitiveDesc.texCoordBuffer))
    { meshPrimitive.setTexCoordBuffer(mBufferViews[SceneDescription::handleToIndex(primitiveDesc.texCoordBuffer)]); }
    if (SceneDescription::handleNotNull(primitiveDesc.colorBuffer))
    { meshPrimitive.setColorBuffer(mBufferViews[SceneDescription::handleToIndex(primitiveDesc.colorBuffer)]); }
    meshPrimitive.primitiveMode = primitiveDesc.primitiveMode;

    meshPrimitive.material = mMaterials[SceneDescription::handleToIndex(primitiveDesc.material)];

    // Add primitive to mesh.
    meshVal.primitives.emplace_back(meshPrimitive);
}

void SceneDescLoader::createMesh(
    const SceneDescription &sceneDesc, res::scene::Scene &primaryScene, 
    const scene::desc::Mesh &meshDesc)
{
    UNUSED(sceneDesc);

	// Create the new mesh.
    auto meshHandle{ primaryScene.createMesh(meshDesc.name) };
	// Get the instance, so we can set its values.
    auto& meshVal{ meshHandle.get() };

	for (const auto& primitiveHandle : meshDesc.primitives)
	{ // Create all of meshes primitives:
		createMeshPrimitive(meshVal, sceneDesc.primitives()[SceneDescription::handleToIndex(primitiveHandle)]);
	}

	mMeshes.emplace_back(meshHandle);
}

void SceneDescLoader::createEntityComponents(const scene::SceneDescription &sceneDesc, EntityHandle &entity, 
    const scene::desc::Entity &entityDesc)
{
    if (SceneDescription::handleNotNull(entityDesc.mesh))
    { // Entity is renderable.
        const auto renderableComp{ entity.add<comp::Renderable>() };
        ASSERT_FAST(renderableComp != nullptr);
        renderableComp->meshHandle = mMeshes[SceneDescription::handleToIndex(entityDesc.mesh)];
    }

    // TODO - Do all entities require transform?
    // All entities have a transform.
    const auto transformComponent{ entity.get<comp::Transform>() };
    ASSERT_FAST(transformComponent != nullptr);
    transformComponent->localTransform = entityDesc.transform;
    transformComponent->dirty = true;

    if (scene::SceneDescription::handleNotNull(entityDesc.camera))
    { // Entity has a camera connected to it.
        const auto cameraComp{ entity.add<comp::Camera>() };
        ASSERT_FAST(cameraComp != nullptr);
        const auto &cameraDesc{ sceneDesc.cameras()[SceneDescription::handleToIndex(entityDesc.camera)] };
        cameraComp->type = cameraDesc.type;
        cameraComp->firstParam.width = cameraDesc.firstParam.width;
        cameraComp->secondParam.height = cameraDesc.secondParam.height;
        cameraComp->zNear = cameraDesc.zNear;
        cameraComp->zFar = cameraDesc.zFar;
    }
}

void SceneDescLoader::createSubSceneEntities(
    const SceneDescription &sceneDesc, res::scene::Scene &primaryScene, 
    const scene::desc::SubScene &subSceneDesc, res::scene::SubSceneHandle &subScene)
{
    /// List of already created entities.
    std::vector<scene::EntityHandle> entities{ };

    for (const auto &entityDesc : subSceneDesc.entities.resources)
    { // Create all of the entities.
        auto entityHandle{ primaryScene.createEntity(subScene, entityDesc.name) };
        if (SceneDescription::handleNotNull(entityDesc.parent))
        { // If we have a parent then connect it in scene graph.
            primaryScene.setEntityAsParentOf(subScene, entities[SceneDescription::handleToIndex(entityDesc.parent)], entityHandle);
        }

        // Setup the entity components.
        createEntityComponents(sceneDesc, entityHandle, entityDesc);

        // Register the entity so we can connect parents to their childeren.
        entities.emplace_back(entityHandle);
    }
}

void SceneDescLoader::createSubScene(
    const SceneDescription &sceneDesc, res::scene::Scene &primaryScene, 
    const scene::desc::SubScene &subSceneDesc)
{
	// Create a new sub-scene for the glTF scene.
    auto subSceneHandle{ primaryScene.createSubScene(subSceneDesc.name) };

    // Fill the sub-scene with entities.
    createSubSceneEntities(sceneDesc, primaryScene, subSceneDesc, subSceneHandle);

	// Create a default camera for looking through the scene.
	createDefaultCamera(primaryScene, subSceneHandle);
}

void SceneDescLoader::createDefaultCamera(res::scene::Scene &primaryScene, res::scene::SubSceneHandle &subScene)
{
    auto cameraEntity{ primaryScene.createEntity(subScene, "DefaultCamera") };
    auto cameraComp{ cameraEntity.add<comp::Camera>() };

	// Create a default perspective camera, without forcing too many parameters.
	cameraComp->type = comp::Camera::CameraType::Perspective;
	cameraComp->firstParam.aspectRatio = 0.0f;
	cameraComp->secondParam.horFov = 0.0f;
	cameraComp->zNear = 0.1f;
	cameraComp->zFar = 100.0f;
}

void SceneDescLoader::createSceneResources(const SceneDescription &sceneDesc, res::scene::Scene &primaryScene)
{
	// Create vertex data buffers.
	for (const auto& buffer : sceneDesc.buffers())
	{ createBuffer(sceneDesc, primaryScene, buffer); }

	// Create buffer views.
	for (const auto& bufferView : sceneDesc.bufferViews())
	{ createBufferView(sceneDesc, primaryScene, bufferView); }

	// Create texture samplers.
	for (const auto& sampler : sceneDesc.samplers())
	{ createSampler(sceneDesc, primaryScene, sampler); }
	// Create default sampler for textures with undefined sampler.
	createDefaultSampler(sceneDesc, primaryScene);

	// Create textures.
	for (const auto& texture : sceneDesc.textures())
	{ createTexture(sceneDesc, primaryScene, texture); }

	// Create materials.
	for (const auto& material : sceneDesc.materials())
	{ createMaterial(sceneDesc, primaryScene, material); }

	// Create meshes.
	for (const auto& mesh : sceneDesc.meshes())
	{ createMesh(sceneDesc, primaryScene, mesh); }
}

void SceneDescLoader::createSceneEntities(const SceneDescription &sceneDesc, res::scene::Scene &primaryScene)
{
	// Create sub-scenes and their entities.
	for (const auto& subScene : sceneDesc.subScenes())
	{ createSubScene(sceneDesc, primaryScene, subScene); }

	if (primaryScene.numSubScenes() <= 0u)
	{ // If there are no scenes, create a default one.
        auto defaultSubScene{ primaryScene.createSubScene("Default") };
		// Create a default camera for looking through the scene.
		createDefaultCamera(primaryScene, defaultSubScene);
	}

    // Choose the active sub-scene.
	primaryScene.setCurrentSubScene(primaryScene.getSubScene(sceneDesc.activeSubScene()));
}

}

}

}
