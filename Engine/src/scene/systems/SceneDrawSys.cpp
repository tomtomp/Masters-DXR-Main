/**
 * @file scene/systems/SceneDrawSys.cpp
 * @author Tomas Polasek
 * @brief System for the main scene universe, which accumulates 
 * a list of entities which should be drawn.
 */

#include "stdafx.h"

#include "engine/scene/systems/SceneDrawSys.h"

namespace quark
{

namespace scene
{

namespace sys
{

SceneDrawSys::SceneDrawSys(const std::shared_ptr<res::scene::DrawableListMgr> &drawListMgr) :
    mCachedDrawables{ drawListMgr->create(DRAWABLE_LIST_NAME) }
{ }

const res::scene::DrawableListHandle &SceneDrawSys::getToDraw() 
{
    refreshToDraw();
    return mCachedDrawables;
}

void SceneDrawSys::refreshToDraw()
{
    auto &cachedDrawables{ mCachedDrawables.get() };

    if (groupChanged() || 
        cachedDrawables.revision() == 0u)
    { // We need to recalculate the list!
        cachedDrawables = calculateToDraw();
        resetGroupChanged();
    }
}

std::vector<res::scene::Drawable> SceneDrawSys::calculateToDraw()
{
    std::vector<res::scene::Drawable> result;

    for (auto &ent : foreach())
    { // Go through all of the renderable entities.
        auto *transform{ ent.get<comp::Transform>() };
        auto *renderable{ ent.get<comp::Renderable>() };

        // Add the entity to the list.
        result.emplace_back(res::scene::Drawable{ 
            transform->globalTransform, 
            renderable->meshHandle });
    }

    return result;
}

} 

} 

} 
