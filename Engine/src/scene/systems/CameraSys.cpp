/**
 * @file scene/systems/CameraSys.cpp
 * @author Tomas Polasek
 * @brief System which allows choosing a camera from 
 * within the current scene and allows access to its transform.
 */

#include "stdafx.h"

#include "engine/scene/systems/CameraSys.h"

namespace quark
{

namespace scene
{

namespace sys
{

CameraSys::CameraSys()
{ /* Automatic */ }

void CameraSys::refresh()
{
    mCameraChanged = false;

    if (!cameraChosen())
    { // Nothing to be done, if we haven't chosen a camera.
        return;
    }

    if (foreachRemoved().size() != 0u)
    { // If there were some cameras which were removed: 
        if (checkChosenCamera())
        { // And our chosen camera has been removed: 
            if (foreach().size() == 0u)
            { // We need at least one, to replace it!
                throw NoCamerasException("Chosen camera has been removed and there are none to replace it!");
            }

            // Else, we choose the first one as a replacement.
            mChosenCamera = *foreach().begin();
            mCameraChanged = true;
        }
    }
}

std::size_t CameraSys::numAvailableCameras()
{ return foreach().size(); }

CameraSys::CameraInfo CameraSys::cameraInfo(std::size_t idx)
{
    if (idx >= numAvailableCameras())
    { throw NoCamerasException("Provided camera index is out of bounds!"); }

    auto &cameraEntity{ foreach().begin()[idx] };

    const auto sceneInfo{ cameraEntity.get<comp::Scene>() };

    const auto cameraParams{ cameraEntity.get<comp::Camera>() };
    ASSERT_FAST(cameraParams);

    return { sceneInfo ? sceneInfo->name : "", *cameraParams };
}

void CameraSys::chooseCamera(std::size_t idx)
{
    if (idx >= numAvailableCameras())
    { throw NoCamerasException("Provided camera index is out of bounds!"); }

    mChosenCamera = foreach().begin()[idx];
    mCameraChanged = true;
}

comp::Transform &CameraSys::chosenCameraTransform()
{
    if (!cameraChosen())
    { throw NoCamerasException("Unable to get camera transform: No camera has been choosen!"); }

    const auto transform{ mChosenCamera.get<comp::Transform>() };
    ASSERT_SLOW(transform);

    return *transform;
}

const comp::Transform &CameraSys::chosenCameraTransform() const
{
    if (!cameraChosen())
    { throw NoCamerasException("Unable to get camera transform: No camera has been choosen!"); }

    const auto transform{ mChosenCamera.get<comp::Transform>() };
    ASSERT_SLOW(transform);

    return *transform;
}

void CameraSys::chosenCameraTransformChanged()
{
    if (!cameraChosen())
    { return; }

    const auto transform{ mChosenCamera.get<comp::Transform>() };
    ASSERT_SLOW(transform);

    transform->dirty = true;
    mCameraDirty = true;
}

comp::Camera &CameraSys::chosenCameraParameters()
{
    if (!cameraChosen())
    { throw NoCamerasException("Unable to get camera parameters: No camera has been chosen!"); }

    const auto parameters{ mChosenCamera.get<comp::Camera>() };
    ASSERT_SLOW(parameters);

    return *parameters;
}

bool CameraSys::cameraChosen() const
{ return mChosenCamera.validId() && mChosenCamera.active(); }

bool CameraSys::cameraChanged() const
{ return mCameraChanged; }

void CameraSys::setViewportSize(float width, float height)
{
    mWidth = width;
    mHeight = height;

    mCameraDirty = true;
}

void CameraSys::setFov(float horFov)
{
    mHorizontalFov = horFov;

    mCameraDirty = true;
}

const dxtk::math::Matrix &CameraSys::viewMatrix()
{
    generateCameraMatrices();
    return mCamera.v();
}

const dxtk::math::Matrix &CameraSys::projectionMatrix()
{
    generateCameraMatrices();
    return mCamera.p();
}

const dxtk::math::Matrix &CameraSys::viewProjectionMatrix()
{
    generateCameraMatrices();
    return mCamera.vp();
}

const helpers::math::Camera &CameraSys::camera() const
{ return mCamera; }

bool CameraSys::checkChosenCamera()
{
    for (const auto &e : foreachRemoved())
    {
        if (e == mChosenCamera)
        { return true; }
    }
    return false;
}

void CameraSys::generateCameraMatrices()
{
    // Do we need to recalculate?
    if (!cameraChosen() || !mCameraDirty)
    { return; }

    auto &transform{ chosenCameraTransform() };
    mCamera.setViewTransform(transform.globalTransform);

    auto cameraSpec{ chosenCameraParameters() };
    switch (cameraSpec.type)
    {
        case comp::Camera::CameraType::Orthographic: 
        {
            mCamera.setOrthoProj(
                cameraSpec.firstParam.width == 0.0f ? mWidth : cameraSpec.firstParam.width, 
                cameraSpec.secondParam.height == 0.0f ? mHeight : cameraSpec.secondParam.height, 
                cameraSpec.zNear, cameraSpec.zFar);
            break;
        }
        case comp::Camera::CameraType::Perspective:
        {
            if (cameraSpec.zFar == 0.0f)
            { // Infinite perspective projection.
                mCamera.setInfinPerspProj(
                    cameraSpec.firstParam.aspectRatio == 0.0f ? mHeight / mWidth : cameraSpec.firstParam.aspectRatio,
                    cameraSpec.secondParam.horFov == 0.0f ? mHorizontalFov : cameraSpec.secondParam.horFov,
                    cameraSpec.zNear);
            }
            else
            { // Finite perspective projection.
                mCamera.setFinPerspProj(
                    cameraSpec.firstParam.aspectRatio == 0.0f ? mHeight / mWidth : cameraSpec.firstParam.aspectRatio,
                    cameraSpec.secondParam.horFov == 0.0f ? mHorizontalFov : cameraSpec.secondParam.horFov,
                    cameraSpec.zNear, cameraSpec.zFar);
            }
            break;
        }
        case comp::Camera::CameraType::Unknown:
        default:
        {
            log<Warning>() << "Camera controller found unknown camera projection type!" << std::endl;
            break;
        }
    }

    // Recalculate the matrices.
    mCamera.vp();

    mCameraDirty = false;
}

}

}

}
