/**
 * @file scene/systems/EntityManipulatorSys.cpp
 * @author Tomas Polasek
 * @brief System which allows manipulation of entities 
 * and ways of their creation, deletion and other changes.
 */

#include "stdafx.h"

#include "engine/scene/systems/EntityManipulatorSys.h"

namespace quark
{

namespace scene
{

namespace sys
{

EntityManipulatorSys::EntityManipulatorSys()
{ /* Automatic */ }

}

}

}
