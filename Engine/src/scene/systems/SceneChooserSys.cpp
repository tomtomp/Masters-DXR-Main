/**
 * @file scene/systems/SceneChooserSys.cpp
 * @author Tomas Polasek
 * @brief System for the main scene universe, which activates only 
 * entities within the chosen scene.
 */

#include "stdafx.h"

#include "engine/scene/systems/SceneChooserSys.h"

namespace quark
{

namespace scene
{

namespace sys
{

SceneChooserSys::SceneChooserSys()
{ /* Automatic */ }

void SceneChooserSys::setActiveScene(SceneSpecification scene)
{
    if (scene != mChosenScene)
    {
        mChosenScene = scene;
        mChangedScenes = true;
    }
}

void SceneChooserSys::refresh()
{
    if (mChangedScenes)
    { processAll(); }
    else
    { processChanges(); }
}

void SceneChooserSys::processAll()
{
    for (auto &e : foreach())
    {
        if (e.get<comp::Scene>()->sceneSpec == mChosenScene)
        { e.activate(); }
        else
        { e.deactivate(); }
    }
    mChangedScenes = false;
}

void SceneChooserSys::processChanges()
{
    for (auto &e : foreachAdded())
    {
        if (e.get<comp::Scene>()->sceneSpec == mChosenScene)
        { e.activate(); }
        else
        { e.deactivate(); }
    }
}

}

}

}
