/**
 * @file scene/systems/SceneGraphDirtifierSys.cpp
 * @author Tomas Polasek
 * @brief System for the main scene universe, which marks entities 
 * dirty within the current sub-scene scene-graph. 
 */

#include "stdafx.h"

#include "engine/scene/systems/SceneGraphDirtifierSys.h"

namespace quark
{

namespace scene
{

namespace sys
{

SceneGraphDirtifierSys::SceneGraphDirtifierSys()
{ /* Automatic */ }

void SceneGraphDirtifierSys::refresh(const res::scene::SceneHandle &chosenScene)
{
    if (!chosenScene)
    { // No scene is chosen.
        return;
    }

    auto &scene{ chosenScene.get() };

    for (auto &e : foreach())
    { // Process all active entities with transform component.
        const auto transform{ e.get<comp::Transform>() };

        // Mark dirty transforms, passing their entities to the scene-graph.
        if (transform->dirty)
        { scene.transformChanged(e); }
    }
}

}

}

}
