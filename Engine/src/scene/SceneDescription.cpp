/**
 * @file resources/scene/SceneDescription.cpp
 * @author Tomas Polasek
 * @brief Container for a description of a scene, its
 * resources and sub-scenes.
 */

#include "stdafx.h"

#include "engine/scene/SceneDescription.h"

namespace quark
{

namespace scene
{

SceneDescription::PtrT SceneDescription::create()
{ return PtrT{ new SceneDescription() }; }

SceneDescription::~SceneDescription()
{ /* Automatic */ }

void SceneDescription::setName(const std::string &name)
{ mName = name; }

const std::string &SceneDescription::name() const
{ return mName; }

quark::scene::desc::BufferHandle SceneDescription::createBuffer(desc::Buffer &&bufferDesc)
{
    // Validate the buffer data: 
    if (bufferDesc.name.empty())
    { log<Warning>() << "Buffer has no name set, which may result in resource duplication!" << std::endl; }
    if (bufferDesc.data.empty())
    { return throwLogBadDesc("Provided buffer description is invalid: Data byte array is empty!"); }

    // Register the buffer.
    return pushDesc(std::move(bufferDesc), mBuffers);
}

desc::BufferHandle SceneDescription::createEmptyBuffer(desc::Buffer &&bufferDesc)
{
    // Validate the buffer data: 
    if (bufferDesc.name.empty())
    { log<Warning>() << "Buffer has no name set, which may result in resource duplication!" << std::endl; }

    // Register the buffer.
    return pushDesc(std::move(bufferDesc), mBuffers);
}

desc::BufferHandle SceneDescription::getBufferByName(const std::string& name) const
{ return getHandleFromName(name, mBuffers); }

quark::scene::desc::BufferViewHandle SceneDescription::createBufferView(desc::BufferView &&bufferViewDesc)
{
    // Validate the buffer view data: 
    if (bufferViewDesc.name.empty())
    { log<Warning>() << "Buffer view has no name set, which may result in resource duplication!" << std::endl; }
    if (!handleValidNotNull(bufferViewDesc.buffer, mBuffers))
    { return throwLogBadDesc("Provided buffer view description is invalid: Buffer handle must be specified and valid!"); }
    if (bufferViewDesc.elementCount == 0u)
    { return throwLogBadDesc("Provided buffer view description is invalid: At least one element must be present!"); }

    // Register the buffer view.
    return pushDesc(std::move(bufferViewDesc), mBufferViews);
}

desc::BufferViewHandle SceneDescription::getBufferViewByName(const std::string& name) const
{ return getHandleFromName(name, mBufferViews); }

quark::scene::desc::SamplerHandle SceneDescription::createSampler(desc::Sampler &&samplerDesc)
{
    // Validate the sampler data: 
    if (samplerDesc.name.empty())
    { log<Warning>() << "Sampler has no name set, which may result in resource duplication!" << std::endl; }

    // Register the sampler.
    return pushDesc(std::move(samplerDesc), mSamplers);
}

desc::SamplerHandle SceneDescription::getSamplerByName(const std::string& name) const
{ return getHandleFromName(name, mSamplers); }

quark::scene::desc::TextureHandle SceneDescription::createTexture(desc::Texture &&textureDesc)
{
    // Validate the texture data: 
    if (textureDesc.name.empty())
    { log<Warning>() << "Texture has no name set, which may result in resource duplication!" << std::endl; }
    if (textureDesc.elementFormat.format == rndr::ComponentFormat::Unknown || textureDesc.elementFormat.numComponents == 0u)
    { return throwLogBadDesc("Provided texture description is invalid: Texture must have element format specified and non-zero number of channels!"); }
    if (!handleValid(textureDesc.sampler, mSamplers))
    { return throwLogBadDesc("Provided texture description is invalid: Sampler must be Null, or otherwise valid handle!"); }
    if (textureDesc.data.empty())
    { return throwLogBadDesc("Provided texture description is invalid: Data array must not be empty!"); }
    if (textureDesc.width == 0u && textureDesc.height == 0u)
    { return throwLogBadDesc("Provided texture description is invalid: Texture dimensions must both be non-zero!"); }
    if (textureDesc.generateMipMaps)
    { return throwLogBadDesc("Provided texture description is invalid: Automatic mip-map generation is not implemented just yet!"); }

    // Register the texture.
    return pushDesc(std::move(textureDesc), mTextures);
}

desc::TextureHandle SceneDescription::getTextureByName(const std::string& name) const
{ return getHandleFromName(name, mSamplers); }

desc::MaterialHandle SceneDescription::createMaterial(desc::Material &&materialDesc)
{
    // Validate the material data: 
    if (materialDesc.name.empty())
    { log<Warning>() << "Material has no name set, which may result in resource duplication!" << std::endl; }
    if (!handleValid(materialDesc.normalTexture, mTextures))
    { return throwLogBadDesc("Provided primitive description is invalid: Normal texture must be Null or otherwise valid!"); }
    if (!handleValid(materialDesc.occlusionTexture, mTextures))
    { return throwLogBadDesc("Provided primitive description is invalid: Occlusion texture must be Null or otherwise valid!"); }
    if (!handleValid(materialDesc.emissiveTexture, mTextures))
    { return throwLogBadDesc("Provided primitive description is invalid: Emissive texture must be Null or otherwise valid!"); }
    if (!handleValidNotNull(materialDesc.pbr.baseColorTexture, mTextures))
    { return throwLogBadDesc("Provided material description is invalid: Valid base color texture must be specified!"); }
    if (!handleValid(materialDesc.pbr.propertyTexture, mTextures))
    { return throwLogBadDesc("Provided primitive description is invalid: Property texture must be Null or otherwise valid!"); }

    // Register the material.
    return pushDesc(std::move(materialDesc), mMaterials);
}

desc::MaterialHandle SceneDescription::getMaterialByName(const std::string& name) const
{ return getHandleFromName(name, mMaterials); }

quark::scene::desc::PrimitiveHandle SceneDescription::createPrimitive(desc::Primitive &&primitiveDesc)
{
    // Validate the primitive data: 
    if (primitiveDesc.name.empty())
    { log<Warning>() << "Primitive has no name set, which may result in resource duplication!" << std::endl; }
    if (!handleValidNotNull(primitiveDesc.indiceBuffer, mBufferViews))
    { return throwLogBadDesc("Provided primitive description is invalid: Valid index buffer must be specified!"); }
    if (!handleValidNotNull(primitiveDesc.positionBuffer, mBufferViews))
    { return throwLogBadDesc("Provided primitive description is invalid: Valid position buffer must be specified!"); }
    if (!handleValid(primitiveDesc.normalBuffer, mBufferViews))
    { return throwLogBadDesc("Provided primitive description is invalid: Normal buffer must be Null or otherwise valid!"); }
    if (!handleValid(primitiveDesc.tangentBuffer, mBufferViews))
    { return throwLogBadDesc("Provided primitive description is invalid: Tangent buffer must be Null or otherwise valid!"); }
    if (!handleValidNotNull(primitiveDesc.texCoordBuffer, mBufferViews))
    { return throwLogBadDesc("Provided primitive description is invalid: Valid texture coordinate buffer must be specified!"); }
    if (!handleValid(primitiveDesc.colorBuffer, mBufferViews))
    { return throwLogBadDesc("Provided primitive description is invalid: Color buffer must be Null or otherwise valid!"); }
    if (!handleValidNotNull(primitiveDesc.material, mMaterials))
    { return throwLogBadDesc("Provided primitive description is invalid: Material must be specified!"); }

    // Register the primitive.
    return pushDesc(std::move(primitiveDesc), mPrimitives);
}

desc::PrimitiveHandle SceneDescription::getPrimitiveByName(const std::string& name) const
{ return getHandleFromName(name, mPrimitives); }

quark::scene::desc::MeshHandle SceneDescription::createMesh(desc::Mesh &&meshDesc)
{
    // Validate the mesh data: 
    if (meshDesc.name.empty())
    { log<Warning>() << "Mesh has no name set, which may result in resource duplication!" << std::endl; }
    if (meshDesc.primitives.empty())
    { return throwLogBadDesc("Provided mesh description is invalid: At least one primitive must be specified!"); }
    for (const auto primitiveHandle : meshDesc.primitives)
    {
        if (!handleValidNotNull(primitiveHandle, mPrimitives))
        { return throwLogBadDesc("Provided mesh description is invalid: All primitive handles must be valid!"); }
    }

    // Register the mesh.
    return pushDesc(std::move(meshDesc), mMeshes);
}

desc::MeshHandle SceneDescription::getMeshByName(const std::string& name) const
{ return getHandleFromName(name, mMeshes); }

quark::scene::desc::CameraHandle SceneDescription::createCamera(desc::Camera &&cameraDesc)
{
    // Validate the camera data: 
    if (cameraDesc.name.empty())
    { log<Warning>() << "Camera has no name set, which may result in resource duplication!" << std::endl; }

    // Register the camera.
    return pushDesc(std::move(cameraDesc), mCameras);
}

desc::CameraHandle SceneDescription::amendCamera(desc::Camera &&cameraDesc)
{
    // Validate the camera data: 
    if (cameraDesc.name.empty())
    { log<Warning>() << "Camera has no name set, which may result in resource duplication!" << std::endl; }

    // Register the camera.
    return pushReplaceDesc(std::move(cameraDesc), mCameras);
}

desc::CameraHandle SceneDescription::getCameraByName(const std::string& name) const
{ return getHandleFromName(name, mCameras); }

quark::scene::desc::EntityHandle SceneDescription::createEntity(desc::Entity &&entityDesc)
{
    // Validate the entity data: 
    if (entityDesc.name.empty())
    { log<Warning>() << "Entity has no name set, which may result in resource duplication!" << std::endl; }
    if (!handleValidNotNull(mCurrentSubScene, mSubScenes))
    { return throwLogBadDesc("Sub-scene must be selected before creating entities!"); }
    auto &entityHolder{ getDesc(mCurrentSubScene, mSubScenes).entities };

    if (!handleValid(entityDesc.mesh, mMeshes))
    { return throwLogBadDesc("Provided entity description is invalid: Mesh must be Null or otherwise valid handle!"); }
    if (!handleValid(entityDesc.parent, entityHolder))
    { return throwLogBadDesc("Provided entity description is invalid: Parent must be Null or otherwise valid handle!"); }
    if (!handleValid(entityDesc.camera, mCameras))
    { return throwLogBadDesc("Provided entity description is invalid: Camera must be Null or otherwise valid handle!"); }

    // Register the entity.
    return pushDesc(std::move(entityDesc), entityHolder);
}

desc::EntityHandle SceneDescription::amendEntity(desc::Entity &&entityDesc)
{
    // Validate the entity data: 
    if (entityDesc.name.empty())
    { log<Warning>() << "Entity has no name set, which may result in resource duplication!" << std::endl; }
    if (!handleValidNotNull(mCurrentSubScene, mSubScenes))
    { return throwLogBadDesc("Sub-scene must be selected before creating entities!"); }
    auto &entityHolder{ getDesc(mCurrentSubScene, mSubScenes).entities };

    if (!handleValid(entityDesc.mesh, mMeshes))
    { return throwLogBadDesc("Provided entity description is invalid: Mesh must be Null or otherwise valid handle!"); }
    if (!handleValid(entityDesc.parent, entityHolder))
    { return throwLogBadDesc("Provided entity description is invalid: Parent must be Null or otherwise valid handle!"); }
    if (!handleValid(entityDesc.camera, mCameras))
    { return throwLogBadDesc("Provided entity description is invalid: Camera must be Null or otherwise valid handle!"); }

    // Register the entity.
    return pushReplaceDesc(std::move(entityDesc), entityHolder);
}

desc::EntityHandle SceneDescription::getEntityByName(const std::string& name) const
{ return getHandleFromName(name, getDesc(mCurrentSubScene, mSubScenes).entities); }

void SceneDescription::clearEntities()
{
    if (handleValidNotNull(mCurrentSubScene, mSubScenes))
    {
        auto &entityHolder{ getDesc(mCurrentSubScene, mSubScenes).entities };
        clearDescs(entityHolder);
    }
}

desc::SubSceneHandle SceneDescription::createSubScene(const std::string &name)
{ 
    // Validate the sub-scene data: 
    if (name.empty())
    { log<Warning>() << "Sub-scene has no name set, which may result in resource duplication!" << std::endl; }

    // Register the sub-scene.
    return pushDesc(desc::SubScene{ name }, mSubScenes); 
}

desc::SubSceneHandle SceneDescription::getSubSceneByName(const std::string& name) const
{ return getHandleFromName(name, mSubScenes); }

void SceneDescription::activateSubScene(desc::SubSceneHandle subScene)
{
    if (!handleValidNotNull(subScene, mSubScenes))
    { throwLogBadDesc("Provided sub-scene handle is not valid!"); }

    mCurrentSubScene = subScene;
}

std::size_t SceneDescription::activeSubScene() const
{ return handleToIndex(mCurrentSubScene); }

bool SceneDescription::handleNotNull(desc::DescHandleBase handle)
{ return handle != desc::NULL_HANDLE_VALUE; }

std::size_t SceneDescription::handleToIndex(desc::DescHandleBase handle)
{
    ASSERT_FAST(handleNotNull(handle));
    return static_cast<std::size_t>(handle - 1u);
}

desc::DescHandleBase SceneDescription::indexToHandle(std::size_t index)
{ return static_cast<desc::DescHandleBase>(index + 1u); }

desc::DescHandleBase SceneDescription::throwLogBadDesc(const char *msg)
{
    if constexpr (THROW_BAD_DESC)
    { throw InvalidDescException(msg); }
    else
    { 
        log<Error>() << msg << std::endl; 
        return desc::NULL_HANDLE_VALUE;
    }
}

SceneDescription::SceneDescription()
{ /* Automatic */ }

} 

}
