/**
 * @file scene/ECS.cpp
 * @author Tomas Polasek
 * @brief Definition of Entity-Component-System universe.
 */

#include "stdafx.h"

#include "engine/scene/ECS.h"

#include "engine/scene/SceneComponents.h"
#include "engine/scene/ComponentRegister.h"

namespace quark
{

namespace scene
{

template ent::Universe<SceneUniverse>;

void SceneUniverse::registerSceneComponents()
{
    // Register the common components.
    ComponentRegister::registerComponent<comp::Transform>();
    ComponentRegister::registerComponent<comp::Scene>();
    ComponentRegister::registerComponent<comp::Renderable>();
    ComponentRegister::registerComponent<comp::Camera>();

    // Finalize the registration.
    ComponentRegister::performRegistration(*this);
}

}

}
