/**
 * @file helpers/d3d12/D3D12Helpers.cpp
 * @author Tomas Polasek
 * @brief Helper functions for Direct3D 12.
 */

#include "stdafx.h"

#include "engine/helpers/d3d12/D3D12Helpers.h"

namespace quark
{

namespace helpers
{

namespace d3d12
{

HANDLE D3D12Helpers::createFenceEvent()
{
    const auto event { ::CreateEvent(
        // Cannot be inherited by a child.
        nullptr,
        // Automatic reset.
        false,
        // Initial state is un-signaled.
        false,
        // No name.
        nullptr)};

    if (!event)
    {
        throw util::winexception("Failed to create fence event!");
    }

    return event;
}

} 

} 

} 
