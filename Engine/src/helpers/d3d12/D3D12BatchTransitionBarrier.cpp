/**
 * @file helpers/d3d12/D3D12BatchTransitionBarrier.cpp
 * @author Tomas Polasek
 * @brief Helper D3D12 class used to batch transition 
 * barriers into one ResourceBarrier command.
 */

#include "stdafx.h"

#include "engine/helpers/d3d12/D3D12BatchTransitionBarrier.h"

namespace quark
{

namespace helpers
{

namespace d3d12
{

D3D12BatchTransitionBarrier::ResourceTransitionRequest::ResourceTransitionRequest(
    res::d3d12::D3D12Resource &resource, ::D3D12_RESOURCE_STATES state, 
    ::UINT subRes, ::D3D12_RESOURCE_BARRIER_FLAGS flag) : 
    res{ &resource }, targetState{ state }, subResource{ subRes }, flags{ flag }
{ }

D3D12BatchTransitionBarrier::ResourceTransitionRequest::ResourceTransitionRequest(
    res::d3d12::D3D12Resource::PtrT &resource, ::D3D12_RESOURCE_STATES state, 
    ::UINT subRes, ::D3D12_RESOURCE_BARRIER_FLAGS flag) : 
    res{ resource.get() }, targetState{ state }, subResource{ subRes }, flags{ flag }
{ }

void D3D12BatchTransitionBarrier::transitionTo(::D3D12_RESOURCE_STATES targetState, 
    res::d3d12::D3D12CommandList &cmdList, std::initializer_list<ResourceTransitionRequest> requests, 
    ::UINT subResource, ::D3D12_RESOURCE_BARRIER_FLAGS flags)
{
    std::vector<::CD3DX12_RESOURCE_BARRIER> barriers{ };
    barriers.reserve(requests.size());

    for (auto &request : requests)
    { // Prepare a barrier for each request.
        // Skip invalid resources.
        if (!request.res || !request.res->valid() || request.res->mState == targetState)
        { continue; }

        barriers.emplace_back(
            ::CD3DX12_RESOURCE_BARRIER::Transition(
                request.res->get(), 
                request.res->mState, targetState, 
                subResource, flags));

        // Presume we will finish successfully.
        request.res->mState = targetState;
    }

    if (!barriers.empty())
    { // Record the barrier commands.
        cmdList->ResourceBarrier(static_cast<::UINT>(barriers.size()), barriers.data());
    }
}

}

}

}
