/**
 * @file helpers/d3d12/D3D12RTHitGroupRegistry.cpp
 * @author Tomas Polasek
 * @brief Registration of hit groups and their shaders.
 */

#include "stdafx.h"

#ifdef ENGINE_RAY_TRACING_ENABLED

#include "engine/helpers/d3d12/D3D12RTHitGroupRegistry.h"

namespace quark
{

namespace helpers
{

namespace d3d12
{

D3D12RTHitGroupRegistry::D3D12RTHitGroupRegistry()
{ clear(); }

D3D12RTHitGroupRegistry::~D3D12RTHitGroupRegistry()
{ /* Automatic */ }

void D3D12RTHitGroupRegistry::clear()
{ mRegistry.clear(); }

void D3D12RTHitGroupRegistry::registerHitGroup(
    const std::wstring &hitGroupName, const std::wstring &closestHitName,
    const std::wstring &anyHitName, const std::wstring &intersectionName, 
    ::D3D12_HIT_GROUP_TYPE type)
{
    const auto findIt{ mRegistry.find(hitGroupName) };
    if (findIt != mRegistry.end())
    { throw AlreadyRegisteredException("Unable to register hit group: Name is already in use!"); }

    mRegistry.emplace(hitGroupName, HitGroupRecord{ closestHitName, anyHitName, intersectionName, type });
}

void D3D12RTHitGroupRegistry::HitGroupRecord::printInfo(const std::wstring &name) const
{
    log<Info>() << "Hit Group: "
        "\n\tName: \"" << util::toString(name) << "\""
        "\n\tClosest Hit Name: \"" << util::toString(closestHitName) << "\""
        "\n\tAny Hit Name: \"" << util::toString(anyHitName) << "\""
        "\n\tIntersection Name: \"" << util::toString(intersectionName) << "\""
        "\n\tType: ";
    switch (type)
    {
        case D3D12_HIT_GROUP_TYPE_TRIANGLES:
        {
            lognp<Info>() << "Triangles";
            break;
        }
        case D3D12_HIT_GROUP_TYPE_PROCEDURAL_PRIMITIVE: 
        {
            lognp<Info>() << "Procedural";
            break;
        }
        default: 
        {
            lognp<Info>() << "Unknown";
            break;
        }
    }
    lognp<Info>() << std::endl;
}

}

}

}

#endif
