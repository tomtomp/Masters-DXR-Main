/**
 * @file helpers/d3d12/D3D12CommittedAllocator.cpp
 * @author Tomas Polasek
 * @brief Allocator for Direct3D 12 committed resources.
 */

#include "stdafx.h"

#include "engine/helpers/d3d12/D3D12CommittedAllocator.h"

namespace quark
{

namespace helpers
{

namespace d3d12
{

D3D12CommittedAllocator::PtrT D3D12CommittedAllocator::create()
{ return PtrT{ new D3D12CommittedAllocator() }; }

D3D12CommittedAllocator::PtrT D3D12CommittedAllocator::create(res::d3d12::D3D12Device &device,
    const::CD3DX12_HEAP_PROPERTIES &props, ::D3D12_HEAP_FLAGS flags)
{ return PtrT{ new D3D12CommittedAllocator(device, props, flags) }; }

D3D12CommittedAllocator::PtrT D3D12CommittedAllocator::create(res::d3d12::D3D12Device &device, 
    const::D3D12_HEAP_PROPERTIES &props, ::D3D12_HEAP_FLAGS flags)
{ return PtrT{ new D3D12CommittedAllocator(device, props, flags) }; }

D3D12CommittedAllocator::D3D12CommittedAllocator()
{ /* Automatic */ }

D3D12CommittedAllocator::D3D12CommittedAllocator(res::d3d12::D3D12Device &device, 
    const ::CD3DX12_HEAP_PROPERTIES &props, ::D3D12_HEAP_FLAGS flags) :
    mDevice{ &device }, mHeapProps{ props }, mHeapFlags{ flags }
{ }

D3D12CommittedAllocator::D3D12CommittedAllocator(res::d3d12::D3D12Device &device, 
    const ::D3D12_HEAP_PROPERTIES &props, ::D3D12_HEAP_FLAGS flags) :
    mDevice{ &device }, mHeapProps{ props }, mHeapFlags{ flags }
{ }

void D3D12CommittedAllocator::allocate(const D3D12_RESOURCE_DESC &desc, D3D12_RESOURCE_STATES initState,
    const D3D12_CLEAR_VALUE *optimizedClearValue, const IID &riid, void **ptr)
{
    ASSERT_FAST(mDevice);

    util::throwIfFailed((*mDevice)->CreateCommittedResource(&mHeapProps, mHeapFlags,
        &desc, initState, optimizedClearValue, riid, ptr), 
        "Failed to allocate committed resource!");
}

void D3D12CommittedAllocator::deallocate(const IID &riid, void **ptr)
{
    UNUSED(riid);
    UNUSED(ptr);
    // No action is necessary.
}

}

}

}
