/**
 * @file helpers/d3d12/D3D12RTRootSignatureRegistry.cpp
 * @author Tomas Polasek
 * @brief Wrapper around a list of root signatures and their 
 * associated shader names.
 */

#include "stdafx.h"

#ifdef ENGINE_RAY_TRACING_ENABLED

#include "engine/helpers/d3d12/D3D12RTRootSignatureRegistry.h"

namespace quark
{

namespace helpers
{

namespace d3d12
{

D3D12RTRootSignatureRegistry::D3D12RTRootSignatureRegistry()
{ clear(); }

D3D12RTRootSignatureRegistry::~D3D12RTRootSignatureRegistry()
{ /* Automatic */ }

void D3D12RTRootSignatureRegistry::clear()
{
    mRegistry.clear();
    mPtrToRegistry.clear();
    mNameToRegistry.clear();
}

void D3D12RTRootSignatureRegistry::registerRootSignature(
    res::d3d12::D3D12RootSignature &rootSignature,
    const std::wstring &name)
{
    // Create it or just get to check the name.
    auto association{ createOrGet(rootSignature.get()) };

    if (!name.empty())
    { // Only when user specifies a name.
        registerName(association, name);
    }
}

void D3D12RTRootSignatureRegistry::associateWith(
    res::d3d12::D3D12RootSignature &rootSignature,
    const std::wstring &shader)
{ associateWith(createOrGet(rootSignature.get()), shader); }

void D3D12RTRootSignatureRegistry::associateWith(
    res::d3d12::D3D12RootSignature &rootSignature,
    const std::initializer_list<std::wstring> &shaders)
{ associateWith(createOrGet(rootSignature.get()), shaders); }

void D3D12RTRootSignatureRegistry::associateWith(
    ComPtr<D3D12RootSignatureT> &rootSignature, 
    const std::wstring &shader)
{ associateWith(createOrGet(rootSignature.Get()), shader); }

void D3D12RTRootSignatureRegistry::associateWith(
    const std::wstring &rootSignatureName, 
    const std::wstring &shader)
{ associateWith(get(rootSignatureName), shader); }

void D3D12RTRootSignatureRegistry::RootSignatureAssociation::printInfo() const
{
    log<Info>() << "Root Signature: "
        "\n\tName: \"" << util::toString(rootSignatureName) << "\""
        "\n\tCompiled [ptr]: " << rootSignature.Get() <<
        "\n\tAssociated Shaders: {";
    for (const auto &shader : associatedShaders)
    { lognp<Info>() << "\n\t\t\"" << util::toString(shader) << "\""; }
    lognp<Info>() << "\n}" << std::endl;
}

D3D12RTRootSignatureRegistry::RootSignatureAssociation &D3D12RTRootSignatureRegistry::createOrGet(
    D3D12RootSignatureT *rootSignature)
{
    const auto findIt{ mPtrToRegistry.find(rootSignature) };
    if (findIt != mPtrToRegistry.end())
    { // Already created, we are done.
        return *findIt->second;
    }

    // Create the record.
    mRegistry.emplace_back(RootSignatureAssociation{ rootSignature });
    // Associate with its pointer.
    mPtrToRegistry.emplace(rootSignature, &(mRegistry.back()));

    return mRegistry.back();
}

void D3D12RTRootSignatureRegistry::registerName(RootSignatureAssociation &association,
    const std::wstring &rootSignatureName)
{
    // Check whether the association is already registered under different name.
    if (!association.rootSignatureName.empty() && 
        association.rootSignatureName != rootSignatureName)
    { throw AlreadyRegisteredException("Unable to register a root signature: Already registered under different name!"); }

    // Check whether the name is unused.
    const auto findIt{ mNameToRegistry.find(rootSignatureName) };
    if (findIt != mNameToRegistry.end())
    {
        if (findIt->second == &association)
        { // Nothing to be done, it is already registered...
            return;
        }
        else
        { // Name used twice.
            throw AlreadyRegisteredException("Unable to register a root signature: Name is already used!");
        }
    }

    // Set name and register.
    association.rootSignatureName = rootSignatureName;
    mNameToRegistry.emplace(rootSignatureName, &association);
}

D3D12RTRootSignatureRegistry::RootSignatureAssociation &D3D12RTRootSignatureRegistry::get(
    const std::wstring &rootSignatureName)
{
    const auto findIt{ mNameToRegistry.find(rootSignatureName) };
    if (findIt == mNameToRegistry.end())
    { throw UnknownNameException("Unable to get a root signature by name: Name is not registered!"); }

    return *findIt->second;
}

void D3D12RTRootSignatureRegistry::associateWith(RootSignatureAssociation &association, 
    const std::wstring &shader)
{ association.associatedShaders.emplace_back(shader); }

void D3D12RTRootSignatureRegistry::associateWith(RootSignatureAssociation &association,
    const std::initializer_list<std::wstring> &shaders)
{
    for (const auto &shader : shaders)
    { associateWith(association, shader); }
}

}

}

}

#endif 
