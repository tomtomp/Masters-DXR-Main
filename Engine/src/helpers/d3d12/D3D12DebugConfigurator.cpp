/**
 * @file helpers/d3d12/D3D12DebugConfigurator.cpp
 * @author Tomas Polasek
 * @brief Direct3D 12 debugging configurator.
 */

#include "stdafx.h"

#include "engine/helpers/d3d12/D3D12DebugConfigurator.h"

namespace quark
{

namespace helpers
{

namespace d3d12
{

D3D12DebugConfigurator::D3D12DebugConfigurator(bool debugLayer, bool gpuValidation) :
    mDebugLayerEnabled{ debugLayer}, mGpuValidationEnabled{ gpuValidation }
{
    if (debugLayer)
    { enableDebugLayer(); }

    if (gpuValidation)
    { enableGpuValidation(); }
}

::D3D12_GPU_BASED_VALIDATION_SHADER_PATCH_MODE D3D12DebugConfigurator::setCmdListValidation(
    ::ID3D12CommandList *cmdList,
    ::D3D12_GPU_BASED_VALIDATION_SHADER_PATCH_MODE mode) const
{
    auto result{ ::D3D12_GPU_BASED_VALIDATION_SHADER_PATCH_MODE_NONE };

    if (mDebugLayerEnabled && mGpuValidationEnabled)
    {
        ComPtr<::ID3D12DebugCommandList1> debugCmdList;
        util::throwIfFailed(cmdList->QueryInterface(IID_PPV_ARGS(&debugCmdList)), 
            "Failed to get the D3D12 debug command list interface!");

        // Backup the original mode.
        ::D3D12_DEBUG_COMMAND_LIST_GPU_BASED_VALIDATION_SETTINGS settings{ };
        debugCmdList->GetDebugParameter(
            ::D3D12_DEBUG_COMMAND_LIST_PARAMETER_GPU_BASED_VALIDATION_SETTINGS,
            &settings, sizeof(settings));
        result = settings.ShaderPatchMode;

        // Set the new one.
        settings.ShaderPatchMode = mode;
        debugCmdList->SetDebugParameter(
            ::D3D12_DEBUG_COMMAND_LIST_PARAMETER_GPU_BASED_VALIDATION_SETTINGS, 
            &settings, sizeof(settings));
    }

    return result;
}

void D3D12DebugConfigurator::enableDebugLayer()
{
    ComPtr<::ID3D12Debug> debugInterface;
    util::throwIfFailed(D3D12GetDebugInterface(IID_PPV_ARGS(&debugInterface)), 
        "Failed to get the D3D12 debug interface!");
    debugInterface->EnableDebugLayer();
}

void D3D12DebugConfigurator::enableGpuValidation()
{
    ComPtr<::ID3D12Debug> debugInterface;
    ComPtr<::ID3D12Debug1> debugInterface1;
    util::throwIfFailed(D3D12GetDebugInterface(IID_PPV_ARGS(&debugInterface)), 
        "Failed to get the D3D12 debug interface!");
    util::throwIfFailed(debugInterface->QueryInterface(IID_PPV_ARGS(&debugInterface1)), 
        "Failed to get the D3D12 debug interface1");
    debugInterface1->SetEnableGPUBasedValidation(true);
}

} 

} 

} 
