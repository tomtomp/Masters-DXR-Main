/**
 * @file helpers/d3d12/D3D12RTShaderLibraryRegistry.cpp
 * @author Tomas Polasek
 * @brief Wrapper around a list of shader libraries and their 
 * associated entry points.
 */

#include "stdafx.h"

#ifdef ENGINE_RAY_TRACING_ENABLED

#include "engine/helpers/d3d12/D3D12RTShaderLibraryRegistry.h"

namespace quark
{

namespace helpers
{

namespace d3d12
{

D3D12RTShaderLibraryRegistry::D3D12RTShaderLibraryRegistry()
{ clear(); }

D3D12RTShaderLibraryRegistry::~D3D12RTShaderLibraryRegistry()
{ /* Automatic */ }

void D3D12RTShaderLibraryRegistry::clear()
{
    mRegistry.clear();
    mPtrToRegistry.clear();
    mNameToRegistry.clear();
    mEntryPointToRegistry.clear();
}

void D3D12RTShaderLibraryRegistry::registerShaderLibrary(
    const ShaderLibrary &library, const std::wstring &name)
{
    // Create it or just get to check the name.
    auto &association{ createOrGet(library) };

    if (!name.empty())
    { // Only when user specifies a name.
        registerName(association, name);
    }
}

void D3D12RTShaderLibraryRegistry::associateEntryPoints(const ShaderLibrary &library,
    const std::initializer_list<std::wstring> &entryPoints)
{ associateWith(createOrGet(library), entryPoints); }

void D3D12RTShaderLibraryRegistry::associateEntryPoints(const std::wstring &libraryName,
    const std::initializer_list<std::wstring> &entryPoints)
{ associateWith(get(libraryName), entryPoints); }

void D3D12RTShaderLibraryRegistry::ShaderLibraryAssociation::printInfo() const
{
    log<Info>() << "Shader library: "
        "\n\tName: \"" << util::toString(shaderLibraryName) << "\""
        "\n\tByte Code [ptr]: " << static_cast<void*>(byteCode.get()) <<
        "\n\tByteCode Length [B]: " << byteCodeLength <<
        "\n\tAssociated Entry Points: {";
    for (const auto &entryPoint : associatedEntryPoints)
    { lognp<Info>() << "\n\t\t\"" << util::toString(entryPoint) << "\""; }
    lognp<Info>() << "\n}" << std::endl;
}

D3D12RTShaderLibraryRegistry::ShaderLibraryAssociation &D3D12RTShaderLibraryRegistry::createOrGet(
    const ShaderLibrary &library)
{
    const auto findIt{ mPtrToRegistry.find(library.byteCode) };
    if (findIt != mPtrToRegistry.end())
    { // Already created, we are done.
        return *findIt->second;
    }

    // Make a safe copy.
    auto memory{ std::make_unique<uint8_t[]>(library.byteCodeLength) };
    std::memcpy(memory.get(), library.byteCode, library.byteCodeLength);

    // Create the record.
    mRegistry.emplace_back(ShaderLibraryAssociation{ std::move(memory), library.byteCodeLength });
    // Associate with its pointer.
    mPtrToRegistry.emplace(library.byteCode, &(mRegistry.back()));

    return mRegistry.back();
}

void D3D12RTShaderLibraryRegistry::registerName(ShaderLibraryAssociation &association,
    const std::wstring &shaderLibraryName)
{
    // Check whether the association is already registered under different name.
    if (!association.shaderLibraryName.empty() && 
        association.shaderLibraryName != shaderLibraryName)
    { throw AlreadyRegisteredException("Unable to register a shader library: Already registered under different name!"); }

    // Check whether the name is unused.
    const auto findIt{ mNameToRegistry.find(shaderLibraryName) };
    if (findIt != mNameToRegistry.end())
    {
        if (findIt->second == &association)
        { // Nothing to be done, it is already registered...
            return;
        }
        else
        { // Name used twice.
            throw AlreadyRegisteredException("Unable to register a shader library: Name is already used!");
        }
    }

    // Set name and register.
    association.shaderLibraryName = shaderLibraryName;
    mNameToRegistry.emplace(shaderLibraryName, &association);
}

D3D12RTShaderLibraryRegistry::ShaderLibraryAssociation &D3D12RTShaderLibraryRegistry::get(
    const std::wstring &shaderLibraryName)
{
    const auto findIt{ mNameToRegistry.find(shaderLibraryName) };
    if (findIt == mNameToRegistry.end())
    { throw UnknownNameException("Unable to get a shader library by name: Name is not registered!"); }

    return *findIt->second;
}

void D3D12RTShaderLibraryRegistry::associateWith(ShaderLibraryAssociation &association, 
    const std::initializer_list<std::wstring> &entryPoints)
{
    for (const auto &entryPoint : entryPoints)
    {
        const auto findIt{ mEntryPointToRegistry.find(entryPoint) };
        if (findIt != mEntryPointToRegistry.end() && findIt->second != &association)
        { throw AlreadyRegisteredException("Unable to register an entry point: Already registered under different library!"); }

        mEntryPointToRegistry.emplace(entryPoint, &association);
        association.associatedEntryPoints.emplace_back(entryPoint);
    }
}

}

}

}

#endif 
