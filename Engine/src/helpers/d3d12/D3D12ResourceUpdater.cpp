/**
 * @file helpers/d3d12/D3D12ResourceUpdater.cpp
 * @author Tomas Polasek
 * @brief Helper class for updating D3D12 resources.
 */

#include "stdafx.h"

#include "engine/helpers/d3d12/D3D12ResourceUpdater.h"

namespace quark
{

namespace helpers
{

namespace d3d12
{

D3D12ResourceUpdater::D3D12ResourceUpdater()
{ /* Automatic */ }

D3D12ResourceUpdater::D3D12ResourceUpdater(res::d3d12::D3D12Device &device)
{ initialize(device); }

D3D12ResourceUpdater::D3D12ResourceUpdater(D3D12CommittedAllocator::PtrT allocator) :
    mAllocator{ allocator }
{
    if (!allocatorMeetsRequirements(*mAllocator))
    {
        throw IncompatibleAllocatorException("Allocator does not meet ResourceUpdater requirements!");
    }
}

D3D12ResourceUpdater::PtrT D3D12ResourceUpdater::create(res::d3d12::D3D12Device &device)
{ return PtrT{ new D3D12ResourceUpdater(device) }; }

D3D12ResourceUpdater::PtrT D3D12ResourceUpdater::create(D3D12CommittedAllocator::PtrT allocator)
{ return PtrT{ new D3D12ResourceUpdater(allocator) }; }

D3D12ResourceUpdater::~D3D12ResourceUpdater()
{
    // The buffers should be explicitly released by calling commandListExecuted().
    if (mStashedBuffers.empty())
    {
        log<Debug>() << "Destructing ResourceUpdater which still contains some stashed buffers!" << std::endl;
    }
}

void D3D12ResourceUpdater::initialize(res::d3d12::D3D12Device &device)
{
    mAllocator = D3D12CommittedAllocator::create(device, requiredHeapProps(), requiredHeapFlags());
    mDevice = &device;
}

void D3D12ResourceUpdater::copyToResource(const void *data, std::size_t sizeInBytes, 
    res::d3d12::D3D12Resource &dest, res::d3d12::D3D12CommandList &cmdList)
{
    if (data == nullptr || sizeInBytes == 0u)
    { return; }

    std::vector<::D3D12_SUBRESOURCE_DATA> subresourceData{ };
    const auto srcData{ static_cast<const uint8_t*>(data) };

    const auto mipLevels{ dest.desc().MipLevels ? dest.desc().MipLevels : 1u };
    const auto format{ dest.desc().Format };
    const auto width{ dest.desc().Width };
    const auto height{ dest.desc().Height };

    subresourceData.resize(mipLevels);

    // Accumulator of already used bytes from the input data.
    std::size_t usedBytes{ 0u };
    for (std::size_t iii = 0u; iii < mipLevels; ++iii)
    { // Update every mip-map level of the texture.
        const auto rowPitch{ D3D12Helpers::rowPitch2D(format, width, iii) };
        const auto rowCount{ D3D12Helpers::rowCount2D(format, height, iii) };
        const auto totalBytes{ rowPitch * rowCount };

        subresourceData[iii].pData = srcData + usedBytes;
        subresourceData[iii].RowPitch = rowPitch;
        subresourceData[iii].SlicePitch = totalBytes;

        usedBytes += totalBytes;
    }

    // Calculate the required size of the intermediate buffer.
    const auto requiredBytes{ mDevice->bytesRequiredForSubresources(dest.desc(), mipLevels) };

    // Create the intermediate resource used for data upload.
    auto intermediateResource{ res::d3d12::D3D12Resource::create(
        ::CD3DX12_RESOURCE_DESC::Buffer(requiredBytes),
        ::D3D12_RESOURCE_STATE_GENERIC_READ, nullptr, mAllocator) };

#ifdef _DEBUG
    if (mDbgName)
    {
        util::throwIfFailed(intermediateResource->get()->SetName(mDbgName),
                            "Failed to set intermediate resource name!");
    }
#endif

    const auto result{ UpdateSubresources(cmdList.get(), dest.get(), intermediateResource->get(),
        0u, 0u, static_cast<::UINT>(subresourceData.size()), subresourceData.data()) };

    if (result < sizeInBytes)
    { // Copied different number of bytes? Probably OK...
        //throw util::winexception("CopyToResource failed, number of bytes copied is smaller than the source buffer size!");
        log<Warning>() << "CopyToResource reported different number of bytes copied, than the specified size in bytes!" << std::endl;
    }

    // Keep the intermediate buffer alive.
    stashBuffer(*intermediateResource);
}

void D3D12ResourceUpdater::setDebugName(const wchar_t *dbgName)
{
#ifdef _DEBUG
    mDbgName = dbgName;
#else
	UNUSED(dbgName);
#endif
}

bool D3D12ResourceUpdater::allocatorMeetsRequirements(const D3D12CommittedAllocator &allocator)
{ return allocator.heapProperties() == requiredHeapProps() && allocator.heapFlags() == requiredHeapFlags(); }

void D3D12ResourceUpdater::commandListExecuted()
{ mStashedBuffers.clear(); }

void D3D12ResourceUpdater::stashBuffer(res::d3d12::D3D12Resource &buffer)
{ mStashedBuffers.push_back(buffer.getPtr()); }

}

}

}
