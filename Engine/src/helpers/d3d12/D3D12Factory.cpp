/**
 * @file helpers/d3d12/D3D12Factory.cpp
 * @author Tomas Polasek
 * @brief Helper class for creation of Direct3D 12 objects.
 */

#include "stdafx.h"

#include "engine/helpers/d3d12/D3D12Factory.h"

namespace quark
{

namespace helpers
{

namespace d3d12
{

helpers::d3d12::D3D12CommittedAllocator::PtrT D3D12Factory::committedAllocator(res::d3d12::D3D12Device &device,
    ::CD3DX12_HEAP_PROPERTIES heapProps, ::D3D12_HEAP_FLAGS heapFlags)
{ return helpers::d3d12::D3D12CommittedAllocator::create(device, heapProps, heapFlags); }

helpers::d3d12::D3D12CommittedAllocator::PtrT D3D12Factory::uploadAllocator(res::d3d12::D3D12Device &device)
{
    return helpers::d3d12::D3D12CommittedAllocator::create(device,
        helpers::d3d12::D3D12ResourceUpdater::requiredHeapProps(),
        helpers::d3d12::D3D12ResourceUpdater::requiredHeapFlags());
}

}

}

}
