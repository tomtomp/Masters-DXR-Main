/**
 * @file helpers/d3d12/D3D12ImGuiWrapper.cpp
 * @author Tomas Polasek
 * @brief Wrapper around ImGui, usable within D3D12.
 */

#include "stdafx.h"

#include "engine/helpers/d3d12/D3D12ImguiWrapper.h"

#include "engine/lib/imgui_impl_dx12.h"

namespace quark
{

namespace helpers
{

namespace d3d12
{

D3D12ImGuiWrapper::D3D12ImGuiWrapper(res::d3d12::D3D12Device &device, 
    res::d3d12::D3D12SwapChain &swapChain) :
    mSrvDescHeap{ 
        res::d3d12::D3D12DescHeap::create(
            device, ::D3D12_DESCRIPTOR_HEAP_TYPE_CBV_SRV_UAV,
            REQUIRED_SRV_DESCRIPTORS, ::D3D12_DESCRIPTOR_HEAP_FLAG_SHADER_VISIBLE) }
{ initialize(device, swapChain); }

D3D12ImGuiWrapper::~D3D12ImGuiWrapper()
{ shutdown(); }

void D3D12ImGuiWrapper::newFrame()
{ newFrameImpl(); }

void D3D12ImGuiWrapper::render(res::d3d12::D3D12CommandList &cmdList)
{ renderImpl(cmdList); }

void D3D12ImGuiWrapper::freeD3D12Objects()
{ freeD3D12ObjectsImpl(); }

void D3D12ImGuiWrapper::createD3D12Objects()
{ createD3D12ObjectsImpl(); }

void D3D12ImGuiWrapper::resize(int width, int height)
{
    ImGuiIO& io = ImGui::GetIO();
    io.DisplaySize = ImVec2(static_cast<float>(width), static_cast<float>(height));
}

void D3D12ImGuiWrapper::initialize(res::d3d12::D3D12Device &device, res::d3d12::D3D12SwapChain &swapChain)
{
    // Forward to the default implementation.
    quark::lib::ImGui::ImGui_ImplDX12_Init(device.get(), 
        swapChain.numBuffers(), swapChain.bufferFormat(), 
        mSrvDescHeap->get()->GetCPUDescriptorHandleForHeapStart(), 
        mSrvDescHeap->get()->GetGPUDescriptorHandleForHeapStart());
}

void D3D12ImGuiWrapper::shutdown()
{
    // Forward to the default implementation.
    quark::lib::ImGui::ImGui_ImplDX12_Shutdown();
}

void D3D12ImGuiWrapper::newFrameImpl()
{
    // Forward to the default implementation.
    quark::lib::ImGui::ImGui_ImplDX12_NewFrame();

    // New frame for the main ImGui library.
    ImGui::NewFrame();
}

void D3D12ImGuiWrapper::renderImpl(res::d3d12::D3D12CommandList &cmdList)
{
    // Use the descriptor heap specified in initialization.
    const auto descHeap{ mSrvDescHeap->get() };
    cmdList->SetDescriptorHeaps(1u, &descHeap);

    // Render for the main ImGui library.
    ImGui::Render();
    // Forward to the default implementation.
    quark::lib::ImGui::ImGui_ImplDX12_RenderDrawData(ImGui::GetDrawData(), cmdList.get());
}

void D3D12ImGuiWrapper::freeD3D12ObjectsImpl()
{
    // Forward to the default implementation.
    quark::lib::ImGui::ImGui_ImplDX12_InvalidateDeviceObjects();
}

void D3D12ImGuiWrapper::createD3D12ObjectsImpl()
{
    // Forward to the default implementation.
    quark::lib::ImGui::ImGui_ImplDX12_CreateDeviceObjects();
}

} 

} 

} 
