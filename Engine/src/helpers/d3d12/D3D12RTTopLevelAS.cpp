/**
 * @file helpers/d3d12/D3D12RTTopLevelAS.cpp
 * @author Tomas Polasek
 * @brief Helper used for building D3D12 top level acceleration 
 * structure for ray tracing.
 */

#include "stdafx.h"

#ifdef ENGINE_RAY_TRACING_ENABLED

#include "engine/helpers/d3d12/D3D12RTTopLevelAS.h"

#include "engine/util/BitwiseEnum.h"

namespace quark
{

namespace helpers
{

namespace d3d12
{

D3D12RTTopLevelAS::D3D12RTTopLevelAS()
{ clear(); }

D3D12RTTopLevelAS::~D3D12RTTopLevelAS()
{ /* Automatic */ }

void D3D12RTTopLevelAS::clear()
{
    mDirty = true;
    mAllowUpdating = false;
    mMinimizeMemory = false;
    mAllowCompacting = false;
    mInstances.clear();
    mInstanceCount = 0u;
    mBaseFlags = DEFAULT_BUILD_PRIORITY;
}

void D3D12RTTopLevelAS::setAllowUpdating(bool allowUpdating)
{
    mDirty = true;
    mAllowUpdating = allowUpdating;
}

void D3D12RTTopLevelAS::setMinimizeMemory(bool minimizeMemory)
{
    mDirty = true;
    mMinimizeMemory = minimizeMemory;
}

void D3D12RTTopLevelAS::setAllowCompacting(bool allowCompacting)
{
    mDirty = true;
    mAllowCompacting = allowCompacting;
}

void D3D12RTTopLevelAS::preferFastRayTracing()
{
    mDirty = true;
    mBaseFlags = ::D3D12_RAYTRACING_ACCELERATION_STRUCTURE_BUILD_FLAG_PREFER_FAST_TRACE;
}

void D3D12RTTopLevelAS::preferFastBuild()
{
    mDirty = true;
    mBaseFlags = ::D3D12_RAYTRACING_ACCELERATION_STRUCTURE_BUILD_FLAG_PREFER_FAST_BUILD;
}

D3D12RTTopLevelAS::InstanceHandleT D3D12RTTopLevelAS::addInstance(res::d3d12::D3D12Resource &bottomLevelAS, 
    const helpers::math::Transform &transform, ::UINT shaderTableOffset, ::UINT instanceID, 
    ::UINT instanceMask, ::D3D12_RAYTRACING_INSTANCE_FLAGS flags)
{
    return addInstanceDesc(generateInstanceDesc(bottomLevelAS->GetGPUVirtualAddress(), 
        transform, shaderTableOffset, instanceID, instanceMask, flags));
}

D3D12RTTopLevelAS::InstanceHandleT D3D12RTTopLevelAS::addInstance(const ::WRAPPED_GPU_POINTER &bottomLevelAS,
    const helpers::math::Transform &transform, ::UINT shaderTableOffset, ::UINT instanceID, 
    ::UINT instanceMask, ::D3D12_RAYTRACING_INSTANCE_FLAGS flags)
{
    return addInstanceDesc(generateInstanceDesc(bottomLevelAS, 
        transform, shaderTableOffset, instanceID, instanceMask, flags));
}

D3D12RTTopLevelAS::InstanceHandleT D3D12RTTopLevelAS::addInstance(const helpers::math::Transform &transform,
    ::UINT shaderTableOffset, ::UINT instanceID, ::UINT instanceMask, ::D3D12_RAYTRACING_INSTANCE_FLAGS flags)
{
    return addInstanceDesc(generateInstanceDesc(0u, 
        transform, shaderTableOffset, instanceID, instanceMask, flags));
}

void D3D12RTTopLevelAS::setBottomLevelAS(const InstanceHandleT &instanceHandle,
    const ::WRAPPED_GPU_POINTER &bottomLevelAS)
{
    auto &desc{ getInstanceDesc(instanceHandle) };

    desc.AccelerationStructure = bottomLevelAS.GpuVA;

    mDirty = true;
}

D3D12RTTopLevelAS::BufferSizeRequirements D3D12RTTopLevelAS::calculateSizeRequirements(
    res::d3d12::D3D12RayTracingDevice &device)
{
    recalculateInfoStructures(device);

    const auto instanceBufferSize{ calculateInstanceBufferSize(mInstances) };

    return BufferSizeRequirements{ 
        mPreBuildInfo.ResultDataMaxSizeInBytes, 
        mPreBuildInfo.ScratchDataSizeInBytes, 
        instanceBufferSize ? instanceBufferSize : 1u};
}

void D3D12RTTopLevelAS::build(res::d3d12::D3D12RayTracingDevice &device,
    res::d3d12::D3D12RayTracingCommandList &directCmdList, res::d3d12::D3D12Resource &instanceBuffer,
    res::d3d12::D3D12Resource &scratchBuffer, res::d3d12::D3D12Resource &outputBuffer)
{
    // Refresh information, if any changed occured.
    recalculateInfoStructures(device);

    // Copy the instance descriptions over to the GPU.
    fillInstanceBuffer(mInstances, instanceBuffer);

    // We need to specify the correct instance buffer first.
    auto instanceInputs{ mInstanceInputs };
    instanceInputs.InstanceDescs = instanceBuffer->GetGPUVirtualAddress();

    // Generate building information, describing how to build the AS.
    const auto buildDesc{ generateBuildDesc(instanceInputs, 
        scratchBuffer, outputBuffer) };

    // Add building command to the command list.
    directCmdList.apply([&] (auto *cmdList)
    { cmdList->BuildRaytracingAccelerationStructure(&buildDesc, 0u, nullptr); });

    // Setup barrier to wait for completion of all of the building commands.
    const auto barrier{ CD3DX12_RESOURCE_BARRIER::UAV(outputBuffer.get()) };
    directCmdList.cmdList()->ResourceBarrier(1u, &barrier);
}

void D3D12RTTopLevelAS::build(res::d3d12::D3D12RayTracingDevice &device,
    res::d3d12::D3D12RayTracingCommandList &directCmdList, res::d3d12::D3D12Resource &instanceBuffer,
    res::d3d12::D3D12Resource &scratchBuffer, res::d3d12::D3D12Resource &outputBuffer,
    res::d3d12::D3D12Resource &pastOutputBuffer, bool forceUpdate)
{
    // Refresh information, if any changed occured.
    recalculateInfoStructures(device);

    // Copy the instance descriptions over to the GPU.
    fillInstanceBuffer(mInstances, instanceBuffer);

    // We need to specify the correct instance buffer first.
    auto instanceInputs{ mInstanceInputs };
    instanceInputs.InstanceDescs = instanceBuffer->GetGPUVirtualAddress();

    // We may need to alter the description for updating.
    // Should we update?
    if (mAllowUpdating && pastOutputBuffer)
    { instanceInputs.Flags = addUpdateFlag(instanceInputs.Flags); }
    else if (forceUpdate)
    { 
        throw InvalidUpdateException("Cannot force update acceleration structure "
        "since it is not configured for updating or pastOutputBuffer is invalid!"); 
    }

    // Generate building information, describing how to build the AS.
    const auto buildDesc{ generateBuildDesc(instanceInputs, 
        scratchBuffer, outputBuffer) };

    // Add building command to the command list.
    directCmdList.apply([&] (auto *cmdList)
    { cmdList->BuildRaytracingAccelerationStructure(&buildDesc, 0u, nullptr); });

    // Setup barrier to wait for completion of all of the building commands.
    const auto barrier{ CD3DX12_RESOURCE_BARRIER::UAV(outputBuffer.get()) };
    directCmdList.cmdList()->ResourceBarrier(1u, &barrier);
}

uint64_t D3D12RTTopLevelAS::instanceCount()
{ return mInstanceCount; }

::D3D12_RAYTRACING_INSTANCE_DESC D3D12RTTopLevelAS::generateInstanceDesc(
    const ::D3D12_GPU_VIRTUAL_ADDRESS &bottomLevelAS, const helpers::math::Transform &transform, 
    ::UINT shaderTableOffset, ::UINT instanceID, ::UINT instanceMask, 
    ::D3D12_RAYTRACING_INSTANCE_FLAGS flags)
{
    ::D3D12_RAYTRACING_INSTANCE_DESC instanceDesc{ };

    // Make sure the fallback is using the same description.
    static_assert(sizeof(::D3D12_RAYTRACING_FALLBACK_INSTANCE_DESC) == 
        sizeof(::D3D12_RAYTRACING_INSTANCE_DESC));

    // Copy over the model->world transform.
    const auto test{ transform.matrixNoRecalculation() };
    transform.rowMajorCopy4x3(&instanceDesc.Transform[0][0]);
    instanceDesc.InstanceID = instanceID;
    instanceDesc.InstanceMask = instanceMask;
    instanceDesc.InstanceContributionToHitGroupIndex = shaderTableOffset;
    instanceDesc.Flags = flags;
    instanceDesc.AccelerationStructure = bottomLevelAS;

    return instanceDesc;
}

::D3D12_RAYTRACING_INSTANCE_DESC D3D12RTTopLevelAS::generateInstanceDesc(
    const ::WRAPPED_GPU_POINTER &bottomLevelAS, const helpers::math::Transform &transform, 
    ::UINT shaderTableOffset, ::UINT instanceID, ::UINT instanceMask,
    ::D3D12_RAYTRACING_INSTANCE_FLAGS flags)
{ return generateInstanceDesc(bottomLevelAS.GpuVA, transform, shaderTableOffset, instanceID, instanceMask, flags); }

D3D12RTTopLevelAS::InstanceHandleT D3D12RTTopLevelAS::addInstanceDesc(const ::D3D12_RAYTRACING_INSTANCE_DESC &desc)
{
    mInstances.emplace_back(desc);
    mInstanceCount++;
    mDirty = true;

    return mInstances.size();
}

::D3D12_RAYTRACING_INSTANCE_DESC & D3D12RTTopLevelAS::getInstanceDesc(const InstanceHandleT &instanceHandle)
{
    if (instanceHandle == 0u || instanceHandle > mInstances.size())
    { throw UnknownHandleException("Given instance handle is invalid!"); }

    return mInstances[instanceHandle - 1u];
}

::D3D12_RAYTRACING_ACCELERATION_STRUCTURE_BUILD_FLAGS D3D12RTTopLevelAS::generateBuildFlags(
    ::D3D12_RAYTRACING_ACCELERATION_STRUCTURE_BUILD_FLAGS baseFlags, bool allowUpdates, 
    bool minimizeMemory, bool allowCompaction)
{
    auto result{ static_cast<std::size_t>(baseFlags) };

    result |= allowUpdates ? ::D3D12_RAYTRACING_ACCELERATION_STRUCTURE_BUILD_FLAG_ALLOW_UPDATE : 0u;
    result |= minimizeMemory ? ::D3D12_RAYTRACING_ACCELERATION_STRUCTURE_BUILD_FLAG_MINIMIZE_MEMORY : 0u;
    result |= allowCompaction ? ::D3D12_RAYTRACING_ACCELERATION_STRUCTURE_BUILD_FLAG_ALLOW_UPDATE : 0u;

    return static_cast<::D3D12_RAYTRACING_ACCELERATION_STRUCTURE_BUILD_FLAGS>(result);
}

::D3D12_RAYTRACING_ACCELERATION_STRUCTURE_BUILD_FLAGS D3D12RTTopLevelAS::addUpdateFlag(
    ::D3D12_RAYTRACING_ACCELERATION_STRUCTURE_BUILD_FLAGS startFlags)
{
    auto result{ static_cast<std::size_t>(startFlags) };

    result |= ::D3D12_RAYTRACING_ACCELERATION_STRUCTURE_BUILD_FLAG_PERFORM_UPDATE;

    return static_cast<::D3D12_RAYTRACING_ACCELERATION_STRUCTURE_BUILD_FLAGS>(result);
}

::D3D12_BUILD_RAYTRACING_ACCELERATION_STRUCTURE_INPUTS D3D12RTTopLevelAS::generateInstanceInputs(
    const std::vector<::D3D12_RAYTRACING_INSTANCE_DESC> &instances,
    ::D3D12_RAYTRACING_ACCELERATION_STRUCTURE_BUILD_FLAGS buildFlags)
{
    ::D3D12_BUILD_RAYTRACING_ACCELERATION_STRUCTURE_INPUTS result{ };

    // Geometry is for the top level.
    result.Type = ::D3D12_RAYTRACING_ACCELERATION_STRUCTURE_TYPE_TOP_LEVEL;

    // We may want to ray trace as fast as possible or save memory.
    result.Flags = buildFlags;

    // Specify provided number of instances.
    result.NumDescs = static_cast<::UINT>(instances.size());
    result.DescsLayout = ::D3D12_ELEMENTS_LAYOUT_ARRAY;

    return result;
}

::D3D12_RAYTRACING_ACCELERATION_STRUCTURE_PREBUILD_INFO D3D12RTTopLevelAS::getPreBuildInfo(
    res::d3d12::D3D12RayTracingDevice &device,
    const ::D3D12_BUILD_RAYTRACING_ACCELERATION_STRUCTURE_INPUTS &buildInputs)
{
    ::D3D12_RAYTRACING_ACCELERATION_STRUCTURE_PREBUILD_INFO result{ };

    // One code for fallback device and hardware.
    device.apply([&](auto d)
    { // Query the device for building information for specified inputs.
        d->GetRaytracingAccelerationStructurePrebuildInfo(&buildInputs, &result);
    });

    return result;
}

void D3D12RTTopLevelAS::recalculateInfoStructures(
    res::d3d12::D3D12RayTracingDevice &device)
{
    if (!mDirty)
    { // Nothing to be done...
        return;
    }

    // Something changed -> regenerate everything!
    mBuildFlags = generateBuildFlags(mBaseFlags, 
        mAllowUpdating, mMinimizeMemory, mAllowCompacting);
    mInstanceInputs = generateInstanceInputs(mInstances, mBuildFlags);
    mPreBuildInfo = getPreBuildInfo(device, mInstanceInputs);
    mDirty = false;
}

std::size_t D3D12RTTopLevelAS::calculateInstanceBufferSize(
    const std::vector<::D3D12_RAYTRACING_INSTANCE_DESC> &instances)
{ return instances.size() * sizeof(::D3D12_RAYTRACING_INSTANCE_DESC); }

void D3D12RTTopLevelAS::fillInstanceBuffer(const std::vector<::D3D12_RAYTRACING_INSTANCE_DESC> &instances,
    res::d3d12::D3D12Resource &instanceBuffer)
{
    auto uploadBuffer{ helpers::d3d12::D3D12UploadBuffer::create(instanceBuffer) };
    uploadBuffer->uploadDataImmediate(instances.data(), instances.size() * sizeof(::D3D12_RAYTRACING_INSTANCE_DESC));
}

::D3D12_BUILD_RAYTRACING_ACCELERATION_STRUCTURE_DESC D3D12RTTopLevelAS::generateBuildDesc(
    const ::D3D12_BUILD_RAYTRACING_ACCELERATION_STRUCTURE_INPUTS &instanceInputs,
    res::d3d12::D3D12Resource &scratchBuffer, res::d3d12::D3D12Resource &outputBuffer,
    res::d3d12::D3D12Resource *pastOutputBuffer)
{
    ::D3D12_BUILD_RAYTRACING_ACCELERATION_STRUCTURE_DESC result{ };

    result.DestAccelerationStructureData = outputBuffer->GetGPUVirtualAddress();
    result.Inputs = instanceInputs;
    result.SourceAccelerationStructureData = pastOutputBuffer ? (*pastOutputBuffer)->GetGPUVirtualAddress() : 0u;
    result.ScratchAccelerationStructureData = scratchBuffer->GetGPUVirtualAddress();

    return result;
}

}

}

}

#endif
