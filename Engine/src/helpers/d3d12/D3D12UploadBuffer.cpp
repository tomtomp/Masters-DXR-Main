/**
 * @file helpers/d3d12/D3D12UploadBuffer.cpp
 * @author Tomas Polasek
 * @brief Wrapper around D3D12Resource, providing simple 
 * uploading and mapping of contained resource.
 */

#include "stdafx.h"

#include "engine/helpers/d3d12/D3D12UploadBuffer.h"

namespace quark
{

namespace helpers
{

namespace d3d12
{

D3D12UploadBuffer::D3D12UploadBuffer()
{ /* Automatic */ }

D3D12UploadBuffer::PtrT D3D12UploadBuffer::create()
{ return PtrT{ new D3D12UploadBuffer() }; }

D3D12UploadBuffer::PtrT D3D12UploadBuffer::create(res::d3d12::D3D12Resource &buffer)
{ return PtrT{ new D3D12UploadBuffer(buffer) }; }

D3D12UploadBuffer::~D3D12UploadBuffer()
{ /* Automatic */ }

D3D12UploadBuffer::D3D12UploadBuffer(res::d3d12::D3D12Resource &buffer) :
    res::d3d12::D3D12Resource(buffer.getPtr(), buffer.state(), buffer.desc())
{ }

void D3D12UploadBuffer::allocate(res::d3d12::D3D12Device &device, 
    std::size_t sizeInBytes, ::D3D12_RESOURCE_STATES initResourceState, 
    const std::wstring &name)
{
    auto alloc{ D3D12CommittedAllocator::create(
        device,
        ::CD3DX12_HEAP_PROPERTIES(::D3D12_HEAP_TYPE_UPLOAD),
        ::D3D12_HEAP_FLAG_NONE) };

    allocate(alloc, sizeInBytes, initResourceState, name);
}

uint8_t *D3D12UploadBuffer::mapForCpuWrite()
{
    if (!checkMapped(MappingType::Write))
    {
        void *result{ nullptr };
        // No reading allowed.
        ::CD3DX12_RANGE readRange{ 0u, 0u };
        util::throwIfFailed(
            getPtr()->Map(0u, &readRange, &result), 
            "Failed to map upload buffer for reading!");

        mMappedType = MappingType::Write;
        mMappedAddress = static_cast<uint8_t*>(result);
    }

    return mMappedAddress;
}

uint8_t *D3D12UploadBuffer::mapForCpuReadWrite()
{
    if (!checkMapped(MappingType::ReadWrite))
    {
        void *result{ nullptr };
        util::throwIfFailed(
            getPtr()->Map(0u, nullptr, &result), 
            // Specify nullptr for full range reading.
            "Failed to map upload buffer for reading!");

        mMappedType = MappingType::ReadWrite;
        mMappedAddress = static_cast<uint8_t*>(result);
    }

    return mMappedAddress;
}

void D3D12UploadBuffer::unmap(const ::CD3DX12_RANGE &writtenRange)
{
    getPtr()->Unmap(0u, &writtenRange);

    mMappedType = MappingType::None;
    mMappedAddress = nullptr;
}

void D3D12UploadBuffer::unmapFullRewrite()
{
    getPtr()->Unmap(0u, nullptr);

    mMappedType = MappingType::None;
    mMappedAddress = nullptr;
}

void D3D12UploadBuffer::uploadData(const void *data, std::size_t sizeInBytes, res::d3d12::D3D12CommandList &cmdList,
    D3D12ResourceUpdater &updater)
{
    // Nothing to be done.
    if (sizeInBytes == 0u || data == nullptr)
    { return; }

    if (sizeInBytes > allocatedSize())
    { throw BufferTooSmallException("Upload buffer is not allocated or it is too small to contain given data!"); }

    updater.copyToResource(data, sizeInBytes, *this, cmdList);
}

void D3D12UploadBuffer::uploadDataImmediate(const void *data, std::size_t sizeInBytes)
{
    // Nothing to be done.
    if (sizeInBytes == 0u || data == nullptr)
    { return; }

    if (sizeInBytes > allocatedSize())
    { throw BufferTooSmallException("Upload buffer is not allocated or it is too small to contain given data!"); }

    // Map and rewrite the buffer memory.
    const auto dstPtr{ mapForCpuWrite() };
    { // Fill it with provided data.
        std::memcpy(dstPtr, data, sizeInBytes);
    } unmapFullRewrite();
}

bool D3D12UploadBuffer::checkMapped(MappingType type)
{
    if (mMappedType == type && mMappedAddress)
    { return true; }

    if (mMappedType == MappingType::Write && type == MappingType::ReadWrite)
    { throw BufferMapIncompatibleException("Upload buffer is already mapped in 'Write' mode, unable to map in 'ReadWrite' mode!"); }

    return false;
}

}

}

}
