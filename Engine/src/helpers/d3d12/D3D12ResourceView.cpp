/**
 * @file helpers/d3d12/D3D12ResourceView.cpp
 * @author Tomas Polasek
 * @brief Wrapper around a D3D12 resource pointer and a resource view.
 */

#include "stdafx.h"

#include "engine/helpers/d3d12/D3D12ResourceView.h"

#include "engine/resources/d3d12/D3D12Resource.h"
#include "engine/resources/d3d12/D3D12DescHeap.h"

namespace quark
{

namespace helpers
{

namespace d3d12
{

D3D12ResourceView::PtrT D3D12ResourceView::create()
{ return PtrT{ new D3D12ResourceView() }; }

D3D12ResourceView::PtrT D3D12ResourceView::create(
    util::PtrT<res::d3d12::D3D12Resource> resource, 
    res::d3d12::D3D12DescAllocatorInterface &descHeap, 
    quark::rndr::ResourceViewType viewType, 
    ::DXGI_FORMAT forceFormat)
{ return PtrT{ new D3D12ResourceView(resource, descHeap, viewType, forceFormat) }; }

D3D12ResourceView::~D3D12ResourceView()
{ /* Automatic */ }

::CD3DX12_CPU_DESCRIPTOR_HANDLE D3D12ResourceView::cpuHandle() const
{ return mCpuHandle; }

::CD3DX12_GPU_DESCRIPTOR_HANDLE D3D12ResourceView::gpuHandle() const
{ return mGpuHandle; }

void D3D12ResourceView::reallocateDescriptor(
    util::PtrT<res::d3d12::D3D12Resource> resource, 
    res::d3d12::D3D12DescAllocatorInterface &descHeap, 
    quark::rndr::ResourceViewType viewType, 
    ::DXGI_FORMAT forceFormat)
{
    if (resource.get() != mTargetResource.lock().get() || viewType != mViewType)
    { // Reset only when we go to different resource/view type.
        mCpuHandle = { };
        mGpuHandle = { };
        mTargetResource = resource;
        mViewType = viewType;
    }

    reallocateDescriptor(descHeap, forceFormat);
}

void D3D12ResourceView::reallocateDescriptor(
    res::d3d12::D3D12DescAllocatorInterface &descHeap, 
    ::DXGI_FORMAT forceFormat)
{
    // Don't do anything if invalid descriptor heap is provided.
    if (!descHeap.initialized())
    { return; }

    const auto targetResource{ mTargetResource.lock() };

    // Or when no valid resource is set.
    if (!targetResource)
    { return; }

    // Is the currently used descriptor on provided descriptor heap?
    const auto isOnThisDescHeap{ 
        descHeap.isOnThisDescHeap(mCpuHandle) && 
        descHeap.isOnThisDescHeap(mGpuHandle) };

    // Check if we need a new descriptor.
    if (!dirty() && isOnThisDescHeap)
    { return; }

    switch (mViewType)
    {
        case quark::rndr::ResourceViewType::ShaderResource:
        {
            auto srvDesc{ targetResource->srvDesc() };
            if (forceFormat != ::DXGI_FORMAT_UNKNOWN)
            { srvDesc.Format = forceFormat; }

            if (isOnThisDescHeap)
            { // Reallocate the same one.
                mCpuHandle = descHeap.createSRV(*targetResource, srvDesc, mCpuHandle);
            }
            else
            { // New heap -> new descriptor.
                mCpuHandle = descHeap.createSRV(*targetResource, srvDesc);
            }

            break;
        }
        case quark::rndr::ResourceViewType::RenderTarget:
        {
            auto rtvDesc{ targetResource->rtvDesc() };
            if (forceFormat != ::DXGI_FORMAT_UNKNOWN)
            { rtvDesc.Format = forceFormat; }

            if (isOnThisDescHeap)
            { // Reallocate the same one.
                mCpuHandle = descHeap.createRTV(*targetResource, rtvDesc, mCpuHandle);
            }
            else
            { // New heap -> new descriptor.
                mCpuHandle = descHeap.createRTV(*targetResource, rtvDesc);
            }

            break;
        }
        case quark::rndr::ResourceViewType::DepthStencil:
        {
            auto dsvDesc{ targetResource->dsvDesc() };
            if (forceFormat != ::DXGI_FORMAT_UNKNOWN)
            { dsvDesc.Format = forceFormat; }

            if (isOnThisDescHeap)
            { // Reallocate the same one.
                mCpuHandle = descHeap.createDSV(*targetResource, dsvDesc, mCpuHandle);
            }
            else
            { // New heap -> new descriptor.
                mCpuHandle = descHeap.createDSV(*targetResource, dsvDesc);
            }

            break;
        }
        case quark::rndr::ResourceViewType::ConstantBuffer:
        {
            const auto cbvDesc{ targetResource->cbvDesc() };

            if (isOnThisDescHeap)
            { // Reallocate the same one.
                mCpuHandle = descHeap.createCBV(cbvDesc, mCpuHandle);
            }
            else
            { // New heap -> new descriptor.
                mCpuHandle = descHeap.createCBV(cbvDesc);
            }

            break;
        }
        case quark::rndr::ResourceViewType::UnorderedAccess:
        {
            auto uavDesc{ targetResource->uavDesc() };
            if (forceFormat != ::DXGI_FORMAT_UNKNOWN)
            { uavDesc.Format = forceFormat; }

            if (isOnThisDescHeap)
            { // Reallocate the same one.
                mCpuHandle = descHeap.createUAV(*targetResource, uavDesc, mCpuHandle);
            }
            else
            { // New heap -> new descriptor.
                mCpuHandle = descHeap.createUAV(*targetResource, uavDesc);
            }

            break;
        }
        default:
        {
            log<Info>() << "Unable to reallocate descriptor for invalid view type!" << std::endl;
            break;
        }
    }

    // Re-fetch the GPU handle.
    mGpuHandle = descHeap.gpuHandle(mCpuHandle);

    // Remember currently viewed resource to check dirtiness.
    mViewedResource = targetResource->get();
}

quark::rndr::ResourceViewType D3D12ResourceView::type() const
{ return mViewType; }

bool D3D12ResourceView::dirty() const
{
    const auto targetResource{ mTargetResource.lock() };
    return (targetResource && targetResource->valid()) ? targetResource->get() != mViewedResource : false;
}

bool D3D12ResourceView::valid() const
{ return !dirty() && mViewedResource && mViewType != quark::rndr::ResourceViewType::Unknown; }

D3D12ResourceView::operator bool() const
{ return valid(); }

D3D12ResourceView::D3D12ResourceView()
{ /* Automatic */ }

D3D12ResourceView::D3D12ResourceView(
    util::PtrT<res::d3d12::D3D12Resource> resource, 
    res::d3d12::D3D12DescAllocatorInterface &descHeap, 
    quark::rndr::ResourceViewType viewType, 
    ::DXGI_FORMAT forceFormat) : 
    D3D12ResourceView()
{ reallocateDescriptor(resource, descHeap, viewType, forceFormat); }

}

}

}
