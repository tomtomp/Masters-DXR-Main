/**
 * @file helpers/d3d12/D3D12DescTable.cpp
 * @author Tomas Polasek
 * @brief Wrapper around D3D12DescHeap and a DescriptorTable, allowing 
 * allocation of descriptors.
 */

#include "stdafx.h"

#include "engine/helpers/d3d12/D3D12DescTable.h"

namespace quark
{

namespace helpers
{

namespace d3d12
{

D3D12DescTable::D3D12DescTable()
{ /* Automatic */ }

D3D12DescTable::~D3D12DescTable()
{ /* Automatic */ }

D3D12DescTable::D3D12DescTable(res::d3d12::D3D12DescHeap &descHeap, const DescriptorTable &descriptorTable) :
    mDescHeap{ &descHeap }, mDescTable{ descriptorTable }
{ clear(); }

::CD3DX12_CPU_DESCRIPTOR_HANDLE D3D12DescTable::createSRV(res::d3d12::D3D12Resource &resource,
    const ::D3D12_SHADER_RESOURCE_VIEW_DESC &desc)
{
    ASSERT_SLOW(bound());
    return mDescHeap->createSRV(resource, desc, allocateNext());
}

::CD3DX12_CPU_DESCRIPTOR_HANDLE D3D12DescTable::createSRV(res::d3d12::D3D12Resource &resource,
    const ::D3D12_SHADER_RESOURCE_VIEW_DESC &desc, const ::CD3DX12_CPU_DESCRIPTOR_HANDLE &target)
{
    ASSERT_SLOW(bound());
    ASSERT_FAST(isOnThisDescHeap(target));
    return mDescHeap->createSRV(resource, desc, target);
}

::CD3DX12_CPU_DESCRIPTOR_HANDLE D3D12DescTable::createRTV(res::d3d12::D3D12Resource &resource,
    const ::D3D12_RENDER_TARGET_VIEW_DESC &desc)
{
    ASSERT_SLOW(bound());
    return mDescHeap->createRTV(resource, desc, allocateNext());
}

::CD3DX12_CPU_DESCRIPTOR_HANDLE D3D12DescTable::createRTV(res::d3d12::D3D12Resource &resource,
    const ::D3D12_RENDER_TARGET_VIEW_DESC &desc, const ::CD3DX12_CPU_DESCRIPTOR_HANDLE &target)
{
    ASSERT_SLOW(bound());
    ASSERT_FAST(isOnThisDescHeap(target));
    return mDescHeap->createRTV(resource, desc, target);
}

::CD3DX12_CPU_DESCRIPTOR_HANDLE D3D12DescTable::createDSV(res::d3d12::D3D12Resource &resource,
    const ::D3D12_DEPTH_STENCIL_VIEW_DESC &desc)
{
    ASSERT_SLOW(bound());
    return mDescHeap->createDSV(resource, desc, allocateNext());
}

::CD3DX12_CPU_DESCRIPTOR_HANDLE D3D12DescTable::createDSV(res::d3d12::D3D12Resource &resource,
    const ::D3D12_DEPTH_STENCIL_VIEW_DESC &desc, const ::CD3DX12_CPU_DESCRIPTOR_HANDLE &target)
{
    ASSERT_SLOW(bound());
    ASSERT_FAST(isOnThisDescHeap(target));
    return mDescHeap->createDSV(resource, desc, target);
}

::CD3DX12_CPU_DESCRIPTOR_HANDLE D3D12DescTable::createUAV(res::d3d12::D3D12Resource &resource,
    const ::D3D12_UNORDERED_ACCESS_VIEW_DESC &desc, res::d3d12::D3D12Resource *counterResource)
{
    ASSERT_SLOW(bound());
    return mDescHeap->createUAV(resource, desc, allocateNext(), counterResource);
}

::CD3DX12_CPU_DESCRIPTOR_HANDLE D3D12DescTable::createUAV(res::d3d12::D3D12Resource &resource,
    const ::D3D12_UNORDERED_ACCESS_VIEW_DESC &desc, const ::CD3DX12_CPU_DESCRIPTOR_HANDLE &target,
    res::d3d12::D3D12Resource *counterResource)
{
    ASSERT_SLOW(bound());
    ASSERT_FAST(isOnThisDescHeap(target));
    return mDescHeap->createUAV(resource, desc, target, counterResource);
}

::CD3DX12_CPU_DESCRIPTOR_HANDLE D3D12DescTable::createCBV(const ::D3D12_CONSTANT_BUFFER_VIEW_DESC &desc)
{
    ASSERT_SLOW(bound());
    return mDescHeap->createCBV(desc, allocateNext());
}

::CD3DX12_CPU_DESCRIPTOR_HANDLE D3D12DescTable::createCBV(const ::D3D12_CONSTANT_BUFFER_VIEW_DESC &desc,
    const ::CD3DX12_CPU_DESCRIPTOR_HANDLE &target)
{
    ASSERT_SLOW(bound());
    ASSERT_FAST(isOnThisDescHeap(target));
    return mDescHeap->createCBV(desc, target);
}

::CD3DX12_CPU_DESCRIPTOR_HANDLE D3D12DescTable::createSrvNullHandle(::D3D12_SRV_DIMENSION dimension)
{
    ASSERT_SLOW(bound());
    return mDescHeap->createSrvNullHandle(dimension, allocateNext());
}

::CD3DX12_CPU_DESCRIPTOR_HANDLE D3D12DescTable::createSrvNullHandle(::D3D12_SRV_DIMENSION dimension,
    const ::CD3DX12_CPU_DESCRIPTOR_HANDLE &target)
{
    ASSERT_SLOW(bound());
    ASSERT_FAST(isOnThisDescHeap(target));
    return mDescHeap->createSrvNullHandle(dimension, target);
}

::CD3DX12_CPU_DESCRIPTOR_HANDLE D3D12DescTable::createUavNullHandle(::D3D12_UAV_DIMENSION dimension)
{
    ASSERT_SLOW(bound());
    return mDescHeap->createUavNullHandle(dimension, allocateNext());
}

::CD3DX12_CPU_DESCRIPTOR_HANDLE D3D12DescTable::createUavNullHandle(::D3D12_UAV_DIMENSION dimension,
    const ::CD3DX12_CPU_DESCRIPTOR_HANDLE &target)
{
    ASSERT_SLOW(bound());
    ASSERT_FAST(isOnThisDescHeap(target));
    return mDescHeap->createUavNullHandle(dimension, target);
}

::CD3DX12_CPU_DESCRIPTOR_HANDLE D3D12DescTable::createCbvNullHandle()
{
    ASSERT_SLOW(bound());
    return mDescHeap->createCbvNullHandle(allocateNext());
}

::CD3DX12_CPU_DESCRIPTOR_HANDLE D3D12DescTable::createCbvNullHandle(const ::CD3DX12_CPU_DESCRIPTOR_HANDLE &target)
{
    ASSERT_SLOW(bound());
    ASSERT_FAST(isOnThisDescHeap(target));
    return mDescHeap->createCbvNullHandle(target);
}

::CD3DX12_CPU_DESCRIPTOR_HANDLE D3D12DescTable::createSampler(const ::D3D12_SAMPLER_DESC &desc)
{
    ASSERT_SLOW(bound());
    return mDescHeap->createSampler(desc, allocateNext());
}

::CD3DX12_CPU_DESCRIPTOR_HANDLE D3D12DescTable::createSampler(const ::D3D12_SAMPLER_DESC &desc,
    const ::CD3DX12_CPU_DESCRIPTOR_HANDLE &target)
{
    ASSERT_SLOW(bound());
    ASSERT_FAST(isOnThisDescHeap(target));
    return mDescHeap->createSampler(desc, target);
}

std::size_t D3D12DescTable::offsetFromStart(const ::CD3DX12_CPU_DESCRIPTOR_HANDLE &cpuHandle) const
{
    ASSERT_SLOW(bound());
    return mDescHeap->offsetFromStart(cpuHandle);
}

std::size_t D3D12DescTable::offsetFromTableStart(const ::CD3DX12_CPU_DESCRIPTOR_HANDLE &cpuHandle) const
{
    ASSERT_FAST(isOnThisDescHeap(cpuHandle));
    return offsetFromStart(cpuHandle) - mDescTable.startOffset();
}

bool D3D12DescTable::isOnThisDescHeap(const ::CD3DX12_CPU_DESCRIPTOR_HANDLE &cHandle) const
{
    if (!bound())
    { return false; }

    const auto offset{ offsetFromStart(cHandle) };
    return offset >= mDescTable.startOffset() && offset < mDescTable.endOffset();
}

bool D3D12DescTable::isOnThisDescHeap(const ::CD3DX12_GPU_DESCRIPTOR_HANDLE &gHandle) const
{
    if (!bound())
    { return false; }

    const auto startPtr{ mDescHeap->gpuHandle(mDescHeap->handleFromTable(mDescTable, 0u)).ptr };
    const auto endPtr{ mDescHeap->gpuHandle(mDescHeap->handleFromTable(mDescTable, mDescTable.size())).ptr };

    return gHandle.ptr >= startPtr && gHandle.ptr < endPtr;
}

::CD3DX12_GPU_DESCRIPTOR_HANDLE D3D12DescTable::gpuHandle(const ::CD3DX12_CPU_DESCRIPTOR_HANDLE &cpuHandle) const
{
    ASSERT_SLOW(bound());
    return mDescHeap->gpuHandle(cpuHandle);
}

::CD3DX12_GPU_DESCRIPTOR_HANDLE D3D12DescTable::baseHandle() const
{
    ASSERT_SLOW(bound());
    return mDescHeap->baseTableHandle(mDescTable);
}

void D3D12DescTable::clear()
{ mDescIt = 0u; }

void D3D12DescTable::initializeSrvNullHandles(::D3D12_SRV_DIMENSION dimension)
{
    ASSERT_SLOW(bound());
    for (auto iii = mDescIt; iii < size(); ++iii)
    { mDescHeap->createSrvNullHandle(dimension, mDescHeap->handleFromTable(mDescTable, iii)); }
}

uint32_t D3D12DescTable::copyFrom(const D3D12DescTable &srcTable)
{
    if (!srcTable || !bound() || unallocated() < srcTable.allocated())
    { // Source/destination table is not bound or we don't have enough space -> returning 0.
        return 0u;
    }

    /*
     * Perform a copy from the allocated descriptors of the source table into 
     * the unallocated descriptors of this (destination) table.
     */
    const auto copiedDescriptors{ 
        res::d3d12::D3D12DescHeap::copyTable(
            *srcTable.mDescHeap, srcTable.allocatedTable(), 
            *mDescHeap, unallocatedTable()) };

    // We now have more descriptors in this table -> update!
    mDescIt += copiedDescriptors;
    ASSERT_FAST(mDescIt <= size());

    return copiedDescriptors;
}

D3D12DescTable::DescriptorTable D3D12DescTable::totalTable() const
{ return mDescTable; }

D3D12DescTable::DescriptorTable D3D12DescTable::allocatedTable() const
{
    return DescriptorTable{
        mDescTable.startOffset(),
        mDescTable.startOffset() + mDescIt
    };
}

D3D12DescTable::DescriptorTable D3D12DescTable::unallocatedTable() const
{
    return DescriptorTable{
        mDescTable.startOffset() + mDescIt,
        mDescTable.endOffset()
    };
}

void D3D12DescTable::pop()
{
    if (mDescIt)
    { mDescIt--; }
}

bool D3D12DescTable::bound() const
{ return mDescHeap; }

D3D12DescTable::operator bool() const
{ return bound(); }

uint32_t D3D12DescTable::size() const
{ return mDescTable.size(); }

uint32_t D3D12DescTable::allocated() const
{ return mDescIt; }

uint32_t D3D12DescTable::unallocated() const
{ return size() - allocated(); }

::CD3DX12_CPU_DESCRIPTOR_HANDLE D3D12DescTable::allocateNext()
{
    ASSERT_SLOW(bound());

    if (mDescIt >= size())
    { throw DescHeapFull("Unable to allocate new descriptor in the descriptor table: Descriptor table is full!"); }

    return mDescHeap->handleFromTable(mDescTable, mDescIt++);
}

}

}

}
