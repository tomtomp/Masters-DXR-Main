/**
 * @file helpers/d3d12/D3D12PlacedAllocator.cpp
 * @author Tomas Polasek
 * @brief Allocator for Direct3D 12 placed resources.
 */

#include "stdafx.h"

#include "engine/helpers/d3d12/D3D12PlacedAllocator.h"

namespace quark
{

namespace helpers
{

namespace d3d12
{

D3D12PlacedAllocator::PtrT D3D12PlacedAllocator::create(res::d3d12::D3D12Device &device, res::d3d12::D3D12Heap::PtrT heap)
{ return PtrT{ new D3D12PlacedAllocator(device, heap) }; }

D3D12PlacedAllocator::PtrT D3D12PlacedAllocator::create(res::d3d12::D3D12Device &device, const::CD3DX12_HEAP_DESC &desc)
{ return PtrT{ new D3D12PlacedAllocator(device, desc) }; }

D3D12PlacedAllocator::PtrT D3D12PlacedAllocator::create(res::d3d12::D3D12Device &device, const::D3D12_HEAP_DESC &desc)
{ return PtrT{ new D3D12PlacedAllocator(device, desc) }; }

D3D12PlacedAllocator::D3D12PlacedAllocator()
{ /* Automatic */ }

D3D12PlacedAllocator::D3D12PlacedAllocator(res::d3d12::D3D12Device &device, res::d3d12::D3D12Heap::PtrT heap) :
    mShared{ std::make_shared<SharedAllocatorData>(device, heap) }
{ }

D3D12PlacedAllocator::D3D12PlacedAllocator(res::d3d12::D3D12Device &device, const ::CD3DX12_HEAP_DESC &desc) :
    mShared{ std::make_shared<SharedAllocatorData>(device, desc) }
{ }

D3D12PlacedAllocator::D3D12PlacedAllocator(res::d3d12::D3D12Device &device, const ::D3D12_HEAP_DESC &desc) :
    mShared{ std::make_shared<SharedAllocatorData>(device, ::CD3DX12_HEAP_DESC(desc)) }
{ }

void D3D12PlacedAllocator::allocate(const D3D12_RESOURCE_DESC &desc, D3D12_RESOURCE_STATES initState,
    const D3D12_CLEAR_VALUE *optimizedClearValue, const IID &riid, void **ptr)
{
    ASSERT_FAST(mShared);

    std::unique_lock l(mShared->mtx);
    ASSERT_FAST(mShared->device);

    // Get information about how to allocate for the resource.
    const auto allocInfo{ (*mShared->device).getAllocationInfo(desc) };
    const auto alignedOffset{ util::math::alignTo(mShared->allocationOffset, allocInfo.Alignment) };

    // Allocate.
    util::throwIfFailed((*mShared->device)->CreatePlacedResource(mShared->allocationHeap->get(), 
        alignedOffset, &desc, initState, optimizedClearValue, riid, ptr), 
        "Failed to create placed resource!");

    // Move the offset.
    mShared->allocationOffset = alignedOffset + allocInfo.SizeInBytes;
}

void D3D12PlacedAllocator::deallocate(const IID &riid, void **ptr)
{
    UNUSED(riid);
    UNUSED(ptr);
    // No actions are required.
}

}

}

}
