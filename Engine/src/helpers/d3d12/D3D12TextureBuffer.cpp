/**
 * @file helpers/d3d12/D3D12TextureBuffer.cpp
 * @author Tomas Polasek
 * @brief Wrapper around D3D12Resource, providing a texture interface.
 */

#include "stdafx.h"

#include "engine/helpers/d3d12/D3D12TextureBuffer.h"

#include "engine/helpers/d3d12/D3D12ResourceView.h"

namespace quark
{

namespace helpers
{

namespace d3d12
{

D3D12TextureBuffer::D3D12TextureBuffer()
{ /* Automatic */ }

D3D12TextureBuffer::PtrT D3D12TextureBuffer::create()
{ return PtrT{ new D3D12TextureBuffer() }; }

D3D12TextureBuffer::~D3D12TextureBuffer()
{ /* Automatic */ }

bool D3D12TextureBuffer::descDirty() const
{ return !mResourceView || !mResourceView->valid(); }

::CD3DX12_CPU_DESCRIPTOR_HANDLE D3D12TextureBuffer::cpuHandle() const
{ return mResourceView ? mResourceView->cpuHandle() : ::CD3DX12_CPU_DESCRIPTOR_HANDLE{ }; }

::CD3DX12_GPU_DESCRIPTOR_HANDLE D3D12TextureBuffer::gpuHandle() const
{ return mResourceView ? mResourceView->gpuHandle() : ::CD3DX12_GPU_DESCRIPTOR_HANDLE{ }; }

::D3D12_GPU_VIRTUAL_ADDRESS D3D12TextureBuffer::gpuAddress() const
{ return get()->GetGPUVirtualAddress(); }

void D3D12TextureBuffer::reallocateDescriptor(res::d3d12::D3D12DescAllocatorInterface &descHeap, 
    rndr::ResourceViewType type, ::DXGI_FORMAT forceFormat)
{
    if (!mResourceView || mResourceView->type() != type)
    { mResourceView = D3D12ResourceView::create(D3D12Resource::shared_from_this(), descHeap, type, forceFormat); }

    mResourceView->reallocateDescriptor(descHeap, forceFormat);
}

::D3D12_SHADER_RESOURCE_VIEW_DESC D3D12TextureBuffer::srvDesc() const
{
    // Start with pre-generated description.
    auto result{ D3D12Resource::srvDesc() };

    // No further changes necessary.
    return result;
}

::D3D12_RENDER_TARGET_VIEW_DESC D3D12TextureBuffer::rtvDesc() const
{
    // Start with pre-generated description.
    auto result{ D3D12Resource::rtvDesc() };

    // No further changes necessary.
    return result;
}

}

}

}
