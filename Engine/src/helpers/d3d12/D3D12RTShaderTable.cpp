/**
 * @file helpers/d3d12/D3D12RTShaderTable.cpp
 * @author Tomas Polasek
 * @brief Wrapper around ray tracing shader table, which allows 
 * storing the shader records and their local root parameters.
 */

#include "stdafx.h"

#ifdef ENGINE_RAY_TRACING_ENABLED

#include "engine/helpers/d3d12/D3D12RTShaderTable.h"

#include "engine/helpers/d3d12/D3D12UploadBuffer.h"

#include "engine/util/Math.h"

namespace quark
{

namespace helpers
{

namespace d3d12
{

D3D12RTShaderTable::D3D12RTShaderTable()
{ clear(); }

D3D12RTShaderTable::~D3D12RTShaderTable()
{ /* Automatic */ }

void D3D12RTShaderTable::clear()
{
    mRayGenerationGroups.clear();
    mMissGroups.clear();
    mHitGroups.clear();

    mDirty = true;
    mNameSize = 0u;
    mRayGenerationRecordSize = 0u;
    mMissRecordSize = 0u;
    mHitRecordSize = 0u;
}

void D3D12RTShaderTable::cleanHitGroups()
{
    mHitGroups.clear();
    mDirty = true;
}

::UINT64 D3D12RTShaderTable::calculateSizeRequirements(res::d3d12::D3D12RayTracingDevice &device) 
{
    recalculateSizes(device);

    /*
    const auto rayGenerationSizes{ rayGenerationPartSize() };
    const auto missSizes{ missPartSize() };
    const auto hitSizes{ hitGroupPartSize() };

    return util::math::alignTo(rayGenerationSizes + missSizes + hitSizes, 
        D3D12_RAYTRACING_SHADER_TABLE_BYTE_ALIGNMENT);
        */

    return util::math::alignTo(hitGroupPartSize(), 
        D3D12_RAYTRACING_SHADER_TABLE_BYTE_ALIGNMENT) + 
        hitGroupPartOffset();
}

void D3D12RTShaderTable::build(res::d3d12::D3D12RayTracingDevice &device,
    res::d3d12::D3D12RayTracingStateObject &pipeline, res::d3d12::D3D12Resource &outputBuffer)
{
    recalculateSizes(device);

    log<Info>() << "Building ray tracing shader table:" << std::endl;

    // Calculate how much memory we need to stage the shader table.
    const auto shaderTableSize{ calculateSizeRequirements(device) };
    // Create memory to store the shader table, before sending it to the GPU.
    auto shaderTableStaging{ createStagingStorage(shaderTableSize) };

    // Wrapper for the buffer, so we can upload the data.
    auto uploadBuffer{ D3D12UploadBuffer::create(outputBuffer) };

    if (uploadBuffer->allocatedSize() < shaderTableSize)
    { throw util::winexception("Unable to build the shader table: Provided buffer is not large enough!"); }

    const auto stagingPtrBase{ shaderTableStaging.data() };
    std::size_t currentStagingOffset{ 0u };

    /*
     * The shader table will contain: 
     *   1) Ray generation shader records.
     *   2) Miss shader records.
     *   3) Hit group shader records.
     */

    printLayoutInfo();

    // Check layout correctness and copy to the staging buffer.
    ASSERT_FAST(currentStagingOffset == rayGenerationPartOffset());
    currentStagingOffset += copyRecords(mRayGenerationGroups, 
        stagingPtrBase + currentStagingOffset, 
        pipeline, mRayGenerationRecordSize, mNameSize);

    // Check layout correctness and copy to the staging buffer.
    ASSERT_FAST(currentStagingOffset == missPartOffset());
    currentStagingOffset += copyRecords(mMissGroups, 
        stagingPtrBase + currentStagingOffset, 
        pipeline, mMissRecordSize, mNameSize);

    if (hitGroupPartSize())
    { // Hit groups are optional.
        // Check layout correctness and copy to the staging buffer.
        ASSERT_FAST(currentStagingOffset == hitGroupPartOffset());
        currentStagingOffset += copyRecords(mHitGroups, 
            stagingPtrBase + currentStagingOffset, 
            pipeline, mHitRecordSize, mNameSize);

        // Final check of the layout.
        ASSERT_FAST(util::math::alignTo(hitGroupPartSize(), D3D12_RAYTRACING_SHADER_TABLE_BYTE_ALIGNMENT) + hitGroupPartOffset() == currentStagingOffset);
    }

    // Copy the staged table onto the GPU.
    uploadBuffer->uploadDataImmediate(stagingPtrBase, currentStagingOffset);

    log<Info>() << "Ray tracing shader table build finished." << std::endl;
}

::UINT64 D3D12RTShaderTable::rayGenerationRecordSize() const
{
    ASSERT_FAST(!mDirty);
    return mRayGenerationRecordSize;
}

::UINT64 D3D12RTShaderTable::rayGenerationPartSize() const
{
    ASSERT_FAST(!mDirty);
    return mRayGenerationRecordSize * mRayGenerationGroups.size();
}

::UINT64 D3D12RTShaderTable::rayGenerationPartOffset() const
{
    ASSERT_FAST(!mDirty);
    return 0u;
}

::UINT64 D3D12RTShaderTable::missRecordSize() const
{
    ASSERT_FAST(!mDirty);
    return mMissRecordSize;
}

::UINT64 D3D12RTShaderTable::missPartSize() const
{
    ASSERT_FAST(!mDirty);
    return mMissRecordSize * mMissGroups.size();
}

::UINT64 D3D12RTShaderTable::missPartOffset() const
{
    ASSERT_FAST(!mDirty);
    return util::math::alignTo(rayGenerationPartOffset() + rayGenerationPartSize(), D3D12_RAYTRACING_SHADER_TABLE_BYTE_ALIGNMENT);
}

::UINT64 D3D12RTShaderTable::hitGroupRecordSize() const
{
    ASSERT_FAST(!mDirty);

    if (!hitGroupPartSize())
    { // No hit groups...
        return 0u;
    }

    return mHitRecordSize;
}

::UINT64 D3D12RTShaderTable::hitGroupPartSize() const
{
    ASSERT_FAST(!mDirty);
    return mHitRecordSize * mHitGroups.size();
}

::UINT64 D3D12RTShaderTable::hitGroupPartOffset() const
{
    ASSERT_FAST(!mDirty);

    if (!hitGroupPartSize())
    { // No hit groups...
        return 0u;
    }

    return util::math::alignTo(missPartOffset() + missPartSize(), D3D12_RAYTRACING_SHADER_TABLE_BYTE_ALIGNMENT);
}

void D3D12RTShaderTable::printLayoutInfo() const 
{
    ASSERT_FAST(!mDirty);
    log<Info>() << "Shader Table Layout: "
        "\n\tRay Generation Part: "
        "\n\t\tRecord Size [B]: " << rayGenerationRecordSize() <<
        "\n\t\tRecord Count [n]: " << mRayGenerationGroups.size() <<
        "\n\t\tOffset [B]: " << rayGenerationPartOffset() <<
        "\n\t\tTotal Size [B]: " << rayGenerationPartSize() <<
        "\n\tMiss Part: "
        "\n\t\tRecord Size [B]: " << missRecordSize() <<
        "\n\t\tRecord Count [n]: " << mMissGroups.size() <<
        "\n\t\tOffset [B]: " << missPartOffset() <<
        "\n\t\tTotal Size [B]: " << missPartSize() <<
        "\n\tHit Group Part: "
        "\n\t\tRecord Size [B]: " << hitGroupRecordSize() <<
        "\n\t\tRecord Count [n]: " << mHitGroups.size() <<
        "\n\t\tOffset [B]: " << hitGroupPartOffset() <<
        "\n\t\tTotal Size [B]: " << hitGroupPartSize() << 
        "\n\tFinal Size [B]: " << hitGroupPartOffset() + hitGroupPartSize() << std::endl;
}

D3D12RTShaderTable::ShaderRecord::ShaderRecord(const std::wstring &name) :
    mName{ name }
{ }

std::size_t D3D12RTShaderTable::ShaderRecord::currentArgumentBytes() const
{ return mRootArguments.size(); }

const uint8_t *D3D12RTShaderTable::ShaderRecord::rootArguments() const
{ return mRootArguments.data(); }

const std::wstring & D3D12RTShaderTable::ShaderRecord::name() const
{ return mName; }

D3D12RTShaderTable::ShaderRecord &D3D12RTShaderTable::addShaderRecord(
    const std::wstring &name, std::vector<ShaderRecord> &holder) const
{
    holder.emplace_back(name);
    return holder.back();
}

std::size_t D3D12RTShaderTable::calculateArgumentPrePadding(
    std::size_t sizeOfArgument, std::size_t currentOffset) const
{
    // Alignment should be 4 bytes for smaller values and 8 bytes for larger.
    const auto alignTo{ sizeOfArgument > 4u ? 8u : 4u };

    /*
     * Calculate the remaining bytes required, so that the 
     * alignment is as specified.
     */
    return util::math::alignTo(currentOffset, alignTo) - currentOffset;
}

std::size_t D3D12RTShaderTable::calculateRecordSize(const ShaderRecord &record, ::UINT64 nameSize) const
{
    return util::math::alignTo(nameSize + record.currentArgumentBytes(), 
        //D3D12_RAYTRACING_SHADER_RECORD_BYTE_ALIGNMENT);
        D3D12_RAYTRACING_SHADER_TABLE_BYTE_ALIGNMENT);
}

::UINT64 D3D12RTShaderTable::getNameSize(res::d3d12::D3D12RayTracingDevice &device) const
{ return device.getShaderIdentifierSize(); }

::UINT64 D3D12RTShaderTable::calculateMaxRecordSize(const std::vector<ShaderRecord> &records, ::UINT64 nameSize) const
{
    ::UINT64 result{ 0u };

    for (const auto &record : records)
    { result = std::max(result, calculateRecordSize(record, nameSize)); }

    return result;
}

void D3D12RTShaderTable::recalculateSizes(res::d3d12::D3D12RayTracingDevice &device)
{
    if (!mDirty)
    { // Nothing to be done...
        return;
    }

    mNameSize = getNameSize(device);
    mRayGenerationRecordSize = calculateMaxRecordSize(mRayGenerationGroups, mNameSize);
    mMissRecordSize = calculateMaxRecordSize(mMissGroups, mNameSize);
    mHitRecordSize = calculateMaxRecordSize(mHitGroups, mNameSize);

    mDirty = false;
}

std::vector<uint8_t> D3D12RTShaderTable::createStagingStorage(::UINT64 sizeInBytes) const
{ return std::vector<uint8_t>(sizeInBytes); }

std::size_t D3D12RTShaderTable::copyRecords(const std::vector<ShaderRecord> &holder, uint8_t *dst,
    res::d3d12::D3D12RayTracingStateObject &pipeline, ::UINT64 recordSize, ::UINT64 nameSize)
{
    std::size_t memoryOffset{ 0u };

    for (const auto record : holder)
    { // Copy over all of the records.
        const auto identifier{ pipeline.getShaderIdentifier(record.name().c_str()) };
        if (!identifier)
        {
            log<WDebug>() << "Shader record name has not been found: " << record.name() << std::endl;
            throw UnknownNameException("Unable to get shader identifier, used for a shader record!");
        }

        // Copy the identifier.
        std::memcpy(dst + memoryOffset, identifier, nameSize);
        // Copy the root arguments.
        std::memcpy(dst + memoryOffset + nameSize, record.rootArguments(), record.currentArgumentBytes());

        // Skip the rest of the unused memory, until the next record.
        memoryOffset += recordSize;
    }

    return util::math::alignTo(memoryOffset, D3D12_RAYTRACING_SHADER_TABLE_BYTE_ALIGNMENT);
}

}

}

}

#endif 
