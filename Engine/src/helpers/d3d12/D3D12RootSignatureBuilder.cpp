/**
 * @file helpers/d3d12/D3D12RootSignatureBuilder.cpp
 * @author Tomas Polasek
 * @brief D3D12 root signature configurator and builder.
 */

#include "stdafx.h"

#include "engine/helpers/d3d12/D3D12RootSignatureBuilder.h"

namespace quark
{

namespace helpers
{

namespace d3d12
{

D3D12RootSignatureBuilder::D3D12RootSignatureBuilder()
{ /* Automatic */ }

D3D12RootSignatureBuilder::~D3D12RootSignatureBuilder()
{ /* Automatic */ }

D3D12RootSignatureBuilder &D3D12RootSignatureBuilder::addRootParameter(const ::CD3DX12_ROOT_PARAMETER1 &parameter)
{
    mRootParameters.emplace_back(parameter);
    return *this;
}

D3D12RootSignatureBuilder &D3D12RootSignatureBuilder::addStaticSampler(UINT shaderRegister, D3D12_FILTER filter,
    D3D12_TEXTURE_ADDRESS_MODE addressU, D3D12_TEXTURE_ADDRESS_MODE addressV, D3D12_TEXTURE_ADDRESS_MODE addressW,
    FLOAT mipLODBias, UINT maxAnisotropy, D3D12_COMPARISON_FUNC comparisonFunc, D3D12_STATIC_BORDER_COLOR borderColor,
    FLOAT minLOD, FLOAT maxLOD, D3D12_SHADER_VISIBILITY shaderVisibility, UINT registerSpace)
{
    const ::CD3DX12_STATIC_SAMPLER_DESC s(
        shaderRegister, filter, 
        addressU, addressV, addressW, 
        mipLODBias, maxAnisotropy, 
        comparisonFunc, borderColor, 
        minLOD, maxLOD, 
        shaderVisibility, registerSpace);

    return addStaticSampler(s);
}

D3D12RootSignatureBuilder &D3D12RootSignatureBuilder::operator<<(const ::CD3DX12_ROOT_PARAMETER1 &parameter)
{ return addRootParameter(parameter); }

D3D12RootSignatureBuilder::ParameterWrapper &D3D12RootSignatureBuilder::nextRootParameter()
{
    mRootParameters.emplace_back();
    return mRootParameters.back();
}

D3D12RootSignatureBuilder::ParameterWrapper &D3D12RootSignatureBuilder::operator[](std::size_t index)
{
    if (index > mRootParameters.size())
    { throw IndexOutOfOrderException("Unable to access root parameter: Cannot add new root parameter out of order!"); }
    else if (index < mRootParameters.size())
    { return mRootParameters[index]; }
    else
    { return nextRootParameter(); }
}

const D3D12RootSignatureBuilder::ParameterWrapper &D3D12RootSignatureBuilder::operator[](std::size_t index) const
{
    if (index >= mRootParameters.size())
    { throw IndexOutOfOrderException("Unable to access root parameter: Cannot add new root parameter on const object!"); }
    else 
    { return mRootParameters[index]; }
}

D3D12RootSignatureBuilder &D3D12RootSignatureBuilder::clearRootParameters()
{
    mRootParameters.clear();
    return *this;
}

D3D12RootSignatureBuilder &D3D12RootSignatureBuilder::addStaticSampler(const ::CD3DX12_STATIC_SAMPLER_DESC &sampler)
{
    mStaticSamplers.emplace_back(sampler);
    return *this;
}

D3D12RootSignatureBuilder &D3D12RootSignatureBuilder::operator<<(const ::CD3DX12_STATIC_SAMPLER_DESC &sampler)
{ return addStaticSampler(sampler); }

D3D12RootSignatureBuilder &D3D12RootSignatureBuilder::clearStaticSamplers()
{
    mStaticSamplers.clear();
    return *this;
}

D3D12RootSignatureBuilder &D3D12RootSignatureBuilder::setFlags(::D3D12_ROOT_SIGNATURE_FLAGS flags)
{
    mFlags = flags;
    return *this;
}

res::d3d12::D3D12RootSignature::PtrT D3D12RootSignatureBuilder::build(res::d3d12::D3D12Device &device) const
{
    // Check for support of root signature version 1.1 .
    const auto supportsRS11{ device.supportsFeatures(
        ::D3D12_FEATURE_DATA_ROOT_SIGNATURE{ ::D3D_ROOT_SIGNATURE_VERSION_1_1 }, 
        ::D3D12_FEATURE_ROOT_SIGNATURE) };

    // TODO - Fix, ugly.
    static_assert(sizeof(::CD3DX12_ROOT_PARAMETER1) == sizeof(mRootParameters[0]));
    return res::d3d12::D3D12RootSignature::create(
        reinterpret_cast<const ::CD3DX12_ROOT_PARAMETER1*>(mRootParameters.data()), mRootParameters.size(), 
        mStaticSamplers.data(), mStaticSamplers.size(), 
        mFlags, supportsRS11, device);
}

#ifdef ENGINE_RAY_TRACING_ENABLED

res::d3d12::D3D12RootSignature::PtrT D3D12RootSignatureBuilder::build(
    res::d3d12::D3D12RayTracingDevice &device) const
{
    // Check for support of root signature version 1.1 .
    const auto supportsRS11{ device.supportsFeatures(
        ::D3D12_FEATURE_DATA_ROOT_SIGNATURE{ ::D3D_ROOT_SIGNATURE_VERSION_1_1 }, 
        ::D3D12_FEATURE_ROOT_SIGNATURE) };

    // TODO - Fix, ugly.
    static_assert(sizeof(::CD3DX12_ROOT_PARAMETER1) == sizeof(mRootParameters[0]));
    return res::d3d12::D3D12RootSignature::create(
        reinterpret_cast<const ::CD3DX12_ROOT_PARAMETER1*>(mRootParameters.data()), mRootParameters.size(), 
        mStaticSamplers.data(), mStaticSamplers.size(), 
        mFlags, supportsRS11, device);
}

#endif

std::size_t D3D12RootSignatureBuilder::rootParameterCount() const
{ return mRootParameters.size(); }

std::size_t D3D12RootSignatureBuilder::lastRootParameterIndex() const
{ return rootParameterCount() - 1u; }

std::size_t D3D12RootSignatureBuilder::staticSamplerCount() const
{ return mStaticSamplers.size(); }

void D3D12RootSignatureBuilder::clear()
{
    clearRootParameters();
    clearStaticSamplers();
    mFlags = ::D3D12_ROOT_SIGNATURE_FLAG_NONE;
}

D3D12RootSignatureBuilder::ParameterWrapper::ParameterWrapper(const ::CD3DX12_ROOT_PARAMETER1 &param) :
    ::D3D12_ROOT_PARAMETER1(param)
{ } 

void D3D12RootSignatureBuilder::ParameterWrapper::asDescriptorTableParameter(
    ::UINT numDescriptorRanges, const ::D3D12_DESCRIPTOR_RANGE1 *descriptorRanges, 
    ::D3D12_SHADER_VISIBILITY visibility)
{ ::CD3DX12_ROOT_PARAMETER1::InitAsDescriptorTable(*this, numDescriptorRanges, descriptorRanges, visibility); }

void D3D12RootSignatureBuilder::ParameterWrapper::asConstantParameter(
    ::UINT num32BitValues, ::UINT shaderRegister, ::UINT registerSpace, 
    ::D3D12_SHADER_VISIBILITY visibility)
{ ::CD3DX12_ROOT_PARAMETER1::InitAsConstants(*this, num32BitValues, shaderRegister, registerSpace, visibility); }

void D3D12RootSignatureBuilder::ParameterWrapper::asConstantBufferViewParameter(
    ::UINT shaderRegister, ::UINT registerSpace, ::D3D12_ROOT_DESCRIPTOR_FLAGS flags, 
    ::D3D12_SHADER_VISIBILITY visibility)
{ ::CD3DX12_ROOT_PARAMETER1::InitAsConstantBufferView(*this, shaderRegister, registerSpace, flags, visibility); }

void D3D12RootSignatureBuilder::ParameterWrapper::asShaderResourceViewParameter(
    ::UINT shaderRegister, ::UINT registerSpace, ::D3D12_ROOT_DESCRIPTOR_FLAGS flags, 
    ::D3D12_SHADER_VISIBILITY visibility)
{ ::CD3DX12_ROOT_PARAMETER1::InitAsShaderResourceView(*this, shaderRegister, registerSpace, flags, visibility); }

void D3D12RootSignatureBuilder::ParameterWrapper::asUnorderedAccessViewParameter(
    ::UINT shaderRegister, ::UINT registerSpace, ::D3D12_ROOT_DESCRIPTOR_FLAGS flags, 
    ::D3D12_SHADER_VISIBILITY visibility)
{ ::CD3DX12_ROOT_PARAMETER1::InitAsUnorderedAccessView(*this, shaderRegister, registerSpace, flags, visibility); }

}

}

}
