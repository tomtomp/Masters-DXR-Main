/**
 * @file helpers/d3d12/D3D12GpuTimerManager.cpp
 * @author Tomas Polasek
 * @brief Helper class allowing profiling of GPU 
 * performance using timers.
 */

#include "stdafx.h"

#include "engine/helpers/d3d12/D3D12GpuTimerManager.h"

#include "engine/helpers/d3d12/D3D12CommittedAllocator.h"

namespace quark
{

namespace helpers
{

namespace d3d12
{

D3D12GpuTimerManager::D3D12GpuTimerManager()
{ /* Automatic */ }

D3D12GpuTimerManager::PtrT D3D12GpuTimerManager::create(res::d3d12::D3D12Device &device, 
    res::d3d12::D3D12CommandQueueMgr& directCmdQueue, std::size_t numTimers)
{ return PtrT{ new D3D12GpuTimerManager(device, directCmdQueue, numTimers) }; }

D3D12GpuTimerManager::~D3D12GpuTimerManager()
{ /* Automatic */ }

D3D12GpuTimerManager::D3D12GpuTimerManager(res::d3d12::D3D12Device &device, 
    res::d3d12::D3D12CommandQueueMgr &directCmdQueue, std::size_t numTimers)
{ initialize(device, directCmdQueue, numTimers); }

D3D12GpuTimerManager::TimerHandleT D3D12GpuTimerManager::allocateTimer()
{
    if (!mFreeTimerHandles.empty())
    { // We have some allocated->freed timers.
        const auto handle{ mFreeTimerHandles.back() };
        mFreeTimerHandles.pop_back();
        return handle;
    }
    // Else we need to allocate a new one.
    if (mLastNeverAllocated >= mMaxTimers)
    { throw TimerNotAvailableException("There are no more timers which can be allocated!"); }

    return mLastNeverAllocated++;
}

void D3D12GpuTimerManager::freeTimer(const TimerHandleT &handle)
{
    if (!handleAllocated(handle))
    { throw TimerNotAvailableException("Unable to free provided timer: Timer has not been allocated before!"); }

    mFreeTimerHandles.push_back(handle);
    checkFreeTimers();
}

bool D3D12GpuTimerManager::hasFreeTimers() const
{ return !mFreeTimerHandles.empty() || hasNeverAllocatedTimers(); }

void D3D12GpuTimerManager::mapTimerResults()
{
    // If the buffer is already mapped, unmap it first.
    if (mMappedBuffer)
    { unmapTimerResults(); }

    // Read-map only the part we actually need.
    ::D3D12_RANGE readRange{ 0u, calculateActiveTimerBufferSize() };
    void *mapResult{ nullptr };

    util::throwIfFailed(
        mTimerBuffer->get()->Map(0u, &readRange, &mapResult), 
        "Failed to map timer buffer!");

    mMappedBuffer = static_cast<TimerValue*>(mapResult);
}

void D3D12GpuTimerManager::unmapTimerResults()
{
    // Specify that we did not write anything.
    ::D3D12_RANGE writtenRange{ 0u, 0u };

    mTimerBuffer->get()->Unmap(0u, &writtenRange);
    mMappedBuffer = nullptr;
}

void D3D12GpuTimerManager::startTimer(res::d3d12::D3D12CommandList &cmdList, const TimerHandleT &handle)
{
    if (!handleAllocated(handle))
    { throw TimerNotAvailableException("Unable to start provided timer: Timer has not been allocated before!"); }

    const auto timerIndex{ TimerValue::handleToStartTimer(handle) };
    cmdList->EndQuery(mQueryHeap->get(), ::D3D12_QUERY_TYPE_TIMESTAMP, timerIndex);
}

void D3D12GpuTimerManager::stopTimer(res::d3d12::D3D12CommandList &cmdList, const TimerHandleT &handle)
{
    if (!handleAllocated(handle))
    { throw TimerNotAvailableException("Unable to stop provided timer: Timer has not been allocated before!"); }

    const auto timerIndex{ TimerValue::handleToEndTimer(handle) };
    cmdList->EndQuery(mQueryHeap->get(), ::D3D12_QUERY_TYPE_TIMESTAMP, timerIndex);
}

void D3D12GpuTimerManager::resolveTimers(res::d3d12::D3D12CommandList &cmdList)
{
    if (!mLastNeverAllocated)
    { return; }

    cmdList->ResolveQueryData(
        mQueryHeap->get(), ::D3D12_QUERY_TYPE_TIMESTAMP, 
        0u, static_cast<::UINT>(calculateTotalQueryTimers()), 
        mTimerBuffer->get(), 0u);

#ifndef PROF_USE_TICKS
    ::UINT64 gpuFrequency{ 0u };
    if (SUCCEEDED(mCommandQueue->get()->GetTimestampFrequency(&gpuFrequency)))
    { mGpuTicksPerSecond = gpuFrequency; }
    else
    { log<Info>() << "Failed to get new GPU timestamp frequency!" << std::endl; }
#endif // !PROF_USE_TICKS
}

const D3D12GpuTimerManager::TimerValue &D3D12GpuTimerManager::readTimerRaw(const TimerHandleT &handle) const
{
    if (!mMappedBuffer)
    { throw TimerNotMappedException("Unable to read timer value: mapTimerResults() has not been called!"); }
    if (!handleAllocated(handle))
    { throw TimerNotAvailableException("Unable to read timer value: Timer has not been allocated before!"); }

    return mMappedBuffer[handleToBufferIndex(handle)];
}

std::size_t D3D12GpuTimerManager::readTimer(const TimerHandleT &handle) const
{
    // Recover timer values.
    const auto &timerValue{ readTimerRaw(handle) };

    return timerValue.ticks();
}

float D3D12GpuTimerManager::readTimerMs(const TimerHandleT &handle) const 
{
    ASSERT_SLOW(mGpuTicksPerSecond);

    // Recover timer values.
    const auto &timerValue{ readTimerRaw(handle) };
    // Get duration in floating point milliseconds.
    const auto timerDuration{ 
        timerValue.duration<std::chrono::duration<float, std::milli>>(mGpuTicksPerSecond) };
    // Return only float.
    return timerDuration.count();
}

std::size_t D3D12GpuTimerManager::gpuTicksPerSecond() const
{ return mGpuTicksPerSecond; }

void D3D12GpuTimerManager::debugDumpTimerValues() const
{
    if (!mMappedBuffer)
    { throw TimerNotMappedException("Unable to read timer value: mapTimerResults() has not been called!"); }

    for (std::size_t iii = 0; iii < mMaxTimers; ++iii)
    {
        log<Debug>() << "Timer #" << iii << ": " << 
            mMappedBuffer[iii].start << " - " << mMappedBuffer[iii].end << std::endl;
    }
}

::UINT D3D12GpuTimerManager::TimerValue::handleToStartTimer(const TimerHandleT &handle)
{ return static_cast<::UINT>(handle) * 2; }

::UINT D3D12GpuTimerManager::TimerValue::handleToEndTimer(const TimerHandleT &handle)
{ return handleToStartTimer(handle) + 1u; }

std::size_t D3D12GpuTimerManager::TimerValue::ticks() const
{ return start >= end ? 0u : end - start; }

void D3D12GpuTimerManager::initialize(res::d3d12::D3D12Device &device, 
    res::d3d12::D3D12CommandQueueMgr &directCmdQueue, std::size_t numTimers)
{
    util::throwIfFailed(
        directCmdQueue->GetTimestampFrequency(&mGpuTicksPerSecond), 
        "Failed to get GPU ticks per second.");
    mCommandQueue = directCmdQueue.shared_from_this();

    // Initialize internal values.
    mMappedBuffer = nullptr;
    mLastNeverAllocated = 0u;
    mMaxTimers = numTimers;;
    mFreeTimerHandles.clear();

    auto alloc{ helpers::d3d12::D3D12CommittedAllocator::create(
        device,
        ::CD3DX12_HEAP_PROPERTIES(::D3D12_HEAP_TYPE_READBACK),
        ::D3D12_HEAP_FLAG_NONE) };

    // Create buffer used for storing timer values.
    const auto timerBufferDesc{ ::CD3DX12_RESOURCE_DESC::Buffer(calculateTotalTimerBufferSize()) };
    mTimerBuffer = res::d3d12::D3D12Resource::create(timerBufferDesc, ::D3D12_RESOURCE_STATE_COPY_DEST, nullptr, alloc);
    mTimerBuffer->get()->SetName(L"TimerBuffer");

    // Create heap containing the query timers.
    ::D3D12_QUERY_HEAP_DESC queryHeapDesc{ };
    queryHeapDesc.Type = ::D3D12_QUERY_HEAP_TYPE_TIMESTAMP;
    queryHeapDesc.Count = static_cast<::UINT>(calculateTotalQueryTimers());
    mQueryHeap = res::d3d12::D3D12QueryHeap::create(device, queryHeapDesc);
    mQueryHeap->get()->SetName(L"TimerQueryHeap");
}

bool D3D12GpuTimerManager::hasNeverAllocatedTimers() const
{ return mLastNeverAllocated < mMaxTimers; }

bool D3D12GpuTimerManager::handleAllocated(const TimerHandleT &handle) const
{
    const auto findIt{ std::find(mFreeTimerHandles.begin(), mFreeTimerHandles.end(), handle) };
    return handle < mLastNeverAllocated && findIt == mFreeTimerHandles.end();
}

std::size_t D3D12GpuTimerManager::calculateActiveTimerBufferSize() const
{ return mLastNeverAllocated * sizeof(TimerValue); }

std::size_t D3D12GpuTimerManager::calculateTotalTimerBufferSize() const
{ return mMaxTimers * sizeof(TimerValue); }

std::size_t D3D12GpuTimerManager::calculateTotalQueryTimers() const
{ return 2u * mMaxTimers; }

void D3D12GpuTimerManager::checkFreeTimers()
{
    // Never allocated anything -> no need to check!
    if (!mLastNeverAllocated)
    { return; }

    // Sort the free handles from smallest to largest.
    mFreeTimerHandles.sort(std::less<TimerHandleT>());

    auto removedTimer{ false };
    do
    { // Keep searching for the last timer handle.
        removedTimer = false;

        const auto handle{ mFreeTimerHandles.back() };
        if (handle + 1u == mLastNeverAllocated)
        { // Handle is the same as the last allocated!
            // We can remove it, and say that it was never allocated.
            mLastNeverAllocated = handle;
            mFreeTimerHandles.pop_back();
            // Continue running.
            removedTimer = true;
        }
    } while (removedTimer);
}

std::size_t D3D12GpuTimerManager::handleToBufferIndex(const TimerHandleT &handle) const
{ return static_cast<std::size_t>(handle); }

}

}

}
