/**
 * @file helpers/d3d12/D3D12DummyAllocator.cpp
 * @author Tomas Polasek
 * @brief Dummy allocator for Direct3D 12 placed
 * resources, which allows computation of required
 * size of the target buffer.
 */

#include "stdafx.h"

#include "engine/helpers/d3d12/D3D12DummyAllocator.h"

namespace quark {

namespace helpers {

namespace d3d12 {

D3D12DummyAllocator::D3D12DummyAllocator()
{ /* Automatic */
}

D3D12DummyAllocator::D3D12DummyAllocator(res::d3d12::D3D12Device& device)
	: mShared{std::make_shared<SharedAllocatorData>(device)}
{
}

void D3D12DummyAllocator::allocate(const D3D12_RESOURCE_DESC& desc,
								   D3D12_RESOURCE_STATES initState,
								   const D3D12_CLEAR_VALUE* optimizedClearValue,
								   const IID& riid,
								   void** ptr)
{
	// Dummy -> Nor using all parameters.
	UNUSED(initState);
	UNUSED(optimizedClearValue);
	UNUSED(riid);
	UNUSED(ptr);

	ASSERT_FAST(mShared);

	std::unique_lock l(mShared->mtx);
	ASSERT_FAST(mShared->device);

	// Get information about how to allocate for the resource.
	const auto allocInfo{mShared->device->getAllocationInfo(desc)};
	const auto alignedOffset{util::math::alignTo(mShared->allocatedSize, allocInfo.Alignment)};

	// Add to the allocated size.
	mShared->allocatedSize = alignedOffset + allocInfo.SizeInBytes;
}

void D3D12DummyAllocator::deallocate(const IID& riid, void** ptr)
{
	// Dummy -> Not using the parameters.
	UNUSED(riid);
	UNUSED(ptr);

	/* Nothing to be done. */
}

D3D12DummyAllocator::PtrT D3D12DummyAllocator::create(res::d3d12::D3D12Device &device)
{ return PtrT{ new D3D12DummyAllocator(device) }; }

std::size_t D3D12DummyAllocator::allocated() const noexcept
{
	// TODO - Without locking?
	return mShared ? mShared->allocatedSize : 0u;
}

} // namespace d3d12

} // namespace helpers

} // namespace quark
