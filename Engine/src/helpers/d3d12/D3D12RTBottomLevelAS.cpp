/**
 * @file helpers/d3d12/D3D12RTBottomLevelAS.cpp
 * @author Tomas Polasek
 * @brief Helper used for building D3D12 bottom level acceleration 
 * structure for ray tracing.
 */

#include "stdafx.h"

#ifdef ENGINE_RAY_TRACING_ENABLED

#include "engine/helpers/d3d12/D3D12RTBottomLevelAS.h"

#include "engine/util/BitwiseEnum.h"

namespace quark
{

namespace helpers
{

namespace d3d12
{

D3D12RTBottomLevelAS::D3D12RTBottomLevelAS()
{ clear(); }

D3D12RTBottomLevelAS::~D3D12RTBottomLevelAS()
{ /* Automatic */ }

void D3D12RTBottomLevelAS::clear()
{
    mDirty = true;
    mAllowUpdating = false;
    mMinimizeMemory = false;
    mAllowCompacting = false;
    mGeometry.clear();
    mTriangleCount = 0u;
    mMeshCount = 0u;
    mBaseFlags = DEFAULT_BUILD_PRIORITY;
}

void D3D12RTBottomLevelAS::setAllowUpdating(bool allowUpdating)
{
    mDirty = true;
    mAllowUpdating = allowUpdating;
}

void D3D12RTBottomLevelAS::setMinimizeMemory(bool minimizeMemory)
{
    mDirty = true;
    mMinimizeMemory = minimizeMemory;
}

void D3D12RTBottomLevelAS::setAllowCompacting(bool allowCompacting)
{
    mDirty = true;
    mAllowCompacting = allowCompacting;
}

void D3D12RTBottomLevelAS::preferFastRayTracing()
{
    mDirty = true;
    mBaseFlags = ::D3D12_RAYTRACING_ACCELERATION_STRUCTURE_BUILD_FLAG_PREFER_FAST_TRACE;
}

void D3D12RTBottomLevelAS::preferFastBuild()
{
    mDirty = true;
    mBaseFlags = ::D3D12_RAYTRACING_ACCELERATION_STRUCTURE_BUILD_FLAG_PREFER_FAST_BUILD;
}

void D3D12RTBottomLevelAS::addGeometryNoTransform(const res::rndr::Primitive &primitive)
{
    if (!checkPrimitiveValid(primitive))
    { return; }

    addGeometryDesc(generateGeometryDesc(primitive));
}

void D3D12RTBottomLevelAS::addGeometryNoTransform(const res::rndr::MeshHandle &meshHandle)
{
    const auto &mesh{ meshHandle.get() };

    for (const auto &primitive : mesh.primitives)
    { // Process all primitives within the whole mesh.
        if (!checkPrimitiveValid(primitive))
        { continue; }

        // Generate description.
        addGeometryDesc(generateGeometryDesc(primitive));
    }
}

D3D12RTBottomLevelAS::BufferSizeRequirements D3D12RTBottomLevelAS::calculateSizeRequirements(
    res::d3d12::D3D12RayTracingDevice &device)
{
    recalculateInfoStructures(device);

    return BufferSizeRequirements{ 
        mPreBuildInfo.ResultDataMaxSizeInBytes, 
        mPreBuildInfo.ScratchDataSizeInBytes };
}

uint64_t D3D12RTBottomLevelAS::triangleCount() const
{ return mTriangleCount; }

uint64_t D3D12RTBottomLevelAS::meshCount() const
{ return mMeshCount; }

void D3D12RTBottomLevelAS::build(res::d3d12::D3D12RayTracingDevice &device, 
    res::d3d12::D3D12RayTracingCommandList &directCmdList, 
    res::d3d12::D3D12Resource &scratchBuffer, 
    res::d3d12::D3D12Resource &outputBuffer)
{
    // Refresh information, if any changed occured.
    recalculateInfoStructures(device);

    // Generate building information, describing how to build the AS.
    const auto buildDesc{ generateBuildDesc(mGeometryInputs, 
        scratchBuffer, outputBuffer) };

    // Add building command to the command list.
    directCmdList.apply([&] (auto *cmdList)
    { cmdList->BuildRaytracingAccelerationStructure(&buildDesc, 0u, nullptr); });

    // Setup barrier to wait for completion of all of the building commands.
    const auto barrier{ CD3DX12_RESOURCE_BARRIER::UAV(outputBuffer.get()) };
    directCmdList.cmdList()->ResourceBarrier(1u, &barrier);
}

void D3D12RTBottomLevelAS::build(res::d3d12::D3D12RayTracingDevice &device, 
    res::d3d12::D3D12RayTracingCommandList &directCmdList, 
    res::d3d12::D3D12Resource &scratchBuffer,
    res::d3d12::D3D12Resource &outputBuffer, 
    res::d3d12::D3D12Resource &pastOutputBuffer, 
    bool forceUpdate)
{
    // Refresh information, if any changed occured.
    recalculateInfoStructures(device);

    // We may need to alter the description for updating.
    auto geometryInputs{ mGeometryInputs };

    // Should we update?
    if (mAllowUpdating && pastOutputBuffer)
    { geometryInputs.Flags = addUpdateFlag(geometryInputs.Flags); }
    else if (forceUpdate)
    { throw InvalidUpdate("Cannot force update acceleration structure since it is not configured for updating or pastOutputBuffer is invalid!"); }

    // Generate building information, describing how to build the AS.
    const auto buildDesc{ generateBuildDesc(geometryInputs, 
        scratchBuffer, outputBuffer) };

    // Add building command to the command list.
    directCmdList.apply([&] (auto *cmdList)
    { cmdList->BuildRaytracingAccelerationStructure(&buildDesc, 0u, nullptr); });

    // Setup barrier to wait for completion of all of the building commands.
    const auto barrier{ CD3DX12_RESOURCE_BARRIER::UAV(outputBuffer.get()) };
    directCmdList.cmdList()->ResourceBarrier(1u, &barrier);
}

std::size_t D3D12RTBottomLevelAS::numDescriptions() const
{ return mGeometry.size(); }

bool D3D12RTBottomLevelAS::checkPrimitiveValid(const res::rndr::Primitive &primitive) const
{
    if (!primitive.indices)
    {
        log<Warning>() << "Provided primitive is invalid and cannot be added as "
                          "geometry in the ray tracing acceleration structure: "
                          "No index data!" << std::endl;
        return false;
    }
    if (!primitive.getPositionBuffer())
    {
        log<Warning>() << "Provided primitive is invalid and cannot be added as "
                          "geometry in the ray tracing acceleration structure: "
                          "No positional vertex data!" << std::endl;
        return false;
    }

    return true;
}

::D3D12_RAYTRACING_GEOMETRY_DESC D3D12RTBottomLevelAS::generateGeometryDesc(const res::rndr::Primitive &primitive) const
{
    ::D3D12_RAYTRACING_GEOMETRY_DESC geometryDesc{ };

    // Only triangles are supported for now.
    geometryDesc.Type = ::D3D12_RAYTRACING_GEOMETRY_TYPE_TRIANGLES;
    // For simplicity, all geometry is opaque.
    geometryDesc.Flags = ::D3D12_RAYTRACING_GEOMETRY_FLAG_OPAQUE;
    // No transform specified.
    geometryDesc.Triangles.Transform3x4 = 0u;
    // Specify index buffer used by the primitive.
    const auto &indexBuffer{ primitive.indices.get() };
    geometryDesc.Triangles.IndexFormat = indexBuffer.elementFormat;
    // TODO - Use ::DXGI_FORMAT_UNKNOWN, when indices are not required.
    // R32 slows down the ray tracing fallback layer to a crawl... Does it though?
    //ASSERT_FAST(indexBuffer.elementFormat == ::DXGI_FORMAT_R16_UINT);
    geometryDesc.Triangles.IndexCount = static_cast<::UINT>(indexBuffer.numElements);
    geometryDesc.Triangles.IndexBuffer = indexBuffer.address();
    // Specify vertex buffer used by the primitive.
    const auto &vertexBuffer{ primitive.getPositionBuffer().get() };
    geometryDesc.Triangles.VertexFormat = vertexBuffer.elementFormat;
    geometryDesc.Triangles.VertexCount = static_cast<::UINT>(vertexBuffer.numElements);
    geometryDesc.Triangles.VertexBuffer = vertexBuffer.addressAndStride();

    return geometryDesc;
}

void D3D12RTBottomLevelAS::addGeometryDesc(const ::D3D12_RAYTRACING_GEOMETRY_DESC &desc)
{
    if (desc.Type == D3D12_RAYTRACING_GEOMETRY_TYPE_TRIANGLES)
    { // Update the triangle count statistic.
        mTriangleCount += desc.Triangles.IndexCount / 3u;
    }

    mGeometry.emplace_back(desc);
    mMeshCount++;
    mDirty = true;
}

::D3D12_RAYTRACING_ACCELERATION_STRUCTURE_BUILD_FLAGS D3D12RTBottomLevelAS::generateBuildFlags(
    ::D3D12_RAYTRACING_ACCELERATION_STRUCTURE_BUILD_FLAGS baseFlags, bool allowUpdates, 
    bool minimizeMemory, bool allowCompaction)
{
    auto result{ static_cast<std::size_t>(baseFlags) };

    result |= allowUpdates ? ::D3D12_RAYTRACING_ACCELERATION_STRUCTURE_BUILD_FLAG_ALLOW_UPDATE : 0u;
    result |= minimizeMemory ? ::D3D12_RAYTRACING_ACCELERATION_STRUCTURE_BUILD_FLAG_MINIMIZE_MEMORY : 0u;
    result |= allowCompaction ? ::D3D12_RAYTRACING_ACCELERATION_STRUCTURE_BUILD_FLAG_ALLOW_UPDATE : 0u;

    return static_cast<::D3D12_RAYTRACING_ACCELERATION_STRUCTURE_BUILD_FLAGS>(result);
}

::D3D12_RAYTRACING_ACCELERATION_STRUCTURE_BUILD_FLAGS D3D12RTBottomLevelAS::addUpdateFlag(
    ::D3D12_RAYTRACING_ACCELERATION_STRUCTURE_BUILD_FLAGS startFlags)
{
    auto result{ static_cast<std::size_t>(startFlags) };

    result |= ::D3D12_RAYTRACING_ACCELERATION_STRUCTURE_BUILD_FLAG_PERFORM_UPDATE;

    return static_cast<::D3D12_RAYTRACING_ACCELERATION_STRUCTURE_BUILD_FLAGS>(result);
}

::D3D12_BUILD_RAYTRACING_ACCELERATION_STRUCTURE_INPUTS D3D12RTBottomLevelAS::generateGeometryInputs(
    const std::vector<::D3D12_RAYTRACING_GEOMETRY_DESC> &geometry,
    D3D12_RAYTRACING_ACCELERATION_STRUCTURE_BUILD_FLAGS buildFlags)
{
    ::D3D12_BUILD_RAYTRACING_ACCELERATION_STRUCTURE_INPUTS result{ };

    // Get iteration boundaries for geometry descriptions.
    const auto descCount{ geometry.size() };
    const auto descData{ geometry.data() };

    // Geometry is for the bottom level.
    result.Type = ::D3D12_RAYTRACING_ACCELERATION_STRUCTURE_TYPE_BOTTOM_LEVEL;

    // We may want to ray trace as fast as possible or save memory.
    result.Flags = buildFlags;

    // Specify provided geometry descriptions.
    result.NumDescs = static_cast<::UINT>(descCount);
    result.DescsLayout = ::D3D12_ELEMENTS_LAYOUT_ARRAY;
    result.pGeometryDescs = descData;

    return result;
}

::D3D12_RAYTRACING_ACCELERATION_STRUCTURE_PREBUILD_INFO D3D12RTBottomLevelAS::getPreBuildInfo(
    res::d3d12::D3D12RayTracingDevice &device,
    const ::D3D12_BUILD_RAYTRACING_ACCELERATION_STRUCTURE_INPUTS &buildInputs)
{
    ::D3D12_RAYTRACING_ACCELERATION_STRUCTURE_PREBUILD_INFO result{ };

    // One code for fallback device and hardware.
    device.apply([&](auto d)
    { // Query the device for building information for specified inputs.
        d->GetRaytracingAccelerationStructurePrebuildInfo(&buildInputs, &result);
    });

    return result;
}

void D3D12RTBottomLevelAS::recalculateInfoStructures(
    res::d3d12::D3D12RayTracingDevice &device)
{
    if (!mDirty)
    { // Nothing to be done...
        return;
    }

    // Something changed -> regenerate everything!
    mBuildFlags = generateBuildFlags(mBaseFlags, 
        mAllowUpdating, mMinimizeMemory, mAllowCompacting);
    mGeometryInputs = generateGeometryInputs(mGeometry, mBuildFlags);
    mPreBuildInfo = getPreBuildInfo(device, mGeometryInputs);
    mDirty = false;
}

::D3D12_BUILD_RAYTRACING_ACCELERATION_STRUCTURE_DESC D3D12RTBottomLevelAS::generateBuildDesc(
    const ::D3D12_BUILD_RAYTRACING_ACCELERATION_STRUCTURE_INPUTS &geometryInputs,
    res::d3d12::D3D12Resource &scratchBuffer, res::d3d12::D3D12Resource &outputBuffer,
    res::d3d12::D3D12Resource *pastOutputBuffer)
{
    ::D3D12_BUILD_RAYTRACING_ACCELERATION_STRUCTURE_DESC result{ };

    result.DestAccelerationStructureData = outputBuffer->GetGPUVirtualAddress();
    result.Inputs = geometryInputs;
    result.SourceAccelerationStructureData = pastOutputBuffer ? (*pastOutputBuffer)->GetGPUVirtualAddress() : 0u;
    result.ScratchAccelerationStructureData = scratchBuffer->GetGPUVirtualAddress();

    return result;
}

}

}

}

#endif
