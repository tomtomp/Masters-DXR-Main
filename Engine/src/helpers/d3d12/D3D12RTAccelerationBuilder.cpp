/**
 * @file helpers/d3d12/D3D12RTAccelerationBuilder.cpp
 * @author Tomas Polasek
 * @brief Helper used for building D3D12 acceleration structures 
 * for ray tracing.
 */

#include "stdafx.h"

#ifdef ENGINE_RAY_TRACING_ENABLED

#include "engine/helpers/d3d12/D3D12RTAccelerationBuilder.h"

#include "engine/renderer/GpuProfiling.h"

namespace quark
{

namespace helpers
{

namespace d3d12
{

D3D12RTAccelerationBuilder::D3D12RTAccelerationBuilder()
{ /* Automatic */ }

D3D12RTAccelerationBuilder::PtrT D3D12RTAccelerationBuilder::create(res::d3d12::D3D12RayTracingDevice &device, 
    res::rndr::GpuMemoryMgr &memoryMgr)
{ return PtrT{ new D3D12RTAccelerationBuilder(device, memoryMgr) }; }

D3D12RTAccelerationBuilder::~D3D12RTAccelerationBuilder()
{ /* Automatic */ }

D3D12RTAccelerationBuilder::D3D12RTAccelerationBuilder(res::d3d12::D3D12RayTracingDevice &device,
    res::rndr::GpuMemoryMgr &memoryMgr) :
    mDevice{ &device }, mMemoryMgr{ &memoryMgr }
{ clear(); }

void D3D12RTAccelerationBuilder::clear()
{
    mDirty = true;
    mStatistics.clear();
    mGeometryHolder.clear();
    mInstanceHolder.clear();
    mScratchBuffer = { };
}

void D3D12RTAccelerationBuilder::clearInstances()
{
    mDirty = true;
    mStatistics.resetTlasInfo();
    mInstanceHolder.clear();
}

D3D12RTAccelerationBuilder::GeometryHandleT D3D12RTAccelerationBuilder::addGeometryNoTransform(
    const res::rndr::Primitive &primitive, const std::wstring &name)
{
    mDirty = true;
    return mGeometryHolder.addGeometryNoTransform(primitive, name, !mBuildOptimized);
}

D3D12RTAccelerationBuilder::GeometryHandleT D3D12RTAccelerationBuilder::addGeometryNoTransform(
    const res::rndr::MeshHandle &meshHandle, const std::wstring &name, bool fastBuild, bool noDuplication)
{
    mDirty = true;
    return mGeometryHolder.addGeometryNoTransform(meshHandle, name, fastBuild, noDuplication);
}

std::size_t D3D12RTAccelerationBuilder::geometryUniqueHash(const GeometryHandleT &geometry) const
{ return mGeometryHolder.geometryHandleToIndex(geometry); }

std::size_t D3D12RTAccelerationBuilder::geometryPrimitiveCount(const GeometryHandleT &geometry) const
{ return mGeometryHolder.geometryPrimitiveCount(geometry); }

void D3D12RTAccelerationBuilder::addInstance(const GeometryHandleT &geometry, 
    const helpers::math::Transform &transform, ::UINT shaderTableOffset, 
    ::UINT instanceID, ::UINT instanceMask, ::D3D12_RAYTRACING_INSTANCE_FLAGS flags)
{
    if (!checkGeometryHandle(geometry))
    { throw UnknownGeometryException("Unknown geometry specified: Invalid handle!"); }

    mDirty = true;

    mInstanceHolder.addInstance(geometry, transform, 
        shaderTableOffset, instanceID, instanceMask, flags);
}

void D3D12RTAccelerationBuilder::buildAccelerationStructure(
    res::d3d12::D3D12CommandList &directCmdList,
    D3D12DescTable &buildDescTable)
{
    if (!mDirty)
    { // No actions necessary.
        return;
    }

    if (!buildDescTable.bound() || buildDescTable.size() < requiredDescriptors())
    { throw DescriptorException("Failed to build acceleration structure: Provided descriptor table is too small or invalid!"); }

    if (buildDescTable.allocated())
    { // The last descriptor is the top-level AS, which we will replace.
        buildDescTable.pop();
    }

    PROF_SCOPE("AccelerationStructureBuild");
    GPU_PROF_SCOPE(directCmdList, "AccelerationStructureBuild");

    log<Info>() << "Building ray tracing acceleration structure." << std::endl;

    // Calculate scratch size requirements for both levels.
    const auto bottomLevelScratchSize{ mGeometryHolder.calculateScratchSize(*mDevice) };
    const auto topLevelScratchSize{ mInstanceHolder.calculateScratchSize(*mDevice) };

    // Make sure we have enough space in the scratch buffer to build both levels.
    assureScratchSize(std::max(bottomLevelScratchSize, topLevelScratchSize));

    // Initial state may be different for fallback layer and actual hardware...
    const auto initialASBufferState{ getInitialASBufferState() };

    // Build the bottom level acceleration structures.
    buildBottomLevelAccelerationStructures(mGeometryHolder, *mScratchBuffer, 
        directCmdList, buildDescTable, initialASBufferState);

    // Finalize any staged geometry instances, now that we have bottom level AS buffers.
    mInstanceHolder.processInstances(mGeometryHolder, !mBuildOptimized);

    // Build the top level acceleration structure.
    buildTopLevelAccelerationStructure(mInstanceHolder, *mScratchBuffer, 
        directCmdList, buildDescTable, initialASBufferState);

    // Finalize building of acceleration structure.
    finalizeAccelerationStructure(directCmdList);

    // Display some statistics about the AS.
    log<Info>() << generateStatisticsString() << std::endl;

    mDirty = false;

    log<Info>() << "Acceleration structure build commands recorded!" << std::endl;
}

uint32_t D3D12RTAccelerationBuilder::requiredDescriptors() const
{
    if (mDevice->usingFallbackDevice())
    { // The fallback layer requires descriptors for wrapping memory.
        // Adding one for the top level acceleration structure.
        return static_cast<uint32_t>(mGeometryHolder.foreachGeometry().size() + 1u);
    }
    else
    { // Hardware solution does not require any.
        return 0u;
    }
}

::WRAPPED_GPU_POINTER D3D12RTAccelerationBuilder::getTopLevelAccelerationStruture() const
{
    ASSERT_FAST(!mDirty);
    return mInstanceHolder.wrappedPointer;
}

std::string D3D12RTAccelerationBuilder::generateStatisticsString() const
{ return mStatistics.generateStatistics(); }

const D3D12RTAccelerationBuilder::Statistics &D3D12RTAccelerationBuilder::statistics() const
{ return mStatistics; }

void D3D12RTAccelerationBuilder::preferFastRayTracing()
{ mBuildOptimized = true; }

void D3D12RTAccelerationBuilder::preferFastBuild()
{ mBuildOptimized = false; }

D3D12RTAccelerationBuilder::GeometryHandleT D3D12RTAccelerationBuilder::GeometryHolder::addGeometryNoTransform(
    const res::rndr::Primitive &primitive, const std::wstring &name, bool fastBuild)
{
    // Create information structure containing information about the primitive.
    GeometryInfo info{ };

    info.accelerationStructure.addGeometryNoTransform(primitive);
    if (fastBuild)
    { info.accelerationStructure.preferFastBuild(); }
    else
    { info.accelerationStructure.preferFastRayTracing(); }

    info.name = name;
    mGeometry.emplace_back(std::move(info));

    // Return a handle to it.
    return mGeometry.size();
}

D3D12RTAccelerationBuilder::GeometryHandleT D3D12RTAccelerationBuilder::GeometryHolder::addGeometryNoTransform(
    const res::rndr::MeshHandle &meshHandle, const std::wstring &name, bool fastBuild, bool noDuplication)
{
    // Check if we haven't added it already.
    auto result{ getMeshGeometryHandle(meshHandle) };

    if (checkGeometryHandle(result) && noDuplication)
    { // Already created, return it.
        return result;
    }

    // Create information structure containing information about the mesh.
    GeometryInfo info{ };

    info.accelerationStructure.addGeometryNoTransform(meshHandle);
    if (fastBuild)
    { info.accelerationStructure.preferFastBuild(); }
    else
    { info.accelerationStructure.preferFastRayTracing(); }

    info.name = name;
    mGeometry.emplace_back(std::move(info));

    // Get a handle to the new geometry.
    result = mGeometry.size();

    // Register it, so we don't duplicate it.
    registerMesh(meshHandle, result);

    // And return the handle to the caller.
    return result;
}

bool D3D12RTAccelerationBuilder::GeometryHolder::checkGeometryHandle(const GeometryHandleT &handle) const
{ return handle <= mGeometry.size() && handle != NullGeometryHandle; }

void D3D12RTAccelerationBuilder::GeometryHolder::clear()
{
    mGeometry.clear();
    mMeshToHandle.clear();
}

D3D12RTAccelerationBuilder::GeometryHolder::GeometryInfo &D3D12RTAccelerationBuilder::GeometryHolder::getGeometry(
    const GeometryHandleT &handle)
{ return mGeometry[geometryHandleToIndex(handle)]; }

const D3D12RTAccelerationBuilder::GeometryHolder::GeometryInfo &D3D12RTAccelerationBuilder::GeometryHolder::getGeometry(
    const GeometryHandleT &handle) const
{ return mGeometry[geometryHandleToIndex(handle)]; }

std::size_t D3D12RTAccelerationBuilder::GeometryHolder::calculateScratchSize(
    res::d3d12::D3D12RayTracingDevice &device) 
{
    std::size_t result{ 0u };

    for (auto &geometry : mGeometry)
    { // Check all of the geometry.
        // Skip the already build ones.
        if (geometry.built())
        { continue; }

        const auto requirements{ 
            geometry.accelerationStructure.calculateSizeRequirements(device) };
        // Take the maximum of all of the buffers.
        result = std::max(result, requirements.scratchBufferSize);
    }

    return result;
}

std::size_t D3D12RTAccelerationBuilder::GeometryHolder::geometryHandleToIndex(const GeometryHandleT &handle) const
{
    if (!checkGeometryHandle(handle))
    { throw UnknownGeometryException("Unknown geometry specified: Invalid handle!"); }

    return static_cast<std::size_t>(handle - 1u);
}

std::size_t D3D12RTAccelerationBuilder::GeometryHolder::geometryPrimitiveCount(const GeometryHandleT &handle) const
{
    const auto &geometry{ getGeometry(handle) };
    return geometry.accelerationStructure.numDescriptions();
}

void D3D12RTAccelerationBuilder::GeometryHolder::registerMesh(const res::rndr::MeshHandle &meshHandle,
    const GeometryHandleT &handle)
{ mMeshToHandle.emplace(util::hashAll(meshHandle), handle); }

D3D12RTAccelerationBuilder::GeometryHandleT D3D12RTAccelerationBuilder::GeometryHolder::getMeshGeometryHandle(
    const res::rndr::MeshHandle &meshHandle)
{
    const auto findIt{ mMeshToHandle.find(util::hashAll(meshHandle)) };
    return findIt == mMeshToHandle.end() ? NullGeometryHandle : findIt->second;
}

void D3D12RTAccelerationBuilder::InstanceHolder::processInstances(
    GeometryHolder &geometry, bool fastBuild)
{
    // Update the flag, so we know when to re-build.
    mAddedInstances = mAddedInstances && !mInstancesToProcess.empty();

    if (fastBuild)
    { accelerationStructure.preferFastBuild(); }
    else
    { accelerationStructure.preferFastRayTracing(); }

    for (const auto &record : mInstancesToProcess)
    { // For each staged instance.
        // Get the geometry, used by the instance.
        auto &geometryInfo{ geometry.getGeometry(record.geometry) };
        if (!geometryInfo.accelerationStructureBuffer)
        { throw UnknownGeometryException("Geometry acceleration structure needs to be built before its use in the instance!"); }

        // Finalize the adding.
        accelerationStructure.setBottomLevelAS(record.instanceHandle, geometryInfo.wrappedPointer);
    }
}

void D3D12RTAccelerationBuilder::InstanceHolder::addInstance(const GeometryHandleT &geometry,
    const helpers::math::Transform &transform, ::UINT shaderTableOffset, ::UINT instanceID, ::UINT instanceMask,
    ::D3D12_RAYTRACING_INSTANCE_FLAGS flags)
{
    const auto instanceHandle{ accelerationStructure.addInstance(
            transform, shaderTableOffset, 
            instanceID, instanceMask, flags) };

    mInstancesToProcess.emplace_back(InstanceRecord{ instanceHandle, geometry });
}

void D3D12RTAccelerationBuilder::InstanceHolder::clear()
{
    accelerationStructure.clear();
    accelerationStructureBuffer = { };
    wrappedPointer = { };
    instanceBuffer = { };

    mInstancesToProcess.clear();
}

std::size_t D3D12RTAccelerationBuilder::InstanceHolder::calculateScratchSize(res::d3d12::D3D12RayTracingDevice &device)
{
    const auto requirements{ accelerationStructure.calculateSizeRequirements(device) };
    return requirements.scratchBufferSize;
}

bool D3D12RTAccelerationBuilder::InstanceHolder::addedInstances() const
{ return mAddedInstances; }

void D3D12RTAccelerationBuilder::InstanceHolder::buildComplete()
{ mAddedInstances = false; }

D3D12RTAccelerationBuilder::Statistics::Statistics()
{ clear(); }

void D3D12RTAccelerationBuilder::Statistics::clear()
{ resetBlasInfo(); resetTlasInfo(); resetScratchInfo(); }

std::string D3D12RTAccelerationBuilder::Statistics::generateStatistics() const
{
    std::stringstream ss;
    ss << "Ray tracing acceleration structure building statistics: "
        "\n\tBottom Level Acceleration Structure: "
        "\n\t\tMaximum [B]: " << maximumBlasSize <<
        "\n\t\tMinimum [B]: " << minimumBlasSize <<
        "\n\t\tCurrently Allocated [B]: " << totalBlasSizes <<
        "\n\t\tCurrently Created [num]: " << numBlasCreated <<
        "\n\t\tCurrent Average [B]: " << totalBlasSizes / (numBlasCreated ? numBlasCreated : 1u) <<
        "\n\t\tMaximum Triangles [num]: " << maximumBlasTriangles <<
        "\n\t\tMinimum Triangles [num]: " << minimumBlasTriangles <<
        "\n\t\tTotal Triangles [num]: " << totalBlasTriangles <<
        "\n\t\tMaximum Meshes [num]: " << maximumBlasMeshes <<
        "\n\t\tMinimum Meshes [num]: " << minimumBlasMeshes <<
        "\n\t\tTotal Meshes [num]: " << totalBlasMeshes <<
        "\n\tTop Level Acceleration Structure: "
        "\n\t\tMaximum [B]: " << maximumTlasSize <<
        "\n\t\tMinimum [B]: " << minimumTlasSize <<
        "\n\t\tCurrently Allocated [B]: " << currentTlasSize <<
        "\n\t\tTotal Allocated [B]: " << totalTlasSizes <<
        "\n\t\tTotal Created [num]: " << numTlasCreated <<
        "\n\t\tAverage Allocated [B]: " << totalTlasSizes / (numTlasCreated ? numTlasCreated : 1u) <<
        "\n\t\tMaximum Instances [num]: " << maximumTlasInstances <<
        "\n\t\tMinimum Instances [num]: " << minimumTlasInstances <<
        "\n\t\tTotal Instances [num]: " << totalTlasInstances <<
        "\n\tScratch Buffer: "
        "\n\t\tMaximum [B]: " << maximumScratchSize <<
        "\n\t\tMinimum [B]: " << minimumScratchSize <<
        "\n\t\tCurrently Allocated [B]: " << currentScratchSize <<
        "\n\t\tTotal Allocated [B]: " << totalScratchSizes <<
        "\n\t\tTotal Created [num]: " << numScratchCreated <<
        "\n\t\tAverage Allocated [B]: " << totalScratchSizes / (numScratchCreated ? numScratchCreated : 1u);

    return ss.str();
}

void D3D12RTAccelerationBuilder::Statistics::addBlasInfo(std::size_t sizeInBytes, 
    std::size_t numTriangles, std::size_t numMeshes)
{
    maximumBlasSize = std::max(maximumBlasSize, sizeInBytes);
    minimumBlasSize = std::min(minimumBlasSize, sizeInBytes);
    totalBlasSizes += sizeInBytes;
    numBlasCreated++;

    maximumBlasTriangles = std::max(maximumBlasTriangles, numTriangles);
    minimumBlasTriangles = std::min(minimumBlasTriangles, numTriangles);
    totalBlasTriangles += numTriangles;

    maximumBlasMeshes = std::max(maximumBlasMeshes, numMeshes);
    minimumBlasMeshes = std::min(minimumBlasMeshes, numMeshes);
    totalBlasMeshes += numMeshes;
}

void D3D12RTAccelerationBuilder::Statistics::addTlasInfo(std::size_t sizeInBytes, std::size_t numInstances)
{
    maximumTlasSize = std::max(maximumTlasSize, sizeInBytes);
    minimumTlasSize = std::min(minimumTlasSize, sizeInBytes);
    currentTlasSize = sizeInBytes;
    totalTlasSizes += sizeInBytes;
    numTlasCreated++;;

    maximumTlasInstances = std::max(maximumTlasInstances, numInstances);
    minimumTlasInstances = std::min(minimumTlasInstances, numInstances);
    totalTlasInstances = numInstances;
}

void D3D12RTAccelerationBuilder::Statistics::addScratchInfo(std::size_t sizeInBytes)
{
    maximumScratchSize = std::max(maximumScratchSize, sizeInBytes);
    minimumScratchSize = std::min(minimumScratchSize, sizeInBytes);
    currentScratchSize = sizeInBytes;
    totalScratchSizes += sizeInBytes;
    numScratchCreated++;;
}

void D3D12RTAccelerationBuilder::Statistics::resetBlasInfo()
{
    maximumBlasSize = 0u;
    minimumBlasSize = std::numeric_limits<decltype(minimumBlasSize)>::max();
    totalBlasSizes = 0u;
    numBlasCreated = 0u;

    maximumBlasTriangles = 0u;
    minimumBlasTriangles = std::numeric_limits<decltype(minimumBlasTriangles)>::max();
    totalBlasTriangles = 0u;

    maximumBlasMeshes = 0u;
    minimumBlasMeshes = std::numeric_limits<decltype(minimumBlasMeshes)>::max();
    totalBlasMeshes = 0u;
}

void D3D12RTAccelerationBuilder::Statistics::resetTlasInfo()
{
    maximumTlasSize = 0u;
    minimumTlasSize = std::numeric_limits<decltype(minimumTlasSize)>::max();
    currentTlasSize = 0u;
    totalTlasSizes = 0u;
    numTlasCreated = 0u;

    maximumTlasInstances = 0u;
    minimumTlasInstances = std::numeric_limits<decltype(minimumTlasInstances)>::max();
    totalTlasInstances = 0u;
}

void D3D12RTAccelerationBuilder::Statistics::resetScratchInfo()
{
    maximumScratchSize = 0u;
    minimumScratchSize = std::numeric_limits<decltype(minimumScratchSize)>::max();
    currentScratchSize = 0u;
    totalScratchSizes = 0u;
    numScratchCreated = 0u;
}

bool D3D12RTAccelerationBuilder::checkGeometryHandle(const GeometryHandleT &handle) const
{ return mGeometryHolder.checkGeometryHandle(handle); }

res::d3d12::D3D12Resource::PtrT D3D12RTAccelerationBuilder::createScratchBuffer(
    std::size_t sizeInBytes)
{
    mStatistics.addScratchInfo(sizeInBytes);

    auto result{ res::d3d12::D3D12Resource::create(
        ::CD3DX12_RESOURCE_DESC::Buffer(
            sizeInBytes,
            ::D3D12_RESOURCE_FLAG_ALLOW_UNORDERED_ACCESS, 
            D3D12_DEFAULT_RESOURCE_PLACEMENT_ALIGNMENT),
        ::D3D12_RESOURCE_STATE_UNORDERED_ACCESS,
        nullptr,
        mMemoryMgr->committedAllocator(
            ::CD3DX12_HEAP_PROPERTIES(::D3D12_HEAP_TYPE_DEFAULT),
            ::D3D12_HEAP_FLAG_NONE)) };

    result->get()->SetName(L"ScratchBuffer");

    return result;
}

void D3D12RTAccelerationBuilder::assureScratchSize(std::size_t sizeInBytes)
{
    if (!mScratchBuffer || mScratchBuffer->allocatedSize() < sizeInBytes)
    {
        log<Info>() << "Resizing scratch buffer to new required size: " << sizeInBytes << std::endl;
        mScratchBuffer = createScratchBuffer(sizeInBytes);
    }
}

::D3D12_RESOURCE_STATES D3D12RTAccelerationBuilder::getInitialASBufferState()
{ return mDevice->getASResourceState(); }

res::d3d12::D3D12Resource::PtrT D3D12RTAccelerationBuilder::createBottomLevelBuffer(std::size_t sizeInBytes,
    ::D3D12_RESOURCE_STATES initialASBufferState, const std::wstring &name)
{
    auto result{ res::d3d12::D3D12Resource::create(
        ::CD3DX12_RESOURCE_DESC::Buffer(
            sizeInBytes,
            ::D3D12_RESOURCE_FLAG_ALLOW_UNORDERED_ACCESS, 
            D3D12_DEFAULT_RESOURCE_PLACEMENT_ALIGNMENT),
        initialASBufferState,
        nullptr,
        mMemoryMgr->committedAllocator(
            ::CD3DX12_HEAP_PROPERTIES(::D3D12_HEAP_TYPE_DEFAULT),
            ::D3D12_HEAP_FLAG_NONE)) };

    std::wstring bufferName(L"BLAS_" + name);
    result->get()->SetName(bufferName.c_str());

    return result;
}

res::d3d12::D3D12Resource::PtrT D3D12RTAccelerationBuilder::createTopLevelBuffer(
    std::size_t sizeInBytes, ::D3D12_RESOURCE_STATES initialASBufferState)
{
    auto result{ res::d3d12::D3D12Resource::create(
        ::CD3DX12_RESOURCE_DESC::Buffer(
            sizeInBytes,
            ::D3D12_RESOURCE_FLAG_ALLOW_UNORDERED_ACCESS, 
            D3D12_DEFAULT_RESOURCE_PLACEMENT_ALIGNMENT),
        initialASBufferState,
        nullptr,
        mMemoryMgr->committedAllocator(
            ::CD3DX12_HEAP_PROPERTIES(::D3D12_HEAP_TYPE_DEFAULT),
            ::D3D12_HEAP_FLAG_NONE)) };

    result->get()->SetName(L"TLAS");

    return result;
}

::WRAPPED_GPU_POINTER D3D12RTAccelerationBuilder::createWrapped(
    D3D12DescTable &buildDescTable, res::d3d12::D3D12Resource &resource) const
{
    ::WRAPPED_GPU_POINTER result{ };

    if (mDevice->usingFallbackDevice())
    { // DXR fallback needs to wrap the memory.
        // If needed, wrap the resource for use with DXR fallback.
        result = mDevice->wrapResource(buildDescTable, resource);
    }
    else
    { // Using hardware, no wrapping necessary.
        result.GpuVA = resource->GetGPUVirtualAddress();
    }

    return result;
}

void D3D12RTAccelerationBuilder::buildBottomLevelAccelerationStructures(GeometryHolder &geometry,
    res::d3d12::D3D12Resource &scratchBuffer, res::d3d12::D3D12CommandList &directCmdList, 
    D3D12DescTable &buildDescTable, ::D3D12_RESOURCE_STATES initialASBufferState)
{
    PROF_SCOPE("BuildBottomLevelAS");
    GPU_PROF_SCOPE(directCmdList, "BuildBottomLevelASGpu");

    // Create a wrapper, which handles the fallback layer.
    res::d3d12::D3D12RayTracingCommandList::PtrT rayTracingCmdList{ res::d3d12::D3D12RayTracingCommandList::create(directCmdList) };
    
    if (mDevice->usingFallbackDevice())
    { // We need to bind the descriptor heap, used by fallback layer.
        rayTracingCmdList = mDevice->toRayTracingCmdList(directCmdList);
        rayTracingCmdList->apply([&] (auto *cmdList)
        {
            // Set descriptor heap used for building.
            ::ID3D12DescriptorHeap *descriptorHeaps[] = { buildDescTable.get() };
            cmdList->SetDescriptorHeaps(util::count<::UINT>(descriptorHeaps), descriptorHeaps);
        });
    }

    log<Info>() << "Looking through geometry for un-built bottom level acceleration structures..." << std::endl;

    for (const auto &geometryHandle : geometry.foreachGeometry())
    { // Build all of the bottom level descriptions, which are un-built.
        // Access geometry information.
        auto &geometryInfo{ geometry.getGeometry(geometryHandle) };

        if (geometryInfo.built())
        { // We already created this one -> skip!
            geometryInfo.wrappedPointer = createWrapped(buildDescTable, *geometryInfo.accelerationStructureBuffer);
            continue;
        }

        // Create the output buffer.
        const auto sizeRequirements{ geometryInfo.accelerationStructure.calculateSizeRequirements(*mDevice) };
        const auto sizeInBytes{ sizeRequirements.outputBufferSize };
        const auto numTriangles{ geometryInfo.accelerationStructure.triangleCount() };
        const auto numMeshes{ geometryInfo.accelerationStructure.meshCount() };
        geometryInfo.accelerationStructureBuffer = createBottomLevelBuffer(
            sizeInBytes, initialASBufferState, geometryInfo.name);

        mStatistics.addBlasInfo(sizeInBytes, numTriangles, numMeshes);

#ifdef ENGINE_INFO_MESSAGES
        log<Info>() << "Building bottom level acceleration structure for: \"" << util::toString(geometryInfo.name) <<
            "\" : \n\tScratch Buffer Size [B]: " << sizeRequirements.scratchBufferSize << 
            "\n\tOutput Buffer Size [B]: " << sizeRequirements.outputBufferSize << std::endl;
#endif // ENGINE_INFO_MESSAGES

        // Record the build commands to build the bottom level AS.
        geometryInfo.accelerationStructure.build(
            *mDevice,
            *rayTracingCmdList,
            scratchBuffer,
            *geometryInfo.accelerationStructureBuffer);    
        
        geometryInfo.wrappedPointer = createWrapped(buildDescTable, *geometryInfo.accelerationStructureBuffer);
    }
}

res::d3d12::D3D12Resource::PtrT D3D12RTAccelerationBuilder::createInstanceBuffer(std::size_t sizeInBytes)
{
    // We need an upload buffer!
    auto result{ res::d3d12::D3D12Resource::create(
        ::CD3DX12_RESOURCE_DESC::Buffer(
            sizeInBytes), 
        ::D3D12_RESOURCE_STATE_GENERIC_READ,
        nullptr,
        mMemoryMgr->committedAllocator(
            ::CD3DX12_HEAP_PROPERTIES(::D3D12_HEAP_TYPE_UPLOAD),
            ::D3D12_HEAP_FLAG_NONE)) };

    result->get()->SetName(L"TLAS_Instances");

    return result;
}

void D3D12RTAccelerationBuilder::buildTopLevelAccelerationStructure(InstanceHolder &instances,
    res::d3d12::D3D12Resource &scratchBuffer, res::d3d12::D3D12CommandList &directCmdList, 
    D3D12DescTable &buildDescTable, ::D3D12_RESOURCE_STATES initialASBufferState)
{
    if (!instances.addedInstances())
    { log<Info>() << "Top level acceleration structure does not need rebuilding!" << std::endl; }

    PROF_SCOPE("BuildTopLevelAS");
    GPU_PROF_SCOPE(directCmdList, "BuildTopLevelASGpu");

    // Create a wrapper, which handles the fallback layer.
    res::d3d12::D3D12RayTracingCommandList::PtrT rayTracingCmdList{ mDevice->toRayTracingCmdList(directCmdList) };

    if (mDevice->usingFallbackDevice())
    { // We need to bind the descriptor heap, used by fallback layer.
        rayTracingCmdList->apply([&](auto *cmdList)
        {
            // Set descriptor heap used for building.
            ::ID3D12DescriptorHeap *descriptorHeaps[] = { buildDescTable.get() };
            cmdList->SetDescriptorHeaps(util::count<::UINT>(descriptorHeaps), descriptorHeaps);
        });
    }

    // Create the instance buffer.
    const auto sizeRequirements{ instances.accelerationStructure.calculateSizeRequirements(*mDevice) };
    const auto sizeInBytes{ sizeRequirements.instanceBufferSize };
    instances.instanceBuffer = createInstanceBuffer(sizeInBytes);
    const auto numInstances{ instances.accelerationStructure.instanceCount() };
    mStatistics.addTlasInfo(sizeInBytes, numInstances);

    // Create the output buffer.
    instances.accelerationStructureBuffer = createTopLevelBuffer(sizeRequirements.outputBufferSize, initialASBufferState);

    log<Info>() << "Building the top level acceleration structure: "
        "\n\tInstance Buffer Size [B]: " << sizeRequirements.instanceBufferSize <<
        "\n\tScratch Buffer Size [B]: " << sizeRequirements.scratchBufferSize <<
        "\n\tOutput Buffer Size [B]: " << sizeRequirements.outputBufferSize << std::endl;

    // Record the build commands to build the top level AS.
    instances.accelerationStructure.build(
        *mDevice, 
        *rayTracingCmdList, 
        *instances.instanceBuffer, 
        scratchBuffer, 
        *instances.accelerationStructureBuffer);

    instances.wrappedPointer = createWrapped(buildDescTable, *instances.accelerationStructureBuffer);
}

void D3D12RTAccelerationBuilder::finalizeAccelerationStructure(res::d3d12::D3D12CommandList &directCmdList)
{ 
    UNUSED(directCmdList);
    /* Nothing for now? */ 
}

}

}

}

#endif
