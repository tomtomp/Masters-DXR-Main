/**
 * @file helpers/d3d12/D3D12RTStateObjectSOBuilder.cpp
 * @author Tomas Polasek
 * @brief Generator of state sub-objects used for 
 * state object configuration.
 */

#include "stdafx.h"

#ifdef ENGINE_RAY_TRACING_ENABLED

#include "engine/helpers/d3d12/D3D12RTStateObjectSOBuilder.h"

namespace quark
{

namespace helpers
{

namespace d3d12
{

D3D12StateObjectSubObjectBase::D3D12StateObjectSubObjectBase(
    ::D3D12_STATE_SUBOBJECT &subObject, std::size_t index) : 
    mSubObj{ &subObject }, mIndex{ index }
{ } 
D3D12StateObjectSubObjectBase::~D3D12StateObjectSubObjectBase()
{ /* Automatic */ }

D3D12DxilLibrary::D3D12DxilLibrary(
    ::D3D12_STATE_SUBOBJECT &subObject, std::size_t index) :
    D3D12StateObjectSubObjectBase(subObject, index)
{
    mSubObj->Type = type();
    mSubObj->pDesc = &mDesc;
}

D3D12DxilLibrary::~D3D12DxilLibrary()
{ /* Automatic */ }

void D3D12DxilLibrary::finalize()
{
    mDesc.NumExports = static_cast<::UINT>(mExports.size());
    mDesc.pExports = mExports.data();
}

void D3D12DxilLibrary::setLibrary(::D3D12_SHADER_BYTECODE *code)
{ mDesc.DXILLibrary = code ? *code : ::D3D12_SHADER_BYTECODE{ }; }

void D3D12DxilLibrary::setLibrary(const ::D3D12_SHADER_BYTECODE &code)
{ mDesc.DXILLibrary = code; }

void D3D12DxilLibrary::setLibrary(::ID3DBlob *code)
{
    mDesc.DXILLibrary.pShaderBytecode = code->GetBufferPointer();
    mDesc.DXILLibrary.BytecodeLength = code->GetBufferSize();
}

void D3D12DxilLibrary::defineExport(const wchar_t *exportName, 
    const wchar_t *renameTo, ::D3D12_EXPORT_FLAGS flags)
{
    ::D3D12_EXPORT_DESC exportDesc{ };
    exportDesc.Name = mStrings.store(exportName);
    exportDesc.ExportToRename = mStrings.store(renameTo);
    exportDesc.Flags = flags;
    mExports.emplace_back(exportDesc);
}

void D3D12DxilLibrary::defineExports(const std::vector<std::wstring> &exportNames)
{
    for (const auto &exportName : exportNames)
    { defineExport(exportName.c_str()); }
}

void D3D12DxilLibrary::defineExports(const wchar_t *exportNames[], std::size_t size)
{
    for (std::size_t iii = 0; iii < size; ++iii)
    { defineExport(exportNames[iii]); }
}

D3D12ExistingCollection::D3D12ExistingCollection(
    ::D3D12_STATE_SUBOBJECT &subObject, std::size_t index) :
    D3D12StateObjectSubObjectBase(subObject, index)
{
    mSubObj->Type = type();
    mSubObj->pDesc = &mDesc;
}

D3D12ExistingCollection::~D3D12ExistingCollection()
{ /* Automatic */ }

void D3D12ExistingCollection::finalize()
{
    mDesc.NumExports = static_cast<::UINT>(mExports.size());
    mDesc.pExports = mExports.data();
}

void D3D12ExistingCollection::setState(::ID3D12StateObject *stateObject)
{ mDesc.pExistingCollection = stateObject; }

void D3D12ExistingCollection::defineExport(const wchar_t *exportName, 
    const wchar_t *renameTo, ::D3D12_EXPORT_FLAGS flags)
{
    ::D3D12_EXPORT_DESC exportDesc{ };
    exportDesc.Name = mStrings.store(exportName);
    exportDesc.ExportToRename = mStrings.store(renameTo);
    exportDesc.Flags = flags;
    mExports.emplace_back(exportDesc);
}

void D3D12ExistingCollection::defineExports(const std::vector<std::wstring> &exportNames)
{
    for (const auto &exportName : exportNames)
    { defineExport(exportName.c_str()); }
}

void D3D12ExistingCollection::defineExports(const wchar_t *exportNames[], std::size_t size)
{
    for (std::size_t iii = 0; iii < size; ++iii)
    { defineExport(exportNames[iii]); }
}

D3D12SubObjectsToExports::D3D12SubObjectsToExports(
    ::D3D12_STATE_SUBOBJECT &subObject, std::size_t index) :
    D3D12StateObjectSubObjectBase(subObject, index)
{
    mSubObj->Type = type();
    mSubObj->pDesc = &mDesc;
}

D3D12SubObjectsToExports::~D3D12SubObjectsToExports()
{ /* Automatic */ }

void D3D12SubObjectsToExports::finalize()
{
    mDesc.NumExports = static_cast<::UINT>(mExports.size());
    mDesc.pExports = mExports.data();
}

void D3D12SubObjectsToExports::finalizePointers(::D3D12_STATE_SUBOBJECT *subObjects) noexcept
{ mDesc.pSubobjectToAssociate = subObjects + mAssociatedSubObjectIndex; }

void D3D12SubObjectsToExports::setSubObject(const D3D12StateObjectSubObjectBase *subObject)
{ mAssociatedSubObjectIndex = subObject->index(); }

void D3D12SubObjectsToExports::addExport(const wchar_t *name)
{
    if (!name)
    { return; }

    mExports.emplace_back(mStrings.store(name));
}

void D3D12SubObjectsToExports::addExports(const std::vector<std::wstring> &exportNames)
{
    for (const auto &exportName : exportNames)
    { addExport(exportName.c_str()); }
}

void D3D12SubObjectsToExports::addExports(const wchar_t *exportNames[], std::size_t size)
{
    for (std::size_t iii = 0; iii < size; ++iii)
    { addExport(exportNames[iii]); }
}

D3D12DxilSubObjectsToExports::D3D12DxilSubObjectsToExports(
    ::D3D12_STATE_SUBOBJECT &subObject, std::size_t index) : 
    D3D12StateObjectSubObjectBase(subObject, index)
{
    mSubObj->Type = type();
    mSubObj->pDesc = &mDesc;
}

D3D12DxilSubObjectsToExports::~D3D12DxilSubObjectsToExports()
{ /* Automatic */ }

void D3D12DxilSubObjectsToExports::finalize()
{
    mDesc.NumExports = static_cast<::UINT>(mExports.size());
    mDesc.pExports = mExports.data();
}

void D3D12DxilSubObjectsToExports::setSubObject(const wchar_t *subObject)
{
    mSubObjectName = subObject;
    mDesc.SubobjectToAssociate = mSubObjectName.c_str();
}

void D3D12DxilSubObjectsToExports::addExport(const wchar_t *name)
{
    if (!name)
    { return; }

    mExports.emplace_back(mStrings.store(name));
}

void D3D12DxilSubObjectsToExports::addExports(const std::vector<std::wstring> &exportNames)
{
    for (const auto &exportName : exportNames)
    { addExport(exportName.c_str()); }
}

void D3D12DxilSubObjectsToExports::addExports(const wchar_t *exportNames[], std::size_t size)
{
    for (std::size_t iii = 0; iii < size; ++iii)
    { addExport(exportNames[iii]); }
}

D3D12HitGroup::D3D12HitGroup(
    ::D3D12_STATE_SUBOBJECT &subObject, std::size_t index) : 
    D3D12StateObjectSubObjectBase(subObject, index)
{
    mSubObj->Type = type();
    mSubObj->pDesc = &mDesc;
} 

D3D12HitGroup::~D3D12HitGroup()
{ /* Automatic */ }

void D3D12HitGroup::finalize()
{
    mDesc.HitGroupExport = mHitGroupName.empty() ? nullptr : mHitGroupName.c_str();
    mDesc.AnyHitShaderImport = mAnyHitShader.empty() ? nullptr : mAnyHitShader.c_str();
    mDesc.ClosestHitShaderImport = mClosestHitShader.empty() ? nullptr : mClosestHitShader.c_str();
    mDesc.IntersectionShaderImport = mIntersectionShader.empty() ? nullptr : mIntersectionShader.c_str();
}

void D3D12HitGroup::setName(const wchar_t *name)
{ mHitGroupName = name; }

void D3D12HitGroup::setName(const std::wstring &name)
{ mHitGroupName = name; }

void D3D12HitGroup::setType(::D3D12_HIT_GROUP_TYPE type)
{ mDesc.Type = type; }

void D3D12HitGroup::setAnyHit(const wchar_t *name)
{ mAnyHitShader = name; }

void D3D12HitGroup::setAnyHit(const std::wstring &name)
{ mAnyHitShader = name; }

void D3D12HitGroup::setClosestHit(const wchar_t *name)
{ mClosestHitShader = name; }

void D3D12HitGroup::setClosestHit(const std::wstring &name)
{ mClosestHitShader = name; }

void D3D12HitGroup::setIntersection(const wchar_t *name)
{ mIntersectionShader = name; }

void D3D12HitGroup::setIntersection(const std::wstring &name)
{ mIntersectionShader = name; }

}

}

}

#endif
