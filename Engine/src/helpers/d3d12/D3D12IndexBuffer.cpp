/**
 * @file helpers/d3d12/D3D12IndexBuffer.cpp
 * @author Tomas Polasek
 * @brief Wrapper around D3D12Resource allowing storage of indices.
 */

#include "stdafx.h"

#include "engine/helpers/d3d12/D3D12IndexBuffer.h"

namespace quark
{

namespace helpers
{

namespace d3d12
{

D3D12IndexBuffer::D3D12IndexBuffer()
{ /* Automatic */ }

D3D12IndexBuffer::PtrT D3D12IndexBuffer::create(std::size_t sizeInBytes, D3D12CommittedAllocator::PtrT allocator)
{ return PtrT{ new D3D12IndexBuffer(sizeInBytes, allocator) }; }

D3D12IndexBuffer::~D3D12IndexBuffer()
{ /* Automatic */ }

D3D12IndexBuffer::D3D12IndexBuffer(std::size_t sizeInBytes, D3D12CommittedAllocator::PtrT allocator)
{ allocate(sizeInBytes, allocator); }

void D3D12IndexBuffer::allocate(std::size_t sizeInBytes, D3D12CommittedAllocator::PtrT allocator, 
    ::D3D12_RESOURCE_FLAGS flags, ::D3D12_RESOURCE_STATES initState)
{
    res::d3d12::D3D12Resource::allocate(
        ::CD3DX12_RESOURCE_DESC::Buffer(sizeInBytes, flags), 
        initState, nullptr, allocator);
}

void D3D12IndexBuffer::uploadData(const void *data, std::size_t sizeInBytes, ::DXGI_FORMAT format, 
    res::d3d12::D3D12CommandList &cmdList, D3D12ResourceUpdater &updater)
{
    if (!valid() || sizeInBytes > allocatedSize())
    {
        throw IBTooSmallException("Index buffer is not allocated or it is too small to contain given data!");
    }
    if (sizeInBytes % D3D12Helpers::sizeOfFormatElement(format) != 0u)
    { // Size of data is not a multiple of element size!
        throw IBWrongFormat("Provided data are in the wrong format, size of data is not multiple of element size!");
    }

    updater.copyToResource(data, sizeInBytes, *this, cmdList);

    mIndexBufferView = createIndexBufferView(*this, sizeInBytes, format);
}

void D3D12IndexBuffer::setDebugName(const wchar_t *dbgName)
{
#ifdef _DEBUG
    if (dbgName && valid())
    {
        util::throwIfFailed(get()->SetName(dbgName),
                            "Failed to set index buffer name.");
    }
#else
	UNUSED(dbgName);
#endif
}

::D3D12_INDEX_BUFFER_VIEW D3D12IndexBuffer::createIndexBufferView(const res::d3d12::D3D12Resource &indexBuffer, 
    std::size_t sizeInBytes, ::DXGI_FORMAT indexFormat)
{
    ASSERT_FAST(indexBuffer.desc().Width >= sizeInBytes);
    return ::D3D12_INDEX_BUFFER_VIEW{ indexBuffer->GetGPUVirtualAddress(), 
        static_cast<UINT>(sizeInBytes), indexFormat };
}

}

}

}
