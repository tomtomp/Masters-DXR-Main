/**
 * @file helpers/d3d12/D3D12RTPipeline.cpp
 * @author Tomas Polasek
 * @brief Wrapper around ray tracing pipeline and its state.
 */

#include "stdafx.h"

#ifdef ENGINE_RAY_TRACING_ENABLED

#include "engine/helpers/d3d12/D3D12RTPipeline.h"

namespace quark
{

namespace helpers
{

namespace d3d12
{

D3D12RTPipeline::D3D12RTPipeline()
{ clear(); }

util::PointerType<D3D12RTPipeline>::PtrT D3D12RTPipeline::create()
{ return PtrT{ new D3D12RTPipeline() }; }

D3D12RTPipeline::~D3D12RTPipeline()
{ /* Automatic */ }

void D3D12RTPipeline::clear()
{
    mShaderLibraries.clear();

    mHitGroups.clear();

    mLocalRootSignatures.clear();
    mGlobalRootSignatures.clear();

    mDirty = true;
    mMaxPayloadSize = 0u;
    mMaxAttributeSize = 0u;
    mMaxRayTracingRecursion = 0u;

    mPipeline = { };
}

void D3D12RTPipeline::setMaxPayloadSize(std::size_t sizeInBytes)
{
    if (mMaxPayloadSize == sizeInBytes)
    { return; }

    mMaxPayloadSize = static_cast<::UINT>(sizeInBytes);
    mDirty = true;
}

void D3D12RTPipeline::setMaxAttributeSize(std::size_t sizeInBytes)
{
    if (mMaxAttributeSize == sizeInBytes)
    { return; }

    mMaxAttributeSize = static_cast<::UINT>(sizeInBytes);
    mDirty = true;
}

void D3D12RTPipeline::setMaxRecursiveDepth(std::size_t depth)
{
    if (mMaxRayTracingRecursion == depth)
    { return; }

    mMaxRayTracingRecursion = static_cast<::UINT>(depth);
    mDirty = true;
}

void D3D12RTPipeline::addShaderLibrary(const void *byteCode, std::size_t byteCodeLength,
    const std::initializer_list<std::wstring> &identifiers)
{
    mShaderLibraries.associateEntryPoints({ byteCode, byteCodeLength }, identifiers);
    mDirty = true;
}

void D3D12RTPipeline::addHitGroup(const std::wstring &hitGroupName, const std::wstring &closestHitName,
    const std::wstring &anyHitName, const std::wstring &intersectionName, ::D3D12_HIT_GROUP_TYPE type)
{
    mHitGroups.registerHitGroup(hitGroupName, 
        closestHitName, anyHitName, intersectionName, 
        type);
    mDirty = true;
}

void D3D12RTPipeline::useLocalRootSignature(res::d3d12::D3D12RootSignature &rootSignature,
    const std::initializer_list<std::wstring> &identifiers)
{
    mLocalRootSignatures.associateWith(rootSignature, identifiers);
    mDirty = true;
}

void D3D12RTPipeline::useGlobalRootSignature(res::d3d12::D3D12RootSignature &rootSignature,
    const std::initializer_list<std::wstring> &identifiers)
{
    mGlobalRootSignatures.associateWith(rootSignature, identifiers);
    mDirty = true;
}

res::d3d12::D3D12RayTracingStateObject::PtrT &D3D12RTPipeline::build(
    res::d3d12::D3D12RayTracingDevice &device)
{
    if (!mDirty)
    { return mPipeline; }

    log<Info>() << "Building ray tracing pipeline:" << std::endl;

    D3D12RTStateObjectBuilder builder(::D3D12_STATE_OBJECT_TYPE_RAYTRACING_PIPELINE);

    buildShaderLibraries(builder, mShaderLibraries);
    buildHitGroups(builder, mHitGroups);
    buildLocalRootSignatures(builder, mLocalRootSignatures);
    buildGlobalRootSignatures(builder, mGlobalRootSignatures);
    buildConfigurations(builder, mMaxPayloadSize, mMaxAttributeSize, mMaxRayTracingRecursion);

    mPipeline = builder.build(device);

    log<Info>() << "Ray tracing pipeline build finished." << std::endl;

    mDirty = false;
    return mPipeline;
}

res::d3d12::D3D12RayTracingStateObject::PtrT &D3D12RTPipeline::get()
{
    ASSERT_FAST(!mDirty);
    return mPipeline;
}

void D3D12RTPipeline::buildShaderLibraries(D3D12RTStateObjectBuilder &builder, 
    D3D12RTShaderLibraryRegistry &libraries)
{
    log<Info>() << "Shader Libraries: " << std::endl;

    for (const auto &record : libraries.libraries())
    { // For each library.
        record.printInfo();

        auto library{ builder.add<sosot::DxilLibrary>() };
        { // Add the library record.
            // Set the library byte code.
            library->setLibrary(::CD3DX12_SHADER_BYTECODE(record.byteCode.get(), record.byteCodeLength));

            // And define the exported symbol names.
            library->defineExports(record.associatedEntryPoints);
        } library->finalize();
    }

    log<Info>() << "End of Shader Libraries." << std::endl;
}

void D3D12RTPipeline::buildHitGroups(D3D12RTStateObjectBuilder &builder, 
    D3D12RTHitGroupRegistry &hitGroups)
{
    log<Info>() << "Hit Groups: " << std::endl;

    for (const auto &hitGroupRecord : hitGroups.hitGroups())
    { // For each hit group.
        hitGroupRecord.second.printInfo(hitGroupRecord.first);

        auto hitGroup{ builder.add<sosot::HitGroup>() };
        { // Add the hit group record.
            // Set type of primitives, this group uses.
            hitGroup->setType(hitGroupRecord.second.type);

            // Set the hit group identifier.
            hitGroup->setName(hitGroupRecord.first);

            // Set shader identifiers used by the group.
            hitGroup->setClosestHit(hitGroupRecord.second.closestHitName);
            hitGroup->setAnyHit(hitGroupRecord.second.anyHitName);
            hitGroup->setIntersection(hitGroupRecord.second.intersectionName);
        } hitGroup->finalize();
    }

    log<Info>() << "End of Hit Groups." << std::endl;
}

template <typename RecordT>
void setRootSignatureHelper(RecordT *record, ::ID3D12RootSignature *rs);

template <>
void setRootSignatureHelper<sosot::LocalRootSignature>(sosot::LocalRootSignature *record, ::ID3D12RootSignature *rs)
{ record->desc().pLocalRootSignature = rs; }

template <>
void setRootSignatureHelper<sosot::GlobalRootSignature>(sosot::GlobalRootSignature *record, ::ID3D12RootSignature *rs)
{ record->desc().pGlobalRootSignature = rs; }

template <typename RecordT>
void buildRootSignatures(D3D12RTStateObjectBuilder &builder,
    D3D12RTRootSignatureRegistry &rootSignatures)
{
    for (const auto &signatureRecord : rootSignatures.associations())
    { // for each root signature and its associated shaders.
        signatureRecord.printInfo();

        if (signatureRecord.associatedShaders.empty())
        { // We can skip this one, since no shaders use it...
            log<Info>() << "### This root signature is unused! ###" << std::endl;
            continue;
        }

        auto rootSignature{ builder.add<RecordT>() };
        { // Add the root signature record.
            // Set the root signature pointer.
            setRootSignatureHelper(rootSignature, signatureRecord.rootSignature.Get());
        } rootSignature->finalize();

        auto association{ builder.add<sosot::SubObjExportsAssociation>() };
        { // Associate shader identifiers to the root signature.
            // Set the shader identifiers.
            association->addExports(signatureRecord.associatedShaders);
            // Associate them to the root signature record.
            association->setSubObject(rootSignature);
        } association->finalize();
    }
}

void D3D12RTPipeline::buildLocalRootSignatures(D3D12RTStateObjectBuilder &builder,
    D3D12RTRootSignatureRegistry &localRootSignatures)
{
    log<Info>() << "Local Root Signatures: " << std::endl;
    buildRootSignatures<sosot::LocalRootSignature>(builder, localRootSignatures);
    log<Info>() << "End of Local Root Signatures." << std::endl;
}

void D3D12RTPipeline::buildGlobalRootSignatures(D3D12RTStateObjectBuilder &builder,
    D3D12RTRootSignatureRegistry &globalRootSignatures)
{
    log<Info>() << "Global Root Signatures: " << std::endl;
    buildRootSignatures<sosot::GlobalRootSignature>(builder, globalRootSignatures);
    log<Info>() << "End of Global Root Signatures." << std::endl;
}

void D3D12RTPipeline::buildConfigurations(D3D12RTStateObjectBuilder &builder, 
    UINT maxPayloadSize, UINT maxAttributeSize, UINT maxRayTracingRecursion)
{
    log<Info>() << "Miscellaneous Configuration: " << std::endl;

    log<Info>() << "Shader Configuration: "
        "\n\tMaximum Payload Size [B]: " << maxPayloadSize <<
        "\n\tMaximum Attribute Size [B]: " << maxAttributeSize << std::endl;

    auto shaderConfig{ builder.add<sosot::RayTracingShaderCfg>() };
    { // Add shader configuration record.
        shaderConfig->desc().MaxPayloadSizeInBytes = maxPayloadSize;
        shaderConfig->desc().MaxAttributeSizeInBytes = maxAttributeSize;
    } shaderConfig->finalize();

    log<Info>() << "Pipeline Configuration: "
        "\n\tMaximum Recursion Depth [n]: " << maxRayTracingRecursion << std::endl;

    auto pipelineConfig{ builder.add<sosot::RayTracingPipelineCfg>() };
    { // Add pipeline configuration record.
        pipelineConfig->desc().MaxTraceRecursionDepth = maxRayTracingRecursion;
    } pipelineConfig->finalize();

    log<Info>() << "End of Miscellaneous Configuration: " << std::endl;
}

}

}

}

#endif
