/**
 * @file helpers/d3d12/D3D12ConstantBuffer.cpp
 * @author Tomas Polasek
 * @brief Wrapper around D3D12Resource, allowing to use it 
 * as a constant buffer - CBV.
 */

#include "stdafx.h"

#include "engine/helpers/d3d12/D3D12ConstantBuffer.h"

namespace quark
{

namespace helpers
{

namespace d3d12
{

D3D12ConstantBuffer::D3D12ConstantBuffer()
{ /* Automatic */ }

D3D12ConstantBuffer::~D3D12ConstantBuffer()
{ /* Automatic */ }

void D3D12ConstantBuffer::createCBV(res::d3d12::D3D12DescHeap &descHeap)
{
    mCpuCBV = descHeap.createCBV(*this, static_cast<::UINT>(allocatedSize()));
    mGpuCBV = descHeap.gpuHandle(mCpuCBV);
}

void D3D12ConstantBuffer::clearCBV()
{
    mCpuCBV = ::CD3DX12_CPU_DESCRIPTOR_HANDLE{ };
    mGpuCBV = ::CD3DX12_GPU_DESCRIPTOR_HANDLE{ };
}

::CD3DX12_CPU_DESCRIPTOR_HANDLE D3D12ConstantBuffer::cpuCBV() const
{ return mCpuCBV; }

::CD3DX12_GPU_DESCRIPTOR_HANDLE D3D12ConstantBuffer::gpuCBV() const
{ return mGpuCBV; }

}

}

}
