/**
 * @file helpers/d3d12/D3D12RTStateObjectBuilder.cpp
 * @author Tomas Polasek
 * @brief D3D12 state object configurator and builder.
 */

#include "stdafx.h"

#ifdef ENGINE_RAY_TRACING_ENABLED

#include "engine/helpers/d3d12/D3D12RTStateObjectBuilder.h"

namespace quark
{

namespace helpers
{

namespace d3d12
{

D3D12RTStateObjectBuilder::D3D12RTStateObjectBuilder(::D3D12_STATE_OBJECT_TYPE type) :
    mType{ type }
{ clear(); }

D3D12RTStateObjectBuilder::~D3D12RTStateObjectBuilder()
{ /* Automatic */ }

void D3D12RTStateObjectBuilder::clear()
{
    mSubObjectManagers.clear();
    mSubObjects.clear();
    mLinearSubObjects.clear();
}

res::d3d12::D3D12StateObject::PtrT D3D12RTStateObjectBuilder::build(res::d3d12::D3D12Device &device)
{
    const auto desc{ generateDesc() };
    return res::d3d12::D3D12StateObject::create(desc, device);
}

res::d3d12::D3D12RayTracingStateObject::PtrT D3D12RTStateObjectBuilder::build(res::d3d12::D3D12RayTracingDevice &device)
{
    const auto desc{ generateDesc() };
    return res::d3d12::D3D12RayTracingStateObject::create(desc, device);
}

::D3D12_STATE_OBJECT_DESC D3D12RTStateObjectBuilder::generateDesc()
{
    ::D3D12_STATE_OBJECT_DESC desc{ };
    desc.Type = mType;

    ASSERT_FAST(mSubObjects.size() == mSubObjectManagers.size());

    // Allocate the linear memory.
    mLinearSubObjects.resize(mSubObjects.size());

    // Finalize any pointers.
    for (auto &mgr : mSubObjectManagers)
    { mgr->finalizePointers(mLinearSubObjects.data()); }

    // Store the sub object in linear memory.
    std::size_t index{ 0u };
    for (const auto &subObject : mSubObjects)
    { mLinearSubObjects[index++] = subObject; }

    // Set the pointer in description.
    desc.NumSubobjects = static_cast<::UINT>(mLinearSubObjects.size());
    desc.pSubobjects = mLinearSubObjects.data();

    return desc;
}

}

}

}

#endif
