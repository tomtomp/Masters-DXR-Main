/**
 * @file helpers/d3d12/D3D12VertexBuffer.cpp
 * @author Tomas Polasek
 * @brief Wrapper around D3D12Resource allowing storage of vertices.
 */

#include "stdafx.h"

#include "engine/helpers/d3d12/D3D12VertexBuffer.h"

namespace quark
{

namespace helpers
{

namespace d3d12
{

D3D12VertexBuffer::D3D12VertexBuffer()
{ /* Automatic */ }

D3D12VertexBuffer::PtrT D3D12VertexBuffer::create()
{ return PtrT{ new D3D12VertexBuffer() }; }

D3D12VertexBuffer::PtrT D3D12VertexBuffer::create(std::size_t sizeInBytes, D3D12CommittedAllocator::PtrT allocator)
{ return PtrT{ new D3D12VertexBuffer(sizeInBytes, allocator) }; }

D3D12VertexBuffer::~D3D12VertexBuffer()
{ /* Automatic */ }

D3D12VertexBuffer::D3D12VertexBuffer(std::size_t sizeInBytes, D3D12CommittedAllocator::PtrT allocator)
{ allocate(sizeInBytes, allocator); }

void D3D12VertexBuffer::allocate(std::size_t sizeInBytes, D3D12CommittedAllocator::PtrT allocator, 
    ::D3D12_RESOURCE_FLAGS flags, ::D3D12_RESOURCE_STATES initState)
{
    res::d3d12::D3D12Resource::allocate(
        ::CD3DX12_RESOURCE_DESC::Buffer(sizeInBytes, flags),
        initState, nullptr, allocator);
}

void D3D12VertexBuffer::uploadData(const void *data, std::size_t sizeInBytes, std::size_t sizeOfVertex, 
    res::d3d12::D3D12CommandList &cmdList, D3D12ResourceUpdater &updater)
{
    if (!valid() || sizeInBytes > allocatedSize()) 
    {
        throw VBTooSmallException("Vertex buffer is not allocated or it is too small to contain given data!");
    }

    updater.copyToResource(data, sizeInBytes, *this, cmdList);

    mVertexBufferView = createVertexBufferView(*this, sizeInBytes, sizeOfVertex);
}

void D3D12VertexBuffer::setDebugName(const wchar_t *dbgName)
{
#ifdef _DEBUG
    if (dbgName && valid())
    {
        util::throwIfFailed(get()->SetName(dbgName), 
                            "Failed to set vertex buffer name.");
    }
#else
	UNUSED(dbgName);
#endif
}

::D3D12_VERTEX_BUFFER_VIEW D3D12VertexBuffer::createVertexBufferView(const res::d3d12::D3D12Resource &vertexBuffer, 
    std::size_t sizeInBytes, std::size_t sizeOfVertex)
{
    ASSERT_FAST(vertexBuffer.desc().Width >= sizeInBytes);
    return ::D3D12_VERTEX_BUFFER_VIEW{ vertexBuffer->GetGPUVirtualAddress(), 
        static_cast<UINT>(sizeInBytes), static_cast<UINT>(sizeOfVertex) };
}

}

}

}
