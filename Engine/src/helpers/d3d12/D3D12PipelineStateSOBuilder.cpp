/**
 * @file helpers/d3d12/D3D12PipelineStateSOBuilder.cpp
 * @author Tomas Polasek
 * @brief Generator of pipeline state sub-objects used for pipeline configuration.
 */

#include "stdafx.h"

#include "engine/helpers/d3d12/D3D12PipelineStateSOBuilder.h"

namespace quark
{

namespace helpers
{

namespace d3d12
{

psot::PrimitiveTopology D3D12PipelineStateSOBuilder::primitiveTopology(::D3D12_PRIMITIVE_TOPOLOGY_TYPE type)
{ return type; }

psot::DepthStencilFormat D3D12PipelineStateSOBuilder::depthStencilFormat(::DXGI_FORMAT format)
{ return format; }

psot::RenderTargetFormats D3D12PipelineStateSOBuilder::renderTargetFormats(
    std::initializer_list<::DXGI_FORMAT> rtFormats)
{
    ::D3D12_RT_FORMAT_ARRAY rtfArray;

    if (rtFormats.size() > util::count(rtfArray.RTFormats))
    { throw util::winexception("Too many render-target formats specified!"); }

    rtfArray.NumRenderTargets = static_cast<::UINT>(rtFormats.size());
    for (std::size_t iii = 0u; iii < util::count(rtfArray.RTFormats); ++iii)
    {
        // Put provided formats into the array, filling the rest with UNKNOWNs.
        if (iii < rtFormats.size())
        { rtfArray.RTFormats[iii] = *(rtFormats.begin() + iii); }
        else
        { rtfArray.RTFormats[iii] = ::DXGI_FORMAT_UNKNOWN; }
    }

    //std::copy_n(rtFormats.begin(), util::count(rtfArray.RTFormats), rtfArray.NumRenderTargets);

    return rtfArray;
}

psot::Rasterizer D3D12PipelineStateSOBuilder::rasterizer(::D3D12_CULL_MODE cullMode, bool frontCounterClockwise, bool fillTriangles)
{
    ::CD3DX12_RASTERIZER_DESC desc{ };

    desc.CullMode = cullMode;
    desc.FrontCounterClockwise = frontCounterClockwise;
    desc.FillMode = fillTriangles ? ::D3D12_FILL_MODE_SOLID : ::D3D12_FILL_MODE_WIREFRAME;

    return desc;
}

}

}

}
