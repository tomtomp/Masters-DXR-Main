/**
 * @file helpers/render/MaterialMeshCache.cpp
 * @author Tomas Polasek
 * @brief Cache of materials and meshes, usable when rendering scenes.
 */

#include "stdafx.h"

#include "engine/helpers/render/MaterialMeshCache.h"

namespace quark
{

namespace helpers
{

namespace rndr
{

const MaterialMeshCache::MaterialMeshSpecification *MaterialMeshCache::MaterialMeshSpecificationIterable::begin() const
{ return mSpecifications.data() + mRecord.primitiveRange.begin(); }

const MaterialMeshCache::MaterialMeshSpecification *MaterialMeshCache::MaterialMeshSpecificationIterable::end() const
{ return mSpecifications.data() + mRecord.primitiveRange.end(); }

bool MaterialMeshCache::MaterialMeshSpecificationIterable::valid() const
{
    return mRecord.primitiveRange.begin() <= mRecord.primitiveRange.end() && 
        mRecord.primitiveRange.begin() < mSpecifications.size() && 
        mRecord.primitiveRange.end() <= mSpecifications.size();
}

MaterialMeshCache::MaterialMeshSpecificationIterable::MaterialMeshSpecificationIterable(
    const std::vector<MaterialMeshSpecification> &specifications, const MaterialMeshRecord &record) :
    mSpecifications{ specifications }, mRecord{ record }
{ }

MaterialMeshCache::MaterialMeshCache()
{ clear(); }

MaterialMeshCache::PtrT MaterialMeshCache::create()
{ return PtrT{ new MaterialMeshCache() }; }

MaterialMeshCache::~MaterialMeshCache()
{ /* Automatic */ }

void MaterialMeshCache::registerMesh(const res::rndr::MeshHandle &meshHandle)
{
    if (!meshHandle.valid())
    { return; }

    const auto *record{ findMesh(meshHandle) };
    if (!record)
    { // Not yet registered.
        // Create it and set it as uninitialized.
        auto &newRecord{ findRegisterMesh(meshHandle) };
        newRecord.primitiveRange = { 0u, 0u };
        // Record the request to initialize.
        mMeshRequests.emplace_back(meshHandle);

        mDirty = true;
        mNewSizeRequirementsDirty = true;
    }
}

MaterialMeshCache::LayoutSizeRequirements MaterialMeshCache::calculateSizeRequirements() 
{
    if (mNewSizeRequirementsDirty)
    {
        mNewSizeRequirements = calculateNewSizeRequirements(mMeshRequests, mCachedSizeRequirements);
        mNewSizeRequirementsDirty = false;
    }

    return mNewSizeRequirements;
}

void MaterialMeshCache::buildCache(d3d12::D3D12DescTable &textureTable, 
    d3d12::D3D12DescTable &bufferTable)
{ buildCacheInner(&textureTable, &bufferTable); }

void MaterialMeshCache::buildTextureCache(d3d12::D3D12DescTable &textureTable)
{ buildCacheInner(&textureTable, nullptr); }

MaterialMeshCache::MaterialMeshRecord MaterialMeshCache::recordForMesh(
    const res::rndr::MeshHandle &meshHandle) const
{
    const auto *record{ findMesh(meshHandle) };
    if (!record || record->primitiveRange.size() == 0u)
    { // Not registered or uninitialized.
        throw MeshNotReadyException("Unable to get record for mesh: Mesh is not yet registered or rebuild of cache is required!");
    }

    return *record;
}

MaterialMeshCache::MaterialMeshSpecificationIterable MaterialMeshCache::iterableForMesh(
    const res::rndr::MeshHandle &meshHandle) const
{ return iterableForRecord(recordForMesh(meshHandle)); }

MaterialMeshCache::MaterialMeshSpecificationIterable MaterialMeshCache::iterableForRecord(
    const MaterialMeshRecord &record) const
{ return MaterialMeshSpecificationIterable(mSpecifications, record); }

const MaterialMeshCache::MaterialMeshSpecification &MaterialMeshCache::specificationForPrimitive(
    const res::rndr::MeshHandle &meshHandle, std::size_t primitiveIndex) const
{
    const auto record{ recordForMesh(meshHandle) };

    if (record.primitiveRange.size() <= primitiveIndex)
    { throw MeshNotReadyException("Unable to get primitive specification: Primitive index is out of range!"); }

    const auto specificationIndex{ record.primitiveRange.begin() + primitiveIndex };

    if (specificationIndex >= mSpecifications.size())
    { throw MeshNotReadyException("Unable to get primitive specification: Specification index is out of range!"); }

    return mSpecifications[specificationIndex];
}

bool MaterialMeshCache::dirty() const
{ return mDirty; }

void MaterialMeshCache::clear()
{
    mDirty = true;
    mMeshRegistry.clear();
    mSpecifications.clear();
    mMeshRequests.clear();
    mCachedSizeRequirements = { };
}

MaterialMeshCache::MaterialMeshRecord *MaterialMeshCache::findMesh(
    const res::rndr::MeshHandle &meshHandle)
{
    const auto findIt{ mMeshRegistry.find(meshHandle.hash()) };
    return findIt != mMeshRegistry.end() ? &findIt->second : nullptr;
}

const MaterialMeshCache::MaterialMeshRecord * MaterialMeshCache::findMesh(
    const res::rndr::MeshHandle &meshHandle) const
{
    const auto findIt{ mMeshRegistry.find(meshHandle.hash()) };
    return findIt != mMeshRegistry.end() ? &findIt->second : nullptr;
}

MaterialMeshCache::MaterialMeshRecord &MaterialMeshCache::findRegisterMesh(
    const res::rndr::MeshHandle &meshHandle)
{
    auto *result{ findMesh(meshHandle) };

    if (!result)
    { // We need to register it.
        result = &mMeshRegistry.emplace(meshHandle.hash(), MaterialMeshRecord{ }).first->second;
    }

    return *result;
}

void MaterialMeshCache::processNewRequests(
    std::vector<res::rndr::MeshHandle> &requests)
{
    for (const auto &meshHandle : requests)
    { // For each new mesh request.
        auto *record{ findMesh(meshHandle) };
        ASSERT_SLOW(record);

        ASSERT_SLOW(meshHandle.valid());
        const auto &mesh{ meshHandle.get() };

        if (mesh.primitives.empty())
        { // Skip meshes with no primitives.
            continue;
        }

        // Calculate indices in the record.
        const auto recordStart{ mSpecifications.size() };
        const auto recordEnd{ recordStart + mesh.primitives.size() };

        for (std::size_t iii = 0; iii < mesh.primitives.size(); ++iii)
        { // Create specification for each primitive.
            mSpecifications.emplace_back(
                MaterialMeshSpecification{ 
                    // Record the mesh and index of primitive within.
                    meshHandle, iii, 
                    // Leave mesh and material for later
                    InvalidMeshSpecification,
                    InvalidMaterialSpecification
            });
        }

        // Finalize the record.
        record->primitiveRange = { recordStart, recordEnd };
    }
}

MaterialMeshCache::LayoutSizeRequirements MaterialMeshCache::calculateNewSizeRequirements(
    const std::vector<res::rndr::MeshHandle> &requests, const LayoutSizeRequirements &oldRequirements) const
{
    std::size_t requiredTextureDescriptors{ 0u };
    std::size_t requiredBufferDescriptors{ 0u };

    for (const auto &meshHandle : requests)
    { // Accumulate all primitives.
        const auto &mesh{ meshHandle.get() };

        for (const auto &primitive : mesh.primitives)
        {
            // Mesh specification, one SRV per buffer: 
            if (primitive.indices.valid())
            { requiredBufferDescriptors++; }
            if (primitive.getPositionBuffer().valid())
            { requiredBufferDescriptors++; }
            if (primitive.getTexCoordBuffer().valid())
            { requiredBufferDescriptors++; }
            if (primitive.getNormalBuffer().valid())
            { requiredBufferDescriptors++; }
            if (primitive.getTangentBuffer().valid())
            { requiredBufferDescriptors++; }

            // Material specification, one SRV per texture: 
            if (primitive.material.valid())
            {
                const auto &material{ primitive.material.get() };
                if (material.pbr.baseColorTexture.valid())
                { requiredTextureDescriptors++; }
                if (material.pbr.propertyTexture.valid())
                { requiredTextureDescriptors++; }
                if (material.normalTexture.valid())
                { requiredTextureDescriptors++; }
            }
        }
    }

    return LayoutSizeRequirements{ 
        static_cast<uint32_t>(oldRequirements.textureDescriptors + requiredTextureDescriptors), 
        static_cast<uint32_t>(oldRequirements.bufferDescriptors + requiredBufferDescriptors) };
}

::CD3DX12_CPU_DESCRIPTOR_HANDLE MaterialMeshCache::createBufferUav(
    d3d12::D3D12DescTable &descTable, res::rndr::BufferView &bufferView)
{
    ::D3D12_UNORDERED_ACCESS_VIEW_DESC desc{ };

    desc.Format = ::DXGI_FORMAT_UNKNOWN;
    desc.ViewDimension = D3D12_UAV_DIMENSION_BUFFER;
    desc.Buffer.FirstElement = bufferView.byteOffset / bufferView.byteStride;
    desc.Buffer.NumElements = static_cast<::UINT>(bufferView.numElements);
    desc.Buffer.StructureByteStride = 0u;
    desc.Buffer.CounterOffsetInBytes = 0u;
    desc.Buffer.Flags = ::D3D12_BUFFER_UAV_FLAG_RAW;

    return descTable.createUAV(bufferView.targetBuffer->buffer, desc, nullptr);
}

::CD3DX12_CPU_DESCRIPTOR_HANDLE MaterialMeshCache::createBufferSrv(
    d3d12::D3D12DescTable &descTable, res::rndr::BufferView &bufferView)
{
    /*
    ::D3D12_SHADER_RESOURCE_VIEW_DESC desc{ };

    // TODO - Structured buffer or raw?

    desc.Format = ::DXGI_FORMAT_R32_TYPELESS;
    desc.ViewDimension = ::D3D12_SRV_DIMENSION_BUFFER;
    desc.Shader4ComponentMapping = D3D12_DEFAULT_SHADER_4_COMPONENT_MAPPING;

    //desc.Buffer.NumElements = static_cast<::UINT>(bufferView.numElements / sizeof(uint32_t));

    // Each element is 32b value, so we need to divide.
    desc.Buffer.FirstElement = bufferView.byteOffset / sizeof(uint32_t);
    // Calculate number of 32b words.
    desc.Buffer.NumElements = static_cast<::UINT>(bufferView.numElements * 
        helpers::d3d12::D3D12Helpers::sizeOfFormatElement(bufferView.elementFormat) / sizeof(uint32_t));
    desc.Buffer.StructureByteStride = 0u;
    desc.Buffer.Flags = ::D3D12_BUFFER_SRV_FLAG_RAW;
    */

    auto desc{ bufferView.targetBuffer->buffer.srvDesc() };
    desc.Format = ::DXGI_FORMAT_R32_TYPELESS;
    // Each element is 32b value, so we need to divide.
    desc.Buffer.FirstElement = bufferView.byteOffset / sizeof(uint32_t);
    // Calculate number of 32b words.
    desc.Buffer.NumElements = static_cast<::UINT>(bufferView.numElements * 
        helpers::d3d12::D3D12Helpers::sizeOfFormatElement(bufferView.elementFormat) / sizeof(uint32_t));
    desc.Buffer.StructureByteStride = 0u;
    desc.Buffer.Flags = ::D3D12_BUFFER_SRV_FLAG_RAW;

    return descTable.createSRV(bufferView.targetBuffer->buffer, desc);
}

::CD3DX12_CPU_DESCRIPTOR_HANDLE MaterialMeshCache::createTextureSrv(
    d3d12::D3D12DescTable &descTable, res::rndr::Texture &texture)
{
    /*
    ::D3D12_SHADER_RESOURCE_VIEW_DESC desc{ };

    desc.Format = texture.texture.desc().Format;
    ASSERT_FAST(texture.texture.desc().Dimension == ::D3D12_RESOURCE_DIMENSION_TEXTURE2D);
    desc.ViewDimension = ::D3D12_SRV_DIMENSION_TEXTURE2D;
    desc.Shader4ComponentMapping = D3D12_DEFAULT_SHADER_4_COMPONENT_MAPPING;

    desc.Texture2D.MostDetailedMip = 0u;
    desc.Texture2D.MipLevels = 1u;
    desc.Texture2D.PlaneSlice = 0u;
    desc.Texture2D.ResourceMinLODClamp = 0.0f;
    */

    const auto desc{ texture.texture.srvDesc() };

    return descTable.createSRV(texture.texture, desc);
}

void MaterialMeshCache::allocateDescriptors(std::vector<MaterialMeshSpecification> &specifications,
    d3d12::D3D12DescTable *textureTable, d3d12::D3D12DescTable *bufferTable)
{
    for (auto &specification : specifications)
    {
        const auto &primitive{ specification.meshHandle.get().primitives[specification.primitiveIndex] };

        // Mesh specification, one UAV per buffer: 
        if (bufferTable)
        {
            // TODO - Color buffer?
            if (primitive.indices.valid())
            {
                if (primitive.indices->elementFormat != ::DXGI_FORMAT_R32_UINT)
                { throw InvalidBufferFormat("Unable to build material/mesh cache: Index buffer has invalid format!"); }

                specification.mesh.indices = 
                    static_cast<uint32_t>(bufferTable->offsetFromTableStart(createBufferSrv(*bufferTable, primitive.indices.get())));
            }
            if (primitive.getPositionBuffer().valid())
            {
                if (primitive.getPositionBuffer()->elementFormat != ::DXGI_FORMAT_R32G32B32_FLOAT)
                { throw InvalidBufferFormat("Unable to build material/mesh cache: Vertex buffer has invalid format!"); }

                specification.mesh.positions = 
                    static_cast<uint32_t>(bufferTable->offsetFromTableStart(createBufferSrv(*bufferTable, primitive.getPositionBuffer().get())));
            }
            if (primitive.getTexCoordBuffer().valid())
            {
                if (primitive.getTexCoordBuffer()->elementFormat != ::DXGI_FORMAT_R32G32_FLOAT)
                { throw InvalidBufferFormat("Unable to build material/mesh cache: Texture coordinate buffer has invalid format!"); }

                specification.mesh.texCoords = 
                    static_cast<uint32_t>(bufferTable->offsetFromTableStart(createBufferSrv(*bufferTable, primitive.getTexCoordBuffer().get())));
            }
            if (primitive.getNormalBuffer().valid())
            {
                if (primitive.getNormalBuffer()->elementFormat != ::DXGI_FORMAT_R32G32B32_FLOAT)
                { throw InvalidBufferFormat("Unable to build material/mesh cache: Normal buffer has invalid format!"); }

                specification.mesh.normals = 
                    static_cast<uint32_t>(bufferTable->offsetFromTableStart(createBufferSrv(*bufferTable, primitive.getNormalBuffer().get())));
            }
            if (primitive.getTangentBuffer().valid())
            {
                if (primitive.getTangentBuffer()->elementFormat != ::DXGI_FORMAT_R32G32B32A32_FLOAT)
                { throw InvalidBufferFormat("Unable to build material/mesh cache: Tangent buffer has invalid format!"); }

                specification.mesh.tangents = 
                    static_cast<uint32_t>(bufferTable->offsetFromTableStart(createBufferSrv(*bufferTable, primitive.getTangentBuffer().get())));
            }
        }

        // Material specification, one SRV per texture: 
        if (textureTable && primitive.material.valid())
        {
            const auto &material{ primitive.material.get() };
            if (material.pbr.baseColorTexture.valid())
            {
                specification.material.colorTextureIndex = 
                    static_cast<uint32_t>(
                        textureTable->offsetFromTableStart(createTextureSrv(*textureTable, material.pbr.baseColorTexture.get())));
            }
            if (material.pbr.propertyTexture.valid())
            {
                specification.material.metallicRoughnessTextureIndex = 
                    static_cast<uint32_t>(
                        textureTable->offsetFromTableStart(createTextureSrv(*textureTable, material.pbr.propertyTexture.get())));
            }
            if (material.normalTexture.valid())
            {
                specification.material.normalTextureIndex = 
                    static_cast<uint32_t>(
                        textureTable->offsetFromTableStart(createTextureSrv(*textureTable, material.normalTexture.get())));
            }

            specification.material.specularReflections = material.specularReflections;
            specification.material.specularRefractions = material.specularRefractions;
        }
    }
}

void MaterialMeshCache::buildCacheInner(d3d12::D3D12DescTable *textureTable, d3d12::D3D12DescTable *bufferTable)
{
    if (!mDirty || (!textureTable && !bufferTable))
    { // No need for a rebuild.
        return;
    }

    // Check that the provided tables are valid.
    ASSERT_FAST((!textureTable || textureTable->bound()) && (!bufferTable || bufferTable->bound()));

    // Calculate the required size of the new descriptor heap.
    const auto newSizeRequirements{ calculateSizeRequirements() };

    // Add records and initialize specifications for the new requests.
    log<Info>() << "Adding " << mMeshRequests.size() << " new meshes into the material/mesh cache!" << std::endl;
    processNewRequests(mMeshRequests);
    mMeshRequests.clear();

    log<Info>() << "New cache requires: "
        "\n\tSRV texture descriptors: " << newSizeRequirements.textureDescriptors <<
        "\n\tSRV buffer descriptors: " << newSizeRequirements.bufferDescriptors << std::endl;

    if (textureTable && textureTable->unallocated() < newSizeRequirements.textureDescriptors)
    { throw TableLayoutException("Failed to build material/texture cache: Provided texture descriptor table is not large enough!"); }
    if (bufferTable && bufferTable->unallocated() < newSizeRequirements.bufferDescriptors)
    { throw TableLayoutException("Failed to build material/texture cache: Provided buffer descriptor table is not large enough!"); }

    // Create the descriptors.
    allocateDescriptors(mSpecifications, textureTable, bufferTable);

    // Switch over to the new size.
    mCachedSizeRequirements = newSizeRequirements;

    mDirty = false;
    mNewSizeRequirementsDirty = true;
}

}

}

}
