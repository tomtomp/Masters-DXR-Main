/**
 * @file Engine.cpp
 * @author Tomas Polasek
 * @brief Main engine class, connects resource manager, renderer and application.
 */

#include "stdafx.h"

#include "Engine.h"

namespace quark
{

Engine::Engine(const EngineRuntimeConfig &cfg, HINSTANCE hInstance) :
    mHInstance{ hInstance },
    mThreadPool{ res::thread::ThreadPool::create() },
    mMessageHandler{ app::MessageHandler::create() },
    mWindowClass{ 
        res::win::WindowClass::create(
            mHInstance, 
            Config::WINDOW_CLASS_MAIN_NAME, 
            app::MessageHandler::windowProcCallback()) },
    mWindow{ 
        res::win::Window::create(
            mHInstance, 
            cfg.windowWidth, cfg.windowHeight, 
            cfg.windowTitle, 
            *mWindowClass, 
            util::rawPtrFromPtrT(mMessageHandler)) },
    mWindowRunner{ 
        app::WindowRunner::create(
            *mThreadPool, *mWindow, 
            *mMessageHandler, "WindowThread") },
    mRenderer{ rndr::RenderSubSystem::create(cfg, *mWindow) },
    mSceneSubSystem{ scene::SceneSubSystem::create(cfg, *mRenderer) }, 
    mGuiSubSystem{ gui::GuiSubSystem::create(cfg) }
{ } 

Engine::~Engine()
{
    mWindowRunner->destroyMessagePump();
    mThreadPool->joinAll();
}

} 
