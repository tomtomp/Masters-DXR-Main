/**
 * @file application/WindowRunner.cpp
 * @author Tomas Polasek
 * @brief Simple class used for instantiating a window and running its 
 * message pump on the same thread.
 */

#include "stdafx.h"

#include "engine/application/WindowRunner.h"

namespace quark
{

namespace app
{

WindowRunner::WindowRunner(res::thread::ThreadPool &workerPool, res::win::Window &window, 
    app::MessageHandler &msgHandler, const char *threadName) : 
    mWindow{ &window }
{
    /// Barrier, used to wait for finishing window initialization.
    util::thread::Barrier b{ 2u };
    /// For catching potential exception from runner.
    std::exception_ptr exceptionPtr{ nullptr };

    // TODO - Refactor without future/promise.

    /// Create the runner task.
    mRunnerFinished = workerPool.addSpecializedWorker(threadName, [&] ()
    {
        std::future<MessageHandler&> handlerProvider;

        try
        {
            handlerProvider = mHandlerProvider.get_future();
            window.initialize();
            msgHandler.registerWindow(window);
        } catch (...)
        { // Capture the exception and pass it to main class.
            // If window has been created, it needs to be destroyed in this thread!
            window.destroy();
            exceptionPtr = std::current_exception();
            // Synchronize, to avoid deadlock.
            b.wait();
            // We are finished...
            return;
        }

        /// Synchronize window initialization.
        b.wait();

        while (!mShuttingDown)
        { // Keep running until we shut down.
            try
            {
                runnerMain(handlerProvider);
            } catch (...)
            { }

            // Get future for further communication.
            if (!mShuttingDown)
            { handlerProvider = mHandlerProvider.get_future(); }
        }

        window.destroy();
    });

    /// Synchronize window initialization.
    b.wait();

    if (exceptionPtr)
    { // The runner has thrown.
        std::rethrow_exception(exceptionPtr);
    }
}

WindowRunner::PtrT WindowRunner::create(res::thread::ThreadPool &workerPool, 
    res::win::Window &window, app ::MessageHandler &msgHandler, const char *threadName)
{ return PtrT{ new WindowRunner(workerPool, window, msgHandler, threadName) }; }

WindowRunner::~WindowRunner()
{ destroyMessagePump(); }

void WindowRunner::runMessagePump(MessageHandler &handler)
{
    if (mRunning)
    { throw std::runtime_error("Unable to run message pump, already running!"); }
    // Specify handler, making the runner run message pump.
    mHandlerProvider.set_value(handler);
    mHandlerProvider = { };

    mRunning = true;
}

void WindowRunner::pauseMessagePump()
{ 
    mWindow->sendExitMessage(); 
    mRunning = false;
}

void WindowRunner::destroyMessagePump()
{
    if (mRunning || !mShuttingDown)
    { // Only destroy if there is something to destroy.
        pauseMessagePump();

        // Notify runner we are shutting down.
        mShuttingDown = true;
        if (!mRunning)
        {
            // Wake up the runner, if no handler has been set.
            mHandlerProvider.set_exception(std::make_exception_ptr(nullptr));
        }
        // Wait for it to finish.
        mRunnerFinished.wait();
    }
}

void WindowRunner::runnerMain(std::future<MessageHandler&> &handlerProvider)
{
    // Wait for the runner.
    auto &handler{ handlerProvider.get() };
    // After receiving it, run message pump.
    handler.runBlockingMsgPump();
}

} 

} 
