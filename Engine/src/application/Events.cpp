/**
 * @file application/Events.cpp
 * @author Tomas Polasek
 * @brief Event structures.
 */

#include "stdafx.h"

#include "engine/application/Events.h"

namespace quark
{

namespace app
{

// Extern template definitions.
DEFINE_EXTERN_MESSAGE_BUS(::quark::app::events::KeyboardEvent);
DEFINE_EXTERN_MESSAGE_BUS(::quark::app::events::MouseButtonEvent);
DEFINE_EXTERN_MESSAGE_BUS(::quark::app::events::MouseMoveEvent);
DEFINE_EXTERN_MESSAGE_BUS(::quark::app::events::MouseScrollEvent);
DEFINE_EXTERN_MESSAGE_BUS(::quark::app::events::ResizeEvent);
DEFINE_EXTERN_MESSAGE_BUS(::quark::app::events::ExitEvent);

} 

} 
