/**
 * @file application/FpsCollector.cpp
 * @author Tomas Polasek
 * @brief Helper class used for collection of fps data.
 */

#include "stdafx.h"

#include "engine/application/FpsCollector.h"

namespace quark
{

namespace app
{

FpsCollector::FpsCollector(const FpsUpdateTimer &timer) : 
        mTargetTimer{ &timer }
{ /* Automatic */ }

FpsCollector::~FpsCollector()
{ /* Automatic */ }

void FpsCollector::clear()
{ mData.clear(); }

void FpsCollector::syncLength(std::size_t dataLength)
{
    if (mData.size() >= dataLength)
    { // No action necessary.
        return;
    }

    // Else we need to align it: copy the last data.
    const auto pointCopy{ mData.empty() ? DataPoint{ } : mData.back() };
    const auto toCreate{ dataLength - mData.size() };
    for (std::size_t iii = 0u; iii < toCreate; ++iii)
    { mData.push_back(pointCopy); }
}

std::size_t FpsCollector::collectData()
{ 
    ASSERT_SLOW(mTargetTimer);

    mData.emplace_back(
        DataPoint{ 
            static_cast<float>(mTargetTimer->fps()), 
            static_cast<float>(mTargetTimer->ups()), 
            mTargetTimer->frameTime(), 
            mTargetTimer->grps()
        }); 

    return 1u;
}

void FpsCollector::aggregateData(ProfilingAggregator &aggregator)
{
    std::vector<float> fpsData{ };
    fpsData.reserve(mData.size());
    std::vector<float> upsData{ };
    upsData.reserve(mData.size());
    std::vector<float> frameTimeData{ };
    frameTimeData.reserve(mData.size());
    std::vector<float> grpsData{ };
    grpsData.reserve(mData.size());

    for (const auto &dataPoint : mData)
    {
        fpsData.emplace_back(dataPoint.fps);
        upsData.emplace_back(dataPoint.ups);
        frameTimeData.emplace_back(dataPoint.frameTime);
        grpsData.emplace_back(dataPoint.grps);
    }

    aggregator.addDataSeries<float>().setName("FPS").setData(std::move(fpsData));
    aggregator.addDataSeries<float>().setName("UPS").setData(std::move(upsData));
    aggregator.addDataSeries<float>().setName("FrameTime").setData(std::move(frameTimeData));
    aggregator.addDataSeries<float>().setName("GigaRays/s").setData(std::move(grpsData));
}

std::size_t FpsCollector::size() const
{ return mData.size(); }

}

}
