/**
 * @file application/ApplicationGui.cpp
 * @author Tomas Polasek
 * @brief Container for common GUI variables used by 
 * all applications.
 */

#include "stdafx.h"
#include "engine/application/ApplicationGui.h"

#include "engine/gui/GuiSubSystem.h"

namespace quark
{

namespace app
{

ApplicationGui::ApplicationGui(const EngineRuntimeConfig &cfg) :
    mCfg{ cfg },
    mRenderWidth{ cfg.windowWidth }, 
    mRenderHeight{ cfg.windowHeight }
{ /* Automatic */ }

ApplicationGui::~ApplicationGui()
{ /* Automatic */ }

void ApplicationGui::prepareGui(gui::GuiSubSystem &gui)
{
    // Create the main window, which will be our default context.
    const auto windowTitle{ util::toString(mCfg.windowTitle) };
    gui.addGuiContext("/MainWindow/", true)
        .setType(quark::gui::GuiContextType::Window)
        .setLabel(windowTitle.c_str())
        .cfg().setWidth(mCfg.guiWidth).setHeight(static_cast<float>(mCfg.windowHeight))
            .setPosition(0.0f, 0.0f);

    gui.addGuiContext("/Stats/")
        .setType(quark::gui::GuiContextType::Category)
        .setLabel("Statistics");
    gui.addGuiVariable<gui::GraphFloat>("CurrentFps", "/Stats/", { })
        .setLabel("FPS [n]")
        .cfg().setMinValue(0.0f);
    gui.addGuiVariable<gui::GraphFloat>("CurrentUps", "/Stats/", { })
        .setLabel("UPS [n]")
        .cfg().setMinValue(0.0f);
    gui.addGuiVariable<gui::GraphFloat>("CurrentFrameTime", "/Stats/", { })
        .setLabel("FT [ms]")
        .cfg().setMinValue(0.0f);
    gui.addGuiVariable<gui::GraphFloat>("CurrentGigaRays", "/Stats/", { })
        .setLabel("RT [GR/s]")
        .cfg().setMinValue(0.0f);

    gui.addGuiVariable<gui::InputInt>("CurrentWidth", "/Stats/", { 0u })
        .setLabel("Width [px]")
        .cfg().setEditable(false)
            .setStep(0);
    gui.addGuiVariable<gui::InputInt>("CurrentHeight", "/Stats/", { 0u })
        .setLabel("Height [px]")
        .cfg().setEditable(false)
            .setStep(0);
}

void ApplicationGui::updateGui(gui::GuiSubSystem &gui)
{
    gui.getVariable<gui::GraphFloat>("CurrentFps").pushDataPoint(static_cast<float>(mFps), PERF_GRAPH_DATA_POINTS);
    gui.getVariable<gui::GraphFloat>("CurrentUps").pushDataPoint(static_cast<float>(mUps), PERF_GRAPH_DATA_POINTS);
    gui.getVariable<gui::GraphFloat>("CurrentFrameTime").pushDataPoint(mFrameTime, PERF_GRAPH_DATA_POINTS);
    gui.getVariable<gui::GraphFloat>("CurrentGigaRays").pushDataPoint(mGrps, PERF_GRAPH_DATA_POINTS);

    gui.getVariable<gui::InputInt>("CurrentWidth").value = static_cast<int>(mRenderWidth);
    gui.getVariable<gui::InputInt>("CurrentHeight").value = static_cast<int>(mRenderHeight);
}

void ApplicationGui::updateFpsTimer(const FpsUpdateTimer &timer)
{
    mFps = timer.fps();
    mUps = timer.ups();
    mFrameTime = timer.frameTime();
    mGrps = timer.grps();
}

void ApplicationGui::updateResolution(const events::ResizeEvent &event)
{
    mRenderWidth = event.newWidth;
    mRenderHeight = event.newHeight;
}

}

}
