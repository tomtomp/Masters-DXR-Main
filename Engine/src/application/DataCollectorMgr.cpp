/**
 * @file application/DataCollectorMgr.cpp
 * @author Tomas Polasek
 * @brief Helper class used for managing multiple 
 * data collectors and their synchronization.
 */

#include "stdafx.h"

#include "engine/application/DataCollectorMgr.h"

#include "engine/application/ProfilingAggregator.h"

namespace quark
{

namespace app
{

BaseCollector::BaseCollector()
{ /* Automatic */ }

BaseCollector::~BaseCollector()
{ /* Automatic */ }

DataCollectorMgr::Aggregation::Aggregation(const std::string &name) :
    mName{ name }
{ }

void DataCollectorMgr::Aggregation::setVerticalTable(bool enable)
{ mVerticalTable = enable; }

void DataCollectorMgr::Aggregation::setAutoIndexing(bool enable)
{ mAutoIndexing = enable; }

std::size_t DataCollectorMgr::Aggregation::maxDataSize() const
{
    const auto maxSize{ std::accumulate(mCollectors.begin(), mCollectors.end(), std::size_t{ 0u },
        [](std::size_t size, const auto &collector) 
        { return std::max(size, collector->size()); }) };

    return maxSize;
}
void DataCollectorMgr::Aggregation::clear()
{ mCollectors.clear(); }

DataCollectorMgr::DataCollectorMgr()
{ /* Automatic */ }

DataCollectorMgr::~DataCollectorMgr()
{ /* Automatic */ }

void DataCollectorMgr::clear()
{
    for (const auto &aggregator : mAggregators)
    {
        for (const auto &collector : aggregator.mCollectors)
        { collector->clear(); }
    }
}

void DataCollectorMgr::reset()
{ mAggregators.clear(); }

void DataCollectorMgr::collectData()
{
    for (const auto &aggregator : mAggregators)
    {
        const auto maxSize{ aggregator.maxDataSize() };
        std::size_t maxAdded{ 0u };

        for (const auto &collector : aggregator.mCollectors)
        { 
            collector->syncLength(maxSize);
            const auto newAdded{ collector->collectData() };
            maxAdded = std::max(maxAdded, newAdded);
        }

        if (maxAdded)
        { // Synchronize new additions.
            for (const auto &collector : aggregator.mCollectors)
            { collector->syncLength(maxSize + maxAdded); }
        }
    }
}

DataCollectorMgr::Aggregation &DataCollectorMgr::pushAggregation(const std::string &name)
{
    mAggregators.emplace_back(name);
    return mAggregators.back();
}

DataCollectorMgr::Aggregation &DataCollectorMgr::getAggregation(const std::string &name)
{
    for (auto &aggregation : mAggregators)
    {
        if (aggregation.mName == name)
        { return aggregation; }
    }

    throw AggregationNotFound("Aggregation with provided name does not exist!");
}

std::string DataCollectorMgr::generateOutputString()
{
    std::stringstream ss;

    for (const auto &aggregator : mAggregators)
    {
        ProfilingAggregator dataAggregator;
        dataAggregator.setAutoIndexing(aggregator.mAutoIndexing);

        for (const auto &collector : aggregator.mCollectors)
        { collector->aggregateData(dataAggregator); }

        ss << aggregator.mName << ": \n";
        ss << dataAggregator.generateTable(
            aggregator.mVerticalTable ?
            ProfilingAggregator::TableOrientation::Vertical :
            ProfilingAggregator::TableOrientation::Horizonal);

        ss << "\n";
    }

    return ss.str();
}

}

}
