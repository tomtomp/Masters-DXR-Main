/**
 * @file application/Application.cpp
 * @author Tomas Polasek
 * @brief Base application class, extensible by the user.
 */

#include "stdafx.h"

#include "engine/application/Application.h"

#include "Engine.h"

namespace quark
{

namespace app
{

namespace impl
{

res::win::Window &engineWindow(Engine &engine)
{ return engine.window(); }

rndr::RenderSubSystem &engineRenderer(Engine &engine)
{ return engine.renderer(); }

scene::SceneSubSystem &engineScene(Engine &engine)
{ return engine.scene(); }

app::MessageHandler &engineMsgHandler(Engine &engine)
{ return engine.msgHandler(); }

gui::GuiSubSystem &engineGui(Engine &engine)
{ return engine.gui(); }

}

}

}
