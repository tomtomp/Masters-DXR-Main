/**
 * @file application/ProfilingCollector.cpp
 * @author Tomas Polasek
 * @brief Helper class used for collection of profiling data.
 */

#include "stdafx.h"

#include "engine/application/ProfilingCollector.h"

namespace quark
{

namespace app
{

/// Helper class used for collecting the data node pointers from profiler.
class CollectorCrawler : public util::prof::CallStackCrawler
{
public:
    /// Pass list of data streams to fill.
    CollectorCrawler(std::vector<ProfilingCollector::DataStream> &dataStreams);

    /// Main entry point.
    virtual void crawl(util::prof::ThreadNode *root) override;
private:
    class NodeCrawler
    {
    public:
        NodeCrawler(const std::map<std::string, ProfilingCollector::DataStream*> &streamMap);
        void operator()(util::prof::CallNode *node) const;
    private:
        const std::map<std::string, ProfilingCollector::DataStream*> &mStreamMap;
    protected:
    }; // class NodeCrawler

    /// List of data streams to fill.
    std::vector<ProfilingCollector::DataStream> &mDataStreams;
protected:
}; // class PrintCrawler

ProfilingCollector::ProfilingCollector()
{ /* Automatic */ }

ProfilingCollector::ProfilingCollector(bool noDuplication, std::initializer_list<const char*> streams) : 
    mNoDuplication{ noDuplication }
{
    mStreamsInitialized = false;
    for (const auto &stream : streams)
    { addStream(stream); }
}

ProfilingCollector::~ProfilingCollector()
{ /* Automatic */ }

void ProfilingCollector::clear()
{
    mDataSize = 0u;
    for (auto &dataStream : mDataStreams)
    { dataStream.data.clear(); }
}

void ProfilingCollector::clearStreams()
{ 
    mDataSize = 0u;
    mStreamsInitialized = false;
    mDataStreams.clear(); 
}

void ProfilingCollector::syncLength(std::size_t dataLength)
{
    if (mDataSize >= dataLength)
    { return; }

    for (auto &stream : mDataStreams)
    {
        auto &data{ stream.data };
        if (data.size() >= dataLength)
        { // No action necessary.
            return;
        }

        // Else we need to align it: copy the last data.
        const auto pointCopy{ data.empty() ? DataPoint{ } : data.back() };
        const auto toCreate{ dataLength - data.size() };
        for (std::size_t iii = 0u; iii < toCreate; ++iii)
        { data.push_back(pointCopy); }
    }

    mDataSize = std::max(mDataSize, dataLength);
}

ProfilingCollector &ProfilingCollector::addStream(const char *stream, 
    std::size_t numberOfNodes, bool useLastNode)
{ 
    mStreamsInitialized = false;
    mDataStreams.emplace_back(DataStream{ stream, numberOfNodes, useLastNode }); 
    return *this;
}

ProfilingCollector &ProfilingCollector::setNoDuplication(bool enable)
{ mNoDuplication = enable; return *this; }

void ProfilingCollector::initialize()
{
    CollectorCrawler cc(mDataStreams);
    PROF_DUMP(cc);

    /*
    const auto maxSize{ std::accumulate(mDataStreams.begin(), mDataStreams.end(), std::size_t{ 0u },
        [](std::size_t size, const DataStream &stream) 
        { return std::max(size, stream.data.size()); }) };
        */
    const auto maxSize{ mDataSize };

    // Presume we are getting all of them.
    mStreamsInitialized = true;

    for (auto &dataStream : mDataStreams)
    { // The new streams should start aligned.
        if (dataStream.nodes.size() != dataStream.targetNodeCount)
        { 
            // Missing at least this stream.
            mStreamsInitialized = false;
            continue; 
        }
        if (dataStream.data.size() < maxSize)
        { dataStream.data.resize(maxSize); }
    }
}

std::size_t ProfilingCollector::collectData()
{
    if (!mStreamsInitialized)
    { initialize(); }

    /// Do we have new data to add?
    bool newPointsAvailable{ false };

    for (auto &dataStream : mDataStreams)
    { // Check if we have new data.
        // Skip non-initialized streams.
        if (dataStream.nodes.empty())
        { continue; }

        if (dataStream.useLastNode)
        {
            dataStream.chosenNode = dataStream.nodes.back();

            const auto &data{ dataStream.nodes.back()->data() };
            const auto samples{ data.getActualNumSamples() };
            if (samples <= dataStream.samples && mNoDuplication)
            { continue; }

            newPointsAvailable = true;
        }
        else
        {
            for (const auto &node : dataStream.nodes)
            {
                const auto &data{ dataStream.nodes.back()->data() };
                const auto samples{ data.getActualNumSamples() };
                newPointsAvailable |= samples > dataStream.samples || !mNoDuplication;

                if (!dataStream.chosenNode || samples > dataStream.chosenNode->data().getActualNumSamples())
                { dataStream.chosenNode = node; }
            }
        }
    }

    if (!newPointsAvailable && mNoDuplication)
    { // No actions necessary.
        return false;
    }

    for (auto &dataStream : mDataStreams)
    { // Update all of the streams.
        // Skip non-initialized streams.
        if (dataStream.nodes.empty())
        { continue; }

        ASSERT_SLOW(dataStream.chosenNode);

        const auto &data{ dataStream.chosenNode->data() };
        const auto samples{ data.getActualNumSamples() };

        // We add duplicated data, since there are new values in other streams.

        const auto total{ data.getActualMegacycles() };
        const auto avg{ data.getActualAvg() };
        const auto last{ data.getLastMegacycles() };

        dataStream.samples = samples;
        dataStream.data.emplace_back(DataPoint{ total, avg, last });
    }

    mDataSize++;

    return 1u;
}

void ProfilingCollector::aggregateData(ProfilingAggregator &aggregator)
{
    for (const auto &dataStream : mDataStreams)
    { // Add all data series.
        if (dataStream.nodes.empty())
        { continue; }

        std::vector<float> data{ };
        data.reserve(dataStream.data.size());
        for (const auto &dataPoint : dataStream.data)
        { data.emplace_back(static_cast<float>(dataPoint.last)); }

        aggregator.addDataSeries<float>().setName(dataStream.name).setData(std::move(data));
    }
}

std::size_t ProfilingCollector::size() const
{ return mDataSize; }

CollectorCrawler::CollectorCrawler(std::vector<ProfilingCollector::DataStream> &dataStreams) : 
    mDataStreams{ dataStreams }
{ }

void CollectorCrawler::crawl(util::prof::ThreadNode *root)
{
    // Create a helper map for fast lookup.
    std::map<std::string, ProfilingCollector::DataStream*> streamMap{ };
    for (auto &dataStream : mDataStreams)
    { streamMap[dataStream.name] = &dataStream; }

    NodeCrawler crawler(streamMap);

    root->foreachnn([&] (util::prof::ThreadNode *thread) 
    { crawler(thread->data().getRoot()); });
}

CollectorCrawler::NodeCrawler::NodeCrawler(const std::map<std::string, ProfilingCollector::DataStream*> &streamMap) : 
    mStreamMap{ streamMap }
{ }

void CollectorCrawler::NodeCrawler::operator()(util::prof::CallNode *node) const
{
    const auto findIt{ mStreamMap.find(node->name()) };
    if (findIt != mStreamMap.end() && 
        findIt->second->nodes.size() != findIt->second->targetNodeCount)
    { 
        auto &nodes{ findIt->second->nodes };
        if (std::find(nodes.begin(), nodes.end(), node) == nodes.end())
        { nodes.push_back(node); }
    }

    node->foreachnn(*this);
}

}

}
