/**
 * @file application/ManualCollector.cpp
 * @author Tomas Polasek
 * @brief Helper class used for collection of any user data.
 */

#include "stdafx.h"

#include "engine/application/ManualCollector.h"

namespace quark
{

namespace app
{

ManualCollector::DataStreamBase::~DataStreamBase()
{ /* Automatic */ }

ManualCollector::ManualCollector()
{ /* Automatic */ }

ManualCollector::~ManualCollector()
{ /* Automatic */ }

void ManualCollector::clear()
{
    mDataSize = 0u;
    for (const auto &stream : mDataStreams)
    { stream->clear(); }
}

void ManualCollector::syncLength(std::size_t dataLength)
{
    if (mDataSize >= dataLength)
    { return; }

    for (const auto &stream : mDataStreams)
    { stream->syncLength(dataLength, mCollectInSync); }

    mDataSize = std::max(mDataSize, dataLength);
}

std::size_t ManualCollector::collectData()
{
    if (mNoCollection)
    { return 0u; }

    for (const auto &stream : mDataStreams)
    { stream->collectData(); }
    mDataSize++;

    return 1u;
}

void ManualCollector::aggregateData(ProfilingAggregator &aggregator)
{
    for (const auto &stream : mDataStreams)
    { stream->aggregateData(aggregator); }
}

std::size_t ManualCollector::size() const
{ return mDataSize; }

void ManualCollector::setCollectInSync(bool enable)
{ mCollectInSync = enable; }

void ManualCollector::setNoCollection(bool enable)
{ mNoCollection = enable; }

}

}
