/**
 * @file application/ProfilingAggregator.cpp
 * @author Tomas Polasek
 * @brief Helper class used for taking multiple data 
 * series and compiling a table from them.
 */

#include "stdafx.h"

#include "engine/application/ProfilingAggregator.h"

namespace quark
{

namespace app
{

ProfilingAggregator::DataSeriesBase::~DataSeriesBase()
{ /* Automatic */ }

ProfilingAggregator::ProfilingAggregator()
{ /* Automatic */ }

ProfilingAggregator::~ProfilingAggregator()
{ /* Automatic */ }

void ProfilingAggregator::setTableSeparator(const std::string &sep)
{ mSeparator = sep; }

void ProfilingAggregator::setAutoIndexing(bool enable)
{ mAutoIndexing = enable; }

std::string ProfilingAggregator::generateTable(TableOrientation orientation)
{
    if (orientation == TableOrientation::Vertical)
    { return generateVerticalTable(); }
    else
    { return generateHorizontalTable(); }
}

void ProfilingAggregator::saveToFile(const res::File &trackFile, TableOrientation orientation)
{
    const auto data{ generateTable(orientation) };
    std::ofstream out(trackFile.file());
    out << data;
}

std::string ProfilingAggregator::generateHorizontalTable()
{
    if (mDataSeriesList.empty())
    { return ""; }

    const auto maxSize{ std::accumulate(mDataSeriesList.begin(), mDataSeriesList.end(), std::size_t{ 0u },
        [](std::size_t size, const auto &series) 
        { return std::max(size, series->size()); }) };

    if (maxSize == 0u)
    { return ""; }

    std::stringstream out;

    for (const auto &dataSeries : mDataSeriesList)
    { // Print the headers.
        out << dataSeries->name() << mSeparator;

        for (std::size_t iii = 0u; iii < maxSize; ++iii)
        { // Print all data points.
            dataSeries->pushDataPoint(out, iii);
            if (iii < maxSize - 1u)
            { out << mSeparator; }
        }

        out << "\n";
    }

    if (mAutoIndexing)
    { // Print automatic indices.
        out << DEFAULT_INDEX_SERIES_NAME << mSeparator;

        for (std::size_t iii = 0u; iii < maxSize; ++iii)
        { // Print all data points.
            out << iii;
            if (iii < maxSize - 1u)
            { out << mSeparator; }
        }

        out << "\n";
    }

    return out.str();
}

std::string ProfilingAggregator::generateVerticalTable()
{
    if (mDataSeriesList.empty())
    { return ""; }

    const auto maxSize{ std::accumulate(mDataSeriesList.begin(), mDataSeriesList.end(), std::size_t{ 0u },
        [](std::size_t size, const auto &series) 
        { return std::max(size, series->size()); }) };

    if (maxSize == 0u)
    { return ""; }

    std::stringstream out;

    for (std::size_t iii = 0u; iii < mDataSeriesList.size(); ++iii)
    { // Print the headers.
        out << mDataSeriesList[iii]->name() << mSeparator;
        if (iii < mDataSeriesList.size() - 1u)
        { out << mSeparator; }
    }

    if (mAutoIndexing)
    { out << mSeparator << DEFAULT_INDEX_SERIES_NAME; }

    out << "\n";

    for (std::size_t iii = 0u; iii < maxSize; ++iii)
    { // Print all data points.
        for (std::size_t jjj = 0u; jjj < mDataSeriesList.size(); ++jjj)
        { 
            mDataSeriesList[jjj]->pushDataPoint(out, jjj);
            if (jjj < mDataSeriesList.size() - 1u)
            { out << mSeparator; }
        }

        if (mAutoIndexing)
        { out << mSeparator << iii; }

        out << "\n";
    }

    return out.str();
}

}

}
