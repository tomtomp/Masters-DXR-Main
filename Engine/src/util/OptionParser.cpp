/**
 * @file util/OptionParser.cpp
 * @author Tomas Polasek
 * @brief Command line options parsing.
 */

#include "stdafx.h"

#include "engine/util/OptionParser.h"

namespace util
{

void OptionParser::addOption(const std::wstring &opt, FlagCbFunT cb, const std::wstring &description)
{
    if (hasRule(opt))
    { // Rule already registered.
        throw util::wruntime_error(std::wstring(L"Option with identifier \"") + 
            opt + L"\" is already registered!");
    }

    setRule(opt, std::make_shared<FlagParser>(opt, description, std::move(cb)));
}

void OptionParser::parse(const wchar_t * const * const argv, int argc)
{ parse(std::vector<std::wstring>(argv, argv + argc)); }

void OptionParser::parse(const std::vector<std::wstring> &args)
{
    if (args.size() <= 1u)
    { // Empty, or only name of application.
        return;
    }

    // Save application name.
    mAppName = args[0];

    // For each argument in the argument vector.
    for (auto it = ++args.begin(); it != args.end(); )
    { // Start at the second argument, first is usually name of application.
        // Attempt to find a rule for current option.
        const auto searchIt{ mRules.find(*it) };

        if (searchIt == mRules.end())
        { // No rules have been found.
            throw ParsingException(unknownOptionMsg(*it) + generateHelp());
        }
        else
        { // Rule has been found.
            // Remember position.
            const auto itBck{ it };

            try
            { // Attempt parsing.
                searchIt->second->parse(it, args.end());
            }
            catch (const ParsingException &err)
            { // Throw the error to user.
                throw ParsingException(errorParsingMsg(searchIt->first, err) + generateHelp());
            }

            if (it == itBck)
            { // Parser didn't move the iterator?
                // Move it, just to be sure we avoid infinite cycle.
                ++it;
                log<Warning>() << "Parser didn't move the iterator!" << std::endl;
            }
        }
    }
}

std::wstring OptionParser::generateHelp() const
{
    std::wstringstream options;
    std::wstringstream optionDescriptions;

    for (auto &kv : mRules)
    {
        options << (kv.second->valueDesc().empty() ?
            kv.second->option() :
            kv.second->valueDesc()) <<
            L" ";

        optionDescriptions << kv.second->generateHelp() << L"\n";
    }

    std::wstringstream helpMsg;

    helpMsg << L"\nAPPLICATION HELP: \n" << mAppName << L" " << options.str() 
            << L"\n\nDESCRIPTION: \n" << optionDescriptions.str() << L"\n";

    return helpMsg.str();
}

std::wstring OptionParser::BaseParser::generateHelp()
{
    return (mValueDesc.empty() ? mOpt : mValueDesc) + L":\n\t" + mDescription;
}

OptionParser::FlagParser::FlagParser(const std::wstring &opt, const std::wstring &description, FlagCbFunT &&cb):
    BaseParser(opt, description),
    mCb{cb}
{
    // Since this is a flag, only the option should be displayed.
    mValueDesc = option();
}

void OptionParser::FlagParser::parse(ArgIter &argIter, const ArgIter &end)
{
    // Flag is always a single parameter.
	UNUSED(end);

    mCb();
    ++argIter;
}

bool OptionParser::hasRule(const std::wstring &opt)
{ return mRules.find(opt) != mRules.end(); }

void OptionParser::setRule(const std::wstring& opt, std::shared_ptr<BaseParser>&& ptr)
{ mRules[opt] = std::forward<std::shared_ptr<BaseParser>>(ptr); }

std::wstring OptionParser::unknownOptionMsg(const std::wstring &opt)
{ return std::wstring(L"Unknown option : ") + opt + L"\n"; }

std::wstring OptionParser::errorParsingMsg(const std::wstring &opt, const util::wruntime_error &err)
{ return std::wstring(L"Parsing error for option : \"") + opt + L"\" : " + err.wwhat() + L"\n"; }

template <>
std::wstring OptionParser::Parser<std::wstring>::parseValue(ArgIter &argIter, const ArgIter &end)
{
    if (++argIter != end)
    { return *(argIter++); }
    else
    { throw ParsingException(L"String value not found!"); }
}

template <>
void OptionParser::Parser<std::wstring>::initValueDesc()
{ mValueDesc = option() + L" STR_VALUE"; }

template <>
uint32_t OptionParser::Parser<uint32_t>::parseValue(ArgIter &argIter, const ArgIter &end)
{
    if (++argIter != end)
    {
        try
        { return std::stoul(*(argIter++)); }
        catch (const std::invalid_argument&)
        { throw ParsingException(L"Unable to convert value to integer!"); }
        catch (const std::out_of_range&)
        { throw ParsingException(L"Value is out of range!"); }
    }
    else
    { throw ParsingException(L"Integer value not found!"); }
}

template <>
void OptionParser::Parser<uint32_t>::initValueDesc()
{ mValueDesc = option() + L" UINT_VALUE"; }

template <>
float OptionParser::Parser<float>::parseValue(ArgIter &argIter, const ArgIter &end)
{
    if (++argIter != end)
    {
        try
        { return std::stof(*(argIter++)); }
        catch (const std::invalid_argument&)
        { throw ParsingException(L"Unable to convert value to float!"); }
        catch (const std::out_of_range&)
        { throw ParsingException(L"Value is out of range!"); }
    }
    else
    { throw ParsingException(L"Integer value not found!"); }
}

template <>
void OptionParser::Parser<float>::initValueDesc()
{ mValueDesc = option() + L" FLOAT_VALUE"; }

template <>
bool OptionParser::Parser<bool>::parseValue(ArgIter &argIter, const ArgIter &end)
{
    if (++argIter != end)
    {
        // Get the argument.
        const auto &strValue{ *argIter };
        // Skip over the argument value.
        ++argIter;

        if (strValue == L"true")
        { return true; }
        else if (strValue == L"false")
        { return false; }
        else
        { throw ParsingException(L"Unknown boolean value!"); }
    }
    else
    { throw ParsingException(L"Boolean value not found!"); }
}

template <>
void OptionParser::Parser<bool>::initValueDesc()
{ mValueDesc = option() + L" true|false"; }

}

