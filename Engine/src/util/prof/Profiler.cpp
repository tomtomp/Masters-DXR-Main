/**
 * @file util/prof/Profiler.cpp
 * @author Tomas Polasek
 * @brief Contains profiling code.
 */

#include "stdafx.h"

#include "engine/util/prof/Profiler.h"

namespace util
{

namespace prof
{

/// Global profiler instance, shared between all threads.
std::unique_ptr<ProfilingManager> gProfInstance;

#ifdef PROF_AUTOMATIC_INITIALIZATION_ENABLED

/**
 * Helper class used for automatic initialization of the 
 * profiling manager.
 */
struct ProfilingManagerInitializer
{
    ProfilingManagerInitializer()
    { ProfilingManager::initialize(); }
}; // struct ProfilingManagerInitializer

/// Instantiate to initialize profiling manager...
ProfilingManagerInitializer gProfilingManagerInitializerDummy;

#endif // PROF_AUTOMATIC_INITIALIZATION_ENABLED

// Static definitions: 
uint64_t ProfilingManager::sConstructions{ 0 };
thread_local ThreadStatus ProfilingManager::mThreadStatus;

void ProfilingManager::initialize()
{ gProfInstance = std::make_unique<ProfilingManager>(); }

ProfilingManager &ProfilingManager::instance()
{
    ASSERT_FAST(gProfInstance);
    return *gProfInstance;
}

ForeachPrint::ForeachPrint(bool skipInactive, uint32_t indent, double parentMegacycles) :
    mIndentLevel(indent),
    mParentMegacycles(parentMegacycles),
    mSkipInactive(skipInactive)
{
}

void ForeachPrint::operator()(CallNode *node, bool rootLevel) const
{
    // TODO - Find a better way?
    static constexpr const char *PADDING{ "................................................................................." };

    auto &data{ node->data() };

    // Skipping inactive scopes? Never skip root node!
    if (mSkipInactive && !rootLevel && !data.lastRun())
    { return; }

    // Get time spent in the last iteration.
    const auto lastTicks{ data.getLastMegacyclesAndReset() };

    // Value including all children.
    const auto including{ data.getActualMegacycles() };

    // Calculate sum of children megacycles.
    ForeachSumMegacycles adder;
    node->foreachnn(adder);
    const auto inner{ adder.sum() };

    // Resolve the actual time, excluding children.
    const auto self{ including - inner };
    const auto selfPercentage{ including ?
        (self / including) * 100.0 :
        (100.0) };

    // Calculate average spent through all iterations.
    const auto avg{ data.getActualAvg() };
    const auto parentPercentage{ mParentMegacycles ?
        (including / mParentMegacycles) * 100.0 :
        (100.0) };

    const auto strNodeData = ::util::format("%12.3f Mt %9.3f %% %9.0llu x %12.3f Mt %12.3f Mt %12.3f Mt %9.3f %% \t %*.*s%s%s\n",
        including, parentPercentage, data.getActualNumSamples(), avg, lastTicks, self,
        selfPercentage, mIndentLevel, mIndentLevel, PADDING, rootLevel ? "" : "/", node->name());
    lognp<Prof>() << "\t\t" << strNodeData << std::endl;

    ForeachPrint printer(mSkipInactive, mIndentLevel + 1, including);
    node->foreachnn(printer);
}

void ForeachSumMegacycles::operator()(CallNode *node)
{
    mSum += node->data().getActualMegacycles();
    mNumSamples += node->data().getActualNumSamples();
}

void ProfilingManager::reset()
{
    delete mRoot;
    mRoot = new ThreadNode("/", nullptr);

    mNumScopeEnter = 0;
    mNumScopeExit = 0;
    mNumThreadEnter = 0;
    mNumThreadExit = 0;

    mThreadStatus.mCurNode = nullptr;
    mThreadStatus.mRoot = nullptr;
    mThreadStatus.mThreadNode = nullptr;
}

void SimulatedProfilingContext::LateScopeSimulator::simulateScope(uint64_t start, uint64_t stop)
{
    ASSERT_SLOW(mThread && mScope);
#ifdef PROF_LOCK_SCOPE
    std::lock_guard l(mThread->data().getLock());
#endif
    mScope->data().simulatedRuntime(start, stop);
}

std::size_t SimulatedProfilingContext::LateScopeSimulator::scopeHash() const 
{ return std::hash<void*>()(mScope); }

SimulatedProfilingContext::LateScopeSimulator::LateScopeSimulator(ThreadNode *thread, CallNode *scope) :
    mThread{ thread }, mScope{ scope }
{ }

SimulatedProfilingContext::~SimulatedProfilingContext()
{
    if (initialized())
    { ProfilingManager::instance().exitSimulatedThread(this); }
}

void SimulatedProfilingContext::enterScope(const char *name)
{
    ASSERT_SLOW(initialized());
    ProfilingManager::instance().enterSimulatedScope(this, name);
}

void SimulatedProfilingContext::simulateScopeRuntime(uint64_t start, uint64_t stop)
{
    ASSERT_SLOW(initialized());
#ifdef PROF_LOCK_SCOPE
    std::lock_guard l(mSimulatedThread->data().getLock());
#endif
    ASSERT_SLOW(mSimulatedScope);
    mSimulatedScope->data().simulatedRuntime(start, stop);
}

SimulatedProfilingContext::LateScopeSimulator SimulatedProfilingContext::lateSimulatorForCurrentScope()
{
    ASSERT_SLOW(initialized());
    return LateScopeSimulator(mSimulatedThread, mSimulatedScope);
}

bool SimulatedProfilingContext::exitScope()
{
    ASSERT_SLOW(initialized());
    return ProfilingManager::instance().exitSimulatedScope(this);
}

void SimulatedProfilingContext::simulateThreadRuntime(uint64_t start, uint64_t stop)
{
    ASSERT_SLOW(initialized());
#ifdef PROF_LOCK_SCOPE
    std::lock_guard l(mSimulatedThread->data().getLock());
#endif
    ASSERT_SLOW(mSimulatedThreadRoot);

	if (start <= stop)
	{ mSimulatedThreadRoot->data().simulatedRuntime(start, stop); }
}

std::size_t SimulatedProfilingContext::currentScopeHash()
{ return std::hash<void*>()(mSimulatedScope); }

SimulatedProfilingContext::SimulatedProfilingContext(ThreadNode *simulatedThread, CallNode *rootNode) :
    mSimulatedThread{ simulatedThread }, mSimulatedThreadRoot{ rootNode }, mSimulatedScope{ rootNode }
{ }

bool SimulatedProfilingContext::initialized() const
{ return mSimulatedThread && mSimulatedThreadRoot && mSimulatedScope; }

ProfilingManager::ProfilingManager() :
    mRoot(new ThreadNode("/", nullptr))
{
    // Only one construction should happen.
    ASSERT_FAST(sConstructions == 0);
    sConstructions++;

    //mRoot->data().timer().start();

    // Measure amount of time per enterScope->exitScope.
    enterThread("main");

    double minOverhead{ std::numeric_limits<float>::max() };

    for (uint64_t jjj = 0; jjj < 100; ++jjj)
    {
        prof::Timer timer;

        for (uint64_t iii = 0; iii < 1000; ++iii)
        {
            timer.start();

            enterScope("Testing");
            exitScope();

            timer.stop();
        }

        const auto newAvg{ timer.avgTicks() };

        if (newAvg < minOverhead)
            minOverhead = newAvg;
    }

    mScopeOverhead = minOverhead;

    exitThread();

    reset();

    // Create the main thread node.
    enterThread("main");
}

ProfilingManager::~ProfilingManager()
{ delete mRoot; }

inline CallNode *ProfilingManager::enterScope(const char *name)
{
    CallNode *retValue{ nullptr };

    {
#ifdef PROF_LOCK_SCOPE
        std::lock_guard l(mThreadStatus.mThreadNode->data().getLock());
#endif

        ASSERT_SLOW(mThreadStatus.mCurNode);
        mThreadStatus.mCurNode = mThreadStatus.mCurNode->getCreateChild(name);
        ASSERT_SLOW(mThreadStatus.mCurNode);
        retValue = mThreadStatus.mCurNode;

        mNumScopeEnter++;

        mThreadStatus.mCurNode->data().startTimer();
    }

    return retValue;
}

inline CallNode *ProfilingManager::exitScope()
{
    CallNode *retValue{ nullptr };

    {
#ifdef PROF_LOCK_SCOPE
        std::lock_guard l(mThreadStatus.mThreadNode->data().getLock());
#endif

        ASSERT_SLOW(mThreadStatus.mCurNode);
        mThreadStatus.mCurNode->data().stopTimer();
        mThreadStatus.mCurNode = mThreadStatus.mCurNode->parent();
        ASSERT_SLOW(mThreadStatus.mCurNode);
        retValue = mThreadStatus.mCurNode;

        mNumScopeExit++;
    }

    return retValue;
}

inline ThreadNode *ProfilingManager::enterThread(const char* name)
{
    ThreadNode *threadNode{ nullptr };

    {
        std::lock_guard l(mGlobalLock);
        threadNode = mRoot->getCreateChild(name);

        std::lock_guard lD(threadNode->data().getLock());

        const auto tryOccupy{ threadNode->data().occupy() };
        // Test for attempting to enter the same thread at different places.
        ASSERT_FATAL(tryOccupy);

        mThreadStatus.mThreadNode = threadNode;
        mThreadStatus.mCurNode = threadNode->data().getRoot();
        mThreadStatus.mRoot = mThreadStatus.mCurNode;

        mThreadStatus.mRoot->data().startTimer();

        mNumThreadEnter++;
    }

    return threadNode;
}

void ProfilingManager::exitThread()
{
    ThreadData &thData = mThreadStatus.mThreadNode->data();

    {
        std::lock_guard l(mGlobalLock);
        std::lock_guard lD(thData.getLock());

        mThreadStatus.mRoot->data().stopTimer();

        mNumThreadExit++;

        thData.freeOccupation();

        mThreadStatus.mThreadNode = nullptr;
        mThreadStatus.mCurNode = nullptr;
        mThreadStatus.mRoot = nullptr;
    }
}

SimulatedProfilingContext ProfilingManager::enterSimulatedThread(const char *name)
{
    ThreadNode *threadNode{ nullptr };
    CallNode *rootNode{ nullptr };

    {
        std::lock_guard l(mGlobalLock);
        threadNode = mRoot->getCreateChild(name);

        std::lock_guard lD(threadNode->data().getLock());

        const auto tryOccupy{ threadNode->data().occupy() };
        // Test for attempting to enter the same thread at different places.
        ASSERT_FATAL(tryOccupy);

        rootNode = threadNode->data().getRoot();
        mNumThreadEnter++;
    }

    return SimulatedProfilingContext(threadNode, rootNode);
}

void ProfilingManager::enterSimulatedScope(SimulatedProfilingContext *ctx, const char *scopeName)
{
#ifdef PROF_LOCK_SCOPE
    std::lock_guard l(mThreadStatus.mThreadNode->data().getLock());
#endif

    ASSERT_SLOW(ctx->mSimulatedScope);
    ctx->mSimulatedScope = ctx->mSimulatedScope->getCreateChild(scopeName);
    ASSERT_SLOW(ctx->mSimulatedScope);

    mNumScopeEnter++;
}

bool ProfilingManager::exitSimulatedScope(SimulatedProfilingContext *ctx)
{
#ifdef PROF_LOCK_SCOPE
    std::lock_guard l(mThreadStatus.mThreadNode->data().getLock());
#endif

    if (ctx->mSimulatedScope == ctx->mSimulatedThreadRoot)
    { return false; }

    ASSERT_SLOW(ctx->mSimulatedScope);
    ctx->mSimulatedScope = ctx->mSimulatedScope->parent();
    ASSERT_SLOW(ctx->mSimulatedScope);

    mNumScopeExit++;

    return true;
}

void ProfilingManager::exitSimulatedThread(SimulatedProfilingContext *ctx)
{
    std::lock_guard l(mGlobalLock);
    std::lock_guard lD(ctx->mSimulatedThread->data().getLock());

    mNumThreadExit++;

    ctx->mSimulatedThread->data().freeOccupation();

    ctx->mSimulatedThread = nullptr;
    ctx->mSimulatedScope = nullptr;
    ctx->mSimulatedThreadRoot = nullptr;
}

inline CallNode *ProfilingManager::getCurrentScope()
{
    ASSERT_SLOW(mThreadStatus.mCurNode);
    return mThreadStatus.mCurNode;
}

void ProfilingManager::useCrawler(CallStackCrawler &crawler)
{
    std::lock_guard l(mGlobalLock);
    crawler.crawl(mRoot);
}

ScopeProfiler::ScopeProfiler(const char *desc)
{ ProfilingManager::instance().enterScope(desc); }

ScopeProfiler::~ScopeProfiler()
{ ProfilingManager::instance().exitScope(); }

BlockProfiler::BlockProfiler(const char *desc)
{ ProfilingManager::instance().enterScope(desc); }

void BlockProfiler::end()
{
    if (!mEnded)
    {
        ProfilingManager::instance().exitScope();
        mEnded = true;
    }
}

void BlockProfiler::endCurrent()
{ ProfilingManager::instance().exitScope(); }

ThreadProfiler::ThreadProfiler(const char *desc)
{ ProfilingManager::instance().enterThread(desc); }

ThreadProfiler::~ThreadProfiler()
{ ProfilingManager::instance().exitThread(); }

SimulatedProfilingContext enterSimulatedThread(const char *name)
{ return ProfilingManager::instance().enterSimulatedThread(name); }

}

}
