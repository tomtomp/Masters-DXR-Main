/**
 * @file util/Util.cpp
 * @author Tomas Polasek
 * @brief Utilities and helper functions/classes.
 */

#include "stdafx.h"

#include "engine/util/Util.h"

namespace util
{

std::string format(const char* fmt, ...)
{
    va_list ap;
    va_start(ap, fmt);

    auto buf = vFormat(fmt, ap);

    va_end(ap);

    return buf;
}

std::string vFormat(const char* fmt, va_list ap)
{
    // Calculate size of the resulting string.
    const auto resultSize{ ::vsnprintf(nullptr, 0u, fmt, ap) };
    // Allocate space for the string.
    std::vector<char> result(resultSize);
    // Print formatted string into the prepared result string.
    const auto written{ ::vsnprintf(result.data(), resultSize, fmt, ap) };

    ASSERT_FAST(written == resultSize);

    return { result.data() };
}

std::string getErrorAsString(DWORD error)
{
    if (error == 0)
    { // No error -> empty message
        return { };
    }

    // Create the message.
    LPSTR msgBuffer{ nullptr };
    const auto msgSize{ FormatMessageA(
        FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM | FORMAT_MESSAGE_IGNORE_INSERTS,
        nullptr, error, MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), reinterpret_cast<LPSTR>(&msgBuffer), 0, nullptr) };

    std::string message(msgBuffer, msgSize);

    // Free the buffer.
    LocalFree(msgBuffer);

    return message;
}

std::wstring getErrorAsWString(DWORD error)
{
    if (error == 0)
    { // No error -> empty message
        return { };
    }

    // Create the message.
    LPWSTR msgBuffer{ nullptr };
    const auto msgSize{ FormatMessageW(
        FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM | FORMAT_MESSAGE_IGNORE_INSERTS,
        nullptr, error, MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), reinterpret_cast<LPWSTR>(&msgBuffer), 0, nullptr) };

    std::wstring message(msgBuffer, msgSize);

    // Free the buffer.
    LocalFree(msgBuffer);

    return message;
}

std::string toString(const std::wstring& original)
{
    if (original.empty())
    { return ""; }

    // Found on https://stackoverflow.com/a/18374698 .
    using convert_type = std::codecvt_utf8<wchar_t>;
    std::wstring_convert<convert_type, wchar_t> converter;
    return converter.to_bytes(original);
}

std::wstring toWString(const std::string& original)
{
    if (original.empty())
    { return L""; }

    // Found on https://stackoverflow.com/a/18374698 .
    using convert_type = std::codecvt_utf8<wchar_t>;
    std::wstring_convert<convert_type, wchar_t> converter;
    return converter.from_bytes(original);
}

std::string getLastErrorAsString()
{
    const auto msg{ getErrorAsString(::GetLastError()) };
    return msg.empty() ? "No last error!" : msg;
}

std::wstring getLastErrorAsWString()
{
    const auto msg{ getErrorAsWString(::GetLastError()) };
    return msg.empty() ? L"No last error!" : msg;
}

std::string hrToString(::HRESULT hr)
{
    const auto msg{ getErrorAsString(hr) };
    return msg.empty() ? (std::string("Error code ") + std::to_string(hr)) : msg;
}

std::wstring hrToWString(::HRESULT hr)
{
    const auto msg{ getErrorAsWString(hr) };
    return msg.empty() ? (std::wstring(L"Error code ") + std::to_wstring(hr)) : msg;
}

void removeTrailingChar(std::string &target, char c)
{
    auto findPos{ std::find_if(target.rbegin(), target.rend(), [c](const auto val) { return val != c; }) };
    target.erase(findPos.base(), target.end());
}

std::size_t longestCommonPrefix(const std::string &first, const std::string &second)
{
    if (first.length() <= second.length())
    { return std::distance(first.begin(), std::mismatch(first.begin(), first.end(), second.begin()).first); }
    else
    { return std::distance(second.begin(), std::mismatch(second.begin(), second.end(), first.begin()).first); }
}

}
