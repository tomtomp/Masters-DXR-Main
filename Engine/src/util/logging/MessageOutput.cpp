/**
 * @file util/logging/MessageOutput.cpp
 * @author Tomas Polasek
 * @brief Tools for redirecting standard console output to WIN32 messages.
 */

#include "stdafx.h"

#include "engine/util/logging/MessageOutput.h"

namespace util
{

static DebugBufferChooser<std::string> doutBuffer;
static DebugBufferChooser<std::wstring> wdoutBuffer;

std::ostream dout(doutBuffer.ptr());
std::wostream wdout(wdoutBuffer.ptr());

static StringBuffer<syncers::MessageBoxSyncer<std::string>> moutBuffer("Util::mout");
static StringBuffer<syncers::MessageBoxSyncer<std::wstring>> wmoutBuffer(L"Util::wmout");

std::ostream mout(&moutBuffer);
std::wostream wmout(&wmoutBuffer);

}
