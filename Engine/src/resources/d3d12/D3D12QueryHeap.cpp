/**
 * @file resources/d3d12/D3D12QueryHeap.cpp
 * @author Tomas Polasek
 * @brief Wrapper around Direct3D 12 query heap.
 */

#include "stdafx.h"

#include "engine/resources/d3d12/D3D12QueryHeap.h"

namespace quark
{

namespace res
{

namespace d3d12
{

D3D12QueryHeap::D3D12QueryHeap()
{ /* Automatic */ }

D3D12QueryHeap::D3D12QueryHeap(D3D12Device &device, 
    const ::D3D12_QUERY_HEAP_DESC &desc)
{ createQueryHeap(device, desc); }

D3D12QueryHeap::PtrT D3D12QueryHeap::create(D3D12Device & device, const::D3D12_QUERY_HEAP_DESC & desc)
{ return PtrT{ new D3D12QueryHeap(device, desc) }; }

D3D12QueryHeap::~D3D12QueryHeap()
{ /* Automatic */ }

void D3D12QueryHeap::createQueryHeap(D3D12Device &device,
    const ::D3D12_QUERY_HEAP_DESC &desc)
{
    util::throwIfFailed(
        device->CreateQueryHeap(&desc, IID_PPV_ARGS(&mQueryHeap)), 
        "Failed to create the query heap!");
}

}

}

}
