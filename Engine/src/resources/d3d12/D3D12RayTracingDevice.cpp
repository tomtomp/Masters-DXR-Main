/**
 * @file resources/d3d12/D3D12RayTracingDevice.cpp
 * @author Tomas Polasek
 * @brief Direct3D 12 device wrapper, allowing for abstraction 
 * of using fallback ray tracing layer.
 */

#include "stdafx.h"

#include "engine/resources/d3d12/D3D12RayTracingDevice.h"

#include "engine/resources/d3d12/D3D12Resource.h"
#include "engine/resources/d3d12/D3D12DescHeap.h"

namespace quark
{

namespace res
{

namespace d3d12
{

D3D12RayTracingDevice::D3D12RayTracingDevice()
{ /* Automatic */ }

D3D12RayTracingDevice::D3D12RayTracingDevice(D3D12Device &srcDevice)
{
#ifdef ENGINE_RAY_TRACING_ENABLED
    initializeRayTracingDevice(srcDevice);
#else
    copySourceDevice(srcDevice);
#endif
}

#ifdef ENGINE_RAY_TRACING_ENABLED

D3D12RayTracingDevice::D3D12RayTracingDevice(D3D12Device &srcDevice, ::CreateRaytracingFallbackDeviceFlags flags)
{ initializeFallbackRayTracingDevice(srcDevice, flags); }

ComPtr<::ID3D12RaytracingFallbackCommandList> D3D12RayTracingDevice::toFallbackCmdList(
    ::ID3D12GraphicsCommandList *cmdList)
{
    if (!mUsingFallbackDevice)
    { throw util::winexception("Unable to generate fallback command list: Fallback device is not active!"); }

    ComPtr<::ID3D12RaytracingFallbackCommandList> result{ };
    mFallbackDevice->QueryRaytracingCommandList(cmdList, IID_PPV_ARGS(&result));
    return result;
}

D3D12RayTracingCommandList::PtrT D3D12RayTracingDevice::toRayTracingCmdList(D3D12CommandList &cmdList)
{ 
    if (usingFallbackDevice())
    { return D3D12RayTracingCommandList::create(cmdList, toFallbackCmdList(cmdList.get())); }
    else
    { return D3D12RayTracingCommandList::create(cmdList); }
}

bool D3D12RayTracingDevice::usingFallbackDevice() const
{ return mUsingFallbackDevice; }

void D3D12RayTracingDevice::serializeRootSignature(
    const ::CD3DX12_VERSIONED_ROOT_SIGNATURE_DESC &desc, 
    ComPtr<::ID3DBlob> &output)
{
    ComPtr<::ID3DBlob> messages;

    HRESULT result{ };
    if (usingFallbackDevice())
    {
        result = mFallbackDevice->D3D12SerializeVersionedRootSignature(
            &desc, &output, &messages);
    }
    else
    {
        result = ::D3DX12SerializeVersionedRootSignature(
            &desc, desc.Version, &output, &messages);
    }

    if (FAILED(result))
    {
        std::stringstream ss;
        ss << "Failed to serialize versioned root signature!";
        if (messages)
        {
            ss << ": \n" << reinterpret_cast<char*>(messages->GetBufferPointer());
        }
        throw util::winexception(ss.str());
    }
}

void D3D12RayTracingDevice::serializeAndCreateRootSignature(
    const ::CD3DX12_VERSIONED_ROOT_SIGNATURE_DESC &desc, 
    const ::IID &riid, void **targetRs)
{
    ComPtr<::ID3DBlob> rsBlobPtr;
    serializeRootSignature(desc, rsBlobPtr);
    const auto rsBlob{ rsBlobPtr->GetBufferPointer() };
    const auto rsBlobSize{ rsBlobPtr->GetBufferSize() };

    if (usingFallbackDevice())
    {
        util::throwIfFailed(mFallbackDevice->CreateRootSignature(
            0u, 
            rsBlob, rsBlobSize, 
            riid, targetRs), 
            "Failed to create root signature!");
    }
    else
    {
        util::throwIfFailed(get()->CreateRootSignature(
            0u, 
            rsBlob, rsBlobSize, 
            riid, targetRs), 
            "Failed to create root signature!");
    }
}

void D3D12RayTracingDevice::createStateObject(const ::D3D12_STATE_OBJECT_DESC &desc, 
    const ::IID &nRiid, void **nTargetSo, 
    const ::IID &fRiid, void **fTargetSo)
{
    if (usingFallbackDevice())
    {
        util::throwIfFailed(mFallbackDevice->CreateStateObject(
            &desc, fRiid, fTargetSo), 
            "Failed to create state object!");
    }
    else
    {
        util::throwIfFailed(D3D12Device::device5()->CreateStateObject(
            &desc, nRiid, nTargetSo), 
            "Failed to create state object!");
    }
}

std::size_t D3D12RayTracingDevice::getShaderIdentifierSize()
{
    if (usingFallbackDevice())
    { return mFallbackDevice->GetShaderIdentifierSize(); }
    else
    { return D3D12_SHADER_IDENTIFIER_SIZE_IN_BYTES; }
}

::D3D12_RESOURCE_STATES D3D12RayTracingDevice::getASResourceState() const
{
    if (usingFallbackDevice())
    { return mFallbackDevice->GetAccelerationStructureResourceState(); }
    else
    { return ::D3D12_RESOURCE_STATE_RAYTRACING_ACCELERATION_STRUCTURE; }
}

::WRAPPED_GPU_POINTER D3D12RayTracingDevice::wrapResource(
    D3D12DescAllocatorInterface &descHeap, D3D12Resource &resource)
{
    // Index of the descriptor within the heap.
    ::UINT descHeapIndex{ 0u };

    if (usingFallbackDevice() && !mFallbackDevice->UsingRaytracingDriver())
    { // Only allocate when we are using DXR fallback.
        ::D3D12_UNORDERED_ACCESS_VIEW_DESC uavDesc{ };

        // Do not interpret, just buffer containing bytes.
        uavDesc.Format = ::DXGI_FORMAT_R32_TYPELESS;
        uavDesc.ViewDimension = ::D3D12_UAV_DIMENSION_BUFFER;

        // Wrap the whole buffer memory.
        uavDesc.Buffer.FirstElement = 0u;
        // One element is 32b float, we need to divide by 4!
        uavDesc.Buffer.NumElements = static_cast<::UINT>(resource.allocatedSize() / sizeof(uint32_t));
        uavDesc.Buffer.StructureByteStride = 0u;
        uavDesc.Buffer.CounterOffsetInBytes = 0u;
        uavDesc.Buffer.Flags = ::D3D12_BUFFER_UAV_FLAG_RAW;

        // Create the descriptor and get offset from start of the descriptor heap.
        const auto descriptor{ descHeap.createUAV(resource, uavDesc) };
        descHeapIndex = static_cast<::UINT>(descHeap.offsetFromStart(descriptor));

        // Create the unordered view, using the descriptor.
        get()->CreateUnorderedAccessView(resource.get(), nullptr, &uavDesc, descriptor);
    }

    // Finalize the creation, automatically deciding whether to wrap or not to wrap.
    return mFallbackDevice->GetWrappedPointerSimple(descHeapIndex, resource->GetGPUVirtualAddress());
}

#endif

D3D12RayTracingDevice::PtrT D3D12RayTracingDevice::create()
{ return PtrT{ new D3D12RayTracingDevice() }; }

D3D12RayTracingDevice::PtrT D3D12RayTracingDevice::create(D3D12Device &srcDevice)
{ return PtrT{ new D3D12RayTracingDevice(srcDevice) }; }

#ifdef ENGINE_RAY_TRACING_ENABLED

D3D12RayTracingDevice::PtrT D3D12RayTracingDevice::create(D3D12Device &srcDevice, ::CreateRaytracingFallbackDeviceFlags flags)
{ return PtrT{ new D3D12RayTracingDevice(srcDevice, flags) }; }

#endif

D3D12RayTracingDevice::~D3D12RayTracingDevice()
{ /* Automatic */ }

void D3D12RayTracingDevice::copySourceDevice(D3D12Device &srcDevice)
{ D3D12Device::copyDevice(srcDevice); }

#ifdef ENGINE_RAY_TRACING_ENABLED

void D3D12RayTracingDevice::initializeRayTracingDevice(D3D12Device &srcDevice)
{
    copySourceDevice(srcDevice);

    mUsingFallbackDevice = false;
}

void D3D12RayTracingDevice::initializeFallbackRayTracingDevice(D3D12Device &srcDevice,
    ::CreateRaytracingFallbackDeviceFlags flags)
{
    copySourceDevice(srcDevice);

    util::throwIfFailed(
        ::D3D12CreateRaytracingFallbackDevice(
            get(), flags, 0u, IID_PPV_ARGS(&mFallbackDevice)), 
        "Failed to create fallback ray tracing device.");

    mUsingFallbackDevice = true;
}

#endif

}

}

}
