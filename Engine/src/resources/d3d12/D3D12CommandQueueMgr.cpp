/**
 * @file resources/d3d12/D3D12CommandQueueMgr.cpp
 * @author Tomas Polasek
 * @brief Dispenser of command lists, handles command queue and allocator creation.
 */

#include "stdafx.h"

#include "engine/resources/d3d12/D3D12CommandQueueMgr.h"

namespace quark
{
namespace res
{
namespace d3d12
{
D3D12CommandQueueMgr::D3D12CommandQueueMgr(D3D12Device &device, D3D12_COMMAND_LIST_TYPE type,
                                           const wchar_t *dbgName) :
    mDevice{&device},
    mFenceValue{0u},
    mFence{res::d3d12::D3D12Fence::create(device, mFenceValue)},
    mType{type}, mCommandQueue{nullptr}
{
    mCommandQueue = mDevice->createCommandQueue(mType);
    setDebugName(dbgName);
}

D3D12CommandQueueMgr::PtrT D3D12CommandQueueMgr::create(D3D12Device &device, D3D12_COMMAND_LIST_TYPE type,
                                                        const wchar_t *dbgName)
{
    return PtrT{new D3D12CommandQueueMgr(device, type, dbgName)};
}

D3D12CommandQueueMgr::~D3D12CommandQueueMgr()
{
    /* Automatic */
}

uint64_t D3D12CommandQueueMgr::executeNoWait(D3D12CommandList &list)
{
    return executeNoWait(list.mList, list.mAllocator);
}

void D3D12CommandQueueMgr::executeAndWait(D3D12CommandList &list)
{
    executeAndWait(list.mList, list.mAllocator);
}

void D3D12CommandQueueMgr::addForBatchExecution(D3D12CommandList &list)
{
    addForBatchExecution(list.mList, list.mAllocator);
}

void D3D12CommandQueueMgr::reset(D3D12CommandList &list)
{
    reset(list.mList, list.mAllocator);
}

uint64_t D3D12CommandQueueMgr::batchExecuteNoWait()
{
    if (mBatchJobs.empty())
    {
        // No jobs.
        return 0u;
    }

    // TODO - Find a better way?
    std::vector<::ID3D12CommandList*> jobs(mBatchJobs.size());
    for (std::size_t iii = 0; iii < mBatchJobs.size(); ++iii)
    {
        jobs[iii] = mBatchJobs[iii].list.Get();
    }

    // Execute command lists.
    mCommandQueue->ExecuteCommandLists(static_cast<UINT>(jobs.size()), jobs.data());

    // Add synchronization command.
    const auto fenceValue(fenceSignal());

    // Recover list and allocator.
    for (auto &job : mBatchJobs)
    {
        recoverCommandList(job.list);
        recoverCommandAllocator(job.allocator, fenceValue);
    }

    return fenceValue;
}

void D3D12CommandQueueMgr::batchExecuteAndWait()
{
    const auto fenceValue{batchExecuteNoWait()};

    if (fenceValue)
    {
        // Non-zero value means we should wait.
        fenceWaitFor(fenceValue);
    }
}

void D3D12CommandQueueMgr::fenceWaitFor(uint64_t value)
{
    mFence->waitForValue(value);
}

bool D3D12CommandQueueMgr::fenceValueReached(uint64_t value) const
{
    return mFence->valueReached(value);
}

uint64_t D3D12CommandQueueMgr::fenceSignal()
{
    // Generate new fence value.
    const auto waitFor{++mFenceValue};
    // Add synchronization command to the queue.
    mCommandQueue->Signal(mFence->get(), waitFor);

    return waitFor;
}

void D3D12CommandQueueMgr::flush()
{
    fenceWaitFor(fenceSignal());
}

D3D12CommandList::PtrT D3D12CommandQueueMgr::getCommandList(ID3D12PipelineState *state)
{
    ComPtr<::ID3D12CommandAllocator> allocator{getCreateAllocator()};
    ComPtr<D3D12GraphicsCommandList2T> list{getCreateList(allocator, state)};

    return D3D12CommandList::create(*this, list, allocator);
}

void D3D12CommandQueueMgr::setDebugName(const wchar_t *dbgName)
{
#ifdef _DEBUG
    if (dbgName)
    {
        mDbgName = dbgName;
        mDbgAllocName = mDbgName + L" Command Allocator";
        mDbgListName = mDbgName + L" Command List";

        util::throwIfFailed(mCommandQueue->SetName(mDbgName.c_str()),
                            "Failed to set command queue name.");
    }
#else
	UNUSED(dbgName);
#endif
}

uint64_t D3D12CommandQueueMgr::executeNoWait(ComPtr<D3D12GraphicsCommandList2T> &list,
                                             ComPtr<::ID3D12CommandAllocator> &allocator)
{
    util::throwIfFailed(list->Close(), "Failed to close the command list!");

    // Prepare the command list.
    ::ID3D12CommandList *jobs[1u] = {list.Get()};

    // Execute command lists.
    mCommandQueue->ExecuteCommandLists(static_cast<UINT>(util::count(jobs)), jobs);

    // Add synchronization command.
    const auto fenceValue(fenceSignal());

    // Recover list and allocator.
    recoverCommandList(list);
    recoverCommandAllocator(allocator, fenceValue);

    return fenceValue;
}

void D3D12CommandQueueMgr::executeAndWait(ComPtr<D3D12GraphicsCommandList2T> &list,
                                          ComPtr<::ID3D12CommandAllocator> &allocator)
{
    fenceWaitFor(executeNoWait(list, allocator));
}

void D3D12CommandQueueMgr::addForBatchExecution(ComPtr<D3D12GraphicsCommandList2T> &list,
                                                ComPtr<::ID3D12CommandAllocator> &allocator)
{
    util::throwIfFailed(list->Close(), "Failed to close the command list!");
    mBatchJobs.push_back({list, allocator});
}

void D3D12CommandQueueMgr::reset(ComPtr<D3D12GraphicsCommandList2T> &list,
                                 ComPtr<::ID3D12CommandAllocator> &allocator)
{
    recoverCommandList(list);
    recoverCommandAllocator(allocator);
}

void D3D12CommandQueueMgr::recoverCommandList(ComPtr<D3D12GraphicsCommandList2T> &list)
{
    if (!list)
    {
        throw std::runtime_error("Unable to recover nullptr command list!");
    }
    mListStorage.push(list);
}

void D3D12CommandQueueMgr::recoverCommandAllocator(ComPtr<::ID3D12CommandAllocator> &allocator,
                                                   uint64_t fenceValue)
{
    if (!allocator)
    {
        throw std::runtime_error("Unable to recover nullptr command allocator!");
    }
    ASSERT_FAST((mAllocatorStorage.size() == 0u) || (mAllocatorStorage.back().fenceValue <= fenceValue));
    mAllocatorStorage.push_back({allocator, fenceValue});
}

void D3D12CommandQueueMgr::recoverCommandAllocator(ComPtr<::ID3D12CommandAllocator> &allocator)
{
    mAllocatorStorage.push_front({allocator, 0u});
}

ComPtr<::ID3D12CommandAllocator> D3D12CommandQueueMgr::getCreateAllocator()
{
    ComPtr<::ID3D12CommandAllocator> result{nullptr};

    if (!mAllocatorStorage.empty() && fenceValueReached(mAllocatorStorage.front().fenceValue))
    {
        // We have allocator which can be re-used.
        result = mAllocatorStorage.front().allocator;
        mAllocatorStorage.pop_front();

        util::throwIfFailed(result->Reset(), "Failed to reset the command allocator!");
    }
    else
    {
        // No re-usable allocators, create a new one.
        log<Debug>() << "Creating new command allocator for " <<
            helpers::d3d12::D3D12Helpers::cmdListTypeToStr(mType) <<
            " command queue." << std::endl;;

        result = mDevice->createCommandAllocator(mType);

#ifdef _DEBUG
        if (!mDbgAllocName.empty())
        {
            util::throwIfFailed(result->SetName(mDbgAllocName.c_str()),
                                "Failed to set command allocator name!");
        }
#endif
    }

    return result;
}

ComPtr<D3D12CommandQueueMgr::D3D12GraphicsCommandList2T> D3D12CommandQueueMgr::getCreateList(
    ComPtr<::ID3D12CommandAllocator> &allocator, ID3D12PipelineState *state)
{
    ComPtr<D3D12GraphicsCommandList2T> result{nullptr};

    if (!mListStorage.empty())
    {
        // We have list which can be re-used.
        result = mListStorage.front();
        mListStorage.pop();

        util::throwIfFailed(result->Reset(allocator.Get(), state), "Failed to reset the graphics command list!");
    }
    else
    {
        // No re-usable lists, create a new one.
        log<Debug>() << "Creating new command list for " <<
            helpers::d3d12::D3D12Helpers::cmdListTypeToStr(mType) <<
            " command queue." << std::endl;;

        result = mDevice->createGraphicsCommandList2(allocator.Get(), mType, state);

#ifdef _DEBUG
        if (!mDbgListName.empty())
        {
            util::throwIfFailed(result->SetName(mDbgListName.c_str()),
                                "Failed to set command list name!");
        }
#endif
    }

    return result;
}
}
}
}
