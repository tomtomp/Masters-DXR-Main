/**
 * @file resources/d3d12/D3D12RootSignature.cpp
 * @author Tomas Polasek
 * @brief Wrapper around a Direct3D 12 root signature.
 */

#include "stdafx.h"

#include "engine/resources/d3d12/D3D12RootSignature.h"

namespace quark
{

namespace res
{

namespace d3d12
{

D3D12RootSignature::D3D12RootSignature()
{ /* Automatic */ }

D3D12RootSignature::PtrT D3D12RootSignature::create(const std::vector<::CD3DX12_ROOT_PARAMETER1> &rootParameters, 
    const std::vector<::CD3DX12_STATIC_SAMPLER_DESC> &staticSamplers, ::D3D12_ROOT_SIGNATURE_FLAGS flags, D3D12Device &device)
{ return PtrT{ new D3D12RootSignature(rootParameters, staticSamplers, flags, device) }; }

D3D12RootSignature::PtrT D3D12RootSignature::create(const::CD3DX12_VERSIONED_ROOT_SIGNATURE_DESC &desc, D3D12Device &device)
{ return PtrT{ new D3D12RootSignature(desc, device) }; }

#ifdef ENGINE_RAY_TRACING_ENABLED

D3D12RootSignature::PtrT D3D12RootSignature::create(const std::vector<::CD3DX12_ROOT_PARAMETER1> &rootParameters, 
    const std::vector<::CD3DX12_STATIC_SAMPLER_DESC> &staticSamplers, ::D3D12_ROOT_SIGNATURE_FLAGS flags, D3D12RayTracingDevice &device)
{ return PtrT{ new D3D12RootSignature(rootParameters, staticSamplers, flags, device) }; }

D3D12RootSignature::PtrT D3D12RootSignature::create(const::CD3DX12_VERSIONED_ROOT_SIGNATURE_DESC &desc, D3D12RayTracingDevice &device)
{ return PtrT{ new D3D12RootSignature(desc, device) }; }

#endif

D3D12RootSignature::~D3D12RootSignature()
{ /* Automatic */ }

D3D12RootSignature::D3D12RootSignature(const std::vector<::CD3DX12_ROOT_PARAMETER1> &rootParameters,
    const std::vector<::CD3DX12_STATIC_SAMPLER_DESC> &staticSamplers, ::D3D12_ROOT_SIGNATURE_FLAGS flags,
    D3D12Device &device) :
    D3D12RootSignature(
        rootParameters.data(), rootParameters.size(), 
        staticSamplers.data(), staticSamplers.size(), 
        flags, 
        device.supportsFeatures(
        ::D3D12_FEATURE_DATA_ROOT_SIGNATURE{ ::D3D_ROOT_SIGNATURE_VERSION_1_1 }, 
        ::D3D12_FEATURE_ROOT_SIGNATURE), 
        device )
{ }

D3D12RootSignature::D3D12RootSignature(const ::CD3DX12_VERSIONED_ROOT_SIGNATURE_DESC &desc, D3D12Device &device)
{ createRootSignature(desc, device); }

#ifdef ENGINE_RAY_TRACING_ENABLED

D3D12RootSignature::D3D12RootSignature(const std::vector<::CD3DX12_ROOT_PARAMETER1> &rootParameters,
    const std::vector<::CD3DX12_STATIC_SAMPLER_DESC> &staticSamplers, ::D3D12_ROOT_SIGNATURE_FLAGS flags,
    D3D12RayTracingDevice &device) : 
    D3D12RootSignature(
        rootParameters.data(), rootParameters.size(), 
        staticSamplers.data(), staticSamplers.size(), 
        flags, 
        device.supportsFeatures(
        ::D3D12_FEATURE_DATA_ROOT_SIGNATURE{ ::D3D_ROOT_SIGNATURE_VERSION_1_1 }, 
        ::D3D12_FEATURE_ROOT_SIGNATURE), 
        device)
{ }

D3D12RootSignature::D3D12RootSignature(const ::CD3DX12_VERSIONED_ROOT_SIGNATURE_DESC &desc, 
    D3D12RayTracingDevice &device)
{ createRootSignatureFallback(desc, device); }

#endif

util::PointerType<D3D12RootSignature>::PtrT D3D12RootSignature::create(const ::CD3DX12_ROOT_PARAMETER1 *rootParameters,
    std::size_t numRootParameters, const ::CD3DX12_STATIC_SAMPLER_DESC *staticSamplers, std::size_t numStaticSamplers,
    ::D3D12_ROOT_SIGNATURE_FLAGS flags, bool supportsRS11, D3D12Device &device)
{ return PtrT{ new D3D12RootSignature(rootParameters, numRootParameters, staticSamplers, numStaticSamplers, flags, supportsRS11, device) }; }

D3D12RootSignature::D3D12RootSignature(
    const ::CD3DX12_ROOT_PARAMETER1 *rootParameters, std::size_t numRootParameters,
    const ::CD3DX12_STATIC_SAMPLER_DESC *staticSamplers, std::size_t numStaticSamplers,
    ::D3D12_ROOT_SIGNATURE_FLAGS flags, bool supportsRS11, D3D12Device &device)
{
    ::CD3DX12_VERSIONED_ROOT_SIGNATURE_DESC desc{ };
    desc.Init_1_1(static_cast<::UINT>(numRootParameters), rootParameters, 
        static_cast<::UINT>(numStaticSamplers), staticSamplers, flags);
    UNUSED(supportsRS11);

    createRootSignature(desc, device);
}

void D3D12RootSignature::serializeRootSignature(const ::CD3DX12_VERSIONED_ROOT_SIGNATURE_DESC &desc, 
    ComPtr<::ID3DBlob> &rsBlob)
{
    ComPtr<::ID3DBlob> messages;

    // Take care of differences between RS 1.0 and 1.1 .
    const auto result{ ::D3DX12SerializeVersionedRootSignature(&desc, 
        desc.Version, &rsBlob, &messages) };
    if (FAILED(result))
    {
        std::stringstream ss;
        ss << "Failed to serialize versioned root signature!";
        if (messages)
        {
            ss << ": \n" << reinterpret_cast<char*>(messages->GetBufferPointer());
        }
        throw util::winexception(ss.str());
    }
}

void D3D12RootSignature::createRootSignature(const ::CD3DX12_VERSIONED_ROOT_SIGNATURE_DESC &desc, D3D12Device &device)
{
    ComPtr<::ID3DBlob> rsBlob;

    serializeRootSignature(desc, rsBlob);

    util::throwIfFailed(device->CreateRootSignature(
        0u, 
        rsBlob->GetBufferPointer(), rsBlob->GetBufferSize(), 
        IID_PPV_ARGS(&mRootSignature)), 
        "Failed to create root signature!");
}

#ifdef ENGINE_RAY_TRACING_ENABLED

util::PointerType<D3D12RootSignature>::PtrT D3D12RootSignature::create(const ::CD3DX12_ROOT_PARAMETER1 *rootParameters,
    std::size_t numRootParameters, const ::CD3DX12_STATIC_SAMPLER_DESC *staticSamplers, std::size_t numStaticSamplers,
    ::D3D12_ROOT_SIGNATURE_FLAGS flags, bool supportsRS11, D3D12RayTracingDevice &device)
{ return PtrT{ new D3D12RootSignature(rootParameters, numRootParameters, staticSamplers, numStaticSamplers, flags, supportsRS11, device) }; }

D3D12RootSignature::D3D12RootSignature(const ::CD3DX12_ROOT_PARAMETER1 *rootParameters, std::size_t numRootParameters,
    const ::CD3DX12_STATIC_SAMPLER_DESC *staticSamplers, std::size_t numStaticSamplers,
    ::D3D12_ROOT_SIGNATURE_FLAGS flags, bool supportsRS11, D3D12RayTracingDevice &device)
{
    ::CD3DX12_VERSIONED_ROOT_SIGNATURE_DESC desc{ };
    desc.Init_1_1(static_cast<::UINT>(numRootParameters), rootParameters, 
        static_cast<::UINT>(numStaticSamplers), staticSamplers, flags);
    UNUSED(supportsRS11);

    createRootSignatureFallback(desc, device);
}

void D3D12RootSignature::createRootSignatureFallback(const ::CD3DX12_VERSIONED_ROOT_SIGNATURE_DESC &desc,
    D3D12RayTracingDevice &device)
{ device.serializeAndCreateRootSignature(desc, IID_PPV_ARGS(&mRootSignature)); }

#endif

}

}

}
