/**
 * @file resources/d3d12/D3D12RayTracingCommandList.cpp
 * @author Tomas Polasek
 * @brief Wrapper for D3D12 command list usable in ray tracing.
 */

#include "stdafx.h"

#include "engine/resources/d3d12/D3D12RayTracingCommandList.h"

namespace quark
{

namespace res
{

namespace d3d12
{

D3D12RayTracingCommandList::D3D12RayTracingCommandList()
{ /* Automatic */ }

D3D12RayTracingCommandList::D3D12RayTracingCommandList(res::d3d12::D3D12CommandList &cmdList) :
    mCmdList{ &cmdList }
{ }

#ifdef ENGINE_RAY_TRACING_ENABLED

D3D12RayTracingCommandList::D3D12RayTracingCommandList(res::d3d12::D3D12CommandList &cmdList,
    ComPtr<D3D12FallbackCommandListT> fallbackCmdList) :
    mCmdList{ &cmdList }, mFallbackCmdList{ fallbackCmdList }
{ }

#endif

D3D12RayTracingCommandList::PtrT D3D12RayTracingCommandList::create(res::d3d12::D3D12CommandList &cmdList)
{ return PtrT{ new D3D12RayTracingCommandList(cmdList) }; }

D3D12RayTracingCommandList::PtrT D3D12RayTracingCommandList::create(res::d3d12::D3D12CommandList &cmdList, ComPtr<D3D12FallbackCommandListT> fallbackCmdList)
{ return PtrT{ new D3D12RayTracingCommandList(cmdList, fallbackCmdList) }; }

res::d3d12::D3D12CommandList &D3D12RayTracingCommandList::cmdList()
{
    if (!mCmdList)
    { throw util::winexception("No command list has been set in the ray tracing command list!"); }

    return *mCmdList;
}

#ifdef ENGINE_RAY_TRACING_ENABLED

D3D12RayTracingCommandList::D3D12FallbackCommandListT *D3D12RayTracingCommandList::fallbackCmdList()
{
    if (!mFallbackCmdList)
    { throw util::winexception("No fallback command list has been set in the ray tracing command list!"); }

    return mFallbackCmdList.Get();
}

void D3D12RayTracingCommandList::setTopLevelAccelerationStructure(::UINT rootParameterIndex,
    const ::WRAPPED_GPU_POINTER &gpuPointer)
{
    if (mFallbackCmdList)
    {
		fallbackCmdList()->SetTopLevelAccelerationStructure(
			rootParameterIndex, gpuPointer);
    }
    else if (mCmdList)
    {
#ifdef ENGINE_WITH_WIN_1809
		// Set the acceleration structure.
		cmdList()->SetComputeRootShaderResourceView(
			rootParameterIndex, gpuPointer.GpuVA);
#else
        throw util::winexception("Ray tracing command lists are available only from Windows 10 1809 upwards!");
#endif
    }
    else
    { throw util::winexception("Command list is not initialized!"); }
}

#endif

}

}

}
