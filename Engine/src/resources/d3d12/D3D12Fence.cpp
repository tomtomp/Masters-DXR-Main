/**
 * @file resources/d3d12/D3D12Fence.cpp
 * @author Tomas Polasek
 * @brief Wrapper around Direct3D 12 fence.
 */

#include "stdafx.h"

#include "engine/resources/d3d12/D3D12Fence.h"

#include "engine/helpers/d3d12/D3D12Helpers.h"

namespace quark
{

namespace res
{

namespace d3d12
{

D3D12Fence::D3D12Fence(D3D12Device& device, uint64_t initialValue, const wchar_t *dbgName)
{
    mFence = device.createFence(initialValue);
    mEvent = helpers::d3d12::D3D12Helpers::createFenceEvent();

#ifdef _DEBUG
    if (dbgName)
    {
        util::throwIfFailed(mFence->SetName(dbgName), 
            "Failed to set fence name!");
    }
#else
	UNUSED(dbgName);
#endif
}

D3D12Fence::PtrT D3D12Fence::create(D3D12Device &device, uint64_t initialValue, const wchar_t *dbgName)
{ return PtrT{ new D3D12Fence(device, initialValue, dbgName) }; }

D3D12Fence::~D3D12Fence()
{
    if (mEvent)
    { // Free the waiting handle.
        ::CloseHandle(mEvent);
    }
}

void D3D12Fence::waitForValue(uint64_t value, std::chrono::milliseconds duration)
{
    if (!valueReached(value))
    { // Not reached yet.
        // Set signalling event.
        util::throwIfFailed(mFence->SetEventOnCompletion(value, mEvent),
            "Failed to set completion event on a fence!");
        // Wait on that event, possibly timeout.
        ::WaitForSingleObject(mEvent, static_cast<DWORD>(duration.count()));
    }
}

}

} 

} 
