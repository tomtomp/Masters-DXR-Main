/**
 * @file resources/d3d12/D3D12RayTracingStateObject.cpp
 * @author Tomas Polasek
 * @brief Wrapper around a Direct3D 12 state object used for 
 * ray tracing.
 */

#include "stdafx.h"

#include "engine/resources/d3d12/D3D12RayTracingStateObject.h"

namespace quark
{

namespace res
{

namespace d3d12
{

D3D12RayTracingStateObject::D3D12RayTracingStateObject()
{ /* Automatic */ }

#ifdef ENGINE_RAY_TRACING_ENABLED

D3D12RayTracingStateObject::PtrT D3D12RayTracingStateObject::create(const::D3D12_STATE_OBJECT_DESC &desc, D3D12RayTracingDevice &device)
{ return PtrT{ new D3D12RayTracingStateObject(desc, device) }; }

#endif

D3D12RayTracingStateObject::~D3D12RayTracingStateObject()
{ /* Automatic */ }

#ifdef ENGINE_RAY_TRACING_ENABLED

D3D12RayTracingStateObject::D3D12RayTracingStateObject(
    const ::D3D12_STATE_OBJECT_DESC &desc, D3D12RayTracingDevice &device)
{ device.createStateObject(desc, IID_PPV_ARGS(&mStateObject), IID_PPV_ARGS(&mFallbackStateObject)); }

void *D3D12RayTracingStateObject::getShaderIdentifier(const wchar_t *name)
{
    if (usingFallbackStateObject())
    { return mFallbackStateObject->GetShaderIdentifier(name); }
    else
    {
        ComPtr<::ID3D12StateObjectPropertiesPrototype> props;
        util::throwIfFailed(mStateObject.As(&props), 
            "Failed to convert state object into its properties!");
        return props->GetShaderIdentifier(name);
    }
}

void D3D12RayTracingStateObject::setPipelineState(D3D12RayTracingCommandList &cmdList)
{
    if (usingFallbackStateObject() && cmdList.fallbackCmdList())
    {
        auto *fallbackCmdList{ cmdList.fallbackCmdList() };
        fallbackCmdList->SetPipelineState1(mFallbackStateObject.Get());
        return;
    }
    else if (mStateObject)
    {
#ifdef ENGINE_WITH_WIN_1809
        auto &standardCmdList{ cmdList.cmdList() };
        standardCmdList.list4()->SetPipelineState1(mStateObject.Get());
        return;
#else
        throw util::winexception("Hardware ray tracing command list is available only from Windows 10 1809 upwards!");
#endif
    }

    throw util::winexception("Unable to set pipeline state: State is not initialized or is using different method!");
}

#endif // ENGINE_RAY_TRACING_ENABLED

}

}

}
