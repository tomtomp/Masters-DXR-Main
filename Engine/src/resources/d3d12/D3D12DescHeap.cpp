/**
 * @file resources/d3d12/D3D12DescHeap.cpp
 * @author Tomas Polasek
 * @brief Wrapper around Direct3D 12 descriptor heap.
 */

#include "stdafx.h"

#include "engine/resources/d3d12/D3D12DescHeap.h"

#include "engine/resources/d3d12/D3D12Resource.h"

namespace quark
{

namespace res
{

namespace d3d12
{

uint32_t D3D12DescHeap::DescriptorTable::size() const
{
    const int64_t result{ end - begin };
    return result >= 0 ? static_cast<uint32_t>(result) : 0u;
}

D3D12DescHeap::DescriptorTable::DescriptorTable() :
    DescriptorTable(0u, 0u)
{ }

D3D12DescHeap::DescriptorTable::DescriptorTable(uint32_t beginVal, uint32_t endVal) : 
    begin{ beginVal }, end{ endVal }
{ }

D3D12DescHeap::D3D12DescHeap()
{ /* Automatic */ }

D3D12DescHeap::D3D12DescHeap(D3D12Device &device, ::D3D12_DESCRIPTOR_HEAP_TYPE type, 
    uint32_t numDescriptors, D3D12_DESCRIPTOR_HEAP_FLAGS flags) :
    D3D12DescHeap(device, { type, numDescriptors, flags, 0u })
{ }

D3D12DescHeap::D3D12DescHeap(D3D12Device &device, const ::D3D12_DESCRIPTOR_HEAP_DESC &desc)
{ initializeHeap(device, desc); }

D3D12DescHeap::PtrT D3D12DescHeap::create(D3D12Device &device, ::D3D12_DESCRIPTOR_HEAP_TYPE type, 
    uint32_t numDescriptors, D3D12_DESCRIPTOR_HEAP_FLAGS flags)
{ return PtrT{ new D3D12DescHeap(device, type, numDescriptors, flags) }; }

D3D12DescHeap::PtrT D3D12DescHeap::create(D3D12Device &device, const::D3D12_DESCRIPTOR_HEAP_DESC &desc)
{ return PtrT{ new D3D12DescHeap(device, desc) }; }

void D3D12DescHeap::initializeHeap(D3D12Device &device, const ::D3D12_DESCRIPTOR_HEAP_DESC &desc)
{
    mDevice = &device;
    mDescHandleSize = device.getDescriptorHandleSize(desc.Type);
    mDescHeap = createDescHeap(device, desc);
    mDesc = desc;

    clear();
}

::CD3DX12_CPU_DESCRIPTOR_HANDLE D3D12DescHeap::nextHandle()
{
    const auto result{ nextFreeHandle() };
    allocateHandle(result);

    return result;
}

D3D12DescHeap::DescriptorTable D3D12DescHeap::allocateTable(uint32_t sizeInHandles)
{ return findAllocateTable(sizeInHandles); }

::CD3DX12_CPU_DESCRIPTOR_HANDLE D3D12DescHeap::handleFromTable(const DescriptorTable &table, uint32_t offset) const
{
    if (!isOnThisDescHeap(table))
    { throw InvalidDescriptorTable("Unable to get handle from descriptor table: Table is not on this descriptor heap!"); }
    if (offset > table.size())
    { throw InvalidDescriptorHandle("Unable to get handle from descriptor table: Provided offset is out of bounds!"); }

    return ::CD3DX12_CPU_DESCRIPTOR_HANDLE{ mWholeHeapBlock.begin, static_cast<::INT>(table.startOffset() + offset), mDescHandleSize };
}

::CD3DX12_CPU_DESCRIPTOR_HANDLE D3D12DescHeap::handleFromOffset(uint32_t offset) const
{
    const auto handle{ ::CD3DX12_CPU_DESCRIPTOR_HANDLE{ mWholeHeapBlock.begin, static_cast<::INT>(offset), mDescHandleSize } };
    if (!isOnThisDescHeap(handle))
    { throw InvalidDescriptorHandle("Unable to get handle from offset: Provided offset is out of bounds!"); }

    return handle;
}

::CD3DX12_CPU_DESCRIPTOR_HANDLE D3D12DescHeap::createCBV(D3D12Resource &buffer, ::UINT sizeInBytes)
{
    ::D3D12_CONSTANT_BUFFER_VIEW_DESC desc{ };

    desc.BufferLocation = buffer->GetGPUVirtualAddress();
    desc.SizeInBytes = sizeInBytes;

    const auto handle{ nextHandle() };
    (*mDevice)->CreateConstantBufferView(&desc, handle);
    return handle;
}

::CD3DX12_CPU_DESCRIPTOR_HANDLE D3D12DescHeap::createRTV(D3D12Resource &resource,
    const ::D3D12_RENDER_TARGET_VIEW_DESC &desc)
{ return createRTV(resource, desc, nextHandle()); }

::CD3DX12_CPU_DESCRIPTOR_HANDLE D3D12DescHeap::createRTV(D3D12Resource &resource,
    const ::D3D12_RENDER_TARGET_VIEW_DESC &desc, const ::CD3DX12_CPU_DESCRIPTOR_HANDLE &target)
{
    ASSERT_FAST(isOnThisDescHeap(target));
    (*mDevice)->CreateRenderTargetView(resource.get(), &desc, target);
    return target;
}

::CD3DX12_CPU_DESCRIPTOR_HANDLE D3D12DescHeap::createSRV(D3D12Resource &resource,
    const ::D3D12_SHADER_RESOURCE_VIEW_DESC &desc)
{ return createSRV(resource, desc, nextHandle()); }

::CD3DX12_CPU_DESCRIPTOR_HANDLE D3D12DescHeap::createSRV(D3D12Resource &resource,
    const ::D3D12_SHADER_RESOURCE_VIEW_DESC &desc, const ::CD3DX12_CPU_DESCRIPTOR_HANDLE &target)
{
    ASSERT_FAST(isOnThisDescHeap(target));
    (*mDevice)->CreateShaderResourceView(resource.get(), &desc, target);
    return target;
}

::CD3DX12_CPU_DESCRIPTOR_HANDLE D3D12DescHeap::createDSV(D3D12Resource &resource, const D3D12_DEPTH_STENCIL_VIEW_DESC &desc)
{ return createDSV(resource, desc, nextHandle()); }

::CD3DX12_CPU_DESCRIPTOR_HANDLE D3D12DescHeap::createDSV(D3D12Resource &resource,
    const ::D3D12_DEPTH_STENCIL_VIEW_DESC &desc, const ::CD3DX12_CPU_DESCRIPTOR_HANDLE &target)
{
    ASSERT_FAST(isOnThisDescHeap(target));
    (*mDevice)->CreateDepthStencilView(resource.get(), &desc, target);
    return target;
}

::CD3DX12_CPU_DESCRIPTOR_HANDLE D3D12DescHeap::createUAV(D3D12Resource &resource,
    const ::D3D12_UNORDERED_ACCESS_VIEW_DESC &desc, D3D12Resource *counterResource)
{ return createUAV(resource, desc, nextHandle(), counterResource); }

::CD3DX12_CPU_DESCRIPTOR_HANDLE D3D12DescHeap::createUAV(D3D12Resource &resource,
    const ::D3D12_UNORDERED_ACCESS_VIEW_DESC &desc, const ::CD3DX12_CPU_DESCRIPTOR_HANDLE &target,
    D3D12Resource *counterResource)
{
    (*mDevice)->CreateUnorderedAccessView(resource.get(), 
        counterResource ? counterResource->get() : nullptr, &desc, target);
    return target;
}

::CD3DX12_CPU_DESCRIPTOR_HANDLE D3D12DescHeap::createCBV(const ::D3D12_CONSTANT_BUFFER_VIEW_DESC &desc)
{ return createCBV(desc, nextHandle()); }

::CD3DX12_CPU_DESCRIPTOR_HANDLE D3D12DescHeap::createCBV(const ::D3D12_CONSTANT_BUFFER_VIEW_DESC &desc,
    const ::CD3DX12_CPU_DESCRIPTOR_HANDLE &target)
{
    ASSERT_FAST(isOnThisDescHeap(target));
    (*mDevice)->CreateConstantBufferView(&desc, target); 
    return target;
}

::CD3DX12_CPU_DESCRIPTOR_HANDLE D3D12DescHeap::createSrvNullHandle(
    ::D3D12_SRV_DIMENSION dimension)
{ return createSrvNullHandle(dimension, nextHandle()); }

::CD3DX12_CPU_DESCRIPTOR_HANDLE D3D12DescHeap::createSrvNullHandle(
    ::D3D12_SRV_DIMENSION dimension, const ::CD3DX12_CPU_DESCRIPTOR_HANDLE &target)
{
    ASSERT_FAST(isOnThisDescHeap(target));

    ::D3D12_SHADER_RESOURCE_VIEW_DESC desc{ };
    desc.Format = ::DXGI_FORMAT_R32_FLOAT;
    desc.Shader4ComponentMapping = D3D12_DEFAULT_SHADER_4_COMPONENT_MAPPING;
    desc.ViewDimension = dimension;
    (*mDevice)->CreateShaderResourceView(nullptr, &desc, target);

    return target;
}

::CD3DX12_CPU_DESCRIPTOR_HANDLE D3D12DescHeap::createUavNullHandle(
    ::D3D12_UAV_DIMENSION dimension)
{ return createUavNullHandle(dimension, nextHandle()); }

::CD3DX12_CPU_DESCRIPTOR_HANDLE D3D12DescHeap::createUavNullHandle(
    ::D3D12_UAV_DIMENSION dimension, const ::CD3DX12_CPU_DESCRIPTOR_HANDLE &target)
{
    ASSERT_FAST(isOnThisDescHeap(target));

    ::D3D12_UNORDERED_ACCESS_VIEW_DESC desc{ };
    desc.Format = ::DXGI_FORMAT_R32_FLOAT;
    desc.ViewDimension = dimension;
    (*mDevice)->CreateUnorderedAccessView(nullptr, nullptr, &desc, target);

    return target;
}

::CD3DX12_CPU_DESCRIPTOR_HANDLE D3D12DescHeap::createCbvNullHandle()
{ return createCbvNullHandle(nextHandle()); }

::CD3DX12_CPU_DESCRIPTOR_HANDLE D3D12DescHeap::createCbvNullHandle(
    const ::CD3DX12_CPU_DESCRIPTOR_HANDLE &target)
{
    ASSERT_FAST(isOnThisDescHeap(target));

    ::D3D12_CONSTANT_BUFFER_VIEW_DESC desc{ };
    desc.SizeInBytes = 0u;
    desc.BufferLocation = 0u;
    (*mDevice)->CreateConstantBufferView(&desc, target);

    return target;
}

::CD3DX12_CPU_DESCRIPTOR_HANDLE D3D12DescHeap::createSampler(const ::D3D12_SAMPLER_DESC &desc)
{ return createSampler(desc, nextHandle()); }

::CD3DX12_CPU_DESCRIPTOR_HANDLE D3D12DescHeap::createSampler(const ::D3D12_SAMPLER_DESC &desc,
    const ::CD3DX12_CPU_DESCRIPTOR_HANDLE &target)
{
    (*mDevice)->CreateSampler(&desc, target);
    return target;
}

bool D3D12DescHeap::isOnThisDescHeap(const ::CD3DX12_CPU_DESCRIPTOR_HANDLE &cHandle) const
{ return mWholeHeapBlock.isInBlock(cHandle); }

bool D3D12DescHeap::isOnThisDescHeap(const ::CD3DX12_GPU_DESCRIPTOR_HANDLE &gHandle) const
//{ return gHandle.ptr >= gpuHandle(mWholeHeapBlock.begin).ptr && gHandle.ptr < gpuHandle(mWholeHeapBlock.end).ptr; }
{
    const auto start{ gpuHandle(mWholeHeapBlock.begin) };
    const auto end{ gpuHandle(mWholeHeapBlock.end) };
    const auto startPtr{ start.ptr };
    const auto endPtr{ end.ptr };
    return gHandle.ptr >= startPtr && gHandle.ptr < endPtr;
}

bool D3D12DescHeap::isOnThisDescHeap(const DescriptorTable &table) const
{ return mWholeHeapBlock.isInBlock(table, mDescHandleSize); }

std::size_t D3D12DescHeap::offsetFromStart(const ::CD3DX12_CPU_DESCRIPTOR_HANDLE &cpuHandle) const
{
    const auto diff{ cpuHandle.ptr - mDescHeap->GetCPUDescriptorHandleForHeapStart().ptr };
    return diff > 0 ? static_cast<std::size_t>(diff) / mDescHandleSize : 0u;
}

::CD3DX12_GPU_DESCRIPTOR_HANDLE D3D12DescHeap::baseHandle() const
{ return ::CD3DX12_GPU_DESCRIPTOR_HANDLE{ mDescHeap->GetGPUDescriptorHandleForHeapStart() }; }

::CD3DX12_GPU_DESCRIPTOR_HANDLE D3D12DescHeap::baseTableHandle(const DescriptorTable &table) const
{
    if (!isOnThisDescHeap(table))
    { throw InvalidDescriptorTable("Unable to get base descriptor table handle: Provided table is not from this descriptor heap!"); }

    return gpuHandle(handleFromTable(table, 0u));
}

::CD3DX12_GPU_DESCRIPTOR_HANDLE D3D12DescHeap::gpuHandle(const ::CD3DX12_CPU_DESCRIPTOR_HANDLE &cpuHandle) const
{ return ::CD3DX12_GPU_DESCRIPTOR_HANDLE( baseHandle(), static_cast<::INT>(offsetFromStart(cpuHandle)), static_cast<::UINT>(mDescHandleSize) ); }

uint32_t D3D12DescHeap::copyTable(
    D3D12DescHeap &srcHeap, const DescriptorTable &srcTable, 
    D3D12DescHeap &dstHeap, const DescriptorTable &dstTable)
{
    ASSERT_FAST(srcHeap.initialized() && dstHeap.initialized());
    ASSERT_FAST(srcHeap.type() == dstHeap.type());

    // Nothing to be done.
    if (srcTable.size() == 0u)
    { return 0u; }

    if (!srcHeap.isOnThisDescHeap(srcTable))
    { throw InvalidDescriptorTable("Unable to copy descriptor tables: Source table is not from source descriptor heap!"); }
    if (!dstHeap.isOnThisDescHeap(dstTable))
    { throw InvalidDescriptorTable("Unable to copy descriptor tables: Destination table is not from destination descriptor heap!"); }

    if (srcTable.size() > dstTable.size())
    { throw InvalidDescriptorTable("Unable to copy descriptor tables: Source table contains more descriptors than the destination table!"); }

    auto &device{ *srcHeap.mDevice };

    // Copy srcHeap:srcTable -> dstHeap:dstTable
    device->CopyDescriptorsSimple(
        srcTable.size(), 
        dstHeap.handleFromTable(dstTable, 0u), srcHeap.handleFromTable(srcTable, 0u), 
        srcHeap.type());

    return srcTable.size();
}

uint32_t D3D12DescHeap::size() const
{ return mTotalDescriptors; }

uint32_t D3D12DescHeap::usedDescriptors() const
{ return mAllocatedDescriptors; }

uint32_t D3D12DescHeap::freeDescriptors() const
{
    const int64_t freeHandles{ size() - usedDescriptors() };
    return freeHandles >= 0 ? static_cast<uint32_t>(freeHandles) : 0u;
}

void D3D12DescHeap::clear()
{
    mFreeBlocks.clear();

    mTotalDescriptors = mDesc.NumDescriptors;
    mAllocatedDescriptors = 0u;

    mWholeHeapBlock.begin = mDescHeap->GetCPUDescriptorHandleForHeapStart();
    mWholeHeapBlock.end.InitOffsetted(mWholeHeapBlock.begin, mDesc.NumDescriptors * mDescHandleSize);

    // Mark whole heap as free...
    mFreeBlocks.emplace_back(mWholeHeapBlock);
}

::D3D12_DESCRIPTOR_HEAP_TYPE D3D12DescHeap::type() const
{ return mDesc.Type; }

bool D3D12DescHeap::DescriptorBlock::isInBlock(const ::CD3DX12_CPU_DESCRIPTOR_HANDLE &handle) const
{ return handle.ptr >= begin.ptr && handle.ptr < end.ptr; }

bool D3D12DescHeap::DescriptorBlock::isInBlock(const DescriptorTable &table, 
    uint32_t descriptorSize) const
{ return table.endOffset() <= sizeInHandles(descriptorSize); }

bool D3D12DescHeap::DescriptorBlock::isFirst(const ::CD3DX12_CPU_DESCRIPTOR_HANDLE &handle) const
{ return handle.ptr == begin.ptr; }

bool D3D12DescHeap::DescriptorBlock::isLast(const ::CD3DX12_CPU_DESCRIPTOR_HANDLE &handle,
    uint32_t descriptorSize) const
{ return handle.ptr == end.ptr - descriptorSize; }

bool D3D12DescHeap::DescriptorBlock::isOneBefore(const ::CD3DX12_CPU_DESCRIPTOR_HANDLE &handle,
    uint32_t descriptorSize) const
{ return handle.ptr == begin.ptr + descriptorSize; }

bool D3D12DescHeap::DescriptorBlock::isOnePast(const ::CD3DX12_CPU_DESCRIPTOR_HANDLE &handle) const
{ return handle.ptr == end.ptr; }

bool D3D12DescHeap::DescriptorBlock::isEmpty() const
{ return begin.ptr == end.ptr; }

uint32_t D3D12DescHeap::DescriptorBlock::sizeInHandles(uint32_t descriptorSize) const
{ return static_cast<uint32_t>((end.ptr - begin.ptr) / descriptorSize); }

ComPtr<::ID3D12DescriptorHeap> D3D12DescHeap::createDescHeap(D3D12Device &device, 
    const ::D3D12_DESCRIPTOR_HEAP_DESC &desc)
{
    ComPtr<D3D12DescHeapT> result;

    util::throwIfFailed(device->CreateDescriptorHeap(&desc, IID_PPV_ARGS(&result)), 
        "Failed to create the descriptor heap!");

    return result;
}

::CD3DX12_CPU_DESCRIPTOR_HANDLE D3D12DescHeap::nextFreeHandle()
{
    if (mFreeBlocks.empty())
    { throw DescHeapFull("Unable to find a free handle on the descriptor heap: No free handles remaining!"); }

    return mFreeBlocks.front().begin;
}

void D3D12DescHeap::allocateHandle(const ::CD3DX12_CPU_DESCRIPTOR_HANDLE &handle)
{
    if (!isOnThisDescHeap(handle))
    { throw InvalidDescriptorHandle("Unable to allocate descriptor handle: Given handle is not on this heap!"); }

    // Find the block which contains given handle.
    const auto findIt{ std::find_if(mFreeBlocks.begin(), mFreeBlocks.end(), 
        [&] (const auto &block)
    { return block.isInBlock(handle); }) };

    if (findIt == mFreeBlocks.end())
    { throw DescHeapFull("Unable to allocate descriptor handle: Handle is already allocated!"); }

    // We need to remove the handle from the block that contains it.
    auto &targetBlock{ *findIt };
    if (targetBlock.isFirst(handle) || targetBlock.isLast(handle, mDescHandleSize))
    { // Removing handle which is first or last in the block is easier.
        if (targetBlock.isFirst(handle))
        { targetBlock.begin.Offset(1, mDescHandleSize); }
        else
        { targetBlock.end.Offset(-1, mDescHandleSize); }

        if (targetBlock.isEmpty())
        { // We allocated all of the descriptors -> delete the block.
            mFreeBlocks.erase(findIt);
        }
    }
    else
    { // Handle is in the middle -> split the block.
        /*
         * Original block : < begin, ..., handle, ..., end )
         * Split into : 
         *   First block : < begin, ..., handle)
         *   Second block : < handle + 1, end )
         */
        const DescriptorBlock firstBlock{
            targetBlock.begin, handle};
        const DescriptorBlock secondBlock{
            ::CD3DX12_CPU_DESCRIPTOR_HANDLE{ handle, 1u, mDescHandleSize }, targetBlock.end };

        // Replace the original block, keeping the list ordered.
        mFreeBlocks.insert(findIt, firstBlock);
        mFreeBlocks.insert(findIt, secondBlock);
        mFreeBlocks.erase(findIt);
    }

    mAllocatedDescriptors++;
}

bool D3D12DescHeap::isBetweenBlocks(const DescriptorBlock &first, const DescriptorBlock &second,
    const ::CD3DX12_CPU_DESCRIPTOR_HANDLE &handle) 
{ return handle.ptr >= first.end.ptr && handle.ptr < second.begin.ptr; }

bool D3D12DescHeap::isAdjacent(const DescriptorBlock &first, const DescriptorBlock &second)
{ return first.end.ptr == second.begin.ptr || second.end.ptr == first.begin.ptr; }

void D3D12DescHeap::freeHandle(const ::CD3DX12_CPU_DESCRIPTOR_HANDLE &handle)
{
    if (!isOnThisDescHeap(handle))
    { throw InvalidDescriptorHandle("Unable to free descriptor handle: Given handle is not on this heap!"); }

    if (mFreeBlocks.empty())
    { // No other free handles -> create a new block.
        mFreeBlocks.emplace_back( DescriptorBlock{ handle, 
            ::CD3DX12_CPU_DESCRIPTOR_HANDLE(handle, 1, mDescHandleSize) });
        mAllocatedDescriptors--;
        return;
    }

    const auto blockBeginIt{ mFreeBlocks.begin() };
    const auto blockEndIt{ mFreeBlocks.end() };
    auto firstBlockIt{ blockBeginIt };
    for (; firstBlockIt != blockEndIt; ++firstBlockIt)
    { // Find the 2 blocks between which the handle lies.
        // Get the second block and check we are not at the end.
        const auto secondBlockIt{ ++firstBlockIt };
        if (secondBlockIt == blockEndIt)
        { // Continue the last iteration, so we can check against end().
            continue;
        }

        if (isBetweenBlocks(*firstBlockIt, *secondBlockIt, handle))
        { // We found the two target blocks!
            break;
        }
    }

    if (firstBlockIt == blockEndIt)
    { // Handle is not allocated -> nothing to be done.
        return;
    }

    // Return the handle to its block.
    const auto secondBlockIt{ ++firstBlockIt };
    if (firstBlockIt->isOnePast(handle))
    { // First block is adjacent to the handle.
        firstBlockIt->end.Offset(1, mDescHandleSize);
    }
    else if (secondBlockIt->isOneBefore(handle, mDescHandleSize))
    { // Second block is adjacent to the handle.
        secondBlockIt->begin.Offset(-1, mDescHandleSize);
    }
    else
    { // The handle is somewhere in the void between.
        // Create a new block with one descriptor and insert it.
        const DescriptorBlock newBlock{ handle,
            ::CD3DX12_CPU_DESCRIPTOR_HANDLE{handle, 1, mDescHandleSize} };
        mFreeBlocks.insert(secondBlockIt, newBlock);
    }

    // Check if the blocks can be joined, after the change.
    if (isAdjacent(*firstBlockIt, *secondBlockIt))
    { // Join them...
        firstBlockIt->end = secondBlockIt->end;
        mFreeBlocks.erase(secondBlockIt);
    }

    mAllocatedDescriptors--;
}

D3D12DescHeap::DescriptorTable D3D12DescHeap::findAllocateTable(uint32_t sizeInHandles)
{
    if (sizeInHandles == 0u)
    { return DescriptorTable{ 0u, 0u }; }

    // Find the first block which is large enough.
    const auto findIt{ std::find_if(mFreeBlocks.begin(), mFreeBlocks.end(), 
        [&] (const auto &block)
    { return block.sizeInHandles(mDescHandleSize) >= sizeInHandles; }) };

    if (findIt == mFreeBlocks.end())
    { throw DescHeapFull("Unable to allocate descriptor table: No block large enough has been found!"); }

    // Create the table.
    const auto startOffset{ static_cast<uint32_t>(offsetFromStart(findIt->begin)) };
    DescriptorTable result{ 
        startOffset, 
        startOffset + sizeInHandles
    };

    // Take the required number of handles from the beginning of the table.
    findIt->begin.Offset(sizeInHandles, mDescHandleSize);

    if (findIt->isEmpty())
    { // We used up whole block -> remove it!
        mFreeBlocks.erase(findIt);
    }

    mAllocatedDescriptors += sizeInHandles;

    return result;
}

void D3D12DescHeap::freeTable(const DescriptorTable &table)
{
    if (!isOnThisDescHeap(table))
    { throw InvalidDescriptorHandle("Unable to free descriptor table: Given table is not on this heap!"); }

    // Nothing to be freed -> return.
    if (table.size() == 0u)
    { return; }

    // Calculate border handles.
    const auto beginHandle{ handleFromTable(table, 0u) };
    const auto endHandle{ ::CD3DX12_CPU_DESCRIPTOR_HANDLE{ beginHandle, static_cast<::INT>(table.size()), mDescHandleSize } };

    if (mFreeBlocks.empty())
    { // No other free handles -> create a new block.
        mFreeBlocks.emplace_back(DescriptorBlock{ beginHandle, endHandle });
        mAllocatedDescriptors -= table.size();
        return;
    }

    // Find the lower and upper bounding block for this table.
    const auto blockBeginIt{ mFreeBlocks.begin() };
    const auto blockEndIt{ mFreeBlocks.end() };
    auto lowerBoundBlock{ blockEndIt };
    auto upperBoundBlock{ blockEndIt };
    for (auto blockIt = blockBeginIt; blockIt != blockEndIt; ++blockIt)
    {
        if (blockIt->end.ptr <= beginHandle.ptr)
        { // Found a new match for lower bound.
            lowerBoundBlock = blockIt;
        }

        if (upperBoundBlock == blockEndIt && endHandle.ptr <= blockIt->begin.ptr)
        { // Take first upper bound, since the list is sorted.
            upperBoundBlock = blockIt;
        }
    }

    // The table is allocated between lowerBoundBlock and upperBoundBlock.

    // Create the free descriptors block.
    DescriptorBlock newBlock{ };
    newBlock.begin = beginHandle;
    newBlock.end = endHandle;

    // Fix the blocks.
    const auto adjacentLowerBound{ lowerBoundBlock != blockEndIt && isAdjacent(*lowerBoundBlock, newBlock) };
    const auto adjacentUpperBound{ upperBoundBlock != blockEndIt && isAdjacent(newBlock, *upperBoundBlock) };

    if (adjacentLowerBound && adjacentUpperBound)
    { // Merge all 3 blocks.
        newBlock.begin = lowerBoundBlock->begin;
        newBlock.end = upperBoundBlock->end;
        mFreeBlocks.insert(lowerBoundBlock, newBlock);
        mFreeBlocks.erase(lowerBoundBlock);
        mFreeBlocks.erase(upperBoundBlock);
    }
    else if (adjacentLowerBound)
    { // Merge with lower bound.
        newBlock.begin = lowerBoundBlock->begin;
        mFreeBlocks.insert(lowerBoundBlock, newBlock);
        mFreeBlocks.erase(lowerBoundBlock);
    }
    else if (adjacentUpperBound)
    { // Merge with upper bound.
        newBlock.end = upperBoundBlock->end;
        mFreeBlocks.insert(upperBoundBlock, newBlock);
        mFreeBlocks.erase(upperBoundBlock);
    }
    else
    { // No merging, just insert.
        mFreeBlocks.insert(upperBoundBlock, newBlock);
    }

    mAllocatedDescriptors -= table.size();
}

}

}

}
