/**
 * @file resources/d3d12/D3D12SwapChain.cpp
 * @author Tomas Polasek
 * @brief Wrapper around a Direct3D 12 swap chain.
 */

#include "stdafx.h"

#include "engine/resources/d3d12/D3D12SwapChain.h"

namespace quark
{

namespace res
{

namespace d3d12
{

D3D12SwapChain::D3D12SwapChain(const win::Window &window, const D3D12CommandQueueMgr &directQueue,
    const D3D12DXGIFactory &factory, uint32_t numBuffers, ::DXGI_USAGE usage, ::DXGI_FORMAT format, 
    ::DXGI_SCALING scaling, ::DXGI_ALPHA_MODE alphaMode) :
    mDevice{ directQueue.device() }, 
    mNumBuffers{ numBuffers }, 
    mDescHandleSize{ mDevice->getDescriptorHandleSize(::D3D12_DESCRIPTOR_HEAP_TYPE_RTV) },
    mTearingSupported{ factory.isTearingSupported() },
    mDescHeap{ 
        res::d3d12::D3D12DescHeap::create(
            *mDevice, 
            ::D3D12_DESCRIPTOR_HEAP_TYPE_RTV, 
            mNumBuffers, 
            ::D3D12_DESCRIPTOR_HEAP_FLAG_NONE) }
{
    auto flags{ 0u };
    // Add bit for tearing support, if the target adapter supports it.
    flags |= factory.isTearingSupported() ? DXGI_SWAP_CHAIN_FLAG_ALLOW_TEARING : 0u;

    log<Info>() << "Creating swap chain " << 
        ((flags & DXGI_SWAP_CHAIN_FLAG_ALLOW_TEARING) ? "WITH" : "WITHOUT") << 
        " tearing support." << std::endl;

    // Create flip-discard swap chain.
    mSwapChain = createFlipDiscard(window.hwnd(), window.clientWidth(), window.clientHeight(), 
        directQueue.get(), factory.get(), mNumBuffers, usage, format, scaling, alphaMode, flags);

    // Create RTVs and store back buffer array.
    mBackBuffers = createBufferRtv(*mDevice, *mDescHeap, mDescHandleSize, numBuffers);

    log<Info>() << "Created swap chain" << std::endl;
}

D3D12Resource &D3D12SwapChain::currentBackbuffer()
{
    const auto idx{ currentBackbufferIndex() };
    ASSERT_FAST(idx < mNumBuffers);
    return *mBackBuffers[idx];
}

CD3DX12_CPU_DESCRIPTOR_HANDLE D3D12SwapChain::currentBackbufferRtv() const
{ return { mDescHeap->get()->GetCPUDescriptorHandleForHeapStart(), static_cast<INT>(currentBackbufferIndex()), mDescHandleSize }; }

void D3D12SwapChain::present(uint32_t syncInterval, uint32_t flags)
{ util::throwIfFailed(mSwapChain->Present(syncInterval, flags), "Failed to present!"); }

void D3D12SwapChain::resize(uint32_t width, uint32_t height)
{
    // Free references to the current backbuffers.
    mBackBuffers.clear();

    // Resize the buffers.
    DXGI_SWAP_CHAIN_DESC swapChainDesc = {};
    util::throwIfFailed(mSwapChain->GetDesc(&swapChainDesc), "Failed to get swap chain description!");
    util::throwIfFailed(mSwapChain->ResizeBuffers(mNumBuffers, width, height,
        swapChainDesc.BufferDesc.Format, swapChainDesc.Flags), "Failed to resize swap chain buffers!");

    // Re-create backbuffer wrappers.
    mBackBuffers = createBufferRtv(*mDevice, *mDescHeap, mDescHandleSize, mNumBuffers);
}

ComPtr<D3D12SwapChain::D3D12SwapChainT> D3D12SwapChain::createFlipDiscard(HWND hwnd, uint32_t width, 
    uint32_t height, ID3D12CommandQueue* queue, IDXGIFactory5 *factory, uint32_t numBuffers, 
    ::DXGI_USAGE usage, ::DXGI_FORMAT format, ::DXGI_SCALING scaling, ::DXGI_ALPHA_MODE alphaMode, 
    UINT flags)
{
    ComPtr<D3D12SwapChainT> swapChain;

    if (numBuffers < 2u)
    {
        throw util::winexception("Number of back buffers must be greater or equal to 2!");
    }

    // Swap chain configuration: 

    // X-axis resolution. Can be set to 0 for auto-detection from window.
    mSwapChainDesc.Width = width;
    // Y-axis resolution. Can be set to 0 for auto-detection from window.
    mSwapChainDesc.Height = height;
    // Display and buffer format.
    mSwapChainDesc.Format = format;
    // Stereoscopic 3D.
    mSwapChainDesc.Stereo = false;
    // Must be 1, 0 for flip swap chain.
    mSwapChainDesc.SampleDesc = { 1, 0 };
    // Usage type and CPU access specification for back buffer.
    mSwapChainDesc.BufferUsage = usage;
    // Number of buffers, including the front buffer. Minimum for flip swap chain is 2.
    mSwapChainDesc.BufferCount = numBuffers;
    // Scaling type when resolution of back buffer is not the same as the resolution of the window.
    mSwapChainDesc.Scaling = scaling;
    // Presentation model - discarding the back buffer after presentation.
    mSwapChainDesc.SwapEffect = ::DXGI_SWAP_EFFECT_FLIP_DISCARD;
    // Transparency behavior for the back buffers.
    mSwapChainDesc.AlphaMode = alphaMode;
    // Reset flags.
    mSwapChainDesc.Flags = flags;

    // Create the swap chain.
    ComPtr<::IDXGISwapChain1> swapChain1;
    util::throwIfFailed(
        factory->CreateSwapChainForHwnd(
            // Direct command queue.
            queue, 
            // Window handle.
            hwnd, 
            // Swap chain configuration.
            &mSwapChainDesc, 
            // Fullscreen swap chain configuration - nullptr for windowed swap chain.
            nullptr, 
            // Pointer to output interface - nullptr for unrestricted output.
            nullptr, 
            // Holder for the swap chain pointer.
            &swapChain1), 
        "Failed to create swap chain!");

    // Cast to DXGI 1.5 swap chain.
    util::throwIfFailed(swapChain1.As(&swapChain), 
        "Failed to cast swap chain 1 to swap chain!");

    return swapChain;
}

D3D12SwapChain::PtrT D3D12SwapChain::create(const win::Window &window, const D3D12CommandQueueMgr &directQueue, 
    const D3D12DXGIFactory &factory, uint32_t numBuffers, ::DXGI_USAGE usage, ::DXGI_FORMAT format, 
    ::DXGI_SCALING scaling, ::DXGI_ALPHA_MODE alphaMode)
{ return PtrT{ new D3D12SwapChain(window, directQueue, factory, numBuffers, usage, format, scaling, alphaMode) }; }

D3D12SwapChain::~D3D12SwapChain()
{
    log<Info>() << "Destroyed swap chain" << std::endl;
}

std::vector<D3D12Resource::PtrT> D3D12SwapChain::createBufferRtv(D3D12Device& device, 
    D3D12DescHeap &descHeap, uint32_t descHandleSize, uint32_t numBuffers)
{
    std::vector<D3D12Resource::PtrT> result;

    // Descriptor iterator.
    ::CD3DX12_CPU_DESCRIPTOR_HANDLE descIt(descHeap->GetCPUDescriptorHandleForHeapStart());

    for (uint32_t iii = 0; iii < numBuffers; ++iii)
    {
        // Get frame buffer from swap chain.
        ComPtr<::ID3D12Resource> backBuffer;
        util::throwIfFailed(mSwapChain->GetBuffer(iii, IID_PPV_ARGS(&backBuffer)), 
            "Failed to get frame buffer from the swap chain!");

        // Name the buffer for clarity.
        const auto bufferName{ std::wstring(L"FrameBuffer#") + std::to_wstring(iii) };
        backBuffer->SetName(bufferName.c_str());

        // Create render target view for the current frame buffer, nullptr -> default settings.
        device->CreateRenderTargetView(backBuffer.Get(), nullptr, descIt);

        // Create resource wrapper.
        result.emplace_back(
            res::d3d12::D3D12Resource::create(
                backBuffer, D3D12_RESOURCE_STATE_PRESENT));

        // Move to the next descriptor.
        descIt.Offset(1u, descHandleSize);
    }

    return result;
}

} 

} 

} 
