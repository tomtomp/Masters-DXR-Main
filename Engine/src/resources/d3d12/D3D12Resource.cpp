/**
 * @file resources/d3d12/D3D12Resource.cpp
 * @author Tomas Polasek
 * @brief Wrapper around a Direct3D 12 resource.
 */

#include "stdafx.h"

#include "engine/resources/d3d12/D3D12Resource.h"

#include "engine/helpers/d3d12/D3D12Helpers.h"

namespace quark
{

namespace res
{

namespace d3d12
{

D3D12Resource::D3D12Resource(const ComPtr<D3D12ResourceT> &res, ::D3D12_RESOURCE_STATES currentState) :
    mResource{ res }, mState{ currentState }, mDesc{ mResource->GetDesc() }
{ }

// TODO - Add MipLevels, height, array, etc...
std::size_t D3D12Resource::allocatedSize() const
{
    auto elementSize{ helpers::d3d12::D3D12Helpers::sizeOfFormatElement(mDesc.Format) };
    return mDesc.Width * mDesc.Height * (elementSize ? elementSize : 1u);
}

ComPtr<D3D12Resource::D3D12ResourceT> D3D12Resource::allocateResource(const ::CD3DX12_RESOURCE_DESC &desc,
    ::D3D12_RESOURCE_STATES initState, const ::D3D12_CLEAR_VALUE *optimizedClearValue,
    helpers::d3d12::D3D12BaseGpuAllocator &allocator)
{
    if (!allocator)
    { return nullptr; }

    ComPtr<D3D12ResourceT> result;

    allocator.allocate(desc, initState, optimizedClearValue, IID_PPV_ARGS(&result));

    return result;
}

D3D12Resource::D3D12Resource(const ComPtr<D3D12ResourceT> &res, ::D3D12_RESOURCE_STATES currentState,
    const ::CD3DX12_RESOURCE_DESC &desc) :
    mResource{ res }, mState{ currentState }, mDesc{ desc }
{ }

D3D12Resource::PtrT D3D12Resource::create(const ComPtr<D3D12ResourceT> &res, ::D3D12_RESOURCE_STATES currentState)
{ return PtrT{ new D3D12Resource(res, currentState) }; }

D3D12Resource::PtrT D3D12Resource::create(const ComPtr<D3D12ResourceT> &res, ::D3D12_RESOURCE_STATES currentState, const::CD3DX12_RESOURCE_DESC &desc)
{ return PtrT{ new D3D12Resource(res, currentState, desc) }; } 

D3D12Resource::~D3D12Resource()
{ destroy(); }

void D3D12Resource::transitionTo(::D3D12_RESOURCE_STATES newState, D3D12CommandList &cmdList, 
    UINT subResource, ::D3D12_RESOURCE_BARRIER_FLAGS flags)
{
    ASSERT_FAST(valid());

    // Prevent transitioning to the same state.
    if (mState == newState)
    { return; }

    // Specify translation barrier.
    const auto barrier{CD3DX12_RESOURCE_BARRIER::Transition(mResource.Get(), mState, newState, subResource, flags)};
    // Record barrier command.
    cmdList->ResourceBarrier(1u, &barrier);

    // Notify resource its state has been changed.
    mState = newState;
}

::D3D12_SHADER_RESOURCE_VIEW_DESC D3D12Resource::srvDesc() const
{
    ::D3D12_SHADER_RESOURCE_VIEW_DESC result{ };

    switch (desc().Dimension)
    {
        case D3D12_RESOURCE_DIMENSION_BUFFER : 
        {
            result.Format = desc().Format;
            result.ViewDimension = ::D3D12_SRV_DIMENSION_BUFFER;
            result.Shader4ComponentMapping = D3D12_DEFAULT_SHADER_4_COMPONENT_MAPPING;

            result.Buffer.FirstElement = 0u;
            result.Buffer.NumElements = static_cast<::UINT>(desc().Width * desc().Height);
            result.Buffer.StructureByteStride = 1u;
            result.Buffer.Flags = ::D3D12_BUFFER_SRV_FLAG_RAW;

            break;
        }
        case D3D12_RESOURCE_DIMENSION_TEXTURE2D	: 
        {
            result.Format = desc().Format;
            result.ViewDimension = ::D3D12_SRV_DIMENSION_TEXTURE2D;
            result.Shader4ComponentMapping = D3D12_DEFAULT_SHADER_4_COMPONENT_MAPPING;

            result.Texture2D.MostDetailedMip = 0u;
            result.Texture2D.MipLevels = std::max<::UINT>(desc().MipLevels, 1u);
            result.Texture2D.PlaneSlice = 0u;
            result.Texture2D.ResourceMinLODClamp = 0.0f;

            break;
        }
        default:
        {
            log<Warning>() << "Generating automatic SRV description for resource with unknown dimension!" << std::endl;
            break;
        }
    }

    return result;
}

::D3D12_UNORDERED_ACCESS_VIEW_DESC D3D12Resource::uavDesc() const
{
    ::D3D12_UNORDERED_ACCESS_VIEW_DESC result{ };

    switch (desc().Dimension)
    {
        case D3D12_RESOURCE_DIMENSION_BUFFER : 
        {
            result.Format = desc().Format;
            result.ViewDimension = ::D3D12_UAV_DIMENSION_BUFFER;

            result.Buffer.FirstElement = 0u;
            result.Buffer.NumElements = static_cast<::UINT>(desc().Width * desc().Height);
            result.Buffer.StructureByteStride = 1u;
            result.Buffer.CounterOffsetInBytes = 0u;;
            result.Buffer.Flags = ::D3D12_BUFFER_UAV_FLAG_RAW;

            break;
        }
        case D3D12_RESOURCE_DIMENSION_TEXTURE2D	: 
        {
            result.Format = desc().Format;
            result.ViewDimension = ::D3D12_UAV_DIMENSION_TEXTURE2D;

            result.Texture2D.MipSlice = 0u;
            result.Texture2D.PlaneSlice = 0u;

            break;
        }
        default:
        {
            log<Warning>() << "Generating automatic UAV description for resource with unknown dimension!" << std::endl;
            break;
        }
    }

    return result;
}

::D3D12_CONSTANT_BUFFER_VIEW_DESC D3D12Resource::cbvDesc() const
{
    ::D3D12_CONSTANT_BUFFER_VIEW_DESC result{ };

    switch (desc().Dimension)
    {
        case D3D12_RESOURCE_DIMENSION_BUFFER : 
        case D3D12_RESOURCE_DIMENSION_TEXTURE2D	: 
        {
            result.BufferLocation = mResource ? mResource->GetGPUVirtualAddress() : 0u;
            result.SizeInBytes = static_cast<::UINT>(allocatedSize());

            break;
        }
        default:
        {
            log<Warning>() << "Generating automatic CBV description for resource with unknown dimension!" << std::endl;
            break;
        }
    }

    return result;
}

::D3D12_RENDER_TARGET_VIEW_DESC D3D12Resource::rtvDesc() const
{
    ::D3D12_RENDER_TARGET_VIEW_DESC result{ };

    switch (desc().Dimension)
    {
        case D3D12_RESOURCE_DIMENSION_BUFFER : 
        {
            result.Format = desc().Format;
            result.ViewDimension = ::D3D12_RTV_DIMENSION_BUFFER;

            result.Buffer.FirstElement = 0u;
            result.Buffer.NumElements = static_cast<::UINT>(desc().Width * desc().Height);

            break;
        }
        case D3D12_RESOURCE_DIMENSION_TEXTURE2D	: 
        {
            result.Format = desc().Format;
            result.ViewDimension = ::D3D12_RTV_DIMENSION_TEXTURE2D;

            result.Texture2D.MipSlice = 0u;
            result.Texture2D.PlaneSlice = 0u;

            break;
        }
        default:
        {
            log<Warning>() << "Generating automatic RTV description for resource with unknown dimension!" << std::endl;
            break;
        }
    }

    return result;
}

::D3D12_DEPTH_STENCIL_VIEW_DESC D3D12Resource::dsvDesc() const
{
    ::D3D12_DEPTH_STENCIL_VIEW_DESC result{ };

    switch (desc().Dimension)
    {
        case D3D12_RESOURCE_DIMENSION_TEXTURE2D	: 
        {
            result.Format = desc().Format;
            result.ViewDimension = ::D3D12_DSV_DIMENSION_TEXTURE2D;
            result.Flags = ::D3D12_DSV_FLAG_NONE;

            result.Texture2D.MipSlice = 0u;

            break;
        }
        default:
        {
            log<Warning>() << "Generating automatic RTV description for resource with unknown dimension!" << std::endl;
            break;
        }
    }

    return result;
}

void D3D12Resource::destroy()
{
    if (mAllocator && mResource)
    { mAllocator->deallocate(IID_PPV_ARGS(&mResource)); }

    mAllocator = { };
    mResource = nullptr;
}

} 

} 

} 
