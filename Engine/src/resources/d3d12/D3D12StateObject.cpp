/**
 * @file resources/d3d12/D3D12StateObject.cpp
 * @author Tomas Polasek
 * @brief Wrapper around a Direct3D 12 state object.
 */

#include "stdafx.h"

#include "engine/resources/d3d12/D3D12StateObject.h"

namespace quark
{

namespace res
{

namespace d3d12
{

D3D12StateObject::D3D12StateObject()
{ /* Automatic */ }

D3D12StateObject::PtrT D3D12StateObject::create(const::D3D12_STATE_OBJECT_DESC &desc, D3D12Device &device)
{ return PtrT{ new D3D12StateObject(desc, device) }; }

D3D12StateObject::~D3D12StateObject()
{ /* Automatic */ }

D3D12StateObject::D3D12StateObject(const ::D3D12_STATE_OBJECT_DESC &desc, D3D12Device &device)
{
#ifdef ENGINE_WITH_WIN_1809
    util::throwIfFailed(device.device5()->CreateStateObject(
        &desc, IID_PPV_ARGS(&mStateObject)), 
        "Failed to crate state object!");
#else
	UNUSED(desc);
	UNUSED(device);

    throw util::winexception("State objects are available only from Windows 10 1809 upwards!");
#endif
}

}

}

}
