/**
 * @file resources/win32/WinFile.cpp
 * @author Tomas Polasek
 * @brief Windows file abstraction.
 */

#include "stdafx.h"

#include "engine/resources/win32/WinFile.h"

#include "engine/util/StlWrapperBegin.h"
#include <Shlwapi.h>
#include "engine/util/StlWrapperEnd.h"

namespace quark
{

namespace res
{

namespace win
{

namespace impl
{

bool WinFileImpl::checkExists(const char *filename)
{ return ::PathFileExistsA(filename); }
/*
{
    std::ifstream testFile(filename);
    return testFile.good();
}
*/

bool WinFileImpl::checkExists(const wchar_t *filename)
{ return ::PathFileExistsW(filename); }
/*
{
    std::ifstream testFile(filename);
    return testFile.good();
}
*/

}

}

}

}
