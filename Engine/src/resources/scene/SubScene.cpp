/**
 * @file resources/scene/SubScene.cpp
 * @author Tomas Polasek
 * @brief Container for data describing the 
 * content of a sub-scene.
 */

#include "stdafx.h"

#include "engine/resources/scene/SubScene.h"
#include "engine/resources/scene/Scene.h"

namespace quark
{

namespace res
{

namespace scene
{

SubScene::SubScene()
{ /* Automatic */ }

SubScene::~SubScene()
{ removeSubSceneEntities(); }

quark::scene::EntityHandle SubScene::createEntity(quark::scene::SceneIdentifier sceneId, const std::string &name)
{
    // Create the entity.
    auto entity{ mUniverse->createEntity() };
    // Mark the entity as belonging to this scene/sub-scene combination.
    entity.add<quark::scene::comp::Scene>(sceneId, mIdentifier, name);

    // Create transform component.
    const auto entityTransform{ entity.add<quark::scene::comp::Transform>() };
    // Register the transform component within scene-graph.
    mSceneGraph.insert(entity.id().id(), { entityTransform });

    return entity;
}

bool SubScene::destroyEntity(quark::scene::SceneIdentifier sceneId, quark::scene::EntityHandle &entity)
{
    const auto sceneComp{ entity.get<quark::scene::comp::Scene>() };

    if (!sceneComp)
    { // Entity is not a scene entity!
        return false;
    }

    if (sceneComp->sceneSpec != quark::scene::SceneSpecification{ sceneId, mIdentifier })
    { // Entity is not within this scene:sub-scene!
        return false;
    }

    // Remember entity ID, since it gets reset on destroy().
    const auto entityId{ entity.id().id() };
    // Success, we have a valid entity -> destroy!
    const auto result{ entity.destroy() };

    if (result)
    { // Destroyed the entity, now to unregister it.
        mSceneGraph.erase(entityId);
    }

    return result;
}

bool SubScene::setEntityAsParentOf(const quark::scene::EntityHandle &parent, 
    const quark::scene::EntityHandle &child)
{
    try
    {
        const auto parentIt{ sceneGraphEntity(parent) };
        const auto childIt{ sceneGraphEntity(child) };

        mSceneGraph.makeParentOf(parentIt, childIt);
    } catch (...)
    {
        return false;
    }

    return true;
}

void SubScene::refresh()
{ mSceneGraph.propagateDirty(); }

void SubScene::transformChanged(const quark::scene::EntityHandle &entity)
{ mSceneGraph.setDownDirty(sceneGraphEntity(entity)); }

quark::scene::SceneIdentifier SubScene::id() const
{ return mIdentifier; }

void SubScene::swap(SubScene &other) noexcept
{
    using std::swap;

    swap(mUniverse, other.mUniverse);
    swap(mIdentifier, other.mIdentifier);
    swap(mSceneGraph, other.mSceneGraph);
}

void SubScene::initialize(quark::scene::SceneUniverse &universe, quark::scene::SceneIdentifier identifier)
{
    mUniverse = &universe;
    mIdentifier = identifier;
}

void SubScene::removeSubSceneEntities()
{
    if (!mUniverse)
    { return; }

    std::vector<quark::scene::EntityId> toDelete;

    for (const auto &record : mSceneGraph)
    { toDelete.emplace_back(record.id); }

    for (const auto &entityId : toDelete)
    { 
        const auto result{ mUniverse->destroyEntity(entityId) };
        if (result)
        { mSceneGraph.erase(entityId); }
    }

    // TODO - Implement.
}

util::SceneGraph<ent::EntityId, quark::scene::comp::Transform *>::iterator SubScene::sceneGraphEntity(
    const quark::scene::EntityHandle &entity)
{ 
    const auto findIt{ mSceneGraph.find(entity.id().id()) };
    // Make sure the entity is registered.
    ASSERT_FAST(findIt != mSceneGraph.end());
    return findIt; 
}

}

}

}
