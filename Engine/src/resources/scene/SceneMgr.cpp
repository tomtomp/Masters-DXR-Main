/**
 * @file scene/SceneMgr.cpp
 * @author Tomas Polasek
 * @brief Manager of scenes.
 */

#include "stdafx.h"

#include "engine/resources/scene/SceneMgr.h"

namespace quark
{

namespace res
{

namespace scene
{

SceneMgr::SceneMgr()
{ /* Automatic */ }

SceneMgr::SceneMgr(rndr::GpuMemoryMgr &gpuMemoryMgr, quark::scene::SceneUniverse &universe)
{ initialize(gpuMemoryMgr, universe); }

void SceneMgr::initialize(rndr::GpuMemoryMgr &gpuMemoryMgr, 
    quark::scene::SceneUniverse &universe)
{
    mGpuMemoryMgr = &gpuMemoryMgr;
    mUniverse = &universe;
}

SceneHandle SceneMgr::getOrNull(const IdentifierT &id)
{
    const auto sceneIdx{ findResourceIdx(id) };
    return SceneHandle::idxValid(sceneIdx) ? SceneHandle{ weak_from_this(), sceneIdx } : SceneHandle{ };
}

SceneHandle SceneMgr::create(const IdentifierT &id)
{
    if (id.empty())
    { return create(); }
    else
    { return createInner(id); }
}

SceneHandle SceneMgr::create()
{ return createInner(generateUniqueId()); }

void SceneMgr::remove(const IdentifierT &id)
{
    const auto idx{ findResourceIdx(id) };
    if (SceneHandle::idxValid(idx))
    { // Only acts if the scene actually exists.
        remove({ weak_from_this(), idx });
    }
}

void SceneMgr::remove(const SceneHandle &handle)
{ mLoadedScenes.erase(std::remove(mLoadedScenes.begin(), mLoadedScenes.end(), handle), mLoadedScenes.end()); }

void SceneMgr::clear()
{ mLoadedScenes.clear(); }

void SceneMgr::update()
{
    // Process unused resources.
    purgeZeroRefResources();
}

quark::scene::SceneIdentifier SceneMgr::SceneCounter::nextUniqueSceneId()
{ return sSceneCounter++; }

quark::scene::SceneIdentifier SceneMgr::SceneCounter::currentUniqueSceneId()
{ return sSceneCounter; }

void SceneMgr::SceneCounter::resetUniqueSceneIds()
{ sSceneCounter = 0u; }

SceneHandle SceneMgr::createInner(const IdentifierT &id)
{
    // Check for previous scene creation.
    auto sceneIdx{ findResourceIdx(id) };
    if (!SceneHandle::idxValid(sceneIdx))
    { // Scene with such ID has not been created yet.
        log<Info>() << "Creating a new scene resource: \"" << id << "\"" << std::endl;
        // Create it.
        sceneIdx = createResource(id);
        // Initialize the scene.
        initializeScene(getResource(sceneIdx)->res());
        // Keep the scene loaded until remove is called.
        mLoadedScenes.emplace_back(SceneHandle{ weak_from_this(), sceneIdx });
    }

    return { weak_from_this(), sceneIdx };
}

BaseHandleMgr<Scene, unsigned short, std::string>::IdentifierT SceneMgr::generateUniqueId()
{ return IdentifierT(UNIQUE_ID_BASE) + std::to_string(SceneCounter::currentUniqueSceneId()); }

void SceneMgr::initializeScene(Scene &scene)
{
    // Manager must be initialized!
    ASSERT_FAST(mGpuMemoryMgr && mUniverse);

    // Scene identifiers are never reused.
    scene.initialize(*mGpuMemoryMgr, *mUniverse, SceneCounter::nextUniqueSceneId());
}

}

}

}
