/**
 * @file resources/scene/SubSceneMgr.cpp
 * @author Tomas Polasek
 * @brief Manager of sub-scenes.
 */

#include "stdafx.h"

#include "engine/resources/scene/SubSceneMgr.h"

namespace quark
{

namespace res
{

namespace scene
{

SubSceneMgr::SubSceneMgr()
{ /* Automatic */ }

SubSceneMgr::SubSceneMgr(quark::scene::SceneUniverse &universe, 
    quark::scene::SceneIdentifier sceneIdentifier)
{ initialize(universe, sceneIdentifier); }

void SubSceneMgr::initialize(quark::scene::SceneUniverse &universe, 
    quark::scene::SceneIdentifier sceneIdentifier)
{
    mUniverse = &universe;
    mSceneIdentifier = sceneIdentifier;
    mSubSceneCounter = 0u;
}

SubSceneHandle SubSceneMgr::create(const IdentifierT &id)
{
    if (id.empty())
    { return create(); }
    else
    { return createInner(id); }
}

SubSceneHandle SubSceneMgr::create()
{ return createInner(generateUniqueId()); }

void SubSceneMgr::remove(const IdentifierT &id)
{
    const auto idx{ findResourceIdx(id) };
    if (SubSceneHandle::idxValid(idx))
    { // Only acts if the sub-scene actually exists.
        remove({ weak_from_this(), idx });
    }
}

void SubSceneMgr::remove(const SubSceneHandle &handle)
{ mLoadedSubScenes.erase(std::remove(mLoadedSubScenes.begin(), mLoadedSubScenes.end(), handle), mLoadedSubScenes.end()); }

void SubSceneMgr::clear()
{ mLoadedSubScenes.clear(); }

std::size_t SubSceneMgr::numSubScenes() const
{ return mLoadedSubScenes.size(); }
 
const SubSceneHandle &SubSceneMgr::getSubScene(std::size_t idx) const
{
    ASSERT_FAST(idx < numSubScenes());
    return mLoadedSubScenes[idx];
}

void SubSceneMgr::update()
{
    // Process unused resources.
    purgeZeroRefResources();
}

SubSceneHandle SubSceneMgr::createInner(const IdentifierT &id)
{
    // Check for previous sub-scene creation.
    auto subSceneIdx{ findResourceIdx(id) };
    if (!SubSceneHandle::idxValid(subSceneIdx))
    { // Sub-scene with such ID has not been created yet.
        log<Info>() << "Creating a new sub-scene resource: \"" << id << "\"" << std::endl;
        // Create it.
        subSceneIdx = createResource(id);
        // Initialize the sub-scene.
        initializeSubScene(getResource(subSceneIdx)->res());
        // Keep the sub-scene loaded until remove is called.
        mLoadedSubScenes.emplace_back(SubSceneHandle{ weak_from_this(), subSceneIdx });
    }

    return { weak_from_this(), subSceneIdx };
}

BaseHandleMgr<SubScene, unsigned short, std::string>::IdentifierT SubSceneMgr::generateUniqueId()
{ return IdentifierT(UNIQUE_ID_BASE) + std::to_string(mSubSceneCounter); }

void SubSceneMgr::initializeSubScene(SubScene &subScene)
{
    // Manager must be initialized!
    ASSERT_FAST(mUniverse);

    // Sub-scene identifiers are never reused.
    subScene.initialize(*mUniverse, mSubSceneCounter++);

}

}

}

}
