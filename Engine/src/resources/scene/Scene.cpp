/**
 * @file scene/Scene.cpp
 * @author Tomas Polasek
 * @brief Scene data container. Contains resource managers with 
 * data required to render the scene and a scene graph containing 
 * all of the objects.
 */

#include "stdafx.h"

#include "engine/resources/scene/Scene.h"

#include "engine/renderer/platform/d3d12/D3D12ElementFormat.h"
#include "engine/renderer/platform/d3d12/D3D12SamplerFiltering.h"

namespace quark
{

namespace res
{

namespace scene
{

Scene::Scene()
{ /* Automatic */ }

Scene::~Scene()
{ /* Automatic */ }

void Scene::finalizeChanges()
{
    mBufferMgr->update();
    mBufferViewMgr->update();
    mTextureMgr->update();
    mSamplerMgr->update();
    mMaterialMgr->update();
    mMeshMgr->update();
    mSubSceneMgr->update();

    // Now we can clear the transient data.
    mTransientData.reset();

    mFinalized = true;
}

void Scene::setTransientData(std::any &&data)
{ mTransientData = std::move(data); }

res::rndr::BufferHandle Scene::createBuffer(const std::string &id, const void *data, std::size_t sizeInBytes)
{ return mBufferMgr->create(id, data, sizeInBytes, ::CD3DX12_RESOURCE_DESC::Buffer(sizeInBytes)); }

res::rndr::BufferHandle Scene::createBuffer(const void *data, std::size_t sizeInBytes)
{ return mBufferMgr->create(data, sizeInBytes, ::CD3DX12_RESOURCE_DESC::Buffer(sizeInBytes)); }

res::rndr::BufferViewHandle Scene::createBufferView(const std::string &id, const res::rndr::BufferHandle &targetBuffer,
    quark::rndr::ElementFormat elementFormat, std::size_t numElements, std::size_t byteOffset, std::size_t byteLength,
    std::size_t byteStride)
{ 
    return mBufferViewMgr->create(id, targetBuffer, 
        quark::rndr::platform::d3d12::elementFormatToD3D12(elementFormat), 
        numElements, byteOffset, byteLength, byteStride); 
}

res::rndr::BufferViewHandle Scene::createBufferView(const res::rndr::BufferHandle &targetBuffer,
    quark::rndr::ElementFormat elementFormat, std::size_t numElements, std::size_t byteOffset, 
    std::size_t byteLength, std::size_t byteStride)
{ 
    return mBufferViewMgr->create(targetBuffer, 
        quark::rndr::platform::d3d12::elementFormatToD3D12(elementFormat), 
        numElements, byteOffset, byteLength, byteStride); 
}

res::rndr::TextureHandle Scene::createTexture(const std::string &id, const void *data, std::size_t sizeInBytes,
    quark::rndr::ElementFormat elementFormat, std::size_t width, std::size_t height, std::size_t mipMapCount)
{ 
    return mTextureMgr->create(id, data, sizeInBytes, 
        ::CD3DX12_RESOURCE_DESC::Tex2D(
            quark::rndr::platform::d3d12::elementFormatToD3D12(elementFormat),
            // 2D texture only -> depth is 1u.
            static_cast<::UINT>(width), static_cast<::UINT>(height), 1u, 
            static_cast<::UINT16>(mipMapCount + 1u)));
}

res::rndr::TextureHandle Scene::createTexture(const void *data, std::size_t sizeInBytes,
    quark::rndr::ElementFormat elementFormat, std::size_t width, std::size_t height, std::size_t mipMapCount)
{ 
    return mTextureMgr->create(data, sizeInBytes, 
        ::CD3DX12_RESOURCE_DESC::Tex2D(
            quark::rndr::platform::d3d12::elementFormatToD3D12(elementFormat),
            // 2D texture only -> depth is 1u.
            static_cast<::UINT>(width), static_cast<::UINT>(height), 
            static_cast<::UINT16>(mipMapCount)));
}

res::rndr::StaticSamplerHandle Scene::createSampler(const std::string &id, 
    quark::rndr::MinMagMipFilter filtering, quark::rndr::EdgeFilterUVW edgeFiltering)
{ 
    ::CD3DX12_STATIC_SAMPLER_DESC samplerDesc{
        0u, quark::rndr::platform::d3d12::sampleFilterToD3D12(filtering), 
        quark::rndr::platform::d3d12::sampleEdgeToD3D12(edgeFiltering.edgeFilterU), 
        quark::rndr::platform::d3d12::sampleEdgeToD3D12(edgeFiltering.edgeFilterV), 
        quark::rndr::platform::d3d12::sampleEdgeToD3D12(edgeFiltering.edgeFilterW)};
    
    return mSamplerMgr->create(id, samplerDesc); 
}

res::rndr::StaticSamplerHandle Scene::createSampler(quark::rndr::MinMagMipFilter filtering, 
    quark::rndr::EdgeFilterUVW edgeFiltering)
{ 
    ::CD3DX12_STATIC_SAMPLER_DESC samplerDesc{
        0u, quark::rndr::platform::d3d12::sampleFilterToD3D12(filtering), 
        quark::rndr::platform::d3d12::sampleEdgeToD3D12(edgeFiltering.edgeFilterU), 
        quark::rndr::platform::d3d12::sampleEdgeToD3D12(edgeFiltering.edgeFilterV), 
        quark::rndr::platform::d3d12::sampleEdgeToD3D12(edgeFiltering.edgeFilterW)};

    return mSamplerMgr->create(samplerDesc); 
}

res::rndr::MaterialHandle Scene::createMaterial(const std::string &id)
{ return mMaterialMgr->create(id); }

res::rndr::MeshHandle Scene::createMesh(const std::string &id)
{ return mMeshMgr->create(id); }

res::scene::SubSceneHandle Scene::createSubScene(const std::string &id)
{ return mSubSceneMgr->create(id); }

void Scene::setCurrentSubScene(const res::scene::SubSceneHandle &subScene)
{ mCurrentSubScene = subScene; }

std::size_t Scene::numSubScenes() const
{ return mSubSceneMgr->numSubScenes(); }

res::scene::SubSceneHandle Scene::getSubScene(std::size_t idx) const
{ return mSubSceneMgr->getSubScene(idx); }

quark::scene::EntityHandle Scene::createEntity(res::scene::SubSceneHandle &subScene, 
    const std::string &name)
{ return subScene->createEntity(mIdentifier, name); }

quark::scene::EntityHandle Scene::createEntity(const std::string &name)
{
    if (mCurrentSubScene)
    { return createEntity(mCurrentSubScene, name); }
    else
    { return { }; }
}

bool Scene::destroyEntity(res::scene::SubSceneHandle &subScene, quark::scene::EntityHandle &entity)
{ return subScene->destroyEntity(mIdentifier, entity); }

bool Scene::destroyEntity(quark::scene::EntityHandle &entity)
{
    if (mCurrentSubScene)
    { return destroyEntity(mCurrentSubScene, entity); }
    else
    { return false; }
}

bool Scene::setEntityAsParentOf(res::scene::SubSceneHandle &subScene, 
    const quark::scene::EntityHandle &parent, const quark::scene::EntityHandle &child)
{ return subScene->setEntityAsParentOf(parent, child); }

quark::scene::SceneSpecification Scene::sceneSpecification(const res::scene::SubSceneHandle &subScene) const
{
    return quark::scene::SceneSpecification{
        mIdentifier, subScene->id()
    };
}

quark::scene::SceneSpecification Scene::currentSceneSpecification() const
{ return sceneSpecification(mCurrentSubScene); }

void Scene::transformChanged(const quark::scene::EntityHandle &entity)
{
    if (mCurrentSubScene)
    { mCurrentSubScene->transformChanged(entity); }
}

void Scene::refresh()
{
    if (mCurrentSubScene)
    { mCurrentSubScene->refresh(); }
}

bool Scene::finalized() const
{ return mFinalized; }

void Scene::swap(Scene &other) noexcept
{
    using std::swap;

    swap(mUniverse, other.mUniverse);
    swap(mIdentifier, other.mIdentifier);

    swap(mBufferMgr, other.mBufferMgr);
    swap(mBufferViewMgr, other.mBufferViewMgr);
    swap(mTextureMgr, other.mTextureMgr);
    swap(mSamplerMgr, other.mSamplerMgr);
    swap(mMaterialMgr, other.mMaterialMgr);
    swap(mMeshMgr, other.mMeshMgr);
    swap(mSubSceneMgr, other.mSubSceneMgr);

    swap(mCurrentSubScene, other.mCurrentSubScene);

    swap(mTransientData, other.mTransientData);
}

void Scene::initialize(rndr::GpuMemoryMgr &gpuMemoryMgr, 
    quark::scene::SceneUniverse &universe, 
    quark::scene::SceneIdentifier identifier)
{
    mBufferMgr = res::rndr::BufferMgr::createMgr();
    mBufferViewMgr = res::rndr::BufferViewMgr::createMgr();
    mTextureMgr = res::rndr::TextureMgr::createMgr();
    mSamplerMgr = res::rndr::StaticSamplerMgr::createMgr();
    mMaterialMgr = res::rndr::MaterialMgr::createMgr();
    mMeshMgr = res::rndr::MeshMgr::createMgr();
    mSubSceneMgr = res::scene::SubSceneMgr::createMgr();

    mBufferMgr->initialize(gpuMemoryMgr);
    mTextureMgr->initialize(gpuMemoryMgr);
    mSubSceneMgr->initialize(universe, identifier);

    mUniverse = &universe;
    mIdentifier = identifier;
    mCurrentSubScene = res::scene::SubSceneHandle{ };

    mFinalized = false;
}

}

}

}
