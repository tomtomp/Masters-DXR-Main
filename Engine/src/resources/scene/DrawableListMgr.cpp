/**
 * @file resources/scene/DrawableListMgr.cpp
 * @author Tomas Polasek
 * @brief Manager of entity draw lists.
 */

#include "stdafx.h"

#include "engine/resources/scene/DrawableListMgr.h"

namespace quark
{

namespace res
{

namespace scene
{

DrawableListMgr::DrawableListMgr()
{ /* Automatic */ }

DrawableListMgr::~DrawableListMgr()
{ /* Automatic */ }

DrawableListHandle DrawableListMgr::create(const IdentifierT &id)
{
    if (id.empty())
    { return create(); }
    else
    { return createInner(id); }
}

DrawableListHandle DrawableListMgr::create()
{ return createInner(generateUniqueId()); }

void DrawableListMgr::update()
{
    // Process unused resources.
    purgeZeroRefResources();
}

DrawableListHandle DrawableListMgr::createInner(const IdentifierT &id)
{
    // Check for previous drawable list creation.
    auto subSceneIdx{ findResourceIdx(id) };
    if (!DrawableListHandle::idxValid(subSceneIdx))
    { // Drawable list with such ID has not been created yet.
        log<Info>() << "Creating a new drawable list resource: \"" << id << "\"" << std::endl;
        // Create it.
        subSceneIdx = createResource(id);
    }

    return { weak_from_this(), subSceneIdx };
}

BaseHandleMgr<util::VersionedVector<Drawable>>::IdentifierT DrawableListMgr::generateUniqueId()
{ return IdentifierT(UNIQUE_ID_BASE) + std::to_string(mDrawableListCounter++); }

}

}

}
