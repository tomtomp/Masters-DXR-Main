/**
 * @file resources/d3d12/D3D12Sampler.cpp
 * @author Tomas Polasek
 * @brief Wrapper around a Direct3D 12 sampler.
 */

#include "stdafx.h"

#include "engine/resources/d3d12/D3D12Sampler.h"

namespace quark
{

namespace res
{

namespace d3d12
{

D3D12Sampler::D3D12Sampler()
{ /* Automatic */ }

D3D12Sampler::~D3D12Sampler()
{ /* Automatic */ }

D3D12Sampler::D3D12Sampler(d3d12::D3D12DescHeap &descHeap, const ::D3D12_SAMPLER_DESC &desc)
{ createSampler(descHeap, desc); }

void D3D12Sampler::createSampler(d3d12::D3D12DescHeap &descHeap, const ::D3D12_SAMPLER_DESC &desc)
{
    descHeap.createSampler(desc);
}

}

}

}
