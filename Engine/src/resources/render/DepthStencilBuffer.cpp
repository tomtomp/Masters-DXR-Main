/**
 * @file resources/render/DepthStencilBuffer.cpp
 * @author Tomas Polasek
 * @brief Depth-stencil buffer abstraction, usable in rendering.
 */

#include "stdafx.h"

#include "engine/resources/render/DepthStencilBuffer.h"

namespace quark
{

namespace res
{

namespace rndr
{

DepthStencilBuffer::DepthStencilBuffer()
{ /* Automatic */ }

DepthStencilBuffer::DepthStencilBuffer(uint32_t width, uint32_t height, res::d3d12::D3D12Device &device)
{ initialize(width, height, device); }

DepthStencilBuffer::PtrT DepthStencilBuffer::create(uint32_t width, uint32_t height, res::d3d12::D3D12Device &device)
{ return PtrT{ new DepthStencilBuffer(width, height, device) }; }

DepthStencilBuffer::~DepthStencilBuffer()
{ /* Automatic */ }

void DepthStencilBuffer::initialize(uint32_t width, uint32_t height, res::d3d12::D3D12Device &device)
{
    if (!mDescHeap)
    { // Create the descriptor heap.
        mDescHeap = res::d3d12::D3D12DescHeap::create(device, ::D3D12_DESCRIPTOR_HEAP_TYPE_DSV, 
            1u, ::D3D12_DESCRIPTOR_HEAP_FLAG_NONE);
    }

    if (!mAllocator)
    { // Create the allocator.
        mAllocator = helpers::d3d12::D3D12CommittedAllocator::create(device,
            ::CD3DX12_HEAP_PROPERTIES(::D3D12_HEAP_TYPE_DEFAULT), ::D3D12_HEAP_FLAG_NONE);
    }

    // Clear the descriptor heap, which will be reused.
    mDescHeap->clear();

    // Setup the optimized clear value.
    mClearValue.Format = mDsFormat;
    mClearValue.DepthStencil = { mDepthClear, mStencilClear };

    // Create the depth-stencil buffer.
    mDepthBuffer = helpers::d3d12::D3D12TextureBuffer::create(
        mAllocator, ::CD3DX12_RESOURCE_DESC::Tex2D(
            // User specified depth-stencil format.
            mDsFormat, 
            // Width in pixels.
            width, 
            // Height in pixels.
            height,
            // Create a single texture.
            1u, 
            // No mip-mapping.
            0u, 
            // Single sample per texel.
            1u, 
            // Singular sampling.
            0u, 
            // Allow the use of this texture as a depth-stencil buffer.
            ::D3D12_RESOURCE_FLAG_ALLOW_DEPTH_STENCIL), 
        mInitResourceState, &mClearValue);

    // Create the depth-stencil view for the texture.
    ::D3D12_DEPTH_STENCIL_VIEW_DESC dsvDesc{ };
    dsvDesc.Format = mDsFormat;
    dsvDesc.ViewDimension = ::D3D12_DSV_DIMENSION_TEXTURE2D;
    dsvDesc.Texture2D.MipSlice = 0u;
    dsvDesc.Flags = ::D3D12_DSV_FLAG_NONE;
    mDepthStencilHandle = mDescHeap->createDSV(*mDepthBuffer, dsvDesc);
}

void DepthStencilBuffer::clear(res::d3d12::D3D12CommandList &cmdList, ::D3D12_CLEAR_FLAGS clearFlags)
{
    if (allocated())
    {
        cmdList->ClearDepthStencilView(mDepthStencilHandle, clearFlags, mDepthClear, mStencilClear, 0u, nullptr);
    }
}

}

}

}
