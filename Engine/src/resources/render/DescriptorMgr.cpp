/**
 * @file resources/render/DescriptorMgr.cpp
 * @author Tomas Polasek
 * @brief Manager of shader resource views - SRVs, UAVs and CBVs.
 */

#include "stdafx.h"

#include "engine/resources/render/DescriptorMgr.h"

namespace quark
{

namespace res
{

namespace rndr
{

DescriptorMgr::DescriptorMgr()
{ /* Automatic */ }

DescriptorMgr::DescriptorMgr(rndr::GpuMemoryMgr &gpuMemoryMgr)
{ initialize(gpuMemoryMgr); }

::CD3DX12_CPU_DESCRIPTOR_HANDLE DescriptorMgr::SrvDescriptorRequest::createDescriptor(
    d3d12::D3D12Resource &resource, const ::D3D12_SHADER_RESOURCE_VIEW_DESC &desc, 
    d3d12::D3D12DescHeap &descHeap, const ::CD3DX12_CPU_DESCRIPTOR_HANDLE &target)
{ return descHeap.createSRV(resource, desc, target); }

::CD3DX12_CPU_DESCRIPTOR_HANDLE DescriptorMgr::UavDescriptorRequest::createDescriptor(
    d3d12::D3D12Resource &resource, const ::D3D12_UNORDERED_ACCESS_VIEW_DESC &desc, 
    d3d12::D3D12DescHeap &descHeap, const ::CD3DX12_CPU_DESCRIPTOR_HANDLE &target)
{ return descHeap.createUAV(resource, desc, target); }

::CD3DX12_CPU_DESCRIPTOR_HANDLE DescriptorMgr::CbvDescriptorRequest::createDescriptor(
    // Resource is unused, just to keep uniform interface.
    d3d12::D3D12Resource &resource, const ::D3D12_CONSTANT_BUFFER_VIEW_DESC &desc, 
    d3d12::D3D12DescHeap &descHeap, const ::CD3DX12_CPU_DESCRIPTOR_HANDLE &target)
{ 
    UNUSED(resource);
    return descHeap.createCBV(desc, target); 
}

void DescriptorMgr::initialize(rndr::GpuMemoryMgr &gpuMemoryMgr)
{ mGpuMemoryMgr = &gpuMemoryMgr; }

DescriptorHandle DescriptorMgr::get(const IdentifierT &id)
{ return { weak_from_this(), findResourceIdx(id) }; }

void DescriptorMgr::update()
{
    // Process unused resources.
    purgeZeroRefResources();

    // Finalize creation of SRVs.
    processRequests();
}

d3d12::D3D12DescHeap::PtrT DescriptorMgr::createDescHeap(std::size_t newDescriptors)
{
    return mGpuMemoryMgr->descHeap(
        static_cast<uint32_t>(mCurrentDescHeap->size() + newDescriptors),
        ::D3D12_DESCRIPTOR_HEAP_TYPE_CBV_SRV_UAV,
        ::D3D12_DESCRIPTOR_HEAP_FLAG_SHADER_VISIBLE);
}

void DescriptorMgr::fixExistingHandles(d3d12::D3D12DescHeap &newDescHeap,
    const d3d12::D3D12DescHeap::DescriptorTable &oldSrvTable, const d3d12::D3D12DescHeap::DescriptorTable &newSrvTable,
    const d3d12::D3D12DescHeap::DescriptorTable &oldUavTable, const d3d12::D3D12DescHeap::DescriptorTable &newUavTable,
    const d3d12::D3D12DescHeap::DescriptorTable &oldCbvTable, const d3d12::D3D12DescHeap::DescriptorTable &newCbvTable)
{
    for (const auto &resourceHolder: *this)
    {
        auto &resource{ resourceHolder.res() };
        const auto oldOffset{ resource.offsetInHeap };
        uint32_t newOffset{ 0u };
        switch (resource.handleType)
        {
            case Descriptor::ResourceType::ShaderResourceView:
            {
                newOffset = oldOffset - oldSrvTable.startOffset() + newSrvTable.startOffset();
                break;
            }
            case Descriptor::ResourceType::UnorderedAccessView:
            {
                newOffset = oldOffset - oldUavTable.startOffset() + newUavTable.startOffset();
                break;
            }
            case Descriptor::ResourceType::ConstantBufferView:
            {
                newOffset = oldOffset - oldCbvTable.startOffset() + newCbvTable.startOffset();
                break;
            }
            case Descriptor::ResourceType::Unknown: 
            default:
            {
                continue;
            }
        }

        resource.offsetInHeap = newOffset;
        resource.cpuHandle = newDescHeap.handleFromOffset(newOffset);
    }
}

void DescriptorMgr::processRequests()
{
    if (mSrvRequests.empty())
    { // Nothing to do...
        return;
    }

    // Calculate how many new descriptors we will be creating.
    const auto newRequests{ 
        mSrvRequests.size() + mUavRequests.size() + mCbvRequests.size() };

    // Create the new descriptor heap to fit them all.
    auto newDescHeap{ createDescHeap(newRequests) };

    // Process the SRV requests.
    const auto newSrvTable{
        processAnyRequests(mSrvTable, *mCurrentDescHeap, *newDescHeap, mSrvRequests) };
    mSrvRequests.clear();

    // Process the UAV requests.
    const auto newUavTable{
        processAnyRequests(mUavTable, *mCurrentDescHeap, *newDescHeap, mUavRequests) };
    mUavRequests.clear();

    // Process the CBV requests.
    const auto newCbvTable{
        processAnyRequests(mCbvTable, *mCurrentDescHeap, *newDescHeap, mCbvRequests) };
    mCbvRequests.clear();

    // Fix descriptor handles for already existing descriptors.
    fixExistingHandles(*newDescHeap, 
        mSrvTable, newSrvTable, 
        mUavTable, newUavTable, 
        mCbvTable, newCbvTable);

    // Move over to the new descriptor heap.
    mCurrentDescHeap = newDescHeap;
    mSrvTable = newSrvTable;
    mUavTable = newUavTable;
    mCbvTable = newCbvTable;
}

}

}

}
