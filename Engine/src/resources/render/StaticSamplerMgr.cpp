/**
* @file resources/render/StaticSamplerMgr.cpp
* @author Tomas Polasek
* @brief Manager of static texture samplers.
*/

#include "stdafx.h"

#include "engine/resources/render/StaticSamplerMgr.h"

namespace quark
{

namespace res
{

namespace rndr
{

StaticSamplerMgr::StaticSamplerMgr()
{ /* Automatic */ }

StaticSamplerMgr::~StaticSamplerMgr()
{ /* Automatic */ }

StaticSamplerMgr::StaticSamplerHandle StaticSamplerMgr::create(const IdentifierT &id,
    const ::CD3DX12_STATIC_SAMPLER_DESC &desc)
{
    if (id.empty())
    { return create(desc); }
    else
    { return createInner(id, desc); }
}

StaticSamplerHandle StaticSamplerMgr::create(const ::CD3DX12_STATIC_SAMPLER_DESC &desc)
{ return create(generateUniqueId(desc), desc); }

void StaticSamplerMgr::update()
{
    // Process unused resources.
    purgeZeroRefResources();
}

StaticSamplerHandle StaticSamplerMgr::createInner(const IdentifierT &id, const ::CD3DX12_STATIC_SAMPLER_DESC &desc)
{
    // Check for previous sampler creation.
    auto samplerIdx{ findResourceIdx(id) };
    if (!StaticSamplerHandle::idxValid(samplerIdx))
    { // Sampler with such ID has not been created yet.
        log<Info>() << "Creating a new static sampler resource: \"" << id << "\"" << std::endl;
        // Create it.
        samplerIdx = createResource(id);
        // Remember provided parameters.
        getResource(samplerIdx)->res() = desc;
    }

    return { weak_from_this(), samplerIdx };
}

BaseHandleMgr<CD3DX12_STATIC_SAMPLER_DESC, unsigned short, std::string>::IdentifierT StaticSamplerMgr::
generateUniqueId(const ::CD3DX12_STATIC_SAMPLER_DESC &desc) const
{ return IdentifierT(UNIQUE_ID_BASE) + std::to_string(util::hashAll(desc)); }

}

}

}
