/**
 * @file renderer/GpuMemoryMgr.cpp
 * @author Tomas Polasek
 * @brief Class used for managing of GPU memory and 
 * uploading of resources.
 */

#include "stdafx.h"

#include "engine/resources/render/GpuMemoryMgr.h"

namespace quark
{

namespace res
{

namespace rndr
{

GpuMemoryMgr::GpuMemoryMgr(res::d3d12::D3D12Device &device, 
    res::d3d12::D3D12CommandQueueMgr &copyCmdQueueMgr) : 
    mDevice{ &device }, mCopyCommandQueue{ &copyCmdQueueMgr },
    mResourceUpdater{ helpers::d3d12::D3D12ResourceUpdater::create(*mDevice) }
{ ASSERT_FAST(mCopyCommandQueue->type() == ::D3D12_COMMAND_LIST_TYPE_COPY); }

GpuMemoryMgr::PtrT GpuMemoryMgr::create(res::d3d12::D3D12Device &device, res::d3d12::D3D12CommandQueueMgr &copyCmdQueueMgr)
{ return PtrT{ new GpuMemoryMgr(device, copyCmdQueueMgr) }; }

GpuMemoryMgr::~GpuMemoryMgr()
{ /* Automatic */ }

helpers::d3d12::D3D12CommittedAllocator::PtrT GpuMemoryMgr::committedAllocator(
    const ::CD3DX12_HEAP_PROPERTIES &heapProps, ::D3D12_HEAP_FLAGS heapFlags)
{
    return helpers::d3d12::D3D12CommittedAllocator::create(*mDevice, 
        heapProps, heapFlags);
}

helpers::d3d12::D3D12PlacedAllocator::PtrT GpuMemoryMgr::placedAllocator(
    std::size_t sizeInBytes, const ::CD3DX12_HEAP_PROPERTIES &heapProps, ::D3D12_HEAP_FLAGS heapFlags)
{
    return helpers::d3d12::D3D12PlacedAllocator::create(*mDevice, 
        ::CD3DX12_HEAP_DESC(sizeInBytes, heapProps, 
            0u, heapFlags));
}

helpers::d3d12::D3D12DummyAllocator::PtrT GpuMemoryMgr::dummyAllocator()
{ return helpers::d3d12::D3D12DummyAllocator::create(*mDevice); }

d3d12::D3D12CommandList::PtrT GpuMemoryMgr::copyCommandList()
{ return mCopyCommandQueue->getCommandList(); }

d3d12::D3D12DescHeap::PtrT GpuMemoryMgr::descHeap(uint32_t size, ::D3D12_DESCRIPTOR_HEAP_TYPE type,
    ::D3D12_DESCRIPTOR_HEAP_FLAGS flags)
{ return d3d12::D3D12DescHeap::create(*mDevice, type, size, flags); }

void GpuMemoryMgr::uploadData(const void *data, std::size_t sizeInBytes, d3d12::D3D12Resource &target,
    d3d12::D3D12CommandList &copyCmdList)
{ mResourceUpdater->copyToResource(data, sizeInBytes, target, copyCmdList); }

helpers::d3d12::D3D12ResourceUpdater &GpuMemoryMgr::updater()
{ return *mResourceUpdater; }

void GpuMemoryMgr::uploadCmdListsExecuted()
{ mResourceUpdater->commandListExecuted(); }

}

}

}
