/**
 * @file resources/render/BufferViewMgr.cpp
 * @author Tomas Polasek
 * @brief Manager of views to the GPU-side buffers.
 */

#include "stdafx.h"

#include "engine/resources/render/BufferViewMgr.h"

namespace quark
{

namespace res
{

namespace rndr
{

BufferViewMgr::BufferViewMgr()
{ /* Automatic */ }

BufferViewMgr::BufferViewHandle BufferViewMgr::create(const IdentifierT &id, const BufferHandle &targetBuffer,
    ::DXGI_FORMAT elementFormat, std::size_t numElements, std::size_t byteOffset, std::size_t byteLength,
    std::size_t byteStride)
{
    if (id.empty())
    { return create(targetBuffer, elementFormat, numElements, byteOffset, byteLength, byteStride); }
    else
    { return createInner(id, targetBuffer, elementFormat, numElements, byteOffset, byteLength, byteStride); }
}

BufferViewHandle BufferViewMgr::create(const BufferHandle &targetBuffer, ::DXGI_FORMAT elementFormat,
    std::size_t numElements, std::size_t byteOffset, std::size_t byteLength, std::size_t byteStride)
{
    return createInner(generateUniqueId(targetBuffer, elementFormat, numElements, byteOffset, byteLength, byteStride), 
        targetBuffer, elementFormat, numElements, byteOffset, byteLength, byteStride);
}

void BufferViewMgr::update()
{
    // Process unused resources.
    purgeZeroRefResources();

    // Finalize creation of buffers.
    processRequests();
}

BufferViewHandle BufferViewMgr::createInner(const IdentifierT &id, const BufferHandle &targetBuffer,
    ::DXGI_FORMAT elementFormat, std::size_t numElements, std::size_t byteOffset, std::size_t byteLength,
    std::size_t byteStride)
{
    // Check for previous buffer view creation.
    auto viewIdx{ findResourceIdx(id) };
    if (!BufferViewHandle::idxValid(viewIdx))
    { // Buffer view with such ID has not been created yet.
        log<Info>() << "Creating a new buffer view resource: \"" << id << "\"" << std::endl;
        // Create it.
        viewIdx = createResource(id);
        // Remember to finalize the resource view.
        appendRequestTicket(getResource(viewIdx), targetBuffer, elementFormat, 
            numElements, byteOffset, byteLength, byteStride);
    }

    return { weak_from_this(), viewIdx };
}

void BufferViewMgr::appendRequestTicket(ResourceHolder *res, const BufferHandle &targetBuffer,
    ::DXGI_FORMAT elementFormat, std::size_t numElements, std::size_t byteOffset, std::size_t byteLength,
    std::size_t byteStride)
{
    // Keep reference to the buffer, GPU address may not be known at this time.
    /*
    res->res() = BufferView{ targetBuffer, 0u, byteOffset, 
        byteLength, byteStride, elementFormat, numElements };
        */
    res->res().targetBuffer = targetBuffer;
    res->res().elementFormat = elementFormat;
    res->res().numElements = numElements;
    res->res().byteOffset = byteOffset;
    res->res().byteLength = byteLength;
    res->res().byteStride = byteStride;

    mBufferViewRequests.emplace_back(res); 
}

void BufferViewMgr::processRequests()
{
    if (mBufferViewRequests.empty())
    { // Nothing to do...
        return;
    }

    log<Info>() << "Processing " << mBufferViewRequests.size() << " new buffer view requests!" << std::endl;

    for (const auto &request : mBufferViewRequests)
    { // Fulfill the requests.
        if (request->refs() != 0u)
        { // Only take requests which are still referenced.
            initializeBufferView(request);
        }
    }
}

void BufferViewMgr::initializeBufferView(ResourceHolder *res)
{
    // Make sure the target buffer has indeed been initialized.
    ASSERT_FAST(res->res().targetBuffer.getHolder().initialized());
    // Acquire memory position of the target buffer and cache it for later use.
    res->res().targetBufferAddress = res->res().targetBuffer->buffer->GetGPUVirtualAddress();
    // Finalized initialization -> mark the resource.
    res->markInitialized();
}

BaseHandleMgr<BufferView, unsigned short, std::string>::IdentifierT BufferViewMgr::generateUniqueId(
    const BufferHandle &targetBuffer, ::DXGI_FORMAT elementFormat, std::size_t numElements, std::size_t byteOffset,
    std::size_t byteLength, std::size_t byteStride) const
{ return IdentifierT(UNIQUE_ID_BASE) + std::to_string(util::hashAll(targetBuffer, elementFormat, numElements, byteOffset, byteLength, byteStride)); }

}

}

}
