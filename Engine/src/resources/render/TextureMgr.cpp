/**
 * @file resources/render/TextureMgr.cpp
 * @author Tomas Polasek
 * @brief Manager of GPU-side textures.
 */

#include "stdafx.h"

#include "engine/resources/render/TextureMgr.h"

#include "engine/helpers/d3d12/D3D12DummyAllocator.h"

namespace quark
{

namespace res
{

namespace rndr
{

TextureMgr::TextureMgr()
{ /* Automatic */ }

TextureMgr::TextureMgr(rndr::GpuMemoryMgr &gpuMemoryMgr)
{ initialize(gpuMemoryMgr); }

void TextureMgr::initialize(rndr::GpuMemoryMgr &gpuMemoryMgr)
{ mGpuMemoryMgr = &gpuMemoryMgr; }

TextureHandle TextureMgr::get(const IdentifierT &id) 
{ return { weak_from_this(), findResourceIdx(id) }; }

TextureHandle TextureMgr::create(const IdentifierT &id, const void *data, std::size_t sizeInBytes,
    const ::CD3DX12_RESOURCE_DESC &desc)
{
    if (id.empty())
    { return create(data, sizeInBytes, desc); }
    else
    { return createInner(id, data, sizeInBytes, desc); }
}

TextureHandle TextureMgr::create(const void *data, std::size_t sizeInBytes, const ::CD3DX12_RESOURCE_DESC &desc)
{ return createInner(generateUniqueId(data, sizeInBytes, desc), data, sizeInBytes, desc); }

TextureHandle TextureMgr::createImmediate(const IdentifierT &id, const void *data, std::size_t sizeInBytes,
    const ::CD3DX12_RESOURCE_DESC &desc)
{
    if (id.empty())
    { return createImmediate(data, sizeInBytes, desc); }
    else
    { return createImmediateInner(id, data, sizeInBytes, desc); }
}

TextureHandle TextureMgr::createImmediate(const void *data, std::size_t sizeInBytes, const ::CD3DX12_RESOURCE_DESC &desc)
{ return createImmediateInner(generateUniqueId(data, sizeInBytes, desc), data, sizeInBytes, desc); }

TextureHandle TextureMgr::createEmpty(const IdentifierT &id, const ::CD3DX12_RESOURCE_DESC &desc,
    ::D3D12_RESOURCE_STATES initialState, const ::D3D12_CLEAR_VALUE *clearValue)
{
    // Check for previous texture creation.
    auto textureIdx{ findResourceIdx(id) };
    if (!TextureHandle::idxValid(textureIdx))
    { // Buffer with such ID has not been created yet.
        log<Info>() << "Creating a new empty texture resource: \"" << id << "\"" << std::endl;
        // Create it.
        textureIdx = createResource(id);
        // Process the request, immediately creating .
        processImmediateEmpty(textureIdx, desc, initialState, clearValue);
    }

    return { weak_from_this(), textureIdx };
}

void TextureMgr::update()
{
    // Process unused resources.
    purgeZeroRefResources();

    // Finalize creation of textures.
    log<Info>() << "Processing " << mBufferRequests.size() << " new texture requests!" << std::endl;
    processRequests();
}

TextureHandle TextureMgr::createInner(const IdentifierT &id, const void *data, std::size_t sizeInBytes,
    const ::CD3DX12_RESOURCE_DESC &desc)
{
    // Check for previous texture creation.
    auto textureIdx{ findResourceIdx(id) };
    if (!TextureHandle::idxValid(textureIdx))
    { // Buffer with such ID has not been created yet.
        log<Info>() << "Creating a new texture resource: \"" << id << "\"" << std::endl;
        // Create it.
        textureIdx = createResource(id);
        // Remember provided parameters.
        appendRequestTicket(getResource(textureIdx), data, sizeInBytes, desc);
    }

    return { weak_from_this(), textureIdx };
}

TextureHandle TextureMgr::createImmediateInner(const IdentifierT &id, const void *data, std::size_t sizeInBytes,
    const ::CD3DX12_RESOURCE_DESC &desc)
{
    // Check for previous texture creation.
    auto textureIdx{ findResourceIdx(id) };
    if (!TextureHandle::idxValid(textureIdx))
    { // Buffer with such ID has not been created yet.
        log<Info>() << "Creating a new texture resource: \"" << id << "\"" << std::endl;
        // Create it.
        textureIdx = createResource(id);
        // Process the request, immediately creating and filling the texture.
        processImmediateRequest(textureIdx, data, sizeInBytes, desc);
    }

    return { weak_from_this(), textureIdx };
}

TextureMgr::TextureRequest TextureMgr::createRequestTicket(ResourceHolder *res, const void *data, std::size_t sizeInBytes,
    const ::CD3DX12_RESOURCE_DESC &desc)
{ return { res, data, sizeInBytes, desc }; }

void TextureMgr::appendRequestTicket(ResourceHolder *res, const void *data, std::size_t sizeInBytes,
    const ::CD3DX12_RESOURCE_DESC &desc)
{ mBufferRequests.emplace_back(createRequestTicket(res, data, sizeInBytes, desc)); }

void TextureMgr::processRequests()
{
    if (mBufferRequests.empty())
    { // Nothing to do...
        return;
    }

    // Calculate required heap size to create all of the requested resources.
    auto dummyAllocator{ mGpuMemoryMgr->dummyAllocator() };
    for (const auto &request : mBufferRequests)
    { // Accumulate total required heap size.
        if (request.target->refs() != 0u)
        { // Only take requests which are still referenced.
            dummyAllocator->allocate(request.desc, ::D3D12_RESOURCE_STATE_COPY_DEST, nullptr, IID_NULL, nullptr);
        }
    }

    const auto totalBytesRequired{ dummyAllocator->allocated() };
    log<Info>() << "Total size of requests is: " << totalBytesRequired << std::endl;

    // Allocate all of the new resources on one heap.
    auto allocator{ mGpuMemoryMgr->placedAllocator(totalBytesRequired, 
            ::CD3DX12_HEAP_PROPERTIES(::D3D12_HEAP_TYPE_DEFAULT), TEXTURE_HEAP_FLAGS) };

    /// Prepare copy command list.
    auto cmdList{ mGpuMemoryMgr->copyCommandList() };

    for (const auto &request : mBufferRequests)
    { // Fulfill the requests.
        if (request.target->refs() != 0u)
        { // Only take requests which are still referenced.
            initializeTexture(request, allocator, *cmdList);
        }
    }

    // Asynchronously execute the copy commands.
    cmdList->executeNoWait();
}

void TextureMgr::processImmediateRequest(const HandleIdxT &id, const void *data, std::size_t sizeInBytes,
    const ::CD3DX12_RESOURCE_DESC &desc)
{
    // Allocate the texture on its own heap.
    auto allocator{ mGpuMemoryMgr->committedAllocator(
        ::CD3DX12_HEAP_PROPERTIES(::D3D12_HEAP_TYPE_DEFAULT), TEXTURE_HEAP_FLAGS) };

    const auto holderPtr{ getResource(id) };
    ASSERT_FAST(holderPtr);
    auto &holder{ *holderPtr };
    auto &res{ holder.res() };

    // Create the GPU texture.
    res.texture.allocate(
        allocator, 
        desc, 
        ::D3D12_RESOURCE_STATE_COMMON, 
        nullptr, 
        // Debug name: 
        util::toWString(holder.id()));

    if (data == nullptr || sizeInBytes == 0u)
    { res.texture.uploadDataImmediate(data, sizeInBytes); }
}

void TextureMgr::processImmediateEmpty(const HandleIdxT &id, const ::CD3DX12_RESOURCE_DESC &desc, 
    ::D3D12_RESOURCE_STATES initialState, const ::D3D12_CLEAR_VALUE *clearValue)
{
    // Allocate the texture on its own heap.
    auto allocator{ mGpuMemoryMgr->committedAllocator(
        ::CD3DX12_HEAP_PROPERTIES(::D3D12_HEAP_TYPE_DEFAULT), TEXTURE_HEAP_FLAGS) };

    const auto holderPtr{ getResource(id) };
    ASSERT_FAST(holderPtr);
    auto &res{ holderPtr->res() };

    // Create the GPU texture.
    res.texture.allocate(
        allocator, 
        desc, 
        initialState, 
        clearValue, 
        // Debug name: 
        util::toWString(holderPtr->id()));
}

void TextureMgr::initializeTexture(const TextureRequest &request, 
    helpers::d3d12::D3D12PlacedAllocator::PtrT allocator,
    d3d12::D3D12CommandList &cmdList)
{
    // Obtain the target resource.
    const auto holderPtr{ request.target };
    ASSERT_FAST(holderPtr && !holderPtr->initialized());
    auto &holder{ *holderPtr };
    auto &res{ holder.res() };

    log<Info>() << "Initializing texture resource: \"" << holder.id() << "\"." << std::endl;

    // Allocate the texture and set debug name.
    res.texture.allocate(
        allocator, 
        request.desc, 
        ::D3D12_RESOURCE_STATE_COMMON, 
        nullptr, 
        // Debug Name: 
        util::toWString(holder.id()));

    // Record commands to upload the data to the GPU.
    mGpuMemoryMgr->uploadData(request.data, request.sizeInBytes, res.texture, cmdList);

    // We can say that we initialized the buffer.
    holder.markInitialized();
}

BaseHandleMgr<d3d12::D3D12Resource, unsigned short, std::string>::IdentifierT TextureMgr::
generateUniqueId(const void *data, std::size_t sizeInBytes, const ::CD3DX12_RESOURCE_DESC &desc) const
{ return IdentifierT(UNIQUE_ID_BASE) + std::to_string(util::hashAll(data, sizeInBytes, desc)); }

}

}

}
