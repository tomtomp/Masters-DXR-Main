/**
 * @file resources/render/MeshMgr.cpp
 * @author Tomas Polasek
 * @brief Manager of meshes, connection of vertex attributes, indices and material.
 */

#include "stdafx.h"

#include "engine/resources/render/MeshMgr.h"

namespace quark
{

namespace res
{

namespace rndr
{

MeshMgr::MeshMgr()
{ /* Automatic */ }

MeshMgr::~MeshMgr()
{ /* Automatic */ }

MeshMgr::MeshHandle MeshMgr::create(const IdentifierT &id)
{
    if (id.empty())
    { return createInner(generateUniqueId()); }
    else
    { return createInner(id); }
}

void MeshMgr::update()
{
    // Process unused resources.
    purgeZeroRefResources();
}

MeshHandle MeshMgr::createInner(const IdentifierT &id)
{
    // Check for previous mesh creation.
    auto meshIdx{ findResourceIdx(id) };
    if (!MeshHandle::idxValid(meshIdx))
    { // Mesh with such ID has not been created yet.
        log<Info>() << "Creating a new mesh resource: \"" << id << "\"" << std::endl;
        // Create it.
        meshIdx = createResource(id);
        // Mark as already initialized.
        getResource(meshIdx)->markInitialized();
    }

    return { weak_from_this(), meshIdx };
}

BaseHandleMgr<Mesh, unsigned short, std::string> ::IdentifierT MeshMgr::generateUniqueId()
{ return IdentifierT(UNIQUE_ID_BASE) + std::to_string(mUniqueIdCounter++); }

}

}

}
