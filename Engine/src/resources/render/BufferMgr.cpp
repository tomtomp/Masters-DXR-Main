/**
 * @file resources/render/BufferMgr.cpp
 * @author Tomas Polasek
 * @brief Manager of GPU-side buffers.
 */

#include "stdafx.h"

#include "engine/resources/render/BufferMgr.h"

#include "engine/helpers/d3d12/D3D12DummyAllocator.h"

namespace quark
{

namespace res
{

namespace rndr
{

BufferMgr::BufferMgr()
{ /* Automatic */ }

BufferMgr::BufferMgr(rndr::GpuMemoryMgr &gpuMemoryMgr)
{ initialize(gpuMemoryMgr); }

void BufferMgr::initialize(rndr::GpuMemoryMgr &gpuMemoryMgr)
{ mGpuMemoryMgr = &gpuMemoryMgr; }

BufferHandle BufferMgr::get(const IdentifierT &id) 
{ return { weak_from_this(), findResourceIdx(id) }; }

BufferHandle BufferMgr::create(const IdentifierT &id, const void *data, std::size_t sizeInBytes,
    const ::CD3DX12_RESOURCE_DESC &desc)
{
    if (id.empty())
    { return create(data, sizeInBytes, desc); }
    else
    { return createInner(id, data, sizeInBytes, desc); }
}

BufferHandle BufferMgr::create(const void *data, std::size_t sizeInBytes, const ::CD3DX12_RESOURCE_DESC &desc)
{ return createInner(generateUniqueId(data, sizeInBytes, desc), data, sizeInBytes, desc); }

BufferHandle BufferMgr::createImmediate(const IdentifierT &id, const void *data, std::size_t sizeInBytes,
    const ::CD3DX12_RESOURCE_DESC &desc)
{
    if (id.empty())
    { return createImmediate(data, sizeInBytes, desc); }
    else
    { return createImmediateInner(id, data, sizeInBytes, desc); }
}

BufferHandle BufferMgr::createImmediate(const void *data, std::size_t sizeInBytes, const ::CD3DX12_RESOURCE_DESC &desc)
{ return createImmediateInner(generateUniqueId(data, sizeInBytes, desc), data, sizeInBytes, desc); }

void BufferMgr::update()
{
    // Process unused resources.
    purgeZeroRefResources();

    // Finalize creation of buffers.
    log<Info>() << "Processing " << mBufferRequests.size() << " new buffer requests!" << std::endl;
    processRequests();
}

BufferHandle BufferMgr::createInner(const IdentifierT &id, const void *data, std::size_t sizeInBytes,
    const ::CD3DX12_RESOURCE_DESC &desc)
{
    // Check for previous buffer creation.
    auto bufferIdx{ findResourceIdx(id) };
    if (!BufferHandle::idxValid(bufferIdx))
    { // Buffer with such ID has not been created yet.
        log<Info>() << "Creating a new buffer resource: \"" << id << "\"" << std::endl;
        // Create it.
        bufferIdx = createResource(id);
        // Remember provided parameters.
        appendRequestTicket(getResource(bufferIdx), data, sizeInBytes, desc);
    }

    return { weak_from_this(), bufferIdx };
}

BufferHandle BufferMgr::createImmediateInner(const IdentifierT &id, const void *data, std::size_t sizeInBytes,
    const ::CD3DX12_RESOURCE_DESC &desc)
{
    // Check for previous buffer creation.
    auto bufferIdx{ findResourceIdx(id) };
    if (!BufferHandle::idxValid(bufferIdx))
    { // Buffer with such ID has not been created yet.
        log<Info>() << "Creating a new buffer resource: \"" << id << "\"" << std::endl;
        // Create it.
        bufferIdx = createResource(id);
        // Process the request, immediately creating and filling the buffer.
        processImmediateRequest(bufferIdx, data, sizeInBytes, desc);
    }

    return { weak_from_this(), bufferIdx };
}

BufferMgr::BufferRequest BufferMgr::createRequestTicket(ResourceHolder *res, const void *data, std::size_t sizeInBytes,
    const ::CD3DX12_RESOURCE_DESC &desc)
{ return { res, data, sizeInBytes, desc }; }

void BufferMgr::appendRequestTicket(ResourceHolder *res, const void *data, std::size_t sizeInBytes,
    const ::CD3DX12_RESOURCE_DESC &desc)
{ mBufferRequests.emplace_back(createRequestTicket(res, data, sizeInBytes, desc)); }

void BufferMgr::processRequests()
{
    if (mBufferRequests.empty())
    { // Nothing to do...
        return;
    }

    // Calculate required heap size to create all of the requested resources.
    auto dummyAllocator{ mGpuMemoryMgr->dummyAllocator() };
    for (const auto &request : mBufferRequests)
    { // Accumulate total required heap size.
        if (request.target->refs() != 0u)
        { // Only take requests which are still referenced.
            dummyAllocator->allocate(request.desc, ::D3D12_RESOURCE_STATE_COPY_DEST, nullptr, IID_NULL, nullptr);
        }
    }

    const auto totalBytesRequired{ dummyAllocator->allocated() };
    log<Info>() << "Total size of requests is: " << totalBytesRequired << std::endl;

    // Allocate all of the new resources on one heap.
    auto allocator{ mGpuMemoryMgr->placedAllocator(totalBytesRequired, 
            ::CD3DX12_HEAP_PROPERTIES(::D3D12_HEAP_TYPE_DEFAULT), BUFFER_HEAP_FLAGS) };

    /// Prepare copy command list.
    auto cmdList{ mGpuMemoryMgr->copyCommandList() };

    for (const auto &request : mBufferRequests)
    { // Fulfill the requests.
        if (request.target->refs() != 0u)
        { // Only take requests which are still referenced.
            initializeBuffer(request, allocator, *cmdList);
        }
    }

    // Asynchronously execute the copy commands.
    cmdList->executeNoWait();
}

void BufferMgr::processImmediateRequest(const HandleIdxT &id, const void *data, std::size_t sizeInBytes,
    const ::CD3DX12_RESOURCE_DESC &desc)
{
    // Allocate the buffer on its own heap.
    auto allocator{ mGpuMemoryMgr->committedAllocator(
        ::CD3DX12_HEAP_PROPERTIES(::D3D12_HEAP_TYPE_DEFAULT), BUFFER_HEAP_FLAGS) };

    const auto holderPtr{ getResource(id) };
    ASSERT_FAST(holderPtr);
    auto &holder{ *holderPtr };
    auto &res{ holder.res() };

    // Calculate iterators for the provided data.
    const auto begin{ static_cast<const uint8_t*>(data) };
    const auto end{ begin + sizeInBytes };

    // Create the GPU buffer and upload if necessary.
    res.buffer.allocateUploadImmediate(
        begin, end, desc,
        allocator, 
        ::D3D12_RESOURCE_STATE_COMMON, 
        // Debug name: 
        util::toWString(holder.id()));

    // No data? Skip the copy phase.
    if (data == nullptr || sizeInBytes == 0u)
    { return; }

    // Map and rewrite the buffer memory.
    auto dstPtr{ res.buffer.mapForCpuWrite() };
    { // Fill it with provided data.
        std::memcpy(dstPtr, data, sizeInBytes);
    } res.buffer.unmapFullRewrite();
}

void BufferMgr::initializeBuffer(const BufferRequest &request, 
    helpers::d3d12::D3D12PlacedAllocator::PtrT allocator,
    d3d12::D3D12CommandList &cmdList)
{
    // Obtain the target resource.
    const auto holderPtr{ request.target };
    ASSERT_FAST(holderPtr && !holderPtr->initialized());
    auto &holder{ *holderPtr };
    auto &res{ holder.res() };

    log<Info>() << "Initializing buffer resource: \"" << holder.id() << "\"." << std::endl;

    // Allocate the buffer and set debug name.
    res.buffer.allocate(
        allocator, 
        request.desc, 
        ::D3D12_RESOURCE_STATE_COMMON, 
        // Debug Name: 
        util::toWString(holder.id()));

    // Record commands to upload the data to the GPU.
    mGpuMemoryMgr->uploadData(request.data, request.sizeInBytes, res.buffer, cmdList);

    // We can say that we initialized the buffer.
    holder.markInitialized();
}

BaseHandleMgr<d3d12::D3D12Resource, unsigned short, std::string>::IdentifierT BufferMgr::
generateUniqueId(const void *data, std::size_t sizeInBytes, const ::CD3DX12_RESOURCE_DESC &desc) const
{ return IdentifierT(UNIQUE_ID_BASE) + std::to_string(util::hashAll(data, sizeInBytes, desc)); }

}

}

}
