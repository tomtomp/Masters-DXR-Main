/**
 * @file resources/render/MaterialMgr.cpp
 * @author Tomas Polasek
 * @brief Manager of materials, each material has its properties and textures.
 */

#include "stdafx.h"

#include "engine/resources/render/MaterialMgr.h"

namespace quark
{

namespace res
{

namespace rndr
{

MaterialMgr::MaterialMgr()
{ /* Automatic */ }

MaterialMgr::~MaterialMgr()
{ /* Automatic */ }

MaterialHandle MaterialMgr::create(const IdentifierT &id)
{
    if (id.empty())
    { return createInner(generateUniqueId()); }
    else
    { return createInner(id); }
}

void MaterialMgr::update()
{
    // Process unused resources.
    purgeZeroRefResources();
}

MaterialHandle MaterialMgr::createInner(const IdentifierT &id)
{
    // Check for previous material creation.
    auto materialIdx{ findResourceIdx(id) };
    if (!MaterialHandle::idxValid(materialIdx))
    { // Material with such ID has not been created yet.
        log<Info>() << "Creating a new material resource: \"" << id << "\"" << std::endl;
        // Create it.
        materialIdx = createResource(id);
        // Set as already initialized.
        getResource(materialIdx)->markInitialized();
    }

    return { weak_from_this(), materialIdx };
}

BaseHandleMgr<Material, unsigned short, std::string>::IdentifierT MaterialMgr::generateUniqueId() 
{ return IdentifierT(UNIQUE_ID_BASE) + std::to_string(mUniqueIdCounter++); }

}

}

}
