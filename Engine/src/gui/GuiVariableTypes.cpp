/**
 * @file gui/GuiVariableTypes.cpp
 * @author Tomas Polasek
 * @brief Definitions of variable types usable 
 * as GUI variables.
 */

#include "stdafx.h"

#include "engine/gui/GuiVariableTypes.h"

#include "engine/gui/GuiModel.h"

#include "engine/lib/imgui.h"
#include "engine/lib/dxtk.h"

namespace quark
{

namespace gui
{

void GuiFrame::newFrame()
{ ImGui::NewFrame(); }

void GuiFrame::endFrame()
{ ImGui::EndFrame(); }

GuiContextConfig &GuiContextConfig::setWidth(float val)
{ width = val; return *this; }

GuiContextConfig &GuiContextConfig::setHeight(float val)
{ height = val; return *this; }

GuiContextConfig &GuiContextConfig::setOpened(bool enable)
{ opened = enable; return *this; }

GuiContextConfig &GuiContextConfig::setPosition(float valPosX, float valPosY)
{ posX = valPosX; posY = valPosY; usePosition = true; return *this; }

void GuiContextConfig::startContext(const GuiContext &ctx) 
{
    const auto name{ ctx.mLabel.empty() ? ctx.mName.c_str() : ctx.mLabel.c_str() };

    switch (ctx.mType)
    {
        case GuiContextType::Window:
        { // Solo window: 
            ImGui::SetNextWindowSize(ImVec2{ width, height }, ImGuiCond_Once);

            if (usePosition)
            { ImGui::SetNextWindowPos(ImVec2{ posX, posY }, ImGuiCond_Once); }

            opened = ImGui::Begin(name);
            break;
        }
        case GuiContextType::ChildWindow:
        { // Sub-window: 
            ImGui::PushStyleVar(ImGuiStyleVar_ChildRounding, 5.0f);
            opened = ImGui::BeginChild(name, ImVec2{ width, height }, true);
            ImGui::BeginGroup();
            ImGui::Text(name);
            break;
        }
        case GuiContextType::Category:
        { // Labeled category: 
            opened = ImGui::CollapsingHeader(name, nullptr, ImGuiTreeNodeFlags_Framed);
            break;
        }
        default: 
        { break; }
    }
}

void GuiContextConfig::stopContext(const GuiContext &ctx)
{
    switch (ctx.mType)
    {
        case GuiContextType::Window:
        { // Solo window: 
            ImGui::End();
            break;
        }
        case GuiContextType::ChildWindow:
        { // Sub-window: 
            ImGui::EndGroup();
            ImGui::EndChild();
            ImGui::PopStyleVar();
            break;
        }
        case GuiContextType::Category:
        { // Labeled category: 
            break;
        }
        default: 
        { break; }
    }
}

Color3::Color3(const dxtk::math::Vector3 &v)
{ value.r() = v.x; value.g() = v.y; value.b() = v.z; }

Color3::operator dxtk::math::Vector3()
{ return { value.r(), value.g(), value.b() }; }

void GuiVariableConfig<Color3>::updateCreateVariable(GuiVariable<Color3> &var) 
{
    const auto name{ var.mLabel.empty() ? var.name().c_str() : var.mLabel.c_str() };
    var.mTarget.valueChanged = ImGui::ColorEdit3(name, var.mTarget.value.data);
}

Color4::Color4(const dxtk::math::Vector4 &v)
{ value.r() = v.x; value.g() = v.y; value.b() = v.z; value.a() = v.w; }

Color4::operator dxtk::math::Vector4()
{ return { value.r(), value.g(), value.b(), value.a() }; }

void GuiVariableConfig<Color4>::updateCreateVariable(GuiVariable<Color4> &var) 
{
    const auto name{ var.mLabel.empty() ? var.name().c_str() : var.mLabel.c_str() };
    var.mTarget.valueChanged = ImGui::ColorEdit4(name, var.mTarget.value.data);
}

Position2::Position2(const dxtk::math::Vector2 &v)
{ value.x() = v.x; value.y() = v.y; }

Position2::operator dxtk::math::Vector2()
{ return { value.x(), value.y() }; }

void GuiVariableConfig<Position2>::updateCreateVariable(GuiVariable<Position2> &var) 
{
    const auto name{ var.mLabel.empty() ? var.name().c_str() : var.mLabel.c_str() };
    var.mTarget.valueChanged = ImGui::InputFloat2(name, var.mTarget.value.data);
}

Position3::Position3(const dxtk::math::Vector3 &v)
{ value.x() = v.x; value.y() = v.y; value.z() = v.z; }

Position3::operator dxtk::math::Vector3()
{ return { value.x(), value.y(), value.z() }; }

void GuiVariableConfig<Position3>::updateCreateVariable(GuiVariable<Position3> &var) 
{
    const auto name{ var.mLabel.empty() ? var.name().c_str() : var.mLabel.c_str() };
    var.mTarget.valueChanged = ImGui::InputFloat3(name, var.mTarget.value.data);
}

GuiVariableConfig<float> &GuiVariableConfig<float>::setFormatted(bool enable)
{ formatted = enable; return *this; }

void GuiVariableConfig<float>::updateCreateVariable(GuiVariable<float> &var)
{
    const auto name{ var.mLabel.empty() ? var.name().c_str() : var.mLabel.c_str() };
    if (formatted)
    { ImGui::Text(name, var.mTarget); }
    else
    {
        ImGui::Text(name);
        ImGui::SameLine(15.0f);
        ImGui::Text("%.3f", var.mTarget);
    }
}

GuiVariableConfig<InputFloat> &GuiVariableConfig<InputFloat>::setMin(float val)
{ min = val; return *this; }

GuiVariableConfig<InputFloat> &GuiVariableConfig<InputFloat>::setMax(float val)
{ max = val; return *this; }

GuiVariableConfig<InputFloat> &GuiVariableConfig<InputFloat>::setStep(float val)
{ step = val; return *this; }

GuiVariableConfig<InputFloat> &GuiVariableConfig<InputFloat>::setEditable(bool enable)
{ editable = enable; return *this; }

void GuiVariableConfig<InputFloat>::updateCreateVariable(GuiVariable<InputFloat> &var) 
{
    const auto name{ var.mLabel.empty() ? var.name().c_str() : var.mLabel.c_str() };
    var.mTarget.valueChanged = ImGui::InputFloat(
        name, 
        &var.mTarget.value, 
        step, 0.0f, 
        "%.5f", 
        editable ? 0 : ImGuiInputTextFlags_ReadOnly);
    var.mTarget.value = util::math::clamp(var.mTarget.value, min, max);
}

GuiVariableConfig<uint64_t> &GuiVariableConfig<uint64_t>::setFormatted(bool enable)
{ formatted = enable; return *this; }

void GuiVariableConfig<uint64_t>::updateCreateVariable(GuiVariable<uint64_t> &var)
{
    const auto name{ var.mLabel.empty() ? var.name().c_str() : var.mLabel.c_str() };
    if (formatted)
    { ImGui::Text(name, var.mTarget); }
    else
    {
        ImGui::Text(name);
        ImGui::SameLine(15.0f);
        ImGui::Text("%d", var.mTarget);
    }
}

GuiVariableConfig<InputInt> &GuiVariableConfig<InputInt>::setMin(int val)
{ min = val; return *this; }

GuiVariableConfig<InputInt> &GuiVariableConfig<InputInt>::setMax(int val)
{ max = val; return *this; }

GuiVariableConfig<InputInt> &GuiVariableConfig<InputInt>::setStep(int val)
{ step = val; return *this; }

GuiVariableConfig<InputInt> &GuiVariableConfig<InputInt>::setEditable(bool enable)
{ editable = enable; return *this; }

void GuiVariableConfig<InputInt>::updateCreateVariable(GuiVariable<InputInt> &var)
{
    const auto name{ var.mLabel.empty() ? var.name().c_str() : var.mLabel.c_str() };
    var.mTarget.valueChanged = ImGui::InputInt(
        name, 
        &var.mTarget.value, 
        step, 0, 
        editable ? 0 : ImGuiInputTextFlags_ReadOnly);
    var.mTarget.value = util::math::clamp(var.mTarget.value, min, max);
}

GuiVariableConfig<Enumeration> & GuiVariableConfig<Enumeration>::setItemGetter(
    std::size_t itemCount, ItemGetterFunT fun)
{ mItemCount = itemCount; mItemGetter = fun; return *this; }

void GuiVariableConfig<Enumeration>::updateCreateVariable(GuiVariable<Enumeration> &var) 
{
    const auto name{ var.mLabel.empty() ? var.name().c_str() : var.mLabel.c_str() };
    var.mTarget.valueChanged = ImGui::Combo(
        name, 
        &var.mTarget.value, 
        &itemGetterAdapter, 
        this, 
        static_cast<int>(mItemCount));

    /*
    if (ImGui::BeginCombo(name, getName(var.mTarget.value)))
    {
        for (int iii = 0; iii < mItemCount; ++iii)
        {
            auto isSelected{ var.mTarget.value == iii };
            if (ImGui::Selectable(getName(iii), &isSelected))
            { 
                var.mTarget.value = iii; 
                var.mTarget.valueChanged = true;
            }
            if (isSelected)
            { ImGui::SetItemDefaultFocus(); }
        }
        ImGui::EndCombo();
    }
    */
}

const char *GuiVariableConfig<Enumeration>::getName(int index)
{
    const char *result{ nullptr };
    const auto conversionResult{ mItemGetter(index, &result) };

    return conversionResult ? result : nullptr;
}

bool GuiVariableConfig<Enumeration>::itemGetterAdapter(void *data, int index, const char **outText)
{
    auto *thisPtr{ reinterpret_cast<const GuiVariableConfig<Enumeration>*>(data) };
    return thisPtr->mItemGetter(index, outText);
}

void GuiVariableConfig<std::string>::updateCreateVariable(GuiVariable<std::string> &var)
{
    ImGui::Text(var.mTarget.c_str());
}

void GuiVariableConfig<DynamicText>::updateCreateVariable(GuiVariable<DynamicText> &var)
{
    const auto &textGetter{ var.mTarget.textGetter };

    std::string displayedText{ };
    if (textGetter)
    { displayedText = textGetter(); }

    ImGui::Text(displayedText.c_str());
}

void GraphFloat::pushDataPoint(float point, std::size_t maxValues)
{
    ASSERT_SLOW(maxValues > 0u);

    if (displayedValues != maxValues)
    { // Initialize for this size.
        data.resize(MAX_DATA_LIMIT * maxValues);
        currentOffset = 0u;
        displayedValues = maxValues;
    }

    if (currentOffset >= data.size())
    { // When we reach the limit, move the data down.
        const auto firstToKeep{ data.size() - maxValues };
        std::copy(data.begin() + firstToKeep, data.end(), data.begin());
        currentOffset = maxValues;
    }

    data[currentOffset] = point;
    currentOffset++;
}

GuiVariableConfig<GraphFloat> &GuiVariableConfig<GraphFloat>::setMaxValue(float max)
{ maxValue = max; return *this; }

GuiVariableConfig<GraphFloat> &GuiVariableConfig<GraphFloat>::setMinValue(float min)
{ minValue = min; return *this; }

void GuiVariableConfig<GraphFloat>::updateCreateVariable(GuiVariable<GraphFloat> &var)
{
    const auto name{ var.mLabel.empty() ? var.name().c_str() : var.mLabel.c_str() };

    const auto displayedValues{ var.mTarget.displayedValues };
    auto dataOffset{ var.mTarget.currentOffset };
    if (dataOffset < displayedValues)
    { dataOffset = 0u; }
    else
    { dataOffset -= displayedValues + 1u; }

    ImGui::PlotLines(
        name, 
        var.mTarget.data.data() + dataOffset, 
        static_cast<int>(displayedValues), 
        0, nullptr, 
        minValue, maxValue);
}

void GuiVariableConfig<Button>::updateCreateVariable(GuiVariable<Button> &var)
{
    const auto name{ var.mLabel.empty() ? var.name().c_str() : var.mLabel.c_str() };

    if (ImGui::Button(name))
    { var.mTarget.callbackFun(); }
}

GuiVariableConfig<InputBool> &GuiVariableConfig<InputBool>::setEditable(bool enable)
{ editable = enable; return *this; }

void GuiVariableConfig<InputBool>::updateCreateVariable(GuiVariable<InputBool> &var)
{
    const auto name{ var.mLabel.empty() ? var.name().c_str() : var.mLabel.c_str() };

    var.mTarget.valueChanged = ImGui::Checkbox(name, &var.mTarget.value);
}

GuiVariableConfig<InputString> &GuiVariableConfig<InputString>::setEditable(bool enable)
{ editable = enable; return *this; }

GuiVariableConfig<InputString>& GuiVariableConfig<InputString>::setMaxSize(std::size_t size)
{ mBuffer.resize(size); return *this; }

void GuiVariableConfig<InputString>::updateCreateVariable(GuiVariable<InputString> &var)
{
    const auto name{ var.mLabel.empty() ? var.name().c_str() : var.mLabel.c_str() };

    if (mBuffer.empty())
    { 
        mBuffer.resize(STARTING_MAX_SIZE, 0u); 
        const auto strSize{ std::min(var.mTarget.value.size(), STARTING_MAX_SIZE - 1u) };
        std::copy(var.mTarget.value.begin(), var.mTarget.value.begin() + strSize, mBuffer.begin());
    }

    var.mTarget.valueChanged = ImGui::InputText(
        name, 
        mBuffer.data(), 
        mBuffer.size(), 
        editable ? 0 : ImGuiInputTextFlags_ReadOnly);

    if (var.mTarget.valueChanged)
    { var.mTarget.value.assign(mBuffer.begin(), mBuffer.end()); }
}

}

}
