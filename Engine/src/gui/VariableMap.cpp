/**
 * @file gui/VariableMap.cpp
 * @author Tomas Polasek
 * @brief Named variable storage.
 */

#include "stdafx.h"

#include "engine/gui/VariableMap.h"

namespace quark
{

namespace gui
{

VariableMap::VariableMap()
{ /* Automatic */ }

VariableMap::~VariableMap()
{ /* Automatic */ }

std::string VariableMap::serialize() const
{
    std::stringstream ss;

    /*
    for (const auto &var : mVariables)
    {
    }
    */
    // TODO - Implement

    return ss.str();
}

void VariableMap::deserialize(const std::string &serialized)
{
    // TODO - Implement
    UNUSED(serialized);
}

VariableMap::VariableRecord *VariableMap::varFind(const KeyT &name)
{
    const auto findIt{ mNameMap.find(name) };

    if (findIt != mNameMap.end())
    {
        const auto varIndex{ findIt->second };
        ASSERT_SLOW(varIndex < mVariables.size());
        return &mVariables[varIndex];
    }

    return nullptr;
}

const VariableMap::VariableRecord *VariableMap::varFind(const KeyT &name) const
{ return const_cast<VariableMap*>(this)->varFind(name); }

}

}
