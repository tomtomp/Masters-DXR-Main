/**
 * @file gui/GuiModel.cpp
 * @author Tomas Polasek
 * @brief Model representing the GUI.
 */

#include "stdafx.h"

#include "engine/gui/GuiModel.h"

#include "engine/util/prof/Profiler.h"

namespace quark
{

namespace gui
{

GuiVariableBase::GuiVariableBase(const std::string &name, const std::string &context) :
    mName{ name }, 
    mContext{ context }
{ }

GuiVariableBase::~GuiVariableBase()
{ /* Automatic */ }

bool GuiVariableBase::operator==(GuiVariableBase &other) const
{ return mContext == other.mContext && mName == other.mName ; }

bool GuiVariableBase::operator<(GuiVariableBase &other) const
//{ return mContext < other.mContext && mName < other.mName ; }
{ return mContext <= other.mContext; }

const std::string &GuiVariableBase::name() const
{ return mName; }

const std::string &GuiVariableBase::context() const
{ return mContext; }

GuiContext::GuiContext(const std::string name) :
    mName{ name }
{ }

GuiContext::~GuiContext()
{ /* Automatic */ }

GuiContext & GuiContext::setType(GuiContextType type)
{ mType = type; return *this; }

GuiContext &GuiContext::setLabel(const std::string &label)
{ mLabel = label; return *this; }

const std::string &GuiContext::name() const
{ return mName; }

GuiContextConfig &GuiContext::cfg()
{ return mConfig; }

void GuiContext::startContext() 
{ mConfig.startContext(*this); }

void GuiContext::stopContext() 
{ mConfig.stopContext(*this); }

GuiModel::GuiModel()
{ /* Automatic */ }

GuiModel::~GuiModel()
{ /* Automatic */ }

GuiContext &GuiModel::addContext(const std::string &name)
{
    if (mContexts.find(name) != mContexts.end())
    { throw ContextNameException("Context with given name already exists!"); }
    if (name.empty() || name[0] != CONTEXT_DIVIDER || name[name.length() - 1u] != CONTEXT_DIVIDER)
    { throw ContextNameException("Context name must not be empty and must begin and end with the context division symbol!"); }

    auto newContextPtr{ std::make_unique<GuiContext>(name) };
    auto &newContext{ *newContextPtr };

    mContexts[name] = std::move(newContextPtr);

    return newContext;
}

void GuiModel::updateGui() 
{
    PROF_SCOPE("GuiModelApply");

    if (mVariables.empty())
    { return; }

    /// Stack of active contexts.
    std::stack<GuiContext*> contextStack{ };

    // Star of GUI processing: 
    GuiFrame::newFrame();

    for (const auto &variable : mVariables)
    {
        // TODO - Precompute the order of variables and contexts?
        // Get to the correct context path. Skip variables in closed contexts.
        if (!processContexts(contextStack, variable->context()))
        { continue; }

        // Display/update the variable.
        variable->updateCreateVariable();
    }

    // Empty the stack
    processContexts(contextStack, ROOT_CONTEXT);

    GuiFrame::endFrame();
    // End of GUI processing.
}

bool GuiModel::processContexts(std::stack<GuiContext*> &contexts, const std::string &targetContext) const
{
    auto currentName{ contexts.empty() ? ROOT_CONTEXT : contexts.top()->name() };
    const auto &targetName{ targetContext };

    const auto lcp{ util::longestCommonPrefix(currentName, targetName) };

    if (lcp < currentName.length())
    { // We need to remove some contexts.
        const auto toRemove{ static_cast<std::size_t>(
            std::count(currentName.begin() + lcp, currentName.end(), CONTEXT_DIVIDER)) };
        for (std::size_t iii = 0; iii < toRemove; ++iii)
        { // Remove the contexts.
            contexts.top()->stopContext();
            contexts.pop();
        }

        currentName.erase(currentName.begin() + lcp, currentName.end());
    }

    // We should be at the position of longest common prefix.
    ASSERT_SLOW(util::longestCommonPrefix(currentName, targetName) == currentName.length());

    for (std::size_t iii = currentName.length(); iii < targetName.length() - 1u;)
    { // Add required contexts.
        // Find where the name of the next context ends at.
        const auto nextContextPos{ std::distance(targetName.begin(), std::find(targetName.begin() + iii + 1u, targetName.end(), CONTEXT_DIVIDER)) };

        const auto findIt{ mContexts.find(targetName.substr(0u, nextContextPos + 1u)) };
        ASSERT_SLOW(findIt != mContexts.end());

        auto newContext{ findIt->second.get() };

        newContext->startContext();
        contexts.push(newContext);

        iii = nextContextPos;
    }

    // Variables in root context should not be displayed...
    return contexts.empty() ? false : contexts.top()->cfg().opened;
}

}

}
