/**
 * @file gui/GuiSubSystem.cpp
 * @author Tomas Polasek
 * @brief Graphical user interface sub-system 
 * based on ImGui.
 */

#include "engine/gui/GuiSubSystem.h"

#include "engine/lib/imgui.h"

namespace quark
{

namespace gui
{

GuiSubSystem::PtrT GuiSubSystem::create(const EngineRuntimeConfig &cfg)
{ return PtrT{ new GuiSubSystem(cfg) }; }

GuiSubSystem::~GuiSubSystem()
{ destruct(); }

bool GuiSubSystem::processEvent(const app::events::KeyboardEvent &event)
{
    if (!enabled())
    { return false; }

    ImGuiIO &io{ ::ImGui::GetIO() };

    io.KeyCtrl = event.modPressed(input::KeyMods::Control);
    io.KeyShift = event.modPressed(input::KeyMods::Shift);
    io.KeyAlt = event.modPressed(input::KeyMods::Alt);
    io.KeySuper = false;

    const auto keyCode{ static_cast<uint32_t>(event.key) };

    if (keyCode < util::count(io.KeysDown))
    { io.KeysDown[keyCode] = event.action != input::KeyAction::Release; }

    if (event.characterData)
    { io.AddInputCharacter(event.characterData); }

    return io.WantCaptureKeyboard && mCaptureInput;
}

bool GuiSubSystem::processEvent(const app::events::MouseButtonEvent &event)
{
    if (!enabled())
    { return false; }

    ImGuiIO &io{ ::ImGui::GetIO() };

    uint32_t button{ 0u };

    switch (event.btn)
    {
        case input::MouseButton::LButton:
        { button = 0u; break; }
        case input::MouseButton::MButton:
        { button = 2u; break; }
        case input::MouseButton::RButton:
        { button = 1u; break; }
        default:
        { return false; }
    }

    io.MouseDown[button] = event.action != input::MouseAction::Release;

    return io.WantCaptureMouse && mCaptureInput;
}

bool GuiSubSystem::processEvent(const app::events::MouseMoveEvent &event)
{
    if (!enabled())
    { return false; }

    ImGuiIO &io{ ::ImGui::GetIO() };

    io.MousePos = ImVec2(event.pos.x, event.pos.y);

    return io.WantCaptureMouse && mCaptureInput;
}

bool GuiSubSystem::processEvent(const app::events::MouseScrollEvent &event)
{
    if (!enabled())
    { return false; }

    ImGuiIO &io{ ::ImGui::GetIO() };

    io.MouseWheel = event.delta;

    return io.WantCaptureMouse && mCaptureInput;
}

bool GuiSubSystem::processEvent(const app::events::ResizeEvent &event)
{
    if (!enabled())
    { return false; }

    ImGuiIO &io{ ::ImGui::GetIO() };

    io.DisplaySize = ImVec2(static_cast<float>(event.newWidth), static_cast<float>(event.newHeight));

    // Never capture resizes.
    return false;
}

bool GuiSubSystem::processEvent(const app::events::ExitEvent &event)
{
    UNUSED(event);
    return false;
}

void GuiSubSystem::update(util::HrTimer::MillisecondsF delta)
{
    if (!enabled())
    { return; }
    
    ImGuiIO &io{ ::ImGui::GetIO() };

    // Milliseconds -> seconds.
    io.DeltaTime = delta.count() / 1000.0f;

    mGuiModel.updateGui();
}

bool GuiSubSystem::enabled() const
{ return mEnabled; }

void GuiSubSystem::setEnabled(bool enabled)
{ mEnabled = enabled; }

bool GuiSubSystem::captureInput() const
{ return mCaptureInput; }

void GuiSubSystem::setCaptureInput(bool enabled)
{ mCaptureInput = enabled; }

GuiContext &GuiSubSystem::addGuiContext(const std::string &name, bool useAsRoot)
{
    if (useAsRoot)
    {
        mRootContext = name;
        return mGuiModel.addContext(name);
    }

    return mGuiModel.addContext(rootPrefixedContext(name));
}

bool GuiSubSystem::hasRootContext() const
{ return !mRootContext.empty(); }

const GuiModel &GuiSubSystem::model() const
{ return mGuiModel; }

void GuiSubSystem::initialize()
{
    // Create the main ImGui context.
    IMGUI_CHECKVERSION();
    ImGui::CreateContext();

    ImGuiIO &io{ ::ImGui::GetIO() };

    // Map keys: 
    io.KeyMap[ImGuiKey_Tab] = VK_TAB;
    io.KeyMap[ImGuiKey_LeftArrow] = VK_LEFT;
    io.KeyMap[ImGuiKey_RightArrow] = VK_RIGHT;
    io.KeyMap[ImGuiKey_UpArrow] = VK_UP;
    io.KeyMap[ImGuiKey_DownArrow] = VK_DOWN;
    io.KeyMap[ImGuiKey_PageUp] = VK_PRIOR;
    io.KeyMap[ImGuiKey_PageDown] = VK_NEXT;
    io.KeyMap[ImGuiKey_Home] = VK_HOME;
    io.KeyMap[ImGuiKey_End] = VK_END;
    io.KeyMap[ImGuiKey_Insert] = VK_INSERT;
    io.KeyMap[ImGuiKey_Delete] = VK_DELETE;
    io.KeyMap[ImGuiKey_Backspace] = VK_BACK;
    io.KeyMap[ImGuiKey_Space] = VK_SPACE;
    io.KeyMap[ImGuiKey_Enter] = VK_RETURN;
    io.KeyMap[ImGuiKey_Escape] = VK_ESCAPE;
    io.KeyMap[ImGuiKey_A] = 'A';
    io.KeyMap[ImGuiKey_C] = 'C';
    io.KeyMap[ImGuiKey_V] = 'V';
    io.KeyMap[ImGuiKey_X] = 'X';
    io.KeyMap[ImGuiKey_Y] = 'Y';
    io.KeyMap[ImGuiKey_Z] = 'Z';

    io.ConfigFlags |= ImGuiConfigFlags_NavEnableKeyboard;
}

void GuiSubSystem::destruct()
{
    // And shut down the actual ImGui library.
    ImGui::DestroyContext();
}

std::string GuiSubSystem::rootPrefixedContext(const std::string &contextName)
{
    if (contextName.empty() || mRootContext == contextName)
    { return mRootContext; }
    if (util::longestCommonPrefix(mRootContext, contextName) == mRootContext.length())
    { return contextName; }

    // Remove duplicate context divider.
    return contextName.front() == GuiModel::CONTEXT_DIVIDER ? mRootContext + contextName.substr(1u) : mRootContext + contextName;
}

GuiSubSystem::GuiSubSystem(const EngineRuntimeConfig &cfg) :
    mEnabled{ cfg.guiEnable }, 
    mCaptureInput{ cfg.guiCaptureInput }
{ initialize(); }

}

}
