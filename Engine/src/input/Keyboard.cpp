/**
 * @file input/Keyboard.cpp
 * @author Tomas Polasek
 * @brief Keyboard handling class.
 */

#include "stdafx.h"

#include "engine/input/Keyboard.h"

namespace quark
{

namespace input
{

Keyboard *Keyboard::sHookKeyboard{ nullptr };

Keyboard::~Keyboard()
{
    if (this == sHookKeyboard)
    {
        resetCallback();
    }
}

void Keyboard::setCallback(HINSTANCE hInstance, AppCallbackT appCallback)
{
    resetCallback();

    mCallbackHook = SetWindowsHookEx(WH_KEYBOARD, callback(), hInstance, 0u);
    if (!mCallbackHook)
    {
        throw util::winexception("Unable to set callback hook!");
    }

    sHookKeyboard = this;
    mCallback = appCallback;
}

void Keyboard::resetCallback()
{
    if (sHookKeyboard)
    {
        sHookKeyboard->resetHook();
        sHookKeyboard = nullptr;
    }
}

LRESULT Keyboard::keyboardCallbackDispatch(int code, WPARAM wParam, LPARAM lParam) noexcept 
{
    if (code < 0 || code == HC_NOREMOVE || code < WM_KEYFIRST || code > WM_KEYLAST)
    { // Pass through, or message we are not interested in.
        return CallNextHookEx(nullptr, code, wParam, lParam);
    }

    // Process the callback with active keyboard.
    const auto event { processEventInner(code, wParam, lParam) };
    if (event && sHookKeyboard->mCallback)
    { // If the event is keyboard event.
        sHookKeyboard->mCallback(event);
    }

    // Continue the hook chain.
    return CallNextHookEx(nullptr, code, wParam, lParam);
}

void Keyboard::resetHook()
{
    if (mCallbackHook)
    {
        UnhookWindowsHookEx(mCallbackHook);
        mCallbackHook = nullptr;
        mCallback = nullptr;
    }
}

KeyboardEvent Keyboard::processEventInner(int messageCode, WPARAM wParam, LPARAM lParam)
{
    switch (messageCode)
    {
    case WM_KEYDOWN:
    case WM_KEYUP:
    case WM_SYSKEYDOWN:
    case WM_SYSKEYUP:
    case WM_CHAR: 
        { // Extract keycode, scan-code and action.
            // Virtual keycode.
            const auto key{ static_cast<Key>(wParam) };
            // Hardware dependent scan code.
            const auto scanCode{ scanCodeFromLParam(lParam) };
            // Get current modifiers from the keyboard.
            const auto mods{ currentModifiers() };
            // Action that happened.
            const auto action{ actionFromLParam(lParam) };

            char characterData{ 0u };
            if (messageCode == WM_CHAR)
            { characterData = static_cast<char>(wParam); }

            // TODO - Check character data.
            /*
            MSG charMsg;
            unsigned int c = 0;
            if (PeekMessage(&charMsg, nullptr, 0, 0, PM_NOREMOVE) && charMsg.message == WM_CHAR)
            {
                GetMessage(&charMsg, nullptr, 0, 0);
                c = static_cast<unsigned int>(charMsg.wParam);
                util::dout << "Found char: '" << c << "'" << std::endl;
            }
            */

            return { key, characterData, scanCode, mods, action };
        }
    default:
        { // Not interested.
            return { };
        }
    }
}

}

}
