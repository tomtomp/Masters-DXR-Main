/**
 * @file EngineRuntimeConfig.cpp
 * @author Tomas Polasek
 * @brief Runtime configuration of the engine.
 */

#include "stdafx.h"

#include "EngineRuntimeConfig.h"

namespace quark
{

EngineRuntimeConfig::EngineRuntimeConfig()
{
    mParser.addOption(L"--help", [&] ()
    {
        metaDisplayHelp = true;
    }, L"Display this help menu.");

    mParser.addOption(L"--warp", [&] ()
    {
        adapterUseWarp = true;
    }, L"Use Windows Advanced Rendering Platform adapter, instead of a physical one.");

    mParser.addOption<uint32_t>(L"--width", [&] (const uint32_t &val)
    {
        windowWidth = val;
    }, L"Starting width of the window, value should be larger or equal to 1.", 
        [] (const uint32_t val)
    {
        return val >= 1;
    });

    mParser.addOption<uint32_t>(L"--height", [&] (const uint32_t &val)
    {
        windowHeight = val;
    }, L"Starting height of the window, value should be larger or equal to 1.", 
        [] (const uint32_t &val) 
    {
        return val >= 1;
    });

    mParser.addOption<std::wstring>(L"--title", [&] (const std::wstring &val)
    {
        windowTitle = val;
    }, L"Main window title.");

    mParser.addOption<std::uint32_t>(L"--back-buffers", [&] (const uint32_t &val)
    {
        swapChainNumBackBuffers = val;
    }, L"Number of back buffers in the main swap chain. "
        "Includes the one which is currently being displayed.", 
        [] (const uint32_t &val)
    {
        if (val < 2u)
        {
            throw util::OptionParser::ParsingException(L"Number of back buffers must be greater or equal to 2!");
        }
        return true;
    });

    mParser.addOption(L"--wire-frame", [&] ()
    {
        renderRasterizerFill = false;
    }, L"Set wire-frame mode for rasterization modes.");
    mParser.addOption(L"--flip-triangles", [&] ()
    {
        renderRasterizerFrontClockwise = false;
    }, L"Set front-facing triangles to be counter clock-wise.");
    mParser.addOption(L"--fast-build-as", [&] ()
    {
        renderRayTracingFastBuildAS = true;
    }, L"Prefer speed when building the acceleration structures. Lowers ray "
       L"tracing performance!");

    mParser.addOption<std::uint32_t>(L"--fps", [&] (const uint32_t &val)
    {
        appTargetFps = val;
    }, L"Target frames per second for the application, when set to "
        "zero, the fps is unlimited.");

    mParser.addOption<std::uint32_t>(L"--ups", [&](const uint32_t &val)
    {
        appTargetUps = val;
    }, L"Target updates per second for the application, must be greater than 0!",
        [](const uint32_t &val) { return val > 0u; });

    mParser.addOption<bool>(L"--vsync", [&] (const bool &val)
    {
        appUseVSync = val;
    }, L"Should the application try to synchronize presentation with "
        "monitor vertical synchronization?");

    mParser.addOption<bool>(L"--tearing", [&] (const bool &val)
    {
        appUseTearing = val;
    }, L"Should the application attempt to present unlimited amount "
        "of frames, independent on monitor refresh rate? works only "
        "if vSync is also set to false!");

    mParser.addOption<bool>(L"--gui-enable", [&] (const bool &val)
    {
        guiEnable = val;
    }, L"Should the GUI be displayed when the application starts?");

    mParser.addOption<bool>(L"--gui-capture-input", [&] (const bool &val)
    {
        guiCaptureInput = val;
    }, L"Should the user input on GUI be captured?");

    mParser.addOption<float>(L"--gui-width", [&] (const float &val)
    {
        guiWidth = val;
    }, L"Width of the main GUI window.");

#ifdef _DEBUG
    mParser.addOption<bool>(L"--debug-layer", [&] (const bool &val)
    {
        debugEnableLayer = val;
    }, L"Should the D3D12 debugging layer be enabled?");

    mParser.addOption<bool>(L"--gpu-validation", [&] (const bool &val)
    {
        debugGpuValidation = val;
    }, L"Should the debugging function, GPU-based validation be enabled?\n"
        "WARNING: May cause memory leaks!");

    mParser.addOption<bool>(L"--live-reporting", [&](const bool &val)
    {
        debugReportLiveObjects = val;
    }, L"Should live objects be reported, when shutting down the application?");

    mParser.addOption(L"--enable-info-messages", [&] ()
    {
        deviceDisableMsgSeverities.erase(D3D12_MESSAGE_SEVERITY_INFO);
    }, L"If the application has enabled DirectX debug layer, this flag enables "
        "printing of info messages - i.e. creation and destruction of objects.");
#endif
}

void EngineRuntimeConfig::setParseCmdArgs()
{
    int argc{ 0u };
    LPWSTR *argv = ::CommandLineToArgvW(::GetCommandLineW(), &argc);

    try
    {
        setParseCmdArgs(argv, argc);
    } catch (...)
    {
        ::LocalFree(argv);
        throw;
    }

    ::LocalFree(argv);
}

void EngineRuntimeConfig::setParseCmdArgs(const wchar_t * const * const argv, int argc)
{
    // Save the parameters.
    std::stringstream ss;
    for (int iii = 0; iii < argc; ++iii)
    {
        ss << util::toString(argv[iii]) << " ";
    }
    commandLineParameters = ss.str();

    // And parse them.
    mParser.parse(argv, argc);
}

} 
