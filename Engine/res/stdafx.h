/**
 * @file res/stdafx.h
 * @author Tomas Polasek
 * @brief Pre-compiled header containing common standard 
 * headers and some engine headers.
 */

#pragma once

#include "targetver.h"

// Windows Header Files:
#include "engine/util/SafeWindows.h"

// D3D12 + DXGI headers: 
#include "engine/util/SafeD3D12.h"

// D3D12 extension library: 
#include "engine/lib/d3dx12.h"

// App specific headers: 
#include "engine/util/Types.h"
