/**
 * @file res/stdafx.cpp
 * @author Tomas Polasek
 * @brief Pre-compiled source containing common standard 
 * headers and some engine headers.
 */

#include "stdafx.h"
