/**
* @file Sandbox.h
* @author Tomas Polasek
* @brief Main application header.
*/

#pragma once

#include "Engine.h"
#include "engine/application/Application.h"

#include "engine/input/KeyBinding.h"

/// Namespace containing the project.
namespace dxr
{

/// Application specific options should be added here.
class AppSpecificConfig : public quark::EngineRuntimeConfig
{
public:
    AppSpecificConfig();

    // Start of options section

    /// Periodically print ou profiler information.
    bool specificPrintProfiler{ false };

    // End of options section
}; // class AppSpecificRuntimeConfig

/// Main application class.
class App : public quark::app::Application<App, AppSpecificConfig>
{
public:
    /// Constructor called by the engine.
    App(ConfigT &cfg, quark::Engine &engine);

    virtual ~App() = default;
    virtual void initialize() override final;
    //virtual void destroy() override final;
    virtual int32_t run() override final;
    virtual void update(Milliseconds delta) override final;
    virtual void render() override final;
    //virtual void processEvents() override final;

    void onEvent(const KeyboardEvent &event) const;
    void onEvent(const MouseButtonEvent &event) const;
    void onEvent(const MouseMoveEvent &event) const;
    void onEvent(const MouseScrollEvent &event) const;
    void onEvent(const ResizeEvent &event);
    void onEvent(const ExitEvent &event);
private:
    /// Main binding scheme.
    quark::input::KeyBinding mBinding;

    /// ImGUI testing window.
    bool mShowDemoWindow{ true };
protected:
}; // class App
    
} // namespace App
