What is This?
=============

This repository contains project made as a part of my Master's named "Hybrid Ray Tracing in DXR" at 
Brno University of Technology, Faculty of Information Technology. The goal of this thesis has been 
testing of hardware accelerated ray tracing, using DirectX Ray Tracing and NVIDIA Turing GPUs, with 
the task of determining whether this technology is ready for use in rendering engines.

Getting Started
===============

In order to get the whole Master's thesis package, please see following link: 
```
    https://drive.google.com/drive/folders/1PZaZUcH1elp3nXUJloC4Tj_kBIexR3ps
```
After downloading the zip archive you can unpack it and see README.txt or 
README_EN.txt for information about its contents.

Content
=======

The directory should contain: 
 * **Apps/** : Applications made using the Quark engine.
 * **DX12-Tutorial/** : Samples made during the creation of the Quark engine.
 * **Engine/** : Folder containing the Quark engine itself.
 * **EngineTests/** : Folder containing Quark engine unit tests.
 * **Lib/** : Libraries common to the engine and applications.
 * **Models/** : Folder containing testing models.
 * **Packages/** : Automatic library packages used by the project.
 * **Prof/** : Automated and manual testing utilities and results.
 * **Res/** : Additional resources.
 * **Sandbox/** : Simple sandbox project used for testing.
 * **Masters-DXR-Main.sln** : Main project solution.
 * **README.md** : Contains this text.
 
How to Build
============

In order to build this project, it should be as simple as cloning the main repository to get 
its content using following command. Note that if you followed **Getting Started** section 
you should already have everything necessary and can skip cloning the repository: 
```
    git clone --recurse-submodules -b Integration https://gitlab.com/tomtomp/Masters-DXR-Main Masters-DXR-Main
```
After the above command finishes pulling all of the required files, the main solution file 
**Masters-DXR-Main.sln** can be opened. The project should now be build-able in **Release** 
or **Debug** configuration.
After the build process completes, the results will be contained within the **build/Measurement/** 
folder. The resulting application can be executed straight from this folder, which should already 
contain all necessary dynamic libraries and configuration files.

For more information about how to control the application you can use the provided manual 
which is contained in the archive gained by following the **Getting Started** section.
