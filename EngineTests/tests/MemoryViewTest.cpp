/**
* @file MemoryViewTest.cpp
* @author Tomas Polasek
* @brief MemoryView unit testing.
*/

#include "stdafx.h"

#include "CatchUtils.h"

#include "engine/util/MemoryView.h"

using namespace util;

TEST_CASE("MemoryView testing", "[MemoryView]")
{
    SECTION("Empty construction")
    {
        REQUIRE_NOTHROW(MemoryView<int>());
    }

    SECTION("Nullptr construction")
    {
        int *myMemory{ nullptr };
        MemoryView<int> mw(myMemory, 42u);
        REQUIRE(!mw);
        REQUIRE(mw.size() == 0u);
        REQUIRE(mw.begin() == mw.end());
        REQUIRE(mw.cbegin() == mw.cend());
    }

    SECTION("Memory construction")
    {
        static constexpr std::size_t MEMORY_SIZE{ 42u };
        std::array<unsigned int, MEMORY_SIZE> mem;
        MemoryView<unsigned int> mw(mem.data(), MEMORY_SIZE);
        REQUIRE(mw);
        REQUIRE(mw.size() == MEMORY_SIZE);
        REQUIRE(mw.begin() + MEMORY_SIZE == mw.end());
        REQUIRE(mw.cbegin() + MEMORY_SIZE == mw.cend());

        static constexpr unsigned int TEST_VALUE{ 0xDEADBEEF };
        for (auto &element : mw)
        { element = TEST_VALUE; }

        const MemoryView<unsigned int> &cmw{ mw };
        for (const auto &element : cmw)
        { REQUIRE(element == TEST_VALUE); }
    }

    SECTION("Copy construction")
    {
        static constexpr std::size_t MEMORY_SIZE{ 42u };
        std::array<unsigned int, MEMORY_SIZE> mem;
        MemoryView<unsigned int> mw(mem.data(), MEMORY_SIZE);
        MemoryView<unsigned int> mwc(mw);

        REQUIRE(mw);
        REQUIRE(mw.size() == MEMORY_SIZE);
        REQUIRE(mw.begin() + MEMORY_SIZE == mw.end());
        REQUIRE(mw.cbegin() + MEMORY_SIZE == mw.cend());

        REQUIRE(mwc);
        REQUIRE(mwc.size() == MEMORY_SIZE);
        REQUIRE(mwc.begin() + MEMORY_SIZE == mwc.end());
        REQUIRE(mwc.cbegin() + MEMORY_SIZE == mwc.cend());

        REQUIRE(mw.begin() == mwc.begin());
        REQUIRE(mw.end() == mwc.end());
        REQUIRE(mw.cbegin() == mwc.cbegin());
        REQUIRE(mw.cend() == mwc.cend());

        static constexpr unsigned int TEST_VALUE{ 0xDEADBEEF };
        for (auto &element : mw)
        { element = TEST_VALUE; }

        for (const auto &element : mwc)
        { REQUIRE(element == TEST_VALUE); }
    }

    SECTION("Move construction")
    {
        static constexpr std::size_t MEMORY_SIZE{ 42u };
        std::array<unsigned int, MEMORY_SIZE> mem;
        MemoryView<unsigned int> mw(mem.data(), MEMORY_SIZE);
        MemoryView<unsigned int> mwc(std::move(mw));

        REQUIRE(mw);
        REQUIRE(mw.size() == MEMORY_SIZE);
        REQUIRE(mw.begin() + MEMORY_SIZE == mw.end());
        REQUIRE(mw.cbegin() + MEMORY_SIZE == mw.cend());

        REQUIRE(mwc);
        REQUIRE(mwc.size() == MEMORY_SIZE);
        REQUIRE(mwc.begin() + MEMORY_SIZE == mwc.end());
        REQUIRE(mwc.cbegin() + MEMORY_SIZE == mwc.cend());
    }

    SECTION("Accessing")
    {
        static constexpr std::size_t MEMORY_SIZE{ 42u };
        std::array<unsigned int, MEMORY_SIZE> mem;
        MemoryView<unsigned int> mw(mem.data(), MEMORY_SIZE);

        static constexpr unsigned int TEST_VALUE{ 0xDEADBEEF };
        for (std::size_t iii = 0; iii < MEMORY_SIZE; ++iii)
        { mw[iii] = TEST_VALUE; }

        for (std::size_t iii = 0; iii < MEMORY_SIZE; ++iii)
        { REQUIRE(mw.at(iii) == TEST_VALUE); }

        REQUIRE_THROWS_AS(mw.at(MEMORY_SIZE), std::out_of_range);
    }
}
