/**
* @file ThreadPoolTest.cpp
* @author Tomas Polasek
* @brief ThreadPool unit testing.
*/

#include "stdafx.h"

#include "CatchUtils.h"

#include "engine/util/Threading.h"
#include "engine/resources/ThreadPool.h"

using namespace ::quark::res::thread;

TEST_CASE("Thread pool", "[ThreadPool]")
{
    SECTION("Initialization")
    {
        REQUIRE_NOTHROW(ThreadPool::create());

        static constexpr std::array<const char*, 5u> NAMES1{ "Thread 1", "Thread 2", "Thread 3", "Thread 4", "Thread 5" };
        {
            auto tp{ ThreadPool::create(NAMES1) };
            tp->joinAll();
        }

        static const std::vector<const char*> NAMES2{ NAMES1.begin(), NAMES1.end() };
        {
            auto tp{ ThreadPool::create(5u, NAMES2) };
            tp->joinAll();
        }

        REQUIRE_THROWS_AS(ThreadPool::create(6u, NAMES2), std::runtime_error);
    }

    SECTION("Recurrent initialization/destruction")
    {
        static constexpr std::array<const char*, 5u> NAMES1{ "Thread 1", "Thread 2", "Thread 3", "Thread 4", "Thread 5" };
        for (uint32_t iii = 0; iii < 10u; ++iii)
        {
            auto tp{ ThreadPool::create(NAMES1) };
            tp->joinAll();
        }
    }

    SECTION("Start and stop")
    {
        static constexpr std::array<const char*, 5u> NAMES{ "Thread 1", "Thread 2", "Thread 3", "Thread 4", "Thread 5" };
        {
            auto tp{ ThreadPool::create(NAMES) };
            tp->waitAll();

            tp->joinAll();
            tp->waitAll();

            tp->joinAll(false);
            tp->joinAll();

            tp->waitAll();
            tp->joinAll(false);
        }
    }

    SECTION("Work")
    {
        static constexpr uint32_t NUM_WORKERS{ 3u };
        auto tp{ ThreadPool::create() };
        tp->addWorker("First");
        tp->addWorker("Second");
        tp->addWorker("Third");
        tp->waitAll();

        REQUIRE(tp->numWorkers() == NUM_WORKERS);
        REQUIRE(tp->hasWorkers());
        REQUIRE(tp->numIdleWorkers() == NUM_WORKERS);
        REQUIRE(tp->hasIdleWorkers());

        {
            auto result1 = tp->addTask([]() { return 42u; });
            REQUIRE(result1.valid());

            auto result2 = tp->addTask([]() { return 1337u; });
            REQUIRE(result2.valid());

            auto result3 = tp->addTask([]() { return 0xDEAD; });
            REQUIRE(result3.valid());

            REQUIRE(result1.get() == 42u);
            REQUIRE(result2.get() == 1337u);
            REQUIRE(result3.get() == 0xDEAD);
        }

        {
            auto flag{ false };
            auto voidWait = tp->addTask([&]()
            {
                std::this_thread::sleep_for(std::chrono::milliseconds(15u));

                flag = true;
            });

            voidWait.get();

            REQUIRE(flag);
        }

        {
            static constexpr uint32_t BARRIER_SIZE{ NUM_WORKERS };

            util::thread::Barrier b(BARRIER_SIZE + 1u);

            for (uint32_t iii = 0; iii < BARRIER_SIZE; ++iii)
            {
                tp->addTask([&b]() { b.wait(); });
            }

            b.wait();

            REQUIRE(true);
        }

        tp->joinAll();
    }

    SECTION("Random work")
    {
        static constexpr uint32_t NUM_TASKS{ 100u };
        static constexpr uint32_t MAX_WAIT_MS{ 5u };
        auto tp{ ThreadPool::create() };

        std::vector<std::future<void>> futures;

        for (uint32_t iii = 0; iii < NUM_TASKS; ++iii)
        {
            const auto waitTime{ static_cast<int>(static_cast<float>(std::rand()) / RAND_MAX * MAX_WAIT_MS) };
            tp->addTask([](int waitTime) { std::this_thread::sleep_for(std::chrono::milliseconds(waitTime)); }, waitTime);
        }

        tp->joinAll();
    }

    SECTION("Exceptions")
    {
        static const std::string firstMessage{ "First" };
        static const std::wstring secondMessage{ L"Second" };

        auto tp{ ThreadPool::create() };

        tp->addWorker("ExceptionalWorker");

        {
            auto future1{ tp->addTask([&]()
            {
                throw std::runtime_error(firstMessage);
            }) };

            auto future2{ tp->addTask([&]()
            {
                throw util::wruntime_error(secondMessage);
            }) };

            auto caught1{ false };
            try
            {
                future1.get();
            }
            catch (std::exception &err)
            {
                caught1 = true;
                REQUIRE(err.what() == firstMessage);
            }

            auto caught2{ false };
            try
            {
                future2.get();
            }
            catch (util::wexception &err)
            {
                caught2 = true;
                REQUIRE(std::string(err.what()) == util::toString(secondMessage));
                REQUIRE(err.wwhat() == secondMessage);
            }

            REQUIRE(caught1);
            REQUIRE(caught2);
        }

        {
            auto future1{ tp->addSpecializedWorker("First", [&]()
            {
                throw std::runtime_error(firstMessage);
            }) };

            auto future2{ tp->addSpecializedWorker("Second", [&]()
            {
                throw util::wruntime_error(secondMessage);
            }) };

            future1.wait();
            future2.wait();
        }

        REQUIRE(tp->hasExceptions());

        const auto exceptions{ tp->fetchExceptions() };
        REQUIRE(exceptions.size() == 2u);

        auto first{ std::string(exceptions[0].threadName) == "First" ? exceptions[0] : exceptions[1] };
        auto second{ std::string(exceptions[0].threadName) == "Second" ? exceptions[0] : exceptions[1] };

        REQUIRE(std::string(first.threadName) == "First");
        REQUIRE(first.err.what() == firstMessage);
        REQUIRE(std::wstring(first.err.wwhat()) == util::toWString(firstMessage));

        REQUIRE(std::string(second.threadName) == "Second");
        REQUIRE(std::string(second.err.what()) == util::toString(secondMessage));
        REQUIRE(second.err.wwhat() == secondMessage);

        tp->joinAll();
    }

    SECTION("Specialized workers")
    {
        auto tp{ ThreadPool::create() };

        tp->addSpecializedWorker("First", [] ()
        {
            return;
        });

        util::thread::Barrier b(2u);

        tp->addSpecializedWorker("Second", [&] ()
        {
            b.wait();
        });

        b.wait();

        tp->joinAll();
    }
}
