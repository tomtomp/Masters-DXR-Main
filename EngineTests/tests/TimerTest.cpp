/**
* @file TimerTest.cpp
* @author Tomas Polasek
* @brief Timer unit testing.
*/

#include "stdafx.h"

#include "CatchUtils.h"

#include "engine/util/Timer.h"

using namespace util;

TEST_CASE("Timer", "[Timer]")
{
    using Milliseconds = HrTimer::MillisecondsF;
    static constexpr uint32_t SLEEP_FOR_MS{ 5u };

    SECTION("Timer")
    {
        HrTimer t; t.reset();
        std::this_thread::sleep_for(HrTimer::Milliseconds(SLEEP_FOR_MS));
        REQUIRE(t.elapsedResetI<HrTimer::Milliseconds>() >= SLEEP_FOR_MS);
        REQUIRE(t.elapsedResetI<HrTimer::Milliseconds>() < SLEEP_FOR_MS);

        std::this_thread::sleep_for(HrTimer::Milliseconds(SLEEP_FOR_MS));
        REQUIRE(t.elapsedReset<HrTimer::MillisecondsF>() >= HrTimer::MillisecondsF(SLEEP_FOR_MS));
        REQUIRE(t.elapsedReset<HrTimer::MillisecondsF>() < HrTimer::MillisecondsF(SLEEP_FOR_MS));
    }

    SECTION("TickTimer")
    {
        uint32_t numCalled{ 0u };
        TickTimer tt(Milliseconds(SLEEP_FOR_MS), [&]() { numCalled++; });
        HrTimer t;

        std::this_thread::sleep_for(HrTimer::Milliseconds(SLEEP_FOR_MS));
        tt.tickUsing(t);

        REQUIRE(numCalled == 1u);
    }

    SECTION("TickDeltaTimer")
    {
        uint32_t numCalled{ 0u };
        TickDeltaTimer<HrTimer::Milliseconds> tdt(Milliseconds(SLEEP_FOR_MS), [&](auto dur) { numCalled++; });
        HrTimer t;

        std::this_thread::sleep_for(HrTimer::Milliseconds(SLEEP_FOR_MS));
        tdt.tickUsing(t);

        REQUIRE(numCalled == 1u);
    }

    SECTION("DeltaTimer")
    {
        DeltaTimer dt;

        std::this_thread::sleep_for(HrTimer::Milliseconds(SLEEP_FOR_MS + 1u));
        const auto startValue{ dt.tick<Milliseconds>() };

        REQUIRE(startValue >= HrTimer::Milliseconds(SLEEP_FOR_MS));

        const auto changedValue{ startValue - Milliseconds(SLEEP_FOR_MS) };
        REQUIRE(dt.take<>(Milliseconds(SLEEP_FOR_MS)).count() == Approx(changedValue.count()));

        const auto largerValue{ changedValue + Milliseconds(1u) };
        REQUIRE_THROWS(dt.take<>(Milliseconds(largerValue)));
        REQUIRE(dt.remaining<Milliseconds>().count() == Approx(changedValue.count()));

        dt.tick<Milliseconds>();
        dt.takeAll<Milliseconds>();
        REQUIRE(dt.remainingI<Milliseconds>() == 0);
    }
}
