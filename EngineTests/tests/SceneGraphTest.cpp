/**
* @file SceneGraphTest.cpp
* @author Tomas Polasek
* @brief SceneGraph unit testing.
*/

#include "stdafx.h"

#include "CatchUtils.h"

#include "engine/util/SceneGraph.h"

using namespace util;

struct DownPropagationDummy : util::DownPropagateBase
{
    void propagateFrom(const DownPropagationDummy &other)
    { max = std::max(max, other.max); }

    void defaultPropagate()
    { }

    int max{ 0 };
};

struct UpPropagationDummy : util::UpPropagateBase
{
    void propagateFrom(const UpPropagationDummy &other)
    { max = std::max(max, other.max); }

    void defaultPropagate()
    { }

    int max{ 0 };
};

namespace DummyData
{
    static constexpr std::size_t TOP_LEVEL_NODES{ 1u };

    static constexpr std::size_t GRAPH_HEIGHT{ 2u };
    static constexpr std::size_t NUM_CHILDREN{ 3u };
}

template <typename T, typename AllocatorT>
using SceneGraphWAT = SceneGraph<T, DownPropagateBase*, UpPropagateBase*, AllocatorT>;

/// Recursively create the children nodes.
template <typename AllocatorT>
void createChildren(typename SceneGraphWAT<int, AllocatorT>::iterator parent, SceneGraph<int> &sc, int &idCounter, 
    std::size_t numChildren, std::size_t currentHeight, std::size_t targetHeight)
{
    if (currentHeight >= targetHeight)
    {
        return;
    }

    for (auto iii = 0u; iii < numChildren; ++iii)
    {
        auto cNode{ sc.insert(idCounter++) };
        REQUIRE(cNode.second);

        log<Debug>() << sc << std::endl;
        REQUIRE_NOTHROW(sc.makeParentOf(parent, cNode.first));

        log<Debug>() << sc << std::endl;
        createChildren<AllocatorT>(cNode.first, sc, idCounter, numChildren, currentHeight + 1u, targetHeight);
    }
}

/// Fill scene graph with dummy data.
template <typename AllocatorT>
void fillWithData(SceneGraphWAT<int, AllocatorT> &sc)
{
    sc.clear();
    int idCounter{ 0u };

    for (auto iii = 0u; iii < DummyData::TOP_LEVEL_NODES; ++iii)
    {
        auto tlNode{ sc.insert(idCounter++) };
        REQUIRE(tlNode.second);
        REQUIRE(tlNode.first->id == idCounter - 1u);

        createChildren<AllocatorT>(tlNode.first, sc, idCounter, 
            DummyData::NUM_CHILDREN, 1u, DummyData::GRAPH_HEIGHT);
        log<Debug>() << sc << std::endl;
    }
}

/// Recursively check children nodes.
template <typename AllocatorT>
void checkChildren(typename SceneGraphWAT<int, AllocatorT>::iterator parent, SceneGraph<int> &sc, int &idCounter, 
    std::size_t numChildren, std::size_t currentHeight, std::size_t targetHeight)
{
    if (currentHeight >= targetHeight + 1u)
    {
        return;
    }

    auto tlBegin{ parent.chldbegin() };
    auto tlEnd{ parent.chldend() };

    if (currentHeight < targetHeight)
    { // Check parents.
        REQUIRE((tlEnd - tlBegin) == numChildren);

        for (auto it = tlBegin; it != tlEnd; ++it)
        { 
            REQUIRE(it->id == idCounter++);
            //SceneGraph<int>::iterator lit{ it };
            REQUIRE(sc.isParentOf(parent, it));
            REQUIRE(sc.isIndirectParentOf(parent, it));
            checkChildren<AllocatorT>(it, sc, idCounter, DummyData::NUM_CHILDREN, currentHeight + 1u, DummyData::GRAPH_HEIGHT);
        }
    }
    else
    { // Check leaves.
        REQUIRE((tlEnd - tlBegin) == 0u);
    }
}

static std::size_t expectedNumNodes(std::size_t topLevelNodes, std::size_t numChildren, std::size_t height)
{ return static_cast<std::size_t>(DummyData::TOP_LEVEL_NODES + DummyData::TOP_LEVEL_NODES * std::pow<std::size_t>(DummyData::NUM_CHILDREN, DummyData::GRAPH_HEIGHT - 1u)); }

/// Check data created with the fillWithData.
template <typename AllocatorT>
void checkData(SceneGraphWAT<int, AllocatorT> &sc)
{
    static const std::size_t EXPECTED_NUM_NODES{ expectedNumNodes(DummyData::TOP_LEVEL_NODES, DummyData::NUM_CHILDREN, DummyData::GRAPH_HEIGHT) };
    int idCounter{ 0u };

    auto tlBegin{ sc.tlbegin() };
    auto tlEnd{ sc.tlend() };
    REQUIRE((tlEnd - tlBegin) == DummyData::TOP_LEVEL_NODES);
    log<Debug>() << sc << std::endl;
    REQUIRE(sc.size() == EXPECTED_NUM_NODES);

    for (auto it = tlBegin; it != tlEnd; ++it)
    { 
        REQUIRE(it->id == idCounter++);
        //SceneGraph<int>::iterator lit{ it };
        checkChildren<AllocatorT>(it, sc, idCounter, DummyData::NUM_CHILDREN, 1u, DummyData::GRAPH_HEIGHT);
    }

    idCounter = 0;
    for (const auto node: sc)
    { idCounter++; }
    REQUIRE(idCounter == EXPECTED_NUM_NODES);
}

TEST_CASE("SceneGraph testing", "[SceneGraph]")
{
    SECTION("Construction")
    {
        {
            SceneGraph<int> sc;
            static_assert(!decltype(sc)::NodeDataT::HasDownPropagate, 
                "No DownPropagateT specified...");
            static_assert(!decltype(sc)::NodeDataT::HasUpPropagate, 
                "No UpPropagateT specified...");
        }

        {
            SceneGraph<int, DownPropagationDummy*, UpPropagationDummy*> sc;
            static_assert(decltype(sc)::NodeDataT::HasDownPropagate, 
                "DownPropagateT has been specified...");
            static_assert(decltype(sc)::NodeDataT::HasUpPropagate, 
                "UpPropagateT has been specified...");
        }

        {
            SceneGraph<int> sc1;
            SceneGraph<int> sc2(sc1);
            SceneGraph<int> sc3 = sc1;
            sc3 = sc2;
            {
                using std::swap;
                swap(sc1, sc2);
            }
            SceneGraph<int> sc4(std::move(sc1));
            sc4 = std::move(sc1);
        }
    }

    SECTION("Insertion, status methods")
    {
        static constexpr int INSERTED_KEY{ 1 };
        SceneGraph<int, DownPropagationDummy*, UpPropagationDummy*> sc;

        REQUIRE(sc.empty());
        REQUIRE(sc.begin() == sc.end());
        REQUIRE(sc.cbegin() == sc.cend());
        REQUIRE(!sc.contains(INSERTED_KEY));
        REQUIRE(sc.find(INSERTED_KEY) == sc.end());
        REQUIRE(sc.size() == 0u);
        REQUIRE(!sc);
        REQUIRE(!sc.erase(INSERTED_KEY));

        DownPropagationDummy dpd1;
        UpPropagationDummy upd1;
        decltype(sc.insert(INSERTED_KEY, { &dpd1, &upd1 })) res1;
        decltype(sc.insert(INSERTED_KEY, { &dpd1, &upd1 })) res2;
        REQUIRE_NOTHROW(res1 = sc.insert(INSERTED_KEY, { &dpd1, &upd1 }));
        REQUIRE(res1.second);
        REQUIRE_NOTHROW(res2 = sc.insert(INSERTED_KEY, { &dpd1, &upd1 }));
        REQUIRE(!res2.second);
        REQUIRE(res1.first == res2.first);

        REQUIRE(!sc.empty());
        REQUIRE(sc.begin() != sc.end());
        REQUIRE(sc.cbegin() != sc.cend());
        REQUIRE(sc.contains(INSERTED_KEY));
        REQUIRE(sc.find(INSERTED_KEY) != sc.end());
        REQUIRE(sc.size() == 1u);
        REQUIRE(sc);

        REQUIRE(sc.erase(INSERTED_KEY));
        REQUIRE(sc.empty());

        REQUIRE_NOTHROW(sc.insert(INSERTED_KEY, { &dpd1, &upd1 }));
        REQUIRE_NOTHROW(sc.clear());
        REQUIRE(sc.empty());
    }

    SECTION("NodeIterator test")
    {
        static constexpr int INSERTED_KEY{ 1 };
        SceneGraph<int, DownPropagationDummy*, UpPropagationDummy*> sc;

        DownPropagationDummy dpd1;
        UpPropagationDummy upd1;
        REQUIRE_NOTHROW(sc.insert(INSERTED_KEY, { &dpd1, &upd1 }));

        REQUIRE((*sc.tlbegin()).data.downPropagate == &dpd1);
        REQUIRE(sc.tlbegin()->data.upPropagate == &upd1);

        {
            auto tlbegin{ sc.tlbegin() };
            REQUIRE(tlbegin++ == sc.tlbegin());
            REQUIRE(tlbegin == sc.tlend());
        }
        {
            auto tlbegin{ sc.tlbegin() };
            REQUIRE(++tlbegin == sc.tlend());
            REQUIRE(tlbegin == sc.tlend());
        }
        {
            auto tlend{ sc.tlend() };
            REQUIRE(sc.tlend() == tlend--);
            REQUIRE(sc.tlbegin() == tlend);

        }
        {
            auto tlend{ sc.tlend() };
            REQUIRE(sc.tlbegin() == --tlend);
            REQUIRE(sc.tlbegin() == tlend);
        }

        REQUIRE(sc.tlbegin() + 1u == sc.tlend());
        REQUIRE(sc.ctlbegin() + 1u == sc.ctlend());
        REQUIRE(1u + sc.tlbegin() == sc.tlend());
        REQUIRE(1u + sc.ctlbegin() == sc.ctlend());

        decltype(sc)::node_iterator begin = sc.tlbegin();
        { 
            decltype(sc)::node_iterator copyConstruct(begin);
            decltype(sc)::node_iterator copyAssign = begin;
            decltype(sc)::node_iterator moveConstruct(std::move(begin));
            begin = sc.tlbegin();
            decltype(sc)::node_iterator moveAssign = std::move(begin);
            begin = sc.tlbegin();
            REQUIRE(begin == copyConstruct);
            REQUIRE(begin == copyAssign);
            REQUIRE(begin == moveConstruct);
            REQUIRE(begin == moveAssign);
        }
        decltype(sc)::const_node_iterator cbegin = sc.ctlbegin();
        {
            decltype(sc)::const_node_iterator copyConstruct(cbegin);
            decltype(sc)::const_node_iterator copyAssign = cbegin;
            decltype(sc)::const_node_iterator moveConstruct(std::move(cbegin));
            cbegin = sc.ctlbegin();
            decltype(sc)::const_node_iterator moveAssign = std::move(cbegin);
            cbegin = sc.ctlbegin();
            REQUIRE(cbegin == copyConstruct);
            REQUIRE(cbegin == copyAssign);
            REQUIRE(cbegin == moveConstruct);
            REQUIRE(cbegin == moveAssign);
        }

        {
            decltype(sc)::const_node_iterator fromNonConst(begin);
            REQUIRE(cbegin == fromNonConst);
        }

        REQUIRE(begin != sc.tlend());
        REQUIRE(cbegin != sc.ctlend());

        begin += 1u;
        cbegin += 1u;
        REQUIRE(begin == sc.tlend());
        REQUIRE(cbegin == sc.ctlend());

        begin -= 1u;
        cbegin -= 1u;
        REQUIRE(begin != sc.tlend());
        REQUIRE(cbegin != sc.ctlend());

        REQUIRE(sc.tlend() - sc.tlbegin() == 1u);
        REQUIRE(sc.ctlend() - sc.ctlbegin() == 1u);

        REQUIRE(sc.tlbegin()[0].data.downPropagate == &dpd1);
        REQUIRE(sc.ctlbegin()[0].data.upPropagate == &upd1);

        REQUIRE(sc.tlbegin() < sc.tlend());
        REQUIRE(sc.ctlbegin() < sc.ctlend());

        REQUIRE(sc.tlend() > sc.tlbegin());
        REQUIRE(sc.ctlend() > sc.ctlbegin());

        sc.clear();

        REQUIRE(sc.tlbegin() >= sc.tlend());
        REQUIRE(sc.ctlbegin() >= sc.ctlend());

        REQUIRE(sc.tlend() <= sc.tlbegin());
        REQUIRE(sc.ctlend() <= sc.ctlbegin());
    }

    SECTION("LinkedIterator test")
    {
        static constexpr int INSERTED_KEY{ 1 };
        SceneGraph<int, DownPropagationDummy*, UpPropagationDummy*> sc;

        DownPropagationDummy dpd1;
        UpPropagationDummy upd1;
        REQUIRE_NOTHROW(sc.insert(INSERTED_KEY, { &dpd1, &upd1 }));

        REQUIRE((*sc.begin()).data.downPropagate == &dpd1);
        REQUIRE(sc.begin()->data.upPropagate == &upd1);

        {
            auto begin{ sc.begin() };
            REQUIRE(begin++ == sc.begin());
            REQUIRE(begin == sc.end());
        }
        {
            auto begin{ sc.begin() };
            REQUIRE(++begin == sc.end());
            REQUIRE(begin == sc.end());
        }

        decltype(sc)::iterator begin = sc.begin();
        { 
            decltype(sc)::iterator copyConstruct(begin);
            decltype(sc)::iterator copyAssign = begin;
            decltype(sc)::iterator moveConstruct(std::move(begin));
            begin = sc.begin();
            decltype(sc)::iterator moveAssign = std::move(begin);
            begin = sc.begin();
            REQUIRE(begin == copyConstruct);
            REQUIRE(begin == copyAssign);
            REQUIRE(begin == moveConstruct);
            REQUIRE(begin == moveAssign);
        }
        decltype(sc)::const_iterator cbegin = sc.cbegin();
        {
            decltype(sc)::const_iterator copyConstruct(cbegin);
            decltype(sc)::const_iterator copyAssign = cbegin;
            decltype(sc)::const_iterator moveConstruct(std::move(cbegin));
            cbegin = sc.cbegin();
            decltype(sc)::const_iterator moveAssign = std::move(cbegin);
            cbegin = sc.cbegin();
            REQUIRE(cbegin == copyConstruct);
            REQUIRE(cbegin == copyAssign);
            REQUIRE(cbegin == moveConstruct);
            REQUIRE(cbegin == moveAssign);
        }

        {
            decltype(sc)::const_iterator fromNonConst(begin);
            REQUIRE(cbegin == fromNonConst);
        }

        REQUIRE(begin != sc.end());
        REQUIRE(cbegin != sc.cend());
    }

    SECTION("Insertion")
    {
        static constexpr std::size_t NUM_ELEMENTS{ 5u };
        SceneGraph<int> sc;

        for (int iii = 0; iii < 5; ++iii)
        { sc.insert(iii); }

        log<Debug>() << sc << std::endl;

        std::size_t counter{ 0 };
        for (const auto node : sc)
        { REQUIRE(node.id == counter++); }

        {
            sc.makeParentOf(sc.find(2), sc.find(3));
            sc.makeParentOf(sc.find(2), sc.find(4));
            sc.makeParentOf(sc.find(0), sc.find(2));
            sc.makeParentOf(sc.find(1), sc.find(0));

            {
                auto it{ sc.begin() };
                REQUIRE(it++->id == 1);
                REQUIRE(it++->id == 0);
                REQUIRE(it++->id == 2);
                REQUIRE(it++->id == 3);
                REQUIRE(it++->id == 4);
            }

            REQUIRE_THROWS_AS(sc.makeParentOf(sc.find(2), sc.find(1)), SceneGraph<int>::InvalidParameter);

            log<Debug>() << sc << std::endl;
            sc.makeParentOf(sc.find(1), sc.find(4));
            sc.makeParentOf(sc.find(4), sc.find(2));
            log<Debug>() << sc << std::endl;

            {
                auto it{ sc.begin() };
                REQUIRE(it++->id == 1);
                REQUIRE(it++->id == 0);
                REQUIRE(it++->id == 4);
                REQUIRE(it++->id == 2);
                REQUIRE(it++->id == 3);
            }
        }
    }

    SECTION("Copying")
    {
        SceneGraph<int> sc;
        fillWithData(sc);
        log<Debug>() << sc << std::endl;
        checkData(sc);

        SceneGraph<int> scc(sc);
        SceneGraph<int> scac = sc;

        checkData(sc);
        checkData(scc);
        checkData(scac);

        SceneGraph<int> scm(std::move(scc));
        SceneGraph<int> scam = std::move(scac);

        checkData(sc);
        checkData(scm);
        checkData(scam);

        log<Debug>() << "sc: \n" << sc << std::endl;
        log<Debug>() << "scm: \n" << scm << std::endl;
        log<Debug>() << "scam: \n" << scam << std::endl;

        REQUIRE(sc.size() == scm.size());
        REQUIRE(sc == scm);
        for (auto it1 = sc.begin(), it2 = scm.begin(); it1 != sc.end(); ++it1, ++it2)
        { REQUIRE((*it1) == (*it2)); }

        REQUIRE(sc.size() == scam.size());
        REQUIRE(sc == scam);
        for (auto it1 = sc.begin(), it2 = scam.begin(); it1 != sc.end(); ++it1, ++it2)
        { REQUIRE((*it1) == (*it2)); }
    }

    SECTION("Copying with allocator")
    {
        using SceneGraphT = SceneGraph<int, DownPropagateBase*, UpPropagateBase*, std::allocator<int>>;
        SceneGraphT sc{std::allocator<int>()};
        fillWithData(sc);
        checkData(sc);

        SceneGraphT scc(sc);
        SceneGraphT scac = sc;

        checkData(sc);
        checkData(scc);
        checkData(scac);

        SceneGraphT scm(std::move(scc));
        SceneGraphT scam = std::move(scac);

        checkData(sc);
        checkData(scm);
        checkData(scam);

        REQUIRE(sc.size() == scm.size());
        REQUIRE(sc == scm);
        for (auto it1 = sc.begin(), it2 = scm.begin(); it1 != sc.end(); ++it1, ++it2)
        { REQUIRE((*it1) == (*it2)); }

        REQUIRE(sc.size() == scam.size());
        REQUIRE(sc == scam);
        for (auto it1 = sc.begin(), it2 = scam.begin(); it1 != sc.end(); ++it1, ++it2)
        { REQUIRE((*it1) == (*it2)); }
    }

    SECTION("Swap")
    {
        SceneGraph<int> sc1;
        SceneGraph<int> sc2;

        fillWithData(sc1);
        checkData(sc1);

        using std::swap;
        swap(sc1, sc2);

        checkData(sc2);
    }

    SECTION("Propagation")
    {
        static constexpr std::size_t NUM_NODES{ 5u };
        SceneGraph<int, DownPropagationDummy*, UpPropagationDummy*> sc;

        std::array<decltype(sc.begin()), NUM_NODES> nodes;
        std::array<DownPropagationDummy, NUM_NODES> downData;
        std::array<UpPropagationDummy, NUM_NODES> upData;

        for (auto iii = 0u; iii < NUM_NODES; ++iii)
        {
            auto insert{ sc.insert(iii, { &downData[iii], &upData[iii] }) };
            nodes[iii] = insert.first;
        }

        sc.makeParentOf(nodes[2], nodes[3]);
        sc.makeParentOf(nodes[2], nodes[4]);
        sc.makeParentOf(nodes[0], nodes[1]);
        sc.makeParentOf(nodes[0], nodes[2]);

        nodes[0]->data.downPropagate->max = NUM_NODES;
        nodes[0].setDownDirty();

        sc.propagateDirty();
        for (auto iii = 0u; iii < NUM_NODES; ++iii)
        {
            REQUIRE(nodes[iii]->data.downPropagate->max == NUM_NODES);
            REQUIRE(nodes[iii]->data.upPropagate->max == 0u);
        }

        nodes[3]->data.upPropagate->max = 2 * NUM_NODES;
        nodes[3].setUpDirty();

        sc.propagateDirty();
        {
            REQUIRE(nodes[0]->data.downPropagate->max == NUM_NODES);
            REQUIRE(nodes[0]->data.upPropagate->max == 2 * NUM_NODES);

            REQUIRE(nodes[1]->data.downPropagate->max == NUM_NODES);
            REQUIRE(nodes[1]->data.upPropagate->max == 0);

            REQUIRE(nodes[2]->data.downPropagate->max == NUM_NODES);
            REQUIRE(nodes[2]->data.upPropagate->max == 2 * NUM_NODES);

            REQUIRE(nodes[3]->data.downPropagate->max == NUM_NODES);
            REQUIRE(nodes[3]->data.upPropagate->max == 2 * NUM_NODES);

            REQUIRE(nodes[4]->data.downPropagate->max == NUM_NODES);
            REQUIRE(nodes[4]->data.upPropagate->max == 0);
        }
    }
}
