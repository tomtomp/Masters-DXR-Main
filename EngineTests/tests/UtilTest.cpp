/**
* @file UtilTest.cpp
* @author Tomas Polasek
* @brief Util unit testing.
*/

#include "stdafx.h"

#include "CatchUtils.h"

#include "engine/util/Util.h"
#include "engine/util/wexception.h"

#pragma warning(push)
// Disable deprecated warning for deprecated function - wide exception what().
#pragma warning(disable: 4996)

TEST_CASE("Utility function and classes testing", "[Util]")
{
    SECTION("Wstring to string conversion")
    {
        REQUIRE(util::toString(L"Hello World") == "Hello World");
    }

    SECTION("Wide exception string handling")
    {
        const util::wexception exc(L"Hello World");

        CHECK_THAT(exc.wwhat(), Catch::Predicate<std::wstring>([] (const std::wstring &val) -> bool
        {
            return val == L"Hello World";
        }));
        CHECK_THAT(exc.what(), Catch::Matchers::Equals("Hello World"));
    }

    SECTION("Wide runtime_error string handling")
    {
        const util::wruntime_error exc(L"Hello World");

        CHECK_THAT(exc.wwhat(), Catch::Predicate<std::wstring>([] (const std::wstring &val) -> bool
        {
            return val == L"Hello World";
        }));
        CHECK_THAT(exc.what(), Catch::Matchers::Equals("Hello World"));
    }
}

#pragma warning(pop) 
