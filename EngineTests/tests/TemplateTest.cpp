/**
* @file TemplateTest.cpp
* @author Tomas Polasek
* @brief Template unit testing.
*/

#include "stdafx.h"

#include "CatchUtils.h"

#include "engine/util/Template.h"

auto hasFun = util::isValidExpr([](auto &&x) -> decltype(x.fun()) {});

TEST_CASE("Template helpers testing", "[Template]")
{
    SECTION("isValidExpr")
    {
        struct A {};
        struct B { void fun(); };

        static constexpr auto A_HAS_FUN{ decltype(hasFun(std::declval<A>()))::value };
        static constexpr auto B_HAS_FUN{ decltype(hasFun(std::declval<B>()))::value };

        REQUIRE(A_HAS_FUN == false);
        REQUIRE(B_HAS_FUN == true);
    }
}
