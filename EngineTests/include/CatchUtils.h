/**
* @file CatchUtils.h
* @author Tomas Polasek
* @brief Utilities for Catch unit testing.
*/

#pragma once

#include <catch2/catch.hpp>

#include "engine/util/Types.h"

/// Catch 2 matcher, which prints out exception text.
class WExceptionPrinterMatcher : public Catch::MatcherBase<util::wexception>
{
public:
    WExceptionPrinterMatcher() = default;

    virtual bool match(const util::wexception &err) const override
    {
        std::cout << err.what() << std::endl;
        return true;
    }

    virtual std::string describe() const override
    {
        return "printing out util::wexception";
    }
private:
protected:
}; // class WExceptionPrinterMatcher
