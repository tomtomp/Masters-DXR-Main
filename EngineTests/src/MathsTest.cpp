/**
* @file MathsTest.cpp
* @author Tomas Polasek
* @brief Maths unit testing.
*/

#include "stdafx.h"

#include "CatchUtils.h"

#include "engine/util/Math.h"

TEST_CASE("Mathematical functions testing", "[Math]")
{
    SECTION("Align to")
    {
        REQUIRE(util::math::alignTo(0u, 0u) == 0u);
        REQUIRE(util::math::alignTo(0u, 1u) == 0u);
        REQUIRE(util::math::alignTo(1u, 1u) == 1u);
        REQUIRE(util::math::alignTo(128u, 1024u) == 1024u);
    }
}
