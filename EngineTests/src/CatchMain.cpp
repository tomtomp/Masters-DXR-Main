/**
* @file CatchMain.cpp
* @author Tomas Polasek
* @brief Main unit testing file.
*/

#include "stdafx.h"

#define CATCH_CONFIG_COLOUR_NONE
#define CATCH_CONFIG_RUNNER
#include "catch2/catch.hpp"

#include "engine/util/logging/MessageOutput.h"

int main(int argc, char *argv[])
{
    /// Configure console output.
    util::MessageOutput<false> msg;

    const auto result{ Catch::Session().run(argc, argv) };

    return result;
}
