/**
* @file VertexShader.hlsl
* @author Tomas Polasek
* @brief Simple vertex shader.
*/

struct ModelViewProjection
{
    matrix mvp;
};

ConstantBuffer<ModelViewProjection> ModelViewProjectionCB : register(b0);

struct VertexPosColor
{
    float3 position : POSITION;
    float3 color : COLOR;
};

struct VertexShaderOutput
{
    float4 color : COLOR;
    float4 position : SV_Position;
};

VertexShaderOutput main(VertexPosColor input)
{
    VertexShaderOutput output;

    output.position = mul(ModelViewProjectionCB.mvp, float4(input.position, 1.0f));
    //output.position = float4(input.position, 1.0f);
    output.color = float4(input.color, 1.0f);

    return output;
}
