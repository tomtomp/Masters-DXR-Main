﻿/**
 * @file main.cpp
 * @author Tomas Polasek
 * @brief Template for the main.cpp file.
 */

#pragma once

#include "App.h"

#include "engine/util/logging/MessageOutput.h"
#include "engine/util/prof/Profiler.h"
#include "engine/util/prof/PrintCrawler.h"
