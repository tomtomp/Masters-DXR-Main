/**
 * @file Factorial.h
 * @author Tomas Polasek
 * @brief Factorial implementation used for testing.
 */

#pragma once

/**
 * Calculate factorial of given number
 * @param num Number to compute the factorial of.
 * @return Returns the factorial of specified number.
 */
unsigned int factorial(unsigned int num);

