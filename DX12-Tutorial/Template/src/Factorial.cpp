/**
 * @file Factorial.cpp
 * @author Tomas Polasek
 * @brief Factorial implementation used for testing.
 */

#include "stdafx.h"
#include "Factorial.h"

unsigned int factorial(unsigned int num)
{
    return num <= 1 ? num : factorial(num - 1) * num;
}
