﻿/**
 * @file main.cpp
 * @author Tomas Polasek
 * @brief Template for the main.cpp file.
 */

#include "stdafx.h"
#include "main.h"

#include "Factorial.h"

int CALLBACK wWinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, PWSTR lpCmdLine, int nCmdShow)
{
	std::cout << "Hello CMake!" << std::endl;
	std::cout << "Factorial of 5 is: " << factorial(5u) << std::endl;

	MessageBox(nullptr, (LPCWSTR)L"Hello World!", (LPCWSTR)L"Hello World!", MB_ICONWARNING | MB_CANCELTRYCONTINUE | MB_DEFBUTTON2);

	return 0;
}
