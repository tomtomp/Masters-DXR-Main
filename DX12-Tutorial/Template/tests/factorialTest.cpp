/**
 * @file factorialTest.cpp
 * @author Tomas Polasek
 * @brief Template for tests.
 */

#define CATCH_CONFIG_MAIN
#include "catch2/catch.hpp"

// Test case begins:
#include "Factorial.h"

TEST_CASE("Factorial computation", "[factorial]")
{
  REQUIRE(factorial(0) == 0);
  REQUIRE(factorial(1) == 1);
  REQUIRE(factorial(2) == 2);
  REQUIRE(factorial(3) == 6);
}

