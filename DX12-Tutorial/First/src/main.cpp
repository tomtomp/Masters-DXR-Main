﻿/**
 * @file main.cpp
 * @author Tomas Polasek
 * @brief Template for the main.cpp file.
 */

#include "stdafx.h"
#include "main.h"

int CALLBACK wWinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, PWSTR lpCmdLine, int nCmdShow)
{
    // Setup message display.
    util::MessageOutput<true> msgOutput;

    int32_t result{EXIT_SUCCESS};

    try
    {
        PROF_SCOPE("Main application loop");

        // Default configuration.
        dxr::AppSpecificConfig cfg;
        // Load command line options.
        cfg.setParseCmdArgs();

        if (cfg.metaDisplayHelp)
        { // If help display is requested, do so.
            util::wdout << cfg.getHelp() << std::endl;
            // No need to continue, help has been displayed.
            return result;
        }

        // Initialize engine.
        quark::Engine engine(cfg, hInstance);

        // Execute engine application.
        result = engine.run<dxr::App>(cfg);
    } catch (const util::OptionParser::ParsingException &err)
    { // Parser specific error -> print to console.
        util::wdout << "Argument parsing error: \n" << err.wwhat() << std::endl;
        result = EXIT_FAILURE;
    } catch (const util::wexception &err)
    { // Some exception from inner code -> display as error message.
        util::wmout << "Exception escaped the main loop: \n" << err.wwhat() << std::endl;
        result = EXIT_FAILURE;
    } catch (const std::exception &err)
    { // Some exception from inner code -> display as error message.
        util::mout << "Exception escaped the main loop: \n" << err.what() << std::endl;
        result = EXIT_FAILURE;
    } catch (...)
    { // Some exception from inner code -> display as error message.
        util::mout << "Unknown exception escaped the main loop!" << std::endl;
        result = EXIT_FAILURE;
    }

    util::prof::PrintCrawler pc;
    PROF_DUMP(pc);

    return result;
}
