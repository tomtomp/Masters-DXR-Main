/**
* @file App.cpp
* @author Tomas Polasek
* @brief Main application header.
*/

#include "stdafx.h"
#include "App.h"

namespace dxr
{

AppSpecificConfig::AppSpecificConfig()
{
    mParser.addOption<bool>(L"--prof", [&] (const bool &val)
    {
        specificPrintProfiler = val;
    }, L"Enable/disable periodic profiler information printing.");
}

App::App(ConfigT &cfg, quark::Engine &engine) :
    Application(cfg, engine)
{
    mBinding.kb().setAction(
        Key::Escape,
        KeyMods::None,
        KeyAction::Press,
        [&]()
    {
        exit();
    });
}

int32_t App::run()
{
    PROF_SCOPE("MainLoop");

    // Profiling printer.
    util::prof::PrintCrawler pc;

    // Target number of milliseconds between updates.
    const auto updateDeltaMs{ updateDelta() };
    util::DeltaTimer updateTimer;

    // Target number of milliseconds between renders.
    const auto renderDeltaMs{ renderDelta() };
    util::DeltaTimer renderTimer;

    /// Time for counting fps/ups.
    quark::app::FpsUpdateTimer fpsTimer(false);

    util::TickTimer debugPrintTimer(Seconds(1u), [&] ()
    {
        if (mCfg.specificPrintProfiler)
        {
            PROF_DUMP(pc);
        }
        fpsTimer.printInfo();
    });

    util::TimerTicker ticker;
    ticker.addTimer(updateTimer);
    ticker.addTimer(renderTimer);
    ticker.addTimer(fpsTimer);
    ticker.addTimer(debugPrintTimer);

    // Start the main loop.
    mRunning = true;
    while (mRunning)
    { // Main application loop
        ticker.tick();

        while (updateTimer.tryTake(updateDeltaMs))
        { // While there is updating to do, 
            PROF_SCOPE("Update");
            update(updateDeltaMs);
            fpsTimer.update();
        }

        if (renderTimer.tryTake(renderDeltaMs))
        { // Target time between renders exceeded.
            PROF_SCOPE("Render");
            render();
            fpsTimer.frame();
            renderTimer.takeAll();
        }
    }

    return EXIT_SUCCESS;
}

void App::update(Milliseconds delta)
{
    // Process any events gathered on the message bus.
    processEvents(delta);
}

void App::onEvent(const KeyboardEvent &event) const
{ mBinding.processEvent(event); }

void App::onEvent(const MouseButtonEvent &event) const
{ mBinding.processEvent(event); }

void App::onEvent(const MouseMoveEvent &event) const
{ mBinding.processEvent(event); }

void App::onEvent(const MouseScrollEvent &event) const
{ mBinding.processEvent(event); }

void App::onEvent(const ResizeEvent &event)
{ defaultResizeHandler(event); }

void App::onEvent(const quark::app::events::ExitEvent &event)
{ mRunning = false; }

}
