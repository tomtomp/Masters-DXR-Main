/**
* @file App.cpp
* @author Tomas Polasek
* @brief Main application header.
*/

#include "stdafx.h"

#include "App.h"
#include "scene/loaders/GlTFLoader.h"

namespace dxr
{

AppSpecificConfig::AppSpecificConfig()
{
    mParser.addOption<bool>(L"--prof", [&] (const bool &val)
    {
        specificPrintProfiler = val;
    }, L"Enable/disable periodic profiler information printing.");
    mParser.addOption<bool>(L"--prof-skip-inactive", [&] (const bool &val)
    {
        specificPrintProfilerSkipInactive = val;
    }, L"Enable/disable skipping of inactive scopes for the periodic profiler.");
    mParser.addOption<std::wstring>(L"--scene", [&] (const std::wstring &val)
    {
        glTFSceneFile = util::toString(val);
    }, L"Choose which glTF scene should be displayed, default "
       L"value loads a cube scene. Provided path should be "
       L"relative to the models/ folder!");
    mParser.addOption<uint32_t>(L"--duplication-offset", [&] (const std::size_t &val)
    {
        duplicationOffset = static_cast<float>(val);
    }, L"Offset of the duplicated entities in world-space.");
    mParser.addOption(L"--duplication-cube", [&] ()
    {
        duplicationCube = true;
    }, L"Create a 3D cube of duplication instead of 2D grid.");
    mParser.addOption(L"--testing-scene", [&] ()
    {
        useTestingScene = true;
    }, L"Overwrite the target scene with a testing scene.");
}

App::App(ConfigT &cfg, quark::Engine &engine) :
    Application(cfg, engine),
    mFpsTimer(false), 
    mImGuiRl{ quark::rndr::ImGuiRL::create(engine.renderer()) },
    mTexturedRl{ quark::rndr::BasicTexturedRL::create(engine.renderer()) },
    mDeferredRl{ quark::rndr::DeferredRL::create(engine.renderer()) }, 
    mRayTracingRl{ quark::rndr::BasicRayTracingRL::create(engine.renderer()) }, 
    mResolveRl{ quark::rndr::ResolveRL::create(engine.renderer()) }
{ }

App::~App()
{ mEngine.renderer().flushAll(); }

void App::initialize()
{
    PROF_SCOPE("Initialization");

    // Prepare the user-selected scene for rendering.
    prepareSceneRendering();

    // Prepare display camera of the scene.
    prepareCamera();

    // Input binding to control the camera etc.
    setupInputBindings();

    // Create common GUI elements, base GUI context and initialize render layer GUIs.
    setupGui();
}

int32_t App::run()
{
    // Profiling printer.
    util::prof::PrintCrawler pc(mCfg.specificPrintProfilerSkipInactive);

    // Target number of milliseconds between updates.
    const auto updateDeltaMs{ updateDelta() };
    util::DeltaTimer updateTimer;

    // Target number of milliseconds between renders.
    const auto renderDeltaMs{ renderDelta() };
    util::DeltaTimer renderTimer;

    mFpsTimer.reset();

    util::TickTimer debugPrintTimer(Seconds(1u), [&] ()
    {
        if (mCfg.specificPrintProfiler)
        {
            PROF_DUMP(pc);
        }
        mFpsTimer.printInfo();
        if (mC.renderMode == Configuration::RenderMode::RayTracing)
        {
            const auto framesPerSecond{ 1000.0 / mFpsTimer.frameTime() };
            const std::size_t raysPerSecond{ mRayTracingRl->raysPerFrame()};
            log<Info>() << "Rendering mode: " << mResolveRl->currentDisplayModeName() << std::endl;
            log<Info>() << "GigaRays/s : " << static_cast<double>(framesPerSecond * raysPerSecond) / std::giga::num << std::endl;
            log<Info>() << "Deferred buffers " << (mRayTracingRl->deferredBuffersRequired() ? "ARE" : "ARE NOT") << " generated!" << std::endl;
        }
        log<Info>() << "Current size of duplication grid: " << mC.duplication << std::endl;
    });

    util::TimerTicker ticker;
    ticker.addTimer(updateTimer);
    ticker.addTimer(renderTimer);
    ticker.addTimer(mFpsTimer);
    ticker.addTimer(debugPrintTimer);

    // Start the main loop.
    mRunning = true;
    while (mRunning)
    { // Main application loop
        PROF_SCOPE("MainLoop");

        ticker.tick();

        while (updateTimer.tryTake(updateDeltaMs))
        { // While there is updating to do, 
            PROF_SCOPE("Update");
            update(updateDeltaMs);
            mFpsTimer.update();
        }

        if (renderTimer.tryTake(renderDeltaMs))
        { // Target time between renders exceeded.
            PROF_SCOPE("Render");
            render();
            mFpsTimer.frame();
            renderTimer.takeAll();
        }
    }

    return EXIT_SUCCESS;
}

void App::update(Milliseconds delta)
{
    // Process any events gathered on the message bus.
    processEvents(delta);

    // Reflect the duplication parameter.
    updateDuplication();

    // Make sure entities within the scene are ready.
    mEngine.scene().refresh();

    // Perform update logic, running any scene systems: 

    mCameraSys->refresh();
    checkChooseCamera();
    mCameraController.update(delta.count());
    mScriptedController.update(delta.count());

    // End of update logic.

    // Update GUI variables.
    updateGui();
}

void App::render()
{
    auto cmdList{ clear(quark::dxtk::math::Color{0.0f, 0.0f, 0.0f}) };

    if (mC.renderMode == Configuration::RenderMode::Rasterization)
    { GPU_PROF_SCOPE(*cmdList, "Textured");
        mTexturedRl->prepare();
        mTexturedRl->render(mCameraSys->camera(), mEngine.renderer().swapChain().currentBackbufferRtv(), *cmdList);
    }

    if (mC.renderMode == Configuration::RenderMode::RayTracing)
    { GPU_PROF_SCOPE(*cmdList, "RayTracing");
        if (mRayTracingRl->deferredBuffersRequired())
        {
            mDeferredRl->prepare();
            mDeferredRl->render(mCameraSys->camera(), *cmdList);
        }

        mRayTracingRl->prepare(*cmdList);
        mRayTracingRl->setDeferredBuffers(mDeferredRl->getDeferredBuffers());
        mRayTracingRl->render(mCameraSys->camera(), renderer().swapChain().currentBackbuffer(), *cmdList);

        mResolveRl->setDeferredBuffers(mDeferredRl->getDeferredBuffers());
        mResolveRl->setRayTracingBuffers(mRayTracingRl->getRayTracingBuffers());
        mResolveRl->render(mCameraSys->camera(), 
            renderer().swapChain().currentBackbuffer(), 
            renderer().swapChain().currentBackbufferRtv(), 
            *cmdList);
    }

    { GPU_PROF_SCOPE(*cmdList, "ImGui");
        mImGuiRl->render(mEngine.renderer().swapChain().currentBackbufferRtv(), gui().model(), *cmdList);
    }

    present(cmdList);
}

void App::onEvent(const KeyboardEvent &event) const
{ mBinding.processEvent(event); }

void App::onEvent(const MouseButtonEvent &event) const
{ mBinding.processEvent(event); }

void App::onEvent(const MouseMoveEvent &event) const
{ mBinding.processEvent(event); }

void App::onEvent(const MouseScrollEvent &event) const
{ mBinding.processEvent(event); }

void App::onEvent(const ResizeEvent &event)
{
    log<Info>() << "Resizing to: " << event.newWidth << "x" << event.newHeight << std::endl;

    if (event.newWidth == 0u || event.newHeight == 0u)
    {
        log<Info>() << "Skipping resize, since resolution is 0!" << std::endl;
        return;
    }

    { // Before resizing the swap chain: 
        mImGuiRl->resizeBeforeSwapChain(event.newWidth, event.newHeight);
    }

    mEngine.renderer().resize(event.newWidth, event.newHeight);

    { // After resizing the swap chain: 
        mImGuiRl->resizeAfterSwapChain();
        mTexturedRl->resize(event.newWidth, event.newHeight);
        mDeferredRl->resize(event.newWidth, event.newHeight);;
        mRayTracingRl->resize(event.newWidth, event.newHeight);
        mResolveRl->resize(event.newWidth, event.newHeight);

        mCameraController.setViewportSize(
            static_cast<float>(event.newWidth), static_cast<float>(event.newHeight));
    }

    mC.windowWidth = static_cast<float>(event.newWidth);
    mC.windowHeight = static_cast<float>(event.newHeight);
}

void App::onEvent(const quark::app::events::ExitEvent &event)
{ mRunning = false; }

void App::registerComponents()
{
    quark::scene::ComponentRegister::registerComponent<AppInfoComponent>();
}

quark::res::scene::SceneHandle App::createPrimaryScene(const std::string &sceneFile)
{
    if (mCfg.useTestingScene)
    { return mEngine.scene().loadTestingScene(); }

    try
    {
        quark::res::File sceneFilePath;
        if (!sceneFile.empty())
        { sceneFilePath = quark::res::File(sceneFile, { DEFAULT_SCENE_SEARCH_PATH_PRIMARY, DEFAULT_SCENE_SEARCH_PATH_SECONDARY }); }
        else
        { sceneFilePath = quark::res::File(DEFAULT_SCENE_FILE, { DEFAULT_SCENE_SEARCH_PATH_PRIMARY, DEFAULT_SCENE_SEARCH_PATH_SECONDARY }); }
        return mEngine.scene().loadGlTFScene(sceneFilePath);
    } catch (const quark::res::File::FileNotFoundException &e)
    {
        log<Error>() << "Unable to find scene file \"" << 
            (sceneFile.empty() ? DEFAULT_SCENE_FILE : sceneFile) << 
            "\"! Make sure it is in the \"" << DEFAULT_SCENE_SEARCH_PATH_PRIMARY << 
            "\" folder!: " << e.what() << std::endl;
    } catch (const quark::scene::loaders::BaseSceneLoader::SceneLoadException &e)
    {
        log<Error>() << "Unable to load the scene!: \n" << e.what() << std::endl;
    }

    return mEngine.scene().loadEmptyScene();
}

quark::res::scene::SceneHandle App::preparePrimaryScene(const std::string &sceneFile)
{
    PROF_SCOPE("SceneLoad");

    auto scene{ createPrimaryScene(sceneFile) };

    mEngine.scene().prepareScene(scene);
    mEngine.scene().setCurrentScene(scene);
    mEngine.scene().refresh();

    return scene;
}

void App::prepareSceneRendering()
{ 
    PROF_SCOPE("ScenePrep");

    auto scene{ preparePrimaryScene(mCfg.glTFSceneFile) };
    const auto toDraw{ mEngine.scene().getToDraw() };
    auto directCmdList{ mEngine.renderer().directCmdQueue().getCommandList() };

    { GPU_PROF_SCOPE(*directCmdList, "ScenePrep");
        mTexturedRl->setDrawablesList(toDraw);
        mTexturedRl->prepare();

        mDeferredRl->setDrawablesList(toDraw);
        mDeferredRl->prepare();

        mRayTracingRl->setDrawablesList(toDraw);
        mRayTracingRl->prepare(*directCmdList);
    }

    { PROF_SCOPE("GpuSidePrep");
        log<Info>() << "Executing preparation command list, this includes building the AS!" << std::endl;
        directCmdList->executeNoWait();
        mEngine.renderer().flushCopyCommands();
    }

    log<Info>() << "Finished preparation!" << std::endl;
}

void App::prepareCamera()
{
    mCameraSys = mEngine.scene().addGetSystem<quark::scene::sys::CameraSys>();
    if (!mCameraSys)
    { throw std::runtime_error("Failed to create the camera management system!"); }

    mCameraController.setCameraSystem(*mCameraSys);
    mCameraController.setFov(DEFAULT_CAMERA_FOV);
    mCameraController.setViewportSize(DEFAULT_VIEWPORT_WIDTH, DEFAULT_VIEWPORT_HEIGHT);

    mScriptedController.setCameraSystem(*mCameraSys);

    mScriptedController.track().pushKeyPoint()
        .setAbsoluteTranslation({ 0.0f, 0.0f, -5.0f })
        .setAbsoluteEuler({ 0.0f, 3.141f, 0.0f })
        .setDuration(2.0f);
    mScriptedController.track().pushKeyPoint()
        .setAbsoluteEuler({ 0.0f, 4.141f, 0.0f })
        .setDuration(2.0f);
    mScriptedController.track().pushKeyPoint()
        .setAbsoluteEuler({ 0.0f, 3.141f, 0.0f })
        .setDuration(2.0f);
    mScriptedController.track().pushKeyPoint()
        .setAbsoluteEuler({ 0.0f, 2.141f, 0.0f })
        .setDuration(2.0f);
    mScriptedController.setLoopTrack(true);
    mScriptedController.startTrack();

    mScriptedController.track().saveToFile(quark::res::File("test.txt"));
    mScriptedController.track().loadFromFile(quark::res::File("test.txt"));
}

void App::setupInputBindings()
{
    // Escape to exit the program.
    mBinding.kb().setAction(
        Key::Escape,
        KeyMods::None,
        KeyAction::Press,
        [&]()
    {
        exit();
    });

    /*
    // Flag used for camera view toggle.
    static bool mousePressed{ false };

    // Set flag corresponding to mouse status.
    mBinding.mouse().setAction(MouseButton::LButton, MouseMods::None, MouseAction::Press, [&](const auto &)
    { mousePressed = true; });
    mBinding.mouse().setAction(MouseButton::LButton, MouseMods::None, MouseAction::Release, [&](const auto &)
    { mousePressed = false; });
    */

    // Mouse movement to control the camera rotation.
    mBinding.mouse().setMoveAction(
        [&] (const MouseMoveEvent &event)
    {
        // Remember last position, for delta computation.
        static auto lastPos{ event.pos };

        // Calculate the new camera rotation.
        if(event.buttonPressed(MouseButton::LButton))
        {
            if (event.modPressed(MouseMods::Control))
            {
                mRayTracingRl->setLightDirection(-screenSpaceToDirection(event.pos.x, event.pos.y));
            }
            else if (event.modPressed(MouseMods::Shift))
            {
                mRayTracingRl->setLightDirection(screenSpaceToDirection(event.pos.x, event.pos.y));
            }
            else
            {
                mCameraController.setRotation(
                    mCameraController.rotationFromMouseDelta(
                        static_cast<float>(event.pos.x - lastPos.x) * DEFAULT_MOUSE_SENSITIVITY_MULT, 
                        static_cast<float>(event.pos.y - lastPos.y) * DEFAULT_MOUSE_SENSITIVITY_MULT));
            }
        }

        lastPos = event.pos;
    });

    // Mouse scrolling for changing instance duplication.
    mBinding.mouse().setScrollAction(
        [&] (const MouseScrollEvent &event)
    {
        if (event.delta > 0)
        { changeDuplicationBy(1); }
        else if (event.delta < 0)
        { changeDuplicationBy(-1); }
    });

    // Sprint when holding down b
    mBinding.kb().setAction(
        Key::B, 
        KeyMods::None, 
        KeyAction::Press, 
        [&] ()
    {
        mCameraController.speedModifier(DEFAULT_MOVEMENT_SPRINT_MULTIPLIER);
    });
    mBinding.kb().setAction(
        Key::B, 
        KeyMods::None, 
        KeyAction::Release, 
        [&] ()
    {
        mCameraController.speedModifier(1.0f);
    });

    // Move the camera forward.
    mBinding.kb().setAction(
        Key::W, 
        KeyMods::None, 
        KeyAction::Press, 
        [&] ()
    {
        mCameraController.forward(DEFAULT_MOVEMENT_UNITS);
    });
    mBinding.kb().setAction(
        Key::W, 
        KeyMods::None, 
        KeyAction::Release, 
        [&] ()
    {
        mCameraController.forward(0.0f);
    });

    // Move the camera backward.
    mBinding.kb().setAction(
        Key::S, 
        KeyMods::None, 
        KeyAction::Press, 
        [&] ()
    {
        mCameraController.backward(DEFAULT_MOVEMENT_UNITS);
    });
    mBinding.kb().setAction(
        Key::S, 
        KeyMods::None, 
        KeyAction::Release, 
        [&] ()
    {
        mCameraController.backward(0.0f);
    });

    // Move the camera left.
    mBinding.kb().setAction(
        Key::A, 
        KeyMods::None, 
        KeyAction::Press, 
        [&] ()
    {
        mCameraController.left(DEFAULT_MOVEMENT_UNITS);
    });
    mBinding.kb().setAction(
        Key::A, 
        KeyMods::None, 
        KeyAction::Release, 
        [&] ()
    {
        mCameraController.left(0.0f);
    });

    // Move the camera right.
    mBinding.kb().setAction(
        Key::D, 
        KeyMods::None, 
        KeyAction::Press, 
        [&] ()
    {
        mCameraController.right(DEFAULT_MOVEMENT_UNITS);
    });
    mBinding.kb().setAction(
        Key::D, 
        KeyMods::None, 
        KeyAction::Release, 
        [&] ()
    {
        mCameraController.right(0.0f);
    });

    // Move the camera up.
    mBinding.kb().setAction(
        Key::Space, 
        KeyMods::None, 
        KeyAction::Press, 
        [&] ()
    {
        mCameraController.up(DEFAULT_MOVEMENT_UNITS);
    });
    mBinding.kb().setAction(
        Key::Space, 
        KeyMods::None, 
        KeyAction::Release, 
        [&] ()
    {
        mCameraController.up(0.0f);
    });

    // Move the camera down.
    mBinding.kb().setAction(
        Key::C, 
        KeyMods::None, 
        KeyAction::Press, 
        [&] ()
    {
        mCameraController.down(DEFAULT_MOVEMENT_UNITS);
    });
    mBinding.kb().setAction(
        Key::C, 
        KeyMods::None, 
        KeyAction::Release, 
        [&] ()
    {
        mCameraController.down(0.0f);
    });

    // Switch rendering mode.
    mBinding.kb().setAction(
        Key::R, 
        KeyMods::None, 
        KeyAction::Press, 
        [&]()
    {
        auto nextMode{static_cast<std::size_t>(mC.renderMode) + 1u};
        if (nextMode >= static_cast<std::size_t>(Configuration::RenderMode::Last))
        { nextMode = 0u; }

        mC.renderMode = static_cast<Configuration::RenderMode>(nextMode);
    });

    // Switch ray tracing rendering mode forward.
    mBinding.kb().setAction(
        Key::N, 
        KeyMods::None, 
        KeyAction::Press, 
        [&]()
    { mResolveRl->prevDisplayMode(); });

    // Switch ray tracing rendering mode back.
    mBinding.kb().setAction(
        Key::M, 
        KeyMods::None, 
        KeyAction::Press, 
        [&]()
    { mResolveRl->nextDisplayMode(); });
}

void App::setupGui()
{
    setupCommonGui();

    /*
    //gui().addContext("/MainWindow/First/").setType(quark::gui::GuiContextType::ChildWindow).setLabel("First");
    gui().addContext("/First/").setType(quark::gui::GuiContextType::ChildWindow).setLabel("First");
    gui().addVariable<quark::gui::Position2>("MyPosition2", "/MainWindow/First/", 
        { 0.0f, 0.0f }).setLabel("My Position");
    gui().addVariable<quark::gui::Position3>("MyPosition3", "/MainWindow/First/", 
        { 0.0f, 0.0f, 0.0f }).setLabel("My Position");

    //gui().addContext("/MainWindow/Second/").setType(quark::gui::GuiContextType::Category).setLabel("Second");
    gui().addContext("Second/").setType(quark::gui::GuiContextType::Category).setLabel("Second");
    gui().addVariable<float>("MyFloat", "/MainWindow/Second/", 
        { 0.0f }).setLabel("My Float").cfg().setMin(-1.0f).setMax(1.0f).setStep(0.001f);
    gui().addVariable<quark::gui::Enumeration>("MyEnum", "/MainWindow/Second/", { 0u })
        .setLabel("My Enum").cfg().setItemGetter(10u, [] (int index, const char **outText)
    {
        switch (index)
        {
        case 0u:
            *outText = "First";
            break;
        case 1u:
            *outText = "Second";
            break;
        case 4u:
            *outText = "Fifth";
            break;
        default:
            *outText = "Some Other";
            break;
        }
        return true;
    });
    gui().addVariable<std::string>("MyString", "/MainWindow/Second/", { "Hello World!" })
        .setLabel("My String");
    gui().addVariable<quark::gui::DynamicText>("MyDynamicText", "/MainWindow/Second/", { [] ()
    {
        static int counter{ 0 };
        return std::to_string(counter++);
    } }).setLabel("My Dynamic String");
    */
}

void App::setupCommonGui()
{
    // Create the main window, which will be our default context.
    const auto windowTitle{ util::toString(mCfg.windowTitle) };
    gui().addGuiContext("/MainWindow/", true)
        .setType(quark::gui::GuiContextType::Window)
        .setLabel(windowTitle.c_str())
        .cfg().setWidth(400.0f).setHeight(mC.windowHeight);

    gui().addGuiContext("/Stats/")
        .setType(quark::gui::GuiContextType::Category)
        .setLabel("Statistics");
    gui().addGuiVariable<uint64_t>("CurrentFps", "/Stats/", 0u)
        .setLabel("FPS: \t\t%d")
        .cfg().setFormatted(true);
    gui().addGuiVariable<float>("CurrentFrameTime", "/Stats/", 0.0f)
        .setLabel("FrameTime: \t\t%.3f")
        .cfg().setFormatted(true);
    gui().addGuiVariable<uint64_t>("CurrentUps", "/Stats/", 0u)
        .setLabel("UPS: \t\t%d")
        .cfg().setFormatted(true);
}

void App::checkChooseCamera()
{
    if (!mCameraSys->cameraChosen())
    {
        log<Info>() << "No camera has been chosen!" << std::endl;
        const auto availableCameras{ mCameraSys->numAvailableCameras() };
        if (availableCameras == 0u)
        {
            log<Warning>() << "No cameras are available in the current scene!" << std::endl;
            return;
        }

        log<Info>() << "Choosing the first camera from " << availableCameras << " total cameras in the current scene." << std::endl;
        mCameraSys->chooseCamera(0u);

        mCameraController.position(quark::dxtk::math::Vector3(0.0f, 0.0f, DEFAULT_CAMERA_DISTANCE));
    }
}

void App::changeDuplicationBy(int32_t offset)
{
    int64_t newDuplication{ static_cast<int64_t>(mC.duplication) + offset };

    if (newDuplication < 1)
    { newDuplication = 1; }

    if (newDuplication != mC.duplication)
    {
        mC.duplication = static_cast<uint32_t>(newDuplication);
        mC.duplicationChanged = true;
    }
}

void App::updateDuplication()
{
    if (!mC.duplicationChanged)
    { return; }

    for (auto &entity : mEngine.scene().foreachSceneEntity())
    { // Cleanup the duplicated entities.
        if (entity.has<AppInfoComponent>() && entity.get<AppInfoComponent>()->duplicated)
        { mEngine.scene().destroyEntity(entity); }
    }

    if (mEngine.scene().foreachSceneEntity().size() == 0u)
    { // No source entity available.
        return;
    }

    // Source entity is always the first one for now.
    auto srcEntity{ *mEngine.scene().foreachSceneEntity().begin() };

    // Factor of duplication if we are duplicating in a cube.
    const auto cubeDuplicates{ mCfg.duplicationCube ? mC.duplication : 1u };
    // Number of duplicates to create.
    const auto duplicates{ mC.duplication * mC.duplication * cubeDuplicates };

    // Create the grid of entities.
    for (std::size_t iii = 1u; iii < duplicates; ++iii)
    { // Skip the first one, which would overlap the original.
        const auto pX{ iii % mC.duplication };
        const auto pY{ (iii / mC.duplication) % mC.duplication };
        const auto pZ{ mCfg.duplicationCube ? iii / (mC.duplication * mC.duplication) : 0.0f };

        auto duplicateEntity{ mEngine.scene().duplicateSceneEntity<quark::scene::comp::Renderable, quark::scene::comp::Transform>(srcEntity) };

        // Offset the location.
        auto transformComp{ duplicateEntity.get<quark::scene::comp::Transform>() };
        transformComp->localTransform.setTranslate({ mCfg.duplicationOffset * pX, mCfg.duplicationOffset * pZ, mCfg.duplicationOffset * pY });
        transformComp->dirty = true;

        // Mark the entity as duplicated.
        auto appInfoComp{ duplicateEntity.add<AppInfoComponent>() };
        appInfoComp->duplicated = true;
    }

    // Duplication has been updated.
    mC.duplicationChanged = false;
}

void App::updateGui()
{
    gui().getVariable<uint64_t>("CurrentFps") = mFpsTimer.fps();
    gui().getVariable<float>("CurrentFrameTime") = mFpsTimer.frameTime();
    gui().getVariable<uint64_t>("CurrentUps") = mFpsTimer.ups();
}

quark::dxtk::math::Vector3 App::screenSpaceToDirection(uint32_t x, uint32_t y)
{
    // Use ray tracing matrix, which transforms from screen-space to world.
    const auto screenToWorld{ mCameraSys->camera().rayTracingVP().Transpose() };

    // Normalized device coordinates <-1.0f, 1.0f>, y-axis is flipped.
    const auto screenSpace{ 
        quark::dxtk::math::Vector4 {
            2.0f * x / mC.windowWidth - 1.0f,
            -(2.0f * y / mC.windowHeight - 1.0f),
            0.0f, 1.0f } };

    // Position of the pixel in world-space
    quark::dxtk::math::Vector4 worldSpace{ XMVector4Transform(screenSpace, screenToWorld) };
    worldSpace.x /= worldSpace.w;
    worldSpace.y /= worldSpace.w;
    worldSpace.z /= worldSpace.w;

    // Position of the camera in world-space.
    const auto cameraPos{ mCameraSys->camera().position() };

    // Calculate a vector from camera to the pixel.
    return { worldSpace.x - cameraPos.x, worldSpace.y - cameraPos.y, worldSpace.z - cameraPos.z };
}

}
