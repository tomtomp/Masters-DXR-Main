/**
* @file App.h
* @author Tomas Polasek
* @brief Main application header.
*/

#pragma once

#define ENGINE_ENABLE_RAY_TRACING
#include "Engine.h"
#include "engine/application/Application.h"

#include "engine/renderer/imGuiRL/ImGuiRL.h"
#include "engine/renderer/basicTexturedRL/BasicTexturedRL.h"
#include "engine/renderer/deferredRL/DeferredRL.h"
#include "engine/renderer/basicRayTracingRL/BasicRayTracingRL.h"
#include "engine/renderer/resolveRL/ResolveRL.h"

#include "engine/input/KeyBinding.h"

#include "engine/scene/systems/CameraSys.h"
#include "engine/scene/logic/FreeLookController.h"
#include "engine/scene/logic/ScriptedCameraController.h"

/// Namespace containing the project.
namespace dxr
{


/// Application specific options should be added here.
class AppSpecificConfig : public quark::EngineRuntimeConfig
{
public:
    AppSpecificConfig();

    // Start of options section

    /// Periodically print out profiler information.
    bool specificPrintProfiler{ false };
    /// Should the periodic profiler skip inactive scopes?
    bool specificPrintProfilerSkipInactive{ true };
    /// GlTF model for the primary scene.
    std::string glTFSceneFile{ "" };
    /// Offset of duplicated instances in world space.
    float duplicationOffset{ 10.0f };
    /// Should the duplication create a 2D grid (false) or 3D cube (true).
    bool duplicationCube{ false };

    /// Should the testing scene be loaded instead?
    bool useTestingScene{ false };

    // End of options section
}; // class AppSpecificRuntimeConfig

/// Main application class.
class App : public quark::app::Application<App, AppSpecificConfig>
{
public:
    /// Constructor called by the engine.
    App(ConfigT &cfg, quark::Engine &engine);

    /// Wait for all GPU command queues and free any resources.
    virtual ~App();

    virtual void initialize() override final;
    //virtual void destroy() override final;
    virtual int32_t run() override final;
    virtual void update(Milliseconds delta) override final;
    virtual void render() override final;
    //virtual void processEvents() override final;

    void onEvent(const KeyboardEvent &event) const;
    void onEvent(const MouseButtonEvent &event) const;
    void onEvent(const MouseMoveEvent &event) const;
    void onEvent(const MouseScrollEvent &event) const;
    void onEvent(const ResizeEvent &event);
    void onEvent(const ExitEvent &event);

    /// Register the app specific components.
    static void registerComponents();
private:
    /// Component used to store app specific information.
    struct AppInfoComponent
    {
        /// Has this entity been duplicated?
        bool duplicated{ false };
    }; // struct AppInfoComponent

    /// Default scene file loaded when none is provided.
    static constexpr const char *DEFAULT_SCENE_FILE{ "Cube/Cube.gltf" };
    /// Default path which will be searched for provided models.
    static constexpr const char *DEFAULT_SCENE_SEARCH_PATH_PRIMARY{ "models/" };
    /// Backup scene path, which works when the executable is in the build hierarchy.
    static constexpr const char *DEFAULT_SCENE_SEARCH_PATH_SECONDARY{ "../../../../models/" };

    /// Default FOV of the camera.
    static constexpr float DEFAULT_CAMERA_FOV{ 80.0f };
    /// Default width of the viewport, before window size changes.
    static constexpr float DEFAULT_VIEWPORT_WIDTH{ 1024.0f };
    /// Default height of the viewport, before window size changes.
    static constexpr float DEFAULT_VIEWPORT_HEIGHT{ 768.0f };

    /// Mouse sensitivity for camera rotation control.
    static constexpr float DEFAULT_MOUSE_SENSITIVITY_MULT{ 0.01f };
    /// How many units does the camera move per one time unit?
    static constexpr float DEFAULT_MOVEMENT_UNITS{ 0.005f };
    /// Unit multiplier when sprinting.
    static constexpr float DEFAULT_MOVEMENT_SPRINT_MULTIPLIER{ 5.0f };

    /// Default camera distance from origin
    static constexpr float DEFAULT_CAMERA_DISTANCE{ 5.0f };

    /**
     * Load provided scene file and return a handle to the loaded scene.
     * If provided scene file is empty or non-existent, a default scene 
     * will be loaded instead. Default scene is a cube, or empty if cube 
     * is unavailable.
     * @param sceneFile Scene file which should be loaded.
     * @return Returns handle to the created scene.
     */
    quark::res::scene::SceneHandle createPrimaryScene(const std::string &sceneFile);

    /**
     * Load provided scene file and return a handle to the loaded scene.
     * If provided scene file is empty or non-existent, a default scene 
     * will be loaded instead. Default scene is a cube, or empty if cube 
     * is unavailable.
     * Also prepares the scene for rendering.
     * @param sceneFile Scene file which should be loaded.
     * @return Returns handle to the created scene.
     */
    quark::res::scene::SceneHandle preparePrimaryScene(const std::string &sceneFile);

    /**
     * Prepare for rendering of the user selected scene, specified within 
     * the runtime configuration.
     */
    void prepareSceneRendering();

    /// Prepare the camera for scene display.
    void prepareCamera();

    /// Setup key bindings for controlling camera etc.
    void setupInputBindings();

    /// Setup common GUI elements, base GUI context and initialize render layer GUIs.
    void setupGui();

    /// Common GUI elements and main window.
    void setupCommonGui();

    /**
     * Check whether a camera has bee already selected, selecting 
     * the first one if it was not.
     */
    void checkChooseCamera();

    /**
     * Change the duplication by given amount, setting the changed 
     * flag if necessary.
     * Final value is automatically set to zero if the result is 
     * negative.
     * @param offset Offset to change the duplication by.
     */
    void changeDuplicationBy(int32_t offset);

    /**
     * Update the scene state to reflect the current duplication.
     * Actions will be taken only if duplicationChanged flag is true.
     */
    void updateDuplication();

    /**
     * Update the current state of the GUI variables and reflect 
     * any changes.
     */
    void updateGui();

    /**
     * Convert location of a pixel on the screen to a direction 
     * in world space.
     * @param x Coordinate of the pixel on the x-axis.
     * @param y Coordinate of the pixel on the y-axis.
     */
    quark::dxtk::math::Vector3 screenSpaceToDirection(uint32_t x, uint32_t y);

    /// Main binding scheme.
    quark::input::KeyBinding mBinding;

    /// ImGUI testing window.
    bool mShowDemoWindow{ true };

    /// Timer for counting fps/ups.
    quark::app::FpsUpdateTimer mFpsTimer;

    /// ImGUI rendering layer.
    quark::rndr::ImGuiRL::PtrT mImGuiRl;
    /// Textured entities rendering layer.
    quark::rndr::BasicTexturedRL::PtrT mTexturedRl;
    /// Rendering layer for G-Buffer generation.
    quark::rndr::DeferredRL::PtrT mDeferredRl;
    /// Ray tracing rendering layer.
    quark::rndr::BasicRayTracingRL::PtrT mRayTracingRl;
    /// Resolve pass for hybrid ray tracing.
    quark::rndr::ResolveRL::PtrT mResolveRl;

    /// Camera management system.
    quark::scene::sys::CameraSys *mCameraSys{ nullptr };
    /// Controller for chosen camera.
    quark::scene::logic::FreeLookController mCameraController{ };
    /// Automated controller for chosen camera.
    quark::scene::logic::ScriptedCameraController mScriptedController{ };

    /// Container for rendering configuration.
    struct Configuration
    {
        /// Enumeration of possible rendering modes.
        enum class RenderMode
        {
            Rasterization,
            RayTracing,
            Last
        }; // enum class RenderMode

        /// Current mode of rendering.
        RenderMode renderMode{ RenderMode::Rasterization };

        /// What is the dimension of the duplication grid?
        uint32_t duplication{ 1u };
        /// Has the duplication changed since last update?
        bool duplicationChanged{ false };

        /// Current width of the main window.
        float windowWidth{ DEFAULT_VIEWPORT_WIDTH };
        /// Current height of the main window.
        float windowHeight{ DEFAULT_VIEWPORT_HEIGHT };

        /// Current per-frame time in milliseconds.
        float frameTime{ 0.0f };
        /// Current frames per second.
        float fps{ 0.0f };
    }; // struct Configuration

    /// Configuration of the current scene rendering.
    Configuration mC;
protected:
}; // class App
    
} // namespace App
