/**
* @file App.cpp
* @author Tomas Polasek
* @brief Main application header.
*/

#include "stdafx.h"

#include "App.h"

namespace dxr
{

AppSpecificConfig::AppSpecificConfig()
{
    mParser.addOption<bool>(L"--prof", [&] (const bool &val)
    {
        specificPrintProfiler = val;
    }, L"Enable/disable periodic profiler information printing.");
}

App::App(ConfigT &cfg, quark::Engine &engine) :
    Application(cfg, engine),
    mTexturedRl{ quark::rndr::BasicTexturedRL::create(engine.renderer()) }
{ }

void App::initialize()
{
    mBinding.kb().setAction(
        Key::Escape,
        KeyMods::None,
        KeyAction::Press,
        [&]()
    {
        exit();
    });
}

int32_t App::run()
{
    PROF_SCOPE("MainLoop");

    //const quark::res::File sceneFile("Cube/Cube.gltf", { "models/" });
    //const quark::res::File sceneFile("SciFiHelmet/SciFiHelmet.gltf", { "models/" });
    const quark::res::File sceneFile("Sponza/Sponza.gltf", { DEFAULT_SCENE_SEARCH_PATH_PRIMARY, DEFAULT_SCENE_SEARCH_PATH_SECONDARY });
    //const quark::res::File sceneFile("Monster/Monster.gltf", { "models/" });
    auto scene{ mEngine.scene().loadGlTFScene(sceneFile) };

    mEngine.scene().prepareScene(scene);
    mEngine.scene().setCurrentScene(scene);
    mEngine.scene().refresh();
    const auto toDraw{ mEngine.scene().getToDraw() };

    mTexturedRl->setDrawablesList(toDraw);
    mTexturedRl->prepare();

    mEngine.renderer().flushAll();
    mEngine.renderer().memoryMgr().uploadCmdListsExecuted();

    static constexpr float MOUSE_ROTATION_DIVIDER{ 64.0f };
    bool rotatingWithMouse{ false };
    util::Point2<int16_t> rotatingWithMouseStartPos{ };
    static constexpr float MOUSE_SCROLL_DIVIDER{ -128.0f };
    static constexpr float CAMERA_DISTANCE_MIN{ 1.0f };
    static constexpr float CAMERA_DISTANCE_MAX{ 16.0f };
    float cameraDistance{ 6.0f };
    float rotX{ 0.0f };
    float rotY{ 0.0f };
    float posY{ 0.0f };

    mCamera.setLookAtTransform(
        // Position of the camera.
        { cameraDistance * cos(rotX), cameraDistance *sin(rotX), cameraDistance }, 
        // Looking at origin.
        { 0.0f, 0.0f, 0.0f }, 
        // Up is on the positive y-axis.
        { 0.0f, 1.0f, 0.0f });

    mBinding.mouse().setAction(
        MouseButton::LButton, 
        MouseMods::None, 
        MouseAction::Press, 
        [&] (const MouseButtonEvent &event)
    {
        rotatingWithMouse = true;
        rotatingWithMouseStartPos = util::Point2<int16_t>{ event.pos.x, event.pos.y };
    });

    mBinding.mouse().setAction(
        MouseButton::LButton, 
        MouseMods::None, 
        MouseAction::Release, 
        [&] (const MouseButtonEvent &event)
    {
        rotatingWithMouse = false;
    });

    mBinding.mouse().setMoveAction(
        [&] (const MouseMoveEvent &event)
    {
        if (rotatingWithMouse)
        {
            const util::Point2<int16_t> relativeToStart{ 
                event.pos.x - rotatingWithMouseStartPos.x, 
                event.pos.y - rotatingWithMouseStartPos.y };
            rotX += relativeToStart.x / MOUSE_ROTATION_DIVIDER;
            rotY += relativeToStart.y / MOUSE_ROTATION_DIVIDER;
            rotatingWithMouseStartPos = util::Point2<int16_t>{ event.pos.x, event.pos.y };

            mCamera.setLookAtTransform(
                // Position of the camera.
                { 
                    cameraDistance * sin(rotX), 
                    posY, 
                    cameraDistance * cos(rotX)
                }, 
                // Looking at origin.
                { 0.0f, 0.0f, 0.0f }, 
                // Up is on the positive y-axis.
                { 0.0f, 1.0f, 0.0f });
        }
    });

    mBinding.mouse().setScrollAction(
        [&] (const MouseScrollEvent &event)
    {
        cameraDistance += event.delta / MOUSE_SCROLL_DIVIDER;
        if (cameraDistance < CAMERA_DISTANCE_MIN)
        { cameraDistance = CAMERA_DISTANCE_MIN; }
        if (cameraDistance > CAMERA_DISTANCE_MAX)
        { cameraDistance = CAMERA_DISTANCE_MAX; }

        mCamera.setLookAtTransform(
            // Position of the camera.
            { 
                cameraDistance * sin(rotX), 
                posY, 
                cameraDistance * cos(rotX)
            }, 
            // Looking at origin.
            { 0.0f, 0.0f, 0.0f }, 
            // Up is on the positive y-axis.
            { 0.0f, 1.0f, 0.0f });
    });

    mBinding.kb().setAction(
        Key::R,
        KeyMods::None,
        KeyAction::Repeat, [&]()
    {
        posY += 0.05f;
        mCamera.setLookAtTransform(
            // Position of the camera.
            { 
                cameraDistance * sin(rotX), 
                posY, 
                cameraDistance * cos(rotX)
            }, 
            // Looking at origin.
            { 0.0f, 0.0f, 0.0f }, 
            // Up is on the positive y-axis.
            { 0.0f, 1.0f, 0.0f });
    });

    mBinding.kb().setAction(
        Key::F,
        KeyMods::None,
        KeyAction::Repeat, [&]()
    {
        posY -= 0.05f;
        mCamera.setLookAtTransform(
            // Position of the camera.
            { 
                cameraDistance * sin(rotX), 
                posY, 
                cameraDistance * cos(rotX)
            }, 
            // Looking at origin.
            { 0.0f, 0.0f, 0.0f }, 
            // Up is on the positive y-axis.
            { 0.0f, 1.0f, 0.0f });
    });

    // Profiling printer.
    util::prof::PrintCrawler pc;

    // Target number of milliseconds between updates.
    const auto updateDeltaMs{ updateDelta() };
    util::DeltaTimer updateTimer;

    // Target number of milliseconds between renders.
    const auto renderDeltaMs{ renderDelta() };
    util::DeltaTimer renderTimer;

    /// Time for counting fps/ups.
    quark::app::FpsUpdateTimer fpsTimer(false);

    util::TickTimer debugPrintTimer(Seconds(1u), [&] ()
    {
        if (mCfg.specificPrintProfiler)
        {
            PROF_DUMP(pc);
        }
        fpsTimer.printInfo();
    });

    util::TimerTicker ticker;
    ticker.addTimer(updateTimer);
    ticker.addTimer(renderTimer);
    ticker.addTimer(fpsTimer);
    ticker.addTimer(debugPrintTimer);

    // Start the main loop.
    mRunning = true;
    while (mRunning)
    { // Main application loop
        ticker.tick();

        while (updateTimer.tryTake(updateDeltaMs))
        { // While there is updating to do, 
            PROF_SCOPE("Update");
            update(updateDeltaMs);
            fpsTimer.update();
        }

        if (renderTimer.tryTake(renderDeltaMs))
        { // Target time between renders exceeded.
            PROF_SCOPE("Render");
            render();
            fpsTimer.frame();
            renderTimer.takeAll();
        }
    }

    return EXIT_SUCCESS;
}

void App::update(Milliseconds delta)
{
    // Process any events gathered on the message bus.
    processEvents(delta);
}

void App::render()
{
    auto cmdList{ clear() };

    mTexturedRl->render(mCamera, mEngine.renderer().swapChain().currentBackbufferRtv(), *cmdList);

    present(std::move(cmdList));
}

void App::onEvent(const KeyboardEvent &event) const
{ mBinding.processEvent(event); }

void App::onEvent(const MouseButtonEvent &event) const
{ mBinding.processEvent(event); }

void App::onEvent(const MouseMoveEvent &event) const
{ mBinding.processEvent(event); }

void App::onEvent(const MouseScrollEvent &event) const
{ mBinding.processEvent(event); }

void App::onEvent(const ResizeEvent &event)
{
    log<Info>() << "Resizing to: " << event.newWidth << "x" << event.newHeight << std::endl;

    if (event.newWidth == 0u || event.newHeight == 0u)
    {
        log<Info>() << "Skipping resize, since resolution is 0!" << std::endl;
        return;
    }

    { // Before resizing the swap chain: 
    }
    mEngine.renderer().resize(event.newWidth, event.newHeight);
    { // After resizing the swap chain: 
        mTexturedRl->resize(event.newWidth, event.newHeight);
    }

    mCamera.setFinPerspProj(
        // Aspect ratio.
        static_cast<float>(event.newHeight) / event.newWidth, 
        // Field-of-view.
        DirectX::XMConvertToRadians(80.0f), 
        // Near and far.
        0.1f, 100.0f);
}

void App::onEvent(const quark::app::events::ExitEvent &event)
{ mRunning = false; }

}
