/**
* @file App.h
* @author Tomas Polasek
* @brief Main application header.
*/

#pragma once

#include "Engine.h"
#include "engine/application/Application.h"

#include "engine/renderer/imGuiRL/ImGuiRL.h"
#include "engine/renderer/basicTexturedRL/BasicTexturedRL.h"

#include "engine/helpers/d3d12/D3D12PipelineStateBuilder.h"
#include "engine/helpers/d3d12/D3D12RootSignatureBuilder.h"

#include "engine/helpers/d3d12/D3D12VertexBuffer.h"
#include "engine/helpers/d3d12/D3D12IndexBuffer.h"

#include "engine/resources/d3d12/D3D12InputLayout.h"
#include "engine/resources/d3d12/D3D12Shader.h"

#include "engine/input/KeyBinding.h"
#include "engine/resources/render/DepthStencilBuffer.h"

/// Namespace containing the project.
namespace dxr
{

/// Application specific options should be added here.
class AppSpecificConfig : public quark::EngineRuntimeConfig
{
public:
    AppSpecificConfig();

    // Start of options section

    /// Periodically print ou profiler information.
    bool specificPrintProfiler{ false };

    // End of options section
}; // class AppSpecificRuntimeConfig

/// Main application class.
class App : public quark::app::Application<App, AppSpecificConfig>
{
public:
    /// Constructor called by the engine.
    App(ConfigT &cfg, quark::Engine &engine);

    virtual ~App() = default;
    virtual void initialize() override final;
    //virtual void destroy() override final;
    virtual int32_t run() override final;
    virtual void update(Milliseconds delta) override final;
    virtual void render() override final;
    //virtual void processEvents() override final;

    void onEvent(const KeyboardEvent &event) const;
    void onEvent(const MouseButtonEvent &event) const;
    void onEvent(const MouseMoveEvent &event) const;
    void onEvent(const MouseScrollEvent &event) const;
    void onEvent(const ResizeEvent &event);
    void onEvent(const ExitEvent &event);
private:
    /// Default path which will be searched for provided models.
    static constexpr const char *DEFAULT_SCENE_SEARCH_PATH_PRIMARY{ "models/" };
    /// Backup scene path, which works when the executable is in the build hierarchy.
    static constexpr const char *DEFAULT_SCENE_SEARCH_PATH_SECONDARY{ "../../../../models/" };

    /// Main binding scheme.
    quark::input::KeyBinding mBinding;

    /// Textured entities rendering layer.
    quark::rndr::BasicTexturedRL::PtrT mTexturedRl;

    /// Main camera.
    quark::helpers::math::Camera mCamera;
protected:
}; // class App
    
} // namespace App
