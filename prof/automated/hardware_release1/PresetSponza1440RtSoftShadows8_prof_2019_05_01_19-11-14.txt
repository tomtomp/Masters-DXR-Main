Configuration: 
Command line parameters  = E:\programming\FIT-DIP\Masters-DXR-Main-git\build\Measurement\x64\Release\Measurement.exe --scene Sponza/Sponza.gltf --profile-automation --profile-camera-track Sponza/SponzaPreset.trk --rt-mode --width 2560 --height 1440 --profile-output PresetSponza1440RtSoftShadows8 --quality-preset SoftShadows8 
Build type               = Release
Device name              = NVIDIA GeForce RTX 2080 Ti
Device memory MB         = 11049
Ray Tracing backend      = Hardware
Window width             = 2560
Window height            = 1421
Window title             = Direct3D App
Back-buffer count        = 3
Fill Triangles           = enabled
Triangles Font Clock-Wise= enabled
Fast Build AS            = disabled
Target FPS               = 0
Target UPS               = 60
Tearing                  = enabled
VSync                    = disabled
GUI rendering            = disabled
Debug layer              = disabled
GPU validation           = disabled
GPU profiling            = enabled
Render mode              = Ray Tracing
Loaded scene file        = Sponza/Sponza.gltf
Using testing scene      = disabled
Duplication              = 1
Duplication in cube      = disabled
Duplication offset       = 10
BLAS duplication         = disabled
Rays per pixel           = 8
Quality preset           = SoftShadows8
Rasterization Quality    : 
  AO                     = disabled
  AO kernel size         = 1
  Shadows                = enabled
  Shadow map resolution  = 8192
  Shadow PCF kernel size = 8
  Reflections            = disabled
Ray Tracing Quality      : 
  AO                     = disabled
  AO rays                = 0
  AO kernel size         = 8
  Shadows                = enabled
  Shadow rays            = 8
  Shadow kernel size     = 8
  Reflections            = disabled

ProfilingAggregator: 
Render	,	14.3723	,	13.8898	,	13.7261	,	13.3239	,	13.3387	,	13.5471	,	13.4488	,	12.9947	,	12.5906	,	12.4792	,	12.3821	,	13.5378	,	13.2835	,	14.8633	,	12.9378	,	12.4537	,	14.2939	,	13.5739	,	12.2391	,	13.7778	,	15.0849	,	14.9535	,	15.0126
Update	,	0.0735	,	0.0774	,	0.0782	,	0.0738	,	0.0746	,	0.0665	,	0.0709	,	0.0706	,	0.0766	,	0.0764	,	0.0627	,	0.0625	,	0.0722	,	0.0709	,	0.0761	,	0.0719	,	0.0639	,	0.0739	,	0.0709	,	0.0796	,	0.085	,	0.0669	,	0.0806
RayTracing	,	0.1965	,	0.1958	,	0.1802	,	0.1768	,	0.1845	,	0.1554	,	0.1755	,	0.1753	,	0.1807	,	0.1786	,	0.2215	,	0.1961	,	0.1749	,	0.1779	,	0.1822	,	0.1722	,	0.1746	,	0.1808	,	0.1732	,	0.2159	,	0.2443	,	0.1838	,	0.2087
RayTracingGpu	,	13.4895	,	12.8908	,	12.7641	,	12.5151	,	12.4612	,	12.8144	,	12.658	,	12.0444	,	11.6629	,	11.5609	,	11.5156	,	12.6836	,	12.4089	,	13.8672	,	12.0682	,	11.3979	,	13.4955	,	12.6732	,	11.2302	,	12.8026	,	13.7822	,	13.9525	,	14.155
DeferredRLGpu	,	2.86733	,	2.67894	,	2.19126	,	1.57878	,	1.36006	,	1.58963	,	2.05798	,	2.38278	,	2.07136	,	1.50627	,	1.35139	,	1.77562	,	2.22301	,	2.70416	,	2.18474	,	1.51827	,	1.96019	,	1.87536	,	1.46061	,	2.87184	,	3.84384	,	3.8591	,	3.85216
RayTracingRLGpu	,	4.9601	,	4.79757	,	5.00899	,	5.53408	,	5.68176	,	5.83258	,	5.01795	,	4.58886	,	4.5431	,	4.86314	,	4.87024	,	5.68202	,	5.11389	,	6.00442	,	4.49235	,	4.29098	,	5.98195	,	5.41946	,	4.21379	,	4.53018	,	4.43283	,	4.43014	,	4.67677
ResolveRLGpu	,	5.66003	,	5.41242	,	5.56122	,	5.40051	,	5.41747	,	5.38928	,	5.5801	,	5.07059	,	5.04528	,	5.18749	,	5.29059	,	5.22422	,	5.07002	,	5.15632	,	5.38643	,	5.58557	,	5.55123	,	5.37536	,	5.55091	,	5.39802	,	5.50349	,	5.66134	,	5.62413
Index	,	0	,	1	,	2	,	3	,	4	,	5	,	6	,	7	,	8	,	9	,	10	,	11	,	12	,	13	,	14	,	15	,	16	,	17	,	18	,	19	,	20	,	21	,	22

ProfilingBuildAggregator: 
BuildBottomLevelAS	,	1.4047
BuildBottomLevelASGpu	,	1.50186
BuildTopLevelAS	,	0.7371
BuildTopLevelASGpu	,	0.087776
Duplication	,	1
Triangles	,	262267
Meshes	,	103
BottomLevels	,	1
TopLevelSize	,	64
BottomLevelsSize	,	14622208
Index	,	0

FpsAggregator: 
FPS	,	50	,	69	,	74	,	73	,	74	,	70	,	74	,	73	,	74	,	80	,	80	,	78	,	75	,	70	,	71	,	81	,	73	,	70	,	77	,	80	,	67	,	66	,	66
UPS	,	59	,	61	,	60	,	61	,	60	,	60	,	61	,	60	,	60	,	61	,	60	,	61	,	60	,	60	,	60	,	61	,	60	,	60	,	61	,	61	,	60	,	60	,	60
FrameTime	,	20.2636	,	14.5516	,	13.6866	,	13.7481	,	13.5852	,	14.302	,	13.6631	,	13.7343	,	13.5626	,	12.6241	,	12.6309	,	12.9601	,	13.3756	,	14.2894	,	14.0946	,	12.4865	,	13.7386	,	14.3737	,	13.1166	,	12.64	,	14.9534	,	15.1761	,	15.2804
GigaRays/s	,	1.43617	,	1.99992	,	2.12632	,	2.11681	,	2.14219	,	2.03483	,	2.12998	,	2.11893	,	2.14576	,	2.30528	,	2.30403	,	2.24551	,	2.17576	,	2.03662	,	2.06477	,	2.33069	,	2.11827	,	2.02468	,	2.21872	,	2.30238	,	1.94619	,	1.91763	,	1.90453
Index	,	0	,	1	,	2	,	3	,	4	,	5	,	6	,	7	,	8	,	9	,	10	,	11	,	12	,	13	,	14	,	15	,	16	,	17	,	18	,	19	,	20	,	21	,	22


AccelerationStructure: 
Ray tracing acceleration structure building statistics: 
	Bottom Level Acceleration Structure: 
		Maximum [B]: 14622208
		Minimum [B]: 14622208
		Currently Allocated [B]: 14622208
		Currently Created [num]: 1
		Current Average [B]: 14622208
		Maximum Triangles [num]: 262267
		Minimum Triangles [num]: 262267
		Total Triangles [num]: 262267
		Maximum Meshes [num]: 103
		Minimum Meshes [num]: 103
		Total Meshes [num]: 103
	Top Level Acceleration Structure: 
		Maximum [B]: 64
		Minimum [B]: 64
		Currently Allocated [B]: 64
		Total Allocated [B]: 64
		Total Created [num]: 1
		Average Allocated [B]: 64
		Maximum Instances [num]: 1
		Minimum Instances [num]: 1
		Total Instances [num]: 1
	Scratch Buffer: 
		Maximum [B]: 11657856
		Minimum [B]: 11657856
		Currently Allocated [B]: 11657856
		Total Allocated [B]: 11657856
		Total Created [num]: 1
		Average Allocated [B]: 11657856
ProfilingStack: 
[PROF] Profiling statistics: 
	Scopes entered : 113074
	Scopes exited : 113073
	Overhead per scope [ticks] : 101
	Threads entered : 3
	Threads exited : 0
	============================
		         Sum[Mt]   Parent[%]  Samples[num]       Avg[Mt]   Last/Curr[Mt]        Self[Mt]     Self[%] 	 Name

	main:

		   26326.832 Mt   100.000 %         1 x    26326.833 Mt    26326.832 Mt        0.285 Mt     0.001 % 	 /

		   26326.565 Mt    99.999 %         1 x    26326.566 Mt    26326.565 Mt      253.011 Mt     0.961 % 	 ./Main application loop

		       2.973 Mt     0.011 %         1 x        2.973 Mt        0.000 Mt        2.973 Mt   100.000 % 	 ../TexturedRLInitialization

		       1.207 Mt     0.005 %         1 x        1.207 Mt        0.000 Mt        1.207 Mt   100.000 % 	 ../ShadowMapRLInitialization

		       4.364 Mt     0.017 %         1 x        4.364 Mt        0.000 Mt        4.364 Mt   100.000 % 	 ../RasterResolveRLInitialization

		      53.115 Mt     0.202 %         1 x       53.115 Mt        0.000 Mt        2.251 Mt     4.238 % 	 ../RayTracingRLInitialization

		      50.865 Mt    95.762 %         1 x       50.865 Mt        0.000 Mt       50.865 Mt   100.000 % 	 .../PipelineBuild

		    2824.881 Mt    10.730 %         1 x     2824.881 Mt        0.000 Mt        0.194 Mt     0.007 % 	 ../Initialization

		    2824.686 Mt    99.993 %         1 x     2824.686 Mt        0.000 Mt        2.398 Mt     0.085 % 	 .../ScenePrep

		    2759.662 Mt    97.698 %         1 x     2759.662 Mt        0.000 Mt      255.285 Mt     9.251 % 	 ..../SceneLoad

		    2175.024 Mt    78.815 %         1 x     2175.024 Mt        0.000 Mt      592.739 Mt    27.252 % 	 ...../glTF-LoadScene

		    1582.285 Mt    72.748 %        69 x       22.932 Mt        0.000 Mt     1582.285 Mt   100.000 % 	 ....../glTF-LoadImageData

		     255.923 Mt     9.274 %         1 x      255.923 Mt        0.000 Mt      255.923 Mt   100.000 % 	 ...../glTF-CreateScene

		      73.430 Mt     2.661 %         1 x       73.430 Mt        0.000 Mt       73.430 Mt   100.000 % 	 ...../SceneDesc-CreateScene

		       0.787 Mt     0.028 %         1 x        0.787 Mt        0.000 Mt        0.003 Mt     0.330 % 	 ..../ShadowMapRLPrep

		       0.785 Mt    99.670 %         1 x        0.785 Mt        0.000 Mt        0.747 Mt    95.234 % 	 ...../PrepareForRendering

		       0.037 Mt     4.766 %         1 x        0.037 Mt        0.000 Mt        0.037 Mt   100.000 % 	 ....../PrepareDrawBundle

		       3.162 Mt     0.112 %         1 x        3.162 Mt        0.000 Mt        0.001 Mt     0.019 % 	 ..../TexturedRLPrep

		       3.162 Mt    99.981 %         1 x        3.162 Mt        0.000 Mt        0.986 Mt    31.183 % 	 ...../PrepareForRendering

		       0.002 Mt     0.060 %         1 x        0.002 Mt        0.000 Mt        0.002 Mt   100.000 % 	 ....../PrepareMaterials

		       2.136 Mt    67.559 %         1 x        2.136 Mt        0.000 Mt        2.136 Mt   100.000 % 	 ....../TexturedRLInitialization

		       0.038 Mt     1.199 %         1 x        0.038 Mt        0.000 Mt        0.038 Mt   100.000 % 	 ....../PrepareDrawBundle

		       7.197 Mt     0.255 %         1 x        7.197 Mt        0.000 Mt        0.001 Mt     0.010 % 	 ..../DeferredRLPrep

		       7.196 Mt    99.990 %         1 x        7.196 Mt        0.000 Mt        0.922 Mt    12.815 % 	 ...../PrepareForRendering

		       0.001 Mt     0.014 %         1 x        0.001 Mt        0.000 Mt        0.001 Mt   100.000 % 	 ....../PrepareMaterials

		       0.494 Mt     6.870 %         1 x        0.494 Mt        0.000 Mt        0.494 Mt   100.000 % 	 ....../BuildMaterialCache

		       5.623 Mt    78.135 %         1 x        5.623 Mt        0.000 Mt        5.623 Mt   100.000 % 	 ....../DeferredRLInitialization

		       0.156 Mt     2.166 %         1 x        0.156 Mt        0.000 Mt        0.156 Mt   100.000 % 	 ....../PrepareDrawBundle

		      25.003 Mt     0.885 %         1 x       25.003 Mt        0.000 Mt        2.668 Mt    10.671 % 	 ..../RayTracingRLPrep

		       0.179 Mt     0.714 %         1 x        0.179 Mt        0.000 Mt        0.179 Mt   100.000 % 	 ...../PrepareAccelerationStructures

		       0.003 Mt     0.011 %         1 x        0.003 Mt        0.000 Mt        0.003 Mt   100.000 % 	 ...../PrepareCache

		      13.729 Mt    54.911 %         1 x       13.729 Mt        0.000 Mt       13.729 Mt   100.000 % 	 ...../PipelineBuild

		       0.712 Mt     2.846 %         1 x        0.712 Mt        0.000 Mt        0.712 Mt   100.000 % 	 ...../BuildCache

		       5.921 Mt    23.680 %         1 x        5.921 Mt        0.000 Mt        0.001 Mt     0.012 % 	 ...../BuildAccelerationStructures

		       5.920 Mt    99.988 %         1 x        5.920 Mt        0.000 Mt        3.778 Mt    63.822 % 	 ....../AccelerationStructureBuild

		       1.405 Mt    23.728 %         1 x        1.405 Mt        0.000 Mt        1.405 Mt   100.000 % 	 ......./BuildBottomLevelAS

		       0.737 Mt    12.451 %         1 x        0.737 Mt        0.000 Mt        0.737 Mt   100.000 % 	 ......./BuildTopLevelAS

		       1.792 Mt     7.166 %         1 x        1.792 Mt        0.000 Mt        1.792 Mt   100.000 % 	 ...../BuildShaderTables

		      26.477 Mt     0.937 %         1 x       26.477 Mt        0.000 Mt       26.477 Mt   100.000 % 	 ..../GpuSidePrep

		       2.741 Mt     0.010 %         1 x        2.741 Mt        0.000 Mt        2.741 Mt   100.000 % 	 ../AmbientOcclusionRLInitialization

		       6.136 Mt     0.023 %         1 x        6.136 Mt        0.000 Mt        6.136 Mt   100.000 % 	 ../DeferredRLInitialization

		       3.674 Mt     0.014 %         1 x        3.674 Mt        0.000 Mt        3.674 Mt   100.000 % 	 ../ResolveRLInitialization

		   23174.463 Mt    88.027 %     89914 x        0.258 Mt       30.499 Mt      423.544 Mt     1.828 % 	 ../MainLoop

		     182.062 Mt     0.786 %      1388 x        0.131 Mt        0.082 Mt      181.449 Mt    99.663 % 	 .../Update

		       0.613 Mt     0.337 %         1 x        0.613 Mt        0.000 Mt        0.613 Mt   100.000 % 	 ..../GuiModelApply

		   22568.857 Mt    97.387 %      1666 x       13.547 Mt       15.381 Mt      205.123 Mt     0.909 % 	 .../Render

		     314.187 Mt     1.392 %      1666 x        0.189 Mt        0.265 Mt       12.269 Mt     3.905 % 	 ..../RayTracing

		     135.100 Mt    43.000 %      1666 x        0.081 Mt        0.128 Mt      133.900 Mt    99.112 % 	 ...../Deferred

		       1.200 Mt     0.888 %      1666 x        0.001 Mt        0.001 Mt        1.200 Mt   100.000 % 	 ....../DeferredRLPrep

		     114.011 Mt    36.288 %      1666 x        0.068 Mt        0.089 Mt      113.048 Mt    99.155 % 	 ...../RayCasting

		       0.963 Mt     0.845 %      1666 x        0.001 Mt        0.001 Mt        0.963 Mt   100.000 % 	 ....../RayTracingRLPrep

		      52.807 Mt    16.808 %      1666 x        0.032 Mt        0.038 Mt       52.807 Mt   100.000 % 	 ...../Resolve

		   22049.547 Mt    97.699 %      1666 x       13.235 Mt       14.953 Mt    22049.547 Mt   100.000 % 	 ..../Present

	WindowThread:

		   26325.685 Mt   100.000 %         1 x    26325.685 Mt    26325.685 Mt    26325.685 Mt   100.000 % 	 /

	GpuThread:

		   21214.109 Mt   100.000 %      1666 x       12.734 Mt       14.452 Mt      135.661 Mt     0.639 % 	 /

		       1.597 Mt     0.008 %         1 x        1.597 Mt        0.000 Mt        0.006 Mt     0.389 % 	 ./ScenePrep

		       1.591 Mt    99.611 %         1 x        1.591 Mt        0.000 Mt        0.001 Mt     0.086 % 	 ../AccelerationStructureBuild

		       1.502 Mt    94.397 %         1 x        1.502 Mt        0.000 Mt        1.502 Mt   100.000 % 	 .../BuildBottomLevelASGpu

		       0.088 Mt     5.517 %         1 x        0.088 Mt        0.000 Mt        0.088 Mt   100.000 % 	 .../BuildTopLevelASGpu

		   21015.845 Mt    99.065 %      1666 x       12.615 Mt       14.241 Mt        4.296 Mt     0.020 % 	 ./RayTracingGpu

		    3608.477 Mt    17.170 %      1666 x        2.166 Mt        4.046 Mt     3608.477 Mt   100.000 % 	 ../DeferredRLGpu

		    8350.663 Mt    39.735 %      1666 x        5.012 Mt        4.613 Mt     8350.663 Mt   100.000 % 	 ../RayTracingRLGpu

		    9052.409 Mt    43.074 %      1666 x        5.434 Mt        5.579 Mt     9052.409 Mt   100.000 % 	 ../ResolveRLGpu

		      61.005 Mt     0.288 %      1666 x        0.037 Mt        0.211 Mt       61.005 Mt   100.000 % 	 ./Present


	============================


