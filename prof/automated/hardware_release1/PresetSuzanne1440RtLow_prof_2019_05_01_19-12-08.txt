Configuration: 
Command line parameters  = E:\programming\FIT-DIP\Masters-DXR-Main-git\build\Measurement\x64\Release\Measurement.exe --scene Suzanne/Suzanne.gltf --profile-automation --profile-camera-track Suzanne/Suzanne.trk --rt-mode --width 2560 --height 1440 --profile-output PresetSuzanne1440RtLow --quality-preset Low 
Build type               = Release
Device name              = NVIDIA GeForce RTX 2080 Ti
Device memory MB         = 11049
Ray Tracing backend      = Hardware
Window width             = 2560
Window height            = 1421
Window title             = Direct3D App
Back-buffer count        = 3
Fill Triangles           = enabled
Triangles Font Clock-Wise= enabled
Fast Build AS            = disabled
Target FPS               = 0
Target UPS               = 60
Tearing                  = enabled
VSync                    = disabled
GUI rendering            = disabled
Debug layer              = disabled
GPU validation           = disabled
GPU profiling            = enabled
Render mode              = Ray Tracing
Loaded scene file        = Suzanne/Suzanne.gltf
Using testing scene      = disabled
Duplication              = 1
Duplication in cube      = disabled
Duplication offset       = 10
BLAS duplication         = disabled
Rays per pixel           = 5
Quality preset           = Low
Rasterization Quality    : 
  AO                     = enabled
  AO kernel size         = 4
  Shadows                = enabled
  Shadow map resolution  = 2048
  Shadow PCF kernel size = 0
  Reflections            = disabled
Ray Tracing Quality      : 
  AO                     = enabled
  AO rays                = 4
  AO kernel size         = 4
  Shadows                = enabled
  Shadow rays            = 1
  Shadow kernel size     = 4
  Reflections            = disabled

ProfilingAggregator: 
Render	,	3.5766	,	3.8	,	3.9685	,	3.8008	,	3.5462	,	3.3742	,	3.28	,	3.2527	,	3.34	,	3.531	,	3.5612	,	3.7875	,	3.3115	,	3.5272	,	3.4853	,	4.0139	,	4.2605	,	4.3618	,	5.9158	,	4.5975	,	4.1198	,	3.8826	,	3.7007	,	3.3509
Update	,	0.074	,	0.0808	,	0.0739	,	0.0748	,	0.0728	,	0.0731	,	0.0734	,	0.0733	,	0.0743	,	0.075	,	0.0721	,	0.0728	,	0.072	,	0.0705	,	0.0723	,	0.0701	,	0.0729	,	0.0562	,	0.0731	,	0.0712	,	0.0651	,	0.086	,	0.073	,	0.071
RayTracing	,	0.1717	,	0.1772	,	0.1753	,	0.1705	,	0.1722	,	0.1765	,	0.1725	,	0.175	,	0.1718	,	0.1754	,	0.1737	,	0.1735	,	0.1711	,	0.1662	,	0.1704	,	0.1838	,	0.1701	,	0.117	,	0.1775	,	0.1704	,	0.1457	,	0.1737	,	0.1743	,	0.1765
RayTracingGpu	,	2.74406	,	2.9791	,	3.13472	,	3.00301	,	2.72611	,	2.55946	,	2.476	,	2.43869	,	2.53245	,	2.70886	,	2.80774	,	2.80762	,	2.52141	,	2.65139	,	2.6417	,	3.13475	,	3.4871	,	3.93325	,	5.06314	,	3.80058	,	3.37197	,	2.84669	,	2.79222	,	2.50848
DeferredRLGpu	,	0.136672	,	0.183776	,	0.19952	,	0.151456	,	0.099936	,	0.06944	,	0.052992	,	0.045504	,	0.063456	,	0.09328	,	0.104352	,	0.079776	,	0.054944	,	0.054816	,	0.088672	,	0.185984	,	0.22656	,	0.256576	,	0.404896	,	0.314144	,	0.263104	,	0.151584	,	0.081024	,	0.071232
RayTracingRLGpu	,	0.332544	,	0.490688	,	0.622464	,	0.547712	,	0.354176	,	0.232544	,	0.185696	,	0.162752	,	0.217184	,	0.34288	,	0.419488	,	0.302528	,	0.222112	,	0.191776	,	0.29264	,	0.639296	,	0.917952	,	1.3304	,	2.25722	,	1.1136	,	0.689088	,	0.32704	,	0.211712	,	0.191136
ResolveRLGpu	,	2.2719	,	2.30259	,	2.3105	,	2.30077	,	2.27018	,	2.25478	,	2.23456	,	2.22883	,	2.25024	,	2.27091	,	2.28026	,	2.4233	,	2.24272	,	2.4033	,	2.2585	,	2.30752	,	2.34029	,	2.34285	,	2.3968	,	2.37091	,	2.41818	,	2.3656	,	2.49792	,	2.24442
Index	,	0	,	1	,	2	,	3	,	4	,	5	,	6	,	7	,	8	,	9	,	10	,	11	,	12	,	13	,	14	,	15	,	16	,	17	,	18	,	19	,	20	,	21	,	22	,	23

ProfilingBuildAggregator: 
BuildBottomLevelAS	,	0.8836
BuildBottomLevelASGpu	,	0.250912
BuildTopLevelAS	,	0.9127
BuildTopLevelASGpu	,	0.060704
Duplication	,	1
Triangles	,	3936
Meshes	,	1
BottomLevels	,	1
TopLevelSize	,	64
BottomLevelsSize	,	222592
Index	,	0

FpsAggregator: 
FPS	,	228	,	259	,	249	,	248	,	264	,	283	,	293	,	293	,	297	,	283	,	273	,	275	,	290	,	297	,	289	,	278	,	235	,	218	,	187	,	189	,	224	,	260	,	284	,	295
UPS	,	59	,	60	,	60	,	61	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	61	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	61	,	60	,	60	,	60
FrameTime	,	4.39187	,	3.86351	,	4.01819	,	4.03782	,	3.79968	,	3.53375	,	3.41573	,	3.4151	,	3.37677	,	3.53484	,	3.67517	,	3.64801	,	3.45585	,	3.36727	,	3.4664	,	3.61089	,	4.27107	,	4.59289	,	5.35297	,	5.31348	,	4.48127	,	3.85504	,	3.5213	,	3.40115
GigaRays/s	,	4.14147	,	4.70784	,	4.52662	,	4.50461	,	4.78693	,	5.14717	,	5.32501	,	5.32599	,	5.38646	,	5.14558	,	4.94911	,	4.98595	,	5.26319	,	5.40164	,	5.24717	,	5.0372	,	4.25861	,	3.9602	,	3.39789	,	3.42314	,	4.05885	,	4.71819	,	5.16537	,	5.34783
Index	,	0	,	1	,	2	,	3	,	4	,	5	,	6	,	7	,	8	,	9	,	10	,	11	,	12	,	13	,	14	,	15	,	16	,	17	,	18	,	19	,	20	,	21	,	22	,	23


AccelerationStructure: 
Ray tracing acceleration structure building statistics: 
	Bottom Level Acceleration Structure: 
		Maximum [B]: 222592
		Minimum [B]: 222592
		Currently Allocated [B]: 222592
		Currently Created [num]: 1
		Current Average [B]: 222592
		Maximum Triangles [num]: 3936
		Minimum Triangles [num]: 3936
		Total Triangles [num]: 3936
		Maximum Meshes [num]: 1
		Minimum Meshes [num]: 1
		Total Meshes [num]: 1
	Top Level Acceleration Structure: 
		Maximum [B]: 64
		Minimum [B]: 64
		Currently Allocated [B]: 64
		Total Allocated [B]: 64
		Total Created [num]: 1
		Average Allocated [B]: 64
		Maximum Instances [num]: 1
		Minimum Instances [num]: 1
		Total Instances [num]: 1
	Scratch Buffer: 
		Maximum [B]: 32896
		Minimum [B]: 32896
		Currently Allocated [B]: 32896
		Total Allocated [B]: 32896
		Total Created [num]: 1
		Average Allocated [B]: 32896
ProfilingStack: 
[PROF] Profiling statistics: 
	Scopes entered : 179894
	Scopes exited : 179893
	Overhead per scope [ticks] : 100.8
	Threads entered : 3
	Threads exited : 0
	============================
		         Sum[Mt]   Parent[%]  Samples[num]       Avg[Mt]   Last/Curr[Mt]        Self[Mt]     Self[%] 	 Name

	main:

		   24552.363 Mt   100.000 %         1 x    24552.363 Mt    24552.362 Mt        0.401 Mt     0.002 % 	 /

		   24551.993 Mt    99.998 %         1 x    24551.995 Mt    24551.993 Mt      270.246 Mt     1.101 % 	 ./Main application loop

		       2.983 Mt     0.012 %         1 x        2.983 Mt        0.000 Mt        2.983 Mt   100.000 % 	 ../TexturedRLInitialization

		       1.224 Mt     0.005 %         1 x        1.224 Mt        0.000 Mt        1.224 Mt   100.000 % 	 ../ShadowMapRLInitialization

		       4.074 Mt     0.017 %         1 x        4.074 Mt        0.000 Mt        4.074 Mt   100.000 % 	 ../RasterResolveRLInitialization

		      51.911 Mt     0.211 %         1 x       51.911 Mt        0.000 Mt        3.046 Mt     5.867 % 	 ../RayTracingRLInitialization

		      48.865 Mt    94.133 %         1 x       48.865 Mt        0.000 Mt       48.865 Mt   100.000 % 	 .../PipelineBuild

		     142.859 Mt     0.582 %         1 x      142.859 Mt        0.000 Mt        0.207 Mt     0.145 % 	 ../Initialization

		     142.652 Mt    99.855 %         1 x      142.652 Mt        0.000 Mt        1.490 Mt     1.045 % 	 .../ScenePrep

		      94.823 Mt    66.472 %         1 x       94.823 Mt        0.000 Mt       20.143 Mt    21.243 % 	 ..../SceneLoad

		      60.688 Mt    64.002 %         1 x       60.688 Mt        0.000 Mt       11.564 Mt    19.055 % 	 ...../glTF-LoadScene

		      49.124 Mt    80.945 %         2 x       24.562 Mt        0.000 Mt       49.124 Mt   100.000 % 	 ....../glTF-LoadImageData

		      11.722 Mt    12.362 %         1 x       11.722 Mt        0.000 Mt       11.722 Mt   100.000 % 	 ...../glTF-CreateScene

		       2.269 Mt     2.393 %         1 x        2.269 Mt        0.000 Mt        2.269 Mt   100.000 % 	 ...../SceneDesc-CreateScene

		       0.370 Mt     0.260 %         1 x        0.370 Mt        0.000 Mt        0.006 Mt     1.485 % 	 ..../ShadowMapRLPrep

		       0.365 Mt    98.515 %         1 x        0.365 Mt        0.000 Mt        0.346 Mt    94.766 % 	 ...../PrepareForRendering

		       0.019 Mt     5.234 %         1 x        0.019 Mt        0.000 Mt        0.019 Mt   100.000 % 	 ....../PrepareDrawBundle

		       2.529 Mt     1.773 %         1 x        2.529 Mt        0.000 Mt        0.001 Mt     0.028 % 	 ..../TexturedRLPrep

		       2.528 Mt    99.972 %         1 x        2.528 Mt        0.000 Mt        0.634 Mt    25.090 % 	 ...../PrepareForRendering

		       0.003 Mt     0.107 %         1 x        0.003 Mt        0.000 Mt        0.003 Mt   100.000 % 	 ....../PrepareMaterials

		       1.884 Mt    74.534 %         1 x        1.884 Mt        0.000 Mt        1.884 Mt   100.000 % 	 ....../TexturedRLInitialization

		       0.007 Mt     0.269 %         1 x        0.007 Mt        0.000 Mt        0.007 Mt   100.000 % 	 ....../PrepareDrawBundle

		       7.302 Mt     5.119 %         1 x        7.302 Mt        0.000 Mt        0.001 Mt     0.012 % 	 ..../DeferredRLPrep

		       7.301 Mt    99.988 %         1 x        7.301 Mt        0.000 Mt        0.985 Mt    13.496 % 	 ...../PrepareForRendering

		       0.002 Mt     0.022 %         1 x        0.002 Mt        0.000 Mt        0.002 Mt   100.000 % 	 ....../PrepareMaterials

		       0.340 Mt     4.657 %         1 x        0.340 Mt        0.000 Mt        0.340 Mt   100.000 % 	 ....../BuildMaterialCache

		       5.966 Mt    81.717 %         1 x        5.966 Mt        0.000 Mt        5.966 Mt   100.000 % 	 ....../DeferredRLInitialization

		       0.008 Mt     0.108 %         1 x        0.008 Mt        0.000 Mt        0.008 Mt   100.000 % 	 ....../PrepareDrawBundle

		      24.744 Mt    17.345 %         1 x       24.744 Mt        0.000 Mt        2.624 Mt    10.603 % 	 ..../RayTracingRLPrep

		       0.101 Mt     0.408 %         1 x        0.101 Mt        0.000 Mt        0.101 Mt   100.000 % 	 ...../PrepareAccelerationStructures

		       0.002 Mt     0.007 %         1 x        0.002 Mt        0.000 Mt        0.002 Mt   100.000 % 	 ...../PrepareCache

		      15.665 Mt    63.311 %         1 x       15.665 Mt        0.000 Mt       15.665 Mt   100.000 % 	 ...../PipelineBuild

		       0.864 Mt     3.492 %         1 x        0.864 Mt        0.000 Mt        0.864 Mt   100.000 % 	 ...../BuildCache

		       4.232 Mt    17.103 %         1 x        4.232 Mt        0.000 Mt        0.001 Mt     0.014 % 	 ...../BuildAccelerationStructures

		       4.231 Mt    99.986 %         1 x        4.231 Mt        0.000 Mt        2.435 Mt    57.546 % 	 ....../AccelerationStructureBuild

		       0.884 Mt    20.883 %         1 x        0.884 Mt        0.000 Mt        0.884 Mt   100.000 % 	 ......./BuildBottomLevelAS

		       0.913 Mt    21.571 %         1 x        0.913 Mt        0.000 Mt        0.913 Mt   100.000 % 	 ......./BuildTopLevelAS

		       1.256 Mt     5.076 %         1 x        1.256 Mt        0.000 Mt        1.256 Mt   100.000 % 	 ...../BuildShaderTables

		      11.394 Mt     7.987 %         1 x       11.394 Mt        0.000 Mt       11.394 Mt   100.000 % 	 ..../GpuSidePrep

		       2.852 Mt     0.012 %         1 x        2.852 Mt        0.000 Mt        2.852 Mt   100.000 % 	 ../AmbientOcclusionRLInitialization

		       6.138 Mt     0.025 %         1 x        6.138 Mt        0.000 Mt        6.138 Mt   100.000 % 	 ../DeferredRLInitialization

		       4.301 Mt     0.018 %         1 x        4.301 Mt        0.000 Mt        4.301 Mt   100.000 % 	 ../ResolveRLInitialization

		   24065.406 Mt    98.018 %     96594 x        0.249 Mt        3.389 Mt      390.307 Mt     1.622 % 	 ../MainLoop

		     184.205 Mt     0.765 %      1444 x        0.128 Mt        0.033 Mt      183.589 Mt    99.666 % 	 .../Update

		       0.615 Mt     0.334 %         1 x        0.615 Mt        0.000 Mt        0.615 Mt   100.000 % 	 ..../GuiModelApply

		   23490.894 Mt    97.613 %      6293 x        3.733 Mt        3.267 Mt      689.247 Mt     2.934 % 	 .../Render

		    1097.077 Mt     4.670 %      6293 x        0.174 Mt        0.209 Mt       44.368 Mt     4.044 % 	 ..../RayTracing

		     493.844 Mt    45.015 %      6293 x        0.078 Mt        0.094 Mt      486.800 Mt    98.574 % 	 ...../Deferred

		       7.044 Mt     1.426 %      6293 x        0.001 Mt        0.001 Mt        7.044 Mt   100.000 % 	 ....../DeferredRLPrep

		     396.152 Mt    36.110 %      6293 x        0.063 Mt        0.080 Mt      392.539 Mt    99.088 % 	 ...../RayCasting

		       3.613 Mt     0.912 %      6293 x        0.001 Mt        0.001 Mt        3.613 Mt   100.000 % 	 ....../RayTracingRLPrep

		     162.712 Mt    14.831 %      6293 x        0.026 Mt        0.028 Mt      162.712 Mt   100.000 % 	 ...../Resolve

		   21704.571 Mt    92.396 %      6293 x        3.449 Mt        2.932 Mt    21704.571 Mt   100.000 % 	 ..../Present

	WindowThread:

		   24551.105 Mt   100.000 %         1 x    24551.105 Mt    24551.105 Mt    24551.105 Mt   100.000 % 	 /

	GpuThread:

		   18590.638 Mt   100.000 %      6293 x        2.954 Mt        2.505 Mt      128.281 Mt     0.690 % 	 /

		       0.318 Mt     0.002 %         1 x        0.318 Mt        0.000 Mt        0.006 Mt     1.731 % 	 ./ScenePrep

		       0.312 Mt    98.269 %         1 x        0.312 Mt        0.000 Mt        0.001 Mt     0.276 % 	 ../AccelerationStructureBuild

		       0.251 Mt    80.297 %         1 x        0.251 Mt        0.000 Mt        0.251 Mt   100.000 % 	 .../BuildBottomLevelASGpu

		       0.061 Mt    19.427 %         1 x        0.061 Mt        0.000 Mt        0.061 Mt   100.000 % 	 .../BuildTopLevelASGpu

		   18381.849 Mt    98.877 %      6293 x        2.921 Mt        2.502 Mt       14.799 Mt     0.081 % 	 ./RayTracingGpu

		     833.682 Mt     4.535 %      6293 x        0.132 Mt        0.070 Mt      833.682 Mt   100.000 % 	 ../DeferredRLGpu

		    2968.506 Mt    16.149 %      6293 x        0.472 Mt        0.191 Mt     2968.506 Mt   100.000 % 	 ../RayTracingRLGpu

		   14564.863 Mt    79.235 %      6293 x        2.314 Mt        2.241 Mt    14564.863 Mt   100.000 % 	 ../ResolveRLGpu

		      80.189 Mt     0.431 %      6293 x        0.013 Mt        0.003 Mt       80.189 Mt   100.000 % 	 ./Present


	============================


