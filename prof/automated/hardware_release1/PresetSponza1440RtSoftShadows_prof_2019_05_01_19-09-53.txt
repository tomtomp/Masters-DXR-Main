Configuration: 
Command line parameters  = E:\programming\FIT-DIP\Masters-DXR-Main-git\build\Measurement\x64\Release\Measurement.exe --scene Sponza/Sponza.gltf --profile-automation --profile-camera-track Sponza/SponzaPreset.trk --rt-mode --width 2560 --height 1440 --profile-output PresetSponza1440RtSoftShadows --quality-preset SoftShadows 
Build type               = Release
Device name              = NVIDIA GeForce RTX 2080 Ti
Device memory MB         = 11049
Ray Tracing backend      = Hardware
Window width             = 2560
Window height            = 1421
Window title             = Direct3D App
Back-buffer count        = 3
Fill Triangles           = enabled
Triangles Font Clock-Wise= enabled
Fast Build AS            = disabled
Target FPS               = 0
Target UPS               = 60
Tearing                  = enabled
VSync                    = disabled
GUI rendering            = disabled
Debug layer              = disabled
GPU validation           = disabled
GPU profiling            = enabled
Render mode              = Ray Tracing
Loaded scene file        = Sponza/Sponza.gltf
Using testing scene      = disabled
Duplication              = 1
Duplication in cube      = disabled
Duplication offset       = 10
BLAS duplication         = disabled
Rays per pixel           = 4
Quality preset           = SoftShadows
Rasterization Quality    : 
  AO                     = disabled
  AO kernel size         = 1
  Shadows                = enabled
  Shadow map resolution  = 4096
  Shadow PCF kernel size = 4
  Reflections            = disabled
Ray Tracing Quality      : 
  AO                     = disabled
  AO rays                = 0
  AO kernel size         = 4
  Shadows                = enabled
  Shadow rays            = 4
  Shadow kernel size     = 4
  Reflections            = disabled

ProfilingAggregator: 
Render	,	7.6728	,	7.1745	,	7.1891	,	6.6135	,	6.6786	,	7.3603	,	7.7222	,	7.8042	,	6.9989	,	6.3745	,	6.6878	,	7.5392	,	7.3831	,	8.0515	,	7.0012	,	6.2636	,	7.4258	,	7.4797	,	6.6485	,	8.1382	,	9.3384	,	9.3662	,	8.7222
Update	,	0.0718	,	0.033	,	0.0716	,	0.0713	,	0.0715	,	0.0731	,	0.0725	,	0.07	,	0.0728	,	0.0733	,	0.0742	,	0.066	,	0.0725	,	0.0719	,	0.0748	,	0.0728	,	0.0687	,	0.0727	,	0.0705	,	0.07	,	0.0712	,	0.0743	,	0.0718
RayTracing	,	0.1762	,	0.0785	,	0.1743	,	0.1732	,	0.1702	,	0.1686	,	0.1722	,	0.1734	,	0.1801	,	0.1736	,	0.1844	,	0.174	,	0.1748	,	0.1839	,	0.1758	,	0.1717	,	0.1704	,	0.1794	,	0.1707	,	0.1691	,	0.1694	,	0.1899	,	0.186
RayTracingGpu	,	6.8376	,	6.72819	,	6.3705	,	5.81779	,	5.94653	,	6.55664	,	6.6729	,	6.93702	,	6.12298	,	5.58173	,	5.78282	,	6.68246	,	6.50112	,	7.1129	,	6.15952	,	5.41811	,	6.6209	,	6.648	,	5.71216	,	7.38019	,	8.49373	,	8.49763	,	7.9584
DeferredRLGpu	,	2.71114	,	2.4872	,	2.20531	,	1.47238	,	1.34038	,	1.59248	,	1.98768	,	2.58064	,	2.07056	,	1.52541	,	1.33898	,	1.73696	,	2.17824	,	2.42813	,	2.14179	,	1.48266	,	1.95434	,	1.87965	,	1.49309	,	2.97971	,	4.15133	,	4.19146	,	3.86899
RayTracingRLGpu	,	2.38576	,	2.47037	,	2.40733	,	2.60061	,	2.84835	,	2.9407	,	2.74736	,	2.65824	,	2.34518	,	2.31738	,	2.47744	,	3.05827	,	2.60413	,	2.96157	,	2.24554	,	2.16307	,	2.88634	,	2.76288	,	2.17587	,	2.63974	,	2.55472	,	2.53238	,	2.32624
ResolveRLGpu	,	1.73872	,	1.76733	,	1.75466	,	1.74173	,	1.75533	,	2.02115	,	1.93571	,	1.69658	,	1.70528	,	1.73702	,	1.96429	,	1.88554	,	1.71622	,	1.71926	,	1.76915	,	1.77043	,	1.77731	,	2.00352	,	2.04125	,	1.75872	,	1.78579	,	1.77037	,	1.76118
Index	,	0	,	1	,	2	,	3	,	4	,	5	,	6	,	7	,	8	,	9	,	10	,	11	,	12	,	13	,	14	,	15	,	16	,	17	,	18	,	19	,	20	,	21	,	22

ProfilingBuildAggregator: 
BuildBottomLevelAS	,	1.8222
BuildBottomLevelASGpu	,	1.85981
BuildTopLevelAS	,	0.7151
BuildTopLevelASGpu	,	0.087776
Duplication	,	1
Triangles	,	262267
Meshes	,	103
BottomLevels	,	1
TopLevelSize	,	64
BottomLevelsSize	,	14622208
Index	,	0

FpsAggregator: 
FPS	,	85	,	117	,	133	,	131	,	140	,	131	,	137	,	126	,	128	,	142	,	146	,	139	,	133	,	121	,	122	,	148	,	134	,	126	,	139	,	147	,	110	,	106	,	107
UPS	,	59	,	60	,	60	,	60	,	61	,	60	,	60	,	60	,	60	,	61	,	60	,	60	,	61	,	60	,	60	,	60	,	60	,	60	,	60	,	61	,	60	,	60	,	60
FrameTime	,	11.7658	,	8.57145	,	7.53032	,	7.64071	,	7.16789	,	7.64806	,	7.33828	,	7.94484	,	7.86271	,	7.07175	,	6.88374	,	7.20811	,	7.5748	,	8.27119	,	8.19809	,	6.77687	,	7.49344	,	7.95995	,	7.21416	,	6.83363	,	9.10286	,	9.4561	,	9.4191
GigaRays/s	,	1.23672	,	1.69762	,	1.93233	,	1.90441	,	2.03003	,	1.90258	,	1.9829	,	1.83151	,	1.85064	,	2.05763	,	2.11383	,	2.0187	,	1.92098	,	1.75924	,	1.77493	,	2.14716	,	1.94184	,	1.82803	,	2.01701	,	2.12933	,	1.59851	,	1.5388	,	1.54484
Index	,	0	,	1	,	2	,	3	,	4	,	5	,	6	,	7	,	8	,	9	,	10	,	11	,	12	,	13	,	14	,	15	,	16	,	17	,	18	,	19	,	20	,	21	,	22


AccelerationStructure: 
Ray tracing acceleration structure building statistics: 
	Bottom Level Acceleration Structure: 
		Maximum [B]: 14622208
		Minimum [B]: 14622208
		Currently Allocated [B]: 14622208
		Currently Created [num]: 1
		Current Average [B]: 14622208
		Maximum Triangles [num]: 262267
		Minimum Triangles [num]: 262267
		Total Triangles [num]: 262267
		Maximum Meshes [num]: 103
		Minimum Meshes [num]: 103
		Total Meshes [num]: 103
	Top Level Acceleration Structure: 
		Maximum [B]: 64
		Minimum [B]: 64
		Currently Allocated [B]: 64
		Total Allocated [B]: 64
		Total Created [num]: 1
		Average Allocated [B]: 64
		Maximum Instances [num]: 1
		Minimum Instances [num]: 1
		Total Instances [num]: 1
	Scratch Buffer: 
		Maximum [B]: 11657856
		Minimum [B]: 11657856
		Currently Allocated [B]: 11657856
		Total Allocated [B]: 11657856
		Total Created [num]: 1
		Average Allocated [B]: 11657856
ProfilingStack: 
[PROF] Profiling statistics: 
	Scopes entered : 55975
	Scopes exited : 55974
	Overhead per scope [ticks] : 103.9
	Threads entered : 3
	Threads exited : 0
	============================
		         Sum[Mt]   Parent[%]  Samples[num]       Avg[Mt]   Last/Curr[Mt]        Self[Mt]     Self[%] 	 Name

	main:

		   26678.104 Mt   100.000 %         1 x    26678.105 Mt    26678.103 Mt        0.433 Mt     0.002 % 	 /

		   26677.703 Mt    99.998 %         1 x    26677.704 Mt    26677.703 Mt      282.287 Mt     1.058 % 	 ./Main application loop

		       2.818 Mt     0.011 %         1 x        2.818 Mt        0.000 Mt        2.818 Mt   100.000 % 	 ../TexturedRLInitialization

		       1.261 Mt     0.005 %         1 x        1.261 Mt        0.000 Mt        1.261 Mt   100.000 % 	 ../ShadowMapRLInitialization

		       3.443 Mt     0.013 %         1 x        3.443 Mt        0.000 Mt        3.443 Mt   100.000 % 	 ../RasterResolveRLInitialization

		      58.430 Mt     0.219 %         1 x       58.430 Mt        0.000 Mt        2.178 Mt     3.728 % 	 ../RayTracingRLInitialization

		      56.252 Mt    96.272 %         1 x       56.252 Mt        0.000 Mt       56.252 Mt   100.000 % 	 .../PipelineBuild

		    3190.966 Mt    11.961 %         1 x     3190.966 Mt        0.000 Mt        0.424 Mt     0.013 % 	 ../Initialization

		    3190.542 Mt    99.987 %         1 x     3190.542 Mt        0.000 Mt        2.256 Mt     0.071 % 	 .../ScenePrep

		    3084.894 Mt    96.689 %         1 x     3084.894 Mt        0.000 Mt      266.621 Mt     8.643 % 	 ..../SceneLoad

		    2144.326 Mt    69.511 %         1 x     2144.326 Mt        0.000 Mt      649.440 Mt    30.286 % 	 ...../glTF-LoadScene

		    1494.886 Mt    69.714 %        69 x       21.665 Mt        0.000 Mt     1494.886 Mt   100.000 % 	 ....../glTF-LoadImageData

		     440.483 Mt    14.279 %         1 x      440.483 Mt        0.000 Mt      440.483 Mt   100.000 % 	 ...../glTF-CreateScene

		     233.463 Mt     7.568 %         1 x      233.463 Mt        0.000 Mt      233.463 Mt   100.000 % 	 ...../SceneDesc-CreateScene

		       2.230 Mt     0.070 %         1 x        2.230 Mt        0.000 Mt        0.002 Mt     0.112 % 	 ..../ShadowMapRLPrep

		       2.227 Mt    99.888 %         1 x        2.227 Mt        0.000 Mt        2.144 Mt    96.287 % 	 ...../PrepareForRendering

		       0.083 Mt     3.713 %         1 x        0.083 Mt        0.000 Mt        0.083 Mt   100.000 % 	 ....../PrepareDrawBundle

		       7.480 Mt     0.234 %         1 x        7.480 Mt        0.000 Mt        0.001 Mt     0.009 % 	 ..../TexturedRLPrep

		       7.479 Mt    99.991 %         1 x        7.479 Mt        0.000 Mt        2.815 Mt    37.636 % 	 ...../PrepareForRendering

		       0.003 Mt     0.040 %         1 x        0.003 Mt        0.000 Mt        0.003 Mt   100.000 % 	 ....../PrepareMaterials

		       4.563 Mt    61.015 %         1 x        4.563 Mt        0.000 Mt        4.563 Mt   100.000 % 	 ....../TexturedRLInitialization

		       0.098 Mt     1.309 %         1 x        0.098 Mt        0.000 Mt        0.098 Mt   100.000 % 	 ....../PrepareDrawBundle

		      12.572 Mt     0.394 %         1 x       12.572 Mt        0.000 Mt        0.001 Mt     0.005 % 	 ..../DeferredRLPrep

		      12.571 Mt    99.995 %         1 x       12.571 Mt        0.000 Mt        1.934 Mt    15.382 % 	 ...../PrepareForRendering

		       0.001 Mt     0.011 %         1 x        0.001 Mt        0.000 Mt        0.001 Mt   100.000 % 	 ....../PrepareMaterials

		       1.369 Mt    10.888 %         1 x        1.369 Mt        0.000 Mt        1.369 Mt   100.000 % 	 ....../BuildMaterialCache

		       9.098 Mt    72.373 %         1 x        9.098 Mt        0.000 Mt        9.098 Mt   100.000 % 	 ....../DeferredRLInitialization

		       0.169 Mt     1.347 %         1 x        0.169 Mt        0.000 Mt        0.169 Mt   100.000 % 	 ....../PrepareDrawBundle

		      46.992 Mt     1.473 %         1 x       46.992 Mt        0.000 Mt        3.511 Mt     7.471 % 	 ..../RayTracingRLPrep

		       0.160 Mt     0.339 %         1 x        0.160 Mt        0.000 Mt        0.160 Mt   100.000 % 	 ...../PrepareAccelerationStructures

		       0.002 Mt     0.005 %         1 x        0.002 Mt        0.000 Mt        0.002 Mt   100.000 % 	 ...../PrepareCache

		      32.127 Mt    68.368 %         1 x       32.127 Mt        0.000 Mt       32.127 Mt   100.000 % 	 ...../PipelineBuild

		       1.177 Mt     2.505 %         1 x        1.177 Mt        0.000 Mt        1.177 Mt   100.000 % 	 ...../BuildCache

		       7.781 Mt    16.558 %         1 x        7.781 Mt        0.000 Mt        0.001 Mt     0.010 % 	 ...../BuildAccelerationStructures

		       7.780 Mt    99.990 %         1 x        7.780 Mt        0.000 Mt        5.243 Mt    67.388 % 	 ....../AccelerationStructureBuild

		       1.822 Mt    23.421 %         1 x        1.822 Mt        0.000 Mt        1.822 Mt   100.000 % 	 ......./BuildBottomLevelAS

		       0.715 Mt     9.191 %         1 x        0.715 Mt        0.000 Mt        0.715 Mt   100.000 % 	 ......./BuildTopLevelAS

		       2.233 Mt     4.753 %         1 x        2.233 Mt        0.000 Mt        2.233 Mt   100.000 % 	 ...../BuildShaderTables

		      34.120 Mt     1.069 %         1 x       34.120 Mt        0.000 Mt       34.120 Mt   100.000 % 	 ..../GpuSidePrep

		       4.233 Mt     0.016 %         1 x        4.233 Mt        0.000 Mt        4.233 Mt   100.000 % 	 ../AmbientOcclusionRLInitialization

		       5.225 Mt     0.020 %         1 x        5.225 Mt        0.000 Mt        5.225 Mt   100.000 % 	 ../DeferredRLInitialization

		       3.508 Mt     0.013 %         1 x        3.508 Mt        0.000 Mt        3.508 Mt   100.000 % 	 ../ResolveRLInitialization

		   23125.532 Mt    86.685 %     16140 x        1.433 Mt       63.881 Mt     1168.875 Mt     5.054 % 	 ../MainLoop

		     198.150 Mt     0.857 %      1384 x        0.143 Mt        0.083 Mt      197.555 Mt    99.699 % 	 .../Update

		       0.596 Mt     0.301 %         1 x        0.596 Mt        0.000 Mt        0.596 Mt   100.000 % 	 ..../GuiModelApply

		   21758.507 Mt    94.089 %      2949 x        7.378 Mt        9.034 Mt      343.408 Mt     1.578 % 	 .../Render

		     538.901 Mt     2.477 %      2949 x        0.183 Mt        0.272 Mt       20.938 Mt     3.885 % 	 ..../RayTracing

		     226.922 Mt    42.108 %      2949 x        0.077 Mt        0.133 Mt      223.710 Mt    98.585 % 	 ...../Deferred

		       3.212 Mt     1.415 %      2949 x        0.001 Mt        0.001 Mt        3.212 Mt   100.000 % 	 ....../DeferredRLPrep

		     199.953 Mt    37.104 %      2949 x        0.068 Mt        0.090 Mt      198.331 Mt    99.189 % 	 ...../RayCasting

		       1.622 Mt     0.811 %      2949 x        0.001 Mt        0.001 Mt        1.622 Mt   100.000 % 	 ....../RayTracingRLPrep

		      91.089 Mt    16.903 %      2949 x        0.031 Mt        0.039 Mt       91.089 Mt   100.000 % 	 ...../Resolve

		   20876.197 Mt    95.945 %      2949 x        7.079 Mt        8.591 Mt    20876.197 Mt   100.000 % 	 ..../Present

	WindowThread:

		   26670.663 Mt   100.000 %         1 x    26670.664 Mt    26670.663 Mt    26670.663 Mt   100.000 % 	 /

	GpuThread:

		   19441.474 Mt   100.000 %      2949 x        6.593 Mt        7.976 Mt      163.374 Mt     0.840 % 	 /

		       1.955 Mt     0.010 %         1 x        1.955 Mt        0.000 Mt        0.006 Mt     0.319 % 	 ./ScenePrep

		       1.949 Mt    99.681 %         1 x        1.949 Mt        0.000 Mt        0.001 Mt     0.072 % 	 ../AccelerationStructureBuild

		       1.860 Mt    95.424 %         1 x        1.860 Mt        0.000 Mt        1.860 Mt   100.000 % 	 .../BuildBottomLevelASGpu

		       0.088 Mt     4.504 %         1 x        0.088 Mt        0.000 Mt        0.088 Mt   100.000 % 	 .../BuildTopLevelASGpu

		   19228.089 Mt    98.902 %      2949 x        6.520 Mt        7.972 Mt        8.141 Mt     0.042 % 	 ./RayTracingGpu

		    6279.744 Mt    32.659 %      2949 x        2.129 Mt        3.873 Mt     6279.744 Mt   100.000 % 	 ../DeferredRLGpu

		    7604.923 Mt    39.551 %      2949 x        2.579 Mt        2.330 Mt     7604.923 Mt   100.000 % 	 ../RayTracingRLGpu

		    5335.280 Mt    27.747 %      2949 x        1.809 Mt        1.768 Mt     5335.280 Mt   100.000 % 	 ../ResolveRLGpu

		      48.057 Mt     0.247 %      2949 x        0.016 Mt        0.002 Mt       48.057 Mt   100.000 % 	 ./Present


	============================


