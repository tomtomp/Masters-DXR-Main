Configuration: 
Command line parameters  = E:\programming\FIT-DIP\Masters-DXR-Main-git\build\Measurement\x64\Release\Measurement.exe --scene Suzanne/Suzanne.gltf --profile-automation --profile-camera-track Suzanne/Suzanne.trk --rt-mode --width 2560 --height 1440 --profile-output PresetSuzanne1440RtHigh --quality-preset High 
Build type               = Release
Device name              = NVIDIA GeForce RTX 2080 Ti
Device memory MB         = 11049
Ray Tracing backend      = Hardware
Window width             = 2560
Window height            = 1421
Window title             = Direct3D App
Back-buffer count        = 3
Fill Triangles           = enabled
Triangles Font Clock-Wise= enabled
Fast Build AS            = disabled
Target FPS               = 0
Target UPS               = 60
Tearing                  = enabled
VSync                    = disabled
GUI rendering            = disabled
Debug layer              = disabled
GPU validation           = disabled
GPU profiling            = enabled
Render mode              = Ray Tracing
Loaded scene file        = Suzanne/Suzanne.gltf
Using testing scene      = disabled
Duplication              = 1
Duplication in cube      = disabled
Duplication offset       = 10
BLAS duplication         = disabled
Rays per pixel           = 13
Quality preset           = High
Rasterization Quality    : 
  AO                     = enabled
  AO kernel size         = 4
  Shadows                = enabled
  Shadow map resolution  = 4096
  Shadow PCF kernel size = 4
  Reflections            = enabled
Ray Tracing Quality      : 
  AO                     = enabled
  AO rays                = 4
  AO kernel size         = 4
  Shadows                = enabled
  Shadow rays            = 4
  Shadow kernel size     = 4
  Reflections            = enabled

ProfilingAggregator: 
Render	,	3.9039	,	4.2697	,	3.9999	,	4.1905	,	3.7218	,	3.4734	,	3.4307	,	3.3897	,	3.4048	,	3.6672	,	4.2725	,	3.7047	,	3.5436	,	3.4901	,	3.6731	,	4.4369	,	5.0157	,	5.7578	,	7.5383	,	5.5239	,	4.634	,	4.126	,	3.4626	,	3.529
Update	,	0.0669	,	0.0724	,	0.0402	,	0.0721	,	0.0693	,	0.0656	,	0.069	,	0.0667	,	0.0718	,	0.0734	,	0.0717	,	0.0728	,	0.071	,	0.0731	,	0.0732	,	0.0686	,	0.0718	,	0.0723	,	0.0714	,	0.0738	,	0.0714	,	0.0717	,	0.0709	,	0.0712
RayTracing	,	0.1764	,	0.179	,	0.0697	,	0.1778	,	0.1734	,	0.1739	,	0.1701	,	0.1746	,	0.171	,	0.1693	,	0.1689	,	0.1726	,	0.1736	,	0.1721	,	0.1714	,	0.1728	,	0.1744	,	0.1796	,	0.1741	,	0.1684	,	0.171	,	0.1909	,	0.1753	,	0.175
RayTracingGpu	,	3.0177	,	3.3551	,	3.57443	,	3.36909	,	2.9631	,	2.7199	,	2.61366	,	2.57062	,	2.68205	,	2.94205	,	3.28451	,	2.8481	,	2.63712	,	2.63328	,	2.85619	,	3.5776	,	4.14941	,	4.92563	,	6.7025	,	4.7496	,	3.84067	,	3.23888	,	2.70547	,	2.65699
DeferredRLGpu	,	0.138816	,	0.183488	,	0.195872	,	0.153248	,	0.10048	,	0.070208	,	0.052992	,	0.045792	,	0.062624	,	0.093248	,	0.124128	,	0.078752	,	0.0576	,	0.05568	,	0.087232	,	0.184928	,	0.226784	,	0.257632	,	0.410432	,	0.311904	,	0.262784	,	0.133344	,	0.081408	,	0.069984
RayTracingRLGpu	,	0.532256	,	0.806592	,	1.00083	,	0.846496	,	0.52432	,	0.318528	,	0.242272	,	0.205376	,	0.29408	,	0.50528	,	0.627616	,	0.436672	,	0.255616	,	0.2544	,	0.432928	,	1.01142	,	1.52486	,	2.25782	,	3.83341	,	2.00461	,	1.1953	,	0.50976	,	0.301184	,	0.26176
ResolveRLGpu	,	2.34362	,	2.36285	,	2.37555	,	2.36589	,	2.33642	,	2.32944	,	2.31642	,	2.31786	,	2.32362	,	2.34157	,	2.53056	,	2.33078	,	2.32064	,	2.32128	,	2.33446	,	2.37786	,	2.39443	,	2.40803	,	2.45501	,	2.43104	,	2.38048	,	2.59408	,	2.32115	,	2.32346
Index	,	0	,	1	,	2	,	3	,	4	,	5	,	6	,	7	,	8	,	9	,	10	,	11	,	12	,	13	,	14	,	15	,	16	,	17	,	18	,	19	,	20	,	21	,	22	,	23

ProfilingBuildAggregator: 
BuildBottomLevelAS	,	0.8875
BuildBottomLevelASGpu	,	0.25568
BuildTopLevelAS	,	0.8068
BuildTopLevelASGpu	,	0.060896
Duplication	,	1
Triangles	,	3936
Meshes	,	1
BottomLevels	,	1
TopLevelSize	,	64
BottomLevelsSize	,	222592
Index	,	0

FpsAggregator: 
FPS	,	207	,	236	,	226	,	224	,	244	,	268	,	282	,	287	,	282	,	270	,	252	,	257	,	275	,	290	,	276	,	244	,	207	,	183	,	145	,	152	,	190	,	234	,	269	,	281
UPS	,	59	,	60	,	61	,	60	,	60	,	60	,	60	,	60	,	60	,	61	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	61	,	60	,	60	,	60	,	60
FrameTime	,	4.84406	,	4.24215	,	4.4405	,	4.47427	,	4.11291	,	3.73393	,	3.55827	,	3.49016	,	3.54921	,	3.71223	,	3.97692	,	3.90213	,	3.64463	,	3.44925	,	3.6345	,	4.1	,	4.83919	,	5.47878	,	6.91393	,	6.59058	,	5.2784	,	4.28973	,	3.72929	,	3.56025
GigaRays/s	,	9.76266	,	11.1479	,	10.6499	,	10.5695	,	11.4981	,	12.6652	,	13.2904	,	13.5498	,	13.3244	,	12.7392	,	11.8913	,	12.1193	,	12.9755	,	13.7105	,	13.0116	,	11.5344	,	9.77247	,	8.63164	,	6.83994	,	7.17553	,	8.95932	,	11.0242	,	12.6809	,	13.283
Index	,	0	,	1	,	2	,	3	,	4	,	5	,	6	,	7	,	8	,	9	,	10	,	11	,	12	,	13	,	14	,	15	,	16	,	17	,	18	,	19	,	20	,	21	,	22	,	23


AccelerationStructure: 
Ray tracing acceleration structure building statistics: 
	Bottom Level Acceleration Structure: 
		Maximum [B]: 222592
		Minimum [B]: 222592
		Currently Allocated [B]: 222592
		Currently Created [num]: 1
		Current Average [B]: 222592
		Maximum Triangles [num]: 3936
		Minimum Triangles [num]: 3936
		Total Triangles [num]: 3936
		Maximum Meshes [num]: 1
		Minimum Meshes [num]: 1
		Total Meshes [num]: 1
	Top Level Acceleration Structure: 
		Maximum [B]: 64
		Minimum [B]: 64
		Currently Allocated [B]: 64
		Total Allocated [B]: 64
		Total Created [num]: 1
		Average Allocated [B]: 64
		Maximum Instances [num]: 1
		Minimum Instances [num]: 1
		Total Instances [num]: 1
	Scratch Buffer: 
		Maximum [B]: 32896
		Minimum [B]: 32896
		Currently Allocated [B]: 32896
		Total Allocated [B]: 32896
		Total Created [num]: 1
		Average Allocated [B]: 32896
ProfilingStack: 
[PROF] Profiling statistics: 
	Scopes entered : 109568
	Scopes exited : 109567
	Overhead per scope [ticks] : 111.5
	Threads entered : 3
	Threads exited : 0
	============================
		         Sum[Mt]   Parent[%]  Samples[num]       Avg[Mt]   Last/Curr[Mt]        Self[Mt]     Self[%] 	 Name

	main:

		   24528.135 Mt   100.000 %         1 x    24528.136 Mt    24528.135 Mt        0.264 Mt     0.001 % 	 /

		   24527.902 Mt    99.999 %         1 x    24527.904 Mt    24527.902 Mt      270.179 Mt     1.102 % 	 ./Main application loop

		       4.238 Mt     0.017 %         1 x        4.238 Mt        0.000 Mt        4.238 Mt   100.000 % 	 ../TexturedRLInitialization

		       1.696 Mt     0.007 %         1 x        1.696 Mt        0.000 Mt        1.696 Mt   100.000 % 	 ../ShadowMapRLInitialization

		       8.036 Mt     0.033 %         1 x        8.036 Mt        0.000 Mt        8.036 Mt   100.000 % 	 ../RasterResolveRLInitialization

		      52.786 Mt     0.215 %         1 x       52.786 Mt        0.000 Mt        3.372 Mt     6.387 % 	 ../RayTracingRLInitialization

		      49.415 Mt    93.613 %         1 x       49.415 Mt        0.000 Mt       49.415 Mt   100.000 % 	 .../PipelineBuild

		     109.438 Mt     0.446 %         1 x      109.438 Mt        0.000 Mt        0.177 Mt     0.161 % 	 ../Initialization

		     109.261 Mt    99.839 %         1 x      109.261 Mt        0.000 Mt        0.919 Mt     0.842 % 	 .../ScenePrep

		      67.250 Mt    61.550 %         1 x       67.250 Mt        0.000 Mt       14.919 Mt    22.184 % 	 ..../SceneLoad

		      39.295 Mt    58.432 %         1 x       39.295 Mt        0.000 Mt        8.395 Mt    21.365 % 	 ...../glTF-LoadScene

		      30.900 Mt    78.635 %         2 x       15.450 Mt        0.000 Mt       30.900 Mt   100.000 % 	 ....../glTF-LoadImageData

		      11.908 Mt    17.707 %         1 x       11.908 Mt        0.000 Mt       11.908 Mt   100.000 % 	 ...../glTF-CreateScene

		       1.127 Mt     1.677 %         1 x        1.127 Mt        0.000 Mt        1.127 Mt   100.000 % 	 ...../SceneDesc-CreateScene

		       0.265 Mt     0.242 %         1 x        0.265 Mt        0.000 Mt        0.002 Mt     0.831 % 	 ..../ShadowMapRLPrep

		       0.263 Mt    99.169 %         1 x        0.263 Mt        0.000 Mt        0.254 Mt    96.724 % 	 ...../PrepareForRendering

		       0.009 Mt     3.276 %         1 x        0.009 Mt        0.000 Mt        0.009 Mt   100.000 % 	 ....../PrepareDrawBundle

		       1.853 Mt     1.696 %         1 x        1.853 Mt        0.000 Mt        0.000 Mt     0.027 % 	 ..../TexturedRLPrep

		       1.852 Mt    99.973 %         1 x        1.852 Mt        0.000 Mt        0.570 Mt    30.765 % 	 ...../PrepareForRendering

		       0.001 Mt     0.065 %         1 x        0.001 Mt        0.000 Mt        0.001 Mt   100.000 % 	 ....../PrepareMaterials

		       1.278 Mt    68.981 %         1 x        1.278 Mt        0.000 Mt        1.278 Mt   100.000 % 	 ....../TexturedRLInitialization

		       0.004 Mt     0.189 %         1 x        0.004 Mt        0.000 Mt        0.004 Mt   100.000 % 	 ....../PrepareDrawBundle

		       5.266 Mt     4.819 %         1 x        5.266 Mt        0.000 Mt        0.001 Mt     0.009 % 	 ..../DeferredRLPrep

		       5.265 Mt    99.991 %         1 x        5.265 Mt        0.000 Mt        0.853 Mt    16.207 % 	 ...../PrepareForRendering

		       0.001 Mt     0.017 %         1 x        0.001 Mt        0.000 Mt        0.001 Mt   100.000 % 	 ....../PrepareMaterials

		       0.299 Mt     5.683 %         1 x        0.299 Mt        0.000 Mt        0.299 Mt   100.000 % 	 ....../BuildMaterialCache

		       4.108 Mt    78.021 %         1 x        4.108 Mt        0.000 Mt        4.108 Mt   100.000 % 	 ....../DeferredRLInitialization

		       0.004 Mt     0.072 %         1 x        0.004 Mt        0.000 Mt        0.004 Mt   100.000 % 	 ....../PrepareDrawBundle

		      22.801 Mt    20.869 %         1 x       22.801 Mt        0.000 Mt        2.179 Mt     9.556 % 	 ..../RayTracingRLPrep

		       0.048 Mt     0.209 %         1 x        0.048 Mt        0.000 Mt        0.048 Mt   100.000 % 	 ...../PrepareAccelerationStructures

		       0.001 Mt     0.004 %         1 x        0.001 Mt        0.000 Mt        0.001 Mt   100.000 % 	 ...../PrepareCache

		      12.201 Mt    53.511 %         1 x       12.201 Mt        0.000 Mt       12.201 Mt   100.000 % 	 ...../PipelineBuild

		       0.323 Mt     1.414 %         1 x        0.323 Mt        0.000 Mt        0.323 Mt   100.000 % 	 ...../BuildCache

		       6.490 Mt    28.465 %         1 x        6.490 Mt        0.000 Mt        0.001 Mt     0.018 % 	 ...../BuildAccelerationStructures

		       6.489 Mt    99.982 %         1 x        6.489 Mt        0.000 Mt        4.795 Mt    73.890 % 	 ....../AccelerationStructureBuild

		       0.887 Mt    13.677 %         1 x        0.887 Mt        0.000 Mt        0.887 Mt   100.000 % 	 ......./BuildBottomLevelAS

		       0.807 Mt    12.433 %         1 x        0.807 Mt        0.000 Mt        0.807 Mt   100.000 % 	 ......./BuildTopLevelAS

		       1.560 Mt     6.841 %         1 x        1.560 Mt        0.000 Mt        1.560 Mt   100.000 % 	 ...../BuildShaderTables

		      10.908 Mt     9.983 %         1 x       10.908 Mt        0.000 Mt       10.908 Mt   100.000 % 	 ..../GpuSidePrep

		       4.442 Mt     0.018 %         1 x        4.442 Mt        0.000 Mt        4.442 Mt   100.000 % 	 ../AmbientOcclusionRLInitialization

		       5.747 Mt     0.023 %         1 x        5.747 Mt        0.000 Mt        5.747 Mt   100.000 % 	 ../DeferredRLInitialization

		       3.697 Mt     0.015 %         1 x        3.697 Mt        0.000 Mt        3.697 Mt   100.000 % 	 ../ResolveRLInitialization

		   24067.643 Mt    98.124 %     32912 x        0.731 Mt       21.280 Mt      396.668 Mt     1.648 % 	 ../MainLoop

		     200.532 Mt     0.833 %      1443 x        0.139 Mt        0.089 Mt      200.216 Mt    99.843 % 	 .../Update

		       0.316 Mt     0.157 %         1 x        0.316 Mt        0.000 Mt        0.316 Mt   100.000 % 	 ..../GuiModelApply

		   23470.443 Mt    97.519 %      5782 x        4.059 Mt        3.792 Mt      645.702 Mt     2.751 % 	 .../Render

		    1013.968 Mt     4.320 %      5782 x        0.175 Mt        0.287 Mt       40.143 Mt     3.959 % 	 ..../RayTracing

		     456.785 Mt    45.049 %      5782 x        0.079 Mt        0.150 Mt      450.038 Mt    98.523 % 	 ...../Deferred

		       6.747 Mt     1.477 %      5782 x        0.001 Mt        0.001 Mt        6.747 Mt   100.000 % 	 ....../DeferredRLPrep

		     366.660 Mt    36.161 %      5782 x        0.063 Mt        0.097 Mt      363.355 Mt    99.099 % 	 ...../RayCasting

		       3.305 Mt     0.901 %      5782 x        0.001 Mt        0.001 Mt        3.305 Mt   100.000 % 	 ....../RayTracingRLPrep

		     150.380 Mt    14.831 %      5782 x        0.026 Mt        0.031 Mt      150.380 Mt   100.000 % 	 ...../Resolve

		   21810.773 Mt    92.929 %      5782 x        3.772 Mt        3.333 Mt    21810.773 Mt   100.000 % 	 ..../Present

	WindowThread:

		   24527.581 Mt   100.000 %         1 x    24527.581 Mt    24527.581 Mt    24527.581 Mt   100.000 % 	 /

	GpuThread:

		   18900.079 Mt   100.000 %      5782 x        3.269 Mt        2.829 Mt      142.101 Mt     0.752 % 	 /

		       0.323 Mt     0.002 %         1 x        0.323 Mt        0.000 Mt        0.006 Mt     1.733 % 	 ./ScenePrep

		       0.318 Mt    98.267 %         1 x        0.318 Mt        0.000 Mt        0.001 Mt     0.322 % 	 ../AccelerationStructureBuild

		       0.256 Mt    80.504 %         1 x        0.256 Mt        0.000 Mt        0.256 Mt   100.000 % 	 .../BuildBottomLevelASGpu

		       0.061 Mt    19.174 %         1 x        0.061 Mt        0.000 Mt        0.061 Mt   100.000 % 	 .../BuildTopLevelASGpu

		   18674.589 Mt    98.807 %      5782 x        3.230 Mt        2.741 Mt       13.367 Mt     0.072 % 	 ./RayTracingGpu

		     741.919 Mt     3.973 %      5782 x        0.128 Mt        0.072 Mt      741.919 Mt   100.000 % 	 ../DeferredRLGpu

		    4134.850 Mt    22.142 %      5782 x        0.715 Mt        0.261 Mt     4134.850 Mt   100.000 % 	 ../RayTracingRLGpu

		   13784.453 Mt    73.814 %      5782 x        2.384 Mt        2.408 Mt    13784.453 Mt   100.000 % 	 ../ResolveRLGpu

		      83.066 Mt     0.439 %      5782 x        0.014 Mt        0.088 Mt       83.066 Mt   100.000 % 	 ./Present


	============================


