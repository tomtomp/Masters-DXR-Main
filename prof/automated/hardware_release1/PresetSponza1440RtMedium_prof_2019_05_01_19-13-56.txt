Configuration: 
Command line parameters  = E:\programming\FIT-DIP\Masters-DXR-Main-git\build\Measurement\x64\Release\Measurement.exe --scene Sponza/Sponza.gltf --profile-automation --profile-camera-track Sponza/SponzaPreset.trk --rt-mode --width 2560 --height 1440 --profile-output PresetSponza1440RtMedium --quality-preset Medium 
Build type               = Release
Device name              = NVIDIA GeForce RTX 2080 Ti
Device memory MB         = 11049
Ray Tracing backend      = Hardware
Window width             = 2560
Window height            = 1421
Window title             = Direct3D App
Back-buffer count        = 3
Fill Triangles           = enabled
Triangles Font Clock-Wise= enabled
Fast Build AS            = disabled
Target FPS               = 0
Target UPS               = 60
Tearing                  = enabled
VSync                    = disabled
GUI rendering            = disabled
Debug layer              = disabled
GPU validation           = disabled
GPU profiling            = enabled
Render mode              = Ray Tracing
Loaded scene file        = Sponza/Sponza.gltf
Using testing scene      = disabled
Duplication              = 1
Duplication in cube      = disabled
Duplication offset       = 10
BLAS duplication         = disabled
Rays per pixel           = 8
Quality preset           = Medium
Rasterization Quality    : 
  AO                     = enabled
  AO kernel size         = 4
  Shadows                = enabled
  Shadow map resolution  = 4096
  Shadow PCF kernel size = 4
  Reflections            = disabled
Ray Tracing Quality      : 
  AO                     = enabled
  AO rays                = 4
  AO kernel size         = 4
  Shadows                = enabled
  Shadow rays            = 4
  Shadow kernel size     = 4
  Reflections            = disabled

ProfilingAggregator: 
Render	,	10.7003	,	10.4188	,	9.9467	,	9.5645	,	9.8283	,	10.286	,	9.5518	,	10.2259	,	9.8533	,	8.8227	,	8.9162	,	9.9037	,	10.3197	,	11.1306	,	9.8306	,	9.2097	,	11.4227	,	10.9101	,	9.527	,	10.4782	,	11.5509	,	11.0367	,	11.2151
Update	,	0.0727	,	0.0712	,	0.0746	,	0.0712	,	0.0725	,	0.0712	,	0.072	,	0.0716	,	0.0736	,	0.0727	,	0.0734	,	0.0709	,	0.0723	,	0.0699	,	0.0747	,	0.1126	,	0.0693	,	0.0748	,	0.0715	,	0.0734	,	0.0714	,	0.0726	,	0.0649
RayTracing	,	0.1774	,	0.1786	,	0.1788	,	0.1703	,	0.1751	,	0.1739	,	0.1771	,	0.1719	,	0.1736	,	0.1755	,	0.1755	,	0.2029	,	0.2062	,	0.2057	,	0.1785	,	0.1804	,	0.1776	,	0.1747	,	0.1738	,	0.1786	,	0.1786	,	0.1777	,	0.1758
RayTracingGpu	,	9.69462	,	9.52154	,	9.09229	,	8.80381	,	9.1319	,	9.43747	,	8.668	,	9.42355	,	9.02582	,	7.95008	,	7.98301	,	8.9649	,	9.22128	,	10.1964	,	8.85981	,	8.30064	,	10.5293	,	10.0878	,	8.71811	,	9.58522	,	10.7209	,	10.2004	,	10.2537
DeferredRLGpu	,	2.92554	,	2.53949	,	2.18749	,	1.47456	,	1.3449	,	1.58675	,	2.00723	,	2.40278	,	2.3312	,	1.51562	,	1.34963	,	1.74464	,	2.17235	,	2.44326	,	2.17434	,	1.49459	,	1.97158	,	1.87402	,	1.48083	,	2.76134	,	3.86598	,	3.83277	,	3.86806
RayTracingRLGpu	,	4.2632	,	4.44355	,	4.37699	,	4.56067	,	4.97693	,	5.15987	,	4.12973	,	4.32173	,	4.23344	,	3.98154	,	4.17299	,	4.48691	,	4.40394	,	5.00547	,	4.15955	,	4.28886	,	5.89229	,	5.54592	,	4.54819	,	4.27677	,	4.10973	,	3.85437	,	3.85466
ResolveRLGpu	,	2.50394	,	2.53165	,	2.52595	,	2.76544	,	2.8079	,	2.68909	,	2.52912	,	2.69696	,	2.4591	,	2.44906	,	2.45811	,	2.73123	,	2.64314	,	2.74614	,	2.52278	,	2.51299	,	2.66368	,	2.66634	,	2.68726	,	2.54211	,	2.74326	,	2.51027	,	2.52797
Index	,	0	,	1	,	2	,	3	,	4	,	5	,	6	,	7	,	8	,	9	,	10	,	11	,	12	,	13	,	14	,	15	,	16	,	17	,	18	,	19	,	20	,	21	,	22

ProfilingBuildAggregator: 
BuildBottomLevelAS	,	1.5325
BuildBottomLevelASGpu	,	1.50624
BuildTopLevelAS	,	0.8001
BuildTopLevelASGpu	,	0.089184
Duplication	,	1
Triangles	,	262267
Meshes	,	103
BottomLevels	,	1
TopLevelSize	,	64
BottomLevelsSize	,	14622208
Index	,	0

FpsAggregator: 
FPS	,	63	,	90	,	98	,	100	,	105	,	98	,	104	,	98	,	99	,	107	,	109	,	106	,	101	,	94	,	94	,	107	,	95	,	89	,	98	,	105	,	89	,	87	,	87
UPS	,	59	,	60	,	61	,	60	,	60	,	60	,	61	,	60	,	60	,	61	,	60	,	60	,	60	,	60	,	61	,	60	,	61	,	60	,	60	,	60	,	61	,	60	,	60
FrameTime	,	15.9412	,	11.1445	,	10.2529	,	10.0596	,	9.53964	,	10.2279	,	9.66985	,	10.2992	,	10.1526	,	9.37622	,	9.18637	,	9.51794	,	9.91299	,	10.6581	,	10.7261	,	9.36822	,	10.621	,	11.249	,	10.2193	,	9.58249	,	11.3553	,	11.5886	,	11.5166
GigaRays/s	,	1.82558	,	2.61134	,	2.83841	,	2.89298	,	3.05065	,	2.84536	,	3.00957	,	2.82567	,	2.86647	,	3.10382	,	3.16796	,	3.0576	,	2.93575	,	2.7305	,	2.7132	,	3.10647	,	2.74005	,	2.58707	,	2.84776	,	3.03701	,	2.56287	,	2.51128	,	2.52697
Index	,	0	,	1	,	2	,	3	,	4	,	5	,	6	,	7	,	8	,	9	,	10	,	11	,	12	,	13	,	14	,	15	,	16	,	17	,	18	,	19	,	20	,	21	,	22


AccelerationStructure: 
Ray tracing acceleration structure building statistics: 
	Bottom Level Acceleration Structure: 
		Maximum [B]: 14622208
		Minimum [B]: 14622208
		Currently Allocated [B]: 14622208
		Currently Created [num]: 1
		Current Average [B]: 14622208
		Maximum Triangles [num]: 262267
		Minimum Triangles [num]: 262267
		Total Triangles [num]: 262267
		Maximum Meshes [num]: 103
		Minimum Meshes [num]: 103
		Total Meshes [num]: 103
	Top Level Acceleration Structure: 
		Maximum [B]: 64
		Minimum [B]: 64
		Currently Allocated [B]: 64
		Total Allocated [B]: 64
		Total Created [num]: 1
		Average Allocated [B]: 64
		Maximum Instances [num]: 1
		Minimum Instances [num]: 1
		Total Instances [num]: 1
	Scratch Buffer: 
		Maximum [B]: 11657856
		Minimum [B]: 11657856
		Currently Allocated [B]: 11657856
		Total Allocated [B]: 11657856
		Total Created [num]: 1
		Average Allocated [B]: 11657856
ProfilingStack: 
[PROF] Profiling statistics: 
	Scopes entered : 62802
	Scopes exited : 62801
	Overhead per scope [ticks] : 103.8
	Threads entered : 3
	Threads exited : 0
	============================
		         Sum[Mt]   Parent[%]  Samples[num]       Avg[Mt]   Last/Curr[Mt]        Self[Mt]     Self[%] 	 Name

	main:

		   26121.721 Mt   100.000 %         1 x    26121.722 Mt    26121.721 Mt        0.297 Mt     0.001 % 	 /

		   26121.459 Mt    99.999 %         1 x    26121.460 Mt    26121.459 Mt      244.907 Mt     0.938 % 	 ./Main application loop

		       4.286 Mt     0.016 %         1 x        4.286 Mt        0.000 Mt        4.286 Mt   100.000 % 	 ../TexturedRLInitialization

		       1.774 Mt     0.007 %         1 x        1.774 Mt        0.000 Mt        1.774 Mt   100.000 % 	 ../ShadowMapRLInitialization

		       4.886 Mt     0.019 %         1 x        4.886 Mt        0.000 Mt        4.886 Mt   100.000 % 	 ../RasterResolveRLInitialization

		      49.951 Mt     0.191 %         1 x       49.951 Mt        0.000 Mt        2.384 Mt     4.772 % 	 ../RayTracingRLInitialization

		      47.567 Mt    95.228 %         1 x       47.567 Mt        0.000 Mt       47.567 Mt   100.000 % 	 .../PipelineBuild

		    2665.044 Mt    10.203 %         1 x     2665.044 Mt        0.000 Mt        0.300 Mt     0.011 % 	 ../Initialization

		    2664.745 Mt    99.989 %         1 x     2664.745 Mt        0.000 Mt        1.911 Mt     0.072 % 	 .../ScenePrep

		    2593.753 Mt    97.336 %         1 x     2593.753 Mt        0.000 Mt      270.233 Mt    10.419 % 	 ..../SceneLoad

		    2029.643 Mt    78.251 %         1 x     2029.643 Mt        0.000 Mt      607.489 Mt    29.931 % 	 ...../glTF-LoadScene

		    1422.154 Mt    70.069 %        69 x       20.611 Mt        0.000 Mt     1422.154 Mt   100.000 % 	 ....../glTF-LoadImageData

		     205.356 Mt     7.917 %         1 x      205.356 Mt        0.000 Mt      205.356 Mt   100.000 % 	 ...../glTF-CreateScene

		      88.522 Mt     3.413 %         1 x       88.522 Mt        0.000 Mt       88.522 Mt   100.000 % 	 ...../SceneDesc-CreateScene

		       1.013 Mt     0.038 %         1 x        1.013 Mt        0.000 Mt        0.003 Mt     0.306 % 	 ..../ShadowMapRLPrep

		       1.010 Mt    99.694 %         1 x        1.010 Mt        0.000 Mt        0.969 Mt    95.920 % 	 ...../PrepareForRendering

		       0.041 Mt     4.080 %         1 x        0.041 Mt        0.000 Mt        0.041 Mt   100.000 % 	 ....../PrepareDrawBundle

		       3.479 Mt     0.131 %         1 x        3.479 Mt        0.000 Mt        0.001 Mt     0.017 % 	 ..../TexturedRLPrep

		       3.479 Mt    99.983 %         1 x        3.479 Mt        0.000 Mt        1.018 Mt    29.278 % 	 ...../PrepareForRendering

		       0.001 Mt     0.037 %         1 x        0.001 Mt        0.000 Mt        0.001 Mt   100.000 % 	 ....../PrepareMaterials

		       2.421 Mt    69.589 %         1 x        2.421 Mt        0.000 Mt        2.421 Mt   100.000 % 	 ....../TexturedRLInitialization

		       0.038 Mt     1.095 %         1 x        0.038 Mt        0.000 Mt        0.038 Mt   100.000 % 	 ....../PrepareDrawBundle

		       7.407 Mt     0.278 %         1 x        7.407 Mt        0.000 Mt        0.000 Mt     0.007 % 	 ..../DeferredRLPrep

		       7.406 Mt    99.993 %         1 x        7.406 Mt        0.000 Mt        1.008 Mt    13.605 % 	 ...../PrepareForRendering

		       0.001 Mt     0.012 %         1 x        0.001 Mt        0.000 Mt        0.001 Mt   100.000 % 	 ....../PrepareMaterials

		       0.494 Mt     6.666 %         1 x        0.494 Mt        0.000 Mt        0.494 Mt   100.000 % 	 ....../BuildMaterialCache

		       5.815 Mt    78.516 %         1 x        5.815 Mt        0.000 Mt        5.815 Mt   100.000 % 	 ....../DeferredRLInitialization

		       0.089 Mt     1.200 %         1 x        0.089 Mt        0.000 Mt        0.089 Mt   100.000 % 	 ....../PrepareDrawBundle

		      24.362 Mt     0.914 %         1 x       24.362 Mt        0.000 Mt        2.790 Mt    11.451 % 	 ..../RayTracingRLPrep

		       0.067 Mt     0.274 %         1 x        0.067 Mt        0.000 Mt        0.067 Mt   100.000 % 	 ...../PrepareAccelerationStructures

		       0.001 Mt     0.004 %         1 x        0.001 Mt        0.000 Mt        0.001 Mt   100.000 % 	 ...../PrepareCache

		      12.722 Mt    52.221 %         1 x       12.722 Mt        0.000 Mt       12.722 Mt   100.000 % 	 ...../PipelineBuild

		       0.680 Mt     2.790 %         1 x        0.680 Mt        0.000 Mt        0.680 Mt   100.000 % 	 ...../BuildCache

		       6.210 Mt    25.489 %         1 x        6.210 Mt        0.000 Mt        0.001 Mt     0.013 % 	 ...../BuildAccelerationStructures

		       6.209 Mt    99.987 %         1 x        6.209 Mt        0.000 Mt        3.876 Mt    62.431 % 	 ....../AccelerationStructureBuild

		       1.532 Mt    24.683 %         1 x        1.532 Mt        0.000 Mt        1.532 Mt   100.000 % 	 ......./BuildBottomLevelAS

		       0.800 Mt    12.887 %         1 x        0.800 Mt        0.000 Mt        0.800 Mt   100.000 % 	 ......./BuildTopLevelAS

		       1.893 Mt     7.771 %         1 x        1.893 Mt        0.000 Mt        1.893 Mt   100.000 % 	 ...../BuildShaderTables

		      32.820 Mt     1.232 %         1 x       32.820 Mt        0.000 Mt       32.820 Mt   100.000 % 	 ..../GpuSidePrep

		       3.978 Mt     0.015 %         1 x        3.978 Mt        0.000 Mt        3.978 Mt   100.000 % 	 ../AmbientOcclusionRLInitialization

		       9.562 Mt     0.037 %         1 x        9.562 Mt        0.000 Mt        9.562 Mt   100.000 % 	 ../DeferredRLInitialization

		       3.680 Mt     0.014 %         1 x        3.680 Mt        0.000 Mt        3.680 Mt   100.000 % 	 ../ResolveRLInitialization

		   23133.392 Mt    88.561 %     32390 x        0.714 Mt       28.972 Mt      429.972 Mt     1.859 % 	 ../MainLoop

		     196.322 Mt     0.849 %      1386 x        0.142 Mt        0.091 Mt      195.707 Mt    99.687 % 	 .../Update

		       0.615 Mt     0.313 %         1 x        0.615 Mt        0.000 Mt        0.615 Mt   100.000 % 	 ..../GuiModelApply

		   22507.097 Mt    97.293 %      2224 x       10.120 Mt       12.110 Mt      263.864 Mt     1.172 % 	 .../Render

		     409.316 Mt     1.819 %      2224 x        0.184 Mt        0.279 Mt       15.583 Mt     3.807 % 	 ..../RayTracing

		     175.922 Mt    42.980 %      2224 x        0.079 Mt        0.132 Mt      174.024 Mt    98.921 % 	 ...../Deferred

		       1.898 Mt     1.079 %      2224 x        0.001 Mt        0.001 Mt        1.898 Mt   100.000 % 	 ....../DeferredRLPrep

		     147.769 Mt    36.101 %      2224 x        0.066 Mt        0.098 Mt      146.486 Mt    99.132 % 	 ...../RayCasting

		       1.283 Mt     0.868 %      2224 x        0.001 Mt        0.001 Mt        1.283 Mt   100.000 % 	 ....../RayTracingRLPrep

		      70.041 Mt    17.112 %      2224 x        0.031 Mt        0.039 Mt       70.041 Mt   100.000 % 	 ...../Resolve

		   21833.918 Mt    97.009 %      2224 x        9.817 Mt       11.646 Mt    21833.918 Mt   100.000 % 	 ..../Present

	WindowThread:

		   26121.134 Mt   100.000 %         1 x    26121.134 Mt    26121.133 Mt    26121.134 Mt   100.000 % 	 /

	GpuThread:

		   20758.543 Mt   100.000 %      2224 x        9.334 Mt       11.054 Mt      151.645 Mt     0.731 % 	 /

		       1.603 Mt     0.008 %         1 x        1.603 Mt        0.000 Mt        0.006 Mt     0.377 % 	 ./ScenePrep

		       1.597 Mt    99.623 %         1 x        1.597 Mt        0.000 Mt        0.002 Mt     0.108 % 	 ../AccelerationStructureBuild

		       1.506 Mt    94.308 %         1 x        1.506 Mt        0.000 Mt        1.506 Mt   100.000 % 	 .../BuildBottomLevelASGpu

		       0.089 Mt     5.584 %         1 x        0.089 Mt        0.000 Mt        0.089 Mt   100.000 % 	 .../BuildTopLevelASGpu

		   20546.259 Mt    98.977 %      2224 x        9.238 Mt       10.960 Mt        5.791 Mt     0.028 % 	 ./RayTracingGpu

		    4791.121 Mt    23.319 %      2224 x        2.154 Mt        4.058 Mt     4791.121 Mt   100.000 % 	 ../DeferredRLGpu

		    9979.976 Mt    48.573 %      2224 x        4.487 Mt        4.245 Mt     9979.976 Mt   100.000 % 	 ../RayTracingRLGpu

		    5769.372 Mt    28.080 %      2224 x        2.594 Mt        2.656 Mt     5769.372 Mt   100.000 % 	 ../ResolveRLGpu

		      59.035 Mt     0.284 %      2224 x        0.027 Mt        0.094 Mt       59.035 Mt   100.000 % 	 ./Present


	============================


