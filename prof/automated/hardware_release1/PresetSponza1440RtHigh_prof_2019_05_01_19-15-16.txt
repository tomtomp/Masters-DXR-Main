Configuration: 
Command line parameters  = E:\programming\FIT-DIP\Masters-DXR-Main-git\build\Measurement\x64\Release\Measurement.exe --scene Sponza/Sponza.gltf --profile-automation --profile-camera-track Sponza/SponzaPreset.trk --rt-mode --width 2560 --height 1440 --profile-output PresetSponza1440RtHigh --quality-preset High 
Build type               = Release
Device name              = NVIDIA GeForce RTX 2080 Ti
Device memory MB         = 11049
Ray Tracing backend      = Hardware
Window width             = 2560
Window height            = 1421
Window title             = Direct3D App
Back-buffer count        = 3
Fill Triangles           = enabled
Triangles Font Clock-Wise= enabled
Fast Build AS            = disabled
Target FPS               = 0
Target UPS               = 60
Tearing                  = enabled
VSync                    = disabled
GUI rendering            = disabled
Debug layer              = disabled
GPU validation           = disabled
GPU profiling            = enabled
Render mode              = Ray Tracing
Loaded scene file        = Sponza/Sponza.gltf
Using testing scene      = disabled
Duplication              = 1
Duplication in cube      = disabled
Duplication offset       = 10
BLAS duplication         = disabled
Rays per pixel           = 13
Quality preset           = High
Rasterization Quality    : 
  AO                     = enabled
  AO kernel size         = 4
  Shadows                = enabled
  Shadow map resolution  = 4096
  Shadow PCF kernel size = 4
  Reflections            = enabled
Ray Tracing Quality      : 
  AO                     = enabled
  AO rays                = 4
  AO kernel size         = 4
  Shadows                = enabled
  Shadow rays            = 4
  Shadow kernel size     = 4
  Reflections            = enabled

ProfilingAggregator: 
Render	,	11.9884	,	11.3621	,	10.8616	,	10.8301	,	10.7724	,	11.6232	,	11.5876	,	12.079	,	12.5365	,	12.0347	,	11.8089	,	12.8287	,	12.7582	,	12.7079	,	11.3629	,	10.4539	,	14.3256	,	13.3874	,	10.2254	,	11.1618	,	11.4615	,	11.783	,	11.8434
Update	,	0.0736	,	0.0725	,	0.0715	,	0.0723	,	0.0682	,	0.0733	,	0.0704	,	0.0742	,	0.0672	,	0.0753	,	0.0645	,	0.0741	,	0.0733	,	0.0786	,	0.0782	,	0.0733	,	0.0682	,	0.0723	,	0.0734	,	0.0736	,	0.0725	,	0.071	,	0.071
RayTracing	,	0.1792	,	0.1809	,	0.1776	,	0.1743	,	0.1762	,	0.1776	,	0.2167	,	0.2191	,	0.224	,	0.1912	,	0.1842	,	0.1937	,	0.1913	,	0.1934	,	0.1815	,	0.1724	,	0.183	,	0.181	,	0.1724	,	0.1718	,	0.182	,	0.1771	,	0.1713
RayTracingGpu	,	11.094	,	10.4149	,	10.0442	,	10.0245	,	9.98458	,	10.754	,	10.4147	,	11.2263	,	11.4867	,	11.1624	,	10.9956	,	11.7707	,	11.5909	,	11.778	,	10.5936	,	9.6696	,	13.4404	,	12.4771	,	9.46675	,	10.3679	,	10.5254	,	10.7223	,	11.0729
DeferredRLGpu	,	2.70214	,	2.45046	,	2.17514	,	1.46486	,	1.35411	,	1.57654	,	2.12912	,	2.39677	,	2.06416	,	1.68301	,	1.36538	,	1.7871	,	2.16829	,	2.46726	,	2.14	,	1.52352	,	1.97683	,	1.8664	,	1.47062	,	3.02093	,	3.85395	,	3.87978	,	3.83718
RayTracingRLGpu	,	5.66074	,	5.39638	,	5.31181	,	5.84058	,	5.80675	,	6.36064	,	5.7015	,	6.29626	,	6.63594	,	6.93315	,	6.91994	,	7.15613	,	6.70989	,	6.77226	,	5.6439	,	5.31866	,	8.74045	,	7.84989	,	5.16726	,	4.80118	,	4.09261	,	4.09674	,	4.39594
ResolveRLGpu	,	2.72944	,	2.5641	,	2.55494	,	2.71709	,	2.82147	,	2.81248	,	2.58211	,	2.53098	,	2.78464	,	2.54378	,	2.7087	,	2.82522	,	2.71037	,	2.53638	,	2.80589	,	2.82499	,	2.72093	,	2.75882	,	2.82682	,	2.54413	,	2.57648	,	2.74205	,	2.83786
Index	,	0	,	1	,	2	,	3	,	4	,	5	,	6	,	7	,	8	,	9	,	10	,	11	,	12	,	13	,	14	,	15	,	16	,	17	,	18	,	19	,	20	,	21	,	22

ProfilingBuildAggregator: 
BuildBottomLevelAS	,	2.061
BuildBottomLevelASGpu	,	1.52195
BuildTopLevelAS	,	2.9715
BuildTopLevelASGpu	,	0.087424
Duplication	,	1
Triangles	,	262267
Meshes	,	103
BottomLevels	,	1
TopLevelSize	,	64
BottomLevelsSize	,	14622208
Index	,	0

FpsAggregator: 
FPS	,	58	,	82	,	89	,	90	,	93	,	89	,	92	,	82	,	80	,	83	,	85	,	82	,	80	,	78	,	81	,	97	,	79	,	72	,	87	,	99	,	85	,	85	,	85
UPS	,	59	,	61	,	60	,	60	,	61	,	60	,	61	,	60	,	60	,	61	,	60	,	61	,	60	,	60	,	61	,	60	,	60	,	61	,	60	,	60	,	61	,	60	,	60
FrameTime	,	17.3473	,	12.2863	,	11.2982	,	11.1933	,	10.7645	,	11.3626	,	10.9837	,	12.2693	,	12.5597	,	12.15	,	11.8756	,	12.2268	,	12.5033	,	12.9554	,	12.461	,	10.338	,	12.7216	,	14.0577	,	11.5092	,	10.1395	,	11.8224	,	11.8253	,	11.8455
GigaRays/s	,	2.72612	,	3.84909	,	4.1857	,	4.22494	,	4.39322	,	4.16197	,	4.30556	,	3.8544	,	3.76528	,	3.89225	,	3.9822	,	3.86779	,	3.78228	,	3.65028	,	3.79512	,	4.57447	,	3.71736	,	3.36406	,	4.10895	,	4.66402	,	4.0001	,	3.99911	,	3.99231
Index	,	0	,	1	,	2	,	3	,	4	,	5	,	6	,	7	,	8	,	9	,	10	,	11	,	12	,	13	,	14	,	15	,	16	,	17	,	18	,	19	,	20	,	21	,	22


AccelerationStructure: 
Ray tracing acceleration structure building statistics: 
	Bottom Level Acceleration Structure: 
		Maximum [B]: 14622208
		Minimum [B]: 14622208
		Currently Allocated [B]: 14622208
		Currently Created [num]: 1
		Current Average [B]: 14622208
		Maximum Triangles [num]: 262267
		Minimum Triangles [num]: 262267
		Total Triangles [num]: 262267
		Maximum Meshes [num]: 103
		Minimum Meshes [num]: 103
		Total Meshes [num]: 103
	Top Level Acceleration Structure: 
		Maximum [B]: 64
		Minimum [B]: 64
		Currently Allocated [B]: 64
		Total Allocated [B]: 64
		Total Created [num]: 1
		Average Allocated [B]: 64
		Maximum Instances [num]: 1
		Minimum Instances [num]: 1
		Total Instances [num]: 1
	Scratch Buffer: 
		Maximum [B]: 11657856
		Minimum [B]: 11657856
		Currently Allocated [B]: 11657856
		Total Allocated [B]: 11657856
		Total Created [num]: 1
		Average Allocated [B]: 11657856
ProfilingStack: 
[PROF] Profiling statistics: 
	Scopes entered : 117490
	Scopes exited : 117489
	Overhead per scope [ticks] : 100.7
	Threads entered : 3
	Threads exited : 0
	============================
		         Sum[Mt]   Parent[%]  Samples[num]       Avg[Mt]   Last/Curr[Mt]        Self[Mt]     Self[%] 	 Name

	main:

		   26181.727 Mt   100.000 %         1 x    26181.727 Mt    26181.726 Mt        0.421 Mt     0.002 % 	 /

		   26181.337 Mt    99.999 %         1 x    26181.338 Mt    26181.337 Mt      243.808 Mt     0.931 % 	 ./Main application loop

		       4.168 Mt     0.016 %         1 x        4.168 Mt        0.000 Mt        4.168 Mt   100.000 % 	 ../TexturedRLInitialization

		       1.281 Mt     0.005 %         1 x        1.281 Mt        0.000 Mt        1.281 Mt   100.000 % 	 ../ShadowMapRLInitialization

		       4.416 Mt     0.017 %         1 x        4.416 Mt        0.000 Mt        4.416 Mt   100.000 % 	 ../RasterResolveRLInitialization

		      50.066 Mt     0.191 %         1 x       50.066 Mt        0.000 Mt        2.651 Mt     5.295 % 	 ../RayTracingRLInitialization

		      47.415 Mt    94.705 %         1 x       47.415 Mt        0.000 Mt       47.415 Mt   100.000 % 	 .../PipelineBuild

		    2693.142 Mt    10.286 %         1 x     2693.142 Mt        0.000 Mt        0.199 Mt     0.007 % 	 ../Initialization

		    2692.943 Mt    99.993 %         1 x     2692.943 Mt        0.000 Mt        2.437 Mt     0.091 % 	 .../ScenePrep

		    2623.985 Mt    97.439 %         1 x     2623.985 Mt        0.000 Mt      249.953 Mt     9.526 % 	 ..../SceneLoad

		    2081.884 Mt    79.341 %         1 x     2081.884 Mt        0.000 Mt      586.097 Mt    28.152 % 	 ...../glTF-LoadScene

		    1495.787 Mt    71.848 %        69 x       21.678 Mt        0.000 Mt     1495.787 Mt   100.000 % 	 ....../glTF-LoadImageData

		     222.707 Mt     8.487 %         1 x      222.707 Mt        0.000 Mt      222.707 Mt   100.000 % 	 ...../glTF-CreateScene

		      69.441 Mt     2.646 %         1 x       69.441 Mt        0.000 Mt       69.441 Mt   100.000 % 	 ...../SceneDesc-CreateScene

		       1.084 Mt     0.040 %         1 x        1.084 Mt        0.000 Mt        0.003 Mt     0.277 % 	 ..../ShadowMapRLPrep

		       1.081 Mt    99.723 %         1 x        1.081 Mt        0.000 Mt        1.038 Mt    96.013 % 	 ...../PrepareForRendering

		       0.043 Mt     3.987 %         1 x        0.043 Mt        0.000 Mt        0.043 Mt   100.000 % 	 ....../PrepareDrawBundle

		       3.230 Mt     0.120 %         1 x        3.230 Mt        0.000 Mt        0.001 Mt     0.015 % 	 ..../TexturedRLPrep

		       3.229 Mt    99.985 %         1 x        3.229 Mt        0.000 Mt        0.944 Mt    29.228 % 	 ...../PrepareForRendering

		       0.001 Mt     0.043 %         1 x        0.001 Mt        0.000 Mt        0.001 Mt   100.000 % 	 ....../PrepareMaterials

		       2.246 Mt    69.558 %         1 x        2.246 Mt        0.000 Mt        2.246 Mt   100.000 % 	 ....../TexturedRLInitialization

		       0.038 Mt     1.171 %         1 x        0.038 Mt        0.000 Mt        0.038 Mt   100.000 % 	 ....../PrepareDrawBundle

		       7.412 Mt     0.275 %         1 x        7.412 Mt        0.000 Mt        0.001 Mt     0.008 % 	 ..../DeferredRLPrep

		       7.411 Mt    99.992 %         1 x        7.411 Mt        0.000 Mt        0.895 Mt    12.076 % 	 ...../PrepareForRendering

		       0.001 Mt     0.011 %         1 x        0.001 Mt        0.000 Mt        0.001 Mt   100.000 % 	 ....../PrepareMaterials

		       0.327 Mt     4.408 %         1 x        0.327 Mt        0.000 Mt        0.327 Mt   100.000 % 	 ....../BuildMaterialCache

		       6.130 Mt    82.715 %         1 x        6.130 Mt        0.000 Mt        6.130 Mt   100.000 % 	 ....../DeferredRLInitialization

		       0.059 Mt     0.789 %         1 x        0.059 Mt        0.000 Mt        0.059 Mt   100.000 % 	 ....../PrepareDrawBundle

		      26.854 Mt     0.997 %         1 x       26.854 Mt        0.000 Mt        2.732 Mt    10.172 % 	 ..../RayTracingRLPrep

		       0.066 Mt     0.245 %         1 x        0.066 Mt        0.000 Mt        0.066 Mt   100.000 % 	 ...../PrepareAccelerationStructures

		       0.001 Mt     0.004 %         1 x        0.001 Mt        0.000 Mt        0.001 Mt   100.000 % 	 ...../PrepareCache

		      13.255 Mt    49.360 %         1 x       13.255 Mt        0.000 Mt       13.255 Mt   100.000 % 	 ...../PipelineBuild

		       0.655 Mt     2.438 %         1 x        0.655 Mt        0.000 Mt        0.655 Mt   100.000 % 	 ...../BuildCache

		       8.353 Mt    31.104 %         1 x        8.353 Mt        0.000 Mt        0.001 Mt     0.006 % 	 ...../BuildAccelerationStructures

		       8.352 Mt    99.994 %         1 x        8.352 Mt        0.000 Mt        3.320 Mt    39.747 % 	 ....../AccelerationStructureBuild

		       2.061 Mt    24.676 %         1 x        2.061 Mt        0.000 Mt        2.061 Mt   100.000 % 	 ......./BuildBottomLevelAS

		       2.971 Mt    35.577 %         1 x        2.971 Mt        0.000 Mt        2.971 Mt   100.000 % 	 ......./BuildTopLevelAS

		       1.793 Mt     6.676 %         1 x        1.793 Mt        0.000 Mt        1.793 Mt   100.000 % 	 ...../BuildShaderTables

		      27.941 Mt     1.038 %         1 x       27.941 Mt        0.000 Mt       27.941 Mt   100.000 % 	 ..../GpuSidePrep

		       2.780 Mt     0.011 %         1 x        2.780 Mt        0.000 Mt        2.780 Mt   100.000 % 	 ../AmbientOcclusionRLInitialization

		       6.001 Mt     0.023 %         1 x        6.001 Mt        0.000 Mt        6.001 Mt   100.000 % 	 ../DeferredRLInitialization

		       3.672 Mt     0.014 %         1 x        3.672 Mt        0.000 Mt        3.672 Mt   100.000 % 	 ../ResolveRLInitialization

		   23172.004 Mt    88.506 %     90846 x        0.255 Mt       31.985 Mt      421.098 Mt     1.817 % 	 ../MainLoop

		     180.165 Mt     0.778 %      1388 x        0.130 Mt        0.092 Mt      179.570 Mt    99.670 % 	 .../Update

		       0.595 Mt     0.330 %         1 x        0.595 Mt        0.000 Mt        0.595 Mt   100.000 % 	 ..../GuiModelApply

		   22570.741 Mt    97.405 %      1934 x       11.670 Mt       12.449 Mt      231.945 Mt     1.028 % 	 .../Render

		     362.277 Mt     1.605 %      1934 x        0.187 Mt        0.288 Mt       13.462 Mt     3.716 % 	 ..../RayTracing

		     155.867 Mt    43.024 %      1934 x        0.081 Mt        0.134 Mt      154.237 Mt    98.954 % 	 ...../Deferred

		       1.630 Mt     1.046 %      1934 x        0.001 Mt        0.001 Mt        1.630 Mt   100.000 % 	 ....../DeferredRLPrep

		     131.366 Mt    36.261 %      1934 x        0.068 Mt        0.102 Mt      130.199 Mt    99.112 % 	 ...../RayCasting

		       1.167 Mt     0.888 %      1934 x        0.001 Mt        0.001 Mt        1.167 Mt   100.000 % 	 ....../RayTracingRLPrep

		      61.583 Mt    16.999 %      1934 x        0.032 Mt        0.042 Mt       61.583 Mt   100.000 % 	 ...../Resolve

		   21976.519 Mt    97.367 %      1934 x       11.363 Mt       11.976 Mt    21976.519 Mt   100.000 % 	 ..../Present

	WindowThread:

		   26178.679 Mt   100.000 %         1 x    26178.679 Mt    26178.678 Mt    26178.679 Mt   100.000 % 	 /

	GpuThread:

		   21046.722 Mt   100.000 %      1934 x       10.882 Mt       11.169 Mt      132.471 Mt     0.629 % 	 /

		       1.617 Mt     0.008 %         1 x        1.617 Mt        0.000 Mt        0.006 Mt     0.374 % 	 ./ScenePrep

		       1.611 Mt    99.626 %         1 x        1.611 Mt        0.000 Mt        0.002 Mt     0.099 % 	 ../AccelerationStructureBuild

		       1.522 Mt    94.474 %         1 x        1.522 Mt        0.000 Mt        1.522 Mt   100.000 % 	 .../BuildBottomLevelASGpu

		       0.087 Mt     5.427 %         1 x        0.087 Mt        0.000 Mt        0.087 Mt   100.000 % 	 .../BuildTopLevelASGpu

		   20855.847 Mt    99.093 %      1934 x       10.784 Mt       11.057 Mt        5.012 Mt     0.024 % 	 ./RayTracingGpu

		    4232.700 Mt    20.295 %      1934 x        2.189 Mt        3.859 Mt     4232.700 Mt   100.000 % 	 ../DeferredRLGpu

		   11451.959 Mt    54.910 %      1934 x        5.921 Mt        4.326 Mt    11451.959 Mt   100.000 % 	 ../RayTracingRLGpu

		    5166.176 Mt    24.771 %      1934 x        2.671 Mt        2.870 Mt     5166.176 Mt   100.000 % 	 ../ResolveRLGpu

		      56.787 Mt     0.270 %      1934 x        0.029 Mt        0.112 Mt       56.787 Mt   100.000 % 	 ./Present


	============================


