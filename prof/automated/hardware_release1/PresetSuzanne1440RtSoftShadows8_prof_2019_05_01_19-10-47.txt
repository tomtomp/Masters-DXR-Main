Configuration: 
Command line parameters  = E:\programming\FIT-DIP\Masters-DXR-Main-git\build\Measurement\x64\Release\Measurement.exe --scene Suzanne/Suzanne.gltf --profile-automation --profile-camera-track Suzanne/Suzanne.trk --rt-mode --width 2560 --height 1440 --profile-output PresetSuzanne1440RtSoftShadows8 --quality-preset SoftShadows8 
Build type               = Release
Device name              = NVIDIA GeForce RTX 2080 Ti
Device memory MB         = 11049
Ray Tracing backend      = Hardware
Window width             = 2560
Window height            = 1421
Window title             = Direct3D App
Back-buffer count        = 3
Fill Triangles           = enabled
Triangles Font Clock-Wise= enabled
Fast Build AS            = disabled
Target FPS               = 0
Target UPS               = 60
Tearing                  = enabled
VSync                    = disabled
GUI rendering            = disabled
Debug layer              = disabled
GPU validation           = disabled
GPU profiling            = enabled
Render mode              = Ray Tracing
Loaded scene file        = Suzanne/Suzanne.gltf
Using testing scene      = disabled
Duplication              = 1
Duplication in cube      = disabled
Duplication offset       = 10
BLAS duplication         = disabled
Rays per pixel           = 8
Quality preset           = SoftShadows8
Rasterization Quality    : 
  AO                     = disabled
  AO kernel size         = 1
  Shadows                = enabled
  Shadow map resolution  = 8192
  Shadow PCF kernel size = 8
  Reflections            = disabled
Ray Tracing Quality      : 
  AO                     = disabled
  AO rays                = 0
  AO kernel size         = 8
  Shadows                = enabled
  Shadow rays            = 8
  Shadow kernel size     = 8
  Reflections            = disabled

ProfilingAggregator: 
Render	,	6.684	,	6.9481	,	7.0664	,	7.259	,	6.8186	,	6.2644	,	6.1866	,	5.7807	,	6.1255	,	6.873	,	7.0178	,	6.698	,	6.2161	,	6.2608	,	6.4358	,	7.1482	,	8.1728	,	9.0822	,	10.9879	,	8.9777	,	7.5086	,	6.6049	,	6.179	,	6.4605
Update	,	0.0754	,	0.0739	,	0.1259	,	0.0724	,	0.0752	,	0.0717	,	0.0736	,	0.0644	,	0.0723	,	0.074	,	0.0701	,	0.0803	,	0.0692	,	0.0698	,	0.0733	,	0.0692	,	0.0698	,	0.0683	,	0.0708	,	0.0663	,	0.0706	,	0.0722	,	0.0708	,	0.0696
RayTracing	,	0.2124	,	0.2048	,	0.1739	,	0.1727	,	0.1785	,	0.1853	,	0.1752	,	0.1422	,	0.176	,	0.1736	,	0.1756	,	0.1731	,	0.1793	,	0.1802	,	0.1769	,	0.1819	,	0.1718	,	0.1732	,	0.1757	,	0.1546	,	0.174	,	0.1745	,	0.1721	,	0.206
RayTracingGpu	,	5.76656	,	6.13978	,	6.32534	,	6.21856	,	5.76224	,	5.38541	,	5.27616	,	5.23891	,	5.36115	,	5.82653	,	5.9577	,	5.82723	,	5.33901	,	5.32	,	5.60205	,	6.36125	,	7.10035	,	8.04352	,	10.2075	,	8.25802	,	6.72278	,	5.73792	,	5.40832	,	5.60118
DeferredRLGpu	,	0.138496	,	0.182528	,	0.197184	,	0.151584	,	0.101088	,	0.06944	,	0.055872	,	0.0456	,	0.063008	,	0.092992	,	0.119232	,	0.078976	,	0.054688	,	0.05664	,	0.088736	,	0.184704	,	0.226528	,	0.260416	,	0.402176	,	0.322976	,	0.26096	,	0.13232	,	0.080864	,	0.070464
RayTracingRLGpu	,	0.600576	,	0.898112	,	1.05334	,	0.874944	,	0.535808	,	0.316576	,	0.238016	,	0.201184	,	0.294688	,	0.524064	,	0.638016	,	0.453216	,	0.29104	,	0.265856	,	0.48752	,	1.10051	,	1.70173	,	2.66192	,	4.52317	,	2.69741	,	1.37034	,	0.569536	,	0.31904	,	0.278592
ResolveRLGpu	,	5.0255	,	5.05715	,	5.07264	,	5.19066	,	5.12413	,	4.9969	,	4.9792	,	4.9897	,	5.0016	,	5.20822	,	5.1975	,	5.29206	,	4.99155	,	4.9944	,	5.02394	,	5.07149	,	5.1705	,	5.11949	,	5.2801	,	5.2361	,	5.08858	,	5.03302	,	5.00586	,	5.2505
Index	,	0	,	1	,	2	,	3	,	4	,	5	,	6	,	7	,	8	,	9	,	10	,	11	,	12	,	13	,	14	,	15	,	16	,	17	,	18	,	19	,	20	,	21	,	22	,	23

ProfilingBuildAggregator: 
BuildBottomLevelAS	,	0.9048
BuildBottomLevelASGpu	,	0.256864
BuildTopLevelAS	,	1.1769
BuildTopLevelASGpu	,	0.060064
Duplication	,	1
Triangles	,	3936
Meshes	,	1
BottomLevels	,	1
TopLevelSize	,	64
BottomLevelsSize	,	222592
Index	,	0

FpsAggregator: 
FPS	,	117	,	140	,	136	,	137	,	144	,	153	,	157	,	161	,	160	,	153	,	147	,	149	,	155	,	158	,	154	,	144	,	129	,	117	,	100	,	102	,	119	,	139	,	153	,	157
UPS	,	59	,	61	,	60	,	60	,	60	,	61	,	60	,	60	,	60	,	60	,	60	,	61	,	60	,	60	,	60	,	60	,	61	,	60	,	60	,	60	,	60	,	60	,	61	,	60
FrameTime	,	8.59428	,	7.18218	,	7.37006	,	7.33677	,	6.96216	,	6.56576	,	6.38397	,	6.22688	,	6.28185	,	6.55455	,	6.80485	,	6.74515	,	6.46991	,	6.33954	,	6.507	,	6.9875	,	7.79629	,	8.56835	,	10.0349	,	9.80801	,	8.40633	,	7.21533	,	6.57412	,	6.39942
GigaRays/s	,	3.38622	,	4.05199	,	3.94869	,	3.9666	,	4.18004	,	4.4324	,	4.55862	,	4.67362	,	4.63272	,	4.43998	,	4.27667	,	4.31452	,	4.49807	,	4.59057	,	4.47242	,	4.16488	,	3.73281	,	3.39646	,	2.90009	,	2.96718	,	3.46192	,	4.03337	,	4.42677	,	4.54761
Index	,	0	,	1	,	2	,	3	,	4	,	5	,	6	,	7	,	8	,	9	,	10	,	11	,	12	,	13	,	14	,	15	,	16	,	17	,	18	,	19	,	20	,	21	,	22	,	23


AccelerationStructure: 
Ray tracing acceleration structure building statistics: 
	Bottom Level Acceleration Structure: 
		Maximum [B]: 222592
		Minimum [B]: 222592
		Currently Allocated [B]: 222592
		Currently Created [num]: 1
		Current Average [B]: 222592
		Maximum Triangles [num]: 3936
		Minimum Triangles [num]: 3936
		Total Triangles [num]: 3936
		Maximum Meshes [num]: 1
		Minimum Meshes [num]: 1
		Total Meshes [num]: 1
	Top Level Acceleration Structure: 
		Maximum [B]: 64
		Minimum [B]: 64
		Currently Allocated [B]: 64
		Total Allocated [B]: 64
		Total Created [num]: 1
		Average Allocated [B]: 64
		Maximum Instances [num]: 1
		Minimum Instances [num]: 1
		Total Instances [num]: 1
	Scratch Buffer: 
		Maximum [B]: 32896
		Minimum [B]: 32896
		Currently Allocated [B]: 32896
		Total Allocated [B]: 32896
		Total Created [num]: 1
		Average Allocated [B]: 32896
ProfilingStack: 
[PROF] Profiling statistics: 
	Scopes entered : 76938
	Scopes exited : 76937
	Overhead per scope [ticks] : 101.6
	Threads entered : 3
	Threads exited : 0
	============================
		         Sum[Mt]   Parent[%]  Samples[num]       Avg[Mt]   Last/Curr[Mt]        Self[Mt]     Self[%] 	 Name

	main:

		   24555.608 Mt   100.000 %         1 x    24555.609 Mt    24555.608 Mt        0.396 Mt     0.002 % 	 /

		   24555.243 Mt    99.999 %         1 x    24555.244 Mt    24555.243 Mt      252.676 Mt     1.029 % 	 ./Main application loop

		       2.891 Mt     0.012 %         1 x        2.891 Mt        0.000 Mt        2.891 Mt   100.000 % 	 ../TexturedRLInitialization

		       1.288 Mt     0.005 %         1 x        1.288 Mt        0.000 Mt        1.288 Mt   100.000 % 	 ../ShadowMapRLInitialization

		       4.677 Mt     0.019 %         1 x        4.677 Mt        0.000 Mt        4.677 Mt   100.000 % 	 ../RasterResolveRLInitialization

		      61.962 Mt     0.252 %         1 x       61.962 Mt        0.000 Mt        2.775 Mt     4.479 % 	 ../RayTracingRLInitialization

		      59.187 Mt    95.521 %         1 x       59.187 Mt        0.000 Mt       59.187 Mt   100.000 % 	 .../PipelineBuild

		     107.933 Mt     0.440 %         1 x      107.933 Mt        0.000 Mt        0.182 Mt     0.169 % 	 ../Initialization

		     107.751 Mt    99.831 %         1 x      107.751 Mt        0.000 Mt        0.959 Mt     0.890 % 	 .../ScenePrep

		      67.247 Mt    62.410 %         1 x       67.247 Mt        0.000 Mt       14.918 Mt    22.183 % 	 ..../SceneLoad

		      39.290 Mt    58.426 %         1 x       39.290 Mt        0.000 Mt        8.496 Mt    21.625 % 	 ...../glTF-LoadScene

		      30.794 Mt    78.375 %         2 x       15.397 Mt        0.000 Mt       30.794 Mt   100.000 % 	 ....../glTF-LoadImageData

		      11.858 Mt    17.633 %         1 x       11.858 Mt        0.000 Mt       11.858 Mt   100.000 % 	 ...../glTF-CreateScene

		       1.182 Mt     1.757 %         1 x        1.182 Mt        0.000 Mt        1.182 Mt   100.000 % 	 ...../SceneDesc-CreateScene

		       0.262 Mt     0.243 %         1 x        0.262 Mt        0.000 Mt        0.002 Mt     0.877 % 	 ..../ShadowMapRLPrep

		       0.260 Mt    99.123 %         1 x        0.260 Mt        0.000 Mt        0.251 Mt    96.653 % 	 ...../PrepareForRendering

		       0.009 Mt     3.347 %         1 x        0.009 Mt        0.000 Mt        0.009 Mt   100.000 % 	 ....../PrepareDrawBundle

		       1.817 Mt     1.686 %         1 x        1.817 Mt        0.000 Mt        0.000 Mt     0.022 % 	 ..../TexturedRLPrep

		       1.817 Mt    99.978 %         1 x        1.817 Mt        0.000 Mt        0.571 Mt    31.431 % 	 ...../PrepareForRendering

		       0.001 Mt     0.072 %         1 x        0.001 Mt        0.000 Mt        0.001 Mt   100.000 % 	 ....../PrepareMaterials

		       1.241 Mt    68.300 %         1 x        1.241 Mt        0.000 Mt        1.241 Mt   100.000 % 	 ....../TexturedRLInitialization

		       0.004 Mt     0.198 %         1 x        0.004 Mt        0.000 Mt        0.004 Mt   100.000 % 	 ....../PrepareDrawBundle

		       5.461 Mt     5.069 %         1 x        5.461 Mt        0.000 Mt        0.001 Mt     0.009 % 	 ..../DeferredRLPrep

		       5.461 Mt    99.991 %         1 x        5.461 Mt        0.000 Mt        0.878 Mt    16.071 % 	 ...../PrepareForRendering

		       0.001 Mt     0.018 %         1 x        0.001 Mt        0.000 Mt        0.001 Mt   100.000 % 	 ....../PrepareMaterials

		       0.292 Mt     5.343 %         1 x        0.292 Mt        0.000 Mt        0.292 Mt   100.000 % 	 ....../BuildMaterialCache

		       4.287 Mt    78.496 %         1 x        4.287 Mt        0.000 Mt        4.287 Mt   100.000 % 	 ....../DeferredRLInitialization

		       0.004 Mt     0.071 %         1 x        0.004 Mt        0.000 Mt        0.004 Mt   100.000 % 	 ....../PrepareDrawBundle

		      21.120 Mt    19.600 %         1 x       21.120 Mt        0.000 Mt        2.992 Mt    14.166 % 	 ..../RayTracingRLPrep

		       0.050 Mt     0.235 %         1 x        0.050 Mt        0.000 Mt        0.050 Mt   100.000 % 	 ...../PrepareAccelerationStructures

		       0.001 Mt     0.004 %         1 x        0.001 Mt        0.000 Mt        0.001 Mt   100.000 % 	 ...../PrepareCache

		      12.968 Mt    61.404 %         1 x       12.968 Mt        0.000 Mt       12.968 Mt   100.000 % 	 ...../PipelineBuild

		       0.346 Mt     1.637 %         1 x        0.346 Mt        0.000 Mt        0.346 Mt   100.000 % 	 ...../BuildCache

		       3.301 Mt    15.629 %         1 x        3.301 Mt        0.000 Mt        0.001 Mt     0.027 % 	 ...../BuildAccelerationStructures

		       3.300 Mt    99.973 %         1 x        3.300 Mt        0.000 Mt        1.218 Mt    36.916 % 	 ....../AccelerationStructureBuild

		       0.905 Mt    27.419 %         1 x        0.905 Mt        0.000 Mt        0.905 Mt   100.000 % 	 ......./BuildBottomLevelAS

		       1.177 Mt    35.665 %         1 x        1.177 Mt        0.000 Mt        1.177 Mt   100.000 % 	 ......./BuildTopLevelAS

		       1.462 Mt     6.925 %         1 x        1.462 Mt        0.000 Mt        1.462 Mt   100.000 % 	 ...../BuildShaderTables

		      10.884 Mt    10.101 %         1 x       10.884 Mt        0.000 Mt       10.884 Mt   100.000 % 	 ..../GpuSidePrep

		       2.822 Mt     0.011 %         1 x        2.822 Mt        0.000 Mt        2.822 Mt   100.000 % 	 ../AmbientOcclusionRLInitialization

		       5.784 Mt     0.024 %         1 x        5.784 Mt        0.000 Mt        5.784 Mt   100.000 % 	 ../DeferredRLInitialization

		       3.724 Mt     0.015 %         1 x        3.724 Mt        0.000 Mt        3.724 Mt   100.000 % 	 ../ResolveRLInitialization

		   24111.486 Mt    98.193 %     31466 x        0.766 Mt        6.532 Mt      399.598 Mt     1.657 % 	 ../MainLoop

		     202.849 Mt     0.841 %      1446 x        0.140 Mt        0.038 Mt      202.275 Mt    99.717 % 	 .../Update

		       0.574 Mt     0.283 %         1 x        0.574 Mt        0.000 Mt        0.574 Mt   100.000 % 	 ..../GuiModelApply

		   23509.039 Mt    97.501 %      3383 x        6.949 Mt        6.401 Mt      386.552 Mt     1.644 % 	 .../Render

		     611.808 Mt     2.602 %      3383 x        0.181 Mt        0.223 Mt       23.271 Mt     3.804 % 	 ..../RayTracing

		     272.975 Mt    44.618 %      3383 x        0.081 Mt        0.097 Mt      269.400 Mt    98.690 % 	 ...../Deferred

		       3.575 Mt     1.310 %      3383 x        0.001 Mt        0.001 Mt        3.575 Mt   100.000 % 	 ....../DeferredRLPrep

		     227.251 Mt    37.144 %      3383 x        0.067 Mt        0.088 Mt      225.362 Mt    99.169 % 	 ...../RayCasting

		       1.889 Mt     0.831 %      3383 x        0.001 Mt        0.001 Mt        1.889 Mt   100.000 % 	 ....../RayTracingRLPrep

		      88.311 Mt    14.434 %      3383 x        0.026 Mt        0.030 Mt       88.311 Mt   100.000 % 	 ...../Resolve

		   22510.679 Mt    95.753 %      3383 x        6.654 Mt        6.024 Mt    22510.679 Mt   100.000 % 	 ..../Present

	WindowThread:

		   24554.346 Mt   100.000 %         1 x    24554.346 Mt    24554.346 Mt    24554.346 Mt   100.000 % 	 /

	GpuThread:

		   20742.009 Mt   100.000 %      3383 x        6.131 Mt        5.518 Mt      142.905 Mt     0.689 % 	 /

		       0.323 Mt     0.002 %         1 x        0.323 Mt        0.000 Mt        0.005 Mt     1.613 % 	 ./ScenePrep

		       0.318 Mt    98.387 %         1 x        0.318 Mt        0.000 Mt        0.001 Mt     0.362 % 	 ../AccelerationStructureBuild

		       0.257 Mt    80.755 %         1 x        0.257 Mt        0.000 Mt        0.257 Mt   100.000 % 	 .../BuildBottomLevelASGpu

		       0.060 Mt    18.883 %         1 x        0.060 Mt        0.000 Mt        0.060 Mt   100.000 % 	 .../BuildTopLevelASGpu

		   20510.795 Mt    98.885 %      3383 x        6.063 Mt        5.430 Mt        7.764 Mt     0.038 % 	 ./RayTracingGpu

		     448.734 Mt     2.188 %      3383 x        0.133 Mt        0.070 Mt      448.734 Mt   100.000 % 	 ../DeferredRLGpu

		    2799.009 Mt    13.647 %      3383 x        0.827 Mt        0.279 Mt     2799.009 Mt   100.000 % 	 ../RayTracingRLGpu

		   17255.288 Mt    84.128 %      3383 x        5.101 Mt        5.080 Mt    17255.288 Mt   100.000 % 	 ../ResolveRLGpu

		      87.985 Mt     0.424 %      3383 x        0.026 Mt        0.088 Mt       87.985 Mt   100.000 % 	 ./Present


	============================


