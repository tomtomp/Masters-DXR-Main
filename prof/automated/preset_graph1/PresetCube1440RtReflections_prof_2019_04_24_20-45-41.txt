Configuration: 
Command line parameters  = E:\programming\FIT-DIP\Masters-DXR-Main-git\build\Measurement\x64\Debug\Measurement.exe --scene Cube/Cube.gltf --profile-automation --profile-camera-track Cube/Cube.trk --rt-mode --width 2560 --height 1440 --profile-output PresetCube1440RtReflections --quality-preset Reflections 
Window width             = 2560
Window height            = 1421
Window title             = Direct3D App
Back-buffer count        = 3
Fill Triangles           = enabled
Triangles Font Clock-Wise= enabled
Fast Build AS            = disabled
Target FPS               = 0
Target UPS               = 60
Tearing                  = enabled
VSync                    = disabled
GUI rendering            = disabled
Debug layer              = enabled
GPU validation           = disabled
GPU profiling            = enabled
Render mode              = Ray Tracing
Loaded scene file        = Cube/Cube.gltf
Using testing scene      = disabled
Duplication              = 1
Duplication in cube      = disabled
Duplication offset       = 10
BLAS duplication         = disabled
Rays per pixel           = 2
Quality preset           = Reflections
Rasterization Quality    : 
  AO                     = disabled
  AO kernel size         = 1
  Shadows                = disabled
  Shadow map resolution  = 1
  Shadow PCF kernel size = 0
  Reflections            = enabled
Ray Tracing Quality      : 
  AO                     = disabled
  AO rays                = 0
  AO kernel size         = 4
  Shadows                = disabled
  Shadow rays            = 0
  Shadow kernel size     = 4
  Reflections            = enabled

ProfilingAggregator: 
Render	,	4.1474	,	3.2183	,	3.731	,	3.7452	,	3.508	,	5.8089	,	6.2063	,	4.2044	,	4.0683	,	6.3649	,	5.6197	,	5.2508	,	3.8329	,	4.0359	,	6.2276	,	4.7912	,	4.2407	,	7.1352	,	6.7928	,	6.3514	,	4.8031	,	3.7632	,	3.6673	,	3.2273	,	4.8783	,	3.1285	,	4.9569	,	3.1066	,	5.2668
Update	,	0.6222	,	0.9158	,	0.6345	,	0.6978	,	0.6938	,	1.2368	,	0.7332	,	1.106	,	0.6098	,	1.3701	,	1.0909	,	1.1833	,	1.0962	,	0.8165	,	0.6232	,	0.661	,	0.9041	,	0.6735	,	1.1129	,	1.2894	,	0.5839	,	1.0944	,	0.6786	,	0.4734	,	1.1223	,	0.5547	,	0.885	,	0.7768	,	0.9134
RayTracing	,	1.8076	,	1.2342	,	1.425	,	1.364	,	1.229	,	2.4614	,	2.6717	,	1.2442	,	1.2538	,	2.9445	,	2.532	,	2.185	,	1.2354	,	1.4005	,	2.8267	,	1.6134	,	1.218	,	2.9179	,	2.8749	,	2.7656	,	1.4509	,	1.3231	,	1.2218	,	1.2091	,	2.2697	,	1.2238	,	2.3729	,	1.2117	,	2.3658
RayTracingGpu	,	0.653536	,	1.04717	,	1.14106	,	1.20646	,	1.30397	,	1.56474	,	1.52752	,	1.7113	,	1.78682	,	1.57149	,	1.41706	,	1.548	,	1.37338	,	1.34694	,	1.54442	,	2.21021	,	1.8567	,	1.99642	,	1.93869	,	1.8361	,	2.0905	,	1.20365	,	1.18515	,	1.07552	,	1.05568	,	0.984544	,	0.954272	,	0.956864	,	0.989376
DeferredRLGpu	,	0.257536	,	0.444512	,	0.512288	,	0.570944	,	0.658624	,	0.854848	,	0.824096	,	0.962496	,	1.01379	,	0.851072	,	0.744192	,	0.84048	,	0.711104	,	0.681024	,	0.828416	,	0.929632	,	1.07094	,	1.16595	,	1.11114	,	1.02061	,	0.837696	,	0.586912	,	0.569664	,	0.47904	,	0.443232	,	0.36816	,	0.31008	,	0.276448	,	0.262304
RayTracingRLGpu	,	0.202752	,	0.278016	,	0.297696	,	0.311744	,	0.331008	,	0.352736	,	0.361216	,	0.370592	,	0.355712	,	0.336032	,	0.328864	,	0.340832	,	0.345344	,	0.358432	,	0.381472	,	0.91536	,	0.377472	,	0.372544	,	0.361984	,	0.348192	,	0.83968	,	0.297952	,	0.325248	,	0.336896	,	0.363776	,	0.380064	,	0.409632	,	0.439264	,	0.476512
ResolveRLGpu	,	0.191552	,	0.32224	,	0.329568	,	0.32176	,	0.311808	,	0.35536	,	0.340416	,	0.376416	,	0.41504	,	0.382624	,	0.34224	,	0.364352	,	0.315136	,	0.305472	,	0.33232	,	0.362976	,	0.405984	,	0.456384	,	0.46352	,	0.464992	,	0.411104	,	0.316736	,	0.288192	,	0.25728	,	0.24688	,	0.234752	,	0.23248	,	0.239392	,	0.248512
Index	,	0	,	1	,	2	,	3	,	4	,	5	,	6	,	7	,	8	,	9	,	10	,	11	,	12	,	13	,	14	,	15	,	16	,	17	,	18	,	19	,	20	,	21	,	22	,	23	,	24	,	25	,	26	,	27	,	28

ProfilingBuildAggregator: 
BuildBottomLevelAS	,	0.9857
BuildBottomLevelASGpu	,	0.088032
BuildTopLevelAS	,	0.9593
BuildTopLevelASGpu	,	0.059872
Duplication	,	1
Triangles	,	12
Meshes	,	1
BottomLevels	,	1
TopLevelSize	,	64
BottomLevelsSize	,	5168
Index	,	0

FpsAggregator: 
FPS	,	279	,	261	,	230	,	227	,	206	,	196	,	201	,	207	,	187	,	186	,	202	,	204	,	193	,	213	,	205	,	201	,	188	,	183	,	178	,	181	,	186	,	209	,	218	,	228	,	236	,	240	,	258	,	257	,	249
UPS	,	59	,	61	,	60	,	60	,	60	,	60	,	60	,	61	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	61	,	60	,	60	,	60	,	61	,	60	,	60	,	60	,	60	,	60	,	61	,	60	,	60	,	60
FrameTime	,	3.59841	,	3.84116	,	4.3631	,	4.41303	,	4.8614	,	5.103	,	5.00553	,	4.84895	,	5.36767	,	5.38871	,	4.95165	,	4.91133	,	5.18246	,	4.70793	,	4.89769	,	4.98295	,	5.34084	,	5.49356	,	5.63894	,	5.55456	,	5.38966	,	4.79455	,	4.59175	,	4.39923	,	4.24484	,	4.17725	,	3.89137	,	3.90263	,	4.03535
GigaRays/s	,	2.02187	,	1.89409	,	1.66751	,	1.64864	,	1.49659	,	1.42573	,	1.4535	,	1.50043	,	1.35543	,	1.35014	,	1.46931	,	1.48137	,	1.40387	,	1.54538	,	1.4855	,	1.46008	,	1.36224	,	1.32437	,	1.29023	,	1.30983	,	1.3499	,	1.51746	,	1.58448	,	1.65382	,	1.71397	,	1.7417	,	1.86966	,	1.86426	,	1.80295
Index	,	0	,	1	,	2	,	3	,	4	,	5	,	6	,	7	,	8	,	9	,	10	,	11	,	12	,	13	,	14	,	15	,	16	,	17	,	18	,	19	,	20	,	21	,	22	,	23	,	24	,	25	,	26	,	27	,	28


AccelerationStructure: 
Ray tracing acceleration structure building statistics: 
	Bottom Level Acceleration Structure: 
		Maximum [B]: 5168
		Minimum [B]: 5168
		Currently Allocated [B]: 5168
		Currently Created [num]: 1
		Current Average [B]: 5168
		Maximum Triangles [num]: 12
		Minimum Triangles [num]: 12
		Total Triangles [num]: 12
		Maximum Meshes [num]: 1
		Minimum Meshes [num]: 1
		Total Meshes [num]: 1
	Top Level Acceleration Structure: 
		Maximum [B]: 64
		Minimum [B]: 64
		Currently Allocated [B]: 64
		Total Allocated [B]: 64
		Total Created [num]: 1
		Average Allocated [B]: 64
		Maximum Instances [num]: 1
		Minimum Instances [num]: 1
		Total Instances [num]: 1
	Scratch Buffer: 
		Maximum [B]: 3584
		Minimum [B]: 3584
		Currently Allocated [B]: 3584
		Total Allocated [B]: 3584
		Total Created [num]: 1
		Average Allocated [B]: 3584
ProfilingStack: 
[PROF] Profiling statistics: 
	Scopes entered : 69359
	Scopes exited : 69358
	Overhead per scope [ticks] : 999.7
	Threads entered : 3
	Threads exited : 0
	============================
		         Sum[Mt]   Parent[%]  Samples[num]       Avg[Mt]   Last/Curr[Mt]        Self[Mt]     Self[%] 	 Name

	main:

		   29648.046 Mt   100.000 %         1 x    29648.050 Mt    29648.045 Mt        1.138 Mt     0.004 % 	 /

		   29647.049 Mt    99.997 %         1 x    29647.056 Mt    29647.048 Mt      391.186 Mt     1.319 % 	 ./Main application loop

		       2.844 Mt     0.010 %         1 x        2.844 Mt        0.000 Mt        2.844 Mt   100.000 % 	 ../TexturedRLInitialization

		       2.448 Mt     0.008 %         1 x        2.448 Mt        0.000 Mt        2.448 Mt   100.000 % 	 ../ShadowMapRLInitialization

		       3.873 Mt     0.013 %         1 x        3.873 Mt        0.000 Mt        3.873 Mt   100.000 % 	 ../AmbientOcclusionRLInitialization

		       6.540 Mt     0.022 %         1 x        6.540 Mt        0.000 Mt        6.540 Mt   100.000 % 	 ../RasterResolveRLInitialization

		     111.911 Mt     0.377 %         1 x      111.911 Mt        0.000 Mt        3.622 Mt     3.237 % 	 ../Initialization

		     108.289 Mt    96.763 %         1 x      108.289 Mt        0.000 Mt        1.068 Mt     0.986 % 	 .../ScenePrep

		      76.224 Mt    70.389 %         1 x       76.224 Mt        0.000 Mt       11.707 Mt    15.358 % 	 ..../SceneLoad

		      57.560 Mt    75.515 %         1 x       57.560 Mt        0.000 Mt       12.458 Mt    21.644 % 	 ...../glTF-LoadScene

		      45.102 Mt    78.356 %         2 x       22.551 Mt        0.000 Mt       45.102 Mt   100.000 % 	 ....../glTF-LoadImageData

		       3.954 Mt     5.187 %         1 x        3.954 Mt        0.000 Mt        3.954 Mt   100.000 % 	 ...../glTF-CreateScene

		       3.003 Mt     3.940 %         1 x        3.003 Mt        0.000 Mt        3.003 Mt   100.000 % 	 ...../SceneDesc-CreateScene

		       0.428 Mt     0.395 %         1 x        0.428 Mt        0.000 Mt        0.005 Mt     1.075 % 	 ..../ShadowMapRLPrep

		       0.423 Mt    98.925 %         1 x        0.423 Mt        0.000 Mt        0.367 Mt    86.730 % 	 ...../PrepareForRendering

		       0.056 Mt    13.270 %         1 x        0.056 Mt        0.000 Mt        0.056 Mt   100.000 % 	 ....../PrepareDrawBundle

		       1.062 Mt     0.981 %         1 x        1.062 Mt        0.000 Mt        0.004 Mt     0.405 % 	 ..../TexturedRLPrep

		       1.058 Mt    99.595 %         1 x        1.058 Mt        0.000 Mt        0.377 Mt    35.624 % 	 ...../PrepareForRendering

		       0.411 Mt    38.886 %         1 x        0.411 Mt        0.000 Mt        0.411 Mt   100.000 % 	 ....../PrepareMaterials

		       0.270 Mt    25.489 %         1 x        0.270 Mt        0.000 Mt        0.270 Mt   100.000 % 	 ....../PrepareDrawBundle

		       4.624 Mt     4.270 %         1 x        4.624 Mt        0.000 Mt        0.005 Mt     0.099 % 	 ..../DeferredRLPrep

		       4.620 Mt    99.901 %         1 x        4.620 Mt        0.000 Mt        1.608 Mt    34.802 % 	 ...../PrepareForRendering

		       0.016 Mt     0.346 %         1 x        0.016 Mt        0.000 Mt        0.016 Mt   100.000 % 	 ....../PrepareMaterials

		       2.722 Mt    58.928 %         1 x        2.722 Mt        0.000 Mt        2.722 Mt   100.000 % 	 ....../BuildMaterialCache

		       0.274 Mt     5.923 %         1 x        0.274 Mt        0.000 Mt        0.274 Mt   100.000 % 	 ....../PrepareDrawBundle

		      14.200 Mt    13.114 %         1 x       14.200 Mt        0.000 Mt        3.685 Mt    25.950 % 	 ..../RayTracingRLPrep

		       0.231 Mt     1.627 %         1 x        0.231 Mt        0.000 Mt        0.231 Mt   100.000 % 	 ...../PrepareAccelerationStructures

		       0.024 Mt     0.168 %         1 x        0.024 Mt        0.000 Mt        0.024 Mt   100.000 % 	 ...../PrepareCache

		       0.427 Mt     3.004 %         1 x        0.427 Mt        0.000 Mt        0.427 Mt   100.000 % 	 ...../BuildCache

		       8.278 Mt    58.297 %         1 x        8.278 Mt        0.000 Mt        0.003 Mt     0.042 % 	 ...../BuildAccelerationStructures

		       8.275 Mt    99.958 %         1 x        8.275 Mt        0.000 Mt        6.330 Mt    76.495 % 	 ....../AccelerationStructureBuild

		       0.986 Mt    11.912 %         1 x        0.986 Mt        0.000 Mt        0.986 Mt   100.000 % 	 ......./BuildBottomLevelAS

		       0.959 Mt    11.593 %         1 x        0.959 Mt        0.000 Mt        0.959 Mt   100.000 % 	 ......./BuildTopLevelAS

		       1.556 Mt    10.954 %         1 x        1.556 Mt        0.000 Mt        1.556 Mt   100.000 % 	 ...../BuildShaderTables

		      10.682 Mt     9.865 %         1 x       10.682 Mt        0.000 Mt       10.682 Mt   100.000 % 	 ..../GpuSidePrep

		       5.358 Mt     0.018 %         1 x        5.358 Mt        0.000 Mt        5.358 Mt   100.000 % 	 ../DeferredRLInitialization

		      50.725 Mt     0.171 %         1 x       50.725 Mt        0.000 Mt        2.207 Mt     4.350 % 	 ../RayTracingRLInitialization

		      48.519 Mt    95.650 %         1 x       48.519 Mt        0.000 Mt       48.519 Mt   100.000 % 	 .../PipelineBuild

		       5.122 Mt     0.017 %         1 x        5.122 Mt        0.000 Mt        5.122 Mt   100.000 % 	 ../ResolveRLInitialization

		   29067.041 Mt    98.044 %     11670 x        2.491 Mt        4.482 Mt      359.792 Mt     1.238 % 	 ../MainLoop

		    1558.556 Mt     5.362 %      1746 x        0.893 Mt        0.402 Mt     1554.565 Mt    99.744 % 	 .../Update

		       3.991 Mt     0.256 %         1 x        3.991 Mt        0.000 Mt        3.991 Mt   100.000 % 	 ..../GuiModelApply

		   27148.693 Mt    93.400 %      6211 x        4.371 Mt        3.561 Mt    16712.241 Mt    61.558 % 	 .../Render

		   10436.452 Mt    38.442 %      6211 x        1.680 Mt        1.360 Mt    10383.195 Mt    99.490 % 	 ..../RayTracing

		      32.047 Mt     0.307 %      6211 x        0.005 Mt        0.002 Mt       32.047 Mt   100.000 % 	 ...../DeferredRLPrep

		      21.209 Mt     0.203 %      6211 x        0.003 Mt        0.003 Mt       21.209 Mt   100.000 % 	 ...../RayTracingRLPrep

	WindowThread:

		   29635.598 Mt   100.000 %         1 x    29635.600 Mt    29635.597 Mt    29635.598 Mt   100.000 % 	 /

	GpuThread:

		    8611.675 Mt   100.000 %      6211 x        1.387 Mt        0.992 Mt      164.920 Mt     1.915 % 	 /

		       0.153 Mt     0.002 %         1 x        0.153 Mt        0.000 Mt        0.005 Mt     3.008 % 	 ./ScenePrep

		       0.149 Mt    96.992 %         1 x        0.149 Mt        0.000 Mt        0.001 Mt     0.474 % 	 ../AccelerationStructureBuild

		       0.088 Mt    59.238 %         1 x        0.088 Mt        0.000 Mt        0.088 Mt   100.000 % 	 .../BuildBottomLevelASGpu

		       0.060 Mt    40.289 %         1 x        0.060 Mt        0.000 Mt        0.060 Mt   100.000 % 	 .../BuildTopLevelASGpu

		    8387.827 Mt    97.401 %      6211 x        1.350 Mt        0.989 Mt       12.535 Mt     0.149 % 	 ./RayTracingGpu

		    4111.889 Mt    49.022 %      6211 x        0.662 Mt        0.262 Mt     4111.889 Mt   100.000 % 	 ../DeferredRLGpu

		    2222.672 Mt    26.499 %      6211 x        0.358 Mt        0.477 Mt     2222.672 Mt   100.000 % 	 ../RayTracingRLGpu

		    2040.731 Mt    24.330 %      6211 x        0.329 Mt        0.248 Mt     2040.731 Mt   100.000 % 	 ../ResolveRLGpu

		      58.775 Mt     0.683 %      6211 x        0.009 Mt        0.003 Mt       58.775 Mt   100.000 % 	 ./Present


	============================


