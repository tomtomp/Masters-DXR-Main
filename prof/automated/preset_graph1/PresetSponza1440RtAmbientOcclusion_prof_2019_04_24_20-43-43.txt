Configuration: 
Command line parameters  = E:\programming\FIT-DIP\Masters-DXR-Main-git\build\Measurement\x64\Debug\Measurement.exe --scene Sponza/Sponza.gltf --profile-automation --profile-camera-track Sponza/SponzaPreset.trk --rt-mode --width 2560 --height 1440 --profile-output PresetSponza1440RtAmbientOcclusion --quality-preset AmbientOcclusion 
Window width             = 2560
Window height            = 1421
Window title             = Direct3D App
Back-buffer count        = 3
Fill Triangles           = enabled
Triangles Font Clock-Wise= enabled
Fast Build AS            = disabled
Target FPS               = 0
Target UPS               = 60
Tearing                  = enabled
VSync                    = disabled
GUI rendering            = disabled
Debug layer              = enabled
GPU validation           = disabled
GPU profiling            = enabled
Render mode              = Ray Tracing
Loaded scene file        = Sponza/Sponza.gltf
Using testing scene      = disabled
Duplication              = 1
Duplication in cube      = disabled
Duplication offset       = 10
BLAS duplication         = disabled
Rays per pixel           = 5
Quality preset           = AmbientOcclusion
Rasterization Quality    : 
  AO                     = enabled
  AO kernel size         = 4
  Shadows                = disabled
  Shadow map resolution  = 1
  Shadow PCF kernel size = 0
  Reflections            = disabled
Ray Tracing Quality      : 
  AO                     = enabled
  AO rays                = 4
  AO kernel size         = 4
  Shadows                = disabled
  Shadow rays            = 0
  Shadow kernel size     = 4
  Reflections            = disabled

ProfilingAggregator: 
Render	,	9.0575	,	8.5268	,	8.1886	,	7.9927	,	8.2637	,	8.4944	,	8.0911	,	8.9678	,	8.0813	,	7.6789	,	8.4087	,	8.494	,	7.9318	,	8.7646	,	8.7236	,	8.5924	,	9.2427	,	8.4263	,	8.043	,	9.2077	,	8.9989	,	9.504	,	8.6732
Update	,	0.5714	,	0.4889	,	0.5025	,	0.579	,	0.9613	,	0.5577	,	0.557	,	0.9597	,	0.9191	,	0.4813	,	0.555	,	0.9723	,	0.4713	,	1.1144	,	0.6519	,	1.0171	,	0.5288	,	0.4863	,	0.5109	,	0.5745	,	0.6537	,	0.6045	,	0.5147
RayTracing	,	3.0174	,	2.517	,	2.5145	,	2.7215	,	2.9636	,	3.1017	,	2.5356	,	3.2046	,	2.9648	,	2.5303	,	2.9596	,	2.9899	,	2.5879	,	2.9876	,	2.9643	,	2.9838	,	2.9896	,	2.5238	,	2.5475	,	3.0731	,	2.9715	,	3.0452	,	2.5049
RayTracingGpu	,	4.60291	,	4.60883	,	4.41722	,	3.75482	,	3.90682	,	3.8392	,	3.99552	,	4.11264	,	3.87946	,	3.86275	,	3.68182	,	3.77232	,	4.14211	,	4.46653	,	4.40566	,	4.14099	,	4.76698	,	4.62563	,	4.17738	,	4.59686	,	4.46656	,	4.59622	,	4.96758
DeferredRLGpu	,	1.45658	,	1.42397	,	1.25757	,	0.764	,	0.71328	,	0.878496	,	1.11146	,	1.17347	,	1.00822	,	0.790752	,	0.680544	,	0.9136	,	1.08419	,	1.22986	,	1.24467	,	0.873952	,	1.06557	,	1.02806	,	0.872352	,	1.31629	,	1.38486	,	1.37094	,	1.68419
RayTracingRLGpu	,	2.15798	,	2.19843	,	2.08518	,	1.99859	,	2.19466	,	1.98342	,	1.9049	,	1.95341	,	1.87853	,	2.08317	,	2.01248	,	1.88467	,	2.08221	,	2.25606	,	2.18301	,	2.26656	,	2.69066	,	2.60656	,	2.30109	,	2.27427	,	2.07709	,	2.07949	,	2.29472
ResolveRLGpu	,	0.987424	,	0.985536	,	1.07344	,	0.991168	,	0.99792	,	0.976128	,	0.978272	,	0.982208	,	0.991936	,	0.987904	,	0.987328	,	0.97312	,	0.9744	,	0.979584	,	0.977088	,	0.999424	,	1.00973	,	0.990112	,	1.00278	,	1.00525	,	1.00352	,	1.1447	,	0.987232
Index	,	0	,	1	,	2	,	3	,	4	,	5	,	6	,	7	,	8	,	9	,	10	,	11	,	12	,	13	,	14	,	15	,	16	,	17	,	18	,	19	,	20	,	21	,	22

ProfilingBuildAggregator: 
BuildBottomLevelAS	,	4.1064
BuildBottomLevelASGpu	,	15.8155
BuildTopLevelAS	,	2.4231
BuildTopLevelASGpu	,	0.615456
Duplication	,	1
Triangles	,	262267
Meshes	,	103
BottomLevels	,	1
TopLevelSize	,	64
BottomLevelsSize	,	14623872
Index	,	0

FpsAggregator: 
FPS	,	74	,	105	,	111	,	116	,	123	,	118	,	120	,	115	,	119	,	122	,	124	,	123	,	120	,	111	,	109	,	115	,	107	,	105	,	109	,	113	,	107	,	110	,	109
UPS	,	59	,	60	,	61	,	60	,	60	,	60	,	60	,	60	,	61	,	60	,	60	,	60	,	61	,	60	,	60	,	60	,	61	,	60	,	60	,	60	,	60	,	60	,	61
FrameTime	,	13.6068	,	9.52749	,	9.04126	,	8.65644	,	8.13145	,	8.49321	,	8.33727	,	8.76074	,	8.45336	,	8.22379	,	8.08232	,	8.14567	,	8.37101	,	9.07382	,	9.18324	,	8.75527	,	9.37209	,	9.52658	,	9.18186	,	8.87912	,	9.38503	,	9.14458	,	9.20264
GigaRays/s	,	1.33675	,	1.90909	,	2.01175	,	2.10119	,	2.23685	,	2.14157	,	2.18163	,	2.07617	,	2.15167	,	2.21173	,	2.25044	,	2.23294	,	2.17283	,	2.00454	,	1.98065	,	2.07747	,	1.94074	,	1.90927	,	1.98095	,	2.04849	,	1.93807	,	1.98902	,	1.97648
Index	,	0	,	1	,	2	,	3	,	4	,	5	,	6	,	7	,	8	,	9	,	10	,	11	,	12	,	13	,	14	,	15	,	16	,	17	,	18	,	19	,	20	,	21	,	22


AccelerationStructure: 
Ray tracing acceleration structure building statistics: 
	Bottom Level Acceleration Structure: 
		Maximum [B]: 14623872
		Minimum [B]: 14623872
		Currently Allocated [B]: 14623872
		Currently Created [num]: 1
		Current Average [B]: 14623872
		Maximum Triangles [num]: 262267
		Minimum Triangles [num]: 262267
		Total Triangles [num]: 262267
		Maximum Meshes [num]: 103
		Minimum Meshes [num]: 103
		Total Meshes [num]: 103
	Top Level Acceleration Structure: 
		Maximum [B]: 64
		Minimum [B]: 64
		Currently Allocated [B]: 64
		Total Allocated [B]: 64
		Total Created [num]: 1
		Average Allocated [B]: 64
		Maximum Instances [num]: 1
		Minimum Instances [num]: 1
		Total Instances [num]: 1
	Scratch Buffer: 
		Maximum [B]: 11660800
		Minimum [B]: 11660800
		Currently Allocated [B]: 11660800
		Total Allocated [B]: 11660800
		Total Created [num]: 1
		Average Allocated [B]: 11660800
ProfilingStack: 
[PROF] Profiling statistics: 
	Scopes entered : 32551
	Scopes exited : 32550
	Overhead per scope [ticks] : 1015.3
	Threads entered : 3
	Threads exited : 0
	============================
		         Sum[Mt]   Parent[%]  Samples[num]       Avg[Mt]   Last/Curr[Mt]        Self[Mt]     Self[%] 	 Name

	main:

		   32508.230 Mt   100.000 %         1 x    32508.234 Mt    32508.229 Mt        0.623 Mt     0.002 % 	 /

		   32507.741 Mt    99.998 %         1 x    32507.747 Mt    32507.740 Mt      339.591 Mt     1.045 % 	 ./Main application loop

		       2.894 Mt     0.009 %         1 x        2.894 Mt        0.000 Mt        2.894 Mt   100.000 % 	 ../TexturedRLInitialization

		       2.319 Mt     0.007 %         1 x        2.319 Mt        0.000 Mt        2.319 Mt   100.000 % 	 ../ShadowMapRLInitialization

		       4.888 Mt     0.015 %         1 x        4.888 Mt        0.000 Mt        4.888 Mt   100.000 % 	 ../AmbientOcclusionRLInitialization

		       4.931 Mt     0.015 %         1 x        4.931 Mt        0.000 Mt        4.931 Mt   100.000 % 	 ../RasterResolveRLInitialization

		    9001.301 Mt    27.690 %         1 x     9001.301 Mt        0.000 Mt       12.176 Mt     0.135 % 	 ../Initialization

		    8989.124 Mt    99.865 %         1 x     8989.124 Mt        0.000 Mt        3.479 Mt     0.039 % 	 .../ScenePrep

		    8875.411 Mt    98.735 %         1 x     8875.411 Mt        0.000 Mt      341.362 Mt     3.846 % 	 ..../SceneLoad

		    8137.031 Mt    91.681 %         1 x     8137.031 Mt        0.000 Mt     1110.378 Mt    13.646 % 	 ...../glTF-LoadScene

		    7026.653 Mt    86.354 %        69 x      101.836 Mt        0.000 Mt     7026.653 Mt   100.000 % 	 ....../glTF-LoadImageData

		     289.706 Mt     3.264 %         1 x      289.706 Mt        0.000 Mt      289.706 Mt   100.000 % 	 ...../glTF-CreateScene

		     107.312 Mt     1.209 %         1 x      107.312 Mt        0.000 Mt      107.312 Mt   100.000 % 	 ...../SceneDesc-CreateScene

		       2.431 Mt     0.027 %         1 x        2.431 Mt        0.000 Mt        0.005 Mt     0.218 % 	 ..../ShadowMapRLPrep

		       2.426 Mt    99.782 %         1 x        2.426 Mt        0.000 Mt        1.733 Mt    71.433 % 	 ...../PrepareForRendering

		       0.693 Mt    28.567 %         1 x        0.693 Mt        0.000 Mt        0.693 Mt   100.000 % 	 ....../PrepareDrawBundle

		       6.040 Mt     0.067 %         1 x        6.040 Mt        0.000 Mt        0.005 Mt     0.083 % 	 ..../TexturedRLPrep

		       6.035 Mt    99.917 %         1 x        6.035 Mt        0.000 Mt        1.146 Mt    18.989 % 	 ...../PrepareForRendering

		       2.664 Mt    44.136 %         1 x        2.664 Mt        0.000 Mt        2.664 Mt   100.000 % 	 ....../PrepareMaterials

		       2.225 Mt    36.875 %         1 x        2.225 Mt        0.000 Mt        2.225 Mt   100.000 % 	 ....../PrepareDrawBundle

		       7.876 Mt     0.088 %         1 x        7.876 Mt        0.000 Mt        0.005 Mt     0.060 % 	 ..../DeferredRLPrep

		       7.871 Mt    99.940 %         1 x        7.871 Mt        0.000 Mt        3.134 Mt    39.821 % 	 ...../PrepareForRendering

		       0.022 Mt     0.274 %         1 x        0.022 Mt        0.000 Mt        0.022 Mt   100.000 % 	 ....../PrepareMaterials

		       2.136 Mt    27.143 %         1 x        2.136 Mt        0.000 Mt        2.136 Mt   100.000 % 	 ....../BuildMaterialCache

		       2.579 Mt    32.762 %         1 x        2.579 Mt        0.000 Mt        2.579 Mt   100.000 % 	 ....../PrepareDrawBundle

		      28.443 Mt     0.316 %         1 x       28.443 Mt        0.000 Mt        6.410 Mt    22.535 % 	 ..../RayTracingRLPrep

		       0.589 Mt     2.071 %         1 x        0.589 Mt        0.000 Mt        0.589 Mt   100.000 % 	 ...../PrepareAccelerationStructures

		       0.024 Mt     0.086 %         1 x        0.024 Mt        0.000 Mt        0.024 Mt   100.000 % 	 ...../PrepareCache

		       5.287 Mt    18.588 %         1 x        5.287 Mt        0.000 Mt        5.287 Mt   100.000 % 	 ...../BuildCache

		      11.719 Mt    41.202 %         1 x       11.719 Mt        0.000 Mt        0.004 Mt     0.034 % 	 ...../BuildAccelerationStructures

		      11.715 Mt    99.966 %         1 x       11.715 Mt        0.000 Mt        5.186 Mt    44.264 % 	 ....../AccelerationStructureBuild

		       4.106 Mt    35.052 %         1 x        4.106 Mt        0.000 Mt        4.106 Mt   100.000 % 	 ......./BuildBottomLevelAS

		       2.423 Mt    20.684 %         1 x        2.423 Mt        0.000 Mt        2.423 Mt   100.000 % 	 ......./BuildTopLevelAS

		       4.414 Mt    15.519 %         1 x        4.414 Mt        0.000 Mt        4.414 Mt   100.000 % 	 ...../BuildShaderTables

		      65.445 Mt     0.728 %         1 x       65.445 Mt        0.000 Mt       65.445 Mt   100.000 % 	 ..../GpuSidePrep

		       4.746 Mt     0.015 %         1 x        4.746 Mt        0.000 Mt        4.746 Mt   100.000 % 	 ../DeferredRLInitialization

		      55.607 Mt     0.171 %         1 x       55.607 Mt        0.000 Mt        1.916 Mt     3.445 % 	 ../RayTracingRLInitialization

		      53.692 Mt    96.555 %         1 x       53.692 Mt        0.000 Mt       53.692 Mt   100.000 % 	 .../PipelineBuild

		       4.629 Mt     0.014 %         1 x        4.629 Mt        0.000 Mt        4.629 Mt   100.000 % 	 ../ResolveRLInitialization

		   23086.835 Mt    71.020 %      7772 x        2.971 Mt        9.328 Mt      156.099 Mt     0.676 % 	 ../MainLoop

		    1151.755 Mt     4.989 %      1385 x        0.832 Mt        0.512 Mt     1150.330 Mt    99.876 % 	 .../Update

		       1.425 Mt     0.124 %         1 x        1.425 Mt        0.000 Mt        1.425 Mt   100.000 % 	 ..../GuiModelApply

		   21778.981 Mt    94.335 %      2587 x        8.419 Mt        8.806 Mt    14554.306 Mt    66.827 % 	 .../Render

		    7224.675 Mt    33.173 %      2587 x        2.793 Mt        2.631 Mt     7205.531 Mt    99.735 % 	 ..../RayTracing

		      10.605 Mt     0.147 %      2587 x        0.004 Mt        0.003 Mt       10.605 Mt   100.000 % 	 ...../DeferredRLPrep

		       8.538 Mt     0.118 %      2587 x        0.003 Mt        0.003 Mt        8.538 Mt   100.000 % 	 ...../RayTracingRLPrep

	WindowThread:

		   32506.439 Mt   100.000 %         1 x    32506.440 Mt    32506.438 Mt    32506.439 Mt   100.000 % 	 /

	GpuThread:

		   11248.794 Mt   100.000 %      2587 x        4.348 Mt        4.769 Mt      274.712 Mt     2.442 % 	 /

		      16.483 Mt     0.147 %         1 x       16.483 Mt        0.000 Mt        0.050 Mt     0.305 % 	 ./ScenePrep

		      16.433 Mt    99.695 %         1 x       16.433 Mt        0.000 Mt        0.002 Mt     0.012 % 	 ../AccelerationStructureBuild

		      15.815 Mt    96.243 %         1 x       15.815 Mt        0.000 Mt       15.815 Mt   100.000 % 	 .../BuildBottomLevelASGpu

		       0.615 Mt     3.745 %         1 x        0.615 Mt        0.000 Mt        0.615 Mt   100.000 % 	 .../BuildTopLevelASGpu

		   10932.023 Mt    97.184 %      2587 x        4.226 Mt        4.674 Mt        3.089 Mt     0.028 % 	 ./RayTracingGpu

		    2813.269 Mt    25.734 %      2587 x        1.087 Mt        1.385 Mt     2813.269 Mt   100.000 % 	 ../DeferredRLGpu

		    5533.296 Mt    50.615 %      2587 x        2.139 Mt        2.064 Mt     5533.296 Mt   100.000 % 	 ../RayTracingRLGpu

		    2582.369 Mt    23.622 %      2587 x        0.998 Mt        1.224 Mt     2582.369 Mt   100.000 % 	 ../ResolveRLGpu

		      25.575 Mt     0.227 %      2587 x        0.010 Mt        0.096 Mt       25.575 Mt   100.000 % 	 ./Present


	============================


