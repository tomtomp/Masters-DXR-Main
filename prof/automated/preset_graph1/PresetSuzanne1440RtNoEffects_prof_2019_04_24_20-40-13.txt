Configuration: 
Command line parameters  = E:\programming\FIT-DIP\Masters-DXR-Main-git\build\Measurement\x64\Debug\Measurement.exe --scene Suzanne/Suzanne.gltf --profile-automation --profile-camera-track Suzanne/Suzanne.trk --rt-mode --width 2560 --height 1440 --profile-output PresetSuzanne1440RtNoEffects --quality-preset NoEffects 
Window width             = 2560
Window height            = 1421
Window title             = Direct3D App
Back-buffer count        = 3
Fill Triangles           = enabled
Triangles Font Clock-Wise= enabled
Fast Build AS            = disabled
Target FPS               = 0
Target UPS               = 60
Tearing                  = enabled
VSync                    = disabled
GUI rendering            = disabled
Debug layer              = enabled
GPU validation           = disabled
GPU profiling            = enabled
Render mode              = Ray Tracing
Loaded scene file        = Suzanne/Suzanne.gltf
Using testing scene      = disabled
Duplication              = 1
Duplication in cube      = disabled
Duplication offset       = 10
BLAS duplication         = disabled
Rays per pixel           = 1
Quality preset           = NoEffects
Rasterization Quality    : 
  AO                     = disabled
  AO kernel size         = 1
  Shadows                = disabled
  Shadow map resolution  = 1
  Shadow PCF kernel size = 0
  Reflections            = disabled
Ray Tracing Quality      : 
  AO                     = disabled
  AO rays                = 0
  AO kernel size         = 4
  Shadows                = disabled
  Shadow rays            = 0
  Shadow kernel size     = 4
  Reflections            = disabled

ProfilingAggregator: 
Render	,	2.714	,	3.1245	,	3.1346	,	4.6746	,	3.0316	,	3.3305	,	3.0165	,	3.7901	,	3.8108	,	4.1905	,	5.7594	,	3.756	,	3.2311	,	5.0279	,	5.1747	,	5.4767	,	3.8847	,	5.0712	,	4.7636	,	3.8756	,	5.1077	,	3.1005	,	3.0103	,	3.1887
Update	,	0.9789	,	0.9376	,	0.4766	,	0.9558	,	0.4729	,	0.7495	,	1.0591	,	0.7148	,	0.8504	,	1.2669	,	0.9334	,	0.4886	,	0.8131	,	0.8179	,	1.1568	,	0.998	,	0.6679	,	1.1063	,	0.4763	,	0.768	,	1.0008	,	0.5328	,	0.4736	,	1.031
RayTracing	,	1.2242	,	1.4894	,	1.2621	,	2.2113	,	1.2264	,	1.2307	,	1.2256	,	1.5955	,	1.2508	,	1.4933	,	2.5198	,	1.2892	,	1.2806	,	2.1601	,	2.5299	,	2.4433	,	1.2289	,	1.8568	,	1.4526	,	1.4359	,	2.4025	,	1.2228	,	1.2206	,	1.253
RayTracingGpu	,	0.45552	,	0.545728	,	0.926144	,	0.92336	,	0.85808	,	0.843296	,	0.882592	,	0.937504	,	1.01878	,	1.18006	,	1.2504	,	1.11366	,	0.98208	,	0.986176	,	1.13955	,	1.60838	,	1.68106	,	1.63398	,	2.00074	,	1.43043	,	1.12698	,	0.941248	,	0.862816	,	0.93776
DeferredRLGpu	,	0.150912	,	0.203712	,	0.364608	,	0.343872	,	0.2656	,	0.213184	,	0.190944	,	0.186752	,	0.246784	,	0.371424	,	0.427552	,	0.319008	,	0.221472	,	0.22368	,	0.343616	,	0.72368	,	0.814528	,	0.807392	,	1.05194	,	0.704928	,	0.498912	,	0.308928	,	0.220448	,	0.222848
RayTracingRLGpu	,	0.18672	,	0.194208	,	0.323136	,	0.351168	,	0.386816	,	0.429408	,	0.479328	,	0.52768	,	0.53024	,	0.5376	,	0.541472	,	0.535776	,	0.529824	,	0.5296	,	0.53536	,	0.551584	,	0.511968	,	0.449696	,	0.452064	,	0.385632	,	0.362336	,	0.415264	,	0.440992	,	0.495232
ResolveRLGpu	,	0.11616	,	0.146496	,	0.236384	,	0.226272	,	0.203904	,	0.1984	,	0.210304	,	0.22128	,	0.239456	,	0.268992	,	0.279104	,	0.256352	,	0.228992	,	0.230592	,	0.258272	,	0.331136	,	0.35232	,	0.37488	,	0.494528	,	0.3376	,	0.263424	,	0.215232	,	0.199552	,	0.217376
Index	,	0	,	1	,	2	,	3	,	4	,	5	,	6	,	7	,	8	,	9	,	10	,	11	,	12	,	13	,	14	,	15	,	16	,	17	,	18	,	19	,	20	,	21	,	22	,	23

ProfilingBuildAggregator: 
BuildBottomLevelAS	,	1.2369
BuildBottomLevelASGpu	,	0.353024
BuildTopLevelAS	,	0.8638
BuildTopLevelASGpu	,	0.088672
Duplication	,	1
Triangles	,	3936
Meshes	,	1
BottomLevels	,	1
TopLevelSize	,	64
BottomLevelsSize	,	224128
Index	,	0

FpsAggregator: 
FPS	,	273	,	281	,	267	,	240	,	238	,	255	,	243	,	239	,	227	,	219	,	207	,	212	,	227	,	235	,	220	,	200	,	189	,	197	,	176	,	189	,	194	,	228	,	252	,	252
UPS	,	59	,	60	,	61	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	61	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	61	,	60	,	60
FrameTime	,	3.66348	,	3.56881	,	3.75695	,	4.18019	,	4.21239	,	3.92505	,	4.11712	,	4.1876	,	4.4161	,	4.5838	,	4.83351	,	4.72169	,	4.41363	,	4.25898	,	4.5508	,	5.02331	,	5.30037	,	5.08013	,	5.68906	,	5.29328	,	5.16138	,	4.39083	,	3.97039	,	3.9725
GigaRays/s	,	0.992978	,	1.01932	,	0.968275	,	0.870238	,	0.863585	,	0.926806	,	0.88357	,	0.868697	,	0.82375	,	0.793613	,	0.752612	,	0.770436	,	0.82421	,	0.854139	,	0.799366	,	0.724176	,	0.686321	,	0.716076	,	0.63943	,	0.687241	,	0.704804	,	0.828489	,	0.916222	,	0.915735
Index	,	0	,	1	,	2	,	3	,	4	,	5	,	6	,	7	,	8	,	9	,	10	,	11	,	12	,	13	,	14	,	15	,	16	,	17	,	18	,	19	,	20	,	21	,	22	,	23


AccelerationStructure: 
Ray tracing acceleration structure building statistics: 
	Bottom Level Acceleration Structure: 
		Maximum [B]: 224128
		Minimum [B]: 224128
		Currently Allocated [B]: 224128
		Currently Created [num]: 1
		Current Average [B]: 224128
		Maximum Triangles [num]: 3936
		Minimum Triangles [num]: 3936
		Total Triangles [num]: 3936
		Maximum Meshes [num]: 1
		Minimum Meshes [num]: 1
		Total Meshes [num]: 1
	Top Level Acceleration Structure: 
		Maximum [B]: 64
		Minimum [B]: 64
		Currently Allocated [B]: 64
		Total Allocated [B]: 64
		Total Created [num]: 1
		Average Allocated [B]: 64
		Maximum Instances [num]: 1
		Minimum Instances [num]: 1
		Total Instances [num]: 1
	Scratch Buffer: 
		Maximum [B]: 34304
		Minimum [B]: 34304
		Currently Allocated [B]: 34304
		Total Allocated [B]: 34304
		Total Created [num]: 1
		Average Allocated [B]: 34304
ProfilingStack: 
[PROF] Profiling statistics: 
	Scopes entered : 57759
	Scopes exited : 57758
	Overhead per scope [ticks] : 1003.7
	Threads entered : 3
	Threads exited : 0
	============================
		         Sum[Mt]   Parent[%]  Samples[num]       Avg[Mt]   Last/Curr[Mt]        Self[Mt]     Self[%] 	 Name

	main:

		   24821.323 Mt   100.000 %         1 x    24821.325 Mt    24821.322 Mt        0.748 Mt     0.003 % 	 /

		   24820.658 Mt    99.997 %         1 x    24820.662 Mt    24820.658 Mt      378.690 Mt     1.526 % 	 ./Main application loop

		       3.704 Mt     0.015 %         1 x        3.704 Mt        0.000 Mt        3.704 Mt   100.000 % 	 ../TexturedRLInitialization

		       2.955 Mt     0.012 %         1 x        2.955 Mt        0.000 Mt        2.955 Mt   100.000 % 	 ../ShadowMapRLInitialization

		       4.898 Mt     0.020 %         1 x        4.898 Mt        0.000 Mt        4.898 Mt   100.000 % 	 ../AmbientOcclusionRLInitialization

		       5.785 Mt     0.023 %         1 x        5.785 Mt        0.000 Mt        5.785 Mt   100.000 % 	 ../RasterResolveRLInitialization

		     276.985 Mt     1.116 %         1 x      276.985 Mt        0.000 Mt       12.190 Mt     4.401 % 	 ../Initialization

		     264.795 Mt    95.599 %         1 x      264.795 Mt        0.000 Mt        1.733 Mt     0.654 % 	 .../ScenePrep

		     223.820 Mt    84.526 %         1 x      223.820 Mt        0.000 Mt       20.960 Mt     9.365 % 	 ..../SceneLoad

		     170.554 Mt    76.201 %         1 x      170.554 Mt        0.000 Mt       20.404 Mt    11.963 % 	 ...../glTF-LoadScene

		     150.150 Mt    88.037 %         2 x       75.075 Mt        0.000 Mt      150.150 Mt   100.000 % 	 ....../glTF-LoadImageData

		      16.368 Mt     7.313 %         1 x       16.368 Mt        0.000 Mt       16.368 Mt   100.000 % 	 ...../glTF-CreateScene

		      15.938 Mt     7.121 %         1 x       15.938 Mt        0.000 Mt       15.938 Mt   100.000 % 	 ...../SceneDesc-CreateScene

		       0.935 Mt     0.353 %         1 x        0.935 Mt        0.000 Mt        0.005 Mt     0.503 % 	 ..../ShadowMapRLPrep

		       0.930 Mt    99.497 %         1 x        0.930 Mt        0.000 Mt        0.870 Mt    93.486 % 	 ...../PrepareForRendering

		       0.061 Mt     6.514 %         1 x        0.061 Mt        0.000 Mt        0.061 Mt   100.000 % 	 ....../PrepareDrawBundle

		       3.613 Mt     1.365 %         1 x        3.613 Mt        0.000 Mt        0.004 Mt     0.125 % 	 ..../TexturedRLPrep

		       3.609 Mt    99.875 %         1 x        3.609 Mt        0.000 Mt        1.967 Mt    54.510 % 	 ...../PrepareForRendering

		       1.381 Mt    38.269 %         1 x        1.381 Mt        0.000 Mt        1.381 Mt   100.000 % 	 ....../PrepareMaterials

		       0.261 Mt     7.221 %         1 x        0.261 Mt        0.000 Mt        0.261 Mt   100.000 % 	 ....../PrepareDrawBundle

		       3.472 Mt     1.311 %         1 x        3.472 Mt        0.000 Mt        0.005 Mt     0.130 % 	 ..../DeferredRLPrep

		       3.467 Mt    99.870 %         1 x        3.467 Mt        0.000 Mt        2.330 Mt    67.206 % 	 ...../PrepareForRendering

		       0.017 Mt     0.496 %         1 x        0.017 Mt        0.000 Mt        0.017 Mt   100.000 % 	 ....../PrepareMaterials

		       0.863 Mt    24.889 %         1 x        0.863 Mt        0.000 Mt        0.863 Mt   100.000 % 	 ....../BuildMaterialCache

		       0.257 Mt     7.409 %         1 x        0.257 Mt        0.000 Mt        0.257 Mt   100.000 % 	 ....../PrepareDrawBundle

		      18.731 Mt     7.074 %         1 x       18.731 Mt        0.000 Mt        4.141 Mt    22.110 % 	 ..../RayTracingRLPrep

		       0.245 Mt     1.309 %         1 x        0.245 Mt        0.000 Mt        0.245 Mt   100.000 % 	 ...../PrepareAccelerationStructures

		       0.024 Mt     0.130 %         1 x        0.024 Mt        0.000 Mt        0.024 Mt   100.000 % 	 ...../PrepareCache

		       2.362 Mt    12.609 %         1 x        2.362 Mt        0.000 Mt        2.362 Mt   100.000 % 	 ...../BuildCache

		       5.684 Mt    30.349 %         1 x        5.684 Mt        0.000 Mt        0.013 Mt     0.227 % 	 ...../BuildAccelerationStructures

		       5.672 Mt    99.773 %         1 x        5.672 Mt        0.000 Mt        3.571 Mt    62.961 % 	 ....../AccelerationStructureBuild

		       1.237 Mt    21.809 %         1 x        1.237 Mt        0.000 Mt        1.237 Mt   100.000 % 	 ......./BuildBottomLevelAS

		       0.864 Mt    15.230 %         1 x        0.864 Mt        0.000 Mt        0.864 Mt   100.000 % 	 ......./BuildTopLevelAS

		       6.274 Mt    33.494 %         1 x        6.274 Mt        0.000 Mt        6.274 Mt   100.000 % 	 ...../BuildShaderTables

		      12.491 Mt     4.717 %         1 x       12.491 Mt        0.000 Mt       12.491 Mt   100.000 % 	 ..../GpuSidePrep

		       5.180 Mt     0.021 %         1 x        5.180 Mt        0.000 Mt        5.180 Mt   100.000 % 	 ../DeferredRLInitialization

		      63.472 Mt     0.256 %         1 x       63.472 Mt        0.000 Mt        2.161 Mt     3.405 % 	 ../RayTracingRLInitialization

		      61.311 Mt    96.595 %         1 x       61.311 Mt        0.000 Mt       61.311 Mt   100.000 % 	 .../PipelineBuild

		      11.628 Mt     0.047 %         1 x       11.628 Mt        0.000 Mt       11.628 Mt   100.000 % 	 ../ResolveRLInitialization

		   24067.360 Mt    96.965 %      7112 x        3.384 Mt       13.792 Mt      927.607 Mt     3.854 % 	 ../MainLoop

		    1291.619 Mt     5.367 %      1445 x        0.894 Mt        1.466 Mt     1287.626 Mt    99.691 % 	 .../Update

		       3.993 Mt     0.309 %         1 x        3.993 Mt        0.000 Mt        3.993 Mt   100.000 % 	 ..../GuiModelApply

		   21848.134 Mt    90.779 %      5462 x        4.000 Mt        8.836 Mt    13025.935 Mt    59.620 % 	 .../Render

		    8822.199 Mt    40.380 %      5462 x        1.615 Mt        4.609 Mt     8776.572 Mt    99.483 % 	 ..../RayTracing

		      27.938 Mt     0.317 %      5462 x        0.005 Mt        0.023 Mt       27.938 Mt   100.000 % 	 ...../DeferredRLPrep

		      17.690 Mt     0.201 %      5462 x        0.003 Mt        0.009 Mt       17.690 Mt   100.000 % 	 ...../RayTracingRLPrep

	WindowThread:

		   24810.303 Mt   100.000 %         1 x    24810.304 Mt    24810.302 Mt    24810.303 Mt   100.000 % 	 /

	GpuThread:

		    6083.524 Mt   100.000 %      5462 x        1.114 Mt        0.979 Mt      194.966 Mt     3.205 % 	 /

		       0.449 Mt     0.007 %         1 x        0.449 Mt        0.000 Mt        0.006 Mt     1.326 % 	 ./ScenePrep

		       0.443 Mt    98.674 %         1 x        0.443 Mt        0.000 Mt        0.001 Mt     0.311 % 	 ../AccelerationStructureBuild

		       0.353 Mt    79.676 %         1 x        0.353 Mt        0.000 Mt        0.353 Mt   100.000 % 	 .../BuildBottomLevelASGpu

		       0.089 Mt    20.013 %         1 x        0.089 Mt        0.000 Mt        0.089 Mt   100.000 % 	 .../BuildTopLevelASGpu

		    5824.444 Mt    95.741 %      5462 x        1.066 Mt        0.975 Mt       11.386 Mt     0.195 % 	 ./RayTracingGpu

		    2017.335 Mt    34.636 %      5462 x        0.369 Mt        0.232 Mt     2017.335 Mt   100.000 % 	 ../DeferredRLGpu

		    2408.364 Mt    41.349 %      5462 x        0.441 Mt        0.517 Mt     2408.364 Mt   100.000 % 	 ../RayTracingRLGpu

		    1387.359 Mt    23.820 %      5462 x        0.254 Mt        0.225 Mt     1387.359 Mt   100.000 % 	 ../ResolveRLGpu

		      63.664 Mt     1.047 %      5462 x        0.012 Mt        0.003 Mt       63.664 Mt   100.000 % 	 ./Present


	============================


