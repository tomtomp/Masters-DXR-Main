Configuration: 
Command line parameters  = E:\programming\FIT-DIP\Masters-DXR-Main-git\build\Measurement\x64\Debug\Measurement.exe --scene Sponza/Sponza.gltf --profile-automation --profile-camera-track Sponza/SponzaPreset.trk --rt-mode --width 2560 --height 1440 --profile-output PresetSponza1440RtAmbientOcclusion16 --quality-preset AmbientOcclusion16 
Window width             = 2560
Window height            = 1421
Window title             = Direct3D App
Back-buffer count        = 3
Fill Triangles           = enabled
Triangles Font Clock-Wise= enabled
Fast Build AS            = disabled
Target FPS               = 0
Target UPS               = 60
Tearing                  = enabled
VSync                    = disabled
GUI rendering            = disabled
Debug layer              = enabled
GPU validation           = disabled
GPU profiling            = enabled
Render mode              = Ray Tracing
Loaded scene file        = Sponza/Sponza.gltf
Using testing scene      = disabled
Duplication              = 1
Duplication in cube      = disabled
Duplication offset       = 10
BLAS duplication         = disabled
Rays per pixel           = 17
Quality preset           = AmbientOcclusion16
Rasterization Quality    : 
  AO                     = enabled
  AO kernel size         = 16
  Shadows                = disabled
  Shadow map resolution  = 1
  Shadow PCF kernel size = 0
  Reflections            = disabled
Ray Tracing Quality      : 
  AO                     = enabled
  AO rays                = 16
  AO kernel size         = 4
  Shadows                = disabled
  Shadow rays            = 0
  Shadow kernel size     = 4
  Reflections            = disabled

ProfilingAggregator: 
Render	,	15.7119	,	15.4322	,	15.4763	,	13.3811	,	13.8313	,	14.4203	,	14.5032	,	14.5681	,	14.2559	,	13.5073	,	13.5119	,	14.6137	,	15.425	,	15.9973	,	15.7448	,	15.6418	,	17.5528	,	17.4553	,	15.7728	,	15.9877	,	15.6985	,	15.4361	,	15.3662
Update	,	0.7894	,	1.0691	,	0.5844	,	0.5194	,	0.5541	,	0.9396	,	1.1568	,	0.7187	,	0.5759	,	0.6815	,	0.5768	,	0.6586	,	0.6896	,	0.5761	,	0.9593	,	1.0704	,	1.022	,	0.4885	,	0.6064	,	0.9545	,	0.6337	,	0.6796	,	0.6732
RayTracing	,	3.0569	,	3.0262	,	3.234	,	3.0145	,	3.0217	,	3.0084	,	3.0205	,	3.0183	,	3.0991	,	3.0639	,	3.0348	,	3.151	,	3.0498	,	3.0554	,	3.0281	,	3.0699	,	3.0907	,	3.0916	,	3.0655	,	3.0987	,	3.5907	,	3.0689	,	3.0501
RayTracingGpu	,	11.1064	,	10.8547	,	10.6857	,	8.76125	,	9.26995	,	9.93091	,	9.88566	,	10.2609	,	9.60608	,	8.85901	,	8.87354	,	9.47674	,	10.6663	,	11.3804	,	11.2831	,	10.9657	,	12.8569	,	12.6432	,	11.0645	,	11.2304	,	10.372	,	10.7555	,	10.6921
DeferredRLGpu	,	1.46531	,	1.41648	,	1.23731	,	0.77104	,	0.707456	,	0.867776	,	1.15235	,	1.29165	,	1.00672	,	0.780928	,	0.694752	,	0.930688	,	1.10624	,	1.21539	,	1.4183	,	0.905504	,	1.09136	,	1.03683	,	0.872448	,	1.38346	,	1.35894	,	1.37498	,	1.36333
RayTracingRLGpu	,	8.67149	,	8.44931	,	8.46176	,	7.00048	,	7.57773	,	8.04525	,	7.72778	,	7.99059	,	7.60451	,	7.09024	,	7.1785	,	7.3631	,	8.56435	,	9.15997	,	8.86339	,	9.0671	,	10.7767	,	10.5898	,	9.18339	,	8.84749	,	8.03408	,	8.37533	,	8.31914
ResolveRLGpu	,	0.968576	,	0.98784	,	0.985472	,	0.988192	,	0.983712	,	1.01648	,	1.00448	,	0.977504	,	0.993664	,	0.98704	,	0.998944	,	1.18045	,	0.99456	,	1.00269	,	1.00042	,	0.990464	,	0.98784	,	1.0113	,	1.00774	,	0.998272	,	0.978016	,	1.00365	,	1.00803
Index	,	0	,	1	,	2	,	3	,	4	,	5	,	6	,	7	,	8	,	9	,	10	,	11	,	12	,	13	,	14	,	15	,	16	,	17	,	18	,	19	,	20	,	21	,	22

ProfilingBuildAggregator: 
BuildBottomLevelAS	,	4.8283
BuildBottomLevelASGpu	,	11.9952
BuildTopLevelAS	,	2.9302
BuildTopLevelASGpu	,	0.442304
Duplication	,	1
Triangles	,	262267
Meshes	,	103
BottomLevels	,	1
TopLevelSize	,	64
BottomLevelsSize	,	14623872
Index	,	0

FpsAggregator: 
FPS	,	40	,	59	,	62	,	66	,	71	,	66	,	70	,	65	,	67	,	69	,	70	,	69	,	66	,	60	,	60	,	62	,	56	,	54	,	58	,	61	,	61	,	63	,	63
UPS	,	59	,	61	,	60	,	61	,	61	,	60	,	61	,	61	,	60	,	61	,	60	,	60	,	60	,	61	,	60	,	60	,	60	,	61	,	60	,	61	,	61	,	60	,	61
FrameTime	,	25.2819	,	17.075	,	16.3111	,	15.2339	,	14.2801	,	15.363	,	14.487	,	15.4974	,	15.0845	,	14.5978	,	14.3657	,	14.5757	,	15.1738	,	16.7923	,	16.6908	,	16.206	,	17.9264	,	18.5824	,	17.3779	,	16.654	,	16.4956	,	15.9743	,	16.1213
GigaRays/s	,	2.4461	,	3.62179	,	3.79139	,	4.05949	,	4.33065	,	4.02538	,	4.26878	,	3.99048	,	4.0997	,	4.23639	,	4.30483	,	4.24281	,	4.07557	,	3.68276	,	3.70516	,	3.81599	,	3.44978	,	3.32798	,	3.55864	,	3.71333	,	3.74899	,	3.87133	,	3.83604
Index	,	0	,	1	,	2	,	3	,	4	,	5	,	6	,	7	,	8	,	9	,	10	,	11	,	12	,	13	,	14	,	15	,	16	,	17	,	18	,	19	,	20	,	21	,	22


AccelerationStructure: 
Ray tracing acceleration structure building statistics: 
	Bottom Level Acceleration Structure: 
		Maximum [B]: 14623872
		Minimum [B]: 14623872
		Currently Allocated [B]: 14623872
		Currently Created [num]: 1
		Current Average [B]: 14623872
		Maximum Triangles [num]: 262267
		Minimum Triangles [num]: 262267
		Total Triangles [num]: 262267
		Maximum Meshes [num]: 103
		Minimum Meshes [num]: 103
		Total Meshes [num]: 103
	Top Level Acceleration Structure: 
		Maximum [B]: 64
		Minimum [B]: 64
		Currently Allocated [B]: 64
		Total Allocated [B]: 64
		Total Created [num]: 1
		Average Allocated [B]: 64
		Maximum Instances [num]: 1
		Minimum Instances [num]: 1
		Total Instances [num]: 1
	Scratch Buffer: 
		Maximum [B]: 11660800
		Minimum [B]: 11660800
		Currently Allocated [B]: 11660800
		Total Allocated [B]: 11660800
		Total Created [num]: 1
		Average Allocated [B]: 11660800
ProfilingStack: 
[PROF] Profiling statistics: 
	Scopes entered : 20563
	Scopes exited : 20562
	Overhead per scope [ticks] : 1023.3
	Threads entered : 3
	Threads exited : 0
	============================
		         Sum[Mt]   Parent[%]  Samples[num]       Avg[Mt]   Last/Curr[Mt]        Self[Mt]     Self[%] 	 Name

	main:

		   32669.752 Mt   100.000 %         1 x    32669.755 Mt    32669.751 Mt        0.706 Mt     0.002 % 	 /

		   32669.165 Mt    99.998 %         1 x    32669.171 Mt    32669.164 Mt      349.673 Mt     1.070 % 	 ./Main application loop

		       3.861 Mt     0.012 %         1 x        3.861 Mt        0.000 Mt        3.861 Mt   100.000 % 	 ../TexturedRLInitialization

		       3.233 Mt     0.010 %         1 x        3.233 Mt        0.000 Mt        3.233 Mt   100.000 % 	 ../ShadowMapRLInitialization

		       8.659 Mt     0.027 %         1 x        8.659 Mt        0.000 Mt        8.659 Mt   100.000 % 	 ../AmbientOcclusionRLInitialization

		       5.622 Mt     0.017 %         1 x        5.622 Mt        0.000 Mt        5.622 Mt   100.000 % 	 ../RasterResolveRLInitialization

		    9038.986 Mt    27.668 %         1 x     9038.986 Mt        0.000 Mt       12.129 Mt     0.134 % 	 ../Initialization

		    9026.857 Mt    99.866 %         1 x     9026.857 Mt        0.000 Mt        3.561 Mt     0.039 % 	 .../ScenePrep

		    8914.126 Mt    98.751 %         1 x     8914.126 Mt        0.000 Mt      354.767 Mt     3.980 % 	 ..../SceneLoad

		    8170.183 Mt    91.654 %         1 x     8170.183 Mt        0.000 Mt     1108.014 Mt    13.562 % 	 ...../glTF-LoadScene

		    7062.169 Mt    86.438 %        69 x      102.350 Mt        0.000 Mt     7062.169 Mt   100.000 % 	 ....../glTF-LoadImageData

		     290.172 Mt     3.255 %         1 x      290.172 Mt        0.000 Mt      290.172 Mt   100.000 % 	 ...../glTF-CreateScene

		      99.003 Mt     1.111 %         1 x       99.003 Mt        0.000 Mt       99.003 Mt   100.000 % 	 ...../SceneDesc-CreateScene

		       2.521 Mt     0.028 %         1 x        2.521 Mt        0.000 Mt        0.005 Mt     0.206 % 	 ..../ShadowMapRLPrep

		       2.515 Mt    99.794 %         1 x        2.515 Mt        0.000 Mt        1.783 Mt    70.874 % 	 ...../PrepareForRendering

		       0.733 Mt    29.126 %         1 x        0.733 Mt        0.000 Mt        0.733 Mt   100.000 % 	 ....../PrepareDrawBundle

		       5.883 Mt     0.065 %         1 x        5.883 Mt        0.000 Mt        0.005 Mt     0.083 % 	 ..../TexturedRLPrep

		       5.878 Mt    99.917 %         1 x        5.878 Mt        0.000 Mt        1.012 Mt    17.213 % 	 ...../PrepareForRendering

		       2.629 Mt    44.726 %         1 x        2.629 Mt        0.000 Mt        2.629 Mt   100.000 % 	 ....../PrepareMaterials

		       2.237 Mt    38.061 %         1 x        2.237 Mt        0.000 Mt        2.237 Mt   100.000 % 	 ....../PrepareDrawBundle

		       7.801 Mt     0.086 %         1 x        7.801 Mt        0.000 Mt        0.005 Mt     0.062 % 	 ..../DeferredRLPrep

		       7.796 Mt    99.938 %         1 x        7.796 Mt        0.000 Mt        3.025 Mt    38.801 % 	 ...../PrepareForRendering

		       0.020 Mt     0.262 %         1 x        0.020 Mt        0.000 Mt        0.020 Mt   100.000 % 	 ....../PrepareMaterials

		       2.119 Mt    27.179 %         1 x        2.119 Mt        0.000 Mt        2.119 Mt   100.000 % 	 ....../BuildMaterialCache

		       2.632 Mt    33.758 %         1 x        2.632 Mt        0.000 Mt        2.632 Mt   100.000 % 	 ....../PrepareDrawBundle

		      30.403 Mt     0.337 %         1 x       30.403 Mt        0.000 Mt        7.814 Mt    25.703 % 	 ..../RayTracingRLPrep

		       0.626 Mt     2.059 %         1 x        0.626 Mt        0.000 Mt        0.626 Mt   100.000 % 	 ...../PrepareAccelerationStructures

		       0.025 Mt     0.081 %         1 x        0.025 Mt        0.000 Mt        0.025 Mt   100.000 % 	 ...../PrepareCache

		       5.312 Mt    17.472 %         1 x        5.312 Mt        0.000 Mt        5.312 Mt   100.000 % 	 ...../BuildCache

		      12.395 Mt    40.770 %         1 x       12.395 Mt        0.000 Mt        0.004 Mt     0.032 % 	 ...../BuildAccelerationStructures

		      12.391 Mt    99.968 %         1 x       12.391 Mt        0.000 Mt        4.633 Mt    37.387 % 	 ....../AccelerationStructureBuild

		       4.828 Mt    38.966 %         1 x        4.828 Mt        0.000 Mt        4.828 Mt   100.000 % 	 ......./BuildBottomLevelAS

		       2.930 Mt    23.647 %         1 x        2.930 Mt        0.000 Mt        2.930 Mt   100.000 % 	 ......./BuildTopLevelAS

		       4.231 Mt    13.916 %         1 x        4.231 Mt        0.000 Mt        4.231 Mt   100.000 % 	 ...../BuildShaderTables

		      62.564 Mt     0.693 %         1 x       62.564 Mt        0.000 Mt       62.564 Mt   100.000 % 	 ..../GpuSidePrep

		       5.452 Mt     0.017 %         1 x        5.452 Mt        0.000 Mt        5.452 Mt   100.000 % 	 ../DeferredRLInitialization

		      48.904 Mt     0.150 %         1 x       48.904 Mt        0.000 Mt        2.282 Mt     4.666 % 	 ../RayTracingRLInitialization

		      46.623 Mt    95.334 %         1 x       46.623 Mt        0.000 Mt       46.623 Mt   100.000 % 	 .../PipelineBuild

		       4.940 Mt     0.015 %         1 x        4.940 Mt        0.000 Mt        4.940 Mt   100.000 % 	 ../ResolveRLInitialization

		   23199.835 Mt    71.014 %      6110 x        3.797 Mt       27.430 Mt      151.523 Mt     0.653 % 	 ../MainLoop

		    1167.433 Mt     5.032 %      1391 x        0.839 Mt        0.709 Mt     1166.023 Mt    99.879 % 	 .../Update

		       1.410 Mt     0.121 %         1 x        1.410 Mt        0.000 Mt        1.410 Mt   100.000 % 	 ..../GuiModelApply

		   21880.879 Mt    94.315 %      1439 x       15.206 Mt       16.975 Mt    17433.444 Mt    79.674 % 	 .../Render

		    4447.435 Mt    20.326 %      1439 x        3.091 Mt        4.014 Mt     4436.901 Mt    99.763 % 	 ..../RayTracing

		       5.067 Mt     0.114 %      1439 x        0.004 Mt        0.005 Mt        5.067 Mt   100.000 % 	 ...../DeferredRLPrep

		       5.466 Mt     0.123 %      1439 x        0.004 Mt        0.007 Mt        5.466 Mt   100.000 % 	 ...../RayTracingRLPrep

	WindowThread:

		   32667.556 Mt   100.000 %         1 x    32667.557 Mt    32667.555 Mt    32667.556 Mt   100.000 % 	 /

	GpuThread:

		   15399.406 Mt   100.000 %      1439 x       10.701 Mt       10.589 Mt      261.326 Mt     1.697 % 	 /

		      12.472 Mt     0.081 %         1 x       12.472 Mt        0.000 Mt        0.032 Mt     0.254 % 	 ./ScenePrep

		      12.440 Mt    99.746 %         1 x       12.440 Mt        0.000 Mt        0.002 Mt     0.020 % 	 ../AccelerationStructureBuild

		      11.995 Mt    96.425 %         1 x       11.995 Mt        0.000 Mt       11.995 Mt   100.000 % 	 .../BuildBottomLevelASGpu

		       0.442 Mt     3.556 %         1 x        0.442 Mt        0.000 Mt        0.442 Mt   100.000 % 	 .../BuildTopLevelASGpu

		   15113.728 Mt    98.145 %      1439 x       10.503 Mt       10.587 Mt        2.013 Mt     0.013 % 	 ./RayTracingGpu

		    1622.752 Mt    10.737 %      1439 x        1.128 Mt        1.575 Mt     1622.752 Mt   100.000 % 	 ../DeferredRLGpu

		   12033.830 Mt    79.622 %      1439 x        8.363 Mt        8.029 Mt    12033.830 Mt   100.000 % 	 ../RayTracingRLGpu

		    1455.133 Mt     9.628 %      1439 x        1.011 Mt        0.982 Mt     1455.133 Mt   100.000 % 	 ../ResolveRLGpu

		      11.880 Mt     0.077 %      1439 x        0.008 Mt        0.002 Mt       11.880 Mt   100.000 % 	 ./Present


	============================


