Configuration: 
Command line parameters  = E:\programming\FIT-DIP\Masters-DXR-Main-git\build\Measurement\x64\Debug\Measurement.exe --scene Sponza/Sponza.gltf --profile-automation --profile-camera-track Sponza/SponzaPreset.trk --rt-mode --width 2560 --height 1440 --profile-output PresetSponza1440RtSoftShadows --quality-preset SoftShadows 
Window width             = 2560
Window height            = 1421
Window title             = Direct3D App
Back-buffer count        = 3
Fill Triangles           = enabled
Triangles Font Clock-Wise= enabled
Fast Build AS            = disabled
Target FPS               = 0
Target UPS               = 60
Tearing                  = enabled
VSync                    = disabled
GUI rendering            = disabled
Debug layer              = enabled
GPU validation           = disabled
GPU profiling            = enabled
Render mode              = Ray Tracing
Loaded scene file        = Sponza/Sponza.gltf
Using testing scene      = disabled
Duplication              = 1
Duplication in cube      = disabled
Duplication offset       = 10
BLAS duplication         = disabled
Rays per pixel           = 5
Quality preset           = SoftShadows
Rasterization Quality    : 
  AO                     = disabled
  AO kernel size         = 1
  Shadows                = enabled
  Shadow map resolution  = 4096
  Shadow PCF kernel size = 4
  Reflections            = disabled
Ray Tracing Quality      : 
  AO                     = disabled
  AO rays                = 0
  AO kernel size         = 4
  Shadows                = enabled
  Shadow rays            = 4
  Shadow kernel size     = 4
  Reflections            = disabled

ProfilingAggregator: 
Render	,	10.515	,	10.2806	,	9.4893	,	9.7859	,	9.9606	,	9.716	,	9.9425	,	10.1878	,	9.9111	,	9.2836	,	9.6946	,	9.3803	,	9.7734	,	10.0939	,	9.2229	,	9.1006	,	9.2907	,	9.7245	,	8.5707	,	9.7583	,	9.6049	,	9.6065	,	10.8151
Update	,	0.9269	,	0.5018	,	0.5147	,	0.5464	,	0.6131	,	0.5333	,	0.4898	,	0.6417	,	0.7379	,	0.9213	,	0.9085	,	0.5262	,	0.4896	,	1.001	,	0.8435	,	0.5766	,	0.4866	,	0.5824	,	1.0238	,	0.5786	,	0.4941	,	0.507	,	0.9111
RayTracing	,	2.8512	,	3.0337	,	2.5665	,	3.0232	,	3.0616	,	2.5572	,	3.0392	,	3.0166	,	3.0593	,	3.0645	,	3.1751	,	2.6694	,	2.5941	,	2.7345	,	2.5436	,	3.0513	,	2.5819	,	3.1027	,	2.5712	,	2.6005	,	2.5965	,	2.7037	,	3.0248
RayTracingGpu	,	6.08886	,	5.74675	,	5.61818	,	5.28813	,	5.33437	,	5.65162	,	5.30253	,	5.38157	,	5.48102	,	4.78602	,	4.86755	,	5.45498	,	5.89747	,	5.97638	,	5.33776	,	4.5976	,	5.43066	,	5.2335	,	4.59542	,	5.53222	,	5.65917	,	5.63549	,	6.0904
DeferredRLGpu	,	1.55747	,	1.41613	,	1.24851	,	0.757696	,	0.699456	,	0.880704	,	1.12861	,	1.18109	,	1.1713	,	0.790112	,	0.678176	,	0.902752	,	1.07923	,	1.23306	,	1.23619	,	0.87616	,	1.07846	,	1.03478	,	0.866272	,	1.35082	,	1.41341	,	1.3855	,	1.37526
RayTracingRLGpu	,	2.81235	,	2.60413	,	2.45942	,	2.8089	,	2.91533	,	3.03738	,	2.46262	,	2.32192	,	2.59674	,	2.29411	,	2.48269	,	2.68566	,	2.93453	,	3.0343	,	2.39315	,	2.01443	,	2.63955	,	2.47021	,	1.99875	,	2.44586	,	2.5281	,	2.53443	,	2.83728
ResolveRLGpu	,	1.71795	,	1.72541	,	1.90931	,	1.71914	,	1.71859	,	1.73248	,	1.71008	,	1.87757	,	1.71219	,	1.69968	,	1.70544	,	1.8656	,	1.88262	,	1.70794	,	1.7073	,	1.70589	,	1.71168	,	1.72768	,	1.72915	,	1.73427	,	1.7167	,	1.7143	,	1.87645
Index	,	0	,	1	,	2	,	3	,	4	,	5	,	6	,	7	,	8	,	9	,	10	,	11	,	12	,	13	,	14	,	15	,	16	,	17	,	18	,	19	,	20	,	21	,	22

ProfilingBuildAggregator: 
BuildBottomLevelAS	,	3.7827
BuildBottomLevelASGpu	,	12.6268
BuildTopLevelAS	,	1.943
BuildTopLevelASGpu	,	0.432256
Duplication	,	1
Triangles	,	262267
Meshes	,	103
BottomLevels	,	1
TopLevelSize	,	64
BottomLevelsSize	,	14623872
Index	,	0

FpsAggregator: 
FPS	,	67	,	94	,	98	,	97	,	98	,	93	,	98	,	98	,	97	,	101	,	101	,	99	,	98	,	94	,	95	,	105	,	99	,	97	,	101	,	104	,	96	,	95	,	96
UPS	,	59	,	61	,	60	,	61	,	60	,	60	,	60	,	60	,	61	,	60	,	60	,	61	,	60	,	60	,	61	,	60	,	61	,	60	,	60	,	60	,	61	,	60	,	60
FrameTime	,	15.0518	,	10.707	,	10.3001	,	10.346	,	10.2481	,	10.8223	,	10.2194	,	10.2355	,	10.3564	,	9.93134	,	9.96232	,	10.1484	,	10.2507	,	10.7116	,	10.5718	,	9.60304	,	10.1774	,	10.3474	,	9.98376	,	9.62993	,	10.4838	,	10.5594	,	10.4894
GigaRays/s	,	1.20841	,	1.69877	,	1.76589	,	1.75805	,	1.77484	,	1.68067	,	1.77983	,	1.77703	,	1.75629	,	1.83145	,	1.82576	,	1.79229	,	1.7744	,	1.69804	,	1.7205	,	1.89407	,	1.78718	,	1.75782	,	1.82184	,	1.88878	,	1.73495	,	1.72253	,	1.73403
Index	,	0	,	1	,	2	,	3	,	4	,	5	,	6	,	7	,	8	,	9	,	10	,	11	,	12	,	13	,	14	,	15	,	16	,	17	,	18	,	19	,	20	,	21	,	22


AccelerationStructure: 
Ray tracing acceleration structure building statistics: 
	Bottom Level Acceleration Structure: 
		Maximum [B]: 14623872
		Minimum [B]: 14623872
		Currently Allocated [B]: 14623872
		Currently Created [num]: 1
		Current Average [B]: 14623872
		Maximum Triangles [num]: 262267
		Minimum Triangles [num]: 262267
		Total Triangles [num]: 262267
		Maximum Meshes [num]: 103
		Minimum Meshes [num]: 103
		Total Meshes [num]: 103
	Top Level Acceleration Structure: 
		Maximum [B]: 64
		Minimum [B]: 64
		Currently Allocated [B]: 64
		Total Allocated [B]: 64
		Total Created [num]: 1
		Average Allocated [B]: 64
		Maximum Instances [num]: 1
		Minimum Instances [num]: 1
		Total Instances [num]: 1
	Scratch Buffer: 
		Maximum [B]: 11660800
		Minimum [B]: 11660800
		Currently Allocated [B]: 11660800
		Total Allocated [B]: 11660800
		Total Created [num]: 1
		Average Allocated [B]: 11660800
ProfilingStack: 
[PROF] Profiling statistics: 
	Scopes entered : 28358
	Scopes exited : 28357
	Overhead per scope [ticks] : 1003.4
	Threads entered : 3
	Threads exited : 0
	============================
		         Sum[Mt]   Parent[%]  Samples[num]       Avg[Mt]   Last/Curr[Mt]        Self[Mt]     Self[%] 	 Name

	main:

		   33745.689 Mt   100.000 %         1 x    33745.691 Mt    33745.688 Mt        0.569 Mt     0.002 % 	 /

		   33745.177 Mt    99.998 %         1 x    33745.180 Mt    33745.177 Mt      373.963 Mt     1.108 % 	 ./Main application loop

		       2.922 Mt     0.009 %         1 x        2.922 Mt        0.000 Mt        2.922 Mt   100.000 % 	 ../TexturedRLInitialization

		       2.275 Mt     0.007 %         1 x        2.275 Mt        0.000 Mt        2.275 Mt   100.000 % 	 ../ShadowMapRLInitialization

		       4.064 Mt     0.012 %         1 x        4.064 Mt        0.000 Mt        4.064 Mt   100.000 % 	 ../AmbientOcclusionRLInitialization

		       6.064 Mt     0.018 %         1 x        6.064 Mt        0.000 Mt        6.064 Mt   100.000 % 	 ../RasterResolveRLInitialization

		   10145.143 Mt    30.064 %         1 x    10145.143 Mt        0.000 Mt       12.309 Mt     0.121 % 	 ../Initialization

		   10132.834 Mt    99.879 %         1 x    10132.834 Mt        0.000 Mt        4.762 Mt     0.047 % 	 .../ScenePrep

		    9965.875 Mt    98.352 %         1 x     9965.875 Mt        0.000 Mt      852.814 Mt     8.557 % 	 ..../SceneLoad

		    8209.533 Mt    82.376 %         1 x     8209.533 Mt        0.000 Mt     1125.545 Mt    13.710 % 	 ...../glTF-LoadScene

		    7083.988 Mt    86.290 %        69 x      102.666 Mt        0.000 Mt     7083.988 Mt   100.000 % 	 ....../glTF-LoadImageData

		     572.883 Mt     5.748 %         1 x      572.883 Mt        0.000 Mt      572.883 Mt   100.000 % 	 ...../glTF-CreateScene

		     330.644 Mt     3.318 %         1 x      330.644 Mt        0.000 Mt      330.644 Mt   100.000 % 	 ...../SceneDesc-CreateScene

		       6.001 Mt     0.059 %         1 x        6.001 Mt        0.000 Mt        0.005 Mt     0.088 % 	 ..../ShadowMapRLPrep

		       5.996 Mt    99.912 %         1 x        5.996 Mt        0.000 Mt        3.441 Mt    57.388 % 	 ...../PrepareForRendering

		       2.555 Mt    42.612 %         1 x        2.555 Mt        0.000 Mt        2.555 Mt   100.000 % 	 ....../PrepareDrawBundle

		      18.362 Mt     0.181 %         1 x       18.362 Mt        0.000 Mt        0.006 Mt     0.035 % 	 ..../TexturedRLPrep

		      18.356 Mt    99.965 %         1 x       18.356 Mt        0.000 Mt        3.105 Mt    16.915 % 	 ...../PrepareForRendering

		       7.405 Mt    40.339 %         1 x        7.405 Mt        0.000 Mt        7.405 Mt   100.000 % 	 ....../PrepareMaterials

		       7.846 Mt    42.746 %         1 x        7.846 Mt        0.000 Mt        7.846 Mt   100.000 % 	 ....../PrepareDrawBundle

		      17.403 Mt     0.172 %         1 x       17.403 Mt        0.000 Mt        0.017 Mt     0.095 % 	 ..../DeferredRLPrep

		      17.387 Mt    99.905 %         1 x       17.387 Mt        0.000 Mt        5.574 Mt    32.058 % 	 ...../PrepareForRendering

		       0.063 Mt     0.361 %         1 x        0.063 Mt        0.000 Mt        0.063 Mt   100.000 % 	 ....../PrepareMaterials

		       2.908 Mt    16.726 %         1 x        2.908 Mt        0.000 Mt        2.908 Mt   100.000 % 	 ....../BuildMaterialCache

		       8.842 Mt    50.855 %         1 x        8.842 Mt        0.000 Mt        8.842 Mt   100.000 % 	 ....../PrepareDrawBundle

		      56.956 Mt     0.562 %         1 x       56.956 Mt        0.000 Mt        9.896 Mt    17.376 % 	 ..../RayTracingRLPrep

		       1.922 Mt     3.375 %         1 x        1.922 Mt        0.000 Mt        1.922 Mt   100.000 % 	 ...../PrepareAccelerationStructures

		       0.078 Mt     0.136 %         1 x        0.078 Mt        0.000 Mt        0.078 Mt   100.000 % 	 ...../PrepareCache

		      16.460 Mt    28.899 %         1 x       16.460 Mt        0.000 Mt       16.460 Mt   100.000 % 	 ...../BuildCache

		      15.771 Mt    27.690 %         1 x       15.771 Mt        0.000 Mt        0.014 Mt     0.088 % 	 ...../BuildAccelerationStructures

		      15.757 Mt    99.912 %         1 x       15.757 Mt        0.000 Mt       10.031 Mt    63.663 % 	 ....../AccelerationStructureBuild

		       3.783 Mt    24.006 %         1 x        3.783 Mt        0.000 Mt        3.783 Mt   100.000 % 	 ......./BuildBottomLevelAS

		       1.943 Mt    12.331 %         1 x        1.943 Mt        0.000 Mt        1.943 Mt   100.000 % 	 ......./BuildTopLevelAS

		      12.829 Mt    22.524 %         1 x       12.829 Mt        0.000 Mt       12.829 Mt   100.000 % 	 ...../BuildShaderTables

		      63.474 Mt     0.626 %         1 x       63.474 Mt        0.000 Mt       63.474 Mt   100.000 % 	 ..../GpuSidePrep

		       4.684 Mt     0.014 %         1 x        4.684 Mt        0.000 Mt        4.684 Mt   100.000 % 	 ../DeferredRLInitialization

		      64.916 Mt     0.192 %         1 x       64.916 Mt        0.000 Mt        2.256 Mt     3.476 % 	 ../RayTracingRLInitialization

		      62.660 Mt    96.524 %         1 x       62.660 Mt        0.000 Mt       62.660 Mt   100.000 % 	 .../PipelineBuild

		       4.904 Mt     0.015 %         1 x        4.904 Mt        0.000 Mt        4.904 Mt   100.000 % 	 ../ResolveRLInitialization

		   23136.243 Mt    68.562 %      6862 x        3.372 Mt       31.996 Mt      387.958 Mt     1.677 % 	 ../MainLoop

		    1117.014 Mt     4.828 %      1387 x        0.805 Mt        0.661 Mt     1115.605 Mt    99.874 % 	 .../Update

		       1.410 Mt     0.126 %         1 x        1.410 Mt        0.000 Mt        1.410 Mt   100.000 % 	 ..../GuiModelApply

		   21631.270 Mt    93.495 %      2222 x        9.735 Mt       11.935 Mt    15303.462 Mt    70.747 % 	 .../Render

		    6327.808 Mt    29.253 %      2222 x        2.848 Mt        3.735 Mt     6311.569 Mt    99.743 % 	 ..../RayTracing

		       8.403 Mt     0.133 %      2222 x        0.004 Mt        0.004 Mt        8.403 Mt   100.000 % 	 ...../DeferredRLPrep

		       7.836 Mt     0.124 %      2222 x        0.004 Mt        0.004 Mt        7.836 Mt   100.000 % 	 ...../RayTracingRLPrep

	WindowThread:

		   33733.907 Mt   100.000 %         1 x    33733.908 Mt    33733.907 Mt    33733.907 Mt   100.000 % 	 /

	GpuThread:

		   12417.773 Mt   100.000 %      2222 x        5.589 Mt        6.270 Mt      256.825 Mt     2.068 % 	 /

		      13.108 Mt     0.106 %         1 x       13.108 Mt        0.000 Mt        0.047 Mt     0.357 % 	 ./ScenePrep

		      13.061 Mt    99.643 %         1 x       13.061 Mt        0.000 Mt        0.002 Mt     0.016 % 	 ../AccelerationStructureBuild

		      12.627 Mt    96.675 %         1 x       12.627 Mt        0.000 Mt       12.627 Mt   100.000 % 	 .../BuildBottomLevelASGpu

		       0.432 Mt     3.309 %         1 x        0.432 Mt        0.000 Mt        0.432 Mt   100.000 % 	 .../BuildTopLevelASGpu

		   12081.407 Mt    97.291 %      2222 x        5.437 Mt        6.150 Mt        2.610 Mt     0.022 % 	 ./RayTracingGpu

		    2459.751 Mt    20.360 %      2222 x        1.107 Mt        1.382 Mt     2459.751 Mt   100.000 % 	 ../DeferredRLGpu

		    5705.537 Mt    47.226 %      2222 x        2.568 Mt        2.749 Mt     5705.537 Mt   100.000 % 	 ../RayTracingRLGpu

		    3913.509 Mt    32.393 %      2222 x        1.761 Mt        2.017 Mt     3913.509 Mt   100.000 % 	 ../ResolveRLGpu

		      66.433 Mt     0.535 %      2222 x        0.030 Mt        0.120 Mt       66.433 Mt   100.000 % 	 ./Present


	============================


