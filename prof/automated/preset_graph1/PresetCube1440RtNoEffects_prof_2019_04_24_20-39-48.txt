Configuration: 
Command line parameters  = E:\programming\FIT-DIP\Masters-DXR-Main-git\build\Measurement\x64\Debug\Measurement.exe --scene Cube/Cube.gltf --profile-automation --profile-camera-track Cube/Cube.trk --rt-mode --width 2560 --height 1440 --profile-output PresetCube1440RtNoEffects --quality-preset NoEffects 
Window width             = 2560
Window height            = 1421
Window title             = Direct3D App
Back-buffer count        = 3
Fill Triangles           = enabled
Triangles Font Clock-Wise= enabled
Fast Build AS            = disabled
Target FPS               = 0
Target UPS               = 60
Tearing                  = enabled
VSync                    = disabled
GUI rendering            = disabled
Debug layer              = enabled
GPU validation           = disabled
GPU profiling            = enabled
Render mode              = Ray Tracing
Loaded scene file        = Cube/Cube.gltf
Using testing scene      = disabled
Duplication              = 1
Duplication in cube      = disabled
Duplication offset       = 10
BLAS duplication         = disabled
Rays per pixel           = 1
Quality preset           = NoEffects
Rasterization Quality    : 
  AO                     = disabled
  AO kernel size         = 1
  Shadows                = disabled
  Shadow map resolution  = 1
  Shadow PCF kernel size = 0
  Reflections            = disabled
Ray Tracing Quality      : 
  AO                     = disabled
  AO rays                = 0
  AO kernel size         = 4
  Shadows                = disabled
  Shadow rays            = 0
  Shadow kernel size     = 4
  Reflections            = disabled

ProfilingAggregator: 
Render	,	3.1418	,	4.6131	,	3.5419	,	3.9847	,	3.7022	,	6.2821	,	5.9852	,	5.5425	,	7.2387	,	4.8047	,	3.9493	,	4.136	,	5.8728	,	4.1235	,	3.8208	,	4.9124	,	4.8522	,	4.4346	,	4.3209	,	4.5244	,	4.8138	,	3.5072	,	4.0122	,	3.4064	,	3.4478	,	3.3818	,	3.3932	,	3.1819	,	3.421
Update	,	0.7801	,	0.8128	,	0.9752	,	0.7074	,	0.7124	,	0.6581	,	0.6503	,	0.9207	,	1.0927	,	0.7256	,	0.4765	,	0.4943	,	0.5868	,	0.6244	,	0.6161	,	1.0197	,	0.4757	,	1.0669	,	1.0711	,	0.6599	,	1.0165	,	0.6041	,	0.7259	,	0.5594	,	0.5328	,	0.5799	,	1.0924	,	0.472	,	1.216
RayTracing	,	1.218	,	2.1985	,	1.3697	,	1.5875	,	1.2409	,	2.7727	,	2.6299	,	2.4715	,	2.6529	,	1.6672	,	1.2499	,	1.2235	,	2.6226	,	1.4966	,	1.2294	,	1.8371	,	1.6521	,	1.2548	,	1.2477	,	1.5279	,	1.5416	,	1.2198	,	1.328	,	1.2234	,	1.2386	,	1.2272	,	1.2303	,	1.2254	,	1.2197
RayTracingGpu	,	0.651424	,	0.789152	,	1.13098	,	1.21114	,	1.29568	,	1.55418	,	1.51968	,	1.71805	,	2.35197	,	1.69075	,	1.52474	,	1.67933	,	1.42883	,	1.43494	,	1.62704	,	1.85008	,	1.90397	,	2.03814	,	1.90621	,	1.84781	,	1.99789	,	1.1519	,	1.15811	,	1.05565	,	1.04189	,	0.996544	,	0.967584	,	0.971808	,	1.0033
DeferredRLGpu	,	0.259968	,	0.342784	,	0.512064	,	0.581408	,	0.658336	,	0.855456	,	0.823968	,	0.978496	,	1.34918	,	0.9528	,	0.83296	,	0.946176	,	0.756448	,	0.74544	,	0.890912	,	1.06928	,	1.11898	,	1.2136	,	1.09107	,	1.03798	,	0.807456	,	0.561088	,	0.562304	,	0.479232	,	0.44416	,	0.380256	,	0.319712	,	0.286816	,	0.271328
RayTracingRLGpu	,	0.203968	,	0.215648	,	0.298144	,	0.314624	,	0.33088	,	0.352672	,	0.359584	,	0.376128	,	0.596576	,	0.361632	,	0.355328	,	0.371712	,	0.361632	,	0.383392	,	0.402816	,	0.415264	,	0.387296	,	0.38112	,	0.358656	,	0.350016	,	0.789632	,	0.290496	,	0.321472	,	0.336768	,	0.364192	,	0.387968	,	0.42032	,	0.451616	,	0.49168
ResolveRLGpu	,	0.185984	,	0.228992	,	0.318976	,	0.312576	,	0.303936	,	0.344256	,	0.33408	,	0.36112	,	0.40416	,	0.374784	,	0.334208	,	0.359648	,	0.308736	,	0.304096	,	0.331584	,	0.364	,	0.39568	,	0.441376	,	0.454464	,	0.458016	,	0.398752	,	0.297792	,	0.272288	,	0.23792	,	0.231264	,	0.226528	,	0.225344	,	0.231616	,	0.237728
Index	,	0	,	1	,	2	,	3	,	4	,	5	,	6	,	7	,	8	,	9	,	10	,	11	,	12	,	13	,	14	,	15	,	16	,	17	,	18	,	19	,	20	,	21	,	22	,	23	,	24	,	25	,	26	,	27	,	28

ProfilingBuildAggregator: 
BuildBottomLevelAS	,	0.9411
BuildBottomLevelASGpu	,	0.119744
BuildTopLevelAS	,	2.5909
BuildTopLevelASGpu	,	0.08528
Duplication	,	1
Triangles	,	12
Meshes	,	1
BottomLevels	,	1
TopLevelSize	,	64
BottomLevelsSize	,	5168
Index	,	0

FpsAggregator: 
FPS	,	262	,	264	,	242	,	223	,	213	,	194	,	196	,	198	,	174	,	175	,	198	,	190	,	198	,	201	,	194	,	177	,	175	,	168	,	176	,	177	,	187	,	213	,	218	,	226	,	220	,	225	,	236	,	240	,	237
UPS	,	59	,	61	,	60	,	60	,	60	,	60	,	60	,	60	,	61	,	60	,	60	,	60	,	60	,	61	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	61	,	60	,	60	,	60	,	60	,	60
FrameTime	,	3.82547	,	3.80478	,	4.14333	,	4.49071	,	4.70895	,	5.17146	,	5.11668	,	5.05889	,	5.77245	,	5.73	,	5.05289	,	5.28258	,	5.059	,	4.9964	,	5.16501	,	5.65138	,	5.73258	,	5.95387	,	5.68941	,	5.65144	,	5.36354	,	4.70041	,	4.58841	,	4.44047	,	4.54888	,	4.44758	,	4.24045	,	4.17602	,	4.22175
GigaRays/s	,	0.950932	,	0.956103	,	0.877979	,	0.810063	,	0.77252	,	0.703429	,	0.71096	,	0.719083	,	0.630193	,	0.634862	,	0.719937	,	0.688634	,	0.719067	,	0.728076	,	0.704309	,	0.643694	,	0.634576	,	0.610991	,	0.639392	,	0.643687	,	0.678239	,	0.773924	,	0.792815	,	0.819229	,	0.799705	,	0.81792	,	0.857872	,	0.871107	,	0.861671
Index	,	0	,	1	,	2	,	3	,	4	,	5	,	6	,	7	,	8	,	9	,	10	,	11	,	12	,	13	,	14	,	15	,	16	,	17	,	18	,	19	,	20	,	21	,	22	,	23	,	24	,	25	,	26	,	27	,	28


AccelerationStructure: 
Ray tracing acceleration structure building statistics: 
	Bottom Level Acceleration Structure: 
		Maximum [B]: 5168
		Minimum [B]: 5168
		Currently Allocated [B]: 5168
		Currently Created [num]: 1
		Current Average [B]: 5168
		Maximum Triangles [num]: 12
		Minimum Triangles [num]: 12
		Total Triangles [num]: 12
		Maximum Meshes [num]: 1
		Minimum Meshes [num]: 1
		Total Meshes [num]: 1
	Top Level Acceleration Structure: 
		Maximum [B]: 64
		Minimum [B]: 64
		Currently Allocated [B]: 64
		Total Allocated [B]: 64
		Total Created [num]: 1
		Average Allocated [B]: 64
		Maximum Instances [num]: 1
		Minimum Instances [num]: 1
		Total Instances [num]: 1
	Scratch Buffer: 
		Maximum [B]: 3584
		Minimum [B]: 3584
		Currently Allocated [B]: 3584
		Total Allocated [B]: 3584
		Total Created [num]: 1
		Average Allocated [B]: 3584
ProfilingStack: 
[PROF] Profiling statistics: 
	Scopes entered : 66461
	Scopes exited : 66460
	Overhead per scope [ticks] : 1007.5
	Threads entered : 3
	Threads exited : 0
	============================
		         Sum[Mt]   Parent[%]  Samples[num]       Avg[Mt]   Last/Curr[Mt]        Self[Mt]     Self[%] 	 Name

	main:

		   29692.934 Mt   100.000 %         1 x    29692.936 Mt    29692.934 Mt        0.737 Mt     0.002 % 	 /

		   29692.251 Mt    99.998 %         1 x    29692.254 Mt    29692.251 Mt      368.264 Mt     1.240 % 	 ./Main application loop

		       2.921 Mt     0.010 %         1 x        2.921 Mt        0.000 Mt        2.921 Mt   100.000 % 	 ../TexturedRLInitialization

		       2.284 Mt     0.008 %         1 x        2.284 Mt        0.000 Mt        2.284 Mt   100.000 % 	 ../ShadowMapRLInitialization

		       4.831 Mt     0.016 %         1 x        4.831 Mt        0.000 Mt        4.831 Mt   100.000 % 	 ../AmbientOcclusionRLInitialization

		       5.033 Mt     0.017 %         1 x        5.033 Mt        0.000 Mt        5.033 Mt   100.000 % 	 ../RasterResolveRLInitialization

		     160.732 Mt     0.541 %         1 x      160.732 Mt        0.000 Mt       11.895 Mt     7.401 % 	 ../Initialization

		     148.837 Mt    92.599 %         1 x      148.837 Mt        0.000 Mt        1.844 Mt     1.239 % 	 .../ScenePrep

		     109.586 Mt    73.628 %         1 x      109.586 Mt        0.000 Mt       15.708 Mt    14.334 % 	 ..../SceneLoad

		      64.633 Mt    58.979 %         1 x       64.633 Mt        0.000 Mt       13.551 Mt    20.965 % 	 ...../glTF-LoadScene

		      51.082 Mt    79.035 %         2 x       25.541 Mt        0.000 Mt       51.082 Mt   100.000 % 	 ....../glTF-LoadImageData

		      13.803 Mt    12.596 %         1 x       13.803 Mt        0.000 Mt       13.803 Mt   100.000 % 	 ...../glTF-CreateScene

		      15.442 Mt    14.091 %         1 x       15.442 Mt        0.000 Mt       15.442 Mt   100.000 % 	 ...../SceneDesc-CreateScene

		       1.069 Mt     0.718 %         1 x        1.069 Mt        0.000 Mt        0.005 Mt     0.477 % 	 ..../ShadowMapRLPrep

		       1.064 Mt    99.523 %         1 x        1.064 Mt        0.000 Mt        1.002 Mt    94.183 % 	 ...../PrepareForRendering

		       0.062 Mt     5.817 %         1 x        0.062 Mt        0.000 Mt        0.062 Mt   100.000 % 	 ....../PrepareDrawBundle

		       2.550 Mt     1.713 %         1 x        2.550 Mt        0.000 Mt        0.004 Mt     0.173 % 	 ..../TexturedRLPrep

		       2.545 Mt    99.827 %         1 x        2.545 Mt        0.000 Mt        1.125 Mt    44.220 % 	 ...../PrepareForRendering

		       1.161 Mt    45.607 %         1 x        1.161 Mt        0.000 Mt        1.161 Mt   100.000 % 	 ....../PrepareMaterials

		       0.259 Mt    10.172 %         1 x        0.259 Mt        0.000 Mt        0.259 Mt   100.000 % 	 ....../PrepareDrawBundle

		       4.387 Mt     2.948 %         1 x        4.387 Mt        0.000 Mt        0.004 Mt     0.103 % 	 ..../DeferredRLPrep

		       4.383 Mt    99.897 %         1 x        4.383 Mt        0.000 Mt        2.082 Mt    47.516 % 	 ...../PrepareForRendering

		       0.017 Mt     0.383 %         1 x        0.017 Mt        0.000 Mt        0.017 Mt   100.000 % 	 ....../PrepareMaterials

		       2.011 Mt    45.880 %         1 x        2.011 Mt        0.000 Mt        2.011 Mt   100.000 % 	 ....../BuildMaterialCache

		       0.273 Mt     6.220 %         1 x        0.273 Mt        0.000 Mt        0.273 Mt   100.000 % 	 ....../PrepareDrawBundle

		      15.542 Mt    10.442 %         1 x       15.542 Mt        0.000 Mt        4.906 Mt    31.567 % 	 ..../RayTracingRLPrep

		       0.235 Mt     1.513 %         1 x        0.235 Mt        0.000 Mt        0.235 Mt   100.000 % 	 ...../PrepareAccelerationStructures

		       0.026 Mt     0.165 %         1 x        0.026 Mt        0.000 Mt        0.026 Mt   100.000 % 	 ...../PrepareCache

		       0.875 Mt     5.628 %         1 x        0.875 Mt        0.000 Mt        0.875 Mt   100.000 % 	 ...../BuildCache

		       7.312 Mt    47.045 %         1 x        7.312 Mt        0.000 Mt        0.004 Mt     0.057 % 	 ...../BuildAccelerationStructures

		       7.308 Mt    99.943 %         1 x        7.308 Mt        0.000 Mt        3.776 Mt    51.667 % 	 ....../AccelerationStructureBuild

		       0.941 Mt    12.878 %         1 x        0.941 Mt        0.000 Mt        0.941 Mt   100.000 % 	 ......./BuildBottomLevelAS

		       2.591 Mt    35.455 %         1 x        2.591 Mt        0.000 Mt        2.591 Mt   100.000 % 	 ......./BuildTopLevelAS

		       2.189 Mt    14.082 %         1 x        2.189 Mt        0.000 Mt        2.189 Mt   100.000 % 	 ...../BuildShaderTables

		      13.859 Mt     9.311 %         1 x       13.859 Mt        0.000 Mt       13.859 Mt   100.000 % 	 ..../GpuSidePrep

		       4.674 Mt     0.016 %         1 x        4.674 Mt        0.000 Mt        4.674 Mt   100.000 % 	 ../DeferredRLInitialization

		      63.686 Mt     0.214 %         1 x       63.686 Mt        0.000 Mt        2.552 Mt     4.008 % 	 ../RayTracingRLInitialization

		      61.133 Mt    95.992 %         1 x       61.133 Mt        0.000 Mt       61.133 Mt   100.000 % 	 .../PipelineBuild

		       5.007 Mt     0.017 %         1 x        5.007 Mt        0.000 Mt        5.007 Mt   100.000 % 	 ../ResolveRLInitialization

		   29074.819 Mt    97.921 %     10680 x        2.722 Mt        7.240 Mt     1099.610 Mt     3.782 % 	 ../MainLoop

		    1528.553 Mt     5.257 %      1746 x        0.875 Mt        0.662 Mt     1526.167 Mt    99.844 % 	 .../Update

		       2.385 Mt     0.156 %         1 x        2.385 Mt        0.000 Mt        2.385 Mt   100.000 % 	 ..../GuiModelApply

		   26446.656 Mt    90.961 %      5999 x        4.409 Mt        5.115 Mt    16313.955 Mt    61.686 % 	 .../Render

		   10132.701 Mt    38.314 %      5999 x        1.689 Mt        2.184 Mt    10080.234 Mt    99.482 % 	 ..../RayTracing

		      31.697 Mt     0.313 %      5999 x        0.005 Mt        0.004 Mt       31.697 Mt   100.000 % 	 ...../DeferredRLPrep

		      20.771 Mt     0.205 %      5999 x        0.003 Mt        0.004 Mt       20.771 Mt   100.000 % 	 ...../RayTracingRLPrep

	WindowThread:

		   29688.494 Mt   100.000 %         1 x    29688.494 Mt    29688.494 Mt    29688.494 Mt   100.000 % 	 /

	GpuThread:

		    8479.630 Mt   100.000 %      5999 x        1.414 Mt        1.012 Mt      177.377 Mt     2.092 % 	 /

		       0.212 Mt     0.003 %         1 x        0.212 Mt        0.000 Mt        0.006 Mt     2.788 % 	 ./ScenePrep

		       0.206 Mt    97.212 %         1 x        0.206 Mt        0.000 Mt        0.001 Mt     0.667 % 	 ../AccelerationStructureBuild

		       0.120 Mt    58.016 %         1 x        0.120 Mt        0.000 Mt        0.120 Mt   100.000 % 	 .../BuildBottomLevelASGpu

		       0.085 Mt    41.318 %         1 x        0.085 Mt        0.000 Mt        0.085 Mt   100.000 % 	 .../BuildTopLevelASGpu

		    8249.830 Mt    97.290 %      5999 x        1.375 Mt        1.008 Mt       11.792 Mt     0.143 % 	 ./RayTracingGpu

		    4140.078 Mt    50.184 %      5999 x        0.690 Mt        0.273 Mt     4140.078 Mt   100.000 % 	 ../DeferredRLGpu

		    2193.677 Mt    26.591 %      5999 x        0.366 Mt        0.493 Mt     2193.677 Mt   100.000 % 	 ../RayTracingRLGpu

		    1904.283 Mt    23.083 %      5999 x        0.317 Mt        0.239 Mt     1904.283 Mt   100.000 % 	 ../ResolveRLGpu

		      52.211 Mt     0.616 %      5999 x        0.009 Mt        0.003 Mt       52.211 Mt   100.000 % 	 ./Present


	============================


