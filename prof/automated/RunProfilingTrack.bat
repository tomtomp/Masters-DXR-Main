@echo off

cd "..\..\build\Measurement\x64\Release\"



start /wait Measurement.exe --scene Cube/Cube.gltf --quality-preset Low --profile-automation --profile-camera-track Cube/Cube.trk --rt-mode --width 1024 --height 768 --profile-output TrackCube768Rt
copy TrackCube768Rt*.txt ..\..\..\..\prof\automated\
start /wait Measurement.exe --scene Cube/Cube.gltf --quality-preset Low --profile-automation --profile-camera-track Cube/Cube.trk --rt-mode --width 2560 --height 1440 --profile-output TrackCube1440Rt
copy TrackCube1440Rt*.txt ..\..\..\..\prof\automated\

start /wait Measurement.exe --scene Suzanne/Suzanne.gltf --quality-preset Low --profile-automation --profile-camera-track Suzanne/Suzanne.trk --rt-mode --width 1024 --height 768 --profile-output TrackSuzanne768Rt
copy TrackSuzanne768Rt*.txt ..\..\..\..\prof\automated\
start /wait Measurement.exe --scene Suzanne/Suzanne.gltf --quality-preset Low --profile-automation --profile-camera-track Suzanne/Suzanne.trk --rt-mode --width 2560 --height 1440 --profile-output TrackSuzanne1440Rt
copy TrackSuzanne1440Rt*.txt ..\..\..\..\prof\automated\

start /wait Measurement.exe --scene Sponza/Sponza.gltf --quality-preset Low --profile-automation --profile-camera-track Sponza/Sponza.trk --rt-mode --width 1024 --height 768 --profile-output TrackSponza768Rt
copy TrackSponza768Rt*.txt ..\..\..\..\prof\automated\
start /wait Measurement.exe --scene Sponza/Sponza.gltf --quality-preset Low --profile-automation --profile-camera-track Sponza/Sponza.trk --rt-mode --width 2560 --height 1440 --profile-output TrackSponza1440Rt
copy TrackSponza1440Rt*.txt ..\..\..\..\prof\automated\

start /wait Measurement.exe --scene Sponza/Sponza.gltf --quality-preset Low --profile-automation --profile-camera-track Sponza/Sponza.trk --rt-mode --width 2560 --height 1440 --profile-output TrackSponza1440Rft --fast-build-as
copy TrackSponza1440Rft*.txt ..\..\..\..\prof\automated\

start /wait Measurement.exe --scene Cube/Cube.gltf --quality-preset Low --profile-automation --profile-camera-track Cube/Cube.trk --rt-mode-pure --width 1024 --height 768 --profile-output TrackCube768Rp
copy TrackCube768Rp*.txt ..\..\..\..\prof\automated\
start /wait Measurement.exe --scene Cube/Cube.gltf --quality-preset Low --profile-automation --profile-camera-track Cube/Cube.trk --rt-mode-pure --width 2560 --height 1440 --profile-output TrackCube1440Rp
copy TrackCube1440Rp*.txt ..\..\..\..\prof\automated\

start /wait Measurement.exe --scene Suzanne/Suzanne.gltf --quality-preset Low --profile-automation --profile-camera-track Suzanne/Suzanne.trk --rt-mode-pure --width 1024 --height 768 --profile-output TrackSuzanne768Rp
copy TrackSuzanne768Rp*.txt ..\..\..\..\prof\automated\
start /wait Measurement.exe --scene Suzanne/Suzanne.gltf --quality-preset Low --profile-automation --profile-camera-track Suzanne/Suzanne.trk --rt-mode-pure --width 2560 --height 1440 --profile-output TrackSuzanne1440Rp
copy TrackSuzanne1440Rp*.txt ..\..\..\..\prof\automated\

start /wait Measurement.exe --scene Sponza/Sponza.gltf --quality-preset Low --profile-automation --profile-camera-track Sponza/Sponza.trk --rt-mode-pure --width 1024 --height 768 --profile-output TrackSponza768Rp
copy TrackSponza768Rp*.txt ..\..\..\..\prof\automated\
start /wait Measurement.exe --scene Sponza/Sponza.gltf --quality-preset Low --profile-automation --profile-camera-track Sponza/Sponza.trk --rt-mode-pure --width 2560 --height 1440 --profile-output TrackSponza1440Rp
copy TrackSponza1440Rp*.txt ..\..\..\..\prof\automated\

start /wait Measurement.exe --scene Sponza/Sponza.gltf --quality-preset Low --profile-automation --profile-camera-track Sponza/Sponza.trk --rt-mode-pure --width 2560 --height 1440 --profile-output TrackSponza1440Rfp --fast-build-as
copy TrackSponza1440Rfp*.txt ..\..\..\..\prof\automated\


start /wait Measurement.exe --scene Cube/Cube.gltf --quality-preset Low --profile-automation --profile-camera-track Cube/Cube.trk --width 1024 --height 768 --profile-output TrackCube768Ra
copy TrackCube768Ra*.txt ..\..\..\..\prof\automated\
start /wait Measurement.exe --scene Cube/Cube.gltf --quality-preset Low --profile-automation --profile-camera-track Cube/Cube.trk --width 2560 --height 1440 --profile-output TrackCube1440Ra
copy TrackCube1440Ra*.txt ..\..\..\..\prof\automated\

start /wait Measurement.exe --scene Suzanne/Suzanne.gltf --quality-preset Low --profile-automation --profile-camera-track Suzanne/Suzanne.trk --width 1024 --height 768 --profile-output TrackSuzanne768Ra
copy TrackSuzanne768Ra*.txt ..\..\..\..\prof\automated\
start /wait Measurement.exe --scene Suzanne/Suzanne.gltf --quality-preset Low --profile-automation --profile-camera-track Suzanne/Suzanne.trk --width 2560 --height 1440 --profile-output TrackSuzanne1440Ra
copy TrackSuzanne1440Ra*.txt ..\..\..\..\prof\automated\

start /wait Measurement.exe --scene Sponza/Sponza.gltf --quality-preset Low --profile-automation --profile-camera-track Sponza/Sponza.trk --width 1024 --height 768 --profile-output TrackSponza768Ra
copy TrackSponza768Ra*.txt ..\..\..\..\prof\automated\
start /wait Measurement.exe --scene Sponza/Sponza.gltf --quality-preset Low --profile-automation --profile-camera-track Sponza/Sponza.trk --width 2560 --height 1440 --profile-output TrackSponza1440Ra
copy TrackSponza1440Ra*.txt ..\..\..\..\prof\automated\



exit