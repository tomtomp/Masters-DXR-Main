Configuration: 
Command line parameters  = Measurement.exe --scene Suzanne/Suzanne.gltf --profile-track Suzanne/Suzanne.trk --width 1024 --height 768 --profile-output TrackSuzanne768Ra 
Window width             = 1024
Window height            = 768
Window title             = Direct3D App
Back-buffer count        = 3
Target FPS               = 0
Target UPS               = 60
Tearing                  = enabled
VSync                    = disabled
GUI rendering            = disabled
Debug layer              = enabled
GPU validation           = disabled
GPU profiling            = enabled
Render mode              = Rasterization
Loaded scene file        = Suzanne/Suzanne.gltf
Using testing scene      = disabled
Duplication              = 1
Duplication in cube      = disabled
Duplication offset       = 10
BLAS duplication         = disabled
Rays per pixel           = 9

ProfilingBuildAggregator: 
BuildBottomLevelAS	,	2.96198
BuildBottomLevelASGpu	,	1.46483
BuildTopLevelAS	,	1.10456
BuildTopLevelASGpu	,	0.069376

ProfilingAggregator: 
Render	,	0.577183	,	0.569334	,	0.571748	,	0.5962	,	0.608276	,	0.577484	,	0.568125	,	0.566617	,	0.576579	,	0.567824	,	0.565409	,	0.587144	,	0.617935	,	0.565711	,	1.35994	,	0.566315	,	0.570843	,	1.31255	,	0.619445	,	0.696121	,	0.60586	,	0.61552	,	0.567825	,	0.566919
Update	,	0.424736	,	0.390322	,	0.390926	,	0.393341	,	0.401492	,	0.401492	,	0.409944	,	0.397265	,	0.407228	,	0.408133	,	0.396058	,	0.398473	,	0.424132	,	0.396058	,	0.656273	,	0.395756	,	0.395152	,	0.403001	,	0.414774	,	0.438321	,	0.415679	,	0.408435	,	0.424434	,	0.390322

FpsAggregator: 
FPS	,	1462	,	1632	,	1632	,	1637	,	1653	,	1639	,	1647	,	1659	,	1640	,	1640	,	1641	,	1647	,	1628	,	1636	,	1293	,	1547	,	1601	,	1525	,	1275	,	1482	,	1466	,	1463	,	1546	,	1592
UPS	,	59	,	60	,	60	,	60	,	61	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60
FrameTime	,	0.684134	,	0.61276	,	0.612826	,	0.610896	,	0.605551	,	0.610368	,	0.607274	,	0.603047	,	0.610065	,	0.609791	,	0.609597	,	0.607273	,	0.614591	,	0.611557	,	0.774274	,	0.646614	,	0.62477	,	0.656069	,	0.784372	,	0.67514	,	0.682233	,	0.683749	,	0.64718	,	0.6284
GigaRays/s	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0

AccelerationStructure: 
Ray tracing acceleration structure building statistics: 
	Bottom Level Acceleration Structure: 
		Maximum [B]: 456560
		Minimum [B]: 456560
		Currently Allocated [B]: 456560
		Currently Created [num]: 1
		Current Average [B]: 456560
		Maximum Triangles [num]: 3936
		Minimum Triangles [num]: 3936
		Total Triangles [num]: 3936
		Maximum Meshes [num]: 1
		Minimum Meshes [num]: 1
		Total Meshes [num]: 1
	Top Level Acceleration Structure: 
		Maximum [B]: 64
		Minimum [B]: 64
		Currently Allocated [B]: 64
		Total Allocated [B]: 64
		Total Created [num]: 1
		Average Allocated [B]: 64
		Maximum Instances [num]: 1
		Minimum Instances [num]: 1
		Total Instances [num]: 1
	Scratch Buffer: 
		Maximum [B]: 490304
		Minimum [B]: 490304
		Currently Allocated [B]: 490304
		Total Allocated [B]: 490304
		Total Created [num]: 1
		Average Allocated [B]: 490304
ProfilingStack: 
[PROF] Profiling statistics: 
	Scopes entered : 192006
	Scopes exited : 192005
	Overhead per scope [ticks] : 1604.15
	Threads entered : 3
	Threads exited : 0
	============================
		         Sum[Mt]   Parent[%]  Samples[num]       Avg[Mt]   Last/Curr[Mt]        Self[Mt]     Self[%] 	 Name

	main:

		   25570.550 Mt   100.000 %         1 x    25570.553 Mt    25570.549 Mt        0.556 Mt     0.002 % 	 /

		   25570.060 Mt    99.998 %         1 x    25570.063 Mt    25570.060 Mt      394.173 Mt     1.542 % 	 ./Main application loop

		       6.140 Mt     0.024 %         1 x        6.140 Mt        0.000 Mt        6.140 Mt   100.000 % 	 ../TexturedRLInitialization

		       7.022 Mt     0.027 %         1 x        7.022 Mt        0.000 Mt        7.022 Mt   100.000 % 	 ../DeferredRLInitialization

		     410.358 Mt     1.605 %         1 x      410.358 Mt        0.000 Mt        2.122 Mt     0.517 % 	 ../RayTracingRLInitialization

		     408.236 Mt    99.483 %         1 x      408.236 Mt        0.000 Mt      408.236 Mt   100.000 % 	 .../PipelineBuild

		      18.589 Mt     0.073 %         1 x       18.589 Mt        0.000 Mt       18.589 Mt   100.000 % 	 ../ResolveRLInitialization

		     882.278 Mt     3.450 %         1 x      882.278 Mt        0.000 Mt        4.007 Mt     0.454 % 	 ../Initialization

		     878.271 Mt    99.546 %         1 x      878.271 Mt        0.000 Mt        2.952 Mt     0.336 % 	 .../ScenePrep

		     256.106 Mt    29.160 %         1 x      256.106 Mt        0.000 Mt       16.020 Mt     6.255 % 	 ..../SceneLoad

		     231.216 Mt    90.281 %         1 x      231.216 Mt        0.000 Mt       25.987 Mt    11.239 % 	 ...../glTF-LoadScene

		     205.230 Mt    88.761 %         2 x      102.615 Mt        0.000 Mt      205.230 Mt   100.000 % 	 ....../glTF-LoadImageData

		       6.169 Mt     2.409 %         1 x        6.169 Mt        0.000 Mt        6.169 Mt   100.000 % 	 ...../glTF-CreateScene

		       2.701 Mt     1.055 %         1 x        2.701 Mt        0.000 Mt        2.701 Mt   100.000 % 	 ...../SceneDesc-CreateScene

		       0.799 Mt     0.091 %         1 x        0.799 Mt        0.000 Mt        0.007 Mt     0.907 % 	 ..../TexturedRLPrep

		       0.792 Mt    99.093 %         1 x        0.792 Mt        0.000 Mt        0.226 Mt    28.555 % 	 ...../PrepareForRendering

		       0.266 Mt    33.549 %         1 x        0.266 Mt        0.000 Mt        0.266 Mt   100.000 % 	 ....../PrepareMaterials

		       0.300 Mt    37.896 %         1 x        0.300 Mt        0.000 Mt        0.300 Mt   100.000 % 	 ....../PrepareDrawBundle

		       1.676 Mt     0.191 %         1 x        1.676 Mt        0.000 Mt        0.007 Mt     0.432 % 	 ..../DeferredRLPrep

		       1.669 Mt    99.568 %         1 x        1.669 Mt        0.000 Mt        1.183 Mt    70.894 % 	 ...../PrepareForRendering

		       0.025 Mt     1.483 %         1 x        0.025 Mt        0.000 Mt        0.025 Mt   100.000 % 	 ....../PrepareMaterials

		       0.187 Mt    11.198 %         1 x        0.187 Mt        0.000 Mt        0.187 Mt   100.000 % 	 ....../BuildMaterialCache

		       0.274 Mt    16.425 %         1 x        0.274 Mt        0.000 Mt        0.274 Mt   100.000 % 	 ....../PrepareDrawBundle

		     611.662 Mt    69.644 %         1 x      611.662 Mt        0.000 Mt        2.682 Mt     0.438 % 	 ..../RayTracingRLPrep

		       0.289 Mt     0.047 %         1 x        0.289 Mt        0.000 Mt        0.289 Mt   100.000 % 	 ...../PrepareAccelerationStructures

		       0.038 Mt     0.006 %         1 x        0.038 Mt        0.000 Mt        0.038 Mt   100.000 % 	 ...../PrepareCache

		       0.243 Mt     0.040 %         1 x        0.243 Mt        0.000 Mt        0.243 Mt   100.000 % 	 ...../BuildCache

		     607.707 Mt    99.353 %         1 x      607.707 Mt        0.000 Mt        0.006 Mt     0.001 % 	 ...../BuildAccelerationStructures

		     607.701 Mt    99.999 %         1 x      607.701 Mt        0.000 Mt      603.635 Mt    99.331 % 	 ....../AccelerationStructureBuild

		       2.962 Mt     0.487 %         1 x        2.962 Mt        0.000 Mt        2.962 Mt   100.000 % 	 ......./BuildBottomLevelAS

		       1.105 Mt     0.182 %         1 x        1.105 Mt        0.000 Mt        1.105 Mt   100.000 % 	 ......./BuildTopLevelAS

		       0.703 Mt     0.115 %         1 x        0.703 Mt        0.000 Mt        0.703 Mt   100.000 % 	 ...../BuildShaderTables

		       5.075 Mt     0.578 %         1 x        5.075 Mt        0.000 Mt        5.075 Mt   100.000 % 	 ..../GpuSidePrep

		   23851.500 Mt    93.279 %     40184 x        0.594 Mt        1.148 Mt      325.508 Mt     1.365 % 	 ../MainLoop

		     647.118 Mt     2.713 %      1441 x        0.449 Mt        0.428 Mt      647.118 Mt   100.000 % 	 .../Update

		   22878.874 Mt    95.922 %     37586 x        0.609 Mt        0.710 Mt    22716.547 Mt    99.290 % 	 .../Render

		     162.328 Mt     0.710 %     37586 x        0.004 Mt        0.005 Mt      162.328 Mt   100.000 % 	 ..../TexturedRLPrep

	WindowThread:

		   25566.894 Mt   100.000 %         1 x    25566.895 Mt    25566.894 Mt    25566.894 Mt   100.000 % 	 /

	GpuThread:

		    1091.380 Mt   100.000 %     37586 x        0.029 Mt        0.026 Mt       85.996 Mt     7.880 % 	 /

		       1.539 Mt     0.141 %         1 x        1.539 Mt        0.000 Mt        0.005 Mt     0.320 % 	 ./ScenePrep

		       1.534 Mt    99.680 %         1 x        1.534 Mt        0.000 Mt        0.000 Mt     0.004 % 	 ../AccelerationStructureBuild

		       1.465 Mt    95.474 %         1 x        1.465 Mt        0.000 Mt        1.465 Mt   100.000 % 	 .../BuildBottomLevelASGpu

		       0.069 Mt     4.522 %         1 x        0.069 Mt        0.000 Mt        0.069 Mt   100.000 % 	 .../BuildTopLevelASGpu

		     920.199 Mt    84.315 %     37586 x        0.024 Mt        0.024 Mt      920.199 Mt   100.000 % 	 ./Textured

		      83.646 Mt     7.664 %     37586 x        0.002 Mt        0.002 Mt       83.646 Mt   100.000 % 	 ./Present


	============================


