Configuration: 
Command line parameters  = Measurement.exe --scene Cube/Cube.gltf --duplication-cube --profile-duplication 10 --width 1024 --height 768 --profile-output DuplicationCube768Ra 
Window width             = 1024
Window height            = 768
Window title             = Direct3D App
Back-buffer count        = 3
Target FPS               = 60
Target UPS               = 60
Tearing                  = disabled
VSync                    = disabled
GUI rendering            = disabled
Debug layer              = enabled
GPU validation           = disabled
GPU profiling            = enabled
Render mode              = Rasterization
Loaded scene file        = Cube/Cube.gltf
Using testing scene      = disabled
Duplication              = 10
Duplication in cube      = enabled
Duplication offset       = 10
BLAS duplication         = enabled
Rays per pixel           = 9

ProfilingBuildAggregator: 
BuildBottomLevelAS	,	2.38208
BuildBottomLevelASGpu	,	0.143968
BuildTopLevelAS	,	1.32311
BuildTopLevelASGpu	,	0.065984

ProfilingAggregator: 
Render	,	1.00494	,	1.06712	,	1.15255	,	1.64944	,	1.7877	,	2.25318	,	3.46702	,	4.45867	,	5.68609	,	7.32194
Update	,	0.454017	,	0.45764	,	0.487827	,	0.579597	,	0.671668	,	0.848264	,	1.04146	,	1.34817	,	1.71706	,	2.26043

FpsAggregator: 
FPS	,	57	,	60	,	60	,	59	,	57	,	54	,	50	,	43	,	34	,	22
UPS	,	59	,	61	,	60	,	60	,	60	,	60	,	59	,	61	,	60	,	59
FrameTime	,	17.5439	,	16.6745	,	16.6672	,	16.9507	,	17.5455	,	18.5218	,	20.0511	,	23.2559	,	29.4118	,	45.4898
GigaRays/s	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0

AccelerationStructure: 
Ray tracing acceleration structure building statistics: 
	Bottom Level Acceleration Structure: 
		Maximum [B]: 1376
		Minimum [B]: 1376
		Currently Allocated [B]: 1376
		Currently Created [num]: 1
		Current Average [B]: 1376
		Maximum Triangles [num]: 12
		Minimum Triangles [num]: 12
		Total Triangles [num]: 12
		Maximum Meshes [num]: 1
		Minimum Meshes [num]: 1
		Total Meshes [num]: 1
	Top Level Acceleration Structure: 
		Maximum [B]: 64
		Minimum [B]: 64
		Currently Allocated [B]: 64
		Total Allocated [B]: 64
		Total Created [num]: 1
		Average Allocated [B]: 64
		Maximum Instances [num]: 1
		Minimum Instances [num]: 1
		Total Instances [num]: 1
	Scratch Buffer: 
		Maximum [B]: 1484
		Minimum [B]: 1484
		Currently Allocated [B]: 1484
		Total Allocated [B]: 1484
		Total Created [num]: 1
		Average Allocated [B]: 1484
ProfilingStack: 
[PROF] Profiling statistics: 
	Scopes entered : 1046107
	Scopes exited : 1046106
	Overhead per scope [ticks] : 1609.28
	Threads entered : 3
	Threads exited : 0
	============================
		         Sum[Mt]   Parent[%]  Samples[num]       Avg[Mt]   Last/Curr[Mt]        Self[Mt]     Self[%] 	 Name

	main:

		   12035.251 Mt   100.000 %         1 x    12035.253 Mt    12035.249 Mt        1.319 Mt     0.011 % 	 /

		   12034.001 Mt    99.990 %         1 x    12034.003 Mt    12034.000 Mt     4433.773 Mt    36.844 % 	 ./Main application loop

		       6.271 Mt     0.052 %         1 x        6.271 Mt        0.000 Mt        6.271 Mt   100.000 % 	 ../TexturedRLInitialization

		       7.106 Mt     0.059 %         1 x        7.106 Mt        0.000 Mt        7.106 Mt   100.000 % 	 ../DeferredRLInitialization

		     539.806 Mt     4.486 %         1 x      539.806 Mt        0.000 Mt        2.404 Mt     0.445 % 	 ../RayTracingRLInitialization

		     537.402 Mt    99.555 %         1 x      537.402 Mt        0.000 Mt      537.402 Mt   100.000 % 	 .../PipelineBuild

		      19.955 Mt     0.166 %         1 x       19.955 Mt        0.000 Mt       19.955 Mt   100.000 % 	 ../ResolveRLInitialization

		     769.260 Mt     6.392 %         1 x      769.260 Mt        0.000 Mt        4.033 Mt     0.524 % 	 ../Initialization

		     765.227 Mt    99.476 %         1 x      765.227 Mt        0.000 Mt        4.528 Mt     0.592 % 	 .../ScenePrep

		     109.192 Mt    14.269 %         1 x      109.192 Mt        0.000 Mt        9.921 Mt     9.086 % 	 ..../SceneLoad

		      93.297 Mt    85.443 %         1 x       93.297 Mt        0.000 Mt       24.066 Mt    25.795 % 	 ...../glTF-LoadScene

		      69.231 Mt    74.205 %         2 x       34.616 Mt        0.000 Mt       69.231 Mt   100.000 % 	 ....../glTF-LoadImageData

		       3.597 Mt     3.294 %         1 x        3.597 Mt        0.000 Mt        3.597 Mt   100.000 % 	 ...../glTF-CreateScene

		       2.376 Mt     2.176 %         1 x        2.376 Mt        0.000 Mt        2.376 Mt   100.000 % 	 ...../SceneDesc-CreateScene

		       0.851 Mt     0.111 %         1 x        0.851 Mt        0.000 Mt        0.008 Mt     0.887 % 	 ..../TexturedRLPrep

		       0.843 Mt    99.113 %         1 x        0.843 Mt        0.000 Mt        0.261 Mt    30.995 % 	 ...../PrepareForRendering

		       0.263 Mt    31.174 %         1 x        0.263 Mt        0.000 Mt        0.263 Mt   100.000 % 	 ....../PrepareMaterials

		       0.319 Mt    37.831 %         1 x        0.319 Mt        0.000 Mt        0.319 Mt   100.000 % 	 ....../PrepareDrawBundle

		       1.696 Mt     0.222 %         1 x        1.696 Mt        0.000 Mt        0.007 Mt     0.392 % 	 ..../DeferredRLPrep

		       1.690 Mt    99.608 %         1 x        1.690 Mt        0.000 Mt        1.136 Mt    67.233 % 	 ...../PrepareForRendering

		       0.026 Mt     1.519 %         1 x        0.026 Mt        0.000 Mt        0.026 Mt   100.000 % 	 ....../PrepareMaterials

		       0.214 Mt    12.650 %         1 x        0.214 Mt        0.000 Mt        0.214 Mt   100.000 % 	 ....../BuildMaterialCache

		       0.314 Mt    18.599 %         1 x        0.314 Mt        0.000 Mt        0.314 Mt   100.000 % 	 ....../PrepareDrawBundle

		     645.473 Mt    84.350 %         1 x      645.473 Mt        0.000 Mt        2.726 Mt     0.422 % 	 ..../RayTracingRLPrep

		       0.319 Mt     0.049 %         1 x        0.319 Mt        0.000 Mt        0.319 Mt   100.000 % 	 ...../PrepareAccelerationStructures

		       0.041 Mt     0.006 %         1 x        0.041 Mt        0.000 Mt        0.041 Mt   100.000 % 	 ...../PrepareCache

		       0.284 Mt     0.044 %         1 x        0.284 Mt        0.000 Mt        0.284 Mt   100.000 % 	 ...../BuildCache

		     641.394 Mt    99.368 %         1 x      641.394 Mt        0.000 Mt        0.007 Mt     0.001 % 	 ...../BuildAccelerationStructures

		     641.387 Mt    99.999 %         1 x      641.387 Mt        0.000 Mt      637.682 Mt    99.422 % 	 ....../AccelerationStructureBuild

		       2.382 Mt     0.371 %         1 x        2.382 Mt        0.000 Mt        2.382 Mt   100.000 % 	 ......./BuildBottomLevelAS

		       1.323 Mt     0.206 %         1 x        1.323 Mt        0.000 Mt        1.323 Mt   100.000 % 	 ......./BuildTopLevelAS

		       0.709 Mt     0.110 %         1 x        0.709 Mt        0.000 Mt        0.709 Mt   100.000 % 	 ...../BuildShaderTables

		       3.488 Mt     0.456 %         1 x        3.488 Mt        0.000 Mt        3.488 Mt   100.000 % 	 ..../GpuSidePrep

		    6257.829 Mt    52.001 %   1043450 x        0.006 Mt        8.973 Mt     2679.624 Mt    42.820 % 	 ../MainLoop

		    2245.301 Mt    35.880 %       600 x        3.742 Mt        2.937 Mt     2245.301 Mt   100.000 % 	 .../Update

		    1332.904 Mt    21.300 %       496 x        2.687 Mt        0.000 Mt     1217.477 Mt    91.340 % 	 .../Render

		     115.427 Mt     8.660 %       496 x        0.233 Mt        0.000 Mt        3.559 Mt     3.084 % 	 ..../TexturedRLPrep

		     111.868 Mt    96.916 %        12 x        9.322 Mt        0.000 Mt        3.685 Mt     3.294 % 	 ...../PrepareForRendering

		      14.650 Mt    13.096 %        12 x        1.221 Mt        0.000 Mt       14.650 Mt   100.000 % 	 ....../PrepareMaterials

		      93.533 Mt    83.610 %        12 x        7.794 Mt        0.000 Mt       93.533 Mt   100.000 % 	 ....../PrepareDrawBundle

	WindowThread:

		   12028.287 Mt   100.000 %         1 x    12028.288 Mt    12028.286 Mt    12028.287 Mt   100.000 % 	 /

	GpuThread:

		     173.262 Mt   100.000 %       496 x        0.349 Mt        0.000 Mt       89.406 Mt    51.602 % 	 /

		       0.215 Mt     0.124 %         1 x        0.215 Mt        0.000 Mt        0.005 Mt     2.540 % 	 ./ScenePrep

		       0.210 Mt    97.460 %         1 x        0.210 Mt        0.000 Mt        0.000 Mt     0.015 % 	 ../AccelerationStructureBuild

		       0.144 Mt    68.561 %         1 x        0.144 Mt        0.000 Mt        0.144 Mt   100.000 % 	 .../BuildBottomLevelASGpu

		       0.066 Mt    31.423 %         1 x        0.066 Mt        0.000 Mt        0.066 Mt   100.000 % 	 .../BuildTopLevelASGpu

		      82.652 Mt    47.703 %       496 x        0.167 Mt        0.000 Mt       82.652 Mt   100.000 % 	 ./Textured

		       0.989 Mt     0.571 %       496 x        0.002 Mt        0.000 Mt        0.989 Mt   100.000 % 	 ./Present


	============================


