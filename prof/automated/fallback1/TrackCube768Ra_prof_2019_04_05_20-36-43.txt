Configuration: 
Command line parameters  = Measurement.exe --scene Cube/Cube.gltf --profile-track Cube/Cube.trk --width 1024 --height 768 --profile-output TrackCube768Ra 
Window width             = 1024
Window height            = 768
Window title             = Direct3D App
Back-buffer count        = 3
Target FPS               = 0
Target UPS               = 60
Tearing                  = enabled
VSync                    = disabled
GUI rendering            = disabled
Debug layer              = enabled
GPU validation           = disabled
GPU profiling            = enabled
Render mode              = Rasterization
Loaded scene file        = Cube/Cube.gltf
Using testing scene      = disabled
Duplication              = 1
Duplication in cube      = disabled
Duplication offset       = 10
BLAS duplication         = disabled
Rays per pixel           = 9

ProfilingBuildAggregator: 
BuildBottomLevelAS	,	2.5617
BuildBottomLevelASGpu	,	0.144992
BuildTopLevelAS	,	1.24915
BuildTopLevelASGpu	,	0.065856

ProfilingAggregator: 
Render	,	0.566013	,	0.575673	,	0.572654	,	0.632425	,	0.620049	,	0.643292	,	0.726911	,	0.671668	,	0.608577	,	0.608275	,	0.619746	,	0.642084	,	0.615822	,	0.613709	,	0.619746	,	0.623369	,	0.623067	,	0.707289	,	0.830152	,	0.867887	,	0.591068	,	0.60103	,	0.610992	,	0.621256	,	0.590464	,	0.588955	,	0.65748	,	0.651443	,	0.578088	,	0.951204
Update	,	0.403303	,	0.405416	,	0.393341	,	0.40119	,	0.396058	,	0.399378	,	0.394549	,	0.393945	,	0.393643	,	0.419001	,	0.393341	,	0.408736	,	0.398775	,	0.399982	,	0.403605	,	0.399982	,	0.402397	,	0.400888	,	0.401794	,	0.415378	,	0.402096	,	0.399681	,	0.391228	,	0.395152	,	0.387908	,	0.423831	,	0.428661	,	0.427755	,	0.393643	,	0.405718

FpsAggregator: 
FPS	,	1525	,	1640	,	1645	,	1638	,	1485	,	1486	,	1474	,	1469	,	1485	,	1492	,	1484	,	1492	,	1485	,	1495	,	1489	,	1474	,	1462	,	1493	,	1514	,	1528	,	1536	,	1538	,	1517	,	1530	,	1540	,	1547	,	1548	,	1543	,	1581	,	1467
UPS	,	59	,	61	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60
FrameTime	,	0.655893	,	0.610358	,	0.607937	,	0.610812	,	0.673402	,	0.673262	,	0.678713	,	0.680838	,	0.673491	,	0.670416	,	0.674013	,	0.670484	,	0.673616	,	0.668959	,	0.671631	,	0.678834	,	0.684198	,	0.670138	,	0.660815	,	0.65501	,	0.65105	,	0.650504	,	0.659222	,	0.653946	,	0.649467	,	0.646482	,	0.646306	,	0.648162	,	0.632531	,	0.682048
GigaRays/s	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0

AccelerationStructure: 
Ray tracing acceleration structure building statistics: 
	Bottom Level Acceleration Structure: 
		Maximum [B]: 1376
		Minimum [B]: 1376
		Currently Allocated [B]: 1376
		Currently Created [num]: 1
		Current Average [B]: 1376
		Maximum Triangles [num]: 12
		Minimum Triangles [num]: 12
		Total Triangles [num]: 12
		Maximum Meshes [num]: 1
		Minimum Meshes [num]: 1
		Total Meshes [num]: 1
	Top Level Acceleration Structure: 
		Maximum [B]: 64
		Minimum [B]: 64
		Currently Allocated [B]: 64
		Total Allocated [B]: 64
		Total Created [num]: 1
		Average Allocated [B]: 64
		Maximum Instances [num]: 1
		Minimum Instances [num]: 1
		Total Instances [num]: 1
	Scratch Buffer: 
		Maximum [B]: 1484
		Minimum [B]: 1484
		Currently Allocated [B]: 1484
		Total Allocated [B]: 1484
		Total Created [num]: 1
		Average Allocated [B]: 1484
ProfilingStack: 
[PROF] Profiling statistics: 
	Scopes entered : 232466
	Scopes exited : 232465
	Overhead per scope [ticks] : 1607.47
	Threads entered : 3
	Threads exited : 0
	============================
		         Sum[Mt]   Parent[%]  Samples[num]       Avg[Mt]   Last/Curr[Mt]        Self[Mt]     Self[%] 	 Name

	main:

		   31469.106 Mt   100.000 %         1 x    31469.109 Mt    31469.105 Mt        0.645 Mt     0.002 % 	 /

		   31468.522 Mt    99.998 %         1 x    31468.525 Mt    31468.522 Mt      476.013 Mt     1.513 % 	 ./Main application loop

		       6.684 Mt     0.021 %         1 x        6.684 Mt        0.000 Mt        6.684 Mt   100.000 % 	 ../TexturedRLInitialization

		       7.284 Mt     0.023 %         1 x        7.284 Mt        0.000 Mt        7.284 Mt   100.000 % 	 ../DeferredRLInitialization

		     406.606 Mt     1.292 %         1 x      406.606 Mt        0.000 Mt        1.831 Mt     0.450 % 	 ../RayTracingRLInitialization

		     404.776 Mt    99.550 %         1 x      404.776 Mt        0.000 Mt      404.776 Mt   100.000 % 	 .../PipelineBuild

		      18.002 Mt     0.057 %         1 x       18.002 Mt        0.000 Mt       18.002 Mt   100.000 % 	 ../ResolveRLInitialization

		     733.119 Mt     2.330 %         1 x      733.119 Mt        0.000 Mt        4.110 Mt     0.561 % 	 ../Initialization

		     729.009 Mt    99.439 %         1 x      729.009 Mt        0.000 Mt        0.931 Mt     0.128 % 	 .../ScenePrep

		     105.300 Mt    14.444 %         1 x      105.300 Mt        0.000 Mt       10.268 Mt     9.751 % 	 ..../SceneLoad

		      89.349 Mt    84.852 %         1 x       89.349 Mt        0.000 Mt       20.621 Mt    23.079 % 	 ...../glTF-LoadScene

		      68.728 Mt    76.921 %         2 x       34.364 Mt        0.000 Mt       68.728 Mt   100.000 % 	 ....../glTF-LoadImageData

		       3.251 Mt     3.088 %         1 x        3.251 Mt        0.000 Mt        3.251 Mt   100.000 % 	 ...../glTF-CreateScene

		       2.432 Mt     2.310 %         1 x        2.432 Mt        0.000 Mt        2.432 Mt   100.000 % 	 ...../SceneDesc-CreateScene

		       0.774 Mt     0.106 %         1 x        0.774 Mt        0.000 Mt        0.007 Mt     0.936 % 	 ..../TexturedRLPrep

		       0.767 Mt    99.064 %         1 x        0.767 Mt        0.000 Mt        0.212 Mt    27.598 % 	 ...../PrepareForRendering

		       0.248 Mt    32.402 %         1 x        0.248 Mt        0.000 Mt        0.248 Mt   100.000 % 	 ....../PrepareMaterials

		       0.307 Mt    40.000 %         1 x        0.307 Mt        0.000 Mt        0.307 Mt   100.000 % 	 ....../PrepareDrawBundle

		       1.657 Mt     0.227 %         1 x        1.657 Mt        0.000 Mt        0.007 Mt     0.419 % 	 ..../DeferredRLPrep

		       1.650 Mt    99.581 %         1 x        1.650 Mt        0.000 Mt        1.140 Mt    69.105 % 	 ...../PrepareForRendering

		       0.025 Mt     1.537 %         1 x        0.025 Mt        0.000 Mt        0.025 Mt   100.000 % 	 ....../PrepareMaterials

		       0.182 Mt    11.030 %         1 x        0.182 Mt        0.000 Mt        0.182 Mt   100.000 % 	 ....../BuildMaterialCache

		       0.302 Mt    18.328 %         1 x        0.302 Mt        0.000 Mt        0.302 Mt   100.000 % 	 ....../PrepareDrawBundle

		     616.928 Mt    84.626 %         1 x      616.928 Mt        0.000 Mt        2.702 Mt     0.438 % 	 ..../RayTracingRLPrep

		       0.310 Mt     0.050 %         1 x        0.310 Mt        0.000 Mt        0.310 Mt   100.000 % 	 ...../PrepareAccelerationStructures

		       0.042 Mt     0.007 %         1 x        0.042 Mt        0.000 Mt        0.042 Mt   100.000 % 	 ...../PrepareCache

		       0.295 Mt     0.048 %         1 x        0.295 Mt        0.000 Mt        0.295 Mt   100.000 % 	 ...../BuildCache

		     612.836 Mt    99.337 %         1 x      612.836 Mt        0.000 Mt        0.007 Mt     0.001 % 	 ...../BuildAccelerationStructures

		     612.829 Mt    99.999 %         1 x      612.829 Mt        0.000 Mt      609.018 Mt    99.378 % 	 ....../AccelerationStructureBuild

		       2.562 Mt     0.418 %         1 x        2.562 Mt        0.000 Mt        2.562 Mt   100.000 % 	 ......./BuildBottomLevelAS

		       1.249 Mt     0.204 %         1 x        1.249 Mt        0.000 Mt        1.249 Mt   100.000 % 	 ......./BuildTopLevelAS

		       0.743 Mt     0.120 %         1 x        0.743 Mt        0.000 Mt        0.743 Mt   100.000 % 	 ...../BuildShaderTables

		       3.418 Mt     0.469 %         1 x        3.418 Mt        0.000 Mt        3.418 Mt   100.000 % 	 ..../GpuSidePrep

		   29820.814 Mt    94.764 %     48212 x        0.619 Mt        3.353 Mt      385.865 Mt     1.294 % 	 ../MainLoop

		     779.390 Mt     2.614 %      1801 x        0.433 Mt        0.435 Mt      779.390 Mt   100.000 % 	 .../Update

		   28655.559 Mt    96.092 %     45604 x        0.628 Mt        2.904 Mt    28455.837 Mt    99.303 % 	 .../Render

		     199.722 Mt     0.697 %     45604 x        0.004 Mt        0.005 Mt      199.722 Mt   100.000 % 	 ..../TexturedRLPrep

	WindowThread:

		   31465.381 Mt   100.000 %         1 x    31465.382 Mt    31465.381 Mt    31465.381 Mt   100.000 % 	 /

	GpuThread:

		    2940.767 Mt   100.000 %     45604 x        0.064 Mt        0.125 Mt       82.234 Mt     2.796 % 	 /

		       0.216 Mt     0.007 %         1 x        0.216 Mt        0.000 Mt        0.005 Mt     2.515 % 	 ./ScenePrep

		       0.211 Mt    97.485 %         1 x        0.211 Mt        0.000 Mt        0.000 Mt     0.015 % 	 ../AccelerationStructureBuild

		       0.145 Mt    68.756 %         1 x        0.145 Mt        0.000 Mt        0.145 Mt   100.000 % 	 .../BuildBottomLevelASGpu

		       0.066 Mt    31.229 %         1 x        0.066 Mt        0.000 Mt        0.066 Mt   100.000 % 	 .../BuildTopLevelASGpu

		    2781.179 Mt    94.573 %     45604 x        0.061 Mt        0.123 Mt     2781.179 Mt   100.000 % 	 ./Textured

		      77.137 Mt     2.623 %     45604 x        0.002 Mt        0.002 Mt       77.137 Mt   100.000 % 	 ./Present


	============================


