Configuration: 
Command line parameters  = Measurement.exe --scene Cube/Cube.gltf --profile-track Cube/Cube.trk --width 2560 --height 1440 --profile-output TrackCube1440Ra 
Window width             = 2560
Window height            = 1421
Window title             = Direct3D App
Back-buffer count        = 3
Target FPS               = 0
Target UPS               = 60
Tearing                  = enabled
VSync                    = disabled
GUI rendering            = disabled
Debug layer              = enabled
GPU validation           = disabled
GPU profiling            = enabled
Render mode              = Rasterization
Loaded scene file        = Cube/Cube.gltf
Using testing scene      = disabled
Duplication              = 1
Duplication in cube      = disabled
Duplication offset       = 10
BLAS duplication         = disabled
Rays per pixel           = 9

ProfilingBuildAggregator: 
BuildBottomLevelAS	,	2.60426
BuildBottomLevelASGpu	,	0.144768
BuildTopLevelAS	,	1.24493
BuildTopLevelASGpu	,	0.0656

ProfilingAggregator: 
Render	,	0.584125	,	0.585333	,	0.581409	,	0.57205	,	0.571144	,	0.580805	,	0.580502	,	0.582918	,	0.589861	,	0.604653	,	0.579295	,	0.658386	,	0.571145	,	0.571447	,	0.573258	,	0.579597	,	0.589257	,	0.594992	,	0.598917	,	0.628803	,	0.601332	,	0.586238	,	0.578692	,	0.579295	,	0.614614	,	0.662914	,	0.579899	,	0.570541	,	0.571749	,	0.622765
Update	,	0.429566	,	0.391831	,	0.413265	,	0.402699	,	0.39787	,	0.410246	,	0.397567	,	0.399077	,	0.39153	,	0.425944	,	0.402397	,	0.392737	,	0.397266	,	0.394548	,	0.398473	,	0.399077	,	0.399681	,	0.400586	,	0.402397	,	0.401492	,	0.457641	,	0.399076	,	0.404812	,	0.402398	,	0.399379	,	0.417793	,	0.427453	,	0.396058	,	0.393342	,	0.380058

FpsAggregator: 
FPS	,	1382	,	1574	,	1606	,	1599	,	1623	,	1613	,	1604	,	1605	,	1584	,	1576	,	1583	,	1601	,	1609	,	1630	,	1623	,	1613	,	1588	,	1573	,	1559	,	1556	,	1556	,	1613	,	1629	,	1636	,	1628	,	1627	,	1631	,	1615	,	1631	,	1626
UPS	,	59	,	61	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60
FrameTime	,	0.72396	,	0.635846	,	0.623	,	0.625603	,	0.616172	,	0.62018	,	0.623694	,	0.623143	,	0.631668	,	0.634734	,	0.63187	,	0.624631	,	0.621525	,	0.613714	,	0.616336	,	0.620213	,	0.629992	,	0.635856	,	0.641614	,	0.643076	,	0.642681	,	0.620258	,	0.614203	,	0.611427	,	0.614461	,	0.614671	,	0.613355	,	0.619542	,	0.613245	,	0.615227
GigaRays/s	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0

AccelerationStructure: 
Ray tracing acceleration structure building statistics: 
	Bottom Level Acceleration Structure: 
		Maximum [B]: 1376
		Minimum [B]: 1376
		Currently Allocated [B]: 1376
		Currently Created [num]: 1
		Current Average [B]: 1376
		Maximum Triangles [num]: 12
		Minimum Triangles [num]: 12
		Total Triangles [num]: 12
		Maximum Meshes [num]: 1
		Minimum Meshes [num]: 1
		Total Meshes [num]: 1
	Top Level Acceleration Structure: 
		Maximum [B]: 64
		Minimum [B]: 64
		Currently Allocated [B]: 64
		Total Allocated [B]: 64
		Total Created [num]: 1
		Average Allocated [B]: 64
		Maximum Instances [num]: 1
		Minimum Instances [num]: 1
		Total Instances [num]: 1
	Scratch Buffer: 
		Maximum [B]: 1484
		Minimum [B]: 1484
		Currently Allocated [B]: 1484
		Total Allocated [B]: 1484
		Total Created [num]: 1
		Average Allocated [B]: 1484
ProfilingStack: 
[PROF] Profiling statistics: 
	Scopes entered : 243884
	Scopes exited : 243883
	Overhead per scope [ticks] : 1608.38
	Threads entered : 3
	Threads exited : 0
	============================
		         Sum[Mt]   Parent[%]  Samples[num]       Avg[Mt]   Last/Curr[Mt]        Self[Mt]     Self[%] 	 Name

	main:

		   31515.313 Mt   100.000 %         1 x    31515.316 Mt    31515.310 Mt        0.581 Mt     0.002 % 	 /

		   31514.801 Mt    99.998 %         1 x    31514.804 Mt    31514.800 Mt      488.076 Mt     1.549 % 	 ./Main application loop

		       6.425 Mt     0.020 %         1 x        6.425 Mt        0.000 Mt        6.425 Mt   100.000 % 	 ../TexturedRLInitialization

		       7.016 Mt     0.022 %         1 x        7.016 Mt        0.000 Mt        7.016 Mt   100.000 % 	 ../DeferredRLInitialization

		     423.586 Mt     1.344 %         1 x      423.586 Mt        0.000 Mt        1.927 Mt     0.455 % 	 ../RayTracingRLInitialization

		     421.659 Mt    99.545 %         1 x      421.659 Mt        0.000 Mt      421.659 Mt   100.000 % 	 .../PipelineBuild

		      18.218 Mt     0.058 %         1 x       18.218 Mt        0.000 Mt       18.218 Mt   100.000 % 	 ../ResolveRLInitialization

		     760.595 Mt     2.413 %         1 x      760.595 Mt        0.000 Mt        3.970 Mt     0.522 % 	 ../Initialization

		     756.624 Mt    99.478 %         1 x      756.624 Mt        0.000 Mt        4.432 Mt     0.586 % 	 .../ScenePrep

		     106.894 Mt    14.128 %         1 x      106.894 Mt        0.000 Mt       10.431 Mt     9.758 % 	 ..../SceneLoad

		      90.744 Mt    84.892 %         1 x       90.744 Mt        0.000 Mt       21.045 Mt    23.191 % 	 ...../glTF-LoadScene

		      69.700 Mt    76.809 %         2 x       34.850 Mt        0.000 Mt       69.700 Mt   100.000 % 	 ....../glTF-LoadImageData

		       3.380 Mt     3.162 %         1 x        3.380 Mt        0.000 Mt        3.380 Mt   100.000 % 	 ...../glTF-CreateScene

		       2.339 Mt     2.188 %         1 x        2.339 Mt        0.000 Mt        2.339 Mt   100.000 % 	 ...../SceneDesc-CreateScene

		       1.031 Mt     0.136 %         1 x        1.031 Mt        0.000 Mt        0.008 Mt     0.820 % 	 ..../TexturedRLPrep

		       1.022 Mt    99.180 %         1 x        1.022 Mt        0.000 Mt        0.270 Mt    26.462 % 	 ...../PrepareForRendering

		       0.350 Mt    34.229 %         1 x        0.350 Mt        0.000 Mt        0.350 Mt   100.000 % 	 ....../PrepareMaterials

		       0.402 Mt    39.309 %         1 x        0.402 Mt        0.000 Mt        0.402 Mt   100.000 % 	 ....../PrepareDrawBundle

		       1.994 Mt     0.263 %         1 x        1.994 Mt        0.000 Mt        0.008 Mt     0.379 % 	 ..../DeferredRLPrep

		       1.986 Mt    99.621 %         1 x        1.986 Mt        0.000 Mt        1.376 Mt    69.281 % 	 ...../PrepareForRendering

		       0.031 Mt     1.550 %         1 x        0.031 Mt        0.000 Mt        0.031 Mt   100.000 % 	 ....../PrepareMaterials

		       0.266 Mt    13.376 %         1 x        0.266 Mt        0.000 Mt        0.266 Mt   100.000 % 	 ....../BuildMaterialCache

		       0.314 Mt    15.793 %         1 x        0.314 Mt        0.000 Mt        0.314 Mt   100.000 % 	 ....../PrepareDrawBundle

		     638.586 Mt    84.399 %         1 x      638.586 Mt        0.000 Mt        2.814 Mt     0.441 % 	 ..../RayTracingRLPrep

		       0.315 Mt     0.049 %         1 x        0.315 Mt        0.000 Mt        0.315 Mt   100.000 % 	 ...../PrepareAccelerationStructures

		       0.043 Mt     0.007 %         1 x        0.043 Mt        0.000 Mt        0.043 Mt   100.000 % 	 ...../PrepareCache

		       1.534 Mt     0.240 %         1 x        1.534 Mt        0.000 Mt        1.534 Mt   100.000 % 	 ...../BuildCache

		     633.108 Mt    99.142 %         1 x      633.108 Mt        0.000 Mt        0.007 Mt     0.001 % 	 ...../BuildAccelerationStructures

		     633.101 Mt    99.999 %         1 x      633.101 Mt        0.000 Mt      629.252 Mt    99.392 % 	 ....../AccelerationStructureBuild

		       2.604 Mt     0.411 %         1 x        2.604 Mt        0.000 Mt        2.604 Mt   100.000 % 	 ......./BuildBottomLevelAS

		       1.245 Mt     0.197 %         1 x        1.245 Mt        0.000 Mt        1.245 Mt   100.000 % 	 ......./BuildTopLevelAS

		       0.772 Mt     0.121 %         1 x        0.772 Mt        0.000 Mt        0.772 Mt   100.000 % 	 ...../BuildShaderTables

		       3.688 Mt     0.487 %         1 x        3.688 Mt        0.000 Mt        3.688 Mt   100.000 % 	 ..../GpuSidePrep

		   29810.885 Mt    94.593 %     50466 x        0.591 Mt        1.227 Mt      398.516 Mt     1.337 % 	 ../MainLoop

		     831.539 Mt     2.789 %      1801 x        0.462 Mt        0.469 Mt      831.539 Mt   100.000 % 	 .../Update

		   28580.830 Mt    95.874 %     47895 x        0.597 Mt        0.746 Mt    28378.903 Mt    99.293 % 	 .../Render

		     201.927 Mt     0.707 %     47895 x        0.004 Mt        0.004 Mt      201.927 Mt   100.000 % 	 ..../TexturedRLPrep

	WindowThread:

		   31511.837 Mt   100.000 %         1 x    31511.838 Mt    31511.836 Mt    31511.837 Mt   100.000 % 	 /

	GpuThread:

		    2534.685 Mt   100.000 %     47895 x        0.053 Mt        0.017 Mt      140.890 Mt     5.558 % 	 /

		       0.216 Mt     0.009 %         1 x        0.216 Mt        0.000 Mt        0.006 Mt     2.579 % 	 ./ScenePrep

		       0.210 Mt    97.421 %         1 x        0.210 Mt        0.000 Mt        0.000 Mt     0.000 % 	 ../AccelerationStructureBuild

		       0.145 Mt    68.817 %         1 x        0.145 Mt        0.000 Mt        0.145 Mt   100.000 % 	 .../BuildBottomLevelASGpu

		       0.066 Mt    31.183 %         1 x        0.066 Mt        0.000 Mt        0.066 Mt   100.000 % 	 .../BuildTopLevelASGpu

		    2348.580 Mt    92.658 %     47895 x        0.049 Mt        0.016 Mt     2348.580 Mt   100.000 % 	 ./Textured

		      44.999 Mt     1.775 %     47895 x        0.001 Mt        0.001 Mt       44.999 Mt   100.000 % 	 ./Present


	============================


