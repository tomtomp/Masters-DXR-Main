Configuration: 
Command line parameters  = Measurement.exe --scene Cube/Cube.gltf --profile-track Cube/Cube.trk --rt-mode-pure --width 2560 --height 1440 --profile-output TrackCube1440Rp 
Window width             = 2560
Window height            = 1421
Window title             = Direct3D App
Back-buffer count        = 3
Target FPS               = 0
Target UPS               = 60
Tearing                  = enabled
VSync                    = disabled
GUI rendering            = disabled
Debug layer              = enabled
GPU validation           = disabled
GPU profiling            = enabled
Render mode              = Ray Tracing
Loaded scene file        = Cube/Cube.gltf
Using testing scene      = disabled
Duplication              = 1
Duplication in cube      = disabled
Duplication offset       = 10
BLAS duplication         = disabled
Rays per pixel           = 10

ProfilingBuildAggregator: 
BuildBottomLevelAS	,	2.81618
BuildBottomLevelASGpu	,	0.145088
BuildTopLevelAS	,	1.4985
BuildTopLevelASGpu	,	0.066816

ProfilingAggregator: 
Render	,	71.5087	,	95.2867	,	100.174	,	85.5784	,	85.1513	,	96.2155	,	88.4305	,	108.315	,	114.238	,	85.7973	,	95.1442	,	85.8145	,	77.58	,	77.452	,	84.7712	,	98.9261	,	116.595	,	123.326	,	122.167	,	111.449	,	79.9008	,	71.6811	,	58.3491	,	51.4148	,	41.9073	,	34.3472	,	30.635	,	28.617
Update	,	0.349268	,	0.342929	,	0.347758	,	0.345645	,	0.403001	,	0.346852	,	0.348061	,	0.345645	,	0.349871	,	0.346852	,	0.346853	,	0.365569	,	0.346551	,	0.354098	,	0.347154	,	0.358323	,	0.344437	,	0.34474	,	0.348966	,	0.343834	,	0.344136	,	0.347758	,	0.345042	,	0.345343	,	0.355305	,	0.357117	,	0.355305	,	0.347155
RayTracing	,	69.5706	,	93.4413	,	98.2639	,	83.7342	,	83.1235	,	94.3078	,	86.5495	,	106.435	,	112.4	,	83.9015	,	93.2717	,	83.69	,	75.717	,	75.1262	,	82.892	,	97.0327	,	114.702	,	121.438	,	120.22	,	109.503	,	78.0598	,	69.8177	,	56.5034	,	49.5401	,	39.975	,	32.4337	,	28.8559	,	26.8587
RayTracingRL	,	62.0996	,	85.9681	,	91.1139	,	76.2564	,	75.9695	,	87.1484	,	79.3985	,	98.9499	,	104.913	,	76.7489	,	85.7868	,	76.5315	,	68.1402	,	67.8813	,	75.3028	,	89.4408	,	107.091	,	114.27	,	113.054	,	102.344	,	70.587	,	62.3444	,	49.0429	,	42.0657	,	32.5192	,	25.2965	,	21.4134	,	19.4131

FpsAggregator: 
FPS	,	14	,	12	,	10	,	11	,	13	,	11	,	11	,	11	,	9	,	10	,	12	,	11	,	13	,	13	,	13	,	11	,	10	,	8	,	8	,	9	,	12	,	13	,	16	,	18	,	21	,	26	,	31	,	34
UPS	,	55	,	62	,	61	,	64	,	64	,	62	,	63	,	63	,	63	,	65	,	64	,	64	,	64	,	61	,	65	,	61	,	64	,	61	,	60	,	67	,	66	,	61	,	64	,	61	,	62	,	60	,	63	,	61
FrameTime	,	71.6655	,	87.1438	,	102.327	,	95.5761	,	82.1915	,	95.314	,	94.8176	,	97.5392	,	116.381	,	106.236	,	89.7765	,	95.421	,	81.8986	,	78.4073	,	83.2122	,	94.4382	,	108.68	,	126.067	,	125.813	,	122.993	,	88.7113	,	77.919	,	65.6647	,	55.8228	,	48.8423	,	38.586	,	33.2309	,	30.1777
GigaRays/s	,	0.507603	,	0.417443	,	0.355504	,	0.380614	,	0.442596	,	0.38166	,	0.383659	,	0.372954	,	0.312573	,	0.342421	,	0.405202	,	0.381233	,	0.444178	,	0.463957	,	0.437166	,	0.3852	,	0.334721	,	0.288557	,	0.289141	,	0.29577	,	0.410067	,	0.466865	,	0.55399	,	0.651663	,	0.744797	,	0.942767	,	1.09469	,	1.20545

AccelerationStructure: 
Ray tracing acceleration structure building statistics: 
	Bottom Level Acceleration Structure: 
		Maximum [B]: 1376
		Minimum [B]: 1376
		Currently Allocated [B]: 1376
		Currently Created [num]: 1
		Current Average [B]: 1376
		Maximum Triangles [num]: 12
		Minimum Triangles [num]: 12
		Total Triangles [num]: 12
		Maximum Meshes [num]: 1
		Minimum Meshes [num]: 1
		Total Meshes [num]: 1
	Top Level Acceleration Structure: 
		Maximum [B]: 64
		Minimum [B]: 64
		Currently Allocated [B]: 64
		Total Allocated [B]: 64
		Total Created [num]: 1
		Average Allocated [B]: 64
		Maximum Instances [num]: 1
		Minimum Instances [num]: 1
		Total Instances [num]: 1
	Scratch Buffer: 
		Maximum [B]: 1484
		Minimum [B]: 1484
		Currently Allocated [B]: 1484
		Total Allocated [B]: 1484
		Total Created [num]: 1
		Average Allocated [B]: 1484
ProfilingStack: 
[PROF] Profiling statistics: 
	Scopes entered : 7134
	Scopes exited : 7133
	Overhead per scope [ticks] : 1608.08
	Threads entered : 3
	Threads exited : 0
	============================
		         Sum[Mt]   Parent[%]  Samples[num]       Avg[Mt]   Last/Curr[Mt]        Self[Mt]     Self[%] 	 Name

	main:

		   30692.690 Mt   100.000 %         1 x    30692.692 Mt    30692.689 Mt        0.583 Mt     0.002 % 	 /

		   30692.170 Mt    99.998 %         1 x    30692.172 Mt    30692.169 Mt      232.700 Mt     0.758 % 	 ./Main application loop

		       5.988 Mt     0.020 %         1 x        5.988 Mt        0.000 Mt        5.988 Mt   100.000 % 	 ../TexturedRLInitialization

		       7.053 Mt     0.023 %         1 x        7.053 Mt        0.000 Mt        7.053 Mt   100.000 % 	 ../DeferredRLInitialization

		     422.556 Mt     1.377 %         1 x      422.556 Mt        0.000 Mt        2.132 Mt     0.504 % 	 ../RayTracingRLInitialization

		     420.424 Mt    99.496 %         1 x      420.424 Mt        0.000 Mt      420.424 Mt   100.000 % 	 .../PipelineBuild

		      18.624 Mt     0.061 %         1 x       18.624 Mt        0.000 Mt       18.624 Mt   100.000 % 	 ../ResolveRLInitialization

		     755.825 Mt     2.463 %         1 x      755.825 Mt        0.000 Mt        3.974 Mt     0.526 % 	 ../Initialization

		     751.850 Mt    99.474 %         1 x      751.850 Mt        0.000 Mt        3.622 Mt     0.482 % 	 .../ScenePrep

		     105.184 Mt    13.990 %         1 x      105.184 Mt        0.000 Mt       10.388 Mt     9.876 % 	 ..../SceneLoad

		      89.007 Mt    84.620 %         1 x       89.007 Mt        0.000 Mt       20.488 Mt    23.019 % 	 ...../glTF-LoadScene

		      68.519 Mt    76.981 %         2 x       34.259 Mt        0.000 Mt       68.519 Mt   100.000 % 	 ....../glTF-LoadImageData

		       3.489 Mt     3.317 %         1 x        3.489 Mt        0.000 Mt        3.489 Mt   100.000 % 	 ...../glTF-CreateScene

		       2.300 Mt     2.187 %         1 x        2.300 Mt        0.000 Mt        2.300 Mt   100.000 % 	 ...../SceneDesc-CreateScene

		       0.849 Mt     0.113 %         1 x        0.849 Mt        0.000 Mt        0.008 Mt     0.889 % 	 ..../TexturedRLPrep

		       0.841 Mt    99.111 %         1 x        0.841 Mt        0.000 Mt        0.251 Mt    29.864 % 	 ...../PrepareForRendering

		       0.280 Mt    33.345 %         1 x        0.280 Mt        0.000 Mt        0.280 Mt   100.000 % 	 ....../PrepareMaterials

		       0.309 Mt    36.791 %         1 x        0.309 Mt        0.000 Mt        0.309 Mt   100.000 % 	 ....../PrepareDrawBundle

		       1.674 Mt     0.223 %         1 x        1.674 Mt        0.000 Mt        0.007 Mt     0.397 % 	 ..../DeferredRLPrep

		       1.668 Mt    99.603 %         1 x        1.668 Mt        0.000 Mt        1.156 Mt    69.285 % 	 ...../PrepareForRendering

		       0.026 Mt     1.557 %         1 x        0.026 Mt        0.000 Mt        0.026 Mt   100.000 % 	 ....../PrepareMaterials

		       0.201 Mt    12.054 %         1 x        0.201 Mt        0.000 Mt        0.201 Mt   100.000 % 	 ....../BuildMaterialCache

		       0.285 Mt    17.104 %         1 x        0.285 Mt        0.000 Mt        0.285 Mt   100.000 % 	 ....../PrepareDrawBundle

		     636.799 Mt    84.698 %         1 x      636.799 Mt        0.000 Mt        2.841 Mt     0.446 % 	 ..../RayTracingRLPrep

		       0.312 Mt     0.049 %         1 x        0.312 Mt        0.000 Mt        0.312 Mt   100.000 % 	 ...../PrepareAccelerationStructures

		       0.041 Mt     0.006 %         1 x        0.041 Mt        0.000 Mt        0.041 Mt   100.000 % 	 ...../PrepareCache

		       2.077 Mt     0.326 %         1 x        2.077 Mt        0.000 Mt        2.077 Mt   100.000 % 	 ...../BuildCache

		     630.696 Mt    99.042 %         1 x      630.696 Mt        0.000 Mt        0.008 Mt     0.001 % 	 ...../BuildAccelerationStructures

		     630.687 Mt    99.999 %         1 x      630.687 Mt        0.000 Mt      626.373 Mt    99.316 % 	 ....../AccelerationStructureBuild

		       2.816 Mt     0.447 %         1 x        2.816 Mt        0.000 Mt        2.816 Mt   100.000 % 	 ......./BuildBottomLevelAS

		       1.498 Mt     0.238 %         1 x        1.498 Mt        0.000 Mt        1.498 Mt   100.000 % 	 ......./BuildTopLevelAS

		       0.832 Mt     0.131 %         1 x        0.832 Mt        0.000 Mt        0.832 Mt   100.000 % 	 ...../BuildShaderTables

		       3.722 Mt     0.495 %         1 x        3.722 Mt        0.000 Mt        3.722 Mt   100.000 % 	 ..../GpuSidePrep

		   29249.424 Mt    95.299 %      2992 x        9.776 Mt       37.264 Mt      131.264 Mt     0.449 % 	 ../MainLoop

		     761.582 Mt     2.604 %      1753 x        0.434 Mt        0.740 Mt      761.582 Mt   100.000 % 	 .../Update

		   28356.579 Mt    96.947 %       392 x       72.338 Mt       29.587 Mt    28354.766 Mt    99.994 % 	 .../Render

		       1.813 Mt     0.006 %       392 x        0.005 Mt        0.005 Mt        1.813 Mt   100.000 % 	 ..../RayTracingRLPrep

	WindowThread:

		   30687.553 Mt   100.000 %         1 x    30687.554 Mt    30687.553 Mt    30687.553 Mt   100.000 % 	 /

	GpuThread:

		   27752.044 Mt   100.000 %       392 x       70.796 Mt       27.569 Mt      143.345 Mt     0.517 % 	 /

		       0.217 Mt     0.001 %         1 x        0.217 Mt        0.000 Mt        0.006 Mt     2.546 % 	 ./ScenePrep

		       0.212 Mt    97.454 %         1 x        0.212 Mt        0.000 Mt        0.000 Mt     0.015 % 	 ../AccelerationStructureBuild

		       0.145 Mt    68.458 %         1 x        0.145 Mt        0.000 Mt        0.145 Mt   100.000 % 	 .../BuildBottomLevelASGpu

		       0.067 Mt    31.526 %         1 x        0.067 Mt        0.000 Mt        0.067 Mt   100.000 % 	 .../BuildTopLevelASGpu

		   27607.959 Mt    99.481 %       392 x       70.428 Mt       27.568 Mt        0.012 Mt     0.000 % 	 ./RayTracing

		   24723.548 Mt    89.552 %       392 x       63.070 Mt       19.568 Mt    24723.548 Mt   100.000 % 	 ../RayTracingRL

		    2884.398 Mt    10.448 %       392 x        7.358 Mt        8.000 Mt     2884.398 Mt   100.000 % 	 ../ResolveRL

		       0.523 Mt     0.002 %       392 x        0.001 Mt        0.001 Mt        0.523 Mt   100.000 % 	 ./Present


	============================


