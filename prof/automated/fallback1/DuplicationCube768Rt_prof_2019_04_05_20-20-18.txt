Configuration: 
Command line parameters  = Measurement.exe --scene Cube/Cube.gltf --duplication-cube --profile-duplication 10 --rt-mode --width 1024 --height 768 --profile-output DuplicationCube768Rt 
Window width             = 1024
Window height            = 768
Window title             = Direct3D App
Back-buffer count        = 3
Target FPS               = 60
Target UPS               = 60
Tearing                  = disabled
VSync                    = disabled
GUI rendering            = disabled
Debug layer              = enabled
GPU validation           = disabled
GPU profiling            = enabled
Render mode              = Ray Tracing
Loaded scene file        = Cube/Cube.gltf
Using testing scene      = disabled
Duplication              = 10
Duplication in cube      = enabled
Duplication offset       = 10
BLAS duplication         = enabled
Rays per pixel           = 9

ProfilingBuildAggregator: 
BuildBottomLevelAS	,	3.0293	,	17.3792	,	43.4634	,	88.8655	,	155.808	,	246.272	,	390.57	,	545.455	,	761.908
BuildBottomLevelASGpu	,	0.143008	,	3.53443	,	7.39133	,	13.9035	,	24.2868	,	38.0368	,	219.27	,	212.371	,	246.667
BuildTopLevelAS	,	1.36296	,	0.593483	,	3.7224	,	3.48513	,	0.720874	,	1.14953	,	0.96992	,	3.22824	,	2.22632
BuildTopLevelASGpu	,	0.066272	,	0.102048	,	0.10064	,	0.112928	,	0.129952	,	0.258848	,	0.271392	,	0.651744	,	0.693632

ProfilingAggregator: 
Render	,	6.54945	,	8.48868	,	10.5565	,	14.8096	,	20.173	,	27.6453	,	37.7499	,	862.335	,	1125.24	,	1482.99
Update	,	0.39002	,	0.39153	,	0.449791	,	0.493261	,	0.5962	,	0.759212	,	0.941241	,	1.23164	,	1.58514	,	2.03765
RayTracing	,	4.2023	,	6.13718	,	7.98544	,	12.131	,	16.9641	,	23.6761	,	32.5642	,	272.92	,	298.706	,	339.622
RayTracingRL	,	2.33424	,	4.224	,	6.28624	,	10.1847	,	15.2157	,	21.8948	,	30.7659	,	48.8163	,	80.0595	,	86.3871

FpsAggregator: 
FPS	,	57	,	59	,	57	,	54	,	37	,	12	,	10	,	1	,	1	,	1
UPS	,	59	,	60	,	61	,	60	,	60	,	60	,	61	,	3	,	68	,	98
FrameTime	,	17.5439	,	17.0791	,	17.5439	,	18.7237	,	27.2787	,	84.6732	,	102.389	,	1138	,	1630.23	,	2227.66
GigaRays/s	,	0.403439	,	0.414419	,	0.403438	,	0.378017	,	0.259466	,	0.0835906	,	0.0691275	,	0.00621957	,	0.00434165	,	0.00317728

AccelerationStructure: 
Ray tracing acceleration structure building statistics: 
	Bottom Level Acceleration Structure: 
		Maximum [B]: 1376
		Minimum [B]: 1376
		Currently Allocated [B]: 1376000
		Currently Created [num]: 1000
		Current Average [B]: 1376
		Maximum Triangles [num]: 12
		Minimum Triangles [num]: 12
		Total Triangles [num]: 12000
		Maximum Meshes [num]: 1
		Minimum Meshes [num]: 1
		Total Meshes [num]: 1000
	Top Level Acceleration Structure: 
		Maximum [B]: 64000
		Minimum [B]: 64000
		Currently Allocated [B]: 64000
		Total Allocated [B]: 64000
		Total Created [num]: 1
		Average Allocated [B]: 64000
		Maximum Instances [num]: 1000
		Minimum Instances [num]: 1000
		Total Instances [num]: 1000
	Scratch Buffer: 
		Maximum [B]: 219988
		Minimum [B]: 219988
		Currently Allocated [B]: 219988
		Total Allocated [B]: 219988
		Total Created [num]: 1
		Average Allocated [B]: 219988
ProfilingStack: 
[PROF] Profiling statistics: 
	Scopes entered : 224281
	Scopes exited : 224280
	Overhead per scope [ticks] : 1600.23
	Threads entered : 3
	Threads exited : 0
	============================
		         Sum[Mt]   Parent[%]  Samples[num]       Avg[Mt]   Last/Curr[Mt]        Self[Mt]     Self[%] 	 Name

	main:

		   13998.852 Mt   100.000 %         1 x    13998.854 Mt    13998.851 Mt        0.707 Mt     0.005 % 	 /

		   13998.204 Mt    99.995 %         1 x    13998.207 Mt    13998.204 Mt     1158.359 Mt     8.275 % 	 ./Main application loop

		       7.326 Mt     0.052 %         1 x        7.326 Mt        0.000 Mt        7.326 Mt   100.000 % 	 ../TexturedRLInitialization

		       8.488 Mt     0.061 %         1 x        8.488 Mt        0.000 Mt        8.488 Mt   100.000 % 	 ../DeferredRLInitialization

		     419.092 Mt     2.994 %         1 x      419.092 Mt        0.000 Mt        2.121 Mt     0.506 % 	 ../RayTracingRLInitialization

		     416.972 Mt    99.494 %         1 x      416.972 Mt        0.000 Mt      416.972 Mt   100.000 % 	 .../PipelineBuild

		      20.213 Mt     0.144 %         1 x       20.213 Mt        0.000 Mt       20.213 Mt   100.000 % 	 ../ResolveRLInitialization

		     735.702 Mt     5.256 %         1 x      735.702 Mt        0.000 Mt        4.053 Mt     0.551 % 	 ../Initialization

		     731.650 Mt    99.449 %         1 x      731.650 Mt        0.000 Mt        2.443 Mt     0.334 % 	 .../ScenePrep

		     107.990 Mt    14.760 %         1 x      107.990 Mt        0.000 Mt       10.575 Mt     9.793 % 	 ..../SceneLoad

		      88.851 Mt    82.277 %         1 x       88.851 Mt        0.000 Mt       20.905 Mt    23.528 % 	 ...../glTF-LoadScene

		      67.946 Mt    76.472 %         2 x       33.973 Mt        0.000 Mt       67.946 Mt   100.000 % 	 ....../glTF-LoadImageData

		       4.981 Mt     4.613 %         1 x        4.981 Mt        0.000 Mt        4.981 Mt   100.000 % 	 ...../glTF-CreateScene

		       3.583 Mt     3.318 %         1 x        3.583 Mt        0.000 Mt        3.583 Mt   100.000 % 	 ...../SceneDesc-CreateScene

		       1.141 Mt     0.156 %         1 x        1.141 Mt        0.000 Mt        0.008 Mt     0.661 % 	 ..../TexturedRLPrep

		       1.134 Mt    99.339 %         1 x        1.134 Mt        0.000 Mt        0.375 Mt    33.094 % 	 ...../PrepareForRendering

		       0.448 Mt    39.537 %         1 x        0.448 Mt        0.000 Mt        0.448 Mt   100.000 % 	 ....../PrepareMaterials

		       0.310 Mt    27.370 %         1 x        0.310 Mt        0.000 Mt        0.310 Mt   100.000 % 	 ....../PrepareDrawBundle

		       3.135 Mt     0.429 %         1 x        3.135 Mt        0.000 Mt        0.007 Mt     0.221 % 	 ..../DeferredRLPrep

		       3.128 Mt    99.779 %         1 x        3.128 Mt        0.000 Mt        1.546 Mt    49.435 % 	 ...../PrepareForRendering

		       0.026 Mt     0.840 %         1 x        0.026 Mt        0.000 Mt        0.026 Mt   100.000 % 	 ....../PrepareMaterials

		       1.233 Mt    39.429 %         1 x        1.233 Mt        0.000 Mt        1.233 Mt   100.000 % 	 ....../BuildMaterialCache

		       0.322 Mt    10.296 %         1 x        0.322 Mt        0.000 Mt        0.322 Mt   100.000 % 	 ....../PrepareDrawBundle

		     613.234 Mt    83.815 %         1 x      613.234 Mt        0.000 Mt        2.981 Mt     0.486 % 	 ..../RayTracingRLPrep

		       0.306 Mt     0.050 %         1 x        0.306 Mt        0.000 Mt        0.306 Mt   100.000 % 	 ...../PrepareAccelerationStructures

		       0.043 Mt     0.007 %         1 x        0.043 Mt        0.000 Mt        0.043 Mt   100.000 % 	 ...../PrepareCache

		       0.529 Mt     0.086 %         1 x        0.529 Mt        0.000 Mt        0.529 Mt   100.000 % 	 ...../BuildCache

		     608.178 Mt    99.176 %         1 x      608.178 Mt        0.000 Mt        0.008 Mt     0.001 % 	 ...../BuildAccelerationStructures

		     608.171 Mt    99.999 %         1 x      608.171 Mt        0.000 Mt      603.779 Mt    99.278 % 	 ....../AccelerationStructureBuild

		       3.029 Mt     0.498 %         1 x        3.029 Mt        0.000 Mt        3.029 Mt   100.000 % 	 ......./BuildBottomLevelAS

		       1.363 Mt     0.224 %         1 x        1.363 Mt        0.000 Mt        1.363 Mt   100.000 % 	 ......./BuildTopLevelAS

		       1.196 Mt     0.195 %         1 x        1.196 Mt        0.000 Mt        1.196 Mt   100.000 % 	 ...../BuildShaderTables

		       3.705 Mt     0.506 %         1 x        3.705 Mt        0.000 Mt        3.705 Mt   100.000 % 	 ..../GpuSidePrep

		   11649.023 Mt    83.218 %    221051 x        0.053 Mt      375.826 Mt      621.002 Mt     5.331 % 	 ../MainLoop

		    2461.658 Mt    21.132 %       723 x        3.405 Mt        2.056 Mt     2461.658 Mt   100.000 % 	 .../Update

		    8566.363 Mt    73.537 %       290 x       29.539 Mt       90.218 Mt     4926.772 Mt    57.513 % 	 .../Render

		     128.735 Mt     1.503 %       290 x        0.444 Mt        0.005 Mt        1.370 Mt     1.064 % 	 ..../DeferredRLPrep

		     127.365 Mt    98.936 %        10 x       12.736 Mt        0.000 Mt        4.013 Mt     3.151 % 	 ...../PrepareForRendering

		      14.733 Mt    11.567 %        10 x        1.473 Mt        0.000 Mt       14.733 Mt   100.000 % 	 ....../PrepareMaterials

		       0.010 Mt     0.008 %        10 x        0.001 Mt        0.000 Mt        0.010 Mt   100.000 % 	 ....../BuildMaterialCache

		     108.609 Mt    85.274 %        10 x       10.861 Mt        0.000 Mt      108.609 Mt   100.000 % 	 ....../PrepareDrawBundle

		    3510.857 Mt    40.984 %       290 x       12.106 Mt        0.005 Mt       39.690 Mt     1.131 % 	 ..../RayTracingRLPrep

		     871.652 Mt    24.827 %        10 x       87.165 Mt        0.000 Mt      871.652 Mt   100.000 % 	 ...../PrepareAccelerationStructures

		      15.028 Mt     0.428 %        10 x        1.503 Mt        0.000 Mt       15.028 Mt   100.000 % 	 ...../PrepareCache

		       8.727 Mt     0.249 %        10 x        0.873 Mt        0.000 Mt        8.727 Mt   100.000 % 	 ...../BuildCache

		    2444.810 Mt    69.636 %        10 x      244.481 Mt        0.000 Mt        0.047 Mt     0.002 % 	 ...../BuildAccelerationStructures

		    2444.763 Mt    99.998 %        10 x      244.476 Mt        0.000 Mt       19.420 Mt     0.794 % 	 ....../AccelerationStructureBuild

		    2407.576 Mt    98.479 %        10 x      240.758 Mt        0.000 Mt     2407.576 Mt   100.000 % 	 ......./BuildBottomLevelAS

		      17.767 Mt     0.727 %        10 x        1.777 Mt        0.000 Mt       17.767 Mt   100.000 % 	 ......./BuildTopLevelAS

		     130.950 Mt     3.730 %        10 x       13.095 Mt        0.000 Mt      130.950 Mt   100.000 % 	 ...../BuildShaderTables

	WindowThread:

		   13995.594 Mt   100.000 %         1 x    13995.594 Mt    13995.593 Mt    13995.594 Mt   100.000 % 	 /

	GpuThread:

		    4154.103 Mt   100.000 %       290 x       14.324 Mt       79.013 Mt       89.998 Mt     2.166 % 	 /

		       0.215 Mt     0.005 %         1 x        0.215 Mt        0.000 Mt        0.006 Mt     2.562 % 	 ./ScenePrep

		       0.209 Mt    97.438 %         1 x        0.209 Mt        0.000 Mt        0.000 Mt     0.015 % 	 ../AccelerationStructureBuild

		       0.143 Mt    68.323 %         1 x        0.143 Mt        0.000 Mt        0.143 Mt   100.000 % 	 .../BuildBottomLevelASGpu

		       0.066 Mt    31.662 %         1 x        0.066 Mt        0.000 Mt        0.066 Mt   100.000 % 	 .../BuildTopLevelASGpu

		    4063.469 Mt    97.818 %       290 x       14.012 Mt       79.012 Mt        0.014 Mt     0.000 % 	 ./RayTracing

		      39.228 Mt     0.965 %       290 x        0.135 Mt        0.235 Mt       39.228 Mt   100.000 % 	 ../DeferredRL

		    2721.937 Mt    66.986 %       290 x        9.386 Mt       76.811 Mt     2721.937 Mt   100.000 % 	 ../RayTracingRL

		     508.776 Mt    12.521 %       290 x        1.754 Mt        1.966 Mt      508.776 Mt   100.000 % 	 ../ResolveRL

		     793.513 Mt    19.528 %        10 x       79.351 Mt        0.000 Mt        0.001 Mt     0.000 % 	 ../AccelerationStructureBuild

		     790.977 Mt    99.680 %        10 x       79.098 Mt        0.000 Mt      790.977 Mt   100.000 % 	 .../BuildBottomLevelASGpu

		       2.535 Mt     0.319 %        10 x        0.254 Mt        0.000 Mt        2.535 Mt   100.000 % 	 .../BuildTopLevelASGpu

		       0.421 Mt     0.010 %       290 x        0.001 Mt        0.001 Mt        0.421 Mt   100.000 % 	 ./Present


	============================


