Configuration: 
Command line parameters  = Measurement.exe --scene Suzanne/Suzanne.gltf --duplication-cube --profile-duplication 10 --width 1024 --height 768 --profile-output DuplicationSuzanne768Ra 
Window width             = 1024
Window height            = 768
Window title             = Direct3D App
Back-buffer count        = 3
Target FPS               = 60
Target UPS               = 60
Tearing                  = disabled
VSync                    = disabled
GUI rendering            = disabled
Debug layer              = enabled
GPU validation           = disabled
GPU profiling            = enabled
Render mode              = Rasterization
Loaded scene file        = Suzanne/Suzanne.gltf
Using testing scene      = disabled
Duplication              = 10
Duplication in cube      = enabled
Duplication offset       = 10
BLAS duplication         = enabled
Rays per pixel           = 9

ProfilingBuildAggregator: 
BuildBottomLevelAS	,	2.55355
BuildBottomLevelASGpu	,	1.45139
BuildTopLevelAS	,	1.05052
BuildTopLevelASGpu	,	0.06912

ProfilingAggregator: 
Render	,	0.956335	,	0.956034	,	1.06682	,	1.51239	,	2.13908	,	2.79716	,	5.93181	,	5.60458	,	6.30312	,	8.61185
Update	,	0.454621	,	0.45432	,	0.48994	,	0.548806	,	0.700347	,	0.825624	,	1.07105	,	1.39617	,	1.74815	,	2.25952

FpsAggregator: 
FPS	,	57	,	60	,	60	,	59	,	57	,	54	,	49	,	42	,	30	,	16
UPS	,	59	,	60	,	60	,	61	,	60	,	60	,	60	,	60	,	60	,	60
FrameTime	,	17.5439	,	16.6667	,	16.6667	,	16.9586	,	17.5466	,	18.5208	,	20.4133	,	23.8172	,	33.3451	,	62.532
GigaRays/s	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0

AccelerationStructure: 
Ray tracing acceleration structure building statistics: 
	Bottom Level Acceleration Structure: 
		Maximum [B]: 456560
		Minimum [B]: 456560
		Currently Allocated [B]: 456560
		Currently Created [num]: 1
		Current Average [B]: 456560
		Maximum Triangles [num]: 3936
		Minimum Triangles [num]: 3936
		Total Triangles [num]: 3936
		Maximum Meshes [num]: 1
		Minimum Meshes [num]: 1
		Total Meshes [num]: 1
	Top Level Acceleration Structure: 
		Maximum [B]: 64
		Minimum [B]: 64
		Currently Allocated [B]: 64
		Total Allocated [B]: 64
		Total Created [num]: 1
		Average Allocated [B]: 64
		Maximum Instances [num]: 1
		Minimum Instances [num]: 1
		Total Instances [num]: 1
	Scratch Buffer: 
		Maximum [B]: 490304
		Minimum [B]: 490304
		Currently Allocated [B]: 490304
		Total Allocated [B]: 490304
		Total Created [num]: 1
		Average Allocated [B]: 490304
ProfilingStack: 
[PROF] Profiling statistics: 
	Scopes entered : 962093
	Scopes exited : 962092
	Overhead per scope [ticks] : 1606.89
	Threads entered : 3
	Threads exited : 0
	============================
		         Sum[Mt]   Parent[%]  Samples[num]       Avg[Mt]   Last/Curr[Mt]        Self[Mt]     Self[%] 	 Name

	main:

		   12166.683 Mt   100.000 %         1 x    12166.685 Mt    12166.682 Mt        0.667 Mt     0.005 % 	 /

		   12166.075 Mt    99.995 %         1 x    12166.078 Mt    12166.074 Mt     4224.222 Mt    34.721 % 	 ./Main application loop

		       5.637 Mt     0.046 %         1 x        5.637 Mt        0.000 Mt        5.637 Mt   100.000 % 	 ../TexturedRLInitialization

		       7.491 Mt     0.062 %         1 x        7.491 Mt        0.000 Mt        7.491 Mt   100.000 % 	 ../DeferredRLInitialization

		     435.363 Mt     3.578 %         1 x      435.363 Mt        0.000 Mt        1.943 Mt     0.446 % 	 ../RayTracingRLInitialization

		     433.419 Mt    99.554 %         1 x      433.419 Mt        0.000 Mt      433.419 Mt   100.000 % 	 .../PipelineBuild

		      18.472 Mt     0.152 %         1 x       18.472 Mt        0.000 Mt       18.472 Mt   100.000 % 	 ../ResolveRLInitialization

		     918.853 Mt     7.553 %         1 x      918.853 Mt        0.000 Mt        4.274 Mt     0.465 % 	 ../Initialization

		     914.579 Mt    99.535 %         1 x      914.579 Mt        0.000 Mt        1.839 Mt     0.201 % 	 .../ScenePrep

		     262.978 Mt    28.754 %         1 x      262.978 Mt        0.000 Mt       14.481 Mt     5.506 % 	 ..../SceneLoad

		     237.166 Mt    90.185 %         1 x      237.166 Mt        0.000 Mt       27.010 Mt    11.389 % 	 ...../glTF-LoadScene

		     210.156 Mt    88.611 %         2 x      105.078 Mt        0.000 Mt      210.156 Mt   100.000 % 	 ....../glTF-LoadImageData

		       6.643 Mt     2.526 %         1 x        6.643 Mt        0.000 Mt        6.643 Mt   100.000 % 	 ...../glTF-CreateScene

		       4.689 Mt     1.783 %         1 x        4.689 Mt        0.000 Mt        4.689 Mt   100.000 % 	 ...../SceneDesc-CreateScene

		       0.888 Mt     0.097 %         1 x        0.888 Mt        0.000 Mt        0.008 Mt     0.850 % 	 ..../TexturedRLPrep

		       0.880 Mt    99.150 %         1 x        0.880 Mt        0.000 Mt        0.299 Mt    33.962 % 	 ...../PrepareForRendering

		       0.264 Mt    29.949 %         1 x        0.264 Mt        0.000 Mt        0.264 Mt   100.000 % 	 ....../PrepareMaterials

		       0.318 Mt    36.089 %         1 x        0.318 Mt        0.000 Mt        0.318 Mt   100.000 % 	 ....../PrepareDrawBundle

		       1.842 Mt     0.201 %         1 x        1.842 Mt        0.000 Mt        0.007 Mt     0.377 % 	 ..../DeferredRLPrep

		       1.835 Mt    99.623 %         1 x        1.835 Mt        0.000 Mt        1.241 Mt    67.643 % 	 ...../PrepareForRendering

		       0.046 Mt     2.500 %         1 x        0.046 Mt        0.000 Mt        0.046 Mt   100.000 % 	 ....../PrepareMaterials

		       0.230 Mt    12.551 %         1 x        0.230 Mt        0.000 Mt        0.230 Mt   100.000 % 	 ....../BuildMaterialCache

		       0.318 Mt    17.305 %         1 x        0.318 Mt        0.000 Mt        0.318 Mt   100.000 % 	 ....../PrepareDrawBundle

		     642.016 Mt    70.198 %         1 x      642.016 Mt        0.000 Mt        2.917 Mt     0.454 % 	 ..../RayTracingRLPrep

		       0.317 Mt     0.049 %         1 x        0.317 Mt        0.000 Mt        0.317 Mt   100.000 % 	 ...../PrepareAccelerationStructures

		       0.043 Mt     0.007 %         1 x        0.043 Mt        0.000 Mt        0.043 Mt   100.000 % 	 ...../PrepareCache

		       2.112 Mt     0.329 %         1 x        2.112 Mt        0.000 Mt        2.112 Mt   100.000 % 	 ...../BuildCache

		     635.913 Mt    99.049 %         1 x      635.913 Mt        0.000 Mt        0.008 Mt     0.001 % 	 ...../BuildAccelerationStructures

		     635.905 Mt    99.999 %         1 x      635.905 Mt        0.000 Mt      632.301 Mt    99.433 % 	 ....../AccelerationStructureBuild

		       2.554 Mt     0.402 %         1 x        2.554 Mt        0.000 Mt        2.554 Mt   100.000 % 	 ......./BuildBottomLevelAS

		       1.051 Mt     0.165 %         1 x        1.051 Mt        0.000 Mt        1.051 Mt   100.000 % 	 ......./BuildTopLevelAS

		       0.716 Mt     0.111 %         1 x        0.716 Mt        0.000 Mt        0.716 Mt   100.000 % 	 ...../BuildShaderTables

		       5.017 Mt     0.549 %         1 x        5.017 Mt        0.000 Mt        5.017 Mt   100.000 % 	 ..../GpuSidePrep

		    6556.038 Mt    53.888 %    959482 x        0.007 Mt        2.520 Mt     2471.285 Mt    37.695 % 	 ../MainLoop

		    2249.886 Mt    34.318 %       601 x        3.744 Mt        2.510 Mt     2249.886 Mt   100.000 % 	 .../Update

		    1834.866 Mt    27.987 %       485 x        3.783 Mt        9.164 Mt     1721.741 Mt    93.835 % 	 .../Render

		     113.125 Mt     6.165 %       485 x        0.233 Mt        0.008 Mt        3.227 Mt     2.852 % 	 ..../TexturedRLPrep

		     109.898 Mt    97.148 %        11 x        9.991 Mt        0.000 Mt        3.867 Mt     3.518 % 	 ...../PrepareForRendering

		      14.230 Mt    12.948 %        11 x        1.294 Mt        0.000 Mt       14.230 Mt   100.000 % 	 ....../PrepareMaterials

		      91.802 Mt    83.533 %        11 x        8.346 Mt        0.000 Mt       91.802 Mt   100.000 % 	 ....../PrepareDrawBundle

	WindowThread:

		   12163.030 Mt   100.000 %         1 x    12163.031 Mt    12163.029 Mt    12163.030 Mt   100.000 % 	 /

	GpuThread:

		     767.962 Mt   100.000 %       485 x        1.583 Mt        1.972 Mt       89.640 Mt    11.672 % 	 /

		       1.526 Mt     0.199 %         1 x        1.526 Mt        0.000 Mt        0.005 Mt     0.327 % 	 ./ScenePrep

		       1.521 Mt    99.673 %         1 x        1.521 Mt        0.000 Mt        0.000 Mt     0.002 % 	 ../AccelerationStructureBuild

		       1.451 Mt    95.452 %         1 x        1.451 Mt        0.000 Mt        1.451 Mt   100.000 % 	 .../BuildBottomLevelASGpu

		       0.069 Mt     4.546 %         1 x        0.069 Mt        0.000 Mt        0.069 Mt   100.000 % 	 .../BuildTopLevelASGpu

		     675.977 Mt    88.022 %       485 x        1.394 Mt        1.971 Mt      675.977 Mt   100.000 % 	 ./Textured

		       0.820 Mt     0.107 %       485 x        0.002 Mt        0.001 Mt        0.820 Mt   100.000 % 	 ./Present


	============================


