Configuration: 
Command line parameters  = E:\programming\FIT-DIP\Masters-DXR-Main-git\build\Measurement\x64\Debug\Measurement.exe --scene Cube/Cube.gltf --profile-track Cube/Cube.trk --rt-mode --width 2560 --height 1440 --profile-output TrackCube1440Rt 
Window width             = 2560
Window height            = 1421
Window title             = Direct3D App
Back-buffer count        = 3
Fill Triangles           = enabled
Triangles Font Clock-Wise= enabled
Fast Build AS            = disabled
Target FPS               = 0
Target UPS               = 60
Tearing                  = enabled
VSync                    = disabled
GUI rendering            = disabled
Debug layer              = enabled
GPU validation           = disabled
GPU profiling            = enabled
Render mode              = Ray Tracing
Loaded scene file        = Cube/Cube.gltf
Using testing scene      = disabled
Duplication              = 1
Duplication in cube      = disabled
Duplication offset       = 10
BLAS duplication         = disabled
Rays per pixel           = 10

ProfilingAggregator: 
Render	,	11.1137	,	9.2754	,	6.9493	,	7.6897	,	9.7286	,	11.1977	,	10.743	,	8.4486	,	11.6628	,	11.6927	,	8.9628	,	11.092	,	9.0704	,	8.1271	,	10.9305	,	8.9792	,	9.6279	,	10.3559	,	14.989	,	11.2683	,	9.1066	,	12.0669	,	10.0023	,	7.0016	,	7.9558	,	8.0208	,	8.8544	,	7.4174	,	8.8487
Update	,	0.3915	,	0.7821	,	0.7161	,	0.6301	,	0.8566	,	0.3424	,	0.5013	,	0.7127	,	0.7395	,	0.796	,	0.6062	,	0.8819	,	0.557	,	0.6007	,	0.6902	,	0.5315	,	0.4282	,	0.5859	,	0.9907	,	0.6724	,	0.7179	,	0.8298	,	0.8163	,	0.4399	,	0.5347	,	0.8248	,	0.6938	,	0.8296	,	0.7836
RayTracing	,	4.4742	,	3.0821	,	1.4871	,	1.8896	,	2.7081	,	3.4686	,	3.3281	,	1.7291	,	3.2238	,	3.627	,	1.8919	,	3.3143	,	2.3095	,	2.0153	,	3.4216	,	1.898	,	1.9404	,	2.0223	,	4.6476	,	2.1537	,	1.4512	,	4.161	,	3.1262	,	1.7817	,	2.2806	,	1.7024	,	3.2161	,	2.3948	,	3.3515
RayTracingGpu	,	3.6553	,	4.02294	,	4.12637	,	4.27514	,	4.83146	,	5.19277	,	4.92986	,	5.3249	,	6.22557	,	5.94826	,	5.66864	,	5.36566	,	4.77427	,	4.5585	,	5.11514	,	5.35043	,	5.93952	,	6.53802	,	7.18298	,	7.38054	,	6.10627	,	5.02134	,	4.44928	,	3.95146	,	4.23725	,	3.68307	,	3.48278	,	3.38672	,	3.00694
DeferredRLGpu	,	0.157728	,	0.210624	,	0.216832	,	0.217824	,	0.224896	,	0.278976	,	0.253952	,	0.29536	,	0.345472	,	0.300768	,	0.266752	,	0.286944	,	0.231328	,	0.21024	,	0.238624	,	0.28384	,	0.330208	,	0.389312	,	0.41232	,	0.408384	,	0.329952	,	0.237984	,	0.192864	,	0.149152	,	0.126944	,	0.097856	,	0.078592	,	0.064384	,	0.05328
RayTracingRLGpu	,	0.766176	,	1.03274	,	1.10752	,	1.25555	,	1.83325	,	2.0352	,	1.84026	,	2.12326	,	2.48992	,	2.48346	,	2.3249	,	2.16586	,	1.73475	,	1.57501	,	1.79766	,	2.14285	,	2.53104	,	2.99373	,	3.13206	,	3.40314	,	2.58832	,	1.78381	,	1.38934	,	1.05613	,	0.877888	,	0.658464	,	0.495424	,	0.413408	,	0.344736
ResolveRLGpu	,	2.73034	,	2.77824	,	2.80083	,	2.80038	,	2.77194	,	2.87725	,	2.8344	,	2.90509	,	3.38899	,	3.16282	,	3.07568	,	2.91168	,	2.80678	,	2.7719	,	3.07754	,	2.92266	,	3.07722	,	3.1536	,	3.63738	,	3.56781	,	3.18688	,	2.9983	,	2.86589	,	2.74502	,	3.23117	,	2.92534	,	2.90762	,	2.90768	,	2.60752
Index	,	0	,	1	,	2	,	3	,	4	,	5	,	6	,	7	,	8	,	9	,	10	,	11	,	12	,	13	,	14	,	15	,	16	,	17	,	18	,	19	,	20	,	21	,	22	,	23	,	24	,	25	,	26	,	27	,	28

ProfilingBuildAggregator: 
BuildBottomLevelAS	,	1.3775
BuildBottomLevelASGpu	,	0.120512
BuildTopLevelAS	,	1.5003
BuildTopLevelASGpu	,	0.082272
Duplication	,	1
Triangles	,	12
Meshes	,	1
BottomLevels	,	1
Index	,	0

FpsAggregator: 
FPS	,	93	,	99	,	103	,	103	,	96	,	91	,	88	,	92	,	90	,	85	,	92	,	84	,	93	,	91	,	94	,	89	,	87	,	85	,	72	,	69	,	72	,	91	,	94	,	100	,	108	,	108	,	120	,	129	,	130
UPS	,	59	,	61	,	60	,	61	,	60	,	60	,	60	,	61	,	60	,	60	,	60	,	60	,	61	,	60	,	60	,	61	,	60	,	60	,	60	,	60	,	61	,	60	,	60	,	61	,	60	,	60	,	60	,	60	,	60
FrameTime	,	10.8409	,	10.1808	,	9.76913	,	9.73614	,	10.4978	,	11.0275	,	11.4052	,	10.9244	,	11.1617	,	11.8126	,	10.8765	,	11.9743	,	10.7722	,	11.0505	,	10.674	,	11.3002	,	11.5258	,	11.8467	,	13.9696	,	14.5154	,	13.9294	,	11.0001	,	10.6697	,	10.072	,	9.29545	,	9.26974	,	8.33547	,	7.75496	,	7.73249
GigaRays/s	,	3.35559	,	3.57316	,	3.72373	,	3.73635	,	3.46526	,	3.29881	,	3.18957	,	3.32993	,	3.25913	,	3.07955	,	3.34461	,	3.03797	,	3.37698	,	3.29193	,	3.40805	,	3.21919	,	3.15618	,	3.07069	,	2.60406	,	2.50613	,	2.61156	,	3.30702	,	3.40945	,	3.61177	,	3.91348	,	3.92434	,	4.36419	,	4.69088	,	4.70451
Index	,	0	,	1	,	2	,	3	,	4	,	5	,	6	,	7	,	8	,	9	,	10	,	11	,	12	,	13	,	14	,	15	,	16	,	17	,	18	,	19	,	20	,	21	,	22	,	23	,	24	,	25	,	26	,	27	,	28


AccelerationStructure: 
Ray tracing acceleration structure building statistics: 
	Bottom Level Acceleration Structure: 
		Maximum [B]: 5168
		Minimum [B]: 5168
		Currently Allocated [B]: 5168
		Currently Created [num]: 1
		Current Average [B]: 5168
		Maximum Triangles [num]: 12
		Minimum Triangles [num]: 12
		Total Triangles [num]: 12
		Maximum Meshes [num]: 1
		Minimum Meshes [num]: 1
		Total Meshes [num]: 1
	Top Level Acceleration Structure: 
		Maximum [B]: 64
		Minimum [B]: 64
		Currently Allocated [B]: 64
		Total Allocated [B]: 64
		Total Created [num]: 1
		Average Allocated [B]: 64
		Maximum Instances [num]: 1
		Minimum Instances [num]: 1
		Total Instances [num]: 1
	Scratch Buffer: 
		Maximum [B]: 3584
		Minimum [B]: 3584
		Currently Allocated [B]: 3584
		Total Allocated [B]: 3584
		Total Created [num]: 1
		Average Allocated [B]: 3584
ProfilingStack: 
[PROF] Profiling statistics: 
	Scopes entered : 30555
	Scopes exited : 30554
	Overhead per scope [ticks] : 1001.5
	Threads entered : 3
	Threads exited : 0
	============================
		         Sum[Mt]   Parent[%]  Samples[num]       Avg[Mt]   Last/Curr[Mt]        Self[Mt]     Self[%] 	 Name

	main:

		   29775.216 Mt   100.000 %         1 x    29775.220 Mt    29775.214 Mt        0.710 Mt     0.002 % 	 /

		   29774.650 Mt    99.998 %         1 x    29774.656 Mt    29774.649 Mt      358.345 Mt     1.204 % 	 ./Main application loop

		       2.864 Mt     0.010 %         1 x        2.864 Mt        0.000 Mt        2.864 Mt   100.000 % 	 ../TexturedRLInitialization

		       6.255 Mt     0.021 %         1 x        6.255 Mt        0.000 Mt        6.255 Mt   100.000 % 	 ../DeferredRLInitialization

		      59.308 Mt     0.199 %         1 x       59.308 Mt        0.000 Mt        1.895 Mt     3.196 % 	 ../RayTracingRLInitialization

		      57.413 Mt    96.804 %         1 x       57.413 Mt        0.000 Mt       57.413 Mt   100.000 % 	 .../PipelineBuild

		      11.854 Mt     0.040 %         1 x       11.854 Mt        0.000 Mt       11.854 Mt   100.000 % 	 ../ResolveRLInitialization

		     182.071 Mt     0.611 %         1 x      182.071 Mt        0.000 Mt        4.302 Mt     2.363 % 	 ../Initialization

		     177.769 Mt    97.637 %         1 x      177.769 Mt        0.000 Mt        2.454 Mt     1.381 % 	 .../ScenePrep

		     133.553 Mt    75.127 %         1 x      133.553 Mt        0.000 Mt       20.665 Mt    15.473 % 	 ..../SceneLoad

		      79.523 Mt    59.545 %         1 x       79.523 Mt        0.000 Mt       31.516 Mt    39.631 % 	 ...../glTF-LoadScene

		      48.007 Mt    60.369 %         2 x       24.004 Mt        0.000 Mt       48.007 Mt   100.000 % 	 ....../glTF-LoadImageData

		      15.115 Mt    11.317 %         1 x       15.115 Mt        0.000 Mt       15.115 Mt   100.000 % 	 ...../glTF-CreateScene

		      18.250 Mt    13.665 %         1 x       18.250 Mt        0.000 Mt       18.250 Mt   100.000 % 	 ...../SceneDesc-CreateScene

		       3.499 Mt     1.968 %         1 x        3.499 Mt        0.000 Mt        0.016 Mt     0.446 % 	 ..../TexturedRLPrep

		       3.484 Mt    99.554 %         1 x        3.484 Mt        0.000 Mt        1.625 Mt    46.639 % 	 ...../PrepareForRendering

		       1.062 Mt    30.477 %         1 x        1.062 Mt        0.000 Mt        1.062 Mt   100.000 % 	 ....../PrepareMaterials

		       0.797 Mt    22.884 %         1 x        0.797 Mt        0.000 Mt        0.797 Mt   100.000 % 	 ....../PrepareDrawBundle

		       5.109 Mt     2.874 %         1 x        5.109 Mt        0.000 Mt        0.013 Mt     0.254 % 	 ..../DeferredRLPrep

		       5.096 Mt    99.746 %         1 x        5.096 Mt        0.000 Mt        3.348 Mt    65.688 % 	 ...../PrepareForRendering

		       0.044 Mt     0.855 %         1 x        0.044 Mt        0.000 Mt        0.044 Mt   100.000 % 	 ....../PrepareMaterials

		       0.972 Mt    19.062 %         1 x        0.972 Mt        0.000 Mt        0.972 Mt   100.000 % 	 ....../BuildMaterialCache

		       0.734 Mt    14.394 %         1 x        0.734 Mt        0.000 Mt        0.734 Mt   100.000 % 	 ....../PrepareDrawBundle

		      21.030 Mt    11.830 %         1 x       21.030 Mt        0.000 Mt        8.114 Mt    38.583 % 	 ..../RayTracingRLPrep

		       0.633 Mt     3.012 %         1 x        0.633 Mt        0.000 Mt        0.633 Mt   100.000 % 	 ...../PrepareAccelerationStructures

		       0.064 Mt     0.302 %         1 x        0.064 Mt        0.000 Mt        0.064 Mt   100.000 % 	 ...../PrepareCache

		       1.091 Mt     5.186 %         1 x        1.091 Mt        0.000 Mt        1.091 Mt   100.000 % 	 ...../BuildCache

		       8.502 Mt    40.430 %         1 x        8.502 Mt        0.000 Mt        0.013 Mt     0.159 % 	 ...../BuildAccelerationStructures

		       8.489 Mt    99.841 %         1 x        8.489 Mt        0.000 Mt        5.611 Mt    66.100 % 	 ....../AccelerationStructureBuild

		       1.377 Mt    16.227 %         1 x        1.377 Mt        0.000 Mt        1.377 Mt   100.000 % 	 ......./BuildBottomLevelAS

		       1.500 Mt    17.673 %         1 x        1.500 Mt        0.000 Mt        1.500 Mt   100.000 % 	 ......./BuildTopLevelAS

		       2.626 Mt    12.487 %         1 x        2.626 Mt        0.000 Mt        2.626 Mt   100.000 % 	 ...../BuildShaderTables

		      12.123 Mt     6.820 %         1 x       12.123 Mt        0.000 Mt       12.123 Mt   100.000 % 	 ..../GpuSidePrep

		   29153.954 Mt    97.915 %      4029 x        7.236 Mt       66.629 Mt     1240.643 Mt     4.255 % 	 ../MainLoop

		    1299.734 Mt     4.458 %      1747 x        0.744 Mt        1.000 Mt     1297.043 Mt    99.793 % 	 .../Update

		       2.691 Mt     0.207 %         1 x        2.691 Mt        0.000 Mt        2.691 Mt   100.000 % 	 ..../GuiModelApply

		   26613.577 Mt    91.286 %      2749 x        9.681 Mt       11.457 Mt    18949.682 Mt    71.203 % 	 .../Render

		    7663.896 Mt    28.797 %      2749 x        2.788 Mt        4.889 Mt     7628.709 Mt    99.541 % 	 ..../RayTracing

		      19.151 Mt     0.250 %      2749 x        0.007 Mt        0.011 Mt       19.151 Mt   100.000 % 	 ...../DeferredRLPrep

		      16.035 Mt     0.209 %      2749 x        0.006 Mt        0.009 Mt       16.035 Mt   100.000 % 	 ...../RayTracingRLPrep

	WindowThread:

		   29766.672 Mt   100.000 %         1 x    29766.674 Mt    29766.671 Mt    29766.672 Mt   100.000 % 	 /

	GpuThread:

		   13343.641 Mt   100.000 %      2749 x        4.854 Mt        3.014 Mt      172.668 Mt     1.294 % 	 /

		       0.211 Mt     0.002 %         1 x        0.211 Mt        0.000 Mt        0.007 Mt     3.338 % 	 ./ScenePrep

		       0.204 Mt    96.662 %         1 x        0.204 Mt        0.000 Mt        0.001 Mt     0.518 % 	 ../AccelerationStructureBuild

		       0.121 Mt    59.121 %         1 x        0.121 Mt        0.000 Mt        0.121 Mt   100.000 % 	 .../BuildBottomLevelASGpu

		       0.082 Mt    40.361 %         1 x        0.082 Mt        0.000 Mt        0.082 Mt   100.000 % 	 .../BuildTopLevelASGpu

		   13169.470 Mt    98.695 %      2749 x        4.791 Mt        3.013 Mt        3.509 Mt     0.027 % 	 ./RayTracingGpu

		     620.143 Mt     4.709 %      2749 x        0.226 Mt        0.055 Mt      620.143 Mt   100.000 % 	 ../DeferredRLGpu

		    4464.076 Mt    33.897 %      2749 x        1.624 Mt        0.348 Mt     4464.076 Mt   100.000 % 	 ../RayTracingRLGpu

		    8081.743 Mt    61.367 %      2749 x        2.940 Mt        2.610 Mt     8081.743 Mt   100.000 % 	 ../ResolveRLGpu

		       1.292 Mt     0.010 %      2749 x        0.000 Mt        0.000 Mt        1.292 Mt   100.000 % 	 ./Present


	============================


