Configuration: 
Command line parameters  = E:\programming\FIT-DIP\Masters-DXR-Main-git\build\Measurement\x64\Debug\Measurement.exe --scene Cube/Cube.gltf --duplication-cube --profile-duplication 10 --rt-mode --width 1024 --height 768 --profile-output DuplicationCube768Rt 
Window width             = 1024
Window height            = 768
Window title             = Direct3D App
Back-buffer count        = 3
Fill Triangles           = enabled
Triangles Font Clock-Wise= enabled
Fast Build AS            = disabled
Target FPS               = 60
Target UPS               = 60
Tearing                  = disabled
VSync                    = disabled
GUI rendering            = disabled
Debug layer              = enabled
GPU validation           = disabled
GPU profiling            = enabled
Render mode              = Ray Tracing
Loaded scene file        = Cube/Cube.gltf
Using testing scene      = disabled
Duplication              = 10
Duplication in cube      = enabled
Duplication offset       = 10
BLAS duplication         = enabled
Rays per pixel           = 10

ProfilingAggregator: 
Render	,	3.7962	,	5.5399	,	5.9669	,	3.6963	,	4.2079	,	5.4879	,	6.6189	,	6.1044	,	6.2846	,	4.6728	,	5.5152	,	7.2892	,	6.3849	,	5.7758	,	6.7616	,	7.6361	,	460.686	,	9.2274	,	772.013	,	10.4895
Update	,	0.3283	,	0.3105	,	0.3075	,	0.3193	,	0.388	,	0.3038	,	0.3704	,	0.3805	,	0.3951	,	0.4678	,	0.504	,	0.4802	,	0.6593	,	1.0036	,	0.7671	,	0.9012	,	0.9291	,	1.1531	,	1.2058	,	1.4372
RayTracing	,	1.5547	,	1.3144	,	1.3427	,	1.4371	,	1.7732	,	1.4076	,	1.7935	,	1.9576	,	2.1008	,	2.4552	,	3.1626	,	2.7268	,	4.0851	,	3.4943	,	4.6077	,	5.2447	,	380.309	,	6.7825	,	540.069	,	7.9483
RayTracingGpu	,	0.919328	,	2.68957	,	3.04586	,	0.928864	,	0.944864	,	2.65683	,	2.93581	,	2.59661	,	2.65104	,	0.969632	,	0.9912	,	3.04045	,	1.00698	,	1.0071	,	1.02611	,	1.02883	,	76.6283	,	1.00614	,	227.7	,	1.3569
DeferredRLGpu	,	0.01776	,	0.095776	,	0.110016	,	0.021632	,	0.025472	,	0.129536	,	0.135264	,	0.137248	,	0.156736	,	0.031392	,	0.03472	,	0.180928	,	0.037312	,	0.0368	,	0.04	,	0.040256	,	0.041824	,	0.042144	,	0.250688	,	0.04672
RayTracingRLGpu	,	0.09488	,	0.437408	,	0.51744	,	0.109344	,	0.125792	,	0.575712	,	0.632032	,	0.642464	,	0.66	,	0.147424	,	0.16512	,	0.754624	,	0.176928	,	0.176832	,	0.190464	,	0.193056	,	0.194432	,	0.192096	,	0.881504	,	0.208544
ResolveRLGpu	,	0.80496	,	2.15194	,	2.41443	,	0.79616	,	0.791904	,	1.9472	,	2.16406	,	1.81258	,	1.83043	,	0.788704	,	0.789664	,	2.10038	,	0.790752	,	0.79072	,	0.79232	,	0.792736	,	0.785184	,	0.769696	,	1.7737	,	1.09926
Index	,	0	,	1	,	2	,	3	,	4	,	5	,	6	,	7	,	8	,	9	,	10	,	11	,	12	,	13	,	14	,	15	,	16	,	17	,	18	,	19

ProfilingBuildAggregator: 
BuildBottomLevelAS	,	1.5373	,	2.2426	,	6.1092	,	16.9546	,	30.7715	,	48.1334	,	74.593	,	110.221	,	155.844	,	247.067
BuildBottomLevelASGpu	,	0.128992	,	2.27261	,	3.25549	,	18.9218	,	32.8419	,	23.762	,	37.7696	,	55.1981	,	75.3527	,	224.193
BuildTopLevelAS	,	1.2758	,	0.6446	,	0.6115	,	0.7865	,	0.674	,	0.663	,	0.7204	,	0.6715	,	0.7516	,	0.8063
BuildTopLevelASGpu	,	0.08432	,	0.25792	,	0.130752	,	0.360896	,	0.451744	,	0.211584	,	0.2128	,	0.201024	,	0.237312	,	0.572096
Duplication	,	1	,	2	,	3	,	4	,	5	,	6	,	7	,	8	,	9	,	10
Triangles	,	12	,	96	,	324	,	768	,	1500	,	2592	,	4116	,	6144	,	8748	,	12000
Meshes	,	1	,	8	,	27	,	64	,	125	,	216	,	343	,	512	,	729	,	1000
BottomLevels	,	1	,	8	,	27	,	64	,	125	,	216	,	343	,	512	,	729	,	1000
Index	,	0	,	1	,	2	,	3	,	4	,	5	,	6	,	7	,	8	,	9

FpsAggregator: 
FPS	,	56	,	59	,	59	,	59	,	58	,	59	,	53	,	59	,	53	,	59	,	39	,	59	,	23	,	59	,	5	,	59	,	2	,	58	,	1	,	55
UPS	,	59	,	60	,	60	,	60	,	60	,	61	,	59	,	61	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	43	,	97	,	10	,	117
FrameTime	,	17.8571	,	16.9492	,	16.9492	,	16.9492	,	17.2414	,	16.9543	,	18.9268	,	16.9492	,	18.9395	,	16.9492	,	25.6411	,	16.9492	,	43.6384	,	16.9492	,	200.001	,	16.9492	,	667.602	,	17.3692	,	1105.32	,	18.3236
GigaRays/s	,	0.440402	,	0.463994	,	0.463995	,	0.463995	,	0.45613	,	0.463853	,	0.415513	,	0.463994	,	0.415233	,	0.463994	,	0.306708	,	0.463995	,	0.180215	,	0.463993	,	0.0393215	,	0.463994	,	0.01178	,	0.452773	,	0.00711498	,	0.429191
Index	,	0	,	1	,	2	,	3	,	4	,	5	,	6	,	7	,	8	,	9	,	10	,	11	,	12	,	13	,	14	,	15	,	16	,	17	,	18	,	19


AccelerationStructure: 
Ray tracing acceleration structure building statistics: 
	Bottom Level Acceleration Structure: 
		Maximum [B]: 5168
		Minimum [B]: 5168
		Currently Allocated [B]: 5168000
		Currently Created [num]: 1000
		Current Average [B]: 5168
		Maximum Triangles [num]: 12
		Minimum Triangles [num]: 12
		Total Triangles [num]: 12000
		Maximum Meshes [num]: 1
		Minimum Meshes [num]: 1
		Total Meshes [num]: 1000
	Top Level Acceleration Structure: 
		Maximum [B]: 64000
		Minimum [B]: 64000
		Currently Allocated [B]: 64000
		Total Allocated [B]: 64000
		Total Created [num]: 1
		Average Allocated [B]: 64000
		Maximum Instances [num]: 1000
		Minimum Instances [num]: 1000
		Total Instances [num]: 1000
	Scratch Buffer: 
		Maximum [B]: 79360
		Minimum [B]: 79360
		Currently Allocated [B]: 79360
		Total Allocated [B]: 79360
		Total Created [num]: 1
		Average Allocated [B]: 79360
ProfilingStack: 
[PROF] Profiling statistics: 
	Scopes entered : 2256708
	Scopes exited : 2256707
	Overhead per scope [ticks] : 1018.7
	Threads entered : 3
	Threads exited : 0
	============================
		         Sum[Mt]   Parent[%]  Samples[num]       Avg[Mt]   Last/Curr[Mt]        Self[Mt]     Self[%] 	 Name

	main:

		   21635.054 Mt   100.000 %         1 x    21635.056 Mt    21635.054 Mt        0.766 Mt     0.004 % 	 /

		   21634.332 Mt    99.997 %         1 x    21634.334 Mt    21634.332 Mt     5510.344 Mt    25.470 % 	 ./Main application loop

		       2.909 Mt     0.013 %         1 x        2.909 Mt        0.000 Mt        2.909 Mt   100.000 % 	 ../TexturedRLInitialization

		       6.219 Mt     0.029 %         1 x        6.219 Mt        0.000 Mt        6.219 Mt   100.000 % 	 ../DeferredRLInitialization

		      64.714 Mt     0.299 %         1 x       64.714 Mt        0.000 Mt        1.874 Mt     2.896 % 	 ../RayTracingRLInitialization

		      62.839 Mt    97.104 %         1 x       62.839 Mt        0.000 Mt       62.839 Mt   100.000 % 	 .../PipelineBuild

		      11.943 Mt     0.055 %         1 x       11.943 Mt        0.000 Mt       11.943 Mt   100.000 % 	 ../ResolveRLInitialization

		     167.785 Mt     0.776 %         1 x      167.785 Mt        0.000 Mt        4.016 Mt     2.394 % 	 ../Initialization

		     163.769 Mt    97.606 %         1 x      163.769 Mt        0.000 Mt        2.423 Mt     1.480 % 	 .../ScenePrep

		     123.567 Mt    75.452 %         1 x      123.567 Mt        0.000 Mt       18.020 Mt    14.583 % 	 ..../SceneLoad

		      79.277 Mt    64.157 %         1 x       79.277 Mt        0.000 Mt       31.680 Mt    39.961 % 	 ...../glTF-LoadScene

		      47.597 Mt    60.039 %         2 x       23.799 Mt        0.000 Mt       47.597 Mt   100.000 % 	 ....../glTF-LoadImageData

		      13.073 Mt    10.580 %         1 x       13.073 Mt        0.000 Mt       13.073 Mt   100.000 % 	 ...../glTF-CreateScene

		      13.197 Mt    10.680 %         1 x       13.197 Mt        0.000 Mt       13.197 Mt   100.000 % 	 ...../SceneDesc-CreateScene

		       4.167 Mt     2.544 %         1 x        4.167 Mt        0.000 Mt        0.015 Mt     0.367 % 	 ..../TexturedRLPrep

		       4.152 Mt    99.633 %         1 x        4.152 Mt        0.000 Mt        1.497 Mt    36.052 % 	 ...../PrepareForRendering

		       1.750 Mt    42.161 %         1 x        1.750 Mt        0.000 Mt        1.750 Mt   100.000 % 	 ....../PrepareMaterials

		       0.904 Mt    21.787 %         1 x        0.904 Mt        0.000 Mt        0.904 Mt   100.000 % 	 ....../PrepareDrawBundle

		       4.836 Mt     2.953 %         1 x        4.836 Mt        0.000 Mt        0.014 Mt     0.287 % 	 ..../DeferredRLPrep

		       4.822 Mt    99.713 %         1 x        4.822 Mt        0.000 Mt        2.981 Mt    61.826 % 	 ...../PrepareForRendering

		       0.049 Mt     1.018 %         1 x        0.049 Mt        0.000 Mt        0.049 Mt   100.000 % 	 ....../PrepareMaterials

		       0.962 Mt    19.942 %         1 x        0.962 Mt        0.000 Mt        0.962 Mt   100.000 % 	 ....../BuildMaterialCache

		       0.830 Mt    17.213 %         1 x        0.830 Mt        0.000 Mt        0.830 Mt   100.000 % 	 ....../PrepareDrawBundle

		      19.077 Mt    11.649 %         1 x       19.077 Mt        0.000 Mt        7.090 Mt    37.165 % 	 ..../RayTracingRLPrep

		       0.688 Mt     3.606 %         1 x        0.688 Mt        0.000 Mt        0.688 Mt   100.000 % 	 ...../PrepareAccelerationStructures

		       0.068 Mt     0.357 %         1 x        0.068 Mt        0.000 Mt        0.068 Mt   100.000 % 	 ...../PrepareCache

		       1.034 Mt     5.422 %         1 x        1.034 Mt        0.000 Mt        1.034 Mt   100.000 % 	 ...../BuildCache

		       7.843 Mt    41.114 %         1 x        7.843 Mt        0.000 Mt        0.015 Mt     0.191 % 	 ...../BuildAccelerationStructures

		       7.828 Mt    99.809 %         1 x        7.828 Mt        0.000 Mt        5.015 Mt    64.065 % 	 ....../AccelerationStructureBuild

		       1.537 Mt    19.637 %         1 x        1.537 Mt        0.000 Mt        1.537 Mt   100.000 % 	 ......./BuildBottomLevelAS

		       1.276 Mt    16.297 %         1 x        1.276 Mt        0.000 Mt        1.276 Mt   100.000 % 	 ......./BuildTopLevelAS

		       2.353 Mt    12.337 %         1 x        2.353 Mt        0.000 Mt        2.353 Mt   100.000 % 	 ...../BuildShaderTables

		       9.699 Mt     5.922 %         1 x        9.699 Mt        0.000 Mt        9.699 Mt   100.000 % 	 ..../GpuSidePrep

		   15870.419 Mt    73.358 %   2246787 x        0.007 Mt       12.979 Mt     4108.277 Mt    25.886 % 	 ../MainLoop

		    1816.163 Mt    11.444 %      1228 x        1.479 Mt        1.405 Mt     1815.268 Mt    99.951 % 	 .../Update

		       0.895 Mt     0.049 %         1 x        0.895 Mt        0.000 Mt        0.895 Mt   100.000 % 	 ..../GuiModelApply

		    9945.979 Mt    62.670 %       935 x       10.637 Mt       11.566 Mt     4397.275 Mt    44.212 % 	 .../Render

		    5548.704 Mt    55.788 %       935 x        5.934 Mt        8.851 Mt     2870.917 Mt    51.740 % 	 ..../RayTracing

		     135.784 Mt     2.447 %       935 x        0.145 Mt        0.004 Mt        3.098 Mt     2.282 % 	 ...../DeferredRLPrep

		     132.686 Mt    97.718 %        16 x        8.293 Mt        0.000 Mt        5.651 Mt     4.259 % 	 ....../PrepareForRendering

		      13.239 Mt     9.977 %        16 x        0.827 Mt        0.000 Mt       13.239 Mt   100.000 % 	 ......./PrepareMaterials

		       0.012 Mt     0.009 %        16 x        0.001 Mt        0.000 Mt        0.012 Mt   100.000 % 	 ......./BuildMaterialCache

		     113.785 Mt    85.755 %        16 x        7.112 Mt        0.000 Mt      113.785 Mt   100.000 % 	 ......./PrepareDrawBundle

		    2542.003 Mt    45.813 %       935 x        2.719 Mt        0.009 Mt       30.914 Mt     1.216 % 	 ...../RayTracingRLPrep

		    1147.837 Mt    45.155 %        16 x       71.740 Mt        0.000 Mt     1147.837 Mt   100.000 % 	 ....../PrepareAccelerationStructures

		      13.204 Mt     0.519 %        16 x        0.825 Mt        0.000 Mt       13.204 Mt   100.000 % 	 ....../PrepareCache

		       0.011 Mt     0.000 %        16 x        0.001 Mt        0.000 Mt        0.011 Mt   100.000 % 	 ....../BuildCache

		    1234.848 Mt    48.578 %        16 x       77.178 Mt        0.000 Mt        0.038 Mt     0.003 % 	 ....../BuildAccelerationStructures

		    1234.810 Mt    99.997 %        16 x       77.176 Mt        0.000 Mt       57.235 Mt     4.635 % 	 ......./AccelerationStructureBuild

		    1165.147 Mt    94.358 %        16 x       72.822 Mt        0.000 Mt     1165.147 Mt   100.000 % 	 ......../BuildBottomLevelAS

		      12.428 Mt     1.006 %        16 x        0.777 Mt        0.000 Mt       12.428 Mt   100.000 % 	 ......../BuildTopLevelAS

		     115.188 Mt     4.531 %        16 x        7.199 Mt        0.000 Mt      115.188 Mt   100.000 % 	 ....../BuildShaderTables

	WindowThread:

		   21631.364 Mt   100.000 %         1 x    21631.365 Mt    21631.364 Mt    21631.364 Mt   100.000 % 	 /

	GpuThread:

		    3068.710 Mt   100.000 %       935 x        3.282 Mt        1.060 Mt      105.626 Mt     3.442 % 	 /

		       0.222 Mt     0.007 %         1 x        0.222 Mt        0.000 Mt        0.007 Mt     3.314 % 	 ./ScenePrep

		       0.215 Mt    96.686 %         1 x        0.215 Mt        0.000 Mt        0.001 Mt     0.671 % 	 ../AccelerationStructureBuild

		       0.129 Mt    60.066 %         1 x        0.129 Mt        0.000 Mt        0.129 Mt   100.000 % 	 .../BuildBottomLevelASGpu

		       0.084 Mt    39.264 %         1 x        0.084 Mt        0.000 Mt        0.084 Mt   100.000 % 	 .../BuildTopLevelASGpu

		    2961.698 Mt    96.513 %       935 x        3.168 Mt        1.059 Mt        5.208 Mt     0.176 % 	 ./RayTracingGpu

		      96.396 Mt     3.255 %       935 x        0.103 Mt        0.046 Mt       96.396 Mt   100.000 % 	 ../DeferredRLGpu

		     500.276 Mt    16.892 %       935 x        0.535 Mt        0.212 Mt      500.276 Mt   100.000 % 	 ../RayTracingRLGpu

		    1397.869 Mt    47.198 %       935 x        1.495 Mt        0.797 Mt     1397.869 Mt   100.000 % 	 ../ResolveRLGpu

		     961.949 Mt    32.480 %        16 x       60.122 Mt        0.000 Mt        0.036 Mt     0.004 % 	 ../AccelerationStructureBuild

		     956.629 Mt    99.447 %        16 x       59.789 Mt        0.000 Mt      956.629 Mt   100.000 % 	 .../BuildBottomLevelASGpu

		       5.285 Mt     0.549 %        16 x        0.330 Mt        0.000 Mt        5.285 Mt   100.000 % 	 .../BuildTopLevelASGpu

		       1.163 Mt     0.038 %       935 x        0.001 Mt        0.001 Mt        1.163 Mt   100.000 % 	 ./Present


	============================


