Configuration: 
Command line parameters  = E:\programming\FIT-DIP\Masters-DXR-Main-git\build\Measurement\x64\Debug\Measurement.exe --scene Cube/Cube.gltf --profile-track Cube/Cube.trk --width 1024 --height 768 --profile-output TrackCube768Ra 
Window width             = 1024
Window height            = 768
Window title             = Direct3D App
Back-buffer count        = 3
Fill Triangles           = enabled
Triangles Font Clock-Wise= enabled
Fast Build AS            = disabled
Target FPS               = 0
Target UPS               = 60
Tearing                  = enabled
VSync                    = disabled
GUI rendering            = disabled
Debug layer              = enabled
GPU validation           = disabled
GPU profiling            = enabled
Render mode              = Rasterization
Loaded scene file        = Cube/Cube.gltf
Using testing scene      = disabled
Duplication              = 1
Duplication in cube      = disabled
Duplication offset       = 10
BLAS duplication         = disabled
Rays per pixel           = 10

ProfilingAggregator: 
Render	,	2.2409	,	0.7082	,	0.7143	,	1.279	,	0.6745	,	0.9322	,	1.2179	,	1.3262	,	1.9974	,	1.9031	,	1.6581	,	0.8972	,	1.026	,	1.4534	,	1.3543	,	0.8565	,	0.8597	,	1.4243	,	1.0426	,	1.655	,	1.364	,	0.7523	,	0.8122	,	0.7976	,	0.7851	,	0.7411	,	0.7716	,	0.7651	,	0.8044	,	0.7745
Update	,	0.2617	,	0.2834	,	0.2889	,	0.2685	,	0.2747	,	0.2736	,	0.2683	,	0.2679	,	0.2766	,	0.5486	,	0.4974	,	0.853	,	0.2726	,	0.2945	,	0.2776	,	0.2885	,	0.2818	,	0.4931	,	0.5302	,	0.4545	,	0.2799	,	0.2765	,	0.2947	,	0.2826	,	0.4887	,	0.277	,	0.2676	,	0.4582	,	0.2663	,	0.2725
Textured	,	0.8757	,	0.2445	,	0.245	,	0.6699	,	0.2421	,	0.3353	,	0.6265	,	0.5851	,	0.6122	,	0.511	,	0.5625	,	0.2578	,	0.2734	,	0.5159	,	0.4694	,	0.2402	,	0.2423	,	0.492	,	0.2908	,	0.7529	,	0.474	,	0.2401	,	0.2422	,	0.2439	,	0.2432	,	0.2432	,	0.2423	,	0.2439	,	0.2393	,	0.2399
TexturedGpu	,	0.006688	,	0.017888	,	0.023584	,	0.025152	,	0.032512	,	0.05216	,	0.049888	,	0.056544	,	0.152704	,	0.148608	,	0.164768	,	0.138208	,	0.115744	,	0.090176	,	0.119616	,	0.12976	,	0.128192	,	0.13024	,	0.127872	,	0.138624	,	0.106304	,	0.022048	,	0.02832	,	0.02864	,	0.012352	,	0.021344	,	0.020704	,	0.009984	,	0.020992	,	0.021216
TexturedRLGpu	,	0.00592	,	0.0168	,	0.022528	,	0.024096	,	0.03072	,	0.049856	,	0.048064	,	0.054752	,	0.10736	,	0.090112	,	0.162496	,	0.135936	,	0.087808	,	0.08256	,	0.089632	,	0.104352	,	0.105568	,	0.12384	,	0.126016	,	0.136832	,	0.102656	,	0.020256	,	0.026528	,	0.026848	,	0.01056	,	0.018176	,	0.0184	,	0.00704	,	0.018176	,	0.018528
Index	,	0	,	1	,	2	,	3	,	4	,	5	,	6	,	7	,	8	,	9	,	10	,	11	,	12	,	13	,	14	,	15	,	16	,	17	,	18	,	19	,	20	,	21	,	22	,	23	,	24	,	25	,	26	,	27	,	28	,	29

ProfilingBuildAggregator: 
BuildBottomLevelAS	,	0.9558
BuildBottomLevelASGpu	,	0.087936
BuildTopLevelAS	,	0.9958
BuildTopLevelASGpu	,	0.059872
Duplication	,	1
Triangles	,	12
Meshes	,	1
BottomLevels	,	1
Index	,	0

FpsAggregator: 
FPS	,	1118	,	1226	,	1293	,	1302	,	1257	,	1189	,	1179	,	1181	,	949	,	763	,	764	,	760	,	782	,	817	,	773	,	805	,	763	,	777	,	752	,	790	,	765	,	921	,	934	,	958	,	917	,	953	,	974	,	967	,	933	,	971
UPS	,	59	,	61	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60
FrameTime	,	0.896366	,	0.816114	,	0.773555	,	0.768536	,	0.796058	,	0.841109	,	0.84884	,	0.847247	,	1.0539	,	1.31122	,	1.31108	,	1.31644	,	1.2801	,	1.22578	,	1.29384	,	1.24259	,	1.31099	,	1.28759	,	1.33064	,	1.2662	,	1.30775	,	1.08607	,	1.07129	,	1.04437	,	1.09052	,	1.04939	,	1.02723	,	1.03454	,	1.072	,	1.0303
GigaRays/s	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0
Index	,	0	,	1	,	2	,	3	,	4	,	5	,	6	,	7	,	8	,	9	,	10	,	11	,	12	,	13	,	14	,	15	,	16	,	17	,	18	,	19	,	20	,	21	,	22	,	23	,	24	,	25	,	26	,	27	,	28	,	29


AccelerationStructure: 
Ray tracing acceleration structure building statistics: 
	Bottom Level Acceleration Structure: 
		Maximum [B]: 5168
		Minimum [B]: 5168
		Currently Allocated [B]: 5168
		Currently Created [num]: 1
		Current Average [B]: 5168
		Maximum Triangles [num]: 12
		Minimum Triangles [num]: 12
		Total Triangles [num]: 12
		Maximum Meshes [num]: 1
		Minimum Meshes [num]: 1
		Total Meshes [num]: 1
	Top Level Acceleration Structure: 
		Maximum [B]: 64
		Minimum [B]: 64
		Currently Allocated [B]: 64
		Total Allocated [B]: 64
		Total Created [num]: 1
		Average Allocated [B]: 64
		Maximum Instances [num]: 1
		Minimum Instances [num]: 1
		Total Instances [num]: 1
	Scratch Buffer: 
		Maximum [B]: 3584
		Minimum [B]: 3584
		Currently Allocated [B]: 3584
		Total Allocated [B]: 3584
		Total Created [num]: 1
		Average Allocated [B]: 3584
ProfilingStack: 
[PROF] Profiling statistics: 
	Scopes entered : 202712
	Scopes exited : 202711
	Overhead per scope [ticks] : 1029.7
	Threads entered : 3
	Threads exited : 0
	============================
		         Sum[Mt]   Parent[%]  Samples[num]       Avg[Mt]   Last/Curr[Mt]        Self[Mt]     Self[%] 	 Name

	main:

		   30536.665 Mt   100.000 %         1 x    30536.669 Mt    30536.663 Mt        0.711 Mt     0.002 % 	 /

		   30536.098 Mt    99.998 %         1 x    30536.104 Mt    30536.097 Mt      427.276 Mt     1.399 % 	 ./Main application loop

		       3.260 Mt     0.011 %         1 x        3.260 Mt        0.000 Mt        3.260 Mt   100.000 % 	 ../TexturedRLInitialization

		       7.131 Mt     0.023 %         1 x        7.131 Mt        0.000 Mt        7.131 Mt   100.000 % 	 ../DeferredRLInitialization

		      53.347 Mt     0.175 %         1 x       53.347 Mt        0.000 Mt        2.031 Mt     3.807 % 	 ../RayTracingRLInitialization

		      51.316 Mt    96.193 %         1 x       51.316 Mt        0.000 Mt       51.316 Mt   100.000 % 	 .../PipelineBuild

		      12.259 Mt     0.040 %         1 x       12.259 Mt        0.000 Mt       12.259 Mt   100.000 % 	 ../ResolveRLInitialization

		     112.311 Mt     0.368 %         1 x      112.311 Mt        0.000 Mt        2.334 Mt     2.078 % 	 ../Initialization

		     109.977 Mt    97.922 %         1 x      109.977 Mt        0.000 Mt        1.160 Mt     1.055 % 	 .../ScenePrep

		      77.277 Mt    70.266 %         1 x       77.277 Mt        0.000 Mt       12.275 Mt    15.884 % 	 ..../SceneLoad

		      57.316 Mt    74.170 %         1 x       57.316 Mt        0.000 Mt       12.482 Mt    21.778 % 	 ...../glTF-LoadScene

		      44.834 Mt    78.222 %         2 x       22.417 Mt        0.000 Mt       44.834 Mt   100.000 % 	 ....../glTF-LoadImageData

		       4.149 Mt     5.369 %         1 x        4.149 Mt        0.000 Mt        4.149 Mt   100.000 % 	 ...../glTF-CreateScene

		       3.537 Mt     4.577 %         1 x        3.537 Mt        0.000 Mt        3.537 Mt   100.000 % 	 ...../SceneDesc-CreateScene

		       2.169 Mt     1.972 %         1 x        2.169 Mt        0.000 Mt        0.006 Mt     0.258 % 	 ..../TexturedRLPrep

		       2.163 Mt    99.742 %         1 x        2.163 Mt        0.000 Mt        0.580 Mt    26.827 % 	 ...../PrepareForRendering

		       0.675 Mt    31.214 %         1 x        0.675 Mt        0.000 Mt        0.675 Mt   100.000 % 	 ....../PrepareMaterials

		       0.908 Mt    41.958 %         1 x        0.908 Mt        0.000 Mt        0.908 Mt   100.000 % 	 ....../PrepareDrawBundle

		       3.491 Mt     3.174 %         1 x        3.491 Mt        0.000 Mt        0.014 Mt     0.415 % 	 ..../DeferredRLPrep

		       3.477 Mt    99.585 %         1 x        3.477 Mt        0.000 Mt        1.729 Mt    49.728 % 	 ...../PrepareForRendering

		       0.052 Mt     1.484 %         1 x        0.052 Mt        0.000 Mt        0.052 Mt   100.000 % 	 ....../PrepareMaterials

		       1.438 Mt    41.363 %         1 x        1.438 Mt        0.000 Mt        1.438 Mt   100.000 % 	 ....../BuildMaterialCache

		       0.258 Mt     7.424 %         1 x        0.258 Mt        0.000 Mt        0.258 Mt   100.000 % 	 ....../PrepareDrawBundle

		      15.384 Mt    13.989 %         1 x       15.384 Mt        0.000 Mt        4.101 Mt    26.658 % 	 ..../RayTracingRLPrep

		       0.239 Mt     1.557 %         1 x        0.239 Mt        0.000 Mt        0.239 Mt   100.000 % 	 ...../PrepareAccelerationStructures

		       0.024 Mt     0.156 %         1 x        0.024 Mt        0.000 Mt        0.024 Mt   100.000 % 	 ...../PrepareCache

		       0.467 Mt     3.033 %         1 x        0.467 Mt        0.000 Mt        0.467 Mt   100.000 % 	 ...../BuildCache

		       8.657 Mt    56.270 %         1 x        8.657 Mt        0.000 Mt        0.006 Mt     0.069 % 	 ...../BuildAccelerationStructures

		       8.651 Mt    99.931 %         1 x        8.651 Mt        0.000 Mt        6.699 Mt    77.440 % 	 ....../AccelerationStructureBuild

		       0.956 Mt    11.049 %         1 x        0.956 Mt        0.000 Mt        0.956 Mt   100.000 % 	 ......./BuildBottomLevelAS

		       0.996 Mt    11.511 %         1 x        0.996 Mt        0.000 Mt        0.996 Mt   100.000 % 	 ......./BuildTopLevelAS

		       1.896 Mt    12.327 %         1 x        1.896 Mt        0.000 Mt        1.896 Mt   100.000 % 	 ...../BuildShaderTables

		      10.496 Mt     9.544 %         1 x       10.496 Mt        0.000 Mt       10.496 Mt   100.000 % 	 ..../GpuSidePrep

		   29920.513 Mt    97.984 %     29663 x        1.009 Mt        1.470 Mt      411.935 Mt     1.377 % 	 ../MainLoop

		     767.058 Mt     2.564 %      1801 x        0.426 Mt        0.299 Mt      764.726 Mt    99.696 % 	 .../Update

		       2.331 Mt     0.304 %         1 x        2.331 Mt        0.000 Mt        2.331 Mt   100.000 % 	 ..../GuiModelApply

		   28741.520 Mt    96.060 %     28535 x        1.007 Mt        1.162 Mt    19846.826 Mt    69.053 % 	 .../Render

		    8894.695 Mt    30.947 %     28535 x        0.312 Mt        0.279 Mt     8781.130 Mt    98.723 % 	 ..../Textured

		     113.565 Mt     1.277 %     28535 x        0.004 Mt        0.003 Mt      113.565 Mt   100.000 % 	 ...../TexturedRLPrep

	WindowThread:

		   30535.875 Mt   100.000 %         1 x    30535.876 Mt    30535.874 Mt    30535.875 Mt   100.000 % 	 /

	GpuThread:

		    2290.879 Mt   100.000 %     28535 x        0.080 Mt        0.055 Mt      158.867 Mt     6.935 % 	 /

		       0.154 Mt     0.007 %         1 x        0.154 Mt        0.000 Mt        0.006 Mt     3.813 % 	 ./ScenePrep

		       0.149 Mt    96.187 %         1 x        0.149 Mt        0.000 Mt        0.001 Mt     0.495 % 	 ../AccelerationStructureBuild

		       0.088 Mt    59.199 %         1 x        0.088 Mt        0.000 Mt        0.088 Mt   100.000 % 	 .../BuildBottomLevelASGpu

		       0.060 Mt    40.306 %         1 x        0.060 Mt        0.000 Mt        0.060 Mt   100.000 % 	 .../BuildTopLevelASGpu

		    1908.723 Mt    83.318 %     28535 x        0.067 Mt        0.050 Mt      316.448 Mt    16.579 % 	 ./TexturedGpu

		    1592.275 Mt    83.421 %     28535 x        0.056 Mt        0.001 Mt     1592.275 Mt   100.000 % 	 ../TexturedRLGpu

		     223.135 Mt     9.740 %     28535 x        0.008 Mt        0.004 Mt      223.135 Mt   100.000 % 	 ./Present


	============================


