Configuration: 
Command line parameters  = E:\programming\FIT-DIP\Masters-DXR-Main-git\build\Measurement\x64\Debug\Measurement.exe --scene Suzanne/Suzanne.gltf --duplication-cube --profile-duplication 10 --rt-mode --width 1024 --height 768 --profile-output DuplicationSuzanne768Rf --fast-build-as 
Window width             = 1024
Window height            = 768
Window title             = Direct3D App
Back-buffer count        = 3
Fill Triangles           = enabled
Triangles Font Clock-Wise= enabled
Fast Build AS            = enabled
Target FPS               = 60
Target UPS               = 60
Tearing                  = disabled
VSync                    = disabled
GUI rendering            = disabled
Debug layer              = enabled
GPU validation           = disabled
GPU profiling            = enabled
Render mode              = Ray Tracing
Loaded scene file        = Suzanne/Suzanne.gltf
Using testing scene      = disabled
Duplication              = 10
Duplication in cube      = enabled
Duplication offset       = 10
BLAS duplication         = enabled
Rays per pixel           = 10

ProfilingAggregator: 
Render	,	3.3557	,	5.2697	,	8.8261	,	7.9673	,	8.3162	,	8.0651	,	8.1143	,	11.9244	,	4.3853	,	4.3343	,	4.973	,	5.0925	,	6.0133	,	6.0204	,	7.4357	,	7.1728	,	10.4827	,	9.1194	,	1140.07	,	10.4841
Update	,	0.2505	,	0.2346	,	0.8283	,	0.5059	,	0.4967	,	0.2623	,	0.5808	,	0.6739	,	0.6966	,	0.3725	,	0.4626	,	0.4741	,	0.61	,	0.6342	,	2.7854	,	0.7792	,	0.9465	,	1.1234	,	1.2245	,	1.3248
RayTracing	,	1.2722	,	1.3016	,	1.3172	,	1.4807	,	1.6259	,	1.4261	,	1.7233	,	3.219	,	2.0464	,	2.0813	,	2.5962	,	2.6462	,	3.4784	,	3.4671	,	4.6221	,	4.625	,	6.4164	,	6.4379	,	676.317	,	7.7644
RayTracingGpu	,	0.917088	,	2.58102	,	5.6849	,	4.43411	,	4.69072	,	4.65699	,	4.40304	,	6.63613	,	1.08419	,	1.08214	,	1.1912	,	1.20058	,	1.28141	,	1.2576	,	1.52893	,	1.25277	,	1.2505	,	1.24426	,	459.678	,	1.46573
DeferredRLGpu	,	0.014368	,	0.049856	,	0.13536	,	0.128832	,	0.234368	,	0.269216	,	0.277184	,	0.334112	,	0.11296	,	0.113312	,	0.193728	,	0.193312	,	0.289152	,	0.289184	,	0.456928	,	0.372512	,	0.449056	,	0.443872	,	2.03082	,	0.615552
RayTracingRLGpu	,	0.095968	,	0.442048	,	1.68797	,	1.35654	,	1.68432	,	1.70272	,	1.65469	,	2.83869	,	0.174432	,	0.172288	,	0.203328	,	0.213856	,	0.240352	,	0.219136	,	0.27632	,	0.231648	,	0.243584	,	0.245152	,	0.325888	,	0.292288
ResolveRLGpu	,	0.805216	,	2.08502	,	3.85702	,	2.94323	,	2.76464	,	2.68144	,	2.46707	,	3.45555	,	0.795072	,	0.795008	,	0.792128	,	0.791232	,	0.74848	,	0.747776	,	0.793312	,	0.646688	,	0.555904	,	0.553568	,	0.67456	,	0.556224
Index	,	0	,	1	,	2	,	3	,	4	,	5	,	6	,	7	,	8	,	9	,	10	,	11	,	12	,	13	,	14	,	15	,	16	,	17	,	18	,	19

ProfilingBuildAggregator: 
BuildBottomLevelAS	,	0.917	,	3.9816	,	13.3077	,	27.9709	,	59.4234	,	77.0461	,	118.944	,	173.422	,	226.776	,	328.997
BuildBottomLevelASGpu	,	0.354368	,	7.13709	,	34.2535	,	67.718	,	119.713	,	201.78	,	284.374	,	323.585	,	237.546	,	456.407
BuildTopLevelAS	,	0.9656	,	1.2263	,	1.1596	,	1.1735	,	1.2166	,	1.093	,	1.2908	,	1.0453	,	1.1348	,	1.0656
BuildTopLevelASGpu	,	0.083104	,	0.246688	,	0.491456	,	0.442272	,	0.560384	,	0.506112	,	0.194912	,	0.17904	,	0.224192	,	0.220064
Duplication	,	1	,	2	,	3	,	4	,	5	,	6	,	7	,	8	,	9	,	10
Triangles	,	3936	,	31488	,	106272	,	251904	,	492000	,	850176	,	1350048	,	2015232	,	2869344	,	3936000
Meshes	,	1	,	8	,	27	,	64	,	125	,	216	,	343	,	512	,	729	,	1000
BottomLevels	,	1	,	8	,	27	,	64	,	125	,	216	,	343	,	512	,	729	,	1000
Index	,	0	,	1	,	2	,	3	,	4	,	5	,	6	,	7	,	8	,	9

FpsAggregator: 
FPS	,	55	,	59	,	57	,	59	,	50	,	59	,	51	,	60	,	43	,	59	,	34	,	59	,	20	,	60	,	4	,	59	,	3	,	58	,	1	,	51
UPS	,	59	,	61	,	59	,	61	,	59	,	61	,	60	,	60	,	61	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	58	,	66	,	10	,	138
FrameTime	,	18.1818	,	16.9532	,	17.5593	,	16.9493	,	20.1375	,	16.9492	,	19.608	,	16.8433	,	23.2559	,	16.9492	,	29.4118	,	16.9492	,	50.0001	,	16.7652	,	250.001	,	16.9492	,	357.992	,	17.2922	,	1467.86	,	19.6497
GigaRays/s	,	0.432537	,	0.463884	,	0.447872	,	0.463991	,	0.390532	,	0.463994	,	0.401078	,	0.46691	,	0.338165	,	0.463995	,	0.267386	,	0.463993	,	0.157286	,	0.469085	,	0.0314572	,	0.463993	,	0.0219679	,	0.45479	,	0.00535769	,	0.400226
Index	,	0	,	1	,	2	,	3	,	4	,	5	,	6	,	7	,	8	,	9	,	10	,	11	,	12	,	13	,	14	,	15	,	16	,	17	,	18	,	19


AccelerationStructure: 
Ray tracing acceleration structure building statistics: 
	Bottom Level Acceleration Structure: 
		Maximum [B]: 224128
		Minimum [B]: 224128
		Currently Allocated [B]: 224128000
		Currently Created [num]: 1000
		Current Average [B]: 224128
		Maximum Triangles [num]: 3936
		Minimum Triangles [num]: 3936
		Total Triangles [num]: 3936000
		Maximum Meshes [num]: 1
		Minimum Meshes [num]: 1
		Total Meshes [num]: 1000
	Top Level Acceleration Structure: 
		Maximum [B]: 64000
		Minimum [B]: 64000
		Currently Allocated [B]: 64000
		Total Allocated [B]: 64000
		Total Created [num]: 1
		Average Allocated [B]: 64000
		Maximum Instances [num]: 1000
		Minimum Instances [num]: 1000
		Total Instances [num]: 1000
	Scratch Buffer: 
		Maximum [B]: 79360
		Minimum [B]: 79360
		Currently Allocated [B]: 79360
		Total Allocated [B]: 79360
		Total Created [num]: 1
		Average Allocated [B]: 79360
ProfilingStack: 
[PROF] Profiling statistics: 
	Scopes entered : 1898272
	Scopes exited : 1898271
	Overhead per scope [ticks] : 1035.4
	Threads entered : 3
	Threads exited : 0
	============================
		         Sum[Mt]   Parent[%]  Samples[num]       Avg[Mt]   Last/Curr[Mt]        Self[Mt]     Self[%] 	 Name

	main:

		   21699.109 Mt   100.000 %         1 x    21699.113 Mt    21699.108 Mt        0.853 Mt     0.004 % 	 /

		   21698.399 Mt    99.997 %         1 x    21698.404 Mt    21698.398 Mt     5051.006 Mt    23.278 % 	 ./Main application loop

		       2.769 Mt     0.013 %         1 x        2.769 Mt        0.000 Mt        2.769 Mt   100.000 % 	 ../TexturedRLInitialization

		       6.460 Mt     0.030 %         1 x        6.460 Mt        0.000 Mt        6.460 Mt   100.000 % 	 ../DeferredRLInitialization

		      48.457 Mt     0.223 %         1 x       48.457 Mt        0.000 Mt        2.675 Mt     5.520 % 	 ../RayTracingRLInitialization

		      45.782 Mt    94.480 %         1 x       45.782 Mt        0.000 Mt       45.782 Mt   100.000 % 	 .../PipelineBuild

		      18.191 Mt     0.084 %         1 x       18.191 Mt        0.000 Mt       18.191 Mt   100.000 % 	 ../ResolveRLInitialization

		     230.634 Mt     1.063 %         1 x      230.634 Mt        0.000 Mt        2.431 Mt     1.054 % 	 ../Initialization

		     228.203 Mt    98.946 %         1 x      228.203 Mt        0.000 Mt        1.258 Mt     0.551 % 	 .../ScenePrep

		     194.773 Mt    85.351 %         1 x      194.773 Mt        0.000 Mt       18.568 Mt     9.533 % 	 ..../SceneLoad

		     164.713 Mt    84.567 %         1 x      164.713 Mt        0.000 Mt       19.517 Mt    11.849 % 	 ...../glTF-LoadScene

		     145.196 Mt    88.151 %         2 x       72.598 Mt        0.000 Mt      145.196 Mt   100.000 % 	 ....../glTF-LoadImageData

		       7.699 Mt     3.953 %         1 x        7.699 Mt        0.000 Mt        7.699 Mt   100.000 % 	 ...../glTF-CreateScene

		       3.793 Mt     1.947 %         1 x        3.793 Mt        0.000 Mt        3.793 Mt   100.000 % 	 ...../SceneDesc-CreateScene

		       2.290 Mt     1.004 %         1 x        2.290 Mt        0.000 Mt        0.006 Mt     0.258 % 	 ..../TexturedRLPrep

		       2.284 Mt    99.742 %         1 x        2.284 Mt        0.000 Mt        0.411 Mt    18.015 % 	 ...../PrepareForRendering

		       0.611 Mt    26.740 %         1 x        0.611 Mt        0.000 Mt        0.611 Mt   100.000 % 	 ....../PrepareMaterials

		       1.262 Mt    55.245 %         1 x        1.262 Mt        0.000 Mt        1.262 Mt   100.000 % 	 ....../PrepareDrawBundle

		       2.832 Mt     1.241 %         1 x        2.832 Mt        0.000 Mt        0.014 Mt     0.498 % 	 ..../DeferredRLPrep

		       2.818 Mt    99.502 %         1 x        2.818 Mt        0.000 Mt        2.026 Mt    71.898 % 	 ...../PrepareForRendering

		       0.054 Mt     1.923 %         1 x        0.054 Mt        0.000 Mt        0.054 Mt   100.000 % 	 ....../PrepareMaterials

		       0.464 Mt    16.457 %         1 x        0.464 Mt        0.000 Mt        0.464 Mt   100.000 % 	 ....../BuildMaterialCache

		       0.274 Mt     9.722 %         1 x        0.274 Mt        0.000 Mt        0.274 Mt   100.000 % 	 ....../PrepareDrawBundle

		      15.478 Mt     6.782 %         1 x       15.478 Mt        0.000 Mt        4.040 Mt    26.101 % 	 ..../RayTracingRLPrep

		       0.247 Mt     1.598 %         1 x        0.247 Mt        0.000 Mt        0.247 Mt   100.000 % 	 ...../PrepareAccelerationStructures

		       0.025 Mt     0.158 %         1 x        0.025 Mt        0.000 Mt        0.025 Mt   100.000 % 	 ...../PrepareCache

		       0.473 Mt     3.054 %         1 x        0.473 Mt        0.000 Mt        0.473 Mt   100.000 % 	 ...../BuildCache

		       8.537 Mt    55.154 %         1 x        8.537 Mt        0.000 Mt        0.009 Mt     0.103 % 	 ...../BuildAccelerationStructures

		       8.528 Mt    99.897 %         1 x        8.528 Mt        0.000 Mt        6.645 Mt    77.924 % 	 ....../AccelerationStructureBuild

		       0.917 Mt    10.753 %         1 x        0.917 Mt        0.000 Mt        0.917 Mt   100.000 % 	 ......./BuildBottomLevelAS

		       0.966 Mt    11.323 %         1 x        0.966 Mt        0.000 Mt        0.966 Mt   100.000 % 	 ......./BuildTopLevelAS

		       2.157 Mt    13.934 %         1 x        2.157 Mt        0.000 Mt        2.157 Mt   100.000 % 	 ...../BuildShaderTables

		      11.572 Mt     5.071 %         1 x       11.572 Mt        0.000 Mt       11.572 Mt   100.000 % 	 ..../GpuSidePrep

		   16340.881 Mt    75.309 %   1888726 x        0.009 Mt       15.989 Mt     3710.459 Mt    22.707 % 	 ../MainLoop

		    2098.616 Mt    12.843 %      1234 x        1.701 Mt        4.737 Mt     2097.728 Mt    99.958 % 	 .../Update

		       0.888 Mt     0.042 %         1 x        0.888 Mt        0.000 Mt        0.888 Mt   100.000 % 	 ..../GuiModelApply

		   10531.806 Mt    64.451 %       901 x       11.689 Mt        0.000 Mt     5530.416 Mt    52.512 % 	 .../Render

		    5001.390 Mt    47.488 %       901 x        5.551 Mt        0.000 Mt     2778.153 Mt    55.548 % 	 ..../RayTracing

		      87.464 Mt     1.749 %       901 x        0.097 Mt        0.000 Mt        3.020 Mt     3.453 % 	 ...../DeferredRLPrep

		      84.445 Mt    96.547 %        11 x        7.677 Mt        0.000 Mt        5.951 Mt     7.048 % 	 ....../PrepareForRendering

		       8.300 Mt     9.828 %        11 x        0.755 Mt        0.000 Mt        8.300 Mt   100.000 % 	 ......./PrepareMaterials

		       0.010 Mt     0.011 %        11 x        0.001 Mt        0.000 Mt        0.010 Mt   100.000 % 	 ......./BuildMaterialCache

		      70.184 Mt    83.112 %        11 x        6.380 Mt        0.000 Mt       70.184 Mt   100.000 % 	 ......./PrepareDrawBundle

		    2135.772 Mt    42.704 %       901 x        2.370 Mt        0.000 Mt       33.150 Mt     1.552 % 	 ...../RayTracingRLPrep

		     880.277 Mt    41.216 %        11 x       80.025 Mt        0.000 Mt      880.277 Mt   100.000 % 	 ....../PrepareAccelerationStructures

		       8.425 Mt     0.394 %        11 x        0.766 Mt        0.000 Mt        8.425 Mt   100.000 % 	 ....../PrepareCache

		       0.011 Mt     0.001 %        11 x        0.001 Mt        0.000 Mt        0.011 Mt   100.000 % 	 ....../BuildCache

		    1117.477 Mt    52.322 %        11 x      101.589 Mt        0.000 Mt        0.041 Mt     0.004 % 	 ....../BuildAccelerationStructures

		    1117.436 Mt    99.996 %        11 x      101.585 Mt        0.000 Mt       59.471 Mt     5.322 % 	 ......./AccelerationStructureBuild

		    1045.359 Mt    93.550 %        11 x       95.033 Mt        0.000 Mt     1045.359 Mt   100.000 % 	 ......../BuildBottomLevelAS

		      12.607 Mt     1.128 %        11 x        1.146 Mt        0.000 Mt       12.607 Mt   100.000 % 	 ......../BuildTopLevelAS

		      96.432 Mt     4.515 %        11 x        8.767 Mt        0.000 Mt       96.432 Mt   100.000 % 	 ....../BuildShaderTables

	WindowThread:

		   21690.279 Mt   100.000 %         1 x    21690.280 Mt    21690.278 Mt    21690.279 Mt   100.000 % 	 /

	GpuThread:

		    4178.998 Mt   100.000 %       901 x        4.638 Mt        0.000 Mt      117.061 Mt     2.801 % 	 /

		       0.445 Mt     0.011 %         1 x        0.445 Mt        0.000 Mt        0.006 Mt     1.460 % 	 ./ScenePrep

		       0.439 Mt    98.540 %         1 x        0.439 Mt        0.000 Mt        0.001 Mt     0.241 % 	 ../AccelerationStructureBuild

		       0.354 Mt    80.809 %         1 x        0.354 Mt        0.000 Mt        0.354 Mt   100.000 % 	 .../BuildBottomLevelASGpu

		       0.083 Mt    18.951 %         1 x        0.083 Mt        0.000 Mt        0.083 Mt   100.000 % 	 .../BuildTopLevelASGpu

		    4060.452 Mt    97.163 %       901 x        4.507 Mt        0.000 Mt        4.581 Mt     0.113 % 	 ./RayTracingGpu

		     214.731 Mt     5.288 %       901 x        0.238 Mt        0.000 Mt      214.731 Mt   100.000 % 	 ../DeferredRLGpu

		     652.709 Mt    16.075 %       901 x        0.724 Mt        0.000 Mt      652.709 Mt   100.000 % 	 ../RayTracingRLGpu

		    1408.040 Mt    34.677 %       901 x        1.563 Mt        0.000 Mt     1408.040 Mt   100.000 % 	 ../ResolveRLGpu

		    1780.391 Mt    43.847 %        11 x      161.854 Mt        0.000 Mt        0.029 Mt     0.002 % 	 ../AccelerationStructureBuild

		    1776.583 Mt    99.786 %        11 x      161.508 Mt        0.000 Mt     1776.583 Mt   100.000 % 	 .../BuildBottomLevelASGpu

		       3.780 Mt     0.212 %        11 x        0.344 Mt        0.000 Mt        3.780 Mt   100.000 % 	 .../BuildTopLevelASGpu

		       1.041 Mt     0.025 %       901 x        0.001 Mt        0.000 Mt        1.041 Mt   100.000 % 	 ./Present


	============================


