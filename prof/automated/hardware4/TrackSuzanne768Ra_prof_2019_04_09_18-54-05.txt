Configuration: 
Command line parameters  = E:\programming\FIT-DIP\Masters-DXR-Main-git\build\Measurement\x64\Debug\Measurement.exe --scene Suzanne/Suzanne.gltf --profile-track Suzanne/Suzanne.trk --width 1024 --height 768 --profile-output TrackSuzanne768Ra 
Window width             = 1024
Window height            = 768
Window title             = Direct3D App
Back-buffer count        = 3
Fill Triangles           = enabled
Triangles Font Clock-Wise= enabled
Fast Build AS            = disabled
Target FPS               = 0
Target UPS               = 60
Tearing                  = enabled
VSync                    = disabled
GUI rendering            = disabled
Debug layer              = enabled
GPU validation           = disabled
GPU profiling            = enabled
Render mode              = Rasterization
Loaded scene file        = Suzanne/Suzanne.gltf
Using testing scene      = disabled
Duplication              = 1
Duplication in cube      = disabled
Duplication offset       = 10
BLAS duplication         = disabled
Rays per pixel           = 10

ProfilingAggregator: 
Render	,	0.6346	,	0.9869	,	0.6956	,	0.6727	,	0.8168	,	2.3413	,	0.8182	,	0.7905	,	1.2806	,	1.485	,	2.1472	,	1.1804	,	0.7892	,	0.8309	,	0.8039	,	0.8044	,	0.8208	,	0.831	,	1.1909	,	0.821	,	1.1701	,	0.8134	,	0.7893	,	0.7911
Update	,	0.2643	,	0.269	,	0.2636	,	0.2648	,	0.4816	,	0.6564	,	0.2731	,	0.2774	,	0.6173	,	0.2826	,	0.8516	,	0.4291	,	0.2812	,	0.2849	,	0.2775	,	0.2782	,	0.2814	,	0.2761	,	0.6204	,	0.3705	,	0.8435	,	0.3016	,	0.2775	,	0.5793
Textured	,	0.2478	,	0.3764	,	0.2435	,	0.2418	,	0.2516	,	0.8886	,	0.2464	,	0.2492	,	0.4508	,	0.7307	,	0.8591	,	0.4001	,	0.2419	,	0.261	,	0.2483	,	0.2442	,	0.2451	,	0.2445	,	0.4592	,	0.2518	,	0.3683	,	0.2471	,	0.2434	,	0.2454
TexturedGpu	,	0.00576	,	0.026336	,	0.038368	,	0.018432	,	0.054848	,	0.048736	,	0.051264	,	0.0392	,	0.048384	,	0.056864	,	0.060128	,	0.05328	,	0.042336	,	0.050752	,	0.051968	,	0.054432	,	0.057312	,	0.061408	,	0.104672	,	0.06208	,	0.10096	,	0.075584	,	0.05328	,	0.04
TexturedRLGpu	,	0.004992	,	0.023616	,	0.036576	,	0.017088	,	0.053056	,	0.046912	,	0.049856	,	0.036	,	0.04656	,	0.055104	,	0.058784	,	0.05056	,	0.040544	,	0.049408	,	0.050144	,	0.053056	,	0.055968	,	0.059584	,	0.1024	,	0.060288	,	0.099616	,	0.07424	,	0.051008	,	0.038208
Index	,	0	,	1	,	2	,	3	,	4	,	5	,	6	,	7	,	8	,	9	,	10	,	11	,	12	,	13	,	14	,	15	,	16	,	17	,	18	,	19	,	20	,	21	,	22	,	23

ProfilingBuildAggregator: 
BuildBottomLevelAS	,	1.0793
BuildBottomLevelASGpu	,	0.353696
BuildTopLevelAS	,	0.9769
BuildTopLevelASGpu	,	0.086144
Duplication	,	1
Triangles	,	3936
Meshes	,	1
BottomLevels	,	1
Index	,	0

FpsAggregator: 
FPS	,	1264	,	1302	,	1192	,	1216	,	1056	,	911	,	942	,	908	,	916	,	928	,	891	,	878	,	913	,	956	,	886	,	901	,	917	,	890	,	852	,	786	,	822	,	762	,	896	,	917
UPS	,	59	,	60	,	60	,	60	,	60	,	60	,	61	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60
FrameTime	,	0.791146	,	0.76831	,	0.838989	,	0.822545	,	0.947005	,	1.09852	,	1.06218	,	1.10203	,	1.09258	,	1.07874	,	1.12238	,	1.139	,	1.09565	,	1.04626	,	1.12943	,	1.11026	,	1.09137	,	1.12452	,	1.17512	,	1.27257	,	1.21684	,	1.3132	,	1.11612	,	1.09109
GigaRays/s	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0
Index	,	0	,	1	,	2	,	3	,	4	,	5	,	6	,	7	,	8	,	9	,	10	,	11	,	12	,	13	,	14	,	15	,	16	,	17	,	18	,	19	,	20	,	21	,	22	,	23


AccelerationStructure: 
Ray tracing acceleration structure building statistics: 
	Bottom Level Acceleration Structure: 
		Maximum [B]: 224128
		Minimum [B]: 224128
		Currently Allocated [B]: 224128
		Currently Created [num]: 1
		Current Average [B]: 224128
		Maximum Triangles [num]: 3936
		Minimum Triangles [num]: 3936
		Total Triangles [num]: 3936
		Maximum Meshes [num]: 1
		Minimum Meshes [num]: 1
		Total Meshes [num]: 1
	Top Level Acceleration Structure: 
		Maximum [B]: 64
		Minimum [B]: 64
		Currently Allocated [B]: 64
		Total Allocated [B]: 64
		Total Created [num]: 1
		Average Allocated [B]: 64
		Maximum Instances [num]: 1
		Minimum Instances [num]: 1
		Total Instances [num]: 1
	Scratch Buffer: 
		Maximum [B]: 34304
		Minimum [B]: 34304
		Currently Allocated [B]: 34304
		Total Allocated [B]: 34304
		Total Created [num]: 1
		Average Allocated [B]: 34304
ProfilingStack: 
[PROF] Profiling statistics: 
	Scopes entered : 163015
	Scopes exited : 163014
	Overhead per scope [ticks] : 1004.3
	Threads entered : 3
	Threads exited : 0
	============================
		         Sum[Mt]   Parent[%]  Samples[num]       Avg[Mt]   Last/Curr[Mt]        Self[Mt]     Self[%] 	 Name

	main:

		   24646.381 Mt   100.000 %         1 x    24646.383 Mt    24646.381 Mt        0.784 Mt     0.003 % 	 /

		   24645.640 Mt    99.997 %         1 x    24645.642 Mt    24645.640 Mt      410.119 Mt     1.664 % 	 ./Main application loop

		       2.925 Mt     0.012 %         1 x        2.925 Mt        0.000 Mt        2.925 Mt   100.000 % 	 ../TexturedRLInitialization

		       7.274 Mt     0.030 %         1 x        7.274 Mt        0.000 Mt        7.274 Mt   100.000 % 	 ../DeferredRLInitialization

		      46.967 Mt     0.191 %         1 x       46.967 Mt        0.000 Mt        2.175 Mt     4.630 % 	 ../RayTracingRLInitialization

		      44.792 Mt    95.370 %         1 x       44.792 Mt        0.000 Mt       44.792 Mt   100.000 % 	 .../PipelineBuild

		      12.187 Mt     0.049 %         1 x       12.187 Mt        0.000 Mt       12.187 Mt   100.000 % 	 ../ResolveRLInitialization

		     232.561 Mt     0.944 %         1 x      232.561 Mt        0.000 Mt        2.312 Mt     0.994 % 	 ../Initialization

		     230.249 Mt    99.006 %         1 x      230.249 Mt        0.000 Mt        1.257 Mt     0.546 % 	 .../ScenePrep

		     196.895 Mt    85.514 %         1 x      196.895 Mt        0.000 Mt       18.111 Mt     9.199 % 	 ..../SceneLoad

		     164.482 Mt    83.538 %         1 x      164.482 Mt        0.000 Mt       19.349 Mt    11.764 % 	 ...../glTF-LoadScene

		     145.133 Mt    88.236 %         2 x       72.566 Mt        0.000 Mt      145.133 Mt   100.000 % 	 ....../glTF-LoadImageData

		       7.915 Mt     4.020 %         1 x        7.915 Mt        0.000 Mt        7.915 Mt   100.000 % 	 ...../glTF-CreateScene

		       6.386 Mt     3.243 %         1 x        6.386 Mt        0.000 Mt        6.386 Mt   100.000 % 	 ...../SceneDesc-CreateScene

		       1.151 Mt     0.500 %         1 x        1.151 Mt        0.000 Mt        0.004 Mt     0.391 % 	 ..../TexturedRLPrep

		       1.147 Mt    99.609 %         1 x        1.147 Mt        0.000 Mt        0.380 Mt    33.104 % 	 ...../PrepareForRendering

		       0.479 Mt    41.752 %         1 x        0.479 Mt        0.000 Mt        0.479 Mt   100.000 % 	 ....../PrepareMaterials

		       0.288 Mt    25.144 %         1 x        0.288 Mt        0.000 Mt        0.288 Mt   100.000 % 	 ....../PrepareDrawBundle

		       3.208 Mt     1.393 %         1 x        3.208 Mt        0.000 Mt        0.004 Mt     0.134 % 	 ..../DeferredRLPrep

		       3.204 Mt    99.866 %         1 x        3.204 Mt        0.000 Mt        1.581 Mt    49.355 % 	 ...../PrepareForRendering

		       0.016 Mt     0.490 %         1 x        0.016 Mt        0.000 Mt        0.016 Mt   100.000 % 	 ....../PrepareMaterials

		       1.361 Mt    42.494 %         1 x        1.361 Mt        0.000 Mt        1.361 Mt   100.000 % 	 ....../BuildMaterialCache

		       0.245 Mt     7.660 %         1 x        0.245 Mt        0.000 Mt        0.245 Mt   100.000 % 	 ....../PrepareDrawBundle

		      15.414 Mt     6.694 %         1 x       15.414 Mt        0.000 Mt        3.994 Mt    25.913 % 	 ..../RayTracingRLPrep

		       0.239 Mt     1.553 %         1 x        0.239 Mt        0.000 Mt        0.239 Mt   100.000 % 	 ...../PrepareAccelerationStructures

		       0.023 Mt     0.149 %         1 x        0.023 Mt        0.000 Mt        0.023 Mt   100.000 % 	 ...../PrepareCache

		       0.520 Mt     3.373 %         1 x        0.520 Mt        0.000 Mt        0.520 Mt   100.000 % 	 ...../BuildCache

		       8.830 Mt    57.287 %         1 x        8.830 Mt        0.000 Mt        0.006 Mt     0.069 % 	 ...../BuildAccelerationStructures

		       8.824 Mt    99.931 %         1 x        8.824 Mt        0.000 Mt        6.768 Mt    76.697 % 	 ....../AccelerationStructureBuild

		       1.079 Mt    12.232 %         1 x        1.079 Mt        0.000 Mt        1.079 Mt   100.000 % 	 ......./BuildBottomLevelAS

		       0.977 Mt    11.071 %         1 x        0.977 Mt        0.000 Mt        0.977 Mt   100.000 % 	 ......./BuildTopLevelAS

		       1.807 Mt    11.725 %         1 x        1.807 Mt        0.000 Mt        1.807 Mt   100.000 % 	 ...../BuildShaderTables

		      12.324 Mt     5.353 %         1 x       12.324 Mt        0.000 Mt       12.324 Mt   100.000 % 	 ..../GpuSidePrep

		   23933.606 Mt    97.111 %     24112 x        0.993 Mt        1.285 Mt      335.272 Mt     1.401 % 	 ../MainLoop

		     642.884 Mt     2.686 %      1441 x        0.446 Mt        0.290 Mt      642.008 Mt    99.864 % 	 .../Update

		       0.876 Mt     0.136 %         1 x        0.876 Mt        0.000 Mt        0.876 Mt   100.000 % 	 ..../GuiModelApply

		   22955.450 Mt    95.913 %     22904 x        1.002 Mt        0.988 Mt    15826.249 Mt    68.943 % 	 .../Render

		    7129.202 Mt    31.057 %     22904 x        0.311 Mt        0.279 Mt     7039.642 Mt    98.744 % 	 ..../Textured

		      89.559 Mt     1.256 %     22904 x        0.004 Mt        0.002 Mt       89.559 Mt   100.000 % 	 ...../TexturedRLPrep

	WindowThread:

		   24642.022 Mt   100.000 %         1 x    24642.022 Mt    24642.021 Mt    24642.022 Mt   100.000 % 	 /

	GpuThread:

		    1619.435 Mt   100.000 %     22904 x        0.071 Mt        0.044 Mt      143.337 Mt     8.851 % 	 /

		       0.447 Mt     0.028 %         1 x        0.447 Mt        0.000 Mt        0.006 Mt     1.416 % 	 ./ScenePrep

		       0.441 Mt    98.584 %         1 x        0.441 Mt        0.000 Mt        0.001 Mt     0.283 % 	 ../AccelerationStructureBuild

		       0.354 Mt    80.187 %         1 x        0.354 Mt        0.000 Mt        0.354 Mt   100.000 % 	 .../BuildBottomLevelASGpu

		       0.086 Mt    19.530 %         1 x        0.086 Mt        0.000 Mt        0.086 Mt   100.000 % 	 .../BuildTopLevelASGpu

		    1254.772 Mt    77.482 %     22904 x        0.055 Mt        0.026 Mt      103.759 Mt     8.269 % 	 ./TexturedGpu

		    1151.012 Mt    91.731 %     22904 x        0.050 Mt        0.001 Mt     1151.012 Mt   100.000 % 	 ../TexturedRLGpu

		     220.879 Mt    13.639 %     22904 x        0.010 Mt        0.017 Mt      220.879 Mt   100.000 % 	 ./Present


	============================


