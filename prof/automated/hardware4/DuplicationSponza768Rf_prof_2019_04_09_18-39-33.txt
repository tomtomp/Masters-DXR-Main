Configuration: 
Command line parameters  = E:\programming\FIT-DIP\Masters-DXR-Main-git\build\Measurement\x64\Debug\Measurement.exe --scene Sponza/Sponza.gltf --duplication-cube --profile-duplication 7 --rt-mode --width 1024 --height 768 --profile-output DuplicationSponza768Rf --fast-build-as 
Window width             = 1024
Window height            = 768
Window title             = Direct3D App
Back-buffer count        = 3
Fill Triangles           = enabled
Triangles Font Clock-Wise= enabled
Fast Build AS            = enabled
Target FPS               = 60
Target UPS               = 60
Tearing                  = disabled
VSync                    = disabled
GUI rendering            = disabled
Debug layer              = enabled
GPU validation           = disabled
GPU profiling            = enabled
Render mode              = Ray Tracing
Loaded scene file        = Sponza/Sponza.gltf
Using testing scene      = disabled
Duplication              = 7
Duplication in cube      = enabled
Duplication offset       = 10
BLAS duplication         = enabled
Rays per pixel           = 10

ProfilingAggregator: 
Render	,	7.0005	,	8.9487	,	19.7788	,	19.9831	,	30.5333	,	29.0609	,	57.4483	,	55.0792	,	1298.44	,	96.5076	,	2196.75	,	162.709	,	3155.41	,	253.541
Update	,	0.7982	,	0.4099	,	0.5212	,	0.5246	,	0.3776	,	0.3122	,	0.4525	,	0.3787	,	0.3194	,	0.4861	,	0.415	,	0.7155	,	0.55	,	0.5296
RayTracing	,	2.5688	,	2.5669	,	11.4089	,	11.5956	,	21.2861	,	20.0245	,	48.7108	,	46.4639	,	935.489	,	86.3306	,	1668.17	,	148.82	,	2486.84	,	234.697
RayTracingGpu	,	3.04259	,	4.94806	,	6.26826	,	6.29107	,	6.94042	,	6.94758	,	5.45059	,	5.45709	,	356.605	,	5.46781	,	503.893	,	6.6408	,	654.808	,	8.83341
DeferredRLGpu	,	0.413088	,	0.594272	,	0.942752	,	0.946432	,	1.6167	,	1.61021	,	2.08886	,	2.09242	,	11.2377	,	2.88858	,	17.8353	,	4.24922	,	29.165	,	6.48758
RayTracingRLGpu	,	1.57699	,	2.61421	,	3.64093	,	3.65942	,	3.66547	,	3.67798	,	2.31098	,	2.30979	,	2.36906	,	1.7872	,	1.79411	,	1.6743	,	1.64538	,	1.62813
ResolveRLGpu	,	1.05037	,	1.73718	,	1.68195	,	1.68288	,	1.65555	,	1.65651	,	1.04858	,	1.05245	,	1.09456	,	0.79024	,	0.786336	,	0.715872	,	0.735392	,	0.716384
Index	,	0	,	1	,	2	,	3	,	4	,	5	,	6	,	7	,	8	,	9	,	10	,	11	,	12	,	13

ProfilingBuildAggregator: 
BuildBottomLevelAS	,	4.606	,	51.4007	,	125.206	,	254.532	,	478.032	,	724.413
BuildBottomLevelASGpu	,	15.8973	,	75.8244	,	191.048	,	341.69	,	483.327	,	623.098
BuildTopLevelAS	,	2.8376	,	1.3697	,	1.7632	,	1.0669	,	1.6802	,	1.2014
BuildTopLevelASGpu	,	0.617184	,	0.251648	,	0.155296	,	0.19536	,	0.137696	,	0.152288
Duplication	,	1	,	3	,	4	,	5	,	6	,	7
Triangles	,	262267	,	7081209	,	16785088	,	32783375	,	56649672	,	89957581
Meshes	,	103	,	2781	,	6592	,	12875	,	22248	,	35329
BottomLevels	,	1	,	27	,	64	,	125	,	216	,	343
Index	,	0	,	1	,	2	,	3	,	4	,	5

FpsAggregator: 
FPS	,	55	,	59	,	41	,	47	,	10	,	33	,	5	,	17	,	1	,	9	,	1	,	5	,	1	,	4
UPS	,	59	,	60	,	60	,	60	,	60	,	61	,	60	,	62	,	4	,	139	,	6	,	189	,	10	,	253
FrameTime	,	18.1992	,	17.0257	,	24.513	,	21.338	,	101.719	,	30.6906	,	205.371	,	60.8868	,	1344.04	,	119.837	,	2269.52	,	210.026	,	3271.4	,	302.987
GigaRays/s	,	0.432123	,	0.461909	,	0.320822	,	0.368559	,	0.0773139	,	0.256245	,	0.0382932	,	0.129163	,	0.00585124	,	0.0656253	,	0.00346519	,	0.0374445	,	0.00240396	,	0.0259559
Index	,	0	,	1	,	2	,	3	,	4	,	5	,	6	,	7	,	8	,	9	,	10	,	11	,	12	,	13


AccelerationStructure: 
Ray tracing acceleration structure building statistics: 
	Bottom Level Acceleration Structure: 
		Maximum [B]: 14623872
		Minimum [B]: 14623872
		Currently Allocated [B]: 5015988096
		Currently Created [num]: 343
		Current Average [B]: 14623872
		Maximum Triangles [num]: 262267
		Minimum Triangles [num]: 262267
		Total Triangles [num]: 89957581
		Maximum Meshes [num]: 103
		Minimum Meshes [num]: 103
		Total Meshes [num]: 35329
	Top Level Acceleration Structure: 
		Maximum [B]: 21952
		Minimum [B]: 21952
		Currently Allocated [B]: 21952
		Total Allocated [B]: 21952
		Total Created [num]: 1
		Average Allocated [B]: 21952
		Maximum Instances [num]: 343
		Minimum Instances [num]: 343
		Total Instances [num]: 343
	Scratch Buffer: 
		Maximum [B]: 11660800
		Minimum [B]: 11660800
		Currently Allocated [B]: 11660800
		Total Allocated [B]: 11660800
		Total Created [num]: 1
		Average Allocated [B]: 11660800
ProfilingStack: 
[PROF] Profiling statistics: 
	Scopes entered : 237146
	Scopes exited : 237145
	Overhead per scope [ticks] : 1008.4
	Threads entered : 3
	Threads exited : 0
	============================
		         Sum[Mt]   Parent[%]  Samples[num]       Avg[Mt]   Last/Curr[Mt]        Self[Mt]     Self[%] 	 Name

	main:

		   28389.147 Mt   100.000 %         1 x    28389.150 Mt    28389.145 Mt        1.083 Mt     0.004 % 	 /

		   28388.207 Mt    99.997 %         1 x    28388.212 Mt    28388.206 Mt      937.113 Mt     3.301 % 	 ./Main application loop

		       2.859 Mt     0.010 %         1 x        2.859 Mt        0.000 Mt        2.859 Mt   100.000 % 	 ../TexturedRLInitialization

		       7.304 Mt     0.026 %         1 x        7.304 Mt        0.000 Mt        7.304 Mt   100.000 % 	 ../DeferredRLInitialization

		      48.267 Mt     0.170 %         1 x       48.267 Mt        0.000 Mt        2.364 Mt     4.899 % 	 ../RayTracingRLInitialization

		      45.903 Mt    95.101 %         1 x       45.903 Mt        0.000 Mt       45.903 Mt   100.000 % 	 .../PipelineBuild

		      11.784 Mt     0.042 %         1 x       11.784 Mt        0.000 Mt       11.784 Mt   100.000 % 	 ../ResolveRLInitialization

		    9280.459 Mt    32.691 %         1 x     9280.459 Mt        0.000 Mt        7.813 Mt     0.084 % 	 ../Initialization

		    9272.646 Mt    99.916 %         1 x     9272.646 Mt        0.000 Mt        3.586 Mt     0.039 % 	 .../ScenePrep

		    9151.136 Mt    98.690 %         1 x     9151.136 Mt        0.000 Mt      407.843 Mt     4.457 % 	 ..../SceneLoad

		    8252.636 Mt    90.182 %         1 x     8252.636 Mt        0.000 Mt     1102.563 Mt    13.360 % 	 ...../glTF-LoadScene

		    7150.074 Mt    86.640 %        69 x      103.624 Mt        0.000 Mt     7150.074 Mt   100.000 % 	 ....../glTF-LoadImageData

		     299.686 Mt     3.275 %         1 x      299.686 Mt        0.000 Mt      299.686 Mt   100.000 % 	 ...../glTF-CreateScene

		     190.971 Mt     2.087 %         1 x      190.971 Mt        0.000 Mt      190.971 Mt   100.000 % 	 ...../SceneDesc-CreateScene

		       7.131 Mt     0.077 %         1 x        7.131 Mt        0.000 Mt        0.005 Mt     0.074 % 	 ..../TexturedRLPrep

		       7.126 Mt    99.926 %         1 x        7.126 Mt        0.000 Mt        2.031 Mt    28.497 % 	 ...../PrepareForRendering

		       2.749 Mt    38.573 %         1 x        2.749 Mt        0.000 Mt        2.749 Mt   100.000 % 	 ....../PrepareMaterials

		       2.346 Mt    32.930 %         1 x        2.346 Mt        0.000 Mt        2.346 Mt   100.000 % 	 ....../PrepareDrawBundle

		       8.679 Mt     0.094 %         1 x        8.679 Mt        0.000 Mt        0.005 Mt     0.060 % 	 ..../DeferredRLPrep

		       8.674 Mt    99.940 %         1 x        8.674 Mt        0.000 Mt        3.851 Mt    44.395 % 	 ...../PrepareForRendering

		       0.021 Mt     0.241 %         1 x        0.021 Mt        0.000 Mt        0.021 Mt   100.000 % 	 ....../PrepareMaterials

		       2.193 Mt    25.288 %         1 x        2.193 Mt        0.000 Mt        2.193 Mt   100.000 % 	 ....../BuildMaterialCache

		       2.609 Mt    30.077 %         1 x        2.609 Mt        0.000 Mt        2.609 Mt   100.000 % 	 ....../PrepareDrawBundle

		      43.115 Mt     0.465 %         1 x       43.115 Mt        0.000 Mt        6.454 Mt    14.969 % 	 ..../RayTracingRLPrep

		       0.576 Mt     1.335 %         1 x        0.576 Mt        0.000 Mt        0.576 Mt   100.000 % 	 ...../PrepareAccelerationStructures

		       0.026 Mt     0.061 %         1 x        0.026 Mt        0.000 Mt        0.026 Mt   100.000 % 	 ...../PrepareCache

		      19.016 Mt    44.106 %         1 x       19.016 Mt        0.000 Mt       19.016 Mt   100.000 % 	 ...../BuildCache

		      12.825 Mt    29.747 %         1 x       12.825 Mt        0.000 Mt        0.021 Mt     0.160 % 	 ...../BuildAccelerationStructures

		      12.805 Mt    99.840 %         1 x       12.805 Mt        0.000 Mt        5.361 Mt    41.869 % 	 ....../AccelerationStructureBuild

		       4.606 Mt    35.971 %         1 x        4.606 Mt        0.000 Mt        4.606 Mt   100.000 % 	 ......./BuildBottomLevelAS

		       2.838 Mt    22.160 %         1 x        2.838 Mt        0.000 Mt        2.838 Mt   100.000 % 	 ......./BuildTopLevelAS

		       4.218 Mt     9.782 %         1 x        4.218 Mt        0.000 Mt        4.218 Mt   100.000 % 	 ...../BuildShaderTables

		      58.998 Mt     0.636 %         1 x       58.998 Mt        0.000 Mt       58.998 Mt   100.000 % 	 ..../GpuSidePrep

		   18100.422 Mt    63.760 %    233236 x        0.078 Mt      304.914 Mt      658.211 Mt     3.636 % 	 ../MainLoop

		    1043.199 Mt     5.763 %      1099 x        0.949 Mt        1.418 Mt     1040.498 Mt    99.741 % 	 .../Update

		       2.701 Mt     0.259 %         1 x        2.701 Mt        0.000 Mt        2.701 Mt   100.000 % 	 ..../GuiModelApply

		   16399.011 Mt    90.600 %       289 x       56.744 Mt      264.216 Mt     4089.070 Mt    24.935 % 	 .../Render

		   12309.941 Mt    75.065 %       289 x       42.595 Mt      243.796 Mt     6769.821 Mt    54.995 % 	 ..../RayTracing

		    1675.623 Mt    13.612 %       289 x        5.798 Mt        0.008 Mt        1.264 Mt     0.075 % 	 ...../DeferredRLPrep

		    1674.360 Mt    99.925 %         7 x      239.194 Mt        0.000 Mt       62.350 Mt     3.724 % 	 ....../PrepareForRendering

		       2.341 Mt     0.140 %         7 x        0.334 Mt        0.000 Mt        2.341 Mt   100.000 % 	 ......./PrepareMaterials

		       0.006 Mt     0.000 %         7 x        0.001 Mt        0.000 Mt        0.006 Mt   100.000 % 	 ......./BuildMaterialCache

		    1609.662 Mt    96.136 %         7 x      229.952 Mt        0.000 Mt     1609.662 Mt   100.000 % 	 ......./PrepareDrawBundle

		    3864.497 Mt    31.393 %       289 x       13.372 Mt        0.011 Mt       19.781 Mt     0.512 % 	 ...../RayTracingRLPrep

		     610.013 Mt    15.785 %         7 x       87.145 Mt        0.000 Mt      610.013 Mt   100.000 % 	 ....../PrepareAccelerationStructures

		       2.176 Mt     0.056 %         7 x        0.311 Mt        0.000 Mt        2.176 Mt   100.000 % 	 ....../PrepareCache

		       0.005 Mt     0.000 %         7 x        0.001 Mt        0.000 Mt        0.005 Mt   100.000 % 	 ....../BuildCache

		    1757.343 Mt    45.474 %         7 x      251.049 Mt        0.000 Mt        0.021 Mt     0.001 % 	 ....../BuildAccelerationStructures

		    1757.322 Mt    99.999 %         7 x      251.046 Mt        0.000 Mt       44.689 Mt     2.543 % 	 ......./AccelerationStructureBuild

		    1702.814 Mt    96.898 %         7 x      243.259 Mt        0.000 Mt     1702.814 Mt   100.000 % 	 ......../BuildBottomLevelAS

		       9.819 Mt     0.559 %         7 x        1.403 Mt        0.000 Mt        9.819 Mt   100.000 % 	 ......../BuildTopLevelAS

		    1475.179 Mt    38.173 %         7 x      210.740 Mt        0.000 Mt     1475.179 Mt   100.000 % 	 ....../BuildShaderTables

	WindowThread:

		   28379.775 Mt   100.000 %         1 x    28379.776 Mt    28379.774 Mt    28379.775 Mt   100.000 % 	 /

	GpuThread:

		    3555.781 Mt   100.000 %       289 x       12.304 Mt        8.846 Mt      150.492 Mt     4.232 % 	 /

		      16.569 Mt     0.466 %         1 x       16.569 Mt        0.000 Mt        0.052 Mt     0.314 % 	 ./ScenePrep

		      16.517 Mt    99.686 %         1 x       16.517 Mt        0.000 Mt        0.002 Mt     0.015 % 	 ../AccelerationStructureBuild

		      15.897 Mt    96.248 %         1 x       15.897 Mt        0.000 Mt       15.897 Mt   100.000 % 	 .../BuildBottomLevelASGpu

		       0.617 Mt     3.737 %         1 x        0.617 Mt        0.000 Mt        0.617 Mt   100.000 % 	 .../BuildTopLevelASGpu

		    3388.440 Mt    95.294 %       289 x       11.725 Mt        8.845 Mt        0.778 Mt     0.023 % 	 ./RayTracingGpu

		     390.174 Mt    11.515 %       289 x        1.350 Mt        6.481 Mt      390.174 Mt   100.000 % 	 ../DeferredRLGpu

		     783.110 Mt    23.111 %       289 x        2.710 Mt        1.642 Mt      783.110 Mt   100.000 % 	 ../RayTracingRLGpu

		     405.401 Mt    11.964 %       289 x        1.403 Mt        0.720 Mt      405.401 Mt   100.000 % 	 ../ResolveRLGpu

		    1808.977 Mt    53.387 %         7 x      258.425 Mt        0.000 Mt        0.010 Mt     0.001 % 	 ../AccelerationStructureBuild

		    1807.687 Mt    99.929 %         7 x      258.241 Mt        0.000 Mt     1807.687 Mt   100.000 % 	 .../BuildBottomLevelASGpu

		       1.280 Mt     0.071 %         7 x        0.183 Mt        0.000 Mt        1.280 Mt   100.000 % 	 .../BuildTopLevelASGpu

		       0.280 Mt     0.008 %       289 x        0.001 Mt        0.000 Mt        0.280 Mt   100.000 % 	 ./Present


	============================


