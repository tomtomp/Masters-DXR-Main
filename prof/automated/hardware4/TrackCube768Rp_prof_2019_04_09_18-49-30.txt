Configuration: 
Command line parameters  = E:\programming\FIT-DIP\Masters-DXR-Main-git\build\Measurement\x64\Debug\Measurement.exe --scene Cube/Cube.gltf --profile-track Cube/Cube.trk --rt-mode-pure --width 1024 --height 768 --profile-output TrackCube768Rp 
Window width             = 1024
Window height            = 768
Window title             = Direct3D App
Back-buffer count        = 3
Fill Triangles           = enabled
Triangles Font Clock-Wise= enabled
Fast Build AS            = disabled
Target FPS               = 0
Target UPS               = 60
Tearing                  = enabled
VSync                    = disabled
GUI rendering            = disabled
Debug layer              = enabled
GPU validation           = disabled
GPU profiling            = enabled
Render mode              = Ray Tracing
Loaded scene file        = Cube/Cube.gltf
Using testing scene      = disabled
Duplication              = 1
Duplication in cube      = disabled
Duplication offset       = 10
BLAS duplication         = disabled
Rays per pixel           = 9

ProfilingAggregator: 
Render	,	4.1439	,	2.8615	,	3.3516	,	3.3969	,	2.9747	,	4.2392	,	2.8386	,	4.6699	,	2.9343	,	6.0001	,	2.8353	,	3.2369	,	2.7739	,	2.8905	,	3.0099	,	4.7985	,	4.5899	,	4.2288	,	3.2159	,	4.92	,	3.8555	,	2.9157	,	3.284	,	4.4801	,	4.1582	,	5.8227	,	4.2454	,	4.1314	,	2.7105
Update	,	0.2702	,	0.3357	,	0.4214	,	0.3933	,	0.2947	,	0.5266	,	0.2917	,	0.6874	,	0.6016	,	0.4457	,	0.3021	,	0.3592	,	0.3021	,	0.3408	,	0.2995	,	0.5799	,	0.3667	,	0.6686	,	0.2923	,	0.777	,	0.8022	,	0.5169	,	0.5982	,	0.3562	,	0.5483	,	0.8784	,	0.3003	,	0.7346	,	0.691
RayTracing	,	1.6153	,	0.8262	,	1.1066	,	1.0398	,	0.7424	,	1.4773	,	0.7243	,	1.6688	,	0.7185	,	2.6123	,	0.7517	,	0.926	,	0.7426	,	0.8792	,	0.7319	,	1.6649	,	1.5618	,	1.5264	,	0.8867	,	1.959	,	1.4786	,	0.7747	,	0.9902	,	1.8357	,	1.5248	,	2.5505	,	1.3714	,	1.5628	,	0.7131
RayTracingGpu	,	1.01226	,	1.16269	,	1.21757	,	1.34595	,	1.49133	,	1.43107	,	1.36941	,	1.48112	,	1.40323	,	1.33878	,	1.31043	,	1.32787	,	1.22131	,	1.15894	,	1.37222	,	1.69085	,	1.40067	,	1.40128	,	1.40464	,	1.40534	,	1.30019	,	1.11216	,	1.16819	,	1.15126	,	1.15261	,	1.09328	,	1.09453	,	1.1097	,	1.1399
RayTracingRLGpu	,	0.362592	,	0.46864	,	0.500896	,	0.594272	,	0.737152	,	0.776768	,	0.715968	,	0.799872	,	0.777376	,	0.735264	,	0.708064	,	0.724032	,	0.617088	,	0.556928	,	0.706912	,	0.804864	,	0.798496	,	0.799488	,	0.80448	,	0.803712	,	0.698176	,	0.510592	,	0.498528	,	0.456288	,	0.419872	,	0.337632	,	0.282784	,	0.250528	,	0.228448
ResolveRLGpu	,	0.648448	,	0.692736	,	0.715584	,	0.750368	,	0.752864	,	0.653184	,	0.65232	,	0.680064	,	0.62464	,	0.602496	,	0.601184	,	0.602784	,	0.6032	,	0.601056	,	0.664192	,	0.884768	,	0.601184	,	0.600768	,	0.599168	,	0.60064	,	0.600992	,	0.600416	,	0.668512	,	0.693792	,	0.73136	,	0.754336	,	0.810176	,	0.857536	,	0.909664
Index	,	0	,	1	,	2	,	3	,	4	,	5	,	6	,	7	,	8	,	9	,	10	,	11	,	12	,	13	,	14	,	15	,	16	,	17	,	18	,	19	,	20	,	21	,	22	,	23	,	24	,	25	,	26	,	27	,	28

ProfilingBuildAggregator: 
BuildBottomLevelAS	,	1.0829
BuildBottomLevelASGpu	,	0.088928
BuildTopLevelAS	,	1.2026
BuildTopLevelASGpu	,	0.059904
Duplication	,	1
Triangles	,	12
Meshes	,	1
BottomLevels	,	1
Index	,	0

FpsAggregator: 
FPS	,	319	,	271	,	279	,	260	,	245	,	257	,	248	,	236	,	255	,	261	,	261	,	271	,	265	,	283	,	264	,	245	,	249	,	251	,	252	,	245	,	258	,	268	,	288	,	288	,	277	,	283	,	282	,	286	,	270
UPS	,	59	,	61	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	61	,	60	,	60	,	60	,	60	,	60	,	60	,	61	,	60	,	60	,	60	,	60	,	60	,	60	,	61	,	60	,	60	,	60	,	60
FrameTime	,	3.1458	,	3.69313	,	3.58644	,	3.84786	,	4.0824	,	3.89422	,	4.04028	,	4.25258	,	3.92514	,	3.85235	,	3.8422	,	3.69601	,	3.78	,	3.53678	,	3.79892	,	4.09409	,	4.02974	,	4.00219	,	3.97211	,	4.0898	,	3.88328	,	3.73597	,	3.48077	,	3.48221	,	3.62716	,	3.54539	,	3.55979	,	3.50449	,	3.70534
GigaRays/s	,	2.24995	,	1.9165	,	1.97351	,	1.83943	,	1.73376	,	1.81754	,	1.75183	,	1.66438	,	1.80322	,	1.83729	,	1.84215	,	1.91501	,	1.87246	,	2.00122	,	1.86313	,	1.72881	,	1.75641	,	1.7685	,	1.7819	,	1.73062	,	1.82265	,	1.89452	,	2.03342	,	2.03259	,	1.95136	,	1.99636	,	1.98829	,	2.01966	,	1.91018
Index	,	0	,	1	,	2	,	3	,	4	,	5	,	6	,	7	,	8	,	9	,	10	,	11	,	12	,	13	,	14	,	15	,	16	,	17	,	18	,	19	,	20	,	21	,	22	,	23	,	24	,	25	,	26	,	27	,	28


AccelerationStructure: 
Ray tracing acceleration structure building statistics: 
	Bottom Level Acceleration Structure: 
		Maximum [B]: 5168
		Minimum [B]: 5168
		Currently Allocated [B]: 5168
		Currently Created [num]: 1
		Current Average [B]: 5168
		Maximum Triangles [num]: 12
		Minimum Triangles [num]: 12
		Total Triangles [num]: 12
		Maximum Meshes [num]: 1
		Minimum Meshes [num]: 1
		Total Meshes [num]: 1
	Top Level Acceleration Structure: 
		Maximum [B]: 64
		Minimum [B]: 64
		Currently Allocated [B]: 64
		Total Allocated [B]: 64
		Total Created [num]: 1
		Average Allocated [B]: 64
		Maximum Instances [num]: 1
		Minimum Instances [num]: 1
		Total Instances [num]: 1
	Scratch Buffer: 
		Maximum [B]: 3584
		Minimum [B]: 3584
		Currently Allocated [B]: 3584
		Total Allocated [B]: 3584
		Total Created [num]: 1
		Average Allocated [B]: 3584
ProfilingStack: 
[PROF] Profiling statistics: 
	Scopes entered : 68015
	Scopes exited : 68014
	Overhead per scope [ticks] : 1006
	Threads entered : 3
	Threads exited : 0
	============================
		         Sum[Mt]   Parent[%]  Samples[num]       Avg[Mt]   Last/Curr[Mt]        Self[Mt]     Self[%] 	 Name

	main:

		   29599.689 Mt   100.000 %         1 x    29599.691 Mt    29599.689 Mt        0.747 Mt     0.003 % 	 /

		   29598.985 Mt    99.998 %         1 x    29598.987 Mt    29598.985 Mt      366.442 Mt     1.238 % 	 ./Main application loop

		       3.091 Mt     0.010 %         1 x        3.091 Mt        0.000 Mt        3.091 Mt   100.000 % 	 ../TexturedRLInitialization

		       6.742 Mt     0.023 %         1 x        6.742 Mt        0.000 Mt        6.742 Mt   100.000 % 	 ../DeferredRLInitialization

		      59.876 Mt     0.202 %         1 x       59.876 Mt        0.000 Mt        2.219 Mt     3.707 % 	 ../RayTracingRLInitialization

		      57.657 Mt    96.293 %         1 x       57.657 Mt        0.000 Mt       57.657 Mt   100.000 % 	 .../PipelineBuild

		      11.943 Mt     0.040 %         1 x       11.943 Mt        0.000 Mt       11.943 Mt   100.000 % 	 ../ResolveRLInitialization

		     128.283 Mt     0.433 %         1 x      128.283 Mt        0.000 Mt        6.103 Mt     4.758 % 	 ../Initialization

		     122.180 Mt    95.242 %         1 x      122.180 Mt        0.000 Mt        1.831 Mt     1.499 % 	 .../ScenePrep

		      86.612 Mt    70.888 %         1 x       86.612 Mt        0.000 Mt       15.461 Mt    17.851 % 	 ..../SceneLoad

		      63.831 Mt    73.697 %         1 x       63.831 Mt        0.000 Mt       12.675 Mt    19.856 % 	 ...../glTF-LoadScene

		      51.156 Mt    80.144 %         2 x       25.578 Mt        0.000 Mt       51.156 Mt   100.000 % 	 ....../glTF-LoadImageData

		       4.110 Mt     4.746 %         1 x        4.110 Mt        0.000 Mt        4.110 Mt   100.000 % 	 ...../glTF-CreateScene

		       3.209 Mt     3.706 %         1 x        3.209 Mt        0.000 Mt        3.209 Mt   100.000 % 	 ...../SceneDesc-CreateScene

		       1.921 Mt     1.572 %         1 x        1.921 Mt        0.000 Mt        0.005 Mt     0.276 % 	 ..../TexturedRLPrep

		       1.916 Mt    99.724 %         1 x        1.916 Mt        0.000 Mt        1.102 Mt    57.519 % 	 ...../PrepareForRendering

		       0.510 Mt    26.645 %         1 x        0.510 Mt        0.000 Mt        0.510 Mt   100.000 % 	 ....../PrepareMaterials

		       0.303 Mt    15.836 %         1 x        0.303 Mt        0.000 Mt        0.303 Mt   100.000 % 	 ....../PrepareDrawBundle

		       2.381 Mt     1.949 %         1 x        2.381 Mt        0.000 Mt        0.004 Mt     0.185 % 	 ..../DeferredRLPrep

		       2.377 Mt    99.815 %         1 x        2.377 Mt        0.000 Mt        1.640 Mt    68.996 % 	 ...../PrepareForRendering

		       0.015 Mt     0.652 %         1 x        0.015 Mt        0.000 Mt        0.015 Mt   100.000 % 	 ....../PrepareMaterials

		       0.460 Mt    19.354 %         1 x        0.460 Mt        0.000 Mt        0.460 Mt   100.000 % 	 ....../BuildMaterialCache

		       0.261 Mt    10.998 %         1 x        0.261 Mt        0.000 Mt        0.261 Mt   100.000 % 	 ....../PrepareDrawBundle

		      16.827 Mt    13.772 %         1 x       16.827 Mt        0.000 Mt        6.495 Mt    38.596 % 	 ..../RayTracingRLPrep

		       0.249 Mt     1.478 %         1 x        0.249 Mt        0.000 Mt        0.249 Mt   100.000 % 	 ...../PrepareAccelerationStructures

		       0.023 Mt     0.136 %         1 x        0.023 Mt        0.000 Mt        0.023 Mt   100.000 % 	 ...../PrepareCache

		       2.364 Mt    14.050 %         1 x        2.364 Mt        0.000 Mt        2.364 Mt   100.000 % 	 ...../BuildCache

		       5.767 Mt    34.270 %         1 x        5.767 Mt        0.000 Mt        0.009 Mt     0.147 % 	 ...../BuildAccelerationStructures

		       5.758 Mt    99.853 %         1 x        5.758 Mt        0.000 Mt        3.473 Mt    60.308 % 	 ....../AccelerationStructureBuild

		       1.083 Mt    18.807 %         1 x        1.083 Mt        0.000 Mt        1.083 Mt   100.000 % 	 ......./BuildBottomLevelAS

		       1.203 Mt    20.885 %         1 x        1.203 Mt        0.000 Mt        1.203 Mt   100.000 % 	 ......./BuildTopLevelAS

		       1.930 Mt    11.470 %         1 x        1.930 Mt        0.000 Mt        1.930 Mt   100.000 % 	 ...../BuildShaderTables

		      12.608 Mt    10.319 %         1 x       12.608 Mt        0.000 Mt       12.608 Mt   100.000 % 	 ..../GpuSidePrep

		   29022.607 Mt    98.053 %     12200 x        2.379 Mt        3.159 Mt      362.646 Mt     1.250 % 	 ../MainLoop

		    1031.554 Mt     3.554 %      1744 x        0.591 Mt        0.297 Mt     1030.696 Mt    99.917 % 	 .../Update

		       0.858 Mt     0.083 %         1 x        0.858 Mt        0.000 Mt        0.858 Mt   100.000 % 	 ..../GuiModelApply

		   27628.407 Mt    95.196 %      7719 x        3.579 Mt        2.855 Mt    18791.953 Mt    68.017 % 	 .../Render

		    8836.454 Mt    31.983 %      7719 x        1.145 Mt        0.780 Mt     8790.655 Mt    99.482 % 	 ..../RayTracing

		      45.799 Mt     0.518 %      7719 x        0.006 Mt        0.003 Mt       45.799 Mt   100.000 % 	 ...../RayTracingRLPrep

	WindowThread:

		   29595.275 Mt   100.000 %         1 x    29595.276 Mt    29595.275 Mt    29595.275 Mt   100.000 % 	 /

	GpuThread:

		    9987.262 Mt   100.000 %      7719 x        1.294 Mt        1.141 Mt      102.661 Mt     1.028 % 	 /

		       0.155 Mt     0.002 %         1 x        0.155 Mt        0.000 Mt        0.005 Mt     3.230 % 	 ./ScenePrep

		       0.150 Mt    96.770 %         1 x        0.150 Mt        0.000 Mt        0.001 Mt     0.471 % 	 ../AccelerationStructureBuild

		       0.089 Mt    59.469 %         1 x        0.089 Mt        0.000 Mt        0.089 Mt   100.000 % 	 .../BuildBottomLevelASGpu

		       0.060 Mt    40.060 %         1 x        0.060 Mt        0.000 Mt        0.060 Mt   100.000 % 	 .../BuildTopLevelASGpu

		    9879.924 Mt    98.925 %      7719 x        1.280 Mt        1.139 Mt        8.716 Mt     0.088 % 	 ./RayTracingGpu

		    4563.965 Mt    46.194 %      7719 x        0.591 Mt        0.228 Mt     4563.965 Mt   100.000 % 	 ../RayTracingRLGpu

		    5307.243 Mt    53.717 %      7719 x        0.688 Mt        0.909 Mt     5307.243 Mt   100.000 % 	 ../ResolveRLGpu

		       4.522 Mt     0.045 %      7719 x        0.001 Mt        0.001 Mt        4.522 Mt   100.000 % 	 ./Present


	============================


