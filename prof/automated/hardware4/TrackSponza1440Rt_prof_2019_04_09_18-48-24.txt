Configuration: 
Command line parameters  = E:\programming\FIT-DIP\Masters-DXR-Main-git\build\Measurement\x64\Debug\Measurement.exe --scene Sponza/Sponza.gltf --profile-track Sponza/Sponza.trk --rt-mode --width 2560 --height 1440 --profile-output TrackSponza1440Rt 
Window width             = 2560
Window height            = 1421
Window title             = Direct3D App
Back-buffer count        = 3
Fill Triangles           = enabled
Triangles Font Clock-Wise= enabled
Fast Build AS            = disabled
Target FPS               = 0
Target UPS               = 60
Tearing                  = enabled
VSync                    = disabled
GUI rendering            = disabled
Debug layer              = enabled
GPU validation           = disabled
GPU profiling            = enabled
Render mode              = Ray Tracing
Loaded scene file        = Sponza/Sponza.gltf
Using testing scene      = disabled
Duplication              = 1
Duplication in cube      = disabled
Duplication offset       = 10
BLAS duplication         = disabled
Rays per pixel           = 10

ProfilingAggregator: 
Render	,	14.9745	,	14.815	,	14.498	,	14.591	,	14.4221	,	14.4478	,	13.7306	,	12.7424	,	13.5441	,	13.7081	,	14.4112	,	14.8164	,	14.1728	,	16.9679	,	14.3242	,	14.1877	,	14.2731	,	13.6921	,	13.7351	,	15.7315	,	17.6121	,	16.408	,	15.5797	,	14.5805	,	14.0068	,	14.2107
Update	,	0.3988	,	0.5985	,	0.3064	,	0.3332	,	0.3445	,	0.335	,	0.4239	,	0.2986	,	0.3489	,	0.4781	,	0.6604	,	0.3741	,	0.3242	,	0.376	,	0.3304	,	0.5084	,	0.731	,	0.3263	,	0.3464	,	0.5798	,	0.4455	,	0.3309	,	0.3333	,	0.7146	,	0.7499	,	0.4915
RayTracing	,	3.0618	,	3.0436	,	2.8658	,	3.0081	,	3.1059	,	2.9983	,	2.9954	,	2.8914	,	3.0232	,	3.0088	,	3.0586	,	3.0722	,	3.0525	,	4.996	,	3.1078	,	2.9933	,	3.0022	,	3.0291	,	3.1	,	3.0432	,	3.0455	,	3.0532	,	3.0533	,	3.0165	,	3.0753	,	3.0654
RayTracingGpu	,	10.4303	,	10.3947	,	10.2111	,	10.0794	,	9.65798	,	10.0489	,	9.27354	,	8.29306	,	8.90333	,	9.10995	,	9.67898	,	10.1939	,	9.54304	,	9.52515	,	9.59094	,	9.76621	,	9.93862	,	9.05088	,	9.00147	,	11.0531	,	13.0637	,	11.9094	,	11.0944	,	10.1372	,	9.32061	,	9.74448
DeferredRLGpu	,	1.40045	,	1.74221	,	1.4495	,	1.48406	,	1.37229	,	1.29328	,	1.15302	,	0.740864	,	0.966336	,	1.15776	,	1.46848	,	1.46736	,	1.40422	,	1.32352	,	1.28298	,	1.19878	,	1.15226	,	0.9376	,	1.04192	,	1.35606	,	2.00298	,	2.00755	,	1.7215	,	1.38147	,	1.22454	,	1.20432
RayTracingRLGpu	,	5.43866	,	5.3087	,	5.3961	,	5.2455	,	4.94	,	5.16717	,	4.7433	,	4.18784	,	4.54902	,	4.5865	,	4.65949	,	5.20912	,	4.8465	,	4.91811	,	4.7569	,	4.99043	,	5.19171	,	4.7425	,	4.58381	,	6.32947	,	7.46211	,	6.62368	,	5.87581	,	5.25082	,	4.74173	,	4.87782
ResolveRLGpu	,	3.58986	,	3.34259	,	3.36301	,	3.3488	,	3.34432	,	3.58714	,	3.376	,	3.36291	,	3.38682	,	3.36454	,	3.54979	,	3.51587	,	3.29088	,	3.28099	,	3.5497	,	3.57574	,	3.59344	,	3.36941	,	3.37443	,	3.36605	,	3.59747	,	3.27683	,	3.49574	,	3.49949	,	3.35178	,	3.66083
Index	,	0	,	1	,	2	,	3	,	4	,	5	,	6	,	7	,	8	,	9	,	10	,	11	,	12	,	13	,	14	,	15	,	16	,	17	,	18	,	19	,	20	,	21	,	22	,	23	,	24	,	25

ProfilingBuildAggregator: 
BuildBottomLevelAS	,	4.7197
BuildBottomLevelASGpu	,	15.8534
BuildTopLevelAS	,	2.1973
BuildTopLevelASGpu	,	0.616544
Duplication	,	1
Triangles	,	262267
Meshes	,	103
BottomLevels	,	1
Index	,	0

FpsAggregator: 
FPS	,	57	,	65	,	65	,	65	,	66	,	67	,	69	,	72	,	72	,	71	,	69	,	68	,	66	,	67	,	68	,	69	,	68	,	69	,	70	,	69	,	56	,	57	,	61	,	64	,	69	,	68
UPS	,	59	,	60	,	61	,	60	,	61	,	60	,	61	,	61	,	60	,	60	,	60	,	61	,	60	,	60	,	61	,	60	,	60	,	61	,	60	,	60	,	61	,	61	,	60	,	60	,	61	,	60
FrameTime	,	17.7261	,	15.4237	,	15.4471	,	15.4591	,	15.3288	,	15.1323	,	14.615	,	14.0639	,	13.9299	,	14.1014	,	14.5855	,	14.7902	,	15.2616	,	14.9902	,	14.7346	,	14.5809	,	14.7346	,	14.6068	,	14.4724	,	14.5292	,	18.0352	,	17.7496	,	16.5534	,	15.6443	,	14.6003	,	14.7811
GigaRays/s	,	2.05221	,	2.35855	,	2.35498	,	2.35316	,	2.37316	,	2.40397	,	2.48907	,	2.58659	,	2.61148	,	2.57971	,	2.49409	,	2.45957	,	2.38361	,	2.42676	,	2.46886	,	2.49488	,	2.46885	,	2.49046	,	2.51359	,	2.50376	,	2.01704	,	2.04948	,	2.19759	,	2.32529	,	2.49157	,	2.46109
Index	,	0	,	1	,	2	,	3	,	4	,	5	,	6	,	7	,	8	,	9	,	10	,	11	,	12	,	13	,	14	,	15	,	16	,	17	,	18	,	19	,	20	,	21	,	22	,	23	,	24	,	25


AccelerationStructure: 
Ray tracing acceleration structure building statistics: 
	Bottom Level Acceleration Structure: 
		Maximum [B]: 14623872
		Minimum [B]: 14623872
		Currently Allocated [B]: 14623872
		Currently Created [num]: 1
		Current Average [B]: 14623872
		Maximum Triangles [num]: 262267
		Minimum Triangles [num]: 262267
		Total Triangles [num]: 262267
		Maximum Meshes [num]: 103
		Minimum Meshes [num]: 103
		Total Meshes [num]: 103
	Top Level Acceleration Structure: 
		Maximum [B]: 64
		Minimum [B]: 64
		Currently Allocated [B]: 64
		Total Allocated [B]: 64
		Total Created [num]: 1
		Average Allocated [B]: 64
		Maximum Instances [num]: 1
		Minimum Instances [num]: 1
		Total Instances [num]: 1
	Scratch Buffer: 
		Maximum [B]: 11660800
		Minimum [B]: 11660800
		Currently Allocated [B]: 11660800
		Total Allocated [B]: 11660800
		Total Created [num]: 1
		Average Allocated [B]: 11660800
ProfilingStack: 
[PROF] Profiling statistics: 
	Scopes entered : 22341
	Scopes exited : 22340
	Overhead per scope [ticks] : 1003.9
	Threads entered : 3
	Threads exited : 0
	============================
		         Sum[Mt]   Parent[%]  Samples[num]       Avg[Mt]   Last/Curr[Mt]        Self[Mt]     Self[%] 	 Name

	main:

		   35958.166 Mt   100.000 %         1 x    35958.168 Mt    35958.165 Mt        0.786 Mt     0.002 % 	 /

		   35957.453 Mt    99.998 %         1 x    35957.457 Mt    35957.453 Mt      341.341 Mt     0.949 % 	 ./Main application loop

		       4.305 Mt     0.012 %         1 x        4.305 Mt        0.000 Mt        4.305 Mt   100.000 % 	 ../TexturedRLInitialization

		       7.862 Mt     0.022 %         1 x        7.862 Mt        0.000 Mt        7.862 Mt   100.000 % 	 ../DeferredRLInitialization

		      49.684 Mt     0.138 %         1 x       49.684 Mt        0.000 Mt        2.080 Mt     4.186 % 	 ../RayTracingRLInitialization

		      47.605 Mt    95.814 %         1 x       47.605 Mt        0.000 Mt       47.605 Mt   100.000 % 	 .../PipelineBuild

		      12.267 Mt     0.034 %         1 x       12.267 Mt        0.000 Mt       12.267 Mt   100.000 % 	 ../ResolveRLInitialization

		    9354.901 Mt    26.017 %         1 x     9354.901 Mt        0.000 Mt        7.910 Mt     0.085 % 	 ../Initialization

		    9346.990 Mt    99.915 %         1 x     9346.990 Mt        0.000 Mt        3.543 Mt     0.038 % 	 .../ScenePrep

		    9234.706 Mt    98.799 %         1 x     9234.706 Mt        0.000 Mt      409.521 Mt     4.435 % 	 ..../SceneLoad

		    8301.131 Mt    89.891 %         1 x     8301.131 Mt        0.000 Mt     1104.871 Mt    13.310 % 	 ...../glTF-LoadScene

		    7196.260 Mt    86.690 %        69 x      104.294 Mt        0.000 Mt     7196.260 Mt   100.000 % 	 ....../glTF-LoadImageData

		     341.494 Mt     3.698 %         1 x      341.494 Mt        0.000 Mt      341.494 Mt   100.000 % 	 ...../glTF-CreateScene

		     182.560 Mt     1.977 %         1 x      182.560 Mt        0.000 Mt      182.560 Mt   100.000 % 	 ...../SceneDesc-CreateScene

		       7.041 Mt     0.075 %         1 x        7.041 Mt        0.000 Mt        0.006 Mt     0.081 % 	 ..../TexturedRLPrep

		       7.035 Mt    99.919 %         1 x        7.035 Mt        0.000 Mt        1.966 Mt    27.939 % 	 ...../PrepareForRendering

		       2.713 Mt    38.556 %         1 x        2.713 Mt        0.000 Mt        2.713 Mt   100.000 % 	 ....../PrepareMaterials

		       2.357 Mt    33.505 %         1 x        2.357 Mt        0.000 Mt        2.357 Mt   100.000 % 	 ....../PrepareDrawBundle

		       8.761 Mt     0.094 %         1 x        8.761 Mt        0.000 Mt        0.005 Mt     0.057 % 	 ..../DeferredRLPrep

		       8.757 Mt    99.943 %         1 x        8.757 Mt        0.000 Mt        3.900 Mt    44.543 % 	 ...../PrepareForRendering

		       0.022 Mt     0.250 %         1 x        0.022 Mt        0.000 Mt        0.022 Mt   100.000 % 	 ....../PrepareMaterials

		       2.197 Mt    25.089 %         1 x        2.197 Mt        0.000 Mt        2.197 Mt   100.000 % 	 ....../BuildMaterialCache

		       2.637 Mt    30.118 %         1 x        2.637 Mt        0.000 Mt        2.637 Mt   100.000 % 	 ....../PrepareDrawBundle

		      28.527 Mt     0.305 %         1 x       28.527 Mt        0.000 Mt        6.435 Mt    22.557 % 	 ..../RayTracingRLPrep

		       0.587 Mt     2.057 %         1 x        0.587 Mt        0.000 Mt        0.587 Mt   100.000 % 	 ...../PrepareAccelerationStructures

		       0.025 Mt     0.088 %         1 x        0.025 Mt        0.000 Mt        0.025 Mt   100.000 % 	 ...../PrepareCache

		       5.349 Mt    18.751 %         1 x        5.349 Mt        0.000 Mt        5.349 Mt   100.000 % 	 ...../BuildCache

		      11.867 Mt    41.597 %         1 x       11.867 Mt        0.000 Mt        0.007 Mt     0.061 % 	 ...../BuildAccelerationStructures

		      11.859 Mt    99.939 %         1 x       11.859 Mt        0.000 Mt        4.942 Mt    41.675 % 	 ....../AccelerationStructureBuild

		       4.720 Mt    39.797 %         1 x        4.720 Mt        0.000 Mt        4.720 Mt   100.000 % 	 ......./BuildBottomLevelAS

		       2.197 Mt    18.528 %         1 x        2.197 Mt        0.000 Mt        2.197 Mt   100.000 % 	 ......./BuildTopLevelAS

		       4.265 Mt    14.950 %         1 x        4.265 Mt        0.000 Mt        4.265 Mt   100.000 % 	 ...../BuildShaderTables

		      64.411 Mt     0.689 %         1 x       64.411 Mt        0.000 Mt       64.411 Mt   100.000 % 	 ..../GpuSidePrep

		   26187.094 Mt    72.828 %      5114 x        5.121 Mt       33.082 Mt      162.875 Mt     0.622 % 	 ../MainLoop

		     832.892 Mt     3.181 %      1570 x        0.531 Mt        0.880 Mt      831.898 Mt    99.881 % 	 .../Update

		       0.994 Mt     0.119 %         1 x        0.994 Mt        0.000 Mt        0.994 Mt   100.000 % 	 ..../GuiModelApply

		   25191.328 Mt    96.197 %      1728 x       14.578 Mt       21.994 Mt    19897.061 Mt    78.984 % 	 .../Render

		    5294.266 Mt    21.016 %      1728 x        3.064 Mt        8.886 Mt     5282.189 Mt    99.772 % 	 ..../RayTracing

		       6.022 Mt     0.114 %      1728 x        0.003 Mt        0.009 Mt        6.022 Mt   100.000 % 	 ...../DeferredRLPrep

		       6.056 Mt     0.114 %      1728 x        0.004 Mt        0.014 Mt        6.056 Mt   100.000 % 	 ...../RayTracingRLPrep

	WindowThread:

		   35954.393 Mt   100.000 %         1 x    35954.394 Mt    35954.393 Mt    35954.393 Mt   100.000 % 	 /

	GpuThread:

		   17433.816 Mt   100.000 %      1728 x       10.089 Mt        9.676 Mt      185.980 Mt     1.067 % 	 /

		      16.523 Mt     0.095 %         1 x       16.523 Mt        0.000 Mt        0.051 Mt     0.306 % 	 ./ScenePrep

		      16.472 Mt    99.694 %         1 x       16.472 Mt        0.000 Mt        0.003 Mt     0.015 % 	 ../AccelerationStructureBuild

		      15.853 Mt    96.242 %         1 x       15.853 Mt        0.000 Mt       15.853 Mt   100.000 % 	 .../BuildBottomLevelASGpu

		       0.617 Mt     3.743 %         1 x        0.617 Mt        0.000 Mt        0.617 Mt   100.000 % 	 .../BuildTopLevelASGpu

		   17230.429 Mt    98.833 %      1728 x        9.971 Mt        9.675 Mt        2.591 Mt     0.015 % 	 ./RayTracingGpu

		    2328.493 Mt    13.514 %      1728 x        1.348 Mt        1.214 Mt     2328.493 Mt   100.000 % 	 ../DeferredRLGpu

		    8900.836 Mt    51.658 %      1728 x        5.151 Mt        4.900 Mt     8900.836 Mt   100.000 % 	 ../RayTracingRLGpu

		    5998.508 Mt    34.813 %      1728 x        3.471 Mt        3.558 Mt     5998.508 Mt   100.000 % 	 ../ResolveRLGpu

		       0.884 Mt     0.005 %      1728 x        0.001 Mt        0.000 Mt        0.884 Mt   100.000 % 	 ./Present


	============================


