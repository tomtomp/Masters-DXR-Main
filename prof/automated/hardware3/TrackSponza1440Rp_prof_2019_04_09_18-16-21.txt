Configuration: 
Command line parameters  = E:\programming\FIT-DIP\Masters-DXR-Main-git\build\Measurement\x64\Debug\Measurement.exe --scene Sponza/Sponza.gltf --profile-track Sponza/Sponza.trk --rt-mode-pure --width 2560 --height 1440 --profile-output TrackSponza1440Rp 
Window width             = 2560
Window height            = 1421
Window title             = Direct3D App
Back-buffer count        = 3
Fill Triangles           = enabled
Triangles Font Clock-Wise= enabled
Fast Build AS            = disabled
Target FPS               = 0
Target UPS               = 60
Tearing                  = enabled
VSync                    = disabled
GUI rendering            = disabled
Debug layer              = enabled
GPU validation           = disabled
GPU profiling            = enabled
Render mode              = Ray Tracing
Loaded scene file        = Sponza/Sponza.gltf
Using testing scene      = disabled
Duplication              = 1
Duplication in cube      = disabled
Duplication offset       = 10
BLAS duplication         = disabled
Rays per pixel           = 9

ProfilingAggregator: 
Render	,	11.7468	,	12.1797	,	12.3777	,	12.0612	,	11.4824	,	12.5337	,	11.5169	,	10.3034	,	10.9879	,	10.7736	,	11.0674	,	11.815	,	11.3921	,	12.1165	,	11.1558	,	11.7142	,	11.8004	,	11.683	,	11.1993	,	13.4471	,	14.3299	,	13.2615	,	12.374	,	12.0409	,	11.8504	,	11.7518
Update	,	0.8114	,	0.4402	,	0.3701	,	0.3596	,	0.3479	,	0.4865	,	0.3686	,	0.2993	,	0.3441	,	0.3516	,	0.3585	,	0.3468	,	0.3589	,	0.619	,	0.4031	,	0.3362	,	0.5765	,	0.6739	,	0.3614	,	0.3484	,	0.3723	,	0.3697	,	0.3553	,	0.6796	,	0.5961	,	0.6957
RayTracing	,	9.00397	,	9.59101	,	9.8839	,	9.16016	,	8.80998	,	8.9017	,	8.75542	,	8.21517	,	8.67053	,	8.70586	,	8.50534	,	9.39066	,	8.83178	,	9.54358	,	8.68211	,	9.04576	,	9.11683	,	9.15296	,	8.53088	,	10.7255	,	11.6226	,	10.6739	,	9.69018	,	9.32858	,	9.25117	,	8.77738
RayTracingRL	,	6.26982	,	6.61549	,	6.92262	,	6.40771	,	6.05664	,	5.82976	,	5.66704	,	5.26483	,	5.69008	,	5.73533	,	5.76746	,	6.41318	,	6.0905	,	6.55354	,	5.93827	,	6.07888	,	5.98518	,	6.17088	,	5.7551	,	7.76646	,	8.48323	,	7.93152	,	6.93453	,	6.17818	,	6.27619	,	6.02701
Index	,	0	,	1	,	2	,	3	,	4	,	5	,	6	,	7	,	8	,	9	,	10	,	11	,	12	,	13	,	14	,	15	,	16	,	17	,	18	,	19	,	20	,	21	,	22	,	23	,	24	,	25

ProfilingBuildAggregator: 
BuildBottomLevelAS	,	4.7441
BuildBottomLevelASGpu	,	9.02534
BuildTopLevelAS	,	2.3939
BuildTopLevelASGpu	,	0.089312
Duplication	,	1
Triangles	,	262267
Meshes	,	103
BottomLevels	,	1
Index	,	0

FpsAggregator: 
FPS	,	70	,	81	,	80	,	80	,	81	,	83	,	85	,	90	,	92	,	92	,	90	,	87	,	84	,	82	,	84	,	84	,	84	,	84	,	84	,	84	,	68	,	70	,	74	,	77	,	83	,	83
UPS	,	59	,	61	,	60	,	61	,	60	,	60	,	60	,	60	,	60	,	60	,	61	,	60	,	61	,	60	,	61	,	60	,	60	,	61	,	60	,	61	,	60	,	61	,	61	,	60	,	60	,	61
FrameTime	,	14.4243	,	12.4611	,	12.5315	,	12.5967	,	12.3602	,	12.0962	,	11.7874	,	11.1525	,	10.8733	,	10.909	,	11.214	,	11.5725	,	12.0332	,	12.2836	,	11.9522	,	11.9156	,	12.0304	,	11.9651	,	12.0173	,	12.0079	,	14.9106	,	14.4505	,	13.5718	,	13.0148	,	12.0933	,	12.1877
GigaRays/s	,	2.26978	,	2.62735	,	2.61261	,	2.59909	,	2.64882	,	2.70663	,	2.77753	,	2.93566	,	3.01104	,	3.00118	,	2.91954	,	2.8291	,	2.7208	,	2.66533	,	2.73924	,	2.74765	,	2.72143	,	2.73629	,	2.72438	,	2.72652	,	2.19574	,	2.26565	,	2.41235	,	2.51558	,	2.70728	,	2.68631
Index	,	0	,	1	,	2	,	3	,	4	,	5	,	6	,	7	,	8	,	9	,	10	,	11	,	12	,	13	,	14	,	15	,	16	,	17	,	18	,	19	,	20	,	21	,	22	,	23	,	24	,	25


AccelerationStructure: 
Ray tracing acceleration structure building statistics: 
	Bottom Level Acceleration Structure: 
		Maximum [B]: 14623872
		Minimum [B]: 14623872
		Currently Allocated [B]: 14623872
		Currently Created [num]: 1
		Current Average [B]: 14623872
		Maximum Triangles [num]: 262267
		Minimum Triangles [num]: 262267
		Total Triangles [num]: 262267
		Maximum Meshes [num]: 103
		Minimum Meshes [num]: 103
		Total Meshes [num]: 103
	Top Level Acceleration Structure: 
		Maximum [B]: 64
		Minimum [B]: 64
		Currently Allocated [B]: 64
		Total Allocated [B]: 64
		Total Created [num]: 1
		Average Allocated [B]: 64
		Maximum Instances [num]: 1
		Minimum Instances [num]: 1
		Total Instances [num]: 1
	Scratch Buffer: 
		Maximum [B]: 11660800
		Minimum [B]: 11660800
		Currently Allocated [B]: 11660800
		Total Allocated [B]: 11660800
		Total Created [num]: 1
		Average Allocated [B]: 11660800
ProfilingStack: 
[PROF] Profiling statistics: 
	Scopes entered : 20626
	Scopes exited : 20625
	Overhead per scope [ticks] : 1003.9
	Threads entered : 3
	Threads exited : 0
	============================
		         Sum[Mt]   Parent[%]  Samples[num]       Avg[Mt]   Last/Curr[Mt]        Self[Mt]     Self[%] 	 Name

	main:

		   35807.158 Mt   100.000 %         1 x    35807.162 Mt    35807.156 Mt        0.740 Mt     0.002 % 	 /

		   35806.559 Mt    99.998 %         1 x    35806.564 Mt    35806.558 Mt      326.692 Mt     0.912 % 	 ./Main application loop

		       2.904 Mt     0.008 %         1 x        2.904 Mt        0.000 Mt        2.904 Mt   100.000 % 	 ../TexturedRLInitialization

		       6.703 Mt     0.019 %         1 x        6.703 Mt        0.000 Mt        6.703 Mt   100.000 % 	 ../DeferredRLInitialization

		      47.870 Mt     0.134 %         1 x       47.870 Mt        0.000 Mt        2.448 Mt     5.115 % 	 ../RayTracingRLInitialization

		      45.421 Mt    94.885 %         1 x       45.421 Mt        0.000 Mt       45.421 Mt   100.000 % 	 .../PipelineBuild

		      11.984 Mt     0.033 %         1 x       11.984 Mt        0.000 Mt       11.984 Mt   100.000 % 	 ../ResolveRLInitialization

		    9230.227 Mt    25.778 %         1 x     9230.227 Mt        0.000 Mt        7.576 Mt     0.082 % 	 ../Initialization

		    9222.651 Mt    99.918 %         1 x     9222.651 Mt        0.000 Mt        3.513 Mt     0.038 % 	 .../ScenePrep

		    9114.377 Mt    98.826 %         1 x     9114.377 Mt        0.000 Mt      357.477 Mt     3.922 % 	 ..../SceneLoad

		    8160.144 Mt    89.530 %         1 x     8160.144 Mt        0.000 Mt     1081.461 Mt    13.253 % 	 ...../glTF-LoadScene

		    7078.683 Mt    86.747 %        69 x      102.590 Mt        0.000 Mt     7078.683 Mt   100.000 % 	 ....../glTF-LoadImageData

		     408.979 Mt     4.487 %         1 x      408.979 Mt        0.000 Mt      408.979 Mt   100.000 % 	 ...../glTF-CreateScene

		     187.776 Mt     2.060 %         1 x      187.776 Mt        0.000 Mt      187.776 Mt   100.000 % 	 ...../SceneDesc-CreateScene

		       7.158 Mt     0.078 %         1 x        7.158 Mt        0.000 Mt        0.005 Mt     0.075 % 	 ..../TexturedRLPrep

		       7.153 Mt    99.925 %         1 x        7.153 Mt        0.000 Mt        2.103 Mt    29.394 % 	 ...../PrepareForRendering

		       2.684 Mt    37.520 %         1 x        2.684 Mt        0.000 Mt        2.684 Mt   100.000 % 	 ....../PrepareMaterials

		       2.366 Mt    33.085 %         1 x        2.366 Mt        0.000 Mt        2.366 Mt   100.000 % 	 ....../PrepareDrawBundle

		       9.466 Mt     0.103 %         1 x        9.466 Mt        0.000 Mt        0.007 Mt     0.071 % 	 ..../DeferredRLPrep

		       9.460 Mt    99.929 %         1 x        9.460 Mt        0.000 Mt        4.523 Mt    47.813 % 	 ...../PrepareForRendering

		       0.023 Mt     0.239 %         1 x        0.023 Mt        0.000 Mt        0.023 Mt   100.000 % 	 ....../PrepareMaterials

		       2.279 Mt    24.094 %         1 x        2.279 Mt        0.000 Mt        2.279 Mt   100.000 % 	 ....../BuildMaterialCache

		       2.635 Mt    27.854 %         1 x        2.635 Mt        0.000 Mt        2.635 Mt   100.000 % 	 ....../PrepareDrawBundle

		      37.892 Mt     0.411 %         1 x       37.892 Mt        0.000 Mt        7.460 Mt    19.687 % 	 ..../RayTracingRLPrep

		       0.588 Mt     1.552 %         1 x        0.588 Mt        0.000 Mt        0.588 Mt   100.000 % 	 ...../PrepareAccelerationStructures

		       0.025 Mt     0.066 %         1 x        0.025 Mt        0.000 Mt        0.025 Mt   100.000 % 	 ...../PrepareCache

		       8.206 Mt    21.656 %         1 x        8.206 Mt        0.000 Mt        8.206 Mt   100.000 % 	 ...../BuildCache

		      12.138 Mt    32.032 %         1 x       12.138 Mt        0.000 Mt        0.007 Mt     0.059 % 	 ...../BuildAccelerationStructures

		      12.130 Mt    99.941 %         1 x       12.130 Mt        0.000 Mt        4.992 Mt    41.156 % 	 ....../AccelerationStructureBuild

		       4.744 Mt    39.109 %         1 x        4.744 Mt        0.000 Mt        4.744 Mt   100.000 % 	 ......./BuildBottomLevelAS

		       2.394 Mt    19.735 %         1 x        2.394 Mt        0.000 Mt        2.394 Mt   100.000 % 	 ......./BuildTopLevelAS

		       9.476 Mt    25.008 %         1 x        9.476 Mt        0.000 Mt        9.476 Mt   100.000 % 	 ...../BuildShaderTables

		      50.244 Mt     0.545 %         1 x       50.244 Mt        0.000 Mt       50.244 Mt   100.000 % 	 ..../GpuSidePrep

		   26180.179 Mt    73.116 %      6122 x        4.276 Mt       16.432 Mt      145.151 Mt     0.554 % 	 ../MainLoop

		     781.362 Mt     2.985 %      1571 x        0.497 Mt        0.556 Mt      780.407 Mt    99.878 % 	 .../Update

		       0.955 Mt     0.122 %         1 x        0.955 Mt        0.000 Mt        0.955 Mt   100.000 % 	 ..../GuiModelApply

		   25253.666 Mt    96.461 %      2138 x       11.812 Mt       15.245 Mt    25244.347 Mt    99.963 % 	 .../Render

		       9.319 Mt     0.037 %      2138 x        0.004 Mt        0.008 Mt        9.319 Mt   100.000 % 	 ..../RayTracingRLPrep

	WindowThread:

		   35805.828 Mt   100.000 %         1 x    35805.830 Mt    35805.827 Mt    35805.828 Mt   100.000 % 	 /

	GpuThread:

		   19793.245 Mt   100.000 %      2138 x        9.258 Mt        9.133 Mt      159.159 Mt     0.804 % 	 /

		       9.168 Mt     0.046 %         1 x        9.168 Mt        0.000 Mt        0.052 Mt     0.563 % 	 ./ScenePrep

		       9.116 Mt    99.437 %         1 x        9.116 Mt        0.000 Mt        0.001 Mt     0.015 % 	 ../AccelerationStructureBuild

		       9.025 Mt    99.005 %         1 x        9.025 Mt        0.000 Mt        9.025 Mt   100.000 % 	 .../BuildBottomLevelASGpu

		       0.089 Mt     0.980 %         1 x        0.089 Mt        0.000 Mt        0.089 Mt   100.000 % 	 .../BuildTopLevelASGpu

		   19623.891 Mt    99.144 %      2138 x        9.179 Mt        9.132 Mt        2.431 Mt     0.012 % 	 ./RayTracing

		   13419.394 Mt    68.383 %      2138 x        6.277 Mt        6.110 Mt    13419.394 Mt   100.000 % 	 ../RayTracingRL

		    6202.066 Mt    31.605 %      2138 x        2.901 Mt        3.021 Mt     6202.066 Mt   100.000 % 	 ../ResolveRL

		       1.027 Mt     0.005 %      2138 x        0.000 Mt        0.000 Mt        1.027 Mt   100.000 % 	 ./Present


	============================


