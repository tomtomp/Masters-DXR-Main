Configuration: 
Command line parameters  = E:\programming\FIT-DIP\Masters-DXR-Main-git\build\Measurement\x64\Debug\Measurement.exe --scene Suzanne/Suzanne.gltf --duplication-cube --profile-duplication 10 --rt-mode --width 1024 --height 768 --profile-output DuplicationSuzanne768Rf --fast-build-as 
Window width             = 1024
Window height            = 768
Window title             = Direct3D App
Back-buffer count        = 3
Fill Triangles           = enabled
Triangles Font Clock-Wise= enabled
Fast Build AS            = enabled
Target FPS               = 60
Target UPS               = 60
Tearing                  = disabled
VSync                    = disabled
GUI rendering            = disabled
Debug layer              = enabled
GPU validation           = disabled
GPU profiling            = enabled
Render mode              = Ray Tracing
Loaded scene file        = Suzanne/Suzanne.gltf
Using testing scene      = disabled
Duplication              = 10
Duplication in cube      = enabled
Duplication offset       = 10
BLAS duplication         = enabled
Rays per pixel           = 10

ProfilingAggregator: 
Render	,	3.404	,	3.3153	,	5.9593	,	7.6285	,	3.4997	,	6.5255	,	5.9933	,	8.4129	,	4.3372	,	4.3072	,	5.0144	,	5.0802	,	6.141	,	5.9712	,	7.1268	,	8.2773	,	10.6397	,	8.7247	,	894.181	,	10.6283
Update	,	0.3017	,	0.2656	,	0.2356	,	0.2372	,	0.2873	,	0.381	,	0.3082	,	0.3688	,	0.3901	,	0.3772	,	1.0764	,	0.4957	,	0.5946	,	0.5849	,	0.7655	,	0.9608	,	0.9353	,	1.5324	,	1.2023	,	1.4478
RayTracing	,	0.921824	,	0.9248	,	2.91702	,	4.27443	,	0.964864	,	2.8801	,	2.6215	,	4.64288	,	1.08323	,	1.08243	,	1.19741	,	1.19734	,	1.33866	,	1.332	,	1.37693	,	1.37264	,	1.51331	,	1.47274	,	405.735	,	1.46016
RayTracingRL	,	0.100736	,	0.104928	,	0.558944	,	1.36138	,	0.137024	,	0.5904	,	0.649632	,	1.84083	,	0.173728	,	0.171648	,	0.208608	,	0.208096	,	0.237472	,	0.23232	,	0.24816	,	0.24544	,	0.279712	,	0.24928	,	0.296768	,	0.29408
Index	,	0	,	1	,	2	,	3	,	4	,	5	,	6	,	7	,	8	,	9	,	10	,	11	,	12	,	13	,	14	,	15	,	16	,	17	,	18	,	19

ProfilingBuildAggregator: 
BuildBottomLevelAS	,	0.8571	,	1.8758	,	9.1768	,	15.0063	,	27.0549	,	52.3841	,	82.1255	,	121.732	,	171.92	,	215.723
BuildBottomLevelASGpu	,	0.357056	,	2.70762	,	34.287	,	60.3583	,	45.0603	,	190.896	,	275.778	,	324.64	,	372.926	,	402.852
BuildTopLevelAS	,	0.9088	,	0.5896	,	0.7574	,	0.6243	,	0.6583	,	0.7028	,	0.9283	,	0.7606	,	0.7957	,	0.7069
BuildTopLevelASGpu	,	0.086304	,	0.090912	,	0.4544	,	0.35664	,	0.19968	,	0.487552	,	0.199872	,	0.160096	,	0.212704	,	0.183872
Duplication	,	1	,	2	,	3	,	4	,	5	,	6	,	7	,	8	,	9	,	10
Triangles	,	3936	,	31488	,	106272	,	251904	,	492000	,	850176	,	1350048	,	2015232	,	2869344	,	3936000
Meshes	,	1	,	8	,	27	,	64	,	125	,	216	,	343	,	512	,	729	,	1000
BottomLevels	,	1	,	8	,	27	,	64	,	125	,	216	,	343	,	512	,	729	,	1000
Index	,	0	,	1	,	2	,	3	,	4	,	5	,	6	,	7	,	8	,	9

FpsAggregator: 
FPS	,	57	,	59	,	60	,	60	,	54	,	60	,	48	,	60	,	40	,	60	,	38	,	60	,	27	,	60	,	14	,	60	,	2	,	60	,	2	,	55
UPS	,	59	,	61	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	59	,	64	,	10	,	124
FrameTime	,	17.5973	,	16.9492	,	16.6667	,	16.6667	,	18.5599	,	16.7189	,	20.8802	,	16.7091	,	25.0001	,	16.6667	,	26.3158	,	16.6667	,	37.045	,	16.6667	,	71.4286	,	16.6667	,	523.34	,	16.6667	,	614.364	,	18.1819
GigaRays/s	,	0.446905	,	0.463995	,	0.471858	,	0.471859	,	0.423726	,	0.470385	,	0.37664	,	0.470662	,	0.314572	,	0.471859	,	0.298844	,	0.471858	,	0.212291	,	0.471859	,	0.1101	,	0.471858	,	0.0150272	,	0.471858	,	0.0128008	,	0.432536
Index	,	0	,	1	,	2	,	3	,	4	,	5	,	6	,	7	,	8	,	9	,	10	,	11	,	12	,	13	,	14	,	15	,	16	,	17	,	18	,	19


AccelerationStructure: 
Ray tracing acceleration structure building statistics: 
	Bottom Level Acceleration Structure: 
		Maximum [B]: 224128
		Minimum [B]: 224128
		Currently Allocated [B]: 224128000
		Currently Created [num]: 1000
		Current Average [B]: 224128
		Maximum Triangles [num]: 3936
		Minimum Triangles [num]: 3936
		Total Triangles [num]: 3936000
		Maximum Meshes [num]: 1
		Minimum Meshes [num]: 1
		Total Meshes [num]: 1000
	Top Level Acceleration Structure: 
		Maximum [B]: 64000
		Minimum [B]: 64000
		Currently Allocated [B]: 64000
		Total Allocated [B]: 64000
		Total Created [num]: 1
		Average Allocated [B]: 64000
		Maximum Instances [num]: 1000
		Minimum Instances [num]: 1000
		Total Instances [num]: 1000
	Scratch Buffer: 
		Maximum [B]: 79360
		Minimum [B]: 79360
		Currently Allocated [B]: 79360
		Total Allocated [B]: 79360
		Total Created [num]: 1
		Average Allocated [B]: 79360
ProfilingStack: 
[PROF] Profiling statistics: 
	Scopes entered : 2469699
	Scopes exited : 2469698
	Overhead per scope [ticks] : 1017.9
	Threads entered : 3
	Threads exited : 0
	============================
		         Sum[Mt]   Parent[%]  Samples[num]       Avg[Mt]   Last/Curr[Mt]        Self[Mt]     Self[%] 	 Name

	main:

		   21555.712 Mt   100.000 %         1 x    21555.713 Mt    21555.712 Mt        1.008 Mt     0.005 % 	 /

		   21554.747 Mt    99.996 %         1 x    21554.749 Mt    21554.747 Mt     6050.140 Mt    28.069 % 	 ./Main application loop

		       2.839 Mt     0.013 %         1 x        2.839 Mt        0.000 Mt        2.839 Mt   100.000 % 	 ../TexturedRLInitialization

		       6.713 Mt     0.031 %         1 x        6.713 Mt        0.000 Mt        6.713 Mt   100.000 % 	 ../DeferredRLInitialization

		      47.307 Mt     0.219 %         1 x       47.307 Mt        0.000 Mt        2.482 Mt     5.247 % 	 ../RayTracingRLInitialization

		      44.825 Mt    94.753 %         1 x       44.825 Mt        0.000 Mt       44.825 Mt   100.000 % 	 .../PipelineBuild

		      11.917 Mt     0.055 %         1 x       11.917 Mt        0.000 Mt       11.917 Mt   100.000 % 	 ../ResolveRLInitialization

		     224.824 Mt     1.043 %         1 x      224.824 Mt        0.000 Mt        2.191 Mt     0.975 % 	 ../Initialization

		     222.632 Mt    99.025 %         1 x      222.632 Mt        0.000 Mt        1.435 Mt     0.644 % 	 .../ScenePrep

		     191.885 Mt    86.189 %         1 x      191.885 Mt        0.000 Mt       16.831 Mt     8.772 % 	 ..../SceneLoad

		     163.939 Mt    85.436 %         1 x      163.939 Mt        0.000 Mt       19.065 Mt    11.630 % 	 ...../glTF-LoadScene

		     144.873 Mt    88.370 %         2 x       72.437 Mt        0.000 Mt      144.873 Mt   100.000 % 	 ....../glTF-LoadImageData

		       6.858 Mt     3.574 %         1 x        6.858 Mt        0.000 Mt        6.858 Mt   100.000 % 	 ...../glTF-CreateScene

		       4.256 Mt     2.218 %         1 x        4.256 Mt        0.000 Mt        4.256 Mt   100.000 % 	 ...../SceneDesc-CreateScene

		       2.065 Mt     0.928 %         1 x        2.065 Mt        0.000 Mt        0.016 Mt     0.755 % 	 ..../TexturedRLPrep

		       2.050 Mt    99.245 %         1 x        2.050 Mt        0.000 Mt        0.569 Mt    27.770 % 	 ...../PrepareForRendering

		       0.574 Mt    27.980 %         1 x        0.574 Mt        0.000 Mt        0.574 Mt   100.000 % 	 ....../PrepareMaterials

		       0.907 Mt    44.250 %         1 x        0.907 Mt        0.000 Mt        0.907 Mt   100.000 % 	 ....../PrepareDrawBundle

		       2.838 Mt     1.275 %         1 x        2.838 Mt        0.000 Mt        0.014 Mt     0.497 % 	 ..../DeferredRLPrep

		       2.824 Mt    99.503 %         1 x        2.824 Mt        0.000 Mt        2.055 Mt    72.763 % 	 ...../PrepareForRendering

		       0.053 Mt     1.863 %         1 x        0.053 Mt        0.000 Mt        0.053 Mt   100.000 % 	 ....../PrepareMaterials

		       0.458 Mt    16.218 %         1 x        0.458 Mt        0.000 Mt        0.458 Mt   100.000 % 	 ....../BuildMaterialCache

		       0.259 Mt     9.157 %         1 x        0.259 Mt        0.000 Mt        0.259 Mt   100.000 % 	 ....../PrepareDrawBundle

		      12.206 Mt     5.482 %         1 x       12.206 Mt        0.000 Mt        3.927 Mt    32.175 % 	 ..../RayTracingRLPrep

		       0.244 Mt     2.002 %         1 x        0.244 Mt        0.000 Mt        0.244 Mt   100.000 % 	 ...../PrepareAccelerationStructures

		       0.024 Mt     0.196 %         1 x        0.024 Mt        0.000 Mt        0.024 Mt   100.000 % 	 ...../PrepareCache

		       0.463 Mt     3.796 %         1 x        0.463 Mt        0.000 Mt        0.463 Mt   100.000 % 	 ...../BuildCache

		       5.873 Mt    48.119 %         1 x        5.873 Mt        0.000 Mt        0.006 Mt     0.107 % 	 ...../BuildAccelerationStructures

		       5.867 Mt    99.893 %         1 x        5.867 Mt        0.000 Mt        4.101 Mt    69.901 % 	 ....../AccelerationStructureBuild

		       0.857 Mt    14.609 %         1 x        0.857 Mt        0.000 Mt        0.857 Mt   100.000 % 	 ......./BuildBottomLevelAS

		       0.909 Mt    15.490 %         1 x        0.909 Mt        0.000 Mt        0.909 Mt   100.000 % 	 ......./BuildTopLevelAS

		       1.674 Mt    13.712 %         1 x        1.674 Mt        0.000 Mt        1.674 Mt   100.000 % 	 ...../BuildShaderTables

		      12.204 Mt     5.482 %         1 x       12.204 Mt        0.000 Mt       12.204 Mt   100.000 % 	 ..../GpuSidePrep

		   15211.007 Mt    70.569 %   2460752 x        0.006 Mt        1.488 Mt     4203.107 Mt    27.632 % 	 ../MainLoop

		    1747.076 Mt    11.486 %      1218 x        1.434 Mt        1.480 Mt     1746.233 Mt    99.952 % 	 .../Update

		       0.844 Mt     0.048 %         1 x        0.844 Mt        0.000 Mt        0.844 Mt   100.000 % 	 ..../GuiModelApply

		    9260.824 Mt    60.882 %       937 x        9.883 Mt       11.250 Mt     7555.890 Mt    81.590 % 	 .../Render

		      92.648 Mt     1.000 %       937 x        0.099 Mt        0.007 Mt        2.697 Mt     2.911 % 	 ..../DeferredRLPrep

		      89.952 Mt    97.089 %        13 x        6.919 Mt        0.000 Mt        4.596 Mt     5.110 % 	 ...../PrepareForRendering

		       8.719 Mt     9.693 %        13 x        0.671 Mt        0.000 Mt        8.719 Mt   100.000 % 	 ....../PrepareMaterials

		       0.010 Mt     0.011 %        13 x        0.001 Mt        0.000 Mt        0.010 Mt   100.000 % 	 ....../BuildMaterialCache

		      76.626 Mt    85.186 %        13 x        5.894 Mt        0.000 Mt       76.626 Mt   100.000 % 	 ....../PrepareDrawBundle

		    1612.286 Mt    17.410 %       937 x        1.721 Mt        0.009 Mt       23.579 Mt     1.462 % 	 ..../RayTracingRLPrep

		     699.301 Mt    43.373 %        13 x       53.792 Mt        0.000 Mt      699.301 Mt   100.000 % 	 ...../PrepareAccelerationStructures

		       8.691 Mt     0.539 %        13 x        0.669 Mt        0.000 Mt        8.691 Mt   100.000 % 	 ...../PrepareCache

		       0.009 Mt     0.001 %        13 x        0.001 Mt        0.000 Mt        0.009 Mt   100.000 % 	 ...../BuildCache

		     807.504 Mt    50.084 %        13 x       62.116 Mt        0.000 Mt        0.032 Mt     0.004 % 	 ...../BuildAccelerationStructures

		     807.472 Mt    99.996 %        13 x       62.113 Mt        0.000 Mt       32.118 Mt     3.978 % 	 ....../AccelerationStructureBuild

		     765.676 Mt    94.824 %        13 x       58.898 Mt        0.000 Mt      765.676 Mt   100.000 % 	 ......./BuildBottomLevelAS

		       9.678 Mt     1.199 %        13 x        0.744 Mt        0.000 Mt        9.678 Mt   100.000 % 	 ......./BuildTopLevelAS

		      73.203 Mt     4.540 %        13 x        5.631 Mt        0.000 Mt       73.203 Mt   100.000 % 	 ...../BuildShaderTables

	WindowThread:

		   21550.414 Mt   100.000 %         1 x    21550.414 Mt    21550.414 Mt    21550.414 Mt   100.000 % 	 /

	GpuThread:

		    3748.032 Mt   100.000 %       937 x        4.000 Mt        1.457 Mt      105.092 Mt     2.804 % 	 /

		       0.451 Mt     0.012 %         1 x        0.451 Mt        0.000 Mt        0.006 Mt     1.403 % 	 ./ScenePrep

		       0.445 Mt    98.597 %         1 x        0.445 Mt        0.000 Mt        0.002 Mt     0.403 % 	 ../AccelerationStructureBuild

		       0.357 Mt    80.210 %         1 x        0.357 Mt        0.000 Mt        0.357 Mt   100.000 % 	 .../BuildBottomLevelASGpu

		       0.086 Mt    19.388 %         1 x        0.086 Mt        0.000 Mt        0.086 Mt   100.000 % 	 .../BuildTopLevelASGpu

		    3641.531 Mt    97.158 %       937 x        3.886 Mt        1.456 Mt        3.055 Mt     0.084 % 	 ./RayTracing

		     197.328 Mt     5.419 %       937 x        0.211 Mt        0.610 Mt      197.328 Mt   100.000 % 	 ../DeferredRL

		     396.278 Mt    10.882 %       937 x        0.423 Mt        0.291 Mt      396.278 Mt   100.000 % 	 ../RayTracingRL

		    1128.362 Mt    30.986 %       937 x        1.204 Mt        0.553 Mt     1128.362 Mt   100.000 % 	 ../ResolveRL

		    1916.509 Mt    52.629 %        13 x      147.424 Mt        0.000 Mt        0.029 Mt     0.002 % 	 ../AccelerationStructureBuild

		    1912.753 Mt    99.804 %        13 x      147.135 Mt        0.000 Mt     1912.753 Mt   100.000 % 	 .../BuildBottomLevelASGpu

		       3.726 Mt     0.194 %        13 x        0.287 Mt        0.000 Mt        3.726 Mt   100.000 % 	 .../BuildTopLevelASGpu

		       0.958 Mt     0.026 %       937 x        0.001 Mt        0.000 Mt        0.958 Mt   100.000 % 	 ./Present


	============================


