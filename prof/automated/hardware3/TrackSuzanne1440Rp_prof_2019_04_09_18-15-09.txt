Configuration: 
Command line parameters  = E:\programming\FIT-DIP\Masters-DXR-Main-git\build\Measurement\x64\Debug\Measurement.exe --scene Suzanne/Suzanne.gltf --profile-track Suzanne/Suzanne.trk --rt-mode-pure --width 2560 --height 1440 --profile-output TrackSuzanne1440Rp 
Window width             = 2560
Window height            = 1421
Window title             = Direct3D App
Back-buffer count        = 3
Fill Triangles           = enabled
Triangles Font Clock-Wise= enabled
Fast Build AS            = disabled
Target FPS               = 0
Target UPS               = 60
Tearing                  = enabled
VSync                    = disabled
GUI rendering            = disabled
Debug layer              = enabled
GPU validation           = disabled
GPU profiling            = enabled
Render mode              = Ray Tracing
Loaded scene file        = Suzanne/Suzanne.gltf
Using testing scene      = disabled
Duplication              = 1
Duplication in cube      = disabled
Duplication offset       = 10
BLAS duplication         = disabled
Rays per pixel           = 9

ProfilingAggregator: 
Render	,	7.8628	,	6.8714	,	6.3059	,	8.7152	,	7.2173	,	8.1019	,	5.7334	,	6.4306	,	8.4022	,	6.9326	,	6.2642	,	7.7032	,	6.8791	,	6.9777	,	7.7878	,	9.1835	,	8.5113	,	9.9027	,	11.9785	,	8.3626	,	8.9098	,	6.4334	,	6.6669	,	7.1492
Update	,	0.8844	,	0.6837	,	0.3926	,	0.553	,	0.6801	,	0.8271	,	0.9333	,	0.856	,	0.9393	,	0.7129	,	0.5035	,	0.8582	,	0.6874	,	0.801	,	0.8289	,	0.9141	,	0.7822	,	0.8781	,	0.922	,	0.6439	,	0.8783	,	0.5185	,	0.6899	,	0.6453
RayTracing	,	3.80323	,	3.97184	,	4.15123	,	3.90954	,	3.51626	,	3.2777	,	3.15517	,	3.35565	,	3.25104	,	3.4985	,	3.6385	,	3.7177	,	3.19795	,	3.18058	,	3.40835	,	4.04848	,	4.55306	,	5.67514	,	6.84016	,	5.27782	,	4.46221	,	3.55053	,	3.63472	,	3.22758
RayTracingRL	,	0.858112	,	1.21677	,	1.41837	,	1.17635	,	0.77968	,	0.525312	,	0.424896	,	0.3816	,	0.498272	,	0.759136	,	0.905632	,	0.659136	,	0.443904	,	0.448832	,	0.675424	,	1.31555	,	1.81901	,	2.58653	,	4.10794	,	2.54563	,	1.72998	,	0.820768	,	0.52896	,	0.475328
Index	,	0	,	1	,	2	,	3	,	4	,	5	,	6	,	7	,	8	,	9	,	10	,	11	,	12	,	13	,	14	,	15	,	16	,	17	,	18	,	19	,	20	,	21	,	22	,	23

ProfilingBuildAggregator: 
BuildBottomLevelAS	,	1.056
BuildBottomLevelASGpu	,	0.358048
BuildTopLevelAS	,	2.5847
BuildTopLevelASGpu	,	0.082688
Duplication	,	1
Triangles	,	3936
Meshes	,	1
BottomLevels	,	1
Index	,	0

FpsAggregator: 
FPS	,	109	,	121	,	130	,	124	,	122	,	132	,	142	,	144	,	143	,	133	,	129	,	131	,	138	,	146	,	142	,	130	,	109	,	102	,	89	,	89	,	103	,	121	,	134	,	139
UPS	,	59	,	60	,	60	,	61	,	60	,	60	,	60	,	60	,	61	,	60	,	60	,	61	,	60	,	60	,	60	,	60	,	61	,	60	,	60	,	60	,	61	,	60	,	60	,	60
FrameTime	,	9.21072	,	8.26938	,	7.70451	,	8.10231	,	8.21453	,	7.59275	,	7.06882	,	6.97008	,	7.05504	,	7.56258	,	7.75952	,	7.66528	,	7.25949	,	6.85226	,	7.05732	,	7.74273	,	9.23389	,	9.85139	,	11.293	,	11.2912	,	9.73577	,	8.27783	,	7.50473	,	7.23409
GigaRays/s	,	3.55454	,	3.95916	,	4.24944	,	4.0408	,	3.9856	,	4.31199	,	4.63158	,	4.6972	,	4.64063	,	4.32919	,	4.21931	,	4.27119	,	4.50994	,	4.77796	,	4.63913	,	4.22846	,	3.54562	,	3.32337	,	2.89914	,	2.89959	,	3.36284	,	3.95512	,	4.36256	,	4.52577
Index	,	0	,	1	,	2	,	3	,	4	,	5	,	6	,	7	,	8	,	9	,	10	,	11	,	12	,	13	,	14	,	15	,	16	,	17	,	18	,	19	,	20	,	21	,	22	,	23


AccelerationStructure: 
Ray tracing acceleration structure building statistics: 
	Bottom Level Acceleration Structure: 
		Maximum [B]: 224128
		Minimum [B]: 224128
		Currently Allocated [B]: 224128
		Currently Created [num]: 1
		Current Average [B]: 224128
		Maximum Triangles [num]: 3936
		Minimum Triangles [num]: 3936
		Total Triangles [num]: 3936
		Maximum Meshes [num]: 1
		Minimum Meshes [num]: 1
		Total Meshes [num]: 1
	Top Level Acceleration Structure: 
		Maximum [B]: 64
		Minimum [B]: 64
		Currently Allocated [B]: 64
		Total Allocated [B]: 64
		Total Created [num]: 1
		Average Allocated [B]: 64
		Maximum Instances [num]: 1
		Minimum Instances [num]: 1
		Total Instances [num]: 1
	Scratch Buffer: 
		Maximum [B]: 34304
		Minimum [B]: 34304
		Currently Allocated [B]: 34304
		Total Allocated [B]: 34304
		Total Created [num]: 1
		Average Allocated [B]: 34304
ProfilingStack: 
[PROF] Profiling statistics: 
	Scopes entered : 26754
	Scopes exited : 26753
	Overhead per scope [ticks] : 1012.9
	Threads entered : 3
	Threads exited : 0
	============================
		         Sum[Mt]   Parent[%]  Samples[num]       Avg[Mt]   Last/Curr[Mt]        Self[Mt]     Self[%] 	 Name

	main:

		   24811.050 Mt   100.000 %         1 x    24811.054 Mt    24811.049 Mt        0.709 Mt     0.003 % 	 /

		   24810.490 Mt    99.998 %         1 x    24810.496 Mt    24810.489 Mt      383.943 Mt     1.548 % 	 ./Main application loop

		       4.530 Mt     0.018 %         1 x        4.530 Mt        0.000 Mt        4.530 Mt   100.000 % 	 ../TexturedRLInitialization

		       8.363 Mt     0.034 %         1 x        8.363 Mt        0.000 Mt        8.363 Mt   100.000 % 	 ../DeferredRLInitialization

		      59.751 Mt     0.241 %         1 x       59.751 Mt        0.000 Mt        2.278 Mt     3.813 % 	 ../RayTracingRLInitialization

		      57.473 Mt    96.187 %         1 x       57.473 Mt        0.000 Mt       57.473 Mt   100.000 % 	 .../PipelineBuild

		      11.923 Mt     0.048 %         1 x       11.923 Mt        0.000 Mt       11.923 Mt   100.000 % 	 ../ResolveRLInitialization

		     259.897 Mt     1.048 %         1 x      259.897 Mt        0.000 Mt        4.450 Mt     1.712 % 	 ../Initialization

		     255.447 Mt    98.288 %         1 x      255.447 Mt        0.000 Mt        1.774 Mt     0.694 % 	 .../ScenePrep

		     219.358 Mt    85.872 %         1 x      219.358 Mt        0.000 Mt       22.709 Mt    10.352 % 	 ..../SceneLoad

		     182.662 Mt    83.271 %         1 x      182.662 Mt        0.000 Mt       35.834 Mt    19.618 % 	 ...../glTF-LoadScene

		     146.827 Mt    80.382 %         2 x       73.414 Mt        0.000 Mt      146.827 Mt   100.000 % 	 ....../glTF-LoadImageData

		       7.938 Mt     3.619 %         1 x        7.938 Mt        0.000 Mt        7.938 Mt   100.000 % 	 ...../glTF-CreateScene

		       6.049 Mt     2.758 %         1 x        6.049 Mt        0.000 Mt        6.049 Mt   100.000 % 	 ...../SceneDesc-CreateScene

		       1.971 Mt     0.772 %         1 x        1.971 Mt        0.000 Mt        0.015 Mt     0.776 % 	 ..../TexturedRLPrep

		       1.956 Mt    99.224 %         1 x        1.956 Mt        0.000 Mt        0.486 Mt    24.838 % 	 ...../PrepareForRendering

		       0.560 Mt    28.631 %         1 x        0.560 Mt        0.000 Mt        0.560 Mt   100.000 % 	 ....../PrepareMaterials

		       0.910 Mt    46.531 %         1 x        0.910 Mt        0.000 Mt        0.910 Mt   100.000 % 	 ....../PrepareDrawBundle

		       4.824 Mt     1.888 %         1 x        4.824 Mt        0.000 Mt        0.014 Mt     0.294 % 	 ..../DeferredRLPrep

		       4.809 Mt    99.706 %         1 x        4.809 Mt        0.000 Mt        2.493 Mt    51.831 % 	 ...../PrepareForRendering

		       0.050 Mt     1.048 %         1 x        0.050 Mt        0.000 Mt        0.050 Mt   100.000 % 	 ....../PrepareMaterials

		       2.011 Mt    41.815 %         1 x        2.011 Mt        0.000 Mt        2.011 Mt   100.000 % 	 ....../BuildMaterialCache

		       0.255 Mt     5.306 %         1 x        0.255 Mt        0.000 Mt        0.255 Mt   100.000 % 	 ....../PrepareDrawBundle

		      15.674 Mt     6.136 %         1 x       15.674 Mt        0.000 Mt        6.285 Mt    40.100 % 	 ..../RayTracingRLPrep

		       0.233 Mt     1.487 %         1 x        0.233 Mt        0.000 Mt        0.233 Mt   100.000 % 	 ...../PrepareAccelerationStructures

		       0.023 Mt     0.147 %         1 x        0.023 Mt        0.000 Mt        0.023 Mt   100.000 % 	 ...../PrepareCache

		       0.527 Mt     3.362 %         1 x        0.527 Mt        0.000 Mt        0.527 Mt   100.000 % 	 ...../BuildCache

		       6.302 Mt    40.208 %         1 x        6.302 Mt        0.000 Mt        0.017 Mt     0.268 % 	 ...../BuildAccelerationStructures

		       6.285 Mt    99.732 %         1 x        6.285 Mt        0.000 Mt        2.645 Mt    42.077 % 	 ....../AccelerationStructureBuild

		       1.056 Mt    16.801 %         1 x        1.056 Mt        0.000 Mt        1.056 Mt   100.000 % 	 ......./BuildBottomLevelAS

		       2.585 Mt    41.122 %         1 x        2.585 Mt        0.000 Mt        2.585 Mt   100.000 % 	 ......./BuildTopLevelAS

		       2.304 Mt    14.697 %         1 x        2.304 Mt        0.000 Mt        2.304 Mt   100.000 % 	 ...../BuildShaderTables

		      11.847 Mt     4.638 %         1 x       11.847 Mt        0.000 Mt       11.847 Mt   100.000 % 	 ..../GpuSidePrep

		   24082.082 Mt    97.064 %      7253 x        3.320 Mt       24.758 Mt      319.240 Mt     1.326 % 	 ../MainLoop

		    1178.291 Mt     4.893 %      1445 x        0.815 Mt        0.480 Mt     1175.599 Mt    99.772 % 	 .../Update

		       2.692 Mt     0.228 %         1 x        2.692 Mt        0.000 Mt        2.692 Mt   100.000 % 	 ..../GuiModelApply

		   22584.552 Mt    93.782 %      3003 x        7.521 Mt        6.324 Mt    22558.828 Mt    99.886 % 	 .../Render

		      25.724 Mt     0.114 %      3003 x        0.009 Mt        0.006 Mt       25.724 Mt   100.000 % 	 ..../RayTracingRLPrep

	WindowThread:

		   24809.812 Mt   100.000 %         1 x    24809.813 Mt    24809.811 Mt    24809.812 Mt   100.000 % 	 /

	GpuThread:

		   11774.677 Mt   100.000 %      3003 x        3.921 Mt        3.380 Mt      167.789 Mt     1.425 % 	 /

		       0.449 Mt     0.004 %         1 x        0.449 Mt        0.000 Mt        0.007 Mt     1.623 % 	 ./ScenePrep

		       0.442 Mt    98.377 %         1 x        0.442 Mt        0.000 Mt        0.001 Mt     0.326 % 	 ../AccelerationStructureBuild

		       0.358 Mt    80.974 %         1 x        0.358 Mt        0.000 Mt        0.358 Mt   100.000 % 	 .../BuildBottomLevelASGpu

		       0.083 Mt    18.700 %         1 x        0.083 Mt        0.000 Mt        0.083 Mt   100.000 % 	 .../BuildTopLevelASGpu

		   11605.063 Mt    98.560 %      3003 x        3.864 Mt        3.379 Mt        2.979 Mt     0.026 % 	 ./RayTracing

		    3126.260 Mt    26.939 %      3003 x        1.041 Mt        0.475 Mt     3126.260 Mt   100.000 % 	 ../RayTracingRL

		    8475.824 Mt    73.036 %      3003 x        2.822 Mt        2.903 Mt     8475.824 Mt   100.000 % 	 ../ResolveRL

		       1.376 Mt     0.012 %      3003 x        0.000 Mt        0.000 Mt        1.376 Mt   100.000 % 	 ./Present


	============================


