Configuration: 
Command line parameters  = E:\programming\FIT-DIP\Masters-DXR-Main-git\build\Measurement\x64\Debug\Measurement.exe --scene Suzanne/Suzanne.gltf --profile-track Suzanne/Suzanne.trk --rt-mode --width 1024 --height 768 --profile-output TrackSuzanne768Rt 
Window width             = 1024
Window height            = 768
Window title             = Direct3D App
Back-buffer count        = 3
Fill Triangles           = enabled
Triangles Font Clock-Wise= enabled
Fast Build AS            = disabled
Target FPS               = 0
Target UPS               = 60
Tearing                  = enabled
VSync                    = disabled
GUI rendering            = disabled
Debug layer              = enabled
GPU validation           = disabled
GPU profiling            = enabled
Render mode              = Ray Tracing
Loaded scene file        = Suzanne/Suzanne.gltf
Using testing scene      = disabled
Duplication              = 1
Duplication in cube      = disabled
Duplication offset       = 10
BLAS duplication         = disabled
Rays per pixel           = 10

ProfilingAggregator: 
Render	,	3.653	,	5.6771	,	4.0127	,	5.9574	,	8.4642	,	4.2973	,	6.185	,	4.3014	,	5.8498	,	5.4235	,	3.8638	,	4.2106	,	4.2058	,	6.7269	,	4.4389	,	3.661	,	5.715	,	4.1233	,	5.5576	,	4.4266	,	3.637	,	5.7627	,	5.4831	,	5.6941
Update	,	0.83	,	0.3277	,	0.2867	,	0.6418	,	0.3041	,	0.6207	,	0.285	,	0.2859	,	0.6348	,	0.5402	,	0.2768	,	0.5949	,	0.6297	,	0.2758	,	0.5738	,	0.6755	,	0.8017	,	0.462	,	0.661	,	0.5237	,	0.4848	,	0.5614	,	0.5743	,	0.4982
RayTracing	,	1.15872	,	1.46656	,	1.72074	,	1.60048	,	1.39302	,	1.3335	,	1.88307	,	1.70003	,	1.85078	,	1.26272	,	1.33405	,	1.47709	,	1.94186	,	2.57654	,	1.18115	,	1.37107	,	1.55357	,	1.87475	,	2.3256	,	1.69107	,	1.41955	,	1.31942	,	1.29651	,	1.35443
RayTracingRL	,	0.312192	,	0.500864	,	0.64576	,	0.525024	,	0.350272	,	0.230208	,	0.508256	,	0.451072	,	0.555936	,	0.286624	,	0.353024	,	0.30992	,	0.499136	,	0.635712	,	0.252544	,	0.413792	,	0.510336	,	0.791424	,	1.26694	,	0.788224	,	0.547488	,	0.341536	,	0.238848	,	0.214848
Index	,	0	,	1	,	2	,	3	,	4	,	5	,	6	,	7	,	8	,	9	,	10	,	11	,	12	,	13	,	14	,	15	,	16	,	17	,	18	,	19	,	20	,	21	,	22	,	23

ProfilingBuildAggregator: 
BuildBottomLevelAS	,	0.7602
BuildBottomLevelASGpu	,	0.248128
BuildTopLevelAS	,	0.826
BuildTopLevelASGpu	,	0.059936
Duplication	,	1
Triangles	,	3936
Meshes	,	1
BottomLevels	,	1
Index	,	0

FpsAggregator: 
FPS	,	233	,	200	,	182	,	190	,	201	,	224	,	207	,	173	,	167	,	205	,	208	,	192	,	206	,	172	,	181	,	213	,	211	,	188	,	164	,	172	,	192	,	206	,	211	,	210
UPS	,	59	,	60	,	61	,	59	,	61	,	60	,	60	,	60	,	61	,	60	,	60	,	60	,	60	,	60	,	61	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	61
FrameTime	,	4.29513	,	5.00321	,	5.51222	,	5.26983	,	5.01359	,	4.48175	,	4.83336	,	5.79561	,	6.00492	,	4.89739	,	4.82186	,	5.21151	,	4.86618	,	5.83018	,	5.55192	,	4.7119	,	4.74012	,	5.33421	,	6.10568	,	5.82402	,	5.21605	,	4.85837	,	4.74307	,	4.78986
GigaRays/s	,	1.83098	,	1.57186	,	1.42671	,	1.49233	,	1.5686	,	1.75475	,	1.62709	,	1.35694	,	1.30965	,	1.60582	,	1.63097	,	1.50903	,	1.61612	,	1.3489	,	1.4165	,	1.66903	,	1.6591	,	1.47432	,	1.28803	,	1.35033	,	1.50772	,	1.61872	,	1.65806	,	1.64187
Index	,	0	,	1	,	2	,	3	,	4	,	5	,	6	,	7	,	8	,	9	,	10	,	11	,	12	,	13	,	14	,	15	,	16	,	17	,	18	,	19	,	20	,	21	,	22	,	23


AccelerationStructure: 
Ray tracing acceleration structure building statistics: 
	Bottom Level Acceleration Structure: 
		Maximum [B]: 224128
		Minimum [B]: 224128
		Currently Allocated [B]: 224128
		Currently Created [num]: 1
		Current Average [B]: 224128
		Maximum Triangles [num]: 3936
		Minimum Triangles [num]: 3936
		Total Triangles [num]: 3936
		Maximum Meshes [num]: 1
		Minimum Meshes [num]: 1
		Total Meshes [num]: 1
	Top Level Acceleration Structure: 
		Maximum [B]: 64
		Minimum [B]: 64
		Currently Allocated [B]: 64
		Total Allocated [B]: 64
		Total Created [num]: 1
		Average Allocated [B]: 64
		Maximum Instances [num]: 1
		Minimum Instances [num]: 1
		Total Instances [num]: 1
	Scratch Buffer: 
		Maximum [B]: 34304
		Minimum [B]: 34304
		Currently Allocated [B]: 34304
		Total Allocated [B]: 34304
		Total Created [num]: 1
		Average Allocated [B]: 34304
ProfilingStack: 
[PROF] Profiling statistics: 
	Scopes entered : 47186
	Scopes exited : 47185
	Overhead per scope [ticks] : 1007.2
	Threads entered : 3
	Threads exited : 0
	============================
		         Sum[Mt]   Parent[%]  Samples[num]       Avg[Mt]   Last/Curr[Mt]        Self[Mt]     Self[%] 	 Name

	main:

		   24828.455 Mt   100.000 %         1 x    24828.457 Mt    24828.454 Mt        0.757 Mt     0.003 % 	 /

		   24827.752 Mt    99.997 %         1 x    24827.755 Mt    24827.752 Mt      369.043 Mt     1.486 % 	 ./Main application loop

		       2.869 Mt     0.012 %         1 x        2.869 Mt        0.000 Mt        2.869 Mt   100.000 % 	 ../TexturedRLInitialization

		       6.937 Mt     0.028 %         1 x        6.937 Mt        0.000 Mt        6.937 Mt   100.000 % 	 ../DeferredRLInitialization

		      89.346 Mt     0.360 %         1 x       89.346 Mt        0.000 Mt        2.309 Mt     2.584 % 	 ../RayTracingRLInitialization

		      87.037 Mt    97.416 %         1 x       87.037 Mt        0.000 Mt       87.037 Mt   100.000 % 	 .../PipelineBuild

		      12.202 Mt     0.049 %         1 x       12.202 Mt        0.000 Mt       12.202 Mt   100.000 % 	 ../ResolveRLInitialization

		     279.361 Mt     1.125 %         1 x      279.361 Mt        0.000 Mt        7.562 Mt     2.707 % 	 ../Initialization

		     271.799 Mt    97.293 %         1 x      271.799 Mt        0.000 Mt        1.685 Mt     0.620 % 	 .../ScenePrep

		     243.909 Mt    89.739 %         1 x      243.909 Mt        0.000 Mt       20.942 Mt     8.586 % 	 ..../SceneLoad

		     199.971 Mt    81.986 %         1 x      199.971 Mt        0.000 Mt       37.405 Mt    18.705 % 	 ...../glTF-LoadScene

		     162.566 Mt    81.295 %         2 x       81.283 Mt        0.000 Mt      162.566 Mt   100.000 % 	 ....../glTF-LoadImageData

		      16.012 Mt     6.565 %         1 x       16.012 Mt        0.000 Mt       16.012 Mt   100.000 % 	 ...../glTF-CreateScene

		       6.984 Mt     2.863 %         1 x        6.984 Mt        0.000 Mt        6.984 Mt   100.000 % 	 ...../SceneDesc-CreateScene

		       1.925 Mt     0.708 %         1 x        1.925 Mt        0.000 Mt        0.005 Mt     0.255 % 	 ..../TexturedRLPrep

		       1.920 Mt    99.745 %         1 x        1.920 Mt        0.000 Mt        0.661 Mt    34.450 % 	 ...../PrepareForRendering

		       0.753 Mt    39.247 %         1 x        0.753 Mt        0.000 Mt        0.753 Mt   100.000 % 	 ....../PrepareMaterials

		       0.505 Mt    26.303 %         1 x        0.505 Mt        0.000 Mt        0.505 Mt   100.000 % 	 ....../PrepareDrawBundle

		       2.900 Mt     1.067 %         1 x        2.900 Mt        0.000 Mt        0.008 Mt     0.276 % 	 ..../DeferredRLPrep

		       2.892 Mt    99.724 %         1 x        2.892 Mt        0.000 Mt        1.749 Mt    60.466 % 	 ...../PrepareForRendering

		       0.030 Mt     1.023 %         1 x        0.030 Mt        0.000 Mt        0.030 Mt   100.000 % 	 ....../PrepareMaterials

		       0.602 Mt    20.804 %         1 x        0.602 Mt        0.000 Mt        0.602 Mt   100.000 % 	 ....../BuildMaterialCache

		       0.512 Mt    17.706 %         1 x        0.512 Mt        0.000 Mt        0.512 Mt   100.000 % 	 ....../PrepareDrawBundle

		      10.567 Mt     3.888 %         1 x       10.567 Mt        0.000 Mt        3.979 Mt    37.659 % 	 ..../RayTracingRLPrep

		       0.419 Mt     3.966 %         1 x        0.419 Mt        0.000 Mt        0.419 Mt   100.000 % 	 ...../PrepareAccelerationStructures

		       0.042 Mt     0.397 %         1 x        0.042 Mt        0.000 Mt        0.042 Mt   100.000 % 	 ...../PrepareCache

		       0.863 Mt     8.163 %         1 x        0.863 Mt        0.000 Mt        0.863 Mt   100.000 % 	 ...../BuildCache

		       3.762 Mt    35.599 %         1 x        3.762 Mt        0.000 Mt        0.010 Mt     0.274 % 	 ...../BuildAccelerationStructures

		       3.751 Mt    99.726 %         1 x        3.751 Mt        0.000 Mt        2.165 Mt    57.716 % 	 ....../AccelerationStructureBuild

		       0.760 Mt    20.265 %         1 x        0.760 Mt        0.000 Mt        0.760 Mt   100.000 % 	 ......./BuildBottomLevelAS

		       0.826 Mt    22.019 %         1 x        0.826 Mt        0.000 Mt        0.826 Mt   100.000 % 	 ......./BuildTopLevelAS

		       1.502 Mt    14.215 %         1 x        1.502 Mt        0.000 Mt        1.502 Mt   100.000 % 	 ...../BuildShaderTables

		      10.813 Mt     3.978 %         1 x       10.813 Mt        0.000 Mt       10.813 Mt   100.000 % 	 ..../GpuSidePrep

		   24067.994 Mt    96.940 %      8022 x        3.000 Mt        6.101 Mt      822.568 Mt     3.418 % 	 ../MainLoop

		     863.360 Mt     3.587 %      1446 x        0.597 Mt        0.306 Mt      861.385 Mt    99.771 % 	 .../Update

		       1.975 Mt     0.229 %         1 x        1.975 Mt        0.000 Mt        1.975 Mt   100.000 % 	 ..../GuiModelApply

		   22382.066 Mt    92.995 %      4710 x        4.752 Mt        5.045 Mt    22339.653 Mt    99.811 % 	 .../Render

		      25.038 Mt     0.112 %      4710 x        0.005 Mt        0.004 Mt       25.038 Mt   100.000 % 	 ..../DeferredRLPrep

		      17.375 Mt     0.078 %      4710 x        0.004 Mt        0.004 Mt       17.375 Mt   100.000 % 	 ..../RayTracingRLPrep

	WindowThread:

		   24817.008 Mt   100.000 %         1 x    24817.008 Mt    24817.007 Mt    24817.008 Mt   100.000 % 	 /

	GpuThread:

		    7393.946 Mt   100.000 %      4710 x        1.570 Mt        1.381 Mt      142.686 Mt     1.930 % 	 /

		       0.314 Mt     0.004 %         1 x        0.314 Mt        0.000 Mt        0.005 Mt     1.621 % 	 ./ScenePrep

		       0.309 Mt    98.379 %         1 x        0.309 Mt        0.000 Mt        0.001 Mt     0.228 % 	 ../AccelerationStructureBuild

		       0.248 Mt    80.361 %         1 x        0.248 Mt        0.000 Mt        0.248 Mt   100.000 % 	 .../BuildBottomLevelASGpu

		       0.060 Mt    19.411 %         1 x        0.060 Mt        0.000 Mt        0.060 Mt   100.000 % 	 .../BuildTopLevelASGpu

		    7247.297 Mt    98.017 %      4710 x        1.539 Mt        1.379 Mt       11.507 Mt     0.159 % 	 ./RayTracing

		     265.685 Mt     3.666 %      4710 x        0.056 Mt        0.031 Mt      265.685 Mt   100.000 % 	 ../DeferredRL

		    2103.867 Mt    29.030 %      4710 x        0.447 Mt        0.234 Mt     2103.867 Mt   100.000 % 	 ../RayTracingRL

		    4866.238 Mt    67.146 %      4710 x        1.033 Mt        1.111 Mt     4866.238 Mt   100.000 % 	 ../ResolveRL

		       3.649 Mt     0.049 %      4710 x        0.001 Mt        0.001 Mt        3.649 Mt   100.000 % 	 ./Present


	============================


