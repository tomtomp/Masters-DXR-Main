Configuration: 
Command line parameters  = E:\programming\FIT-DIP\Masters-DXR-Main-git\build\Measurement\x64\Debug\Measurement.exe --scene Cube/Cube.gltf --duplication-cube --profile-duplication 10 --rt-mode --width 1024 --height 768 --profile-output DuplicationCube768Rt 
Window width             = 1024
Window height            = 768
Window title             = Direct3D App
Back-buffer count        = 3
Fill Triangles           = enabled
Triangles Font Clock-Wise= enabled
Fast Build AS            = disabled
Target FPS               = 60
Target UPS               = 60
Tearing                  = disabled
VSync                    = disabled
GUI rendering            = disabled
Debug layer              = enabled
GPU validation           = disabled
GPU profiling            = enabled
Render mode              = Ray Tracing
Loaded scene file        = Cube/Cube.gltf
Using testing scene      = disabled
Duplication              = 10
Duplication in cube      = enabled
Duplication offset       = 10
BLAS duplication         = enabled
Rays per pixel           = 10

ProfilingAggregator: 
Render	,	3.5649	,	5.5827	,	6.0978	,	3.7256	,	3.4995	,	3.5451	,	5.8774	,	10.7399	,	4.2453	,	4.2296	,	7.9264	,	11.9369	,	7.8086	,	7.8777	,	7.0151	,	6.8011	,	8.4783	,	8.632	,	639.676	,	10.5674
Update	,	0.2827	,	0.7125	,	0.2837	,	0.353	,	0.293	,	0.2791	,	0.3018	,	0.4695	,	0.7614	,	0.4672	,	0.5756	,	0.8179	,	0.6398	,	0.6964	,	0.9028	,	0.7992	,	1.6326	,	1.186	,	1.1979	,	1.4004
RayTracing	,	0.919904	,	2.83203	,	3.17469	,	0.92832	,	0.946304	,	0.943808	,	2.74346	,	7.34899	,	0.968896	,	0.975328	,	4.11098	,	7.53037	,	2.68294	,	2.66102	,	1.02579	,	1.0247	,	1.04202	,	1.03786	,	107.418	,	0.904448
RayTracingRL	,	0.096768	,	0.472736	,	0.567744	,	0.109056	,	0.126368	,	0.124224	,	0.63616	,	3.34998	,	0.146752	,	0.152992	,	0.833248	,	3.30026	,	0.798048	,	0.772288	,	0.189536	,	0.188832	,	0.201024	,	0.198432	,	0.209472	,	0.182144
Index	,	0	,	1	,	2	,	3	,	4	,	5	,	6	,	7	,	8	,	9	,	10	,	11	,	12	,	13	,	14	,	15	,	16	,	17	,	18	,	19

ProfilingBuildAggregator: 
BuildBottomLevelAS	,	3.6086	,	2.185	,	5.916	,	13.7256	,	55.0053	,	52.347	,	120.527	,	192.481	,	161.101	,	217.794
BuildBottomLevelASGpu	,	0.119744	,	2.35946	,	3.49098	,	6.92483	,	42.997	,	65.2776	,	98.0045	,	188.659	,	176.42	,	106.053
BuildTopLevelAS	,	2.9394	,	0.6797	,	0.5661	,	0.6321	,	0.8345	,	1.06	,	0.883	,	0.9021	,	1.057	,	0.7046
BuildTopLevelASGpu	,	0.087424	,	0.269376	,	0.133504	,	0.14144	,	0.608896	,	0.597056	,	0.409408	,	0.724416	,	0.277856	,	0.280512
Duplication	,	1	,	2	,	3	,	4	,	5	,	6	,	7	,	8	,	9	,	10
Triangles	,	12	,	96	,	324	,	768	,	1500	,	2592	,	4116	,	6144	,	8748	,	12000
Meshes	,	1	,	8	,	27	,	64	,	125	,	216	,	343	,	512	,	729	,	1000
BottomLevels	,	1	,	8	,	27	,	64	,	125	,	216	,	343	,	512	,	729	,	1000
Index	,	0	,	1	,	2	,	3	,	4	,	5	,	6	,	7	,	8	,	9

FpsAggregator: 
FPS	,	55	,	59	,	59	,	60	,	58	,	60	,	55	,	60	,	49	,	60	,	45	,	60	,	33	,	60	,	16	,	60	,	12	,	59	,	2	,	56
UPS	,	59	,	60	,	61	,	60	,	60	,	60	,	60	,	60	,	61	,	60	,	60	,	60	,	61	,	60	,	60	,	61	,	60	,	60	,	58	,	112
FrameTime	,	18.1818	,	17.0116	,	16.9492	,	16.7142	,	17.266	,	16.6987	,	18.2521	,	16.8155	,	20.4082	,	16.7071	,	22.2222	,	16.8431	,	30.4922	,	16.7865	,	62.9239	,	16.7705	,	83.7094	,	16.9492	,	913.408	,	18.0289
GigaRays/s	,	0.432537	,	0.462292	,	0.463994	,	0.470516	,	0.45548	,	0.470955	,	0.430871	,	0.467683	,	0.385351	,	0.470717	,	0.353894	,	0.466916	,	0.257912	,	0.468491	,	0.124981	,	0.468937	,	0.0939479	,	0.463994	,	0.00860987	,	0.436207
Index	,	0	,	1	,	2	,	3	,	4	,	5	,	6	,	7	,	8	,	9	,	10	,	11	,	12	,	13	,	14	,	15	,	16	,	17	,	18	,	19


AccelerationStructure: 
Ray tracing acceleration structure building statistics: 
	Bottom Level Acceleration Structure: 
		Maximum [B]: 5168
		Minimum [B]: 5168
		Currently Allocated [B]: 5168000
		Currently Created [num]: 1000
		Current Average [B]: 5168
		Maximum Triangles [num]: 12
		Minimum Triangles [num]: 12
		Total Triangles [num]: 12000
		Maximum Meshes [num]: 1
		Minimum Meshes [num]: 1
		Total Meshes [num]: 1000
	Top Level Acceleration Structure: 
		Maximum [B]: 64000
		Minimum [B]: 64000
		Currently Allocated [B]: 64000
		Total Allocated [B]: 64000
		Total Created [num]: 1
		Average Allocated [B]: 64000
		Maximum Instances [num]: 1000
		Minimum Instances [num]: 1000
		Total Instances [num]: 1000
	Scratch Buffer: 
		Maximum [B]: 79360
		Minimum [B]: 79360
		Currently Allocated [B]: 79360
		Total Allocated [B]: 79360
		Total Created [num]: 1
		Average Allocated [B]: 79360
ProfilingStack: 
[PROF] Profiling statistics: 
	Scopes entered : 2441982
	Scopes exited : 2441981
	Overhead per scope [ticks] : 1010.4
	Threads entered : 3
	Threads exited : 0
	============================
		         Sum[Mt]   Parent[%]  Samples[num]       Avg[Mt]   Last/Curr[Mt]        Self[Mt]     Self[%] 	 Name

	main:

		   22340.790 Mt   100.000 %         1 x    22340.792 Mt    22340.788 Mt        1.373 Mt     0.006 % 	 /

		   22339.463 Mt    99.994 %         1 x    22339.465 Mt    22339.463 Mt     5947.677 Mt    26.624 % 	 ./Main application loop

		       2.833 Mt     0.013 %         1 x        2.833 Mt        0.000 Mt        2.833 Mt   100.000 % 	 ../TexturedRLInitialization

		       5.581 Mt     0.025 %         1 x        5.581 Mt        0.000 Mt        5.581 Mt   100.000 % 	 ../DeferredRLInitialization

		      59.957 Mt     0.268 %         1 x       59.957 Mt        0.000 Mt        3.365 Mt     5.612 % 	 ../RayTracingRLInitialization

		      56.592 Mt    94.388 %         1 x       56.592 Mt        0.000 Mt       56.592 Mt   100.000 % 	 .../PipelineBuild

		      11.534 Mt     0.052 %         1 x       11.534 Mt        0.000 Mt       11.534 Mt   100.000 % 	 ../ResolveRLInitialization

		     362.383 Mt     1.622 %         1 x      362.383 Mt        0.000 Mt       33.770 Mt     9.319 % 	 ../Initialization

		     328.612 Mt    90.681 %         1 x      328.612 Mt        0.000 Mt        3.955 Mt     1.204 % 	 .../ScenePrep

		     246.269 Mt    74.942 %         1 x      246.269 Mt        0.000 Mt       33.608 Mt    13.647 % 	 ..../SceneLoad

		     153.578 Mt    62.362 %         1 x      153.578 Mt        0.000 Mt       83.718 Mt    54.512 % 	 ...../glTF-LoadScene

		      69.860 Mt    45.488 %         2 x       34.930 Mt        0.000 Mt       69.860 Mt   100.000 % 	 ....../glTF-LoadImageData

		       8.506 Mt     3.454 %         1 x        8.506 Mt        0.000 Mt        8.506 Mt   100.000 % 	 ...../glTF-CreateScene

		      50.577 Mt    20.537 %         1 x       50.577 Mt        0.000 Mt       50.577 Mt   100.000 % 	 ...../SceneDesc-CreateScene

		       7.359 Mt     2.239 %         1 x        7.359 Mt        0.000 Mt        0.014 Mt     0.197 % 	 ..../TexturedRLPrep

		       7.345 Mt    99.803 %         1 x        7.345 Mt        0.000 Mt        1.861 Mt    25.338 % 	 ...../PrepareForRendering

		       2.143 Mt    29.183 %         1 x        2.143 Mt        0.000 Mt        2.143 Mt   100.000 % 	 ....../PrepareMaterials

		       3.340 Mt    45.478 %         1 x        3.340 Mt        0.000 Mt        3.340 Mt   100.000 % 	 ....../PrepareDrawBundle

		       6.568 Mt     1.999 %         1 x        6.568 Mt        0.000 Mt        0.015 Mt     0.224 % 	 ..../DeferredRLPrep

		       6.553 Mt    99.776 %         1 x        6.553 Mt        0.000 Mt        3.974 Mt    60.651 % 	 ...../PrepareForRendering

		       0.055 Mt     0.833 %         1 x        0.055 Mt        0.000 Mt        0.055 Mt   100.000 % 	 ....../PrepareMaterials

		       1.333 Mt    20.338 %         1 x        1.333 Mt        0.000 Mt        1.333 Mt   100.000 % 	 ....../BuildMaterialCache

		       1.191 Mt    18.178 %         1 x        1.191 Mt        0.000 Mt        1.191 Mt   100.000 % 	 ....../PrepareDrawBundle

		      46.319 Mt    14.095 %         1 x       46.319 Mt        0.000 Mt       25.848 Mt    55.804 % 	 ..../RayTracingRLPrep

		       2.079 Mt     4.487 %         1 x        2.079 Mt        0.000 Mt        2.079 Mt   100.000 % 	 ...../PrepareAccelerationStructures

		       0.080 Mt     0.172 %         1 x        0.080 Mt        0.000 Mt        0.080 Mt   100.000 % 	 ...../PrepareCache

		       2.365 Mt     5.106 %         1 x        2.365 Mt        0.000 Mt        2.365 Mt   100.000 % 	 ...../BuildCache

		      12.562 Mt    27.120 %         1 x       12.562 Mt        0.000 Mt        0.021 Mt     0.169 % 	 ...../BuildAccelerationStructures

		      12.540 Mt    99.831 %         1 x       12.540 Mt        0.000 Mt        5.992 Mt    47.784 % 	 ....../AccelerationStructureBuild

		       3.609 Mt    28.776 %         1 x        3.609 Mt        0.000 Mt        3.609 Mt   100.000 % 	 ......./BuildBottomLevelAS

		       2.939 Mt    23.440 %         1 x        2.939 Mt        0.000 Mt        2.939 Mt   100.000 % 	 ......./BuildTopLevelAS

		       3.386 Mt     7.310 %         1 x        3.386 Mt        0.000 Mt        3.386 Mt   100.000 % 	 ...../BuildShaderTables

		      18.143 Mt     5.521 %         1 x       18.143 Mt        0.000 Mt       18.143 Mt   100.000 % 	 ..../GpuSidePrep

		   15949.498 Mt    71.396 %   2432671 x        0.007 Mt       13.106 Mt     4308.091 Mt    27.011 % 	 ../MainLoop

		    1884.730 Mt    11.817 %      1254 x        1.503 Mt        1.551 Mt     1869.910 Mt    99.214 % 	 .../Update

		      14.820 Mt     0.786 %         1 x       14.820 Mt        0.000 Mt       14.820 Mt   100.000 % 	 ..../GuiModelApply

		    9756.677 Mt    61.172 %       978 x        9.976 Mt        0.000 Mt     7396.296 Mt    75.808 % 	 .../Render

		     118.210 Mt     1.212 %       978 x        0.121 Mt        0.000 Mt        3.740 Mt     3.164 % 	 ..../DeferredRLPrep

		     114.471 Mt    96.836 %        13 x        8.805 Mt        0.000 Mt        5.418 Mt     4.733 % 	 ...../PrepareForRendering

		      10.941 Mt     9.558 %        13 x        0.842 Mt        0.000 Mt       10.941 Mt   100.000 % 	 ....../PrepareMaterials

		       0.009 Mt     0.008 %        13 x        0.001 Mt        0.000 Mt        0.009 Mt   100.000 % 	 ....../BuildMaterialCache

		      98.102 Mt    85.700 %        13 x        7.546 Mt        0.000 Mt       98.102 Mt   100.000 % 	 ....../PrepareDrawBundle

		    2242.171 Mt    22.981 %       978 x        2.293 Mt        0.000 Mt       29.262 Mt     1.305 % 	 ..../RayTracingRLPrep

		     958.333 Mt    42.741 %        13 x       73.718 Mt        0.000 Mt      958.333 Mt   100.000 % 	 ...../PrepareAccelerationStructures

		      11.414 Mt     0.509 %        13 x        0.878 Mt        0.000 Mt       11.414 Mt   100.000 % 	 ...../PrepareCache

		       0.009 Mt     0.000 %        13 x        0.001 Mt        0.000 Mt        0.009 Mt   100.000 % 	 ...../BuildCache

		    1136.148 Mt    50.672 %        13 x       87.396 Mt        0.000 Mt        0.034 Mt     0.003 % 	 ...../BuildAccelerationStructures

		    1136.114 Mt    99.997 %        13 x       87.393 Mt        0.000 Mt       44.065 Mt     3.879 % 	 ....../AccelerationStructureBuild

		    1081.698 Mt    95.210 %        13 x       83.208 Mt        0.000 Mt     1081.698 Mt   100.000 % 	 ......./BuildBottomLevelAS

		      10.351 Mt     0.911 %        13 x        0.796 Mt        0.000 Mt       10.351 Mt   100.000 % 	 ......./BuildTopLevelAS

		     107.004 Mt     4.772 %        13 x        8.231 Mt        0.000 Mt      107.004 Mt   100.000 % 	 ...../BuildShaderTables

	WindowThread:

		   22324.649 Mt   100.000 %         1 x    22324.650 Mt    22324.649 Mt    22324.649 Mt   100.000 % 	 /

	GpuThread:

		    3262.146 Mt   100.000 %       978 x        3.336 Mt        0.000 Mt      167.260 Mt     5.127 % 	 /

		       0.215 Mt     0.007 %         1 x        0.215 Mt        0.000 Mt        0.007 Mt     3.285 % 	 ./ScenePrep

		       0.208 Mt    96.715 %         1 x        0.208 Mt        0.000 Mt        0.001 Mt     0.507 % 	 ../AccelerationStructureBuild

		       0.120 Mt    57.507 %         1 x        0.120 Mt        0.000 Mt        0.120 Mt   100.000 % 	 .../BuildBottomLevelASGpu

		       0.087 Mt    41.986 %         1 x        0.087 Mt        0.000 Mt        0.087 Mt   100.000 % 	 .../BuildTopLevelASGpu

		    3093.334 Mt    94.825 %       978 x        3.163 Mt        0.000 Mt        4.345 Mt     0.140 % 	 ./RayTracing

		     109.607 Mt     3.543 %       978 x        0.112 Mt        0.000 Mt      109.607 Mt   100.000 % 	 ../DeferredRL

		     524.580 Mt    16.958 %       978 x        0.536 Mt        0.000 Mt      524.580 Mt   100.000 % 	 ../RayTracingRL

		    1478.047 Mt    47.782 %       978 x        1.511 Mt        0.000 Mt     1478.047 Mt   100.000 % 	 ../ResolveRL

		     976.754 Mt    31.576 %        13 x       75.135 Mt        0.000 Mt        0.031 Mt     0.003 % 	 ../AccelerationStructureBuild

		     972.452 Mt    99.560 %        13 x       74.804 Mt        0.000 Mt      972.452 Mt   100.000 % 	 .../BuildBottomLevelASGpu

		       4.271 Mt     0.437 %        13 x        0.329 Mt        0.000 Mt        4.271 Mt   100.000 % 	 .../BuildTopLevelASGpu

		       1.337 Mt     0.041 %       978 x        0.001 Mt        0.000 Mt        1.337 Mt   100.000 % 	 ./Present


	============================


