Configuration: 
Command line parameters  = E:\programming\FIT-DIP\Masters-DXR-Main-git\build\Measurement\x64\Debug\Measurement.exe --scene Cube/Cube.gltf --profile-track Cube/Cube.trk --rt-mode --width 1024 --height 768 --profile-output TrackCube768Rt 
Window width             = 1024
Window height            = 768
Window title             = Direct3D App
Back-buffer count        = 3
Fill Triangles           = enabled
Triangles Font Clock-Wise= enabled
Fast Build AS            = disabled
Target FPS               = 0
Target UPS               = 60
Tearing                  = enabled
VSync                    = disabled
GUI rendering            = disabled
Debug layer              = enabled
GPU validation           = disabled
GPU profiling            = enabled
Render mode              = Ray Tracing
Loaded scene file        = Cube/Cube.gltf
Using testing scene      = disabled
Duplication              = 1
Duplication in cube      = disabled
Duplication offset       = 10
BLAS duplication         = disabled
Rays per pixel           = 10

ProfilingAggregator: 
Render	,	3.4952	,	4.4251	,	3.5725	,	4.5749	,	5.8837	,	9.1108	,	3.6653	,	7.5699	,	6.7254	,	4.1281	,	4.4668	,	6.4309	,	3.9832	,	3.9569	,	6.3589	,	6.0797	,	6.628	,	7.3991	,	6.6076	,	3.989	,	5.9911	,	3.6055	,	6.1675	,	3.7726	,	3.5782	,	4.0181	,	3.9604	,	5.6811	,	6.4982
Update	,	0.2906	,	0.3792	,	0.7248	,	0.3835	,	0.5864	,	0.2903	,	0.3014	,	0.7078	,	0.7125	,	0.3092	,	0.7211	,	0.8521	,	0.7504	,	0.8631	,	0.7459	,	0.5803	,	0.64	,	0.2896	,	0.3428	,	0.3797	,	0.7614	,	0.2974	,	0.6345	,	0.3048	,	0.4222	,	0.2937	,	0.8272	,	0.6694	,	0.5376
RayTracing	,	1.23597	,	1.38918	,	1.39357	,	1.46067	,	1.89955	,	2.02141	,	1.50285	,	1.5847	,	1.77718	,	1.73587	,	1.65309	,	1.8696	,	1.76029	,	1.78989	,	1.9967	,	2.1295	,	2.12592	,	1.89229	,	1.8496	,	1.85786	,	1.64486	,	1.35907	,	1.49846	,	1.46605	,	1.45654	,	1.45968	,	1.82528	,	1.45142	,	2.15075
RayTracingRL	,	0.344672	,	0.45904	,	0.472896	,	0.537408	,	0.817024	,	0.928576	,	0.668672	,	0.721632	,	0.807328	,	0.793568	,	0.765696	,	0.875392	,	0.775616	,	0.756256	,	0.901632	,	0.993024	,	0.976128	,	0.870624	,	0.855712	,	0.85984	,	0.721184	,	0.51072	,	0.52496	,	0.493792	,	0.445344	,	0.379456	,	0.301984	,	0.259808	,	0.567584
Index	,	0	,	1	,	2	,	3	,	4	,	5	,	6	,	7	,	8	,	9	,	10	,	11	,	12	,	13	,	14	,	15	,	16	,	17	,	18	,	19	,	20	,	21	,	22	,	23	,	24	,	25	,	26	,	27	,	28

ProfilingBuildAggregator: 
BuildBottomLevelAS	,	0.7639
BuildBottomLevelASGpu	,	0.12448
BuildTopLevelAS	,	0.77
BuildTopLevelASGpu	,	0.083136
Duplication	,	1
Triangles	,	12
Meshes	,	1
BottomLevels	,	1
Index	,	0

FpsAggregator: 
FPS	,	232	,	224	,	229	,	201	,	170	,	172	,	204	,	178	,	185	,	181	,	197	,	172	,	167	,	178	,	169	,	170	,	165	,	169	,	182	,	177	,	189	,	196	,	207	,	206	,	193	,	195	,	200	,	193	,	174
UPS	,	59	,	61	,	60	,	60	,	60	,	60	,	60	,	61	,	60	,	60	,	60	,	60	,	60	,	61	,	60	,	60	,	60	,	61	,	60	,	60	,	60	,	61	,	60	,	60	,	60	,	60	,	60	,	60	,	60
FrameTime	,	4.3205	,	4.47989	,	4.37339	,	4.98097	,	5.8915	,	5.84281	,	4.91878	,	5.65566	,	5.41061	,	5.52735	,	5.0944	,	5.84759	,	5.989	,	5.6403	,	5.95426	,	5.90833	,	6.09569	,	5.9528	,	5.51218	,	5.66762	,	5.3064	,	5.11915	,	4.83175	,	4.86323	,	5.18376	,	5.14319	,	5.01044	,	5.18226	,	5.78395
GigaRays/s	,	1.82024	,	1.75547	,	1.79822	,	1.57887	,	1.33486	,	1.34598	,	1.59884	,	1.39052	,	1.4535	,	1.4228	,	1.54372	,	1.34488	,	1.31313	,	1.39431	,	1.32079	,	1.33106	,	1.29015	,	1.32111	,	1.42672	,	1.38759	,	1.48204	,	1.53626	,	1.62763	,	1.6171	,	1.51711	,	1.52907	,	1.56959	,	1.51755	,	1.35968
Index	,	0	,	1	,	2	,	3	,	4	,	5	,	6	,	7	,	8	,	9	,	10	,	11	,	12	,	13	,	14	,	15	,	16	,	17	,	18	,	19	,	20	,	21	,	22	,	23	,	24	,	25	,	26	,	27	,	28


AccelerationStructure: 
Ray tracing acceleration structure building statistics: 
	Bottom Level Acceleration Structure: 
		Maximum [B]: 5168
		Minimum [B]: 5168
		Currently Allocated [B]: 5168
		Currently Created [num]: 1
		Current Average [B]: 5168
		Maximum Triangles [num]: 12
		Minimum Triangles [num]: 12
		Total Triangles [num]: 12
		Maximum Meshes [num]: 1
		Minimum Meshes [num]: 1
		Total Meshes [num]: 1
	Top Level Acceleration Structure: 
		Maximum [B]: 64
		Minimum [B]: 64
		Currently Allocated [B]: 64
		Total Allocated [B]: 64
		Total Created [num]: 1
		Average Allocated [B]: 64
		Maximum Instances [num]: 1
		Minimum Instances [num]: 1
		Total Instances [num]: 1
	Scratch Buffer: 
		Maximum [B]: 3584
		Minimum [B]: 3584
		Currently Allocated [B]: 3584
		Total Allocated [B]: 3584
		Total Created [num]: 1
		Average Allocated [B]: 3584
ProfilingStack: 
[PROF] Profiling statistics: 
	Scopes entered : 55094
	Scopes exited : 55093
	Overhead per scope [ticks] : 1006.8
	Threads entered : 3
	Threads exited : 0
	============================
		         Sum[Mt]   Parent[%]  Samples[num]       Avg[Mt]   Last/Curr[Mt]        Self[Mt]     Self[%] 	 Name

	main:

		   29690.593 Mt   100.000 %         1 x    29690.595 Mt    29690.593 Mt        0.755 Mt     0.003 % 	 /

		   29689.888 Mt    99.998 %         1 x    29689.890 Mt    29689.888 Mt      372.691 Mt     1.255 % 	 ./Main application loop

		       3.896 Mt     0.013 %         1 x        3.896 Mt        0.000 Mt        3.896 Mt   100.000 % 	 ../TexturedRLInitialization

		       5.514 Mt     0.019 %         1 x        5.514 Mt        0.000 Mt        5.514 Mt   100.000 % 	 ../DeferredRLInitialization

		      70.270 Mt     0.237 %         1 x       70.270 Mt        0.000 Mt        2.881 Mt     4.100 % 	 ../RayTracingRLInitialization

		      67.389 Mt    95.900 %         1 x       67.389 Mt        0.000 Mt       67.389 Mt   100.000 % 	 .../PipelineBuild

		      15.241 Mt     0.051 %         1 x       15.241 Mt        0.000 Mt       15.241 Mt   100.000 % 	 ../ResolveRLInitialization

		     131.868 Mt     0.444 %         1 x      131.868 Mt        0.000 Mt        2.230 Mt     1.691 % 	 ../Initialization

		     129.638 Mt    98.309 %         1 x      129.638 Mt        0.000 Mt        2.004 Mt     1.546 % 	 .../ScenePrep

		     102.045 Mt    78.715 %         1 x      102.045 Mt        0.000 Mt       12.076 Mt    11.834 % 	 ..../SceneLoad

		      71.032 Mt    69.608 %         1 x       71.032 Mt        0.000 Mt       25.945 Mt    36.526 % 	 ...../glTF-LoadScene

		      45.087 Mt    63.474 %         2 x       22.543 Mt        0.000 Mt       45.087 Mt   100.000 % 	 ....../glTF-LoadImageData

		      11.439 Mt    11.209 %         1 x       11.439 Mt        0.000 Mt       11.439 Mt   100.000 % 	 ...../glTF-CreateScene

		       7.499 Mt     7.349 %         1 x        7.499 Mt        0.000 Mt        7.499 Mt   100.000 % 	 ...../SceneDesc-CreateScene

		       2.789 Mt     2.152 %         1 x        2.789 Mt        0.000 Mt        0.005 Mt     0.186 % 	 ..../TexturedRLPrep

		       2.784 Mt    99.814 %         1 x        2.784 Mt        0.000 Mt        1.024 Mt    36.780 % 	 ...../PrepareForRendering

		       1.240 Mt    44.546 %         1 x        1.240 Mt        0.000 Mt        1.240 Mt   100.000 % 	 ....../PrepareMaterials

		       0.520 Mt    18.674 %         1 x        0.520 Mt        0.000 Mt        0.520 Mt   100.000 % 	 ....../PrepareDrawBundle

		       3.436 Mt     2.650 %         1 x        3.436 Mt        0.000 Mt        0.006 Mt     0.183 % 	 ..../DeferredRLPrep

		       3.429 Mt    99.817 %         1 x        3.429 Mt        0.000 Mt        2.407 Mt    70.189 % 	 ...../PrepareForRendering

		       0.029 Mt     0.834 %         1 x        0.029 Mt        0.000 Mt        0.029 Mt   100.000 % 	 ....../PrepareMaterials

		       0.672 Mt    19.584 %         1 x        0.672 Mt        0.000 Mt        0.672 Mt   100.000 % 	 ....../BuildMaterialCache

		       0.322 Mt     9.393 %         1 x        0.322 Mt        0.000 Mt        0.322 Mt   100.000 % 	 ....../PrepareDrawBundle

		      10.861 Mt     8.378 %         1 x       10.861 Mt        0.000 Mt        3.820 Mt    35.168 % 	 ..../RayTracingRLPrep

		       0.250 Mt     2.304 %         1 x        0.250 Mt        0.000 Mt        0.250 Mt   100.000 % 	 ...../PrepareAccelerationStructures

		       0.028 Mt     0.254 %         1 x        0.028 Mt        0.000 Mt        0.028 Mt   100.000 % 	 ...../PrepareCache

		       1.320 Mt    12.157 %         1 x        1.320 Mt        0.000 Mt        1.320 Mt   100.000 % 	 ...../BuildCache

		       3.989 Mt    36.728 %         1 x        3.989 Mt        0.000 Mt        0.008 Mt     0.203 % 	 ...../BuildAccelerationStructures

		       3.981 Mt    99.797 %         1 x        3.981 Mt        0.000 Mt        2.447 Mt    61.470 % 	 ....../AccelerationStructureBuild

		       0.764 Mt    19.188 %         1 x        0.764 Mt        0.000 Mt        0.764 Mt   100.000 % 	 ......./BuildBottomLevelAS

		       0.770 Mt    19.341 %         1 x        0.770 Mt        0.000 Mt        0.770 Mt   100.000 % 	 ......./BuildTopLevelAS

		       1.454 Mt    13.390 %         1 x        1.454 Mt        0.000 Mt        1.454 Mt   100.000 % 	 ...../BuildShaderTables

		       8.502 Mt     6.558 %         1 x        8.502 Mt        0.000 Mt        8.502 Mt   100.000 % 	 ..../GpuSidePrep

		   29090.409 Mt    97.981 %      9503 x        3.061 Mt       46.737 Mt      997.739 Mt     3.430 % 	 ../MainLoop

		    1015.222 Mt     3.490 %      1745 x        0.582 Mt        0.386 Mt     1014.347 Mt    99.914 % 	 .../Update

		       0.875 Mt     0.086 %         1 x        0.875 Mt        0.000 Mt        0.875 Mt   100.000 % 	 ..../GuiModelApply

		   27077.447 Mt    93.080 %      5476 x        4.945 Mt        6.434 Mt    27025.539 Mt    99.808 % 	 .../Render

		      30.472 Mt     0.113 %      5476 x        0.006 Mt        0.005 Mt       30.472 Mt   100.000 % 	 ..../DeferredRLPrep

		      21.436 Mt     0.079 %      5476 x        0.004 Mt        0.004 Mt       21.436 Mt   100.000 % 	 ..../RayTracingRLPrep

	WindowThread:

		   29686.305 Mt   100.000 %         1 x    29686.305 Mt    29686.305 Mt    29686.305 Mt   100.000 % 	 /

	GpuThread:

		    9333.749 Mt   100.000 %      5476 x        1.704 Mt        2.855 Mt      101.583 Mt     1.088 % 	 /

		       0.218 Mt     0.002 %         1 x        0.218 Mt        0.000 Mt        0.009 Mt     4.032 % 	 ./ScenePrep

		       0.209 Mt    95.968 %         1 x        0.209 Mt        0.000 Mt        0.002 Mt     0.871 % 	 ../AccelerationStructureBuild

		       0.124 Mt    59.435 %         1 x        0.124 Mt        0.000 Mt        0.124 Mt   100.000 % 	 .../BuildBottomLevelASGpu

		       0.083 Mt    39.694 %         1 x        0.083 Mt        0.000 Mt        0.083 Mt   100.000 % 	 .../BuildTopLevelASGpu

		    9227.712 Mt    98.864 %      5476 x        1.685 Mt        2.853 Mt       10.535 Mt     0.114 % 	 ./RayTracing

		     493.311 Mt     5.346 %      5476 x        0.090 Mt        0.133 Mt      493.311 Mt   100.000 % 	 ../DeferredRL

		    3585.227 Mt    38.853 %      5476 x        0.655 Mt        0.574 Mt     3585.227 Mt   100.000 % 	 ../RayTracingRL

		    5138.639 Mt    55.687 %      5476 x        0.938 Mt        2.142 Mt     5138.639 Mt   100.000 % 	 ../ResolveRL

		       4.235 Mt     0.045 %      5476 x        0.001 Mt        0.001 Mt        4.235 Mt   100.000 % 	 ./Present


	============================


