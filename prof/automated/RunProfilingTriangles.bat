@echo off

cd "..\..\build\Measurement\x64\Release\"



start /wait Measurement.exe --scene Cube/Cube.gltf --duplication-cube --duplication-offset 2.5 --profile-triangles --profile-camera-track Cube/CubeFast.trk --profile-max-duplication 10 --rt-mode --width 1024 --height 768 --profile-output TrianglesCube768Rt
copy TrianglesCube768Rt*.txt ..\..\..\..\prof\automated\
start /wait Measurement.exe --scene Cube/Cube.gltf --duplication-cube --duplication-offset 2.5 --profile-triangles --profile-camera-track Cube/CubeFast.trk --profile-max-duplication 10 --rt-mode --width 2560 --height 1440 --profile-output TrianglesCube1440Rt
copy TrianglesCube1440Rt*.txt ..\..\..\..\prof\automated\

start /wait Measurement.exe --scene Suzanne/Suzanne.gltf --duplication-cube --duplication-offset 3 --profile-triangles --profile-camera-track Suzanne/SuzanneFast.trk --profile-max-duplication 10 --rt-mode --width 1024 --height 768 --profile-output TrianglesSuzanne768Rt
copy TrianglesSuzanne768Rt*.txt ..\..\..\..\prof\automated\
start /wait Measurement.exe --scene Suzanne/Suzanne.gltf --duplication-cube --duplication-offset 3 --profile-triangles --profile-camera-track Suzanne/SuzanneFast.trk --profile-max-duplication 10 --rt-mode --width 2560 --height 1440 --profile-output TrianglesSuzanne1440Rt
copy TrianglesSuzanne1440Rt*.txt ..\..\..\..\prof\automated\

start /wait Measurement.exe --scene Sponza/Sponza.gltf --duplication-cube --duplication-offset 25 --profile-triangles --profile-camera-track Sponza/SponzaFast.trk --profile-max-duplication 7 --rt-mode --width 1024 --height 768 --profile-output TrianglesSponza768Rt
copy TrianglesSponza768Rt*.txt ..\..\..\..\prof\automated\
start /wait Measurement.exe --scene Sponza/Sponza.gltf --duplication-cube --duplication-offset 25 --profile-triangles --profile-camera-track Sponza/SponzaFast.trk --profile-max-duplication 7 --rt-mode --width 2560 --height 1440 --profile-output TrianglesSponza1440Rt
copy TrianglesSponza1440Rt*.txt ..\..\..\..\prof\automated\

start /wait Measurement.exe --scene Sponza/Sponza.gltf --duplication-cube --duplication-offset 0 --profile-triangles --profile-camera-track Sponza/SponzaFast.trk --profile-max-duplication 7 --rt-mode --width 1024 --height 768 --profile-output TrianglesSponzaNoOffset768Rt
copy TrianglesSponzaNoOffset768Rt*.txt ..\..\..\..\prof\automated\
start /wait Measurement.exe --scene Sponza/Sponza.gltf --duplication-cube --duplication-offset 0 --profile-triangles --profile-camera-track Sponza/SponzaFast.trk --profile-max-duplication 5 --rt-mode --width 2560 --height 1440 --profile-output TrianglesSponzaNoOffset1440Rt
copy TrianglesSponzaNoOffset1440Rt*.txt ..\..\..\..\prof\automated\


start /wait Measurement.exe --scene Cube/Cube.gltf --duplication-cube --duplication-offset 2.5 --profile-triangles --profile-camera-track Cube/CubeFast.trk --profile-max-duplication 10 --rt-mode-pure --width 1024 --height 768 --profile-output TrianglesCube768Rp
copy TrianglesCube768Rp*.txt ..\..\..\..\prof\automated\
start /wait Measurement.exe --scene Cube/Cube.gltf --duplication-cube --duplication-offset 2.5 --profile-triangles --profile-camera-track Cube/CubeFast.trk --profile-max-duplication 10 --rt-mode-pure --width 2560 --height 1440 --profile-output TrianglesCube1440Rp
copy TrianglesCube1440Rp*.txt ..\..\..\..\prof\automated\

start /wait Measurement.exe --scene Suzanne/Suzanne.gltf --duplication-cube --duplication-offset 3 --profile-triangles --profile-camera-track Suzanne/SuzanneFast.trk --profile-max-duplication 10 --rt-mode-pure --width 1024 --height 768 --profile-output TrianglesSuzanne768Rp
copy TrianglesSuzanne768Rp*.txt ..\..\..\..\prof\automated\
start /wait Measurement.exe --scene Suzanne/Suzanne.gltf --duplication-cube --duplication-offset 3 --profile-triangles --profile-camera-track Suzanne/SuzanneFast.trk --profile-max-duplication 10 --rt-mode-pure --width 2560 --height 1440 --profile-output TrianglesSuzanne1440Rp
copy TrianglesSuzanne1440Rp*.txt ..\..\..\..\prof\automated\

start /wait Measurement.exe --scene Sponza/Sponza.gltf --duplication-cube --duplication-offset 25 --profile-triangles --profile-camera-track Sponza/SponzaFast.trk --profile-max-duplication 7 --rt-mode-pure --width 1024 --height 768 --profile-output TrianglesSponza768Rp
copy TrianglesSponza768Rp*.txt ..\..\..\..\prof\automated\
start /wait Measurement.exe --scene Sponza/Sponza.gltf --duplication-cube --duplication-offset 25 --profile-triangles --profile-camera-track Sponza/SponzaFast.trk --profile-max-duplication 7 --rt-mode-pure --width 2560 --height 1440 --profile-output TrianglesSponza1440Rp
copy TrianglesSponza1440Rp*.txt ..\..\..\..\prof\automated\

start /wait Measurement.exe --scene Sponza/Sponza.gltf --duplication-cube --duplication-offset 0 --profile-triangles --profile-camera-track Sponza/SponzaFast.trk --profile-max-duplication 7 --rt-mode-pure --width 1024 --height 768 --profile-output TrianglesSponzaNoOffset768Rp
copy TrianglesSponzaNoOffset768Rp*.txt ..\..\..\..\prof\automated\
start /wait Measurement.exe --scene Sponza/Sponza.gltf --duplication-cube --duplication-offset 0 --profile-triangles --profile-camera-track Sponza/SponzaFast.trk --profile-max-duplication 5 --rt-mode-pure --width 2560 --height 1440 --profile-output TrianglesSponzaNoOffset1440Rp
copy TrianglesSponzaNoOffset1440Rp*.txt ..\..\..\..\prof\automated\


exit