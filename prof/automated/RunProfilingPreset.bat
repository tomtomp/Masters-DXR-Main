@echo off

cd "..\..\build\Measurement\x64\Release\"


setlocal enabledelayedexpansion 
set Presets[0]=NoEffects
set Presets[1]=HardShadows
set Presets[2]=AmbientOcclusion
set Presets[3]=AmbientOcclusion16
set Presets[4]=Reflections
set Presets[5]=SoftShadows
set Presets[6]=SoftShadows8
set Presets[7]=Low
set Presets[8]=Medium
set Presets[9]=High
set Presets[10]=Ultra

for /l %%n in (0,1,10) do (
	set preset_name=!Presets[%%n]!
	
	start /wait Measurement.exe --scene Cube/Cube.gltf --profile-automation --profile-camera-track Cube/Cube.trk --rt-mode --width 2560 --height 1440 --profile-output PresetCube1440Rt!preset_name! --quality-preset !preset_name!
	copy PresetCube1440Rt!preset_name!*.txt ..\..\..\..\prof\automated\
	
	start /wait Measurement.exe --scene Suzanne/Suzanne.gltf --profile-automation --profile-camera-track Suzanne/Suzanne.trk --rt-mode --width 2560 --height 1440 --profile-output PresetSuzanne1440Rt!preset_name! --quality-preset !preset_name!
	copy PresetSuzanne1440Rt!preset_name!*.txt ..\..\..\..\prof\automated\
	
	start /wait Measurement.exe --scene Sponza/Sponza.gltf --profile-automation --profile-camera-track Sponza/SponzaPreset.trk --rt-mode --width 2560 --height 1440 --profile-output PresetSponza1440Rt!preset_name! --quality-preset !preset_name!
	copy PresetSponza1440Rt!preset_name!*.txt ..\..\..\..\prof\automated\
	
	start /wait Measurement.exe --scene Cube/Cube.gltf --profile-automation --profile-camera-track Cube/Cube.trk --rt-mode-pure --width 2560 --height 1440 --profile-output PresetCube1440Rp!preset_name! --quality-preset !preset_name!
	copy PresetCube1440Rp!preset_name!*.txt ..\..\..\..\prof\automated\
	
	start /wait Measurement.exe --scene Suzanne/Suzanne.gltf --profile-automation --profile-camera-track Suzanne/Suzanne.trk --rt-mode-pure --width 2560 --height 1440 --profile-output PresetSuzanne1440Rp!preset_name! --quality-preset !preset_name!
	copy PresetSuzanne1440Rp!preset_name!*.txt ..\..\..\..\prof\automated\
	
	start /wait Measurement.exe --scene Sponza/Sponza.gltf --profile-automation --profile-camera-track Sponza/SponzaPreset.trk --rt-mode-pure --width 2560 --height 1440 --profile-output PresetSponza1440Rp!preset_name! --quality-preset !preset_name!
	copy PresetSponza1440Rp!preset_name!*.txt ..\..\..\..\prof\automated\
	
	start /wait Measurement.exe --scene Cube/Cube.gltf --profile-automation --profile-camera-track Cube/Cube.trk --width 2560 --height 1440 --profile-output PresetCube1440Ra!preset_name! --quality-preset !preset_name!
	copy PresetCube1440Ra!preset_name!*.txt ..\..\..\..\prof\automated\
	
	start /wait Measurement.exe --scene Suzanne/Suzanne.gltf --profile-automation --profile-camera-track Suzanne/Suzanne.trk --width 2560 --height 1440 --profile-output PresetSuzanne1440Ra!preset_name! --quality-preset !preset_name!
	copy PresetSuzanne1440Ra!preset_name!*.txt ..\..\..\..\prof\automated\
	
	start /wait Measurement.exe --scene Sponza/Sponza.gltf --profile-automation --profile-camera-track Sponza/SponzaPreset.trk --width 2560 --height 1440 --profile-output PresetSponza1440Ra!preset_name! --quality-preset !preset_name!
	copy PresetSponza1440Ra!preset_name!*.txt ..\..\..\..\prof\automated\
)


exit