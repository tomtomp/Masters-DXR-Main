@echo off

cd "..\..\build\Measurement\x64\Release\"



start /wait Measurement.exe --scene Cube/Cube.gltf --duplication-cube --profile-duplication --profile-max-duplication  10 --rt-mode --width 1024 --height 768 --profile-output DuplicationCube768Rt
copy DuplicationCube768Rt*.txt ..\..\..\..\prof\automated\

start /wait Measurement.exe --scene Suzanne/Suzanne.gltf --duplication-cube --profile-duplication --profile-max-duplication  10 --rt-mode --width 1024 --height 768 --profile-output DuplicationSuzanne768Rt
copy DuplicationSuzanne768Rt*.txt ..\..\..\..\prof\automated\

start /wait Measurement.exe --scene Sponza/Sponza.gltf --duplication-cube --profile-duplication --profile-max-duplication  5 --rt-mode --width 1024 --height 768 --profile-output DuplicationSponza768Rt
copy DuplicationSponza768Rt*.txt ..\..\..\..\prof\automated\


start /wait Measurement.exe --scene Cube/Cube.gltf --duplication-cube --profile-duplication --profile-max-duplication  10 --rt-mode --width 1024 --height 768 --profile-output DuplicationCube768Rf --fast-build-as
copy DuplicationCube768Rf*.txt ..\..\..\..\prof\automated\

start /wait Measurement.exe --scene Suzanne/Suzanne.gltf --duplication-cube --profile-duplication --profile-max-duplication  10 --rt-mode --width 1024 --height 768 --profile-output DuplicationSuzanne768Rf --fast-build-as
copy DuplicationSuzanne768Rf*.txt ..\..\..\..\prof\automated\

start /wait Measurement.exe --scene Sponza/Sponza.gltf --duplication-cube --profile-duplication --profile-max-duplication  5 --rt-mode --width 1024 --height 768 --profile-output DuplicationSponza768Rf --fast-build-as
copy DuplicationSponza768Rf*.txt ..\..\..\..\prof\automated\


exit