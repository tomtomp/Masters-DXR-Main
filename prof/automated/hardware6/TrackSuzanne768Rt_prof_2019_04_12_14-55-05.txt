Configuration: 
Command line parameters  = E:\programming\FIT-DIP\Masters-DXR-Main-git\build\Measurement\x64\Debug\Measurement.exe --scene Suzanne/Suzanne.gltf --profile-automation --profile-camera-track Suzanne/Suzanne.trk --rt-mode --width 1024 --height 768 --profile-output TrackSuzanne768Rt 
Window width             = 1024
Window height            = 768
Window title             = Direct3D App
Back-buffer count        = 3
Fill Triangles           = enabled
Triangles Font Clock-Wise= enabled
Fast Build AS            = disabled
Target FPS               = 0
Target UPS               = 60
Tearing                  = enabled
VSync                    = disabled
GUI rendering            = disabled
Debug layer              = enabled
GPU validation           = disabled
GPU profiling            = enabled
Render mode              = Ray Tracing
Loaded scene file        = Suzanne/Suzanne.gltf
Using testing scene      = disabled
Duplication              = 1
Duplication in cube      = disabled
Duplication offset       = 10
BLAS duplication         = disabled
Rays per pixel           = 10

ProfilingAggregator: 
Render	,	6.0574	,	5.1149	,	3.1043	,	2.9715	,	2.7667	,	2.5906	,	2.5717	,	2.6119	,	2.7506	,	3.0636	,	3.8963	,	2.9235	,	2.9275	,	2.6527	,	2.9896	,	8.643	,	3.4028	,	3.7435	,	3.7914	,	3.1154	,	2.769	,	2.6641	,	2.5082	,	2.5417
Update	,	0.2768	,	0.6009	,	0.2813	,	0.2794	,	0.28	,	0.2824	,	0.2889	,	0.279	,	0.2812	,	0.2869	,	0.2702	,	0.2822	,	0.2755	,	0.2813	,	0.2806	,	0.8906	,	0.2817	,	0.2867	,	0.2856	,	0.283	,	0.2844	,	0.2876	,	0.2821	,	0.2719
RayTracing	,	3.4991	,	2.8046	,	1.2219	,	1.2182	,	1.2231	,	1.2153	,	1.2301	,	1.2263	,	1.2415	,	1.2223	,	1.2437	,	1.222	,	1.2212	,	1.2223	,	1.2174	,	4.3564	,	1.2225	,	1.2303	,	1.2189	,	1.2231	,	1.2198	,	1.2201	,	1.2236	,	1.2214
RayTracingGpu	,	0.419968	,	0.55968	,	0.989664	,	0.882912	,	0.665664	,	0.523104	,	0.44736	,	0.497664	,	0.652	,	0.978336	,	1.79955	,	0.858176	,	0.553856	,	0.590528	,	0.880832	,	1.33958	,	1.25027	,	1.57322	,	1.70064	,	1.0073	,	0.725632	,	0.536256	,	0.418272	,	0.427936
DeferredRLGpu	,	0.041216	,	0.059712	,	0.091776	,	0.081536	,	0.06128	,	0.051392	,	0.050528	,	0.050656	,	0.064096	,	0.095168	,	0.104448	,	0.079296	,	0.054752	,	0.058496	,	0.09184	,	0.128192	,	0.110144	,	0.127328	,	0.126528	,	0.095712	,	0.075712	,	0.054976	,	0.039456	,	0.044544
RayTracingRLGpu	,	0.322112	,	0.435616	,	0.804544	,	0.711776	,	0.524352	,	0.39504	,	0.317824	,	0.35536	,	0.493632	,	0.781536	,	1.5935	,	0.681856	,	0.405504	,	0.43776	,	0.690304	,	1.10707	,	1.04426	,	1.3345	,	1.43725	,	0.825312	,	0.580768	,	0.406304	,	0.309408	,	0.309216
ResolveRLGpu	,	0.055072	,	0.063168	,	0.091168	,	0.087392	,	0.077952	,	0.073984	,	0.07744	,	0.08816	,	0.091648	,	0.098656	,	0.099488	,	0.094624	,	0.091424	,	0.090784	,	0.095776	,	0.102464	,	0.09376	,	0.108448	,	0.135264	,	0.084384	,	0.06752	,	0.073088	,	0.066752	,	0.0712
Index	,	0	,	1	,	2	,	3	,	4	,	5	,	6	,	7	,	8	,	9	,	10	,	11	,	12	,	13	,	14	,	15	,	16	,	17	,	18	,	19	,	20	,	21	,	22	,	23

ProfilingBuildAggregator: 
BuildBottomLevelAS	,	0.8774
BuildBottomLevelASGpu	,	0.357728
BuildTopLevelAS	,	0.9998
BuildTopLevelASGpu	,	0.083072
Duplication	,	1
Triangles	,	3936
Meshes	,	1
BottomLevels	,	1
Index	,	0

FpsAggregator: 
FPS	,	336	,	345	,	301	,	286	,	303	,	295	,	346	,	331	,	345	,	311	,	261	,	279	,	323	,	348	,	305	,	252	,	256	,	250	,	226	,	254	,	274	,	326	,	346	,	350
UPS	,	59	,	61	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	61	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	61	,	60	,	60	,	60	,	60
FrameTime	,	2.98281	,	2.91011	,	3.32879	,	3.49802	,	3.30434	,	3.39521	,	2.89477	,	3.02145	,	2.90561	,	3.22497	,	3.84348	,	3.58473	,	3.10412	,	2.88057	,	3.28848	,	3.96852	,	3.91375	,	4.00434	,	4.44116	,	3.94666	,	3.65626	,	3.07392	,	2.8918	,	2.85987
GigaRays/s	,	2.63655	,	2.70241	,	2.36252	,	2.24822	,	2.38	,	2.3163	,	2.71673	,	2.60283	,	2.7066	,	2.43857	,	2.04615	,	2.19384	,	2.53351	,	2.73013	,	2.39148	,	1.98168	,	2.00941	,	1.96395	,	1.77078	,	1.99265	,	2.15092	,	2.5584	,	2.71952	,	2.74988
Index	,	0	,	1	,	2	,	3	,	4	,	5	,	6	,	7	,	8	,	9	,	10	,	11	,	12	,	13	,	14	,	15	,	16	,	17	,	18	,	19	,	20	,	21	,	22	,	23


AccelerationStructure: 
Ray tracing acceleration structure building statistics: 
	Bottom Level Acceleration Structure: 
		Maximum [B]: 224128
		Minimum [B]: 224128
		Currently Allocated [B]: 224128
		Currently Created [num]: 1
		Current Average [B]: 224128
		Maximum Triangles [num]: 3936
		Minimum Triangles [num]: 3936
		Total Triangles [num]: 3936
		Maximum Meshes [num]: 1
		Minimum Meshes [num]: 1
		Total Meshes [num]: 1
	Top Level Acceleration Structure: 
		Maximum [B]: 64
		Minimum [B]: 64
		Currently Allocated [B]: 64
		Total Allocated [B]: 64
		Total Created [num]: 1
		Average Allocated [B]: 64
		Maximum Instances [num]: 1
		Minimum Instances [num]: 1
		Total Instances [num]: 1
	Scratch Buffer: 
		Maximum [B]: 34304
		Minimum [B]: 34304
		Currently Allocated [B]: 34304
		Total Allocated [B]: 34304
		Total Created [num]: 1
		Average Allocated [B]: 34304
ProfilingStack: 
[PROF] Profiling statistics: 
	Scopes entered : 77349
	Scopes exited : 77348
	Overhead per scope [ticks] : 1011.3
	Threads entered : 3
	Threads exited : 0
	============================
		         Sum[Mt]   Parent[%]  Samples[num]       Avg[Mt]   Last/Curr[Mt]        Self[Mt]     Self[%] 	 Name

	main:

		   24813.405 Mt   100.000 %         1 x    24813.407 Mt    24813.404 Mt        0.709 Mt     0.003 % 	 /

		   24812.765 Mt    99.997 %         1 x    24812.767 Mt    24812.764 Mt      438.068 Mt     1.765 % 	 ./Main application loop

		       2.753 Mt     0.011 %         1 x        2.753 Mt        0.000 Mt        2.753 Mt   100.000 % 	 ../TexturedRLInitialization

		       5.125 Mt     0.021 %         1 x        5.125 Mt        0.000 Mt        5.125 Mt   100.000 % 	 ../DeferredRLInitialization

		      44.150 Mt     0.178 %         1 x       44.150 Mt        0.000 Mt        1.674 Mt     3.791 % 	 ../RayTracingRLInitialization

		      42.476 Mt    96.209 %         1 x       42.476 Mt        0.000 Mt       42.476 Mt   100.000 % 	 .../PipelineBuild

		       2.924 Mt     0.012 %         1 x        2.924 Mt        0.000 Mt        2.924 Mt   100.000 % 	 ../ResolveRLInitialization

		     255.335 Mt     1.029 %         1 x      255.335 Mt        0.000 Mt        3.566 Mt     1.397 % 	 ../Initialization

		     251.769 Mt    98.603 %         1 x      251.769 Mt        0.000 Mt        1.805 Mt     0.717 % 	 .../ScenePrep

		     214.067 Mt    85.025 %         1 x      214.067 Mt        0.000 Mt       22.795 Mt    10.649 % 	 ..../SceneLoad

		     164.601 Mt    76.892 %         1 x      164.601 Mt        0.000 Mt       19.564 Mt    11.886 % 	 ...../glTF-LoadScene

		     145.037 Mt    88.114 %         2 x       72.519 Mt        0.000 Mt      145.037 Mt   100.000 % 	 ....../glTF-LoadImageData

		      14.944 Mt     6.981 %         1 x       14.944 Mt        0.000 Mt       14.944 Mt   100.000 % 	 ...../glTF-CreateScene

		      11.726 Mt     5.478 %         1 x       11.726 Mt        0.000 Mt       11.726 Mt   100.000 % 	 ...../SceneDesc-CreateScene

		       4.172 Mt     1.657 %         1 x        4.172 Mt        0.000 Mt        0.005 Mt     0.115 % 	 ..../TexturedRLPrep

		       4.167 Mt    99.885 %         1 x        4.167 Mt        0.000 Mt        1.314 Mt    31.524 % 	 ...../PrepareForRendering

		       2.558 Mt    61.392 %         1 x        2.558 Mt        0.000 Mt        2.558 Mt   100.000 % 	 ....../PrepareMaterials

		       0.295 Mt     7.084 %         1 x        0.295 Mt        0.000 Mt        0.295 Mt   100.000 % 	 ....../PrepareDrawBundle

		       3.251 Mt     1.291 %         1 x        3.251 Mt        0.000 Mt        0.005 Mt     0.157 % 	 ..../DeferredRLPrep

		       3.246 Mt    99.843 %         1 x        3.246 Mt        0.000 Mt        2.183 Mt    67.261 % 	 ...../PrepareForRendering

		       0.016 Mt     0.499 %         1 x        0.016 Mt        0.000 Mt        0.016 Mt   100.000 % 	 ....../PrepareMaterials

		       0.797 Mt    24.565 %         1 x        0.797 Mt        0.000 Mt        0.797 Mt   100.000 % 	 ....../BuildMaterialCache

		       0.249 Mt     7.675 %         1 x        0.249 Mt        0.000 Mt        0.249 Mt   100.000 % 	 ....../PrepareDrawBundle

		      16.493 Mt     6.551 %         1 x       16.493 Mt        0.000 Mt        4.421 Mt    26.809 % 	 ..../RayTracingRLPrep

		       0.240 Mt     1.458 %         1 x        0.240 Mt        0.000 Mt        0.240 Mt   100.000 % 	 ...../PrepareAccelerationStructures

		       0.023 Mt     0.141 %         1 x        0.023 Mt        0.000 Mt        0.023 Mt   100.000 % 	 ...../PrepareCache

		       0.829 Mt     5.026 %         1 x        0.829 Mt        0.000 Mt        0.829 Mt   100.000 % 	 ...../BuildCache

		       8.408 Mt    50.982 %         1 x        8.408 Mt        0.000 Mt        0.005 Mt     0.062 % 	 ...../BuildAccelerationStructures

		       8.403 Mt    99.938 %         1 x        8.403 Mt        0.000 Mt        6.526 Mt    77.660 % 	 ....../AccelerationStructureBuild

		       0.877 Mt    10.442 %         1 x        0.877 Mt        0.000 Mt        0.877 Mt   100.000 % 	 ......./BuildBottomLevelAS

		       1.000 Mt    11.898 %         1 x        1.000 Mt        0.000 Mt        1.000 Mt   100.000 % 	 ......./BuildTopLevelAS

		       2.570 Mt    15.585 %         1 x        2.570 Mt        0.000 Mt        2.570 Mt   100.000 % 	 ...../BuildShaderTables

		      11.981 Mt     4.759 %         1 x       11.981 Mt        0.000 Mt       11.981 Mt   100.000 % 	 ..../GpuSidePrep

		   24064.410 Mt    96.984 %     10607 x        2.269 Mt        6.082 Mt      764.301 Mt     3.176 % 	 ../MainLoop

		     567.895 Mt     2.360 %      1445 x        0.393 Mt        0.509 Mt      566.782 Mt    99.804 % 	 .../Update

		       1.113 Mt     0.196 %         1 x        1.113 Mt        0.000 Mt        1.113 Mt   100.000 % 	 ..../GuiModelApply

		   22732.214 Mt    94.464 %      7251 x        3.135 Mt        4.404 Mt    12981.421 Mt    57.106 % 	 .../Render

		    9750.793 Mt    42.894 %      7251 x        1.345 Mt        2.457 Mt     9705.524 Mt    99.536 % 	 ..../RayTracing

		      24.346 Mt     0.250 %      7251 x        0.003 Mt        0.006 Mt       24.346 Mt   100.000 % 	 ...../DeferredRLPrep

		      20.923 Mt     0.215 %      7251 x        0.003 Mt        0.006 Mt       20.923 Mt   100.000 % 	 ...../RayTracingRLPrep

	WindowThread:

		   24802.786 Mt   100.000 %         1 x    24802.787 Mt    24802.786 Mt    24802.786 Mt   100.000 % 	 /

	GpuThread:

		    5844.570 Mt   100.000 %      7251 x        0.806 Mt        0.466 Mt      120.745 Mt     2.066 % 	 /

		       0.450 Mt     0.008 %         1 x        0.450 Mt        0.000 Mt        0.007 Mt     1.630 % 	 ./ScenePrep

		       0.442 Mt    98.370 %         1 x        0.442 Mt        0.000 Mt        0.002 Mt     0.340 % 	 ../AccelerationStructureBuild

		       0.358 Mt    80.878 %         1 x        0.358 Mt        0.000 Mt        0.358 Mt   100.000 % 	 .../BuildBottomLevelASGpu

		       0.083 Mt    18.782 %         1 x        0.083 Mt        0.000 Mt        0.083 Mt   100.000 % 	 .../BuildTopLevelASGpu

		    5703.122 Mt    97.580 %      7251 x        0.787 Mt        0.463 Mt       16.542 Mt     0.290 % 	 ./RayTracingGpu

		     531.885 Mt     9.326 %      7251 x        0.073 Mt        0.047 Mt      531.885 Mt   100.000 % 	 ../DeferredRLGpu

		    4531.681 Mt    79.460 %      7251 x        0.625 Mt        0.342 Mt     4531.681 Mt   100.000 % 	 ../RayTracingRLGpu

		     623.014 Mt    10.924 %      7251 x        0.086 Mt        0.071 Mt      623.014 Mt   100.000 % 	 ../ResolveRLGpu

		      20.253 Mt     0.347 %      7251 x        0.003 Mt        0.003 Mt       20.253 Mt   100.000 % 	 ./Present


	============================


