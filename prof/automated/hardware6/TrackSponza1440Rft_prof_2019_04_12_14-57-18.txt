Configuration: 
Command line parameters  = E:\programming\FIT-DIP\Masters-DXR-Main-git\build\Measurement\x64\Debug\Measurement.exe --scene Sponza/Sponza.gltf --profile-automation --profile-camera-track Sponza/Sponza.trk --rt-mode --width 2560 --height 1440 --profile-output TrackSponza1440Rft --fast-build-as 
Window width             = 2560
Window height            = 1421
Window title             = Direct3D App
Back-buffer count        = 3
Fill Triangles           = enabled
Triangles Font Clock-Wise= enabled
Fast Build AS            = enabled
Target FPS               = 0
Target UPS               = 60
Tearing                  = enabled
VSync                    = disabled
GUI rendering            = disabled
Debug layer              = enabled
GPU validation           = disabled
GPU profiling            = enabled
Render mode              = Ray Tracing
Loaded scene file        = Sponza/Sponza.gltf
Using testing scene      = disabled
Duplication              = 1
Duplication in cube      = disabled
Duplication offset       = 10
BLAS duplication         = disabled
Rays per pixel           = 10

ProfilingAggregator: 
Render	,	11.5483	,	11.2263	,	11.8579	,	11.511	,	10.8553	,	11.2077	,	10.6984	,	9.1385	,	9.8013	,	10.0687	,	10.5313	,	10.5946	,	11.0425	,	11.0566	,	10.4769	,	10.498	,	10.6868	,	10.5398	,	10.1288	,	12.3134	,	14.4968	,	13.8181	,	12.1906	,	11.2066	,	10.1486	,	10.3369
Update	,	0.3244	,	0.3123	,	0.344	,	0.3349	,	0.2893	,	0.3313	,	0.3066	,	0.2897	,	0.294	,	0.2944	,	0.2906	,	0.3008	,	0.3373	,	0.3367	,	0.2949	,	0.3152	,	0.2924	,	0.3315	,	0.3056	,	0.3171	,	0.3572	,	0.3202	,	0.3351	,	0.3355	,	0.296	,	0.2991
RayTracing	,	2.9955	,	2.803	,	3.078	,	2.9351	,	2.64	,	3.0171	,	2.9552	,	2.5263	,	2.535	,	2.5391	,	2.5087	,	2.5373	,	2.9007	,	2.8844	,	2.5124	,	2.746	,	2.7464	,	3.0196	,	2.6067	,	2.9723	,	3.0361	,	3.0393	,	3.0112	,	3.0663	,	2.5607	,	2.516
RayTracingGpu	,	7.28899	,	7.25485	,	7.3689	,	7.22368	,	7.11338	,	6.96288	,	6.37584	,	5.43078	,	6.02838	,	6.27766	,	6.94826	,	6.86138	,	6.80602	,	6.81235	,	6.75334	,	6.62909	,	6.63162	,	6.18534	,	6.43293	,	8.13072	,	10.1305	,	9.58698	,	7.83306	,	6.82778	,	6.4313	,	6.5712
DeferredRLGpu	,	1.39357	,	1.45242	,	1.46019	,	1.47914	,	1.37802	,	1.30518	,	1.1513	,	0.748608	,	0.957056	,	1.16266	,	1.46198	,	1.46832	,	1.39898	,	1.3352	,	1.28883	,	1.18947	,	1.16179	,	0.910208	,	1.02	,	1.29482	,	1.97974	,	2.01312	,	1.75565	,	1.37306	,	1.21642	,	1.1887
RayTracingRLGpu	,	5.38218	,	5.28768	,	5.39485	,	5.224	,	5.20528	,	5.14522	,	4.71152	,	4.17869	,	4.5625	,	4.59373	,	4.96544	,	4.87683	,	4.89123	,	4.95706	,	4.77766	,	4.9113	,	4.94406	,	4.7537	,	4.88064	,	6.29837	,	7.61549	,	7.04672	,	5.58141	,	4.97382	,	4.7185	,	4.87456
ResolveRLGpu	,	0.511808	,	0.513824	,	0.51296	,	0.518144	,	0.528672	,	0.511072	,	0.511936	,	0.502496	,	0.507808	,	0.519872	,	0.519424	,	0.515104	,	0.514816	,	0.519104	,	0.685856	,	0.526912	,	0.524288	,	0.517408	,	0.53136	,	0.536128	,	0.533952	,	0.526272	,	0.49488	,	0.479808	,	0.493824	,	0.506624
Index	,	0	,	1	,	2	,	3	,	4	,	5	,	6	,	7	,	8	,	9	,	10	,	11	,	12	,	13	,	14	,	15	,	16	,	17	,	18	,	19	,	20	,	21	,	22	,	23	,	24	,	25

ProfilingBuildAggregator: 
BuildBottomLevelAS	,	4.1293
BuildBottomLevelASGpu	,	15.5233
BuildTopLevelAS	,	1.6808
BuildTopLevelASGpu	,	0.611936
Duplication	,	1
Triangles	,	262267
Meshes	,	103
BottomLevels	,	1
Index	,	0

FpsAggregator: 
FPS	,	77	,	85	,	86	,	84	,	86	,	88	,	91	,	97	,	102	,	99	,	93	,	90	,	86	,	88	,	91	,	92	,	92	,	94	,	95	,	93	,	69	,	69	,	76	,	82	,	93	,	92
UPS	,	59	,	60	,	61	,	60	,	61	,	60	,	60	,	60	,	60	,	61	,	60	,	60	,	60	,	60	,	61	,	60	,	60	,	61	,	60	,	60	,	60	,	60	,	61	,	60	,	61	,	60
FrameTime	,	13.0814	,	11.7692	,	11.7505	,	11.977	,	11.6791	,	11.365	,	11.0932	,	10.3101	,	9.83498	,	10.1335	,	10.8204	,	11.159	,	11.6475	,	11.3973	,	11.0694	,	10.8941	,	10.9423	,	10.6883	,	10.6293	,	10.7738	,	14.5164	,	14.5093	,	13.2697	,	12.2209	,	10.8485	,	10.9014
GigaRays/s	,	2.78087	,	3.09092	,	3.09583	,	3.03729	,	3.11475	,	3.20085	,	3.27926	,	3.52834	,	3.6988	,	3.58984	,	3.36194	,	3.25993	,	3.12321	,	3.19178	,	3.28632	,	3.33921	,	3.32449	,	3.40351	,	3.42239	,	3.37647	,	2.50597	,	2.5072	,	2.74141	,	2.97668	,	3.35323	,	3.33697
Index	,	0	,	1	,	2	,	3	,	4	,	5	,	6	,	7	,	8	,	9	,	10	,	11	,	12	,	13	,	14	,	15	,	16	,	17	,	18	,	19	,	20	,	21	,	22	,	23	,	24	,	25


AccelerationStructure: 
Ray tracing acceleration structure building statistics: 
	Bottom Level Acceleration Structure: 
		Maximum [B]: 14623872
		Minimum [B]: 14623872
		Currently Allocated [B]: 14623872
		Currently Created [num]: 1
		Current Average [B]: 14623872
		Maximum Triangles [num]: 262267
		Minimum Triangles [num]: 262267
		Total Triangles [num]: 262267
		Maximum Meshes [num]: 103
		Minimum Meshes [num]: 103
		Total Meshes [num]: 103
	Top Level Acceleration Structure: 
		Maximum [B]: 64
		Minimum [B]: 64
		Currently Allocated [B]: 64
		Total Allocated [B]: 64
		Total Created [num]: 1
		Average Allocated [B]: 64
		Maximum Instances [num]: 1
		Minimum Instances [num]: 1
		Total Instances [num]: 1
	Scratch Buffer: 
		Maximum [B]: 11660800
		Minimum [B]: 11660800
		Currently Allocated [B]: 11660800
		Total Allocated [B]: 11660800
		Total Created [num]: 1
		Average Allocated [B]: 11660800
ProfilingStack: 
[PROF] Profiling statistics: 
	Scopes entered : 28029
	Scopes exited : 28028
	Overhead per scope [ticks] : 1015.7
	Threads entered : 3
	Threads exited : 0
	============================
		         Sum[Mt]   Parent[%]  Samples[num]       Avg[Mt]   Last/Curr[Mt]        Self[Mt]     Self[%] 	 Name

	main:

		   35541.205 Mt   100.000 %         1 x    35541.207 Mt    35541.205 Mt        0.740 Mt     0.002 % 	 /

		   35540.511 Mt    99.998 %         1 x    35540.513 Mt    35540.511 Mt      320.442 Mt     0.902 % 	 ./Main application loop

		       4.625 Mt     0.013 %         1 x        4.625 Mt        0.000 Mt        4.625 Mt   100.000 % 	 ../TexturedRLInitialization

		       9.962 Mt     0.028 %         1 x        9.962 Mt        0.000 Mt        9.962 Mt   100.000 % 	 ../DeferredRLInitialization

		      66.612 Mt     0.187 %         1 x       66.612 Mt        0.000 Mt        2.867 Mt     4.305 % 	 ../RayTracingRLInitialization

		      63.745 Mt    95.695 %         1 x       63.745 Mt        0.000 Mt       63.745 Mt   100.000 % 	 .../PipelineBuild

		       3.259 Mt     0.009 %         1 x        3.259 Mt        0.000 Mt        3.259 Mt   100.000 % 	 ../ResolveRLInitialization

		    9011.254 Mt    25.355 %         1 x     9011.254 Mt        0.000 Mt        8.400 Mt     0.093 % 	 ../Initialization

		    9002.855 Mt    99.907 %         1 x     9002.855 Mt        0.000 Mt        3.417 Mt     0.038 % 	 .../ScenePrep

		    8906.361 Mt    98.928 %         1 x     8906.361 Mt        0.000 Mt      315.040 Mt     3.537 % 	 ..../SceneLoad

		    8161.739 Mt    91.639 %         1 x     8161.739 Mt        0.000 Mt     1106.548 Mt    13.558 % 	 ...../glTF-LoadScene

		    7055.191 Mt    86.442 %        69 x      102.249 Mt        0.000 Mt     7055.191 Mt   100.000 % 	 ....../glTF-LoadImageData

		     317.325 Mt     3.563 %         1 x      317.325 Mt        0.000 Mt      317.325 Mt   100.000 % 	 ...../glTF-CreateScene

		     112.257 Mt     1.260 %         1 x      112.257 Mt        0.000 Mt      112.257 Mt   100.000 % 	 ...../SceneDesc-CreateScene

		       7.069 Mt     0.079 %         1 x        7.069 Mt        0.000 Mt        0.006 Mt     0.086 % 	 ..../TexturedRLPrep

		       7.063 Mt    99.914 %         1 x        7.063 Mt        0.000 Mt        1.950 Mt    27.605 % 	 ...../PrepareForRendering

		       2.769 Mt    39.210 %         1 x        2.769 Mt        0.000 Mt        2.769 Mt   100.000 % 	 ....../PrepareMaterials

		       2.344 Mt    33.186 %         1 x        2.344 Mt        0.000 Mt        2.344 Mt   100.000 % 	 ....../PrepareDrawBundle

		       8.349 Mt     0.093 %         1 x        8.349 Mt        0.000 Mt        0.005 Mt     0.059 % 	 ..../DeferredRLPrep

		       8.344 Mt    99.941 %         1 x        8.344 Mt        0.000 Mt        3.661 Mt    43.879 % 	 ...../PrepareForRendering

		       0.021 Mt     0.258 %         1 x        0.021 Mt        0.000 Mt        0.021 Mt   100.000 % 	 ....../PrepareMaterials

		       2.238 Mt    26.823 %         1 x        2.238 Mt        0.000 Mt        2.238 Mt   100.000 % 	 ....../BuildMaterialCache

		       2.423 Mt    29.040 %         1 x        2.423 Mt        0.000 Mt        2.423 Mt   100.000 % 	 ....../PrepareDrawBundle

		      26.320 Mt     0.292 %         1 x       26.320 Mt        0.000 Mt        5.442 Mt    20.675 % 	 ..../RayTracingRLPrep

		       0.587 Mt     2.231 %         1 x        0.587 Mt        0.000 Mt        0.587 Mt   100.000 % 	 ...../PrepareAccelerationStructures

		       0.025 Mt     0.094 %         1 x        0.025 Mt        0.000 Mt        0.025 Mt   100.000 % 	 ...../PrepareCache

		       5.245 Mt    19.930 %         1 x        5.245 Mt        0.000 Mt        5.245 Mt   100.000 % 	 ...../BuildCache

		       8.992 Mt    34.166 %         1 x        8.992 Mt        0.000 Mt        0.004 Mt     0.047 % 	 ...../BuildAccelerationStructures

		       8.988 Mt    99.953 %         1 x        8.988 Mt        0.000 Mt        3.178 Mt    35.358 % 	 ....../AccelerationStructureBuild

		       4.129 Mt    45.942 %         1 x        4.129 Mt        0.000 Mt        4.129 Mt   100.000 % 	 ......./BuildBottomLevelAS

		       1.681 Mt    18.700 %         1 x        1.681 Mt        0.000 Mt        1.681 Mt   100.000 % 	 ......./BuildTopLevelAS

		       6.028 Mt    22.904 %         1 x        6.028 Mt        0.000 Mt        6.028 Mt   100.000 % 	 ...../BuildShaderTables

		      51.339 Mt     0.570 %         1 x       51.339 Mt        0.000 Mt       51.339 Mt   100.000 % 	 ..../GpuSidePrep

		   26124.357 Mt    73.506 %      5738 x        4.553 Mt       22.629 Mt      142.456 Mt     0.545 % 	 ../MainLoop

		     586.686 Mt     2.246 %      1567 x        0.374 Mt        0.467 Mt      585.735 Mt    99.838 % 	 .../Update

		       0.952 Mt     0.162 %         1 x        0.952 Mt        0.000 Mt        0.952 Mt   100.000 % 	 ..../GuiModelApply

		   25395.215 Mt    97.209 %      2291 x       11.085 Mt       13.811 Mt    18930.291 Mt    74.543 % 	 .../Render

		    6464.924 Mt    25.457 %      2291 x        2.822 Mt        4.805 Mt     6449.910 Mt    99.768 % 	 ..../RayTracing

		       7.517 Mt     0.116 %      2291 x        0.003 Mt        0.005 Mt        7.517 Mt   100.000 % 	 ...../DeferredRLPrep

		       7.497 Mt     0.116 %      2291 x        0.003 Mt        0.007 Mt        7.497 Mt   100.000 % 	 ...../RayTracingRLPrep

	WindowThread:

		   35536.470 Mt   100.000 %         1 x    35536.470 Mt    35536.470 Mt    35536.470 Mt   100.000 % 	 /

	GpuThread:

		   16228.920 Mt   100.000 %      2291 x        7.084 Mt        6.596 Mt      159.500 Mt     0.983 % 	 /

		      16.175 Mt     0.100 %         1 x       16.175 Mt        0.000 Mt        0.037 Mt     0.229 % 	 ./ScenePrep

		      16.138 Mt    99.771 %         1 x       16.138 Mt        0.000 Mt        0.003 Mt     0.016 % 	 ../AccelerationStructureBuild

		      15.523 Mt    96.192 %         1 x       15.523 Mt        0.000 Mt       15.523 Mt   100.000 % 	 .../BuildBottomLevelASGpu

		       0.612 Mt     3.792 %         1 x        0.612 Mt        0.000 Mt        0.612 Mt   100.000 % 	 .../BuildTopLevelASGpu

		   16035.885 Mt    98.811 %      2291 x        7.000 Mt        6.594 Mt        3.170 Mt     0.020 % 	 ./RayTracingGpu

		    3046.756 Mt    19.000 %      2291 x        1.330 Mt        1.206 Mt     3046.756 Mt   100.000 % 	 ../DeferredRLGpu

		   11795.116 Mt    73.555 %      2291 x        5.148 Mt        4.878 Mt    11795.116 Mt   100.000 % 	 ../RayTracingRLGpu

		    1190.844 Mt     7.426 %      2291 x        0.520 Mt        0.507 Mt     1190.844 Mt   100.000 % 	 ../ResolveRLGpu

		      17.360 Mt     0.107 %      2291 x        0.008 Mt        0.001 Mt       17.360 Mt   100.000 % 	 ./Present


	============================


