Configuration: 
Command line parameters  = E:\programming\FIT-DIP\Masters-DXR-Main-git\build\Measurement\x64\Debug\Measurement.exe --scene Suzanne/Suzanne.gltf --profile-automation --profile-camera-track Suzanne/Suzanne.trk --rt-mode --width 2560 --height 1440 --profile-output TrackSuzanne1440Rt 
Window width             = 2560
Window height            = 1421
Window title             = Direct3D App
Back-buffer count        = 3
Fill Triangles           = enabled
Triangles Font Clock-Wise= enabled
Fast Build AS            = disabled
Target FPS               = 0
Target UPS               = 60
Tearing                  = enabled
VSync                    = disabled
GUI rendering            = disabled
Debug layer              = enabled
GPU validation           = disabled
GPU profiling            = enabled
Render mode              = Ray Tracing
Loaded scene file        = Suzanne/Suzanne.gltf
Using testing scene      = disabled
Duplication              = 1
Duplication in cube      = disabled
Duplication offset       = 10
BLAS duplication         = disabled
Rays per pixel           = 10

ProfilingAggregator: 
Render	,	3.3895	,	3.697	,	3.791	,	3.3788	,	3.0093	,	2.7737	,	2.6855	,	2.7342	,	3.0788	,	3.6287	,	3.4594	,	3.2147	,	2.9517	,	3.1652	,	3.6391	,	4.0159	,	4.1603	,	5.1635	,	7.0795	,	9.8419	,	4.0634	,	3.0785	,	2.7246	,	2.7851
Update	,	0.2872	,	0.2873	,	0.2926	,	0.2851	,	0.2854	,	0.2917	,	0.2865	,	0.2832	,	0.2886	,	0.2841	,	0.2848	,	0.2812	,	0.3166	,	0.2868	,	0.2832	,	0.2946	,	0.286	,	0.2918	,	0.3495	,	0.3463	,	0.2934	,	0.2852	,	0.2877	,	0.2764
RayTracing	,	1.2323	,	1.2478	,	1.2351	,	1.2369	,	1.2555	,	1.2281	,	1.2285	,	1.241	,	1.2335	,	1.2452	,	1.2329	,	1.2469	,	1.2918	,	1.2411	,	1.2399	,	1.2446	,	1.2406	,	1.2496	,	1.5784	,	4.4168	,	1.239	,	1.2328	,	1.2311	,	1.2317
RayTracingGpu	,	1.2447	,	1.48301	,	1.54634	,	1.28566	,	0.85632	,	0.68592	,	0.601664	,	0.612928	,	0.937952	,	1.41386	,	1.30688	,	0.945376	,	0.632192	,	0.827264	,	1.28694	,	1.71843	,	1.92394	,	2.85904	,	4.29261	,	2.696	,	1.91763	,	0.941472	,	0.61232	,	0.675232
DeferredRLGpu	,	0.13568	,	0.15968	,	0.165568	,	0.12864	,	0.085984	,	0.072704	,	0.062112	,	0.064	,	0.0968	,	0.141696	,	0.124736	,	0.092224	,	0.064128	,	0.085632	,	0.137472	,	0.184928	,	0.189504	,	0.222272	,	0.328832	,	0.257184	,	0.214816	,	0.111712	,	0.067136	,	0.071648
RayTracingRLGpu	,	0.897408	,	1.0953	,	1.13677	,	0.927552	,	0.569152	,	0.429504	,	0.362048	,	0.363936	,	0.589408	,	0.992448	,	0.963232	,	0.653632	,	0.386976	,	0.494944	,	0.876704	,	1.28531	,	1.45856	,	2.31258	,	3.5504	,	2.11894	,	1.43962	,	0.629728	,	0.36432	,	0.421664
ResolveRLGpu	,	0.209984	,	0.227072	,	0.243072	,	0.228384	,	0.200256	,	0.182336	,	0.176352	,	0.183168	,	0.250208	,	0.277664	,	0.217504	,	0.198368	,	0.179712	,	0.244448	,	0.270912	,	0.246784	,	0.27488	,	0.3232	,	0.412256	,	0.318784	,	0.26224	,	0.198784	,	0.179744	,	0.180576
Index	,	0	,	1	,	2	,	3	,	4	,	5	,	6	,	7	,	8	,	9	,	10	,	11	,	12	,	13	,	14	,	15	,	16	,	17	,	18	,	19	,	20	,	21	,	22	,	23

ProfilingBuildAggregator: 
BuildBottomLevelAS	,	1.0292
BuildBottomLevelASGpu	,	0.353472
BuildTopLevelAS	,	1.0982
BuildTopLevelASGpu	,	0.083296
Duplication	,	1
Triangles	,	3936
Meshes	,	1
BottomLevels	,	1
Index	,	0

FpsAggregator: 
FPS	,	256	,	241	,	245	,	255	,	294	,	313	,	340	,	335	,	315	,	273	,	259	,	283	,	322	,	336	,	292	,	242	,	229	,	205	,	152	,	152	,	198	,	267	,	320	,	340
UPS	,	59	,	61	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	61	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	61	,	60
FrameTime	,	3.91761	,	4.1612	,	4.08854	,	3.92878	,	3.4032	,	3.19599	,	2.94491	,	2.98529	,	3.183	,	3.66949	,	3.86789	,	3.5444	,	3.11049	,	2.98052	,	3.42571	,	4.13399	,	4.37019	,	4.88192	,	6.60382	,	6.59611	,	5.06183	,	3.75107	,	3.12628	,	2.94292
GigaRays/s	,	9.28567	,	8.74209	,	8.89745	,	9.25926	,	10.6892	,	11.3823	,	12.3527	,	12.1856	,	11.4287	,	9.91352	,	9.40501	,	10.2634	,	11.6952	,	12.2051	,	10.619	,	8.79963	,	8.32403	,	7.4515	,	5.50857	,	5.515	,	7.18665	,	9.69794	,	11.6361	,	12.3611
Index	,	0	,	1	,	2	,	3	,	4	,	5	,	6	,	7	,	8	,	9	,	10	,	11	,	12	,	13	,	14	,	15	,	16	,	17	,	18	,	19	,	20	,	21	,	22	,	23


AccelerationStructure: 
Ray tracing acceleration structure building statistics: 
	Bottom Level Acceleration Structure: 
		Maximum [B]: 224128
		Minimum [B]: 224128
		Currently Allocated [B]: 224128
		Currently Created [num]: 1
		Current Average [B]: 224128
		Maximum Triangles [num]: 3936
		Minimum Triangles [num]: 3936
		Total Triangles [num]: 3936
		Maximum Meshes [num]: 1
		Minimum Meshes [num]: 1
		Total Meshes [num]: 1
	Top Level Acceleration Structure: 
		Maximum [B]: 64
		Minimum [B]: 64
		Currently Allocated [B]: 64
		Total Allocated [B]: 64
		Total Created [num]: 1
		Average Allocated [B]: 64
		Maximum Instances [num]: 1
		Minimum Instances [num]: 1
		Total Instances [num]: 1
	Scratch Buffer: 
		Maximum [B]: 34304
		Minimum [B]: 34304
		Currently Allocated [B]: 34304
		Total Allocated [B]: 34304
		Total Created [num]: 1
		Average Allocated [B]: 34304
ProfilingStack: 
[PROF] Profiling statistics: 
	Scopes entered : 67491
	Scopes exited : 67490
	Overhead per scope [ticks] : 1002.1
	Threads entered : 3
	Threads exited : 0
	============================
		         Sum[Mt]   Parent[%]  Samples[num]       Avg[Mt]   Last/Curr[Mt]        Self[Mt]     Self[%] 	 Name

	main:

		   24738.805 Mt   100.000 %         1 x    24738.807 Mt    24738.804 Mt        0.754 Mt     0.003 % 	 /

		   24738.100 Mt    99.997 %         1 x    24738.102 Mt    24738.100 Mt      357.028 Mt     1.443 % 	 ./Main application loop

		       2.812 Mt     0.011 %         1 x        2.812 Mt        0.000 Mt        2.812 Mt   100.000 % 	 ../TexturedRLInitialization

		       5.527 Mt     0.022 %         1 x        5.527 Mt        0.000 Mt        5.527 Mt   100.000 % 	 ../DeferredRLInitialization

		      50.623 Mt     0.205 %         1 x       50.623 Mt        0.000 Mt        1.984 Mt     3.920 % 	 ../RayTracingRLInitialization

		      48.639 Mt    96.080 %         1 x       48.639 Mt        0.000 Mt       48.639 Mt   100.000 % 	 .../PipelineBuild

		       3.234 Mt     0.013 %         1 x        3.234 Mt        0.000 Mt        3.234 Mt   100.000 % 	 ../ResolveRLInitialization

		     263.250 Mt     1.064 %         1 x      263.250 Mt        0.000 Mt        8.355 Mt     3.174 % 	 ../Initialization

		     254.894 Mt    96.826 %         1 x      254.894 Mt        0.000 Mt        1.976 Mt     0.775 % 	 .../ScenePrep

		     214.082 Mt    83.988 %         1 x      214.082 Mt        0.000 Mt       21.485 Mt    10.036 % 	 ..../SceneLoad

		     164.803 Mt    76.981 %         1 x      164.803 Mt        0.000 Mt       19.956 Mt    12.109 % 	 ...../glTF-LoadScene

		     144.847 Mt    87.891 %         2 x       72.424 Mt        0.000 Mt      144.847 Mt   100.000 % 	 ....../glTF-LoadImageData

		      14.641 Mt     6.839 %         1 x       14.641 Mt        0.000 Mt       14.641 Mt   100.000 % 	 ...../glTF-CreateScene

		      13.152 Mt     6.144 %         1 x       13.152 Mt        0.000 Mt       13.152 Mt   100.000 % 	 ...../SceneDesc-CreateScene

		       1.838 Mt     0.721 %         1 x        1.838 Mt        0.000 Mt        0.005 Mt     0.267 % 	 ..../TexturedRLPrep

		       1.833 Mt    99.733 %         1 x        1.833 Mt        0.000 Mt        0.727 Mt    39.652 % 	 ...../PrepareForRendering

		       0.812 Mt    44.305 %         1 x        0.812 Mt        0.000 Mt        0.812 Mt   100.000 % 	 ....../PrepareMaterials

		       0.294 Mt    16.043 %         1 x        0.294 Mt        0.000 Mt        0.294 Mt   100.000 % 	 ....../PrepareDrawBundle

		       4.011 Mt     1.574 %         1 x        4.011 Mt        0.000 Mt        0.006 Mt     0.152 % 	 ..../DeferredRLPrep

		       4.005 Mt    99.848 %         1 x        4.005 Mt        0.000 Mt        1.915 Mt    47.814 % 	 ...../PrepareForRendering

		       0.020 Mt     0.492 %         1 x        0.020 Mt        0.000 Mt        0.020 Mt   100.000 % 	 ....../PrepareMaterials

		       1.228 Mt    30.666 %         1 x        1.228 Mt        0.000 Mt        1.228 Mt   100.000 % 	 ....../BuildMaterialCache

		       0.842 Mt    21.028 %         1 x        0.842 Mt        0.000 Mt        0.842 Mt   100.000 % 	 ....../PrepareDrawBundle

		      21.582 Mt     8.467 %         1 x       21.582 Mt        0.000 Mt        5.219 Mt    24.180 % 	 ..../RayTracingRLPrep

		       0.740 Mt     3.428 %         1 x        0.740 Mt        0.000 Mt        0.740 Mt   100.000 % 	 ...../PrepareAccelerationStructures

		       0.075 Mt     0.349 %         1 x        0.075 Mt        0.000 Mt        0.075 Mt   100.000 % 	 ...../PrepareCache

		       1.000 Mt     4.632 %         1 x        1.000 Mt        0.000 Mt        1.000 Mt   100.000 % 	 ...../BuildCache

		       8.284 Mt    38.383 %         1 x        8.284 Mt        0.000 Mt        0.012 Mt     0.150 % 	 ...../BuildAccelerationStructures

		       8.271 Mt    99.850 %         1 x        8.271 Mt        0.000 Mt        6.144 Mt    74.280 % 	 ....../AccelerationStructureBuild

		       1.029 Mt    12.443 %         1 x        1.029 Mt        0.000 Mt        1.029 Mt   100.000 % 	 ......./BuildBottomLevelAS

		       1.098 Mt    13.277 %         1 x        1.098 Mt        0.000 Mt        1.098 Mt   100.000 % 	 ......./BuildTopLevelAS

		       6.265 Mt    29.028 %         1 x        6.265 Mt        0.000 Mt        6.265 Mt   100.000 % 	 ...../BuildShaderTables

		      11.405 Mt     4.475 %         1 x       11.405 Mt        0.000 Mt       11.405 Mt   100.000 % 	 ..../GpuSidePrep

		   24055.626 Mt    97.241 %      7815 x        3.078 Mt        4.247 Mt      729.847 Mt     3.034 % 	 ../MainLoop

		     548.857 Mt     2.282 %      1444 x        0.380 Mt        0.261 Mt      547.964 Mt    99.837 % 	 .../Update

		       0.893 Mt     0.163 %         1 x        0.893 Mt        0.000 Mt        0.893 Mt   100.000 % 	 ..../GuiModelApply

		   22776.923 Mt    94.684 %      6466 x        3.523 Mt        3.607 Mt    14390.710 Mt    63.181 % 	 .../Render

		    8386.212 Mt    36.819 %      6466 x        1.297 Mt        1.589 Mt     8347.063 Mt    99.533 % 	 ..../RayTracing

		      21.134 Mt     0.252 %      6466 x        0.003 Mt        0.004 Mt       21.134 Mt   100.000 % 	 ...../DeferredRLPrep

		      18.015 Mt     0.215 %      6466 x        0.003 Mt        0.004 Mt       18.015 Mt   100.000 % 	 ...../RayTracingRLPrep

	WindowThread:

		   24727.360 Mt   100.000 %         1 x    24727.360 Mt    24727.360 Mt    24727.360 Mt   100.000 % 	 /

	GpuThread:

		    8235.102 Mt   100.000 %      6466 x        1.274 Mt        0.680 Mt      160.786 Mt     1.952 % 	 /

		       0.445 Mt     0.005 %         1 x        0.445 Mt        0.000 Mt        0.006 Mt     1.461 % 	 ./ScenePrep

		       0.438 Mt    98.539 %         1 x        0.438 Mt        0.000 Mt        0.001 Mt     0.292 % 	 ../AccelerationStructureBuild

		       0.353 Mt    80.693 %         1 x        0.353 Mt        0.000 Mt        0.353 Mt   100.000 % 	 .../BuildBottomLevelASGpu

		       0.083 Mt    19.015 %         1 x        0.083 Mt        0.000 Mt        0.083 Mt   100.000 % 	 .../BuildTopLevelASGpu

		    8050.122 Mt    97.754 %      6466 x        1.245 Mt        0.677 Mt        9.298 Mt     0.115 % 	 ./RayTracingGpu

		     797.929 Mt     9.912 %      6466 x        0.123 Mt        0.072 Mt      797.929 Mt   100.000 % 	 ../DeferredRLGpu

		    5749.126 Mt    71.417 %      6466 x        0.889 Mt        0.424 Mt     5749.126 Mt   100.000 % 	 ../RayTracingRLGpu

		    1493.769 Mt    18.556 %      6466 x        0.231 Mt        0.179 Mt     1493.769 Mt   100.000 % 	 ../ResolveRLGpu

		      23.749 Mt     0.288 %      6466 x        0.004 Mt        0.002 Mt       23.749 Mt   100.000 % 	 ./Present


	============================


