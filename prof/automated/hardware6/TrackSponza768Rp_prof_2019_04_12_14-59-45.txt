Configuration: 
Command line parameters  = E:\programming\FIT-DIP\Masters-DXR-Main-git\build\Measurement\x64\Debug\Measurement.exe --scene Sponza/Sponza.gltf --profile-automation --profile-camera-track Sponza/Sponza.trk --rt-mode-pure --width 1024 --height 768 --profile-output TrackSponza768Rp 
Window width             = 1024
Window height            = 768
Window title             = Direct3D App
Back-buffer count        = 3
Fill Triangles           = enabled
Triangles Font Clock-Wise= enabled
Fast Build AS            = disabled
Target FPS               = 0
Target UPS               = 60
Tearing                  = enabled
VSync                    = disabled
GUI rendering            = disabled
Debug layer              = enabled
GPU validation           = disabled
GPU profiling            = enabled
Render mode              = Ray Tracing
Loaded scene file        = Sponza/Sponza.gltf
Using testing scene      = disabled
Duplication              = 1
Duplication in cube      = disabled
Duplication offset       = 10
BLAS duplication         = disabled
Rays per pixel           = 9

ProfilingAggregator: 
Render	,	3.6337	,	3.816	,	3.6256	,	3.4766	,	4.2019	,	4.0634	,	4.2157	,	3.8346	,	3.2106	,	4.0525	,	4.1214	,	3.8848	,	3.4914	,	3.7907	,	3.5825	,	4.0047	,	3.8606	,	3.5515	,	3.3558	,	3.8628	,	4.9263	,	3.9325	,	3.8585	,	3.4502	,	3.6096	,	3.8823
Update	,	0.4604	,	0.8718	,	0.2858	,	0.4664	,	0.8088	,	0.3938	,	0.6722	,	0.8803	,	0.8703	,	0.324	,	0.8642	,	0.549	,	0.4805	,	0.3927	,	0.2818	,	0.6942	,	0.2782	,	0.3342	,	0.329	,	0.6505	,	0.2845	,	0.3365	,	0.3422	,	0.2848	,	0.4155	,	0.3729
RayTracing	,	1.0634	,	1.2409	,	1.0642	,	1.0617	,	1.5312	,	1.5257	,	1.7018	,	1.4568	,	1.0491	,	1.1609	,	1.3576	,	1.2414	,	1.0606	,	1.2529	,	1.2415	,	1.3572	,	1.2569	,	1.2421	,	1.0758	,	1.2342	,	1.5412	,	1.1116	,	1.2296	,	1.0542	,	1.0621	,	1.2835
RayTracingGpu	,	1.72496	,	1.49773	,	1.55776	,	1.5073	,	1.40086	,	1.36112	,	1.31914	,	1.19661	,	1.30275	,	1.56483	,	1.39757	,	1.47286	,	1.44746	,	1.46922	,	1.40202	,	1.42074	,	1.42989	,	1.37165	,	1.37677	,	1.40086	,	2.18224	,	1.87997	,	1.6927	,	1.55917	,	1.39984	,	1.45491
RayTracingRLGpu	,	1.64538	,	1.43469	,	1.49424	,	1.4447	,	1.33795	,	1.29853	,	1.25642	,	1.13315	,	1.23949	,	1.50122	,	1.3345	,	1.41082	,	1.38493	,	1.40691	,	1.34003	,	1.35853	,	1.36768	,	1.30934	,	1.31392	,	1.33878	,	2.12022	,	1.81741	,	1.63053	,	1.49728	,	1.33744	,	1.39229
ResolveRLGpu	,	0.078848	,	0.062272	,	0.062624	,	0.061984	,	0.062144	,	0.061856	,	0.061824	,	0.062528	,	0.062144	,	0.062464	,	0.062272	,	0.06112	,	0.061792	,	0.061568	,	0.06112	,	0.06144	,	0.061568	,	0.061696	,	0.061984	,	0.06128	,	0.061248	,	0.061856	,	0.061248	,	0.061024	,	0.06176	,	0.06176
Index	,	0	,	1	,	2	,	3	,	4	,	5	,	6	,	7	,	8	,	9	,	10	,	11	,	12	,	13	,	14	,	15	,	16	,	17	,	18	,	19	,	20	,	21	,	22	,	23	,	24	,	25

ProfilingBuildAggregator: 
BuildBottomLevelAS	,	3.0802
BuildBottomLevelASGpu	,	16.0119
BuildTopLevelAS	,	1.0867
BuildTopLevelASGpu	,	0.609568
Duplication	,	1
Triangles	,	262267
Meshes	,	103
BottomLevels	,	1
Index	,	0

FpsAggregator: 
FPS	,	229	,	260	,	255	,	256	,	256	,	260	,	272	,	279	,	276	,	274	,	266	,	258	,	258	,	257	,	262	,	263	,	260	,	266	,	265	,	271	,	236	,	220	,	242	,	244	,	268	,	261
UPS	,	59	,	61	,	60	,	60	,	60	,	60	,	60	,	60	,	61	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	61	,	60	,	60	,	60	,	60	,	61	,	60	,	60	,	60
FrameTime	,	4.37641	,	3.8579	,	3.93151	,	3.91221	,	3.92105	,	3.85534	,	3.68695	,	3.58971	,	3.63087	,	3.66081	,	3.77004	,	3.8857	,	3.87747	,	3.89152	,	3.82757	,	3.80366	,	3.85691	,	3.7727	,	3.77788	,	3.69899	,	4.25284	,	4.56226	,	4.14545	,	4.10314	,	3.74418	,	3.84471
GigaRays/s	,	1.61728	,	1.83465	,	1.8003	,	1.80918	,	1.8051	,	1.83586	,	1.91971	,	1.97172	,	1.94937	,	1.93342	,	1.8774	,	1.82152	,	1.82539	,	1.8188	,	1.84919	,	1.86081	,	1.83512	,	1.87608	,	1.87351	,	1.91346	,	1.66427	,	1.5514	,	1.70739	,	1.72499	,	1.89037	,	1.84094
Index	,	0	,	1	,	2	,	3	,	4	,	5	,	6	,	7	,	8	,	9	,	10	,	11	,	12	,	13	,	14	,	15	,	16	,	17	,	18	,	19	,	20	,	21	,	22	,	23	,	24	,	25


AccelerationStructure: 
Ray tracing acceleration structure building statistics: 
	Bottom Level Acceleration Structure: 
		Maximum [B]: 14623872
		Minimum [B]: 14623872
		Currently Allocated [B]: 14623872
		Currently Created [num]: 1
		Current Average [B]: 14623872
		Maximum Triangles [num]: 262267
		Minimum Triangles [num]: 262267
		Total Triangles [num]: 262267
		Maximum Meshes [num]: 103
		Minimum Meshes [num]: 103
		Total Meshes [num]: 103
	Top Level Acceleration Structure: 
		Maximum [B]: 64
		Minimum [B]: 64
		Currently Allocated [B]: 64
		Total Allocated [B]: 64
		Total Created [num]: 1
		Average Allocated [B]: 64
		Maximum Instances [num]: 1
		Minimum Instances [num]: 1
		Total Instances [num]: 1
	Scratch Buffer: 
		Maximum [B]: 11660800
		Minimum [B]: 11660800
		Currently Allocated [B]: 11660800
		Total Allocated [B]: 11660800
		Total Created [num]: 1
		Average Allocated [B]: 11660800
ProfilingStack: 
[PROF] Profiling statistics: 
	Scopes entered : 58931
	Scopes exited : 58930
	Overhead per scope [ticks] : 1009.1
	Threads entered : 3
	Threads exited : 0
	============================
		         Sum[Mt]   Parent[%]  Samples[num]       Avg[Mt]   Last/Curr[Mt]        Self[Mt]     Self[%] 	 Name

	main:

		   35520.372 Mt   100.000 %         1 x    35520.373 Mt    35520.371 Mt        0.774 Mt     0.002 % 	 /

		   35519.642 Mt    99.998 %         1 x    35519.644 Mt    35519.642 Mt      374.379 Mt     1.054 % 	 ./Main application loop

		       2.747 Mt     0.008 %         1 x        2.747 Mt        0.000 Mt        2.747 Mt   100.000 % 	 ../TexturedRLInitialization

		       6.234 Mt     0.018 %         1 x        6.234 Mt        0.000 Mt        6.234 Mt   100.000 % 	 ../DeferredRLInitialization

		      50.825 Mt     0.143 %         1 x       50.825 Mt        0.000 Mt        2.320 Mt     4.564 % 	 ../RayTracingRLInitialization

		      48.505 Mt    95.436 %         1 x       48.505 Mt        0.000 Mt       48.505 Mt   100.000 % 	 .../PipelineBuild

		       6.516 Mt     0.018 %         1 x        6.516 Mt        0.000 Mt        6.516 Mt   100.000 % 	 ../ResolveRLInitialization

		    9056.301 Mt    25.497 %         1 x     9056.301 Mt        0.000 Mt        8.370 Mt     0.092 % 	 ../Initialization

		    9047.931 Mt    99.908 %         1 x     9047.931 Mt        0.000 Mt        4.076 Mt     0.045 % 	 .../ScenePrep

		    8918.736 Mt    98.572 %         1 x     8918.736 Mt        0.000 Mt      356.313 Mt     3.995 % 	 ..../SceneLoad

		    8164.198 Mt    91.540 %         1 x     8164.198 Mt        0.000 Mt     1122.239 Mt    13.746 % 	 ...../glTF-LoadScene

		    7041.958 Mt    86.254 %        69 x      102.057 Mt        0.000 Mt     7041.958 Mt   100.000 % 	 ....../glTF-LoadImageData

		     300.113 Mt     3.365 %         1 x      300.113 Mt        0.000 Mt      300.113 Mt   100.000 % 	 ...../glTF-CreateScene

		      98.113 Mt     1.100 %         1 x       98.113 Mt        0.000 Mt       98.113 Mt   100.000 % 	 ...../SceneDesc-CreateScene

		      19.777 Mt     0.219 %         1 x       19.777 Mt        0.000 Mt        0.007 Mt     0.033 % 	 ..../TexturedRLPrep

		      19.770 Mt    99.967 %         1 x       19.770 Mt        0.000 Mt        2.623 Mt    13.267 % 	 ...../PrepareForRendering

		       9.027 Mt    45.661 %         1 x        9.027 Mt        0.000 Mt        9.027 Mt   100.000 % 	 ....../PrepareMaterials

		       8.120 Mt    41.072 %         1 x        8.120 Mt        0.000 Mt        8.120 Mt   100.000 % 	 ....../PrepareDrawBundle

		      14.633 Mt     0.162 %         1 x       14.633 Mt        0.000 Mt        0.013 Mt     0.091 % 	 ..../DeferredRLPrep

		      14.620 Mt    99.909 %         1 x       14.620 Mt        0.000 Mt        6.098 Mt    41.709 % 	 ...../PrepareForRendering

		       0.049 Mt     0.337 %         1 x        0.049 Mt        0.000 Mt        0.049 Mt   100.000 % 	 ....../PrepareMaterials

		       3.510 Mt    24.008 %         1 x        3.510 Mt        0.000 Mt        3.510 Mt   100.000 % 	 ....../BuildMaterialCache

		       4.963 Mt    33.946 %         1 x        4.963 Mt        0.000 Mt        4.963 Mt   100.000 % 	 ....../PrepareDrawBundle

		      28.094 Mt     0.310 %         1 x       28.094 Mt        0.000 Mt        6.495 Mt    23.118 % 	 ..../RayTracingRLPrep

		       0.944 Mt     3.359 %         1 x        0.944 Mt        0.000 Mt        0.944 Mt   100.000 % 	 ...../PrepareAccelerationStructures

		       0.027 Mt     0.098 %         1 x        0.027 Mt        0.000 Mt        0.027 Mt   100.000 % 	 ...../PrepareCache

		       7.382 Mt    26.275 %         1 x        7.382 Mt        0.000 Mt        7.382 Mt   100.000 % 	 ...../BuildCache

		       8.412 Mt    29.943 %         1 x        8.412 Mt        0.000 Mt        0.004 Mt     0.051 % 	 ...../BuildAccelerationStructures

		       8.408 Mt    99.949 %         1 x        8.408 Mt        0.000 Mt        4.241 Mt    50.441 % 	 ....../AccelerationStructureBuild

		       3.080 Mt    36.635 %         1 x        3.080 Mt        0.000 Mt        3.080 Mt   100.000 % 	 ......./BuildBottomLevelAS

		       1.087 Mt    12.925 %         1 x        1.087 Mt        0.000 Mt        1.087 Mt   100.000 % 	 ......./BuildTopLevelAS

		       4.834 Mt    17.207 %         1 x        4.834 Mt        0.000 Mt        4.834 Mt   100.000 % 	 ...../BuildShaderTables

		      62.614 Mt     0.692 %         1 x       62.614 Mt        0.000 Mt       62.614 Mt   100.000 % 	 ..../GpuSidePrep

		   26022.640 Mt    73.263 %     10250 x        2.539 Mt        4.003 Mt      202.068 Mt     0.777 % 	 ../MainLoop

		     870.727 Mt     3.346 %      1564 x        0.557 Mt        0.310 Mt      869.850 Mt    99.899 % 	 .../Update

		       0.877 Mt     0.101 %         1 x        0.877 Mt        0.000 Mt        0.877 Mt   100.000 % 	 ..../GuiModelApply

		   24949.846 Mt    95.877 %      6716 x        3.715 Mt        3.683 Mt    16955.353 Mt    67.958 % 	 .../Render

		    7994.493 Mt    32.042 %      6716 x        1.190 Mt        1.143 Mt     7960.900 Mt    99.580 % 	 ..../RayTracing

		      33.592 Mt     0.420 %      6716 x        0.005 Mt        0.003 Mt       33.592 Mt   100.000 % 	 ...../RayTracingRLPrep

	WindowThread:

		   35515.283 Mt   100.000 %         1 x    35515.283 Mt    35515.283 Mt    35515.283 Mt   100.000 % 	 /

	GpuThread:

		   10199.045 Mt   100.000 %      6716 x        1.519 Mt        1.459 Mt      153.326 Mt     1.503 % 	 /

		      16.675 Mt     0.163 %         1 x       16.675 Mt        0.000 Mt        0.051 Mt     0.306 % 	 ./ScenePrep

		      16.624 Mt    99.694 %         1 x       16.624 Mt        0.000 Mt        0.002 Mt     0.013 % 	 ../AccelerationStructureBuild

		      16.012 Mt    96.321 %         1 x       16.012 Mt        0.000 Mt       16.012 Mt   100.000 % 	 .../BuildBottomLevelASGpu

		       0.610 Mt     3.667 %         1 x        0.610 Mt        0.000 Mt        0.610 Mt   100.000 % 	 .../BuildTopLevelASGpu

		   10018.552 Mt    98.230 %      6716 x        1.492 Mt        1.457 Mt        5.460 Mt     0.054 % 	 ./RayTracingGpu

		    9594.092 Mt    95.763 %      6716 x        1.429 Mt        1.395 Mt     9594.092 Mt   100.000 % 	 ../RayTracingRLGpu

		     419.000 Mt     4.182 %      6716 x        0.062 Mt        0.061 Mt      419.000 Mt   100.000 % 	 ../ResolveRLGpu

		      10.493 Mt     0.103 %      6716 x        0.002 Mt        0.002 Mt       10.493 Mt   100.000 % 	 ./Present


	============================


