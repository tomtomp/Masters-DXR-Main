Configuration: 
Command line parameters  = E:\programming\FIT-DIP\Masters-DXR-Main-git\build\Measurement\x64\Debug\Measurement.exe --scene Sponza/Sponza.gltf --profile-automation --profile-camera-track Sponza/Sponza.trk --rt-mode-pure --width 2560 --height 1440 --profile-output TrackSponza1440Rfp --fast-build-as 
Window width             = 2560
Window height            = 1421
Window title             = Direct3D App
Back-buffer count        = 3
Fill Triangles           = enabled
Triangles Font Clock-Wise= enabled
Fast Build AS            = enabled
Target FPS               = 0
Target UPS               = 60
Tearing                  = enabled
VSync                    = disabled
GUI rendering            = disabled
Debug layer              = enabled
GPU validation           = disabled
GPU profiling            = enabled
Render mode              = Ray Tracing
Loaded scene file        = Sponza/Sponza.gltf
Using testing scene      = disabled
Duplication              = 1
Duplication in cube      = disabled
Duplication offset       = 10
BLAS duplication         = disabled
Rays per pixel           = 9

ProfilingAggregator: 
Render	,	9.4487	,	9.4224	,	10.6147	,	9.2344	,	9.1599	,	8.7982	,	8.7497	,	8.0237	,	8.1891	,	8.8405	,	8.7777	,	9.2169	,	9.2978	,	9.6092	,	9.9965	,	8.7613	,	8.9594	,	8.8182	,	12.6553	,	10.3363	,	11.2905	,	10.798	,	9.8571	,	9.3488	,	8.8249	,	9.4045
Update	,	0.3616	,	0.3882	,	0.3904	,	0.3645	,	0.5157	,	0.8799	,	0.8091	,	0.3574	,	0.5459	,	0.3708	,	0.5175	,	0.3792	,	0.8529	,	0.4243	,	0.8851	,	0.3614	,	0.3351	,	0.3901	,	0.5344	,	0.3834	,	0.7095	,	0.3484	,	0.3644	,	0.3715	,	0.3783	,	0.3509
RayTracing	,	1.3507	,	1.3251	,	2.1109	,	1.3528	,	1.3142	,	1.2756	,	1.3305	,	1.3149	,	1.3117	,	1.3458	,	1.3319	,	1.3567	,	1.3672	,	1.4453	,	1.2846	,	1.326	,	1.36	,	1.3775	,	3.1499	,	1.3635	,	1.3377	,	1.3866	,	1.328	,	1.3456	,	1.3789	,	1.3341
RayTracingGpu	,	6.52317	,	6.8743	,	7.08262	,	6.63184	,	6.66266	,	6.44112	,	5.95859	,	5.34163	,	5.70611	,	6.05299	,	6.12688	,	6.36646	,	6.4631	,	6.75859	,	6.11757	,	6.32656	,	6.26058	,	6.11018	,	6.12176	,	7.59098	,	8.72182	,	8.14896	,	7.27341	,	6.7672	,	6.10835	,	6.43411
RayTracingRLGpu	,	6.24778	,	6.59053	,	6.8184	,	6.36752	,	6.38339	,	6.1633	,	5.69389	,	5.06701	,	5.44179	,	5.78429	,	5.85251	,	6.0927	,	6.09805	,	6.47251	,	5.85325	,	6.0449	,	5.98624	,	5.84538	,	5.8543	,	7.3176	,	8.45741	,	7.88445	,	7.00893	,	6.48477	,	5.84336	,	6.1607
ResolveRLGpu	,	0.274336	,	0.282624	,	0.263488	,	0.263552	,	0.278656	,	0.277184	,	0.263968	,	0.273888	,	0.263584	,	0.263072	,	0.273024	,	0.272288	,	0.364352	,	0.284992	,	0.263424	,	0.280928	,	0.27344	,	0.264032	,	0.262208	,	0.27264	,	0.26336	,	0.263872	,	0.263744	,	0.281856	,	0.263648	,	0.272544
Index	,	0	,	1	,	2	,	3	,	4	,	5	,	6	,	7	,	8	,	9	,	10	,	11	,	12	,	13	,	14	,	15	,	16	,	17	,	18	,	19	,	20	,	21	,	22	,	23	,	24	,	25

ProfilingBuildAggregator: 
BuildBottomLevelAS	,	3.9565
BuildBottomLevelASGpu	,	15.6649
BuildTopLevelAS	,	1.7842
BuildTopLevelASGpu	,	0.614464
Duplication	,	1
Triangles	,	262267
Meshes	,	103
BottomLevels	,	1
Index	,	0

FpsAggregator: 
FPS	,	91	,	102	,	102	,	102	,	104	,	106	,	111	,	114	,	117	,	116	,	112	,	107	,	102	,	103	,	106	,	109	,	108	,	109	,	107	,	108	,	84	,	86	,	93	,	99	,	107	,	106
UPS	,	59	,	61	,	60	,	60	,	61	,	60	,	60	,	61	,	60	,	60	,	61	,	60	,	60	,	60	,	60	,	60	,	61	,	60	,	60	,	61	,	60	,	60	,	60	,	61	,	60	,	60
FrameTime	,	11.0458	,	9.88541	,	9.8475	,	9.88147	,	9.67497	,	9.44663	,	9.048	,	8.8147	,	8.59728	,	8.67705	,	9.00924	,	9.35651	,	9.8318	,	9.72536	,	9.45912	,	9.17816	,	9.30518	,	9.21688	,	9.43343	,	9.33008	,	11.9865	,	11.6363	,	10.7663	,	10.1737	,	9.3664	,	9.48877
GigaRays/s	,	2.96401	,	3.31194	,	3.32469	,	3.31326	,	3.38397	,	3.46577	,	3.61846	,	3.71423	,	3.80816	,	3.77315	,	3.63403	,	3.49915	,	3.32999	,	3.36644	,	3.46119	,	3.56714	,	3.51845	,	3.55216	,	3.47062	,	3.50906	,	2.7314	,	2.8136	,	3.04097	,	3.21808	,	3.49545	,	3.45038
Index	,	0	,	1	,	2	,	3	,	4	,	5	,	6	,	7	,	8	,	9	,	10	,	11	,	12	,	13	,	14	,	15	,	16	,	17	,	18	,	19	,	20	,	21	,	22	,	23	,	24	,	25


AccelerationStructure: 
Ray tracing acceleration structure building statistics: 
	Bottom Level Acceleration Structure: 
		Maximum [B]: 14623872
		Minimum [B]: 14623872
		Currently Allocated [B]: 14623872
		Currently Created [num]: 1
		Current Average [B]: 14623872
		Maximum Triangles [num]: 262267
		Minimum Triangles [num]: 262267
		Total Triangles [num]: 262267
		Maximum Meshes [num]: 103
		Minimum Meshes [num]: 103
		Total Meshes [num]: 103
	Top Level Acceleration Structure: 
		Maximum [B]: 64
		Minimum [B]: 64
		Currently Allocated [B]: 64
		Total Allocated [B]: 64
		Total Created [num]: 1
		Average Allocated [B]: 64
		Maximum Instances [num]: 1
		Minimum Instances [num]: 1
		Total Instances [num]: 1
	Scratch Buffer: 
		Maximum [B]: 11660800
		Minimum [B]: 11660800
		Currently Allocated [B]: 11660800
		Total Allocated [B]: 11660800
		Total Created [num]: 1
		Average Allocated [B]: 11660800
ProfilingStack: 
[PROF] Profiling statistics: 
	Scopes entered : 27645
	Scopes exited : 27644
	Overhead per scope [ticks] : 1041.1
	Threads entered : 3
	Threads exited : 0
	============================
		         Sum[Mt]   Parent[%]  Samples[num]       Avg[Mt]   Last/Curr[Mt]        Self[Mt]     Self[%] 	 Name

	main:

		   35743.312 Mt   100.000 %         1 x    35743.314 Mt    35743.311 Mt        0.952 Mt     0.003 % 	 /

		   35742.422 Mt    99.998 %         1 x    35742.425 Mt    35742.422 Mt      363.802 Mt     1.018 % 	 ./Main application loop

		       3.353 Mt     0.009 %         1 x        3.353 Mt        0.000 Mt        3.353 Mt   100.000 % 	 ../TexturedRLInitialization

		       6.644 Mt     0.019 %         1 x        6.644 Mt        0.000 Mt        6.644 Mt   100.000 % 	 ../DeferredRLInitialization

		      56.311 Mt     0.158 %         1 x       56.311 Mt        0.000 Mt        2.714 Mt     4.820 % 	 ../RayTracingRLInitialization

		      53.597 Mt    95.180 %         1 x       53.597 Mt        0.000 Mt       53.597 Mt   100.000 % 	 .../PipelineBuild

		       3.239 Mt     0.009 %         1 x        3.239 Mt        0.000 Mt        3.239 Mt   100.000 % 	 ../ResolveRLInitialization

		    9192.089 Mt    25.718 %         1 x     9192.089 Mt        0.000 Mt        6.301 Mt     0.069 % 	 ../Initialization

		    9185.789 Mt    99.931 %         1 x     9185.789 Mt        0.000 Mt        2.503 Mt     0.027 % 	 .../ScenePrep

		    9090.748 Mt    98.965 %         1 x     9090.748 Mt        0.000 Mt      339.831 Mt     3.738 % 	 ..../SceneLoad

		    8209.594 Mt    90.307 %         1 x     8209.594 Mt        0.000 Mt     1135.572 Mt    13.832 % 	 ...../glTF-LoadScene

		    7074.022 Mt    86.168 %        69 x      102.522 Mt        0.000 Mt     7074.022 Mt   100.000 % 	 ....../glTF-LoadImageData

		     377.117 Mt     4.148 %         1 x      377.117 Mt        0.000 Mt      377.117 Mt   100.000 % 	 ...../glTF-CreateScene

		     164.206 Mt     1.806 %         1 x      164.206 Mt        0.000 Mt      164.206 Mt   100.000 % 	 ...../SceneDesc-CreateScene

		       7.940 Mt     0.086 %         1 x        7.940 Mt        0.000 Mt        0.006 Mt     0.082 % 	 ..../TexturedRLPrep

		       7.933 Mt    99.918 %         1 x        7.933 Mt        0.000 Mt        2.241 Mt    28.245 % 	 ...../PrepareForRendering

		       2.957 Mt    37.268 %         1 x        2.957 Mt        0.000 Mt        2.957 Mt   100.000 % 	 ....../PrepareMaterials

		       2.736 Mt    34.486 %         1 x        2.736 Mt        0.000 Mt        2.736 Mt   100.000 % 	 ....../PrepareDrawBundle

		       8.017 Mt     0.087 %         1 x        8.017 Mt        0.000 Mt        0.006 Mt     0.074 % 	 ..../DeferredRLPrep

		       8.011 Mt    99.926 %         1 x        8.011 Mt        0.000 Mt        3.440 Mt    42.946 % 	 ...../PrepareForRendering

		       0.030 Mt     0.372 %         1 x        0.030 Mt        0.000 Mt        0.030 Mt   100.000 % 	 ....../PrepareMaterials

		       2.063 Mt    25.758 %         1 x        2.063 Mt        0.000 Mt        2.063 Mt   100.000 % 	 ....../BuildMaterialCache

		       2.477 Mt    30.924 %         1 x        2.477 Mt        0.000 Mt        2.477 Mt   100.000 % 	 ....../PrepareDrawBundle

		      23.870 Mt     0.260 %         1 x       23.870 Mt        0.000 Mt        5.560 Mt    23.294 % 	 ..../RayTracingRLPrep

		       0.616 Mt     2.581 %         1 x        0.616 Mt        0.000 Mt        0.616 Mt   100.000 % 	 ...../PrepareAccelerationStructures

		       0.030 Mt     0.125 %         1 x        0.030 Mt        0.000 Mt        0.030 Mt   100.000 % 	 ...../PrepareCache

		       5.271 Mt    22.082 %         1 x        5.271 Mt        0.000 Mt        5.271 Mt   100.000 % 	 ...../BuildCache

		       8.766 Mt    36.726 %         1 x        8.766 Mt        0.000 Mt        0.004 Mt     0.050 % 	 ...../BuildAccelerationStructures

		       8.762 Mt    99.950 %         1 x        8.762 Mt        0.000 Mt        3.021 Mt    34.482 % 	 ....../AccelerationStructureBuild

		       3.957 Mt    45.155 %         1 x        3.957 Mt        0.000 Mt        3.957 Mt   100.000 % 	 ......./BuildBottomLevelAS

		       1.784 Mt    20.363 %         1 x        1.784 Mt        0.000 Mt        1.784 Mt   100.000 % 	 ......./BuildTopLevelAS

		       3.626 Mt    15.192 %         1 x        3.626 Mt        0.000 Mt        3.626 Mt   100.000 % 	 ...../BuildShaderTables

		      52.711 Mt     0.574 %         1 x       52.711 Mt        0.000 Mt       52.711 Mt   100.000 % 	 ..../GpuSidePrep

		   26116.984 Mt    73.070 %      6989 x        3.737 Mt       21.881 Mt      198.861 Mt     0.761 % 	 ../MainLoop

		     894.276 Mt     3.424 %      1567 x        0.571 Mt        0.462 Mt      893.382 Mt    99.900 % 	 .../Update

		       0.894 Mt     0.100 %         1 x        0.894 Mt        0.000 Mt        0.894 Mt   100.000 % 	 ..../GuiModelApply

		   25023.847 Mt    95.814 %      2712 x        9.227 Mt       10.272 Mt    21353.844 Mt    85.334 % 	 .../Render

		    3670.003 Mt    14.666 %      2712 x        1.353 Mt        1.916 Mt     3656.650 Mt    99.636 % 	 ..../RayTracing

		      13.354 Mt     0.364 %      2712 x        0.005 Mt        0.006 Mt       13.354 Mt   100.000 % 	 ...../RayTracingRLPrep

	WindowThread:

		   35728.891 Mt   100.000 %         1 x    35728.892 Mt    35728.891 Mt    35728.891 Mt   100.000 % 	 /

	GpuThread:

		   17949.917 Mt   100.000 %      2712 x        6.619 Mt        6.434 Mt      157.069 Mt     0.875 % 	 /

		      16.318 Mt     0.091 %         1 x       16.318 Mt        0.000 Mt        0.036 Mt     0.221 % 	 ./ScenePrep

		      16.282 Mt    99.779 %         1 x       16.282 Mt        0.000 Mt        0.003 Mt     0.016 % 	 ../AccelerationStructureBuild

		      15.665 Mt    96.210 %         1 x       15.665 Mt        0.000 Mt       15.665 Mt   100.000 % 	 .../BuildBottomLevelASGpu

		       0.614 Mt     3.774 %         1 x        0.614 Mt        0.000 Mt        0.614 Mt   100.000 % 	 .../BuildTopLevelASGpu

		   17764.940 Mt    98.969 %      2712 x        6.550 Mt        6.432 Mt        2.493 Mt     0.014 % 	 ./RayTracingGpu

		   17015.290 Mt    95.780 %      2712 x        6.274 Mt        6.160 Mt    17015.290 Mt   100.000 % 	 ../RayTracingRLGpu

		     747.157 Mt     4.206 %      2712 x        0.276 Mt        0.271 Mt      747.157 Mt   100.000 % 	 ../ResolveRLGpu

		      11.590 Mt     0.065 %      2712 x        0.004 Mt        0.002 Mt       11.590 Mt   100.000 % 	 ./Present


	============================


