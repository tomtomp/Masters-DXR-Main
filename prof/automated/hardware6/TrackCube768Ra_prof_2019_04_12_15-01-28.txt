Configuration: 
Command line parameters  = E:\programming\FIT-DIP\Masters-DXR-Main-git\build\Measurement\x64\Debug\Measurement.exe --scene Cube/Cube.gltf --profile-automation --profile-camera-track Cube/Cube.trk --width 1024 --height 768 --profile-output TrackCube768Ra 
Window width             = 1024
Window height            = 768
Window title             = Direct3D App
Back-buffer count        = 3
Fill Triangles           = enabled
Triangles Font Clock-Wise= enabled
Fast Build AS            = disabled
Target FPS               = 0
Target UPS               = 60
Tearing                  = enabled
VSync                    = disabled
GUI rendering            = disabled
Debug layer              = enabled
GPU validation           = disabled
GPU profiling            = enabled
Render mode              = Rasterization
Loaded scene file        = Cube/Cube.gltf
Using testing scene      = disabled
Duplication              = 1
Duplication in cube      = disabled
Duplication offset       = 10
BLAS duplication         = disabled
Rays per pixel           = 10

ProfilingAggregator: 
Render	,	0.6367	,	2.2029	,	1.2545	,	1.5458	,	1.8904	,	1.8512	,	0.72	,	1.5089	,	2.1568	,	2.3152	,	1.211	,	1.387	,	0.9101	,	1.3412	,	0.8923	,	0.9726	,	0.971	,	1.5286	,	0.9127	,	1.7473	,	1.3063	,	0.8135	,	0.7501	,	1.2546	,	0.7446	,	0.733	,	0.755	,	0.7641	,	0.7523	,	1.1229
Update	,	0.2875	,	0.8866	,	0.4302	,	0.529	,	0.7474	,	0.2748	,	0.2768	,	0.2747	,	0.272	,	0.5327	,	0.4485	,	0.5239	,	0.9091	,	0.5239	,	0.4763	,	0.508	,	0.5058	,	0.2726	,	0.5066	,	0.8723	,	0.2881	,	0.2789	,	0.2799	,	0.5466	,	0.4916	,	0.2719	,	0.2733	,	0.3132	,	0.2749	,	0.4694
Textured	,	0.2436	,	0.8002	,	0.4027	,	0.5267	,	0.7359	,	0.7265	,	0.2445	,	0.3899	,	0.7272	,	0.7371	,	0.3998	,	0.5595	,	0.2487	,	0.554	,	0.2504	,	0.2483	,	0.268	,	0.6704	,	0.253	,	0.7829	,	0.4569	,	0.2441	,	0.2485	,	0.4554	,	0.2456	,	0.2428	,	0.2458	,	0.2436	,	0.2476	,	0.4368
TexturedGpu	,	0.006112	,	0.019616	,	0.02576	,	0.027136	,	0.02928	,	0.056128	,	0.042144	,	0.08064	,	0.095264	,	0.091008	,	0.183488	,	0.157856	,	0.10288	,	0.101184	,	0.107424	,	0.129984	,	0.114368	,	0.135776	,	0.130048	,	0.143936	,	0.07152	,	0.036288	,	0.022368	,	0.020384	,	0.021184	,	0.021088	,	0.006752	,	0.00256	,	0.024256	,	0.01936
TexturedRLGpu	,	0.00528	,	0.018752	,	0.02496	,	0.025792	,	0.027936	,	0.046304	,	0.040288	,	0.054272	,	0.093888	,	0.089632	,	0.16	,	0.156448	,	0.099904	,	0.099808	,	0.10368	,	0.12816	,	0.102272	,	0.10848	,	0.128224	,	0.142528	,	0.03728	,	0.030688	,	0.002112	,	0.018976	,	0.0184	,	0.018464	,	0.004928	,	0.000736	,	0.02192	,	0.001632
Index	,	0	,	1	,	2	,	3	,	4	,	5	,	6	,	7	,	8	,	9	,	10	,	11	,	12	,	13	,	14	,	15	,	16	,	17	,	18	,	19	,	20	,	21	,	22	,	23	,	24	,	25	,	26	,	27	,	28	,	29

ProfilingBuildAggregator: 
BuildBottomLevelAS	,	1.0038
BuildBottomLevelASGpu	,	0.089568
BuildTopLevelAS	,	1.0024
BuildTopLevelASGpu	,	0.06016
Duplication	,	1
Triangles	,	12
Meshes	,	1
BottomLevels	,	1
Index	,	0

FpsAggregator: 
FPS	,	1322	,	1304	,	1206	,	1194	,	1078	,	1167	,	1181	,	1213	,	1125	,	757	,	783	,	751	,	786	,	805	,	840	,	759	,	760	,	778	,	777	,	768	,	755	,	966	,	933	,	979	,	968	,	968	,	990	,	955	,	1031	,	999
UPS	,	59	,	60	,	61	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	61	,	60	,	60	,	60	,	60
FrameTime	,	0.756491	,	0.767598	,	0.829837	,	0.838095	,	0.929089	,	0.857557	,	0.846846	,	0.824571	,	0.890427	,	1.32186	,	1.27821	,	1.33206	,	1.27304	,	1.24291	,	1.191	,	1.31798	,	1.31684	,	1.28586	,	1.28784	,	1.30401	,	1.32561	,	1.03575	,	1.0726	,	1.02244	,	1.03327	,	1.03382	,	1.01073	,	1.04781	,	0.970323	,	1.00179
GigaRays/s	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0
Index	,	0	,	1	,	2	,	3	,	4	,	5	,	6	,	7	,	8	,	9	,	10	,	11	,	12	,	13	,	14	,	15	,	16	,	17	,	18	,	19	,	20	,	21	,	22	,	23	,	24	,	25	,	26	,	27	,	28	,	29


AccelerationStructure: 
Ray tracing acceleration structure building statistics: 
	Bottom Level Acceleration Structure: 
		Maximum [B]: 5168
		Minimum [B]: 5168
		Currently Allocated [B]: 5168
		Currently Created [num]: 1
		Current Average [B]: 5168
		Maximum Triangles [num]: 12
		Minimum Triangles [num]: 12
		Total Triangles [num]: 12
		Maximum Meshes [num]: 1
		Minimum Meshes [num]: 1
		Total Meshes [num]: 1
	Top Level Acceleration Structure: 
		Maximum [B]: 64
		Minimum [B]: 64
		Currently Allocated [B]: 64
		Total Allocated [B]: 64
		Total Created [num]: 1
		Average Allocated [B]: 64
		Maximum Instances [num]: 1
		Minimum Instances [num]: 1
		Total Instances [num]: 1
	Scratch Buffer: 
		Maximum [B]: 3584
		Minimum [B]: 3584
		Currently Allocated [B]: 3584
		Total Allocated [B]: 3584
		Total Created [num]: 1
		Average Allocated [B]: 3584
ProfilingStack: 
[PROF] Profiling statistics: 
	Scopes entered : 208527
	Scopes exited : 208526
	Overhead per scope [ticks] : 1014
	Threads entered : 3
	Threads exited : 0
	============================
		         Sum[Mt]   Parent[%]  Samples[num]       Avg[Mt]   Last/Curr[Mt]        Self[Mt]     Self[%] 	 Name

	main:

		   30559.782 Mt   100.000 %         1 x    30559.786 Mt    30559.780 Mt        1.038 Mt     0.003 % 	 /

		   30558.892 Mt    99.997 %         1 x    30558.897 Mt    30558.891 Mt      455.324 Mt     1.490 % 	 ./Main application loop

		       2.915 Mt     0.010 %         1 x        2.915 Mt        0.000 Mt        2.915 Mt   100.000 % 	 ../TexturedRLInitialization

		       6.011 Mt     0.020 %         1 x        6.011 Mt        0.000 Mt        6.011 Mt   100.000 % 	 ../DeferredRLInitialization

		      51.102 Mt     0.167 %         1 x       51.102 Mt        0.000 Mt        2.266 Mt     4.434 % 	 ../RayTracingRLInitialization

		      48.837 Mt    95.566 %         1 x       48.837 Mt        0.000 Mt       48.837 Mt   100.000 % 	 .../PipelineBuild

		       3.364 Mt     0.011 %         1 x        3.364 Mt        0.000 Mt        3.364 Mt   100.000 % 	 ../ResolveRLInitialization

		     111.939 Mt     0.366 %         1 x      111.939 Mt        0.000 Mt        2.446 Mt     2.185 % 	 ../Initialization

		     109.493 Mt    97.815 %         1 x      109.493 Mt        0.000 Mt        1.147 Mt     1.047 % 	 .../ScenePrep

		      77.560 Mt    70.836 %         1 x       77.560 Mt        0.000 Mt       12.635 Mt    16.290 % 	 ..../SceneLoad

		      57.614 Mt    74.283 %         1 x       57.614 Mt        0.000 Mt       12.619 Mt    21.903 % 	 ...../glTF-LoadScene

		      44.995 Mt    78.097 %         2 x       22.497 Mt        0.000 Mt       44.995 Mt   100.000 % 	 ....../glTF-LoadImageData

		       4.129 Mt     5.324 %         1 x        4.129 Mt        0.000 Mt        4.129 Mt   100.000 % 	 ...../glTF-CreateScene

		       3.183 Mt     4.103 %         1 x        3.183 Mt        0.000 Mt        3.183 Mt   100.000 % 	 ...../SceneDesc-CreateScene

		       1.160 Mt     1.060 %         1 x        1.160 Mt        0.000 Mt        0.005 Mt     0.396 % 	 ..../TexturedRLPrep

		       1.156 Mt    99.604 %         1 x        1.156 Mt        0.000 Mt        0.402 Mt    34.830 % 	 ...../PrepareForRendering

		       0.467 Mt    40.421 %         1 x        0.467 Mt        0.000 Mt        0.467 Mt   100.000 % 	 ....../PrepareMaterials

		       0.286 Mt    24.749 %         1 x        0.286 Mt        0.000 Mt        0.286 Mt   100.000 % 	 ....../PrepareDrawBundle

		       3.294 Mt     3.009 %         1 x        3.294 Mt        0.000 Mt        0.005 Mt     0.143 % 	 ..../DeferredRLPrep

		       3.289 Mt    99.857 %         1 x        3.289 Mt        0.000 Mt        1.542 Mt    46.889 % 	 ...../PrepareForRendering

		       0.015 Mt     0.471 %         1 x        0.015 Mt        0.000 Mt        0.015 Mt   100.000 % 	 ....../PrepareMaterials

		       1.468 Mt    44.639 %         1 x        1.468 Mt        0.000 Mt        1.468 Mt   100.000 % 	 ....../BuildMaterialCache

		       0.263 Mt     8.001 %         1 x        0.263 Mt        0.000 Mt        0.263 Mt   100.000 % 	 ....../PrepareDrawBundle

		      15.341 Mt    14.011 %         1 x       15.341 Mt        0.000 Mt        4.250 Mt    27.701 % 	 ..../RayTracingRLPrep

		       0.222 Mt     1.446 %         1 x        0.222 Mt        0.000 Mt        0.222 Mt   100.000 % 	 ...../PrepareAccelerationStructures

		       0.024 Mt     0.153 %         1 x        0.024 Mt        0.000 Mt        0.024 Mt   100.000 % 	 ...../PrepareCache

		       0.480 Mt     3.128 %         1 x        0.480 Mt        0.000 Mt        0.480 Mt   100.000 % 	 ...../BuildCache

		       8.627 Mt    56.235 %         1 x        8.627 Mt        0.000 Mt        0.004 Mt     0.045 % 	 ...../BuildAccelerationStructures

		       8.623 Mt    99.955 %         1 x        8.623 Mt        0.000 Mt        6.617 Mt    76.735 % 	 ....../AccelerationStructureBuild

		       1.004 Mt    11.641 %         1 x        1.004 Mt        0.000 Mt        1.004 Mt   100.000 % 	 ......./BuildBottomLevelAS

		       1.002 Mt    11.624 %         1 x        1.002 Mt        0.000 Mt        1.002 Mt   100.000 % 	 ......./BuildTopLevelAS

		       1.739 Mt    11.337 %         1 x        1.739 Mt        0.000 Mt        1.739 Mt   100.000 % 	 ...../BuildShaderTables

		      10.990 Mt    10.037 %         1 x       10.990 Mt        0.000 Mt       10.990 Mt   100.000 % 	 ..../GpuSidePrep

		   29928.235 Mt    97.936 %     33287 x        0.899 Mt        2.032 Mt      415.853 Mt     1.389 % 	 ../MainLoop

		     749.287 Mt     2.504 %      1802 x        0.416 Mt        0.315 Mt      748.388 Mt    99.880 % 	 .../Update

		       0.899 Mt     0.120 %         1 x        0.899 Mt        0.000 Mt        0.899 Mt   100.000 % 	 ..../GuiModelApply

		   28763.095 Mt    96.107 %     28900 x        0.995 Mt        1.708 Mt    19781.937 Mt    68.775 % 	 .../Render

		    8981.158 Mt    31.225 %     28900 x        0.311 Mt        0.285 Mt     8865.988 Mt    98.718 % 	 ..../Textured

		     115.170 Mt     1.282 %     28900 x        0.004 Mt        0.003 Mt      115.170 Mt   100.000 % 	 ...../TexturedRLPrep

	WindowThread:

		   30549.046 Mt   100.000 %         1 x    30549.047 Mt    30549.045 Mt    30549.046 Mt   100.000 % 	 /

	GpuThread:

		    2161.524 Mt   100.000 %     28900 x        0.075 Mt        0.081 Mt      152.188 Mt     7.041 % 	 /

		       0.156 Mt     0.007 %         1 x        0.156 Mt        0.000 Mt        0.006 Mt     3.682 % 	 ./ScenePrep

		       0.151 Mt    96.318 %         1 x        0.151 Mt        0.000 Mt        0.001 Mt     0.616 % 	 ../AccelerationStructureBuild

		       0.090 Mt    59.452 %         1 x        0.090 Mt        0.000 Mt        0.090 Mt   100.000 % 	 .../BuildBottomLevelASGpu

		       0.060 Mt    39.932 %         1 x        0.060 Mt        0.000 Mt        0.060 Mt   100.000 % 	 .../BuildTopLevelASGpu

		    1788.115 Mt    82.725 %     28900 x        0.062 Mt        0.062 Mt      255.381 Mt    14.282 % 	 ./TexturedGpu

		    1532.734 Mt    85.718 %     28900 x        0.053 Mt        0.058 Mt     1532.734 Mt   100.000 % 	 ../TexturedRLGpu

		     221.064 Mt    10.227 %     28900 x        0.008 Mt        0.018 Mt      221.064 Mt   100.000 % 	 ./Present


	============================


