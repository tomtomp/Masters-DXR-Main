Configuration: 
Command line parameters  = E:\programming\FIT-DIP\Masters-DXR-Main-git\build\Measurement\x64\Release\Measurement.exe --scene Cube/Cube.gltf --profile-builds --profile-output BuildsCube 
Build type               = Release
Device name              = NVIDIA GeForce RTX 2080 Ti
Device memory MB         = 11049
Ray Tracing backend      = Hardware
Window width             = 1024
Window height            = 768
Window title             = Direct3D App
Back-buffer count        = 3
Fill Triangles           = enabled
Triangles Font Clock-Wise= enabled
Fast Build AS            = disabled
Target FPS               = 60
Target UPS               = 60
Tearing                  = disabled
VSync                    = disabled
GUI rendering            = enabled
Debug layer              = disabled
GPU validation           = disabled
GPU profiling            = enabled
Render mode              = Rasterization
Loaded scene file        = Cube/Cube.gltf
Using testing scene      = disabled
Duplication              = 1
Duplication in cube      = disabled
Duplication offset       = 10
BLAS duplication         = disabled
Rays per pixel           = 13
Quality preset           = High
Rasterization Quality    : 
  AO                     = enabled
  AO kernel size         = 4
  Shadows                = enabled
  Shadow map resolution  = 4096
  Shadow PCF kernel size = 4
  Reflections            = enabled
Ray Tracing Quality      : 
  AO                     = enabled
  AO rays                = 4
  AO kernel size         = 4
  Shadows                = enabled
  Shadow rays            = 4
  Shadow kernel size     = 4
  Reflections            = enabled

ProfilingAggregator: 

ProfilingBuildAggregator: 
BuildBottomLevelAS	,	0.9532	,	0.9532
BuildBottomLevelASGpu	,	0.0144	,	0.171488
BuildTopLevelAS	,	0.8565	,	0.8565
BuildTopLevelASGpu	,	0.078816	,	0.082176
Duplication	,	1	,	1
Triangles	,	12	,	12
Meshes	,	1	,	1
BottomLevels	,	1	,	1
TopLevelSize	,	64	,	64
BottomLevelsSize	,	1152	,	4480
Index	,	0	,	1

FpsAggregator: 
FPS	,	0	,	0
UPS	,	0	,	0
FrameTime	,	0	,	0
GigaRays/s	,	0	,	0
Index	,	0	,	1


AccelerationStructure: 
Ray tracing acceleration structure building statistics: 
	Bottom Level Acceleration Structure: 
		Maximum [B]: 4480
		Minimum [B]: 4480
		Currently Allocated [B]: 4480
		Currently Created [num]: 1
		Current Average [B]: 4480
		Maximum Triangles [num]: 12
		Minimum Triangles [num]: 12
		Total Triangles [num]: 12
		Maximum Meshes [num]: 1
		Minimum Meshes [num]: 1
		Total Meshes [num]: 1
	Top Level Acceleration Structure: 
		Maximum [B]: 64
		Minimum [B]: 64
		Currently Allocated [B]: 64
		Total Allocated [B]: 64
		Total Created [num]: 1
		Average Allocated [B]: 64
		Maximum Instances [num]: 1
		Minimum Instances [num]: 1
		Total Instances [num]: 1
	Scratch Buffer: 
		Maximum [B]: 896
		Minimum [B]: 896
		Currently Allocated [B]: 896
		Total Allocated [B]: 896
		Total Created [num]: 1
		Average Allocated [B]: 896
ProfilingStack: 
[PROF] Profiling statistics: 
	Scopes entered : 62
	Scopes exited : 61
	Overhead per scope [ticks] : 104.1
	Threads entered : 3
	Threads exited : 0
	============================
		         Sum[Mt]   Parent[%]  Samples[num]       Avg[Mt]   Last/Curr[Mt]        Self[Mt]     Self[%] 	 Name

	main:

		     517.310 Mt   100.000 %         1 x      517.310 Mt      517.310 Mt        0.390 Mt     0.075 % 	 /

		     516.938 Mt    99.928 %         1 x      516.938 Mt      516.938 Mt      282.633 Mt    54.675 % 	 ./Main application loop

		       2.393 Mt     0.463 %         1 x        2.393 Mt        2.393 Mt        2.393 Mt   100.000 % 	 ../TexturedRLInitialization

		       1.355 Mt     0.262 %         1 x        1.355 Mt        1.355 Mt        1.355 Mt   100.000 % 	 ../ShadowMapRLInitialization

		       5.164 Mt     0.999 %         1 x        5.164 Mt        5.164 Mt        5.164 Mt   100.000 % 	 ../RasterResolveRLInitialization

		       3.943 Mt     0.763 %         1 x        3.943 Mt        3.943 Mt        3.943 Mt   100.000 % 	 ../ResolveRLInitialization

		      29.060 Mt     5.622 %         3 x        9.687 Mt        5.643 Mt       20.081 Mt    69.101 % 	 ../AccelerationStructureBuild

		       6.039 Mt    20.782 %         3 x        2.013 Mt        0.401 Mt        6.039 Mt   100.000 % 	 .../BuildBottomLevelAS

		       2.940 Mt    10.117 %         3 x        0.980 Mt        0.476 Mt        2.940 Mt   100.000 % 	 .../BuildTopLevelAS

		       2.698 Mt     0.522 %         1 x        2.698 Mt        2.698 Mt        2.698 Mt   100.000 % 	 ../AmbientOcclusionRLInitialization

		       5.447 Mt     1.054 %         1 x        5.447 Mt        5.447 Mt        5.447 Mt   100.000 % 	 ../DeferredRLInitialization

		      69.558 Mt    13.456 %         1 x       69.558 Mt       69.558 Mt        3.027 Mt     4.351 % 	 ../RayTracingRLInitialization

		      66.531 Mt    95.649 %         1 x       66.531 Mt       66.531 Mt       66.531 Mt   100.000 % 	 .../PipelineBuild

		     114.687 Mt    22.186 %         1 x      114.687 Mt      114.687 Mt        0.280 Mt     0.244 % 	 ../Initialization

		     114.406 Mt    99.756 %         1 x      114.406 Mt      114.406 Mt        1.858 Mt     1.624 % 	 .../ScenePrep

		      49.229 Mt    43.030 %         1 x       49.229 Mt       49.229 Mt       13.624 Mt    27.674 % 	 ..../SceneLoad

		      12.143 Mt    24.666 %         1 x       12.143 Mt       12.143 Mt        4.194 Mt    34.540 % 	 ...../glTF-LoadScene

		       7.949 Mt    65.460 %         2 x        3.974 Mt        1.188 Mt        7.949 Mt   100.000 % 	 ....../glTF-LoadImageData

		      10.320 Mt    20.964 %         1 x       10.320 Mt       10.320 Mt       10.320 Mt   100.000 % 	 ...../glTF-CreateScene

		      13.142 Mt    26.696 %         1 x       13.142 Mt       13.142 Mt       13.142 Mt   100.000 % 	 ...../SceneDesc-CreateScene

		       0.780 Mt     0.682 %         1 x        0.780 Mt        0.780 Mt        0.001 Mt     0.064 % 	 ..../ShadowMapRLPrep

		       0.779 Mt    99.936 %         1 x        0.779 Mt        0.779 Mt        0.770 Mt    98.858 % 	 ...../PrepareForRendering

		       0.009 Mt     1.142 %         1 x        0.009 Mt        0.009 Mt        0.009 Mt   100.000 % 	 ....../PrepareDrawBundle

		       2.837 Mt     2.480 %         1 x        2.837 Mt        2.837 Mt        0.000 Mt     0.014 % 	 ..../TexturedRLPrep

		       2.837 Mt    99.986 %         1 x        2.837 Mt        2.837 Mt        2.831 Mt    99.817 % 	 ...../PrepareForRendering

		       0.001 Mt     0.035 %         1 x        0.001 Mt        0.001 Mt        0.001 Mt   100.000 % 	 ....../PrepareMaterials

		       0.004 Mt     0.148 %         1 x        0.004 Mt        0.004 Mt        0.004 Mt   100.000 % 	 ....../PrepareDrawBundle

		       2.522 Mt     2.205 %         1 x        2.522 Mt        2.522 Mt        0.001 Mt     0.036 % 	 ..../DeferredRLPrep

		       2.522 Mt    99.964 %         1 x        2.522 Mt        2.522 Mt        1.809 Mt    71.731 % 	 ...../PrepareForRendering

		       0.001 Mt     0.024 %         1 x        0.001 Mt        0.001 Mt        0.001 Mt   100.000 % 	 ....../PrepareMaterials

		       0.709 Mt    28.102 %         1 x        0.709 Mt        0.709 Mt        0.709 Mt   100.000 % 	 ....../BuildMaterialCache

		       0.004 Mt     0.143 %         1 x        0.004 Mt        0.004 Mt        0.004 Mt   100.000 % 	 ....../PrepareDrawBundle

		      44.741 Mt    39.107 %         1 x       44.741 Mt       44.741 Mt        7.376 Mt    16.486 % 	 ..../RayTracingRLPrep

		       0.073 Mt     0.164 %         1 x        0.073 Mt        0.073 Mt        0.073 Mt   100.000 % 	 ...../PrepareAccelerationStructures

		       0.001 Mt     0.002 %         1 x        0.001 Mt        0.001 Mt        0.001 Mt   100.000 % 	 ...../PrepareCache

		      28.932 Mt    64.665 %         1 x       28.932 Mt       28.932 Mt       28.932 Mt   100.000 % 	 ...../PipelineBuild

		       0.677 Mt     1.514 %         1 x        0.677 Mt        0.677 Mt        0.677 Mt   100.000 % 	 ...../BuildCache

		       5.482 Mt    12.254 %         1 x        5.482 Mt        5.482 Mt        0.001 Mt     0.022 % 	 ...../BuildAccelerationStructures

		       5.481 Mt    99.978 %         1 x        5.481 Mt        5.481 Mt        3.671 Mt    66.984 % 	 ....../AccelerationStructureBuild

		       0.953 Mt    17.390 %         1 x        0.953 Mt        0.953 Mt        0.953 Mt   100.000 % 	 ......./BuildBottomLevelAS

		       0.857 Mt    15.626 %         1 x        0.857 Mt        0.857 Mt        0.857 Mt   100.000 % 	 ......./BuildTopLevelAS

		       2.199 Mt     4.916 %         1 x        2.199 Mt        2.199 Mt        2.199 Mt   100.000 % 	 ...../BuildShaderTables

		      12.440 Mt    10.873 %         1 x       12.440 Mt       12.440 Mt       12.440 Mt   100.000 % 	 ..../GpuSidePrep

	WindowThread:

		     515.790 Mt   100.000 %         1 x      515.790 Mt      515.790 Mt      515.790 Mt   100.000 % 	 /

	GpuThread:

		      50.064 Mt   100.000 %         2 x       25.032 Mt        0.255 Mt       49.349 Mt    98.573 % 	 /

		       0.270 Mt     0.540 %         1 x        0.270 Mt        0.270 Mt        0.007 Mt     2.758 % 	 ./ScenePrep

		       0.263 Mt    97.242 %         1 x        0.263 Mt        0.263 Mt        0.002 Mt     0.706 % 	 ../AccelerationStructureBuild

		       0.172 Mt    65.620 %         1 x        0.172 Mt        0.172 Mt        0.172 Mt   100.000 % 	 .../BuildBottomLevelASGpu

		       0.089 Mt    33.674 %         1 x        0.089 Mt        0.089 Mt        0.089 Mt   100.000 % 	 .../BuildTopLevelASGpu

		       0.444 Mt     0.887 %         3 x        0.148 Mt        0.255 Mt        0.004 Mt     0.886 % 	 ./AccelerationStructureBuild

		       0.200 Mt    45.107 %         3 x        0.067 Mt        0.171 Mt        0.200 Mt   100.000 % 	 ../BuildBottomLevelASGpu

		       0.240 Mt    54.007 %         3 x        0.080 Mt        0.082 Mt        0.240 Mt   100.000 % 	 ../BuildTopLevelASGpu


	============================


