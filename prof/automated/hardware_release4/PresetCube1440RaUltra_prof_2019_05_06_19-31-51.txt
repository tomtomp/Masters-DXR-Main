Configuration: 
Command line parameters  = E:\programming\FIT-DIP\Masters-DXR-Main-git\build\Measurement\x64\Release\Measurement.exe --scene Cube/Cube.gltf --profile-automation --profile-camera-track Cube/Cube.trk --width 2560 --height 1440 --profile-output PresetCube1440RaUltra --quality-preset Ultra 
Build type               = Release
Device name              = NVIDIA GeForce RTX 2080 Ti
Device memory MB         = 11049
Ray Tracing backend      = Hardware
Window width             = 2560
Window height            = 1421
Window title             = Direct3D App
Back-buffer count        = 3
Fill Triangles           = enabled
Triangles Font Clock-Wise= enabled
Fast Build AS            = disabled
Target FPS               = 0
Target UPS               = 60
Tearing                  = enabled
VSync                    = disabled
GUI rendering            = disabled
Debug layer              = disabled
GPU validation           = disabled
GPU profiling            = enabled
Render mode              = Rasterization
Loaded scene file        = Cube/Cube.gltf
Using testing scene      = disabled
Duplication              = 1
Duplication in cube      = disabled
Duplication offset       = 10
BLAS duplication         = disabled
Rays per pixel           = 33
Quality preset           = Ultra
Rasterization Quality    : 
  AO                     = enabled
  AO kernel size         = 16
  Shadows                = enabled
  Shadow map resolution  = 8192
  Shadow PCF kernel size = 8
  Reflections            = enabled
Ray Tracing Quality      : 
  AO                     = enabled
  AO rays                = 16
  AO kernel size         = 8
  Shadows                = enabled
  Shadow rays            = 8
  Shadow kernel size     = 8
  Reflections            = enabled

ProfilingAggregator: 
Render	,	15.5581	,	15.796	,	15.9163	,	16.1521	,	16.3067	,	16.7002	,	16.1953	,	16.3549	,	16.5524	,	16.4653	,	16.4191	,	16.4559	,	16.1252	,	15.8337	,	16.2054	,	16.2195	,	16.8014	,	16.7451	,	17.1389	,	16.5308	,	16.4814	,	15.9152	,	15.587	,	15.598	,	15.3269	,	15.129	,	14.9006	,	14.8733	,	14.7927
Update	,	0.0899	,	0.0772	,	0.0765	,	0.0788	,	0.0756	,	0.0753	,	0.0778	,	0.0793	,	0.076	,	0.0767	,	0.0742	,	0.077	,	0.0749	,	0.0759	,	0.0751	,	0.0778	,	0.0753	,	0.0759	,	0.0762	,	0.0772	,	0.0758	,	0.0767	,	0.0779	,	0.0776	,	0.0853	,	0.0794	,	0.0822	,	0.0772	,	0.0763
DeferredRLGpu	,	0.188992	,	0.252064	,	0.269312	,	0.277024	,	0.295392	,	0.356896	,	0.327968	,	0.378208	,	0.443456	,	0.370944	,	0.35264	,	0.36576	,	0.293696	,	0.266784	,	0.307456	,	0.360992	,	0.432384	,	0.499328	,	0.533088	,	0.526176	,	0.410304	,	0.29872	,	0.241056	,	0.188096	,	0.156352	,	0.120544	,	0.091872	,	0.07728	,	0.067232
Index	,	0	,	1	,	2	,	3	,	4	,	5	,	6	,	7	,	8	,	9	,	10	,	11	,	12	,	13	,	14	,	15	,	16	,	17	,	18	,	19	,	20	,	21	,	22	,	23	,	24	,	25	,	26	,	27	,	28

ProfilingBuildAggregator: 
BuildBottomLevelAS	,	0.826
BuildBottomLevelASGpu	,	0.127904
BuildTopLevelAS	,	1.0086
BuildTopLevelASGpu	,	0.060736
Duplication	,	1
Triangles	,	12
Meshes	,	1
BottomLevels	,	1
TopLevelSize	,	64
BottomLevelsSize	,	4480
Index	,	0

FpsAggregator: 
FPS	,	50	,	63	,	62	,	62	,	62	,	61	,	61	,	61	,	60	,	60	,	61	,	60	,	61	,	63	,	62	,	61	,	60	,	59	,	59	,	59	,	59	,	62	,	63	,	64	,	65	,	65	,	66	,	67	,	67
UPS	,	59	,	61	,	60	,	60	,	61	,	61	,	60	,	61	,	60	,	61	,	60	,	60	,	60	,	61	,	60	,	60	,	61	,	60	,	60	,	61	,	60	,	60	,	61	,	61	,	60	,	61	,	60	,	60	,	61
FrameTime	,	20.2548	,	16.0665	,	16.1538	,	16.1868	,	16.306	,	16.5956	,	16.5825	,	16.4904	,	16.7249	,	16.9293	,	16.404	,	16.681	,	16.4197	,	16.0205	,	16.2501	,	16.4787	,	16.707	,	17.0072	,	17.0697	,	17.0395	,	16.9515	,	16.2991	,	16.0711	,	15.7799	,	15.5926	,	15.4185	,	15.2181	,	15.077	,	15.0303
GigaRays/s	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0
Index	,	0	,	1	,	2	,	3	,	4	,	5	,	6	,	7	,	8	,	9	,	10	,	11	,	12	,	13	,	14	,	15	,	16	,	17	,	18	,	19	,	20	,	21	,	22	,	23	,	24	,	25	,	26	,	27	,	28


AccelerationStructure: 
Ray tracing acceleration structure building statistics: 
	Bottom Level Acceleration Structure: 
		Maximum [B]: 4480
		Minimum [B]: 4480
		Currently Allocated [B]: 4480
		Currently Created [num]: 1
		Current Average [B]: 4480
		Maximum Triangles [num]: 12
		Minimum Triangles [num]: 12
		Total Triangles [num]: 12
		Maximum Meshes [num]: 1
		Minimum Meshes [num]: 1
		Total Meshes [num]: 1
	Top Level Acceleration Structure: 
		Maximum [B]: 64
		Minimum [B]: 64
		Currently Allocated [B]: 64
		Total Allocated [B]: 64
		Total Created [num]: 1
		Average Allocated [B]: 64
		Maximum Instances [num]: 1
		Minimum Instances [num]: 1
		Total Instances [num]: 1
	Scratch Buffer: 
		Maximum [B]: 896
		Minimum [B]: 896
		Currently Allocated [B]: 896
		Total Allocated [B]: 896
		Total Created [num]: 1
		Average Allocated [B]: 896
ProfilingStack: 
[PROF] Profiling statistics: 
	Scopes entered : 113675
	Scopes exited : 113674
	Overhead per scope [ticks] : 104
	Threads entered : 3
	Threads exited : 0
	============================
		         Sum[Mt]   Parent[%]  Samples[num]       Avg[Mt]   Last/Curr[Mt]        Self[Mt]     Self[%] 	 Name

	main:

		   29627.920 Mt   100.000 %         1 x    29627.922 Mt    29627.920 Mt        0.407 Mt     0.001 % 	 /

		   29627.546 Mt    99.999 %         1 x    29627.547 Mt    29627.545 Mt      261.035 Mt     0.881 % 	 ./Main application loop

		       2.299 Mt     0.008 %         1 x        2.299 Mt        0.000 Mt        2.299 Mt   100.000 % 	 ../TexturedRLInitialization

		       1.413 Mt     0.005 %         1 x        1.413 Mt        0.000 Mt        1.413 Mt   100.000 % 	 ../ShadowMapRLInitialization

		       5.264 Mt     0.018 %         1 x        5.264 Mt        0.000 Mt        5.264 Mt   100.000 % 	 ../RasterResolveRLInitialization

		       3.583 Mt     0.012 %         1 x        3.583 Mt        0.000 Mt        3.583 Mt   100.000 % 	 ../ResolveRLInitialization

		   29230.365 Mt    98.659 %     92232 x        0.317 Mt       29.004 Mt      421.992 Mt     1.444 % 	 ../MainLoop

		     216.571 Mt     0.741 %      1752 x        0.124 Mt        0.092 Mt      215.952 Mt    99.715 % 	 .../Update

		       0.618 Mt     0.285 %         1 x        0.618 Mt        0.000 Mt        0.618 Mt   100.000 % 	 ..../GuiModelApply

		   28591.803 Mt    97.815 %      1786 x       16.009 Mt       15.085 Mt      213.730 Mt     0.748 % 	 .../Render

		     343.433 Mt     1.201 %      1786 x        0.192 Mt        0.242 Mt      341.215 Mt    99.354 % 	 ..../Rasterization

		       1.179 Mt     0.343 %      1786 x        0.001 Mt        0.001 Mt        1.179 Mt   100.000 % 	 ...../DeferredRLPrep

		       1.039 Mt     0.303 %      1786 x        0.001 Mt        0.001 Mt        1.039 Mt   100.000 % 	 ...../ShadowMapRLPrep

		   28034.640 Mt    98.051 %      1786 x       15.697 Mt       14.679 Mt    28034.640 Mt   100.000 % 	 ..../Present

		       2.903 Mt     0.010 %         1 x        2.903 Mt        0.000 Mt        2.903 Mt   100.000 % 	 ../AmbientOcclusionRLInitialization

		       5.834 Mt     0.020 %         1 x        5.834 Mt        0.000 Mt        5.834 Mt   100.000 % 	 ../DeferredRLInitialization

		      51.384 Mt     0.173 %         1 x       51.384 Mt        0.000 Mt        2.350 Mt     4.573 % 	 ../RayTracingRLInitialization

		      49.034 Mt    95.427 %         1 x       49.034 Mt        0.000 Mt       49.034 Mt   100.000 % 	 .../PipelineBuild

		      63.465 Mt     0.214 %         1 x       63.465 Mt        0.000 Mt        0.182 Mt     0.286 % 	 ../Initialization

		      63.283 Mt    99.714 %         1 x       63.283 Mt        0.000 Mt        1.118 Mt     1.767 % 	 .../ScenePrep

		      22.628 Mt    35.757 %         1 x       22.628 Mt        0.000 Mt        6.152 Mt    27.189 % 	 ..../SceneLoad

		      10.701 Mt    47.290 %         1 x       10.701 Mt        0.000 Mt        2.775 Mt    25.929 % 	 ...../glTF-LoadScene

		       7.926 Mt    74.071 %         2 x        3.963 Mt        0.000 Mt        7.926 Mt   100.000 % 	 ....../glTF-LoadImageData

		       2.258 Mt     9.977 %         1 x        2.258 Mt        0.000 Mt        2.258 Mt   100.000 % 	 ...../glTF-CreateScene

		       3.517 Mt    15.544 %         1 x        3.517 Mt        0.000 Mt        3.517 Mt   100.000 % 	 ...../SceneDesc-CreateScene

		       0.903 Mt     1.426 %         1 x        0.903 Mt        0.000 Mt        0.001 Mt     0.122 % 	 ..../ShadowMapRLPrep

		       0.901 Mt    99.878 %         1 x        0.901 Mt        0.000 Mt        0.893 Mt    99.068 % 	 ...../PrepareForRendering

		       0.008 Mt     0.932 %         1 x        0.008 Mt        0.000 Mt        0.008 Mt   100.000 % 	 ....../PrepareDrawBundle

		       1.451 Mt     2.293 %         1 x        1.451 Mt        0.000 Mt        0.001 Mt     0.041 % 	 ..../TexturedRLPrep

		       1.450 Mt    99.959 %         1 x        1.450 Mt        0.000 Mt        1.446 Mt    99.710 % 	 ...../PrepareForRendering

		       0.001 Mt     0.090 %         1 x        0.001 Mt        0.000 Mt        0.001 Mt   100.000 % 	 ....../PrepareMaterials

		       0.003 Mt     0.200 %         1 x        0.003 Mt        0.000 Mt        0.003 Mt   100.000 % 	 ....../PrepareDrawBundle

		       1.362 Mt     2.152 %         1 x        1.362 Mt        0.000 Mt        0.001 Mt     0.037 % 	 ..../DeferredRLPrep

		       1.361 Mt    99.963 %         1 x        1.361 Mt        0.000 Mt        1.059 Mt    77.804 % 	 ...../PrepareForRendering

		       0.000 Mt     0.029 %         1 x        0.000 Mt        0.000 Mt        0.000 Mt   100.000 % 	 ....../PrepareMaterials

		       0.299 Mt    21.954 %         1 x        0.299 Mt        0.000 Mt        0.299 Mt   100.000 % 	 ....../BuildMaterialCache

		       0.003 Mt     0.213 %         1 x        0.003 Mt        0.000 Mt        0.003 Mt   100.000 % 	 ....../PrepareDrawBundle

		      24.981 Mt    39.475 %         1 x       24.981 Mt        0.000 Mt        2.569 Mt    10.282 % 	 ..../RayTracingRLPrep

		       0.055 Mt     0.220 %         1 x        0.055 Mt        0.000 Mt        0.055 Mt   100.000 % 	 ...../PrepareAccelerationStructures

		       0.001 Mt     0.003 %         1 x        0.001 Mt        0.000 Mt        0.001 Mt   100.000 % 	 ...../PrepareCache

		      12.812 Mt    51.288 %         1 x       12.812 Mt        0.000 Mt       12.812 Mt   100.000 % 	 ...../PipelineBuild

		       0.329 Mt     1.317 %         1 x        0.329 Mt        0.000 Mt        0.329 Mt   100.000 % 	 ...../BuildCache

		       7.865 Mt    31.485 %         1 x        7.865 Mt        0.000 Mt        0.001 Mt     0.006 % 	 ...../BuildAccelerationStructures

		       7.865 Mt    99.994 %         1 x        7.865 Mt        0.000 Mt        6.030 Mt    76.673 % 	 ....../AccelerationStructureBuild

		       0.826 Mt    10.502 %         1 x        0.826 Mt        0.000 Mt        0.826 Mt   100.000 % 	 ......./BuildBottomLevelAS

		       1.009 Mt    12.824 %         1 x        1.009 Mt        0.000 Mt        1.009 Mt   100.000 % 	 ......./BuildTopLevelAS

		       1.350 Mt     5.405 %         1 x        1.350 Mt        0.000 Mt        1.350 Mt   100.000 % 	 ...../BuildShaderTables

		      10.841 Mt    17.130 %         1 x       10.841 Mt        0.000 Mt       10.841 Mt   100.000 % 	 ..../GpuSidePrep

	WindowThread:

		   29626.543 Mt   100.000 %         1 x    29626.543 Mt    29626.543 Mt    29626.543 Mt   100.000 % 	 /

	GpuThread:

		   27032.401 Mt   100.000 %      1786 x       15.136 Mt       13.913 Mt      133.475 Mt     0.494 % 	 /

		       0.194 Mt     0.001 %         1 x        0.194 Mt        0.000 Mt        0.005 Mt     2.520 % 	 ./ScenePrep

		       0.189 Mt    97.480 %         1 x        0.189 Mt        0.000 Mt        0.001 Mt     0.405 % 	 ../AccelerationStructureBuild

		       0.128 Mt    67.528 %         1 x        0.128 Mt        0.000 Mt        0.128 Mt   100.000 % 	 .../BuildBottomLevelASGpu

		       0.061 Mt    32.066 %         1 x        0.061 Mt        0.000 Mt        0.061 Mt   100.000 % 	 .../BuildTopLevelASGpu

		   26751.455 Mt    98.961 %      1786 x       14.978 Mt       13.565 Mt        5.370 Mt     0.020 % 	 ./RasterizationGpu

		     535.625 Mt     2.002 %      1786 x        0.300 Mt        0.067 Mt      535.625 Mt   100.000 % 	 ../DeferredRLGpu

		      52.500 Mt     0.196 %      1786 x        0.029 Mt        0.029 Mt       52.500 Mt   100.000 % 	 ../ShadowMapRLGpu

		   12473.443 Mt    46.627 %      1786 x        6.984 Mt        6.367 Mt    12473.443 Mt   100.000 % 	 ../AmbientOcclusionRLGpu

		   13684.517 Mt    51.154 %      1786 x        7.662 Mt        7.101 Mt    13684.517 Mt   100.000 % 	 ../RasterResolveRLGpu

		     147.277 Mt     0.545 %      1786 x        0.082 Mt        0.348 Mt      147.277 Mt   100.000 % 	 ./Present


	============================


