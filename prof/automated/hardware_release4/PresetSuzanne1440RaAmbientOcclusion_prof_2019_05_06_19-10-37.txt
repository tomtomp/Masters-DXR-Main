Configuration: 
Command line parameters  = E:\programming\FIT-DIP\Masters-DXR-Main-git\build\Measurement\x64\Release\Measurement.exe --scene Suzanne/Suzanne.gltf --profile-automation --profile-camera-track Suzanne/Suzanne.trk --width 2560 --height 1440 --profile-output PresetSuzanne1440RaAmbientOcclusion --quality-preset AmbientOcclusion 
Build type               = Release
Device name              = NVIDIA GeForce RTX 2080 Ti
Device memory MB         = 11049
Ray Tracing backend      = Hardware
Window width             = 2560
Window height            = 1421
Window title             = Direct3D App
Back-buffer count        = 3
Fill Triangles           = enabled
Triangles Font Clock-Wise= enabled
Fast Build AS            = disabled
Target FPS               = 0
Target UPS               = 60
Tearing                  = enabled
VSync                    = disabled
GUI rendering            = disabled
Debug layer              = disabled
GPU validation           = disabled
GPU profiling            = enabled
Render mode              = Rasterization
Loaded scene file        = Suzanne/Suzanne.gltf
Using testing scene      = disabled
Duplication              = 1
Duplication in cube      = disabled
Duplication offset       = 10
BLAS duplication         = disabled
Rays per pixel           = 4
Quality preset           = AmbientOcclusion
Rasterization Quality    : 
  AO                     = enabled
  AO kernel size         = 4
  Shadows                = disabled
  Shadow map resolution  = 1
  Shadow PCF kernel size = 0
  Reflections            = disabled
Ray Tracing Quality      : 
  AO                     = enabled
  AO rays                = 4
  AO kernel size         = 4
  Shadows                = disabled
  Shadow rays            = 0
  Shadow kernel size     = 4
  Reflections            = disabled

ProfilingAggregator: 
Render	,	1.1151	,	1.3602	,	1.531	,	1.3541	,	1.1446	,	0.8773	,	1.1637	,	1.0301	,	1.0751	,	1.0817	,	1.1443	,	1.1218	,	0.9009	,	0.9403	,	1.4695	,	1.6186	,	1.4367	,	2.0677	,	2.4016	,	2.0969	,	1.8342	,	1.8016	,	2.3405	,	2.2476
Update	,	0.065	,	0.0739	,	0.0773	,	0.0781	,	0.0758	,	0.0432	,	0.0562	,	0.0437	,	0.0912	,	0.067	,	0.0653	,	0.0721	,	0.0387	,	0.0683	,	0.0705	,	0.0781	,	0.0877	,	0.0774	,	0.0783	,	0.069	,	0.0682	,	0.0574	,	0.069	,	0.0684
DeferredRLGpu	,	0.139744	,	0.185408	,	0.199808	,	0.154016	,	0.100992	,	0.070624	,	0.053792	,	0.046144	,	0.063392	,	0.092704	,	0.104992	,	0.079552	,	0.056352	,	0.055104	,	0.086784	,	0.184928	,	0.227968	,	0.259968	,	0.406528	,	0.31536	,	0.268384	,	0.13824	,	0.083168	,	0.071872
Index	,	0	,	1	,	2	,	3	,	4	,	5	,	6	,	7	,	8	,	9	,	10	,	11	,	12	,	13	,	14	,	15	,	16	,	17	,	18	,	19	,	20	,	21	,	22	,	23

ProfilingBuildAggregator: 
BuildBottomLevelAS	,	0.8711
BuildBottomLevelASGpu	,	0.452
BuildTopLevelAS	,	0.7708
BuildTopLevelASGpu	,	0.061728
Duplication	,	1
Triangles	,	3936
Meshes	,	1
BottomLevels	,	1
TopLevelSize	,	64
BottomLevelsSize	,	282112
Index	,	0

FpsAggregator: 
FPS	,	666	,	713	,	690	,	682	,	778	,	940	,	942	,	948	,	927	,	900	,	852	,	863	,	924	,	1007	,	892	,	762	,	637	,	618	,	509	,	507	,	565	,	690	,	860	,	906
UPS	,	59	,	60	,	61	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60
FrameTime	,	1.50183	,	1.40379	,	1.45129	,	1.46694	,	1.28603	,	1.06472	,	1.06173	,	1.05528	,	1.07878	,	1.1119	,	1.17403	,	1.15962	,	1.08273	,	0.993477	,	1.12121	,	1.31326	,	1.56986	,	1.62141	,	1.96572	,	1.97416	,	1.77072	,	1.45018	,	1.16355	,	1.1045
GigaRays/s	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0
Index	,	0	,	1	,	2	,	3	,	4	,	5	,	6	,	7	,	8	,	9	,	10	,	11	,	12	,	13	,	14	,	15	,	16	,	17	,	18	,	19	,	20	,	21	,	22	,	23


AccelerationStructure: 
Ray tracing acceleration structure building statistics: 
	Bottom Level Acceleration Structure: 
		Maximum [B]: 282112
		Minimum [B]: 282112
		Currently Allocated [B]: 282112
		Currently Created [num]: 1
		Current Average [B]: 282112
		Maximum Triangles [num]: 3936
		Minimum Triangles [num]: 3936
		Total Triangles [num]: 3936
		Maximum Meshes [num]: 1
		Minimum Meshes [num]: 1
		Total Meshes [num]: 1
	Top Level Acceleration Structure: 
		Maximum [B]: 64
		Minimum [B]: 64
		Currently Allocated [B]: 64
		Total Allocated [B]: 64
		Total Created [num]: 1
		Average Allocated [B]: 64
		Maximum Instances [num]: 1
		Minimum Instances [num]: 1
		Total Instances [num]: 1
	Scratch Buffer: 
		Maximum [B]: 82944
		Minimum [B]: 82944
		Currently Allocated [B]: 82944
		Total Allocated [B]: 82944
		Total Created [num]: 1
		Average Allocated [B]: 82944
ProfilingStack: 
[PROF] Profiling statistics: 
	Scopes entered : 298343
	Scopes exited : 298342
	Overhead per scope [ticks] : 103.6
	Threads entered : 3
	Threads exited : 0
	============================
		         Sum[Mt]   Parent[%]  Samples[num]       Avg[Mt]   Last/Curr[Mt]        Self[Mt]     Self[%] 	 Name

	main:

		   24467.411 Mt   100.000 %         1 x    24467.412 Mt    24467.411 Mt        0.399 Mt     0.002 % 	 /

		   24467.029 Mt    99.998 %         1 x    24467.030 Mt    24467.029 Mt      275.796 Mt     1.127 % 	 ./Main application loop

		       2.313 Mt     0.009 %         1 x        2.313 Mt        0.000 Mt        2.313 Mt   100.000 % 	 ../TexturedRLInitialization

		       1.337 Mt     0.005 %         1 x        1.337 Mt        0.000 Mt        1.337 Mt   100.000 % 	 ../ShadowMapRLInitialization

		       4.837 Mt     0.020 %         1 x        4.837 Mt        0.000 Mt        4.837 Mt   100.000 % 	 ../RasterResolveRLInitialization

		       3.675 Mt     0.015 %         1 x        3.675 Mt        0.000 Mt        3.675 Mt   100.000 % 	 ../ResolveRLInitialization

		   24018.267 Mt    98.166 %    109057 x        0.220 Mt        1.214 Mt      348.876 Mt     1.453 % 	 ../MainLoop

		     176.029 Mt     0.733 %      1441 x        0.122 Mt        0.063 Mt      175.434 Mt    99.662 % 	 .../Update

		       0.595 Mt     0.338 %         1 x        0.595 Mt        0.000 Mt        0.595 Mt   100.000 % 	 ..../GuiModelApply

		   23493.362 Mt    97.815 %     18780 x        1.251 Mt        1.149 Mt     1364.702 Mt     5.809 % 	 .../Render

		    2021.039 Mt     8.603 %     18780 x        0.108 Mt        0.087 Mt     1998.035 Mt    98.862 % 	 ..../Rasterization

		      13.315 Mt     0.659 %     18780 x        0.001 Mt        0.001 Mt       13.315 Mt   100.000 % 	 ...../DeferredRLPrep

		       9.689 Mt     0.479 %     18780 x        0.001 Mt        0.001 Mt        9.689 Mt   100.000 % 	 ...../ShadowMapRLPrep

		   20107.622 Mt    85.589 %     18780 x        1.071 Mt        1.001 Mt    20107.622 Mt   100.000 % 	 ..../Present

		       2.608 Mt     0.011 %         1 x        2.608 Mt        0.000 Mt        2.608 Mt   100.000 % 	 ../AmbientOcclusionRLInitialization

		       5.840 Mt     0.024 %         1 x        5.840 Mt        0.000 Mt        5.840 Mt   100.000 % 	 ../DeferredRLInitialization

		      50.723 Mt     0.207 %         1 x       50.723 Mt        0.000 Mt        2.388 Mt     4.708 % 	 ../RayTracingRLInitialization

		      48.334 Mt    95.292 %         1 x       48.334 Mt        0.000 Mt       48.334 Mt   100.000 % 	 .../PipelineBuild

		     101.634 Mt     0.415 %         1 x      101.634 Mt        0.000 Mt        0.184 Mt     0.181 % 	 ../Initialization

		     101.450 Mt    99.819 %         1 x      101.450 Mt        0.000 Mt        1.155 Mt     1.138 % 	 .../ScenePrep

		      60.534 Mt    59.669 %         1 x       60.534 Mt        0.000 Mt       12.955 Mt    21.401 % 	 ..../SceneLoad

		      39.342 Mt    64.992 %         1 x       39.342 Mt        0.000 Mt        8.599 Mt    21.857 % 	 ...../glTF-LoadScene

		      30.743 Mt    78.143 %         2 x       15.372 Mt        0.000 Mt       30.743 Mt   100.000 % 	 ....../glTF-LoadImageData

		       6.451 Mt    10.657 %         1 x        6.451 Mt        0.000 Mt        6.451 Mt   100.000 % 	 ...../glTF-CreateScene

		       1.786 Mt     2.950 %         1 x        1.786 Mt        0.000 Mt        1.786 Mt   100.000 % 	 ...../SceneDesc-CreateScene

		       0.986 Mt     0.972 %         1 x        0.986 Mt        0.000 Mt        0.000 Mt     0.030 % 	 ..../ShadowMapRLPrep

		       0.986 Mt    99.970 %         1 x        0.986 Mt        0.000 Mt        0.978 Mt    99.168 % 	 ...../PrepareForRendering

		       0.008 Mt     0.832 %         1 x        0.008 Mt        0.000 Mt        0.008 Mt   100.000 % 	 ....../PrepareDrawBundle

		       1.446 Mt     1.425 %         1 x        1.446 Mt        0.000 Mt        0.000 Mt     0.035 % 	 ..../TexturedRLPrep

		       1.445 Mt    99.965 %         1 x        1.445 Mt        0.000 Mt        1.442 Mt    99.737 % 	 ...../PrepareForRendering

		       0.001 Mt     0.055 %         1 x        0.001 Mt        0.000 Mt        0.001 Mt   100.000 % 	 ....../PrepareMaterials

		       0.003 Mt     0.208 %         1 x        0.003 Mt        0.000 Mt        0.003 Mt   100.000 % 	 ....../PrepareDrawBundle

		       1.327 Mt     1.308 %         1 x        1.327 Mt        0.000 Mt        0.001 Mt     0.045 % 	 ..../DeferredRLPrep

		       1.327 Mt    99.955 %         1 x        1.327 Mt        0.000 Mt        1.019 Mt    76.837 % 	 ...../PrepareForRendering

		       0.000 Mt     0.030 %         1 x        0.000 Mt        0.000 Mt        0.000 Mt   100.000 % 	 ....../PrepareMaterials

		       0.304 Mt    22.929 %         1 x        0.304 Mt        0.000 Mt        0.304 Mt   100.000 % 	 ....../BuildMaterialCache

		       0.003 Mt     0.204 %         1 x        0.003 Mt        0.000 Mt        0.003 Mt   100.000 % 	 ....../PrepareDrawBundle

		      24.764 Mt    24.410 %         1 x       24.764 Mt        0.000 Mt        2.606 Mt    10.524 % 	 ..../RayTracingRLPrep

		       0.048 Mt     0.192 %         1 x        0.048 Mt        0.000 Mt        0.048 Mt   100.000 % 	 ...../PrepareAccelerationStructures

		       0.001 Mt     0.003 %         1 x        0.001 Mt        0.000 Mt        0.001 Mt   100.000 % 	 ...../PrepareCache

		      12.622 Mt    50.970 %         1 x       12.622 Mt        0.000 Mt       12.622 Mt   100.000 % 	 ...../PipelineBuild

		       0.350 Mt     1.414 %         1 x        0.350 Mt        0.000 Mt        0.350 Mt   100.000 % 	 ...../BuildCache

		       7.724 Mt    31.191 %         1 x        7.724 Mt        0.000 Mt        0.000 Mt     0.004 % 	 ...../BuildAccelerationStructures

		       7.724 Mt    99.996 %         1 x        7.724 Mt        0.000 Mt        6.082 Mt    78.742 % 	 ....../AccelerationStructureBuild

		       0.871 Mt    11.278 %         1 x        0.871 Mt        0.000 Mt        0.871 Mt   100.000 % 	 ......./BuildBottomLevelAS

		       0.771 Mt     9.980 %         1 x        0.771 Mt        0.000 Mt        0.771 Mt   100.000 % 	 ......./BuildTopLevelAS

		       1.413 Mt     5.706 %         1 x        1.413 Mt        0.000 Mt        1.413 Mt   100.000 % 	 ...../BuildShaderTables

		      11.238 Mt    11.077 %         1 x       11.238 Mt        0.000 Mt       11.238 Mt   100.000 % 	 ..../GpuSidePrep

	WindowThread:

		   24465.575 Mt   100.000 %         1 x    24465.576 Mt    24465.575 Mt    24465.575 Mt   100.000 % 	 /

	GpuThread:

		   13530.398 Mt   100.000 %     18780 x        0.720 Mt        0.595 Mt      133.915 Mt     0.990 % 	 /

		       0.520 Mt     0.004 %         1 x        0.520 Mt        0.000 Mt        0.006 Mt     1.113 % 	 ./ScenePrep

		       0.515 Mt    98.887 %         1 x        0.515 Mt        0.000 Mt        0.001 Mt     0.174 % 	 ../AccelerationStructureBuild

		       0.452 Mt    87.831 %         1 x        0.452 Mt        0.000 Mt        0.452 Mt   100.000 % 	 .../BuildBottomLevelASGpu

		       0.062 Mt    11.995 %         1 x        0.062 Mt        0.000 Mt        0.062 Mt   100.000 % 	 .../BuildTopLevelASGpu

		   13343.984 Mt    98.622 %     18780 x        0.711 Mt        0.591 Mt       48.040 Mt     0.360 % 	 ./RasterizationGpu

		    2408.871 Mt    18.052 %     18780 x        0.128 Mt        0.071 Mt     2408.871 Mt   100.000 % 	 ../DeferredRLGpu

		    8791.278 Mt    65.882 %     18780 x        0.468 Mt        0.445 Mt     8791.278 Mt   100.000 % 	 ../AmbientOcclusionRLGpu

		    2095.795 Mt    15.706 %     18780 x        0.112 Mt        0.074 Mt     2095.795 Mt   100.000 % 	 ../RasterResolveRLGpu

		      51.979 Mt     0.384 %     18780 x        0.003 Mt        0.002 Mt       51.979 Mt   100.000 % 	 ./Present


	============================


