Configuration: 
Command line parameters  = E:\programming\FIT-DIP\Masters-DXR-Main-git\build\Measurement\x64\Release\Measurement.exe --scene Sponza/Sponza.gltf --profile-automation --profile-camera-track Sponza/SponzaPreset.trk --width 2560 --height 1440 --profile-output PresetSponza1440RaUltra --quality-preset Ultra 
Build type               = Release
Device name              = NVIDIA GeForce RTX 2080 Ti
Device memory MB         = 11049
Ray Tracing backend      = Hardware
Window width             = 2560
Window height            = 1421
Window title             = Direct3D App
Back-buffer count        = 3
Fill Triangles           = enabled
Triangles Font Clock-Wise= enabled
Fast Build AS            = disabled
Target FPS               = 0
Target UPS               = 60
Tearing                  = enabled
VSync                    = disabled
GUI rendering            = disabled
Debug layer              = disabled
GPU validation           = disabled
GPU profiling            = enabled
Render mode              = Rasterization
Loaded scene file        = Sponza/Sponza.gltf
Using testing scene      = disabled
Duplication              = 1
Duplication in cube      = disabled
Duplication offset       = 10
BLAS duplication         = disabled
Rays per pixel           = 33
Quality preset           = Ultra
Rasterization Quality    : 
  AO                     = enabled
  AO kernel size         = 16
  Shadows                = enabled
  Shadow map resolution  = 8192
  Shadow PCF kernel size = 8
  Reflections            = enabled
Ray Tracing Quality      : 
  AO                     = enabled
  AO rays                = 16
  AO kernel size         = 8
  Shadows                = enabled
  Shadow rays            = 8
  Shadow kernel size     = 8
  Reflections            = enabled

ProfilingAggregator: 
Render	,	19.6699	,	19.5544	,	19.3842	,	18.0119	,	18.1861	,	18.5023	,	18.9557	,	19.5168	,	19.551	,	18.5802	,	18.231	,	18.8531	,	19.5381	,	19.5874	,	19.007	,	18.4365	,	19.0973	,	18.6289	,	18.408	,	19.781	,	21.2245	,	20.8761	,	20.9905
Update	,	0.0781	,	0.039	,	0.0775	,	0.0767	,	0.0783	,	0.0764	,	0.0766	,	0.0755	,	0.1184	,	0.0771	,	0.0436	,	0.0787	,	0.0746	,	0.079	,	0.0721	,	0.0767	,	0.0475	,	0.0756	,	0.0762	,	0.0477	,	0.0762	,	0.0768	,	0.0468
DeferredRLGpu	,	2.75136	,	2.48954	,	2.3415	,	1.4944	,	1.44493	,	1.60282	,	2.03952	,	2.36819	,	2.06109	,	1.5201	,	1.38717	,	1.77037	,	2.18874	,	2.43494	,	2.18282	,	1.54966	,	2.03178	,	1.91302	,	1.51254	,	2.96362	,	3.85018	,	3.87722	,	3.89347
Index	,	0	,	1	,	2	,	3	,	4	,	5	,	6	,	7	,	8	,	9	,	10	,	11	,	12	,	13	,	14	,	15	,	16	,	17	,	18	,	19	,	20	,	21	,	22

ProfilingBuildAggregator: 
BuildBottomLevelAS	,	1.7439
BuildBottomLevelASGpu	,	2.38314
BuildTopLevelAS	,	0.776
BuildTopLevelASGpu	,	0.0896
Duplication	,	1
Triangles	,	262267
Meshes	,	103
BottomLevels	,	1
TopLevelSize	,	64
BottomLevelsSize	,	18390656
Index	,	0

FpsAggregator: 
FPS	,	33	,	50	,	52	,	53	,	55	,	54	,	54	,	51	,	51	,	53	,	54	,	54	,	52	,	51	,	51	,	54	,	53	,	53	,	54	,	53	,	48	,	47	,	47
UPS	,	58	,	62	,	60	,	61	,	60	,	61	,	60	,	61	,	60	,	61	,	60	,	60	,	61	,	60	,	60	,	61	,	61	,	60	,	61	,	61	,	60	,	60	,	61
FrameTime	,	30.3652	,	20.3657	,	19.4845	,	18.9838	,	18.3752	,	18.6583	,	18.7069	,	19.8483	,	19.6151	,	19.0947	,	18.6237	,	18.5675	,	19.3271	,	19.7175	,	19.7078	,	18.7593	,	19.0199	,	19.0796	,	18.8371	,	18.9546	,	20.9886	,	21.4351	,	21.3949
GigaRays/s	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0
Index	,	0	,	1	,	2	,	3	,	4	,	5	,	6	,	7	,	8	,	9	,	10	,	11	,	12	,	13	,	14	,	15	,	16	,	17	,	18	,	19	,	20	,	21	,	22


AccelerationStructure: 
Ray tracing acceleration structure building statistics: 
	Bottom Level Acceleration Structure: 
		Maximum [B]: 18390656
		Minimum [B]: 18390656
		Currently Allocated [B]: 18390656
		Currently Created [num]: 1
		Current Average [B]: 18390656
		Maximum Triangles [num]: 262267
		Minimum Triangles [num]: 262267
		Total Triangles [num]: 262267
		Maximum Meshes [num]: 103
		Minimum Meshes [num]: 103
		Total Meshes [num]: 103
	Top Level Acceleration Structure: 
		Maximum [B]: 64
		Minimum [B]: 64
		Currently Allocated [B]: 64
		Total Allocated [B]: 64
		Total Created [num]: 1
		Average Allocated [B]: 64
		Maximum Instances [num]: 1
		Minimum Instances [num]: 1
		Total Instances [num]: 1
	Scratch Buffer: 
		Maximum [B]: 14995072
		Minimum [B]: 14995072
		Currently Allocated [B]: 14995072
		Total Allocated [B]: 14995072
		Total Created [num]: 1
		Average Allocated [B]: 14995072
ProfilingStack: 
[PROF] Profiling statistics: 
	Scopes entered : 90176
	Scopes exited : 90175
	Overhead per scope [ticks] : 104.8
	Threads entered : 3
	Threads exited : 0
	============================
		         Sum[Mt]   Parent[%]  Samples[num]       Avg[Mt]   Last/Curr[Mt]        Self[Mt]     Self[%] 	 Name

	main:

		   26137.493 Mt   100.000 %         1 x    26137.494 Mt    26137.493 Mt        0.414 Mt     0.002 % 	 /

		   26137.111 Mt    99.999 %         1 x    26137.113 Mt    26137.111 Mt      250.201 Mt     0.957 % 	 ./Main application loop

		       3.543 Mt     0.014 %         1 x        3.543 Mt        0.000 Mt        3.543 Mt   100.000 % 	 ../TexturedRLInitialization

		       1.327 Mt     0.005 %         1 x        1.327 Mt        0.000 Mt        1.327 Mt   100.000 % 	 ../ShadowMapRLInitialization

		       4.697 Mt     0.018 %         1 x        4.697 Mt        0.000 Mt        4.697 Mt   100.000 % 	 ../RasterResolveRLInitialization

		       3.760 Mt     0.014 %         1 x        3.760 Mt        0.000 Mt        3.760 Mt   100.000 % 	 ../ResolveRLInitialization

		   23223.931 Mt    88.854 %     75715 x        0.307 Mt       34.674 Mt      416.398 Mt     1.793 % 	 ../MainLoop

		     187.611 Mt     0.808 %      1391 x        0.135 Mt        0.087 Mt      187.260 Mt    99.813 % 	 .../Update

		       0.351 Mt     0.187 %         1 x        0.351 Mt        0.000 Mt        0.351 Mt   100.000 % 	 ..../GuiModelApply

		   22619.923 Mt    97.399 %      1178 x       19.202 Mt       21.040 Mt      145.668 Mt     0.644 % 	 .../Render

		     237.675 Mt     1.051 %      1178 x        0.202 Mt        0.221 Mt      236.237 Mt    99.395 % 	 ..../Rasterization

		       0.713 Mt     0.300 %      1178 x        0.001 Mt        0.001 Mt        0.713 Mt   100.000 % 	 ...../DeferredRLPrep

		       0.726 Mt     0.305 %      1178 x        0.001 Mt        0.001 Mt        0.726 Mt   100.000 % 	 ...../ShadowMapRLPrep

		   22236.579 Mt    98.305 %      1178 x       18.877 Mt       20.653 Mt    22236.579 Mt   100.000 % 	 ..../Present

		       2.587 Mt     0.010 %         1 x        2.587 Mt        0.000 Mt        2.587 Mt   100.000 % 	 ../AmbientOcclusionRLInitialization

		       4.693 Mt     0.018 %         1 x        4.693 Mt        0.000 Mt        4.693 Mt   100.000 % 	 ../DeferredRLInitialization

		      51.962 Mt     0.199 %         1 x       51.962 Mt        0.000 Mt        2.377 Mt     4.575 % 	 ../RayTracingRLInitialization

		      49.585 Mt    95.425 %         1 x       49.585 Mt        0.000 Mt       49.585 Mt   100.000 % 	 .../PipelineBuild

		    2590.409 Mt     9.911 %         1 x     2590.409 Mt        0.000 Mt        0.199 Mt     0.008 % 	 ../Initialization

		    2590.210 Mt    99.992 %         1 x     2590.210 Mt        0.000 Mt        2.272 Mt     0.088 % 	 .../ScenePrep

		    2522.451 Mt    97.384 %         1 x     2522.451 Mt        0.000 Mt      253.446 Mt    10.048 % 	 ..../SceneLoad

		    1984.909 Mt    78.690 %         1 x     1984.909 Mt        0.000 Mt      576.399 Mt    29.039 % 	 ...../glTF-LoadScene

		    1408.510 Mt    70.961 %        69 x       20.413 Mt        0.000 Mt     1408.510 Mt   100.000 % 	 ....../glTF-LoadImageData

		     210.292 Mt     8.337 %         1 x      210.292 Mt        0.000 Mt      210.292 Mt   100.000 % 	 ...../glTF-CreateScene

		      73.804 Mt     2.926 %         1 x       73.804 Mt        0.000 Mt       73.804 Mt   100.000 % 	 ...../SceneDesc-CreateScene

		       1.183 Mt     0.046 %         1 x        1.183 Mt        0.000 Mt        0.000 Mt     0.042 % 	 ..../ShadowMapRLPrep

		       1.183 Mt    99.958 %         1 x        1.183 Mt        0.000 Mt        1.139 Mt    96.255 % 	 ...../PrepareForRendering

		       0.044 Mt     3.745 %         1 x        0.044 Mt        0.000 Mt        0.044 Mt   100.000 % 	 ....../PrepareDrawBundle

		       3.613 Mt     0.140 %         1 x        3.613 Mt        0.000 Mt        0.001 Mt     0.017 % 	 ..../TexturedRLPrep

		       3.613 Mt    99.983 %         1 x        3.613 Mt        0.000 Mt        3.576 Mt    98.987 % 	 ...../PrepareForRendering

		       0.001 Mt     0.030 %         1 x        0.001 Mt        0.000 Mt        0.001 Mt   100.000 % 	 ....../PrepareMaterials

		       0.035 Mt     0.983 %         1 x        0.035 Mt        0.000 Mt        0.035 Mt   100.000 % 	 ....../PrepareDrawBundle

		       3.512 Mt     0.136 %         1 x        3.512 Mt        0.000 Mt        0.001 Mt     0.026 % 	 ..../DeferredRLPrep

		       3.511 Mt    99.974 %         1 x        3.511 Mt        0.000 Mt        2.976 Mt    84.744 % 	 ...../PrepareForRendering

		       0.001 Mt     0.017 %         1 x        0.001 Mt        0.000 Mt        0.001 Mt   100.000 % 	 ....../PrepareMaterials

		       0.456 Mt    12.975 %         1 x        0.456 Mt        0.000 Mt        0.456 Mt   100.000 % 	 ....../BuildMaterialCache

		       0.080 Mt     2.264 %         1 x        0.080 Mt        0.000 Mt        0.080 Mt   100.000 % 	 ....../PrepareDrawBundle

		      26.664 Mt     1.029 %         1 x       26.664 Mt        0.000 Mt        2.381 Mt     8.928 % 	 ..../RayTracingRLPrep

		       0.060 Mt     0.225 %         1 x        0.060 Mt        0.000 Mt        0.060 Mt   100.000 % 	 ...../PrepareAccelerationStructures

		       0.001 Mt     0.003 %         1 x        0.001 Mt        0.000 Mt        0.001 Mt   100.000 % 	 ...../PrepareCache

		      13.007 Mt    48.783 %         1 x       13.007 Mt        0.000 Mt       13.007 Mt   100.000 % 	 ...../PipelineBuild

		       0.674 Mt     2.529 %         1 x        0.674 Mt        0.000 Mt        0.674 Mt   100.000 % 	 ...../BuildCache

		       5.683 Mt    21.313 %         1 x        5.683 Mt        0.000 Mt        0.001 Mt     0.011 % 	 ...../BuildAccelerationStructures

		       5.682 Mt    99.989 %         1 x        5.682 Mt        0.000 Mt        3.162 Mt    55.653 % 	 ....../AccelerationStructureBuild

		       1.744 Mt    30.691 %         1 x        1.744 Mt        0.000 Mt        1.744 Mt   100.000 % 	 ......./BuildBottomLevelAS

		       0.776 Mt    13.657 %         1 x        0.776 Mt        0.000 Mt        0.776 Mt   100.000 % 	 ......./BuildTopLevelAS

		       4.858 Mt    18.219 %         1 x        4.858 Mt        0.000 Mt        4.858 Mt   100.000 % 	 ...../BuildShaderTables

		      30.514 Mt     1.178 %         1 x       30.514 Mt        0.000 Mt       30.514 Mt   100.000 % 	 ..../GpuSidePrep

	WindowThread:

		   26136.091 Mt   100.000 %         1 x    26136.091 Mt    26136.090 Mt    26136.091 Mt   100.000 % 	 /

	GpuThread:

		   21610.859 Mt   100.000 %      1178 x       18.345 Mt       19.905 Mt      161.344 Mt     0.747 % 	 /

		       2.480 Mt     0.011 %         1 x        2.480 Mt        0.000 Mt        0.006 Mt     0.250 % 	 ./ScenePrep

		       2.474 Mt    99.750 %         1 x        2.474 Mt        0.000 Mt        0.001 Mt     0.058 % 	 ../AccelerationStructureBuild

		       2.383 Mt    96.320 %         1 x        2.383 Mt        0.000 Mt        2.383 Mt   100.000 % 	 .../BuildBottomLevelASGpu

		       0.090 Mt     3.621 %         1 x        0.090 Mt        0.000 Mt        0.090 Mt   100.000 % 	 .../BuildTopLevelASGpu

		   21343.060 Mt    98.761 %      1178 x       18.118 Mt       19.542 Mt        4.266 Mt     0.020 % 	 ./RasterizationGpu

		    2578.787 Mt    12.083 %      1178 x        2.189 Mt        3.858 Mt     2578.787 Mt   100.000 % 	 ../DeferredRLGpu

		     489.741 Mt     2.295 %      1178 x        0.416 Mt        0.374 Mt      489.741 Mt   100.000 % 	 ../ShadowMapRLGpu

		    8506.259 Mt    39.855 %      1178 x        7.221 Mt        6.925 Mt     8506.259 Mt   100.000 % 	 ../AmbientOcclusionRLGpu

		    9764.006 Mt    45.748 %      1178 x        8.289 Mt        8.382 Mt     9764.006 Mt   100.000 % 	 ../RasterResolveRLGpu

		     103.974 Mt     0.481 %      1178 x        0.088 Mt        0.363 Mt      103.974 Mt   100.000 % 	 ./Present


	============================


