Configuration: 
Command line parameters  = E:\programming\FIT-DIP\Masters-DXR-Main-git\build\Measurement\x64\Release\Measurement.exe --scene Cube/Cube.gltf --quality-preset Low --profile-automation --profile-camera-track Cube/Cube.trk --width 1024 --height 768 --profile-output TrackCube768Ra 
Build type               = Release
Device name              = NVIDIA GeForce RTX 2080 Ti
Device memory MB         = 11049
Ray Tracing backend      = Hardware
Window width             = 1024
Window height            = 768
Window title             = Direct3D App
Back-buffer count        = 3
Fill Triangles           = enabled
Triangles Font Clock-Wise= enabled
Fast Build AS            = disabled
Target FPS               = 0
Target UPS               = 60
Tearing                  = enabled
VSync                    = disabled
GUI rendering            = disabled
Debug layer              = disabled
GPU validation           = disabled
GPU profiling            = enabled
Render mode              = Rasterization
Loaded scene file        = Cube/Cube.gltf
Using testing scene      = disabled
Duplication              = 1
Duplication in cube      = disabled
Duplication offset       = 10
BLAS duplication         = disabled
Rays per pixel           = 5
Quality preset           = Low
Rasterization Quality    : 
  AO                     = enabled
  AO kernel size         = 4
  Shadows                = enabled
  Shadow map resolution  = 2048
  Shadow PCF kernel size = 0
  Reflections            = disabled
Ray Tracing Quality      : 
  AO                     = enabled
  AO rays                = 4
  AO kernel size         = 4
  Shadows                = enabled
  Shadow rays            = 1
  Shadow kernel size     = 4
  Reflections            = disabled

ProfilingAggregator: 
Render	,	0.5303	,	0.4676	,	0.5536	,	0.6485	,	0.66	,	0.504	,	0.4832	,	0.5613	,	0.5454	,	0.6075	,	0.6753	,	0.6815	,	0.4678	,	0.9344	,	0.485	,	0.6766	,	0.5443	,	0.5115	,	0.5209	,	0.654	,	0.4872	,	0.4756	,	0.4395	,	0.6032	,	0.471	,	0.5511	,	0.3568	,	0.3702	,	0.4408	,	0.5287
Update	,	0.0223	,	0.0606	,	0.0383	,	0.0638	,	0.025	,	0.031	,	0.0244	,	0.0429	,	0.0289	,	0.0429	,	0.0327	,	0.0292	,	0.0365	,	0.0613	,	0.0246	,	0.0299	,	0.0315	,	0.0221	,	0.0348	,	0.0512	,	0.0226	,	0.0499	,	0.0218	,	0.0307	,	0.0667	,	0.0505	,	0.0607	,	0.0252	,	0.0426	,	0.0378
DeferredRLGpu	,	0.053888	,	0.070304	,	0.074624	,	0.085216	,	0.080512	,	0.098496	,	0.090848	,	0.098784	,	0.104448	,	0.102496	,	0.092512	,	0.097568	,	0.082528	,	0.072704	,	0.084448	,	0.09856	,	0.1072	,	0.10832	,	0.108896	,	0.108032	,	0.092768	,	0.067392	,	0.061888	,	0.053312	,	0.045728	,	0.036576	,	0.028384	,	0.022592	,	0.019904	,	0.019616
Index	,	0	,	1	,	2	,	3	,	4	,	5	,	6	,	7	,	8	,	9	,	10	,	11	,	12	,	13	,	14	,	15	,	16	,	17	,	18	,	19	,	20	,	21	,	22	,	23	,	24	,	25	,	26	,	27	,	28	,	29

ProfilingBuildAggregator: 
BuildBottomLevelAS	,	0.8797
BuildBottomLevelASGpu	,	0.127392
BuildTopLevelAS	,	0.8018
BuildTopLevelASGpu	,	0.060032
Duplication	,	1
Triangles	,	12
Meshes	,	1
BottomLevels	,	1
TopLevelSize	,	64
BottomLevelsSize	,	4480
Index	,	0

FpsAggregator: 
FPS	,	1747	,	1830	,	1788	,	1770	,	1755	,	1699	,	1713	,	1704	,	1642	,	1676	,	1723	,	1661	,	1810	,	1878	,	1781	,	1719	,	1711	,	1640	,	1704	,	1717	,	1723	,	1826	,	1889	,	1978	,	2010	,	2077	,	2073	,	2165	,	2256	,	2258
UPS	,	59	,	60	,	61	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60
FrameTime	,	0.572476	,	0.546485	,	0.559538	,	0.565232	,	0.569892	,	0.588673	,	0.584005	,	0.586958	,	0.609066	,	0.596807	,	0.580707	,	0.602382	,	0.552664	,	0.532697	,	0.561512	,	0.581875	,	0.584648	,	0.609781	,	0.586912	,	0.58258	,	0.580616	,	0.547863	,	0.52949	,	0.505726	,	0.497714	,	0.481549	,	0.482472	,	0.461925	,	0.443298	,	0.443032
GigaRays/s	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0
Index	,	0	,	1	,	2	,	3	,	4	,	5	,	6	,	7	,	8	,	9	,	10	,	11	,	12	,	13	,	14	,	15	,	16	,	17	,	18	,	19	,	20	,	21	,	22	,	23	,	24	,	25	,	26	,	27	,	28	,	29


AccelerationStructure: 
Ray tracing acceleration structure building statistics: 
	Bottom Level Acceleration Structure: 
		Maximum [B]: 4480
		Minimum [B]: 4480
		Currently Allocated [B]: 4480
		Currently Created [num]: 1
		Current Average [B]: 4480
		Maximum Triangles [num]: 12
		Minimum Triangles [num]: 12
		Total Triangles [num]: 12
		Maximum Meshes [num]: 1
		Minimum Meshes [num]: 1
		Total Meshes [num]: 1
	Top Level Acceleration Structure: 
		Maximum [B]: 64
		Minimum [B]: 64
		Currently Allocated [B]: 64
		Total Allocated [B]: 64
		Total Created [num]: 1
		Average Allocated [B]: 64
		Maximum Instances [num]: 1
		Minimum Instances [num]: 1
		Total Instances [num]: 1
	Scratch Buffer: 
		Maximum [B]: 896
		Minimum [B]: 896
		Currently Allocated [B]: 896
		Total Allocated [B]: 896
		Total Created [num]: 1
		Average Allocated [B]: 896
ProfilingStack: 
[PROF] Profiling statistics: 
	Scopes entered : 685850
	Scopes exited : 685849
	Overhead per scope [ticks] : 102
	Threads entered : 3
	Threads exited : 0
	============================
		         Sum[Mt]   Parent[%]  Samples[num]       Avg[Mt]   Last/Curr[Mt]        Self[Mt]     Self[%] 	 Name

	main:

		   30442.747 Mt   100.000 %         1 x    30442.747 Mt    30442.747 Mt        0.497 Mt     0.002 % 	 /

		   30442.259 Mt    99.998 %         1 x    30442.260 Mt    30442.259 Mt      294.066 Mt     0.966 % 	 ./Main application loop

		       4.141 Mt     0.014 %         1 x        4.141 Mt        0.000 Mt        4.141 Mt   100.000 % 	 ../TexturedRLInitialization

		       1.312 Mt     0.004 %         1 x        1.312 Mt        0.000 Mt        1.312 Mt   100.000 % 	 ../ShadowMapRLInitialization

		       4.636 Mt     0.015 %         1 x        4.636 Mt        0.000 Mt        4.636 Mt   100.000 % 	 ../RasterResolveRLInitialization

		       3.770 Mt     0.012 %         1 x        3.770 Mt        0.000 Mt        3.770 Mt   100.000 % 	 ../ResolveRLInitialization

		   30009.653 Mt    98.579 %     79829 x        0.376 Mt        0.861 Mt      335.612 Mt     1.118 % 	 ../MainLoop

		     122.633 Mt     0.409 %      1801 x        0.068 Mt        0.079 Mt      122.374 Mt    99.789 % 	 .../Update

		       0.259 Mt     0.211 %         1 x        0.259 Mt        0.000 Mt        0.259 Mt   100.000 % 	 ..../GuiModelApply

		   29551.408 Mt    98.473 %     54925 x        0.538 Mt        0.780 Mt     1848.803 Mt     6.256 % 	 .../Render

		    3685.515 Mt    12.472 %     54925 x        0.067 Mt        0.173 Mt     3652.424 Mt    99.102 % 	 ..../Rasterization

		      18.460 Mt     0.501 %     54925 x        0.000 Mt        0.000 Mt       18.460 Mt   100.000 % 	 ...../DeferredRLPrep

		      14.630 Mt     0.397 %     54925 x        0.000 Mt        0.000 Mt       14.630 Mt   100.000 % 	 ...../ShadowMapRLPrep

		   24017.090 Mt    81.272 %     54925 x        0.437 Mt        0.463 Mt    24017.090 Mt   100.000 % 	 ..../Present

		       2.824 Mt     0.009 %         1 x        2.824 Mt        0.000 Mt        2.824 Mt   100.000 % 	 ../AmbientOcclusionRLInitialization

		       4.881 Mt     0.016 %         1 x        4.881 Mt        0.000 Mt        4.881 Mt   100.000 % 	 ../DeferredRLInitialization

		      51.390 Mt     0.169 %         1 x       51.390 Mt        0.000 Mt        2.364 Mt     4.601 % 	 ../RayTracingRLInitialization

		      49.026 Mt    95.399 %         1 x       49.026 Mt        0.000 Mt       49.026 Mt   100.000 % 	 .../PipelineBuild

		      65.586 Mt     0.215 %         1 x       65.586 Mt        0.000 Mt        0.178 Mt     0.272 % 	 ../Initialization

		      65.408 Mt    99.728 %         1 x       65.408 Mt        0.000 Mt        0.980 Mt     1.499 % 	 .../ScenePrep

		      22.684 Mt    34.681 %         1 x       22.684 Mt        0.000 Mt        6.534 Mt    28.805 % 	 ..../SceneLoad

		      10.688 Mt    47.116 %         1 x       10.688 Mt        0.000 Mt        2.660 Mt    24.883 % 	 ...../glTF-LoadScene

		       8.028 Mt    75.117 %         2 x        4.014 Mt        0.000 Mt        8.028 Mt   100.000 % 	 ....../glTF-LoadImageData

		       2.307 Mt    10.171 %         1 x        2.307 Mt        0.000 Mt        2.307 Mt   100.000 % 	 ...../glTF-CreateScene

		       3.155 Mt    13.908 %         1 x        3.155 Mt        0.000 Mt        3.155 Mt   100.000 % 	 ...../SceneDesc-CreateScene

		       3.845 Mt     5.878 %         1 x        3.845 Mt        0.000 Mt        0.000 Mt     0.010 % 	 ..../ShadowMapRLPrep

		       3.844 Mt    99.990 %         1 x        3.844 Mt        0.000 Mt        3.836 Mt    99.789 % 	 ...../PrepareForRendering

		       0.008 Mt     0.211 %         1 x        0.008 Mt        0.000 Mt        0.008 Mt   100.000 % 	 ....../PrepareDrawBundle

		       1.609 Mt     2.460 %         1 x        1.609 Mt        0.000 Mt        0.000 Mt     0.019 % 	 ..../TexturedRLPrep

		       1.609 Mt    99.981 %         1 x        1.609 Mt        0.000 Mt        1.604 Mt    99.739 % 	 ...../PrepareForRendering

		       0.001 Mt     0.056 %         1 x        0.001 Mt        0.000 Mt        0.001 Mt   100.000 % 	 ....../PrepareMaterials

		       0.003 Mt     0.205 %         1 x        0.003 Mt        0.000 Mt        0.003 Mt   100.000 % 	 ....../PrepareDrawBundle

		       1.409 Mt     2.154 %         1 x        1.409 Mt        0.000 Mt        0.001 Mt     0.035 % 	 ..../DeferredRLPrep

		       1.408 Mt    99.965 %         1 x        1.408 Mt        0.000 Mt        1.064 Mt    75.563 % 	 ...../PrepareForRendering

		       0.001 Mt     0.043 %         1 x        0.001 Mt        0.000 Mt        0.001 Mt   100.000 % 	 ....../PrepareMaterials

		       0.340 Mt    24.174 %         1 x        0.340 Mt        0.000 Mt        0.340 Mt   100.000 % 	 ....../BuildMaterialCache

		       0.003 Mt     0.220 %         1 x        0.003 Mt        0.000 Mt        0.003 Mt   100.000 % 	 ....../PrepareDrawBundle

		      24.123 Mt    36.881 %         1 x       24.123 Mt        0.000 Mt        2.705 Mt    11.215 % 	 ..../RayTracingRLPrep

		       0.044 Mt     0.184 %         1 x        0.044 Mt        0.000 Mt        0.044 Mt   100.000 % 	 ...../PrepareAccelerationStructures

		       0.001 Mt     0.003 %         1 x        0.001 Mt        0.000 Mt        0.001 Mt   100.000 % 	 ...../PrepareCache

		      13.307 Mt    55.164 %         1 x       13.307 Mt        0.000 Mt       13.307 Mt   100.000 % 	 ...../PipelineBuild

		       0.379 Mt     1.572 %         1 x        0.379 Mt        0.000 Mt        0.379 Mt   100.000 % 	 ...../BuildCache

		       6.226 Mt    25.807 %         1 x        6.226 Mt        0.000 Mt        0.001 Mt     0.008 % 	 ...../BuildAccelerationStructures

		       6.225 Mt    99.992 %         1 x        6.225 Mt        0.000 Mt        4.543 Mt    72.988 % 	 ....../AccelerationStructureBuild

		       0.880 Mt    14.132 %         1 x        0.880 Mt        0.000 Mt        0.880 Mt   100.000 % 	 ......./BuildBottomLevelAS

		       0.802 Mt    12.880 %         1 x        0.802 Mt        0.000 Mt        0.802 Mt   100.000 % 	 ......./BuildTopLevelAS

		       1.461 Mt     6.056 %         1 x        1.461 Mt        0.000 Mt        1.461 Mt   100.000 % 	 ...../BuildShaderTables

		      10.759 Mt    16.449 %         1 x       10.759 Mt        0.000 Mt       10.759 Mt   100.000 % 	 ..../GpuSidePrep

	WindowThread:

		   30440.567 Mt   100.000 %         1 x    30440.568 Mt    30440.567 Mt    30440.567 Mt   100.000 % 	 /

	GpuThread:

		   14557.779 Mt   100.000 %     54925 x        0.265 Mt        0.151 Mt      135.117 Mt     0.928 % 	 /

		       0.194 Mt     0.001 %         1 x        0.194 Mt        0.000 Mt        0.006 Mt     2.988 % 	 ./ScenePrep

		       0.188 Mt    97.012 %         1 x        0.188 Mt        0.000 Mt        0.001 Mt     0.323 % 	 ../AccelerationStructureBuild

		       0.127 Mt    67.750 %         1 x        0.127 Mt        0.000 Mt        0.127 Mt   100.000 % 	 .../BuildBottomLevelASGpu

		       0.060 Mt    31.926 %         1 x        0.060 Mt        0.000 Mt        0.060 Mt   100.000 % 	 .../BuildTopLevelASGpu

		   14326.258 Mt    98.410 %     54925 x        0.261 Mt        0.149 Mt      154.605 Mt     1.079 % 	 ./RasterizationGpu

		    4013.194 Mt    28.013 %     54925 x        0.073 Mt        0.020 Mt     4013.194 Mt   100.000 % 	 ../DeferredRLGpu

		     217.450 Mt     1.518 %     54925 x        0.004 Mt        0.004 Mt      217.450 Mt   100.000 % 	 ../ShadowMapRLGpu

		    7025.313 Mt    49.038 %     54925 x        0.128 Mt        0.104 Mt     7025.313 Mt   100.000 % 	 ../AmbientOcclusionRLGpu

		    2915.697 Mt    20.352 %     54925 x        0.053 Mt        0.020 Mt     2915.697 Mt   100.000 % 	 ../RasterResolveRLGpu

		      96.210 Mt     0.661 %     54925 x        0.002 Mt        0.002 Mt       96.210 Mt   100.000 % 	 ./Present


	============================


