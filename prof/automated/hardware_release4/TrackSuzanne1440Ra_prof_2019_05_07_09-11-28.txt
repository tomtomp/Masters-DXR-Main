Configuration: 
Command line parameters  = E:\programming\FIT-DIP\Masters-DXR-Main-git\build\Measurement\x64\Release\Measurement.exe --scene Suzanne/Suzanne.gltf --quality-preset Low --profile-automation --profile-camera-track Suzanne/Suzanne.trk --width 2560 --height 1440 --profile-output TrackSuzanne1440Ra 
Build type               = Release
Device name              = NVIDIA GeForce RTX 2080 Ti
Device memory MB         = 11049
Ray Tracing backend      = Hardware
Window width             = 2560
Window height            = 1421
Window title             = Direct3D App
Back-buffer count        = 3
Fill Triangles           = enabled
Triangles Font Clock-Wise= enabled
Fast Build AS            = disabled
Target FPS               = 0
Target UPS               = 60
Tearing                  = enabled
VSync                    = disabled
GUI rendering            = disabled
Debug layer              = disabled
GPU validation           = disabled
GPU profiling            = enabled
Render mode              = Rasterization
Loaded scene file        = Suzanne/Suzanne.gltf
Using testing scene      = disabled
Duplication              = 1
Duplication in cube      = disabled
Duplication offset       = 10
BLAS duplication         = disabled
Rays per pixel           = 5
Quality preset           = Low
Rasterization Quality    : 
  AO                     = enabled
  AO kernel size         = 4
  Shadows                = enabled
  Shadow map resolution  = 2048
  Shadow PCF kernel size = 0
  Reflections            = disabled
Ray Tracing Quality      : 
  AO                     = enabled
  AO rays                = 4
  AO kernel size         = 4
  Shadows                = enabled
  Shadow rays            = 1
  Shadow kernel size     = 4
  Reflections            = disabled

ProfilingAggregator: 
Render	,	1.2009	,	1.2621	,	1.3889	,	1.3204	,	1.2203	,	0.9752	,	1.0933	,	0.8031	,	0.9179	,	1.1026	,	1.1363	,	1.1335	,	1.0563	,	1.0606	,	1.0969	,	1.6208	,	1.5829	,	1.6938	,	2.0479	,	1.971	,	1.7095	,	1.0999	,	1.1275	,	1.0561
Update	,	0.0708	,	0.0505	,	0.0538	,	0.0469	,	0.0569	,	0.0424	,	0.0361	,	0.0409	,	0.0422	,	0.0554	,	0.0839	,	0.0832	,	0.0421	,	0.0613	,	0.0775	,	0.0584	,	0.0598	,	0.0874	,	0.0875	,	0.0717	,	0.0675	,	0.077	,	0.0432	,	0.0514
DeferredRLGpu	,	0.158752	,	0.188032	,	0.204	,	0.158464	,	0.100224	,	0.070048	,	0.054176	,	0.047616	,	0.064672	,	0.093216	,	0.108736	,	0.078784	,	0.05472	,	0.055648	,	0.08752	,	0.187808	,	0.226208	,	0.257152	,	0.401152	,	0.323648	,	0.276128	,	0.137568	,	0.083552	,	0.071328
Index	,	0	,	1	,	2	,	3	,	4	,	5	,	6	,	7	,	8	,	9	,	10	,	11	,	12	,	13	,	14	,	15	,	16	,	17	,	18	,	19	,	20	,	21	,	22	,	23

ProfilingBuildAggregator: 
BuildBottomLevelAS	,	0.9656
BuildBottomLevelASGpu	,	0.44896
BuildTopLevelAS	,	0.9544
BuildTopLevelASGpu	,	0.062176
Duplication	,	1
Triangles	,	3936
Meshes	,	1
BottomLevels	,	1
TopLevelSize	,	64
BottomLevelsSize	,	282112
Index	,	0

FpsAggregator: 
FPS	,	623	,	657	,	628	,	683	,	731	,	778	,	827	,	912	,	880	,	814	,	747	,	758	,	802	,	851	,	803	,	701	,	598	,	560	,	490	,	504	,	537	,	635	,	795	,	812
UPS	,	59	,	60	,	61	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60
FrameTime	,	1.6063	,	1.5222	,	1.59386	,	1.46467	,	1.36848	,	1.28618	,	1.21008	,	1.09693	,	1.13677	,	1.22861	,	1.33949	,	1.31967	,	1.24693	,	1.17593	,	1.24666	,	1.42838	,	1.67447	,	1.78868	,	2.04409	,	1.9844	,	1.86405	,	1.57485	,	1.25922	,	1.23188
GigaRays/s	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0
Index	,	0	,	1	,	2	,	3	,	4	,	5	,	6	,	7	,	8	,	9	,	10	,	11	,	12	,	13	,	14	,	15	,	16	,	17	,	18	,	19	,	20	,	21	,	22	,	23


AccelerationStructure: 
Ray tracing acceleration structure building statistics: 
	Bottom Level Acceleration Structure: 
		Maximum [B]: 282112
		Minimum [B]: 282112
		Currently Allocated [B]: 282112
		Currently Created [num]: 1
		Current Average [B]: 282112
		Maximum Triangles [num]: 3936
		Minimum Triangles [num]: 3936
		Total Triangles [num]: 3936
		Maximum Meshes [num]: 1
		Minimum Meshes [num]: 1
		Total Meshes [num]: 1
	Top Level Acceleration Structure: 
		Maximum [B]: 64
		Minimum [B]: 64
		Currently Allocated [B]: 64
		Total Allocated [B]: 64
		Total Created [num]: 1
		Average Allocated [B]: 64
		Maximum Instances [num]: 1
		Minimum Instances [num]: 1
		Total Instances [num]: 1
	Scratch Buffer: 
		Maximum [B]: 82944
		Minimum [B]: 82944
		Currently Allocated [B]: 82944
		Total Allocated [B]: 82944
		Total Created [num]: 1
		Average Allocated [B]: 82944
ProfilingStack: 
[PROF] Profiling statistics: 
	Scopes entered : 292876
	Scopes exited : 292875
	Overhead per scope [ticks] : 100.7
	Threads entered : 3
	Threads exited : 0
	============================
		         Sum[Mt]   Parent[%]  Samples[num]       Avg[Mt]   Last/Curr[Mt]        Self[Mt]     Self[%] 	 Name

	main:

		   24512.827 Mt   100.000 %         1 x    24512.827 Mt    24512.827 Mt        0.374 Mt     0.002 % 	 /

		   24512.463 Mt    99.999 %         1 x    24512.464 Mt    24512.463 Mt      267.331 Mt     1.091 % 	 ./Main application loop

		       4.253 Mt     0.017 %         1 x        4.253 Mt        0.000 Mt        4.253 Mt   100.000 % 	 ../TexturedRLInitialization

		       1.246 Mt     0.005 %         1 x        1.246 Mt        0.000 Mt        1.246 Mt   100.000 % 	 ../ShadowMapRLInitialization

		       4.868 Mt     0.020 %         1 x        4.868 Mt        0.000 Mt        4.868 Mt   100.000 % 	 ../RasterResolveRLInitialization

		       3.733 Mt     0.015 %         1 x        3.733 Mt        0.000 Mt        3.733 Mt   100.000 % 	 ../ResolveRLInitialization

		   24021.682 Mt    97.998 %    102982 x        0.233 Mt        1.379 Mt      351.346 Mt     1.463 % 	 ../MainLoop

		     172.365 Mt     0.718 %      1441 x        0.120 Mt        0.080 Mt      172.099 Mt    99.846 % 	 .../Update

		       0.265 Mt     0.154 %         1 x        0.265 Mt        0.000 Mt        0.265 Mt   100.000 % 	 ..../GuiModelApply

		   23497.971 Mt    97.820 %     17128 x        1.372 Mt        1.296 Mt     1312.505 Mt     5.586 % 	 .../Render

		    2189.082 Mt     9.316 %     17128 x        0.128 Mt        0.200 Mt     2167.783 Mt    99.027 % 	 ..../Rasterization

		      12.867 Mt     0.588 %     17128 x        0.001 Mt        0.001 Mt       12.867 Mt   100.000 % 	 ...../DeferredRLPrep

		       8.432 Mt     0.385 %     17128 x        0.000 Mt        0.001 Mt        8.432 Mt   100.000 % 	 ...../ShadowMapRLPrep

		   19996.383 Mt    85.098 %     17128 x        1.167 Mt        0.983 Mt    19996.383 Mt   100.000 % 	 ..../Present

		       2.766 Mt     0.011 %         1 x        2.766 Mt        0.000 Mt        2.766 Mt   100.000 % 	 ../AmbientOcclusionRLInitialization

		       5.670 Mt     0.023 %         1 x        5.670 Mt        0.000 Mt        5.670 Mt   100.000 % 	 ../DeferredRLInitialization

		      54.036 Mt     0.220 %         1 x       54.036 Mt        0.000 Mt        2.290 Mt     4.238 % 	 ../RayTracingRLInitialization

		      51.746 Mt    95.762 %         1 x       51.746 Mt        0.000 Mt       51.746 Mt   100.000 % 	 .../PipelineBuild

		     146.877 Mt     0.599 %         1 x      146.877 Mt        0.000 Mt        0.191 Mt     0.130 % 	 ../Initialization

		     146.686 Mt    99.870 %         1 x      146.686 Mt        0.000 Mt        1.460 Mt     0.996 % 	 .../ScenePrep

		      92.740 Mt    63.224 %         1 x       92.740 Mt        0.000 Mt       20.264 Mt    21.851 % 	 ..../SceneLoad

		      62.999 Mt    67.931 %         1 x       62.999 Mt        0.000 Mt        8.962 Mt    14.226 % 	 ...../glTF-LoadScene

		      54.037 Mt    85.774 %         2 x       27.018 Mt        0.000 Mt       54.037 Mt   100.000 % 	 ....../glTF-LoadImageData

		       7.274 Mt     7.843 %         1 x        7.274 Mt        0.000 Mt        7.274 Mt   100.000 % 	 ...../glTF-CreateScene

		       2.203 Mt     2.376 %         1 x        2.203 Mt        0.000 Mt        2.203 Mt   100.000 % 	 ...../SceneDesc-CreateScene

		       0.378 Mt     0.258 %         1 x        0.378 Mt        0.000 Mt        0.001 Mt     0.238 % 	 ..../ShadowMapRLPrep

		       0.378 Mt    99.762 %         1 x        0.378 Mt        0.000 Mt        0.360 Mt    95.444 % 	 ...../PrepareForRendering

		       0.017 Mt     4.556 %         1 x        0.017 Mt        0.000 Mt        0.017 Mt   100.000 % 	 ....../PrepareDrawBundle

		       2.869 Mt     1.956 %         1 x        2.869 Mt        0.000 Mt        0.001 Mt     0.035 % 	 ..../TexturedRLPrep

		       2.868 Mt    99.965 %         1 x        2.868 Mt        0.000 Mt        2.860 Mt    99.707 % 	 ...../PrepareForRendering

		       0.002 Mt     0.066 %         1 x        0.002 Mt        0.000 Mt        0.002 Mt   100.000 % 	 ....../PrepareMaterials

		       0.006 Mt     0.227 %         1 x        0.006 Mt        0.000 Mt        0.006 Mt   100.000 % 	 ....../PrepareDrawBundle

		       1.988 Mt     1.355 %         1 x        1.988 Mt        0.000 Mt        0.001 Mt     0.050 % 	 ..../DeferredRLPrep

		       1.987 Mt    99.950 %         1 x        1.987 Mt        0.000 Mt        1.601 Mt    80.567 % 	 ...../PrepareForRendering

		       0.001 Mt     0.055 %         1 x        0.001 Mt        0.000 Mt        0.001 Mt   100.000 % 	 ....../PrepareMaterials

		       0.380 Mt    19.101 %         1 x        0.380 Mt        0.000 Mt        0.380 Mt   100.000 % 	 ....../BuildMaterialCache

		       0.005 Mt     0.277 %         1 x        0.005 Mt        0.000 Mt        0.005 Mt   100.000 % 	 ....../PrepareDrawBundle

		      32.114 Mt    21.893 %         1 x       32.114 Mt        0.000 Mt        3.239 Mt    10.087 % 	 ..../RayTracingRLPrep

		       0.131 Mt     0.408 %         1 x        0.131 Mt        0.000 Mt        0.131 Mt   100.000 % 	 ...../PrepareAccelerationStructures

		       0.002 Mt     0.005 %         1 x        0.002 Mt        0.000 Mt        0.002 Mt   100.000 % 	 ...../PrepareCache

		      21.991 Mt    68.480 %         1 x       21.991 Mt        0.000 Mt       21.991 Mt   100.000 % 	 ...../PipelineBuild

		       0.390 Mt     1.213 %         1 x        0.390 Mt        0.000 Mt        0.390 Mt   100.000 % 	 ...../BuildCache

		       4.763 Mt    14.832 %         1 x        4.763 Mt        0.000 Mt        0.001 Mt     0.013 % 	 ...../BuildAccelerationStructures

		       4.763 Mt    99.987 %         1 x        4.763 Mt        0.000 Mt        2.843 Mt    59.685 % 	 ....../AccelerationStructureBuild

		       0.966 Mt    20.275 %         1 x        0.966 Mt        0.000 Mt        0.966 Mt   100.000 % 	 ......./BuildBottomLevelAS

		       0.954 Mt    20.040 %         1 x        0.954 Mt        0.000 Mt        0.954 Mt   100.000 % 	 ......./BuildTopLevelAS

		       1.598 Mt     4.975 %         1 x        1.598 Mt        0.000 Mt        1.598 Mt   100.000 % 	 ...../BuildShaderTables

		      15.136 Mt    10.318 %         1 x       15.136 Mt        0.000 Mt       15.136 Mt   100.000 % 	 ..../GpuSidePrep

	WindowThread:

		   24510.840 Mt   100.000 %         1 x    24510.840 Mt    24510.840 Mt    24510.840 Mt   100.000 % 	 /

	GpuThread:

		   12581.927 Mt   100.000 %     17128 x        0.735 Mt        0.632 Mt      132.770 Mt     1.055 % 	 /

		       0.517 Mt     0.004 %         1 x        0.517 Mt        0.000 Mt        0.005 Mt     0.953 % 	 ./ScenePrep

		       0.512 Mt    99.047 %         1 x        0.512 Mt        0.000 Mt        0.001 Mt     0.175 % 	 ../AccelerationStructureBuild

		       0.449 Mt    87.682 %         1 x        0.449 Mt        0.000 Mt        0.449 Mt   100.000 % 	 .../BuildBottomLevelASGpu

		       0.062 Mt    12.143 %         1 x        0.062 Mt        0.000 Mt        0.062 Mt   100.000 % 	 .../BuildTopLevelASGpu

		   12405.689 Mt    98.599 %     17128 x        0.724 Mt        0.629 Mt       54.331 Mt     0.438 % 	 ./RasterizationGpu

		    2252.484 Mt    18.157 %     17128 x        0.132 Mt        0.072 Mt     2252.484 Mt   100.000 % 	 ../DeferredRLGpu

		      94.478 Mt     0.762 %     17128 x        0.006 Mt        0.005 Mt       94.478 Mt   100.000 % 	 ../ShadowMapRLGpu

		    8050.224 Mt    64.891 %     17128 x        0.470 Mt        0.474 Mt     8050.224 Mt   100.000 % 	 ../AmbientOcclusionRLGpu

		    1954.172 Mt    15.752 %     17128 x        0.114 Mt        0.077 Mt     1954.172 Mt   100.000 % 	 ../RasterResolveRLGpu

		      42.951 Mt     0.341 %     17128 x        0.003 Mt        0.002 Mt       42.951 Mt   100.000 % 	 ./Present


	============================


