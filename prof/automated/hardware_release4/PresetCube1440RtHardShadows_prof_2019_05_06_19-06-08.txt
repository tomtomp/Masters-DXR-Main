Configuration: 
Command line parameters  = E:\programming\FIT-DIP\Masters-DXR-Main-git\build\Measurement\x64\Release\Measurement.exe --scene Cube/Cube.gltf --profile-automation --profile-camera-track Cube/Cube.trk --rt-mode --width 2560 --height 1440 --profile-output PresetCube1440RtHardShadows --quality-preset HardShadows 
Build type               = Release
Device name              = NVIDIA GeForce RTX 2080 Ti
Device memory MB         = 11049
Ray Tracing backend      = Hardware
Window width             = 2560
Window height            = 1421
Window title             = Direct3D App
Back-buffer count        = 3
Fill Triangles           = enabled
Triangles Font Clock-Wise= enabled
Fast Build AS            = disabled
Target FPS               = 0
Target UPS               = 60
Tearing                  = enabled
VSync                    = disabled
GUI rendering            = disabled
Debug layer              = disabled
GPU validation           = disabled
GPU profiling            = enabled
Render mode              = Ray Tracing
Loaded scene file        = Cube/Cube.gltf
Using testing scene      = disabled
Duplication              = 1
Duplication in cube      = disabled
Duplication offset       = 10
BLAS duplication         = disabled
Rays per pixel           = 1
Quality preset           = HardShadows
Rasterization Quality    : 
  AO                     = disabled
  AO kernel size         = 1
  Shadows                = enabled
  Shadow map resolution  = 2048
  Shadow PCF kernel size = 0
  Reflections            = disabled
Ray Tracing Quality      : 
  AO                     = disabled
  AO rays                = 0
  AO kernel size         = 4
  Shadows                = enabled
  Shadow rays            = 1
  Shadow kernel size     = 4
  Reflections            = disabled

ProfilingAggregator: 
Render	,	2.67	,	2.7517	,	2.7923	,	2.7957	,	3.3384	,	3.3935	,	3.3908	,	3.413	,	3.213	,	3.1744	,	3.0155	,	3.0102	,	3.255	,	2.7988	,	2.8835	,	3.0109	,	3.1246	,	3.3043	,	3.4117	,	3.2985	,	3.1345	,	2.8311	,	2.5825	,	2.5542	,	2.517	,	2.365	,	2.315	,	2.3414	,	2.2576	,	2.1793
Update	,	0.0789	,	0.0806	,	0.0868	,	0.0832	,	0.0781	,	0.0803	,	0.0833	,	0.0826	,	0.0845	,	0.0874	,	0.0803	,	0.0819	,	0.0798	,	0.0822	,	0.0826	,	0.0747	,	0.0817	,	0.0808	,	0.081	,	0.0864	,	0.0801	,	0.0814	,	0.0807	,	0.0806	,	0.0757	,	0.0725	,	0.081	,	0.0779	,	0.0812	,	0.0883
RayTracing	,	0.2035	,	0.2045	,	0.1757	,	0.1731	,	0.1844	,	0.1725	,	0.1751	,	0.1775	,	0.1877	,	0.1773	,	0.1761	,	0.1703	,	0.6979	,	0.1713	,	0.1711	,	0.1746	,	0.1747	,	0.175	,	0.1737	,	0.1752	,	0.1754	,	0.182	,	0.1706	,	0.1746	,	0.1696	,	0.1688	,	0.1683	,	0.1762	,	0.1713	,	0.1784
RayTracingGpu	,	1.80397	,	1.96867	,	2.028	,	2.03808	,	2.1833	,	2.5583	,	2.50118	,	2.3415	,	2.39427	,	2.31168	,	2.18838	,	2.24614	,	2.04595	,	1.98211	,	2.0641	,	2.18938	,	2.34019	,	2.48966	,	2.56538	,	2.57347	,	2.3753	,	2.03645	,	1.93222	,	1.8031	,	1.72131	,	1.63792	,	1.5657	,	1.52326	,	1.50598	,	1.50189
DeferredRLGpu	,	0.181344	,	0.242208	,	0.257312	,	0.2624	,	0.277152	,	0.337088	,	0.312	,	0.354464	,	0.415328	,	0.373024	,	0.318976	,	0.351008	,	0.281088	,	0.2528	,	0.28784	,	0.336416	,	0.395104	,	0.466304	,	0.488	,	0.510752	,	0.416256	,	0.288096	,	0.235808	,	0.183968	,	0.15232	,	0.119232	,	0.090752	,	0.07472	,	0.063872	,	0.06416
RayTracingRLGpu	,	0.195264	,	0.232736	,	0.241056	,	0.241152	,	0.243712	,	0.27504	,	0.263968	,	0.285856	,	0.313344	,	0.29472	,	0.266592	,	0.282656	,	0.246784	,	0.231264	,	0.25104	,	0.275392	,	0.305024	,	0.33744	,	0.349248	,	0.347264	,	0.310944	,	0.243392	,	0.21744	,	0.19264	,	0.176416	,	0.15824	,	0.140704	,	0.131104	,	0.12608	,	0.126464
ResolveRLGpu	,	1.42538	,	1.49162	,	1.52752	,	1.53242	,	1.65984	,	1.94394	,	1.92227	,	1.6985	,	1.6625	,	1.64163	,	1.60086	,	1.6103	,	1.51523	,	1.49594	,	1.52218	,	1.57434	,	1.63802	,	1.68394	,	1.7241	,	1.71229	,	1.64586	,	1.5017	,	1.47574	,	1.42442	,	1.39046	,	1.35821	,	1.33149	,	1.31581	,	1.31398	,	1.30822
Index	,	0	,	1	,	2	,	3	,	4	,	5	,	6	,	7	,	8	,	9	,	10	,	11	,	12	,	13	,	14	,	15	,	16	,	17	,	18	,	19	,	20	,	21	,	22	,	23	,	24	,	25	,	26	,	27	,	28	,	29

ProfilingBuildAggregator: 
BuildBottomLevelAS	,	0.7881
BuildBottomLevelASGpu	,	0.12816
BuildTopLevelAS	,	0.7667
BuildTopLevelASGpu	,	0.060256
Duplication	,	1
Triangles	,	12
Meshes	,	1
BottomLevels	,	1
TopLevelSize	,	64
BottomLevelsSize	,	4480
Index	,	0

FpsAggregator: 
FPS	,	306	,	358	,	342	,	343	,	337	,	324	,	324	,	326	,	310	,	301	,	330	,	323	,	327	,	348	,	342	,	329	,	314	,	299	,	287	,	287	,	290	,	338	,	347	,	363	,	383	,	388	,	402	,	413	,	413	,	418
UPS	,	59	,	60	,	61	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	61	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	61	,	60	,	60	,	60	,	60	,	60
FrameTime	,	3.26953	,	2.79621	,	2.92924	,	2.91873	,	2.97663	,	3.09071	,	3.08839	,	3.06894	,	3.23521	,	3.32808	,	3.03291	,	3.10293	,	3.06671	,	2.87385	,	2.92832	,	3.04385	,	3.19175	,	3.35529	,	3.48456	,	3.49017	,	3.4586	,	2.95975	,	2.88297	,	2.75769	,	2.61286	,	2.57913	,	2.48918	,	2.42439	,	2.42205	,	2.39396
GigaRays/s	,	1.11262	,	1.30096	,	1.24188	,	1.24635	,	1.22211	,	1.177	,	1.17788	,	1.18535	,	1.12443	,	1.09305	,	1.19943	,	1.17236	,	1.18621	,	1.26581	,	1.24227	,	1.19512	,	1.13974	,	1.08419	,	1.04397	,	1.04229	,	1.0518	,	1.22908	,	1.26181	,	1.31913	,	1.39225	,	1.41046	,	1.46143	,	1.50048	,	1.50194	,	1.51956
Index	,	0	,	1	,	2	,	3	,	4	,	5	,	6	,	7	,	8	,	9	,	10	,	11	,	12	,	13	,	14	,	15	,	16	,	17	,	18	,	19	,	20	,	21	,	22	,	23	,	24	,	25	,	26	,	27	,	28	,	29


AccelerationStructure: 
Ray tracing acceleration structure building statistics: 
	Bottom Level Acceleration Structure: 
		Maximum [B]: 4480
		Minimum [B]: 4480
		Currently Allocated [B]: 4480
		Currently Created [num]: 1
		Current Average [B]: 4480
		Maximum Triangles [num]: 12
		Minimum Triangles [num]: 12
		Total Triangles [num]: 12
		Maximum Meshes [num]: 1
		Minimum Meshes [num]: 1
		Total Meshes [num]: 1
	Top Level Acceleration Structure: 
		Maximum [B]: 64
		Minimum [B]: 64
		Currently Allocated [B]: 64
		Total Allocated [B]: 64
		Total Created [num]: 1
		Average Allocated [B]: 64
		Maximum Instances [num]: 1
		Minimum Instances [num]: 1
		Total Instances [num]: 1
	Scratch Buffer: 
		Maximum [B]: 896
		Minimum [B]: 896
		Currently Allocated [B]: 896
		Total Allocated [B]: 896
		Total Created [num]: 1
		Average Allocated [B]: 896
ProfilingStack: 
[PROF] Profiling statistics: 
	Scopes entered : 235432
	Scopes exited : 235431
	Overhead per scope [ticks] : 100.6
	Threads entered : 3
	Threads exited : 0
	============================
		         Sum[Mt]   Parent[%]  Samples[num]       Avg[Mt]   Last/Curr[Mt]        Self[Mt]     Self[%] 	 Name

	main:

		   30453.680 Mt   100.000 %         1 x    30453.680 Mt    30453.679 Mt        0.391 Mt     0.001 % 	 /

		   30453.305 Mt    99.999 %         1 x    30453.306 Mt    30453.304 Mt      260.030 Mt     0.854 % 	 ./Main application loop

		       2.073 Mt     0.007 %         1 x        2.073 Mt        0.000 Mt        2.073 Mt   100.000 % 	 ../TexturedRLInitialization

		       1.209 Mt     0.004 %         1 x        1.209 Mt        0.000 Mt        1.209 Mt   100.000 % 	 ../ShadowMapRLInitialization

		       4.431 Mt     0.015 %         1 x        4.431 Mt        0.000 Mt        4.431 Mt   100.000 % 	 ../RasterResolveRLInitialization

		       3.689 Mt     0.012 %         1 x        3.689 Mt        0.000 Mt        3.689 Mt   100.000 % 	 ../ResolveRLInitialization

		   30058.545 Mt    98.704 %    100802 x        0.298 Mt        2.248 Mt      485.516 Mt     1.615 % 	 ../MainLoop

		     231.224 Mt     0.769 %      1803 x        0.128 Mt        0.072 Mt      230.605 Mt    99.732 % 	 .../Update

		       0.620 Mt     0.268 %         1 x        0.620 Mt        0.000 Mt        0.620 Mt   100.000 % 	 ..../GuiModelApply

		   29341.804 Mt    97.616 %     10214 x        2.873 Mt        2.172 Mt     1129.047 Mt     3.848 % 	 .../Render

		    1790.220 Mt     6.101 %     10214 x        0.175 Mt        0.171 Mt       69.106 Mt     3.860 % 	 ..../RayTracing

		     803.952 Mt    44.908 %     10214 x        0.079 Mt        0.076 Mt      792.169 Mt    98.534 % 	 ...../Deferred

		      11.784 Mt     1.466 %     10214 x        0.001 Mt        0.001 Mt       11.784 Mt   100.000 % 	 ....../DeferredRLPrep

		     631.826 Mt    35.293 %     10214 x        0.062 Mt        0.065 Mt      625.920 Mt    99.065 % 	 ...../RayCasting

		       5.907 Mt     0.935 %     10214 x        0.001 Mt        0.001 Mt        5.907 Mt   100.000 % 	 ....../RayTracingRLPrep

		     285.335 Mt    15.939 %     10214 x        0.028 Mt        0.024 Mt      285.335 Mt   100.000 % 	 ...../Resolve

		   26422.538 Mt    90.051 %     10214 x        2.587 Mt        1.872 Mt    26422.538 Mt   100.000 % 	 ..../Present

		       2.569 Mt     0.008 %         1 x        2.569 Mt        0.000 Mt        2.569 Mt   100.000 % 	 ../AmbientOcclusionRLInitialization

		       4.739 Mt     0.016 %         1 x        4.739 Mt        0.000 Mt        4.739 Mt   100.000 % 	 ../DeferredRLInitialization

		      50.264 Mt     0.165 %         1 x       50.264 Mt        0.000 Mt        2.388 Mt     4.752 % 	 ../RayTracingRLInitialization

		      47.875 Mt    95.248 %         1 x       47.875 Mt        0.000 Mt       47.875 Mt   100.000 % 	 .../PipelineBuild

		      65.756 Mt     0.216 %         1 x       65.756 Mt        0.000 Mt        0.183 Mt     0.278 % 	 ../Initialization

		      65.573 Mt    99.722 %         1 x       65.573 Mt        0.000 Mt        0.941 Mt     1.435 % 	 .../ScenePrep

		      22.491 Mt    34.300 %         1 x       22.491 Mt        0.000 Mt        6.087 Mt    27.066 % 	 ..../SceneLoad

		      10.732 Mt    47.716 %         1 x       10.732 Mt        0.000 Mt        2.793 Mt    26.030 % 	 ...../glTF-LoadScene

		       7.939 Mt    73.970 %         2 x        3.969 Mt        0.000 Mt        7.939 Mt   100.000 % 	 ....../glTF-LoadImageData

		       2.224 Mt     9.888 %         1 x        2.224 Mt        0.000 Mt        2.224 Mt   100.000 % 	 ...../glTF-CreateScene

		       3.448 Mt    15.331 %         1 x        3.448 Mt        0.000 Mt        3.448 Mt   100.000 % 	 ...../SceneDesc-CreateScene

		       3.624 Mt     5.527 %         1 x        3.624 Mt        0.000 Mt        0.000 Mt     0.011 % 	 ..../ShadowMapRLPrep

		       3.624 Mt    99.989 %         1 x        3.624 Mt        0.000 Mt        3.616 Mt    99.788 % 	 ...../PrepareForRendering

		       0.008 Mt     0.212 %         1 x        0.008 Mt        0.000 Mt        0.008 Mt   100.000 % 	 ....../PrepareDrawBundle

		       1.475 Mt     2.250 %         1 x        1.475 Mt        0.000 Mt        0.000 Mt     0.027 % 	 ..../TexturedRLPrep

		       1.475 Mt    99.973 %         1 x        1.475 Mt        0.000 Mt        1.471 Mt    99.715 % 	 ...../PrepareForRendering

		       0.001 Mt     0.054 %         1 x        0.001 Mt        0.000 Mt        0.001 Mt   100.000 % 	 ....../PrepareMaterials

		       0.003 Mt     0.231 %         1 x        0.003 Mt        0.000 Mt        0.003 Mt   100.000 % 	 ....../PrepareDrawBundle

		       1.443 Mt     2.201 %         1 x        1.443 Mt        0.000 Mt        0.000 Mt     0.035 % 	 ..../DeferredRLPrep

		       1.443 Mt    99.965 %         1 x        1.443 Mt        0.000 Mt        1.138 Mt    78.847 % 	 ...../PrepareForRendering

		       0.001 Mt     0.035 %         1 x        0.001 Mt        0.000 Mt        0.001 Mt   100.000 % 	 ....../PrepareMaterials

		       0.302 Mt    20.911 %         1 x        0.302 Mt        0.000 Mt        0.302 Mt   100.000 % 	 ....../BuildMaterialCache

		       0.003 Mt     0.208 %         1 x        0.003 Mt        0.000 Mt        0.003 Mt   100.000 % 	 ....../PrepareDrawBundle

		      24.469 Mt    37.315 %         1 x       24.469 Mt        0.000 Mt        2.504 Mt    10.233 % 	 ..../RayTracingRLPrep

		       0.063 Mt     0.257 %         1 x        0.063 Mt        0.000 Mt        0.063 Mt   100.000 % 	 ...../PrepareAccelerationStructures

		       0.001 Mt     0.003 %         1 x        0.001 Mt        0.000 Mt        0.001 Mt   100.000 % 	 ...../PrepareCache

		      13.015 Mt    53.192 %         1 x       13.015 Mt        0.000 Mt       13.015 Mt   100.000 % 	 ...../PipelineBuild

		       0.330 Mt     1.347 %         1 x        0.330 Mt        0.000 Mt        0.330 Mt   100.000 % 	 ...../BuildCache

		       3.894 Mt    15.913 %         1 x        3.894 Mt        0.000 Mt        0.001 Mt     0.013 % 	 ...../BuildAccelerationStructures

		       3.893 Mt    99.987 %         1 x        3.893 Mt        0.000 Mt        2.338 Mt    60.065 % 	 ....../AccelerationStructureBuild

		       0.788 Mt    20.242 %         1 x        0.788 Mt        0.000 Mt        0.788 Mt   100.000 % 	 ......./BuildBottomLevelAS

		       0.767 Mt    19.693 %         1 x        0.767 Mt        0.000 Mt        0.767 Mt   100.000 % 	 ......./BuildTopLevelAS

		       4.662 Mt    19.054 %         1 x        4.662 Mt        0.000 Mt        4.662 Mt   100.000 % 	 ...../BuildShaderTables

		      11.129 Mt    16.972 %         1 x       11.129 Mt        0.000 Mt       11.129 Mt   100.000 % 	 ..../GpuSidePrep

	WindowThread:

		   30451.900 Mt   100.000 %         1 x    30451.901 Mt    30451.900 Mt    30451.900 Mt   100.000 % 	 /

	GpuThread:

		   21185.751 Mt   100.000 %     10214 x        2.074 Mt        1.499 Mt      136.964 Mt     0.646 % 	 /

		       0.194 Mt     0.001 %         1 x        0.194 Mt        0.000 Mt        0.005 Mt     2.637 % 	 ./ScenePrep

		       0.189 Mt    97.363 %         1 x        0.189 Mt        0.000 Mt        0.001 Mt     0.339 % 	 ../AccelerationStructureBuild

		       0.128 Mt    67.789 %         1 x        0.128 Mt        0.000 Mt        0.128 Mt   100.000 % 	 .../BuildBottomLevelASGpu

		       0.060 Mt    31.872 %         1 x        0.060 Mt        0.000 Mt        0.060 Mt   100.000 % 	 .../BuildTopLevelASGpu

		   20927.222 Mt    98.780 %     10214 x        2.049 Mt        1.496 Mt       27.906 Mt     0.133 % 	 ./RayTracingGpu

		    2735.232 Mt    13.070 %     10214 x        0.268 Mt        0.064 Mt     2735.232 Mt   100.000 % 	 ../DeferredRLGpu

		    2425.614 Mt    11.591 %     10214 x        0.237 Mt        0.123 Mt     2425.614 Mt   100.000 % 	 ../RayTracingRLGpu

		   15738.470 Mt    75.206 %     10214 x        1.541 Mt        1.307 Mt    15738.470 Mt   100.000 % 	 ../ResolveRLGpu

		     121.370 Mt     0.573 %     10214 x        0.012 Mt        0.002 Mt      121.370 Mt   100.000 % 	 ./Present


	============================


