Configuration: 
Command line parameters  = E:\programming\FIT-DIP\Masters-DXR-Main-git\build\Measurement\x64\Release\Measurement.exe --scene Suzanne/Suzanne.gltf --profile-automation --profile-camera-track Suzanne/Suzanne.trk --rt-mode --width 2560 --height 1440 --profile-output PresetSuzanne1440RtSoftShadows8 --quality-preset SoftShadows8 
Build type               = Release
Device name              = NVIDIA GeForce RTX 2080 Ti
Device memory MB         = 11049
Ray Tracing backend      = Hardware
Window width             = 2560
Window height            = 1421
Window title             = Direct3D App
Back-buffer count        = 3
Fill Triangles           = enabled
Triangles Font Clock-Wise= enabled
Fast Build AS            = disabled
Target FPS               = 0
Target UPS               = 60
Tearing                  = enabled
VSync                    = disabled
GUI rendering            = disabled
Debug layer              = disabled
GPU validation           = disabled
GPU profiling            = enabled
Render mode              = Ray Tracing
Loaded scene file        = Suzanne/Suzanne.gltf
Using testing scene      = disabled
Duplication              = 1
Duplication in cube      = disabled
Duplication offset       = 10
BLAS duplication         = disabled
Rays per pixel           = 8
Quality preset           = SoftShadows8
Rasterization Quality    : 
  AO                     = disabled
  AO kernel size         = 1
  Shadows                = enabled
  Shadow map resolution  = 8192
  Shadow PCF kernel size = 8
  Reflections            = disabled
Ray Tracing Quality      : 
  AO                     = disabled
  AO rays                = 0
  AO kernel size         = 8
  Shadows                = enabled
  Shadow rays            = 8
  Shadow kernel size     = 8
  Reflections            = disabled

ProfilingAggregator: 
Render	,	5.7513	,	6.0936	,	6.7742	,	6.4989	,	6.1353	,	5.7143	,	5.3817	,	5.3534	,	5.2692	,	5.5501	,	6.2147	,	5.932	,	5.829	,	5.382	,	5.5674	,	6.2429	,	6.8041	,	7.5712	,	9.5017	,	8.3496	,	6.6368	,	5.7359	,	5.1549	,	5.8308
Update	,	0.072	,	0.075	,	0.0737	,	0.0762	,	0.0781	,	0.0787	,	0.0768	,	0.0693	,	0.0826	,	0.0774	,	0.0779	,	0.0765	,	0.078	,	0.0765	,	0.0768	,	0.0785	,	0.0773	,	0.079	,	0.0758	,	0.0762	,	0.0758	,	0.0809	,	0.0701	,	0.068
RayTracing	,	0.1782	,	0.1721	,	0.179	,	0.1728	,	0.1781	,	0.1749	,	0.1771	,	0.1742	,	0.1711	,	0.176	,	0.1763	,	0.1755	,	0.1769	,	0.1749	,	0.1721	,	0.2073	,	0.2025	,	0.2036	,	0.2008	,	0.1743	,	0.1775	,	0.1731	,	0.1523	,	0.1764
RayTracingGpu	,	4.92912	,	5.26246	,	5.56986	,	5.65165	,	5.05747	,	4.83312	,	4.52128	,	4.49238	,	4.58198	,	4.8199	,	5.03197	,	5.06874	,	4.73267	,	4.5416	,	4.74589	,	5.43437	,	5.95834	,	6.63357	,	8.27216	,	7.43203	,	5.7919	,	4.88323	,	4.61142	,	4.68403
DeferredRLGpu	,	0.140608	,	0.187584	,	0.199584	,	0.154464	,	0.101088	,	0.070592	,	0.053632	,	0.04592	,	0.063072	,	0.093504	,	0.10592	,	0.079008	,	0.058816	,	0.056288	,	0.089568	,	0.186912	,	0.228768	,	0.264096	,	0.407552	,	0.318016	,	0.266208	,	0.13184	,	0.080896	,	0.078016
RayTracingRLGpu	,	0.481536	,	0.718912	,	0.87136	,	0.740288	,	0.458368	,	0.2784	,	0.213376	,	0.184416	,	0.255648	,	0.430112	,	0.520864	,	0.364576	,	0.223264	,	0.223232	,	0.370432	,	0.83312	,	1.2439	,	1.81683	,	2.97107	,	2.21386	,	1.08739	,	0.453984	,	0.268512	,	0.237728
ResolveRLGpu	,	4.30502	,	4.35386	,	4.49731	,	4.7543	,	4.49632	,	4.4825	,	4.25139	,	4.2593	,	4.26163	,	4.29443	,	4.40288	,	4.62349	,	4.44874	,	4.25939	,	4.28416	,	4.41226	,	4.48355	,	4.55066	,	4.89178	,	4.89818	,	4.43613	,	4.2953	,	4.25946	,	4.36694
Index	,	0	,	1	,	2	,	3	,	4	,	5	,	6	,	7	,	8	,	9	,	10	,	11	,	12	,	13	,	14	,	15	,	16	,	17	,	18	,	19	,	20	,	21	,	22	,	23

ProfilingBuildAggregator: 
BuildBottomLevelAS	,	0.8032
BuildBottomLevelASGpu	,	0.440576
BuildTopLevelAS	,	0.8009
BuildTopLevelASGpu	,	0.062304
Duplication	,	1
Triangles	,	3936
Meshes	,	1
BottomLevels	,	1
TopLevelSize	,	64
BottomLevelsSize	,	282112
Index	,	0

FpsAggregator: 
FPS	,	135	,	159	,	155	,	154	,	164	,	175	,	179	,	180	,	179	,	178	,	169	,	171	,	179	,	179	,	178	,	164	,	147	,	136	,	117	,	118	,	134	,	158	,	177	,	180
UPS	,	59	,	61	,	60	,	60	,	60	,	60	,	61	,	60	,	60	,	60	,	60	,	60	,	61	,	60	,	60	,	60	,	60	,	60	,	61	,	60	,	60	,	61	,	60	,	60
FrameTime	,	7.44377	,	6.32364	,	6.48472	,	6.52016	,	6.10319	,	5.71884	,	5.61377	,	5.56482	,	5.61534	,	5.61878	,	5.94376	,	5.85049	,	5.61473	,	5.61175	,	5.63788	,	6.1175	,	6.80619	,	7.36123	,	8.59162	,	8.51278	,	7.50529	,	6.34069	,	5.67856	,	5.58683
GigaRays/s	,	3.90959	,	4.60211	,	4.48779	,	4.4634	,	4.76834	,	5.08881	,	5.18405	,	5.22966	,	5.1826	,	5.17943	,	4.89624	,	4.97429	,	5.18317	,	5.18592	,	5.16188	,	4.75718	,	4.27583	,	3.95342	,	3.38726	,	3.41863	,	3.87754	,	4.58973	,	5.1249	,	5.20905
Index	,	0	,	1	,	2	,	3	,	4	,	5	,	6	,	7	,	8	,	9	,	10	,	11	,	12	,	13	,	14	,	15	,	16	,	17	,	18	,	19	,	20	,	21	,	22	,	23


AccelerationStructure: 
Ray tracing acceleration structure building statistics: 
	Bottom Level Acceleration Structure: 
		Maximum [B]: 282112
		Minimum [B]: 282112
		Currently Allocated [B]: 282112
		Currently Created [num]: 1
		Current Average [B]: 282112
		Maximum Triangles [num]: 3936
		Minimum Triangles [num]: 3936
		Total Triangles [num]: 3936
		Maximum Meshes [num]: 1
		Minimum Meshes [num]: 1
		Total Meshes [num]: 1
	Top Level Acceleration Structure: 
		Maximum [B]: 64
		Minimum [B]: 64
		Currently Allocated [B]: 64
		Total Allocated [B]: 64
		Total Created [num]: 1
		Average Allocated [B]: 64
		Maximum Instances [num]: 1
		Minimum Instances [num]: 1
		Total Instances [num]: 1
	Scratch Buffer: 
		Maximum [B]: 82944
		Minimum [B]: 82944
		Currently Allocated [B]: 82944
		Total Allocated [B]: 82944
		Total Created [num]: 1
		Average Allocated [B]: 82944
ProfilingStack: 
[PROF] Profiling statistics: 
	Scopes entered : 145642
	Scopes exited : 145641
	Overhead per scope [ticks] : 110.8
	Threads entered : 3
	Threads exited : 0
	============================
		         Sum[Mt]   Parent[%]  Samples[num]       Avg[Mt]   Last/Curr[Mt]        Self[Mt]     Self[%] 	 Name

	main:

		   24526.332 Mt   100.000 %         1 x    24526.333 Mt    24526.332 Mt        0.392 Mt     0.002 % 	 /

		   24525.971 Mt    99.999 %         1 x    24525.973 Mt    24525.971 Mt      247.174 Mt     1.008 % 	 ./Main application loop

		       2.175 Mt     0.009 %         1 x        2.175 Mt        0.000 Mt        2.175 Mt   100.000 % 	 ../TexturedRLInitialization

		       1.175 Mt     0.005 %         1 x        1.175 Mt        0.000 Mt        1.175 Mt   100.000 % 	 ../ShadowMapRLInitialization

		       4.989 Mt     0.020 %         1 x        4.989 Mt        0.000 Mt        4.989 Mt   100.000 % 	 ../RasterResolveRLInitialization

		       3.692 Mt     0.015 %         1 x        3.692 Mt        0.000 Mt        3.692 Mt   100.000 % 	 ../ResolveRLInitialization

		   24100.970 Mt    98.267 %     93894 x        0.257 Mt       20.882 Mt      398.287 Mt     1.653 % 	 ../MainLoop

		     202.355 Mt     0.840 %      1445 x        0.140 Mt        0.090 Mt      201.761 Mt    99.707 % 	 .../Update

		       0.593 Mt     0.293 %         1 x        0.593 Mt        0.000 Mt        0.593 Mt   100.000 % 	 ..../GuiModelApply

		   23500.327 Mt    97.508 %      3866 x        6.079 Mt        5.617 Mt      428.626 Mt     1.824 % 	 .../Render

		     694.500 Mt     2.955 %      3866 x        0.180 Mt        0.236 Mt       26.453 Mt     3.809 % 	 ..../RayTracing

		     311.593 Mt    44.866 %      3866 x        0.081 Mt        0.105 Mt      307.725 Mt    98.759 % 	 ...../Deferred

		       3.868 Mt     1.241 %      3866 x        0.001 Mt        0.001 Mt        3.868 Mt   100.000 % 	 ....../DeferredRLPrep

		     245.952 Mt    35.414 %      3866 x        0.064 Mt        0.089 Mt      243.717 Mt    99.091 % 	 ...../RayCasting

		       2.235 Mt     0.909 %      3866 x        0.001 Mt        0.001 Mt        2.235 Mt   100.000 % 	 ....../RayTracingRLPrep

		     110.502 Mt    15.911 %      3866 x        0.029 Mt        0.034 Mt      110.502 Mt   100.000 % 	 ...../Resolve

		   22377.201 Mt    95.221 %      3866 x        5.788 Mt        5.216 Mt    22377.201 Mt   100.000 % 	 ..../Present

		       2.556 Mt     0.010 %         1 x        2.556 Mt        0.000 Mt        2.556 Mt   100.000 % 	 ../AmbientOcclusionRLInitialization

		       7.656 Mt     0.031 %         1 x        7.656 Mt        0.000 Mt        7.656 Mt   100.000 % 	 ../DeferredRLInitialization

		      51.851 Mt     0.211 %         1 x       51.851 Mt        0.000 Mt        2.378 Mt     4.585 % 	 ../RayTracingRLInitialization

		      49.474 Mt    95.415 %         1 x       49.474 Mt        0.000 Mt       49.474 Mt   100.000 % 	 .../PipelineBuild

		     103.733 Mt     0.423 %         1 x      103.733 Mt        0.000 Mt        0.178 Mt     0.172 % 	 ../Initialization

		     103.555 Mt    99.828 %         1 x      103.555 Mt        0.000 Mt        1.811 Mt     1.749 % 	 .../ScenePrep

		      62.725 Mt    60.572 %         1 x       62.725 Mt        0.000 Mt       15.151 Mt    24.155 % 	 ..../SceneLoad

		      39.341 Mt    62.720 %         1 x       39.341 Mt        0.000 Mt        8.407 Mt    21.371 % 	 ...../glTF-LoadScene

		      30.934 Mt    78.629 %         2 x       15.467 Mt        0.000 Mt       30.934 Mt   100.000 % 	 ....../glTF-LoadImageData

		       6.450 Mt    10.284 %         1 x        6.450 Mt        0.000 Mt        6.450 Mt   100.000 % 	 ...../glTF-CreateScene

		       1.782 Mt     2.842 %         1 x        1.782 Mt        0.000 Mt        1.782 Mt   100.000 % 	 ...../SceneDesc-CreateScene

		       0.281 Mt     0.271 %         1 x        0.281 Mt        0.000 Mt        0.000 Mt     0.142 % 	 ..../ShadowMapRLPrep

		       0.281 Mt    99.858 %         1 x        0.281 Mt        0.000 Mt        0.273 Mt    97.149 % 	 ...../PrepareForRendering

		       0.008 Mt     2.851 %         1 x        0.008 Mt        0.000 Mt        0.008 Mt   100.000 % 	 ....../PrepareDrawBundle

		       1.395 Mt     1.347 %         1 x        1.395 Mt        0.000 Mt        0.000 Mt     0.022 % 	 ..../TexturedRLPrep

		       1.395 Mt    99.978 %         1 x        1.395 Mt        0.000 Mt        1.391 Mt    99.713 % 	 ...../PrepareForRendering

		       0.001 Mt     0.079 %         1 x        0.001 Mt        0.000 Mt        0.001 Mt   100.000 % 	 ....../PrepareMaterials

		       0.003 Mt     0.208 %         1 x        0.003 Mt        0.000 Mt        0.003 Mt   100.000 % 	 ....../PrepareDrawBundle

		       1.325 Mt     1.279 %         1 x        1.325 Mt        0.000 Mt        0.000 Mt     0.038 % 	 ..../DeferredRLPrep

		       1.324 Mt    99.962 %         1 x        1.324 Mt        0.000 Mt        1.023 Mt    77.247 % 	 ...../PrepareForRendering

		       0.001 Mt     0.045 %         1 x        0.001 Mt        0.000 Mt        0.001 Mt   100.000 % 	 ....../PrepareMaterials

		       0.298 Mt    22.519 %         1 x        0.298 Mt        0.000 Mt        0.298 Mt   100.000 % 	 ....../BuildMaterialCache

		       0.003 Mt     0.189 %         1 x        0.003 Mt        0.000 Mt        0.003 Mt   100.000 % 	 ....../PrepareDrawBundle

		      24.460 Mt    23.621 %         1 x       24.460 Mt        0.000 Mt        2.581 Mt    10.552 % 	 ..../RayTracingRLPrep

		       0.047 Mt     0.192 %         1 x        0.047 Mt        0.000 Mt        0.047 Mt   100.000 % 	 ...../PrepareAccelerationStructures

		       0.001 Mt     0.003 %         1 x        0.001 Mt        0.000 Mt        0.001 Mt   100.000 % 	 ...../PrepareCache

		      12.703 Mt    51.933 %         1 x       12.703 Mt        0.000 Mt       12.703 Mt   100.000 % 	 ...../PipelineBuild

		       0.321 Mt     1.311 %         1 x        0.321 Mt        0.000 Mt        0.321 Mt   100.000 % 	 ...../BuildCache

		       4.008 Mt    16.384 %         1 x        4.008 Mt        0.000 Mt        0.000 Mt     0.012 % 	 ...../BuildAccelerationStructures

		       4.007 Mt    99.988 %         1 x        4.007 Mt        0.000 Mt        2.403 Mt    59.970 % 	 ....../AccelerationStructureBuild

		       0.803 Mt    20.044 %         1 x        0.803 Mt        0.000 Mt        0.803 Mt   100.000 % 	 ......./BuildBottomLevelAS

		       0.801 Mt    19.987 %         1 x        0.801 Mt        0.000 Mt        0.801 Mt   100.000 % 	 ......./BuildTopLevelAS

		       4.800 Mt    19.625 %         1 x        4.800 Mt        0.000 Mt        4.800 Mt   100.000 % 	 ...../BuildShaderTables

		      11.558 Mt    11.161 %         1 x       11.558 Mt        0.000 Mt       11.558 Mt   100.000 % 	 ..../GpuSidePrep

	WindowThread:

		   24524.952 Mt   100.000 %         1 x    24524.952 Mt    24524.952 Mt    24524.952 Mt   100.000 % 	 /

	GpuThread:

		   20327.947 Mt   100.000 %      3866 x        5.258 Mt        4.583 Mt      129.342 Mt     0.636 % 	 /

		       0.510 Mt     0.003 %         1 x        0.510 Mt        0.000 Mt        0.006 Mt     1.186 % 	 ./ScenePrep

		       0.504 Mt    98.814 %         1 x        0.504 Mt        0.000 Mt        0.001 Mt     0.203 % 	 ../AccelerationStructureBuild

		       0.441 Mt    87.433 %         1 x        0.441 Mt        0.000 Mt        0.441 Mt   100.000 % 	 .../BuildBottomLevelASGpu

		       0.062 Mt    12.364 %         1 x        0.062 Mt        0.000 Mt        0.062 Mt   100.000 % 	 .../BuildTopLevelASGpu

		   20062.915 Mt    98.696 %      3866 x        5.190 Mt        4.579 Mt        8.695 Mt     0.043 % 	 ./RayTracingGpu

		     519.480 Mt     2.589 %      3866 x        0.134 Mt        0.071 Mt      519.480 Mt   100.000 % 	 ../DeferredRLGpu

		    2491.889 Mt    12.420 %      3866 x        0.645 Mt        0.238 Mt     2491.889 Mt   100.000 % 	 ../RayTracingRLGpu

		   17042.851 Mt    84.947 %      3866 x        4.408 Mt        4.268 Mt    17042.851 Mt   100.000 % 	 ../ResolveRLGpu

		     135.181 Mt     0.665 %      3866 x        0.035 Mt        0.002 Mt      135.181 Mt   100.000 % 	 ./Present


	============================


