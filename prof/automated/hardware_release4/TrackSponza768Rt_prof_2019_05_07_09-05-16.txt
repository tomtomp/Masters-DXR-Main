Configuration: 
Command line parameters  = E:\programming\FIT-DIP\Masters-DXR-Main-git\build\Measurement\x64\Release\Measurement.exe --scene Sponza/Sponza.gltf --quality-preset Low --profile-automation --profile-camera-track Sponza/Sponza.trk --rt-mode --width 1024 --height 768 --profile-output TrackSponza768Rt 
Build type               = Release
Device name              = NVIDIA GeForce RTX 2080 Ti
Device memory MB         = 11049
Ray Tracing backend      = Hardware
Window width             = 1024
Window height            = 768
Window title             = Direct3D App
Back-buffer count        = 3
Fill Triangles           = enabled
Triangles Font Clock-Wise= enabled
Fast Build AS            = disabled
Target FPS               = 0
Target UPS               = 60
Tearing                  = enabled
VSync                    = disabled
GUI rendering            = disabled
Debug layer              = disabled
GPU validation           = disabled
GPU profiling            = enabled
Render mode              = Ray Tracing
Loaded scene file        = Sponza/Sponza.gltf
Using testing scene      = disabled
Duplication              = 1
Duplication in cube      = disabled
Duplication offset       = 10
BLAS duplication         = disabled
Rays per pixel           = 5
Quality preset           = Low
Rasterization Quality    : 
  AO                     = enabled
  AO kernel size         = 4
  Shadows                = enabled
  Shadow map resolution  = 2048
  Shadow PCF kernel size = 0
  Reflections            = disabled
Ray Tracing Quality      : 
  AO                     = enabled
  AO rays                = 4
  AO kernel size         = 4
  Shadows                = enabled
  Shadow rays            = 1
  Shadow kernel size     = 4
  Reflections            = disabled

ProfilingAggregator: 
Render	,	2.9534	,	2.9472	,	2.9751	,	2.7224	,	2.7029	,	2.6	,	2.5828	,	2.3105	,	2.5687	,	2.7396	,	2.8744	,	3.4816	,	3.3645	,	3.0565	,	3.4002	,	3.1539	,	3.0706	,	2.7125	,	3.1534	,	2.9925	,	3.0845	,	3.0762	,	3.0798	,	3.0285	,	3.0297	,	2.5518
Update	,	0.0791	,	0.0794	,	0.0793	,	0.0834	,	0.0549	,	0.069	,	0.0699	,	0.0807	,	0.0787	,	0.0806	,	0.0782	,	0.0788	,	0.0833	,	0.0538	,	0.0789	,	0.0807	,	0.0812	,	0.0805	,	0.0772	,	0.0795	,	0.0775	,	0.0844	,	0.0793	,	0.0782	,	0.0795	,	0.0488
RayTracing	,	0.171	,	0.1726	,	0.1737	,	0.1435	,	0.1934	,	0.1504	,	0.1782	,	0.1725	,	0.2018	,	0.1702	,	0.1698	,	0.1734	,	0.1705	,	0.1446	,	0.1696	,	0.1726	,	0.1735	,	0.173	,	0.1707	,	0.2001	,	0.1728	,	0.1618	,	0.1717	,	0.1698	,	0.1721	,	0.0978
RayTracingGpu	,	2.12806	,	2.14416	,	2.17309	,	2.1439	,	2.04445	,	1.97174	,	1.87939	,	1.51037	,	1.74282	,	1.96762	,	2.13882	,	2.68419	,	2.50058	,	2.43366	,	2.2808	,	2.33331	,	2.26813	,	1.86422	,	2.23757	,	2.21005	,	2.34698	,	2.464	,	2.3327	,	2.25312	,	2.24086	,	2.18317
DeferredRLGpu	,	1.07731	,	1.0855	,	1.09904	,	1.05965	,	0.989856	,	0.914976	,	0.822304	,	0.521856	,	0.690016	,	0.908	,	1.12755	,	1.4087	,	1.45622	,	1.2872	,	1.18848	,	1.30253	,	1.12227	,	0.87104	,	1.14003	,	1.21194	,	1.27408	,	1.40877	,	1.30368	,	1.24032	,	1.23101	,	1.17018
RayTracingRLGpu	,	0.525344	,	0.535616	,	0.547712	,	0.56048	,	0.532416	,	0.532416	,	0.53312	,	0.466304	,	0.52336	,	0.527232	,	0.4968	,	0.65376	,	0.532672	,	0.633024	,	0.476384	,	0.514688	,	0.628192	,	0.476704	,	0.579968	,	0.475264	,	0.559136	,	0.54768	,	0.52192	,	0.502624	,	0.495904	,	0.500128
ResolveRLGpu	,	0.523264	,	0.520864	,	0.522144	,	0.519968	,	0.52016	,	0.52112	,	0.520736	,	0.518304	,	0.525568	,	0.530336	,	0.509632	,	0.619968	,	0.507776	,	0.510336	,	0.612576	,	0.512224	,	0.516064	,	0.512224	,	0.515616	,	0.519648	,	0.509728	,	0.50576	,	0.502272	,	0.507072	,	0.511872	,	0.509024
Index	,	0	,	1	,	2	,	3	,	4	,	5	,	6	,	7	,	8	,	9	,	10	,	11	,	12	,	13	,	14	,	15	,	16	,	17	,	18	,	19	,	20	,	21	,	22	,	23	,	24	,	25

ProfilingBuildAggregator: 
BuildBottomLevelAS	,	1.2837
BuildBottomLevelASGpu	,	2.32883
BuildTopLevelAS	,	0.6033
BuildTopLevelASGpu	,	0.088928
Duplication	,	1
Triangles	,	262267
Meshes	,	103
BottomLevels	,	1
TopLevelSize	,	64
BottomLevelsSize	,	18390656
Index	,	0

FpsAggregator: 
FPS	,	290	,	325	,	354	,	330	,	342	,	359	,	368	,	398	,	403	,	382	,	340	,	321	,	324	,	325	,	334	,	334	,	339	,	367	,	369	,	343	,	313	,	308	,	311	,	317	,	333	,	329
UPS	,	59	,	61	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	61	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	61	,	60	,	60	,	60	,	60	,	60
FrameTime	,	3.45781	,	3.0842	,	2.83261	,	3.03288	,	2.93168	,	2.79106	,	2.72095	,	2.51552	,	2.48695	,	2.62345	,	2.94963	,	3.12578	,	3.08953	,	3.07917	,	2.99464	,	2.99758	,	2.9522	,	2.72869	,	2.71235	,	2.92417	,	3.20469	,	3.25536	,	3.22354	,	3.15581	,	3.00466	,	3.04327
GigaRays/s	,	1.13718	,	1.27494	,	1.38817	,	1.29651	,	1.34127	,	1.40884	,	1.44514	,	1.56316	,	1.58112	,	1.49885	,	1.3331	,	1.25798	,	1.27274	,	1.27702	,	1.31307	,	1.31178	,	1.33194	,	1.44104	,	1.44972	,	1.34471	,	1.227	,	1.2079	,	1.21983	,	1.24601	,	1.30869	,	1.29208
Index	,	0	,	1	,	2	,	3	,	4	,	5	,	6	,	7	,	8	,	9	,	10	,	11	,	12	,	13	,	14	,	15	,	16	,	17	,	18	,	19	,	20	,	21	,	22	,	23	,	24	,	25


AccelerationStructure: 
Ray tracing acceleration structure building statistics: 
	Bottom Level Acceleration Structure: 
		Maximum [B]: 18390656
		Minimum [B]: 18390656
		Currently Allocated [B]: 18390656
		Currently Created [num]: 1
		Current Average [B]: 18390656
		Maximum Triangles [num]: 262267
		Minimum Triangles [num]: 262267
		Total Triangles [num]: 262267
		Maximum Meshes [num]: 103
		Minimum Meshes [num]: 103
		Total Meshes [num]: 103
	Top Level Acceleration Structure: 
		Maximum [B]: 64
		Minimum [B]: 64
		Currently Allocated [B]: 64
		Total Allocated [B]: 64
		Total Created [num]: 1
		Average Allocated [B]: 64
		Maximum Instances [num]: 1
		Minimum Instances [num]: 1
		Total Instances [num]: 1
	Scratch Buffer: 
		Maximum [B]: 14995072
		Minimum [B]: 14995072
		Currently Allocated [B]: 14995072
		Total Allocated [B]: 14995072
		Total Created [num]: 1
		Average Allocated [B]: 14995072
ProfilingStack: 
[PROF] Profiling statistics: 
	Scopes entered : 172340
	Scopes exited : 172339
	Overhead per scope [ticks] : 100.8
	Threads entered : 3
	Threads exited : 0
	============================
		         Sum[Mt]   Parent[%]  Samples[num]       Avg[Mt]   Last/Curr[Mt]        Self[Mt]     Self[%] 	 Name

	main:

		   32630.244 Mt   100.000 %         1 x    32630.244 Mt    32630.244 Mt        0.444 Mt     0.001 % 	 /

		   32629.809 Mt    99.999 %         1 x    32629.809 Mt    32629.809 Mt      314.771 Mt     0.965 % 	 ./Main application loop

		       2.302 Mt     0.007 %         1 x        2.302 Mt        0.000 Mt        2.302 Mt   100.000 % 	 ../TexturedRLInitialization

		       1.323 Mt     0.004 %         1 x        1.323 Mt        0.000 Mt        1.323 Mt   100.000 % 	 ../ShadowMapRLInitialization

		       6.607 Mt     0.020 %         1 x        6.607 Mt        0.000 Mt        6.607 Mt   100.000 % 	 ../RasterResolveRLInitialization

		       3.796 Mt     0.012 %         1 x        3.796 Mt        0.000 Mt        3.796 Mt   100.000 % 	 ../ResolveRLInitialization

		   26057.066 Mt    79.857 %     55485 x        0.470 Mt        2.669 Mt      355.128 Mt     1.363 % 	 ../MainLoop

		     190.295 Mt     0.730 %      1563 x        0.122 Mt        0.039 Mt      189.675 Mt    99.674 % 	 .../Update

		       0.620 Mt     0.326 %         1 x        0.620 Mt        0.000 Mt        0.620 Mt   100.000 % 	 ..../GuiModelApply

		   25511.643 Mt    97.907 %      8860 x        2.879 Mt        2.629 Mt      947.861 Mt     3.715 % 	 .../Render

		    1502.616 Mt     5.890 %      8860 x        0.170 Mt        0.098 Mt       57.166 Mt     3.804 % 	 ..../RayTracing

		     630.810 Mt    41.981 %      8860 x        0.071 Mt        0.037 Mt      621.456 Mt    98.517 % 	 ...../Deferred

		       9.354 Mt     1.483 %      8860 x        0.001 Mt        0.000 Mt        9.354 Mt   100.000 % 	 ....../DeferredRLPrep

		     535.447 Mt    35.634 %      8860 x        0.060 Mt        0.041 Mt      530.391 Mt    99.056 % 	 ...../RayCasting

		       5.055 Mt     0.944 %      8860 x        0.001 Mt        0.000 Mt        5.055 Mt   100.000 % 	 ....../RayTracingRLPrep

		     279.193 Mt    18.580 %      8860 x        0.032 Mt        0.017 Mt      279.193 Mt   100.000 % 	 ...../Resolve

		   23061.166 Mt    90.395 %      8860 x        2.603 Mt        2.462 Mt    23061.166 Mt   100.000 % 	 ..../Present

		       2.886 Mt     0.009 %         1 x        2.886 Mt        0.000 Mt        2.886 Mt   100.000 % 	 ../AmbientOcclusionRLInitialization

		       5.575 Mt     0.017 %         1 x        5.575 Mt        0.000 Mt        5.575 Mt   100.000 % 	 ../DeferredRLInitialization

		      73.294 Mt     0.225 %         1 x       73.294 Mt        0.000 Mt        2.486 Mt     3.391 % 	 ../RayTracingRLInitialization

		      70.809 Mt    96.609 %         1 x       70.809 Mt        0.000 Mt       70.809 Mt   100.000 % 	 .../PipelineBuild

		    6162.190 Mt    18.885 %         1 x     6162.190 Mt        0.000 Mt        0.199 Mt     0.003 % 	 ../Initialization

		    6161.992 Mt    99.997 %         1 x     6161.992 Mt        0.000 Mt        2.119 Mt     0.034 % 	 .../ScenePrep

		    6105.947 Mt    99.090 %         1 x     6105.947 Mt        0.000 Mt     3326.831 Mt    54.485 % 	 ..../SceneLoad

		    2202.327 Mt    36.069 %         1 x     2202.327 Mt        0.000 Mt      810.643 Mt    36.808 % 	 ...../glTF-LoadScene

		    1391.685 Mt    63.192 %        69 x       20.169 Mt        0.000 Mt     1391.685 Mt   100.000 % 	 ....../glTF-LoadImageData

		     471.468 Mt     7.721 %         1 x      471.468 Mt        0.000 Mt      471.468 Mt   100.000 % 	 ...../glTF-CreateScene

		     105.320 Mt     1.725 %         1 x      105.320 Mt        0.000 Mt      105.320 Mt   100.000 % 	 ...../SceneDesc-CreateScene

		       0.914 Mt     0.015 %         1 x        0.914 Mt        0.000 Mt        0.001 Mt     0.066 % 	 ..../ShadowMapRLPrep

		       0.914 Mt    99.934 %         1 x        0.914 Mt        0.000 Mt        0.879 Mt    96.180 % 	 ...../PrepareForRendering

		       0.035 Mt     3.820 %         1 x        0.035 Mt        0.000 Mt        0.035 Mt   100.000 % 	 ....../PrepareDrawBundle

		       3.100 Mt     0.050 %         1 x        3.100 Mt        0.000 Mt        0.000 Mt     0.016 % 	 ..../TexturedRLPrep

		       3.099 Mt    99.984 %         1 x        3.099 Mt        0.000 Mt        3.065 Mt    98.880 % 	 ...../PrepareForRendering

		       0.001 Mt     0.035 %         1 x        0.001 Mt        0.000 Mt        0.001 Mt   100.000 % 	 ....../PrepareMaterials

		       0.034 Mt     1.084 %         1 x        0.034 Mt        0.000 Mt        0.034 Mt   100.000 % 	 ....../PrepareDrawBundle

		       2.916 Mt     0.047 %         1 x        2.916 Mt        0.000 Mt        0.001 Mt     0.031 % 	 ..../DeferredRLPrep

		       2.915 Mt    99.969 %         1 x        2.915 Mt        0.000 Mt        2.378 Mt    81.567 % 	 ...../PrepareForRendering

		       0.001 Mt     0.021 %         1 x        0.001 Mt        0.000 Mt        0.001 Mt   100.000 % 	 ....../PrepareMaterials

		       0.268 Mt     9.204 %         1 x        0.268 Mt        0.000 Mt        0.268 Mt   100.000 % 	 ....../BuildMaterialCache

		       0.268 Mt     9.208 %         1 x        0.268 Mt        0.000 Mt        0.268 Mt   100.000 % 	 ....../PrepareDrawBundle

		      18.630 Mt     0.302 %         1 x       18.630 Mt        0.000 Mt        1.846 Mt     9.906 % 	 ..../RayTracingRLPrep

		       0.179 Mt     0.961 %         1 x        0.179 Mt        0.000 Mt        0.179 Mt   100.000 % 	 ...../PrepareAccelerationStructures

		       0.003 Mt     0.014 %         1 x        0.003 Mt        0.000 Mt        0.003 Mt   100.000 % 	 ...../PrepareCache

		      11.680 Mt    62.693 %         1 x       11.680 Mt        0.000 Mt       11.680 Mt   100.000 % 	 ...../PipelineBuild

		       0.498 Mt     2.671 %         1 x        0.498 Mt        0.000 Mt        0.498 Mt   100.000 % 	 ...../BuildCache

		       3.516 Mt    18.874 %         1 x        3.516 Mt        0.000 Mt        0.001 Mt     0.017 % 	 ...../BuildAccelerationStructures

		       3.516 Mt    99.983 %         1 x        3.516 Mt        0.000 Mt        1.629 Mt    46.325 % 	 ....../AccelerationStructureBuild

		       1.284 Mt    36.514 %         1 x        1.284 Mt        0.000 Mt        1.284 Mt   100.000 % 	 ......./BuildBottomLevelAS

		       0.603 Mt    17.161 %         1 x        0.603 Mt        0.000 Mt        0.603 Mt   100.000 % 	 ......./BuildTopLevelAS

		       0.909 Mt     4.881 %         1 x        0.909 Mt        0.000 Mt        0.909 Mt   100.000 % 	 ...../BuildShaderTables

		      28.366 Mt     0.460 %         1 x       28.366 Mt        0.000 Mt       28.366 Mt   100.000 % 	 ..../GpuSidePrep

	WindowThread:

		   32621.431 Mt   100.000 %         1 x    32621.431 Mt    32621.431 Mt    32621.431 Mt   100.000 % 	 /

	GpuThread:

		   19098.719 Mt   100.000 %      8860 x        2.156 Mt        2.187 Mt      123.123 Mt     0.645 % 	 /

		       2.426 Mt     0.013 %         1 x        2.426 Mt        0.000 Mt        0.007 Mt     0.295 % 	 ./ScenePrep

		       2.419 Mt    99.705 %         1 x        2.419 Mt        0.000 Mt        0.001 Mt     0.060 % 	 ../AccelerationStructureBuild

		       2.329 Mt    96.265 %         1 x        2.329 Mt        0.000 Mt        2.329 Mt   100.000 % 	 .../BuildBottomLevelASGpu

		       0.089 Mt     3.676 %         1 x        0.089 Mt        0.000 Mt        0.089 Mt   100.000 % 	 .../BuildTopLevelASGpu

		   18922.507 Mt    99.077 %      8860 x        2.136 Mt        2.184 Mt       28.263 Mt     0.149 % 	 ./RayTracingGpu

		    9583.548 Mt    50.646 %      8860 x        1.082 Mt        1.170 Mt     9583.548 Mt   100.000 % 	 ../DeferredRLGpu

		    4642.872 Mt    24.536 %      8860 x        0.524 Mt        0.497 Mt     4642.872 Mt   100.000 % 	 ../RayTracingRLGpu

		    4667.824 Mt    24.668 %      8860 x        0.527 Mt        0.515 Mt     4667.824 Mt   100.000 % 	 ../ResolveRLGpu

		      50.662 Mt     0.265 %      8860 x        0.006 Mt        0.002 Mt       50.662 Mt   100.000 % 	 ./Present


	============================


