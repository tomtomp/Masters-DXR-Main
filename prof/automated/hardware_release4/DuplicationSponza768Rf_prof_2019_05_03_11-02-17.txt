Configuration: 
Command line parameters  = E:\programming\FIT-DIP\Masters-DXR-Main-git\build\Measurement\x64\Release\Measurement.exe --scene Sponza/Sponza.gltf --duplication-cube --profile-duplication --profile-max-duplication 7 --rt-mode --width 1024 --height 768 --profile-output DuplicationSponza768Rf --fast-build-as 
Build type               = Release
Device name              = NVIDIA GeForce RTX 2080 Ti
Device memory MB         = 11049
Ray Tracing backend      = Hardware
Window width             = 1024
Window height            = 768
Window title             = Direct3D App
Back-buffer count        = 3
Fill Triangles           = enabled
Triangles Font Clock-Wise= enabled
Fast Build AS            = enabled
Target FPS               = 60
Target UPS               = 60
Tearing                  = disabled
VSync                    = disabled
GUI rendering            = disabled
Debug layer              = disabled
GPU validation           = disabled
GPU profiling            = enabled
Render mode              = Ray Tracing
Loaded scene file        = Sponza/Sponza.gltf
Using testing scene      = disabled
Duplication              = 7
Duplication in cube      = enabled
Duplication offset       = 10
BLAS duplication         = enabled
Rays per pixel           = 13
Quality preset           = High
Rasterization Quality    : 
  AO                     = enabled
  AO kernel size         = 4
  Shadows                = enabled
  Shadow map resolution  = 4096
  Shadow PCF kernel size = 4
  Reflections            = enabled
Ray Tracing Quality      : 
  AO                     = enabled
  AO rays                = 4
  AO kernel size         = 4
  Shadows                = enabled
  Shadow rays            = 4
  Shadow kernel size     = 4
  Reflections            = enabled

ProfilingAggregator: 
Render	,	3.9094	,	4.0968	,	7.791	,	7.2689	,	10.3064	,	10.2208	,	13.7607	,	13.5646	,	17.8806	,	17.801	,	719.622	,	22.5543	,	1074.11	,	1077.4
Update	,	0.0221	,	0.0152	,	0.02	,	0.0298	,	0.0401	,	0.0706	,	0.0726	,	0.0655	,	0.0827	,	0.0737	,	0.0224	,	0.0896	,	3.5011	,	0.0254
RayTracing	,	0.0486	,	0.0491	,	0.0748	,	0.0704	,	0.1295	,	0.1746	,	0.1963	,	0.2166	,	0.2064	,	0.1957	,	455.394	,	0.2165	,	662.46	,	665.854
RayTracingGpu	,	3.4648	,	3.76378	,	7.3352	,	6.74003	,	9.61206	,	9.40419	,	12.8682	,	12.7851	,	16.9951	,	17.0175	,	263.174	,	21.7135	,	410.801	,	410.487
DeferredRLGpu	,	0.895264	,	0.90096	,	4.35494	,	3.79536	,	7.10608	,	7.10144	,	10.7597	,	10.475	,	14.7241	,	14.9348	,	19.557	,	19.3598	,	25.5089	,	25.2718
RayTracingRLGpu	,	1.84704	,	2.12432	,	2.30128	,	2.27226	,	1.95962	,	1.77565	,	1.58442	,	1.76189	,	1.74243	,	1.55786	,	1.82054	,	1.81984	,	1.78986	,	1.78854
ResolveRLGpu	,	0.718496	,	0.73488	,	0.676992	,	0.668032	,	0.542336	,	0.523584	,	0.519328	,	0.546112	,	0.525504	,	0.521056	,	0.531456	,	0.531232	,	0.528032	,	0.528864
Index	,	0	,	1	,	2	,	3	,	4	,	5	,	6	,	7	,	8	,	9	,	10	,	11	,	12	,	13

ProfilingBuildAggregator: 
BuildBottomLevelAS	,	1.22	,	9.9691	,	32.1393	,	78.9649	,	147.381	,	271.365	,	449.595	,	428.245
BuildBottomLevelASGpu	,	1.48029	,	12.6646	,	39.0084	,	71.6831	,	139.043	,	241.129	,	382.815	,	382.733
BuildTopLevelAS	,	0.7676	,	1.1495	,	1.2238	,	1.5858	,	1.6523	,	1.4512	,	1.6733	,	1.4932
BuildTopLevelASGpu	,	0.086464	,	0.097344	,	0.11952	,	0.09744	,	0.12752	,	0.13104	,	0.153344	,	0.157216
Duplication	,	1	,	2	,	3	,	4	,	5	,	6	,	7	,	7
Triangles	,	262267	,	2098136	,	7081209	,	16785088	,	32783375	,	56649672	,	89957581	,	89957581
Meshes	,	103	,	824	,	2781	,	6592	,	12875	,	22248	,	35329	,	35329
BottomLevels	,	1	,	8	,	27	,	64	,	125	,	216	,	343	,	343
TopLevelSize	,	64	,	512	,	1728	,	4096	,	8000	,	13824	,	21952	,	21952
BottomLevelsSize	,	14622208	,	116977664	,	394799616	,	935821312	,	1827776000	,	3158396928	,	5015417344	,	5015417344
Index	,	0	,	1	,	2	,	3	,	4	,	5	,	6	,	7

FpsAggregator: 
FPS	,	52	,	59	,	54	,	59	,	44	,	59	,	30	,	59	,	8	,	55	,	2	,	44	,	1	,	1
UPS	,	59	,	60	,	60	,	61	,	59	,	61	,	60	,	60	,	60	,	60	,	47	,	103	,	1	,	66
FrameTime	,	19.2308	,	16.9492	,	18.5813	,	16.9492	,	22.8185	,	16.9492	,	33.5943	,	16.961	,	125.454	,	18.3552	,	739.189	,	23.2322	,	1094.01	,	1104
GigaRays/s	,	0.531628	,	0.603193	,	0.550211	,	0.603193	,	0.44804	,	0.603193	,	0.304326	,	0.602771	,	0.0814932	,	0.556988	,	0.0138309	,	0.440062	,	0.00934507	,	0.00926052
Index	,	0	,	1	,	2	,	3	,	4	,	5	,	6	,	7	,	8	,	9	,	10	,	11	,	12	,	13


AccelerationStructure: 
Ray tracing acceleration structure building statistics: 
	Bottom Level Acceleration Structure: 
		Maximum [B]: 14622208
		Minimum [B]: 14622208
		Currently Allocated [B]: 5015417344
		Currently Created [num]: 343
		Current Average [B]: 14622208
		Maximum Triangles [num]: 262267
		Minimum Triangles [num]: 262267
		Total Triangles [num]: 89957581
		Maximum Meshes [num]: 103
		Minimum Meshes [num]: 103
		Total Meshes [num]: 35329
	Top Level Acceleration Structure: 
		Maximum [B]: 21952
		Minimum [B]: 21952
		Currently Allocated [B]: 21952
		Total Allocated [B]: 21952
		Total Created [num]: 1
		Average Allocated [B]: 21952
		Maximum Instances [num]: 343
		Minimum Instances [num]: 343
		Total Instances [num]: 343
	Scratch Buffer: 
		Maximum [B]: 11657856
		Minimum [B]: 11657856
		Currently Allocated [B]: 11657856
		Total Allocated [B]: 11657856
		Total Created [num]: 1
		Average Allocated [B]: 11657856
ProfilingStack: 
[PROF] Profiling statistics: 
	Scopes entered : 11862979
	Scopes exited : 11862978
	Overhead per scope [ticks] : 103.9
	Threads entered : 3
	Threads exited : 0
	============================
		         Sum[Mt]   Parent[%]  Samples[num]       Avg[Mt]   Last/Curr[Mt]        Self[Mt]     Self[%] 	 Name

	main:

		   19787.417 Mt   100.000 %         1 x    19787.419 Mt    19787.417 Mt        0.406 Mt     0.002 % 	 /

		   19787.053 Mt    99.998 %         1 x    19787.055 Mt    19787.053 Mt      986.075 Mt     4.983 % 	 ./Main application loop

		       3.085 Mt     0.016 %         1 x        3.085 Mt        0.000 Mt        3.085 Mt   100.000 % 	 ../TexturedRLInitialization

		       3.104 Mt     0.016 %         1 x        3.104 Mt        0.000 Mt        3.104 Mt   100.000 % 	 ../AmbientOcclusionRLInitialization

		       5.032 Mt     0.025 %         1 x        5.032 Mt        0.000 Mt        5.032 Mt   100.000 % 	 ../RasterResolveRLInitialization

		       5.108 Mt     0.026 %         1 x        5.108 Mt        0.000 Mt        5.108 Mt   100.000 % 	 ../DeferredRLInitialization

		   14641.410 Mt    73.995 %  11854928 x        0.001 Mt       64.262 Mt     3412.099 Mt    23.304 % 	 ../MainLoop

		     139.800 Mt     0.955 %       883 x        0.158 Mt        0.025 Mt      139.204 Mt    99.574 % 	 .../Update

		       0.596 Mt     0.426 %         1 x        0.596 Mt        0.000 Mt        0.596 Mt   100.000 % 	 ..../GuiModelApply

		   11089.511 Mt    75.741 %       528 x       21.003 Mt       28.900 Mt       50.871 Mt     0.459 % 	 .../Render

		    3559.172 Mt    32.095 %       528 x        6.741 Mt        0.306 Mt        2.786 Mt     0.078 % 	 ..../RayTracing

		     377.924 Mt    10.618 %       528 x        0.716 Mt        0.144 Mt       34.581 Mt     9.150 % 	 ...../Deferred

		     343.343 Mt    90.850 %       528 x        0.650 Mt        0.001 Mt        0.270 Mt     0.079 % 	 ....../DeferredRLPrep

		     343.073 Mt    99.921 %        12 x       28.589 Mt        0.000 Mt      132.376 Mt    38.585 % 	 ......./PrepareForRendering

		       0.205 Mt     0.060 %        12 x        0.017 Mt        0.000 Mt        0.205 Mt   100.000 % 	 ......../PrepareMaterials

		       0.002 Mt     0.001 %        12 x        0.000 Mt        0.000 Mt        0.002 Mt   100.000 % 	 ......../BuildMaterialCache

		     210.490 Mt    61.354 %        12 x       17.541 Mt        0.000 Mt      210.490 Mt   100.000 % 	 ......../PrepareDrawBundle

		    3165.149 Mt    88.929 %       528 x        5.995 Mt        0.108 Mt       29.528 Mt     0.933 % 	 ...../RayCasting

		    3135.621 Mt    99.067 %       528 x        5.939 Mt        0.001 Mt       32.934 Mt     1.050 % 	 ....../RayTracingRLPrep

		     530.289 Mt    16.912 %        12 x       44.191 Mt        0.000 Mt      530.289 Mt   100.000 % 	 ......./PrepareAccelerationStructures

		       0.074 Mt     0.002 %        12 x        0.006 Mt        0.000 Mt        0.074 Mt   100.000 % 	 ......./PrepareCache

		     205.578 Mt     6.556 %        12 x       17.131 Mt        0.000 Mt      205.578 Mt   100.000 % 	 ......./PipelineBuild

		       0.003 Mt     0.000 %        12 x        0.000 Mt        0.000 Mt        0.003 Mt   100.000 % 	 ......./BuildCache

		    2074.934 Mt    66.173 %        12 x      172.911 Mt        0.000 Mt        0.011 Mt     0.001 % 	 ......./BuildAccelerationStructures

		    2074.923 Mt    99.999 %        12 x      172.910 Mt        0.000 Mt       67.565 Mt     3.256 % 	 ......../AccelerationStructureBuild

		    1990.326 Mt    95.923 %        12 x      165.860 Mt        0.000 Mt     1990.326 Mt   100.000 % 	 ........./BuildBottomLevelAS

		      17.033 Mt     0.821 %        12 x        1.419 Mt        0.000 Mt       17.033 Mt   100.000 % 	 ........./BuildTopLevelAS

		     291.809 Mt     9.306 %        12 x       24.317 Mt        0.000 Mt      291.809 Mt   100.000 % 	 ......./BuildShaderTables

		      13.313 Mt     0.374 %       528 x        0.025 Mt        0.043 Mt       13.313 Mt   100.000 % 	 ...../Resolve

		    7479.468 Mt    67.446 %       528 x       14.166 Mt       28.186 Mt     7479.468 Mt   100.000 % 	 ..../Present

		       1.562 Mt     0.008 %         1 x        1.562 Mt        0.000 Mt        1.562 Mt   100.000 % 	 ../ShadowMapRLInitialization

		      56.017 Mt     0.283 %         1 x       56.017 Mt        0.000 Mt        2.306 Mt     4.117 % 	 ../RayTracingRLInitialization

		      53.710 Mt    95.883 %         1 x       53.710 Mt        0.000 Mt       53.710 Mt   100.000 % 	 .../PipelineBuild

		       4.414 Mt     0.022 %         1 x        4.414 Mt        0.000 Mt        4.414 Mt   100.000 % 	 ../ResolveRLInitialization

		    4081.246 Mt    20.626 %         1 x     4081.246 Mt        0.000 Mt        0.281 Mt     0.007 % 	 ../Initialization

		    4080.965 Mt    99.993 %         1 x     4080.965 Mt        0.000 Mt        2.941 Mt     0.072 % 	 .../ScenePrep

		    3995.781 Mt    97.913 %         1 x     3995.781 Mt        0.000 Mt     1589.076 Mt    39.769 % 	 ..../SceneLoad

		    2093.228 Mt    52.386 %         1 x     2093.228 Mt        0.000 Mt      589.491 Mt    28.162 % 	 ...../glTF-LoadScene

		    1503.737 Mt    71.838 %        69 x       21.793 Mt        0.000 Mt     1503.737 Mt   100.000 % 	 ....../glTF-LoadImageData

		     256.680 Mt     6.424 %         1 x      256.680 Mt        0.000 Mt      256.680 Mt   100.000 % 	 ...../glTF-CreateScene

		      56.798 Mt     1.421 %         1 x       56.798 Mt        0.000 Mt       56.798 Mt   100.000 % 	 ...../SceneDesc-CreateScene

		       0.939 Mt     0.023 %         1 x        0.939 Mt        0.000 Mt        0.003 Mt     0.266 % 	 ..../ShadowMapRLPrep

		       0.937 Mt    99.734 %         1 x        0.937 Mt        0.000 Mt        0.895 Mt    95.612 % 	 ...../PrepareForRendering

		       0.041 Mt     4.388 %         1 x        0.041 Mt        0.000 Mt        0.041 Mt   100.000 % 	 ....../PrepareDrawBundle

		       4.269 Mt     0.105 %         1 x        4.269 Mt        0.000 Mt        0.001 Mt     0.014 % 	 ..../TexturedRLPrep

		       4.268 Mt    99.986 %         1 x        4.268 Mt        0.000 Mt        4.230 Mt    99.119 % 	 ...../PrepareForRendering

		       0.002 Mt     0.052 %         1 x        0.002 Mt        0.000 Mt        0.002 Mt   100.000 % 	 ....../PrepareMaterials

		       0.035 Mt     0.829 %         1 x        0.035 Mt        0.000 Mt        0.035 Mt   100.000 % 	 ....../PrepareDrawBundle

		       4.073 Mt     0.100 %         1 x        4.073 Mt        0.000 Mt        0.000 Mt     0.012 % 	 ..../DeferredRLPrep

		       4.073 Mt    99.988 %         1 x        4.073 Mt        0.000 Mt        3.708 Mt    91.048 % 	 ...../PrepareForRendering

		       0.001 Mt     0.029 %         1 x        0.001 Mt        0.000 Mt        0.001 Mt   100.000 % 	 ....../PrepareMaterials

		       0.309 Mt     7.590 %         1 x        0.309 Mt        0.000 Mt        0.309 Mt   100.000 % 	 ....../BuildMaterialCache

		       0.054 Mt     1.333 %         1 x        0.054 Mt        0.000 Mt        0.054 Mt   100.000 % 	 ....../PrepareDrawBundle

		      21.602 Mt     0.529 %         1 x       21.602 Mt        0.000 Mt        2.735 Mt    12.662 % 	 ..../RayTracingRLPrep

		       0.068 Mt     0.315 %         1 x        0.068 Mt        0.000 Mt        0.068 Mt   100.000 % 	 ...../PrepareAccelerationStructures

		       0.004 Mt     0.018 %         1 x        0.004 Mt        0.000 Mt        0.004 Mt   100.000 % 	 ...../PrepareCache

		      12.567 Mt    58.174 %         1 x       12.567 Mt        0.000 Mt       12.567 Mt   100.000 % 	 ...../PipelineBuild

		       0.586 Mt     2.712 %         1 x        0.586 Mt        0.000 Mt        0.586 Mt   100.000 % 	 ...../BuildCache

		       4.382 Mt    20.286 %         1 x        4.382 Mt        0.000 Mt        0.001 Mt     0.014 % 	 ...../BuildAccelerationStructures

		       4.382 Mt    99.986 %         1 x        4.382 Mt        0.000 Mt        2.394 Mt    54.638 % 	 ....../AccelerationStructureBuild

		       1.220 Mt    27.844 %         1 x        1.220 Mt        0.000 Mt        1.220 Mt   100.000 % 	 ......./BuildBottomLevelAS

		       0.768 Mt    17.519 %         1 x        0.768 Mt        0.000 Mt        0.768 Mt   100.000 % 	 ......./BuildTopLevelAS

		       1.260 Mt     5.834 %         1 x        1.260 Mt        0.000 Mt        1.260 Mt   100.000 % 	 ...../BuildShaderTables

		      51.359 Mt     1.259 %         1 x       51.359 Mt        0.000 Mt       51.359 Mt   100.000 % 	 ..../GpuSidePrep

	WindowThread:

		   19786.725 Mt   100.000 %         1 x    19786.725 Mt    19786.725 Mt    19786.725 Mt   100.000 % 	 /

	GpuThread:

		    7386.623 Mt   100.000 %       528 x       13.990 Mt       27.594 Mt      145.234 Mt     1.966 % 	 /

		       1.575 Mt     0.021 %         1 x        1.575 Mt        0.000 Mt        0.006 Mt     0.396 % 	 ./ScenePrep

		       1.568 Mt    99.604 %         1 x        1.568 Mt        0.000 Mt        0.002 Mt     0.102 % 	 ../AccelerationStructureBuild

		       1.480 Mt    94.385 %         1 x        1.480 Mt        0.000 Mt        1.480 Mt   100.000 % 	 .../BuildBottomLevelASGpu

		       0.086 Mt     5.513 %         1 x        0.086 Mt        0.000 Mt        0.086 Mt   100.000 % 	 .../BuildTopLevelASGpu

		    7238.086 Mt    97.989 %       528 x       13.708 Mt       27.590 Mt        1.784 Mt     0.025 % 	 ./RayTracingGpu

		    4141.007 Mt    57.211 %       528 x        7.843 Mt       24.989 Mt     4141.007 Mt   100.000 % 	 ../DeferredRLGpu

		    1001.224 Mt    13.833 %       528 x        1.896 Mt        2.051 Mt     1001.224 Mt   100.000 % 	 ../RayTracingRLGpu

		     320.132 Mt     4.423 %       528 x        0.606 Mt        0.548 Mt      320.132 Mt   100.000 % 	 ../ResolveRLGpu

		    1773.939 Mt    24.508 %        12 x      147.828 Mt        0.000 Mt        0.023 Mt     0.001 % 	 ../AccelerationStructureBuild

		    1772.455 Mt    99.916 %        12 x      147.705 Mt        0.000 Mt     1772.455 Mt   100.000 % 	 .../BuildBottomLevelASGpu

		       1.461 Mt     0.082 %        12 x        0.122 Mt        0.000 Mt        1.461 Mt   100.000 % 	 .../BuildTopLevelASGpu

		       1.729 Mt     0.023 %       528 x        0.003 Mt        0.002 Mt        1.729 Mt   100.000 % 	 ./Present


	============================


