Configuration: 
Command line parameters  = E:\programming\FIT-DIP\Masters-DXR-Main-git\build\Measurement\x64\Release\Measurement.exe --scene Sponza/Sponza.gltf --profile-automation --profile-camera-track Sponza/SponzaPreset.trk --width 2560 --height 1440 --profile-output PresetSponza1440RaMedium --quality-preset Medium 
Build type               = Release
Device name              = NVIDIA GeForce RTX 2080 Ti
Device memory MB         = 11049
Ray Tracing backend      = Hardware
Window width             = 2560
Window height            = 1421
Window title             = Direct3D App
Back-buffer count        = 3
Fill Triangles           = enabled
Triangles Font Clock-Wise= enabled
Fast Build AS            = disabled
Target FPS               = 0
Target UPS               = 60
Tearing                  = enabled
VSync                    = disabled
GUI rendering            = disabled
Debug layer              = disabled
GPU validation           = disabled
GPU profiling            = enabled
Render mode              = Rasterization
Loaded scene file        = Sponza/Sponza.gltf
Using testing scene      = disabled
Duplication              = 1
Duplication in cube      = disabled
Duplication offset       = 10
BLAS duplication         = disabled
Rays per pixel           = 8
Quality preset           = Medium
Rasterization Quality    : 
  AO                     = enabled
  AO kernel size         = 4
  Shadows                = enabled
  Shadow map resolution  = 4096
  Shadow PCF kernel size = 4
  Reflections            = disabled
Ray Tracing Quality      : 
  AO                     = enabled
  AO rays                = 4
  AO kernel size         = 4
  Shadows                = enabled
  Shadow rays            = 4
  Shadow kernel size     = 4
  Reflections            = disabled

ProfilingAggregator: 
Render	,	6.6913	,	6.4488	,	6.0174	,	5.3476	,	5.7124	,	5.8024	,	5.8942	,	6.2398	,	6.4547	,	5.8889	,	5.3039	,	5.7122	,	6.1343	,	6.3146	,	6.4587	,	5.9619	,	5.8826	,	5.7216	,	5.3719	,	6.657	,	8.5519	,	8.7415	,	8.6653
Update	,	0.08	,	0.0771	,	0.0767	,	0.0776	,	0.078	,	0.0759	,	0.0858	,	0.0745	,	0.0806	,	0.0735	,	0.0776	,	0.0774	,	0.08	,	0.0773	,	0.0779	,	0.0783	,	0.0782	,	0.1044	,	0.0752	,	0.0781	,	0.0757	,	0.0753	,	0.0755
DeferredRLGpu	,	2.6887	,	2.48496	,	2.17971	,	1.47514	,	1.3416	,	1.78966	,	1.9479	,	2.35414	,	2.03926	,	1.73283	,	1.3472	,	1.72906	,	2.14525	,	2.39715	,	2.17421	,	1.7096	,	1.94922	,	1.88829	,	1.48112	,	2.74698	,	3.89869	,	4.28803	,	4.31267
Index	,	0	,	1	,	2	,	3	,	4	,	5	,	6	,	7	,	8	,	9	,	10	,	11	,	12	,	13	,	14	,	15	,	16	,	17	,	18	,	19	,	20	,	21	,	22

ProfilingBuildAggregator: 
BuildBottomLevelAS	,	1.7293
BuildBottomLevelASGpu	,	2.35414
BuildTopLevelAS	,	0.8257
BuildTopLevelASGpu	,	0.090272
Duplication	,	1
Triangles	,	262267
Meshes	,	103
BottomLevels	,	1
TopLevelSize	,	64
BottomLevelsSize	,	18390656
Index	,	0

FpsAggregator: 
FPS	,	95	,	142	,	155	,	166	,	180	,	179	,	177	,	152	,	155	,	169	,	181	,	177	,	164	,	154	,	151	,	174	,	171	,	166	,	176	,	172	,	130	,	121	,	120
UPS	,	59	,	60	,	60	,	61	,	60	,	60	,	60	,	61	,	60	,	60	,	61	,	60	,	60	,	60	,	60	,	61	,	60	,	60	,	60	,	60	,	61	,	60	,	60
FrameTime	,	10.5276	,	7.04354	,	6.47959	,	6.05096	,	5.57609	,	5.60872	,	5.68234	,	6.60501	,	6.49085	,	5.94079	,	5.54556	,	5.65084	,	6.12651	,	6.51651	,	6.64676	,	5.77735	,	5.86075	,	6.03908	,	5.69889	,	5.85054	,	7.73323	,	8.29038	,	8.34043
GigaRays/s	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0
Index	,	0	,	1	,	2	,	3	,	4	,	5	,	6	,	7	,	8	,	9	,	10	,	11	,	12	,	13	,	14	,	15	,	16	,	17	,	18	,	19	,	20	,	21	,	22


AccelerationStructure: 
Ray tracing acceleration structure building statistics: 
	Bottom Level Acceleration Structure: 
		Maximum [B]: 18390656
		Minimum [B]: 18390656
		Currently Allocated [B]: 18390656
		Currently Created [num]: 1
		Current Average [B]: 18390656
		Maximum Triangles [num]: 262267
		Minimum Triangles [num]: 262267
		Total Triangles [num]: 262267
		Maximum Meshes [num]: 103
		Minimum Meshes [num]: 103
		Total Meshes [num]: 103
	Top Level Acceleration Structure: 
		Maximum [B]: 64
		Minimum [B]: 64
		Currently Allocated [B]: 64
		Total Allocated [B]: 64
		Total Created [num]: 1
		Average Allocated [B]: 64
		Maximum Instances [num]: 1
		Minimum Instances [num]: 1
		Total Instances [num]: 1
	Scratch Buffer: 
		Maximum [B]: 14995072
		Minimum [B]: 14995072
		Currently Allocated [B]: 14995072
		Total Allocated [B]: 14995072
		Total Created [num]: 1
		Average Allocated [B]: 14995072
ProfilingStack: 
[PROF] Profiling statistics: 
	Scopes entered : 77352
	Scopes exited : 77351
	Overhead per scope [ticks] : 129.1
	Threads entered : 3
	Threads exited : 0
	============================
		         Sum[Mt]   Parent[%]  Samples[num]       Avg[Mt]   Last/Curr[Mt]        Self[Mt]     Self[%] 	 Name

	main:

		   26137.420 Mt   100.000 %         1 x    26137.421 Mt    26137.420 Mt        0.479 Mt     0.002 % 	 /

		   26136.980 Mt    99.998 %         1 x    26136.982 Mt    26136.980 Mt      255.717 Mt     0.978 % 	 ./Main application loop

		       3.595 Mt     0.014 %         1 x        3.595 Mt        0.000 Mt        3.595 Mt   100.000 % 	 ../TexturedRLInitialization

		       1.348 Mt     0.005 %         1 x        1.348 Mt        0.000 Mt        1.348 Mt   100.000 % 	 ../ShadowMapRLInitialization

		       5.299 Mt     0.020 %         1 x        5.299 Mt        0.000 Mt        5.299 Mt   100.000 % 	 ../RasterResolveRLInitialization

		       3.809 Mt     0.015 %         1 x        3.809 Mt        0.000 Mt        3.809 Mt   100.000 % 	 ../ResolveRLInitialization

		   23107.090 Mt    88.408 %     35935 x        0.643 Mt        8.840 Mt      422.800 Mt     1.830 % 	 ../MainLoop

		     205.694 Mt     0.890 %      1386 x        0.148 Mt        0.047 Mt      205.065 Mt    99.694 % 	 .../Update

		       0.629 Mt     0.306 %         1 x        0.629 Mt        0.000 Mt        0.629 Mt   100.000 % 	 ..../GuiModelApply

		   22478.597 Mt    97.280 %      3629 x        6.194 Mt        8.698 Mt      409.491 Mt     1.822 % 	 .../Render

		     617.340 Mt     2.746 %      3629 x        0.170 Mt        0.183 Mt      611.684 Mt    99.084 % 	 ..../Rasterization

		       3.552 Mt     0.575 %      3629 x        0.001 Mt        0.001 Mt        3.552 Mt   100.000 % 	 ...../DeferredRLPrep

		       2.104 Mt     0.341 %      3629 x        0.001 Mt        0.001 Mt        2.104 Mt   100.000 % 	 ...../ShadowMapRLPrep

		   21451.766 Mt    95.432 %      3629 x        5.911 Mt        8.387 Mt    21451.766 Mt   100.000 % 	 ..../Present

		       3.460 Mt     0.013 %         1 x        3.460 Mt        0.000 Mt        3.460 Mt   100.000 % 	 ../AmbientOcclusionRLInitialization

		       5.512 Mt     0.021 %         1 x        5.512 Mt        0.000 Mt        5.512 Mt   100.000 % 	 ../DeferredRLInitialization

		      52.254 Mt     0.200 %         1 x       52.254 Mt        0.000 Mt        2.340 Mt     4.477 % 	 ../RayTracingRLInitialization

		      49.914 Mt    95.523 %         1 x       49.914 Mt        0.000 Mt       49.914 Mt   100.000 % 	 .../PipelineBuild

		    2698.896 Mt    10.326 %         1 x     2698.896 Mt        0.000 Mt        0.447 Mt     0.017 % 	 ../Initialization

		    2698.448 Mt    99.983 %         1 x     2698.448 Mt        0.000 Mt        2.163 Mt     0.080 % 	 .../ScenePrep

		    2625.426 Mt    97.294 %         1 x     2625.426 Mt        0.000 Mt      253.530 Mt     9.657 % 	 ..../SceneLoad

		    2081.433 Mt    79.280 %         1 x     2081.433 Mt        0.000 Mt      583.200 Mt    28.019 % 	 ...../glTF-LoadScene

		    1498.233 Mt    71.981 %        69 x       21.714 Mt        0.000 Mt     1498.233 Mt   100.000 % 	 ....../glTF-LoadImageData

		     229.173 Mt     8.729 %         1 x      229.173 Mt        0.000 Mt      229.173 Mt   100.000 % 	 ...../glTF-CreateScene

		      61.291 Mt     2.335 %         1 x       61.291 Mt        0.000 Mt       61.291 Mt   100.000 % 	 ...../SceneDesc-CreateScene

		       1.180 Mt     0.044 %         1 x        1.180 Mt        0.000 Mt        0.001 Mt     0.051 % 	 ..../ShadowMapRLPrep

		       1.179 Mt    99.949 %         1 x        1.179 Mt        0.000 Mt        1.143 Mt    96.973 % 	 ...../PrepareForRendering

		       0.036 Mt     3.027 %         1 x        0.036 Mt        0.000 Mt        0.036 Mt   100.000 % 	 ....../PrepareDrawBundle

		       3.524 Mt     0.131 %         1 x        3.524 Mt        0.000 Mt        0.000 Mt     0.011 % 	 ..../TexturedRLPrep

		       3.524 Mt    99.989 %         1 x        3.524 Mt        0.000 Mt        3.486 Mt    98.907 % 	 ...../PrepareForRendering

		       0.001 Mt     0.031 %         1 x        0.001 Mt        0.000 Mt        0.001 Mt   100.000 % 	 ....../PrepareMaterials

		       0.037 Mt     1.061 %         1 x        0.037 Mt        0.000 Mt        0.037 Mt   100.000 % 	 ....../PrepareDrawBundle

		       5.228 Mt     0.194 %         1 x        5.228 Mt        0.000 Mt        0.000 Mt     0.010 % 	 ..../DeferredRLPrep

		       5.228 Mt    99.990 %         1 x        5.228 Mt        0.000 Mt        4.692 Mt    89.766 % 	 ...../PrepareForRendering

		       0.001 Mt     0.011 %         1 x        0.001 Mt        0.000 Mt        0.001 Mt   100.000 % 	 ....../PrepareMaterials

		       0.453 Mt     8.662 %         1 x        0.453 Mt        0.000 Mt        0.453 Mt   100.000 % 	 ....../BuildMaterialCache

		       0.082 Mt     1.561 %         1 x        0.082 Mt        0.000 Mt        0.082 Mt   100.000 % 	 ....../PrepareDrawBundle

		      26.939 Mt     0.998 %         1 x       26.939 Mt        0.000 Mt        2.623 Mt     9.736 % 	 ..../RayTracingRLPrep

		       0.064 Mt     0.237 %         1 x        0.064 Mt        0.000 Mt        0.064 Mt   100.000 % 	 ...../PrepareAccelerationStructures

		       0.001 Mt     0.003 %         1 x        0.001 Mt        0.000 Mt        0.001 Mt   100.000 % 	 ...../PrepareCache

		      12.958 Mt    48.101 %         1 x       12.958 Mt        0.000 Mt       12.958 Mt   100.000 % 	 ...../PipelineBuild

		       0.692 Mt     2.568 %         1 x        0.692 Mt        0.000 Mt        0.692 Mt   100.000 % 	 ...../BuildCache

		       5.814 Mt    21.581 %         1 x        5.814 Mt        0.000 Mt        0.001 Mt     0.010 % 	 ...../BuildAccelerationStructures

		       5.813 Mt    99.990 %         1 x        5.813 Mt        0.000 Mt        3.258 Mt    56.049 % 	 ....../AccelerationStructureBuild

		       1.729 Mt    29.747 %         1 x        1.729 Mt        0.000 Mt        1.729 Mt   100.000 % 	 ......./BuildBottomLevelAS

		       0.826 Mt    14.204 %         1 x        0.826 Mt        0.000 Mt        0.826 Mt   100.000 % 	 ......./BuildTopLevelAS

		       4.788 Mt    17.774 %         1 x        4.788 Mt        0.000 Mt        4.788 Mt   100.000 % 	 ...../BuildShaderTables

		      33.988 Mt     1.260 %         1 x       33.988 Mt        0.000 Mt       33.988 Mt   100.000 % 	 ..../GpuSidePrep

	WindowThread:

		   26135.812 Mt   100.000 %         1 x    26135.812 Mt    26135.812 Mt    26135.812 Mt   100.000 % 	 /

	GpuThread:

		   19563.592 Mt   100.000 %      3629 x        5.391 Mt        7.712 Mt      165.613 Mt     0.847 % 	 /

		       2.452 Mt     0.013 %         1 x        2.452 Mt        0.000 Mt        0.007 Mt     0.269 % 	 ./ScenePrep

		       2.446 Mt    99.731 %         1 x        2.446 Mt        0.000 Mt        0.001 Mt     0.060 % 	 ../AccelerationStructureBuild

		       2.354 Mt    96.249 %         1 x        2.354 Mt        0.000 Mt        2.354 Mt   100.000 % 	 .../BuildBottomLevelASGpu

		       0.090 Mt     3.691 %         1 x        0.090 Mt        0.000 Mt        0.090 Mt   100.000 % 	 .../BuildTopLevelASGpu

		   19307.820 Mt    98.693 %      3629 x        5.320 Mt        7.338 Mt       14.589 Mt     0.076 % 	 ./RasterizationGpu

		    7712.579 Mt    39.945 %      3629 x        2.125 Mt        3.966 Mt     7712.579 Mt   100.000 % 	 ../DeferredRLGpu

		     580.366 Mt     3.006 %      3629 x        0.160 Mt        0.231 Mt      580.366 Mt   100.000 % 	 ../ShadowMapRLGpu

		    2314.004 Mt    11.985 %      3629 x        0.638 Mt        0.618 Mt     2314.004 Mt   100.000 % 	 ../AmbientOcclusionRLGpu

		    8686.281 Mt    44.988 %      3629 x        2.394 Mt        2.521 Mt     8686.281 Mt   100.000 % 	 ../RasterResolveRLGpu

		      87.706 Mt     0.448 %      3629 x        0.024 Mt        0.374 Mt       87.706 Mt   100.000 % 	 ./Present


	============================


