Configuration: 
Command line parameters  = E:\programming\FIT-DIP\Masters-DXR-Main-git\build\Measurement\x64\Release\Measurement.exe --scene Cube/Cube.gltf --profile-automation --profile-camera-track Cube/Cube.trk --width 2560 --height 1440 --profile-output PresetCube1440RaMedium --quality-preset Medium 
Build type               = Release
Device name              = NVIDIA GeForce RTX 2080 Ti
Device memory MB         = 11049
Ray Tracing backend      = Hardware
Window width             = 2560
Window height            = 1421
Window title             = Direct3D App
Back-buffer count        = 3
Fill Triangles           = enabled
Triangles Font Clock-Wise= enabled
Fast Build AS            = disabled
Target FPS               = 0
Target UPS               = 60
Tearing                  = enabled
VSync                    = disabled
GUI rendering            = disabled
Debug layer              = disabled
GPU validation           = disabled
GPU profiling            = enabled
Render mode              = Rasterization
Loaded scene file        = Cube/Cube.gltf
Using testing scene      = disabled
Duplication              = 1
Duplication in cube      = disabled
Duplication offset       = 10
BLAS duplication         = disabled
Rays per pixel           = 8
Quality preset           = Medium
Rasterization Quality    : 
  AO                     = enabled
  AO kernel size         = 4
  Shadows                = enabled
  Shadow map resolution  = 4096
  Shadow PCF kernel size = 4
  Reflections            = disabled
Ray Tracing Quality      : 
  AO                     = enabled
  AO rays                = 4
  AO kernel size         = 4
  Shadows                = enabled
  Shadow rays            = 4
  Shadow kernel size     = 4
  Reflections            = disabled

ProfilingAggregator: 
Render	,	3.6423	,	3.7352	,	3.7258	,	3.6713	,	3.7226	,	4.2636	,	3.9492	,	4.016	,	4.1237	,	3.9808	,	3.8513	,	3.8866	,	3.8711	,	3.7548	,	4.2522	,	4.204	,	4.06	,	4.0811	,	4.1047	,	3.9907	,	4.0951	,	3.7642	,	3.6685	,	3.4966	,	3.5526	,	3.4157	,	3.8711	,	3.4524	,	3.3519
Update	,	0.0778	,	0.0756	,	0.079	,	0.0791	,	0.078	,	0.0784	,	0.0768	,	0.0801	,	0.0778	,	0.0935	,	0.0775	,	0.0841	,	0.0782	,	0.0771	,	0.0791	,	0.0836	,	0.0769	,	0.0789	,	0.0825	,	0.0786	,	0.0781	,	0.0772	,	0.0774	,	0.0733	,	0.0787	,	0.0798	,	0.078	,	0.0771	,	0.0786
DeferredRLGpu	,	0.182112	,	0.243008	,	0.256096	,	0.262784	,	0.277856	,	0.336896	,	0.312448	,	0.355744	,	0.415232	,	0.369056	,	0.322752	,	0.351712	,	0.281344	,	0.254112	,	0.288704	,	0.337888	,	0.396864	,	0.467936	,	0.488736	,	0.488896	,	0.41312	,	0.286784	,	0.235392	,	0.183072	,	0.152736	,	0.119296	,	0.091136	,	0.07504	,	0.064896
Index	,	0	,	1	,	2	,	3	,	4	,	5	,	6	,	7	,	8	,	9	,	10	,	11	,	12	,	13	,	14	,	15	,	16	,	17	,	18	,	19	,	20	,	21	,	22	,	23	,	24	,	25	,	26	,	27	,	28

ProfilingBuildAggregator: 
BuildBottomLevelAS	,	0.7674
BuildBottomLevelASGpu	,	0.127712
BuildTopLevelAS	,	0.809
BuildTopLevelASGpu	,	0.059648
Duplication	,	1
Triangles	,	12
Meshes	,	1
BottomLevels	,	1
TopLevelSize	,	64
BottomLevelsSize	,	4480
Index	,	0

FpsAggregator: 
FPS	,	213	,	255	,	257	,	257	,	253	,	246	,	245	,	247	,	239	,	239	,	247	,	244	,	248	,	258	,	254	,	250	,	248	,	238	,	236	,	236	,	235	,	251	,	258	,	269	,	269	,	275	,	280	,	285	,	288
UPS	,	59	,	61	,	60	,	60	,	60	,	60	,	60	,	60	,	61	,	60	,	60	,	60	,	60	,	60	,	60	,	61	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	61	,	60	,	60	,	60	,	60
FrameTime	,	4.70937	,	3.9278	,	3.90467	,	3.89419	,	3.95847	,	4.0763	,	4.09605	,	4.05406	,	4.197	,	4.19837	,	4.05336	,	4.11264	,	4.04381	,	3.87725	,	3.94889	,	4.01179	,	4.04666	,	4.21361	,	4.24037	,	4.23986	,	4.25808	,	3.99519	,	3.88129	,	3.72417	,	3.7299	,	3.64852	,	3.57832	,	3.5197	,	3.48239
GigaRays/s	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0
Index	,	0	,	1	,	2	,	3	,	4	,	5	,	6	,	7	,	8	,	9	,	10	,	11	,	12	,	13	,	14	,	15	,	16	,	17	,	18	,	19	,	20	,	21	,	22	,	23	,	24	,	25	,	26	,	27	,	28


AccelerationStructure: 
Ray tracing acceleration structure building statistics: 
	Bottom Level Acceleration Structure: 
		Maximum [B]: 4480
		Minimum [B]: 4480
		Currently Allocated [B]: 4480
		Currently Created [num]: 1
		Current Average [B]: 4480
		Maximum Triangles [num]: 12
		Minimum Triangles [num]: 12
		Total Triangles [num]: 12
		Maximum Meshes [num]: 1
		Minimum Meshes [num]: 1
		Total Meshes [num]: 1
	Top Level Acceleration Structure: 
		Maximum [B]: 64
		Minimum [B]: 64
		Currently Allocated [B]: 64
		Total Allocated [B]: 64
		Total Created [num]: 1
		Average Allocated [B]: 64
		Maximum Instances [num]: 1
		Minimum Instances [num]: 1
		Total Instances [num]: 1
	Scratch Buffer: 
		Maximum [B]: 896
		Minimum [B]: 896
		Currently Allocated [B]: 896
		Total Allocated [B]: 896
		Total Created [num]: 1
		Average Allocated [B]: 896
ProfilingStack: 
[PROF] Profiling statistics: 
	Scopes entered : 174603
	Scopes exited : 174602
	Overhead per scope [ticks] : 103.5
	Threads entered : 3
	Threads exited : 0
	============================
		         Sum[Mt]   Parent[%]  Samples[num]       Avg[Mt]   Last/Curr[Mt]        Self[Mt]     Self[%] 	 Name

	main:

		   29533.564 Mt   100.000 %         1 x    29533.565 Mt    29533.564 Mt        0.322 Mt     0.001 % 	 /

		   29533.275 Mt    99.999 %         1 x    29533.276 Mt    29533.275 Mt      318.792 Mt     1.079 % 	 ./Main application loop

		       2.273 Mt     0.008 %         1 x        2.273 Mt        0.000 Mt        2.273 Mt   100.000 % 	 ../TexturedRLInitialization

		       1.325 Mt     0.004 %         1 x        1.325 Mt        0.000 Mt        1.325 Mt   100.000 % 	 ../ShadowMapRLInitialization

		       3.889 Mt     0.013 %         1 x        3.889 Mt        0.000 Mt        3.889 Mt   100.000 % 	 ../RasterResolveRLInitialization

		       3.836 Mt     0.013 %         1 x        3.836 Mt        0.000 Mt        3.836 Mt   100.000 % 	 ../ResolveRLInitialization

		   29077.486 Mt    98.457 %     92283 x        0.315 Mt       17.149 Mt      427.828 Mt     1.471 % 	 ../MainLoop

		     218.425 Mt     0.751 %      1744 x        0.125 Mt        0.089 Mt      217.828 Mt    99.727 % 	 .../Update

		       0.597 Mt     0.273 %         1 x        0.597 Mt        0.000 Mt        0.597 Mt   100.000 % 	 ..../GuiModelApply

		   28431.232 Mt    97.777 %      7321 x        3.884 Mt        3.525 Mt      801.174 Mt     2.818 % 	 .../Render

		    1234.762 Mt     4.343 %      7321 x        0.169 Mt        0.212 Mt     1222.017 Mt    98.968 % 	 ..../Rasterization

		       8.440 Mt     0.684 %      7321 x        0.001 Mt        0.001 Mt        8.440 Mt   100.000 % 	 ...../DeferredRLPrep

		       4.305 Mt     0.349 %      7321 x        0.001 Mt        0.001 Mt        4.305 Mt   100.000 % 	 ...../ShadowMapRLPrep

		   26395.296 Mt    92.839 %      7321 x        3.605 Mt        3.158 Mt    26395.296 Mt   100.000 % 	 ..../Present

		       3.136 Mt     0.011 %         1 x        3.136 Mt        0.000 Mt        3.136 Mt   100.000 % 	 ../AmbientOcclusionRLInitialization

		       4.766 Mt     0.016 %         1 x        4.766 Mt        0.000 Mt        4.766 Mt   100.000 % 	 ../DeferredRLInitialization

		      52.514 Mt     0.178 %         1 x       52.514 Mt        0.000 Mt        2.372 Mt     4.517 % 	 ../RayTracingRLInitialization

		      50.142 Mt    95.483 %         1 x       50.142 Mt        0.000 Mt       50.142 Mt   100.000 % 	 .../PipelineBuild

		      65.259 Mt     0.221 %         1 x       65.259 Mt        0.000 Mt        0.192 Mt     0.295 % 	 ../Initialization

		      65.066 Mt    99.705 %         1 x       65.066 Mt        0.000 Mt        0.895 Mt     1.376 % 	 .../ScenePrep

		      22.726 Mt    34.927 %         1 x       22.726 Mt        0.000 Mt        6.353 Mt    27.955 % 	 ..../SceneLoad

		      10.790 Mt    47.477 %         1 x       10.790 Mt        0.000 Mt        2.755 Mt    25.531 % 	 ...../glTF-LoadScene

		       8.035 Mt    74.469 %         2 x        4.017 Mt        0.000 Mt        8.035 Mt   100.000 % 	 ....../glTF-LoadImageData

		       3.830 Mt    16.852 %         1 x        3.830 Mt        0.000 Mt        3.830 Mt   100.000 % 	 ...../glTF-CreateScene

		       1.754 Mt     7.717 %         1 x        1.754 Mt        0.000 Mt        1.754 Mt   100.000 % 	 ...../SceneDesc-CreateScene

		       3.366 Mt     5.173 %         1 x        3.366 Mt        0.000 Mt        0.000 Mt     0.015 % 	 ..../ShadowMapRLPrep

		       3.365 Mt    99.985 %         1 x        3.365 Mt        0.000 Mt        3.358 Mt    99.786 % 	 ...../PrepareForRendering

		       0.007 Mt     0.214 %         1 x        0.007 Mt        0.000 Mt        0.007 Mt   100.000 % 	 ....../PrepareDrawBundle

		       1.422 Mt     2.186 %         1 x        1.422 Mt        0.000 Mt        0.000 Mt     0.021 % 	 ..../TexturedRLPrep

		       1.422 Mt    99.979 %         1 x        1.422 Mt        0.000 Mt        1.418 Mt    99.740 % 	 ...../PrepareForRendering

		       0.001 Mt     0.049 %         1 x        0.001 Mt        0.000 Mt        0.001 Mt   100.000 % 	 ....../PrepareMaterials

		       0.003 Mt     0.211 %         1 x        0.003 Mt        0.000 Mt        0.003 Mt   100.000 % 	 ....../PrepareDrawBundle

		       1.360 Mt     2.090 %         1 x        1.360 Mt        0.000 Mt        0.000 Mt     0.029 % 	 ..../DeferredRLPrep

		       1.360 Mt    99.971 %         1 x        1.360 Mt        0.000 Mt        1.055 Mt    77.600 % 	 ...../PrepareForRendering

		       0.001 Mt     0.037 %         1 x        0.001 Mt        0.000 Mt        0.001 Mt   100.000 % 	 ....../PrepareMaterials

		       0.301 Mt    22.143 %         1 x        0.301 Mt        0.000 Mt        0.301 Mt   100.000 % 	 ....../BuildMaterialCache

		       0.003 Mt     0.221 %         1 x        0.003 Mt        0.000 Mt        0.003 Mt   100.000 % 	 ....../PrepareDrawBundle

		      24.392 Mt    37.487 %         1 x       24.392 Mt        0.000 Mt        2.527 Mt    10.359 % 	 ..../RayTracingRLPrep

		       0.041 Mt     0.170 %         1 x        0.041 Mt        0.000 Mt        0.041 Mt   100.000 % 	 ...../PrepareAccelerationStructures

		       0.001 Mt     0.003 %         1 x        0.001 Mt        0.000 Mt        0.001 Mt   100.000 % 	 ...../PrepareCache

		      12.945 Mt    53.073 %         1 x       12.945 Mt        0.000 Mt       12.945 Mt   100.000 % 	 ...../PipelineBuild

		       0.327 Mt     1.339 %         1 x        0.327 Mt        0.000 Mt        0.327 Mt   100.000 % 	 ...../BuildCache

		       3.910 Mt    16.029 %         1 x        3.910 Mt        0.000 Mt        0.000 Mt     0.010 % 	 ...../BuildAccelerationStructures

		       3.909 Mt    99.990 %         1 x        3.909 Mt        0.000 Mt        2.333 Mt    59.676 % 	 ....../AccelerationStructureBuild

		       0.767 Mt    19.630 %         1 x        0.767 Mt        0.000 Mt        0.767 Mt   100.000 % 	 ......./BuildBottomLevelAS

		       0.809 Mt    20.694 %         1 x        0.809 Mt        0.000 Mt        0.809 Mt   100.000 % 	 ......./BuildTopLevelAS

		       4.641 Mt    19.027 %         1 x        4.641 Mt        0.000 Mt        4.641 Mt   100.000 % 	 ...../BuildShaderTables

		      10.905 Mt    16.761 %         1 x       10.905 Mt        0.000 Mt       10.905 Mt   100.000 % 	 ..../GpuSidePrep

	WindowThread:

		   29532.134 Mt   100.000 %         1 x    29532.135 Mt    29532.134 Mt    29532.134 Mt   100.000 % 	 /

	GpuThread:

		   22604.348 Mt   100.000 %      7321 x        3.088 Mt        2.555 Mt      129.934 Mt     0.575 % 	 /

		       0.194 Mt     0.001 %         1 x        0.194 Mt        0.000 Mt        0.006 Mt     3.165 % 	 ./ScenePrep

		       0.188 Mt    96.835 %         1 x        0.188 Mt        0.000 Mt        0.001 Mt     0.340 % 	 ../AccelerationStructureBuild

		       0.128 Mt    67.932 %         1 x        0.128 Mt        0.000 Mt        0.128 Mt   100.000 % 	 .../BuildBottomLevelASGpu

		       0.060 Mt    31.728 %         1 x        0.060 Mt        0.000 Mt        0.060 Mt   100.000 % 	 .../BuildTopLevelASGpu

		   22318.431 Mt    98.735 %      7321 x        3.049 Mt        2.551 Mt       24.603 Mt     0.110 % 	 ./RasterizationGpu

		    2069.285 Mt     9.272 %      7321 x        0.283 Mt        0.064 Mt     2069.285 Mt   100.000 % 	 ../DeferredRLGpu

		      68.970 Mt     0.309 %      7321 x        0.009 Mt        0.009 Mt       68.970 Mt   100.000 % 	 ../ShadowMapRLGpu

		    4155.237 Mt    18.618 %      7321 x        0.568 Mt        0.456 Mt     4155.237 Mt   100.000 % 	 ../AmbientOcclusionRLGpu

		   16000.336 Mt    71.691 %      7321 x        2.186 Mt        2.020 Mt    16000.336 Mt   100.000 % 	 ../RasterResolveRLGpu

		     155.788 Mt     0.689 %      7321 x        0.021 Mt        0.002 Mt      155.788 Mt   100.000 % 	 ./Present


	============================


