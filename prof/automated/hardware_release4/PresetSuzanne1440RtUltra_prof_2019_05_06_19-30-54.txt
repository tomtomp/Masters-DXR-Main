Configuration: 
Command line parameters  = E:\programming\FIT-DIP\Masters-DXR-Main-git\build\Measurement\x64\Release\Measurement.exe --scene Suzanne/Suzanne.gltf --profile-automation --profile-camera-track Suzanne/Suzanne.trk --rt-mode --width 2560 --height 1440 --profile-output PresetSuzanne1440RtUltra --quality-preset Ultra 
Build type               = Release
Device name              = NVIDIA GeForce RTX 2080 Ti
Device memory MB         = 11049
Ray Tracing backend      = Hardware
Window width             = 2560
Window height            = 1421
Window title             = Direct3D App
Back-buffer count        = 3
Fill Triangles           = enabled
Triangles Font Clock-Wise= enabled
Fast Build AS            = disabled
Target FPS               = 0
Target UPS               = 60
Tearing                  = enabled
VSync                    = disabled
GUI rendering            = disabled
Debug layer              = disabled
GPU validation           = disabled
GPU profiling            = enabled
Render mode              = Ray Tracing
Loaded scene file        = Suzanne/Suzanne.gltf
Using testing scene      = disabled
Duplication              = 1
Duplication in cube      = disabled
Duplication offset       = 10
BLAS duplication         = disabled
Rays per pixel           = 33
Quality preset           = Ultra
Rasterization Quality    : 
  AO                     = enabled
  AO kernel size         = 16
  Shadows                = enabled
  Shadow map resolution  = 8192
  Shadow PCF kernel size = 8
  Reflections            = enabled
Ray Tracing Quality      : 
  AO                     = enabled
  AO rays                = 16
  AO kernel size         = 8
  Shadows                = enabled
  Shadow rays            = 8
  Shadow kernel size     = 8
  Reflections            = enabled

ProfilingAggregator: 
Render	,	8.9723	,	9.692	,	10.7173	,	10.0697	,	8.933	,	8.3757	,	8.1804	,	8.1168	,	8.1493	,	8.8663	,	9.3578	,	8.9756	,	8.5759	,	8.1794	,	8.6113	,	10.6668	,	11.7124	,	13.4614	,	16.5526	,	12.8975	,	11.2477	,	9.3228	,	8.6654	,	8.5969
Update	,	0.0756	,	0.0759	,	0.0791	,	0.0715	,	0.0779	,	0.0943	,	0.0791	,	0.0806	,	0.0909	,	0.0821	,	0.0802	,	0.0841	,	0.0786	,	0.0801	,	0.0938	,	0.0758	,	0.0798	,	0.0743	,	0.0778	,	0.0806	,	0.0767	,	0.079	,	0.077	,	0.0746
RayTracing	,	0.2267	,	0.2257	,	0.1987	,	0.177	,	0.2206	,	0.1686	,	0.1736	,	0.1854	,	0.179	,	0.1888	,	0.1735	,	0.1869	,	0.1762	,	0.1868	,	0.1801	,	0.1884	,	0.1811	,	0.1798	,	0.1927	,	0.1825	,	0.1764	,	0.2011	,	0.1727	,	0.1727
RayTracingGpu	,	8.07786	,	8.80634	,	9.63322	,	8.95405	,	7.99955	,	7.53869	,	7.40806	,	7.31309	,	7.37382	,	8.10803	,	8.5463	,	7.83965	,	7.3848	,	7.26086	,	7.74931	,	9.36211	,	10.7305	,	12.5102	,	15.5598	,	12.0122	,	10.1963	,	8.34445	,	7.77136	,	7.7009
DeferredRLGpu	,	0.137952	,	0.18448	,	0.20016	,	0.153312	,	0.104384	,	0.07056	,	0.056512	,	0.045888	,	0.062912	,	0.094208	,	0.106048	,	0.08176	,	0.05472	,	0.057344	,	0.092704	,	0.18832	,	0.229792	,	0.264576	,	0.408416	,	0.321344	,	0.268352	,	0.134176	,	0.080608	,	0.078816
RayTracingRLGpu	,	1.13318	,	1.77018	,	2.21085	,	1.86026	,	1.11354	,	0.610464	,	0.436096	,	0.35472	,	0.557696	,	1.032	,	1.27904	,	0.869184	,	0.458848	,	0.46432	,	0.872256	,	2.14928	,	3.14211	,	5.00682	,	7.79917	,	4.2087	,	2.78419	,	1.06586	,	0.572064	,	0.48624
ResolveRLGpu	,	6.80509	,	6.84826	,	7.2191	,	6.93862	,	6.77827	,	6.85629	,	6.91258	,	6.91088	,	6.75152	,	6.98006	,	7.15949	,	6.88726	,	6.86893	,	6.73702	,	6.78118	,	7.02198	,	7.35648	,	7.23667	,	7.34947	,	7.47901	,	7.14074	,	7.14189	,	7.11677	,	7.13418
Index	,	0	,	1	,	2	,	3	,	4	,	5	,	6	,	7	,	8	,	9	,	10	,	11	,	12	,	13	,	14	,	15	,	16	,	17	,	18	,	19	,	20	,	21	,	22	,	23

ProfilingBuildAggregator: 
BuildBottomLevelAS	,	0.8998
BuildBottomLevelASGpu	,	0.442496
BuildTopLevelAS	,	0.8362
BuildTopLevelASGpu	,	0.062016
Duplication	,	1
Triangles	,	3936
Meshes	,	1
BottomLevels	,	1
TopLevelSize	,	64
BottomLevelsSize	,	282112
Index	,	0

FpsAggregator: 
FPS	,	88	,	101	,	97	,	96	,	104	,	113	,	118	,	121	,	119	,	114	,	107	,	109	,	116	,	119	,	117	,	104	,	90	,	82	,	66	,	70	,	83	,	101	,	114	,	117
UPS	,	59	,	60	,	61	,	60	,	60	,	61	,	60	,	60	,	60	,	60	,	60	,	60	,	61	,	60	,	60	,	60	,	60	,	61	,	60	,	60	,	61	,	60	,	61	,	60
FrameTime	,	11.4051	,	9.90957	,	10.3879	,	10.462	,	9.67869	,	8.89569	,	8.47933	,	8.27026	,	8.41397	,	8.81386	,	9.36826	,	9.20823	,	8.62596	,	8.45536	,	8.57141	,	9.65268	,	11.1225	,	12.2952	,	15.1984	,	14.4047	,	12.1492	,	9.98328	,	8.81524	,	8.56545
GigaRays/s	,	10.5256	,	12.1142	,	11.5563	,	11.4745	,	12.4031	,	13.4949	,	14.1575	,	14.5154	,	14.2675	,	13.6202	,	12.8141	,	13.0368	,	13.9168	,	14.1976	,	14.0054	,	12.4365	,	10.793	,	9.76362	,	7.89859	,	8.3338	,	9.88098	,	12.0247	,	13.618	,	14.0151
Index	,	0	,	1	,	2	,	3	,	4	,	5	,	6	,	7	,	8	,	9	,	10	,	11	,	12	,	13	,	14	,	15	,	16	,	17	,	18	,	19	,	20	,	21	,	22	,	23


AccelerationStructure: 
Ray tracing acceleration structure building statistics: 
	Bottom Level Acceleration Structure: 
		Maximum [B]: 282112
		Minimum [B]: 282112
		Currently Allocated [B]: 282112
		Currently Created [num]: 1
		Current Average [B]: 282112
		Maximum Triangles [num]: 3936
		Minimum Triangles [num]: 3936
		Total Triangles [num]: 3936
		Maximum Meshes [num]: 1
		Minimum Meshes [num]: 1
		Total Meshes [num]: 1
	Top Level Acceleration Structure: 
		Maximum [B]: 64
		Minimum [B]: 64
		Currently Allocated [B]: 64
		Total Allocated [B]: 64
		Total Created [num]: 1
		Average Allocated [B]: 64
		Maximum Instances [num]: 1
		Minimum Instances [num]: 1
		Total Instances [num]: 1
	Scratch Buffer: 
		Maximum [B]: 82944
		Minimum [B]: 82944
		Currently Allocated [B]: 82944
		Total Allocated [B]: 82944
		Total Created [num]: 1
		Average Allocated [B]: 82944
ProfilingStack: 
[PROF] Profiling statistics: 
	Scopes entered : 55559
	Scopes exited : 55558
	Overhead per scope [ticks] : 110.9
	Threads entered : 3
	Threads exited : 0
	============================
		         Sum[Mt]   Parent[%]  Samples[num]       Avg[Mt]   Last/Curr[Mt]        Self[Mt]     Self[%] 	 Name

	main:

		   24594.382 Mt   100.000 %         1 x    24594.383 Mt    24594.382 Mt        0.421 Mt     0.002 % 	 /

		   24593.992 Mt    99.998 %         1 x    24593.993 Mt    24593.992 Mt      273.887 Mt     1.114 % 	 ./Main application loop

		       3.669 Mt     0.015 %         1 x        3.669 Mt        0.000 Mt        3.669 Mt   100.000 % 	 ../TexturedRLInitialization

		       1.365 Mt     0.006 %         1 x        1.365 Mt        0.000 Mt        1.365 Mt   100.000 % 	 ../ShadowMapRLInitialization

		       4.763 Mt     0.019 %         1 x        4.763 Mt        0.000 Mt        4.763 Mt   100.000 % 	 ../RasterResolveRLInitialization

		       3.617 Mt     0.015 %         1 x        3.617 Mt        0.000 Mt        3.617 Mt   100.000 % 	 ../ResolveRLInitialization

		   24127.899 Mt    98.105 %     21983 x        1.098 Mt        8.470 Mt      377.973 Mt     1.567 % 	 ../MainLoop

		     199.852 Mt     0.828 %      1447 x        0.138 Mt        0.040 Mt      199.496 Mt    99.822 % 	 .../Update

		       0.355 Mt     0.178 %         1 x        0.355 Mt        0.000 Mt        0.355 Mt   100.000 % 	 ..../GuiModelApply

		   23550.075 Mt    97.605 %      2468 x        9.542 Mt        8.336 Mt      284.453 Mt     1.208 % 	 .../Render

		     455.776 Mt     1.935 %      2468 x        0.185 Mt        0.208 Mt       17.983 Mt     3.946 % 	 ..../RayTracing

		     204.174 Mt    44.797 %      2468 x        0.083 Mt        0.091 Mt      201.876 Mt    98.874 % 	 ...../Deferred

		       2.299 Mt     1.126 %      2468 x        0.001 Mt        0.001 Mt        2.299 Mt   100.000 % 	 ....../DeferredRLPrep

		     162.927 Mt    35.747 %      2468 x        0.066 Mt        0.078 Mt      161.499 Mt    99.124 % 	 ...../RayCasting

		       1.428 Mt     0.876 %      2468 x        0.001 Mt        0.001 Mt        1.428 Mt   100.000 % 	 ....../RayTracingRLPrep

		      70.692 Mt    15.510 %      2468 x        0.029 Mt        0.032 Mt       70.692 Mt   100.000 % 	 ...../Resolve

		   22809.845 Mt    96.857 %      2468 x        9.242 Mt        7.996 Mt    22809.845 Mt   100.000 % 	 ..../Present

		       2.758 Mt     0.011 %         1 x        2.758 Mt        0.000 Mt        2.758 Mt   100.000 % 	 ../AmbientOcclusionRLInitialization

		       4.950 Mt     0.020 %         1 x        4.950 Mt        0.000 Mt        4.950 Mt   100.000 % 	 ../DeferredRLInitialization

		      67.254 Mt     0.273 %         1 x       67.254 Mt        0.000 Mt        2.347 Mt     3.489 % 	 ../RayTracingRLInitialization

		      64.908 Mt    96.511 %         1 x       64.908 Mt        0.000 Mt       64.908 Mt   100.000 % 	 .../PipelineBuild

		     103.830 Mt     0.422 %         1 x      103.830 Mt        0.000 Mt        0.184 Mt     0.177 % 	 ../Initialization

		     103.646 Mt    99.823 %         1 x      103.646 Mt        0.000 Mt        2.271 Mt     2.191 % 	 .../ScenePrep

		      62.258 Mt    60.068 %         1 x       62.258 Mt        0.000 Mt       12.889 Mt    20.703 % 	 ..../SceneLoad

		      40.706 Mt    65.383 %         1 x       40.706 Mt        0.000 Mt        9.871 Mt    24.249 % 	 ...../glTF-LoadScene

		      30.835 Mt    75.751 %         2 x       15.418 Mt        0.000 Mt       30.835 Mt   100.000 % 	 ....../glTF-LoadImageData

		       6.829 Mt    10.969 %         1 x        6.829 Mt        0.000 Mt        6.829 Mt   100.000 % 	 ...../glTF-CreateScene

		       1.833 Mt     2.945 %         1 x        1.833 Mt        0.000 Mt        1.833 Mt   100.000 % 	 ...../SceneDesc-CreateScene

		       0.277 Mt     0.267 %         1 x        0.277 Mt        0.000 Mt        0.000 Mt     0.145 % 	 ..../ShadowMapRLPrep

		       0.276 Mt    99.855 %         1 x        0.276 Mt        0.000 Mt        0.268 Mt    96.994 % 	 ...../PrepareForRendering

		       0.008 Mt     3.006 %         1 x        0.008 Mt        0.000 Mt        0.008 Mt   100.000 % 	 ....../PrepareDrawBundle

		       1.392 Mt     1.343 %         1 x        1.392 Mt        0.000 Mt        0.000 Mt     0.029 % 	 ..../TexturedRLPrep

		       1.392 Mt    99.971 %         1 x        1.392 Mt        0.000 Mt        1.387 Mt    99.691 % 	 ...../PrepareForRendering

		       0.001 Mt     0.057 %         1 x        0.001 Mt        0.000 Mt        0.001 Mt   100.000 % 	 ....../PrepareMaterials

		       0.004 Mt     0.252 %         1 x        0.004 Mt        0.000 Mt        0.004 Mt   100.000 % 	 ....../PrepareDrawBundle

		       1.320 Mt     1.274 %         1 x        1.320 Mt        0.000 Mt        0.000 Mt     0.038 % 	 ..../DeferredRLPrep

		       1.320 Mt    99.962 %         1 x        1.320 Mt        0.000 Mt        1.018 Mt    77.108 % 	 ...../PrepareForRendering

		       0.001 Mt     0.038 %         1 x        0.001 Mt        0.000 Mt        0.001 Mt   100.000 % 	 ....../PrepareMaterials

		       0.298 Mt    22.596 %         1 x        0.298 Mt        0.000 Mt        0.298 Mt   100.000 % 	 ....../BuildMaterialCache

		       0.003 Mt     0.258 %         1 x        0.003 Mt        0.000 Mt        0.003 Mt   100.000 % 	 ....../PrepareDrawBundle

		      24.855 Mt    23.981 %         1 x       24.855 Mt        0.000 Mt        2.641 Mt    10.626 % 	 ..../RayTracingRLPrep

		       0.043 Mt     0.173 %         1 x        0.043 Mt        0.000 Mt        0.043 Mt   100.000 % 	 ...../PrepareAccelerationStructures

		       0.001 Mt     0.003 %         1 x        0.001 Mt        0.000 Mt        0.001 Mt   100.000 % 	 ...../PrepareCache

		      12.685 Mt    51.037 %         1 x       12.685 Mt        0.000 Mt       12.685 Mt   100.000 % 	 ...../PipelineBuild

		       0.313 Mt     1.261 %         1 x        0.313 Mt        0.000 Mt        0.313 Mt   100.000 % 	 ...../BuildCache

		       7.819 Mt    31.460 %         1 x        7.819 Mt        0.000 Mt        0.000 Mt     0.006 % 	 ...../BuildAccelerationStructures

		       7.819 Mt    99.994 %         1 x        7.819 Mt        0.000 Mt        6.083 Mt    77.798 % 	 ....../AccelerationStructureBuild

		       0.900 Mt    11.508 %         1 x        0.900 Mt        0.000 Mt        0.900 Mt   100.000 % 	 ......./BuildBottomLevelAS

		       0.836 Mt    10.694 %         1 x        0.836 Mt        0.000 Mt        0.836 Mt   100.000 % 	 ......./BuildTopLevelAS

		       1.352 Mt     5.440 %         1 x        1.352 Mt        0.000 Mt        1.352 Mt   100.000 % 	 ...../BuildShaderTables

		      11.274 Mt    10.877 %         1 x       11.274 Mt        0.000 Mt       11.274 Mt   100.000 % 	 ..../GpuSidePrep

	WindowThread:

		   24592.667 Mt   100.000 %         1 x    24592.668 Mt    24592.667 Mt    24592.667 Mt   100.000 % 	 /

	GpuThread:

		   21441.783 Mt   100.000 %      2468 x        8.688 Mt        7.503 Mt      126.667 Mt     0.591 % 	 /

		       0.513 Mt     0.002 %         1 x        0.513 Mt        0.000 Mt        0.006 Mt     1.186 % 	 ./ScenePrep

		       0.507 Mt    98.814 %         1 x        0.507 Mt        0.000 Mt        0.002 Mt     0.404 % 	 ../AccelerationStructureBuild

		       0.442 Mt    87.353 %         1 x        0.442 Mt        0.000 Mt        0.442 Mt   100.000 % 	 .../BuildBottomLevelASGpu

		       0.062 Mt    12.243 %         1 x        0.062 Mt        0.000 Mt        0.062 Mt   100.000 % 	 .../BuildTopLevelASGpu

		   21170.390 Mt    98.734 %      2468 x        8.578 Mt        7.415 Mt        5.459 Mt     0.026 % 	 ./RayTracingGpu

		     324.249 Mt     1.532 %      2468 x        0.131 Mt        0.071 Mt      324.249 Mt   100.000 % 	 ../DeferredRLGpu

		    3718.905 Mt    17.567 %      2468 x        1.507 Mt        0.490 Mt     3718.905 Mt   100.000 % 	 ../RayTracingRLGpu

		   17121.777 Mt    80.876 %      2468 x        6.938 Mt        6.853 Mt    17121.777 Mt   100.000 % 	 ../ResolveRLGpu

		     144.212 Mt     0.673 %      2468 x        0.058 Mt        0.088 Mt      144.212 Mt   100.000 % 	 ./Present


	============================


