Configuration: 
Command line parameters  = Measurement.exe --scene Sponza/Sponza.gltf --duplication-cube --duplication-offset 0 --profile-triangles --profile-camera-track Sponza/SponzaFast.trk --profile-max-duplication 5 --rt-mode --width 2560 --height 1440 --profile-output TrianglesSponzaNoOffset1440Rt 
Build type               = Release
Device name              = NVIDIA GeForce RTX 2080 Ti
Device memory MB         = 11049
Ray Tracing backend      = Hardware
Window width             = 2560
Window height            = 1421
Window title             = Direct3D App
Back-buffer count        = 3
Fill Triangles           = enabled
Triangles Font Clock-Wise= enabled
Fast Build AS            = disabled
Target FPS               = 0
Target UPS               = 60
Tearing                  = enabled
VSync                    = disabled
GUI rendering            = disabled
Debug layer              = disabled
GPU validation           = disabled
GPU profiling            = enabled
Render mode              = Ray Tracing
Loaded scene file        = Sponza/Sponza.gltf
Using testing scene      = disabled
Duplication              = 5
Duplication in cube      = enabled
Duplication offset       = 0
BLAS duplication         = enabled
Rays per pixel           = 13
Quality preset           = High
Rasterization Quality    : 
  AO                     = enabled
  AO kernel size         = 4
  Shadows                = enabled
  Shadow map resolution  = 4096
  Shadow PCF kernel size = 4
  Reflections            = enabled
Ray Tracing Quality      : 
  AO                     = enabled
  AO rays                = 4
  AO kernel size         = 4
  Shadows                = enabled
  Shadow rays            = 4
  Shadow kernel size     = 4
  Reflections            = enabled

ProfilingAggregator: 
Render	,	10.6494	,	10.6494	,	11.3892	,	11.3892	,	10.5284	,	10.5284	,	11.446	,	11.446	,	14.0916	,	14.0916	,	11.9441	,	45.0232	,	45.0232	,	45.6435	,	45.6435	,	39.7632	,	39.7632	,	67.344	,	67.344	,	67.0631	,	67.0631	,	44.9668	,	132.545	,	132.545	,	136.514	,	136.514	,	115.919	,	115.919	,	240.844	,	240.844	,	201.944	,	201.944	,	138.18	,	331.839	,	331.839	,	301.606	,	301.606	,	268.502	,	268.502	,	676.461	,	676.461	,	338.915	,	338.915	,	327.977	,	597.018	,	597.018	,	640.973	,	640.973	,	1411.91	,	1411.91	,	677.093
Update	,	0.0741	,	0.0741	,	0.0729	,	0.0729	,	0.0705	,	0.0705	,	0.0643	,	0.0643	,	0.0694	,	0.0694	,	0.0691	,	0.0387	,	0.0387	,	0.0339	,	0.0339	,	0.0391	,	0.0391	,	0.0387	,	0.0387	,	0.0427	,	0.0427	,	0.0312	,	0.03	,	0.03	,	0.0282	,	0.0282	,	0.0305	,	0.0305	,	0.0283	,	0.0283	,	0.0292	,	0.0292	,	0.0308	,	0.0301	,	0.0301	,	0.0294	,	0.0294	,	0.0302	,	0.0302	,	0.0303	,	0.0303	,	0.0299	,	0.0299	,	0.0288	,	0.0282	,	0.0282	,	0.0305	,	0.0305	,	0.0308	,	0.0308	,	0.0202
RayTracing	,	0.1762	,	0.1762	,	0.1814	,	0.1814	,	0.2052	,	0.2052	,	0.212	,	0.212	,	0.2115	,	0.2115	,	0.1723	,	0.1935	,	0.1935	,	0.1979	,	0.1979	,	0.2252	,	0.2252	,	0.195	,	0.195	,	0.2313	,	0.2313	,	0.2195	,	0.2535	,	0.2535	,	0.2552	,	0.2552	,	0.2374	,	0.2374	,	0.2495	,	0.2495	,	0.2771	,	0.2771	,	0.2654	,	0.2857	,	0.2857	,	0.2685	,	0.2685	,	0.2742	,	0.2742	,	0.2738	,	0.2738	,	0.2713	,	0.2713	,	0.2765	,	0.2985	,	0.2985	,	0.285	,	0.285	,	0.3011	,	0.3011	,	0.2739
RayTracingGpu	,	9.69494	,	9.69494	,	10.3997	,	10.3997	,	9.61168	,	9.61168	,	10.4781	,	10.4781	,	13.1183	,	13.1183	,	11.1929	,	44.1843	,	44.1843	,	44.7189	,	44.7189	,	38.7612	,	38.7612	,	66.4308	,	66.4308	,	66.0748	,	66.0748	,	43.9459	,	131.524	,	131.524	,	135.525	,	135.525	,	114.933	,	114.933	,	239.867	,	239.867	,	200.955	,	200.955	,	137.236	,	330.758	,	330.758	,	300.529	,	300.529	,	267.449	,	267.449	,	675.419	,	675.419	,	337.929	,	337.929	,	326.899	,	595.874	,	595.874	,	639.797	,	639.797	,	1410.73	,	1410.73	,	676.009
DeferredRLGpu	,	2.30787	,	2.30787	,	3.58506	,	3.58506	,	2.68822	,	2.68822	,	3.22176	,	3.22176	,	4.22032	,	4.22032	,	3.5615	,	19.2937	,	19.2937	,	21.6072	,	21.6072	,	16.5575	,	16.5575	,	25.0971	,	25.0971	,	25.9636	,	25.9636	,	22.9097	,	63.943	,	63.943	,	68.0348	,	68.0348	,	51.9028	,	51.9028	,	81.7607	,	81.7607	,	80.6978	,	80.6978	,	72.9899	,	176.234	,	176.234	,	154.346	,	154.346	,	114.073	,	114.073	,	192.3	,	192.3	,	184.701	,	184.701	,	170.859	,	284.197	,	284.197	,	339.093	,	339.093	,	375.211	,	375.211	,	332.251
RayTracingRLGpu	,	4.62454	,	4.62454	,	4.27974	,	4.27974	,	4.40176	,	4.40176	,	4.72013	,	4.72013	,	5.99987	,	5.99987	,	4.83978	,	22.1113	,	22.1113	,	20.56	,	20.56	,	19.4225	,	19.4225	,	38.7908	,	38.7908	,	37.5819	,	37.5819	,	18.4827	,	65.0552	,	65.0552	,	64.9378	,	64.9378	,	60.4675	,	60.4675	,	155.554	,	155.554	,	117.723	,	117.723	,	61.6961	,	151.994	,	151.994	,	143.635	,	143.635	,	150.826	,	150.826	,	480.566	,	480.566	,	150.678	,	150.678	,	153.495	,	309.129	,	309.129	,	298.169	,	298.169	,	1032.97	,	1032.97	,	341.213
ResolveRLGpu	,	2.76054	,	2.76054	,	2.53171	,	2.53171	,	2.5167	,	2.5167	,	2.53203	,	2.53203	,	2.89619	,	2.89619	,	2.78995	,	2.77642	,	2.77642	,	2.54848	,	2.54848	,	2.77738	,	2.77738	,	2.54048	,	2.54048	,	2.52714	,	2.52714	,	2.55024	,	2.52352	,	2.52352	,	2.55034	,	2.55034	,	2.56019	,	2.56019	,	2.54989	,	2.54989	,	2.5311	,	2.5311	,	2.54797	,	2.52714	,	2.52714	,	2.54563	,	2.54563	,	2.54749	,	2.54749	,	2.55114	,	2.55114	,	2.54688	,	2.54688	,	2.54314	,	2.54326	,	2.54326	,	2.53165	,	2.53165	,	2.54531	,	2.54531	,	2.54262
Index	,	0	,	1	,	2	,	3	,	4	,	5	,	6	,	7	,	8	,	9	,	10	,	11	,	12	,	13	,	14	,	15	,	16	,	17	,	18	,	19	,	20	,	21	,	22	,	23	,	24	,	25	,	26	,	27	,	28	,	29	,	30	,	31	,	32	,	33	,	34	,	35	,	36	,	37	,	38	,	39	,	40	,	41	,	42	,	43	,	44	,	45	,	46	,	47	,	48	,	49	,	50

ProfilingBuildAggregator: 
BuildBottomLevelAS	,	1.6763	,	10.5871	,	34.1548	,	85.9843	,	166.554
BuildBottomLevelASGpu	,	1.51914	,	9.34339	,	30.2294	,	71.4484	,	139.93
BuildTopLevelAS	,	0.7822	,	1.5614	,	1.8081	,	1.7542	,	1.6412
BuildTopLevelASGpu	,	0.088032	,	0.080352	,	0.165504	,	0.269952	,	0.348864
Duplication	,	1	,	2	,	3	,	4	,	5
Triangles	,	262267	,	2098136	,	7081209	,	16785088	,	32783375
Meshes	,	103	,	824	,	2781	,	6592	,	12875
BottomLevels	,	1	,	8	,	27	,	64	,	125
TopLevelSize	,	64	,	512	,	1728	,	4096	,	8000
BottomLevelsSize	,	14622208	,	116977664	,	394799616	,	935821312	,	1827776000
Index	,	0	,	1	,	2	,	3	,	4

FpsAggregator: 
FPS	,	88	,	88	,	85	,	85	,	85	,	85	,	87	,	87	,	62	,	62	,	78	,	26	,	26	,	21	,	21	,	23	,	23	,	24	,	24	,	12	,	12	,	20	,	9	,	9	,	7	,	7	,	8	,	8	,	7	,	7	,	4	,	4	,	7	,	4	,	4	,	3	,	3	,	4	,	4	,	2	,	2	,	3	,	3	,	3	,	2	,	2	,	2	,	2	,	1	,	1	,	2
UPS	,	60	,	60	,	60	,	60	,	61	,	61	,	59	,	59	,	61	,	61	,	60	,	63	,	63	,	61	,	61	,	61	,	61	,	60	,	60	,	63	,	63	,	62	,	64	,	64	,	63	,	63	,	63	,	63	,	53	,	53	,	67	,	67	,	67	,	68	,	68	,	65	,	65	,	71	,	71	,	37	,	37	,	100	,	100	,	63	,	87	,	87	,	71	,	71	,	38	,	38	,	131
FrameTime	,	11.4072	,	11.4072	,	11.8438	,	11.8438	,	11.7833	,	11.7833	,	11.4952	,	11.4952	,	16.2477	,	16.2477	,	12.9244	,	40.1463	,	40.1463	,	48.809	,	48.809	,	43.5695	,	43.5695	,	42.8704	,	42.8704	,	87.5677	,	87.5677	,	50.7398	,	120.231	,	120.231	,	148.811	,	148.811	,	128.403	,	128.403	,	144.322	,	144.322	,	270.031	,	270.031	,	150.788	,	289.844	,	289.844	,	351.566	,	351.566	,	288.81	,	288.81	,	510.441	,	510.441	,	444.196	,	444.196	,	343.385	,	682.806	,	682.806	,	611.047	,	611.047	,	1455.34	,	1455.34	,	701.375
GigaRays/s	,	4.14571	,	4.14571	,	3.99287	,	3.99287	,	4.01337	,	4.01337	,	4.11397	,	4.11397	,	2.91062	,	2.91062	,	3.65905	,	1.17796	,	1.17796	,	0.968896	,	0.968896	,	1.08541	,	1.08541	,	1.10311	,	1.10311	,	0.540049	,	0.540049	,	0.932027	,	0.393334	,	0.393334	,	0.317791	,	0.317791	,	0.3683	,	0.3683	,	0.327677	,	0.327677	,	0.175131	,	0.175131	,	0.313624	,	0.16316	,	0.16316	,	0.134515	,	0.134515	,	0.163744	,	0.163744	,	0.092647	,	0.092647	,	0.106464	,	0.106464	,	0.137719	,	0.0692596	,	0.0692596	,	0.0773932	,	0.0773932	,	0.0324948	,	0.0324948	,	0.067426
Duplication	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	2	,	2	,	2	,	2	,	2	,	2	,	2	,	2	,	2	,	2	,	2	,	3	,	3	,	3	,	3	,	3	,	3	,	3	,	3	,	3	,	3	,	3	,	4	,	4	,	4	,	4	,	4	,	4	,	4	,	4	,	4	,	4	,	4	,	5	,	5	,	5	,	5	,	5	,	5	,	5
Index	,	0	,	1	,	2	,	3	,	4	,	5	,	6	,	7	,	8	,	9	,	10	,	11	,	12	,	13	,	14	,	15	,	16	,	17	,	18	,	19	,	20	,	21	,	22	,	23	,	24	,	25	,	26	,	27	,	28	,	29	,	30	,	31	,	32	,	33	,	34	,	35	,	36	,	37	,	38	,	39	,	40	,	41	,	42	,	43	,	44	,	45	,	46	,	47	,	48	,	49	,	50


AccelerationStructure: 
Ray tracing acceleration structure building statistics: 
	Bottom Level Acceleration Structure: 
		Maximum [B]: 14622208
		Minimum [B]: 14622208
		Currently Allocated [B]: 1827776000
		Currently Created [num]: 125
		Current Average [B]: 14622208
		Maximum Triangles [num]: 262267
		Minimum Triangles [num]: 262267
		Total Triangles [num]: 32783375
		Maximum Meshes [num]: 103
		Minimum Meshes [num]: 103
		Total Meshes [num]: 12875
	Top Level Acceleration Structure: 
		Maximum [B]: 8000
		Minimum [B]: 8000
		Currently Allocated [B]: 8000
		Total Allocated [B]: 8000
		Total Created [num]: 1
		Average Allocated [B]: 8000
		Maximum Instances [num]: 125
		Minimum Instances [num]: 125
		Total Instances [num]: 125
	Scratch Buffer: 
		Maximum [B]: 11657856
		Minimum [B]: 11657856
		Currently Allocated [B]: 11657856
		Total Allocated [B]: 11657856
		Total Created [num]: 1
		Average Allocated [B]: 11657856
ProfilingStack: 
[PROF] Profiling statistics: 
	Scopes entered : 41469
	Scopes exited : 41468
	Overhead per scope [ticks] : 120.4
	Threads entered : 3
	Threads exited : 0
	============================
		         Sum[Mt]   Parent[%]  Samples[num]       Avg[Mt]   Last/Curr[Mt]        Self[Mt]     Self[%] 	 Name

	main:

		   53304.394 Mt   100.000 %         1 x    53304.395 Mt    53304.394 Mt        0.447 Mt     0.001 % 	 /

		   53303.978 Mt    99.999 %         1 x    53303.980 Mt    53303.978 Mt      347.243 Mt     0.651 % 	 ./Main application loop

		       6.610 Mt     0.012 %         1 x        6.610 Mt        0.000 Mt        6.610 Mt   100.000 % 	 ../TexturedRLInitialization

		       2.920 Mt     0.005 %         1 x        2.920 Mt        0.000 Mt        2.920 Mt   100.000 % 	 ../ShadowMapRLInitialization

		       8.765 Mt     0.016 %         1 x        8.765 Mt        0.000 Mt        8.765 Mt   100.000 % 	 ../RasterResolveRLInitialization

		       6.820 Mt     0.013 %         1 x        6.820 Mt        0.000 Mt        6.820 Mt   100.000 % 	 ../AmbientOcclusionRLInitialization

		      10.130 Mt     0.019 %         1 x       10.130 Mt        0.000 Mt       10.130 Mt   100.000 % 	 ../DeferredRLInitialization

		     415.700 Mt     0.780 %         1 x      415.700 Mt        0.000 Mt        3.650 Mt     0.878 % 	 ../RayTracingRLInitialization

		     412.050 Mt    99.122 %         1 x      412.050 Mt        0.000 Mt      412.050 Mt   100.000 % 	 .../PipelineBuild

		       7.159 Mt     0.013 %         1 x        7.159 Mt        0.000 Mt        7.159 Mt   100.000 % 	 ../ResolveRLInitialization

		    4277.132 Mt     8.024 %         1 x     4277.132 Mt        0.000 Mt        0.290 Mt     0.007 % 	 ../Initialization

		    4276.842 Mt    99.993 %         1 x     4276.842 Mt        0.000 Mt        2.789 Mt     0.065 % 	 .../ScenePrep

		    4190.131 Mt    97.973 %         1 x     4190.131 Mt        0.000 Mt     1419.706 Mt    33.882 % 	 ..../SceneLoad

		    2042.430 Mt    48.744 %         1 x     2042.430 Mt        0.000 Mt      713.193 Mt    34.919 % 	 ...../glTF-LoadScene

		    1329.237 Mt    65.081 %        69 x       19.264 Mt        0.000 Mt     1329.237 Mt   100.000 % 	 ....../glTF-LoadImageData

		     466.862 Mt    11.142 %         1 x      466.862 Mt        0.000 Mt      466.862 Mt   100.000 % 	 ...../glTF-CreateScene

		     261.132 Mt     6.232 %         1 x      261.132 Mt        0.000 Mt      261.132 Mt   100.000 % 	 ...../SceneDesc-CreateScene

		       1.836 Mt     0.043 %         1 x        1.836 Mt        0.000 Mt        0.001 Mt     0.038 % 	 ..../ShadowMapRLPrep

		       1.835 Mt    99.962 %         1 x        1.835 Mt        0.000 Mt        1.792 Mt    97.625 % 	 ...../PrepareForRendering

		       0.044 Mt     2.375 %         1 x        0.044 Mt        0.000 Mt        0.044 Mt   100.000 % 	 ....../PrepareDrawBundle

		       5.099 Mt     0.119 %         1 x        5.099 Mt        0.000 Mt        0.000 Mt     0.008 % 	 ..../TexturedRLPrep

		       5.098 Mt    99.992 %         1 x        5.098 Mt        0.000 Mt        5.058 Mt    99.210 % 	 ...../PrepareForRendering

		       0.002 Mt     0.045 %         1 x        0.002 Mt        0.000 Mt        0.002 Mt   100.000 % 	 ....../PrepareMaterials

		       0.038 Mt     0.745 %         1 x        0.038 Mt        0.000 Mt        0.038 Mt   100.000 % 	 ....../PrepareDrawBundle

		       5.432 Mt     0.127 %         1 x        5.432 Mt        0.000 Mt        0.001 Mt     0.015 % 	 ..../DeferredRLPrep

		       5.431 Mt    99.985 %         1 x        5.431 Mt        0.000 Mt        2.964 Mt    54.571 % 	 ...../PrepareForRendering

		       0.001 Mt     0.020 %         1 x        0.001 Mt        0.000 Mt        0.001 Mt   100.000 % 	 ....../PrepareMaterials

		       2.413 Mt    44.422 %         1 x        2.413 Mt        0.000 Mt        2.413 Mt   100.000 % 	 ....../BuildMaterialCache

		       0.054 Mt     0.987 %         1 x        0.054 Mt        0.000 Mt        0.054 Mt   100.000 % 	 ....../PrepareDrawBundle

		      43.520 Mt     1.018 %         1 x       43.520 Mt        0.000 Mt        3.670 Mt     8.432 % 	 ..../RayTracingRLPrep

		       0.084 Mt     0.193 %         1 x        0.084 Mt        0.000 Mt        0.084 Mt   100.000 % 	 ...../PrepareAccelerationStructures

		       0.001 Mt     0.002 %         1 x        0.001 Mt        0.000 Mt        0.001 Mt   100.000 % 	 ...../PrepareCache

		      27.561 Mt    63.329 %         1 x       27.561 Mt        0.000 Mt       27.561 Mt   100.000 % 	 ...../PipelineBuild

		       1.107 Mt     2.544 %         1 x        1.107 Mt        0.000 Mt        1.107 Mt   100.000 % 	 ...../BuildCache

		       8.604 Mt    19.770 %         1 x        8.604 Mt        0.000 Mt        0.000 Mt     0.006 % 	 ...../BuildAccelerationStructures

		       8.604 Mt    99.994 %         1 x        8.604 Mt        0.000 Mt        6.145 Mt    71.424 % 	 ....../AccelerationStructureBuild

		       1.676 Mt    19.484 %         1 x        1.676 Mt        0.000 Mt        1.676 Mt   100.000 % 	 ......./BuildBottomLevelAS

		       0.782 Mt     9.092 %         1 x        0.782 Mt        0.000 Mt        0.782 Mt   100.000 % 	 ......./BuildTopLevelAS

		       2.493 Mt     5.728 %         1 x        2.493 Mt        0.000 Mt        2.493 Mt   100.000 % 	 ...../BuildShaderTables

		      28.036 Mt     0.656 %         1 x       28.036 Mt        0.000 Mt       28.036 Mt   100.000 % 	 ..../GpuSidePrep

		   48221.500 Mt    90.465 %     25728 x        1.874 Mt      748.903 Mt     2142.563 Mt     4.443 % 	 ../MainLoop

		     235.358 Mt     0.488 %      2848 x        0.083 Mt        0.019 Mt      232.261 Mt    98.684 % 	 .../Update

		       3.097 Mt     1.316 %         1 x        3.097 Mt        0.000 Mt        3.097 Mt   100.000 % 	 ..../GuiModelApply

		   45843.579 Mt    95.069 %       977 x       46.923 Mt      693.398 Mt      131.720 Mt     0.287 % 	 .../Render

		    1028.626 Mt     2.244 %       977 x        1.053 Mt        0.245 Mt        7.583 Mt     0.737 % 	 ..../RayTracing

		     170.342 Mt    16.560 %       977 x        0.174 Mt        0.120 Mt       86.499 Mt    50.779 % 	 ...../Deferred

		      83.844 Mt    49.221 %       977 x        0.086 Mt        0.001 Mt        0.865 Mt     1.032 % 	 ....../DeferredRLPrep

		      82.979 Mt    98.968 %         5 x       16.596 Mt        0.000 Mt       41.453 Mt    49.957 % 	 ......./PrepareForRendering

		       0.034 Mt     0.040 %         5 x        0.007 Mt        0.000 Mt        0.034 Mt   100.000 % 	 ......../PrepareMaterials

		       0.001 Mt     0.001 %         5 x        0.000 Mt        0.000 Mt        0.001 Mt   100.000 % 	 ......../BuildMaterialCache

		      41.491 Mt    50.002 %         5 x        8.298 Mt        0.000 Mt       41.491 Mt   100.000 % 	 ......../PrepareDrawBundle

		     816.507 Mt    79.378 %       977 x        0.836 Mt        0.081 Mt       70.808 Mt     8.672 % 	 ...../RayCasting

		     745.699 Mt    91.328 %       977 x        0.763 Mt        0.001 Mt       21.287 Mt     2.855 % 	 ....../RayTracingRLPrep

		      49.960 Mt     6.700 %         5 x        9.992 Mt        0.000 Mt       49.960 Mt   100.000 % 	 ......./PrepareAccelerationStructures

		       0.018 Mt     0.002 %         5 x        0.004 Mt        0.000 Mt        0.018 Mt   100.000 % 	 ......./PrepareCache

		     235.143 Mt    31.533 %         5 x       47.029 Mt        0.000 Mt      235.143 Mt   100.000 % 	 ......./PipelineBuild

		       0.001 Mt     0.000 %         5 x        0.000 Mt        0.000 Mt        0.001 Mt   100.000 % 	 ......./BuildCache

		     368.117 Mt    49.365 %         5 x       73.623 Mt        0.000 Mt        0.007 Mt     0.002 % 	 ......./BuildAccelerationStructures

		     368.110 Mt    99.998 %         5 x       73.622 Mt        0.000 Mt       52.075 Mt    14.147 % 	 ......../AccelerationStructureBuild

		     308.070 Mt    83.689 %         5 x       61.614 Mt        0.000 Mt      308.070 Mt   100.000 % 	 ........./BuildBottomLevelAS

		       7.966 Mt     2.164 %         5 x        1.593 Mt        0.000 Mt        7.966 Mt   100.000 % 	 ........./BuildTopLevelAS

		      71.173 Mt     9.545 %         5 x       14.235 Mt        0.000 Mt       71.173 Mt   100.000 % 	 ......./BuildShaderTables

		      34.194 Mt     3.324 %       977 x        0.035 Mt        0.034 Mt       34.194 Mt   100.000 % 	 ...../Resolve

		   44683.234 Mt    97.469 %       977 x       45.735 Mt      692.980 Mt    44683.234 Mt   100.000 % 	 ..../Present

	WindowThread:

		   53303.153 Mt   100.000 %         1 x    53303.154 Mt    53303.153 Mt    53303.153 Mt   100.000 % 	 /

	GpuThread:

		   44248.043 Mt   100.000 %       977 x       45.290 Mt      692.404 Mt      163.999 Mt     0.371 % 	 /

		       1.614 Mt     0.004 %         1 x        1.614 Mt        0.000 Mt        0.006 Mt     0.379 % 	 ./ScenePrep

		       1.608 Mt    99.621 %         1 x        1.608 Mt        0.000 Mt        0.001 Mt     0.076 % 	 ../AccelerationStructureBuild

		       1.519 Mt    94.451 %         1 x        1.519 Mt        0.000 Mt        1.519 Mt   100.000 % 	 .../BuildBottomLevelASGpu

		       0.088 Mt     5.473 %         1 x        0.088 Mt        0.000 Mt        0.088 Mt   100.000 % 	 .../BuildTopLevelASGpu

		   44053.347 Mt    99.560 %       977 x       45.090 Mt      692.318 Mt        2.473 Mt     0.006 % 	 ./RayTracingGpu

		   18152.040 Mt    41.205 %       977 x       18.579 Mt      348.601 Mt    18152.040 Mt   100.000 % 	 ../DeferredRLGpu

		   23051.286 Mt    52.326 %       977 x       23.594 Mt      341.069 Mt    23051.286 Mt   100.000 % 	 ../RayTracingRLGpu

		    2586.290 Mt     5.871 %       977 x        2.647 Mt        2.646 Mt     2586.290 Mt   100.000 % 	 ../ResolveRLGpu

		     261.258 Mt     0.593 %         5 x       52.252 Mt        0.000 Mt        0.008 Mt     0.003 % 	 ../AccelerationStructureBuild

		     260.305 Mt    99.635 %         5 x       52.061 Mt        0.000 Mt      260.305 Mt   100.000 % 	 .../BuildBottomLevelASGpu

		       0.945 Mt     0.362 %         5 x        0.189 Mt        0.000 Mt        0.945 Mt   100.000 % 	 .../BuildTopLevelASGpu

		      29.082 Mt     0.066 %       977 x        0.030 Mt        0.086 Mt       29.082 Mt   100.000 % 	 ./Present


	============================


