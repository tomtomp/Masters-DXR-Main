Configuration: 
Command line parameters  = E:\programming\FIT-DIP\Masters-DXR-Main-git\build\Measurement\x64\Release\Measurement.exe --scene Sponza/Sponza.gltf --profile-automation --profile-camera-track Sponza/SponzaPreset.trk --rt-mode --width 2560 --height 1440 --profile-output PresetSponza1440RtLow --quality-preset Low 
Build type               = Release
Device name              = NVIDIA GeForce RTX 2080 Ti
Device memory MB         = 11049
Ray Tracing backend      = Hardware
Window width             = 2560
Window height            = 1421
Window title             = Direct3D App
Back-buffer count        = 3
Fill Triangles           = enabled
Triangles Font Clock-Wise= enabled
Fast Build AS            = disabled
Target FPS               = 0
Target UPS               = 60
Tearing                  = enabled
VSync                    = disabled
GUI rendering            = disabled
Debug layer              = disabled
GPU validation           = disabled
GPU profiling            = enabled
Render mode              = Ray Tracing
Loaded scene file        = Sponza/Sponza.gltf
Using testing scene      = disabled
Duplication              = 1
Duplication in cube      = disabled
Duplication offset       = 10
BLAS duplication         = disabled
Rays per pixel           = 5
Quality preset           = Low
Rasterization Quality    : 
  AO                     = enabled
  AO kernel size         = 4
  Shadows                = enabled
  Shadow map resolution  = 2048
  Shadow PCF kernel size = 0
  Reflections            = disabled
Ray Tracing Quality      : 
  AO                     = enabled
  AO rays                = 4
  AO kernel size         = 4
  Shadows                = enabled
  Shadow rays            = 1
  Shadow kernel size     = 4
  Reflections            = disabled

ProfilingAggregator: 
Render	,	8.1701	,	8.4767	,	7.6311	,	6.623	,	6.7247	,	7.0326	,	7.1768	,	8.0577	,	7.1335	,	6.567	,	6.5256	,	7.5522	,	8.1959	,	8.4242	,	7.9125	,	6.8143	,	7.6088	,	7.5678	,	6.7814	,	7.7788	,	9.209	,	9.5933	,	9.1382
Update	,	0.0779	,	0.0278	,	0.0325	,	0.0796	,	0.0739	,	0.0738	,	0.0735	,	0.0744	,	0.0735	,	0.0735	,	0.0729	,	0.0736	,	0.0752	,	0.0749	,	0.0796	,	0.0709	,	0.0739	,	0.0762	,	0.0683	,	0.0722	,	0.0406	,	0.0606	,	0.0768
RayTracing	,	0.1732	,	0.0558	,	0.0578	,	0.1639	,	0.1519	,	0.1472	,	0.149	,	0.0544	,	0.1452	,	0.1442	,	0.1658	,	0.1428	,	0.1427	,	0.1465	,	0.1042	,	0.1487	,	0.1511	,	0.1671	,	0.1444	,	0.1477	,	0.1661	,	0.1404	,	0.1727
RayTracingGpu	,	7.45078	,	8.08317	,	7.11482	,	5.87338	,	5.98323	,	6.34659	,	6.59104	,	7.61882	,	6.55366	,	6.03738	,	5.88166	,	6.82675	,	7.65037	,	7.78515	,	7.35722	,	6.17651	,	6.96054	,	6.81539	,	6.18099	,	7.21507	,	8.39123	,	8.98582	,	8.33062
DeferredRLGpu	,	2.73818	,	2.58256	,	2.27098	,	1.46016	,	1.35485	,	1.59917	,	2.00006	,	2.78134	,	2.04669	,	1.53773	,	1.33072	,	1.8265	,	2.32589	,	2.83763	,	2.54608	,	1.49155	,	1.96634	,	1.88819	,	1.50394	,	2.70902	,	3.82467	,	4.21389	,	3.84717
RayTracingRLGpu	,	2.33939	,	2.90486	,	2.47251	,	2.07165	,	2.19955	,	2.34838	,	2.16454	,	2.36854	,	2.10122	,	2.06998	,	2.11802	,	2.48362	,	2.71866	,	2.48707	,	2.30816	,	2.26333	,	2.54013	,	2.46691	,	2.26083	,	2.1767	,	2.1103	,	2.30355	,	2.11347
ResolveRLGpu	,	2.37066	,	2.59418	,	2.36659	,	2.33891	,	2.42627	,	2.39626	,	2.42381	,	2.46675	,	2.404	,	2.4279	,	2.43123	,	2.51395	,	2.60448	,	2.45917	,	2.50096	,	2.41994	,	2.45238	,	2.45853	,	2.41366	,	2.32746	,	2.45414	,	2.46499	,	2.36806
Index	,	0	,	1	,	2	,	3	,	4	,	5	,	6	,	7	,	8	,	9	,	10	,	11	,	12	,	13	,	14	,	15	,	16	,	17	,	18	,	19	,	20	,	21	,	22

ProfilingBuildAggregator: 
BuildBottomLevelAS	,	1.6246
BuildBottomLevelASGpu	,	2.32966
BuildTopLevelAS	,	0.7555
BuildTopLevelASGpu	,	0.089888
Duplication	,	1
Triangles	,	262267
Meshes	,	103
BottomLevels	,	1
TopLevelSize	,	64
BottomLevelsSize	,	18390656
Index	,	0

FpsAggregator: 
FPS	,	79	,	115	,	126	,	135	,	149	,	139	,	144	,	126	,	131	,	140	,	148	,	145	,	135	,	127	,	126	,	143	,	132	,	127	,	134	,	141	,	112	,	108	,	107
UPS	,	59	,	61	,	60	,	60	,	60	,	60	,	61	,	60	,	60	,	61	,	60	,	60	,	60	,	60	,	60	,	61	,	60	,	60	,	60	,	60	,	60	,	61	,	60
FrameTime	,	12.7157	,	8.7672	,	7.94349	,	7.43163	,	6.71841	,	7.21246	,	6.98159	,	7.99519	,	7.67391	,	7.16697	,	6.78202	,	6.93627	,	7.42358	,	7.88226	,	7.94296	,	7.01041	,	7.61382	,	7.88214	,	7.47883	,	7.11013	,	8.95506	,	9.33683	,	9.39312
GigaRays/s	,	1.43042	,	2.07464	,	2.28978	,	2.44748	,	2.70731	,	2.52186	,	2.60525	,	2.27497	,	2.37021	,	2.53787	,	2.68192	,	2.62227	,	2.45014	,	2.30756	,	2.28993	,	2.59454	,	2.38892	,	2.3076	,	2.43204	,	2.55815	,	2.03112	,	1.94807	,	1.9364
Index	,	0	,	1	,	2	,	3	,	4	,	5	,	6	,	7	,	8	,	9	,	10	,	11	,	12	,	13	,	14	,	15	,	16	,	17	,	18	,	19	,	20	,	21	,	22


AccelerationStructure: 
Ray tracing acceleration structure building statistics: 
	Bottom Level Acceleration Structure: 
		Maximum [B]: 18390656
		Minimum [B]: 18390656
		Currently Allocated [B]: 18390656
		Currently Created [num]: 1
		Current Average [B]: 18390656
		Maximum Triangles [num]: 262267
		Minimum Triangles [num]: 262267
		Total Triangles [num]: 262267
		Maximum Meshes [num]: 103
		Minimum Meshes [num]: 103
		Total Meshes [num]: 103
	Top Level Acceleration Structure: 
		Maximum [B]: 64
		Minimum [B]: 64
		Currently Allocated [B]: 64
		Total Allocated [B]: 64
		Total Created [num]: 1
		Average Allocated [B]: 64
		Maximum Instances [num]: 1
		Minimum Instances [num]: 1
		Total Instances [num]: 1
	Scratch Buffer: 
		Maximum [B]: 14995072
		Minimum [B]: 14995072
		Currently Allocated [B]: 14995072
		Total Allocated [B]: 14995072
		Total Created [num]: 1
		Average Allocated [B]: 14995072
ProfilingStack: 
[PROF] Profiling statistics: 
	Scopes entered : 73166
	Scopes exited : 73165
	Overhead per scope [ticks] : 100.8
	Threads entered : 3
	Threads exited : 0
	============================
		         Sum[Mt]   Parent[%]  Samples[num]       Avg[Mt]   Last/Curr[Mt]        Self[Mt]     Self[%] 	 Name

	main:

		   26159.169 Mt   100.000 %         1 x    26159.170 Mt    26159.168 Mt        0.324 Mt     0.001 % 	 /

		   26158.883 Mt    99.999 %         1 x    26158.884 Mt    26158.883 Mt      249.080 Mt     0.952 % 	 ./Main application loop

		       2.281 Mt     0.009 %         1 x        2.281 Mt        0.000 Mt        2.281 Mt   100.000 % 	 ../TexturedRLInitialization

		       1.354 Mt     0.005 %         1 x        1.354 Mt        0.000 Mt        1.354 Mt   100.000 % 	 ../ShadowMapRLInitialization

		       5.592 Mt     0.021 %         1 x        5.592 Mt        0.000 Mt        5.592 Mt   100.000 % 	 ../RasterResolveRLInitialization

		       3.717 Mt     0.014 %         1 x        3.717 Mt        0.000 Mt        3.717 Mt   100.000 % 	 ../ResolveRLInitialization

		   23106.061 Mt    88.330 %     33059 x        0.699 Mt       24.370 Mt      378.733 Mt     1.639 % 	 ../MainLoop

		     193.392 Mt     0.837 %      1385 x        0.140 Mt        0.090 Mt      192.758 Mt    99.672 % 	 .../Update

		       0.634 Mt     0.328 %         1 x        0.634 Mt        0.000 Mt        0.634 Mt   100.000 % 	 ..../GuiModelApply

		   22533.936 Mt    97.524 %      2970 x        7.587 Mt       10.163 Mt      271.570 Mt     1.205 % 	 .../Render

		     451.176 Mt     2.002 %      2970 x        0.152 Mt        0.260 Mt       16.485 Mt     3.654 % 	 ..../RayTracing

		     195.033 Mt    43.228 %      2970 x        0.066 Mt        0.126 Mt      192.908 Mt    98.910 % 	 ...../Deferred

		       2.125 Mt     1.090 %      2970 x        0.001 Mt        0.001 Mt        2.125 Mt   100.000 % 	 ....../DeferredRLPrep

		     157.493 Mt    34.907 %      2970 x        0.053 Mt        0.086 Mt      156.142 Mt    99.143 % 	 ...../RayCasting

		       1.350 Mt     0.857 %      2970 x        0.000 Mt        0.001 Mt        1.350 Mt   100.000 % 	 ....../RayTracingRLPrep

		      82.165 Mt    18.211 %      2970 x        0.028 Mt        0.039 Mt       82.165 Mt   100.000 % 	 ...../Resolve

		   21811.190 Mt    96.793 %      2970 x        7.344 Mt        9.739 Mt    21811.190 Mt   100.000 % 	 ..../Present

		       2.885 Mt     0.011 %         1 x        2.885 Mt        0.000 Mt        2.885 Mt   100.000 % 	 ../AmbientOcclusionRLInitialization

		       5.989 Mt     0.023 %         1 x        5.989 Mt        0.000 Mt        5.989 Mt   100.000 % 	 ../DeferredRLInitialization

		      56.389 Mt     0.216 %         1 x       56.389 Mt        0.000 Mt        2.459 Mt     4.361 % 	 ../RayTracingRLInitialization

		      53.929 Mt    95.639 %         1 x       53.929 Mt        0.000 Mt       53.929 Mt   100.000 % 	 .../PipelineBuild

		    2725.535 Mt    10.419 %         1 x     2725.535 Mt        0.000 Mt        0.402 Mt     0.015 % 	 ../Initialization

		    2725.133 Mt    99.985 %         1 x     2725.133 Mt        0.000 Mt        2.249 Mt     0.083 % 	 .../ScenePrep

		    2642.860 Mt    96.981 %         1 x     2642.860 Mt        0.000 Mt      263.014 Mt     9.952 % 	 ..../SceneLoad

		    2008.410 Mt    75.994 %         1 x     2008.410 Mt        0.000 Mt      600.437 Mt    29.896 % 	 ...../glTF-LoadScene

		    1407.973 Mt    70.104 %        69 x       20.405 Mt        0.000 Mt     1407.973 Mt   100.000 % 	 ....../glTF-LoadImageData

		     283.049 Mt    10.710 %         1 x      283.049 Mt        0.000 Mt      283.049 Mt   100.000 % 	 ...../glTF-CreateScene

		      88.388 Mt     3.344 %         1 x       88.388 Mt        0.000 Mt       88.388 Mt   100.000 % 	 ...../SceneDesc-CreateScene

		       1.132 Mt     0.042 %         1 x        1.132 Mt        0.000 Mt        0.001 Mt     0.062 % 	 ..../ShadowMapRLPrep

		       1.131 Mt    99.938 %         1 x        1.131 Mt        0.000 Mt        1.094 Mt    96.668 % 	 ...../PrepareForRendering

		       0.038 Mt     3.332 %         1 x        0.038 Mt        0.000 Mt        0.038 Mt   100.000 % 	 ....../PrepareDrawBundle

		       3.621 Mt     0.133 %         1 x        3.621 Mt        0.000 Mt        0.000 Mt     0.011 % 	 ..../TexturedRLPrep

		       3.620 Mt    99.989 %         1 x        3.620 Mt        0.000 Mt        3.583 Mt    98.959 % 	 ...../PrepareForRendering

		       0.002 Mt     0.041 %         1 x        0.002 Mt        0.000 Mt        0.002 Mt   100.000 % 	 ....../PrepareMaterials

		       0.036 Mt     1.000 %         1 x        0.036 Mt        0.000 Mt        0.036 Mt   100.000 % 	 ....../PrepareDrawBundle

		       3.857 Mt     0.142 %         1 x        3.857 Mt        0.000 Mt        0.001 Mt     0.021 % 	 ..../DeferredRLPrep

		       3.856 Mt    99.979 %         1 x        3.856 Mt        0.000 Mt        3.244 Mt    84.131 % 	 ...../PrepareForRendering

		       0.001 Mt     0.013 %         1 x        0.001 Mt        0.000 Mt        0.001 Mt   100.000 % 	 ....../PrepareMaterials

		       0.453 Mt    11.751 %         1 x        0.453 Mt        0.000 Mt        0.453 Mt   100.000 % 	 ....../BuildMaterialCache

		       0.158 Mt     4.105 %         1 x        0.158 Mt        0.000 Mt        0.158 Mt   100.000 % 	 ....../PrepareDrawBundle

		      35.810 Mt     1.314 %         1 x       35.810 Mt        0.000 Mt        2.763 Mt     7.716 % 	 ..../RayTracingRLPrep

		       0.210 Mt     0.587 %         1 x        0.210 Mt        0.000 Mt        0.210 Mt   100.000 % 	 ...../PrepareAccelerationStructures

		       0.003 Mt     0.007 %         1 x        0.003 Mt        0.000 Mt        0.003 Mt   100.000 % 	 ...../PrepareCache

		      25.168 Mt    70.282 %         1 x       25.168 Mt        0.000 Mt       25.168 Mt   100.000 % 	 ...../PipelineBuild

		       0.676 Mt     1.887 %         1 x        0.676 Mt        0.000 Mt        0.676 Mt   100.000 % 	 ...../BuildCache

		       5.564 Mt    15.537 %         1 x        5.564 Mt        0.000 Mt        0.001 Mt     0.011 % 	 ...../BuildAccelerationStructures

		       5.563 Mt    99.989 %         1 x        5.563 Mt        0.000 Mt        3.183 Mt    57.218 % 	 ....../AccelerationStructureBuild

		       1.625 Mt    29.202 %         1 x        1.625 Mt        0.000 Mt        1.625 Mt   100.000 % 	 ......./BuildBottomLevelAS

		       0.755 Mt    13.580 %         1 x        0.755 Mt        0.000 Mt        0.755 Mt   100.000 % 	 ......./BuildTopLevelAS

		       1.426 Mt     3.983 %         1 x        1.426 Mt        0.000 Mt        1.426 Mt   100.000 % 	 ...../BuildShaderTables

		      35.605 Mt     1.307 %         1 x       35.605 Mt        0.000 Mt       35.605 Mt   100.000 % 	 ..../GpuSidePrep

	WindowThread:

		   26158.397 Mt   100.000 %         1 x    26158.397 Mt    26158.396 Mt    26158.397 Mt   100.000 % 	 /

	GpuThread:

		   20828.843 Mt   100.000 %      2970 x        7.013 Mt        9.011 Mt      147.709 Mt     0.709 % 	 /

		       2.427 Mt     0.012 %         1 x        2.427 Mt        0.000 Mt        0.006 Mt     0.253 % 	 ./ScenePrep

		       2.421 Mt    99.747 %         1 x        2.421 Mt        0.000 Mt        0.001 Mt     0.059 % 	 ../AccelerationStructureBuild

		       2.330 Mt    96.228 %         1 x        2.330 Mt        0.000 Mt        2.330 Mt   100.000 % 	 .../BuildBottomLevelASGpu

		       0.090 Mt     3.713 %         1 x        0.090 Mt        0.000 Mt        0.090 Mt   100.000 % 	 .../BuildTopLevelASGpu

		   20591.302 Mt    98.860 %      2970 x        6.933 Mt        8.649 Mt        7.004 Mt     0.034 % 	 ./RayTracingGpu

		    6348.591 Mt    30.831 %      2970 x        2.138 Mt        3.856 Mt     6348.591 Mt   100.000 % 	 ../DeferredRLGpu

		    6894.665 Mt    33.483 %      2970 x        2.321 Mt        2.293 Mt     6894.665 Mt   100.000 % 	 ../RayTracingRLGpu

		    7341.042 Mt    35.651 %      2970 x        2.472 Mt        2.498 Mt     7341.042 Mt   100.000 % 	 ../ResolveRLGpu

		      87.404 Mt     0.420 %      2970 x        0.029 Mt        0.362 Mt       87.404 Mt   100.000 % 	 ./Present


	============================


