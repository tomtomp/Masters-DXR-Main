Configuration: 
Command line parameters  = E:\programming\FIT-DIP\Masters-DXR-Main-git\build\Measurement\x64\Release\Measurement.exe --scene Suzanne/Suzanne.gltf --duplication-cube --profile-duplication --profile-max-duplication 10 --rt-mode --width 1024 --height 768 --profile-output DuplicationSuzanne768Rf --fast-build-as 
Build type               = Release
Device name              = NVIDIA GeForce RTX 2080 Ti
Device memory MB         = 11049
Ray Tracing backend      = Hardware
Window width             = 1024
Window height            = 768
Window title             = Direct3D App
Back-buffer count        = 3
Fill Triangles           = enabled
Triangles Font Clock-Wise= enabled
Fast Build AS            = enabled
Target FPS               = 60
Target UPS               = 60
Tearing                  = disabled
VSync                    = disabled
GUI rendering            = disabled
Debug layer              = disabled
GPU validation           = disabled
GPU profiling            = enabled
Render mode              = Ray Tracing
Loaded scene file        = Suzanne/Suzanne.gltf
Using testing scene      = disabled
Duplication              = 10
Duplication in cube      = enabled
Duplication offset       = 10
BLAS duplication         = enabled
Rays per pixel           = 13
Quality preset           = High
Rasterization Quality    : 
  AO                     = enabled
  AO kernel size         = 4
  Shadows                = enabled
  Shadow map resolution  = 4096
  Shadow PCF kernel size = 4
  Reflections            = enabled
Ray Tracing Quality      : 
  AO                     = enabled
  AO rays                = 4
  AO kernel size         = 4
  Shadows                = enabled
  Shadow rays            = 4
  Shadow kernel size     = 4
  Reflections            = enabled

ProfilingAggregator: 
Render	,	1.1198	,	1.1397	,	1.1477	,	1.1412	,	1.1831	,	1.2736	,	1.2089	,	1.2886	,	1.299	,	1.365	,	1.3791	,	1.3877	,	1.1771	,	1.1485	,	1.2919	,	1.3018	,	2.5395	,	1.4709	,	700.109	,	1.7184
Update	,	0.0149	,	0.0167	,	0.0169	,	0.0177	,	0.0142	,	0.0174	,	0.0144	,	0.0196	,	0.0154	,	0.0232	,	0.02	,	0.0228	,	0.0178	,	0.0208	,	0.0201	,	0.0223	,	0.0429	,	0.0214	,	0.036	,	0.0262
RayTracing	,	0.0514	,	0.0602	,	0.0542	,	0.0529	,	0.0516	,	0.0726	,	0.0483	,	0.0628	,	0.0511	,	0.0581	,	0.0791	,	0.0568	,	0.0483	,	0.0493	,	0.0459	,	0.0478	,	0.2552	,	0.0523	,	459.923	,	0.0706
RayTracingGpu	,	0.746176	,	0.752416	,	0.763136	,	0.774912	,	0.796992	,	0.787904	,	0.853952	,	0.86112	,	0.927968	,	0.925472	,	0.956992	,	0.966976	,	0.833376	,	0.82624	,	0.9672	,	0.973408	,	1.13421	,	1.13558	,	238.843	,	1.34499
DeferredRLGpu	,	0.015712	,	0.01616	,	0.021408	,	0.020896	,	0.028832	,	0.029568	,	0.062112	,	0.062944	,	0.117024	,	0.117664	,	0.185184	,	0.186432	,	0.217664	,	0.21824	,	0.327616	,	0.326144	,	0.46336	,	0.464064	,	0.635104	,	0.634016
RayTracingRLGpu	,	0.102368	,	0.11392	,	0.109792	,	0.118464	,	0.129504	,	0.127552	,	0.156736	,	0.161312	,	0.17056	,	0.16672	,	0.17568	,	0.182592	,	0.157312	,	0.151776	,	0.180352	,	0.189696	,	0.210432	,	0.212768	,	0.26304	,	0.250656
ResolveRLGpu	,	0.625984	,	0.618944	,	0.629824	,	0.630592	,	0.632736	,	0.627808	,	0.630752	,	0.63488	,	0.637408	,	0.637952	,	0.593088	,	0.594176	,	0.456128	,	0.452736	,	0.455072	,	0.455424	,	0.45616	,	0.456864	,	0.46016	,	0.457728
Index	,	0	,	1	,	2	,	3	,	4	,	5	,	6	,	7	,	8	,	9	,	10	,	11	,	12	,	13	,	14	,	15	,	16	,	17	,	18	,	19

ProfilingBuildAggregator: 
BuildBottomLevelAS	,	0.9732	,	2.4567	,	9.1158	,	20.0277	,	40.0409	,	62.1492	,	101.498	,	137.077	,	140.313	,	237.372
BuildBottomLevelASGpu	,	0.354144	,	2.70128	,	9.07526	,	22.9293	,	43.0323	,	74.8276	,	82.0179	,	122.77	,	172.879	,	237.298
BuildTopLevelAS	,	0.9048	,	0.7393	,	1.1088	,	0.9232	,	0.9054	,	0.8028	,	0.8408	,	0.8113	,	0.5433	,	0.9354
BuildTopLevelASGpu	,	0.082528	,	0.092064	,	0.122784	,	0.129824	,	0.173248	,	0.200672	,	0.128448	,	0.12944	,	0.165568	,	0.179776
Duplication	,	1	,	2	,	3	,	4	,	5	,	6	,	7	,	8	,	9	,	10
Triangles	,	3936	,	31488	,	106272	,	251904	,	492000	,	850176	,	1350048	,	2015232	,	2869344	,	3936000
Meshes	,	1	,	8	,	27	,	64	,	125	,	216	,	343	,	512	,	729	,	1000
BottomLevels	,	1	,	8	,	27	,	64	,	125	,	216	,	343	,	512	,	729	,	1000
TopLevelSize	,	64	,	512	,	1728	,	4096	,	8000	,	13824	,	21952	,	32768	,	46656	,	64000
BottomLevelsSize	,	222592	,	1780736	,	6009984	,	14245888	,	27824000	,	48079872	,	76349056	,	113967104	,	162269568	,	222592000
Index	,	0	,	1	,	2	,	3	,	4	,	5	,	6	,	7	,	8	,	9

FpsAggregator: 
FPS	,	54	,	59	,	57	,	59	,	54	,	60	,	51	,	59	,	45	,	60	,	37	,	59	,	26	,	59	,	17	,	60	,	3	,	59	,	3	,	60
UPS	,	59	,	60	,	61	,	60	,	60	,	59	,	61	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	38	,	103
FrameTime	,	18.5185	,	16.9492	,	17.5442	,	16.9492	,	18.5185	,	16.6867	,	19.6078	,	16.9492	,	22.2222	,	16.6892	,	27.027	,	16.9492	,	38.4615	,	16.9492	,	58.8235	,	16.6871	,	334.527	,	16.9492	,	449.557	,	16.6667
GigaRays/s	,	0.552075	,	0.603193	,	0.582736	,	0.603193	,	0.552075	,	0.61268	,	0.521404	,	0.603193	,	0.460063	,	0.612588	,	0.378274	,	0.603193	,	0.265814	,	0.603193	,	0.173801	,	0.612667	,	0.0305614	,	0.603193	,	0.0227415	,	0.613417
Index	,	0	,	1	,	2	,	3	,	4	,	5	,	6	,	7	,	8	,	9	,	10	,	11	,	12	,	13	,	14	,	15	,	16	,	17	,	18	,	19


AccelerationStructure: 
Ray tracing acceleration structure building statistics: 
	Bottom Level Acceleration Structure: 
		Maximum [B]: 222592
		Minimum [B]: 222592
		Currently Allocated [B]: 222592000
		Currently Created [num]: 1000
		Current Average [B]: 222592
		Maximum Triangles [num]: 3936
		Minimum Triangles [num]: 3936
		Total Triangles [num]: 3936000
		Maximum Meshes [num]: 1
		Minimum Meshes [num]: 1
		Total Meshes [num]: 1000
	Top Level Acceleration Structure: 
		Maximum [B]: 64000
		Minimum [B]: 64000
		Currently Allocated [B]: 64000
		Total Allocated [B]: 64000
		Total Created [num]: 1
		Average Allocated [B]: 64000
		Maximum Instances [num]: 1000
		Minimum Instances [num]: 1000
		Total Instances [num]: 1000
	Scratch Buffer: 
		Maximum [B]: 78336
		Minimum [B]: 78336
		Currently Allocated [B]: 78336
		Total Allocated [B]: 78336
		Total Created [num]: 1
		Average Allocated [B]: 78336
ProfilingStack: 
[PROF] Profiling statistics: 
	Scopes entered : 70894361
	Scopes exited : 70894360
	Overhead per scope [ticks] : 102.4
	Threads entered : 3
	Threads exited : 0
	============================
		         Sum[Mt]   Parent[%]  Samples[num]       Avg[Mt]   Last/Curr[Mt]        Self[Mt]     Self[%] 	 Name

	main:

		   23341.600 Mt   100.000 %         1 x    23341.601 Mt    23341.600 Mt        0.428 Mt     0.002 % 	 /

		   23341.181 Mt    99.998 %         1 x    23341.182 Mt    23341.181 Mt     3446.220 Mt    14.765 % 	 ./Main application loop

		       3.140 Mt     0.013 %         1 x        3.140 Mt        0.000 Mt        3.140 Mt   100.000 % 	 ../TexturedRLInitialization

		       3.530 Mt     0.015 %         1 x        3.530 Mt        0.000 Mt        3.530 Mt   100.000 % 	 ../AmbientOcclusionRLInitialization

		       5.134 Mt     0.022 %         1 x        5.134 Mt        0.000 Mt        5.134 Mt   100.000 % 	 ../RasterResolveRLInitialization

		       5.237 Mt     0.022 %         1 x        5.237 Mt        0.000 Mt        5.237 Mt   100.000 % 	 ../DeferredRLInitialization

		   19707.405 Mt    84.432 %  70880560 x        0.000 Mt        2.248 Mt    13653.440 Mt    69.281 % 	 ../MainLoop

		     141.117 Mt     0.716 %      1222 x        0.115 Mt        0.066 Mt      140.652 Mt    99.671 % 	 .../Update

		       0.465 Mt     0.329 %         1 x        0.465 Mt        0.000 Mt        0.465 Mt   100.000 % 	 ..../GuiModelApply

		    5912.848 Mt    30.003 %       942 x        6.277 Mt        2.180 Mt       39.266 Mt     0.664 % 	 .../Render

		    3123.234 Mt    52.821 %       942 x        3.316 Mt        0.102 Mt        2.094 Mt     0.067 % 	 ..../RayTracing

		      58.214 Mt     1.864 %       942 x        0.062 Mt        0.043 Mt       27.353 Mt    46.987 % 	 ...../Deferred

		      30.861 Mt    53.013 %       942 x        0.033 Mt        0.000 Mt        0.224 Mt     0.726 % 	 ....../DeferredRLPrep

		      30.637 Mt    99.274 %        18 x        1.702 Mt        0.000 Mt       22.567 Mt    73.661 % 	 ......./PrepareForRendering

		       0.543 Mt     1.774 %        18 x        0.030 Mt        0.000 Mt        0.543 Mt   100.000 % 	 ......../PrepareMaterials

		       0.003 Mt     0.008 %        18 x        0.000 Mt        0.000 Mt        0.003 Mt   100.000 % 	 ......../BuildMaterialCache

		       7.524 Mt    24.557 %        18 x        0.418 Mt        0.000 Mt        7.524 Mt   100.000 % 	 ......../PrepareDrawBundle

		    3052.603 Mt    97.739 %       942 x        3.241 Mt        0.039 Mt       23.143 Mt     0.758 % 	 ...../RayCasting

		    3029.460 Mt    99.242 %       942 x        3.216 Mt        0.000 Mt       31.279 Mt     1.032 % 	 ....../RayTracingRLPrep

		     968.349 Mt    31.964 %        18 x       53.797 Mt        0.000 Mt      968.349 Mt   100.000 % 	 ......./PrepareAccelerationStructures

		       0.251 Mt     0.008 %        18 x        0.014 Mt        0.000 Mt        0.251 Mt   100.000 % 	 ......./PrepareCache

		     384.280 Mt    12.685 %        18 x       21.349 Mt        0.000 Mt      384.280 Mt   100.000 % 	 ......./PipelineBuild

		       0.005 Mt     0.000 %        18 x        0.000 Mt        0.000 Mt        0.005 Mt   100.000 % 	 ......./BuildCache

		    1611.813 Mt    53.205 %        18 x       89.545 Mt        0.000 Mt        0.012 Mt     0.001 % 	 ......./BuildAccelerationStructures

		    1611.801 Mt    99.999 %        18 x       89.544 Mt        0.000 Mt       80.180 Mt     4.975 % 	 ......../AccelerationStructureBuild

		    1516.664 Mt    94.097 %        18 x       84.259 Mt        0.000 Mt     1516.664 Mt   100.000 % 	 ........./BuildBottomLevelAS

		      14.957 Mt     0.928 %        18 x        0.831 Mt        0.000 Mt       14.957 Mt   100.000 % 	 ........./BuildTopLevelAS

		      33.483 Mt     1.105 %        18 x        1.860 Mt        0.000 Mt       33.483 Mt   100.000 % 	 ......./BuildShaderTables

		      10.322 Mt     0.331 %       942 x        0.011 Mt        0.017 Mt       10.322 Mt   100.000 % 	 ...../Resolve

		    2750.348 Mt    46.515 %       942 x        2.920 Mt        1.993 Mt     2750.348 Mt   100.000 % 	 ..../Present

		       1.526 Mt     0.007 %         1 x        1.526 Mt        0.000 Mt        1.526 Mt   100.000 % 	 ../ShadowMapRLInitialization

		      52.293 Mt     0.224 %         1 x       52.293 Mt        0.000 Mt        3.032 Mt     5.798 % 	 ../RayTracingRLInitialization

		      49.261 Mt    94.202 %         1 x       49.261 Mt        0.000 Mt       49.261 Mt   100.000 % 	 .../PipelineBuild

		       4.689 Mt     0.020 %         1 x        4.689 Mt        0.000 Mt        4.689 Mt   100.000 % 	 ../ResolveRLInitialization

		     112.008 Mt     0.480 %         1 x      112.008 Mt        0.000 Mt        0.550 Mt     0.491 % 	 ../Initialization

		     111.459 Mt    99.509 %         1 x      111.459 Mt        0.000 Mt        1.073 Mt     0.963 % 	 .../ScenePrep

		      63.270 Mt    56.766 %         1 x       63.270 Mt        0.000 Mt       15.529 Mt    24.544 % 	 ..../SceneLoad

		      39.406 Mt    62.283 %         1 x       39.406 Mt        0.000 Mt        8.388 Mt    21.286 % 	 ...../glTF-LoadScene

		      31.018 Mt    78.714 %         2 x       15.509 Mt        0.000 Mt       31.018 Mt   100.000 % 	 ....../glTF-LoadImageData

		       6.190 Mt     9.784 %         1 x        6.190 Mt        0.000 Mt        6.190 Mt   100.000 % 	 ...../glTF-CreateScene

		       2.144 Mt     3.389 %         1 x        2.144 Mt        0.000 Mt        2.144 Mt   100.000 % 	 ...../SceneDesc-CreateScene

		       0.334 Mt     0.300 %         1 x        0.334 Mt        0.000 Mt        0.003 Mt     0.778 % 	 ..../ShadowMapRLPrep

		       0.332 Mt    99.222 %         1 x        0.332 Mt        0.000 Mt        0.324 Mt    97.618 % 	 ...../PrepareForRendering

		       0.008 Mt     2.382 %         1 x        0.008 Mt        0.000 Mt        0.008 Mt   100.000 % 	 ....../PrepareDrawBundle

		       0.964 Mt     0.864 %         1 x        0.964 Mt        0.000 Mt        0.000 Mt     0.031 % 	 ..../TexturedRLPrep

		       0.963 Mt    99.969 %         1 x        0.963 Mt        0.000 Mt        0.958 Mt    99.502 % 	 ...../PrepareForRendering

		       0.001 Mt     0.135 %         1 x        0.001 Mt        0.000 Mt        0.001 Mt   100.000 % 	 ....../PrepareMaterials

		       0.004 Mt     0.363 %         1 x        0.004 Mt        0.000 Mt        0.004 Mt   100.000 % 	 ....../PrepareDrawBundle

		       1.837 Mt     1.648 %         1 x        1.837 Mt        0.000 Mt        0.000 Mt     0.022 % 	 ..../DeferredRLPrep

		       1.837 Mt    99.978 %         1 x        1.837 Mt        0.000 Mt        1.430 Mt    77.887 % 	 ...../PrepareForRendering

		       0.001 Mt     0.049 %         1 x        0.001 Mt        0.000 Mt        0.001 Mt   100.000 % 	 ....../PrepareMaterials

		       0.402 Mt    21.895 %         1 x        0.402 Mt        0.000 Mt        0.402 Mt   100.000 % 	 ....../BuildMaterialCache

		       0.003 Mt     0.169 %         1 x        0.003 Mt        0.000 Mt        0.003 Mt   100.000 % 	 ....../PrepareDrawBundle

		      31.240 Mt    28.029 %         1 x       31.240 Mt        0.000 Mt        3.076 Mt     9.846 % 	 ..../RayTracingRLPrep

		       0.051 Mt     0.162 %         1 x        0.051 Mt        0.000 Mt        0.051 Mt   100.000 % 	 ...../PrepareAccelerationStructures

		       0.002 Mt     0.007 %         1 x        0.002 Mt        0.000 Mt        0.002 Mt   100.000 % 	 ...../PrepareCache

		      17.286 Mt    55.331 %         1 x       17.286 Mt        0.000 Mt       17.286 Mt   100.000 % 	 ...../PipelineBuild

		       0.436 Mt     1.397 %         1 x        0.436 Mt        0.000 Mt        0.436 Mt   100.000 % 	 ...../BuildCache

		       4.917 Mt    15.739 %         1 x        4.917 Mt        0.000 Mt        0.000 Mt     0.008 % 	 ...../BuildAccelerationStructures

		       4.917 Mt    99.992 %         1 x        4.917 Mt        0.000 Mt        3.039 Mt    61.803 % 	 ....../AccelerationStructureBuild

		       0.973 Mt    19.794 %         1 x        0.973 Mt        0.000 Mt        0.973 Mt   100.000 % 	 ......./BuildBottomLevelAS

		       0.905 Mt    18.403 %         1 x        0.905 Mt        0.000 Mt        0.905 Mt   100.000 % 	 ......./BuildTopLevelAS

		       5.473 Mt    17.518 %         1 x        5.473 Mt        0.000 Mt        5.473 Mt   100.000 % 	 ...../BuildShaderTables

		      12.740 Mt    11.431 %         1 x       12.740 Mt        0.000 Mt       12.740 Mt   100.000 % 	 ..../GpuSidePrep

	WindowThread:

		   23339.515 Mt   100.000 %         1 x    23339.515 Mt    23339.514 Mt    23339.515 Mt   100.000 % 	 /

	GpuThread:

		    2566.577 Mt   100.000 %       942 x        2.725 Mt        1.579 Mt      114.773 Mt     4.472 % 	 /

		       0.444 Mt     0.017 %         1 x        0.444 Mt        0.000 Mt        0.006 Mt     1.348 % 	 ./ScenePrep

		       0.438 Mt    98.652 %         1 x        0.438 Mt        0.000 Mt        0.001 Mt     0.256 % 	 ../AccelerationStructureBuild

		       0.354 Mt    80.893 %         1 x        0.354 Mt        0.000 Mt        0.354 Mt   100.000 % 	 .../BuildBottomLevelASGpu

		       0.083 Mt    18.851 %         1 x        0.083 Mt        0.000 Mt        0.083 Mt   100.000 % 	 .../BuildTopLevelASGpu

		    2449.169 Mt    95.425 %       942 x        2.600 Mt        1.440 Mt        2.900 Mt     0.118 % 	 ./RayTracingGpu

		     157.827 Mt     6.444 %       942 x        0.168 Mt        0.635 Mt      157.827 Mt   100.000 % 	 ../DeferredRLGpu

		     148.774 Mt     6.074 %       942 x        0.158 Mt        0.247 Mt      148.774 Mt   100.000 % 	 ../RayTracingRLGpu

		     541.963 Mt    22.128 %       942 x        0.575 Mt        0.556 Mt      541.963 Mt   100.000 % 	 ../ResolveRLGpu

		    1597.705 Mt    65.235 %        18 x       88.761 Mt        0.000 Mt        0.046 Mt     0.003 % 	 ../AccelerationStructureBuild

		    1594.922 Mt    99.826 %        18 x       88.607 Mt        0.000 Mt     1594.922 Mt   100.000 % 	 .../BuildBottomLevelASGpu

		       2.737 Mt     0.171 %        18 x        0.152 Mt        0.000 Mt        2.737 Mt   100.000 % 	 .../BuildTopLevelASGpu

		       2.191 Mt     0.085 %       942 x        0.002 Mt        0.138 Mt        2.191 Mt   100.000 % 	 ./Present


	============================


