Configuration: 
Command line parameters  = E:\programming\FIT-DIP\Masters-DXR-Main-git\build\Measurement\x64\Release\Measurement.exe --scene Sponza/Sponza.gltf --profile-automation --profile-camera-track Sponza/SponzaPreset.trk --width 2560 --height 1440 --profile-output PresetSponza1440RaNoEffects --quality-preset NoEffects 
Build type               = Release
Device name              = NVIDIA GeForce RTX 2080 Ti
Device memory MB         = 11049
Ray Tracing backend      = Hardware
Window width             = 2560
Window height            = 1421
Window title             = Direct3D App
Back-buffer count        = 3
Fill Triangles           = enabled
Triangles Font Clock-Wise= enabled
Fast Build AS            = disabled
Target FPS               = 0
Target UPS               = 60
Tearing                  = enabled
VSync                    = disabled
GUI rendering            = disabled
Debug layer              = disabled
GPU validation           = disabled
GPU profiling            = enabled
Render mode              = Rasterization
Loaded scene file        = Sponza/Sponza.gltf
Using testing scene      = disabled
Duplication              = 1
Duplication in cube      = disabled
Duplication offset       = 10
BLAS duplication         = disabled
Rays per pixel           = 0
Quality preset           = NoEffects
Rasterization Quality    : 
  AO                     = disabled
  AO kernel size         = 1
  Shadows                = disabled
  Shadow map resolution  = 1
  Shadow PCF kernel size = 0
  Reflections            = disabled
Ray Tracing Quality      : 
  AO                     = disabled
  AO rays                = 0
  AO kernel size         = 4
  Shadows                = disabled
  Shadow rays            = 0
  Shadow kernel size     = 4
  Reflections            = disabled

ProfilingAggregator: 
Render	,	3.925	,	3.7226	,	3.2982	,	2.6915	,	2.4197	,	2.7003	,	3.1809	,	3.4295	,	3.1774	,	2.605	,	2.4224	,	3.4115	,	3.48	,	3.7219	,	3.4222	,	2.7181	,	3.2032	,	3.1179	,	2.7176	,	3.7826	,	5.0672	,	5.0394	,	5.1764
Update	,	0.088	,	0.0811	,	0.0771	,	0.0807	,	0.0798	,	0.0792	,	0.0807	,	0.0915	,	0.0799	,	0.0806	,	0.0815	,	0.0811	,	0.0805	,	0.0797	,	0.0887	,	0.0816	,	0.0815	,	0.0809	,	0.0803	,	0.071	,	0.0788	,	0.0709	,	0.0774
DeferredRLGpu	,	2.72982	,	2.49562	,	2.18883	,	1.46643	,	1.33658	,	1.59968	,	2.00787	,	2.3895	,	2.08816	,	1.5199	,	1.3439	,	1.72531	,	2.168	,	2.43206	,	2.17136	,	1.48333	,	1.96762	,	1.89315	,	1.50371	,	2.62125	,	3.84842	,	3.85005	,	3.96192
Index	,	0	,	1	,	2	,	3	,	4	,	5	,	6	,	7	,	8	,	9	,	10	,	11	,	12	,	13	,	14	,	15	,	16	,	17	,	18	,	19	,	20	,	21	,	22

ProfilingBuildAggregator: 
BuildBottomLevelAS	,	1.7247
BuildBottomLevelASGpu	,	2.45504
BuildTopLevelAS	,	1.0059
BuildTopLevelASGpu	,	0.09008
Duplication	,	1
Triangles	,	262267
Meshes	,	103
BottomLevels	,	1
TopLevelSize	,	64
BottomLevelsSize	,	18390656
Index	,	0

FpsAggregator: 
FPS	,	165	,	245	,	294	,	313	,	388	,	370	,	344	,	269	,	282	,	321	,	377	,	357	,	308	,	274	,	270	,	344	,	324	,	306	,	333	,	343	,	210	,	188	,	189
UPS	,	59	,	60	,	61	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	61	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	61
FrameTime	,	6.06204	,	4.08624	,	3.41131	,	3.19779	,	2.58342	,	2.70951	,	2.90822	,	3.72287	,	3.55137	,	3.11964	,	2.65327	,	2.80707	,	3.2568	,	3.6588	,	3.71342	,	2.91176	,	3.09423	,	3.26991	,	3.00836	,	2.92625	,	4.76565	,	5.32293	,	5.305
GigaRays/s	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0
Index	,	0	,	1	,	2	,	3	,	4	,	5	,	6	,	7	,	8	,	9	,	10	,	11	,	12	,	13	,	14	,	15	,	16	,	17	,	18	,	19	,	20	,	21	,	22


AccelerationStructure: 
Ray tracing acceleration structure building statistics: 
	Bottom Level Acceleration Structure: 
		Maximum [B]: 18390656
		Minimum [B]: 18390656
		Currently Allocated [B]: 18390656
		Currently Created [num]: 1
		Current Average [B]: 18390656
		Maximum Triangles [num]: 262267
		Minimum Triangles [num]: 262267
		Total Triangles [num]: 262267
		Maximum Meshes [num]: 103
		Minimum Meshes [num]: 103
		Total Meshes [num]: 103
	Top Level Acceleration Structure: 
		Maximum [B]: 64
		Minimum [B]: 64
		Currently Allocated [B]: 64
		Total Allocated [B]: 64
		Total Created [num]: 1
		Average Allocated [B]: 64
		Maximum Instances [num]: 1
		Minimum Instances [num]: 1
		Total Instances [num]: 1
	Scratch Buffer: 
		Maximum [B]: 14995072
		Minimum [B]: 14995072
		Currently Allocated [B]: 14995072
		Total Allocated [B]: 14995072
		Total Created [num]: 1
		Average Allocated [B]: 14995072
ProfilingStack: 
[PROF] Profiling statistics: 
	Scopes entered : 158961
	Scopes exited : 158960
	Overhead per scope [ticks] : 104.1
	Threads entered : 3
	Threads exited : 0
	============================
		         Sum[Mt]   Parent[%]  Samples[num]       Avg[Mt]   Last/Curr[Mt]        Self[Mt]     Self[%] 	 Name

	main:

		   26009.568 Mt   100.000 %         1 x    26009.569 Mt    26009.568 Mt        0.381 Mt     0.001 % 	 /

		   26009.220 Mt    99.999 %         1 x    26009.222 Mt    26009.220 Mt      279.018 Mt     1.073 % 	 ./Main application loop

		       2.245 Mt     0.009 %         1 x        2.245 Mt        0.000 Mt        2.245 Mt   100.000 % 	 ../TexturedRLInitialization

		       1.262 Mt     0.005 %         1 x        1.262 Mt        0.000 Mt        1.262 Mt   100.000 % 	 ../ShadowMapRLInitialization

		       4.933 Mt     0.019 %         1 x        4.933 Mt        0.000 Mt        4.933 Mt   100.000 % 	 ../RasterResolveRLInitialization

		       3.720 Mt     0.014 %         1 x        3.720 Mt        0.000 Mt        3.720 Mt   100.000 % 	 ../ResolveRLInitialization

		   23064.902 Mt    88.680 %     96122 x        0.240 Mt        5.299 Mt      437.286 Mt     1.896 % 	 ../MainLoop

		     213.849 Mt     0.927 %      1383 x        0.155 Mt        0.125 Mt      213.201 Mt    99.697 % 	 .../Update

		       0.648 Mt     0.303 %         1 x        0.648 Mt        0.000 Mt        0.648 Mt   100.000 % 	 ..../GuiModelApply

		   22413.767 Mt    97.177 %      6816 x        3.288 Mt        5.171 Mt      740.801 Mt     3.305 % 	 .../Render

		     865.759 Mt     3.863 %      6816 x        0.127 Mt        0.164 Mt      854.518 Mt    98.702 % 	 ..../Rasterization

		       7.234 Mt     0.836 %      6816 x        0.001 Mt        0.001 Mt        7.234 Mt   100.000 % 	 ...../DeferredRLPrep

		       4.007 Mt     0.463 %      6816 x        0.001 Mt        0.001 Mt        4.007 Mt   100.000 % 	 ...../ShadowMapRLPrep

		   20807.207 Mt    92.832 %      6816 x        3.053 Mt        4.853 Mt    20807.207 Mt   100.000 % 	 ..../Present

		       2.937 Mt     0.011 %         1 x        2.937 Mt        0.000 Mt        2.937 Mt   100.000 % 	 ../AmbientOcclusionRLInitialization

		       5.085 Mt     0.020 %         1 x        5.085 Mt        0.000 Mt        5.085 Mt   100.000 % 	 ../DeferredRLInitialization

		      50.520 Mt     0.194 %         1 x       50.520 Mt        0.000 Mt        2.327 Mt     4.605 % 	 ../RayTracingRLInitialization

		      48.194 Mt    95.395 %         1 x       48.194 Mt        0.000 Mt       48.194 Mt   100.000 % 	 .../PipelineBuild

		    2594.597 Mt     9.976 %         1 x     2594.597 Mt        0.000 Mt        0.234 Mt     0.009 % 	 ../Initialization

		    2594.364 Mt    99.991 %         1 x     2594.364 Mt        0.000 Mt        2.209 Mt     0.085 % 	 .../ScenePrep

		    2521.298 Mt    97.184 %         1 x     2521.298 Mt        0.000 Mt      252.795 Mt    10.026 % 	 ..../SceneLoad

		    1969.519 Mt    78.115 %         1 x     1969.519 Mt        0.000 Mt      596.091 Mt    30.266 % 	 ...../glTF-LoadScene

		    1373.428 Mt    69.734 %        69 x       19.905 Mt        0.000 Mt     1373.428 Mt   100.000 % 	 ....../glTF-LoadImageData

		     226.160 Mt     8.970 %         1 x      226.160 Mt        0.000 Mt      226.160 Mt   100.000 % 	 ...../glTF-CreateScene

		      72.823 Mt     2.888 %         1 x       72.823 Mt        0.000 Mt       72.823 Mt   100.000 % 	 ...../SceneDesc-CreateScene

		       1.149 Mt     0.044 %         1 x        1.149 Mt        0.000 Mt        0.001 Mt     0.061 % 	 ..../ShadowMapRLPrep

		       1.149 Mt    99.939 %         1 x        1.149 Mt        0.000 Mt        1.106 Mt    96.283 % 	 ...../PrepareForRendering

		       0.043 Mt     3.717 %         1 x        0.043 Mt        0.000 Mt        0.043 Mt   100.000 % 	 ....../PrepareDrawBundle

		       3.686 Mt     0.142 %         1 x        3.686 Mt        0.000 Mt        0.000 Mt     0.011 % 	 ..../TexturedRLPrep

		       3.685 Mt    99.989 %         1 x        3.685 Mt        0.000 Mt        3.647 Mt    98.969 % 	 ...../PrepareForRendering

		       0.001 Mt     0.027 %         1 x        0.001 Mt        0.000 Mt        0.001 Mt   100.000 % 	 ....../PrepareMaterials

		       0.037 Mt     1.004 %         1 x        0.037 Mt        0.000 Mt        0.037 Mt   100.000 % 	 ....../PrepareDrawBundle

		       4.856 Mt     0.187 %         1 x        4.856 Mt        0.000 Mt        0.001 Mt     0.012 % 	 ..../DeferredRLPrep

		       4.856 Mt    99.988 %         1 x        4.856 Mt        0.000 Mt        4.278 Mt    88.107 % 	 ...../PrepareForRendering

		       0.001 Mt     0.014 %         1 x        0.001 Mt        0.000 Mt        0.001 Mt   100.000 % 	 ....../PrepareMaterials

		       0.494 Mt    10.165 %         1 x        0.494 Mt        0.000 Mt        0.494 Mt   100.000 % 	 ....../BuildMaterialCache

		       0.083 Mt     1.713 %         1 x        0.083 Mt        0.000 Mt        0.083 Mt   100.000 % 	 ....../PrepareDrawBundle

		      29.110 Mt     1.122 %         1 x       29.110 Mt        0.000 Mt        2.800 Mt     9.619 % 	 ..../RayTracingRLPrep

		       0.062 Mt     0.213 %         1 x        0.062 Mt        0.000 Mt        0.062 Mt   100.000 % 	 ...../PrepareAccelerationStructures

		       0.001 Mt     0.003 %         1 x        0.001 Mt        0.000 Mt        0.001 Mt   100.000 % 	 ...../PrepareCache

		      13.930 Mt    47.854 %         1 x       13.930 Mt        0.000 Mt       13.930 Mt   100.000 % 	 ...../PipelineBuild

		       0.704 Mt     2.417 %         1 x        0.704 Mt        0.000 Mt        0.704 Mt   100.000 % 	 ...../BuildCache

		       9.920 Mt    34.076 %         1 x        9.920 Mt        0.000 Mt        0.001 Mt     0.005 % 	 ...../BuildAccelerationStructures

		       9.919 Mt    99.995 %         1 x        9.919 Mt        0.000 Mt        7.189 Mt    72.471 % 	 ....../AccelerationStructureBuild

		       1.725 Mt    17.388 %         1 x        1.725 Mt        0.000 Mt        1.725 Mt   100.000 % 	 ......./BuildBottomLevelAS

		       1.006 Mt    10.141 %         1 x        1.006 Mt        0.000 Mt        1.006 Mt   100.000 % 	 ......./BuildTopLevelAS

		       1.694 Mt     5.818 %         1 x        1.694 Mt        0.000 Mt        1.694 Mt   100.000 % 	 ...../BuildShaderTables

		      32.056 Mt     1.236 %         1 x       32.056 Mt        0.000 Mt       32.056 Mt   100.000 % 	 ..../GpuSidePrep

	WindowThread:

		   26008.334 Mt   100.000 %         1 x    26008.334 Mt    26008.334 Mt    26008.334 Mt   100.000 % 	 /

	GpuThread:

		   17456.135 Mt   100.000 %      6816 x        2.561 Mt        4.342 Mt      172.172 Mt     0.986 % 	 /

		       2.554 Mt     0.015 %         1 x        2.554 Mt        0.000 Mt        0.007 Mt     0.276 % 	 ./ScenePrep

		       2.547 Mt    99.724 %         1 x        2.547 Mt        0.000 Mt        0.001 Mt     0.057 % 	 ../AccelerationStructureBuild

		       2.455 Mt    96.406 %         1 x        2.455 Mt        0.000 Mt        2.455 Mt   100.000 % 	 .../BuildBottomLevelASGpu

		       0.090 Mt     3.537 %         1 x        0.090 Mt        0.000 Mt        0.090 Mt   100.000 % 	 .../BuildTopLevelASGpu

		   17245.301 Mt    98.792 %      6816 x        2.530 Mt        4.338 Mt       13.232 Mt     0.077 % 	 ./RasterizationGpu

		   14048.724 Mt    81.464 %      6816 x        2.061 Mt        3.854 Mt    14048.724 Mt   100.000 % 	 ../DeferredRLGpu

		    3183.346 Mt    18.459 %      6816 x        0.467 Mt        0.483 Mt     3183.346 Mt   100.000 % 	 ../RasterResolveRLGpu

		      36.108 Mt     0.207 %      6816 x        0.005 Mt        0.002 Mt       36.108 Mt   100.000 % 	 ./Present


	============================


