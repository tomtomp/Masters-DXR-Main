Configuration: 
Command line parameters  = E:\programming\FIT-DIP\Masters-DXR-Main-git\build\Measurement\x64\Release\Measurement.exe --scene Cube/Cube.gltf --profile-automation --profile-camera-track Cube/Cube.trk --width 2560 --height 1440 --profile-output PresetCube1440RaAmbientOcclusion16 --quality-preset AmbientOcclusion16 
Build type               = Release
Device name              = NVIDIA GeForce RTX 2080 Ti
Device memory MB         = 11049
Ray Tracing backend      = Hardware
Window width             = 2560
Window height            = 1421
Window title             = Direct3D App
Back-buffer count        = 3
Fill Triangles           = enabled
Triangles Font Clock-Wise= enabled
Fast Build AS            = disabled
Target FPS               = 0
Target UPS               = 60
Tearing                  = enabled
VSync                    = disabled
GUI rendering            = disabled
Debug layer              = disabled
GPU validation           = disabled
GPU profiling            = enabled
Render mode              = Rasterization
Loaded scene file        = Cube/Cube.gltf
Using testing scene      = disabled
Duplication              = 1
Duplication in cube      = disabled
Duplication offset       = 10
BLAS duplication         = disabled
Rays per pixel           = 16
Quality preset           = AmbientOcclusion16
Rasterization Quality    : 
  AO                     = enabled
  AO kernel size         = 16
  Shadows                = disabled
  Shadow map resolution  = 1
  Shadow PCF kernel size = 0
  Reflections            = disabled
Ray Tracing Quality      : 
  AO                     = enabled
  AO rays                = 16
  AO kernel size         = 4
  Shadows                = disabled
  Shadow rays            = 0
  Shadow kernel size     = 4
  Reflections            = disabled

ProfilingAggregator: 
Render	,	7.924	,	8.5994	,	8.6953	,	8.4515	,	8.5561	,	8.7567	,	8.9179	,	8.8341	,	9.3431	,	9.1416	,	9.0203	,	8.8216	,	8.4532	,	8.4601	,	8.6897	,	9.0039	,	8.9159	,	9.1868	,	9.6105	,	9.191	,	9.1615	,	8.5145	,	15.5272	,	8.0464	,	7.6667	,	8.0239	,	7.9042	,	7.8095	,	7.962
Update	,	0.0815	,	0.0793	,	0.0788	,	0.079	,	0.0796	,	0.0757	,	0.0769	,	0.0811	,	0.0792	,	0.0823	,	0.0793	,	0.0756	,	0.0876	,	0.0816	,	0.0762	,	0.0744	,	0.0738	,	0.0785	,	0.075	,	0.0691	,	0.0757	,	0.0629	,	0.0747	,	0.0758	,	0.077	,	0.0752	,	0.0774	,	0.076	,	0.0745
DeferredRLGpu	,	0.195168	,	0.263264	,	0.277632	,	0.286528	,	0.3024	,	0.3744	,	0.345248	,	0.397632	,	0.462496	,	0.403232	,	0.35936	,	0.388352	,	0.304736	,	0.273728	,	0.316192	,	0.376704	,	0.442976	,	0.523456	,	0.553856	,	0.54192	,	0.440256	,	0.309664	,	0.256864	,	0.196928	,	0.15712	,	0.126624	,	0.095904	,	0.079648	,	0.069472
Index	,	0	,	1	,	2	,	3	,	4	,	5	,	6	,	7	,	8	,	9	,	10	,	11	,	12	,	13	,	14	,	15	,	16	,	17	,	18	,	19	,	20	,	21	,	22	,	23	,	24	,	25	,	26	,	27	,	28

ProfilingBuildAggregator: 
BuildBottomLevelAS	,	0.8068
BuildBottomLevelASGpu	,	0.128608
BuildTopLevelAS	,	0.8005
BuildTopLevelASGpu	,	0.063008
Duplication	,	1
Triangles	,	12
Meshes	,	1
BottomLevels	,	1
TopLevelSize	,	64
BottomLevelsSize	,	4480
Index	,	0

FpsAggregator: 
FPS	,	99	,	117	,	116	,	116	,	115	,	113	,	113	,	114	,	110	,	108	,	114	,	111	,	113	,	118	,	117	,	113	,	111	,	107	,	105	,	105	,	106	,	113	,	117	,	120	,	123	,	125	,	127	,	128	,	129
UPS	,	59	,	61	,	60	,	60	,	60	,	61	,	60	,	61	,	60	,	60	,	60	,	61	,	60	,	60	,	61	,	60	,	60	,	60	,	61	,	60	,	61	,	60	,	60	,	60	,	61	,	60	,	60	,	60	,	60
FrameTime	,	10.1744	,	8.60731	,	8.65674	,	8.64833	,	8.69978	,	8.89277	,	8.92105	,	8.83935	,	9.15717	,	9.26775	,	8.8131	,	9.06301	,	8.86005	,	8.5439	,	8.6032	,	8.85501	,	9.07768	,	9.36686	,	9.58764	,	9.59187	,	9.50784	,	8.86083	,	8.62608	,	8.36187	,	8.1588	,	8.02233	,	7.88977	,	7.825	,	7.77493
GigaRays/s	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0
Index	,	0	,	1	,	2	,	3	,	4	,	5	,	6	,	7	,	8	,	9	,	10	,	11	,	12	,	13	,	14	,	15	,	16	,	17	,	18	,	19	,	20	,	21	,	22	,	23	,	24	,	25	,	26	,	27	,	28


AccelerationStructure: 
Ray tracing acceleration structure building statistics: 
	Bottom Level Acceleration Structure: 
		Maximum [B]: 4480
		Minimum [B]: 4480
		Currently Allocated [B]: 4480
		Currently Created [num]: 1
		Current Average [B]: 4480
		Maximum Triangles [num]: 12
		Minimum Triangles [num]: 12
		Total Triangles [num]: 12
		Maximum Meshes [num]: 1
		Minimum Meshes [num]: 1
		Total Meshes [num]: 1
	Top Level Acceleration Structure: 
		Maximum [B]: 64
		Minimum [B]: 64
		Currently Allocated [B]: 64
		Total Allocated [B]: 64
		Total Created [num]: 1
		Average Allocated [B]: 64
		Maximum Instances [num]: 1
		Minimum Instances [num]: 1
		Total Instances [num]: 1
	Scratch Buffer: 
		Maximum [B]: 896
		Minimum [B]: 896
		Currently Allocated [B]: 896
		Total Allocated [B]: 896
		Total Created [num]: 1
		Average Allocated [B]: 896
ProfilingStack: 
[PROF] Profiling statistics: 
	Scopes entered : 128881
	Scopes exited : 128880
	Overhead per scope [ticks] : 102
	Threads entered : 3
	Threads exited : 0
	============================
		         Sum[Mt]   Parent[%]  Samples[num]       Avg[Mt]   Last/Curr[Mt]        Self[Mt]     Self[%] 	 Name

	main:

		   29578.789 Mt   100.000 %         1 x    29578.789 Mt    29578.788 Mt        0.444 Mt     0.002 % 	 /

		   29578.376 Mt    99.999 %         1 x    29578.377 Mt    29578.375 Mt      289.979 Mt     0.980 % 	 ./Main application loop

		       2.262 Mt     0.008 %         1 x        2.262 Mt        0.000 Mt        2.262 Mt   100.000 % 	 ../TexturedRLInitialization

		       1.498 Mt     0.005 %         1 x        1.498 Mt        0.000 Mt        1.498 Mt   100.000 % 	 ../ShadowMapRLInitialization

		       4.787 Mt     0.016 %         1 x        4.787 Mt        0.000 Mt        4.787 Mt   100.000 % 	 ../RasterResolveRLInitialization

		       3.800 Mt     0.013 %         1 x        3.800 Mt        0.000 Mt        3.800 Mt   100.000 % 	 ../ResolveRLInitialization

		   29154.354 Mt    98.566 %     93848 x        0.311 Mt       21.324 Mt      407.073 Mt     1.396 % 	 ../MainLoop

		     214.913 Mt     0.737 %      1748 x        0.123 Mt        0.086 Mt      214.284 Mt    99.707 % 	 .../Update

		       0.629 Mt     0.293 %         1 x        0.629 Mt        0.000 Mt        0.629 Mt   100.000 % 	 ..../GuiModelApply

		   28532.368 Mt    97.867 %      3324 x        8.584 Mt        7.683 Mt      378.613 Mt     1.327 % 	 .../Render

		     510.161 Mt     1.788 %      3324 x        0.153 Mt        0.197 Mt      505.036 Mt    98.996 % 	 ..../Rasterization

		       3.155 Mt     0.619 %      3324 x        0.001 Mt        0.001 Mt        3.155 Mt   100.000 % 	 ...../DeferredRLPrep

		       1.969 Mt     0.386 %      3324 x        0.001 Mt        0.006 Mt        1.969 Mt   100.000 % 	 ...../ShadowMapRLPrep

		   27643.595 Mt    96.885 %      3324 x        8.316 Mt        7.333 Mt    27643.595 Mt   100.000 % 	 ..../Present

		       2.567 Mt     0.009 %         1 x        2.567 Mt        0.000 Mt        2.567 Mt   100.000 % 	 ../AmbientOcclusionRLInitialization

		       4.365 Mt     0.015 %         1 x        4.365 Mt        0.000 Mt        4.365 Mt   100.000 % 	 ../DeferredRLInitialization

		      51.339 Mt     0.174 %         1 x       51.339 Mt        0.000 Mt        2.314 Mt     4.508 % 	 ../RayTracingRLInitialization

		      49.025 Mt    95.492 %         1 x       49.025 Mt        0.000 Mt       49.025 Mt   100.000 % 	 .../PipelineBuild

		      63.426 Mt     0.214 %         1 x       63.426 Mt        0.000 Mt        0.192 Mt     0.302 % 	 ../Initialization

		      63.234 Mt    99.698 %         1 x       63.234 Mt        0.000 Mt        1.141 Mt     1.804 % 	 .../ScenePrep

		      22.490 Mt    35.566 %         1 x       22.490 Mt        0.000 Mt        6.245 Mt    27.770 % 	 ..../SceneLoad

		      10.560 Mt    46.957 %         1 x       10.560 Mt        0.000 Mt        2.570 Mt    24.338 % 	 ...../glTF-LoadScene

		       7.990 Mt    75.662 %         2 x        3.995 Mt        0.000 Mt        7.990 Mt   100.000 % 	 ....../glTF-LoadImageData

		       2.167 Mt     9.634 %         1 x        2.167 Mt        0.000 Mt        2.167 Mt   100.000 % 	 ...../glTF-CreateScene

		       3.517 Mt    15.639 %         1 x        3.517 Mt        0.000 Mt        3.517 Mt   100.000 % 	 ...../SceneDesc-CreateScene

		       0.939 Mt     1.484 %         1 x        0.939 Mt        0.000 Mt        0.001 Mt     0.096 % 	 ..../ShadowMapRLPrep

		       0.938 Mt    99.904 %         1 x        0.938 Mt        0.000 Mt        0.929 Mt    99.104 % 	 ...../PrepareForRendering

		       0.008 Mt     0.896 %         1 x        0.008 Mt        0.000 Mt        0.008 Mt   100.000 % 	 ....../PrepareDrawBundle

		       1.464 Mt     2.315 %         1 x        1.464 Mt        0.000 Mt        0.000 Mt     0.034 % 	 ..../TexturedRLPrep

		       1.464 Mt    99.966 %         1 x        1.464 Mt        0.000 Mt        1.459 Mt    99.686 % 	 ...../PrepareForRendering

		       0.001 Mt     0.061 %         1 x        0.001 Mt        0.000 Mt        0.001 Mt   100.000 % 	 ....../PrepareMaterials

		       0.004 Mt     0.253 %         1 x        0.004 Mt        0.000 Mt        0.004 Mt   100.000 % 	 ....../PrepareDrawBundle

		       1.352 Mt     2.139 %         1 x        1.352 Mt        0.000 Mt        0.000 Mt     0.037 % 	 ..../DeferredRLPrep

		       1.352 Mt    99.963 %         1 x        1.352 Mt        0.000 Mt        1.050 Mt    77.631 % 	 ...../PrepareForRendering

		       0.000 Mt     0.030 %         1 x        0.000 Mt        0.000 Mt        0.000 Mt   100.000 % 	 ....../PrepareMaterials

		       0.299 Mt    22.124 %         1 x        0.299 Mt        0.000 Mt        0.299 Mt   100.000 % 	 ....../BuildMaterialCache

		       0.003 Mt     0.215 %         1 x        0.003 Mt        0.000 Mt        0.003 Mt   100.000 % 	 ....../PrepareDrawBundle

		      24.851 Mt    39.300 %         1 x       24.851 Mt        0.000 Mt        2.557 Mt    10.291 % 	 ..../RayTracingRLPrep

		       0.041 Mt     0.164 %         1 x        0.041 Mt        0.000 Mt        0.041 Mt   100.000 % 	 ...../PrepareAccelerationStructures

		       0.001 Mt     0.003 %         1 x        0.001 Mt        0.000 Mt        0.001 Mt   100.000 % 	 ...../PrepareCache

		      12.757 Mt    51.335 %         1 x       12.757 Mt        0.000 Mt       12.757 Mt   100.000 % 	 ...../PipelineBuild

		       0.337 Mt     1.356 %         1 x        0.337 Mt        0.000 Mt        0.337 Mt   100.000 % 	 ...../BuildCache

		       7.776 Mt    31.290 %         1 x        7.776 Mt        0.000 Mt        0.001 Mt     0.006 % 	 ...../BuildAccelerationStructures

		       7.775 Mt    99.994 %         1 x        7.775 Mt        0.000 Mt        6.168 Mt    79.328 % 	 ....../AccelerationStructureBuild

		       0.807 Mt    10.376 %         1 x        0.807 Mt        0.000 Mt        0.807 Mt   100.000 % 	 ......./BuildBottomLevelAS

		       0.800 Mt    10.295 %         1 x        0.800 Mt        0.000 Mt        0.800 Mt   100.000 % 	 ......./BuildTopLevelAS

		       1.382 Mt     5.562 %         1 x        1.382 Mt        0.000 Mt        1.382 Mt   100.000 % 	 ...../BuildShaderTables

		      10.998 Mt    17.392 %         1 x       10.998 Mt        0.000 Mt       10.998 Mt   100.000 % 	 ..../GpuSidePrep

	WindowThread:

		   29577.381 Mt   100.000 %         1 x    29577.381 Mt    29577.381 Mt    29577.381 Mt   100.000 % 	 /

	GpuThread:

		   26077.665 Mt   100.000 %      3324 x        7.845 Mt        6.715 Mt      121.516 Mt     0.466 % 	 /

		       0.199 Mt     0.001 %         1 x        0.199 Mt        0.000 Mt        0.006 Mt     3.248 % 	 ./ScenePrep

		       0.193 Mt    96.752 %         1 x        0.193 Mt        0.000 Mt        0.001 Mt     0.482 % 	 ../AccelerationStructureBuild

		       0.129 Mt    66.794 %         1 x        0.129 Mt        0.000 Mt        0.129 Mt   100.000 % 	 .../BuildBottomLevelASGpu

		       0.063 Mt    32.724 %         1 x        0.063 Mt        0.000 Mt        0.063 Mt   100.000 % 	 .../BuildTopLevelASGpu

		   25944.197 Mt    99.488 %      3324 x        7.805 Mt        6.712 Mt        8.491 Mt     0.033 % 	 ./RasterizationGpu

		    1029.970 Mt     3.970 %      3324 x        0.310 Mt        0.069 Mt     1029.970 Mt   100.000 % 	 ../DeferredRLGpu

		   24008.585 Mt    92.539 %      3324 x        7.223 Mt        6.554 Mt    24008.585 Mt   100.000 % 	 ../AmbientOcclusionRLGpu

		     897.151 Mt     3.458 %      3324 x        0.270 Mt        0.088 Mt      897.151 Mt   100.000 % 	 ../RasterResolveRLGpu

		      11.753 Mt     0.045 %      3324 x        0.004 Mt        0.003 Mt       11.753 Mt   100.000 % 	 ./Present


	============================


