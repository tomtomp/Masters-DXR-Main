Configuration: 
Command line parameters  = E:\programming\FIT-DIP\Masters-DXR-Main-git\build\Measurement\x64\Release\Measurement.exe --scene Sponza/Sponza.gltf --quality-preset Low --profile-automation --profile-camera-track Sponza/Sponza.trk --rt-mode-pure --width 2560 --height 1440 --profile-output TrackSponza1440Rp 
Build type               = Release
Device name              = NVIDIA GeForce RTX 2080 Ti
Device memory MB         = 11049
Ray Tracing backend      = Hardware
Window width             = 2560
Window height            = 1421
Window title             = Direct3D App
Back-buffer count        = 3
Fill Triangles           = enabled
Triangles Font Clock-Wise= enabled
Fast Build AS            = disabled
Target FPS               = 0
Target UPS               = 60
Tearing                  = enabled
VSync                    = disabled
GUI rendering            = disabled
Debug layer              = disabled
GPU validation           = disabled
GPU profiling            = enabled
Render mode              = Ray Tracing
Loaded scene file        = Sponza/Sponza.gltf
Using testing scene      = disabled
Duplication              = 1
Duplication in cube      = disabled
Duplication offset       = 10
BLAS duplication         = disabled
Rays per pixel           = 6
Quality preset           = Low
Rasterization Quality    : 
  AO                     = enabled
  AO kernel size         = 4
  Shadows                = enabled
  Shadow map resolution  = 2048
  Shadow PCF kernel size = 0
  Reflections            = disabled
Ray Tracing Quality      : 
  AO                     = enabled
  AO rays                = 4
  AO kernel size         = 4
  Shadows                = enabled
  Shadow rays            = 1
  Shadow kernel size     = 4
  Reflections            = disabled

ProfilingAggregator: 
Render	,	5.2288	,	5.1361	,	4.9856	,	4.822	,	4.9007	,	4.6517	,	4.4119	,	4.1276	,	4.2939	,	4.1453	,	4.9744	,	4.9242	,	4.4048	,	4.7723	,	4.4487	,	4.6386	,	4.8597	,	4.6887	,	4.7416	,	4.6042	,	4.6558	,	4.6979	,	4.8784	,	4.589	,	4.6444	,	4.6315
Update	,	0.0813	,	0.0779	,	0.0749	,	0.0737	,	0.0805	,	0.0798	,	0.0813	,	0.0828	,	0.0917	,	0.0754	,	0.0822	,	0.0806	,	0.035	,	0.0813	,	0.0744	,	0.0837	,	0.0819	,	0.0809	,	0.0828	,	0.0823	,	0.0823	,	0.0809	,	0.0816	,	0.08	,	0.0832	,	0.0789
RayTracing	,	0.0928	,	0.0943	,	0.0899	,	0.086	,	0.0949	,	0.0907	,	0.0904	,	0.0943	,	0.0938	,	0.0882	,	0.0883	,	0.095	,	0.0747	,	0.0895	,	0.0808	,	0.0998	,	0.0922	,	0.0892	,	0.0916	,	0.0939	,	0.096	,	0.0959	,	0.0983	,	0.0897	,	0.0919	,	0.0924
RayTracingGpu	,	4.47558	,	4.51098	,	4.21482	,	4.2217	,	4.12109	,	4.00346	,	3.68605	,	3.46742	,	3.64042	,	3.5617	,	4.23606	,	4.21091	,	3.97955	,	3.98445	,	3.8648	,	3.8559	,	4.12342	,	3.95418	,	3.9449	,	3.7719	,	3.86362	,	3.92771	,	4.11654	,	3.98998	,	3.95114	,	3.89709
RayTracingRLGpu	,	4.47453	,	4.51005	,	4.21405	,	4.22019	,	4.12013	,	4.00163	,	3.68403	,	3.46659	,	3.63946	,	3.55965	,	4.23514	,	4.21008	,	3.97789	,	3.98368	,	3.8641	,	3.85498	,	4.12221	,	3.9521	,	3.94397	,	3.7711	,	3.86278	,	3.92691	,	4.11574	,	3.9881	,	3.95002	,	3.89629
Index	,	0	,	1	,	2	,	3	,	4	,	5	,	6	,	7	,	8	,	9	,	10	,	11	,	12	,	13	,	14	,	15	,	16	,	17	,	18	,	19	,	20	,	21	,	22	,	23	,	24	,	25

ProfilingBuildAggregator: 
BuildBottomLevelAS	,	1.6673
BuildBottomLevelASGpu	,	2.35722
BuildTopLevelAS	,	0.6737
BuildTopLevelASGpu	,	0.089632
Duplication	,	1
Triangles	,	262267
Meshes	,	103
BottomLevels	,	1
TopLevelSize	,	64
BottomLevelsSize	,	18390656
Index	,	0

FpsAggregator: 
FPS	,	145	,	191	,	199	,	199	,	195	,	204	,	214	,	223	,	228	,	232	,	223	,	209	,	209	,	210	,	209	,	214	,	214	,	221	,	226	,	220	,	212	,	211	,	205	,	200	,	206	,	206
UPS	,	59	,	60	,	61	,	60	,	60	,	60	,	60	,	60	,	60	,	61	,	60	,	60	,	60	,	60	,	61	,	60	,	60	,	60	,	60	,	60	,	60	,	61	,	60	,	60	,	60	,	60
FrameTime	,	6.90638	,	5.23888	,	5.04244	,	5.02595	,	5.14053	,	4.9183	,	4.68264	,	4.4947	,	4.39786	,	4.32316	,	4.50373	,	4.80247	,	4.80065	,	4.77245	,	4.80343	,	4.67847	,	4.69495	,	4.53197	,	4.42819	,	4.5618	,	4.72542	,	4.75657	,	4.88545	,	5.00472	,	4.87539	,	4.8568
GigaRays/s	,	3.16035	,	4.16626	,	4.32857	,	4.34278	,	4.24597	,	4.43783	,	4.66117	,	4.85606	,	4.963	,	5.04876	,	4.84633	,	4.54487	,	4.54658	,	4.57345	,	4.54395	,	4.66532	,	4.64894	,	4.81614	,	4.929	,	4.78464	,	4.61897	,	4.58872	,	4.46767	,	4.3612	,	4.47688	,	4.49402
Index	,	0	,	1	,	2	,	3	,	4	,	5	,	6	,	7	,	8	,	9	,	10	,	11	,	12	,	13	,	14	,	15	,	16	,	17	,	18	,	19	,	20	,	21	,	22	,	23	,	24	,	25


AccelerationStructure: 
Ray tracing acceleration structure building statistics: 
	Bottom Level Acceleration Structure: 
		Maximum [B]: 18390656
		Minimum [B]: 18390656
		Currently Allocated [B]: 18390656
		Currently Created [num]: 1
		Current Average [B]: 18390656
		Maximum Triangles [num]: 262267
		Minimum Triangles [num]: 262267
		Total Triangles [num]: 262267
		Maximum Meshes [num]: 103
		Minimum Meshes [num]: 103
		Total Meshes [num]: 103
	Top Level Acceleration Structure: 
		Maximum [B]: 64
		Minimum [B]: 64
		Currently Allocated [B]: 64
		Total Allocated [B]: 64
		Total Created [num]: 1
		Average Allocated [B]: 64
		Maximum Instances [num]: 1
		Minimum Instances [num]: 1
		Total Instances [num]: 1
	Scratch Buffer: 
		Maximum [B]: 14995072
		Minimum [B]: 14995072
		Currently Allocated [B]: 14995072
		Total Allocated [B]: 14995072
		Total Created [num]: 1
		Average Allocated [B]: 14995072
ProfilingStack: 
[PROF] Profiling statistics: 
	Scopes entered : 94265
	Scopes exited : 94264
	Overhead per scope [ticks] : 101.7
	Threads entered : 3
	Threads exited : 0
	============================
		         Sum[Mt]   Parent[%]  Samples[num]       Avg[Mt]   Last/Curr[Mt]        Self[Mt]     Self[%] 	 Name

	main:

		   29763.525 Mt   100.000 %         1 x    29763.526 Mt    29763.525 Mt        0.469 Mt     0.002 % 	 /

		   29763.088 Mt    99.999 %         1 x    29763.090 Mt    29763.088 Mt      267.499 Mt     0.899 % 	 ./Main application loop

		       2.187 Mt     0.007 %         1 x        2.187 Mt        0.000 Mt        2.187 Mt   100.000 % 	 ../TexturedRLInitialization

		       1.057 Mt     0.004 %         1 x        1.057 Mt        0.000 Mt        1.057 Mt   100.000 % 	 ../ShadowMapRLInitialization

		       4.805 Mt     0.016 %         1 x        4.805 Mt        0.000 Mt        4.805 Mt   100.000 % 	 ../RasterResolveRLInitialization

		       3.701 Mt     0.012 %         1 x        3.701 Mt        0.000 Mt        3.701 Mt   100.000 % 	 ../ResolveRLInitialization

		   26087.749 Mt    87.651 %     49172 x        0.531 Mt        4.929 Mt      445.723 Mt     1.709 % 	 ../MainLoop

		     209.768 Mt     0.804 %      1565 x        0.134 Mt        0.047 Mt      209.149 Mt    99.705 % 	 .../Update

		       0.619 Mt     0.295 %         1 x        0.619 Mt        0.000 Mt        0.619 Mt   100.000 % 	 ..../GuiModelApply

		   25432.258 Mt    97.487 %      5427 x        4.686 Mt        4.782 Mt      577.751 Mt     2.272 % 	 .../Render

		     514.248 Mt     2.022 %      5427 x        0.095 Mt        0.143 Mt       33.355 Mt     6.486 % 	 ..../RayTracing

		     480.893 Mt    93.514 %      5427 x        0.089 Mt        0.134 Mt      475.536 Mt    98.886 % 	 ...../RayCasting

		       5.357 Mt     1.114 %      5427 x        0.001 Mt        0.001 Mt        5.357 Mt   100.000 % 	 ....../RayTracingRLPrep

		   24340.258 Mt    95.706 %      5427 x        4.485 Mt        4.479 Mt    24340.258 Mt   100.000 % 	 ..../Present

		       2.529 Mt     0.008 %         1 x        2.529 Mt        0.000 Mt        2.529 Mt   100.000 % 	 ../AmbientOcclusionRLInitialization

		       4.478 Mt     0.015 %         1 x        4.478 Mt        0.000 Mt        4.478 Mt   100.000 % 	 ../DeferredRLInitialization

		      49.223 Mt     0.165 %         1 x       49.223 Mt        0.000 Mt        2.077 Mt     4.219 % 	 ../RayTracingRLInitialization

		      47.147 Mt    95.781 %         1 x       47.147 Mt        0.000 Mt       47.147 Mt   100.000 % 	 .../PipelineBuild

		    3339.860 Mt    11.221 %         1 x     3339.860 Mt        0.000 Mt        0.209 Mt     0.006 % 	 ../Initialization

		    3339.650 Mt    99.994 %         1 x     3339.650 Mt        0.000 Mt        2.282 Mt     0.068 % 	 .../ScenePrep

		    3274.365 Mt    98.045 %         1 x     3274.365 Mt        0.000 Mt      927.315 Mt    28.320 % 	 ..../SceneLoad

		    2045.351 Mt    62.466 %         1 x     2045.351 Mt        0.000 Mt      583.549 Mt    28.530 % 	 ...../glTF-LoadScene

		    1461.803 Mt    71.470 %        69 x       21.186 Mt        0.000 Mt     1461.803 Mt   100.000 % 	 ....../glTF-LoadImageData

		     247.135 Mt     7.548 %         1 x      247.135 Mt        0.000 Mt      247.135 Mt   100.000 % 	 ...../glTF-CreateScene

		      54.564 Mt     1.666 %         1 x       54.564 Mt        0.000 Mt       54.564 Mt   100.000 % 	 ...../SceneDesc-CreateScene

		       0.964 Mt     0.029 %         1 x        0.964 Mt        0.000 Mt        0.001 Mt     0.073 % 	 ..../ShadowMapRLPrep

		       0.964 Mt    99.927 %         1 x        0.964 Mt        0.000 Mt        0.925 Mt    96.026 % 	 ...../PrepareForRendering

		       0.038 Mt     3.974 %         1 x        0.038 Mt        0.000 Mt        0.038 Mt   100.000 % 	 ....../PrepareDrawBundle

		       3.215 Mt     0.096 %         1 x        3.215 Mt        0.000 Mt        0.001 Mt     0.022 % 	 ..../TexturedRLPrep

		       3.214 Mt    99.978 %         1 x        3.214 Mt        0.000 Mt        3.177 Mt    98.840 % 	 ...../PrepareForRendering

		       0.001 Mt     0.037 %         1 x        0.001 Mt        0.000 Mt        0.001 Mt   100.000 % 	 ....../PrepareMaterials

		       0.036 Mt     1.123 %         1 x        0.036 Mt        0.000 Mt        0.036 Mt   100.000 % 	 ....../PrepareDrawBundle

		       2.593 Mt     0.078 %         1 x        2.593 Mt        0.000 Mt        0.000 Mt     0.015 % 	 ..../DeferredRLPrep

		       2.592 Mt    99.985 %         1 x        2.592 Mt        0.000 Mt        2.191 Mt    84.513 % 	 ...../PrepareForRendering

		       0.001 Mt     0.023 %         1 x        0.001 Mt        0.000 Mt        0.001 Mt   100.000 % 	 ....../PrepareMaterials

		       0.290 Mt    11.178 %         1 x        0.290 Mt        0.000 Mt        0.290 Mt   100.000 % 	 ....../BuildMaterialCache

		       0.111 Mt     4.285 %         1 x        0.111 Mt        0.000 Mt        0.111 Mt   100.000 % 	 ....../PrepareDrawBundle

		      24.562 Mt     0.735 %         1 x       24.562 Mt        0.000 Mt        2.271 Mt     9.246 % 	 ..../RayTracingRLPrep

		       0.071 Mt     0.288 %         1 x        0.071 Mt        0.000 Mt        0.071 Mt   100.000 % 	 ...../PrepareAccelerationStructures

		       0.001 Mt     0.003 %         1 x        0.001 Mt        0.000 Mt        0.001 Mt   100.000 % 	 ...../PrepareCache

		      14.670 Mt    59.727 %         1 x       14.670 Mt        0.000 Mt       14.670 Mt   100.000 % 	 ...../PipelineBuild

		       0.555 Mt     2.258 %         1 x        0.555 Mt        0.000 Mt        0.555 Mt   100.000 % 	 ...../BuildCache

		       4.716 Mt    19.202 %         1 x        4.716 Mt        0.000 Mt        0.000 Mt     0.008 % 	 ...../BuildAccelerationStructures

		       4.716 Mt    99.992 %         1 x        4.716 Mt        0.000 Mt        2.375 Mt    50.359 % 	 ....../AccelerationStructureBuild

		       1.667 Mt    35.355 %         1 x        1.667 Mt        0.000 Mt        1.667 Mt   100.000 % 	 ......./BuildBottomLevelAS

		       0.674 Mt    14.286 %         1 x        0.674 Mt        0.000 Mt        0.674 Mt   100.000 % 	 ......./BuildTopLevelAS

		       2.279 Mt     9.277 %         1 x        2.279 Mt        0.000 Mt        2.279 Mt   100.000 % 	 ...../BuildShaderTables

		      31.669 Mt     0.948 %         1 x       31.669 Mt        0.000 Mt       31.669 Mt   100.000 % 	 ..../GpuSidePrep

	WindowThread:

		   29762.034 Mt   100.000 %         1 x    29762.035 Mt    29762.034 Mt    29762.034 Mt   100.000 % 	 /

	GpuThread:

		   21736.918 Mt   100.000 %      5427 x        4.005 Mt        3.864 Mt      160.862 Mt     0.740 % 	 /

		       2.455 Mt     0.011 %         1 x        2.455 Mt        0.000 Mt        0.007 Mt     0.275 % 	 ./ScenePrep

		       2.448 Mt    99.725 %         1 x        2.448 Mt        0.000 Mt        0.001 Mt     0.059 % 	 ../AccelerationStructureBuild

		       2.357 Mt    96.280 %         1 x        2.357 Mt        0.000 Mt        2.357 Mt   100.000 % 	 .../BuildBottomLevelASGpu

		       0.090 Mt     3.661 %         1 x        0.090 Mt        0.000 Mt        0.090 Mt   100.000 % 	 .../BuildTopLevelASGpu

		   21561.830 Mt    99.195 %      5427 x        3.973 Mt        3.861 Mt        7.101 Mt     0.033 % 	 ./RayTracingGpu

		   21554.729 Mt    99.967 %      5427 x        3.972 Mt        3.861 Mt    21554.729 Mt   100.000 % 	 ../RayTracingRLGpu

		      11.771 Mt     0.054 %      5427 x        0.002 Mt        0.003 Mt       11.771 Mt   100.000 % 	 ./Present


	============================


