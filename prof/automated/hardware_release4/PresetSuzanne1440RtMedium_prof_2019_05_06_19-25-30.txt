Configuration: 
Command line parameters  = E:\programming\FIT-DIP\Masters-DXR-Main-git\build\Measurement\x64\Release\Measurement.exe --scene Suzanne/Suzanne.gltf --profile-automation --profile-camera-track Suzanne/Suzanne.trk --rt-mode --width 2560 --height 1440 --profile-output PresetSuzanne1440RtMedium --quality-preset Medium 
Build type               = Release
Device name              = NVIDIA GeForce RTX 2080 Ti
Device memory MB         = 11049
Ray Tracing backend      = Hardware
Window width             = 2560
Window height            = 1421
Window title             = Direct3D App
Back-buffer count        = 3
Fill Triangles           = enabled
Triangles Font Clock-Wise= enabled
Fast Build AS            = disabled
Target FPS               = 0
Target UPS               = 60
Tearing                  = enabled
VSync                    = disabled
GUI rendering            = disabled
Debug layer              = disabled
GPU validation           = disabled
GPU profiling            = enabled
Render mode              = Ray Tracing
Loaded scene file        = Suzanne/Suzanne.gltf
Using testing scene      = disabled
Duplication              = 1
Duplication in cube      = disabled
Duplication offset       = 10
BLAS duplication         = disabled
Rays per pixel           = 8
Quality preset           = Medium
Rasterization Quality    : 
  AO                     = enabled
  AO kernel size         = 4
  Shadows                = enabled
  Shadow map resolution  = 4096
  Shadow PCF kernel size = 4
  Reflections            = disabled
Ray Tracing Quality      : 
  AO                     = enabled
  AO rays                = 4
  AO kernel size         = 4
  Shadows                = enabled
  Shadow rays            = 4
  Shadow kernel size     = 4
  Reflections            = disabled

ProfilingAggregator: 
Render	,	18.6971	,	3.7543	,	3.6911	,	3.6693	,	3.3943	,	3.1588	,	3.198	,	3.0379	,	3.0995	,	3.362	,	3.8996	,	3.4056	,	3.1791	,	3.1369	,	3.436	,	4.2663	,	4.974	,	5.1866	,	6.792	,	4.9664	,	4.2296	,	3.3954	,	3.1623	,	3.0859
Update	,	0.5149	,	0.0782	,	0.077	,	0.0768	,	0.0763	,	0.0778	,	0.0763	,	0.0772	,	0.078	,	0.0789	,	0.0779	,	0.0777	,	0.0775	,	0.0769	,	0.076	,	0.0774	,	0.0781	,	0.0768	,	0.0755	,	0.0768	,	0.0768	,	0.0763	,	0.0771	,	0.0738
RayTracing	,	3.8492	,	0.1746	,	0.1505	,	0.1736	,	0.1739	,	0.1722	,	0.1734	,	0.1717	,	0.1733	,	0.1721	,	0.1718	,	0.172	,	0.173	,	0.1745	,	0.1741	,	0.1704	,	0.1705	,	0.1798	,	0.1705	,	0.1767	,	0.2022	,	0.1799	,	0.1707	,	0.1724
RayTracingGpu	,	2.6271	,	2.9351	,	3.11955	,	2.91571	,	2.56982	,	2.35805	,	2.27232	,	2.2377	,	2.33536	,	2.55046	,	3.01968	,	2.46298	,	2.28163	,	2.28608	,	2.48672	,	3.44518	,	4.00214	,	4.29059	,	5.82762	,	4.10813	,	3.3785	,	2.61277	,	2.3511	,	2.31328
DeferredRLGpu	,	0.138816	,	0.185504	,	0.198752	,	0.153344	,	0.100608	,	0.069696	,	0.053184	,	0.045952	,	0.062912	,	0.093888	,	0.104928	,	0.079296	,	0.054464	,	0.055904	,	0.0896	,	0.187296	,	0.227392	,	0.259616	,	0.408	,	0.320192	,	0.268864	,	0.13344	,	0.081216	,	0.07072
RayTracingRLGpu	,	0.435488	,	0.65136	,	0.795392	,	0.667968	,	0.417952	,	0.261536	,	0.200608	,	0.176	,	0.242336	,	0.399968	,	0.481248	,	0.342752	,	0.214208	,	0.214304	,	0.344704	,	0.769088	,	1.45878	,	1.79987	,	2.97984	,	1.54461	,	0.958048	,	0.423328	,	0.255008	,	0.224544
ResolveRLGpu	,	2.0511	,	2.09622	,	2.12349	,	2.09226	,	2.04963	,	2.02515	,	2.01581	,	2.01411	,	2.02851	,	2.05488	,	2.43184	,	2.03917	,	2.01018	,	2.01427	,	2.04832	,	2.48669	,	2.31331	,	2.22918	,	2.43814	,	2.24016	,	2.14954	,	2.05408	,	2.01306	,	2.01517
Index	,	0	,	1	,	2	,	3	,	4	,	5	,	6	,	7	,	8	,	9	,	10	,	11	,	12	,	13	,	14	,	15	,	16	,	17	,	18	,	19	,	20	,	21	,	22	,	23

ProfilingBuildAggregator: 
BuildBottomLevelAS	,	0.8086
BuildBottomLevelASGpu	,	0.448864
BuildTopLevelAS	,	0.8197
BuildTopLevelASGpu	,	0.064064
Duplication	,	1
Triangles	,	3936
Meshes	,	1
BottomLevels	,	1
TopLevelSize	,	64
BottomLevelsSize	,	282112
Index	,	0

FpsAggregator: 
FPS	,	225	,	264	,	250	,	251	,	272	,	296	,	308	,	316	,	314	,	299	,	282	,	285	,	304	,	319	,	306	,	273	,	233	,	212	,	173	,	177	,	219	,	257	,	296	,	317
UPS	,	59	,	61	,	61	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	61	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	61	,	60
FrameTime	,	4.51717	,	3.80049	,	4.00826	,	3.99438	,	3.67762	,	3.38152	,	3.25211	,	3.16829	,	3.18718	,	3.34914	,	3.55944	,	3.51518	,	3.29116	,	3.1378	,	3.27082	,	3.66574	,	4.29539	,	4.72855	,	5.79021	,	5.66344	,	4.57419	,	3.90003	,	3.38909	,	3.16362
GigaRays/s	,	6.44255	,	7.65745	,	7.26053	,	7.28575	,	7.91328	,	8.60623	,	8.94869	,	9.18543	,	9.13099	,	8.68941	,	8.17603	,	8.27897	,	8.8425	,	9.27469	,	8.89748	,	7.93893	,	6.77519	,	6.15455	,	5.02608	,	5.13859	,	6.36224	,	7.46202	,	8.58699	,	9.19899
Index	,	0	,	1	,	2	,	3	,	4	,	5	,	6	,	7	,	8	,	9	,	10	,	11	,	12	,	13	,	14	,	15	,	16	,	17	,	18	,	19	,	20	,	21	,	22	,	23


AccelerationStructure: 
Ray tracing acceleration structure building statistics: 
	Bottom Level Acceleration Structure: 
		Maximum [B]: 282112
		Minimum [B]: 282112
		Currently Allocated [B]: 282112
		Currently Created [num]: 1
		Current Average [B]: 282112
		Maximum Triangles [num]: 3936
		Minimum Triangles [num]: 3936
		Total Triangles [num]: 3936
		Maximum Meshes [num]: 1
		Minimum Meshes [num]: 1
		Total Meshes [num]: 1
	Top Level Acceleration Structure: 
		Maximum [B]: 64
		Minimum [B]: 64
		Currently Allocated [B]: 64
		Total Allocated [B]: 64
		Total Created [num]: 1
		Average Allocated [B]: 64
		Maximum Instances [num]: 1
		Minimum Instances [num]: 1
		Total Instances [num]: 1
	Scratch Buffer: 
		Maximum [B]: 82944
		Minimum [B]: 82944
		Currently Allocated [B]: 82944
		Total Allocated [B]: 82944
		Total Created [num]: 1
		Average Allocated [B]: 82944
ProfilingStack: 
[PROF] Profiling statistics: 
	Scopes entered : 143548
	Scopes exited : 143547
	Overhead per scope [ticks] : 113.4
	Threads entered : 3
	Threads exited : 0
	============================
		         Sum[Mt]   Parent[%]  Samples[num]       Avg[Mt]   Last/Curr[Mt]        Self[Mt]     Self[%] 	 Name

	main:

		   24493.180 Mt   100.000 %         1 x    24493.181 Mt    24493.180 Mt        0.335 Mt     0.001 % 	 /

		   24492.877 Mt    99.999 %         1 x    24492.879 Mt    24492.877 Mt      249.461 Mt     1.019 % 	 ./Main application loop

		       2.342 Mt     0.010 %         1 x        2.342 Mt        0.000 Mt        2.342 Mt   100.000 % 	 ../TexturedRLInitialization

		       1.286 Mt     0.005 %         1 x        1.286 Mt        0.000 Mt        1.286 Mt   100.000 % 	 ../ShadowMapRLInitialization

		       4.468 Mt     0.018 %         1 x        4.468 Mt        0.000 Mt        4.468 Mt   100.000 % 	 ../RasterResolveRLInitialization

		       3.739 Mt     0.015 %         1 x        3.739 Mt        0.000 Mt        3.739 Mt   100.000 % 	 ../ResolveRLInitialization

		   24071.122 Mt    98.278 %     58209 x        0.414 Mt        3.292 Mt      418.980 Mt     1.741 % 	 ../MainLoop

		     198.095 Mt     0.823 %      1444 x        0.137 Mt        0.095 Mt      197.503 Mt    99.701 % 	 .../Update

		       0.592 Mt     0.299 %         1 x        0.592 Mt        0.000 Mt        0.592 Mt   100.000 % 	 ..../GuiModelApply

		   23454.047 Mt    97.436 %      6450 x        3.636 Mt        3.193 Mt      702.631 Mt     2.996 % 	 .../Render

		    1124.808 Mt     4.796 %      6450 x        0.174 Mt        0.210 Mt       43.410 Mt     3.859 % 	 ..../RayTracing

		     498.701 Mt    44.337 %      6450 x        0.077 Mt        0.097 Mt      491.926 Mt    98.641 % 	 ...../Deferred

		       6.775 Mt     1.359 %      6450 x        0.001 Mt        0.001 Mt        6.775 Mt   100.000 % 	 ....../DeferredRLPrep

		     397.377 Mt    35.328 %      6450 x        0.062 Mt        0.075 Mt      393.575 Mt    99.043 % 	 ...../RayCasting

		       3.802 Mt     0.957 %      6450 x        0.001 Mt        0.001 Mt        3.802 Mt   100.000 % 	 ....../RayTracingRLPrep

		     185.321 Mt    16.476 %      6450 x        0.029 Mt        0.031 Mt      185.321 Mt   100.000 % 	 ...../Resolve

		   21626.608 Mt    92.208 %      6450 x        3.353 Mt        2.850 Mt    21626.608 Mt   100.000 % 	 ..../Present

		       2.756 Mt     0.011 %         1 x        2.756 Mt        0.000 Mt        2.756 Mt   100.000 % 	 ../AmbientOcclusionRLInitialization

		       4.939 Mt     0.020 %         1 x        4.939 Mt        0.000 Mt        4.939 Mt   100.000 % 	 ../DeferredRLInitialization

		      50.694 Mt     0.207 %         1 x       50.694 Mt        0.000 Mt        2.453 Mt     4.838 % 	 ../RayTracingRLInitialization

		      48.242 Mt    95.162 %         1 x       48.242 Mt        0.000 Mt       48.242 Mt   100.000 % 	 .../PipelineBuild

		     102.070 Mt     0.417 %         1 x      102.070 Mt        0.000 Mt        0.424 Mt     0.415 % 	 ../Initialization

		     101.646 Mt    99.585 %         1 x      101.646 Mt        0.000 Mt        1.007 Mt     0.991 % 	 .../ScenePrep

		      62.775 Mt    61.759 %         1 x       62.775 Mt        0.000 Mt       15.710 Mt    25.025 % 	 ..../SceneLoad

		      39.210 Mt    62.462 %         1 x       39.210 Mt        0.000 Mt        8.359 Mt    21.319 % 	 ...../glTF-LoadScene

		      30.851 Mt    78.681 %         2 x       15.426 Mt        0.000 Mt       30.851 Mt   100.000 % 	 ....../glTF-LoadImageData

		       6.071 Mt     9.671 %         1 x        6.071 Mt        0.000 Mt        6.071 Mt   100.000 % 	 ...../glTF-CreateScene

		       1.784 Mt     2.842 %         1 x        1.784 Mt        0.000 Mt        1.784 Mt   100.000 % 	 ...../SceneDesc-CreateScene

		       0.282 Mt     0.278 %         1 x        0.282 Mt        0.000 Mt        0.000 Mt     0.106 % 	 ..../ShadowMapRLPrep

		       0.282 Mt    99.894 %         1 x        0.282 Mt        0.000 Mt        0.274 Mt    97.162 % 	 ...../PrepareForRendering

		       0.008 Mt     2.838 %         1 x        0.008 Mt        0.000 Mt        0.008 Mt   100.000 % 	 ....../PrepareDrawBundle

		       1.413 Mt     1.391 %         1 x        1.413 Mt        0.000 Mt        0.000 Mt     0.028 % 	 ..../TexturedRLPrep

		       1.413 Mt    99.972 %         1 x        1.413 Mt        0.000 Mt        1.408 Mt    99.667 % 	 ...../PrepareForRendering

		       0.001 Mt     0.078 %         1 x        0.001 Mt        0.000 Mt        0.001 Mt   100.000 % 	 ....../PrepareMaterials

		       0.004 Mt     0.255 %         1 x        0.004 Mt        0.000 Mt        0.004 Mt   100.000 % 	 ....../PrepareDrawBundle

		       1.412 Mt     1.390 %         1 x        1.412 Mt        0.000 Mt        0.001 Mt     0.035 % 	 ..../DeferredRLPrep

		       1.412 Mt    99.965 %         1 x        1.412 Mt        0.000 Mt        1.097 Mt    77.668 % 	 ...../PrepareForRendering

		       0.000 Mt     0.028 %         1 x        0.000 Mt        0.000 Mt        0.000 Mt   100.000 % 	 ....../PrepareMaterials

		       0.312 Mt    22.084 %         1 x        0.312 Mt        0.000 Mt        0.312 Mt   100.000 % 	 ....../BuildMaterialCache

		       0.003 Mt     0.220 %         1 x        0.003 Mt        0.000 Mt        0.003 Mt   100.000 % 	 ....../PrepareDrawBundle

		      22.415 Mt    22.052 %         1 x       22.415 Mt        0.000 Mt        2.601 Mt    11.604 % 	 ..../RayTracingRLPrep

		       0.058 Mt     0.257 %         1 x        0.058 Mt        0.000 Mt        0.058 Mt   100.000 % 	 ...../PrepareAccelerationStructures

		       0.001 Mt     0.003 %         1 x        0.001 Mt        0.000 Mt        0.001 Mt   100.000 % 	 ...../PrepareCache

		      13.947 Mt    62.221 %         1 x       13.947 Mt        0.000 Mt       13.947 Mt   100.000 % 	 ...../PipelineBuild

		       0.326 Mt     1.453 %         1 x        0.326 Mt        0.000 Mt        0.326 Mt   100.000 % 	 ...../BuildCache

		       4.070 Mt    18.156 %         1 x        4.070 Mt        0.000 Mt        0.001 Mt     0.015 % 	 ...../BuildAccelerationStructures

		       4.069 Mt    99.985 %         1 x        4.069 Mt        0.000 Mt        2.441 Mt    59.983 % 	 ....../AccelerationStructureBuild

		       0.809 Mt    19.872 %         1 x        0.809 Mt        0.000 Mt        0.809 Mt   100.000 % 	 ......./BuildBottomLevelAS

		       0.820 Mt    20.145 %         1 x        0.820 Mt        0.000 Mt        0.820 Mt   100.000 % 	 ......./BuildTopLevelAS

		       1.414 Mt     6.307 %         1 x        1.414 Mt        0.000 Mt        1.414 Mt   100.000 % 	 ...../BuildShaderTables

		      12.341 Mt    12.141 %         1 x       12.341 Mt        0.000 Mt       12.341 Mt   100.000 % 	 ..../GpuSidePrep

	WindowThread:

		   24490.231 Mt   100.000 %         1 x    24490.231 Mt    24490.231 Mt    24490.231 Mt   100.000 % 	 /

	GpuThread:

		   18359.393 Mt   100.000 %      6450 x        2.846 Mt        2.318 Mt      128.360 Mt     0.699 % 	 /

		       0.520 Mt     0.003 %         1 x        0.520 Mt        0.000 Mt        0.005 Mt     0.966 % 	 ./ScenePrep

		       0.515 Mt    99.034 %         1 x        0.515 Mt        0.000 Mt        0.002 Mt     0.373 % 	 ../AccelerationStructureBuild

		       0.449 Mt    87.184 %         1 x        0.449 Mt        0.000 Mt        0.449 Mt   100.000 % 	 .../BuildBottomLevelASGpu

		       0.064 Mt    12.443 %         1 x        0.064 Mt        0.000 Mt        0.064 Mt   100.000 % 	 .../BuildTopLevelASGpu

		   18095.785 Mt    98.564 %      6450 x        2.806 Mt        2.315 Mt       14.784 Mt     0.082 % 	 ./RayTracingGpu

		     844.288 Mt     4.666 %      6450 x        0.131 Mt        0.071 Mt      844.288 Mt   100.000 % 	 ../DeferredRLGpu

		    3615.057 Mt    19.977 %      6450 x        0.560 Mt        0.223 Mt     3615.057 Mt   100.000 % 	 ../RayTracingRLGpu

		   13621.655 Mt    75.275 %      6450 x        2.112 Mt        2.020 Mt    13621.655 Mt   100.000 % 	 ../ResolveRLGpu

		     134.729 Mt     0.734 %      6450 x        0.021 Mt        0.002 Mt      134.729 Mt   100.000 % 	 ./Present


	============================


