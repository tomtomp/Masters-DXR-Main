Configuration: 
Command line parameters  = E:\programming\FIT-DIP\Masters-DXR-Main-git\build\Measurement\x64\Debug\Measurement.exe --scene Suzanne/Suzanne.gltf --duplication-cube --profile-duplication --profile-max-duplication 10 --rt-mode --width 1024 --height 768 --profile-output DuplicationSuzanne768Rf --fast-build-as 
Window width             = 1024
Window height            = 768
Window title             = Direct3D App
Back-buffer count        = 3
Fill Triangles           = enabled
Triangles Font Clock-Wise= enabled
Fast Build AS            = enabled
Target FPS               = 60
Target UPS               = 60
Tearing                  = disabled
VSync                    = disabled
GUI rendering            = disabled
Debug layer              = enabled
GPU validation           = disabled
GPU profiling            = enabled
Render mode              = Ray Tracing
Loaded scene file        = Suzanne/Suzanne.gltf
Using testing scene      = disabled
Duplication              = 10
Duplication in cube      = enabled
Duplication offset       = 10
BLAS duplication         = enabled
Rays per pixel           = 10

ProfilingAggregator: 

ProfilingBuildAggregator: 

FpsAggregator: 


AccelerationStructure: 
Ray tracing acceleration structure building statistics: 
	Bottom Level Acceleration Structure: 
		Maximum [B]: 224128
		Minimum [B]: 224128
		Currently Allocated [B]: 224128000
		Currently Created [num]: 1000
		Current Average [B]: 224128
		Maximum Triangles [num]: 3936
		Minimum Triangles [num]: 3936
		Total Triangles [num]: 3936000
		Maximum Meshes [num]: 1
		Minimum Meshes [num]: 1
		Total Meshes [num]: 1000
	Top Level Acceleration Structure: 
		Maximum [B]: 64000
		Minimum [B]: 64000
		Currently Allocated [B]: 64000
		Total Allocated [B]: 64000
		Total Created [num]: 1
		Average Allocated [B]: 64000
		Maximum Instances [num]: 1000
		Minimum Instances [num]: 1000
		Total Instances [num]: 1000
	Scratch Buffer: 
		Maximum [B]: 79360
		Minimum [B]: 79360
		Currently Allocated [B]: 79360
		Total Allocated [B]: 79360
		Total Created [num]: 1
		Average Allocated [B]: 79360
ProfilingStack: 
[PROF] Profiling statistics: 
	Scopes entered : 2525043
	Scopes exited : 2525042
	Overhead per scope [ticks] : 1014
	Threads entered : 3
	Threads exited : 0
	============================
		         Sum[Mt]   Parent[%]  Samples[num]       Avg[Mt]   Last/Curr[Mt]        Self[Mt]     Self[%] 	 Name

	main:

		   21514.040 Mt   100.000 %         1 x    21514.041 Mt    21514.039 Mt        0.698 Mt     0.003 % 	 /

		   21513.387 Mt    99.997 %         1 x    21513.389 Mt    21513.387 Mt     6260.103 Mt    29.099 % 	 ./Main application loop

		       2.786 Mt     0.013 %         1 x        2.786 Mt        0.000 Mt        2.786 Mt   100.000 % 	 ../TexturedRLInitialization

		       5.687 Mt     0.026 %         1 x        5.687 Mt        0.000 Mt        5.687 Mt   100.000 % 	 ../DeferredRLInitialization

		      54.971 Mt     0.256 %         1 x       54.971 Mt        0.000 Mt        2.116 Mt     3.849 % 	 ../RayTracingRLInitialization

		      52.856 Mt    96.151 %         1 x       52.856 Mt        0.000 Mt       52.856 Mt   100.000 % 	 .../PipelineBuild

		       3.298 Mt     0.015 %         1 x        3.298 Mt        0.000 Mt        3.298 Mt   100.000 % 	 ../ResolveRLInitialization

		     239.206 Mt     1.112 %         1 x      239.206 Mt        0.000 Mt        3.025 Mt     1.265 % 	 ../Initialization

		     236.181 Mt    98.735 %         1 x      236.181 Mt        0.000 Mt        1.195 Mt     0.506 % 	 .../ScenePrep

		     204.637 Mt    86.644 %         1 x      204.637 Mt        0.000 Mt       18.632 Mt     9.105 % 	 ..../SceneLoad

		     163.765 Mt    80.027 %         1 x      163.765 Mt        0.000 Mt       19.278 Mt    11.771 % 	 ...../glTF-LoadScene

		     144.487 Mt    88.229 %         2 x       72.244 Mt        0.000 Mt      144.487 Mt   100.000 % 	 ....../glTF-LoadImageData

		      14.473 Mt     7.073 %         1 x       14.473 Mt        0.000 Mt       14.473 Mt   100.000 % 	 ...../glTF-CreateScene

		       7.767 Mt     3.795 %         1 x        7.767 Mt        0.000 Mt        7.767 Mt   100.000 % 	 ...../SceneDesc-CreateScene

		       1.026 Mt     0.434 %         1 x        1.026 Mt        0.000 Mt        0.004 Mt     0.439 % 	 ..../TexturedRLPrep

		       1.021 Mt    99.561 %         1 x        1.021 Mt        0.000 Mt        0.330 Mt    32.263 % 	 ...../PrepareForRendering

		       0.401 Mt    39.283 %         1 x        0.401 Mt        0.000 Mt        0.401 Mt   100.000 % 	 ....../PrepareMaterials

		       0.291 Mt    28.454 %         1 x        0.291 Mt        0.000 Mt        0.291 Mt   100.000 % 	 ....../PrepareDrawBundle

		       4.234 Mt     1.793 %         1 x        4.234 Mt        0.000 Mt        0.005 Mt     0.118 % 	 ..../DeferredRLPrep

		       4.229 Mt    99.882 %         1 x        4.229 Mt        0.000 Mt        1.596 Mt    37.728 % 	 ...../PrepareForRendering

		       0.015 Mt     0.367 %         1 x        0.015 Mt        0.000 Mt        0.015 Mt   100.000 % 	 ....../PrepareMaterials

		       1.937 Mt    45.802 %         1 x        1.937 Mt        0.000 Mt        1.937 Mt   100.000 % 	 ....../BuildMaterialCache

		       0.681 Mt    16.103 %         1 x        0.681 Mt        0.000 Mt        0.681 Mt   100.000 % 	 ....../PrepareDrawBundle

		      13.049 Mt     5.525 %         1 x       13.049 Mt        0.000 Mt        4.387 Mt    33.616 % 	 ..../RayTracingRLPrep

		       0.550 Mt     4.213 %         1 x        0.550 Mt        0.000 Mt        0.550 Mt   100.000 % 	 ...../PrepareAccelerationStructures

		       0.055 Mt     0.419 %         1 x        0.055 Mt        0.000 Mt        0.055 Mt   100.000 % 	 ...../PrepareCache

		       0.409 Mt     3.131 %         1 x        0.409 Mt        0.000 Mt        0.409 Mt   100.000 % 	 ...../BuildCache

		       5.941 Mt    45.527 %         1 x        5.941 Mt        0.000 Mt        0.005 Mt     0.077 % 	 ...../BuildAccelerationStructures

		       5.936 Mt    99.923 %         1 x        5.936 Mt        0.000 Mt        4.027 Mt    67.842 % 	 ....../AccelerationStructureBuild

		       0.960 Mt    16.167 %         1 x        0.960 Mt        0.000 Mt        0.960 Mt   100.000 % 	 ......./BuildBottomLevelAS

		       0.949 Mt    15.991 %         1 x        0.949 Mt        0.000 Mt        0.949 Mt   100.000 % 	 ......./BuildTopLevelAS

		       1.708 Mt    13.093 %         1 x        1.708 Mt        0.000 Mt        1.708 Mt   100.000 % 	 ...../BuildShaderTables

		      12.040 Mt     5.098 %         1 x       12.040 Mt        0.000 Mt       12.040 Mt   100.000 % 	 ..../GpuSidePrep

		   14947.335 Mt    69.479 %   2515095 x        0.006 Mt       11.659 Mt     4264.001 Mt    28.527 % 	 ../MainLoop

		    1747.894 Mt    11.694 %      1216 x        1.437 Mt        1.407 Mt     1747.029 Mt    99.951 % 	 .../Update

		       0.865 Mt     0.049 %         1 x        0.865 Mt        0.000 Mt        0.865 Mt   100.000 % 	 ..../GuiModelApply

		    8935.441 Mt    59.779 %       946 x        9.445 Mt       10.244 Mt     4564.603 Mt    51.084 % 	 .../Render

		    4370.837 Mt    48.916 %       946 x        4.620 Mt        7.864 Mt     2643.355 Mt    60.477 % 	 ..../RayTracing

		      85.638 Mt     1.959 %       946 x        0.091 Mt        0.003 Mt        2.627 Mt     3.068 % 	 ...../DeferredRLPrep

		      83.010 Mt    96.932 %        12 x        6.918 Mt        0.000 Mt        4.473 Mt     5.389 % 	 ....../PrepareForRendering

		       8.313 Mt    10.014 %        12 x        0.693 Mt        0.000 Mt        8.313 Mt   100.000 % 	 ......./PrepareMaterials

		       0.009 Mt     0.011 %        12 x        0.001 Mt        0.000 Mt        0.009 Mt   100.000 % 	 ......./BuildMaterialCache

		      70.215 Mt    84.586 %        12 x        5.851 Mt        0.000 Mt       70.215 Mt   100.000 % 	 ......./PrepareDrawBundle

		    1641.844 Mt    37.564 %       946 x        1.736 Mt        0.004 Mt       21.789 Mt     1.327 % 	 ...../RayTracingRLPrep

		     700.980 Mt    42.695 %        12 x       58.415 Mt        0.000 Mt      700.980 Mt   100.000 % 	 ....../PrepareAccelerationStructures

		       8.254 Mt     0.503 %        12 x        0.688 Mt        0.000 Mt        8.254 Mt   100.000 % 	 ....../PrepareCache

		       0.008 Mt     0.000 %        12 x        0.001 Mt        0.000 Mt        0.008 Mt   100.000 % 	 ....../BuildCache

		     831.652 Mt    50.654 %        12 x       69.304 Mt        0.000 Mt        0.028 Mt     0.003 % 	 ....../BuildAccelerationStructures

		     831.624 Mt    99.997 %        12 x       69.302 Mt        0.000 Mt       30.198 Mt     3.631 % 	 ......./AccelerationStructureBuild

		     791.971 Mt    95.232 %        12 x       65.998 Mt        0.000 Mt      791.971 Mt   100.000 % 	 ......../BuildBottomLevelAS

		       9.455 Mt     1.137 %        12 x        0.788 Mt        0.000 Mt        9.455 Mt   100.000 % 	 ......../BuildTopLevelAS

		      79.163 Mt     4.822 %        12 x        6.597 Mt        0.000 Mt       79.163 Mt   100.000 % 	 ....../BuildShaderTables

	WindowThread:

		   21509.840 Mt   100.000 %         1 x    21509.840 Mt    21509.839 Mt    21509.840 Mt   100.000 % 	 /

	GpuThread:

		    3260.276 Mt   100.000 %       946 x        3.446 Mt        0.959 Mt      107.290 Mt     3.291 % 	 /

		       0.447 Mt     0.014 %         1 x        0.447 Mt        0.000 Mt        0.006 Mt     1.453 % 	 ./ScenePrep

		       0.441 Mt    98.547 %         1 x        0.441 Mt        0.000 Mt        0.001 Mt     0.283 % 	 ../AccelerationStructureBuild

		       0.356 Mt    80.794 %         1 x        0.356 Mt        0.000 Mt        0.356 Mt   100.000 % 	 .../BuildBottomLevelASGpu

		       0.083 Mt    18.922 %         1 x        0.083 Mt        0.000 Mt        0.083 Mt   100.000 % 	 .../BuildTopLevelASGpu

		    3128.716 Mt    95.965 %       946 x        3.307 Mt        0.958 Mt        5.253 Mt     0.168 % 	 ./RayTracingGpu

		     235.865 Mt     7.539 %       946 x        0.249 Mt        0.608 Mt      235.865 Mt   100.000 % 	 ../DeferredRLGpu

		     715.938 Mt    22.883 %       946 x        0.757 Mt        0.297 Mt      715.938 Mt   100.000 % 	 ../RayTracingRLGpu

		     447.281 Mt    14.296 %       946 x        0.473 Mt        0.051 Mt      447.281 Mt   100.000 % 	 ../ResolveRLGpu

		    1724.379 Mt    55.115 %        12 x      143.698 Mt        0.000 Mt        0.030 Mt     0.002 % 	 ../AccelerationStructureBuild

		    1720.510 Mt    99.776 %        12 x      143.376 Mt        0.000 Mt     1720.510 Mt   100.000 % 	 .../BuildBottomLevelASGpu

		       3.839 Mt     0.223 %        12 x        0.320 Mt        0.000 Mt        3.839 Mt   100.000 % 	 .../BuildTopLevelASGpu

		      23.823 Mt     0.731 %       946 x        0.025 Mt        0.002 Mt       23.823 Mt   100.000 % 	 ./Present


	============================


