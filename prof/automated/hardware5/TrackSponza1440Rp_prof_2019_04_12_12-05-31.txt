Configuration: 
Command line parameters  = E:\programming\FIT-DIP\Masters-DXR-Main-git\build\Measurement\x64\Debug\Measurement.exe --scene Sponza/Sponza.gltf --profile-automation --profile-camera-track Sponza/Sponza.trk --rt-mode-pure --width 2560 --height 1440 --profile-output TrackSponza1440Rp 
Window width             = 2560
Window height            = 1421
Window title             = Direct3D App
Back-buffer count        = 3
Fill Triangles           = enabled
Triangles Font Clock-Wise= enabled
Fast Build AS            = disabled
Target FPS               = 0
Target UPS               = 60
Tearing                  = enabled
VSync                    = disabled
GUI rendering            = disabled
Debug layer              = enabled
GPU validation           = disabled
GPU profiling            = enabled
Render mode              = Ray Tracing
Loaded scene file        = Sponza/Sponza.gltf
Using testing scene      = disabled
Duplication              = 1
Duplication in cube      = disabled
Duplication offset       = 10
BLAS duplication         = disabled
Rays per pixel           = 9

ProfilingAggregator: 
Render	,	9.6004	,	9.6579	,	9.6127	,	9.3779	,	9.0861	,	8.8324	,	8.7289	,	8.0044	,	8.319	,	8.4519	,	8.6802	,	9.0702	,	9.5694	,	9.1392	,	9.2323	,	8.692	,	9.1953	,	9.2145	,	8.6107	,	10.1741	,	11.9033	,	11.0439	,	9.9967	,	9.2954	,	9.0303	,	9.1597
Update	,	0.4991	,	0.719	,	0.6178	,	0.3563	,	0.3653	,	0.5156	,	0.5802	,	0.3533	,	0.6961	,	0.7156	,	0.7142	,	0.4125	,	0.525	,	0.427	,	0.3353	,	0.3393	,	0.362	,	0.4252	,	0.3989	,	0.5667	,	0.3366	,	0.3229	,	0.33	,	0.356	,	0.4062	,	0.3605
RayTracing	,	1.4705	,	1.5763	,	1.3695	,	1.3095	,	1.3566	,	1.3009	,	1.3348	,	1.3342	,	1.302	,	1.311	,	1.2982	,	1.3335	,	1.486	,	1.3339	,	1.4421	,	1.3204	,	1.3316	,	1.5583	,	1.321	,	1.3393	,	1.365	,	1.3804	,	1.3564	,	1.371	,	1.512	,	1.3723
RayTracingGpu	,	6.75354	,	6.86211	,	6.86182	,	6.89104	,	6.30186	,	6.18118	,	6.20726	,	5.5425	,	5.70102	,	5.80422	,	6.14922	,	6.39965	,	6.45882	,	6.4583	,	6.2439	,	6.11856	,	6.25712	,	6.45677	,	6.2049	,	7.65402	,	9.15075	,	8.29754	,	7.27651	,	6.5407	,	6.40723	,	6.33699
RayTracingRLGpu	,	6.47098	,	6.57997	,	6.58755	,	6.61059	,	6.03683	,	5.91674	,	5.92464	,	5.26035	,	5.42778	,	5.53056	,	5.87562	,	6.12438	,	6.18499	,	6.19446	,	5.97062	,	5.8545	,	5.98314	,	6.17475	,	5.92083	,	7.3711	,	8.86525	,	8.02438	,	7.00275	,	6.26554	,	6.12173	,	6.06093
ResolveRLGpu	,	0.281408	,	0.281216	,	0.273216	,	0.27968	,	0.263808	,	0.26352	,	0.281568	,	0.281568	,	0.27248	,	0.273024	,	0.27296	,	0.274336	,	0.273056	,	0.263232	,	0.272512	,	0.263168	,	0.273248	,	0.28064	,	0.283168	,	0.282272	,	0.284288	,	0.272384	,	0.272832	,	0.274208	,	0.284448	,	0.275264
Index	,	0	,	1	,	2	,	3	,	4	,	5	,	6	,	7	,	8	,	9	,	10	,	11	,	12	,	13	,	14	,	15	,	16	,	17	,	18	,	19	,	20	,	21	,	22	,	23	,	24	,	25

ProfilingBuildAggregator: 
BuildBottomLevelAS	,	4.2512
BuildBottomLevelASGpu	,	16.3363
BuildTopLevelAS	,	2.5432
BuildTopLevelASGpu	,	0.619136
Duplication	,	1
Triangles	,	262267
Meshes	,	103
BottomLevels	,	1
Index	,	0

FpsAggregator: 
FPS	,	91	,	103	,	103	,	102	,	104	,	107	,	111	,	113	,	117	,	116	,	113	,	109	,	102	,	104	,	108	,	110	,	108	,	108	,	107	,	108	,	83	,	86	,	93	,	98	,	107	,	107
UPS	,	59	,	61	,	60	,	60	,	61	,	60	,	61	,	60	,	60	,	60	,	60	,	60	,	60	,	61	,	60	,	61	,	60	,	60	,	60	,	60	,	60	,	61	,	60	,	60	,	60	,	61
FrameTime	,	11.0486	,	9.76272	,	9.7962	,	9.84743	,	9.69707	,	9.42377	,	9.03068	,	8.86397	,	8.59748	,	8.62843	,	8.85365	,	9.22158	,	9.80639	,	9.65845	,	9.34259	,	9.167	,	9.29023	,	9.27665	,	9.36461	,	9.28631	,	12.0701	,	11.6645	,	10.762	,	10.2467	,	9.42263	,	9.42368
GigaRays/s	,	2.96327	,	3.35356	,	3.3421	,	3.32471	,	3.37626	,	3.47418	,	3.6254	,	3.69358	,	3.80808	,	3.79441	,	3.69789	,	3.55035	,	3.33862	,	3.38976	,	3.50436	,	3.57149	,	3.52412	,	3.52927	,	3.49612	,	3.5256	,	2.71248	,	2.8068	,	3.04217	,	3.19516	,	3.4746	,	3.47421
Index	,	0	,	1	,	2	,	3	,	4	,	5	,	6	,	7	,	8	,	9	,	10	,	11	,	12	,	13	,	14	,	15	,	16	,	17	,	18	,	19	,	20	,	21	,	22	,	23	,	24	,	25


AccelerationStructure: 
Ray tracing acceleration structure building statistics: 
	Bottom Level Acceleration Structure: 
		Maximum [B]: 14623872
		Minimum [B]: 14623872
		Currently Allocated [B]: 14623872
		Currently Created [num]: 1
		Current Average [B]: 14623872
		Maximum Triangles [num]: 262267
		Minimum Triangles [num]: 262267
		Total Triangles [num]: 262267
		Maximum Meshes [num]: 103
		Minimum Meshes [num]: 103
		Total Meshes [num]: 103
	Top Level Acceleration Structure: 
		Maximum [B]: 64
		Minimum [B]: 64
		Currently Allocated [B]: 64
		Total Allocated [B]: 64
		Total Created [num]: 1
		Average Allocated [B]: 64
		Maximum Instances [num]: 1
		Minimum Instances [num]: 1
		Total Instances [num]: 1
	Scratch Buffer: 
		Maximum [B]: 11660800
		Minimum [B]: 11660800
		Currently Allocated [B]: 11660800
		Total Allocated [B]: 11660800
		Total Created [num]: 1
		Average Allocated [B]: 11660800
ProfilingStack: 
[PROF] Profiling statistics: 
	Scopes entered : 27483
	Scopes exited : 27482
	Overhead per scope [ticks] : 1015
	Threads entered : 3
	Threads exited : 0
	============================
		         Sum[Mt]   Parent[%]  Samples[num]       Avg[Mt]   Last/Curr[Mt]        Self[Mt]     Self[%] 	 Name

	main:

		   35622.231 Mt   100.000 %         1 x    35622.234 Mt    35622.229 Mt        0.689 Mt     0.002 % 	 /

		   35621.659 Mt    99.998 %         1 x    35621.664 Mt    35621.658 Mt      342.719 Mt     0.962 % 	 ./Main application loop

		       2.861 Mt     0.008 %         1 x        2.861 Mt        0.000 Mt        2.861 Mt   100.000 % 	 ../TexturedRLInitialization

		       6.426 Mt     0.018 %         1 x        6.426 Mt        0.000 Mt        6.426 Mt   100.000 % 	 ../DeferredRLInitialization

		      48.671 Mt     0.137 %         1 x       48.671 Mt        0.000 Mt        2.791 Mt     5.735 % 	 ../RayTracingRLInitialization

		      45.880 Mt    94.265 %         1 x       45.880 Mt        0.000 Mt       45.880 Mt   100.000 % 	 .../PipelineBuild

		       3.239 Mt     0.009 %         1 x        3.239 Mt        0.000 Mt        3.239 Mt   100.000 % 	 ../ResolveRLInitialization

		    9094.410 Mt    25.531 %         1 x     9094.410 Mt        0.000 Mt        8.238 Mt     0.091 % 	 ../Initialization

		    9086.171 Mt    99.909 %         1 x     9086.171 Mt        0.000 Mt        3.225 Mt     0.035 % 	 .../ScenePrep

		    8969.845 Mt    98.720 %         1 x     8969.845 Mt        0.000 Mt      353.625 Mt     3.942 % 	 ..../SceneLoad

		    8112.987 Mt    90.447 %         1 x     8112.987 Mt        0.000 Mt     1102.257 Mt    13.586 % 	 ...../glTF-LoadScene

		    7010.730 Mt    86.414 %        69 x      101.605 Mt        0.000 Mt     7010.730 Mt   100.000 % 	 ....../glTF-LoadImageData

		     304.440 Mt     3.394 %         1 x      304.440 Mt        0.000 Mt      304.440 Mt   100.000 % 	 ...../glTF-CreateScene

		     198.793 Mt     2.216 %         1 x      198.793 Mt        0.000 Mt      198.793 Mt   100.000 % 	 ...../SceneDesc-CreateScene

		       6.854 Mt     0.075 %         1 x        6.854 Mt        0.000 Mt        0.006 Mt     0.082 % 	 ..../TexturedRLPrep

		       6.849 Mt    99.918 %         1 x        6.849 Mt        0.000 Mt        1.874 Mt    27.366 % 	 ...../PrepareForRendering

		       2.621 Mt    38.265 %         1 x        2.621 Mt        0.000 Mt        2.621 Mt   100.000 % 	 ....../PrepareMaterials

		       2.354 Mt    34.369 %         1 x        2.354 Mt        0.000 Mt        2.354 Mt   100.000 % 	 ....../PrepareDrawBundle

		       8.751 Mt     0.096 %         1 x        8.751 Mt        0.000 Mt        0.005 Mt     0.058 % 	 ..../DeferredRLPrep

		       8.745 Mt    99.942 %         1 x        8.745 Mt        0.000 Mt        4.081 Mt    46.667 % 	 ...../PrepareForRendering

		       0.021 Mt     0.238 %         1 x        0.021 Mt        0.000 Mt        0.021 Mt   100.000 % 	 ....../PrepareMaterials

		       2.187 Mt    25.004 %         1 x        2.187 Mt        0.000 Mt        2.187 Mt   100.000 % 	 ....../BuildMaterialCache

		       2.457 Mt    28.091 %         1 x        2.457 Mt        0.000 Mt        2.457 Mt   100.000 % 	 ....../PrepareDrawBundle

		      29.696 Mt     0.327 %         1 x       29.696 Mt        0.000 Mt        6.502 Mt    21.895 % 	 ..../RayTracingRLPrep

		       0.573 Mt     1.930 %         1 x        0.573 Mt        0.000 Mt        0.573 Mt   100.000 % 	 ...../PrepareAccelerationStructures

		       0.024 Mt     0.080 %         1 x        0.024 Mt        0.000 Mt        0.024 Mt   100.000 % 	 ...../PrepareCache

		       5.280 Mt    17.781 %         1 x        5.280 Mt        0.000 Mt        5.280 Mt   100.000 % 	 ...../BuildCache

		      12.921 Mt    43.511 %         1 x       12.921 Mt        0.000 Mt        0.004 Mt     0.031 % 	 ...../BuildAccelerationStructures

		      12.917 Mt    99.969 %         1 x       12.917 Mt        0.000 Mt        6.123 Mt    47.399 % 	 ....../AccelerationStructureBuild

		       4.251 Mt    32.912 %         1 x        4.251 Mt        0.000 Mt        4.251 Mt   100.000 % 	 ......./BuildBottomLevelAS

		       2.543 Mt    19.689 %         1 x        2.543 Mt        0.000 Mt        2.543 Mt   100.000 % 	 ......./BuildTopLevelAS

		       4.396 Mt    14.803 %         1 x        4.396 Mt        0.000 Mt        4.396 Mt   100.000 % 	 ...../BuildShaderTables

		      67.800 Mt     0.746 %         1 x       67.800 Mt        0.000 Mt       67.800 Mt   100.000 % 	 ..../GpuSidePrep

		   26123.333 Mt    73.336 %      6770 x        3.859 Mt       13.235 Mt      175.638 Mt     0.672 % 	 ../MainLoop

		     850.535 Mt     3.256 %      1568 x        0.542 Mt        0.468 Mt      849.536 Mt    99.883 % 	 .../Update

		       0.999 Mt     0.117 %         1 x        0.999 Mt        0.000 Mt        0.999 Mt   100.000 % 	 ..../GuiModelApply

		   25097.160 Mt    96.072 %      2720 x        9.227 Mt       12.129 Mt    21364.278 Mt    85.126 % 	 .../Render

		    3732.882 Mt    14.874 %      2720 x        1.372 Mt        3.289 Mt     3720.083 Mt    99.657 % 	 ..../RayTracing

		      12.799 Mt     0.343 %      2720 x        0.005 Mt        0.006 Mt       12.799 Mt   100.000 % 	 ...../RayTracingRLPrep

	WindowThread:

		   35620.265 Mt   100.000 %         1 x    35620.266 Mt    35620.264 Mt    35620.265 Mt   100.000 % 	 /

	GpuThread:

		   18010.541 Mt   100.000 %      2720 x        6.622 Mt        6.231 Mt      172.123 Mt     0.956 % 	 /

		      17.009 Mt     0.094 %         1 x       17.009 Mt        0.000 Mt        0.050 Mt     0.297 % 	 ./ScenePrep

		      16.958 Mt    99.703 %         1 x       16.958 Mt        0.000 Mt        0.003 Mt     0.015 % 	 ../AccelerationStructureBuild

		      16.336 Mt    96.334 %         1 x       16.336 Mt        0.000 Mt       16.336 Mt   100.000 % 	 .../BuildBottomLevelASGpu

		       0.619 Mt     3.651 %         1 x        0.619 Mt        0.000 Mt        0.619 Mt   100.000 % 	 .../BuildTopLevelASGpu

		   17814.727 Mt    98.913 %      2720 x        6.550 Mt        6.229 Mt        2.417 Mt     0.014 % 	 ./RayTracingGpu

		   17059.553 Mt    95.761 %      2720 x        6.272 Mt        5.963 Mt    17059.553 Mt   100.000 % 	 ../RayTracingRLGpu

		     752.757 Mt     4.225 %      2720 x        0.277 Mt        0.265 Mt      752.757 Mt   100.000 % 	 ../ResolveRLGpu

		       6.682 Mt     0.037 %      2720 x        0.002 Mt        0.002 Mt        6.682 Mt   100.000 % 	 ./Present


	============================


