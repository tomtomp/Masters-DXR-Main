Configuration: 
Command line parameters  = E:\programming\FIT-DIP\Masters-DXR-Main-git\build\Measurement\x64\Debug\Measurement.exe --scene Cube/Cube.gltf --profile-automation --profile-camera-track Cube/Cube.trk --rt-mode --width 1024 --height 768 --profile-output TrackCube768Rt 
Window width             = 1024
Window height            = 768
Window title             = Direct3D App
Back-buffer count        = 3
Fill Triangles           = enabled
Triangles Font Clock-Wise= enabled
Fast Build AS            = disabled
Target FPS               = 0
Target UPS               = 60
Tearing                  = enabled
VSync                    = disabled
GUI rendering            = disabled
Debug layer              = enabled
GPU validation           = disabled
GPU profiling            = enabled
Render mode              = Ray Tracing
Loaded scene file        = Cube/Cube.gltf
Using testing scene      = disabled
Duplication              = 1
Duplication in cube      = disabled
Duplication offset       = 10
BLAS duplication         = disabled
Rays per pixel           = 10

ProfilingAggregator: 
Render	,	3.9116	,	2.7844	,	4.8857	,	5.0661	,	4.7574	,	6.1945	,	4.0543	,	5.9843	,	6.126	,	9.5343	,	4.3827	,	5.9719	,	3.6212	,	3.7069	,	4.039	,	4.0554	,	4.4605	,	4.5643	,	6.6305	,	4.1797	,	5.5624	,	5.0212	,	5.4074	,	5.8355	,	3.8743	,	3.4578	,	4.9243	,	3.6819	,	2.8488
Update	,	0.4613	,	0.3014	,	0.4824	,	0.5201	,	0.4773	,	0.3331	,	0.8608	,	0.8469	,	0.4808	,	0.6759	,	0.3732	,	0.6383	,	0.5488	,	0.6966	,	0.2824	,	0.3455	,	0.3897	,	0.3883	,	0.7152	,	0.3745	,	0.6503	,	0.7267	,	0.5402	,	0.7415	,	0.3318	,	0.867	,	0.5269	,	0.8627	,	0.3177
RayTracing	,	2.04	,	1.2406	,	2.3178	,	2.3094	,	1.7843	,	2.645	,	1.2463	,	2.7394	,	2.4843	,	4.6395	,	1.5579	,	2.809	,	1.2769	,	1.2998	,	1.2334	,	1.2579	,	1.5337	,	1.431	,	2.6911	,	1.2893	,	2.5016	,	2.3319	,	2.5117	,	2.5556	,	1.2348	,	1.2372	,	2.1718	,	1.6098	,	1.2546
RayTracingGpu	,	0.487552	,	0.628256	,	1.03517	,	1.27782	,	1.67082	,	1.86154	,	1.72518	,	1.84918	,	1.88634	,	1.84202	,	1.76726	,	1.75206	,	1.41738	,	1.28301	,	1.66829	,	1.88496	,	1.93546	,	1.90922	,	1.91139	,	1.91939	,	1.61974	,	1.15613	,	1.21642	,	1.16406	,	1.33341	,	0.933664	,	0.797728	,	0.733312	,	0.654752
DeferredRLGpu	,	0.062912	,	0.084	,	0.137984	,	0.159936	,	0.182912	,	0.2024	,	0.1872	,	0.201472	,	0.20448	,	0.196448	,	0.182016	,	0.180928	,	0.145312	,	0.130432	,	0.173152	,	0.194176	,	0.2008	,	0.196704	,	0.19616	,	0.197984	,	0.16048	,	0.118912	,	0.131456	,	0.124384	,	0.117792	,	0.101728	,	0.090048	,	0.084608	,	0.072736
RayTracingRLGpu	,	0.345952	,	0.453376	,	0.766336	,	0.98752	,	1.36253	,	1.52317	,	1.4073	,	1.51232	,	1.54339	,	1.50467	,	1.45235	,	1.4369	,	1.15078	,	1.03632	,	1.36774	,	1.55344	,	1.59421	,	1.57238	,	1.57459	,	1.58019	,	1.32909	,	0.926656	,	0.977728	,	0.937024	,	0.850816	,	0.739008	,	0.61424	,	0.54528	,	0.481056
ResolveRLGpu	,	0.077216	,	0.089664	,	0.128928	,	0.128448	,	0.1232	,	0.133792	,	0.128768	,	0.13376	,	0.136544	,	0.139296	,	0.130688	,	0.13232	,	0.11904	,	0.11408	,	0.124896	,	0.135168	,	0.138848	,	0.138496	,	0.138752	,	0.139584	,	0.127712	,	0.108128	,	0.10448	,	0.100544	,	0.362368	,	0.091072	,	0.09184	,	0.101504	,	0.097952
Index	,	0	,	1	,	2	,	3	,	4	,	5	,	6	,	7	,	8	,	9	,	10	,	11	,	12	,	13	,	14	,	15	,	16	,	17	,	18	,	19	,	20	,	21	,	22	,	23	,	24	,	25	,	26	,	27	,	28

ProfilingBuildAggregator: 
BuildBottomLevelAS	,	1.0531
BuildBottomLevelASGpu	,	0.119456
BuildTopLevelAS	,	0.9811
BuildTopLevelASGpu	,	0.082496
Duplication	,	1
Triangles	,	12
Meshes	,	1
BottomLevels	,	1
Index	,	0

FpsAggregator: 
FPS	,	299	,	285	,	254	,	218	,	189	,	178	,	182	,	190	,	177	,	180	,	192	,	185	,	203	,	223	,	202	,	185	,	179	,	177	,	171	,	174	,	181	,	210	,	225	,	218	,	232	,	237	,	245	,	268	,	285
UPS	,	59	,	61	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	61	,	60	,	60	,	60	,	60	,	61	,	60	,	60	,	60	,	60	,	60	,	60	,	61	,	60	,	60	,	60	,	60	,	60	,	61
FrameTime	,	3.35217	,	3.5143	,	3.94372	,	4.61251	,	5.2927	,	5.63095	,	5.50159	,	5.26679	,	5.67624	,	5.5677	,	5.2273	,	5.42369	,	4.93241	,	4.49374	,	4.95343	,	5.4285	,	5.59416	,	5.67263	,	5.87659	,	5.74759	,	5.55117	,	4.76955	,	4.45946	,	4.60814	,	4.31124	,	4.22943	,	4.08588	,	3.73881	,	3.51553
GigaRays/s	,	2.34604	,	2.23781	,	1.99414	,	1.705	,	1.48588	,	1.39662	,	1.42946	,	1.49319	,	1.38548	,	1.41249	,	1.50447	,	1.45	,	1.59442	,	1.75006	,	1.58765	,	1.44871	,	1.40581	,	1.38636	,	1.33825	,	1.36828	,	1.4167	,	1.64886	,	1.76351	,	1.70661	,	1.82414	,	1.85943	,	1.92475	,	2.10343	,	2.23702
Index	,	0	,	1	,	2	,	3	,	4	,	5	,	6	,	7	,	8	,	9	,	10	,	11	,	12	,	13	,	14	,	15	,	16	,	17	,	18	,	19	,	20	,	21	,	22	,	23	,	24	,	25	,	26	,	27	,	28


AccelerationStructure: 
Ray tracing acceleration structure building statistics: 
	Bottom Level Acceleration Structure: 
		Maximum [B]: 5168
		Minimum [B]: 5168
		Currently Allocated [B]: 5168
		Currently Created [num]: 1
		Current Average [B]: 5168
		Maximum Triangles [num]: 12
		Minimum Triangles [num]: 12
		Total Triangles [num]: 12
		Maximum Meshes [num]: 1
		Minimum Meshes [num]: 1
		Total Meshes [num]: 1
	Top Level Acceleration Structure: 
		Maximum [B]: 64
		Minimum [B]: 64
		Currently Allocated [B]: 64
		Total Allocated [B]: 64
		Total Created [num]: 1
		Average Allocated [B]: 64
		Maximum Instances [num]: 1
		Minimum Instances [num]: 1
		Total Instances [num]: 1
	Scratch Buffer: 
		Maximum [B]: 3584
		Minimum [B]: 3584
		Currently Allocated [B]: 3584
		Total Allocated [B]: 3584
		Total Created [num]: 1
		Average Allocated [B]: 3584
ProfilingStack: 
[PROF] Profiling statistics: 
	Scopes entered : 65109
	Scopes exited : 65108
	Overhead per scope [ticks] : 1018.2
	Threads entered : 3
	Threads exited : 0
	============================
		         Sum[Mt]   Parent[%]  Samples[num]       Avg[Mt]   Last/Curr[Mt]        Self[Mt]     Self[%] 	 Name

	main:

		   29656.345 Mt   100.000 %         1 x    29656.349 Mt    29656.344 Mt        0.732 Mt     0.002 % 	 /

		   29655.739 Mt    99.998 %         1 x    29655.744 Mt    29655.738 Mt      370.915 Mt     1.251 % 	 ./Main application loop

		       2.897 Mt     0.010 %         1 x        2.897 Mt        0.000 Mt        2.897 Mt   100.000 % 	 ../TexturedRLInitialization

		       5.997 Mt     0.020 %         1 x        5.997 Mt        0.000 Mt        5.997 Mt   100.000 % 	 ../DeferredRLInitialization

		      62.445 Mt     0.211 %         1 x       62.445 Mt        0.000 Mt        2.365 Mt     3.787 % 	 ../RayTracingRLInitialization

		      60.080 Mt    96.213 %         1 x       60.080 Mt        0.000 Mt       60.080 Mt   100.000 % 	 .../PipelineBuild

		       3.316 Mt     0.011 %         1 x        3.316 Mt        0.000 Mt        3.316 Mt   100.000 % 	 ../ResolveRLInitialization

		     139.284 Mt     0.470 %         1 x      139.284 Mt        0.000 Mt        3.202 Mt     2.299 % 	 ../Initialization

		     136.082 Mt    97.701 %         1 x      136.082 Mt        0.000 Mt        1.757 Mt     1.291 % 	 .../ScenePrep

		      98.603 Mt    72.459 %         1 x       98.603 Mt        0.000 Mt       13.620 Mt    13.813 % 	 ..../SceneLoad

		      58.394 Mt    59.221 %         1 x       58.394 Mt        0.000 Mt       13.346 Mt    22.856 % 	 ...../glTF-LoadScene

		      45.047 Mt    77.144 %         2 x       22.524 Mt        0.000 Mt       45.047 Mt   100.000 % 	 ....../glTF-LoadImageData

		      10.803 Mt    10.955 %         1 x       10.803 Mt        0.000 Mt       10.803 Mt   100.000 % 	 ...../glTF-CreateScene

		      15.787 Mt    16.011 %         1 x       15.787 Mt        0.000 Mt       15.787 Mt   100.000 % 	 ...../SceneDesc-CreateScene

		       4.517 Mt     3.319 %         1 x        4.517 Mt        0.000 Mt        0.005 Mt     0.120 % 	 ..../TexturedRLPrep

		       4.512 Mt    99.880 %         1 x        4.512 Mt        0.000 Mt        0.935 Mt    20.724 % 	 ...../PrepareForRendering

		       3.282 Mt    72.746 %         1 x        3.282 Mt        0.000 Mt        3.282 Mt   100.000 % 	 ....../PrepareMaterials

		       0.295 Mt     6.530 %         1 x        0.295 Mt        0.000 Mt        0.295 Mt   100.000 % 	 ....../PrepareDrawBundle

		       3.375 Mt     2.480 %         1 x        3.375 Mt        0.000 Mt        0.004 Mt     0.127 % 	 ..../DeferredRLPrep

		       3.371 Mt    99.873 %         1 x        3.371 Mt        0.000 Mt        2.223 Mt    65.928 % 	 ...../PrepareForRendering

		       0.016 Mt     0.486 %         1 x        0.016 Mt        0.000 Mt        0.016 Mt   100.000 % 	 ....../PrepareMaterials

		       0.862 Mt    25.555 %         1 x        0.862 Mt        0.000 Mt        0.862 Mt   100.000 % 	 ....../BuildMaterialCache

		       0.271 Mt     8.030 %         1 x        0.271 Mt        0.000 Mt        0.271 Mt   100.000 % 	 ....../PrepareDrawBundle

		      16.619 Mt    12.212 %         1 x       16.619 Mt        0.000 Mt        4.589 Mt    27.611 % 	 ..../RayTracingRLPrep

		       0.248 Mt     1.493 %         1 x        0.248 Mt        0.000 Mt        0.248 Mt   100.000 % 	 ...../PrepareAccelerationStructures

		       0.024 Mt     0.145 %         1 x        0.024 Mt        0.000 Mt        0.024 Mt   100.000 % 	 ...../PrepareCache

		       0.989 Mt     5.949 %         1 x        0.989 Mt        0.000 Mt        0.989 Mt   100.000 % 	 ...../BuildCache

		       8.384 Mt    50.446 %         1 x        8.384 Mt        0.000 Mt        0.004 Mt     0.048 % 	 ...../BuildAccelerationStructures

		       8.380 Mt    99.952 %         1 x        8.380 Mt        0.000 Mt        6.345 Mt    75.724 % 	 ....../AccelerationStructureBuild

		       1.053 Mt    12.567 %         1 x        1.053 Mt        0.000 Mt        1.053 Mt   100.000 % 	 ......./BuildBottomLevelAS

		       0.981 Mt    11.708 %         1 x        0.981 Mt        0.000 Mt        0.981 Mt   100.000 % 	 ......./BuildTopLevelAS

		       2.386 Mt    14.355 %         1 x        2.386 Mt        0.000 Mt        2.386 Mt   100.000 % 	 ...../BuildShaderTables

		      11.211 Mt     8.238 %         1 x       11.211 Mt        0.000 Mt       11.211 Mt   100.000 % 	 ..../GpuSidePrep

		   29070.884 Mt    98.028 %      8011 x        3.629 Mt       11.269 Mt     1007.887 Mt     3.467 % 	 ../MainLoop

		    1017.172 Mt     3.499 %      1746 x        0.583 Mt        0.792 Mt     1016.285 Mt    99.913 % 	 .../Update

		       0.887 Mt     0.087 %         1 x        0.887 Mt        0.000 Mt        0.887 Mt   100.000 % 	 ..../GuiModelApply

		   27045.825 Mt    93.034 %      6146 x        4.401 Mt        9.544 Mt    16510.893 Mt    61.048 % 	 .../Render

		   10534.932 Mt    38.952 %      6146 x        1.714 Mt        5.316 Mt    10480.738 Mt    99.486 % 	 ..../RayTracing

		      32.136 Mt     0.305 %      6146 x        0.005 Mt        0.009 Mt       32.136 Mt   100.000 % 	 ...../DeferredRLPrep

		      22.057 Mt     0.209 %      6146 x        0.004 Mt        0.009 Mt       22.057 Mt   100.000 % 	 ...../RayTracingRLPrep

	WindowThread:

		   29654.500 Mt   100.000 %         1 x    29654.502 Mt    29654.500 Mt    29654.500 Mt   100.000 % 	 /

	GpuThread:

		    8499.940 Mt   100.000 %      6146 x        1.383 Mt        0.655 Mt      130.109 Mt     1.531 % 	 /

		       0.209 Mt     0.002 %         1 x        0.209 Mt        0.000 Mt        0.006 Mt     3.103 % 	 ./ScenePrep

		       0.203 Mt    96.897 %         1 x        0.203 Mt        0.000 Mt        0.001 Mt     0.442 % 	 ../AccelerationStructureBuild

		       0.119 Mt    58.889 %         1 x        0.119 Mt        0.000 Mt        0.119 Mt   100.000 % 	 .../BuildBottomLevelASGpu

		       0.082 Mt    40.669 %         1 x        0.082 Mt        0.000 Mt        0.082 Mt   100.000 % 	 .../BuildTopLevelASGpu

		    8342.469 Mt    98.147 %      6146 x        1.357 Mt        0.651 Mt       13.614 Mt     0.163 % 	 ./RayTracingGpu

		     885.939 Mt    10.620 %      6146 x        0.144 Mt        0.072 Mt      885.939 Mt   100.000 % 	 ../DeferredRLGpu

		    6718.286 Mt    80.531 %      6146 x        1.093 Mt        0.476 Mt     6718.286 Mt   100.000 % 	 ../RayTracingRLGpu

		     724.629 Mt     8.686 %      6146 x        0.118 Mt        0.099 Mt      724.629 Mt   100.000 % 	 ../ResolveRLGpu

		      27.153 Mt     0.319 %      6146 x        0.004 Mt        0.003 Mt       27.153 Mt   100.000 % 	 ./Present


	============================


