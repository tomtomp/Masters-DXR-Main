Configuration: 
Command line parameters  = E:\programming\FIT-DIP\Masters-DXR-Main-git\build\Measurement\x64\Debug\Measurement.exe --scene Sponza/Sponza.gltf --profile-automation --profile-camera-track Sponza/Sponza.trk --width 2560 --height 1440 --profile-output TrackSponza1440Ra 
Window width             = 2560
Window height            = 1421
Window title             = Direct3D App
Back-buffer count        = 3
Fill Triangles           = enabled
Triangles Font Clock-Wise= enabled
Fast Build AS            = disabled
Target FPS               = 0
Target UPS               = 60
Tearing                  = enabled
VSync                    = disabled
GUI rendering            = disabled
Debug layer              = enabled
GPU validation           = disabled
GPU profiling            = enabled
Render mode              = Rasterization
Loaded scene file        = Sponza/Sponza.gltf
Using testing scene      = disabled
Duplication              = 1
Duplication in cube      = disabled
Duplication offset       = 10
BLAS duplication         = disabled
Rays per pixel           = 10

ProfilingAggregator: 
Render	,	1.8178	,	2.5519	,	2.0451	,	2.0996	,	2.0462	,	2.1913	,	2.9572	,	2.1979	,	2.9201	,	2.8392	,	2.5072	,	2.6159	,	2.366	,	2.3958	,	2.1892	,	2.1325	,	2.271	,	1.8374	,	2.0526	,	2.0709	,	2.6569	,	2.5615	,	2.9208	,	2.9366	,	2.5583	,	2.6842
Update	,	0.2765	,	0.2793	,	0.2869	,	0.2837	,	0.4715	,	0.2924	,	0.5157	,	0.2958	,	0.4702	,	0.8705	,	0.2871	,	0.2707	,	0.3964	,	0.3176	,	0.4025	,	0.2788	,	0.5012	,	0.2811	,	0.8656	,	0.4683	,	0.764	,	0.3954	,	0.817	,	0.3179	,	0.4413	,	0.5658
Textured	,	1.0692	,	1.4236	,	1.0754	,	1.1256	,	1.0705	,	1.0713	,	1.5054	,	1.0838	,	1.5522	,	1.4267	,	1.1227	,	1.3561	,	1.1342	,	1.1953	,	1.0619	,	1.0742	,	1.1419	,	1.0788	,	1.0693	,	1.0771	,	1.2865	,	1.3091	,	1.4167	,	1.6678	,	1.3355	,	1.4614
TexturedGpu	,	0.264736	,	0.37232	,	0.370272	,	0.388832	,	0.392864	,	0.39824	,	0.364672	,	0.220384	,	0.263392	,	0.330496	,	0.482624	,	0.485088	,	0.463968	,	0.434304	,	0.433856	,	0.39776	,	0.374592	,	0.274176	,	0.335296	,	0.402496	,	0.574208	,	0.6464	,	0.624448	,	0.507296	,	0.462688	,	0.419424
TexturedRLGpu	,	0.262688	,	0.36928	,	0.36864	,	0.386336	,	0.389248	,	0.39744	,	0.362208	,	0.21776	,	0.26256	,	0.3272	,	0.479424	,	0.482592	,	0.463136	,	0.429344	,	0.432192	,	0.395264	,	0.372096	,	0.27168	,	0.33424	,	0.401376	,	0.573088	,	0.643904	,	0.619488	,	0.5048	,	0.459936	,	0.41776
Index	,	0	,	1	,	2	,	3	,	4	,	5	,	6	,	7	,	8	,	9	,	10	,	11	,	12	,	13	,	14	,	15	,	16	,	17	,	18	,	19	,	20	,	21	,	22	,	23	,	24	,	25

ProfilingBuildAggregator: 
BuildBottomLevelAS	,	4.5923
BuildBottomLevelASGpu	,	15.5261
BuildTopLevelAS	,	2.8864
BuildTopLevelASGpu	,	0.609344
Duplication	,	1
Triangles	,	262267
Meshes	,	103
BottomLevels	,	1
Index	,	0

FpsAggregator: 
FPS	,	425	,	451	,	415	,	412	,	406	,	401	,	408	,	433	,	452	,	435	,	401	,	388	,	394	,	390	,	403	,	400	,	405	,	422	,	424	,	406	,	385	,	366	,	370	,	370	,	387	,	394
UPS	,	59	,	60	,	61	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	61	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60
FrameTime	,	2.35508	,	2.2185	,	2.41256	,	2.43139	,	2.46467	,	2.49505	,	2.45737	,	2.31032	,	2.21435	,	2.3008	,	2.49663	,	2.58198	,	2.54051	,	2.56565	,	2.48586	,	2.50148	,	2.46974	,	2.37416	,	2.36016	,	2.46806	,	2.60315	,	2.73657	,	2.70576	,	2.70409	,	2.58463	,	2.54492
GigaRays/s	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0
Index	,	0	,	1	,	2	,	3	,	4	,	5	,	6	,	7	,	8	,	9	,	10	,	11	,	12	,	13	,	14	,	15	,	16	,	17	,	18	,	19	,	20	,	21	,	22	,	23	,	24	,	25


AccelerationStructure: 
Ray tracing acceleration structure building statistics: 
	Bottom Level Acceleration Structure: 
		Maximum [B]: 14623872
		Minimum [B]: 14623872
		Currently Allocated [B]: 14623872
		Currently Created [num]: 1
		Current Average [B]: 14623872
		Maximum Triangles [num]: 262267
		Minimum Triangles [num]: 262267
		Total Triangles [num]: 262267
		Maximum Meshes [num]: 103
		Minimum Meshes [num]: 103
		Total Meshes [num]: 103
	Top Level Acceleration Structure: 
		Maximum [B]: 64
		Minimum [B]: 64
		Currently Allocated [B]: 64
		Total Allocated [B]: 64
		Total Created [num]: 1
		Average Allocated [B]: 64
		Maximum Instances [num]: 1
		Minimum Instances [num]: 1
		Total Instances [num]: 1
	Scratch Buffer: 
		Maximum [B]: 11660800
		Minimum [B]: 11660800
		Currently Allocated [B]: 11660800
		Total Allocated [B]: 11660800
		Total Created [num]: 1
		Average Allocated [B]: 11660800
ProfilingStack: 
[PROF] Profiling statistics: 
	Scopes entered : 79066
	Scopes exited : 79065
	Overhead per scope [ticks] : 1016.1
	Threads entered : 3
	Threads exited : 0
	============================
		         Sum[Mt]   Parent[%]  Samples[num]       Avg[Mt]   Last/Curr[Mt]        Self[Mt]     Self[%] 	 Name

	main:

		   35589.300 Mt   100.000 %         1 x    35589.301 Mt    35589.300 Mt        0.734 Mt     0.002 % 	 /

		   35588.611 Mt    99.998 %         1 x    35588.613 Mt    35588.611 Mt      393.900 Mt     1.107 % 	 ./Main application loop

		       2.859 Mt     0.008 %         1 x        2.859 Mt        0.000 Mt        2.859 Mt   100.000 % 	 ../TexturedRLInitialization

		       6.577 Mt     0.018 %         1 x        6.577 Mt        0.000 Mt        6.577 Mt   100.000 % 	 ../DeferredRLInitialization

		      52.195 Mt     0.147 %         1 x       52.195 Mt        0.000 Mt        2.322 Mt     4.450 % 	 ../RayTracingRLInitialization

		      49.872 Mt    95.550 %         1 x       49.872 Mt        0.000 Mt       49.872 Mt   100.000 % 	 .../PipelineBuild

		       3.343 Mt     0.009 %         1 x        3.343 Mt        0.000 Mt        3.343 Mt   100.000 % 	 ../ResolveRLInitialization

		    9165.680 Mt    25.755 %         1 x     9165.680 Mt        0.000 Mt        8.234 Mt     0.090 % 	 ../Initialization

		    9157.445 Mt    99.910 %         1 x     9157.445 Mt        0.000 Mt        3.554 Mt     0.039 % 	 .../ScenePrep

		    9043.975 Mt    98.761 %         1 x     9043.975 Mt        0.000 Mt      341.572 Mt     3.777 % 	 ..../SceneLoad

		    8157.604 Mt    90.199 %         1 x     8157.604 Mt        0.000 Mt     1107.593 Mt    13.577 % 	 ...../glTF-LoadScene

		    7050.012 Mt    86.423 %        69 x      102.174 Mt        0.000 Mt     7050.012 Mt   100.000 % 	 ....../glTF-LoadImageData

		     376.607 Mt     4.164 %         1 x      376.607 Mt        0.000 Mt      376.607 Mt   100.000 % 	 ...../glTF-CreateScene

		     168.192 Mt     1.860 %         1 x      168.192 Mt        0.000 Mt      168.192 Mt   100.000 % 	 ...../SceneDesc-CreateScene

		       7.096 Mt     0.077 %         1 x        7.096 Mt        0.000 Mt        0.005 Mt     0.075 % 	 ..../TexturedRLPrep

		       7.091 Mt    99.925 %         1 x        7.091 Mt        0.000 Mt        2.089 Mt    29.457 % 	 ...../PrepareForRendering

		       2.730 Mt    38.499 %         1 x        2.730 Mt        0.000 Mt        2.730 Mt   100.000 % 	 ....../PrepareMaterials

		       2.272 Mt    32.044 %         1 x        2.272 Mt        0.000 Mt        2.272 Mt   100.000 % 	 ....../PrepareDrawBundle

		       8.643 Mt     0.094 %         1 x        8.643 Mt        0.000 Mt        0.005 Mt     0.056 % 	 ..../DeferredRLPrep

		       8.638 Mt    99.944 %         1 x        8.638 Mt        0.000 Mt        3.838 Mt    44.434 % 	 ...../PrepareForRendering

		       0.021 Mt     0.242 %         1 x        0.021 Mt        0.000 Mt        0.021 Mt   100.000 % 	 ....../PrepareMaterials

		       2.280 Mt    26.394 %         1 x        2.280 Mt        0.000 Mt        2.280 Mt   100.000 % 	 ....../BuildMaterialCache

		       2.499 Mt    28.930 %         1 x        2.499 Mt        0.000 Mt        2.499 Mt   100.000 % 	 ....../PrepareDrawBundle

		      29.683 Mt     0.324 %         1 x       29.683 Mt        0.000 Mt        6.605 Mt    22.251 % 	 ..../RayTracingRLPrep

		       0.593 Mt     1.998 %         1 x        0.593 Mt        0.000 Mt        0.593 Mt   100.000 % 	 ...../PrepareAccelerationStructures

		       0.024 Mt     0.081 %         1 x        0.024 Mt        0.000 Mt        0.024 Mt   100.000 % 	 ...../PrepareCache

		       5.279 Mt    17.784 %         1 x        5.279 Mt        0.000 Mt        5.279 Mt   100.000 % 	 ...../BuildCache

		      12.846 Mt    43.277 %         1 x       12.846 Mt        0.000 Mt        0.004 Mt     0.030 % 	 ...../BuildAccelerationStructures

		      12.842 Mt    99.970 %         1 x       12.842 Mt        0.000 Mt        5.364 Mt    41.765 % 	 ....../AccelerationStructureBuild

		       4.592 Mt    35.759 %         1 x        4.592 Mt        0.000 Mt        4.592 Mt   100.000 % 	 ......./BuildBottomLevelAS

		       2.886 Mt    22.476 %         1 x        2.886 Mt        0.000 Mt        2.886 Mt   100.000 % 	 ......./BuildTopLevelAS

		       4.337 Mt    14.610 %         1 x        4.337 Mt        0.000 Mt        4.337 Mt   100.000 % 	 ...../BuildShaderTables

		      64.493 Mt     0.704 %         1 x       64.493 Mt        0.000 Mt       64.493 Mt   100.000 % 	 ..../GpuSidePrep

		   25964.058 Mt    72.956 %     14129 x        1.838 Mt        4.469 Mt      226.176 Mt     0.871 % 	 ../MainLoop

		     876.826 Mt     3.377 %      1562 x        0.561 Mt        0.452 Mt      875.964 Mt    99.902 % 	 .../Update

		       0.863 Mt     0.098 %         1 x        0.863 Mt        0.000 Mt        0.863 Mt   100.000 % 	 ..../GuiModelApply

		   24861.056 Mt    95.752 %     10545 x        2.358 Mt        4.006 Mt    12030.136 Mt    48.389 % 	 .../Render

		   12830.920 Mt    51.611 %     10545 x        1.217 Mt        2.453 Mt    12778.523 Mt    99.592 % 	 ..../Textured

		      52.397 Mt     0.408 %     10545 x        0.005 Mt        0.004 Mt       52.397 Mt   100.000 % 	 ...../TexturedRLPrep

	WindowThread:

		   35584.814 Mt   100.000 %         1 x    35584.815 Mt    35584.814 Mt    35584.814 Mt   100.000 % 	 /

	GpuThread:

		    4485.006 Mt   100.000 %     10545 x        0.425 Mt        0.432 Mt      176.302 Mt     3.931 % 	 /

		      16.188 Mt     0.361 %         1 x       16.188 Mt        0.000 Mt        0.051 Mt     0.314 % 	 ./ScenePrep

		      16.138 Mt    99.686 %         1 x       16.138 Mt        0.000 Mt        0.002 Mt     0.013 % 	 ../AccelerationStructureBuild

		      15.526 Mt    96.211 %         1 x       15.526 Mt        0.000 Mt       15.526 Mt   100.000 % 	 .../BuildBottomLevelASGpu

		       0.609 Mt     3.776 %         1 x        0.609 Mt        0.000 Mt        0.609 Mt   100.000 % 	 .../BuildTopLevelASGpu

		    4262.507 Mt    95.039 %     10545 x        0.404 Mt        0.429 Mt       22.078 Mt     0.518 % 	 ./TexturedGpu

		    4240.429 Mt    99.482 %     10545 x        0.402 Mt        0.426 Mt     4240.429 Mt   100.000 % 	 ../TexturedRLGpu

		      30.008 Mt     0.669 %     10545 x        0.003 Mt        0.003 Mt       30.008 Mt   100.000 % 	 ./Present


	============================


