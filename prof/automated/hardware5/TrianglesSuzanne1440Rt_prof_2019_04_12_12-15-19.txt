Configuration: 
Command line parameters  = E:\programming\FIT-DIP\Masters-DXR-Main-git\build\Measurement\x64\Debug\Measurement.exe --scene Suzanne/Suzanne.gltf --duplication-cube --duplication-offset 3 --profile-triangles --profile-camera-track Suzanne/SuzanneFast.trk --profile-max-duplication 10 --rt-mode --width 2560 --height 1440 --profile-output TrianglesSuzanne1440Rt 
Window width             = 2560
Window height            = 1421
Window title             = Direct3D App
Back-buffer count        = 3
Fill Triangles           = enabled
Triangles Font Clock-Wise= enabled
Fast Build AS            = disabled
Target FPS               = 0
Target UPS               = 60
Tearing                  = enabled
VSync                    = disabled
GUI rendering            = disabled
Debug layer              = enabled
GPU validation           = disabled
GPU profiling            = enabled
Render mode              = Ray Tracing
Loaded scene file        = Suzanne/Suzanne.gltf
Using testing scene      = disabled
Duplication              = 10
Duplication in cube      = enabled
Duplication offset       = 3
BLAS duplication         = enabled
Rays per pixel           = 10

ProfilingAggregator: 
Render	,	3.284	,	3.284	,	3.6083	,	3.6083	,	4.9355	,	4.9355	,	6.77	,	6.77	,	4.2949	,	4.2949	,	2.7056	,	3.6741	,	3.6741	,	3.3612	,	3.3612	,	4.2411	,	4.2411	,	4.882	,	4.882	,	4.913	,	4.913	,	2.9798	,	4.6277	,	4.6277	,	3.6495	,	3.6495	,	4.9339	,	4.9339	,	5.1464	,	5.1464	,	6.1147	,	6.1147	,	4.0404	,	5.2445	,	5.2445	,	3.5818	,	3.5818	,	5.7162	,	5.7162	,	7.3037	,	7.3037	,	7.4726	,	7.4726	,	5.0978	,	5.9587	,	5.9587	,	4.8298	,	4.8298	,	6.6786	,	6.6786	,	8.8458	,	8.8458	,	8.3548	,	8.3548	,	4.7637	,	7.1092	,	7.1092	,	5.7006	,	5.7006	,	7.0474	,	7.0474	,	9.8306	,	9.8306	,	9.4754	,	9.4754	,	5.9792	,	9.0777	,	9.0777	,	6.435	,	6.435	,	9.1051	,	9.1051	,	11.5109	,	11.5109	,	10.3717	,	10.3717	,	6.9968	,	10.2446	,	10.2446	,	8.4194	,	8.4194	,	10.1121	,	10.1121	,	12.3974	,	12.3974	,	13.2127	,	13.2127	,	9.4532	,	10.3121	,	10.3121	,	12.6549	,	12.6549	,	14.3161	,	14.3161	,	15.1257	,	15.1257	,	10.9821	,	10.9821	,	10.0385	,	12.1876	,	12.1876	,	17.1426	,	17.1426	,	17.3815	,	17.3815	,	17.7538	,	17.7538	,	12.6278
Update	,	0.4436	,	0.4436	,	0.3011	,	0.3011	,	0.8671	,	0.8671	,	0.4821	,	0.4821	,	0.3696	,	0.3696	,	0.2745	,	0.6441	,	0.6441	,	0.6893	,	0.6893	,	0.287	,	0.287	,	0.4481	,	0.4481	,	0.2852	,	0.2852	,	0.4983	,	0.8554	,	0.8554	,	0.4059	,	0.4059	,	0.3009	,	0.3009	,	0.5161	,	0.5161	,	0.3246	,	0.3246	,	0.3819	,	0.3756	,	0.3756	,	0.6426	,	0.6426	,	0.4151	,	0.4151	,	0.5344	,	0.5344	,	0.409	,	0.409	,	0.8577	,	0.4642	,	0.4642	,	0.5362	,	0.5362	,	0.4826	,	0.4826	,	0.883	,	0.883	,	0.4843	,	0.4843	,	0.9116	,	1.0325	,	1.0325	,	0.652	,	0.652	,	0.7749	,	0.7749	,	1.2273	,	1.2273	,	0.6192	,	0.6192	,	0.868	,	0.7421	,	0.7421	,	0.9264	,	0.9264	,	0.9072	,	0.9072	,	0.7635	,	0.7635	,	0.7085	,	0.7085	,	1.1364	,	1.3417	,	1.3417	,	1.0556	,	1.0556	,	1.1121	,	1.1121	,	0.8371	,	0.8371	,	0.9549	,	0.9549	,	0.9429	,	1.2753	,	1.2753	,	1.3938	,	1.3938	,	1.3154	,	1.3154	,	1.2432	,	1.2432	,	1.2933	,	1.2933	,	1.2527	,	1.3512	,	1.3512	,	1.559	,	1.559	,	1.4554	,	1.4554	,	1.662	,	1.662	,	1.3559
RayTracing	,	1.2222	,	1.2222	,	1.7495	,	1.7495	,	1.4686	,	1.4686	,	1.736	,	1.736	,	1.2318	,	1.2318	,	1.2449	,	1.267	,	1.267	,	1.4971	,	1.4971	,	1.2801	,	1.2801	,	1.2928	,	1.2928	,	1.4123	,	1.4123	,	1.293	,	1.4041	,	1.4041	,	1.6082	,	1.6082	,	1.44	,	1.44	,	1.3999	,	1.3999	,	1.3977	,	1.3977	,	1.6361	,	1.645	,	1.645	,	1.6385	,	1.6385	,	1.9316	,	1.9316	,	1.891	,	1.891	,	1.9215	,	1.9215	,	2.1961	,	2.0607	,	2.0607	,	2.3944	,	2.3944	,	2.2374	,	2.2374	,	2.4099	,	2.4099	,	2.425	,	2.425	,	1.9861	,	2.6143	,	2.6143	,	3.1495	,	3.1495	,	2.6573	,	2.6573	,	2.823	,	2.823	,	2.6554	,	2.6554	,	2.9576	,	4.0877	,	4.0877	,	3.8611	,	3.8611	,	4.1506	,	4.1506	,	4.1822	,	4.1822	,	3.5193	,	3.5193	,	3.6773	,	4.7971	,	4.7971	,	5.4204	,	5.4204	,	4.6374	,	4.6374	,	4.5958	,	4.5958	,	5.442	,	5.442	,	5.524	,	6.0451	,	6.0451	,	7.1878	,	7.1878	,	6.0786	,	6.0786	,	6.0622	,	6.0622	,	6.0041	,	6.0041	,	6.0248	,	7.906	,	7.906	,	9.0968	,	9.0968	,	8.097	,	8.097	,	8.9852	,	8.9852	,	7.9436
RayTracingGpu	,	1.10451	,	1.10451	,	0.638272	,	0.638272	,	2.35216	,	2.35216	,	3.816	,	3.816	,	1.95971	,	1.95971	,	0.552832	,	1.49898	,	1.49898	,	0.680928	,	0.680928	,	1.73558	,	1.73558	,	2.37443	,	2.37443	,	2.29971	,	2.29971	,	0.86304	,	2.11222	,	2.11222	,	0.876544	,	0.876544	,	2.43763	,	2.43763	,	2.77091	,	2.77091	,	3.62762	,	3.62762	,	1.47251	,	2.59955	,	2.59955	,	1.00205	,	1.00205	,	2.67706	,	2.67706	,	4.37021	,	4.37021	,	4.39616	,	4.39616	,	1.42851	,	2.84256	,	2.84256	,	1.18634	,	1.18634	,	3.17526	,	3.17526	,	5.16925	,	5.16925	,	4.83312	,	4.83312	,	1.6935	,	3.26141	,	3.26141	,	1.28502	,	1.28502	,	3.36995	,	3.36995	,	5.64262	,	5.64262	,	5.72618	,	5.72618	,	1.92029	,	3.46822	,	3.46822	,	1.44902	,	1.44902	,	3.65354	,	3.65354	,	6.1272	,	6.1272	,	5.65875	,	5.65875	,	2.2135	,	4.05834	,	4.05834	,	1.67779	,	1.67779	,	4.26346	,	4.26346	,	6.54781	,	6.54781	,	6.47382	,	6.47382	,	2.53325	,	2.96586	,	2.96586	,	4.19133	,	4.19133	,	7.16035	,	7.16035	,	7.73802	,	7.73802	,	3.68112	,	3.68112	,	2.89363	,	2.85347	,	2.85347	,	6.57811	,	6.57811	,	7.7665	,	7.7665	,	7.52662	,	7.52662	,	3.3055
DeferredRLGpu	,	0.114112	,	0.114112	,	0.067136	,	0.067136	,	0.248192	,	0.248192	,	0.26336	,	0.26336	,	0.223776	,	0.223776	,	0.05696	,	0.162752	,	0.162752	,	0.075232	,	0.075232	,	0.195936	,	0.195936	,	0.254752	,	0.254752	,	0.320288	,	0.320288	,	0.122304	,	0.267104	,	0.267104	,	0.112672	,	0.112672	,	0.292736	,	0.292736	,	0.350528	,	0.350528	,	0.575616	,	0.575616	,	0.194496	,	0.344224	,	0.344224	,	0.1416	,	0.1416	,	0.374752	,	0.374752	,	0.693408	,	0.693408	,	0.764416	,	0.764416	,	0.26608	,	0.397568	,	0.397568	,	0.17792	,	0.17792	,	0.461472	,	0.461472	,	0.847872	,	0.847872	,	0.979232	,	0.979232	,	0.352736	,	0.489472	,	0.489472	,	0.20592	,	0.20592	,	0.52704	,	0.52704	,	0.959968	,	0.959968	,	1.13427	,	1.13427	,	0.425888	,	0.56016	,	0.56016	,	0.25504	,	0.25504	,	0.680192	,	0.680192	,	1.07286	,	1.07286	,	1.31392	,	1.31392	,	0.541824	,	0.685024	,	0.685024	,	0.351424	,	0.351424	,	0.834176	,	0.834176	,	1.34614	,	1.34614	,	1.50592	,	1.50592	,	0.677344	,	0.624416	,	0.624416	,	0.816992	,	0.816992	,	1.52035	,	1.52035	,	2.09117	,	2.09117	,	0.986368	,	0.986368	,	0.821952	,	0.936416	,	0.936416	,	1.68656	,	1.68656	,	1.88902	,	1.88902	,	2.05344	,	2.05344	,	1.00291
RayTracingRLGpu	,	0.783008	,	0.783008	,	0.383904	,	0.383904	,	1.76899	,	1.76899	,	3.16659	,	3.16659	,	1.47357	,	1.47357	,	0.317888	,	1.0911	,	1.0911	,	0.418496	,	0.418496	,	1.27629	,	1.27629	,	1.82838	,	1.82838	,	1.69917	,	1.69917	,	0.544864	,	1.55837	,	1.55837	,	0.563008	,	0.563008	,	1.83779	,	1.83779	,	2.10896	,	2.10896	,	2.6872	,	2.6872	,	0.767936	,	1.94019	,	1.94019	,	0.65488	,	0.65488	,	1.98608	,	1.98608	,	3.28506	,	3.28506	,	3.24346	,	3.24346	,	0.938112	,	2.1168	,	2.1168	,	0.790272	,	0.790272	,	2.37405	,	2.37405	,	3.8921	,	3.8921	,	3.45619	,	3.45619	,	1.10877	,	2.42566	,	2.42566	,	0.858848	,	0.858848	,	2.49213	,	2.49213	,	4.22307	,	4.22307	,	3.85059	,	3.85059	,	1.25264	,	2.5535	,	2.5535	,	0.966432	,	0.966432	,	2.61773	,	2.61773	,	4.58602	,	4.58602	,	3.91824	,	3.91824	,	1.42275	,	2.99114	,	2.99114	,	1.08986	,	1.08986	,	3.0391	,	3.0391	,	4.72973	,	4.72973	,	4.51002	,	4.51002	,	1.59974	,	2.03802	,	2.03802	,	2.9992	,	2.9992	,	5.12666	,	5.12666	,	5.14458	,	5.14458	,	2.39469	,	2.39469	,	1.81219	,	1.66893	,	1.66893	,	4.51014	,	4.51014	,	5.37418	,	5.37418	,	4.99245	,	4.99245	,	2.03968
ResolveRLGpu	,	0.205824	,	0.205824	,	0.185824	,	0.185824	,	0.332768	,	0.332768	,	0.384768	,	0.384768	,	0.26112	,	0.26112	,	0.177152	,	0.243968	,	0.243968	,	0.186048	,	0.186048	,	0.262496	,	0.262496	,	0.290208	,	0.290208	,	0.279104	,	0.279104	,	0.194624	,	0.28464	,	0.28464	,	0.199616	,	0.199616	,	0.30624	,	0.30624	,	0.310144	,	0.310144	,	0.363872	,	0.363872	,	0.50912	,	0.313888	,	0.313888	,	0.204736	,	0.204736	,	0.315136	,	0.315136	,	0.390752	,	0.390752	,	0.386912	,	0.386912	,	0.223392	,	0.327264	,	0.327264	,	0.216992	,	0.216992	,	0.338784	,	0.338784	,	0.42816	,	0.42816	,	0.396576	,	0.396576	,	0.231008	,	0.345408	,	0.345408	,	0.219264	,	0.219264	,	0.34976	,	0.34976	,	0.458752	,	0.458752	,	0.740192	,	0.740192	,	0.240352	,	0.3536	,	0.3536	,	0.226432	,	0.226432	,	0.354208	,	0.354208	,	0.4672	,	0.4672	,	0.425376	,	0.425376	,	0.247936	,	0.380736	,	0.380736	,	0.235072	,	0.235072	,	0.389088	,	0.389088	,	0.470816	,	0.470816	,	0.456448	,	0.456448	,	0.255232	,	0.302464	,	0.302464	,	0.373664	,	0.373664	,	0.51184	,	0.51184	,	0.501408	,	0.501408	,	0.299264	,	0.299264	,	0.258528	,	0.246304	,	0.246304	,	0.377856	,	0.377856	,	0.50224	,	0.50224	,	0.47888	,	0.47888	,	0.26192
Index	,	0	,	1	,	2	,	3	,	4	,	5	,	6	,	7	,	8	,	9	,	10	,	11	,	12	,	13	,	14	,	15	,	16	,	17	,	18	,	19	,	20	,	21	,	22	,	23	,	24	,	25	,	26	,	27	,	28	,	29	,	30	,	31	,	32	,	33	,	34	,	35	,	36	,	37	,	38	,	39	,	40	,	41	,	42	,	43	,	44	,	45	,	46	,	47	,	48	,	49	,	50	,	51	,	52	,	53	,	54	,	55	,	56	,	57	,	58	,	59	,	60	,	61	,	62	,	63	,	64	,	65	,	66	,	67	,	68	,	69	,	70	,	71	,	72	,	73	,	74	,	75	,	76	,	77	,	78	,	79	,	80	,	81	,	82	,	83	,	84	,	85	,	86	,	87	,	88	,	89	,	90	,	91	,	92	,	93	,	94	,	95	,	96	,	97	,	98	,	99	,	100	,	101	,	102	,	103	,	104	,	105	,	106	,	107

ProfilingBuildAggregator: 
BuildBottomLevelAS	,	0.9375	,	1.9105	,	6.3354	,	14.0296	,	27.2838	,	46.745	,	73.756	,	106.699	,	156.643	,	216.068
BuildBottomLevelASGpu	,	0.35616	,	2.40598	,	6.39251	,	15.7629	,	30.7516	,	52.5036	,	82.7554	,	125.091	,	176.569	,	242.652
BuildTopLevelAS	,	2.2987	,	0.5965	,	0.5664	,	0.5715	,	0.5995	,	0.6272	,	0.6613	,	0.6286	,	0.6797	,	0.6952
BuildTopLevelASGpu	,	0.089024	,	0.06848	,	0.090432	,	0.099072	,	0.124608	,	0.173376	,	0.152832	,	0.136192	,	0.1904	,	0.228672
Duplication	,	1	,	2	,	3	,	4	,	5	,	6	,	7	,	8	,	9	,	10
Triangles	,	3936	,	31488	,	106272	,	251904	,	492000	,	850176	,	1350048	,	2015232	,	2869344	,	3936000
Meshes	,	1	,	8	,	27	,	64	,	125	,	216	,	343	,	512	,	729	,	1000
BottomLevels	,	1	,	8	,	27	,	64	,	125	,	216	,	343	,	512	,	729	,	1000
Index	,	0	,	1	,	2	,	3	,	4	,	5	,	6	,	7	,	8	,	9

FpsAggregator: 
FPS	,	322	,	322	,	270	,	270	,	263	,	263	,	195	,	195	,	172	,	172	,	282	,	290	,	290	,	246	,	246	,	264	,	264	,	193	,	193	,	190	,	190	,	234	,	251	,	251	,	209	,	209	,	228	,	228	,	172	,	172	,	152	,	152	,	196	,	222	,	222	,	184	,	184	,	200	,	200	,	138	,	138	,	119	,	119	,	170	,	193	,	193	,	158	,	158	,	175	,	175	,	122	,	122	,	109	,	109	,	146	,	166	,	166	,	138	,	138	,	150	,	150	,	100	,	100	,	93	,	93	,	126	,	138	,	138	,	117	,	117	,	126	,	126	,	92	,	92	,	83	,	83	,	108	,	110	,	110	,	97	,	97	,	102	,	102	,	75	,	75	,	70	,	70	,	90	,	76	,	76	,	90	,	90	,	69	,	69	,	60	,	60	,	66	,	66	,	86	,	59	,	59	,	63	,	63	,	49	,	49	,	51	,	51	,	60
UPS	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	61	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	61	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	61	,	61	,	60	,	61	,	61	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	61	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	61	,	61	,	60	,	60	,	60	,	61	,	61	,	60	,	60	,	60	,	60	,	61	,	61	,	60	,	60	,	60	,	61	,	61	,	60	,	60	,	60	,	60	,	60	,	60	,	61	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	61	,	61	,	60	,	60	,	60	,	61	,	61	,	60	,	60	,	61	,	61	,	60
FrameTime	,	3.10912	,	3.10912	,	3.70784	,	3.70784	,	3.81378	,	3.81378	,	5.15871	,	5.15871	,	5.82718	,	5.82718	,	3.55278	,	3.45652	,	3.45652	,	4.06737	,	4.06737	,	3.80067	,	3.80067	,	5.18882	,	5.18882	,	5.27729	,	5.27729	,	4.2765	,	3.98616	,	3.98616	,	4.79516	,	4.79516	,	4.39392	,	4.39392	,	5.83108	,	5.83108	,	6.60665	,	6.60665	,	5.11972	,	4.52268	,	4.52268	,	5.43921	,	5.43921	,	5.00139	,	5.00139	,	7.29894	,	7.29894	,	8.42719	,	8.42719	,	5.89787	,	5.18612	,	5.18612	,	6.35319	,	6.35319	,	5.72123	,	5.72123	,	8.20848	,	8.20848	,	9.1896	,	9.1896	,	6.8663	,	6.04562	,	6.04562	,	7.26208	,	7.26208	,	6.70908	,	6.70908	,	10.0497	,	10.0497	,	10.801	,	10.801	,	7.95798	,	7.29247	,	7.29247	,	8.60254	,	8.60254	,	7.9487	,	7.9487	,	10.9713	,	10.9713	,	12.1309	,	12.1309	,	9.27056	,	9.18346	,	9.18346	,	10.3776	,	10.3776	,	9.80892	,	9.80892	,	13.441	,	13.441	,	14.3886	,	14.3886	,	11.1262	,	13.1989	,	13.1989	,	11.1129	,	11.1129	,	14.5449	,	14.5449	,	16.7471	,	16.7471	,	15.2267	,	15.2267	,	11.6597	,	17.0926	,	17.0926	,	16.0634	,	16.0634	,	20.4402	,	20.4402	,	19.9847	,	19.9847	,	16.6909
GigaRays/s	,	11.7003	,	11.7003	,	9.81099	,	9.81099	,	9.53846	,	9.53846	,	7.05168	,	7.05168	,	6.24275	,	6.24275	,	10.2392	,	10.5243	,	10.5243	,	8.94377	,	8.94377	,	9.57138	,	9.57138	,	7.01077	,	7.01077	,	6.89323	,	6.89323	,	8.5064	,	9.12599	,	9.12599	,	7.58632	,	7.58632	,	8.27908	,	8.27908	,	6.23857	,	6.23857	,	5.50621	,	5.50621	,	7.10539	,	8.04337	,	8.04337	,	6.68803	,	6.68803	,	7.2735	,	7.2735	,	4.98396	,	4.98396	,	4.31669	,	4.31669	,	6.16792	,	7.01442	,	7.01442	,	5.72588	,	5.72588	,	6.35835	,	6.35835	,	4.43171	,	4.43171	,	3.95856	,	3.95856	,	5.29799	,	6.01719	,	6.01719	,	5.00925	,	5.00925	,	5.42214	,	5.42214	,	3.61977	,	3.61977	,	3.36797	,	3.36797	,	4.57121	,	4.98838	,	4.98838	,	4.2287	,	4.2287	,	4.57655	,	4.57655	,	3.3157	,	3.3157	,	2.99876	,	2.99876	,	3.92399	,	3.96121	,	3.96121	,	3.50541	,	3.50541	,	3.70863	,	3.70863	,	2.70646	,	2.70646	,	2.52822	,	2.52822	,	3.26956	,	2.75611	,	2.75611	,	3.27345	,	3.27345	,	2.50106	,	2.50106	,	2.17218	,	2.17218	,	2.38906	,	2.38906	,	3.11994	,	2.12827	,	2.12827	,	2.26462	,	2.26462	,	1.77971	,	1.77971	,	1.82027	,	1.82027	,	2.17949
Duplication	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	2	,	2	,	2	,	2	,	2	,	2	,	2	,	2	,	2	,	2	,	2	,	3	,	3	,	3	,	3	,	3	,	3	,	3	,	3	,	3	,	3	,	3	,	4	,	4	,	4	,	4	,	4	,	4	,	4	,	4	,	4	,	4	,	4	,	5	,	5	,	5	,	5	,	5	,	5	,	5	,	5	,	5	,	5	,	5	,	6	,	6	,	6	,	6	,	6	,	6	,	6	,	6	,	6	,	6	,	6	,	7	,	7	,	7	,	7	,	7	,	7	,	7	,	7	,	7	,	7	,	7	,	8	,	8	,	8	,	8	,	8	,	8	,	8	,	8	,	8	,	8	,	8	,	9	,	9	,	9	,	9	,	9	,	9	,	9	,	9	,	9	,	9	,	9	,	10	,	10	,	10	,	10	,	10	,	10	,	10	,	10	,	10
Index	,	0	,	1	,	2	,	3	,	4	,	5	,	6	,	7	,	8	,	9	,	10	,	11	,	12	,	13	,	14	,	15	,	16	,	17	,	18	,	19	,	20	,	21	,	22	,	23	,	24	,	25	,	26	,	27	,	28	,	29	,	30	,	31	,	32	,	33	,	34	,	35	,	36	,	37	,	38	,	39	,	40	,	41	,	42	,	43	,	44	,	45	,	46	,	47	,	48	,	49	,	50	,	51	,	52	,	53	,	54	,	55	,	56	,	57	,	58	,	59	,	60	,	61	,	62	,	63	,	64	,	65	,	66	,	67	,	68	,	69	,	70	,	71	,	72	,	73	,	74	,	75	,	76	,	77	,	78	,	79	,	80	,	81	,	82	,	83	,	84	,	85	,	86	,	87	,	88	,	89	,	90	,	91	,	92	,	93	,	94	,	95	,	96	,	97	,	98	,	99	,	100	,	101	,	102	,	103	,	104	,	105	,	106	,	107


AccelerationStructure: 
Ray tracing acceleration structure building statistics: 
	Bottom Level Acceleration Structure: 
		Maximum [B]: 224128
		Minimum [B]: 224128
		Currently Allocated [B]: 224128000
		Currently Created [num]: 1000
		Current Average [B]: 224128
		Maximum Triangles [num]: 3936
		Minimum Triangles [num]: 3936
		Total Triangles [num]: 3936000
		Maximum Meshes [num]: 1
		Minimum Meshes [num]: 1
		Total Meshes [num]: 1000
	Top Level Acceleration Structure: 
		Maximum [B]: 64000
		Minimum [B]: 64000
		Currently Allocated [B]: 64000
		Total Allocated [B]: 64000
		Total Created [num]: 1
		Average Allocated [B]: 64000
		Maximum Instances [num]: 1000
		Minimum Instances [num]: 1000
		Total Instances [num]: 1000
	Scratch Buffer: 
		Maximum [B]: 79360
		Minimum [B]: 79360
		Currently Allocated [B]: 79360
		Total Allocated [B]: 79360
		Total Created [num]: 1
		Average Allocated [B]: 79360
ProfilingStack: 
[PROF] Profiling statistics: 
	Scopes entered : 135600
	Scopes exited : 135599
	Overhead per scope [ticks] : 1005
	Threads entered : 3
	Threads exited : 0
	============================
		         Sum[Mt]   Parent[%]  Samples[num]       Avg[Mt]   Last/Curr[Mt]        Self[Mt]     Self[%] 	 Name

	main:

		   91345.032 Mt   100.000 %         1 x    91345.033 Mt    91345.031 Mt        0.719 Mt     0.001 % 	 /

		   91344.359 Mt    99.999 %         1 x    91344.361 Mt    91344.359 Mt      421.226 Mt     0.461 % 	 ./Main application loop

		       2.860 Mt     0.003 %         1 x        2.860 Mt        0.000 Mt        2.860 Mt   100.000 % 	 ../TexturedRLInitialization

		       6.449 Mt     0.007 %         1 x        6.449 Mt        0.000 Mt        6.449 Mt   100.000 % 	 ../DeferredRLInitialization

		      48.851 Mt     0.053 %         1 x       48.851 Mt        0.000 Mt        2.951 Mt     6.040 % 	 ../RayTracingRLInitialization

		      45.901 Mt    93.960 %         1 x       45.901 Mt        0.000 Mt       45.901 Mt   100.000 % 	 .../PipelineBuild

		       3.401 Mt     0.004 %         1 x        3.401 Mt        0.000 Mt        3.401 Mt   100.000 % 	 ../ResolveRLInitialization

		     230.151 Mt     0.252 %         1 x      230.151 Mt        0.000 Mt        2.514 Mt     1.092 % 	 ../Initialization

		     227.637 Mt    98.908 %         1 x      227.637 Mt        0.000 Mt        1.284 Mt     0.564 % 	 .../ScenePrep

		     194.827 Mt    85.587 %         1 x      194.827 Mt        0.000 Mt       18.858 Mt     9.679 % 	 ..../SceneLoad

		     163.819 Mt    84.084 %         1 x      163.819 Mt        0.000 Mt       19.163 Mt    11.698 % 	 ...../glTF-LoadScene

		     144.656 Mt    88.302 %         2 x       72.328 Mt        0.000 Mt      144.656 Mt   100.000 % 	 ....../glTF-LoadImageData

		       7.811 Mt     4.009 %         1 x        7.811 Mt        0.000 Mt        7.811 Mt   100.000 % 	 ...../glTF-CreateScene

		       4.339 Mt     2.227 %         1 x        4.339 Mt        0.000 Mt        4.339 Mt   100.000 % 	 ...../SceneDesc-CreateScene

		       1.167 Mt     0.513 %         1 x        1.167 Mt        0.000 Mt        0.004 Mt     0.386 % 	 ..../TexturedRLPrep

		       1.162 Mt    99.614 %         1 x        1.162 Mt        0.000 Mt        0.392 Mt    33.692 % 	 ...../PrepareForRendering

		       0.482 Mt    41.435 %         1 x        0.482 Mt        0.000 Mt        0.482 Mt   100.000 % 	 ....../PrepareMaterials

		       0.289 Mt    24.873 %         1 x        0.289 Mt        0.000 Mt        0.289 Mt   100.000 % 	 ....../PrepareDrawBundle

		       3.414 Mt     1.500 %         1 x        3.414 Mt        0.000 Mt        0.004 Mt     0.129 % 	 ..../DeferredRLPrep

		       3.410 Mt    99.871 %         1 x        3.410 Mt        0.000 Mt        1.547 Mt    45.384 % 	 ...../PrepareForRendering

		       0.015 Mt     0.449 %         1 x        0.015 Mt        0.000 Mt        0.015 Mt   100.000 % 	 ....../PrepareMaterials

		       1.590 Mt    46.633 %         1 x        1.590 Mt        0.000 Mt        1.590 Mt   100.000 % 	 ....../BuildMaterialCache

		       0.257 Mt     7.534 %         1 x        0.257 Mt        0.000 Mt        0.257 Mt   100.000 % 	 ....../PrepareDrawBundle

		      14.955 Mt     6.570 %         1 x       14.955 Mt        0.000 Mt        5.979 Mt    39.982 % 	 ..../RayTracingRLPrep

		       0.239 Mt     1.600 %         1 x        0.239 Mt        0.000 Mt        0.239 Mt   100.000 % 	 ...../PrepareAccelerationStructures

		       0.023 Mt     0.154 %         1 x        0.023 Mt        0.000 Mt        0.023 Mt   100.000 % 	 ...../PrepareCache

		       0.608 Mt     4.068 %         1 x        0.608 Mt        0.000 Mt        0.608 Mt   100.000 % 	 ...../BuildCache

		       6.194 Mt    41.419 %         1 x        6.194 Mt        0.000 Mt        0.004 Mt     0.066 % 	 ...../BuildAccelerationStructures

		       6.190 Mt    99.934 %         1 x        6.190 Mt        0.000 Mt        2.954 Mt    47.720 % 	 ....../AccelerationStructureBuild

		       0.938 Mt    15.145 %         1 x        0.938 Mt        0.000 Mt        0.938 Mt   100.000 % 	 ......./BuildBottomLevelAS

		       2.299 Mt    37.135 %         1 x        2.299 Mt        0.000 Mt        2.299 Mt   100.000 % 	 ......./BuildTopLevelAS

		       1.911 Mt    12.776 %         1 x        1.911 Mt        0.000 Mt        1.911 Mt   100.000 % 	 ...../BuildShaderTables

		      11.991 Mt     5.267 %         1 x       11.991 Mt        0.000 Mt       11.991 Mt   100.000 % 	 ..../GpuSidePrep

		   90631.420 Mt    99.220 %     17189 x        5.273 Mt       33.894 Mt      566.417 Mt     0.625 % 	 ../MainLoop

		    5522.439 Mt     6.093 %      5441 x        1.015 Mt        2.213 Mt     5521.391 Mt    99.981 % 	 .../Update

		       1.048 Mt     0.019 %         1 x        1.048 Mt        0.000 Mt        1.048 Mt   100.000 % 	 ..../GuiModelApply

		   84542.564 Mt    93.282 %     12518 x        6.754 Mt       23.630 Mt    49799.674 Mt    58.905 % 	 .../Render

		   34742.890 Mt    41.095 %     12518 x        2.775 Mt       17.610 Mt    31590.274 Mt    90.926 % 	 ..../RayTracing

		     207.534 Mt     0.597 %     12518 x        0.017 Mt        0.006 Mt       53.014 Mt    25.545 % 	 ...../DeferredRLPrep

		     154.521 Mt    74.455 %        18 x        8.584 Mt        0.000 Mt        5.564 Mt     3.601 % 	 ....../PrepareForRendering

		      16.042 Mt    10.382 %        18 x        0.891 Mt        0.000 Mt       16.042 Mt   100.000 % 	 ......./PrepareMaterials

		       0.014 Mt     0.009 %        18 x        0.001 Mt        0.000 Mt        0.014 Mt   100.000 % 	 ......./BuildMaterialCache

		     132.900 Mt    86.008 %        18 x        7.383 Mt        0.000 Mt      132.900 Mt   100.000 % 	 ......./PrepareDrawBundle

		    2945.082 Mt     8.477 %     12518 x        0.235 Mt        0.013 Mt       64.389 Mt     2.186 % 	 ...../RayTracingRLPrep

		    1332.117 Mt    45.232 %        18 x       74.007 Mt        0.000 Mt     1332.117 Mt   100.000 % 	 ....../PrepareAccelerationStructures

		      16.152 Mt     0.548 %        18 x        0.897 Mt        0.000 Mt       16.152 Mt   100.000 % 	 ....../PrepareCache

		       0.012 Mt     0.000 %        18 x        0.001 Mt        0.000 Mt        0.012 Mt   100.000 % 	 ....../BuildCache

		    1401.466 Mt    47.587 %        18 x       77.859 Mt        0.000 Mt        0.041 Mt     0.003 % 	 ....../BuildAccelerationStructures

		    1401.425 Mt    99.997 %        18 x       77.857 Mt        0.000 Mt       47.823 Mt     3.412 % 	 ......./AccelerationStructureBuild

		    1341.899 Mt    95.752 %        18 x       74.550 Mt        0.000 Mt     1341.899 Mt   100.000 % 	 ......../BuildBottomLevelAS

		      11.704 Mt     0.835 %        18 x        0.650 Mt        0.000 Mt       11.704 Mt   100.000 % 	 ......../BuildTopLevelAS

		     130.947 Mt     4.446 %        18 x        7.275 Mt        0.000 Mt      130.947 Mt   100.000 % 	 ....../BuildShaderTables

	WindowThread:

		   91340.272 Mt   100.000 %         1 x    91340.273 Mt    91340.272 Mt    91340.272 Mt   100.000 % 	 /

	GpuThread:

		   35039.058 Mt   100.000 %     12518 x        2.799 Mt        3.305 Mt      136.922 Mt     0.391 % 	 /

		       0.454 Mt     0.001 %         1 x        0.454 Mt        0.000 Mt        0.007 Mt     1.475 % 	 ./ScenePrep

		       0.447 Mt    98.525 %         1 x        0.447 Mt        0.000 Mt        0.002 Mt     0.372 % 	 ../AccelerationStructureBuild

		       0.356 Mt    79.705 %         1 x        0.356 Mt        0.000 Mt        0.356 Mt   100.000 % 	 .../BuildBottomLevelASGpu

		       0.089 Mt    19.923 %         1 x        0.089 Mt        0.000 Mt        0.089 Mt   100.000 % 	 .../BuildTopLevelASGpu

		   34876.223 Mt    99.535 %     12518 x        2.786 Mt        3.304 Mt       14.794 Mt     0.042 % 	 ./RayTracingGpu

		    5652.978 Mt    16.209 %     12518 x        0.452 Mt        1.003 Mt     5652.978 Mt   100.000 % 	 ../DeferredRLGpu

		   23926.745 Mt    68.605 %     12518 x        1.911 Mt        2.038 Mt    23926.745 Mt   100.000 % 	 ../RayTracingRLGpu

		    3752.520 Mt    10.760 %     12518 x        0.300 Mt        0.261 Mt     3752.520 Mt   100.000 % 	 ../ResolveRLGpu

		    1529.186 Mt     4.385 %        18 x       84.955 Mt        0.000 Mt        0.020 Mt     0.001 % 	 ../AccelerationStructureBuild

		    1526.601 Mt    99.831 %        18 x       84.811 Mt        0.000 Mt     1526.601 Mt   100.000 % 	 .../BuildBottomLevelASGpu

		       2.564 Mt     0.168 %        18 x        0.142 Mt        0.000 Mt        2.564 Mt   100.000 % 	 .../BuildTopLevelASGpu

		      25.460 Mt     0.073 %     12518 x        0.002 Mt        0.002 Mt       25.460 Mt   100.000 % 	 ./Present


	============================


