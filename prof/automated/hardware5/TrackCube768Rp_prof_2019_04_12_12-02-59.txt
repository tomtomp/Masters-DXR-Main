Configuration: 
Command line parameters  = E:\programming\FIT-DIP\Masters-DXR-Main-git\build\Measurement\x64\Debug\Measurement.exe --scene Cube/Cube.gltf --profile-automation --profile-camera-track Cube/Cube.trk --rt-mode-pure --width 1024 --height 768 --profile-output TrackCube768Rp 
Window width             = 1024
Window height            = 768
Window title             = Direct3D App
Back-buffer count        = 3
Fill Triangles           = enabled
Triangles Font Clock-Wise= enabled
Fast Build AS            = disabled
Target FPS               = 0
Target UPS               = 60
Tearing                  = enabled
VSync                    = disabled
GUI rendering            = disabled
Debug layer              = enabled
GPU validation           = disabled
GPU profiling            = enabled
Render mode              = Ray Tracing
Loaded scene file        = Cube/Cube.gltf
Using testing scene      = disabled
Duplication              = 1
Duplication in cube      = disabled
Duplication offset       = 10
BLAS duplication         = disabled
Rays per pixel           = 9

ProfilingAggregator: 
Render	,	1.8049	,	2.2725	,	2.2938	,	2.5764	,	3.6053	,	3.0794	,	4.7287	,	4.0813	,	4.3987	,	3.9966	,	2.8856	,	2.7343	,	2.592	,	2.9229	,	3.3373	,	3.6965	,	4.2869	,	3.5198	,	4.4613	,	4.2999	,	3.5582	,	3.014	,	3.6862	,	2.4554	,	3.5257	,	3.2046	,	3.0646	,	2.436	,	3.065	,	2.2765
Update	,	0.408	,	0.2825	,	0.5814	,	0.3644	,	0.6614	,	0.6093	,	0.7818	,	0.5475	,	0.4859	,	0.6291	,	0.3842	,	0.3386	,	0.6788	,	0.7529	,	0.757	,	0.7367	,	0.6939	,	0.5394	,	0.7929	,	0.5663	,	0.3481	,	0.4985	,	0.543	,	0.2577	,	0.6058	,	0.7231	,	0.4601	,	0.4716	,	0.2628	,	0.6227
RayTracing	,	0.7193	,	0.7674	,	0.7203	,	0.7904	,	1.3966	,	0.7883	,	1.6042	,	0.9706	,	1.5043	,	1.5217	,	0.7455	,	0.7459	,	0.7411	,	0.9029	,	0.805	,	1.1	,	1.5152	,	1.0001	,	1.5845	,	1.5553	,	1.4981	,	1.1232	,	1.4069	,	0.7202	,	1.3436	,	1.3393	,	1.289	,	0.9561	,	1.3463	,	0.7279
RayTracingGpu	,	0.424128	,	0.753824	,	0.844992	,	0.986592	,	1.24464	,	1.46922	,	1.65558	,	1.73162	,	1.29466	,	1.26947	,	1.28973	,	1.21514	,	1.04778	,	1.06512	,	1.24522	,	1.46448	,	1.33587	,	1.34109	,	1.38867	,	1.30336	,	1.10637	,	0.89056	,	0.865728	,	0.817632	,	0.759648	,	0.662784	,	0.598656	,	0.561408	,	0.561312	,	0.61392
RayTracingRLGpu	,	0.360928	,	0.665472	,	0.755776	,	0.897056	,	1.15594	,	1.38058	,	1.55005	,	1.64317	,	1.22707	,	1.20262	,	1.20144	,	1.14787	,	0.981408	,	0.976192	,	1.15597	,	1.37501	,	1.26934	,	1.2743	,	1.32128	,	1.23798	,	1.04086	,	0.823008	,	0.776672	,	0.728448	,	0.670176	,	0.57312	,	0.508128	,	0.47024	,	0.469088	,	0.520288
ResolveRLGpu	,	0.062528	,	0.087008	,	0.087808	,	0.088032	,	0.087424	,	0.087424	,	0.103904	,	0.086528	,	0.066528	,	0.065792	,	0.087232	,	0.066208	,	0.06512	,	0.087264	,	0.087776	,	0.087968	,	0.06528	,	0.06576	,	0.065824	,	0.064384	,	0.064544	,	0.066272	,	0.087392	,	0.087712	,	0.087968	,	0.088448	,	0.088768	,	0.08912	,	0.09072	,	0.09216
Index	,	0	,	1	,	2	,	3	,	4	,	5	,	6	,	7	,	8	,	9	,	10	,	11	,	12	,	13	,	14	,	15	,	16	,	17	,	18	,	19	,	20	,	21	,	22	,	23	,	24	,	25	,	26	,	27	,	28	,	29

ProfilingBuildAggregator: 
BuildBottomLevelAS	,	1.0141
BuildBottomLevelASGpu	,	0.087136
BuildTopLevelAS	,	1.0241
BuildTopLevelASGpu	,	0.059552
Duplication	,	1
Triangles	,	12
Meshes	,	1
BottomLevels	,	1
Index	,	0

FpsAggregator: 
FPS	,	434	,	387	,	357	,	339	,	282	,	240	,	255	,	258	,	253	,	256	,	282	,	275	,	280	,	297	,	280	,	250	,	257	,	249	,	248	,	263	,	263	,	319	,	325	,	352	,	375	,	370	,	368	,	381	,	386	,	388
UPS	,	59	,	61	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	61	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	61	,	60	,	60	,	60	,	60	,	60	,	60	,	60
FrameTime	,	2.307	,	2.58979	,	2.80477	,	2.95325	,	3.55244	,	4.17554	,	3.92904	,	3.88174	,	3.9575	,	3.91106	,	3.55559	,	3.63873	,	3.5814	,	3.37323	,	3.57457	,	4.00673	,	3.89735	,	4.02015	,	4.03559	,	3.81711	,	3.80614	,	3.13656	,	3.08514	,	2.84347	,	2.6721	,	2.70984	,	2.71769	,	2.62821	,	2.59445	,	2.57983
GigaRays/s	,	3.068	,	2.733	,	2.52352	,	2.39665	,	1.9924	,	1.69508	,	1.80143	,	1.82338	,	1.78847	,	1.80971	,	1.99064	,	1.94515	,	1.97629	,	2.09825	,	1.98006	,	1.7665	,	1.81608	,	1.7606	,	1.75387	,	1.85425	,	1.8596	,	2.25658	,	2.29419	,	2.48917	,	2.64881	,	2.61192	,	2.60437	,	2.69305	,	2.72809	,	2.74355
Index	,	0	,	1	,	2	,	3	,	4	,	5	,	6	,	7	,	8	,	9	,	10	,	11	,	12	,	13	,	14	,	15	,	16	,	17	,	18	,	19	,	20	,	21	,	22	,	23	,	24	,	25	,	26	,	27	,	28	,	29


AccelerationStructure: 
Ray tracing acceleration structure building statistics: 
	Bottom Level Acceleration Structure: 
		Maximum [B]: 5168
		Minimum [B]: 5168
		Currently Allocated [B]: 5168
		Currently Created [num]: 1
		Current Average [B]: 5168
		Maximum Triangles [num]: 12
		Minimum Triangles [num]: 12
		Total Triangles [num]: 12
		Maximum Meshes [num]: 1
		Minimum Meshes [num]: 1
		Total Meshes [num]: 1
	Top Level Acceleration Structure: 
		Maximum [B]: 64
		Minimum [B]: 64
		Currently Allocated [B]: 64
		Total Allocated [B]: 64
		Total Created [num]: 1
		Average Allocated [B]: 64
		Maximum Instances [num]: 1
		Minimum Instances [num]: 1
		Total Instances [num]: 1
	Scratch Buffer: 
		Maximum [B]: 3584
		Minimum [B]: 3584
		Currently Allocated [B]: 3584
		Total Allocated [B]: 3584
		Total Created [num]: 1
		Average Allocated [B]: 3584
ProfilingStack: 
[PROF] Profiling statistics: 
	Scopes entered : 77147
	Scopes exited : 77146
	Overhead per scope [ticks] : 1009.4
	Threads entered : 3
	Threads exited : 0
	============================
		         Sum[Mt]   Parent[%]  Samples[num]       Avg[Mt]   Last/Curr[Mt]        Self[Mt]     Self[%] 	 Name

	main:

		   30539.235 Mt   100.000 %         1 x    30539.237 Mt    30539.235 Mt        0.709 Mt     0.002 % 	 /

		   30538.570 Mt    99.998 %         1 x    30538.571 Mt    30538.570 Mt      373.831 Mt     1.224 % 	 ./Main application loop

		       2.917 Mt     0.010 %         1 x        2.917 Mt        0.000 Mt        2.917 Mt   100.000 % 	 ../TexturedRLInitialization

		       5.627 Mt     0.018 %         1 x        5.627 Mt        0.000 Mt        5.627 Mt   100.000 % 	 ../DeferredRLInitialization

		      47.233 Mt     0.155 %         1 x       47.233 Mt        0.000 Mt        2.394 Mt     5.069 % 	 ../RayTracingRLInitialization

		      44.839 Mt    94.931 %         1 x       44.839 Mt        0.000 Mt       44.839 Mt   100.000 % 	 .../PipelineBuild

		       3.371 Mt     0.011 %         1 x        3.371 Mt        0.000 Mt        3.371 Mt   100.000 % 	 ../ResolveRLInitialization

		     112.169 Mt     0.367 %         1 x      112.169 Mt        0.000 Mt        2.377 Mt     2.119 % 	 ../Initialization

		     109.792 Mt    97.881 %         1 x      109.792 Mt        0.000 Mt        1.226 Mt     1.117 % 	 .../ScenePrep

		      78.031 Mt    71.072 %         1 x       78.031 Mt        0.000 Mt       13.131 Mt    16.828 % 	 ..../SceneLoad

		      57.541 Mt    73.741 %         1 x       57.541 Mt        0.000 Mt       12.514 Mt    21.749 % 	 ...../glTF-LoadScene

		      45.026 Mt    78.251 %         2 x       22.513 Mt        0.000 Mt       45.026 Mt   100.000 % 	 ....../glTF-LoadImageData

		       4.121 Mt     5.281 %         1 x        4.121 Mt        0.000 Mt        4.121 Mt   100.000 % 	 ...../glTF-CreateScene

		       3.239 Mt     4.150 %         1 x        3.239 Mt        0.000 Mt        3.239 Mt   100.000 % 	 ...../SceneDesc-CreateScene

		       1.154 Mt     1.051 %         1 x        1.154 Mt        0.000 Mt        0.004 Mt     0.390 % 	 ..../TexturedRLPrep

		       1.149 Mt    99.610 %         1 x        1.149 Mt        0.000 Mt        0.397 Mt    34.545 % 	 ...../PrepareForRendering

		       0.461 Mt    40.104 %         1 x        0.461 Mt        0.000 Mt        0.461 Mt   100.000 % 	 ....../PrepareMaterials

		       0.291 Mt    25.350 %         1 x        0.291 Mt        0.000 Mt        0.291 Mt   100.000 % 	 ....../PrepareDrawBundle

		       3.661 Mt     3.335 %         1 x        3.661 Mt        0.000 Mt        0.005 Mt     0.123 % 	 ..../DeferredRLPrep

		       3.657 Mt    99.877 %         1 x        3.657 Mt        0.000 Mt        1.600 Mt    43.764 % 	 ...../PrepareForRendering

		       0.015 Mt     0.407 %         1 x        0.015 Mt        0.000 Mt        0.015 Mt   100.000 % 	 ....../PrepareMaterials

		       1.781 Mt    48.697 %         1 x        1.781 Mt        0.000 Mt        1.781 Mt   100.000 % 	 ....../BuildMaterialCache

		       0.261 Mt     7.132 %         1 x        0.261 Mt        0.000 Mt        0.261 Mt   100.000 % 	 ....../PrepareDrawBundle

		      15.297 Mt    13.933 %         1 x       15.297 Mt        0.000 Mt        4.124 Mt    26.957 % 	 ..../RayTracingRLPrep

		       0.233 Mt     1.521 %         1 x        0.233 Mt        0.000 Mt        0.233 Mt   100.000 % 	 ...../PrepareAccelerationStructures

		       0.023 Mt     0.153 %         1 x        0.023 Mt        0.000 Mt        0.023 Mt   100.000 % 	 ...../PrepareCache

		       0.481 Mt     3.147 %         1 x        0.481 Mt        0.000 Mt        0.481 Mt   100.000 % 	 ...../BuildCache

		       8.651 Mt    56.556 %         1 x        8.651 Mt        0.000 Mt        0.004 Mt     0.045 % 	 ...../BuildAccelerationStructures

		       8.648 Mt    99.955 %         1 x        8.648 Mt        0.000 Mt        6.609 Mt    76.430 % 	 ....../AccelerationStructureBuild

		       1.014 Mt    11.727 %         1 x        1.014 Mt        0.000 Mt        1.014 Mt   100.000 % 	 ......./BuildBottomLevelAS

		       1.024 Mt    11.843 %         1 x        1.024 Mt        0.000 Mt        1.024 Mt   100.000 % 	 ......./BuildTopLevelAS

		       1.785 Mt    11.666 %         1 x        1.785 Mt        0.000 Mt        1.785 Mt   100.000 % 	 ...../BuildShaderTables

		      10.423 Mt     9.493 %         1 x       10.423 Mt        0.000 Mt       10.423 Mt   100.000 % 	 ..../GpuSidePrep

		   29993.421 Mt    98.215 %     10408 x        2.882 Mt        2.822 Mt      381.097 Mt     1.271 % 	 ../MainLoop

		    1039.482 Mt     3.466 %      1804 x        0.576 Mt        0.210 Mt     1038.609 Mt    99.916 % 	 .../Update

		       0.873 Mt     0.084 %         1 x        0.873 Mt        0.000 Mt        0.873 Mt   100.000 % 	 ..../GuiModelApply

		   28572.842 Mt    95.264 %      9271 x        3.082 Mt        2.293 Mt    18884.433 Mt    66.092 % 	 .../Render

		    9688.409 Mt    33.908 %      9271 x        1.045 Mt        0.799 Mt     9636.800 Mt    99.467 % 	 ..../RayTracing

		      51.610 Mt     0.533 %      9271 x        0.006 Mt        0.003 Mt       51.610 Mt   100.000 % 	 ...../RayTracingRLPrep

	WindowThread:

		   30534.658 Mt   100.000 %         1 x    30534.658 Mt    30534.658 Mt    30534.658 Mt   100.000 % 	 /

	GpuThread:

		    9287.181 Mt   100.000 %      9271 x        1.002 Mt        0.616 Mt      117.648 Mt     1.267 % 	 /

		       0.153 Mt     0.002 %         1 x        0.153 Mt        0.000 Mt        0.005 Mt     3.439 % 	 ./ScenePrep

		       0.147 Mt    96.561 %         1 x        0.147 Mt        0.000 Mt        0.001 Mt     0.456 % 	 ../AccelerationStructureBuild

		       0.087 Mt    59.131 %         1 x        0.087 Mt        0.000 Mt        0.087 Mt   100.000 % 	 .../BuildBottomLevelASGpu

		       0.060 Mt    40.413 %         1 x        0.060 Mt        0.000 Mt        0.060 Mt   100.000 % 	 .../BuildTopLevelASGpu

		    9143.484 Mt    98.453 %      9271 x        0.986 Mt        0.612 Mt       13.073 Mt     0.143 % 	 ./RayTracingGpu

		    8382.605 Mt    91.678 %      9271 x        0.904 Mt        0.520 Mt     8382.605 Mt   100.000 % 	 ../RayTracingRLGpu

		     747.806 Mt     8.179 %      9271 x        0.081 Mt        0.091 Mt      747.806 Mt   100.000 % 	 ../ResolveRLGpu

		      25.896 Mt     0.279 %      9271 x        0.003 Mt        0.003 Mt       25.896 Mt   100.000 % 	 ./Present


	============================


