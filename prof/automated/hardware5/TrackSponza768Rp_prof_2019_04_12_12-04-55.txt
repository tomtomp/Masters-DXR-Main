Configuration: 
Command line parameters  = E:\programming\FIT-DIP\Masters-DXR-Main-git\build\Measurement\x64\Debug\Measurement.exe --scene Sponza/Sponza.gltf --profile-automation --profile-camera-track Sponza/Sponza.trk --rt-mode-pure --width 1024 --height 768 --profile-output TrackSponza768Rp 
Window width             = 1024
Window height            = 768
Window title             = Direct3D App
Back-buffer count        = 3
Fill Triangles           = enabled
Triangles Font Clock-Wise= enabled
Fast Build AS            = disabled
Target FPS               = 0
Target UPS               = 60
Tearing                  = enabled
VSync                    = disabled
GUI rendering            = disabled
Debug layer              = enabled
GPU validation           = disabled
GPU profiling            = enabled
Render mode              = Ray Tracing
Loaded scene file        = Sponza/Sponza.gltf
Using testing scene      = disabled
Duplication              = 1
Duplication in cube      = disabled
Duplication offset       = 10
BLAS duplication         = disabled
Rays per pixel           = 9

ProfilingAggregator: 
Render	,	3.8363	,	3.8185	,	3.4182	,	3.3735	,	4.0244	,	3.7224	,	3.7826	,	3.3927	,	3.7086	,	3.6646	,	3.2423	,	3.3464	,	3.6134	,	3.6886	,	3.7835	,	3.8496	,	3.9105	,	8.2473	,	4.0065	,	3.3418	,	7.1345	,	4.6136	,	3.9966	,	3.6012	,	3.6049	,	3.6524
Update	,	0.6109	,	0.5316	,	0.2754	,	0.5319	,	0.4798	,	0.4997	,	0.8518	,	0.3773	,	0.3762	,	0.4012	,	0.673	,	0.8579	,	0.2715	,	0.4349	,	0.3705	,	0.4731	,	0.742	,	0.7477	,	0.465	,	0.5615	,	0.7533	,	0.8654	,	0.3182	,	0.686	,	0.7856	,	0.6671
RayTracing	,	1.2289	,	1.2318	,	1.0416	,	1.0491	,	1.3078	,	1.0476	,	1.1981	,	1.0401	,	1.3456	,	1.2298	,	1.0484	,	1.0415	,	1.1787	,	1.2569	,	1.2357	,	1.2574	,	1.2358	,	1.2627	,	1.3901	,	1.0613	,	4.24	,	1.5067	,	1.2563	,	1.0486	,	1.0457	,	1.2299
RayTracingGpu	,	1.5119	,	1.49555	,	1.55475	,	1.52288	,	1.63645	,	1.5841	,	1.32525	,	1.18637	,	1.28202	,	1.35418	,	1.39587	,	1.46672	,	1.44691	,	1.44954	,	1.41526	,	1.4287	,	1.44854	,	1.38422	,	1.39203	,	1.41696	,	2.01494	,	1.87187	,	1.70467	,	1.56966	,	1.39638	,	1.4584
RayTracingRLGpu	,	1.44893	,	1.43309	,	1.49194	,	1.46019	,	1.57456	,	1.52224	,	1.26214	,	1.12352	,	1.21904	,	1.2912	,	1.33299	,	1.40448	,	1.38486	,	1.38717	,	1.35286	,	1.36659	,	1.3856	,	1.32144	,	1.32976	,	1.3545	,	1.95248	,	1.80912	,	1.64218	,	1.50749	,	1.33402	,	1.39597
ResolveRLGpu	,	0.062336	,	0.061856	,	0.062048	,	0.062048	,	0.06112	,	0.061088	,	0.062048	,	0.062048	,	0.062176	,	0.061952	,	0.062048	,	0.061504	,	0.06128	,	0.061568	,	0.061472	,	0.061312	,	0.062176	,	0.061856	,	0.061472	,	0.061856	,	0.061664	,	0.061824	,	0.06192	,	0.0616	,	0.061632	,	0.061664
Index	,	0	,	1	,	2	,	3	,	4	,	5	,	6	,	7	,	8	,	9	,	10	,	11	,	12	,	13	,	14	,	15	,	16	,	17	,	18	,	19	,	20	,	21	,	22	,	23	,	24	,	25

ProfilingBuildAggregator: 
BuildBottomLevelAS	,	4.3493
BuildBottomLevelASGpu	,	15.874
BuildTopLevelAS	,	3.0231
BuildTopLevelASGpu	,	0.632768
Duplication	,	1
Triangles	,	262267
Meshes	,	103
BottomLevels	,	1
Index	,	0

FpsAggregator: 
FPS	,	185	,	254	,	260	,	254	,	259	,	262	,	272	,	278	,	278	,	274	,	265	,	264	,	262	,	257	,	262	,	261	,	261	,	267	,	262	,	270	,	235	,	224	,	241	,	242	,	260	,	263
UPS	,	59	,	60	,	60	,	61	,	60	,	60	,	60	,	60	,	60	,	60	,	61	,	60	,	60	,	60	,	60	,	60	,	60	,	61	,	60	,	60	,	60	,	60	,	61	,	60	,	60	,	60
FrameTime	,	5.41579	,	3.93744	,	3.84645	,	3.95143	,	3.87213	,	3.81877	,	3.68196	,	3.6092	,	3.60815	,	3.65841	,	3.78286	,	3.79098	,	3.83043	,	3.8967	,	3.81976	,	3.83676	,	3.84607	,	3.7775	,	3.8194	,	3.71367	,	4.28224	,	4.47203	,	4.1616	,	4.14172	,	3.85603	,	3.80631
GigaRays/s	,	1.3069	,	1.79758	,	1.84011	,	1.79122	,	1.8279	,	1.85345	,	1.92231	,	1.96107	,	1.96164	,	1.93469	,	1.87104	,	1.86703	,	1.84781	,	1.81638	,	1.85297	,	1.84476	,	1.84029	,	1.8737	,	1.85314	,	1.9059	,	1.65285	,	1.5827	,	1.70076	,	1.70893	,	1.83554	,	1.85951
Index	,	0	,	1	,	2	,	3	,	4	,	5	,	6	,	7	,	8	,	9	,	10	,	11	,	12	,	13	,	14	,	15	,	16	,	17	,	18	,	19	,	20	,	21	,	22	,	23	,	24	,	25


AccelerationStructure: 
Ray tracing acceleration structure building statistics: 
	Bottom Level Acceleration Structure: 
		Maximum [B]: 14623872
		Minimum [B]: 14623872
		Currently Allocated [B]: 14623872
		Currently Created [num]: 1
		Current Average [B]: 14623872
		Maximum Triangles [num]: 262267
		Minimum Triangles [num]: 262267
		Total Triangles [num]: 262267
		Maximum Meshes [num]: 103
		Minimum Meshes [num]: 103
		Total Meshes [num]: 103
	Top Level Acceleration Structure: 
		Maximum [B]: 64
		Minimum [B]: 64
		Currently Allocated [B]: 64
		Total Allocated [B]: 64
		Total Created [num]: 1
		Average Allocated [B]: 64
		Maximum Instances [num]: 1
		Minimum Instances [num]: 1
		Total Instances [num]: 1
	Scratch Buffer: 
		Maximum [B]: 11660800
		Minimum [B]: 11660800
		Currently Allocated [B]: 11660800
		Total Allocated [B]: 11660800
		Total Created [num]: 1
		Average Allocated [B]: 11660800
ProfilingStack: 
[PROF] Profiling statistics: 
	Scopes entered : 59120
	Scopes exited : 59119
	Overhead per scope [ticks] : 1003.7
	Threads entered : 3
	Threads exited : 0
	============================
		         Sum[Mt]   Parent[%]  Samples[num]       Avg[Mt]   Last/Curr[Mt]        Self[Mt]     Self[%] 	 Name

	main:

		   35809.584 Mt   100.000 %         1 x    35809.586 Mt    35809.584 Mt        0.680 Mt     0.002 % 	 /

		   35808.948 Mt    99.998 %         1 x    35808.950 Mt    35808.948 Mt      370.082 Mt     1.033 % 	 ./Main application loop

		       2.847 Mt     0.008 %         1 x        2.847 Mt        0.000 Mt        2.847 Mt   100.000 % 	 ../TexturedRLInitialization

		       6.453 Mt     0.018 %         1 x        6.453 Mt        0.000 Mt        6.453 Mt   100.000 % 	 ../DeferredRLInitialization

		      59.961 Mt     0.167 %         1 x       59.961 Mt        0.000 Mt        2.116 Mt     3.529 % 	 ../RayTracingRLInitialization

		      57.845 Mt    96.471 %         1 x       57.845 Mt        0.000 Mt       57.845 Mt   100.000 % 	 .../PipelineBuild

		       3.108 Mt     0.009 %         1 x        3.108 Mt        0.000 Mt        3.108 Mt   100.000 % 	 ../ResolveRLInitialization

		    9343.577 Mt    26.093 %         1 x     9343.577 Mt        0.000 Mt        8.235 Mt     0.088 % 	 ../Initialization

		    9335.342 Mt    99.912 %         1 x     9335.342 Mt        0.000 Mt        3.789 Mt     0.041 % 	 .../ScenePrep

		    9214.327 Mt    98.704 %         1 x     9214.327 Mt        0.000 Mt      432.167 Mt     4.690 % 	 ..../SceneLoad

		    8193.021 Mt    88.916 %         1 x     8193.021 Mt        0.000 Mt     1131.211 Mt    13.807 % 	 ...../glTF-LoadScene

		    7061.810 Mt    86.193 %        69 x      102.345 Mt        0.000 Mt     7061.810 Mt   100.000 % 	 ....../glTF-LoadImageData

		     347.175 Mt     3.768 %         1 x      347.175 Mt        0.000 Mt      347.175 Mt   100.000 % 	 ...../glTF-CreateScene

		     241.964 Mt     2.626 %         1 x      241.964 Mt        0.000 Mt      241.964 Mt   100.000 % 	 ...../SceneDesc-CreateScene

		       7.076 Mt     0.076 %         1 x        7.076 Mt        0.000 Mt        0.006 Mt     0.082 % 	 ..../TexturedRLPrep

		       7.070 Mt    99.918 %         1 x        7.070 Mt        0.000 Mt        2.021 Mt    28.579 % 	 ...../PrepareForRendering

		       2.719 Mt    38.457 %         1 x        2.719 Mt        0.000 Mt        2.719 Mt   100.000 % 	 ....../PrepareMaterials

		       2.330 Mt    32.964 %         1 x        2.330 Mt        0.000 Mt        2.330 Mt   100.000 % 	 ....../PrepareDrawBundle

		       8.387 Mt     0.090 %         1 x        8.387 Mt        0.000 Mt        0.005 Mt     0.058 % 	 ..../DeferredRLPrep

		       8.382 Mt    99.942 %         1 x        8.382 Mt        0.000 Mt        3.763 Mt    44.888 % 	 ...../PrepareForRendering

		       0.022 Mt     0.266 %         1 x        0.022 Mt        0.000 Mt        0.022 Mt   100.000 % 	 ....../PrepareMaterials

		       2.158 Mt    25.739 %         1 x        2.158 Mt        0.000 Mt        2.158 Mt   100.000 % 	 ....../BuildMaterialCache

		       2.440 Mt    29.107 %         1 x        2.440 Mt        0.000 Mt        2.440 Mt   100.000 % 	 ....../PrepareDrawBundle

		      38.120 Mt     0.408 %         1 x       38.120 Mt        0.000 Mt        6.157 Mt    16.152 % 	 ..../RayTracingRLPrep

		       0.575 Mt     1.509 %         1 x        0.575 Mt        0.000 Mt        0.575 Mt   100.000 % 	 ...../PrepareAccelerationStructures

		       0.024 Mt     0.062 %         1 x        0.024 Mt        0.000 Mt        0.024 Mt   100.000 % 	 ...../PrepareCache

		       5.209 Mt    13.664 %         1 x        5.209 Mt        0.000 Mt        5.209 Mt   100.000 % 	 ...../BuildCache

		      12.832 Mt    33.662 %         1 x       12.832 Mt        0.000 Mt        0.005 Mt     0.041 % 	 ...../BuildAccelerationStructures

		      12.827 Mt    99.959 %         1 x       12.827 Mt        0.000 Mt        5.454 Mt    42.523 % 	 ....../AccelerationStructureBuild

		       4.349 Mt    33.908 %         1 x        4.349 Mt        0.000 Mt        4.349 Mt   100.000 % 	 ......./BuildBottomLevelAS

		       3.023 Mt    23.569 %         1 x        3.023 Mt        0.000 Mt        3.023 Mt   100.000 % 	 ......./BuildTopLevelAS

		      13.323 Mt    34.951 %         1 x       13.323 Mt        0.000 Mt       13.323 Mt   100.000 % 	 ...../BuildShaderTables

		      63.643 Mt     0.682 %         1 x       63.643 Mt        0.000 Mt       63.643 Mt   100.000 % 	 ..../GpuSidePrep

		   26022.919 Mt    72.672 %     10733 x        2.425 Mt        3.937 Mt      218.836 Mt     0.841 % 	 ../MainLoop

		     909.767 Mt     3.496 %      1564 x        0.582 Mt        0.310 Mt      908.921 Mt    99.907 % 	 .../Update

		       0.847 Mt     0.093 %         1 x        0.847 Mt        0.000 Mt        0.847 Mt   100.000 % 	 ..../GuiModelApply

		   24894.316 Mt    95.663 %      6674 x        3.730 Mt        3.618 Mt    16967.471 Mt    68.158 % 	 .../Render

		    7926.845 Mt    31.842 %      6674 x        1.188 Mt        1.162 Mt     7894.614 Mt    99.593 % 	 ..../RayTracing

		      32.231 Mt     0.407 %      6674 x        0.005 Mt        0.003 Mt       32.231 Mt   100.000 % 	 ...../RayTracingRLPrep

	WindowThread:

		   35805.093 Mt   100.000 %         1 x    35805.094 Mt    35805.093 Mt    35805.093 Mt   100.000 % 	 /

	GpuThread:

		   10309.377 Mt   100.000 %      6674 x        1.545 Mt        1.470 Mt      187.663 Mt     1.820 % 	 /

		      16.563 Mt     0.161 %         1 x       16.563 Mt        0.000 Mt        0.051 Mt     0.308 % 	 ./ScenePrep

		      16.512 Mt    99.692 %         1 x       16.512 Mt        0.000 Mt        0.006 Mt     0.034 % 	 ../AccelerationStructureBuild

		      15.874 Mt    96.134 %         1 x       15.874 Mt        0.000 Mt       15.874 Mt   100.000 % 	 .../BuildBottomLevelASGpu

		       0.633 Mt     3.832 %         1 x        0.633 Mt        0.000 Mt        0.633 Mt   100.000 % 	 .../BuildTopLevelASGpu

		   10090.141 Mt    97.873 %      6674 x        1.512 Mt        1.467 Mt        5.327 Mt     0.053 % 	 ./RayTracingGpu

		    9661.788 Mt    95.755 %      6674 x        1.448 Mt        1.405 Mt     9661.788 Mt   100.000 % 	 ../RayTracingRLGpu

		     423.027 Mt     4.192 %      6674 x        0.063 Mt        0.062 Mt      423.027 Mt   100.000 % 	 ../ResolveRLGpu

		      15.010 Mt     0.146 %      6674 x        0.002 Mt        0.001 Mt       15.010 Mt   100.000 % 	 ./Present


	============================


