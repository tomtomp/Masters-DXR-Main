Configuration: 
Command line parameters  = E:\programming\FIT-DIP\Masters-DXR-Main-git\build\Measurement\x64\Debug\Measurement.exe --scene Sponza/Sponza.gltf --duplication-cube --profile-duplication --profile-max-duplication 7 --rt-mode --width 1024 --height 768 --profile-output DuplicationSponza768Rt 
Window width             = 1024
Window height            = 768
Window title             = Direct3D App
Back-buffer count        = 3
Fill Triangles           = enabled
Triangles Font Clock-Wise= enabled
Fast Build AS            = disabled
Target FPS               = 60
Target UPS               = 60
Tearing                  = disabled
VSync                    = disabled
GUI rendering            = disabled
Debug layer              = enabled
GPU validation           = disabled
GPU profiling            = enabled
Render mode              = Ray Tracing
Loaded scene file        = Sponza/Sponza.gltf
Using testing scene      = disabled
Duplication              = 7
Duplication in cube      = enabled
Duplication offset       = 10
BLAS duplication         = enabled
Rays per pixel           = 10

ProfilingAggregator: 

ProfilingBuildAggregator: 

FpsAggregator: 


AccelerationStructure: 
Ray tracing acceleration structure building statistics: 
	Bottom Level Acceleration Structure: 
		Maximum [B]: 14623872
		Minimum [B]: 14623872
		Currently Allocated [B]: 5015988096
		Currently Created [num]: 343
		Current Average [B]: 14623872
		Maximum Triangles [num]: 262267
		Minimum Triangles [num]: 262267
		Total Triangles [num]: 89957581
		Maximum Meshes [num]: 103
		Minimum Meshes [num]: 103
		Total Meshes [num]: 35329
	Top Level Acceleration Structure: 
		Maximum [B]: 21952
		Minimum [B]: 21952
		Currently Allocated [B]: 21952
		Total Allocated [B]: 21952
		Total Created [num]: 1
		Average Allocated [B]: 21952
		Maximum Instances [num]: 343
		Minimum Instances [num]: 343
		Total Instances [num]: 343
	Scratch Buffer: 
		Maximum [B]: 11660800
		Minimum [B]: 11660800
		Currently Allocated [B]: 11660800
		Total Allocated [B]: 11660800
		Total Created [num]: 1
		Average Allocated [B]: 11660800
ProfilingStack: 
[PROF] Profiling statistics: 
	Scopes entered : 336463
	Scopes exited : 336462
	Overhead per scope [ticks] : 1006.7
	Threads entered : 3
	Threads exited : 0
	============================
		         Sum[Mt]   Parent[%]  Samples[num]       Avg[Mt]   Last/Curr[Mt]        Self[Mt]     Self[%] 	 Name

	main:

		   28074.950 Mt   100.000 %         1 x    28074.954 Mt    28074.949 Mt        0.753 Mt     0.003 % 	 /

		   28074.344 Mt    99.998 %         1 x    28074.350 Mt    28074.343 Mt     1140.619 Mt     4.063 % 	 ./Main application loop

		       3.007 Mt     0.011 %         1 x        3.007 Mt        0.000 Mt        3.007 Mt   100.000 % 	 ../TexturedRLInitialization

		       6.528 Mt     0.023 %         1 x        6.528 Mt        0.000 Mt        6.528 Mt   100.000 % 	 ../DeferredRLInitialization

		      72.864 Mt     0.260 %         1 x       72.864 Mt        0.000 Mt        2.581 Mt     3.543 % 	 ../RayTracingRLInitialization

		      70.282 Mt    96.457 %         1 x       70.282 Mt        0.000 Mt       70.282 Mt   100.000 % 	 .../PipelineBuild

		       3.290 Mt     0.012 %         1 x        3.290 Mt        0.000 Mt        3.290 Mt   100.000 % 	 ../ResolveRLInitialization

		    9543.909 Mt    33.995 %         1 x     9543.909 Mt        0.000 Mt        8.227 Mt     0.086 % 	 ../Initialization

		    9535.682 Mt    99.914 %         1 x     9535.682 Mt        0.000 Mt        4.749 Mt     0.050 % 	 .../ScenePrep

		    9412.514 Mt    98.708 %         1 x     9412.514 Mt        0.000 Mt      367.150 Mt     3.901 % 	 ..../SceneLoad

		    8228.608 Mt    87.422 %         1 x     8228.608 Mt        0.000 Mt     1143.701 Mt    13.899 % 	 ...../glTF-LoadScene

		    7084.907 Mt    86.101 %        69 x      102.680 Mt        0.000 Mt     7084.907 Mt   100.000 % 	 ....../glTF-LoadImageData

		     515.305 Mt     5.475 %         1 x      515.305 Mt        0.000 Mt      515.305 Mt   100.000 % 	 ...../glTF-CreateScene

		     301.451 Mt     3.203 %         1 x      301.451 Mt        0.000 Mt      301.451 Mt   100.000 % 	 ...../SceneDesc-CreateScene

		      21.268 Mt     0.223 %         1 x       21.268 Mt        0.000 Mt        0.007 Mt     0.031 % 	 ..../TexturedRLPrep

		      21.261 Mt    99.969 %         1 x       21.261 Mt        0.000 Mt        3.600 Mt    16.933 % 	 ...../PrepareForRendering

		       9.511 Mt    44.733 %         1 x        9.511 Mt        0.000 Mt        9.511 Mt   100.000 % 	 ....../PrepareMaterials

		       8.150 Mt    38.333 %         1 x        8.150 Mt        0.000 Mt        8.150 Mt   100.000 % 	 ....../PrepareDrawBundle

		      14.351 Mt     0.151 %         1 x       14.351 Mt        0.000 Mt        0.007 Mt     0.046 % 	 ..../DeferredRLPrep

		      14.345 Mt    99.954 %         1 x       14.345 Mt        0.000 Mt        5.450 Mt    37.993 % 	 ...../PrepareForRendering

		       0.028 Mt     0.197 %         1 x        0.028 Mt        0.000 Mt        0.028 Mt   100.000 % 	 ....../PrepareMaterials

		       2.333 Mt    16.264 %         1 x        2.333 Mt        0.000 Mt        2.333 Mt   100.000 % 	 ....../BuildMaterialCache

		       6.534 Mt    45.547 %         1 x        6.534 Mt        0.000 Mt        6.534 Mt   100.000 % 	 ....../PrepareDrawBundle

		      38.903 Mt     0.408 %         1 x       38.903 Mt        0.000 Mt        9.163 Mt    23.554 % 	 ..../RayTracingRLPrep

		       0.595 Mt     1.528 %         1 x        0.595 Mt        0.000 Mt        0.595 Mt   100.000 % 	 ...../PrepareAccelerationStructures

		       0.026 Mt     0.068 %         1 x        0.026 Mt        0.000 Mt        0.026 Mt   100.000 % 	 ...../PrepareCache

		      10.429 Mt    26.809 %         1 x       10.429 Mt        0.000 Mt       10.429 Mt   100.000 % 	 ...../BuildCache

		      13.839 Mt    35.574 %         1 x       13.839 Mt        0.000 Mt        0.006 Mt     0.046 % 	 ...../BuildAccelerationStructures

		      13.833 Mt    99.954 %         1 x       13.833 Mt        0.000 Mt        9.066 Mt    65.536 % 	 ....../AccelerationStructureBuild

		       3.646 Mt    26.354 %         1 x        3.646 Mt        0.000 Mt        3.646 Mt   100.000 % 	 ......./BuildBottomLevelAS

		       1.122 Mt     8.110 %         1 x        1.122 Mt        0.000 Mt        1.122 Mt   100.000 % 	 ......./BuildTopLevelAS

		       4.850 Mt    12.466 %         1 x        4.850 Mt        0.000 Mt        4.850 Mt   100.000 % 	 ...../BuildShaderTables

		      43.897 Mt     0.460 %         1 x       43.897 Mt        0.000 Mt       43.897 Mt   100.000 % 	 ..../GpuSidePrep

		   17304.126 Mt    61.637 %    332296 x        0.052 Mt      293.120 Mt      774.006 Mt     4.473 % 	 ../MainLoop

		     771.206 Mt     4.457 %      1062 x        0.726 Mt        1.348 Mt      770.198 Mt    99.869 % 	 .../Update

		       1.007 Mt     0.131 %         1 x        1.007 Mt        0.000 Mt        1.007 Mt   100.000 % 	 ..../GuiModelApply

		   15758.915 Mt    91.070 %       320 x       49.247 Mt      260.677 Mt     3977.639 Mt    25.241 % 	 .../Render

		   11781.275 Mt    74.759 %       320 x       36.816 Mt      241.377 Mt     6774.236 Mt    57.500 % 	 ..../RayTracing

		    1694.184 Mt    14.380 %       320 x        5.294 Mt        0.007 Mt        1.060 Mt     0.063 % 	 ...../DeferredRLPrep

		    1693.124 Mt    99.937 %         8 x      211.641 Mt        0.000 Mt       48.610 Mt     2.871 % 	 ....../PrepareForRendering

		       2.274 Mt     0.134 %         8 x        0.284 Mt        0.000 Mt        2.274 Mt   100.000 % 	 ......./PrepareMaterials

		       0.007 Mt     0.000 %         8 x        0.001 Mt        0.000 Mt        0.007 Mt   100.000 % 	 ......./BuildMaterialCache

		    1642.234 Mt    96.994 %         8 x      205.279 Mt        0.000 Mt     1642.234 Mt   100.000 % 	 ......./PrepareDrawBundle

		    3312.856 Mt    28.120 %       320 x       10.353 Mt        0.010 Mt       13.637 Mt     0.412 % 	 ...../RayTracingRLPrep

		     536.429 Mt    16.192 %         8 x       67.054 Mt        0.000 Mt      536.429 Mt   100.000 % 	 ....../PrepareAccelerationStructures

		       2.178 Mt     0.066 %         8 x        0.272 Mt        0.000 Mt        2.178 Mt   100.000 % 	 ....../PrepareCache

		       0.005 Mt     0.000 %         8 x        0.001 Mt        0.000 Mt        0.005 Mt   100.000 % 	 ....../BuildCache

		    1302.258 Mt    39.309 %         8 x      162.782 Mt        0.000 Mt        0.024 Mt     0.002 % 	 ....../BuildAccelerationStructures

		    1302.234 Mt    99.998 %         8 x      162.779 Mt        0.000 Mt       29.628 Mt     2.275 % 	 ......./AccelerationStructureBuild

		    1266.444 Mt    97.252 %         8 x      158.305 Mt        0.000 Mt     1266.444 Mt   100.000 % 	 ......../BuildBottomLevelAS

		       6.162 Mt     0.473 %         8 x        0.770 Mt        0.000 Mt        6.162 Mt   100.000 % 	 ......../BuildTopLevelAS

		    1458.349 Mt    44.021 %         8 x      182.294 Mt        0.000 Mt     1458.349 Mt   100.000 % 	 ....../BuildShaderTables

	WindowThread:

		   28068.819 Mt   100.000 %         1 x    28068.820 Mt    28068.818 Mt    28068.819 Mt   100.000 % 	 /

	GpuThread:

		    3446.472 Mt   100.000 %       320 x       10.770 Mt        8.129 Mt      141.461 Mt     4.105 % 	 /

		       1.648 Mt     0.048 %         1 x        1.648 Mt        0.000 Mt        0.007 Mt     0.406 % 	 ./ScenePrep

		       1.642 Mt    99.594 %         1 x        1.642 Mt        0.000 Mt        0.002 Mt     0.099 % 	 ../AccelerationStructureBuild

		       1.550 Mt    94.433 %         1 x        1.550 Mt        0.000 Mt        1.550 Mt   100.000 % 	 .../BuildBottomLevelASGpu

		       0.090 Mt     5.468 %         1 x        0.090 Mt        0.000 Mt        0.090 Mt   100.000 % 	 .../BuildTopLevelASGpu

		    3302.016 Mt    95.809 %       320 x       10.319 Mt        8.127 Mt        0.639 Mt     0.019 % 	 ./RayTracingGpu

		     435.012 Mt    13.174 %       320 x        1.359 Mt        6.371 Mt      435.012 Mt   100.000 % 	 ../DeferredRLGpu

		    1009.328 Mt    30.567 %       320 x        3.154 Mt        1.642 Mt     1009.328 Mt   100.000 % 	 ../RayTracingRLGpu

		      44.837 Mt     1.358 %       320 x        0.140 Mt        0.112 Mt       44.837 Mt   100.000 % 	 ../ResolveRLGpu

		    1812.201 Mt    54.882 %         8 x      226.525 Mt        0.000 Mt        0.011 Mt     0.001 % 	 ../AccelerationStructureBuild

		    1810.721 Mt    99.918 %         8 x      226.340 Mt        0.000 Mt     1810.721 Mt   100.000 % 	 .../BuildBottomLevelASGpu

		       1.468 Mt     0.081 %         8 x        0.184 Mt        0.000 Mt        1.468 Mt   100.000 % 	 .../BuildTopLevelASGpu

		       1.347 Mt     0.039 %       320 x        0.004 Mt        0.002 Mt        1.347 Mt   100.000 % 	 ./Present


	============================


