Configuration: 
Command line parameters  = E:\programming\FIT-DIP\Masters-DXR-Main-git\build\Measurement\x64\Debug\Measurement.exe --scene Suzanne/Suzanne.gltf --profile-automation --profile-camera-track Suzanne/Suzanne.trk --rt-mode-pure --width 2560 --height 1440 --profile-output TrackSuzanne1440Rp 
Window width             = 2560
Window height            = 1421
Window title             = Direct3D App
Back-buffer count        = 3
Fill Triangles           = enabled
Triangles Font Clock-Wise= enabled
Fast Build AS            = disabled
Target FPS               = 0
Target UPS               = 60
Tearing                  = enabled
VSync                    = disabled
GUI rendering            = disabled
Debug layer              = enabled
GPU validation           = disabled
GPU profiling            = enabled
Render mode              = Ray Tracing
Loaded scene file        = Suzanne/Suzanne.gltf
Using testing scene      = disabled
Duplication              = 1
Duplication in cube      = disabled
Duplication offset       = 10
BLAS duplication         = disabled
Rays per pixel           = 9

ProfilingAggregator: 
Render	,	4.0892	,	3.0032	,	6.8437	,	3.4126	,	2.8575	,	2.6496	,	2.3155	,	2.7932	,	2.3129	,	3.3326	,	4.2738	,	9.1756	,	2.6873	,	2.5713	,	3.9053	,	3.2196	,	10.8162	,	4.3994	,	7.2127	,	7.2175	,	4.9354	,	5.7641	,	2.2143	,	3.5425
Update	,	0.6743	,	0.5668	,	0.7892	,	0.2969	,	0.539	,	0.4952	,	0.2588	,	0.716	,	0.2769	,	0.3234	,	0.7008	,	0.7796	,	0.271	,	0.2878	,	0.5643	,	0.393	,	0.8036	,	0.2926	,	0.5143	,	0.8715	,	0.6281	,	0.2766	,	0.3772	,	0.693
RayTracing	,	1.5393	,	0.7196	,	2.6007	,	1.0314	,	0.8879	,	0.856	,	0.7123	,	1.1184	,	0.7148	,	1.0826	,	1.3647	,	6.9994	,	0.7622	,	0.7618	,	1.2931	,	0.7467	,	1.1657	,	0.7811	,	1.3379	,	2.4305	,	1.6199	,	2.5653	,	0.7094	,	1.457
RayTracingGpu	,	1.37293	,	1.4865	,	1.68576	,	1.44	,	1.0441	,	0.835072	,	0.762048	,	0.752288	,	0.951712	,	1.32282	,	1.34531	,	1.17478	,	0.853056	,	0.898816	,	1.61853	,	1.58285	,	2.08458	,	2.82086	,	4.65072	,	2.81043	,	2.01834	,	1.09888	,	0.797888	,	0.833152
RayTracingRLGpu	,	1.10762	,	1.22314	,	1.42038	,	1.17542	,	0.780256	,	0.571008	,	0.497792	,	0.48672	,	0.685216	,	1.05594	,	1.07507	,	0.79328	,	0.587712	,	0.632576	,	0.95808	,	1.31875	,	1.80819	,	2.55613	,	4.36963	,	2.54499	,	1.75386	,	0.834784	,	0.533536	,	0.56912
ResolveRLGpu	,	0.26448	,	0.26256	,	0.264448	,	0.263904	,	0.263072	,	0.263232	,	0.263552	,	0.264576	,	0.265632	,	0.265984	,	0.269152	,	0.380736	,	0.264096	,	0.265184	,	0.65952	,	0.263424	,	0.275584	,	0.263968	,	0.279872	,	0.264512	,	0.263456	,	0.263296	,	0.263584	,	0.263296
Index	,	0	,	1	,	2	,	3	,	4	,	5	,	6	,	7	,	8	,	9	,	10	,	11	,	12	,	13	,	14	,	15	,	16	,	17	,	18	,	19	,	20	,	21	,	22	,	23

ProfilingBuildAggregator: 
BuildBottomLevelAS	,	0.8694
BuildBottomLevelASGpu	,	0.358304
BuildTopLevelAS	,	2.4826
BuildTopLevelASGpu	,	0.08624
Duplication	,	1
Triangles	,	3936
Meshes	,	1
BottomLevels	,	1
Index	,	0

FpsAggregator: 
FPS	,	231	,	246	,	240	,	233	,	276	,	340	,	371	,	372	,	348	,	292	,	263	,	271	,	321	,	349	,	309	,	256	,	215	,	209	,	139	,	149	,	192	,	243	,	329	,	354
UPS	,	59	,	61	,	59	,	61	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	61	,	60	,	60	,	60	,	60	,	61	,	60	,	60	,	60	,	60	,	60	,	61
FrameTime	,	4.33978	,	4.07531	,	4.17089	,	4.30556	,	3.62632	,	2.94717	,	2.69982	,	2.68905	,	2.87958	,	3.42933	,	3.81254	,	3.69671	,	3.11903	,	2.86763	,	3.23632	,	3.91722	,	4.69265	,	4.80197	,	7.23599	,	6.72308	,	5.21302	,	4.12514	,	3.04362	,	2.83605
GigaRays/s	,	7.54412	,	8.0337	,	7.84961	,	7.60408	,	9.02839	,	11.1089	,	12.1267	,	12.1753	,	11.3696	,	9.54702	,	8.5874	,	8.85647	,	10.4968	,	11.4171	,	10.1164	,	8.35792	,	6.97683	,	6.818	,	4.52458	,	4.86977	,	6.2804	,	7.93666	,	10.7569	,	11.5442
Index	,	0	,	1	,	2	,	3	,	4	,	5	,	6	,	7	,	8	,	9	,	10	,	11	,	12	,	13	,	14	,	15	,	16	,	17	,	18	,	19	,	20	,	21	,	22	,	23


AccelerationStructure: 
Ray tracing acceleration structure building statistics: 
	Bottom Level Acceleration Structure: 
		Maximum [B]: 224128
		Minimum [B]: 224128
		Currently Allocated [B]: 224128
		Currently Created [num]: 1
		Current Average [B]: 224128
		Maximum Triangles [num]: 3936
		Minimum Triangles [num]: 3936
		Total Triangles [num]: 3936
		Maximum Meshes [num]: 1
		Minimum Meshes [num]: 1
		Total Meshes [num]: 1
	Top Level Acceleration Structure: 
		Maximum [B]: 64
		Minimum [B]: 64
		Currently Allocated [B]: 64
		Total Allocated [B]: 64
		Total Created [num]: 1
		Average Allocated [B]: 64
		Maximum Instances [num]: 1
		Minimum Instances [num]: 1
		Total Instances [num]: 1
	Scratch Buffer: 
		Maximum [B]: 34304
		Minimum [B]: 34304
		Currently Allocated [B]: 34304
		Total Allocated [B]: 34304
		Total Created [num]: 1
		Average Allocated [B]: 34304
ProfilingStack: 
[PROF] Profiling statistics: 
	Scopes entered : 57832
	Scopes exited : 57831
	Overhead per scope [ticks] : 1016.5
	Threads entered : 3
	Threads exited : 0
	============================
		         Sum[Mt]   Parent[%]  Samples[num]       Avg[Mt]   Last/Curr[Mt]        Self[Mt]     Self[%] 	 Name

	main:

		   24702.295 Mt   100.000 %         1 x    24702.296 Mt    24702.294 Mt        0.680 Mt     0.003 % 	 /

		   24701.658 Mt    99.997 %         1 x    24701.660 Mt    24701.658 Mt      371.027 Mt     1.502 % 	 ./Main application loop

		       2.869 Mt     0.012 %         1 x        2.869 Mt        0.000 Mt        2.869 Mt   100.000 % 	 ../TexturedRLInitialization

		       6.728 Mt     0.027 %         1 x        6.728 Mt        0.000 Mt        6.728 Mt   100.000 % 	 ../DeferredRLInitialization

		      57.156 Mt     0.231 %         1 x       57.156 Mt        0.000 Mt        2.368 Mt     4.144 % 	 ../RayTracingRLInitialization

		      54.788 Mt    95.856 %         1 x       54.788 Mt        0.000 Mt       54.788 Mt   100.000 % 	 .../PipelineBuild

		       3.316 Mt     0.013 %         1 x        3.316 Mt        0.000 Mt        3.316 Mt   100.000 % 	 ../ResolveRLInitialization

		     240.086 Mt     0.972 %         1 x      240.086 Mt        0.000 Mt        2.839 Mt     1.182 % 	 ../Initialization

		     237.247 Mt    98.818 %         1 x      237.247 Mt        0.000 Mt        1.314 Mt     0.554 % 	 .../ScenePrep

		     205.303 Mt    86.535 %         1 x      205.303 Mt        0.000 Mt       18.945 Mt     9.228 % 	 ..../SceneLoad

		     164.245 Mt    80.001 %         1 x      164.245 Mt        0.000 Mt       19.285 Mt    11.741 % 	 ...../glTF-LoadScene

		     144.960 Mt    88.259 %         2 x       72.480 Mt        0.000 Mt      144.960 Mt   100.000 % 	 ....../glTF-LoadImageData

		      14.361 Mt     6.995 %         1 x       14.361 Mt        0.000 Mt       14.361 Mt   100.000 % 	 ...../glTF-CreateScene

		       7.751 Mt     3.776 %         1 x        7.751 Mt        0.000 Mt        7.751 Mt   100.000 % 	 ...../SceneDesc-CreateScene

		       1.028 Mt     0.433 %         1 x        1.028 Mt        0.000 Mt        0.005 Mt     0.448 % 	 ..../TexturedRLPrep

		       1.023 Mt    99.552 %         1 x        1.023 Mt        0.000 Mt        0.335 Mt    32.740 % 	 ...../PrepareForRendering

		       0.402 Mt    39.310 %         1 x        0.402 Mt        0.000 Mt        0.402 Mt   100.000 % 	 ....../PrepareMaterials

		       0.286 Mt    27.950 %         1 x        0.286 Mt        0.000 Mt        0.286 Mt   100.000 % 	 ....../PrepareDrawBundle

		       4.038 Mt     1.702 %         1 x        4.038 Mt        0.000 Mt        0.006 Mt     0.149 % 	 ..../DeferredRLPrep

		       4.032 Mt    99.851 %         1 x        4.032 Mt        0.000 Mt        1.837 Mt    45.554 % 	 ...../PrepareForRendering

		       0.015 Mt     0.372 %         1 x        0.015 Mt        0.000 Mt        0.015 Mt   100.000 % 	 ....../PrepareMaterials

		       1.327 Mt    32.918 %         1 x        1.327 Mt        0.000 Mt        1.327 Mt   100.000 % 	 ....../BuildMaterialCache

		       0.853 Mt    21.156 %         1 x        0.853 Mt        0.000 Mt        0.853 Mt   100.000 % 	 ....../PrepareDrawBundle

		      13.061 Mt     5.505 %         1 x       13.061 Mt        0.000 Mt        4.755 Mt    36.403 % 	 ..../RayTracingRLPrep

		       0.736 Mt     5.633 %         1 x        0.736 Mt        0.000 Mt        0.736 Mt   100.000 % 	 ...../PrepareAccelerationStructures

		       0.072 Mt     0.554 %         1 x        0.072 Mt        0.000 Mt        0.072 Mt   100.000 % 	 ...../PrepareCache

		       0.398 Mt     3.050 %         1 x        0.398 Mt        0.000 Mt        0.398 Mt   100.000 % 	 ...../BuildCache

		       5.168 Mt    39.566 %         1 x        5.168 Mt        0.000 Mt        0.005 Mt     0.095 % 	 ...../BuildAccelerationStructures

		       5.163 Mt    99.905 %         1 x        5.163 Mt        0.000 Mt        1.811 Mt    35.071 % 	 ....../AccelerationStructureBuild

		       0.869 Mt    16.840 %         1 x        0.869 Mt        0.000 Mt        0.869 Mt   100.000 % 	 ......./BuildBottomLevelAS

		       2.483 Mt    48.088 %         1 x        2.483 Mt        0.000 Mt        2.483 Mt   100.000 % 	 ......./BuildTopLevelAS

		       1.932 Mt    14.794 %         1 x        1.932 Mt        0.000 Mt        1.932 Mt   100.000 % 	 ...../BuildShaderTables

		      12.504 Mt     5.270 %         1 x       12.504 Mt        0.000 Mt       12.504 Mt   100.000 % 	 ..../GpuSidePrep

		   24020.476 Mt    97.242 %     10500 x        2.288 Mt        2.704 Mt      289.695 Mt     1.206 % 	 ../MainLoop

		     879.166 Mt     3.660 %      1444 x        0.609 Mt        0.299 Mt      876.476 Mt    99.694 % 	 .../Update

		       2.690 Mt     0.306 %         1 x        2.690 Mt        0.000 Mt        2.690 Mt   100.000 % 	 ..../GuiModelApply

		   22851.616 Mt    95.134 %      6550 x        3.489 Mt        2.397 Mt    15696.755 Mt    68.690 % 	 .../Render

		    7154.861 Mt    31.310 %      6550 x        1.092 Mt        0.762 Mt     7118.246 Mt    99.488 % 	 ..../RayTracing

		      36.615 Mt     0.512 %      6550 x        0.006 Mt        0.003 Mt       36.615 Mt   100.000 % 	 ...../RayTracingRLPrep

	WindowThread:

		   24697.766 Mt   100.000 %         1 x    24697.766 Mt    24697.765 Mt    24697.766 Mt   100.000 % 	 /

	GpuThread:

		    8848.796 Mt   100.000 %      6550 x        1.351 Mt        0.836 Mt      163.431 Mt     1.847 % 	 /

		       0.453 Mt     0.005 %         1 x        0.453 Mt        0.000 Mt        0.007 Mt     1.520 % 	 ./ScenePrep

		       0.446 Mt    98.480 %         1 x        0.446 Mt        0.000 Mt        0.001 Mt     0.287 % 	 ../AccelerationStructureBuild

		       0.358 Mt    80.369 %         1 x        0.358 Mt        0.000 Mt        0.358 Mt   100.000 % 	 .../BuildBottomLevelASGpu

		       0.086 Mt    19.344 %         1 x        0.086 Mt        0.000 Mt        0.086 Mt   100.000 % 	 .../BuildTopLevelASGpu

		    8669.297 Mt    97.971 %      6550 x        1.324 Mt        0.834 Mt        6.162 Mt     0.071 % 	 ./RayTracingGpu

		    6896.222 Mt    79.548 %      6550 x        1.053 Mt        0.569 Mt     6896.222 Mt   100.000 % 	 ../RayTracingRLGpu

		    1766.913 Mt    20.381 %      6550 x        0.270 Mt        0.264 Mt     1766.913 Mt   100.000 % 	 ../ResolveRLGpu

		      15.615 Mt     0.176 %      6550 x        0.002 Mt        0.002 Mt       15.615 Mt   100.000 % 	 ./Present


	============================


