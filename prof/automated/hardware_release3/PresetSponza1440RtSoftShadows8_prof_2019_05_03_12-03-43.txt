Configuration: 
Command line parameters  = E:\programming\FIT-DIP\Masters-DXR-Main-git\build\Measurement\x64\Release\Measurement.exe --scene Sponza/Sponza.gltf --profile-automation --profile-camera-track Sponza/SponzaPreset.trk --rt-mode --width 2560 --height 1440 --profile-output PresetSponza1440RtSoftShadows8 --quality-preset SoftShadows8 
Build type               = Release
Device name              = NVIDIA GeForce RTX 2080 Ti
Device memory MB         = 11049
Ray Tracing backend      = Hardware
Window width             = 2560
Window height            = 1421
Window title             = Direct3D App
Back-buffer count        = 3
Fill Triangles           = enabled
Triangles Font Clock-Wise= enabled
Fast Build AS            = disabled
Target FPS               = 0
Target UPS               = 60
Tearing                  = enabled
VSync                    = disabled
GUI rendering            = disabled
Debug layer              = disabled
GPU validation           = disabled
GPU profiling            = enabled
Render mode              = Ray Tracing
Loaded scene file        = Sponza/Sponza.gltf
Using testing scene      = disabled
Duplication              = 1
Duplication in cube      = disabled
Duplication offset       = 10
BLAS duplication         = disabled
Rays per pixel           = 8
Quality preset           = SoftShadows8
Rasterization Quality    : 
  AO                     = disabled
  AO kernel size         = 1
  Shadows                = enabled
  Shadow map resolution  = 8192
  Shadow PCF kernel size = 8
  Reflections            = disabled
Ray Tracing Quality      : 
  AO                     = disabled
  AO rays                = 0
  AO kernel size         = 8
  Shadows                = enabled
  Shadow rays            = 8
  Shadow kernel size     = 8
  Reflections            = disabled

ProfilingAggregator: 
Render	,	14.0514	,	13.1639	,	13.3329	,	12.0462	,	11.3738	,	12.3909	,	12.8977	,	13.0408	,	12.2374	,	12.2658	,	11.6194	,	13.0069	,	13.1864	,	13.5127	,	12.5356	,	11.9405	,	12.2869	,	12.6366	,	11.1621	,	13.5765	,	14.5455	,	14.7215	,	14.634
Update	,	0.086	,	0.0875	,	0.0908	,	0.0875	,	0.0895	,	0.0857	,	0.0867	,	0.0848	,	0.0862	,	0.0839	,	0.0872	,	0.0799	,	0.0796	,	0.0831	,	0.083	,	0.085	,	0.0821	,	0.0846	,	0.0794	,	0.0851	,	0.0817	,	0.0826	,	0.0833
RayTracing	,	0.1911	,	0.2097	,	0.2138	,	0.192	,	0.2063	,	0.2003	,	0.2069	,	0.2035	,	0.189	,	0.1877	,	0.1923	,	0.1839	,	0.2012	,	0.1795	,	0.1858	,	0.1825	,	0.1853	,	0.1839	,	0.2095	,	0.2167	,	0.1961	,	0.1902	,	0.2167
RayTracingGpu	,	12.998	,	12.2723	,	12.2153	,	11.1157	,	10.4367	,	11.3472	,	11.5356	,	12.081	,	11.4174	,	10.9757	,	10.7752	,	12.0553	,	12.1064	,	12.6756	,	11.31	,	11.1723	,	11.3672	,	11.6727	,	10.2386	,	12.6765	,	13.5032	,	13.7271	,	13.5035
DeferredRLGpu	,	2.74954	,	2.52032	,	2.17443	,	1.46918	,	1.35674	,	1.58586	,	1.99229	,	2.6119	,	2.05747	,	1.51078	,	1.35635	,	1.79021	,	2.20419	,	2.51408	,	2.14179	,	1.85878	,	1.9727	,	1.86934	,	1.45242	,	2.96707	,	4.10262	,	3.88672	,	3.88374
RayTracingRLGpu	,	5.01946	,	4.6881	,	4.84214	,	4.30256	,	3.98413	,	4.22707	,	4.2527	,	4.53885	,	4.40874	,	4.3655	,	4.41446	,	5.05069	,	4.56125	,	5.0976	,	3.95443	,	3.92848	,	4.18035	,	4.17974	,	3.67382	,	4.45766	,	4.26378	,	4.54758	,	4.20582
ResolveRLGpu	,	5.22726	,	5.06038	,	5.19696	,	5.34099	,	5.09184	,	5.53107	,	5.28902	,	4.92803	,	4.94621	,	5.09635	,	5.00195	,	5.212	,	5.33741	,	5.06266	,	5.21206	,	5.38301	,	5.2111	,	5.62154	,	5.10934	,	5.24672	,	5.13443	,	5.29072	,	5.41091
Index	,	0	,	1	,	2	,	3	,	4	,	5	,	6	,	7	,	8	,	9	,	10	,	11	,	12	,	13	,	14	,	15	,	16	,	17	,	18	,	19	,	20	,	21	,	22

ProfilingBuildAggregator: 
BuildBottomLevelAS	,	1.7346
BuildBottomLevelASGpu	,	2.56301
BuildTopLevelAS	,	0.8634
BuildTopLevelASGpu	,	0.089472
Duplication	,	1
Triangles	,	262267
Meshes	,	103
BottomLevels	,	1
TopLevelSize	,	64
BottomLevelsSize	,	18390656
Index	,	0

FpsAggregator: 
FPS	,	49	,	69	,	75	,	78	,	86	,	82	,	81	,	75	,	76	,	80	,	81	,	80	,	78	,	75	,	76	,	85	,	79	,	78	,	83	,	84	,	69	,	68	,	68
UPS	,	59	,	61	,	60	,	60	,	61	,	60	,	60	,	61	,	60	,	61	,	60	,	61	,	60	,	61	,	61	,	60	,	60	,	61	,	61	,	60	,	60	,	61	,	61
FrameTime	,	20.5994	,	14.584	,	13.4691	,	12.824	,	11.7468	,	12.2765	,	12.3559	,	13.4392	,	13.1967	,	12.6103	,	12.4379	,	12.6592	,	12.9748	,	13.4654	,	13.2975	,	11.8417	,	12.6984	,	12.944	,	12.1671	,	12.0617	,	14.5724	,	14.8843	,	14.8933
GigaRays/s	,	1.41276	,	1.99548	,	2.16066	,	2.26934	,	2.47744	,	2.37055	,	2.35532	,	2.16547	,	2.20525	,	2.3078	,	2.33979	,	2.29888	,	2.24297	,	2.16125	,	2.18854	,	2.45759	,	2.29179	,	2.24831	,	2.39187	,	2.41277	,	1.99706	,	1.95522	,	1.95404
Index	,	0	,	1	,	2	,	3	,	4	,	5	,	6	,	7	,	8	,	9	,	10	,	11	,	12	,	13	,	14	,	15	,	16	,	17	,	18	,	19	,	20	,	21	,	22


AccelerationStructure: 
Ray tracing acceleration structure building statistics: 
	Bottom Level Acceleration Structure: 
		Maximum [B]: 18390656
		Minimum [B]: 18390656
		Currently Allocated [B]: 18390656
		Currently Created [num]: 1
		Current Average [B]: 18390656
		Maximum Triangles [num]: 262267
		Minimum Triangles [num]: 262267
		Total Triangles [num]: 262267
		Maximum Meshes [num]: 103
		Minimum Meshes [num]: 103
		Total Meshes [num]: 103
	Top Level Acceleration Structure: 
		Maximum [B]: 64
		Minimum [B]: 64
		Currently Allocated [B]: 64
		Total Allocated [B]: 64
		Total Created [num]: 1
		Average Allocated [B]: 64
		Maximum Instances [num]: 1
		Minimum Instances [num]: 1
		Total Instances [num]: 1
	Scratch Buffer: 
		Maximum [B]: 14995072
		Minimum [B]: 14995072
		Currently Allocated [B]: 14995072
		Total Allocated [B]: 14995072
		Total Created [num]: 1
		Average Allocated [B]: 14995072
ProfilingStack: 
[PROF] Profiling statistics: 
	Scopes entered : 66014
	Scopes exited : 66013
	Overhead per scope [ticks] : 100.6
	Threads entered : 3
	Threads exited : 0
	============================
		         Sum[Mt]   Parent[%]  Samples[num]       Avg[Mt]   Last/Curr[Mt]        Self[Mt]     Self[%] 	 Name

	main:

		   26229.338 Mt   100.000 %         1 x    26229.339 Mt    26229.337 Mt        0.437 Mt     0.002 % 	 /

		   26228.933 Mt    99.998 %         1 x    26228.935 Mt    26228.933 Mt      339.053 Mt     1.293 % 	 ./Main application loop

		       1.407 Mt     0.005 %         1 x        1.407 Mt        0.000 Mt        1.407 Mt   100.000 % 	 ../ShadowMapRLInitialization

		       3.983 Mt     0.015 %         1 x        3.983 Mt        0.000 Mt        3.983 Mt   100.000 % 	 ../ResolveRLInitialization

		   23222.147 Mt    88.536 %     41683 x        0.557 Mt       36.088 Mt      478.697 Mt     2.061 % 	 ../MainLoop

		     220.888 Mt     0.951 %      1391 x        0.159 Mt        0.087 Mt      220.261 Mt    99.716 % 	 .../Update

		       0.628 Mt     0.284 %         1 x        0.628 Mt        0.000 Mt        0.628 Mt   100.000 % 	 ..../GuiModelApply

		   22522.562 Mt    96.987 %      1756 x       12.826 Mt       15.099 Mt      233.718 Mt     1.038 % 	 .../Render

		     357.373 Mt     1.587 %      1756 x        0.204 Mt        0.274 Mt       13.238 Mt     3.704 % 	 ..../RayTracing

		     149.020 Mt    41.699 %      1756 x        0.085 Mt        0.128 Mt      147.624 Mt    99.063 % 	 ...../Deferred

		       1.396 Mt     0.937 %      1756 x        0.001 Mt        0.001 Mt        1.396 Mt   100.000 % 	 ....../DeferredRLPrep

		     135.178 Mt    37.826 %      1756 x        0.077 Mt        0.097 Mt      134.131 Mt    99.225 % 	 ...../RayCasting

		       1.048 Mt     0.775 %      1756 x        0.001 Mt        0.001 Mt        1.048 Mt   100.000 % 	 ....../RayTracingRLPrep

		      59.936 Mt    16.771 %      1756 x        0.034 Mt        0.040 Mt       59.936 Mt   100.000 % 	 ...../Resolve

		   21931.471 Mt    97.376 %      1756 x       12.489 Mt       14.640 Mt    21931.471 Mt   100.000 % 	 ..../Present

		       3.012 Mt     0.011 %         1 x        3.012 Mt        0.000 Mt        3.012 Mt   100.000 % 	 ../TexturedRLInitialization

		       3.675 Mt     0.014 %         1 x        3.675 Mt        0.000 Mt        3.675 Mt   100.000 % 	 ../AmbientOcclusionRLInitialization

		       4.386 Mt     0.017 %         1 x        4.386 Mt        0.000 Mt        4.386 Mt   100.000 % 	 ../RasterResolveRLInitialization

		       4.642 Mt     0.018 %         1 x        4.642 Mt        0.000 Mt        4.642 Mt   100.000 % 	 ../DeferredRLInitialization

		      44.151 Mt     0.168 %         1 x       44.151 Mt        0.000 Mt        1.749 Mt     3.962 % 	 ../RayTracingRLInitialization

		      42.402 Mt    96.038 %         1 x       42.402 Mt        0.000 Mt       42.402 Mt   100.000 % 	 .../PipelineBuild

		    2602.476 Mt     9.922 %         1 x     2602.476 Mt        0.000 Mt        0.406 Mt     0.016 % 	 ../Initialization

		    2602.070 Mt    99.984 %         1 x     2602.070 Mt        0.000 Mt        2.561 Mt     0.098 % 	 .../ScenePrep

		    2528.624 Mt    97.177 %         1 x     2528.624 Mt        0.000 Mt      228.707 Mt     9.045 % 	 ..../SceneLoad

		    1987.744 Mt    78.610 %         1 x     1987.744 Mt        0.000 Mt      581.065 Mt    29.232 % 	 ...../glTF-LoadScene

		    1406.679 Mt    70.768 %        69 x       20.387 Mt        0.000 Mt     1406.679 Mt   100.000 % 	 ....../glTF-LoadImageData

		     252.779 Mt     9.997 %         1 x      252.779 Mt        0.000 Mt      252.779 Mt   100.000 % 	 ...../glTF-CreateScene

		      59.393 Mt     2.349 %         1 x       59.393 Mt        0.000 Mt       59.393 Mt   100.000 % 	 ...../SceneDesc-CreateScene

		       1.105 Mt     0.042 %         1 x        1.105 Mt        0.000 Mt        0.001 Mt     0.081 % 	 ..../ShadowMapRLPrep

		       1.104 Mt    99.919 %         1 x        1.104 Mt        0.000 Mt        1.066 Mt    96.577 % 	 ...../PrepareForRendering

		       0.038 Mt     3.423 %         1 x        0.038 Mt        0.000 Mt        0.038 Mt   100.000 % 	 ....../PrepareDrawBundle

		       3.864 Mt     0.148 %         1 x        3.864 Mt        0.000 Mt        0.000 Mt     0.008 % 	 ..../TexturedRLPrep

		       3.863 Mt    99.992 %         1 x        3.863 Mt        0.000 Mt        3.823 Mt    98.957 % 	 ...../PrepareForRendering

		       0.002 Mt     0.047 %         1 x        0.002 Mt        0.000 Mt        0.002 Mt   100.000 % 	 ....../PrepareMaterials

		       0.038 Mt     0.997 %         1 x        0.038 Mt        0.000 Mt        0.038 Mt   100.000 % 	 ....../PrepareDrawBundle

		       4.123 Mt     0.158 %         1 x        4.123 Mt        0.000 Mt        0.000 Mt     0.010 % 	 ..../DeferredRLPrep

		       4.122 Mt    99.990 %         1 x        4.122 Mt        0.000 Mt        3.677 Mt    89.205 % 	 ...../PrepareForRendering

		       0.001 Mt     0.019 %         1 x        0.001 Mt        0.000 Mt        0.001 Mt   100.000 % 	 ....../PrepareMaterials

		       0.388 Mt     9.415 %         1 x        0.388 Mt        0.000 Mt        0.388 Mt   100.000 % 	 ....../BuildMaterialCache

		       0.056 Mt     1.361 %         1 x        0.056 Mt        0.000 Mt        0.056 Mt   100.000 % 	 ....../PrepareDrawBundle

		      27.999 Mt     1.076 %         1 x       27.999 Mt        0.000 Mt        2.816 Mt    10.058 % 	 ..../RayTracingRLPrep

		       0.064 Mt     0.229 %         1 x        0.064 Mt        0.000 Mt        0.064 Mt   100.000 % 	 ...../PrepareAccelerationStructures

		       0.001 Mt     0.004 %         1 x        0.001 Mt        0.000 Mt        0.001 Mt   100.000 % 	 ...../PrepareCache

		      12.556 Mt    44.845 %         1 x       12.556 Mt        0.000 Mt       12.556 Mt   100.000 % 	 ...../PipelineBuild

		       0.676 Mt     2.414 %         1 x        0.676 Mt        0.000 Mt        0.676 Mt   100.000 % 	 ...../BuildCache

		       6.024 Mt    21.514 %         1 x        6.024 Mt        0.000 Mt        0.001 Mt     0.010 % 	 ...../BuildAccelerationStructures

		       6.023 Mt    99.990 %         1 x        6.023 Mt        0.000 Mt        3.425 Mt    56.867 % 	 ....../AccelerationStructureBuild

		       1.735 Mt    28.798 %         1 x        1.735 Mt        0.000 Mt        1.735 Mt   100.000 % 	 ......./BuildBottomLevelAS

		       0.863 Mt    14.334 %         1 x        0.863 Mt        0.000 Mt        0.863 Mt   100.000 % 	 ......./BuildTopLevelAS

		       5.862 Mt    20.935 %         1 x        5.862 Mt        0.000 Mt        5.862 Mt   100.000 % 	 ...../BuildShaderTables

		      33.795 Mt     1.299 %         1 x       33.795 Mt        0.000 Mt       33.795 Mt   100.000 % 	 ..../GpuSidePrep

	WindowThread:

		   26226.076 Mt   100.000 %         1 x    26226.076 Mt    26226.075 Mt    26226.076 Mt   100.000 % 	 /

	GpuThread:

		   20988.245 Mt   100.000 %      1756 x       11.952 Mt       13.826 Mt      159.452 Mt     0.760 % 	 /

		       2.662 Mt     0.013 %         1 x        2.662 Mt        0.000 Mt        0.008 Mt     0.304 % 	 ./ScenePrep

		       2.654 Mt    99.696 %         1 x        2.654 Mt        0.000 Mt        0.001 Mt     0.053 % 	 ../AccelerationStructureBuild

		       2.563 Mt    96.576 %         1 x        2.563 Mt        0.000 Mt        2.563 Mt   100.000 % 	 .../BuildBottomLevelASGpu

		       0.089 Mt     3.371 %         1 x        0.089 Mt        0.000 Mt        0.089 Mt   100.000 % 	 .../BuildTopLevelASGpu

		   20722.753 Mt    98.735 %      1756 x       11.801 Mt       13.483 Mt        4.515 Mt     0.022 % 	 ./RayTracingGpu

		    3810.434 Mt    18.388 %      1756 x        2.170 Mt        3.887 Mt     3810.434 Mt   100.000 % 	 ../DeferredRLGpu

		    7739.800 Mt    37.349 %      1756 x        4.408 Mt        4.419 Mt     7739.800 Mt   100.000 % 	 ../RayTracingRLGpu

		    9168.004 Mt    44.241 %      1756 x        5.221 Mt        5.175 Mt     9168.004 Mt   100.000 % 	 ../ResolveRLGpu

		     103.379 Mt     0.493 %      1756 x        0.059 Mt        0.343 Mt      103.379 Mt   100.000 % 	 ./Present


	============================


