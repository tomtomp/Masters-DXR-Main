Configuration: 
Command line parameters  = E:\programming\FIT-DIP\Masters-DXR-Main-git\build\Measurement\x64\Release\Measurement.exe --scene Cube/Cube.gltf --profile-automation --profile-camera-track Cube/Cube.trk --rt-mode --width 2560 --height 1440 --profile-output PresetCube1440RtSoftShadows --quality-preset SoftShadows 
Build type               = Release
Device name              = NVIDIA GeForce RTX 2080 Ti
Device memory MB         = 11049
Ray Tracing backend      = Hardware
Window width             = 2560
Window height            = 1421
Window title             = Direct3D App
Back-buffer count        = 3
Fill Triangles           = enabled
Triangles Font Clock-Wise= enabled
Fast Build AS            = disabled
Target FPS               = 0
Target UPS               = 60
Tearing                  = enabled
VSync                    = disabled
GUI rendering            = disabled
Debug layer              = disabled
GPU validation           = disabled
GPU profiling            = enabled
Render mode              = Ray Tracing
Loaded scene file        = Cube/Cube.gltf
Using testing scene      = disabled
Duplication              = 1
Duplication in cube      = disabled
Duplication offset       = 10
BLAS duplication         = disabled
Rays per pixel           = 4
Quality preset           = SoftShadows
Rasterization Quality    : 
  AO                     = disabled
  AO kernel size         = 1
  Shadows                = enabled
  Shadow map resolution  = 4096
  Shadow PCF kernel size = 4
  Reflections            = disabled
Ray Tracing Quality      : 
  AO                     = disabled
  AO rays                = 0
  AO kernel size         = 4
  Shadows                = enabled
  Shadow rays            = 4
  Shadow kernel size     = 4
  Reflections            = disabled

ProfilingAggregator: 
Render	,	2.8546	,	3.1571	,	3.8211	,	3.6012	,	3.3289	,	3.5722	,	3.3431	,	3.4994	,	3.5103	,	3.2174	,	3.3113	,	3.5953	,	3.1486	,	3.3627	,	3.4988	,	3.6094	,	3.7227	,	3.8307	,	3.9494	,	3.921	,	3.6725	,	3.2274	,	3.0356	,	2.8266	,	2.6735	,	2.9789	,	2.7398	,	2.7043	,	2.3789
Update	,	0.0835	,	0.0862	,	0.0889	,	0.0845	,	0.0896	,	0.0882	,	0.0801	,	0.0919	,	0.0406	,	0.0333	,	0.0835	,	0.0885	,	0.0783	,	0.0827	,	0.0807	,	0.0875	,	0.0857	,	0.0841	,	0.0833	,	0.086	,	0.088	,	0.0856	,	0.0857	,	0.0853	,	0.0779	,	0.0831	,	0.0826	,	0.0861	,	0.0774
RayTracing	,	0.1792	,	0.1826	,	0.1887	,	0.176	,	0.1823	,	0.1828	,	0.1754	,	0.1654	,	0.1453	,	0.1033	,	0.2271	,	0.1625	,	0.1656	,	0.1806	,	0.1619	,	0.1728	,	0.1793	,	0.171	,	0.1642	,	0.1697	,	0.1732	,	0.1754	,	0.1698	,	0.1712	,	0.1725	,	0.1702	,	0.1689	,	0.1688	,	0.1679
RayTracingGpu	,	2.00301	,	2.25837	,	2.30912	,	2.57584	,	2.4209	,	2.64394	,	2.53094	,	2.72509	,	2.93571	,	2.7984	,	2.59782	,	2.95296	,	2.40371	,	2.39574	,	2.75757	,	2.63424	,	2.87747	,	3.12442	,	3.18323	,	3.1521	,	2.88515	,	2.41568	,	2.23418	,	2.02531	,	1.89491	,	1.88813	,	1.98762	,	1.772	,	1.5471
DeferredRLGpu	,	0.181248	,	0.239904	,	0.25056	,	0.261568	,	0.275616	,	0.334016	,	0.309024	,	0.351712	,	0.411296	,	0.369696	,	0.319648	,	0.347296	,	0.277824	,	0.245152	,	0.28512	,	0.33392	,	0.391776	,	0.463168	,	0.484032	,	0.484096	,	0.408352	,	0.285024	,	0.233088	,	0.182112	,	0.15088	,	0.117952	,	0.089184	,	0.074976	,	0.06384
RayTracingRLGpu	,	0.377184	,	0.490016	,	0.515168	,	0.526144	,	0.552288	,	0.67488	,	0.621728	,	0.715072	,	0.83072	,	0.750144	,	0.640992	,	0.859328	,	0.56032	,	0.507872	,	0.576768	,	0.672896	,	0.794976	,	0.9328	,	0.972288	,	0.974208	,	0.832416	,	0.593088	,	0.494048	,	0.39072	,	0.329408	,	0.26656	,	0.216832	,	0.183264	,	0.165472
ResolveRLGpu	,	1.44262	,	1.52528	,	1.54106	,	1.7865	,	1.59098	,	1.632	,	1.59715	,	1.65622	,	1.6919	,	1.6768	,	1.6353	,	1.74458	,	1.5639	,	1.63571	,	1.89414	,	1.62557	,	1.68656	,	1.72646	,	1.72378	,	1.69181	,	1.64269	,	1.53482	,	1.50515	,	1.45056	,	1.41184	,	1.50227	,	1.67984	,	1.51088	,	1.3159
Index	,	0	,	1	,	2	,	3	,	4	,	5	,	6	,	7	,	8	,	9	,	10	,	11	,	12	,	13	,	14	,	15	,	16	,	17	,	18	,	19	,	20	,	21	,	22	,	23	,	24	,	25	,	26	,	27	,	28

ProfilingBuildAggregator: 
BuildBottomLevelAS	,	1.4562
BuildBottomLevelASGpu	,	0.174208
BuildTopLevelAS	,	0.938
BuildTopLevelASGpu	,	0.084
Duplication	,	1
Triangles	,	12
Meshes	,	1
BottomLevels	,	1
TopLevelSize	,	64
BottomLevelsSize	,	4480
Index	,	0

FpsAggregator: 
FPS	,	265	,	306	,	296	,	301	,	294	,	278	,	273	,	278	,	269	,	274	,	293	,	261	,	275	,	305	,	294	,	282	,	262	,	243	,	235	,	229	,	232	,	279	,	298	,	328	,	341	,	355	,	376	,	391	,	409
UPS	,	59	,	61	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	61	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	61	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	61	,	60	,	60
FrameTime	,	3.7835	,	3.27003	,	3.38078	,	3.32319	,	3.41119	,	3.6071	,	3.66851	,	3.60844	,	3.72486	,	3.65251	,	3.42362	,	3.84202	,	3.64042	,	3.28897	,	3.40222	,	3.55781	,	3.8273	,	4.12777	,	4.26994	,	4.36915	,	4.31219	,	3.5952	,	3.364	,	3.0511	,	2.93723	,	2.82406	,	2.663	,	2.56133	,	2.44939
GigaRays/s	,	3.84592	,	4.44982	,	4.30405	,	4.37864	,	4.26568	,	4.03399	,	3.96647	,	4.0325	,	3.90647	,	3.98385	,	4.25019	,	3.78734	,	3.99708	,	4.42419	,	4.27693	,	4.08989	,	3.80191	,	3.52516	,	3.40778	,	3.3304	,	3.3744	,	4.04736	,	4.32552	,	4.76911	,	4.954	,	5.15252	,	5.46415	,	5.68106	,	5.94068
Index	,	0	,	1	,	2	,	3	,	4	,	5	,	6	,	7	,	8	,	9	,	10	,	11	,	12	,	13	,	14	,	15	,	16	,	17	,	18	,	19	,	20	,	21	,	22	,	23	,	24	,	25	,	26	,	27	,	28


AccelerationStructure: 
Ray tracing acceleration structure building statistics: 
	Bottom Level Acceleration Structure: 
		Maximum [B]: 4480
		Minimum [B]: 4480
		Currently Allocated [B]: 4480
		Currently Created [num]: 1
		Current Average [B]: 4480
		Maximum Triangles [num]: 12
		Minimum Triangles [num]: 12
		Total Triangles [num]: 12
		Maximum Meshes [num]: 1
		Minimum Meshes [num]: 1
		Total Meshes [num]: 1
	Top Level Acceleration Structure: 
		Maximum [B]: 64
		Minimum [B]: 64
		Currently Allocated [B]: 64
		Total Allocated [B]: 64
		Total Created [num]: 1
		Average Allocated [B]: 64
		Maximum Instances [num]: 1
		Minimum Instances [num]: 1
		Total Instances [num]: 1
	Scratch Buffer: 
		Maximum [B]: 896
		Minimum [B]: 896
		Currently Allocated [B]: 896
		Total Allocated [B]: 896
		Total Created [num]: 1
		Average Allocated [B]: 896
ProfilingStack: 
[PROF] Profiling statistics: 
	Scopes entered : 194589
	Scopes exited : 194588
	Overhead per scope [ticks] : 103.7
	Threads entered : 3
	Threads exited : 0
	============================
		         Sum[Mt]   Parent[%]  Samples[num]       Avg[Mt]   Last/Curr[Mt]        Self[Mt]     Self[%] 	 Name

	main:

		   29597.657 Mt   100.000 %         1 x    29597.658 Mt    29597.657 Mt        0.424 Mt     0.001 % 	 /

		   29597.265 Mt    99.999 %         1 x    29597.266 Mt    29597.265 Mt      278.332 Mt     0.940 % 	 ./Main application loop

		       3.939 Mt     0.013 %         1 x        3.939 Mt        0.000 Mt        3.939 Mt   100.000 % 	 ../ShadowMapRLInitialization

		       4.561 Mt     0.015 %         1 x        4.561 Mt        0.000 Mt        4.561 Mt   100.000 % 	 ../ResolveRLInitialization

		   29104.060 Mt    98.334 %     81986 x        0.355 Mt        2.589 Mt     1496.426 Mt     5.142 % 	 ../MainLoop

		     227.012 Mt     0.780 %      1746 x        0.130 Mt        0.034 Mt      226.395 Mt    99.728 % 	 .../Update

		       0.617 Mt     0.272 %         1 x        0.617 Mt        0.000 Mt        0.617 Mt   100.000 % 	 ..../GuiModelApply

		   27380.622 Mt    94.078 %      8524 x        3.212 Mt        2.419 Mt      942.704 Mt     3.443 % 	 .../Render

		    1462.763 Mt     5.342 %      8524 x        0.172 Mt        0.202 Mt       56.404 Mt     3.856 % 	 ..../RayTracing

		     661.689 Mt    45.236 %      8524 x        0.078 Mt        0.089 Mt      652.872 Mt    98.668 % 	 ...../Deferred

		       8.817 Mt     1.332 %      8524 x        0.001 Mt        0.001 Mt        8.817 Mt   100.000 % 	 ....../DeferredRLPrep

		     518.828 Mt    35.469 %      8524 x        0.061 Mt        0.075 Mt      513.984 Mt    99.066 % 	 ...../RayCasting

		       4.844 Mt     0.934 %      8524 x        0.001 Mt        0.001 Mt        4.844 Mt   100.000 % 	 ....../RayTracingRLPrep

		     225.842 Mt    15.439 %      8524 x        0.026 Mt        0.030 Mt      225.842 Mt   100.000 % 	 ...../Resolve

		   24975.155 Mt    91.215 %      8524 x        2.930 Mt        2.091 Mt    24975.155 Mt   100.000 % 	 ..../Present

		       3.368 Mt     0.011 %         1 x        3.368 Mt        0.000 Mt        3.368 Mt   100.000 % 	 ../TexturedRLInitialization

		       3.539 Mt     0.012 %         1 x        3.539 Mt        0.000 Mt        3.539 Mt   100.000 % 	 ../AmbientOcclusionRLInitialization

		       5.502 Mt     0.019 %         1 x        5.502 Mt        0.000 Mt        5.502 Mt   100.000 % 	 ../RasterResolveRLInitialization

		       6.563 Mt     0.022 %         1 x        6.563 Mt        0.000 Mt        6.563 Mt   100.000 % 	 ../DeferredRLInitialization

		      66.179 Mt     0.224 %         1 x       66.179 Mt        0.000 Mt        2.850 Mt     4.306 % 	 ../RayTracingRLInitialization

		      63.330 Mt    95.694 %         1 x       63.330 Mt        0.000 Mt       63.330 Mt   100.000 % 	 .../PipelineBuild

		     121.221 Mt     0.410 %         1 x      121.221 Mt        0.000 Mt        0.238 Mt     0.197 % 	 ../Initialization

		     120.983 Mt    99.803 %         1 x      120.983 Mt        0.000 Mt        2.095 Mt     1.732 % 	 .../ScenePrep

		      50.243 Mt    41.529 %         1 x       50.243 Mt        0.000 Mt       11.502 Mt    22.892 % 	 ..../SceneLoad

		      11.442 Mt    22.774 %         1 x       11.442 Mt        0.000 Mt        3.381 Mt    29.546 % 	 ...../glTF-LoadScene

		       8.062 Mt    70.454 %         2 x        4.031 Mt        0.000 Mt        8.062 Mt   100.000 % 	 ....../glTF-LoadImageData

		      12.108 Mt    24.100 %         1 x       12.108 Mt        0.000 Mt       12.108 Mt   100.000 % 	 ...../glTF-CreateScene

		      15.190 Mt    30.234 %         1 x       15.190 Mt        0.000 Mt       15.190 Mt   100.000 % 	 ...../SceneDesc-CreateScene

		       1.084 Mt     0.896 %         1 x        1.084 Mt        0.000 Mt        0.001 Mt     0.055 % 	 ..../ShadowMapRLPrep

		       1.083 Mt    99.945 %         1 x        1.083 Mt        0.000 Mt        1.074 Mt    99.114 % 	 ...../PrepareForRendering

		       0.010 Mt     0.886 %         1 x        0.010 Mt        0.000 Mt        0.010 Mt   100.000 % 	 ....../PrepareDrawBundle

		       3.833 Mt     3.169 %         1 x        3.833 Mt        0.000 Mt        0.000 Mt     0.010 % 	 ..../TexturedRLPrep

		       3.833 Mt    99.990 %         1 x        3.833 Mt        0.000 Mt        3.827 Mt    99.846 % 	 ...../PrepareForRendering

		       0.001 Mt     0.037 %         1 x        0.001 Mt        0.000 Mt        0.001 Mt   100.000 % 	 ....../PrepareMaterials

		       0.004 Mt     0.117 %         1 x        0.004 Mt        0.000 Mt        0.004 Mt   100.000 % 	 ....../PrepareDrawBundle

		       2.814 Mt     2.326 %         1 x        2.814 Mt        0.000 Mt        0.001 Mt     0.021 % 	 ..../DeferredRLPrep

		       2.813 Mt    99.979 %         1 x        2.813 Mt        0.000 Mt        1.989 Mt    70.688 % 	 ...../PrepareForRendering

		       0.001 Mt     0.025 %         1 x        0.001 Mt        0.000 Mt        0.001 Mt   100.000 % 	 ....../PrepareMaterials

		       0.820 Mt    29.134 %         1 x        0.820 Mt        0.000 Mt        0.820 Mt   100.000 % 	 ....../BuildMaterialCache

		       0.004 Mt     0.153 %         1 x        0.004 Mt        0.000 Mt        0.004 Mt   100.000 % 	 ....../PrepareDrawBundle

		      47.225 Mt    39.034 %         1 x       47.225 Mt        0.000 Mt        3.637 Mt     7.702 % 	 ..../RayTracingRLPrep

		       0.059 Mt     0.124 %         1 x        0.059 Mt        0.000 Mt        0.059 Mt   100.000 % 	 ...../PrepareAccelerationStructures

		       0.001 Mt     0.002 %         1 x        0.001 Mt        0.000 Mt        0.001 Mt   100.000 % 	 ...../PrepareCache

		      29.058 Mt    61.531 %         1 x       29.058 Mt        0.000 Mt       29.058 Mt   100.000 % 	 ...../PipelineBuild

		       0.860 Mt     1.822 %         1 x        0.860 Mt        0.000 Mt        0.860 Mt   100.000 % 	 ...../BuildCache

		       6.989 Mt    14.799 %         1 x        6.989 Mt        0.000 Mt        0.001 Mt     0.011 % 	 ...../BuildAccelerationStructures

		       6.988 Mt    99.989 %         1 x        6.988 Mt        0.000 Mt        4.594 Mt    65.738 % 	 ....../AccelerationStructureBuild

		       1.456 Mt    20.839 %         1 x        1.456 Mt        0.000 Mt        1.456 Mt   100.000 % 	 ......./BuildBottomLevelAS

		       0.938 Mt    13.423 %         1 x        0.938 Mt        0.000 Mt        0.938 Mt   100.000 % 	 ......./BuildTopLevelAS

		       6.621 Mt    14.020 %         1 x        6.621 Mt        0.000 Mt        6.621 Mt   100.000 % 	 ...../BuildShaderTables

		      13.689 Mt    11.315 %         1 x       13.689 Mt        0.000 Mt       13.689 Mt   100.000 % 	 ..../GpuSidePrep

	WindowThread:

		   29595.892 Mt   100.000 %         1 x    29595.892 Mt    29595.892 Mt    29595.892 Mt   100.000 % 	 /

	GpuThread:

		   20842.424 Mt   100.000 %      8524 x        2.445 Mt        1.558 Mt      138.848 Mt     0.666 % 	 /

		       0.266 Mt     0.001 %         1 x        0.266 Mt        0.000 Mt        0.006 Mt     2.289 % 	 ./ScenePrep

		       0.259 Mt    97.711 %         1 x        0.259 Mt        0.000 Mt        0.001 Mt     0.493 % 	 ../AccelerationStructureBuild

		       0.174 Mt    67.135 %         1 x        0.174 Mt        0.000 Mt        0.174 Mt   100.000 % 	 .../BuildBottomLevelASGpu

		       0.084 Mt    32.371 %         1 x        0.084 Mt        0.000 Mt        0.084 Mt   100.000 % 	 .../BuildTopLevelASGpu

		   20598.131 Mt    98.828 %      8524 x        2.416 Mt        1.556 Mt       20.670 Mt     0.100 % 	 ./RayTracingGpu

		    2298.424 Mt    11.158 %      8524 x        0.270 Mt        0.064 Mt     2298.424 Mt   100.000 % 	 ../DeferredRLGpu

		    4804.426 Mt    23.325 %      8524 x        0.564 Mt        0.164 Mt     4804.426 Mt   100.000 % 	 ../RayTracingRLGpu

		   13474.610 Mt    65.417 %      8524 x        1.581 Mt        1.328 Mt    13474.610 Mt   100.000 % 	 ../ResolveRLGpu

		     105.180 Mt     0.505 %      8524 x        0.012 Mt        0.002 Mt      105.180 Mt   100.000 % 	 ./Present


	============================


