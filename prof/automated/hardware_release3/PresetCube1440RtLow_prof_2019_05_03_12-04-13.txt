Configuration: 
Command line parameters  = E:\programming\FIT-DIP\Masters-DXR-Main-git\build\Measurement\x64\Release\Measurement.exe --scene Cube/Cube.gltf --profile-automation --profile-camera-track Cube/Cube.trk --rt-mode --width 2560 --height 1440 --profile-output PresetCube1440RtLow --quality-preset Low 
Build type               = Release
Device name              = NVIDIA GeForce RTX 2080 Ti
Device memory MB         = 11049
Ray Tracing backend      = Hardware
Window width             = 2560
Window height            = 1421
Window title             = Direct3D App
Back-buffer count        = 3
Fill Triangles           = enabled
Triangles Font Clock-Wise= enabled
Fast Build AS            = disabled
Target FPS               = 0
Target UPS               = 60
Tearing                  = enabled
VSync                    = disabled
GUI rendering            = disabled
Debug layer              = disabled
GPU validation           = disabled
GPU profiling            = enabled
Render mode              = Ray Tracing
Loaded scene file        = Cube/Cube.gltf
Using testing scene      = disabled
Duplication              = 1
Duplication in cube      = disabled
Duplication offset       = 10
BLAS duplication         = disabled
Rays per pixel           = 5
Quality preset           = Low
Rasterization Quality    : 
  AO                     = enabled
  AO kernel size         = 4
  Shadows                = enabled
  Shadow map resolution  = 2048
  Shadow PCF kernel size = 0
  Reflections            = disabled
Ray Tracing Quality      : 
  AO                     = enabled
  AO rays                = 4
  AO kernel size         = 4
  Shadows                = enabled
  Shadow rays            = 1
  Shadow kernel size     = 4
  Reflections            = disabled

ProfilingAggregator: 
Render	,	3.4666	,	3.9237	,	3.9439	,	3.966	,	3.9833	,	4.4392	,	4.6237	,	4.8178	,	5.3143	,	4.5709	,	4.2498	,	4.4254	,	4.0961	,	3.9572	,	4.766	,	4.9151	,	4.7101	,	5.0249	,	5.0944	,	5.0372	,	4.7102	,	4.0754	,	3.7683	,	4.0865	,	3.5229	,	3.3716	,	3.1425	,	3.0849	,	2.9701
Update	,	0.0762	,	0.0826	,	0.0825	,	0.0817	,	0.0769	,	0.0612	,	0.083	,	0.0833	,	0.0815	,	0.0834	,	0.0831	,	0.0792	,	0.0833	,	0.0817	,	0.0767	,	0.0759	,	0.0826	,	0.0796	,	0.0806	,	0.0808	,	0.0818	,	0.0768	,	0.0816	,	0.0769	,	0.0808	,	0.0823	,	0.0819	,	0.0823	,	0.0805
RayTracing	,	0.1891	,	0.2033	,	0.1708	,	0.1765	,	0.1824	,	0.1544	,	0.1749	,	0.172	,	0.1724	,	0.1968	,	0.1704	,	0.1721	,	0.1719	,	0.1688	,	0.1675	,	0.1837	,	0.1711	,	0.1752	,	0.1737	,	0.1704	,	0.1712	,	0.1697	,	0.1703	,	0.1777	,	0.1789	,	0.173	,	0.1693	,	0.1705	,	0.1732
RayTracingGpu	,	2.81216	,	3.11037	,	3.18243	,	3.20781	,	3.28032	,	3.90813	,	3.51453	,	3.72426	,	4.37795	,	3.68541	,	3.54627	,	3.66141	,	3.32064	,	3.19414	,	3.91619	,	3.93942	,	3.89667	,	4.20067	,	4.31082	,	4.27587	,	3.8944	,	3.26557	,	3.05286	,	2.91226	,	2.65011	,	2.50813	,	2.38787	,	2.32541	,	2.28826
DeferredRLGpu	,	0.181312	,	0.242656	,	0.256416	,	0.263328	,	0.277792	,	0.33664	,	0.312128	,	0.354848	,	0.414912	,	0.368032	,	0.321888	,	0.351136	,	0.281184	,	0.2536	,	0.288448	,	0.33776	,	0.39664	,	0.46704	,	0.488512	,	0.487872	,	0.408736	,	0.286496	,	0.233952	,	0.183328	,	0.151808	,	0.118432	,	0.08992	,	0.073664	,	0.064992
RayTracingRLGpu	,	0.452096	,	0.593088	,	0.628576	,	0.642464	,	0.679456	,	0.834368	,	0.772192	,	0.881152	,	1.40026	,	0.913856	,	0.850048	,	0.925952	,	0.736416	,	0.66288	,	1.10093	,	1.10058	,	1.05373	,	1.24666	,	1.30547	,	1.30518	,	1.0977	,	0.752672	,	0.5992	,	0.46784	,	0.394912	,	0.308768	,	0.243584	,	0.208096	,	0.186304
ResolveRLGpu	,	2.17632	,	2.27197	,	2.29536	,	2.29981	,	2.32112	,	2.73546	,	2.42858	,	2.48669	,	2.56093	,	2.40141	,	2.37232	,	2.38224	,	2.30096	,	2.27571	,	2.52384	,	2.4985	,	2.44432	,	2.48451	,	2.51485	,	2.47962	,	2.38486	,	2.22432	,	2.21568	,	2.2593	,	2.09923	,	2.07622	,	2.05264	,	2.04077	,	2.03504
Index	,	0	,	1	,	2	,	3	,	4	,	5	,	6	,	7	,	8	,	9	,	10	,	11	,	12	,	13	,	14	,	15	,	16	,	17	,	18	,	19	,	20	,	21	,	22	,	23	,	24	,	25	,	26	,	27	,	28

ProfilingBuildAggregator: 
BuildBottomLevelAS	,	0.625
BuildBottomLevelASGpu	,	0.133888
BuildTopLevelAS	,	0.5446
BuildTopLevelASGpu	,	0.06112
Duplication	,	1
Triangles	,	12
Meshes	,	1
BottomLevels	,	1
TopLevelSize	,	64
BottomLevelsSize	,	4480
Index	,	0

FpsAggregator: 
FPS	,	226	,	248	,	242	,	242	,	238	,	228	,	230	,	227	,	209	,	206	,	233	,	212	,	224	,	243	,	237	,	224	,	211	,	198	,	189	,	187	,	193	,	222	,	241	,	261	,	274	,	285	,	297	,	307	,	312
UPS	,	59	,	61	,	60	,	60	,	60	,	60	,	60	,	60	,	61	,	60	,	60	,	60	,	60	,	61	,	60	,	60	,	60	,	60	,	60	,	60	,	61	,	60	,	60	,	60	,	60	,	61	,	60	,	60	,	60
FrameTime	,	4.43964	,	4.03636	,	4.1475	,	4.14772	,	4.21253	,	4.3943	,	4.35173	,	4.40982	,	4.80357	,	4.86734	,	4.30534	,	4.73755	,	4.48029	,	4.12486	,	4.2362	,	4.47374	,	4.74565	,	5.06865	,	5.31373	,	5.34864	,	5.19638	,	4.50667	,	4.16481	,	3.84649	,	3.66159	,	3.5162	,	3.37577	,	3.26355	,	3.20572
GigaRays/s	,	4.09691	,	4.50624	,	4.38548	,	4.38526	,	4.31779	,	4.13918	,	4.17967	,	4.12462	,	3.78652	,	3.7369	,	4.2247	,	3.83929	,	4.05974	,	4.40956	,	4.29366	,	4.06568	,	3.83273	,	3.58849	,	3.42298	,	3.40064	,	3.50028	,	4.03597	,	4.36726	,	4.72867	,	4.96746	,	5.17286	,	5.38804	,	5.57331	,	5.67385
Index	,	0	,	1	,	2	,	3	,	4	,	5	,	6	,	7	,	8	,	9	,	10	,	11	,	12	,	13	,	14	,	15	,	16	,	17	,	18	,	19	,	20	,	21	,	22	,	23	,	24	,	25	,	26	,	27	,	28


AccelerationStructure: 
Ray tracing acceleration structure building statistics: 
	Bottom Level Acceleration Structure: 
		Maximum [B]: 4480
		Minimum [B]: 4480
		Currently Allocated [B]: 4480
		Currently Created [num]: 1
		Current Average [B]: 4480
		Maximum Triangles [num]: 12
		Minimum Triangles [num]: 12
		Total Triangles [num]: 12
		Maximum Meshes [num]: 1
		Minimum Meshes [num]: 1
		Total Meshes [num]: 1
	Top Level Acceleration Structure: 
		Maximum [B]: 64
		Minimum [B]: 64
		Currently Allocated [B]: 64
		Total Allocated [B]: 64
		Total Created [num]: 1
		Average Allocated [B]: 64
		Maximum Instances [num]: 1
		Minimum Instances [num]: 1
		Total Instances [num]: 1
	Scratch Buffer: 
		Maximum [B]: 896
		Minimum [B]: 896
		Currently Allocated [B]: 896
		Total Allocated [B]: 896
		Total Created [num]: 1
		Average Allocated [B]: 896
ProfilingStack: 
[PROF] Profiling statistics: 
	Scopes entered : 188626
	Scopes exited : 188625
	Overhead per scope [ticks] : 101.7
	Threads entered : 3
	Threads exited : 0
	============================
		         Sum[Mt]   Parent[%]  Samples[num]       Avg[Mt]   Last/Curr[Mt]        Self[Mt]     Self[%] 	 Name

	main:

		   29473.336 Mt   100.000 %         1 x    29473.337 Mt    29473.335 Mt        0.344 Mt     0.001 % 	 /

		   29473.022 Mt    99.999 %         1 x    29473.023 Mt    29473.022 Mt      251.918 Mt     0.855 % 	 ./Main application loop

		       1.650 Mt     0.006 %         1 x        1.650 Mt        0.000 Mt        1.650 Mt   100.000 % 	 ../ShadowMapRLInitialization

		       4.445 Mt     0.015 %         1 x        4.445 Mt        0.000 Mt        4.445 Mt   100.000 % 	 ../ResolveRLInitialization

		   29092.271 Mt    98.708 %     97812 x        0.297 Mt        3.339 Mt      499.765 Mt     1.718 % 	 ../MainLoop

		     226.028 Mt     0.777 %      1745 x        0.130 Mt        0.085 Mt      225.425 Mt    99.733 % 	 .../Update

		       0.603 Mt     0.267 %         1 x        0.603 Mt        0.000 Mt        0.603 Mt   100.000 % 	 ..../GuiModelApply

		   28366.478 Mt    97.505 %      6848 x        4.142 Mt        3.251 Mt      785.273 Mt     2.768 % 	 .../Render

		    1194.893 Mt     4.212 %      6848 x        0.174 Mt        0.194 Mt       45.406 Mt     3.800 % 	 ..../RayTracing

		     542.102 Mt    45.368 %      6848 x        0.079 Mt        0.085 Mt      534.479 Mt    98.594 % 	 ...../Deferred

		       7.623 Mt     1.406 %      6848 x        0.001 Mt        0.001 Mt        7.623 Mt   100.000 % 	 ....../DeferredRLPrep

		     421.437 Mt    35.270 %      6848 x        0.062 Mt        0.073 Mt      417.445 Mt    99.053 % 	 ...../RayCasting

		       3.992 Mt     0.947 %      6848 x        0.001 Mt        0.001 Mt        3.992 Mt   100.000 % 	 ....../RayTracingRLPrep

		     185.948 Mt    15.562 %      6848 x        0.027 Mt        0.029 Mt      185.948 Mt   100.000 % 	 ...../Resolve

		   26386.312 Mt    93.019 %      6848 x        3.853 Mt        2.932 Mt    26386.312 Mt   100.000 % 	 ..../Present

		       3.306 Mt     0.011 %         1 x        3.306 Mt        0.000 Mt        3.306 Mt   100.000 % 	 ../TexturedRLInitialization

		       3.381 Mt     0.011 %         1 x        3.381 Mt        0.000 Mt        3.381 Mt   100.000 % 	 ../AmbientOcclusionRLInitialization

		       5.577 Mt     0.019 %         1 x        5.577 Mt        0.000 Mt        5.577 Mt   100.000 % 	 ../RasterResolveRLInitialization

		       5.469 Mt     0.019 %         1 x        5.469 Mt        0.000 Mt        5.469 Mt   100.000 % 	 ../DeferredRLInitialization

		      52.759 Mt     0.179 %         1 x       52.759 Mt        0.000 Mt        2.665 Mt     5.051 % 	 ../RayTracingRLInitialization

		      50.094 Mt    94.949 %         1 x       50.094 Mt        0.000 Mt       50.094 Mt   100.000 % 	 .../PipelineBuild

		      52.246 Mt     0.177 %         1 x       52.246 Mt        0.000 Mt        0.172 Mt     0.329 % 	 ../Initialization

		      52.074 Mt    99.671 %         1 x       52.074 Mt        0.000 Mt        0.726 Mt     1.394 % 	 .../ScenePrep

		      21.192 Mt    40.696 %         1 x       21.192 Mt        0.000 Mt        5.747 Mt    27.117 % 	 ..../SceneLoad

		      10.423 Mt    49.185 %         1 x       10.423 Mt        0.000 Mt        2.459 Mt    23.591 % 	 ...../glTF-LoadScene

		       7.964 Mt    76.409 %         2 x        3.982 Mt        0.000 Mt        7.964 Mt   100.000 % 	 ....../glTF-LoadImageData

		       1.896 Mt     8.946 %         1 x        1.896 Mt        0.000 Mt        1.896 Mt   100.000 % 	 ...../glTF-CreateScene

		       3.126 Mt    14.752 %         1 x        3.126 Mt        0.000 Mt        3.126 Mt   100.000 % 	 ...../SceneDesc-CreateScene

		       0.148 Mt     0.284 %         1 x        0.148 Mt        0.000 Mt        0.001 Mt     0.406 % 	 ..../ShadowMapRLPrep

		       0.147 Mt    99.594 %         1 x        0.147 Mt        0.000 Mt        0.138 Mt    93.618 % 	 ...../PrepareForRendering

		       0.009 Mt     6.382 %         1 x        0.009 Mt        0.000 Mt        0.009 Mt   100.000 % 	 ....../PrepareDrawBundle

		       0.500 Mt     0.960 %         1 x        0.500 Mt        0.000 Mt        0.001 Mt     0.140 % 	 ..../TexturedRLPrep

		       0.499 Mt    99.860 %         1 x        0.499 Mt        0.000 Mt        0.494 Mt    99.078 % 	 ...../PrepareForRendering

		       0.001 Mt     0.281 %         1 x        0.001 Mt        0.000 Mt        0.001 Mt   100.000 % 	 ....../PrepareMaterials

		       0.003 Mt     0.641 %         1 x        0.003 Mt        0.000 Mt        0.003 Mt   100.000 % 	 ....../PrepareDrawBundle

		       1.236 Mt     2.374 %         1 x        1.236 Mt        0.000 Mt        0.000 Mt     0.040 % 	 ..../DeferredRLPrep

		       1.236 Mt    99.960 %         1 x        1.236 Mt        0.000 Mt        1.103 Mt    89.245 % 	 ...../PrepareForRendering

		       0.001 Mt     0.049 %         1 x        0.001 Mt        0.000 Mt        0.001 Mt   100.000 % 	 ....../PrepareMaterials

		       0.129 Mt    10.423 %         1 x        0.129 Mt        0.000 Mt        0.129 Mt   100.000 % 	 ....../BuildMaterialCache

		       0.004 Mt     0.283 %         1 x        0.004 Mt        0.000 Mt        0.004 Mt   100.000 % 	 ....../PrepareDrawBundle

		      20.367 Mt    39.111 %         1 x       20.367 Mt        0.000 Mt        1.649 Mt     8.098 % 	 ..../RayTracingRLPrep

		       0.048 Mt     0.236 %         1 x        0.048 Mt        0.000 Mt        0.048 Mt   100.000 % 	 ...../PrepareAccelerationStructures

		       0.001 Mt     0.004 %         1 x        0.001 Mt        0.000 Mt        0.001 Mt   100.000 % 	 ...../PrepareCache

		      15.715 Mt    77.160 %         1 x       15.715 Mt        0.000 Mt       15.715 Mt   100.000 % 	 ...../PipelineBuild

		       0.153 Mt     0.753 %         1 x        0.153 Mt        0.000 Mt        0.153 Mt   100.000 % 	 ...../BuildCache

		       2.186 Mt    10.732 %         1 x        2.186 Mt        0.000 Mt        0.001 Mt     0.023 % 	 ...../BuildAccelerationStructures

		       2.185 Mt    99.977 %         1 x        2.185 Mt        0.000 Mt        1.016 Mt    46.476 % 	 ....../AccelerationStructureBuild

		       0.625 Mt    28.602 %         1 x        0.625 Mt        0.000 Mt        0.625 Mt   100.000 % 	 ......./BuildBottomLevelAS

		       0.545 Mt    24.922 %         1 x        0.545 Mt        0.000 Mt        0.545 Mt   100.000 % 	 ......./BuildTopLevelAS

		       0.614 Mt     3.017 %         1 x        0.614 Mt        0.000 Mt        0.614 Mt   100.000 % 	 ...../BuildShaderTables

		       7.906 Mt    15.181 %         1 x        7.906 Mt        0.000 Mt        7.906 Mt   100.000 % 	 ..../GpuSidePrep

	WindowThread:

		   29472.316 Mt   100.000 %         1 x    29472.316 Mt    29472.316 Mt    29472.316 Mt   100.000 % 	 /

	GpuThread:

		   22967.638 Mt   100.000 %      6848 x        3.354 Mt        2.474 Mt      126.696 Mt     0.552 % 	 /

		       0.201 Mt     0.001 %         1 x        0.201 Mt        0.000 Mt        0.005 Mt     2.594 % 	 ./ScenePrep

		       0.196 Mt    97.406 %         1 x        0.196 Mt        0.000 Mt        0.001 Mt     0.425 % 	 ../AccelerationStructureBuild

		       0.134 Mt    68.366 %         1 x        0.134 Mt        0.000 Mt        0.134 Mt   100.000 % 	 .../BuildBottomLevelASGpu

		       0.061 Mt    31.209 %         1 x        0.061 Mt        0.000 Mt        0.061 Mt   100.000 % 	 .../BuildTopLevelASGpu

		   22678.753 Mt    98.742 %      6848 x        3.312 Mt        2.385 Mt       21.609 Mt     0.095 % 	 ./RayTracingGpu

		    1864.605 Mt     8.222 %      6848 x        0.272 Mt        0.064 Mt     1864.605 Mt   100.000 % 	 ../DeferredRLGpu

		    4894.568 Mt    21.582 %      6848 x        0.715 Mt        0.184 Mt     4894.568 Mt   100.000 % 	 ../RayTracingRLGpu

		   15897.971 Mt    70.101 %      6848 x        2.322 Mt        2.136 Mt    15897.971 Mt   100.000 % 	 ../ResolveRLGpu

		     161.988 Mt     0.705 %      6848 x        0.024 Mt        0.089 Mt      161.988 Mt   100.000 % 	 ./Present


	============================


