Configuration: 
Command line parameters  = E:\programming\FIT-DIP\Masters-DXR-Main-git\build\Measurement\x64\Release\Measurement.exe --scene Sponza/Sponza.gltf --profile-automation --profile-camera-track Sponza/SponzaPreset.trk --rt-mode --width 2560 --height 1440 --profile-output PresetSponza1440RtSoftShadows --quality-preset SoftShadows 
Build type               = Release
Device name              = NVIDIA GeForce RTX 2080 Ti
Device memory MB         = 11049
Ray Tracing backend      = Hardware
Window width             = 2560
Window height            = 1421
Window title             = Direct3D App
Back-buffer count        = 3
Fill Triangles           = enabled
Triangles Font Clock-Wise= enabled
Fast Build AS            = disabled
Target FPS               = 0
Target UPS               = 60
Tearing                  = enabled
VSync                    = disabled
GUI rendering            = disabled
Debug layer              = disabled
GPU validation           = disabled
GPU profiling            = enabled
Render mode              = Ray Tracing
Loaded scene file        = Sponza/Sponza.gltf
Using testing scene      = disabled
Duplication              = 1
Duplication in cube      = disabled
Duplication offset       = 10
BLAS duplication         = disabled
Rays per pixel           = 4
Quality preset           = SoftShadows
Rasterization Quality    : 
  AO                     = disabled
  AO kernel size         = 1
  Shadows                = enabled
  Shadow map resolution  = 4096
  Shadow PCF kernel size = 4
  Reflections            = disabled
Ray Tracing Quality      : 
  AO                     = disabled
  AO rays                = 0
  AO kernel size         = 4
  Shadows                = enabled
  Shadow rays            = 4
  Shadow kernel size     = 4
  Reflections            = disabled

ProfilingAggregator: 
Render	,	7.8302	,	7.4497	,	6.9824	,	5.9968	,	6.668	,	6.6019	,	6.7335	,	7.2897	,	6.7826	,	6.2501	,	6.5572	,	7.4596	,	7.1019	,	7.5166	,	6.7898	,	5.9225	,	6.6553	,	7.0668	,	6.6621	,	8.2395	,	8.6561	,	9.0493	,	9.4329
Update	,	0.0816	,	0.0833	,	0.0759	,	0.0846	,	0.0836	,	0.0822	,	0.0834	,	0.0904	,	0.0896	,	0.0892	,	0.0852	,	0.087	,	0.0855	,	0.0836	,	0.0817	,	0.089	,	0.0801	,	0.0815	,	0.0836	,	0.0847	,	0.0785	,	0.0766	,	0.0773
RayTracing	,	0.1825	,	0.1741	,	0.1719	,	0.1803	,	0.1795	,	0.2075	,	0.1907	,	0.1977	,	0.1853	,	0.1851	,	0.18	,	0.1896	,	0.1815	,	0.1873	,	0.1892	,	0.1736	,	0.1735	,	0.1706	,	0.1711	,	0.1735	,	0.1894	,	0.1746	,	0.1751
RayTracingGpu	,	6.87331	,	6.67462	,	6.27158	,	5.22256	,	5.61446	,	5.63834	,	5.85946	,	6.29088	,	5.9976	,	5.4439	,	5.38528	,	6.43034	,	6.20202	,	6.54538	,	5.90176	,	5.10387	,	5.86141	,	6.14205	,	5.73706	,	7.37184	,	7.82064	,	7.8935	,	8.56301
DeferredRLGpu	,	2.73971	,	2.51555	,	2.20806	,	1.47565	,	1.34454	,	1.77914	,	1.95517	,	2.38906	,	2.04429	,	1.52774	,	1.34669	,	1.84227	,	2.15805	,	2.45498	,	2.1425	,	1.47312	,	1.94998	,	1.87968	,	1.55018	,	3.12426	,	3.8671	,	3.84214	,	4.01834
RayTracingRLGpu	,	2.38832	,	2.40608	,	2.30998	,	2.00058	,	2.37498	,	2.1369	,	2.16109	,	2.20986	,	2.2432	,	2.19827	,	2.23158	,	2.71411	,	2.31411	,	2.3784	,	2.02237	,	1.88019	,	2.16112	,	2.14774	,	2.22013	,	2.36973	,	2.20304	,	2.20493	,	2.67075
ResolveRLGpu	,	1.74259	,	1.75002	,	1.75146	,	1.74426	,	1.89331	,	1.71917	,	1.74112	,	1.68698	,	1.70819	,	1.71594	,	1.80531	,	1.87155	,	1.72794	,	1.70998	,	1.73478	,	1.74848	,	1.74819	,	2.11261	,	1.96474	,	1.87456	,	1.74845	,	1.84486	,	1.87267
Index	,	0	,	1	,	2	,	3	,	4	,	5	,	6	,	7	,	8	,	9	,	10	,	11	,	12	,	13	,	14	,	15	,	16	,	17	,	18	,	19	,	20	,	21	,	22

ProfilingBuildAggregator: 
BuildBottomLevelAS	,	2.0249
BuildBottomLevelASGpu	,	2.62717
BuildTopLevelAS	,	0.9292
BuildTopLevelASGpu	,	0.09024
Duplication	,	1
Triangles	,	262267
Meshes	,	103
BottomLevels	,	1
TopLevelSize	,	64
BottomLevelsSize	,	18390656
Index	,	0

FpsAggregator: 
FPS	,	84	,	112	,	128	,	137	,	154	,	148	,	147	,	127	,	130	,	141	,	148	,	145	,	135	,	126	,	127	,	151	,	142	,	137	,	147	,	150	,	111	,	106	,	106
UPS	,	59	,	61	,	60	,	60	,	60	,	60	,	60	,	61	,	60	,	60	,	60	,	60	,	61	,	60	,	60	,	60	,	60	,	61	,	60	,	60	,	60	,	61	,	60
FrameTime	,	11.9485	,	8.96413	,	7.83207	,	7.30625	,	6.53134	,	6.77541	,	6.82679	,	7.88218	,	7.71512	,	7.11223	,	6.79346	,	6.91771	,	7.45016	,	7.9588	,	7.89589	,	6.63171	,	7.0569	,	7.3341	,	6.81731	,	6.68461	,	9.05547	,	9.51671	,	9.48804
GigaRays/s	,	1.21781	,	1.62325	,	1.85788	,	1.99159	,	2.22788	,	2.14762	,	2.13146	,	1.84607	,	1.88604	,	2.04592	,	2.14192	,	2.10345	,	1.95312	,	1.8283	,	1.84286	,	2.19416	,	2.06196	,	1.98403	,	2.13443	,	2.1768	,	1.60688	,	1.529	,	1.53362
Index	,	0	,	1	,	2	,	3	,	4	,	5	,	6	,	7	,	8	,	9	,	10	,	11	,	12	,	13	,	14	,	15	,	16	,	17	,	18	,	19	,	20	,	21	,	22


AccelerationStructure: 
Ray tracing acceleration structure building statistics: 
	Bottom Level Acceleration Structure: 
		Maximum [B]: 18390656
		Minimum [B]: 18390656
		Currently Allocated [B]: 18390656
		Currently Created [num]: 1
		Current Average [B]: 18390656
		Maximum Triangles [num]: 262267
		Minimum Triangles [num]: 262267
		Total Triangles [num]: 262267
		Maximum Meshes [num]: 103
		Minimum Meshes [num]: 103
		Total Meshes [num]: 103
	Top Level Acceleration Structure: 
		Maximum [B]: 64
		Minimum [B]: 64
		Currently Allocated [B]: 64
		Total Allocated [B]: 64
		Total Created [num]: 1
		Average Allocated [B]: 64
		Maximum Instances [num]: 1
		Minimum Instances [num]: 1
		Total Instances [num]: 1
	Scratch Buffer: 
		Maximum [B]: 14995072
		Minimum [B]: 14995072
		Currently Allocated [B]: 14995072
		Total Allocated [B]: 14995072
		Total Created [num]: 1
		Average Allocated [B]: 14995072
ProfilingStack: 
[PROF] Profiling statistics: 
	Scopes entered : 68726
	Scopes exited : 68725
	Overhead per scope [ticks] : 102.7
	Threads entered : 3
	Threads exited : 0
	============================
		         Sum[Mt]   Parent[%]  Samples[num]       Avg[Mt]   Last/Curr[Mt]        Self[Mt]     Self[%] 	 Name

	main:

		   26679.142 Mt   100.000 %         1 x    26679.143 Mt    26679.142 Mt        0.528 Mt     0.002 % 	 /

		   26678.644 Mt    99.998 %         1 x    26678.646 Mt    26678.644 Mt      271.426 Mt     1.017 % 	 ./Main application loop

		       1.562 Mt     0.006 %         1 x        1.562 Mt        0.000 Mt        1.562 Mt   100.000 % 	 ../ShadowMapRLInitialization

		       4.577 Mt     0.017 %         1 x        4.577 Mt        0.000 Mt        4.577 Mt   100.000 % 	 ../ResolveRLInitialization

		   23153.427 Mt    86.786 %     27693 x        0.836 Mt        9.045 Mt     1359.912 Mt     5.873 % 	 ../MainLoop

		     227.443 Mt     0.982 %      1388 x        0.164 Mt        0.032 Mt      226.834 Mt    99.732 % 	 .../Update

		       0.610 Mt     0.268 %         1 x        0.610 Mt        0.000 Mt        0.610 Mt   100.000 % 	 ..../GuiModelApply

		   21566.071 Mt    93.144 %      3041 x        7.092 Mt        8.855 Mt      362.783 Mt     1.682 % 	 .../Render

		     556.547 Mt     2.581 %      3041 x        0.183 Mt        0.226 Mt       20.769 Mt     3.732 % 	 ..../RayTracing

		     237.173 Mt    42.615 %      3041 x        0.078 Mt        0.091 Mt      234.222 Mt    98.756 % 	 ...../Deferred

		       2.951 Mt     1.244 %      3041 x        0.001 Mt        0.001 Mt        2.951 Mt   100.000 % 	 ....../DeferredRLPrep

		     201.560 Mt    36.216 %      3041 x        0.066 Mt        0.087 Mt      199.745 Mt    99.100 % 	 ...../RayCasting

		       1.815 Mt     0.900 %      3041 x        0.001 Mt        0.001 Mt        1.815 Mt   100.000 % 	 ....../RayTracingRLPrep

		      97.045 Mt    17.437 %      3041 x        0.032 Mt        0.039 Mt       97.045 Mt   100.000 % 	 ...../Resolve

		   20646.741 Mt    95.737 %      3041 x        6.789 Mt        8.470 Mt    20646.741 Mt   100.000 % 	 ..../Present

		       3.217 Mt     0.012 %         1 x        3.217 Mt        0.000 Mt        3.217 Mt   100.000 % 	 ../TexturedRLInitialization

		       3.181 Mt     0.012 %         1 x        3.181 Mt        0.000 Mt        3.181 Mt   100.000 % 	 ../AmbientOcclusionRLInitialization

		       5.817 Mt     0.022 %         1 x        5.817 Mt        0.000 Mt        5.817 Mt   100.000 % 	 ../RasterResolveRLInitialization

		       5.035 Mt     0.019 %         1 x        5.035 Mt        0.000 Mt        5.035 Mt   100.000 % 	 ../DeferredRLInitialization

		      61.126 Mt     0.229 %         1 x       61.126 Mt        0.000 Mt        2.456 Mt     4.017 % 	 ../RayTracingRLInitialization

		      58.670 Mt    95.983 %         1 x       58.670 Mt        0.000 Mt       58.670 Mt   100.000 % 	 .../PipelineBuild

		    3169.276 Mt    11.879 %         1 x     3169.276 Mt        0.000 Mt        0.494 Mt     0.016 % 	 ../Initialization

		    3168.782 Mt    99.984 %         1 x     3168.782 Mt        0.000 Mt        2.434 Mt     0.077 % 	 .../ScenePrep

		    3075.135 Mt    97.045 %         1 x     3075.135 Mt        0.000 Mt      275.741 Mt     8.967 % 	 ..../SceneLoad

		    2013.806 Mt    65.487 %         1 x     2013.806 Mt        0.000 Mt      652.920 Mt    32.422 % 	 ...../glTF-LoadScene

		    1360.886 Mt    67.578 %        69 x       19.723 Mt        0.000 Mt     1360.886 Mt   100.000 % 	 ....../glTF-LoadImageData

		     507.936 Mt    16.518 %         1 x      507.936 Mt        0.000 Mt      507.936 Mt   100.000 % 	 ...../glTF-CreateScene

		     277.653 Mt     9.029 %         1 x      277.653 Mt        0.000 Mt      277.653 Mt   100.000 % 	 ...../SceneDesc-CreateScene

		       1.990 Mt     0.063 %         1 x        1.990 Mt        0.000 Mt        0.001 Mt     0.045 % 	 ..../ShadowMapRLPrep

		       1.989 Mt    99.955 %         1 x        1.989 Mt        0.000 Mt        1.946 Mt    97.823 % 	 ...../PrepareForRendering

		       0.043 Mt     2.177 %         1 x        0.043 Mt        0.000 Mt        0.043 Mt   100.000 % 	 ....../PrepareDrawBundle

		       5.362 Mt     0.169 %         1 x        5.362 Mt        0.000 Mt        0.001 Mt     0.013 % 	 ..../TexturedRLPrep

		       5.361 Mt    99.987 %         1 x        5.361 Mt        0.000 Mt        5.318 Mt    99.194 % 	 ...../PrepareForRendering

		       0.001 Mt     0.026 %         1 x        0.001 Mt        0.000 Mt        0.001 Mt   100.000 % 	 ....../PrepareMaterials

		       0.042 Mt     0.780 %         1 x        0.042 Mt        0.000 Mt        0.042 Mt   100.000 % 	 ....../PrepareDrawBundle

		       6.805 Mt     0.215 %         1 x        6.805 Mt        0.000 Mt        0.001 Mt     0.012 % 	 ..../DeferredRLPrep

		       6.804 Mt    99.988 %         1 x        6.804 Mt        0.000 Mt        4.566 Mt    67.105 % 	 ...../PrepareForRendering

		       0.002 Mt     0.024 %         1 x        0.002 Mt        0.000 Mt        0.002 Mt   100.000 % 	 ....../PrepareMaterials

		       2.123 Mt    31.207 %         1 x        2.123 Mt        0.000 Mt        2.123 Mt   100.000 % 	 ....../BuildMaterialCache

		       0.113 Mt     1.665 %         1 x        0.113 Mt        0.000 Mt        0.113 Mt   100.000 % 	 ....../PrepareDrawBundle

		      45.967 Mt     1.451 %         1 x       45.967 Mt        0.000 Mt        5.255 Mt    11.432 % 	 ..../RayTracingRLPrep

		       0.142 Mt     0.310 %         1 x        0.142 Mt        0.000 Mt        0.142 Mt   100.000 % 	 ...../PrepareAccelerationStructures

		       0.002 Mt     0.004 %         1 x        0.002 Mt        0.000 Mt        0.002 Mt   100.000 % 	 ...../PrepareCache

		      30.813 Mt    67.032 %         1 x       30.813 Mt        0.000 Mt       30.813 Mt   100.000 % 	 ...../PipelineBuild

		       1.224 Mt     2.662 %         1 x        1.224 Mt        0.000 Mt        1.224 Mt   100.000 % 	 ...../BuildCache

		       6.851 Mt    14.903 %         1 x        6.851 Mt        0.000 Mt        0.001 Mt     0.013 % 	 ...../BuildAccelerationStructures

		       6.850 Mt    99.987 %         1 x        6.850 Mt        0.000 Mt        3.896 Mt    56.873 % 	 ....../AccelerationStructureBuild

		       2.025 Mt    29.562 %         1 x        2.025 Mt        0.000 Mt        2.025 Mt   100.000 % 	 ......./BuildBottomLevelAS

		       0.929 Mt    13.566 %         1 x        0.929 Mt        0.000 Mt        0.929 Mt   100.000 % 	 ......./BuildTopLevelAS

		       1.681 Mt     3.657 %         1 x        1.681 Mt        0.000 Mt        1.681 Mt   100.000 % 	 ...../BuildShaderTables

		      31.089 Mt     0.981 %         1 x       31.089 Mt        0.000 Mt       31.089 Mt   100.000 % 	 ..../GpuSidePrep

	WindowThread:

		   26672.348 Mt   100.000 %         1 x    26672.348 Mt    26672.347 Mt    26672.348 Mt   100.000 % 	 /

	GpuThread:

		   19094.384 Mt   100.000 %      3041 x        6.279 Mt        7.825 Mt      162.815 Mt     0.853 % 	 /

		       2.725 Mt     0.014 %         1 x        2.725 Mt        0.000 Mt        0.006 Mt     0.214 % 	 ./ScenePrep

		       2.719 Mt    99.786 %         1 x        2.719 Mt        0.000 Mt        0.001 Mt     0.051 % 	 ../AccelerationStructureBuild

		       2.627 Mt    96.630 %         1 x        2.627 Mt        0.000 Mt        2.627 Mt   100.000 % 	 .../BuildBottomLevelASGpu

		       0.090 Mt     3.319 %         1 x        0.090 Mt        0.000 Mt        0.090 Mt   100.000 % 	 .../BuildTopLevelASGpu

		   18847.074 Mt    98.705 %      3041 x        6.198 Mt        7.822 Mt        7.897 Mt     0.042 % 	 ./RayTracingGpu

		    6469.506 Mt    34.326 %      3041 x        2.127 Mt        3.855 Mt     6469.506 Mt   100.000 % 	 ../DeferredRLGpu

		    6883.969 Mt    36.525 %      3041 x        2.264 Mt        2.213 Mt     6883.969 Mt   100.000 % 	 ../RayTracingRLGpu

		    5485.700 Mt    29.106 %      3041 x        1.804 Mt        1.754 Mt     5485.700 Mt   100.000 % 	 ../ResolveRLGpu

		      81.771 Mt     0.428 %      3041 x        0.027 Mt        0.003 Mt       81.771 Mt   100.000 % 	 ./Present


	============================


