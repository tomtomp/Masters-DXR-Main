Configuration: 
Command line parameters  = E:\programming\FIT-DIP\Masters-DXR-Main-git\build\Measurement\x64\Release\Measurement.exe --scene Suzanne/Suzanne.gltf --profile-automation --profile-camera-track Suzanne/Suzanne.trk --rt-mode --width 2560 --height 1440 --profile-output PresetSuzanne1440RtMedium --quality-preset Medium 
Build type               = Release
Device name              = NVIDIA GeForce RTX 2080 Ti
Device memory MB         = 11049
Ray Tracing backend      = Hardware
Window width             = 2560
Window height            = 1421
Window title             = Direct3D App
Back-buffer count        = 3
Fill Triangles           = enabled
Triangles Font Clock-Wise= enabled
Fast Build AS            = disabled
Target FPS               = 0
Target UPS               = 60
Tearing                  = enabled
VSync                    = disabled
GUI rendering            = disabled
Debug layer              = disabled
GPU validation           = disabled
GPU profiling            = enabled
Render mode              = Ray Tracing
Loaded scene file        = Suzanne/Suzanne.gltf
Using testing scene      = disabled
Duplication              = 1
Duplication in cube      = disabled
Duplication offset       = 10
BLAS duplication         = disabled
Rays per pixel           = 8
Quality preset           = Medium
Rasterization Quality    : 
  AO                     = enabled
  AO kernel size         = 4
  Shadows                = enabled
  Shadow map resolution  = 4096
  Shadow PCF kernel size = 4
  Reflections            = disabled
Ray Tracing Quality      : 
  AO                     = enabled
  AO rays                = 4
  AO kernel size         = 4
  Shadows                = enabled
  Shadow rays            = 4
  Shadow kernel size     = 4
  Reflections            = disabled

ProfilingAggregator: 
Render	,	3.9004	,	4.2605	,	3.9943	,	3.6106	,	3.2981	,	3.1274	,	2.9894	,	2.959	,	3.0438	,	3.2162	,	3.3318	,	3.223	,	3.0227	,	3.1123	,	3.1324	,	4.246	,	4.2109	,	4.8278	,	6.8732	,	5.0295	,	4.2838	,	3.3999	,	3.0846	,	3.0458
Update	,	0.0825	,	0.0741	,	0.0833	,	0.0798	,	0.0817	,	0.0826	,	0.0822	,	0.0834	,	0.0822	,	0.0816	,	0.0797	,	0.0889	,	0.0829	,	0.0823	,	0.0815	,	0.0905	,	0.0789	,	0.085	,	0.0826	,	0.0854	,	0.0821	,	0.0826	,	0.083	,	0.0807
RayTracing	,	0.174	,	0.1727	,	0.1743	,	0.1693	,	0.1736	,	0.1719	,	0.1701	,	0.1822	,	0.1738	,	0.1713	,	0.1714	,	0.1704	,	0.1687	,	0.1683	,	0.1732	,	0.1768	,	0.1725	,	0.1696	,	0.1711	,	0.177	,	0.1717	,	0.1685	,	0.1701	,	0.1693
RayTracingGpu	,	2.74637	,	3.14835	,	3.12979	,	2.92643	,	2.58237	,	2.34557	,	2.24806	,	2.22659	,	2.31757	,	2.5328	,	2.65184	,	2.45469	,	2.26813	,	2.26262	,	2.47021	,	3.2023	,	3.53136	,	4.088	,	5.91526	,	4.13274	,	3.39085	,	2.61491	,	2.35306	,	2.31024
DeferredRLGpu	,	0.13936	,	0.191296	,	0.208192	,	0.1584	,	0.101824	,	0.069856	,	0.053024	,	0.046048	,	0.062976	,	0.093728	,	0.106528	,	0.08	,	0.05472	,	0.055712	,	0.08864	,	0.186656	,	0.23136	,	0.262912	,	0.520192	,	0.3296	,	0.26624	,	0.135872	,	0.082304	,	0.069472
RayTracingRLGpu	,	0.435392	,	0.647488	,	0.792256	,	0.673376	,	0.422592	,	0.261408	,	0.2008	,	0.175936	,	0.24	,	0.394976	,	0.482752	,	0.344864	,	0.213824	,	0.212032	,	0.338816	,	0.759776	,	1.10086	,	1.5839	,	2.9441	,	1.54067	,	0.95664	,	0.42272	,	0.254976	,	0.224352
ResolveRLGpu	,	2.17011	,	2.30794	,	2.12685	,	2.0921	,	2.05613	,	2.01158	,	1.99274	,	2.00314	,	2.01318	,	2.04234	,	2.06099	,	2.02822	,	1.99805	,	1.99318	,	2.04099	,	2.25466	,	2.19718	,	2.23808	,	2.44835	,	2.25942	,	2.16493	,	2.05469	,	2.01315	,	2.01459
Index	,	0	,	1	,	2	,	3	,	4	,	5	,	6	,	7	,	8	,	9	,	10	,	11	,	12	,	13	,	14	,	15	,	16	,	17	,	18	,	19	,	20	,	21	,	22	,	23

ProfilingBuildAggregator: 
BuildBottomLevelAS	,	1.224
BuildBottomLevelASGpu	,	0.44272
BuildTopLevelAS	,	0.8759
BuildTopLevelASGpu	,	0.063712
Duplication	,	1
Triangles	,	3936
Meshes	,	1
BottomLevels	,	1
TopLevelSize	,	64
BottomLevelsSize	,	282112
Index	,	0

FpsAggregator: 
FPS	,	239	,	261	,	251	,	249	,	274	,	297	,	310	,	318	,	316	,	301	,	282	,	285	,	309	,	317	,	307	,	272	,	244	,	211	,	174	,	177	,	208	,	256	,	301	,	314
UPS	,	59	,	61	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	61	,	60	,	60	,	60	,	60	,	60	,	60	,	61	,	60	,	60	,	60	,	60
FrameTime	,	4.19474	,	3.84204	,	3.99989	,	4.02092	,	3.65222	,	3.36865	,	3.22864	,	3.14527	,	3.16504	,	3.32651	,	3.55719	,	3.51465	,	3.24477	,	3.1604	,	3.26677	,	3.68846	,	4.09877	,	4.74143	,	5.77724	,	5.6706	,	4.81518	,	3.91579	,	3.32305	,	3.19264
GigaRays/s	,	6.93775	,	7.57463	,	7.27572	,	7.23767	,	7.96833	,	8.63909	,	9.01372	,	9.25265	,	9.19484	,	8.74853	,	8.18121	,	8.28021	,	8.96892	,	9.20837	,	8.90853	,	7.89003	,	7.1002	,	6.13783	,	5.03737	,	5.1321	,	6.04382	,	7.43198	,	8.75765	,	9.11538
Index	,	0	,	1	,	2	,	3	,	4	,	5	,	6	,	7	,	8	,	9	,	10	,	11	,	12	,	13	,	14	,	15	,	16	,	17	,	18	,	19	,	20	,	21	,	22	,	23


AccelerationStructure: 
Ray tracing acceleration structure building statistics: 
	Bottom Level Acceleration Structure: 
		Maximum [B]: 282112
		Minimum [B]: 282112
		Currently Allocated [B]: 282112
		Currently Created [num]: 1
		Current Average [B]: 282112
		Maximum Triangles [num]: 3936
		Minimum Triangles [num]: 3936
		Total Triangles [num]: 3936
		Maximum Meshes [num]: 1
		Minimum Meshes [num]: 1
		Total Meshes [num]: 1
	Top Level Acceleration Structure: 
		Maximum [B]: 64
		Minimum [B]: 64
		Currently Allocated [B]: 64
		Total Allocated [B]: 64
		Total Created [num]: 1
		Average Allocated [B]: 64
		Maximum Instances [num]: 1
		Minimum Instances [num]: 1
		Total Instances [num]: 1
	Scratch Buffer: 
		Maximum [B]: 82944
		Minimum [B]: 82944
		Currently Allocated [B]: 82944
		Total Allocated [B]: 82944
		Total Created [num]: 1
		Average Allocated [B]: 82944
ProfilingStack: 
[PROF] Profiling statistics: 
	Scopes entered : 181998
	Scopes exited : 181997
	Overhead per scope [ticks] : 103.5
	Threads entered : 3
	Threads exited : 0
	============================
		         Sum[Mt]   Parent[%]  Samples[num]       Avg[Mt]   Last/Curr[Mt]        Self[Mt]     Self[%] 	 Name

	main:

		   24524.349 Mt   100.000 %         1 x    24524.350 Mt    24524.349 Mt        0.433 Mt     0.002 % 	 /

		   24523.952 Mt    99.998 %         1 x    24523.954 Mt    24523.952 Mt      269.351 Mt     1.098 % 	 ./Main application loop

		       1.524 Mt     0.006 %         1 x        1.524 Mt        0.000 Mt        1.524 Mt   100.000 % 	 ../ShadowMapRLInitialization

		       4.502 Mt     0.018 %         1 x        4.502 Mt        0.000 Mt        4.502 Mt   100.000 % 	 ../ResolveRLInitialization

		   24065.198 Mt    98.129 %     96334 x        0.250 Mt        3.878 Mt      440.982 Mt     1.832 % 	 ../MainLoop

		     205.228 Mt     0.853 %      1444 x        0.142 Mt        0.047 Mt      204.922 Mt    99.851 % 	 .../Update

		       0.306 Mt     0.149 %         1 x        0.306 Mt        0.000 Mt        0.306 Mt   100.000 % 	 ..../GuiModelApply

		   23418.988 Mt    97.315 %      6475 x        3.617 Mt        3.737 Mt      719.677 Mt     3.073 % 	 .../Render

		    1114.040 Mt     4.757 %      6475 x        0.172 Mt        0.207 Mt       44.220 Mt     3.969 % 	 ..../RayTracing

		     501.418 Mt    45.009 %      6475 x        0.077 Mt        0.089 Mt      495.238 Mt    98.767 % 	 ...../Deferred

		       6.181 Mt     1.233 %      6475 x        0.001 Mt        0.001 Mt        6.181 Mt   100.000 % 	 ....../DeferredRLPrep

		     394.527 Mt    35.414 %      6475 x        0.061 Mt        0.079 Mt      390.877 Mt    99.075 % 	 ...../RayCasting

		       3.651 Mt     0.925 %      6475 x        0.001 Mt        0.001 Mt        3.651 Mt   100.000 % 	 ....../RayTracingRLPrep

		     173.874 Mt    15.608 %      6475 x        0.027 Mt        0.032 Mt      173.874 Mt   100.000 % 	 ...../Resolve

		   21585.271 Mt    92.170 %      6475 x        3.334 Mt        3.398 Mt    21585.271 Mt   100.000 % 	 ..../Present

		       3.175 Mt     0.013 %         1 x        3.175 Mt        0.000 Mt        3.175 Mt   100.000 % 	 ../TexturedRLInitialization

		       3.290 Mt     0.013 %         1 x        3.290 Mt        0.000 Mt        3.290 Mt   100.000 % 	 ../AmbientOcclusionRLInitialization

		       6.229 Mt     0.025 %         1 x        6.229 Mt        0.000 Mt        6.229 Mt   100.000 % 	 ../RasterResolveRLInitialization

		       5.861 Mt     0.024 %         1 x        5.861 Mt        0.000 Mt        5.861 Mt   100.000 % 	 ../DeferredRLInitialization

		      55.817 Mt     0.228 %         1 x       55.817 Mt        0.000 Mt        2.445 Mt     4.380 % 	 ../RayTracingRLInitialization

		      53.372 Mt    95.620 %         1 x       53.372 Mt        0.000 Mt       53.372 Mt   100.000 % 	 .../PipelineBuild

		     109.006 Mt     0.444 %         1 x      109.006 Mt        0.000 Mt        0.180 Mt     0.165 % 	 ../Initialization

		     108.826 Mt    99.835 %         1 x      108.826 Mt        0.000 Mt        1.098 Mt     1.009 % 	 .../ScenePrep

		      64.758 Mt    59.506 %         1 x       64.758 Mt        0.000 Mt       16.798 Mt    25.939 % 	 ..../SceneLoad

		      39.461 Mt    60.937 %         1 x       39.461 Mt        0.000 Mt        8.509 Mt    21.564 % 	 ...../glTF-LoadScene

		      30.952 Mt    78.436 %         2 x       15.476 Mt        0.000 Mt       30.952 Mt   100.000 % 	 ....../glTF-LoadImageData

		       6.530 Mt    10.084 %         1 x        6.530 Mt        0.000 Mt        6.530 Mt   100.000 % 	 ...../glTF-CreateScene

		       1.969 Mt     3.040 %         1 x        1.969 Mt        0.000 Mt        1.969 Mt   100.000 % 	 ...../SceneDesc-CreateScene

		       0.312 Mt     0.287 %         1 x        0.312 Mt        0.000 Mt        0.000 Mt     0.128 % 	 ..../ShadowMapRLPrep

		       0.312 Mt    99.872 %         1 x        0.312 Mt        0.000 Mt        0.304 Mt    97.339 % 	 ...../PrepareForRendering

		       0.008 Mt     2.661 %         1 x        0.008 Mt        0.000 Mt        0.008 Mt   100.000 % 	 ....../PrepareDrawBundle

		       0.960 Mt     0.882 %         1 x        0.960 Mt        0.000 Mt        0.000 Mt     0.021 % 	 ..../TexturedRLPrep

		       0.959 Mt    99.979 %         1 x        0.959 Mt        0.000 Mt        0.955 Mt    99.573 % 	 ...../PrepareForRendering

		       0.001 Mt     0.125 %         1 x        0.001 Mt        0.000 Mt        0.001 Mt   100.000 % 	 ....../PrepareMaterials

		       0.003 Mt     0.302 %         1 x        0.003 Mt        0.000 Mt        0.003 Mt   100.000 % 	 ....../PrepareDrawBundle

		       2.040 Mt     1.875 %         1 x        2.040 Mt        0.000 Mt        0.001 Mt     0.025 % 	 ..../DeferredRLPrep

		       2.040 Mt    99.975 %         1 x        2.040 Mt        0.000 Mt        1.642 Mt    80.514 % 	 ...../PrepareForRendering

		       0.001 Mt     0.039 %         1 x        0.001 Mt        0.000 Mt        0.001 Mt   100.000 % 	 ....../PrepareMaterials

		       0.393 Mt    19.275 %         1 x        0.393 Mt        0.000 Mt        0.393 Mt   100.000 % 	 ....../BuildMaterialCache

		       0.004 Mt     0.172 %         1 x        0.004 Mt        0.000 Mt        0.004 Mt   100.000 % 	 ....../PrepareDrawBundle

		      28.015 Mt    25.743 %         1 x       28.015 Mt        0.000 Mt        2.858 Mt    10.201 % 	 ..../RayTracingRLPrep

		       0.046 Mt     0.163 %         1 x        0.046 Mt        0.000 Mt        0.046 Mt   100.000 % 	 ...../PrepareAccelerationStructures

		       0.001 Mt     0.003 %         1 x        0.001 Mt        0.000 Mt        0.001 Mt   100.000 % 	 ...../PrepareCache

		      18.108 Mt    64.638 %         1 x       18.108 Mt        0.000 Mt       18.108 Mt   100.000 % 	 ...../PipelineBuild

		       0.541 Mt     1.931 %         1 x        0.541 Mt        0.000 Mt        0.541 Mt   100.000 % 	 ...../BuildCache

		       4.940 Mt    17.632 %         1 x        4.940 Mt        0.000 Mt        0.001 Mt     0.010 % 	 ...../BuildAccelerationStructures

		       4.939 Mt    99.990 %         1 x        4.939 Mt        0.000 Mt        2.839 Mt    57.484 % 	 ....../AccelerationStructureBuild

		       1.224 Mt    24.782 %         1 x        1.224 Mt        0.000 Mt        1.224 Mt   100.000 % 	 ......./BuildBottomLevelAS

		       0.876 Mt    17.734 %         1 x        0.876 Mt        0.000 Mt        0.876 Mt   100.000 % 	 ......./BuildTopLevelAS

		       1.522 Mt     5.432 %         1 x        1.522 Mt        0.000 Mt        1.522 Mt   100.000 % 	 ...../BuildShaderTables

		      11.643 Mt    10.699 %         1 x       11.643 Mt        0.000 Mt       11.643 Mt   100.000 % 	 ..../GpuSidePrep

	WindowThread:

		   24523.256 Mt   100.000 %         1 x    24523.256 Mt    24523.255 Mt    24523.256 Mt   100.000 % 	 /

	GpuThread:

		   18413.590 Mt   100.000 %      6475 x        2.844 Mt        2.748 Mt      130.659 Mt     0.710 % 	 /

		       0.512 Mt     0.003 %         1 x        0.512 Mt        0.000 Mt        0.005 Mt     0.931 % 	 ./ScenePrep

		       0.508 Mt    99.069 %         1 x        0.508 Mt        0.000 Mt        0.001 Mt     0.233 % 	 ../AccelerationStructureBuild

		       0.443 Mt    87.216 %         1 x        0.443 Mt        0.000 Mt        0.443 Mt   100.000 % 	 .../BuildBottomLevelASGpu

		       0.064 Mt    12.551 %         1 x        0.064 Mt        0.000 Mt        0.064 Mt   100.000 % 	 .../BuildTopLevelASGpu

		   18147.374 Mt    98.554 %      6475 x        2.803 Mt        2.411 Mt       14.452 Mt     0.080 % 	 ./RayTracingGpu

		     855.869 Mt     4.716 %      6475 x        0.132 Mt        0.072 Mt      855.869 Mt   100.000 % 	 ../DeferredRLGpu

		    3608.552 Mt    19.885 %      6475 x        0.557 Mt        0.227 Mt     3608.552 Mt   100.000 % 	 ../RayTracingRLGpu

		   13668.502 Mt    75.319 %      6475 x        2.111 Mt        2.110 Mt    13668.502 Mt   100.000 % 	 ../ResolveRLGpu

		     135.045 Mt     0.733 %      6475 x        0.021 Mt        0.337 Mt      135.045 Mt   100.000 % 	 ./Present


	============================


