Configuration: 
Command line parameters  = E:\programming\FIT-DIP\Masters-DXR-Main-git\build\Measurement\x64\Release\Measurement.exe --scene Sponza/Sponza.gltf --profile-automation --profile-camera-track Sponza/SponzaPreset.trk --rt-mode --width 2560 --height 1440 --profile-output PresetSponza1440RtUltra --quality-preset Ultra 
Build type               = Release
Device name              = NVIDIA GeForce RTX 2080 Ti
Device memory MB         = 11049
Ray Tracing backend      = Hardware
Window width             = 2560
Window height            = 1421
Window title             = Direct3D App
Back-buffer count        = 3
Fill Triangles           = enabled
Triangles Font Clock-Wise= enabled
Fast Build AS            = disabled
Target FPS               = 0
Target UPS               = 60
Tearing                  = enabled
VSync                    = disabled
GUI rendering            = disabled
Debug layer              = disabled
GPU validation           = disabled
GPU profiling            = enabled
Render mode              = Ray Tracing
Loaded scene file        = Sponza/Sponza.gltf
Using testing scene      = disabled
Duplication              = 1
Duplication in cube      = disabled
Duplication offset       = 10
BLAS duplication         = disabled
Rays per pixel           = 33
Quality preset           = Ultra
Rasterization Quality    : 
  AO                     = enabled
  AO kernel size         = 16
  Shadows                = enabled
  Shadow map resolution  = 8192
  Shadow PCF kernel size = 8
  Reflections            = enabled
Ray Tracing Quality      : 
  AO                     = enabled
  AO rays                = 16
  AO kernel size         = 8
  Shadows                = enabled
  Shadow rays            = 8
  Shadow kernel size     = 8
  Reflections            = enabled

ProfilingAggregator: 
Render	,	25.2038	,	25.2134	,	23.9374	,	22.2508	,	22.3202	,	24.2301	,	24.162	,	25.124	,	24.5671	,	24.8727	,	24.8554	,	25.1893	,	25.6491	,	25.2789	,	22.863	,	22.7273	,	27.3554	,	26.0437	,	21.8128	,	23.126	,	23.4303	,	23.352	,	23.6346
Update	,	0.0451	,	0.083	,	0.0814	,	0.0794	,	0.045	,	0.0451	,	0.0822	,	0.0785	,	0.0189	,	0.0357	,	0.0832	,	0.0827	,	0.0855	,	0.0459	,	0.0825	,	0.0453	,	0.0858	,	0.0459	,	0.0854	,	0.083	,	0.0765	,	0.0754	,	0.085
RayTracing	,	0.2054	,	0.2676	,	0.2138	,	0.2027	,	0.2119	,	0.2505	,	0.2413	,	0.1932	,	0.1182	,	0.2348	,	0.2136	,	0.2129	,	0.2195	,	0.2069	,	0.2329	,	0.1952	,	0.2175	,	0.2708	,	0.2144	,	0.2378	,	0.2002	,	0.2193	,	0.2066
RayTracingGpu	,	24.2764	,	23.9625	,	22.5817	,	20.9684	,	21.1078	,	23.0995	,	22.8999	,	24.2801	,	24.0174	,	23.7268	,	23.6945	,	23.9577	,	24.4635	,	24.2328	,	21.8354	,	21.6938	,	26.1027	,	24.8173	,	20.7002	,	22.0773	,	22.3912	,	22.4276	,	22.4335
DeferredRLGpu	,	3.11949	,	2.53194	,	2.19613	,	1.4695	,	1.36083	,	1.58048	,	1.98941	,	2.38118	,	2.05165	,	1.50109	,	1.35622	,	1.76355	,	2.26659	,	2.44746	,	2.11859	,	1.58387	,	1.97094	,	1.85734	,	1.45738	,	3.05309	,	3.86848	,	3.88605	,	3.86406
RayTracingRLGpu	,	13.3222	,	13.7106	,	12.741	,	11.7769	,	12.0052	,	13.7344	,	13.1427	,	14.2325	,	14.3136	,	14.4923	,	14.6847	,	14.6061	,	14.5907	,	14.0916	,	11.9238	,	12.3211	,	16.3469	,	14.9891	,	11.3134	,	11.2911	,	10.7152	,	10.7645	,	10.7874
ResolveRLGpu	,	7.83066	,	7.7185	,	7.64301	,	7.72026	,	7.74	,	7.78304	,	7.76618	,	7.66365	,	7.65056	,	7.73018	,	7.6497	,	7.58518	,	7.60451	,	7.6903	,	7.79082	,	7.78726	,	7.78112	,	7.96883	,	7.9263	,	7.72864	,	7.80448	,	7.77514	,	7.78054
Index	,	0	,	1	,	2	,	3	,	4	,	5	,	6	,	7	,	8	,	9	,	10	,	11	,	12	,	13	,	14	,	15	,	16	,	17	,	18	,	19	,	20	,	21	,	22

ProfilingBuildAggregator: 
BuildBottomLevelAS	,	1.6775
BuildBottomLevelASGpu	,	2.35638
BuildTopLevelAS	,	0.8707
BuildTopLevelASGpu	,	0.089696
Duplication	,	1
Triangles	,	262267
Meshes	,	103
BottomLevels	,	1
TopLevelSize	,	64
BottomLevelsSize	,	18390656
Index	,	0

FpsAggregator: 
FPS	,	28	,	38	,	41	,	42	,	45	,	42	,	43	,	40	,	40	,	40	,	40	,	40	,	39	,	39	,	41	,	45	,	39	,	37	,	42	,	46	,	42	,	42	,	42
UPS	,	59	,	60	,	61	,	60	,	61	,	60	,	60	,	61	,	62	,	60	,	60	,	61	,	60	,	61	,	61	,	61	,	60	,	61	,	61	,	61	,	60	,	60	,	60
FrameTime	,	36.2224	,	26.4662	,	24.6919	,	23.8191	,	22.4191	,	23.8729	,	23.3768	,	25.6284	,	25.5081	,	25.0494	,	25.2271	,	25.3437	,	25.7427	,	25.8647	,	24.8591	,	22.3518	,	26.0682	,	27.2412	,	24.1956	,	22.1678	,	23.8987	,	23.8293	,	23.8317
GigaRays/s	,	3.31414	,	4.53583	,	4.86176	,	5.0399	,	5.35463	,	5.02855	,	5.13528	,	4.68411	,	4.7062	,	4.79238	,	4.75862	,	4.73672	,	4.6633	,	4.64131	,	4.82907	,	5.37075	,	4.60509	,	4.40678	,	4.96148	,	5.41533	,	5.02313	,	5.03774	,	5.03725
Index	,	0	,	1	,	2	,	3	,	4	,	5	,	6	,	7	,	8	,	9	,	10	,	11	,	12	,	13	,	14	,	15	,	16	,	17	,	18	,	19	,	20	,	21	,	22


AccelerationStructure: 
Ray tracing acceleration structure building statistics: 
	Bottom Level Acceleration Structure: 
		Maximum [B]: 18390656
		Minimum [B]: 18390656
		Currently Allocated [B]: 18390656
		Currently Created [num]: 1
		Current Average [B]: 18390656
		Maximum Triangles [num]: 262267
		Minimum Triangles [num]: 262267
		Total Triangles [num]: 262267
		Maximum Meshes [num]: 103
		Minimum Meshes [num]: 103
		Total Meshes [num]: 103
	Top Level Acceleration Structure: 
		Maximum [B]: 64
		Minimum [B]: 64
		Currently Allocated [B]: 64
		Total Allocated [B]: 64
		Total Created [num]: 1
		Average Allocated [B]: 64
		Maximum Instances [num]: 1
		Minimum Instances [num]: 1
		Total Instances [num]: 1
	Scratch Buffer: 
		Maximum [B]: 14995072
		Minimum [B]: 14995072
		Currently Allocated [B]: 14995072
		Total Allocated [B]: 14995072
		Total Created [num]: 1
		Average Allocated [B]: 14995072
ProfilingStack: 
[PROF] Profiling statistics: 
	Scopes entered : 39855
	Scopes exited : 39854
	Overhead per scope [ticks] : 102.3
	Threads entered : 3
	Threads exited : 0
	============================
		         Sum[Mt]   Parent[%]  Samples[num]       Avg[Mt]   Last/Curr[Mt]        Self[Mt]     Self[%] 	 Name

	main:

		   26357.205 Mt   100.000 %         1 x    26357.206 Mt    26357.205 Mt        0.328 Mt     0.001 % 	 /

		   26356.908 Mt    99.999 %         1 x    26356.910 Mt    26356.908 Mt      270.309 Mt     1.026 % 	 ./Main application loop

		       1.485 Mt     0.006 %         1 x        1.485 Mt        0.000 Mt        1.485 Mt   100.000 % 	 ../ShadowMapRLInitialization

		       4.553 Mt     0.017 %         1 x        4.553 Mt        0.000 Mt        4.553 Mt   100.000 % 	 ../ResolveRLInitialization

		   23261.368 Mt    88.255 %     26208 x        0.888 Mt       40.557 Mt      446.078 Mt     1.918 % 	 ../MainLoop

		     201.422 Mt     0.866 %      1393 x        0.145 Mt        0.036 Mt      200.789 Mt    99.686 % 	 .../Update

		       0.633 Mt     0.314 %         1 x        0.633 Mt        0.000 Mt        0.633 Mt   100.000 % 	 ..../GuiModelApply

		   22613.869 Mt    97.216 %       934 x       24.212 Mt       24.165 Mt      142.209 Mt     0.629 % 	 .../Render

		     212.046 Mt     0.938 %       934 x        0.227 Mt        0.275 Mt        7.747 Mt     3.654 % 	 ..../RayTracing

		      91.122 Mt    42.973 %       934 x        0.098 Mt        0.127 Mt       90.425 Mt    99.234 % 	 ...../Deferred

		       0.698 Mt     0.766 %       934 x        0.001 Mt        0.001 Mt        0.698 Mt   100.000 % 	 ....../DeferredRLPrep

		      78.262 Mt    36.908 %       934 x        0.084 Mt        0.093 Mt       77.667 Mt    99.240 % 	 ...../RayCasting

		       0.595 Mt     0.760 %       934 x        0.001 Mt        0.001 Mt        0.595 Mt   100.000 % 	 ....../RayTracingRLPrep

		      34.915 Mt    16.466 %       934 x        0.037 Mt        0.046 Mt       34.915 Mt   100.000 % 	 ...../Resolve

		   22259.614 Mt    98.433 %       934 x       23.833 Mt       23.712 Mt    22259.614 Mt   100.000 % 	 ..../Present

		       3.228 Mt     0.012 %         1 x        3.228 Mt        0.000 Mt        3.228 Mt   100.000 % 	 ../TexturedRLInitialization

		       3.257 Mt     0.012 %         1 x        3.257 Mt        0.000 Mt        3.257 Mt   100.000 % 	 ../AmbientOcclusionRLInitialization

		       6.482 Mt     0.025 %         1 x        6.482 Mt        0.000 Mt        6.482 Mt   100.000 % 	 ../RasterResolveRLInitialization

		       5.847 Mt     0.022 %         1 x        5.847 Mt        0.000 Mt        5.847 Mt   100.000 % 	 ../DeferredRLInitialization

		      71.692 Mt     0.272 %         1 x       71.692 Mt        0.000 Mt        2.274 Mt     3.172 % 	 ../RayTracingRLInitialization

		      69.418 Mt    96.828 %         1 x       69.418 Mt        0.000 Mt       69.418 Mt   100.000 % 	 .../PipelineBuild

		    2728.687 Mt    10.353 %         1 x     2728.687 Mt        0.000 Mt        0.347 Mt     0.013 % 	 ../Initialization

		    2728.340 Mt    99.987 %         1 x     2728.340 Mt        0.000 Mt        2.338 Mt     0.086 % 	 .../ScenePrep

		    2653.375 Mt    97.252 %         1 x     2653.375 Mt        0.000 Mt      250.750 Mt     9.450 % 	 ..../SceneLoad

		    2096.937 Mt    79.029 %         1 x     2096.937 Mt        0.000 Mt      586.575 Mt    27.973 % 	 ...../glTF-LoadScene

		    1510.362 Mt    72.027 %        69 x       21.889 Mt        0.000 Mt     1510.362 Mt   100.000 % 	 ....../glTF-LoadImageData

		     238.603 Mt     8.992 %         1 x      238.603 Mt        0.000 Mt      238.603 Mt   100.000 % 	 ...../glTF-CreateScene

		      67.085 Mt     2.528 %         1 x       67.085 Mt        0.000 Mt       67.085 Mt   100.000 % 	 ...../SceneDesc-CreateScene

		       1.008 Mt     0.037 %         1 x        1.008 Mt        0.000 Mt        0.000 Mt     0.050 % 	 ..../ShadowMapRLPrep

		       1.007 Mt    99.950 %         1 x        1.007 Mt        0.000 Mt        0.962 Mt    95.513 % 	 ...../PrepareForRendering

		       0.045 Mt     4.487 %         1 x        0.045 Mt        0.000 Mt        0.045 Mt   100.000 % 	 ....../PrepareDrawBundle

		       4.026 Mt     0.148 %         1 x        4.026 Mt        0.000 Mt        0.001 Mt     0.017 % 	 ..../TexturedRLPrep

		       4.026 Mt    99.983 %         1 x        4.026 Mt        0.000 Mt        3.987 Mt    99.044 % 	 ...../PrepareForRendering

		       0.002 Mt     0.042 %         1 x        0.002 Mt        0.000 Mt        0.002 Mt   100.000 % 	 ....../PrepareMaterials

		       0.037 Mt     0.914 %         1 x        0.037 Mt        0.000 Mt        0.037 Mt   100.000 % 	 ....../PrepareDrawBundle

		       5.255 Mt     0.193 %         1 x        5.255 Mt        0.000 Mt        0.001 Mt     0.011 % 	 ..../DeferredRLPrep

		       5.254 Mt    99.989 %         1 x        5.254 Mt        0.000 Mt        4.750 Mt    90.397 % 	 ...../PrepareForRendering

		       0.002 Mt     0.034 %         1 x        0.002 Mt        0.000 Mt        0.002 Mt   100.000 % 	 ....../PrepareMaterials

		       0.344 Mt     6.556 %         1 x        0.344 Mt        0.000 Mt        0.344 Mt   100.000 % 	 ....../BuildMaterialCache

		       0.158 Mt     3.013 %         1 x        0.158 Mt        0.000 Mt        0.158 Mt   100.000 % 	 ....../PrepareDrawBundle

		      28.531 Mt     1.046 %         1 x       28.531 Mt        0.000 Mt        3.294 Mt    11.547 % 	 ..../RayTracingRLPrep

		       0.174 Mt     0.610 %         1 x        0.174 Mt        0.000 Mt        0.174 Mt   100.000 % 	 ...../PrepareAccelerationStructures

		       0.003 Mt     0.010 %         1 x        0.003 Mt        0.000 Mt        0.003 Mt   100.000 % 	 ...../PrepareCache

		      12.394 Mt    43.442 %         1 x       12.394 Mt        0.000 Mt       12.394 Mt   100.000 % 	 ...../PipelineBuild

		       0.658 Mt     2.307 %         1 x        0.658 Mt        0.000 Mt        0.658 Mt   100.000 % 	 ...../BuildCache

		       9.815 Mt    34.401 %         1 x        9.815 Mt        0.000 Mt        0.000 Mt     0.004 % 	 ...../BuildAccelerationStructures

		       9.815 Mt    99.996 %         1 x        9.815 Mt        0.000 Mt        7.266 Mt    74.036 % 	 ....../AccelerationStructureBuild

		       1.677 Mt    17.092 %         1 x        1.677 Mt        0.000 Mt        1.677 Mt   100.000 % 	 ......./BuildBottomLevelAS

		       0.871 Mt     8.872 %         1 x        0.871 Mt        0.000 Mt        0.871 Mt   100.000 % 	 ......./BuildTopLevelAS

		       2.192 Mt     7.684 %         1 x        2.192 Mt        0.000 Mt        2.192 Mt   100.000 % 	 ...../BuildShaderTables

		      33.807 Mt     1.239 %         1 x       33.807 Mt        0.000 Mt       33.807 Mt   100.000 % 	 ..../GpuSidePrep

	WindowThread:

		   26356.446 Mt   100.000 %         1 x    26356.446 Mt    26356.446 Mt    26356.446 Mt   100.000 % 	 /

	GpuThread:

		   21742.634 Mt   100.000 %       934 x       23.279 Mt       22.880 Mt      156.567 Mt     0.720 % 	 /

		       2.454 Mt     0.011 %         1 x        2.454 Mt        0.000 Mt        0.006 Mt     0.245 % 	 ./ScenePrep

		       2.447 Mt    99.755 %         1 x        2.447 Mt        0.000 Mt        0.001 Mt     0.058 % 	 ../AccelerationStructureBuild

		       2.356 Mt    96.278 %         1 x        2.356 Mt        0.000 Mt        2.356 Mt   100.000 % 	 .../BuildBottomLevelASGpu

		       0.090 Mt     3.665 %         1 x        0.090 Mt        0.000 Mt        0.090 Mt   100.000 % 	 .../BuildTopLevelASGpu

		   21522.413 Mt    98.987 %       934 x       23.043 Mt       22.785 Mt        2.277 Mt     0.011 % 	 ./RayTracingGpu

		    2057.119 Mt     9.558 %       934 x        2.202 Mt        4.230 Mt     2057.119 Mt   100.000 % 	 ../DeferredRLGpu

		   12203.709 Mt    56.702 %       934 x       13.066 Mt       10.881 Mt    12203.709 Mt   100.000 % 	 ../RayTracingRLGpu

		    7259.308 Mt    33.729 %       934 x        7.772 Mt        7.673 Mt     7259.308 Mt   100.000 % 	 ../ResolveRLGpu

		      61.201 Mt     0.281 %       934 x        0.066 Mt        0.094 Mt       61.201 Mt   100.000 % 	 ./Present


	============================


