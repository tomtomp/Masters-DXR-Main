Configuration: 
Command line parameters  = E:\programming\FIT-DIP\Masters-DXR-Main-git\build\Measurement\x64\Release\Measurement.exe --scene Suzanne/Suzanne.gltf --profile-automation --profile-camera-track Suzanne/Suzanne.trk --rt-mode --width 2560 --height 1440 --profile-output PresetSuzanne1440RtSoftShadows --quality-preset SoftShadows 
Build type               = Release
Device name              = NVIDIA GeForce RTX 2080 Ti
Device memory MB         = 11049
Ray Tracing backend      = Hardware
Window width             = 2560
Window height            = 1421
Window title             = Direct3D App
Back-buffer count        = 3
Fill Triangles           = enabled
Triangles Font Clock-Wise= enabled
Fast Build AS            = disabled
Target FPS               = 0
Target UPS               = 60
Tearing                  = enabled
VSync                    = disabled
GUI rendering            = disabled
Debug layer              = disabled
GPU validation           = disabled
GPU profiling            = enabled
Render mode              = Ray Tracing
Loaded scene file        = Suzanne/Suzanne.gltf
Using testing scene      = disabled
Duplication              = 1
Duplication in cube      = disabled
Duplication offset       = 10
BLAS duplication         = disabled
Rays per pixel           = 4
Quality preset           = SoftShadows
Rasterization Quality    : 
  AO                     = disabled
  AO kernel size         = 1
  Shadows                = enabled
  Shadow map resolution  = 4096
  Shadow PCF kernel size = 4
  Reflections            = disabled
Ray Tracing Quality      : 
  AO                     = disabled
  AO rays                = 0
  AO kernel size         = 4
  Shadows                = enabled
  Shadow rays            = 4
  Shadow kernel size     = 4
  Reflections            = disabled

ProfilingAggregator: 
Render	,	2.5402	,	2.6888	,	2.8931	,	2.6638	,	2.4414	,	2.28	,	2.2192	,	2.2747	,	2.3073	,	2.4346	,	2.5214	,	2.3651	,	2.1696	,	2.2446	,	2.3109	,	3.2054	,	3.5629	,	3.8727	,	4.7159	,	4.0814	,	3.5242	,	2.9316	,	2.5781	,	2.75
Update	,	0.0818	,	0.0832	,	0.0822	,	0.0833	,	0.0833	,	0.0828	,	0.083	,	0.0724	,	0.0746	,	0.0843	,	0.0818	,	0.0779	,	0.082	,	0.0951	,	0.0651	,	0.0846	,	0.0819	,	0.0715	,	0.0824	,	0.0804	,	0.0815	,	0.0844	,	0.0848	,	0.0822
RayTracing	,	0.2017	,	0.1792	,	0.1741	,	0.17	,	0.1709	,	0.141	,	0.1722	,	0.174	,	0.1451	,	0.1474	,	0.1748	,	0.1697	,	0.1828	,	0.1534	,	0.1303	,	0.1704	,	0.1667	,	0.1384	,	0.1765	,	0.1714	,	0.173	,	0.1421	,	0.1454	,	0.1538
RayTracingGpu	,	1.74909	,	1.95325	,	2.07587	,	1.94224	,	1.70768	,	1.56502	,	1.49494	,	1.4768	,	1.53869	,	1.67872	,	1.74906	,	1.62589	,	1.50483	,	1.51162	,	1.66477	,	2.4104	,	2.67302	,	3.252	,	3.85146	,	2.93949	,	2.39421	,	1.84771	,	1.57917	,	1.74109
DeferredRLGpu	,	0.142176	,	0.1896	,	0.208544	,	0.164544	,	0.101856	,	0.069088	,	0.052768	,	0.045728	,	0.06272	,	0.093088	,	0.106112	,	0.0808	,	0.05456	,	0.054848	,	0.10704	,	0.224448	,	0.227872	,	0.2632	,	0.407264	,	0.327904	,	0.266272	,	0.13776	,	0.083296	,	0.072672
RayTracingRLGpu	,	0.27824	,	0.389536	,	0.464512	,	0.402112	,	0.268	,	0.183808	,	0.151136	,	0.13936	,	0.172864	,	0.253952	,	0.300736	,	0.22656	,	0.157056	,	0.155392	,	0.22128	,	0.447008	,	0.63856	,	1.25501	,	1.49354	,	0.965888	,	0.58512	,	0.275744	,	0.183744	,	0.16608
ResolveRLGpu	,	1.32656	,	1.37238	,	1.4007	,	1.37238	,	1.33526	,	1.30886	,	1.28966	,	1.28979	,	1.29955	,	1.32867	,	1.34054	,	1.31702	,	1.29187	,	1.29917	,	1.33331	,	1.73696	,	1.80445	,	1.73034	,	1.94861	,	1.64298	,	1.54122	,	1.43213	,	1.30723	,	1.50061
Index	,	0	,	1	,	2	,	3	,	4	,	5	,	6	,	7	,	8	,	9	,	10	,	11	,	12	,	13	,	14	,	15	,	16	,	17	,	18	,	19	,	20	,	21	,	22	,	23

ProfilingBuildAggregator: 
BuildBottomLevelAS	,	1.1639
BuildBottomLevelASGpu	,	0.439616
BuildTopLevelAS	,	1.1378
BuildTopLevelASGpu	,	0.061728
Duplication	,	1
Triangles	,	3936
Meshes	,	1
BottomLevels	,	1
TopLevelSize	,	64
BottomLevelsSize	,	282112
Index	,	0

FpsAggregator: 
FPS	,	326	,	334	,	331	,	330	,	362	,	395	,	407	,	418	,	412	,	405	,	374	,	382	,	403	,	416	,	404	,	361	,	312	,	300	,	241	,	245	,	276	,	339	,	390	,	399
UPS	,	59	,	60	,	60	,	60	,	61	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	61	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60
FrameTime	,	3.06973	,	2.99409	,	3.02241	,	3.03334	,	2.76769	,	2.53412	,	2.45736	,	2.39724	,	2.43254	,	2.47158	,	2.67915	,	2.62387	,	2.48383	,	2.4081	,	2.47934	,	2.77381	,	3.21049	,	3.33857	,	4.1541	,	4.08218	,	3.62748	,	2.95245	,	2.56598	,	2.50734
GigaRays/s	,	4.74017	,	4.85992	,	4.81438	,	4.79703	,	5.25747	,	5.74204	,	5.92141	,	6.0699	,	5.98184	,	5.88734	,	5.43122	,	5.54564	,	5.8583	,	6.04254	,	5.86891	,	5.24588	,	4.53234	,	4.35846	,	3.50282	,	3.56453	,	4.01134	,	4.92846	,	5.67075	,	5.80338
Index	,	0	,	1	,	2	,	3	,	4	,	5	,	6	,	7	,	8	,	9	,	10	,	11	,	12	,	13	,	14	,	15	,	16	,	17	,	18	,	19	,	20	,	21	,	22	,	23


AccelerationStructure: 
Ray tracing acceleration structure building statistics: 
	Bottom Level Acceleration Structure: 
		Maximum [B]: 282112
		Minimum [B]: 282112
		Currently Allocated [B]: 282112
		Currently Created [num]: 1
		Current Average [B]: 282112
		Maximum Triangles [num]: 3936
		Minimum Triangles [num]: 3936
		Total Triangles [num]: 3936
		Maximum Meshes [num]: 1
		Minimum Meshes [num]: 1
		Total Meshes [num]: 1
	Top Level Acceleration Structure: 
		Maximum [B]: 64
		Minimum [B]: 64
		Currently Allocated [B]: 64
		Total Allocated [B]: 64
		Total Created [num]: 1
		Average Allocated [B]: 64
		Maximum Instances [num]: 1
		Minimum Instances [num]: 1
		Total Instances [num]: 1
	Scratch Buffer: 
		Maximum [B]: 82944
		Minimum [B]: 82944
		Currently Allocated [B]: 82944
		Total Allocated [B]: 82944
		Total Created [num]: 1
		Average Allocated [B]: 82944
ProfilingStack: 
[PROF] Profiling statistics: 
	Scopes entered : 151865
	Scopes exited : 151864
	Overhead per scope [ticks] : 102.9
	Threads entered : 3
	Threads exited : 0
	============================
		         Sum[Mt]   Parent[%]  Samples[num]       Avg[Mt]   Last/Curr[Mt]        Self[Mt]     Self[%] 	 Name

	main:

		   24624.051 Mt   100.000 %         1 x    24624.052 Mt    24624.051 Mt        0.483 Mt     0.002 % 	 /

		   24623.594 Mt    99.998 %         1 x    24623.595 Mt    24623.594 Mt      268.375 Mt     1.090 % 	 ./Main application loop

		       1.596 Mt     0.006 %         1 x        1.596 Mt        0.000 Mt        1.596 Mt   100.000 % 	 ../ShadowMapRLInitialization

		       4.475 Mt     0.018 %         1 x        4.475 Mt        0.000 Mt        4.475 Mt   100.000 % 	 ../ResolveRLInitialization

		   24080.341 Mt    97.794 %     39043 x        0.617 Mt        2.395 Mt     1316.416 Mt     5.467 % 	 ../MainLoop

		     195.059 Mt     0.810 %      1445 x        0.135 Mt        0.033 Mt      194.767 Mt    99.850 % 	 .../Update

		       0.293 Mt     0.150 %         1 x        0.293 Mt        0.000 Mt        0.293 Mt   100.000 % 	 ..../GuiModelApply

		   22568.865 Mt    93.723 %      8564 x        2.635 Mt        2.209 Mt      915.710 Mt     4.057 % 	 .../Render

		    1423.921 Mt     6.309 %      8564 x        0.166 Mt        0.170 Mt       56.240 Mt     3.950 % 	 ..../RayTracing

		     649.164 Mt    45.590 %      8564 x        0.076 Mt        0.077 Mt      640.127 Mt    98.608 % 	 ...../Deferred

		       9.037 Mt     1.392 %      8564 x        0.001 Mt        0.001 Mt        9.037 Mt   100.000 % 	 ....../DeferredRLPrep

		     497.047 Mt    34.907 %      8564 x        0.058 Mt        0.062 Mt      492.161 Mt    99.017 % 	 ...../RayCasting

		       4.886 Mt     0.983 %      8564 x        0.001 Mt        0.001 Mt        4.886 Mt   100.000 % 	 ....../RayTracingRLPrep

		     221.471 Mt    15.554 %      8564 x        0.026 Mt        0.024 Mt      221.471 Mt   100.000 % 	 ...../Resolve

		   20229.234 Mt    89.633 %      8564 x        2.362 Mt        1.933 Mt    20229.234 Mt   100.000 % 	 ..../Present

		       2.790 Mt     0.011 %         1 x        2.790 Mt        0.000 Mt        2.790 Mt   100.000 % 	 ../TexturedRLInitialization

		       3.409 Mt     0.014 %         1 x        3.409 Mt        0.000 Mt        3.409 Mt   100.000 % 	 ../AmbientOcclusionRLInitialization

		       4.153 Mt     0.017 %         1 x        4.153 Mt        0.000 Mt        4.153 Mt   100.000 % 	 ../RasterResolveRLInitialization

		       5.226 Mt     0.021 %         1 x        5.226 Mt        0.000 Mt        5.226 Mt   100.000 % 	 ../DeferredRLInitialization

		      65.129 Mt     0.265 %         1 x       65.129 Mt        0.000 Mt        2.778 Mt     4.266 % 	 ../RayTracingRLInitialization

		      62.351 Mt    95.734 %         1 x       62.351 Mt        0.000 Mt       62.351 Mt   100.000 % 	 .../PipelineBuild

		     188.100 Mt     0.764 %         1 x      188.100 Mt        0.000 Mt        0.494 Mt     0.263 % 	 ../Initialization

		     187.606 Mt    99.737 %         1 x      187.606 Mt        0.000 Mt        2.392 Mt     1.275 % 	 .../ScenePrep

		     116.918 Mt    62.321 %         1 x      116.918 Mt        0.000 Mt       22.427 Mt    19.182 % 	 ..../SceneLoad

		      64.330 Mt    55.021 %         1 x       64.330 Mt        0.000 Mt       12.666 Mt    19.688 % 	 ...../glTF-LoadScene

		      51.664 Mt    80.312 %         2 x       25.832 Mt        0.000 Mt       51.664 Mt   100.000 % 	 ....../glTF-LoadImageData

		      14.911 Mt    12.754 %         1 x       14.911 Mt        0.000 Mt       14.911 Mt   100.000 % 	 ...../glTF-CreateScene

		      15.250 Mt    13.043 %         1 x       15.250 Mt        0.000 Mt       15.250 Mt   100.000 % 	 ...../SceneDesc-CreateScene

		       0.964 Mt     0.514 %         1 x        0.964 Mt        0.000 Mt        0.001 Mt     0.125 % 	 ..../ShadowMapRLPrep

		       0.963 Mt    99.875 %         1 x        0.963 Mt        0.000 Mt        0.943 Mt    97.922 % 	 ...../PrepareForRendering

		       0.020 Mt     2.078 %         1 x        0.020 Mt        0.000 Mt        0.020 Mt   100.000 % 	 ....../PrepareDrawBundle

		       2.520 Mt     1.343 %         1 x        2.520 Mt        0.000 Mt        0.001 Mt     0.032 % 	 ..../TexturedRLPrep

		       2.519 Mt    99.968 %         1 x        2.519 Mt        0.000 Mt        2.509 Mt    99.623 % 	 ...../PrepareForRendering

		       0.003 Mt     0.115 %         1 x        0.003 Mt        0.000 Mt        0.003 Mt   100.000 % 	 ....../PrepareMaterials

		       0.007 Mt     0.262 %         1 x        0.007 Mt        0.000 Mt        0.007 Mt   100.000 % 	 ....../PrepareDrawBundle

		       3.812 Mt     2.032 %         1 x        3.812 Mt        0.000 Mt        0.001 Mt     0.026 % 	 ..../DeferredRLPrep

		       3.811 Mt    99.974 %         1 x        3.811 Mt        0.000 Mt        3.029 Mt    79.469 % 	 ...../PrepareForRendering

		       0.002 Mt     0.039 %         1 x        0.002 Mt        0.000 Mt        0.002 Mt   100.000 % 	 ....../PrepareMaterials

		       0.775 Mt    20.334 %         1 x        0.775 Mt        0.000 Mt        0.775 Mt   100.000 % 	 ....../BuildMaterialCache

		       0.006 Mt     0.157 %         1 x        0.006 Mt        0.000 Mt        0.006 Mt   100.000 % 	 ....../PrepareDrawBundle

		      48.803 Mt    26.014 %         1 x       48.803 Mt        0.000 Mt        4.178 Mt     8.561 % 	 ..../RayTracingRLPrep

		       0.129 Mt     0.265 %         1 x        0.129 Mt        0.000 Mt        0.129 Mt   100.000 % 	 ...../PrepareAccelerationStructures

		       0.002 Mt     0.003 %         1 x        0.002 Mt        0.000 Mt        0.002 Mt   100.000 % 	 ...../PrepareCache

		      31.449 Mt    64.441 %         1 x       31.449 Mt        0.000 Mt       31.449 Mt   100.000 % 	 ...../PipelineBuild

		       0.817 Mt     1.674 %         1 x        0.817 Mt        0.000 Mt        0.817 Mt   100.000 % 	 ...../BuildCache

		       6.193 Mt    12.690 %         1 x        6.193 Mt        0.000 Mt        0.001 Mt     0.011 % 	 ...../BuildAccelerationStructures

		       6.192 Mt    99.989 %         1 x        6.192 Mt        0.000 Mt        3.891 Mt    62.830 % 	 ....../AccelerationStructureBuild

		       1.164 Mt    18.796 %         1 x        1.164 Mt        0.000 Mt        1.164 Mt   100.000 % 	 ......./BuildBottomLevelAS

		       1.138 Mt    18.374 %         1 x        1.138 Mt        0.000 Mt        1.138 Mt   100.000 % 	 ......./BuildTopLevelAS

		       6.035 Mt    12.365 %         1 x        6.035 Mt        0.000 Mt        6.035 Mt   100.000 % 	 ...../BuildShaderTables

		      12.197 Mt     6.501 %         1 x       12.197 Mt        0.000 Mt       12.197 Mt   100.000 % 	 ..../GpuSidePrep

	WindowThread:

		   24617.007 Mt   100.000 %         1 x    24617.007 Mt    24617.007 Mt    24617.007 Mt   100.000 % 	 /

	GpuThread:

		   16172.620 Mt   100.000 %      8564 x        1.888 Mt        1.532 Mt      128.177 Mt     0.793 % 	 /

		       0.507 Mt     0.003 %         1 x        0.507 Mt        0.000 Mt        0.005 Mt     0.959 % 	 ./ScenePrep

		       0.502 Mt    99.041 %         1 x        0.502 Mt        0.000 Mt        0.001 Mt     0.191 % 	 ../AccelerationStructureBuild

		       0.440 Mt    87.520 %         1 x        0.440 Mt        0.000 Mt        0.440 Mt   100.000 % 	 .../BuildBottomLevelASGpu

		       0.062 Mt    12.289 %         1 x        0.062 Mt        0.000 Mt        0.062 Mt   100.000 % 	 .../BuildTopLevelASGpu

		   15930.068 Mt    98.500 %      8564 x        1.860 Mt        1.530 Mt       21.683 Mt     0.136 % 	 ./RayTracingGpu

		    1135.906 Mt     7.131 %      8564 x        0.133 Mt        0.071 Mt     1135.906 Mt   100.000 % 	 ../DeferredRLGpu

		    3001.256 Mt    18.840 %      8564 x        0.350 Mt        0.164 Mt     3001.256 Mt   100.000 % 	 ../RayTracingRLGpu

		   11771.222 Mt    73.893 %      8564 x        1.375 Mt        1.294 Mt    11771.222 Mt   100.000 % 	 ../ResolveRLGpu

		     113.868 Mt     0.704 %      8564 x        0.013 Mt        0.002 Mt      113.868 Mt   100.000 % 	 ./Present


	============================


