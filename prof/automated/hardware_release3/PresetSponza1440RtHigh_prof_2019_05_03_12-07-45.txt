Configuration: 
Command line parameters  = E:\programming\FIT-DIP\Masters-DXR-Main-git\build\Measurement\x64\Release\Measurement.exe --scene Sponza/Sponza.gltf --profile-automation --profile-camera-track Sponza/SponzaPreset.trk --rt-mode --width 2560 --height 1440 --profile-output PresetSponza1440RtHigh --quality-preset High 
Build type               = Release
Device name              = NVIDIA GeForce RTX 2080 Ti
Device memory MB         = 11049
Ray Tracing backend      = Hardware
Window width             = 2560
Window height            = 1421
Window title             = Direct3D App
Back-buffer count        = 3
Fill Triangles           = enabled
Triangles Font Clock-Wise= enabled
Fast Build AS            = disabled
Target FPS               = 0
Target UPS               = 60
Tearing                  = enabled
VSync                    = disabled
GUI rendering            = disabled
Debug layer              = disabled
GPU validation           = disabled
GPU profiling            = enabled
Render mode              = Ray Tracing
Loaded scene file        = Sponza/Sponza.gltf
Using testing scene      = disabled
Duplication              = 1
Duplication in cube      = disabled
Duplication offset       = 10
BLAS duplication         = disabled
Rays per pixel           = 13
Quality preset           = High
Rasterization Quality    : 
  AO                     = enabled
  AO kernel size         = 4
  Shadows                = enabled
  Shadow map resolution  = 4096
  Shadow PCF kernel size = 4
  Reflections            = enabled
Ray Tracing Quality      : 
  AO                     = enabled
  AO rays                = 4
  AO kernel size         = 4
  Shadows                = enabled
  Shadow rays            = 4
  Shadow kernel size     = 4
  Reflections            = enabled

ProfilingAggregator: 
Render	,	11.1665	,	10.5709	,	10.7056	,	10.0665	,	8.9837	,	10.3055	,	10.8307	,	11.6387	,	12.02	,	11.5024	,	11.5953	,	11.7106	,	12.1697	,	12.1603	,	10.62	,	9.9253	,	12.759	,	12.081	,	9.1185	,	10.2297	,	11.2227	,	11.0998	,	11.1383
Update	,	0.0742	,	0.03	,	0.0736	,	0.0717	,	0.0316	,	0.0341	,	0.0369	,	0.0726	,	0.0753	,	0.0774	,	0.0832	,	0.0749	,	0.0767	,	0.0774	,	0.0805	,	0.0762	,	0.0848	,	0.0693	,	0.0871	,	0.0847	,	0.0844	,	0.0871	,	0.0808
RayTracing	,	0.16	,	0.0622	,	0.1592	,	0.1482	,	0.1559	,	0.1948	,	0.0789	,	0.1901	,	0.1567	,	0.1516	,	0.1948	,	0.1664	,	0.1638	,	0.1821	,	0.1812	,	0.178	,	0.2092	,	0.1585	,	0.2136	,	0.2116	,	0.1826	,	0.1844	,	0.1769
RayTracingGpu	,	10.4804	,	10.1789	,	9.96496	,	9.47891	,	8.20096	,	9.52112	,	10.3776	,	10.8512	,	11.2334	,	10.8263	,	10.5914	,	10.9609	,	11.3123	,	11.0635	,	9.72253	,	8.99101	,	11.8136	,	11.0928	,	8.18275	,	9.30653	,	10.2848	,	10.2467	,	10.2583
DeferredRLGpu	,	2.75069	,	2.51053	,	2.1936	,	1.63894	,	1.35168	,	1.60413	,	2.00227	,	2.3895	,	2.0687	,	1.53613	,	1.54784	,	1.76128	,	2.17491	,	2.47002	,	2.15757	,	1.54602	,	2.3128	,	1.88323	,	1.4767	,	2.79523	,	3.88016	,	3.84694	,	3.86317
RayTracingRLGpu	,	5.14938	,	5.1177	,	4.86656	,	5.04851	,	4.29507	,	5.27962	,	5.56826	,	5.93354	,	6.4792	,	6.51984	,	6.49949	,	6.30048	,	6.35699	,	5.7129	,	4.65597	,	4.61539	,	6.79478	,	6.468	,	4.15475	,	3.95363	,	3.85693	,	3.84918	,	3.8519
ResolveRLGpu	,	2.57776	,	2.548	,	2.9032	,	2.78998	,	2.55174	,	2.63469	,	2.80538	,	2.52659	,	2.68403	,	2.76803	,	2.54214	,	2.89664	,	2.77638	,	2.8784	,	2.90698	,	2.82666	,	2.70438	,	2.73872	,	2.54934	,	2.55558	,	2.54461	,	2.54845	,	2.54118
Index	,	0	,	1	,	2	,	3	,	4	,	5	,	6	,	7	,	8	,	9	,	10	,	11	,	12	,	13	,	14	,	15	,	16	,	17	,	18	,	19	,	20	,	21	,	22

ProfilingBuildAggregator: 
BuildBottomLevelAS	,	1.4064
BuildBottomLevelASGpu	,	2.42285
BuildTopLevelAS	,	0.6566
BuildTopLevelASGpu	,	0.089376
Duplication	,	1
Triangles	,	262267
Meshes	,	103
BottomLevels	,	1
TopLevelSize	,	64
BottomLevelsSize	,	18390656
Index	,	0

FpsAggregator: 
FPS	,	60	,	84	,	92	,	97	,	105	,	102	,	101	,	86	,	84	,	86	,	88	,	87	,	85	,	81	,	85	,	104	,	88	,	81	,	94	,	105	,	87	,	85	,	85
UPS	,	59	,	60	,	60	,	61	,	60	,	60	,	61	,	60	,	61	,	60	,	60	,	61	,	60	,	60	,	60	,	60	,	61	,	60	,	61	,	60	,	60	,	60	,	60
FrameTime	,	16.7157	,	11.9369	,	10.8944	,	10.3941	,	9.5395	,	9.86787	,	10.0035	,	11.7077	,	12.0378	,	11.6537	,	11.4101	,	11.5852	,	11.8109	,	12.3464	,	11.7647	,	9.66721	,	11.4251	,	12.4927	,	10.7187	,	9.52922	,	11.5009	,	11.7987	,	11.7762
GigaRays/s	,	2.82913	,	3.96175	,	4.34086	,	4.54978	,	4.95738	,	4.79241	,	4.72745	,	4.03931	,	3.92852	,	4.05801	,	4.14465	,	4.08202	,	4.004	,	3.83035	,	4.01972	,	4.89188	,	4.13921	,	3.78549	,	4.41198	,	4.96272	,	4.11194	,	4.00815	,	4.0158
Index	,	0	,	1	,	2	,	3	,	4	,	5	,	6	,	7	,	8	,	9	,	10	,	11	,	12	,	13	,	14	,	15	,	16	,	17	,	18	,	19	,	20	,	21	,	22


AccelerationStructure: 
Ray tracing acceleration structure building statistics: 
	Bottom Level Acceleration Structure: 
		Maximum [B]: 18390656
		Minimum [B]: 18390656
		Currently Allocated [B]: 18390656
		Currently Created [num]: 1
		Current Average [B]: 18390656
		Maximum Triangles [num]: 262267
		Minimum Triangles [num]: 262267
		Total Triangles [num]: 262267
		Maximum Meshes [num]: 103
		Minimum Meshes [num]: 103
		Total Meshes [num]: 103
	Top Level Acceleration Structure: 
		Maximum [B]: 64
		Minimum [B]: 64
		Currently Allocated [B]: 64
		Total Allocated [B]: 64
		Total Created [num]: 1
		Average Allocated [B]: 64
		Maximum Instances [num]: 1
		Minimum Instances [num]: 1
		Total Instances [num]: 1
	Scratch Buffer: 
		Maximum [B]: 14995072
		Minimum [B]: 14995072
		Currently Allocated [B]: 14995072
		Total Allocated [B]: 14995072
		Total Created [num]: 1
		Average Allocated [B]: 14995072
ProfilingStack: 
[PROF] Profiling statistics: 
	Scopes entered : 96133
	Scopes exited : 96132
	Overhead per scope [ticks] : 102.9
	Threads entered : 3
	Threads exited : 0
	============================
		         Sum[Mt]   Parent[%]  Samples[num]       Avg[Mt]   Last/Curr[Mt]        Self[Mt]     Self[%] 	 Name

	main:

		   25948.013 Mt   100.000 %         1 x    25948.014 Mt    25948.013 Mt        0.360 Mt     0.001 % 	 /

		   25947.683 Mt    99.999 %         1 x    25947.685 Mt    25947.683 Mt      222.815 Mt     0.859 % 	 ./Main application loop

		       1.550 Mt     0.006 %         1 x        1.550 Mt        0.000 Mt        1.550 Mt   100.000 % 	 ../ShadowMapRLInitialization

		       4.494 Mt     0.017 %         1 x        4.494 Mt        0.000 Mt        4.494 Mt   100.000 % 	 ../ResolveRLInitialization

		   23132.450 Mt    89.150 %     67946 x        0.340 Mt       29.061 Mt      425.118 Mt     1.838 % 	 ../MainLoop

		     189.150 Mt     0.818 %      1386 x        0.136 Mt        0.095 Mt      188.880 Mt    99.857 % 	 .../Update

		       0.270 Mt     0.143 %         1 x        0.270 Mt        0.000 Mt        0.270 Mt   100.000 % 	 ..../GuiModelApply

		   22518.182 Mt    97.345 %      2053 x       10.968 Mt       12.208 Mt      222.825 Mt     0.990 % 	 .../Render

		     346.236 Mt     1.538 %      2053 x        0.169 Mt        0.273 Mt       12.983 Mt     3.750 % 	 ..../RayTracing

		     147.488 Mt    42.598 %      2053 x        0.072 Mt        0.133 Mt      145.952 Mt    98.958 % 	 ...../Deferred

		       1.537 Mt     1.042 %      2053 x        0.001 Mt        0.001 Mt        1.537 Mt   100.000 % 	 ....../DeferredRLPrep

		     126.084 Mt    36.416 %      2053 x        0.061 Mt        0.091 Mt      125.094 Mt    99.215 % 	 ...../RayCasting

		       0.990 Mt     0.785 %      2053 x        0.000 Mt        0.001 Mt        0.990 Mt   100.000 % 	 ....../RayTracingRLPrep

		      59.680 Mt    17.237 %      2053 x        0.029 Mt        0.040 Mt       59.680 Mt   100.000 % 	 ...../Resolve

		   21949.122 Mt    97.473 %      2053 x       10.691 Mt       11.755 Mt    21949.122 Mt   100.000 % 	 ..../Present

		       3.158 Mt     0.012 %         1 x        3.158 Mt        0.000 Mt        3.158 Mt   100.000 % 	 ../TexturedRLInitialization

		       7.781 Mt     0.030 %         1 x        7.781 Mt        0.000 Mt        7.781 Mt   100.000 % 	 ../AmbientOcclusionRLInitialization

		       4.109 Mt     0.016 %         1 x        4.109 Mt        0.000 Mt        4.109 Mt   100.000 % 	 ../RasterResolveRLInitialization

		       4.371 Mt     0.017 %         1 x        4.371 Mt        0.000 Mt        4.371 Mt   100.000 % 	 ../DeferredRLInitialization

		      50.750 Mt     0.196 %         1 x       50.750 Mt        0.000 Mt        2.901 Mt     5.717 % 	 ../RayTracingRLInitialization

		      47.849 Mt    94.283 %         1 x       47.849 Mt        0.000 Mt       47.849 Mt   100.000 % 	 .../PipelineBuild

		    2516.204 Mt     9.697 %         1 x     2516.204 Mt        0.000 Mt        0.190 Mt     0.008 % 	 ../Initialization

		    2516.015 Mt    99.992 %         1 x     2516.015 Mt        0.000 Mt        1.990 Mt     0.079 % 	 .../ScenePrep

		    2457.609 Mt    97.679 %         1 x     2457.609 Mt        0.000 Mt      212.639 Mt     8.652 % 	 ..../SceneLoad

		    1970.072 Mt    80.162 %         1 x     1970.072 Mt        0.000 Mt      577.403 Mt    29.309 % 	 ...../glTF-LoadScene

		    1392.669 Mt    70.691 %        69 x       20.184 Mt        0.000 Mt     1392.669 Mt   100.000 % 	 ....../glTF-LoadImageData

		     214.539 Mt     8.730 %         1 x      214.539 Mt        0.000 Mt      214.539 Mt   100.000 % 	 ...../glTF-CreateScene

		      60.358 Mt     2.456 %         1 x       60.358 Mt        0.000 Mt       60.358 Mt   100.000 % 	 ...../SceneDesc-CreateScene

		       0.886 Mt     0.035 %         1 x        0.886 Mt        0.000 Mt        0.001 Mt     0.079 % 	 ..../ShadowMapRLPrep

		       0.885 Mt    99.921 %         1 x        0.885 Mt        0.000 Mt        0.847 Mt    95.718 % 	 ...../PrepareForRendering

		       0.038 Mt     4.282 %         1 x        0.038 Mt        0.000 Mt        0.038 Mt   100.000 % 	 ....../PrepareDrawBundle

		       3.296 Mt     0.131 %         1 x        3.296 Mt        0.000 Mt        0.000 Mt     0.012 % 	 ..../TexturedRLPrep

		       3.296 Mt    99.988 %         1 x        3.296 Mt        0.000 Mt        3.256 Mt    98.799 % 	 ...../PrepareForRendering

		       0.002 Mt     0.064 %         1 x        0.002 Mt        0.000 Mt        0.002 Mt   100.000 % 	 ....../PrepareMaterials

		       0.037 Mt     1.138 %         1 x        0.037 Mt        0.000 Mt        0.037 Mt   100.000 % 	 ....../PrepareDrawBundle

		       2.751 Mt     0.109 %         1 x        2.751 Mt        0.000 Mt        0.001 Mt     0.018 % 	 ..../DeferredRLPrep

		       2.750 Mt    99.982 %         1 x        2.750 Mt        0.000 Mt        2.400 Mt    87.259 % 	 ...../PrepareForRendering

		       0.001 Mt     0.036 %         1 x        0.001 Mt        0.000 Mt        0.001 Mt   100.000 % 	 ....../PrepareMaterials

		       0.283 Mt    10.297 %         1 x        0.283 Mt        0.000 Mt        0.283 Mt   100.000 % 	 ....../BuildMaterialCache

		       0.066 Mt     2.407 %         1 x        0.066 Mt        0.000 Mt        0.066 Mt   100.000 % 	 ....../PrepareDrawBundle

		      20.778 Mt     0.826 %         1 x       20.778 Mt        0.000 Mt        1.940 Mt     9.335 % 	 ..../RayTracingRLPrep

		       0.074 Mt     0.356 %         1 x        0.074 Mt        0.000 Mt        0.074 Mt   100.000 % 	 ...../PrepareAccelerationStructures

		       0.001 Mt     0.005 %         1 x        0.001 Mt        0.000 Mt        0.001 Mt   100.000 % 	 ...../PrepareCache

		      13.205 Mt    63.550 %         1 x       13.205 Mt        0.000 Mt       13.205 Mt   100.000 % 	 ...../PipelineBuild

		       0.540 Mt     2.599 %         1 x        0.540 Mt        0.000 Mt        0.540 Mt   100.000 % 	 ...../BuildCache

		       4.064 Mt    19.560 %         1 x        4.064 Mt        0.000 Mt        0.001 Mt     0.017 % 	 ...../BuildAccelerationStructures

		       4.064 Mt    99.983 %         1 x        4.064 Mt        0.000 Mt        2.001 Mt    49.232 % 	 ....../AccelerationStructureBuild

		       1.406 Mt    34.610 %         1 x        1.406 Mt        0.000 Mt        1.406 Mt   100.000 % 	 ......./BuildBottomLevelAS

		       0.657 Mt    16.158 %         1 x        0.657 Mt        0.000 Mt        0.657 Mt   100.000 % 	 ......./BuildTopLevelAS

		       0.955 Mt     4.594 %         1 x        0.955 Mt        0.000 Mt        0.955 Mt   100.000 % 	 ...../BuildShaderTables

		      28.704 Mt     1.141 %         1 x       28.704 Mt        0.000 Mt       28.704 Mt   100.000 % 	 ..../GpuSidePrep

	WindowThread:

		   25946.029 Mt   100.000 %         1 x    25946.030 Mt    25946.029 Mt    25946.029 Mt   100.000 % 	 /

	GpuThread:

		   21027.873 Mt   100.000 %      2053 x       10.243 Mt       11.181 Mt      137.355 Mt     0.653 % 	 /

		       2.521 Mt     0.012 %         1 x        2.521 Mt        0.000 Mt        0.008 Mt     0.303 % 	 ./ScenePrep

		       2.514 Mt    99.697 %         1 x        2.514 Mt        0.000 Mt        0.001 Mt     0.056 % 	 ../AccelerationStructureBuild

		       2.423 Mt    96.388 %         1 x        2.423 Mt        0.000 Mt        2.423 Mt   100.000 % 	 .../BuildBottomLevelASGpu

		       0.089 Mt     3.556 %         1 x        0.089 Mt        0.000 Mt        0.089 Mt   100.000 % 	 .../BuildTopLevelASGpu

		   20797.966 Mt    98.907 %      2053 x       10.131 Mt       11.087 Mt        4.890 Mt     0.024 % 	 ./RayTracingGpu

		    4474.593 Mt    21.515 %      2053 x        2.180 Mt        3.962 Mt     4474.593 Mt   100.000 % 	 ../DeferredRLGpu

		   10823.115 Mt    52.039 %      2053 x        5.272 Mt        4.426 Mt    10823.115 Mt   100.000 % 	 ../RayTracingRLGpu

		    5495.367 Mt    26.423 %      2053 x        2.677 Mt        2.698 Mt     5495.367 Mt   100.000 % 	 ../ResolveRLGpu

		      90.031 Mt     0.428 %      2053 x        0.044 Mt        0.094 Mt       90.031 Mt   100.000 % 	 ./Present


	============================


