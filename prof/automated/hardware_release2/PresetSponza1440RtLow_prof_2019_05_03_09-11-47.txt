Configuration: 
Command line parameters  = E:\programming\FIT-DIP\Masters-DXR-Main-git\build\Measurement\x64\Release\Measurement.exe --scene Sponza/Sponza.gltf --profile-automation --profile-camera-track Sponza/SponzaPreset.trk --rt-mode --width 2560 --height 1440 --profile-output PresetSponza1440RtLow --quality-preset Low 
Build type               = Release
Device name              = NVIDIA GeForce RTX 2080 Ti
Device memory MB         = 11049
Ray Tracing backend      = Hardware
Window width             = 2560
Window height            = 1421
Window title             = Direct3D App
Back-buffer count        = 3
Fill Triangles           = enabled
Triangles Font Clock-Wise= enabled
Fast Build AS            = disabled
Target FPS               = 0
Target UPS               = 60
Tearing                  = enabled
VSync                    = disabled
GUI rendering            = disabled
Debug layer              = disabled
GPU validation           = disabled
GPU profiling            = enabled
Render mode              = Ray Tracing
Loaded scene file        = Sponza/Sponza.gltf
Using testing scene      = disabled
Duplication              = 1
Duplication in cube      = disabled
Duplication offset       = 10
BLAS duplication         = disabled
Rays per pixel           = 5
Quality preset           = Low
Rasterization Quality    : 
  AO                     = enabled
  AO kernel size         = 4
  Shadows                = enabled
  Shadow map resolution  = 2048
  Shadow PCF kernel size = 0
  Reflections            = disabled
Ray Tracing Quality      : 
  AO                     = enabled
  AO rays                = 4
  AO kernel size         = 4
  Shadows                = enabled
  Shadow rays            = 1
  Shadow kernel size     = 4
  Reflections            = disabled

ProfilingAggregator: 
Render	,	9.1228	,	9.2223	,	8.7742	,	7.8976	,	8.0109	,	7.3085	,	7.6509	,	8.5631	,	8.4299	,	7.6691	,	7.8667	,	7.8877	,	8.7849	,	8.526	,	8.0118	,	8.268	,	9.438	,	8.6213	,	7.5983	,	9.3291	,	9.5737	,	9.5427	,	9.9984
Update	,	0.0731	,	0.072	,	0.0721	,	0.0716	,	0.0702	,	0.0394	,	0.0704	,	0.0725	,	0.0641	,	0.0725	,	0.0723	,	0.0706	,	0.0643	,	0.0707	,	0.0726	,	0.072	,	0.0709	,	0.09	,	0.0644	,	0.072	,	0.071	,	0.0711	,	0.068
RayTracing	,	0.1764	,	0.1704	,	0.1718	,	0.1671	,	0.1687	,	0.0945	,	0.1497	,	0.1705	,	0.1971	,	0.1997	,	0.2039	,	0.2006	,	0.2045	,	0.1742	,	0.1812	,	0.1745	,	0.1758	,	0.2252	,	0.1735	,	0.1702	,	0.1772	,	0.173	,	0.1753
RayTracingGpu	,	8.19206	,	8.19114	,	7.83088	,	6.97677	,	7.09779	,	6.82061	,	6.89898	,	7.69834	,	7.54371	,	6.75488	,	6.82982	,	6.99338	,	8.0505	,	7.66307	,	7.2585	,	7.31942	,	8.36387	,	7.67328	,	6.76765	,	8.38486	,	8.6935	,	8.64835	,	8.93955
DeferredRLGpu	,	2.70957	,	2.49482	,	2.19866	,	1.46538	,	1.35075	,	1.59523	,	1.97581	,	2.41795	,	2.07568	,	1.54355	,	1.35072	,	1.88797	,	2.46195	,	2.43898	,	2.20458	,	1.48141	,	2.15987	,	1.8953	,	1.49779	,	2.7057	,	3.88733	,	3.8608	,	3.86624
RayTracingRLGpu	,	2.57206	,	3.0584	,	2.98602	,	2.75146	,	2.99274	,	2.70282	,	2.39261	,	2.44042	,	2.76125	,	2.34102	,	2.83603	,	2.6128	,	2.89059	,	2.73293	,	2.52704	,	3.08746	,	3.65894	,	3.24858	,	2.75034	,	3.02806	,	2.27437	,	2.27469	,	2.27603
ResolveRLGpu	,	2.90838	,	2.63635	,	2.64349	,	2.75757	,	2.75206	,	2.51818	,	2.52774	,	2.83766	,	2.7047	,	2.86816	,	2.63946	,	2.48765	,	2.69635	,	2.4881	,	2.52362	,	2.74851	,	2.54323	,	2.52746	,	2.51587	,	2.64947	,	2.52982	,	2.50957	,	2.79565
Index	,	0	,	1	,	2	,	3	,	4	,	5	,	6	,	7	,	8	,	9	,	10	,	11	,	12	,	13	,	14	,	15	,	16	,	17	,	18	,	19	,	20	,	21	,	22

ProfilingBuildAggregator: 
BuildBottomLevelAS	,	1.553
BuildBottomLevelASGpu	,	1.48883
BuildTopLevelAS	,	0.9629
BuildTopLevelASGpu	,	0.087552
Duplication	,	1
Triangles	,	262267
Meshes	,	103
BottomLevels	,	1
TopLevelSize	,	64
BottomLevelsSize	,	14622208
Index	,	0

FpsAggregator: 
FPS	,	74	,	107	,	117	,	122	,	132	,	129	,	133	,	117	,	118	,	127	,	134	,	131	,	123	,	115	,	113	,	126	,	115	,	109	,	118	,	124	,	104	,	100	,	101
UPS	,	59	,	60	,	60	,	61	,	60	,	60	,	60	,	61	,	60	,	60	,	60	,	60	,	60	,	60	,	61	,	60	,	60	,	60	,	61	,	60	,	61	,	60	,	60
FrameTime	,	13.577	,	9.37294	,	8.55182	,	8.20289	,	7.58639	,	7.7792	,	7.56813	,	8.60033	,	8.50955	,	7.87604	,	7.48025	,	7.65996	,	8.13082	,	8.72424	,	8.90638	,	7.98275	,	8.73649	,	9.18502	,	8.53712	,	8.12555	,	9.69673	,	10.0008	,	9.97592
GigaRays/s	,	1.33967	,	1.94056	,	2.12689	,	2.21736	,	2.39756	,	2.33813	,	2.40334	,	2.1149	,	2.13746	,	2.30938	,	2.43158	,	2.37453	,	2.23702	,	2.08486	,	2.04222	,	2.27851	,	2.08193	,	1.98027	,	2.13056	,	2.23847	,	1.87577	,	1.81874	,	1.82327
Index	,	0	,	1	,	2	,	3	,	4	,	5	,	6	,	7	,	8	,	9	,	10	,	11	,	12	,	13	,	14	,	15	,	16	,	17	,	18	,	19	,	20	,	21	,	22


AccelerationStructure: 
Ray tracing acceleration structure building statistics: 
	Bottom Level Acceleration Structure: 
		Maximum [B]: 14622208
		Minimum [B]: 14622208
		Currently Allocated [B]: 14622208
		Currently Created [num]: 1
		Current Average [B]: 14622208
		Maximum Triangles [num]: 262267
		Minimum Triangles [num]: 262267
		Total Triangles [num]: 262267
		Maximum Meshes [num]: 103
		Minimum Meshes [num]: 103
		Total Meshes [num]: 103
	Top Level Acceleration Structure: 
		Maximum [B]: 64
		Minimum [B]: 64
		Currently Allocated [B]: 64
		Total Allocated [B]: 64
		Total Created [num]: 1
		Average Allocated [B]: 64
		Maximum Instances [num]: 1
		Minimum Instances [num]: 1
		Total Instances [num]: 1
	Scratch Buffer: 
		Maximum [B]: 11657856
		Minimum [B]: 11657856
		Currently Allocated [B]: 11657856
		Total Allocated [B]: 11657856
		Total Created [num]: 1
		Average Allocated [B]: 11657856
ProfilingStack: 
[PROF] Profiling statistics: 
	Scopes entered : 116379
	Scopes exited : 116378
	Overhead per scope [ticks] : 101.7
	Threads entered : 3
	Threads exited : 0
	============================
		         Sum[Mt]   Parent[%]  Samples[num]       Avg[Mt]   Last/Curr[Mt]        Self[Mt]     Self[%] 	 Name

	main:

		   26666.360 Mt   100.000 %         1 x    26666.361 Mt    26666.360 Mt        0.418 Mt     0.002 % 	 /

		   26665.974 Mt    99.999 %         1 x    26665.976 Mt    26665.974 Mt      291.942 Mt     1.095 % 	 ./Main application loop

		       3.028 Mt     0.011 %         1 x        3.028 Mt        0.000 Mt        3.028 Mt   100.000 % 	 ../TexturedRLInitialization

		       1.305 Mt     0.005 %         1 x        1.305 Mt        0.000 Mt        1.305 Mt   100.000 % 	 ../ShadowMapRLInitialization

		       5.719 Mt     0.021 %         1 x        5.719 Mt        0.000 Mt        5.719 Mt   100.000 % 	 ../RasterResolveRLInitialization

		       2.954 Mt     0.011 %         1 x        2.954 Mt        0.000 Mt        2.954 Mt   100.000 % 	 ../AmbientOcclusionRLInitialization

		       5.348 Mt     0.020 %         1 x        5.348 Mt        0.000 Mt        5.348 Mt   100.000 % 	 ../DeferredRLInitialization

		      51.285 Mt     0.192 %         1 x       51.285 Mt        0.000 Mt        2.347 Mt     4.576 % 	 ../RayTracingRLInitialization

		      48.939 Mt    95.424 %         1 x       48.939 Mt        0.000 Mt       48.939 Mt   100.000 % 	 .../PipelineBuild

		       3.716 Mt     0.014 %         1 x        3.716 Mt        0.000 Mt        3.716 Mt   100.000 % 	 ../ResolveRLInitialization

		    3184.783 Mt    11.943 %         1 x     3184.783 Mt        0.000 Mt        0.198 Mt     0.006 % 	 ../Initialization

		    3184.585 Mt    99.994 %         1 x     3184.585 Mt        0.000 Mt        1.961 Mt     0.062 % 	 .../ScenePrep

		    3119.147 Mt    97.945 %         1 x     3119.147 Mt        0.000 Mt      916.363 Mt    29.379 % 	 ..../SceneLoad

		    1914.512 Mt    61.379 %         1 x     1914.512 Mt        0.000 Mt      572.357 Mt    29.896 % 	 ...../glTF-LoadScene

		    1342.154 Mt    70.104 %        69 x       19.452 Mt        0.000 Mt     1342.154 Mt   100.000 % 	 ....../glTF-LoadImageData

		     220.464 Mt     7.068 %         1 x      220.464 Mt        0.000 Mt      220.464 Mt   100.000 % 	 ...../glTF-CreateScene

		      67.807 Mt     2.174 %         1 x       67.807 Mt        0.000 Mt       67.807 Mt   100.000 % 	 ...../SceneDesc-CreateScene

		       1.076 Mt     0.034 %         1 x        1.076 Mt        0.000 Mt        0.001 Mt     0.046 % 	 ..../ShadowMapRLPrep

		       1.075 Mt    99.954 %         1 x        1.075 Mt        0.000 Mt        0.990 Mt    92.068 % 	 ...../PrepareForRendering

		       0.085 Mt     7.932 %         1 x        0.085 Mt        0.000 Mt        0.085 Mt   100.000 % 	 ....../PrepareDrawBundle

		       3.658 Mt     0.115 %         1 x        3.658 Mt        0.000 Mt        0.000 Mt     0.011 % 	 ..../TexturedRLPrep

		       3.658 Mt    99.989 %         1 x        3.658 Mt        0.000 Mt        3.617 Mt    98.885 % 	 ...../PrepareForRendering

		       0.002 Mt     0.057 %         1 x        0.002 Mt        0.000 Mt        0.002 Mt   100.000 % 	 ....../PrepareMaterials

		       0.039 Mt     1.058 %         1 x        0.039 Mt        0.000 Mt        0.039 Mt   100.000 % 	 ....../PrepareDrawBundle

		       2.672 Mt     0.084 %         1 x        2.672 Mt        0.000 Mt        0.000 Mt     0.011 % 	 ..../DeferredRLPrep

		       2.671 Mt    99.989 %         1 x        2.671 Mt        0.000 Mt        2.168 Mt    81.152 % 	 ...../PrepareForRendering

		       0.001 Mt     0.030 %         1 x        0.001 Mt        0.000 Mt        0.001 Mt   100.000 % 	 ....../PrepareMaterials

		       0.446 Mt    16.688 %         1 x        0.446 Mt        0.000 Mt        0.446 Mt   100.000 % 	 ....../BuildMaterialCache

		       0.057 Mt     2.130 %         1 x        0.057 Mt        0.000 Mt        0.057 Mt   100.000 % 	 ....../PrepareDrawBundle

		      26.663 Mt     0.837 %         1 x       26.663 Mt        0.000 Mt        2.428 Mt     9.108 % 	 ..../RayTracingRLPrep

		       0.084 Mt     0.317 %         1 x        0.084 Mt        0.000 Mt        0.084 Mt   100.000 % 	 ...../PrepareAccelerationStructures

		       0.001 Mt     0.003 %         1 x        0.001 Mt        0.000 Mt        0.001 Mt   100.000 % 	 ...../PrepareCache

		      14.788 Mt    55.465 %         1 x       14.788 Mt        0.000 Mt       14.788 Mt   100.000 % 	 ...../PipelineBuild

		       0.663 Mt     2.486 %         1 x        0.663 Mt        0.000 Mt        0.663 Mt   100.000 % 	 ...../BuildCache

		       7.832 Mt    29.373 %         1 x        7.832 Mt        0.000 Mt        0.001 Mt     0.008 % 	 ...../BuildAccelerationStructures

		       7.831 Mt    99.992 %         1 x        7.831 Mt        0.000 Mt        5.315 Mt    67.873 % 	 ....../AccelerationStructureBuild

		       1.553 Mt    19.831 %         1 x        1.553 Mt        0.000 Mt        1.553 Mt   100.000 % 	 ......./BuildBottomLevelAS

		       0.963 Mt    12.296 %         1 x        0.963 Mt        0.000 Mt        0.963 Mt   100.000 % 	 ......./BuildTopLevelAS

		       0.866 Mt     3.249 %         1 x        0.866 Mt        0.000 Mt        0.866 Mt   100.000 % 	 ...../BuildShaderTables

		      29.409 Mt     0.923 %         1 x       29.409 Mt        0.000 Mt       29.409 Mt   100.000 % 	 ..../GpuSidePrep

		   23115.894 Mt    86.687 %     79912 x        0.289 Mt       30.236 Mt      442.400 Mt     1.914 % 	 ../MainLoop

		     182.982 Mt     0.792 %      1385 x        0.132 Mt        0.087 Mt      182.357 Mt    99.658 % 	 .../Update

		       0.625 Mt     0.342 %         1 x        0.625 Mt        0.000 Mt        0.625 Mt   100.000 % 	 ..../GuiModelApply

		   22490.511 Mt    97.295 %      2690 x        8.361 Mt        9.852 Mt      313.325 Mt     1.393 % 	 .../Render

		     482.563 Mt     2.146 %      2690 x        0.179 Mt        0.282 Mt       18.143 Mt     3.760 % 	 ..../RayTracing

		     211.628 Mt    43.855 %      2690 x        0.079 Mt        0.131 Mt      208.973 Mt    98.745 % 	 ...../Deferred

		       2.655 Mt     1.255 %      2690 x        0.001 Mt        0.001 Mt        2.655 Mt   100.000 % 	 ....../DeferredRLPrep

		     168.441 Mt    34.905 %      2690 x        0.063 Mt        0.093 Mt      166.887 Mt    99.077 % 	 ...../RayCasting

		       1.554 Mt     0.923 %      2690 x        0.001 Mt        0.001 Mt        1.554 Mt   100.000 % 	 ....../RayTracingRLPrep

		      84.352 Mt    17.480 %      2690 x        0.031 Mt        0.041 Mt       84.352 Mt   100.000 % 	 ...../Resolve

		   21694.622 Mt    96.461 %      2690 x        8.065 Mt        9.384 Mt    21694.622 Mt   100.000 % 	 ..../Present

	WindowThread:

		   26665.002 Mt   100.000 %         1 x    26665.002 Mt    26665.002 Mt    26665.002 Mt   100.000 % 	 /

	GpuThread:

		   20307.936 Mt   100.000 %      2690 x        7.549 Mt        8.828 Mt      143.468 Mt     0.706 % 	 /

		       1.584 Mt     0.008 %         1 x        1.584 Mt        0.000 Mt        0.007 Mt     0.412 % 	 ./ScenePrep

		       1.578 Mt    99.588 %         1 x        1.578 Mt        0.000 Mt        0.001 Mt     0.077 % 	 ../AccelerationStructureBuild

		       1.489 Mt    94.373 %         1 x        1.489 Mt        0.000 Mt        1.489 Mt   100.000 % 	 .../BuildBottomLevelASGpu

		       0.088 Mt     5.550 %         1 x        0.088 Mt        0.000 Mt        0.088 Mt   100.000 % 	 .../BuildTopLevelASGpu

		   20087.066 Mt    98.912 %      2690 x        7.467 Mt        8.739 Mt        6.950 Mt     0.035 % 	 ./RayTracingGpu

		    5790.189 Mt    28.825 %      2690 x        2.152 Mt        3.852 Mt     5790.189 Mt   100.000 % 	 ../DeferredRLGpu

		    7270.409 Mt    36.194 %      2690 x        2.703 Mt        2.273 Mt     7270.409 Mt   100.000 % 	 ../RayTracingRLGpu

		    7019.518 Mt    34.945 %      2690 x        2.609 Mt        2.614 Mt     7019.518 Mt   100.000 % 	 ../ResolveRLGpu

		      75.818 Mt     0.373 %      2690 x        0.028 Mt        0.088 Mt       75.818 Mt   100.000 % 	 ./Present


	============================


