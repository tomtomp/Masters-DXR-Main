Configuration: 
Command line parameters  = E:\programming\FIT-DIP\Masters-DXR-Main-git\build\Measurement\x64\Release\Measurement.exe --scene Suzanne/Suzanne.gltf --profile-automation --profile-camera-track Suzanne/Suzanne.trk --width 1024 --height 768 --profile-output TrackSuzanne768Ra 
Build type               = Release
Device name              = NVIDIA GeForce RTX 2080 Ti
Device memory MB         = 11049
Ray Tracing backend      = Hardware
Window width             = 1024
Window height            = 768
Window title             = Direct3D App
Back-buffer count        = 3
Fill Triangles           = enabled
Triangles Font Clock-Wise= enabled
Fast Build AS            = disabled
Target FPS               = 0
Target UPS               = 60
Tearing                  = enabled
VSync                    = disabled
GUI rendering            = disabled
Debug layer              = disabled
GPU validation           = disabled
GPU profiling            = enabled
Render mode              = Rasterization
Loaded scene file        = Suzanne/Suzanne.gltf
Using testing scene      = disabled
Duplication              = 1
Duplication in cube      = disabled
Duplication offset       = 10
BLAS duplication         = disabled
Rays per pixel           = 13
Quality preset           = High
Rasterization Quality    : 
  AO                     = enabled
  AO kernel size         = 4
  Shadows                = enabled
  Shadow map resolution  = 4096
  Shadow PCF kernel size = 4
  Reflections            = enabled
Ray Tracing Quality      : 
  AO                     = enabled
  AO rays                = 4
  AO kernel size         = 4
  Shadows                = enabled
  Shadow rays            = 4
  Shadow kernel size     = 4
  Reflections            = enabled

ProfilingAggregator: 
Render	,	1.0714	,	1.1141	,	1.1318	,	1.1078	,	1.0566	,	1.0107	,	1.1301	,	0.9832	,	0.9841	,	1.0456	,	1.0578	,	0.9753	,	1.0354	,	1.0093	,	0.8585	,	1.0154	,	1.0431	,	1.0554	,	1.0457	,	1.276	,	1.2753	,	1.1947	,	1.0711	,	1.1176
Update	,	0.0258	,	0.0664	,	0.0635	,	0.0616	,	0.0623	,	0.0594	,	0.0368	,	0.0626	,	0.0377	,	0.0554	,	0.0649	,	0.061	,	0.0649	,	0.0462	,	0.0396	,	0.0584	,	0.0386	,	0.0578	,	0.0599	,	0.0591	,	0.0781	,	0.0603	,	0.0572	,	0.0393
DeferredRLGpu	,	0.04096	,	0.058112	,	0.059776	,	0.048416	,	0.030976	,	0.019296	,	0.016672	,	0.012896	,	0.018112	,	0.027648	,	0.030528	,	0.0216	,	0.016384	,	0.016672	,	0.024608	,	0.041952	,	0.03936	,	0.05504	,	0.089888	,	0.081376	,	0.073728	,	0.040864	,	0.022304	,	0.020128
Index	,	0	,	1	,	2	,	3	,	4	,	5	,	6	,	7	,	8	,	9	,	10	,	11	,	12	,	13	,	14	,	15	,	16	,	17	,	18	,	19	,	20	,	21	,	22	,	23

ProfilingBuildAggregator: 
BuildBottomLevelAS	,	0.8539
BuildBottomLevelASGpu	,	0.253984
BuildTopLevelAS	,	0.8219
BuildTopLevelASGpu	,	0.061376
Duplication	,	1
Triangles	,	3936
Meshes	,	1
BottomLevels	,	1
TopLevelSize	,	64
BottomLevelsSize	,	222592
Index	,	0

FpsAggregator: 
FPS	,	823	,	899	,	866	,	880	,	915	,	944	,	973	,	975	,	976	,	961	,	932	,	940	,	960	,	983	,	983	,	945	,	934	,	916	,	872	,	779	,	829	,	871	,	946	,	974
UPS	,	59	,	60	,	60	,	61	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60
FrameTime	,	1.21533	,	1.11285	,	1.15523	,	1.13736	,	1.0931	,	1.0597	,	1.02788	,	1.02602	,	1.02552	,	1.04167	,	1.07388	,	1.06384	,	1.0425	,	1.01775	,	1.01815	,	1.05926	,	1.07176	,	1.09205	,	1.14796	,	1.28376	,	1.20652	,	1.14857	,	1.05763	,	1.02689
GigaRays/s	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0
Index	,	0	,	1	,	2	,	3	,	4	,	5	,	6	,	7	,	8	,	9	,	10	,	11	,	12	,	13	,	14	,	15	,	16	,	17	,	18	,	19	,	20	,	21	,	22	,	23


AccelerationStructure: 
Ray tracing acceleration structure building statistics: 
	Bottom Level Acceleration Structure: 
		Maximum [B]: 222592
		Minimum [B]: 222592
		Currently Allocated [B]: 222592
		Currently Created [num]: 1
		Current Average [B]: 222592
		Maximum Triangles [num]: 3936
		Minimum Triangles [num]: 3936
		Total Triangles [num]: 3936
		Maximum Meshes [num]: 1
		Minimum Meshes [num]: 1
		Total Meshes [num]: 1
	Top Level Acceleration Structure: 
		Maximum [B]: 64
		Minimum [B]: 64
		Currently Allocated [B]: 64
		Total Allocated [B]: 64
		Total Created [num]: 1
		Average Allocated [B]: 64
		Maximum Instances [num]: 1
		Minimum Instances [num]: 1
		Total Instances [num]: 1
	Scratch Buffer: 
		Maximum [B]: 32896
		Minimum [B]: 32896
		Currently Allocated [B]: 32896
		Total Allocated [B]: 32896
		Total Created [num]: 1
		Average Allocated [B]: 32896
ProfilingStack: 
[PROF] Profiling statistics: 
	Scopes entered : 355959
	Scopes exited : 355958
	Overhead per scope [ticks] : 103.1
	Threads entered : 3
	Threads exited : 0
	============================
		         Sum[Mt]   Parent[%]  Samples[num]       Avg[Mt]   Last/Curr[Mt]        Self[Mt]     Self[%] 	 Name

	main:

		   24455.007 Mt   100.000 %         1 x    24455.008 Mt    24455.007 Mt        0.387 Mt     0.002 % 	 /

		   24454.636 Mt    99.998 %         1 x    24454.637 Mt    24454.635 Mt      248.417 Mt     1.016 % 	 ./Main application loop

		       3.110 Mt     0.013 %         1 x        3.110 Mt        0.000 Mt        3.110 Mt   100.000 % 	 ../TexturedRLInitialization

		       1.321 Mt     0.005 %         1 x        1.321 Mt        0.000 Mt        1.321 Mt   100.000 % 	 ../ShadowMapRLInitialization

		       3.731 Mt     0.015 %         1 x        3.731 Mt        0.000 Mt        3.731 Mt   100.000 % 	 ../RasterResolveRLInitialization

		       2.619 Mt     0.011 %         1 x        2.619 Mt        0.000 Mt        2.619 Mt   100.000 % 	 ../AmbientOcclusionRLInitialization

		       4.210 Mt     0.017 %         1 x        4.210 Mt        0.000 Mt        4.210 Mt   100.000 % 	 ../DeferredRLInitialization

		      49.377 Mt     0.202 %         1 x       49.377 Mt        0.000 Mt        2.082 Mt     4.217 % 	 ../RayTracingRLInitialization

		      47.295 Mt    95.783 %         1 x       47.295 Mt        0.000 Mt       47.295 Mt   100.000 % 	 .../PipelineBuild

		       3.839 Mt     0.016 %         1 x        3.839 Mt        0.000 Mt        3.839 Mt   100.000 % 	 ../ResolveRLInitialization

		     117.187 Mt     0.479 %         1 x      117.187 Mt        0.000 Mt        0.175 Mt     0.149 % 	 ../Initialization

		     117.012 Mt    99.851 %         1 x      117.012 Mt        0.000 Mt        1.658 Mt     1.417 % 	 .../ScenePrep

		      58.554 Mt    50.041 %         1 x       58.554 Mt        0.000 Mt       12.990 Mt    22.184 % 	 ..../SceneLoad

		      39.513 Mt    67.482 %         1 x       39.513 Mt        0.000 Mt        8.581 Mt    21.718 % 	 ...../glTF-LoadScene

		      30.932 Mt    78.282 %         2 x       15.466 Mt        0.000 Mt       30.932 Mt   100.000 % 	 ....../glTF-LoadImageData

		       5.153 Mt     8.800 %         1 x        5.153 Mt        0.000 Mt        5.153 Mt   100.000 % 	 ...../glTF-CreateScene

		       0.898 Mt     1.534 %         1 x        0.898 Mt        0.000 Mt        0.898 Mt   100.000 % 	 ...../SceneDesc-CreateScene

		       0.314 Mt     0.268 %         1 x        0.314 Mt        0.000 Mt        0.001 Mt     0.383 % 	 ..../ShadowMapRLPrep

		       0.312 Mt    99.617 %         1 x        0.312 Mt        0.000 Mt        0.291 Mt    93.086 % 	 ...../PrepareForRendering

		       0.022 Mt     6.914 %         1 x        0.022 Mt        0.000 Mt        0.022 Mt   100.000 % 	 ....../PrepareDrawBundle

		       0.764 Mt     0.653 %         1 x        0.764 Mt        0.000 Mt        0.001 Mt     0.105 % 	 ..../TexturedRLPrep

		       0.764 Mt    99.895 %         1 x        0.764 Mt        0.000 Mt        0.754 Mt    98.782 % 	 ...../PrepareForRendering

		       0.003 Mt     0.367 %         1 x        0.003 Mt        0.000 Mt        0.003 Mt   100.000 % 	 ....../PrepareMaterials

		       0.006 Mt     0.851 %         1 x        0.006 Mt        0.000 Mt        0.006 Mt   100.000 % 	 ....../PrepareDrawBundle

		       1.680 Mt     1.436 %         1 x        1.680 Mt        0.000 Mt        0.001 Mt     0.060 % 	 ..../DeferredRLPrep

		       1.679 Mt    99.940 %         1 x        1.679 Mt        0.000 Mt        1.413 Mt    84.123 % 	 ...../PrepareForRendering

		       0.002 Mt     0.101 %         1 x        0.002 Mt        0.000 Mt        0.002 Mt   100.000 % 	 ....../PrepareMaterials

		       0.258 Mt    15.341 %         1 x        0.258 Mt        0.000 Mt        0.258 Mt   100.000 % 	 ....../BuildMaterialCache

		       0.007 Mt     0.435 %         1 x        0.007 Mt        0.000 Mt        0.007 Mt   100.000 % 	 ....../PrepareDrawBundle

		      40.318 Mt    34.456 %         1 x       40.318 Mt        0.000 Mt        2.917 Mt     7.236 % 	 ..../RayTracingRLPrep

		       0.145 Mt     0.360 %         1 x        0.145 Mt        0.000 Mt        0.145 Mt   100.000 % 	 ...../PrepareAccelerationStructures

		       0.003 Mt     0.006 %         1 x        0.003 Mt        0.000 Mt        0.003 Mt   100.000 % 	 ...../PrepareCache

		      31.061 Mt    77.042 %         1 x       31.061 Mt        0.000 Mt       31.061 Mt   100.000 % 	 ...../PipelineBuild

		       0.390 Mt     0.968 %         1 x        0.390 Mt        0.000 Mt        0.390 Mt   100.000 % 	 ...../BuildCache

		       4.343 Mt    10.773 %         1 x        4.343 Mt        0.000 Mt        0.001 Mt     0.014 % 	 ...../BuildAccelerationStructures

		       4.343 Mt    99.986 %         1 x        4.343 Mt        0.000 Mt        2.667 Mt    61.412 % 	 ....../AccelerationStructureBuild

		       0.854 Mt    19.662 %         1 x        0.854 Mt        0.000 Mt        0.854 Mt   100.000 % 	 ......./BuildBottomLevelAS

		       0.822 Mt    18.926 %         1 x        0.822 Mt        0.000 Mt        0.822 Mt   100.000 % 	 ......./BuildTopLevelAS

		       1.458 Mt     3.615 %         1 x        1.458 Mt        0.000 Mt        1.458 Mt   100.000 % 	 ...../BuildShaderTables

		      13.725 Mt    11.729 %         1 x       13.725 Mt        0.000 Mt       13.725 Mt   100.000 % 	 ..../GpuSidePrep

		   24020.824 Mt    98.226 %    111615 x        0.215 Mt        1.462 Mt      344.084 Mt     1.432 % 	 ../MainLoop

		     136.156 Mt     0.567 %      1441 x        0.094 Mt        0.085 Mt      135.714 Mt    99.675 % 	 .../Update

		       0.442 Mt     0.325 %         1 x        0.442 Mt        0.000 Mt        0.442 Mt   100.000 % 	 ..../GuiModelApply

		   23540.584 Mt    98.001 %     22078 x        1.066 Mt        1.341 Mt     1487.341 Mt     6.318 % 	 .../Render

		    2665.932 Mt    11.325 %     22078 x        0.121 Mt        0.206 Mt     2638.991 Mt    98.989 % 	 ..../Rasterization

		      15.201 Mt     0.570 %     22078 x        0.001 Mt        0.001 Mt       15.201 Mt   100.000 % 	 ...../DeferredRLPrep

		      11.740 Mt     0.440 %     22078 x        0.001 Mt        0.001 Mt       11.740 Mt   100.000 % 	 ...../ShadowMapRLPrep

		   19387.311 Mt    82.357 %     22078 x        0.878 Mt        0.987 Mt    19387.311 Mt   100.000 % 	 ..../Present

	WindowThread:

		   24452.038 Mt   100.000 %         1 x    24452.039 Mt    24452.038 Mt    24452.038 Mt   100.000 % 	 /

	GpuThread:

		   13747.839 Mt   100.000 %     22078 x        0.623 Mt        0.576 Mt      118.157 Mt     0.859 % 	 /

		       0.322 Mt     0.002 %         1 x        0.322 Mt        0.000 Mt        0.005 Mt     1.681 % 	 ./ScenePrep

		       0.316 Mt    98.319 %         1 x        0.316 Mt        0.000 Mt        0.001 Mt     0.273 % 	 ../AccelerationStructureBuild

		       0.254 Mt    80.318 %         1 x        0.254 Mt        0.000 Mt        0.254 Mt   100.000 % 	 .../BuildBottomLevelASGpu

		       0.061 Mt    19.409 %         1 x        0.061 Mt        0.000 Mt        0.061 Mt   100.000 % 	 .../BuildTopLevelASGpu

		   13581.886 Mt    98.793 %     22078 x        0.615 Mt        0.574 Mt       78.305 Mt     0.577 % 	 ./RasterizationGpu

		     815.962 Mt     6.008 %     22078 x        0.037 Mt        0.021 Mt      815.962 Mt   100.000 % 	 ../DeferredRLGpu

		     206.777 Mt     1.522 %     22078 x        0.009 Mt        0.010 Mt      206.777 Mt   100.000 % 	 ../ShadowMapRLGpu

		    2425.809 Mt    17.861 %     22078 x        0.110 Mt        0.102 Mt     2425.809 Mt   100.000 % 	 ../AmbientOcclusionRLGpu

		   10055.034 Mt    74.033 %     22078 x        0.455 Mt        0.440 Mt    10055.034 Mt   100.000 % 	 ../RasterResolveRLGpu

		      47.474 Mt     0.345 %     22078 x        0.002 Mt        0.003 Mt       47.474 Mt   100.000 % 	 ./Present


	============================


