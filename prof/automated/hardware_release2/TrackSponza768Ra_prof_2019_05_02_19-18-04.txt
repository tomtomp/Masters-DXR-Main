Configuration: 
Command line parameters  = E:\programming\FIT-DIP\Masters-DXR-Main-git\build\Measurement\x64\Release\Measurement.exe --scene Sponza/Sponza.gltf --profile-automation --profile-camera-track Sponza/Sponza.trk --width 1024 --height 768 --profile-output TrackSponza768Ra 
Build type               = Release
Device name              = NVIDIA GeForce RTX 2080 Ti
Device memory MB         = 11049
Ray Tracing backend      = Hardware
Window width             = 1024
Window height            = 768
Window title             = Direct3D App
Back-buffer count        = 3
Fill Triangles           = enabled
Triangles Font Clock-Wise= enabled
Fast Build AS            = disabled
Target FPS               = 0
Target UPS               = 60
Tearing                  = enabled
VSync                    = disabled
GUI rendering            = disabled
Debug layer              = disabled
GPU validation           = disabled
GPU profiling            = enabled
Render mode              = Rasterization
Loaded scene file        = Sponza/Sponza.gltf
Using testing scene      = disabled
Duplication              = 1
Duplication in cube      = disabled
Duplication offset       = 10
BLAS duplication         = disabled
Rays per pixel           = 13
Quality preset           = High
Rasterization Quality    : 
  AO                     = enabled
  AO kernel size         = 4
  Shadows                = enabled
  Shadow map resolution  = 4096
  Shadow PCF kernel size = 4
  Reflections            = enabled
Ray Tracing Quality      : 
  AO                     = enabled
  AO rays                = 4
  AO kernel size         = 4
  Shadows                = enabled
  Shadow rays            = 4
  Shadow kernel size     = 4
  Reflections            = enabled

ProfilingAggregator: 
Render	,	2.7046	,	2.6843	,	2.5893	,	2.5823	,	2.5368	,	2.0859	,	2.3758	,	2.0585	,	2.2996	,	2.5091	,	2.641	,	2.9353	,	2.8675	,	2.774	,	2.7121	,	2.7485	,	2.5739	,	2.3289	,	2.5572	,	2.8186	,	2.9097	,	2.9908	,	2.9749	,	2.9031	,	2.8395	,	2.7704
Update	,	0.0715	,	0.0704	,	0.0672	,	0.064	,	0.0674	,	0.0392	,	0.0689	,	0.0674	,	0.0629	,	0.0606	,	0.0684	,	0.0673	,	0.0679	,	0.0675	,	0.0674	,	0.0681	,	0.0701	,	0.0661	,	0.0691	,	0.0732	,	0.0687	,	0.0679	,	0.0662	,	0.0688	,	0.0686	,	0.0667
DeferredRLGpu	,	1.09734	,	1.07997	,	1.07686	,	1.09437	,	0.986464	,	0.912096	,	0.81216	,	0.500928	,	0.693056	,	0.903072	,	1.11885	,	1.32384	,	1.24512	,	1.16528	,	1.15197	,	1.12749	,	1.05507	,	0.828704	,	1.04131	,	1.20045	,	1.30045	,	1.38237	,	1.33987	,	1.26707	,	1.22394	,	1.17629
Index	,	0	,	1	,	2	,	3	,	4	,	5	,	6	,	7	,	8	,	9	,	10	,	11	,	12	,	13	,	14	,	15	,	16	,	17	,	18	,	19	,	20	,	21	,	22	,	23	,	24	,	25

ProfilingBuildAggregator: 
BuildBottomLevelAS	,	1.4231
BuildBottomLevelASGpu	,	1.53008
BuildTopLevelAS	,	0.6812
BuildTopLevelASGpu	,	0.087328
Duplication	,	1
Triangles	,	262267
Meshes	,	103
BottomLevels	,	1
TopLevelSize	,	64
BottomLevelsSize	,	14622208
Index	,	0

FpsAggregator: 
FPS	,	304	,	356	,	363	,	364	,	370	,	385	,	403	,	432	,	440	,	416	,	375	,	345	,	337	,	346	,	353	,	357	,	364	,	392	,	388	,	358	,	338	,	332	,	323	,	338	,	340	,	348
UPS	,	59	,	61	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	61	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60
FrameTime	,	3.29471	,	2.81482	,	2.75935	,	2.75231	,	2.70408	,	2.60034	,	2.48605	,	2.31583	,	2.27326	,	2.40605	,	2.67025	,	2.90376	,	2.97308	,	2.89216	,	2.83292	,	2.80117	,	2.74955	,	2.55546	,	2.58156	,	2.79526	,	2.96311	,	3.01373	,	3.09636	,	2.96211	,	2.94321	,	2.88128
GigaRays/s	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0
Index	,	0	,	1	,	2	,	3	,	4	,	5	,	6	,	7	,	8	,	9	,	10	,	11	,	12	,	13	,	14	,	15	,	16	,	17	,	18	,	19	,	20	,	21	,	22	,	23	,	24	,	25


AccelerationStructure: 
Ray tracing acceleration structure building statistics: 
	Bottom Level Acceleration Structure: 
		Maximum [B]: 14622208
		Minimum [B]: 14622208
		Currently Allocated [B]: 14622208
		Currently Created [num]: 1
		Current Average [B]: 14622208
		Maximum Triangles [num]: 262267
		Minimum Triangles [num]: 262267
		Total Triangles [num]: 262267
		Maximum Meshes [num]: 103
		Minimum Meshes [num]: 103
		Total Meshes [num]: 103
	Top Level Acceleration Structure: 
		Maximum [B]: 64
		Minimum [B]: 64
		Currently Allocated [B]: 64
		Total Allocated [B]: 64
		Total Created [num]: 1
		Average Allocated [B]: 64
		Maximum Instances [num]: 1
		Minimum Instances [num]: 1
		Total Instances [num]: 1
	Scratch Buffer: 
		Maximum [B]: 11657856
		Minimum [B]: 11657856
		Currently Allocated [B]: 11657856
		Total Allocated [B]: 11657856
		Total Created [num]: 1
		Average Allocated [B]: 11657856
ProfilingStack: 
[PROF] Profiling statistics: 
	Scopes entered : 202513
	Scopes exited : 202512
	Overhead per scope [ticks] : 101.3
	Threads entered : 3
	Threads exited : 0
	============================
		         Sum[Mt]   Parent[%]  Samples[num]       Avg[Mt]   Last/Curr[Mt]        Self[Mt]     Self[%] 	 Name

	main:

		   28881.776 Mt   100.000 %         1 x    28881.777 Mt    28881.776 Mt        0.443 Mt     0.002 % 	 /

		   28881.364 Mt    99.999 %         1 x    28881.365 Mt    28881.364 Mt      253.610 Mt     0.878 % 	 ./Main application loop

		       3.014 Mt     0.010 %         1 x        3.014 Mt        0.000 Mt        3.014 Mt   100.000 % 	 ../TexturedRLInitialization

		       2.961 Mt     0.010 %         1 x        2.961 Mt        0.000 Mt        2.961 Mt   100.000 % 	 ../ShadowMapRLInitialization

		       6.126 Mt     0.021 %         1 x        6.126 Mt        0.000 Mt        6.126 Mt   100.000 % 	 ../RasterResolveRLInitialization

		       2.808 Mt     0.010 %         1 x        2.808 Mt        0.000 Mt        2.808 Mt   100.000 % 	 ../AmbientOcclusionRLInitialization

		       5.380 Mt     0.019 %         1 x        5.380 Mt        0.000 Mt        5.380 Mt   100.000 % 	 ../DeferredRLInitialization

		      51.258 Mt     0.177 %         1 x       51.258 Mt        0.000 Mt        2.255 Mt     4.399 % 	 ../RayTracingRLInitialization

		      49.003 Mt    95.601 %         1 x       49.003 Mt        0.000 Mt       49.003 Mt   100.000 % 	 .../PipelineBuild

		       3.729 Mt     0.013 %         1 x        3.729 Mt        0.000 Mt        3.729 Mt   100.000 % 	 ../ResolveRLInitialization

		    2507.826 Mt     8.683 %         1 x     2507.826 Mt        0.000 Mt        0.209 Mt     0.008 % 	 ../Initialization

		    2507.617 Mt    99.992 %         1 x     2507.617 Mt        0.000 Mt        1.612 Mt     0.064 % 	 .../ScenePrep

		    2441.935 Mt    97.381 %         1 x     2441.935 Mt        0.000 Mt      253.413 Mt    10.378 % 	 ..../SceneLoad

		    1899.821 Mt    77.800 %         1 x     1899.821 Mt        0.000 Mt      563.372 Mt    29.654 % 	 ...../glTF-LoadScene

		    1336.449 Mt    70.346 %        69 x       19.369 Mt        0.000 Mt     1336.449 Mt   100.000 % 	 ....../glTF-LoadImageData

		     221.982 Mt     9.090 %         1 x      221.982 Mt        0.000 Mt      221.982 Mt   100.000 % 	 ...../glTF-CreateScene

		      66.718 Mt     2.732 %         1 x       66.718 Mt        0.000 Mt       66.718 Mt   100.000 % 	 ...../SceneDesc-CreateScene

		       0.789 Mt     0.031 %         1 x        0.789 Mt        0.000 Mt        0.001 Mt     0.063 % 	 ..../ShadowMapRLPrep

		       0.789 Mt    99.937 %         1 x        0.789 Mt        0.000 Mt        0.753 Mt    95.512 % 	 ...../PrepareForRendering

		       0.035 Mt     4.488 %         1 x        0.035 Mt        0.000 Mt        0.035 Mt   100.000 % 	 ....../PrepareDrawBundle

		       3.295 Mt     0.131 %         1 x        3.295 Mt        0.000 Mt        0.001 Mt     0.015 % 	 ..../TexturedRLPrep

		       3.295 Mt    99.985 %         1 x        3.295 Mt        0.000 Mt        3.259 Mt    98.922 % 	 ...../PrepareForRendering

		       0.001 Mt     0.042 %         1 x        0.001 Mt        0.000 Mt        0.001 Mt   100.000 % 	 ....../PrepareMaterials

		       0.034 Mt     1.035 %         1 x        0.034 Mt        0.000 Mt        0.034 Mt   100.000 % 	 ....../PrepareDrawBundle

		       2.478 Mt     0.099 %         1 x        2.478 Mt        0.000 Mt        0.000 Mt     0.012 % 	 ..../DeferredRLPrep

		       2.477 Mt    99.988 %         1 x        2.477 Mt        0.000 Mt        2.082 Mt    84.028 % 	 ...../PrepareForRendering

		       0.002 Mt     0.065 %         1 x        0.002 Mt        0.000 Mt        0.002 Mt   100.000 % 	 ....../PrepareMaterials

		       0.320 Mt    12.929 %         1 x        0.320 Mt        0.000 Mt        0.320 Mt   100.000 % 	 ....../BuildMaterialCache

		       0.074 Mt     2.979 %         1 x        0.074 Mt        0.000 Mt        0.074 Mt   100.000 % 	 ....../PrepareDrawBundle

		      29.964 Mt     1.195 %         1 x       29.964 Mt        0.000 Mt        6.142 Mt    20.498 % 	 ..../RayTracingRLPrep

		       0.069 Mt     0.231 %         1 x        0.069 Mt        0.000 Mt        0.069 Mt   100.000 % 	 ...../PrepareAccelerationStructures

		       0.001 Mt     0.003 %         1 x        0.001 Mt        0.000 Mt        0.001 Mt   100.000 % 	 ...../PrepareCache

		      16.996 Mt    56.722 %         1 x       16.996 Mt        0.000 Mt       16.996 Mt   100.000 % 	 ...../PipelineBuild

		       0.652 Mt     2.176 %         1 x        0.652 Mt        0.000 Mt        0.652 Mt   100.000 % 	 ...../BuildCache

		       4.871 Mt    16.256 %         1 x        4.871 Mt        0.000 Mt        0.000 Mt     0.006 % 	 ...../BuildAccelerationStructures

		       4.871 Mt    99.994 %         1 x        4.871 Mt        0.000 Mt        2.766 Mt    56.797 % 	 ....../AccelerationStructureBuild

		       1.423 Mt    29.218 %         1 x        1.423 Mt        0.000 Mt        1.423 Mt   100.000 % 	 ......./BuildBottomLevelAS

		       0.681 Mt    13.986 %         1 x        0.681 Mt        0.000 Mt        0.681 Mt   100.000 % 	 ......./BuildTopLevelAS

		       1.233 Mt     4.114 %         1 x        1.233 Mt        0.000 Mt        1.233 Mt   100.000 % 	 ...../BuildShaderTables

		      27.544 Mt     1.098 %         1 x       27.544 Mt        0.000 Mt       27.544 Mt   100.000 % 	 ..../GpuSidePrep

		   26044.651 Mt    90.178 %     96679 x        0.269 Mt        3.245 Mt      390.935 Mt     1.501 % 	 ../MainLoop

		     164.352 Mt     0.631 %      1563 x        0.105 Mt        0.039 Mt      164.096 Mt    99.844 % 	 .../Update

		       0.257 Mt     0.156 %         1 x        0.257 Mt        0.000 Mt        0.257 Mt   100.000 % 	 ..../GuiModelApply

		   25489.363 Mt    97.868 %      9469 x        2.692 Mt        3.116 Mt     1047.245 Mt     4.109 % 	 .../Render

		    1540.578 Mt     6.044 %      9469 x        0.163 Mt        0.188 Mt     1524.249 Mt    98.940 % 	 ..../Rasterization

		      10.735 Mt     0.697 %      9469 x        0.001 Mt        0.001 Mt       10.735 Mt   100.000 % 	 ...../DeferredRLPrep

		       5.593 Mt     0.363 %      9469 x        0.001 Mt        0.001 Mt        5.593 Mt   100.000 % 	 ...../ShadowMapRLPrep

		   22901.540 Mt    89.847 %      9469 x        2.419 Mt        2.787 Mt    22901.540 Mt   100.000 % 	 ..../Present

	WindowThread:

		   28880.511 Mt   100.000 %         1 x    28880.512 Mt    28880.511 Mt    28880.511 Mt   100.000 % 	 /

	GpuThread:

		   18408.191 Mt   100.000 %      9469 x        1.944 Mt        2.009 Mt      121.203 Mt     0.658 % 	 /

		       1.625 Mt     0.009 %         1 x        1.625 Mt        0.000 Mt        0.006 Mt     0.360 % 	 ./ScenePrep

		       1.619 Mt    99.640 %         1 x        1.619 Mt        0.000 Mt        0.001 Mt     0.085 % 	 ../AccelerationStructureBuild

		       1.530 Mt    94.520 %         1 x        1.530 Mt        0.000 Mt        1.530 Mt   100.000 % 	 .../BuildBottomLevelASGpu

		       0.087 Mt     5.395 %         1 x        0.087 Mt        0.000 Mt        0.087 Mt   100.000 % 	 .../BuildTopLevelASGpu

		   18251.791 Mt    99.150 %      9469 x        1.928 Mt        2.007 Mt       42.972 Mt     0.235 % 	 ./RasterizationGpu

		   10242.371 Mt    56.117 %      9469 x        1.082 Mt        1.177 Mt    10242.371 Mt   100.000 % 	 ../DeferredRLGpu

		    1389.989 Mt     7.616 %      9469 x        0.147 Mt        0.147 Mt     1389.989 Mt   100.000 % 	 ../ShadowMapRLGpu

		    1350.437 Mt     7.399 %      9469 x        0.143 Mt        0.140 Mt     1350.437 Mt   100.000 % 	 ../AmbientOcclusionRLGpu

		    5226.023 Mt    28.633 %      9469 x        0.552 Mt        0.541 Mt     5226.023 Mt   100.000 % 	 ../RasterResolveRLGpu

		      33.572 Mt     0.182 %      9469 x        0.004 Mt        0.002 Mt       33.572 Mt   100.000 % 	 ./Present


	============================


