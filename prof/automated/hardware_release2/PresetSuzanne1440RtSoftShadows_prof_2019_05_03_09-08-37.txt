Configuration: 
Command line parameters  = E:\programming\FIT-DIP\Masters-DXR-Main-git\build\Measurement\x64\Release\Measurement.exe --scene Suzanne/Suzanne.gltf --profile-automation --profile-camera-track Suzanne/Suzanne.trk --rt-mode --width 2560 --height 1440 --profile-output PresetSuzanne1440RtSoftShadows --quality-preset SoftShadows 
Build type               = Release
Device name              = NVIDIA GeForce RTX 2080 Ti
Device memory MB         = 11049
Ray Tracing backend      = Hardware
Window width             = 2560
Window height            = 1421
Window title             = Direct3D App
Back-buffer count        = 3
Fill Triangles           = enabled
Triangles Font Clock-Wise= enabled
Fast Build AS            = disabled
Target FPS               = 0
Target UPS               = 60
Tearing                  = enabled
VSync                    = disabled
GUI rendering            = disabled
Debug layer              = disabled
GPU validation           = disabled
GPU profiling            = enabled
Render mode              = Ray Tracing
Loaded scene file        = Suzanne/Suzanne.gltf
Using testing scene      = disabled
Duplication              = 1
Duplication in cube      = disabled
Duplication offset       = 10
BLAS duplication         = disabled
Rays per pixel           = 4
Quality preset           = SoftShadows
Rasterization Quality    : 
  AO                     = disabled
  AO kernel size         = 1
  Shadows                = enabled
  Shadow map resolution  = 4096
  Shadow PCF kernel size = 4
  Reflections            = disabled
Ray Tracing Quality      : 
  AO                     = disabled
  AO rays                = 0
  AO kernel size         = 4
  Shadows                = enabled
  Shadow rays            = 4
  Shadow kernel size     = 4
  Reflections            = disabled

ProfilingAggregator: 
Render	,	2.6681	,	2.8346	,	2.9942	,	2.7928	,	2.3518	,	2.3462	,	2.3368	,	2.2809	,	2.3633	,	2.5143	,	2.3851	,	2.9592	,	2.449	,	2.2843	,	2.5505	,	2.681	,	3.4363	,	3.971	,	4.9781	,	3.9052	,	3.2342	,	2.5226	,	2.4357	,	2.2876
Update	,	0.0739	,	0.0707	,	0.0735	,	0.0715	,	0.0702	,	0.0738	,	0.0723	,	0.0729	,	0.0642	,	0.0741	,	0.0628	,	0.0741	,	0.0743	,	0.0713	,	0.0732	,	0.0308	,	0.0715	,	0.0735	,	0.0714	,	0.0733	,	0.0714	,	0.0713	,	0.0722	,	0.0731
RayTracing	,	0.1768	,	0.171	,	0.1765	,	0.1712	,	0.143	,	0.1721	,	0.1718	,	0.1903	,	0.1701	,	0.1707	,	0.1477	,	0.1723	,	0.1698	,	0.1523	,	0.1765	,	0.1497	,	0.1759	,	0.1674	,	0.1724	,	0.168	,	0.1688	,	0.1711	,	0.1943	,	0.1627
RayTracingGpu	,	1.80013	,	2.0271	,	2.15194	,	1.98666	,	1.72838	,	1.58147	,	1.51094	,	1.49005	,	1.55978	,	1.7151	,	1.8167	,	1.79507	,	1.53638	,	1.53878	,	1.69267	,	2.16438	,	2.5703	,	3.10842	,	4.16838	,	3.10563	,	2.42896	,	1.80694	,	1.60435	,	1.55558
DeferredRLGpu	,	0.142912	,	0.185024	,	0.201056	,	0.154272	,	0.100864	,	0.06976	,	0.052928	,	0.045376	,	0.063136	,	0.092704	,	0.113152	,	0.079488	,	0.055264	,	0.05632	,	0.090752	,	0.1888	,	0.233536	,	0.26896	,	0.415392	,	0.318208	,	0.268	,	0.134336	,	0.081984	,	0.070848
RayTracingRLGpu	,	0.329056	,	0.468	,	0.548768	,	0.464384	,	0.302368	,	0.201408	,	0.165088	,	0.148064	,	0.187584	,	0.288512	,	0.35472	,	0.267488	,	0.176384	,	0.17584	,	0.261504	,	0.565088	,	0.862336	,	1.32429	,	2.13856	,	1.24486	,	0.724736	,	0.31952	,	0.205952	,	0.185632
ResolveRLGpu	,	1.32611	,	1.37171	,	1.40016	,	1.3649	,	1.32371	,	1.30861	,	1.2912	,	1.29485	,	1.30611	,	1.33226	,	1.34701	,	1.44653	,	1.30285	,	1.30496	,	1.3353	,	1.40877	,	1.47248	,	1.51315	,	1.61242	,	1.54058	,	1.4343	,	1.35088	,	1.31488	,	1.29725
Index	,	0	,	1	,	2	,	3	,	4	,	5	,	6	,	7	,	8	,	9	,	10	,	11	,	12	,	13	,	14	,	15	,	16	,	17	,	18	,	19	,	20	,	21	,	22	,	23

ProfilingBuildAggregator: 
BuildBottomLevelAS	,	1.065
BuildBottomLevelASGpu	,	0.249184
BuildTopLevelAS	,	0.939
BuildTopLevelASGpu	,	0.061696
Duplication	,	1
Triangles	,	3936
Meshes	,	1
BottomLevels	,	1
TopLevelSize	,	64
BottomLevelsSize	,	222592
Index	,	0

FpsAggregator: 
FPS	,	306	,	334	,	321	,	321	,	355	,	386	,	404	,	411	,	405	,	388	,	369	,	391	,	394	,	403	,	393	,	367	,	302	,	260	,	208	,	213	,	260	,	328	,	382	,	394
UPS	,	59	,	61	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	61	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	61	,	60	,	60	,	60	,	60
FrameTime	,	3.26988	,	3.00269	,	3.1242	,	3.11844	,	2.82037	,	2.59453	,	2.48035	,	2.43763	,	2.47224	,	2.58248	,	2.71608	,	2.56409	,	2.54009	,	2.48613	,	2.5464	,	2.73062	,	3.31411	,	3.85815	,	4.82944	,	4.70071	,	3.8468	,	3.05445	,	2.62136	,	2.54126
GigaRays/s	,	4.45002	,	4.846	,	4.65752	,	4.66613	,	5.15926	,	5.60834	,	5.86653	,	5.96933	,	5.88577	,	5.63453	,	5.35736	,	5.67492	,	5.72855	,	5.85289	,	5.71435	,	5.32883	,	4.39064	,	3.77151	,	3.01299	,	3.0955	,	3.78263	,	4.76388	,	5.55095	,	5.7259
Index	,	0	,	1	,	2	,	3	,	4	,	5	,	6	,	7	,	8	,	9	,	10	,	11	,	12	,	13	,	14	,	15	,	16	,	17	,	18	,	19	,	20	,	21	,	22	,	23


AccelerationStructure: 
Ray tracing acceleration structure building statistics: 
	Bottom Level Acceleration Structure: 
		Maximum [B]: 222592
		Minimum [B]: 222592
		Currently Allocated [B]: 222592
		Currently Created [num]: 1
		Current Average [B]: 222592
		Maximum Triangles [num]: 3936
		Minimum Triangles [num]: 3936
		Total Triangles [num]: 3936
		Maximum Meshes [num]: 1
		Minimum Meshes [num]: 1
		Total Meshes [num]: 1
	Top Level Acceleration Structure: 
		Maximum [B]: 64
		Minimum [B]: 64
		Currently Allocated [B]: 64
		Total Allocated [B]: 64
		Total Created [num]: 1
		Average Allocated [B]: 64
		Maximum Instances [num]: 1
		Minimum Instances [num]: 1
		Total Instances [num]: 1
	Scratch Buffer: 
		Maximum [B]: 32896
		Minimum [B]: 32896
		Currently Allocated [B]: 32896
		Total Allocated [B]: 32896
		Total Created [num]: 1
		Average Allocated [B]: 32896
ProfilingStack: 
[PROF] Profiling statistics: 
	Scopes entered : 156328
	Scopes exited : 156327
	Overhead per scope [ticks] : 101.9
	Threads entered : 3
	Threads exited : 0
	============================
		         Sum[Mt]   Parent[%]  Samples[num]       Avg[Mt]   Last/Curr[Mt]        Self[Mt]     Self[%] 	 Name

	main:

		   24663.473 Mt   100.000 %         1 x    24663.474 Mt    24663.472 Mt        0.538 Mt     0.002 % 	 /

		   24662.965 Mt    99.998 %         1 x    24662.966 Mt    24662.965 Mt      305.722 Mt     1.240 % 	 ./Main application loop

		       5.038 Mt     0.020 %         1 x        5.038 Mt        0.000 Mt        5.038 Mt   100.000 % 	 ../TexturedRLInitialization

		       1.751 Mt     0.007 %         1 x        1.751 Mt        0.000 Mt        1.751 Mt   100.000 % 	 ../ShadowMapRLInitialization

		       5.911 Mt     0.024 %         1 x        5.911 Mt        0.000 Mt        5.911 Mt   100.000 % 	 ../RasterResolveRLInitialization

		       3.447 Mt     0.014 %         1 x        3.447 Mt        0.000 Mt        3.447 Mt   100.000 % 	 ../AmbientOcclusionRLInitialization

		       5.524 Mt     0.022 %         1 x        5.524 Mt        0.000 Mt        5.524 Mt   100.000 % 	 ../DeferredRLInitialization

		      63.534 Mt     0.258 %         1 x       63.534 Mt        0.000 Mt        3.113 Mt     4.899 % 	 ../RayTracingRLInitialization

		      60.421 Mt    95.101 %         1 x       60.421 Mt        0.000 Mt       60.421 Mt   100.000 % 	 .../PipelineBuild

		       3.786 Mt     0.015 %         1 x        3.786 Mt        0.000 Mt        3.786 Mt   100.000 % 	 ../ResolveRLInitialization

		     187.180 Mt     0.759 %         1 x      187.180 Mt        0.000 Mt        0.200 Mt     0.107 % 	 ../Initialization

		     186.980 Mt    99.893 %         1 x      186.980 Mt        0.000 Mt        1.877 Mt     1.004 % 	 .../ScenePrep

		     108.837 Mt    58.208 %         1 x      108.837 Mt        0.000 Mt       20.108 Mt    18.476 % 	 ..../SceneLoad

		      62.832 Mt    57.730 %         1 x       62.832 Mt        0.000 Mt       11.804 Mt    18.786 % 	 ...../glTF-LoadScene

		      51.028 Mt    81.214 %         2 x       25.514 Mt        0.000 Mt       51.028 Mt   100.000 % 	 ....../glTF-LoadImageData

		      10.701 Mt     9.833 %         1 x       10.701 Mt        0.000 Mt       10.701 Mt   100.000 % 	 ...../glTF-CreateScene

		      15.195 Mt    13.961 %         1 x       15.195 Mt        0.000 Mt       15.195 Mt   100.000 % 	 ...../SceneDesc-CreateScene

		       0.781 Mt     0.418 %         1 x        0.781 Mt        0.000 Mt        0.000 Mt     0.064 % 	 ..../ShadowMapRLPrep

		       0.781 Mt    99.936 %         1 x        0.781 Mt        0.000 Mt        0.771 Mt    98.745 % 	 ...../PrepareForRendering

		       0.010 Mt     1.255 %         1 x        0.010 Mt        0.000 Mt        0.010 Mt   100.000 % 	 ....../PrepareDrawBundle

		       2.276 Mt     1.217 %         1 x        2.276 Mt        0.000 Mt        0.001 Mt     0.026 % 	 ..../TexturedRLPrep

		       2.275 Mt    99.974 %         1 x        2.275 Mt        0.000 Mt        2.270 Mt    99.767 % 	 ...../PrepareForRendering

		       0.003 Mt     0.110 %         1 x        0.003 Mt        0.000 Mt        0.003 Mt   100.000 % 	 ....../PrepareMaterials

		       0.003 Mt     0.123 %         1 x        0.003 Mt        0.000 Mt        0.003 Mt   100.000 % 	 ....../PrepareDrawBundle

		       5.013 Mt     2.681 %         1 x        5.013 Mt        0.000 Mt        0.000 Mt     0.010 % 	 ..../DeferredRLPrep

		       5.013 Mt    99.990 %         1 x        5.013 Mt        0.000 Mt        4.242 Mt    84.630 % 	 ...../PrepareForRendering

		       0.001 Mt     0.014 %         1 x        0.001 Mt        0.000 Mt        0.001 Mt   100.000 % 	 ....../PrepareMaterials

		       0.761 Mt    15.181 %         1 x        0.761 Mt        0.000 Mt        0.761 Mt   100.000 % 	 ....../BuildMaterialCache

		       0.009 Mt     0.176 %         1 x        0.009 Mt        0.000 Mt        0.009 Mt   100.000 % 	 ....../PrepareDrawBundle

		      55.257 Mt    29.552 %         1 x       55.257 Mt        0.000 Mt        5.360 Mt     9.701 % 	 ..../RayTracingRLPrep

		       0.168 Mt     0.303 %         1 x        0.168 Mt        0.000 Mt        0.168 Mt   100.000 % 	 ...../PrepareAccelerationStructures

		       0.003 Mt     0.005 %         1 x        0.003 Mt        0.000 Mt        0.003 Mt   100.000 % 	 ...../PrepareCache

		      40.655 Mt    73.574 %         1 x       40.655 Mt        0.000 Mt       40.655 Mt   100.000 % 	 ...../PipelineBuild

		       0.800 Mt     1.449 %         1 x        0.800 Mt        0.000 Mt        0.800 Mt   100.000 % 	 ...../BuildCache

		       5.903 Mt    10.683 %         1 x        5.903 Mt        0.000 Mt        0.001 Mt     0.015 % 	 ...../BuildAccelerationStructures

		       5.902 Mt    99.985 %         1 x        5.902 Mt        0.000 Mt        3.898 Mt    66.048 % 	 ....../AccelerationStructureBuild

		       1.065 Mt    18.044 %         1 x        1.065 Mt        0.000 Mt        1.065 Mt   100.000 % 	 ......./BuildBottomLevelAS

		       0.939 Mt    15.909 %         1 x        0.939 Mt        0.000 Mt        0.939 Mt   100.000 % 	 ......./BuildTopLevelAS

		       2.368 Mt     4.285 %         1 x        2.368 Mt        0.000 Mt        2.368 Mt   100.000 % 	 ...../BuildShaderTables

		      12.938 Mt     6.919 %         1 x       12.938 Mt        0.000 Mt       12.938 Mt   100.000 % 	 ..../GpuSidePrep

		   24081.072 Mt    97.641 %     46977 x        0.513 Mt        2.188 Mt     1043.030 Mt     4.331 % 	 ../MainLoop

		     199.116 Mt     0.827 %      1445 x        0.138 Mt        0.013 Mt      198.522 Mt    99.702 % 	 .../Update

		       0.594 Mt     0.298 %         1 x        0.594 Mt        0.000 Mt        0.594 Mt   100.000 % 	 ..../GuiModelApply

		   22838.926 Mt    94.842 %      8297 x        2.753 Mt        2.106 Mt      913.584 Mt     4.000 % 	 .../Render

		    1436.581 Mt     6.290 %      8297 x        0.173 Mt        0.115 Mt       55.583 Mt     3.869 % 	 ..../RayTracing

		     651.211 Mt    45.331 %      8297 x        0.078 Mt        0.052 Mt      641.467 Mt    98.504 % 	 ...../Deferred

		       9.744 Mt     1.496 %      8297 x        0.001 Mt        0.000 Mt        9.744 Mt   100.000 % 	 ....../DeferredRLPrep

		     508.067 Mt    35.366 %      8297 x        0.061 Mt        0.045 Mt      503.327 Mt    99.067 % 	 ...../RayCasting

		       4.740 Mt     0.933 %      8297 x        0.001 Mt        0.001 Mt        4.740 Mt   100.000 % 	 ....../RayTracingRLPrep

		     221.719 Mt    15.434 %      8297 x        0.027 Mt        0.015 Mt      221.719 Mt   100.000 % 	 ...../Resolve

		   20488.761 Mt    89.710 %      8297 x        2.469 Mt        1.912 Mt    20488.761 Mt   100.000 % 	 ..../Present

	WindowThread:

		   24659.091 Mt   100.000 %         1 x    24659.091 Mt    24659.090 Mt    24659.091 Mt   100.000 % 	 /

	GpuThread:

		   16326.976 Mt   100.000 %      8297 x        1.968 Mt        1.569 Mt      149.181 Mt     0.914 % 	 /

		       0.317 Mt     0.002 %         1 x        0.317 Mt        0.000 Mt        0.005 Mt     1.515 % 	 ./ScenePrep

		       0.312 Mt    98.485 %         1 x        0.312 Mt        0.000 Mt        0.001 Mt     0.369 % 	 ../AccelerationStructureBuild

		       0.249 Mt    79.858 %         1 x        0.249 Mt        0.000 Mt        0.249 Mt   100.000 % 	 .../BuildBottomLevelASGpu

		       0.062 Mt    19.772 %         1 x        0.062 Mt        0.000 Mt        0.062 Mt   100.000 % 	 .../BuildTopLevelASGpu

		   16093.628 Mt    98.571 %      8297 x        1.940 Mt        1.565 Mt       18.944 Mt     0.118 % 	 ./RayTracingGpu

		    1070.109 Mt     6.649 %      8297 x        0.129 Mt        0.070 Mt     1070.109 Mt   100.000 % 	 ../DeferredRLGpu

		    3524.518 Mt    21.900 %      8297 x        0.425 Mt        0.183 Mt     3524.518 Mt   100.000 % 	 ../RayTracingRLGpu

		   11480.058 Mt    71.333 %      8297 x        1.384 Mt        1.310 Mt    11480.058 Mt   100.000 % 	 ../ResolveRLGpu

		      83.850 Mt     0.514 %      8297 x        0.010 Mt        0.002 Mt       83.850 Mt   100.000 % 	 ./Present


	============================


