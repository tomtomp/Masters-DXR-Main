Configuration: 
Command line parameters  = E:\programming\FIT-DIP\Masters-DXR-Main-git\build\Measurement\x64\Release\Measurement.exe --scene Sponza/Sponza.gltf --profile-automation --profile-camera-track Sponza/SponzaPreset.trk --rt-mode --width 2560 --height 1440 --profile-output PresetSponza1440RtHigh --quality-preset High 
Build type               = Release
Device name              = NVIDIA GeForce RTX 2080 Ti
Device memory MB         = 11049
Ray Tracing backend      = Hardware
Window width             = 2560
Window height            = 1421
Window title             = Direct3D App
Back-buffer count        = 3
Fill Triangles           = enabled
Triangles Font Clock-Wise= enabled
Fast Build AS            = disabled
Target FPS               = 0
Target UPS               = 60
Tearing                  = enabled
VSync                    = disabled
GUI rendering            = disabled
Debug layer              = disabled
GPU validation           = disabled
GPU profiling            = enabled
Render mode              = Ray Tracing
Loaded scene file        = Sponza/Sponza.gltf
Using testing scene      = disabled
Duplication              = 1
Duplication in cube      = disabled
Duplication offset       = 10
BLAS duplication         = disabled
Rays per pixel           = 13
Quality preset           = High
Rasterization Quality    : 
  AO                     = enabled
  AO kernel size         = 4
  Shadows                = enabled
  Shadow map resolution  = 4096
  Shadow PCF kernel size = 4
  Reflections            = enabled
Ray Tracing Quality      : 
  AO                     = enabled
  AO rays                = 4
  AO kernel size         = 4
  Shadows                = enabled
  Shadow rays            = 4
  Shadow kernel size     = 4
  Reflections            = enabled

ProfilingAggregator: 
Render	,	11.5676	,	12.045	,	11.7144	,	10.4893	,	10.8074	,	12.0461	,	11.9149	,	12.0412	,	12.6206	,	12.171	,	12.1691	,	12.9496	,	13.08	,	12.6397	,	10.9952	,	10.5464	,	14.6623	,	13.5368	,	9.9115	,	11.5315	,	11.4172	,	11.4731	,	11.4133
Update	,	0.0708	,	0.0758	,	0.0736	,	0.0702	,	0.0712	,	0.0642	,	0.0705	,	0.0714	,	0.0721	,	0.0725	,	0.0719	,	0.0692	,	0.0697	,	0.0719	,	0.0742	,	0.0696	,	0.0719	,	0.0697	,	0.07	,	0.07	,	0.0701	,	0.0692	,	0.0698
RayTracing	,	0.2072	,	0.1855	,	0.1821	,	0.1913	,	0.1718	,	0.1695	,	0.1787	,	0.1751	,	0.1751	,	0.2024	,	0.172	,	0.1899	,	0.1838	,	0.1763	,	0.1738	,	0.172	,	0.1851	,	0.1755	,	0.1746	,	0.2053	,	0.2084	,	0.2108	,	0.1775
RayTracingGpu	,	10.6545	,	10.983	,	10.8404	,	9.58608	,	9.77197	,	11.1618	,	11.131	,	11.1567	,	11.7685	,	11.0919	,	11.1572	,	11.8706	,	12.2197	,	11.7631	,	10.1188	,	9.50182	,	13.5754	,	12.6163	,	8.95181	,	10.5766	,	10.5109	,	10.5252	,	10.5364
DeferredRLGpu	,	2.71296	,	2.50278	,	2.49654	,	1.4632	,	1.36099	,	1.58	,	2.41946	,	2.40262	,	2.07194	,	1.51635	,	1.56176	,	1.77411	,	2.58995	,	2.45133	,	2.12032	,	1.53043	,	1.96413	,	1.89024	,	1.47674	,	2.85997	,	3.84051	,	3.85146	,	3.85712
RayTracingRLGpu	,	5.36973	,	5.77344	,	5.66746	,	5.55046	,	5.53344	,	6.77562	,	5.89139	,	6.22928	,	6.90861	,	6.64944	,	7.02083	,	7.31078	,	6.90746	,	6.76646	,	5.41037	,	5.10646	,	8.88374	,	7.91264	,	4.89654	,	4.91459	,	4.10547	,	4.10138	,	4.10634
ResolveRLGpu	,	2.56746	,	2.70509	,	2.67517	,	2.57037	,	2.87587	,	2.80323	,	2.81821	,	2.52237	,	2.78589	,	2.92397	,	2.57088	,	2.78205	,	2.72067	,	2.54218	,	2.5848	,	2.86208	,	2.72554	,	2.81014	,	2.57654	,	2.80006	,	2.56291	,	2.56925	,	2.5704
Index	,	0	,	1	,	2	,	3	,	4	,	5	,	6	,	7	,	8	,	9	,	10	,	11	,	12	,	13	,	14	,	15	,	16	,	17	,	18	,	19	,	20	,	21	,	22

ProfilingBuildAggregator: 
BuildBottomLevelAS	,	1.7147
BuildBottomLevelASGpu	,	1.5449
BuildTopLevelAS	,	1.7797
BuildTopLevelASGpu	,	0.0888
Duplication	,	1
Triangles	,	262267
Meshes	,	103
BottomLevels	,	1
TopLevelSize	,	64
BottomLevelsSize	,	14622208
Index	,	0

FpsAggregator: 
FPS	,	56	,	81	,	87	,	89	,	92	,	87	,	90	,	80	,	79	,	82	,	83	,	81	,	79	,	76	,	79	,	96	,	78	,	70	,	86	,	98	,	84	,	83	,	83
UPS	,	59	,	61	,	60	,	61	,	60	,	60	,	61	,	60	,	60	,	61	,	61	,	60	,	61	,	60	,	60	,	61	,	60	,	60	,	60	,	61	,	60	,	60	,	61
FrameTime	,	18.062	,	12.4625	,	11.5476	,	11.3004	,	10.915	,	11.5313	,	11.1857	,	12.5497	,	12.7811	,	12.3377	,	12.1787	,	12.4847	,	12.7657	,	13.1756	,	12.6662	,	10.5001	,	12.9231	,	14.3008	,	11.6772	,	10.3084	,	11.9852	,	12.0573	,	12.106
GigaRays/s	,	2.61825	,	3.79466	,	4.09532	,	4.18489	,	4.33264	,	4.10108	,	4.22779	,	3.76829	,	3.70005	,	3.83303	,	3.88309	,	3.78791	,	3.70452	,	3.58926	,	3.73362	,	4.50385	,	3.6594	,	3.30686	,	4.04984	,	4.58761	,	3.94577	,	3.92217	,	3.90641
Index	,	0	,	1	,	2	,	3	,	4	,	5	,	6	,	7	,	8	,	9	,	10	,	11	,	12	,	13	,	14	,	15	,	16	,	17	,	18	,	19	,	20	,	21	,	22


AccelerationStructure: 
Ray tracing acceleration structure building statistics: 
	Bottom Level Acceleration Structure: 
		Maximum [B]: 14622208
		Minimum [B]: 14622208
		Currently Allocated [B]: 14622208
		Currently Created [num]: 1
		Current Average [B]: 14622208
		Maximum Triangles [num]: 262267
		Minimum Triangles [num]: 262267
		Total Triangles [num]: 262267
		Maximum Meshes [num]: 103
		Minimum Meshes [num]: 103
		Total Meshes [num]: 103
	Top Level Acceleration Structure: 
		Maximum [B]: 64
		Minimum [B]: 64
		Currently Allocated [B]: 64
		Total Allocated [B]: 64
		Total Created [num]: 1
		Average Allocated [B]: 64
		Maximum Instances [num]: 1
		Minimum Instances [num]: 1
		Total Instances [num]: 1
	Scratch Buffer: 
		Maximum [B]: 11657856
		Minimum [B]: 11657856
		Currently Allocated [B]: 11657856
		Total Allocated [B]: 11657856
		Total Created [num]: 1
		Average Allocated [B]: 11657856
ProfilingStack: 
[PROF] Profiling statistics: 
	Scopes entered : 83990
	Scopes exited : 83989
	Overhead per scope [ticks] : 103.7
	Threads entered : 3
	Threads exited : 0
	============================
		         Sum[Mt]   Parent[%]  Samples[num]       Avg[Mt]   Last/Curr[Mt]        Self[Mt]     Self[%] 	 Name

	main:

		   26591.303 Mt   100.000 %         1 x    26591.304 Mt    26591.303 Mt        0.427 Mt     0.002 % 	 /

		   26590.906 Mt    99.999 %         1 x    26590.907 Mt    26590.906 Mt      266.347 Mt     1.002 % 	 ./Main application loop

		       2.890 Mt     0.011 %         1 x        2.890 Mt        0.000 Mt        2.890 Mt   100.000 % 	 ../TexturedRLInitialization

		       1.162 Mt     0.004 %         1 x        1.162 Mt        0.000 Mt        1.162 Mt   100.000 % 	 ../ShadowMapRLInitialization

		       4.255 Mt     0.016 %         1 x        4.255 Mt        0.000 Mt        4.255 Mt   100.000 % 	 ../RasterResolveRLInitialization

		       2.688 Mt     0.010 %         1 x        2.688 Mt        0.000 Mt        2.688 Mt   100.000 % 	 ../AmbientOcclusionRLInitialization

		       5.104 Mt     0.019 %         1 x        5.104 Mt        0.000 Mt        5.104 Mt   100.000 % 	 ../DeferredRLInitialization

		      51.787 Mt     0.195 %         1 x       51.787 Mt        0.000 Mt        3.496 Mt     6.751 % 	 ../RayTracingRLInitialization

		      48.291 Mt    93.249 %         1 x       48.291 Mt        0.000 Mt       48.291 Mt   100.000 % 	 .../PipelineBuild

		       3.790 Mt     0.014 %         1 x        3.790 Mt        0.000 Mt        3.790 Mt   100.000 % 	 ../ResolveRLInitialization

		    3064.097 Mt    11.523 %         1 x     3064.097 Mt        0.000 Mt        0.407 Mt     0.013 % 	 ../Initialization

		    3063.690 Mt    99.987 %         1 x     3063.690 Mt        0.000 Mt        2.053 Mt     0.067 % 	 .../ScenePrep

		    2994.644 Mt    97.746 %         1 x     2994.644 Mt        0.000 Mt      804.543 Mt    26.866 % 	 ..../SceneLoad

		    1898.500 Mt    63.397 %         1 x     1898.500 Mt        0.000 Mt      545.619 Mt    28.739 % 	 ...../glTF-LoadScene

		    1352.882 Mt    71.261 %        69 x       19.607 Mt        0.000 Mt     1352.882 Mt   100.000 % 	 ....../glTF-LoadImageData

		     223.670 Mt     7.469 %         1 x      223.670 Mt        0.000 Mt      223.670 Mt   100.000 % 	 ...../glTF-CreateScene

		      67.931 Mt     2.268 %         1 x       67.931 Mt        0.000 Mt       67.931 Mt   100.000 % 	 ...../SceneDesc-CreateScene

		       1.054 Mt     0.034 %         1 x        1.054 Mt        0.000 Mt        0.001 Mt     0.057 % 	 ..../ShadowMapRLPrep

		       1.053 Mt    99.943 %         1 x        1.053 Mt        0.000 Mt        1.009 Mt    95.757 % 	 ...../PrepareForRendering

		       0.045 Mt     4.243 %         1 x        0.045 Mt        0.000 Mt        0.045 Mt   100.000 % 	 ....../PrepareDrawBundle

		       3.722 Mt     0.121 %         1 x        3.722 Mt        0.000 Mt        0.001 Mt     0.013 % 	 ..../TexturedRLPrep

		       3.721 Mt    99.987 %         1 x        3.721 Mt        0.000 Mt        3.683 Mt    98.965 % 	 ...../PrepareForRendering

		       0.002 Mt     0.048 %         1 x        0.002 Mt        0.000 Mt        0.002 Mt   100.000 % 	 ....../PrepareMaterials

		       0.037 Mt     0.986 %         1 x        0.037 Mt        0.000 Mt        0.037 Mt   100.000 % 	 ....../PrepareDrawBundle

		       3.135 Mt     0.102 %         1 x        3.135 Mt        0.000 Mt        0.001 Mt     0.029 % 	 ..../DeferredRLPrep

		       3.134 Mt    99.971 %         1 x        3.134 Mt        0.000 Mt        2.422 Mt    77.298 % 	 ...../PrepareForRendering

		       0.001 Mt     0.026 %         1 x        0.001 Mt        0.000 Mt        0.001 Mt   100.000 % 	 ....../PrepareMaterials

		       0.496 Mt    15.844 %         1 x        0.496 Mt        0.000 Mt        0.496 Mt   100.000 % 	 ....../BuildMaterialCache

		       0.214 Mt     6.832 %         1 x        0.214 Mt        0.000 Mt        0.214 Mt   100.000 % 	 ....../PrepareDrawBundle

		      25.964 Mt     0.847 %         1 x       25.964 Mt        0.000 Mt        2.807 Mt    10.812 % 	 ..../RayTracingRLPrep

		       0.184 Mt     0.708 %         1 x        0.184 Mt        0.000 Mt        0.184 Mt   100.000 % 	 ...../PrepareAccelerationStructures

		       0.003 Mt     0.010 %         1 x        0.003 Mt        0.000 Mt        0.003 Mt   100.000 % 	 ...../PrepareCache

		      13.947 Mt    53.714 %         1 x       13.947 Mt        0.000 Mt       13.947 Mt   100.000 % 	 ...../PipelineBuild

		       0.708 Mt     2.727 %         1 x        0.708 Mt        0.000 Mt        0.708 Mt   100.000 % 	 ...../BuildCache

		       6.736 Mt    25.943 %         1 x        6.736 Mt        0.000 Mt        0.000 Mt     0.004 % 	 ...../BuildAccelerationStructures

		       6.736 Mt    99.996 %         1 x        6.736 Mt        0.000 Mt        3.241 Mt    48.120 % 	 ....../AccelerationStructureBuild

		       1.715 Mt    25.457 %         1 x        1.715 Mt        0.000 Mt        1.715 Mt   100.000 % 	 ......./BuildBottomLevelAS

		       1.780 Mt    26.422 %         1 x        1.780 Mt        0.000 Mt        1.780 Mt   100.000 % 	 ......./BuildTopLevelAS

		       1.580 Mt     6.087 %         1 x        1.580 Mt        0.000 Mt        1.580 Mt   100.000 % 	 ...../BuildShaderTables

		      33.118 Mt     1.081 %         1 x       33.118 Mt        0.000 Mt       33.118 Mt   100.000 % 	 ..../GpuSidePrep

		   23188.785 Mt    87.206 %     57775 x        0.401 Mt       12.460 Mt      442.654 Mt     1.909 % 	 ../MainLoop

		     185.456 Mt     0.800 %      1390 x        0.133 Mt        0.035 Mt      184.826 Mt    99.660 % 	 .../Update

		       0.630 Mt     0.340 %         1 x        0.630 Mt        0.000 Mt        0.630 Mt   100.000 % 	 ..../GuiModelApply

		   22560.675 Mt    97.291 %      1901 x       11.868 Mt       12.326 Mt      231.231 Mt     1.025 % 	 .../Render

		     351.740 Mt     1.559 %      1901 x        0.185 Mt        0.251 Mt       12.748 Mt     3.624 % 	 ..../RayTracing

		     151.690 Mt    43.126 %      1901 x        0.080 Mt        0.099 Mt      150.113 Mt    98.960 % 	 ...../Deferred

		       1.577 Mt     1.040 %      1901 x        0.001 Mt        0.001 Mt        1.577 Mt   100.000 % 	 ....../DeferredRLPrep

		     124.846 Mt    35.494 %      1901 x        0.066 Mt        0.101 Mt      123.707 Mt    99.088 % 	 ...../RayCasting

		       1.139 Mt     0.912 %      1901 x        0.001 Mt        0.001 Mt        1.139 Mt   100.000 % 	 ....../RayTracingRLPrep

		      62.456 Mt    17.756 %      1901 x        0.033 Mt        0.042 Mt       62.456 Mt   100.000 % 	 ...../Resolve

		   21977.705 Mt    97.416 %      1901 x       11.561 Mt       11.902 Mt    21977.705 Mt   100.000 % 	 ..../Present

	WindowThread:

		   26590.065 Mt   100.000 %         1 x    26590.066 Mt    26590.065 Mt    26590.065 Mt   100.000 % 	 /

	GpuThread:

		   20929.703 Mt   100.000 %      1901 x       11.010 Mt       11.384 Mt      150.011 Mt     0.717 % 	 /

		       1.641 Mt     0.008 %         1 x        1.641 Mt        0.000 Mt        0.006 Mt     0.372 % 	 ./ScenePrep

		       1.635 Mt    99.628 %         1 x        1.635 Mt        0.000 Mt        0.002 Mt     0.094 % 	 ../AccelerationStructureBuild

		       1.545 Mt    94.476 %         1 x        1.545 Mt        0.000 Mt        1.545 Mt   100.000 % 	 .../BuildBottomLevelASGpu

		       0.089 Mt     5.430 %         1 x        0.089 Mt        0.000 Mt        0.089 Mt   100.000 % 	 .../BuildTopLevelASGpu

		   20703.802 Mt    98.921 %      1901 x       10.891 Mt       11.290 Mt        4.892 Mt     0.024 % 	 ./RayTracingGpu

		    4176.666 Mt    20.173 %      1901 x        2.197 Mt        4.174 Mt     4176.666 Mt   100.000 % 	 ../DeferredRLGpu

		   11374.904 Mt    54.941 %      1901 x        5.984 Mt        4.426 Mt    11374.904 Mt   100.000 % 	 ../RayTracingRLGpu

		    5147.340 Mt    24.862 %      1901 x        2.708 Mt        2.689 Mt     5147.340 Mt   100.000 % 	 ../ResolveRLGpu

		      74.248 Mt     0.355 %      1901 x        0.039 Mt        0.094 Mt       74.248 Mt   100.000 % 	 ./Present


	============================


