Configuration: 
Command line parameters  = E:\programming\FIT-DIP\Masters-DXR-Main-git\build\Measurement\x64\Release\Measurement.exe --scene Suzanne/Suzanne.gltf --profile-automation --profile-camera-track Suzanne/Suzanne.trk --rt-mode-pure --width 2560 --height 1440 --profile-output TrackSuzanne1440Rp 
Build type               = Release
Device name              = NVIDIA GeForce RTX 2080 Ti
Device memory MB         = 11049
Ray Tracing backend      = Hardware
Window width             = 2560
Window height            = 1421
Window title             = Direct3D App
Back-buffer count        = 3
Fill Triangles           = enabled
Triangles Font Clock-Wise= enabled
Fast Build AS            = disabled
Target FPS               = 0
Target UPS               = 60
Tearing                  = enabled
VSync                    = disabled
GUI rendering            = disabled
Debug layer              = disabled
GPU validation           = disabled
GPU profiling            = enabled
Render mode              = Ray Tracing
Loaded scene file        = Suzanne/Suzanne.gltf
Using testing scene      = disabled
Duplication              = 1
Duplication in cube      = disabled
Duplication offset       = 10
BLAS duplication         = disabled
Rays per pixel           = 14
Quality preset           = High
Rasterization Quality    : 
  AO                     = enabled
  AO kernel size         = 4
  Shadows                = enabled
  Shadow map resolution  = 4096
  Shadow PCF kernel size = 4
  Reflections            = enabled
Ray Tracing Quality      : 
  AO                     = enabled
  AO rays                = 4
  AO kernel size         = 4
  Shadows                = enabled
  Shadow rays            = 4
  Shadow kernel size     = 4
  Reflections            = enabled

ProfilingAggregator: 
Render	,	1.41	,	1.8347	,	2.0397	,	1.8497	,	1.326	,	1.0105	,	0.8282	,	0.8061	,	0.9362	,	1.2214	,	1.4684	,	1.1283	,	0.8379	,	0.8943	,	1.2195	,	2.0516	,	2.6028	,	3.4526	,	5.1538	,	3.2119	,	2.2671	,	1.3306	,	0.9613	,	0.8649
Update	,	0.0749	,	0.0717	,	0.0718	,	0.0675	,	0.0551	,	0.0452	,	0.0639	,	0.0528	,	0.0626	,	0.0688	,	0.0715	,	0.0695	,	0.0708	,	0.0494	,	0.0596	,	0.0736	,	0.0693	,	0.0727	,	0.0702	,	0.0715	,	0.075	,	0.0725	,	0.1161	,	0.0564
RayTracing	,	0.0648	,	0.0882	,	0.0873	,	0.0808	,	0.0552	,	0.0702	,	0.0561	,	0.0617	,	0.0673	,	0.0705	,	0.0815	,	0.0739	,	0.0609	,	0.0747	,	0.058	,	0.0862	,	0.0859	,	0.0878	,	0.0969	,	0.0894	,	0.0821	,	0.0602	,	0.0689	,	0.0478
RayTracingGpu	,	0.82608	,	1.15075	,	1.35757	,	1.17181	,	0.808256	,	0.568384	,	0.474144	,	0.432704	,	0.538816	,	0.783392	,	0.925824	,	0.703168	,	0.496128	,	0.493088	,	0.696128	,	1.33907	,	1.91203	,	2.72704	,	4.42275	,	2.51859	,	1.61024	,	0.814304	,	0.555552	,	0.505984
RayTracingRLGpu	,	0.825312	,	1.14995	,	1.35568	,	1.17082	,	0.807232	,	0.566528	,	0.473248	,	0.430752	,	0.5368	,	0.782592	,	0.924896	,	0.701152	,	0.494368	,	0.491264	,	0.694272	,	1.33702	,	1.91126	,	2.72502	,	4.42182	,	2.5175	,	1.60851	,	0.813504	,	0.553568	,	0.505056
Index	,	0	,	1	,	2	,	3	,	4	,	5	,	6	,	7	,	8	,	9	,	10	,	11	,	12	,	13	,	14	,	15	,	16	,	17	,	18	,	19	,	20	,	21	,	22	,	23

ProfilingBuildAggregator: 
BuildBottomLevelAS	,	1.0977
BuildBottomLevelASGpu	,	0.2528
BuildTopLevelAS	,	0.9524
BuildTopLevelASGpu	,	0.06288
Duplication	,	1
Triangles	,	3936
Meshes	,	1
BottomLevels	,	1
TopLevelSize	,	64
BottomLevelsSize	,	222592
Index	,	0

FpsAggregator: 
FPS	,	577	,	571	,	486	,	483	,	584	,	812	,	1014	,	1086	,	1069	,	864	,	649	,	663	,	899	,	1064	,	929	,	603	,	420	,	325	,	225	,	241	,	352	,	532	,	816	,	1001
UPS	,	59	,	60	,	61	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	61	,	60	,	60	,	60
FrameTime	,	1.73456	,	1.75249	,	2.06129	,	2.07386	,	1.71366	,	1.23231	,	0.986881	,	0.921036	,	0.935848	,	1.15796	,	1.54169	,	1.50869	,	1.11238	,	0.940536	,	1.07724	,	1.65997	,	2.38695	,	3.07727	,	4.4542	,	4.15709	,	2.8437	,	1.87979	,	1.22649	,	0.999116
GigaRays/s	,	29.3611	,	29.0608	,	24.7071	,	24.5575	,	29.7192	,	41.3278	,	51.6057	,	55.295	,	54.4198	,	43.9813	,	33.0343	,	33.7568	,	45.7836	,	54.1485	,	47.2771	,	30.6805	,	21.3363	,	16.55	,	11.4338	,	12.251	,	17.9093	,	27.0928	,	41.524	,	50.9737
Index	,	0	,	1	,	2	,	3	,	4	,	5	,	6	,	7	,	8	,	9	,	10	,	11	,	12	,	13	,	14	,	15	,	16	,	17	,	18	,	19	,	20	,	21	,	22	,	23


AccelerationStructure: 
Ray tracing acceleration structure building statistics: 
	Bottom Level Acceleration Structure: 
		Maximum [B]: 222592
		Minimum [B]: 222592
		Currently Allocated [B]: 222592
		Currently Created [num]: 1
		Current Average [B]: 222592
		Maximum Triangles [num]: 3936
		Minimum Triangles [num]: 3936
		Total Triangles [num]: 3936
		Maximum Meshes [num]: 1
		Minimum Meshes [num]: 1
		Total Meshes [num]: 1
	Top Level Acceleration Structure: 
		Maximum [B]: 64
		Minimum [B]: 64
		Currently Allocated [B]: 64
		Total Allocated [B]: 64
		Total Created [num]: 1
		Average Allocated [B]: 64
		Maximum Instances [num]: 1
		Minimum Instances [num]: 1
		Total Instances [num]: 1
	Scratch Buffer: 
		Maximum [B]: 32896
		Minimum [B]: 32896
		Currently Allocated [B]: 32896
		Total Allocated [B]: 32896
		Total Created [num]: 1
		Average Allocated [B]: 32896
ProfilingStack: 
[PROF] Profiling statistics: 
	Scopes entered : 207554
	Scopes exited : 207553
	Overhead per scope [ticks] : 101.3
	Threads entered : 3
	Threads exited : 0
	============================
		         Sum[Mt]   Parent[%]  Samples[num]       Avg[Mt]   Last/Curr[Mt]        Self[Mt]     Self[%] 	 Name

	main:

		   24487.533 Mt   100.000 %         1 x    24487.533 Mt    24487.533 Mt        0.571 Mt     0.002 % 	 /

		   24486.974 Mt    99.998 %         1 x    24486.976 Mt    24486.974 Mt      270.562 Mt     1.105 % 	 ./Main application loop

		       3.263 Mt     0.013 %         1 x        3.263 Mt        0.000 Mt        3.263 Mt   100.000 % 	 ../TexturedRLInitialization

		       1.356 Mt     0.006 %         1 x        1.356 Mt        0.000 Mt        1.356 Mt   100.000 % 	 ../ShadowMapRLInitialization

		       4.877 Mt     0.020 %         1 x        4.877 Mt        0.000 Mt        4.877 Mt   100.000 % 	 ../RasterResolveRLInitialization

		       2.892 Mt     0.012 %         1 x        2.892 Mt        0.000 Mt        2.892 Mt   100.000 % 	 ../AmbientOcclusionRLInitialization

		       5.385 Mt     0.022 %         1 x        5.385 Mt        0.000 Mt        5.385 Mt   100.000 % 	 ../DeferredRLInitialization

		      65.801 Mt     0.269 %         1 x       65.801 Mt        0.000 Mt        2.196 Mt     3.337 % 	 ../RayTracingRLInitialization

		      63.605 Mt    96.663 %         1 x       63.605 Mt        0.000 Mt       63.605 Mt   100.000 % 	 .../PipelineBuild

		       3.834 Mt     0.016 %         1 x        3.834 Mt        0.000 Mt        3.834 Mt   100.000 % 	 ../ResolveRLInitialization

		     102.434 Mt     0.418 %         1 x      102.434 Mt        0.000 Mt        0.459 Mt     0.448 % 	 ../Initialization

		     101.975 Mt    99.552 %         1 x      101.975 Mt        0.000 Mt        1.732 Mt     1.699 % 	 .../ScenePrep

		      63.073 Mt    61.851 %         1 x       63.073 Mt        0.000 Mt       15.051 Mt    23.863 % 	 ..../SceneLoad

		      39.587 Mt    62.763 %         1 x       39.587 Mt        0.000 Mt        8.665 Mt    21.889 % 	 ...../glTF-LoadScene

		      30.922 Mt    78.111 %         2 x       15.461 Mt        0.000 Mt       30.922 Mt   100.000 % 	 ....../glTF-LoadImageData

		       6.465 Mt    10.250 %         1 x        6.465 Mt        0.000 Mt        6.465 Mt   100.000 % 	 ...../glTF-CreateScene

		       1.970 Mt     3.124 %         1 x        1.970 Mt        0.000 Mt        1.970 Mt   100.000 % 	 ...../SceneDesc-CreateScene

		       0.302 Mt     0.296 %         1 x        0.302 Mt        0.000 Mt        0.001 Mt     0.165 % 	 ..../ShadowMapRLPrep

		       0.302 Mt    99.835 %         1 x        0.302 Mt        0.000 Mt        0.293 Mt    97.150 % 	 ...../PrepareForRendering

		       0.009 Mt     2.850 %         1 x        0.009 Mt        0.000 Mt        0.009 Mt   100.000 % 	 ....../PrepareDrawBundle

		       0.790 Mt     0.775 %         1 x        0.790 Mt        0.000 Mt        0.001 Mt     0.063 % 	 ..../TexturedRLPrep

		       0.790 Mt    99.937 %         1 x        0.790 Mt        0.000 Mt        0.786 Mt    99.481 % 	 ...../PrepareForRendering

		       0.002 Mt     0.203 %         1 x        0.002 Mt        0.000 Mt        0.002 Mt   100.000 % 	 ....../PrepareMaterials

		       0.003 Mt     0.317 %         1 x        0.003 Mt        0.000 Mt        0.003 Mt   100.000 % 	 ....../PrepareDrawBundle

		       1.531 Mt     1.502 %         1 x        1.531 Mt        0.000 Mt        0.000 Mt     0.020 % 	 ..../DeferredRLPrep

		       1.531 Mt    99.980 %         1 x        1.531 Mt        0.000 Mt        1.172 Mt    76.558 % 	 ...../PrepareForRendering

		       0.001 Mt     0.046 %         1 x        0.001 Mt        0.000 Mt        0.001 Mt   100.000 % 	 ....../PrepareMaterials

		       0.355 Mt    23.194 %         1 x        0.355 Mt        0.000 Mt        0.355 Mt   100.000 % 	 ....../BuildMaterialCache

		       0.003 Mt     0.202 %         1 x        0.003 Mt        0.000 Mt        0.003 Mt   100.000 % 	 ....../PrepareDrawBundle

		      22.696 Mt    22.256 %         1 x       22.696 Mt        0.000 Mt        2.687 Mt    11.839 % 	 ..../RayTracingRLPrep

		       0.043 Mt     0.192 %         1 x        0.043 Mt        0.000 Mt        0.043 Mt   100.000 % 	 ...../PrepareAccelerationStructures

		       0.001 Mt     0.004 %         1 x        0.001 Mt        0.000 Mt        0.001 Mt   100.000 % 	 ...../PrepareCache

		      13.340 Mt    58.776 %         1 x       13.340 Mt        0.000 Mt       13.340 Mt   100.000 % 	 ...../PipelineBuild

		       0.361 Mt     1.592 %         1 x        0.361 Mt        0.000 Mt        0.361 Mt   100.000 % 	 ...../BuildCache

		       4.731 Mt    20.844 %         1 x        4.731 Mt        0.000 Mt        0.000 Mt     0.011 % 	 ...../BuildAccelerationStructures

		       4.730 Mt    99.989 %         1 x        4.730 Mt        0.000 Mt        2.680 Mt    56.659 % 	 ....../AccelerationStructureBuild

		       1.098 Mt    23.206 %         1 x        1.098 Mt        0.000 Mt        1.098 Mt   100.000 % 	 ......./BuildBottomLevelAS

		       0.952 Mt    20.134 %         1 x        0.952 Mt        0.000 Mt        0.952 Mt   100.000 % 	 ......./BuildTopLevelAS

		       1.533 Mt     6.754 %         1 x        1.533 Mt        0.000 Mt        1.533 Mt   100.000 % 	 ...../BuildShaderTables

		      11.850 Mt    11.620 %         1 x       11.850 Mt        0.000 Mt       11.850 Mt   100.000 % 	 ..../GpuSidePrep

		   24026.571 Mt    98.120 %     75931 x        0.316 Mt        1.039 Mt      411.163 Mt     1.711 % 	 ../MainLoop

		     167.866 Mt     0.699 %      1442 x        0.116 Mt        0.053 Mt      167.586 Mt    99.833 % 	 .../Update

		       0.280 Mt     0.167 %         1 x        0.280 Mt        0.000 Mt        0.280 Mt   100.000 % 	 ..../GuiModelApply

		   23447.541 Mt    97.590 %     16267 x        1.441 Mt        0.984 Mt     1256.205 Mt     5.358 % 	 .../Render

		    1163.228 Mt     4.961 %     16267 x        0.072 Mt        0.068 Mt       74.054 Mt     6.366 % 	 ..../RayTracing

		    1089.174 Mt    93.634 %     16267 x        0.067 Mt        0.064 Mt     1076.862 Mt    98.870 % 	 ...../RayCasting

		      12.312 Mt     1.130 %     16267 x        0.001 Mt        0.001 Mt       12.312 Mt   100.000 % 	 ....../RayTracingRLPrep

		   21028.107 Mt    89.682 %     16267 x        1.293 Mt        0.843 Mt    21028.107 Mt   100.000 % 	 ..../Present

	WindowThread:

		   24483.764 Mt   100.000 %         1 x    24483.764 Mt    24483.764 Mt    24483.764 Mt   100.000 % 	 /

	GpuThread:

		   14345.693 Mt   100.000 %     16267 x        0.882 Mt        0.505 Mt      137.098 Mt     0.956 % 	 /

		       0.323 Mt     0.002 %         1 x        0.323 Mt        0.000 Mt        0.006 Mt     1.745 % 	 ./ScenePrep

		       0.317 Mt    98.255 %         1 x        0.317 Mt        0.000 Mt        0.001 Mt     0.464 % 	 ../AccelerationStructureBuild

		       0.253 Mt    79.709 %         1 x        0.253 Mt        0.000 Mt        0.253 Mt   100.000 % 	 .../BuildBottomLevelASGpu

		       0.063 Mt    19.826 %         1 x        0.063 Mt        0.000 Mt        0.063 Mt   100.000 % 	 .../BuildTopLevelASGpu

		   14175.774 Mt    98.816 %     16267 x        0.871 Mt        0.502 Mt       22.188 Mt     0.157 % 	 ./RayTracingGpu

		   14153.585 Mt    99.843 %     16267 x        0.870 Mt        0.501 Mt    14153.585 Mt   100.000 % 	 ../RayTracingRLGpu

		      32.499 Mt     0.227 %     16267 x        0.002 Mt        0.003 Mt       32.499 Mt   100.000 % 	 ./Present


	============================


