Configuration: 
Command line parameters  = E:\programming\FIT-DIP\Masters-DXR-Main-git\build\Measurement\x64\Release\Measurement.exe --scene Sponza/Sponza.gltf --profile-automation --profile-camera-track Sponza/Sponza.trk --rt-mode --width 1024 --height 768 --profile-output TrackSponza768Rt 
Build type               = Release
Device name              = NVIDIA GeForce RTX 2080 Ti
Device memory MB         = 11049
Ray Tracing backend      = Hardware
Window width             = 1024
Window height            = 768
Window title             = Direct3D App
Back-buffer count        = 3
Fill Triangles           = enabled
Triangles Font Clock-Wise= enabled
Fast Build AS            = disabled
Target FPS               = 0
Target UPS               = 60
Tearing                  = enabled
VSync                    = disabled
GUI rendering            = disabled
Debug layer              = disabled
GPU validation           = disabled
GPU profiling            = enabled
Render mode              = Ray Tracing
Loaded scene file        = Sponza/Sponza.gltf
Using testing scene      = disabled
Duplication              = 1
Duplication in cube      = disabled
Duplication offset       = 10
BLAS duplication         = disabled
Rays per pixel           = 13
Quality preset           = High
Rasterization Quality    : 
  AO                     = enabled
  AO kernel size         = 4
  Shadows                = enabled
  Shadow map resolution  = 4096
  Shadow PCF kernel size = 4
  Reflections            = enabled
Ray Tracing Quality      : 
  AO                     = enabled
  AO rays                = 4
  AO kernel size         = 4
  Shadows                = enabled
  Shadow rays            = 4
  Shadow kernel size     = 4
  Reflections            = enabled

ProfilingAggregator: 
Render	,	3.8825	,	3.8065	,	3.807	,	3.6433	,	3.5914	,	3.3215	,	3.353	,	3.0609	,	3.1647	,	3.3419	,	3.3964	,	3.6509	,	3.57	,	3.5275	,	3.3766	,	3.4019	,	3.4304	,	3.6083	,	3.4088	,	4.0125	,	5.0044	,	4.5967	,	4.1499	,	3.8616	,	3.6205	,	3.5833
Update	,	0.0683	,	0.0694	,	0.07	,	0.0711	,	0.0745	,	0.0646	,	0.0707	,	0.0687	,	0.0703	,	0.0688	,	0.0686	,	0.0691	,	0.0679	,	0.0707	,	0.0689	,	0.0687	,	0.069	,	0.0703	,	0.0625	,	0.0692	,	0.0695	,	0.0685	,	0.0709	,	0.0628	,	0.0697	,	0.0673
RayTracing	,	0.1882	,	0.1752	,	0.1753	,	0.1779	,	0.1772	,	0.1563	,	0.1806	,	0.1724	,	0.1759	,	0.1736	,	0.1787	,	0.178	,	0.176	,	0.1751	,	0.1726	,	0.1735	,	0.1767	,	0.2033	,	0.2053	,	0.5261	,	0.1754	,	0.1733	,	0.1841	,	0.1731	,	0.1743	,	0.1841
RayTracingGpu	,	3.01834	,	3.02269	,	2.98714	,	2.90922	,	2.76803	,	2.66358	,	2.55587	,	2.3055	,	2.3887	,	2.64531	,	2.54026	,	2.79277	,	2.72976	,	2.70541	,	2.6193	,	2.64115	,	2.59002	,	2.66352	,	2.65264	,	2.88115	,	4.20867	,	3.74525	,	3.34384	,	3.04054	,	2.80189	,	2.79683
DeferredRLGpu	,	1.0913	,	1.06208	,	1.09024	,	1.05923	,	0.98544	,	0.908672	,	0.8136	,	0.530304	,	0.693344	,	0.907552	,	1.14278	,	1.33885	,	1.26186	,	1.18499	,	1.14682	,	1.12726	,	1.01642	,	0.858624	,	1.02547	,	1.21494	,	1.47504	,	1.40566	,	1.31798	,	1.23264	,	1.23578	,	1.15402
RayTracingRLGpu	,	1.37181	,	1.40717	,	1.3409	,	1.29398	,	1.22483	,	1.19862	,	1.1896	,	1.2191	,	1.1376	,	1.15507	,	0.843328	,	0.906272	,	0.9192	,	0.967936	,	0.923936	,	0.959104	,	1.01664	,	1.04224	,	1.06803	,	1.1055	,	2.16886	,	1.7799	,	1.46864	,	1.24896	,	1.01043	,	1.07859
ResolveRLGpu	,	0.552096	,	0.551264	,	0.55216	,	0.55216	,	0.553184	,	0.552992	,	0.549568	,	0.55392	,	0.552768	,	0.577728	,	0.550912	,	0.545536	,	0.5448	,	0.547616	,	0.54464	,	0.55264	,	0.553024	,	0.75904	,	0.555904	,	0.558624	,	0.56096	,	0.557728	,	0.555104	,	0.55472	,	0.552672	,	0.560352
Index	,	0	,	1	,	2	,	3	,	4	,	5	,	6	,	7	,	8	,	9	,	10	,	11	,	12	,	13	,	14	,	15	,	16	,	17	,	18	,	19	,	20	,	21	,	22	,	23	,	24	,	25

ProfilingBuildAggregator: 
BuildBottomLevelAS	,	1.4409
BuildBottomLevelASGpu	,	1.51034
BuildTopLevelAS	,	0.7732
BuildTopLevelASGpu	,	0.087392
Duplication	,	1
Triangles	,	262267
Meshes	,	103
BottomLevels	,	1
TopLevelSize	,	64
BottomLevelsSize	,	14622208
Index	,	0

FpsAggregator: 
FPS	,	209	,	249	,	256	,	256	,	268	,	277	,	287	,	298	,	300	,	311	,	294	,	282	,	275	,	276	,	284	,	285	,	284	,	295	,	286	,	276	,	224	,	209	,	225	,	240	,	266	,	270
UPS	,	59	,	61	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	61	,	60	,	60	,	60	,	60	,	60	,	61	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	61	,	60	,	60
FrameTime	,	4.79344	,	4.02884	,	3.9196	,	3.90895	,	3.74171	,	3.62061	,	3.48781	,	3.35727	,	3.34288	,	3.22554	,	3.40642	,	3.55713	,	3.64776	,	3.63459	,	3.53119	,	3.52069	,	3.52563	,	3.39999	,	3.49736	,	3.63758	,	4.4689	,	4.79566	,	4.44538	,	4.18072	,	3.76144	,	3.71125
GigaRays/s	,	2.13283	,	2.53761	,	2.60833	,	2.61544	,	2.73234	,	2.82373	,	2.93124	,	3.04522	,	3.05833	,	3.16958	,	3.00128	,	2.87412	,	2.80271	,	2.81287	,	2.89523	,	2.90386	,	2.8998	,	3.00696	,	2.92324	,	2.81056	,	2.28772	,	2.13185	,	2.29983	,	2.44542	,	2.71801	,	2.75476
Index	,	0	,	1	,	2	,	3	,	4	,	5	,	6	,	7	,	8	,	9	,	10	,	11	,	12	,	13	,	14	,	15	,	16	,	17	,	18	,	19	,	20	,	21	,	22	,	23	,	24	,	25


AccelerationStructure: 
Ray tracing acceleration structure building statistics: 
	Bottom Level Acceleration Structure: 
		Maximum [B]: 14622208
		Minimum [B]: 14622208
		Currently Allocated [B]: 14622208
		Currently Created [num]: 1
		Current Average [B]: 14622208
		Maximum Triangles [num]: 262267
		Minimum Triangles [num]: 262267
		Total Triangles [num]: 262267
		Maximum Meshes [num]: 103
		Minimum Meshes [num]: 103
		Total Meshes [num]: 103
	Top Level Acceleration Structure: 
		Maximum [B]: 64
		Minimum [B]: 64
		Currently Allocated [B]: 64
		Total Allocated [B]: 64
		Total Created [num]: 1
		Average Allocated [B]: 64
		Maximum Instances [num]: 1
		Minimum Instances [num]: 1
		Total Instances [num]: 1
	Scratch Buffer: 
		Maximum [B]: 11657856
		Minimum [B]: 11657856
		Currently Allocated [B]: 11657856
		Total Allocated [B]: 11657856
		Total Created [num]: 1
		Average Allocated [B]: 11657856
ProfilingStack: 
[PROF] Profiling statistics: 
	Scopes entered : 186115
	Scopes exited : 186114
	Overhead per scope [ticks] : 122.8
	Threads entered : 3
	Threads exited : 0
	============================
		         Sum[Mt]   Parent[%]  Samples[num]       Avg[Mt]   Last/Curr[Mt]        Self[Mt]     Self[%] 	 Name

	main:

		   29280.559 Mt   100.000 %         1 x    29280.560 Mt    29280.559 Mt        0.358 Mt     0.001 % 	 /

		   29280.231 Mt    99.999 %         1 x    29280.233 Mt    29280.231 Mt      298.445 Mt     1.019 % 	 ./Main application loop

		       3.075 Mt     0.011 %         1 x        3.075 Mt        0.000 Mt        3.075 Mt   100.000 % 	 ../TexturedRLInitialization

		       1.392 Mt     0.005 %         1 x        1.392 Mt        0.000 Mt        1.392 Mt   100.000 % 	 ../ShadowMapRLInitialization

		       4.514 Mt     0.015 %         1 x        4.514 Mt        0.000 Mt        4.514 Mt   100.000 % 	 ../RasterResolveRLInitialization

		       4.861 Mt     0.017 %         1 x        4.861 Mt        0.000 Mt        4.861 Mt   100.000 % 	 ../AmbientOcclusionRLInitialization

		       4.884 Mt     0.017 %         1 x        4.884 Mt        0.000 Mt        4.884 Mt   100.000 % 	 ../DeferredRLInitialization

		      58.297 Mt     0.199 %         1 x       58.297 Mt        0.000 Mt        2.954 Mt     5.067 % 	 ../RayTracingRLInitialization

		      55.343 Mt    94.933 %         1 x       55.343 Mt        0.000 Mt       55.343 Mt   100.000 % 	 .../PipelineBuild

		       3.693 Mt     0.013 %         1 x        3.693 Mt        0.000 Mt        3.693 Mt   100.000 % 	 ../ResolveRLInitialization

		    2824.177 Mt     9.645 %         1 x     2824.177 Mt        0.000 Mt        0.178 Mt     0.006 % 	 ../Initialization

		    2823.998 Mt    99.994 %         1 x     2823.998 Mt        0.000 Mt        1.945 Mt     0.069 % 	 .../ScenePrep

		    2764.681 Mt    97.900 %         1 x     2764.681 Mt        0.000 Mt      287.063 Mt    10.383 % 	 ..../SceneLoad

		    1913.998 Mt    69.230 %         1 x     1913.998 Mt        0.000 Mt      572.169 Mt    29.894 % 	 ...../glTF-LoadScene

		    1341.829 Mt    70.106 %        69 x       19.447 Mt        0.000 Mt     1341.829 Mt   100.000 % 	 ....../glTF-LoadImageData

		     436.104 Mt    15.774 %         1 x      436.104 Mt        0.000 Mt      436.104 Mt   100.000 % 	 ...../glTF-CreateScene

		     127.516 Mt     4.612 %         1 x      127.516 Mt        0.000 Mt      127.516 Mt   100.000 % 	 ...../SceneDesc-CreateScene

		       0.865 Mt     0.031 %         1 x        0.865 Mt        0.000 Mt        0.001 Mt     0.104 % 	 ..../ShadowMapRLPrep

		       0.864 Mt    99.896 %         1 x        0.864 Mt        0.000 Mt        0.815 Mt    94.282 % 	 ...../PrepareForRendering

		       0.049 Mt     5.718 %         1 x        0.049 Mt        0.000 Mt        0.049 Mt   100.000 % 	 ....../PrepareDrawBundle

		       3.504 Mt     0.124 %         1 x        3.504 Mt        0.000 Mt        0.000 Mt     0.011 % 	 ..../TexturedRLPrep

		       3.504 Mt    99.989 %         1 x        3.504 Mt        0.000 Mt        3.464 Mt    98.881 % 	 ...../PrepareForRendering

		       0.001 Mt     0.040 %         1 x        0.001 Mt        0.000 Mt        0.001 Mt   100.000 % 	 ....../PrepareMaterials

		       0.038 Mt     1.079 %         1 x        0.038 Mt        0.000 Mt        0.038 Mt   100.000 % 	 ....../PrepareDrawBundle

		       2.497 Mt     0.088 %         1 x        2.497 Mt        0.000 Mt        0.000 Mt     0.016 % 	 ..../DeferredRLPrep

		       2.496 Mt    99.984 %         1 x        2.496 Mt        0.000 Mt        2.050 Mt    82.108 % 	 ...../PrepareForRendering

		       0.001 Mt     0.036 %         1 x        0.001 Mt        0.000 Mt        0.001 Mt   100.000 % 	 ....../PrepareMaterials

		       0.355 Mt    14.242 %         1 x        0.355 Mt        0.000 Mt        0.355 Mt   100.000 % 	 ....../BuildMaterialCache

		       0.090 Mt     3.614 %         1 x        0.090 Mt        0.000 Mt        0.090 Mt   100.000 % 	 ....../PrepareDrawBundle

		      24.236 Mt     0.858 %         1 x       24.236 Mt        0.000 Mt        2.589 Mt    10.683 % 	 ..../RayTracingRLPrep

		       0.085 Mt     0.351 %         1 x        0.085 Mt        0.000 Mt        0.085 Mt   100.000 % 	 ...../PrepareAccelerationStructures

		       0.001 Mt     0.004 %         1 x        0.001 Mt        0.000 Mt        0.001 Mt   100.000 % 	 ...../PrepareCache

		      12.832 Mt    52.947 %         1 x       12.832 Mt        0.000 Mt       12.832 Mt   100.000 % 	 ...../PipelineBuild

		       0.703 Mt     2.899 %         1 x        0.703 Mt        0.000 Mt        0.703 Mt   100.000 % 	 ...../BuildCache

		       5.366 Mt    22.142 %         1 x        5.366 Mt        0.000 Mt        0.000 Mt     0.007 % 	 ...../BuildAccelerationStructures

		       5.366 Mt    99.993 %         1 x        5.366 Mt        0.000 Mt        3.152 Mt    58.737 % 	 ....../AccelerationStructureBuild

		       1.441 Mt    26.853 %         1 x        1.441 Mt        0.000 Mt        1.441 Mt   100.000 % 	 ......./BuildBottomLevelAS

		       0.773 Mt    14.410 %         1 x        0.773 Mt        0.000 Mt        0.773 Mt   100.000 % 	 ......./BuildTopLevelAS

		       2.660 Mt    10.974 %         1 x        2.660 Mt        0.000 Mt        2.660 Mt   100.000 % 	 ...../BuildShaderTables

		      26.271 Mt     0.930 %         1 x       26.271 Mt        0.000 Mt       26.271 Mt   100.000 % 	 ..../GpuSidePrep

		   26076.893 Mt    89.060 %     93647 x        0.278 Mt        3.808 Mt      406.541 Mt     1.559 % 	 ../MainLoop

		     167.327 Mt     0.642 %      1564 x        0.107 Mt        0.089 Mt      167.046 Mt    99.832 % 	 .../Update

		       0.281 Mt     0.168 %         1 x        0.281 Mt        0.000 Mt        0.281 Mt   100.000 % 	 ..../GuiModelApply

		   25503.025 Mt    97.799 %      6984 x        3.652 Mt        3.715 Mt      821.254 Mt     3.220 % 	 .../Render

		    1248.419 Mt     4.895 %      6984 x        0.179 Mt        0.221 Mt       47.536 Mt     3.808 % 	 ..../RayTracing

		     532.972 Mt    42.692 %      6984 x        0.076 Mt        0.089 Mt      525.027 Mt    98.509 % 	 ...../Deferred

		       7.946 Mt     1.491 %      6984 x        0.001 Mt        0.001 Mt        7.946 Mt   100.000 % 	 ....../DeferredRLPrep

		     447.445 Mt    35.841 %      6984 x        0.064 Mt        0.087 Mt      443.128 Mt    99.035 % 	 ...../RayCasting

		       4.317 Mt     0.965 %      6984 x        0.001 Mt        0.001 Mt        4.317 Mt   100.000 % 	 ....../RayTracingRLPrep

		     220.465 Mt    17.660 %      6984 x        0.032 Mt        0.037 Mt      220.465 Mt   100.000 % 	 ...../Resolve

		   23433.352 Mt    91.885 %      6984 x        3.355 Mt        3.353 Mt    23433.352 Mt   100.000 % 	 ..../Present

	WindowThread:

		   29272.486 Mt   100.000 %         1 x    29272.486 Mt    29272.486 Mt    29272.486 Mt   100.000 % 	 /

	GpuThread:

		   20009.160 Mt   100.000 %      6984 x        2.865 Mt        2.836 Mt      115.486 Mt     0.577 % 	 /

		       1.605 Mt     0.008 %         1 x        1.605 Mt        0.000 Mt        0.006 Mt     0.375 % 	 ./ScenePrep

		       1.599 Mt    99.625 %         1 x        1.599 Mt        0.000 Mt        0.001 Mt     0.092 % 	 ../AccelerationStructureBuild

		       1.510 Mt    94.443 %         1 x        1.510 Mt        0.000 Mt        1.510 Mt   100.000 % 	 .../BuildBottomLevelASGpu

		       0.087 Mt     5.465 %         1 x        0.087 Mt        0.000 Mt        0.087 Mt   100.000 % 	 .../BuildTopLevelASGpu

		   19863.227 Mt    99.271 %      6984 x        2.844 Mt        2.833 Mt       25.499 Mt     0.128 % 	 ./RayTracingGpu

		    7557.855 Mt    38.049 %      6984 x        1.082 Mt        1.188 Mt     7557.855 Mt   100.000 % 	 ../DeferredRLGpu

		    8362.390 Mt    42.100 %      6984 x        1.197 Mt        1.081 Mt     8362.390 Mt   100.000 % 	 ../RayTracingRLGpu

		    3917.483 Mt    19.722 %      6984 x        0.561 Mt        0.563 Mt     3917.483 Mt   100.000 % 	 ../ResolveRLGpu

		      28.842 Mt     0.144 %      6984 x        0.004 Mt        0.002 Mt       28.842 Mt   100.000 % 	 ./Present


	============================


