Configuration: 
Command line parameters  = E:\programming\FIT-DIP\Masters-DXR-Main-git\build\Measurement\x64\Release\Measurement.exe --scene Suzanne/Suzanne.gltf --profile-automation --profile-camera-track Suzanne/Suzanne.trk --rt-mode --width 2560 --height 1440 --profile-output PresetSuzanne1440RtUltra --quality-preset Ultra 
Build type               = Release
Device name              = NVIDIA GeForce RTX 2080 Ti
Device memory MB         = 11049
Ray Tracing backend      = Hardware
Window width             = 2560
Window height            = 1421
Window title             = Direct3D App
Back-buffer count        = 3
Fill Triangles           = enabled
Triangles Font Clock-Wise= enabled
Fast Build AS            = disabled
Target FPS               = 0
Target UPS               = 60
Tearing                  = enabled
VSync                    = disabled
GUI rendering            = disabled
Debug layer              = disabled
GPU validation           = disabled
GPU profiling            = enabled
Render mode              = Ray Tracing
Loaded scene file        = Suzanne/Suzanne.gltf
Using testing scene      = disabled
Duplication              = 1
Duplication in cube      = disabled
Duplication offset       = 10
BLAS duplication         = disabled
Rays per pixel           = 33
Quality preset           = Ultra
Rasterization Quality    : 
  AO                     = enabled
  AO kernel size         = 16
  Shadows                = enabled
  Shadow map resolution  = 8192
  Shadow PCF kernel size = 8
  Reflections            = enabled
Ray Tracing Quality      : 
  AO                     = enabled
  AO rays                = 16
  AO kernel size         = 8
  Shadows                = enabled
  Shadow rays            = 8
  Shadow kernel size     = 8
  Reflections            = enabled

ProfilingAggregator: 
Render	,	9.7509	,	10.1914	,	10.9093	,	10.8177	,	9.6666	,	8.9788	,	8.5527	,	8.0973	,	8.4217	,	9.6335	,	9.5917	,	8.8239	,	8.6408	,	10.7828	,	8.9449	,	11.4194	,	12.9879	,	15.6885	,	20.279	,	14.3222	,	11.9565	,	9.1327	,	8.4931	,	8.3743
Update	,	0.0704	,	0.0719	,	0.0685	,	0.073	,	0.0708	,	0.069	,	0.07	,	0.0695	,	0.0713	,	0.0703	,	0.0697	,	0.0713	,	0.0704	,	0.0417	,	0.071	,	0.0636	,	0.0796	,	0.0692	,	0.0724	,	0.065	,	0.0684	,	0.0649	,	0.0689	,	0.0659
RayTracing	,	0.1732	,	0.1915	,	0.1847	,	0.1727	,	0.1693	,	0.1819	,	0.1859	,	0.2031	,	0.2001	,	0.1983	,	0.1771	,	0.1826	,	0.1728	,	0.1198	,	0.1828	,	0.1827	,	0.1616	,	0.1794	,	0.1818	,	0.1825	,	0.1763	,	0.173	,	0.2088	,	0.2052
RayTracingGpu	,	8.7609	,	9.31309	,	10.0056	,	9.90451	,	8.79891	,	8.09533	,	7.59811	,	7.2599	,	7.58704	,	8.70474	,	8.7143	,	8.03882	,	7.82406	,	7.49379	,	8.07363	,	10.2295	,	12.1297	,	14.5713	,	19.3753	,	13.3779	,	10.9426	,	8.27126	,	7.5959	,	7.45741
DeferredRLGpu	,	0.138304	,	0.187648	,	0.200224	,	0.152736	,	0.117408	,	0.070176	,	0.04736	,	0.046624	,	0.063488	,	0.093472	,	0.108288	,	0.07968	,	0.054752	,	0.057472	,	0.091808	,	0.185984	,	0.225984	,	0.26656	,	0.409568	,	0.319872	,	0.26224	,	0.132128	,	0.081792	,	0.07168
RayTracingRLGpu	,	1.64157	,	2.24765	,	2.88637	,	2.46157	,	1.45952	,	0.826752	,	0.591104	,	0.458752	,	0.746304	,	1.39981	,	1.77053	,	1.17117	,	0.631808	,	0.673312	,	1.19373	,	2.97491	,	4.7551	,	7.11952	,	11.4327	,	5.73194	,	3.5455	,	1.3231	,	0.74096	,	0.618688
ResolveRLGpu	,	6.97939	,	6.87568	,	6.91699	,	7.28861	,	7.22038	,	7.19677	,	6.95398	,	6.75277	,	6.77565	,	7.20989	,	6.83338	,	6.78627	,	7.13587	,	6.76086	,	6.78602	,	7.06688	,	7.14714	,	7.18358	,	7.53107	,	7.32432	,	7.1319	,	6.81389	,	6.77139	,	6.76403
Index	,	0	,	1	,	2	,	3	,	4	,	5	,	6	,	7	,	8	,	9	,	10	,	11	,	12	,	13	,	14	,	15	,	16	,	17	,	18	,	19	,	20	,	21	,	22	,	23

ProfilingBuildAggregator: 
BuildBottomLevelAS	,	0.93
BuildBottomLevelASGpu	,	0.2536
BuildTopLevelAS	,	0.8172
BuildTopLevelASGpu	,	0.059712
Duplication	,	1
Triangles	,	3936
Meshes	,	1
BottomLevels	,	1
TopLevelSize	,	64
BottomLevelsSize	,	222592
Index	,	0

FpsAggregator: 
FPS	,	85	,	97	,	91	,	90	,	98	,	109	,	115	,	118	,	117	,	110	,	102	,	104	,	113	,	117	,	113	,	98	,	81	,	71	,	55	,	60	,	76	,	96	,	111	,	115
UPS	,	59	,	60	,	60	,	61	,	60	,	60	,	60	,	61	,	60	,	60	,	61	,	60	,	60	,	61	,	60	,	60	,	60	,	61	,	60	,	61	,	60	,	60	,	60	,	60
FrameTime	,	11.7723	,	10.3439	,	11.0503	,	11.1871	,	10.218	,	9.18276	,	8.71749	,	8.51854	,	8.57899	,	9.14705	,	9.88109	,	9.65064	,	8.91108	,	8.62874	,	8.88366	,	10.285	,	12.3864	,	14.1353	,	18.4339	,	16.7061	,	13.1681	,	10.4249	,	9.04603	,	8.71091
GigaRays/s	,	10.1974	,	11.6055	,	10.8636	,	10.7307	,	11.7485	,	13.073	,	13.7707	,	14.0923	,	13.993	,	13.124	,	12.1491	,	12.4392	,	13.4715	,	13.9124	,	13.5131	,	11.672	,	9.69176	,	8.49263	,	6.51224	,	7.18575	,	9.1164	,	11.5153	,	13.2706	,	13.7811
Index	,	0	,	1	,	2	,	3	,	4	,	5	,	6	,	7	,	8	,	9	,	10	,	11	,	12	,	13	,	14	,	15	,	16	,	17	,	18	,	19	,	20	,	21	,	22	,	23


AccelerationStructure: 
Ray tracing acceleration structure building statistics: 
	Bottom Level Acceleration Structure: 
		Maximum [B]: 222592
		Minimum [B]: 222592
		Currently Allocated [B]: 222592
		Currently Created [num]: 1
		Current Average [B]: 222592
		Maximum Triangles [num]: 3936
		Minimum Triangles [num]: 3936
		Total Triangles [num]: 3936
		Maximum Meshes [num]: 1
		Minimum Meshes [num]: 1
		Total Meshes [num]: 1
	Top Level Acceleration Structure: 
		Maximum [B]: 64
		Minimum [B]: 64
		Currently Allocated [B]: 64
		Total Allocated [B]: 64
		Total Created [num]: 1
		Average Allocated [B]: 64
		Maximum Instances [num]: 1
		Minimum Instances [num]: 1
		Total Instances [num]: 1
	Scratch Buffer: 
		Maximum [B]: 32896
		Minimum [B]: 32896
		Currently Allocated [B]: 32896
		Total Allocated [B]: 32896
		Total Created [num]: 1
		Average Allocated [B]: 32896
ProfilingStack: 
[PROF] Profiling statistics: 
	Scopes entered : 123741
	Scopes exited : 123740
	Overhead per scope [ticks] : 100.7
	Threads entered : 3
	Threads exited : 0
	============================
		         Sum[Mt]   Parent[%]  Samples[num]       Avg[Mt]   Last/Curr[Mt]        Self[Mt]     Self[%] 	 Name

	main:

		   24586.694 Mt   100.000 %         1 x    24586.695 Mt    24586.694 Mt        0.289 Mt     0.001 % 	 /

		   24586.435 Mt    99.999 %         1 x    24586.437 Mt    24586.435 Mt      278.703 Mt     1.134 % 	 ./Main application loop

		       3.134 Mt     0.013 %         1 x        3.134 Mt        0.000 Mt        3.134 Mt   100.000 % 	 ../TexturedRLInitialization

		       1.281 Mt     0.005 %         1 x        1.281 Mt        0.000 Mt        1.281 Mt   100.000 % 	 ../ShadowMapRLInitialization

		       5.606 Mt     0.023 %         1 x        5.606 Mt        0.000 Mt        5.606 Mt   100.000 % 	 ../RasterResolveRLInitialization

		       2.874 Mt     0.012 %         1 x        2.874 Mt        0.000 Mt        2.874 Mt   100.000 % 	 ../AmbientOcclusionRLInitialization

		       4.685 Mt     0.019 %         1 x        4.685 Mt        0.000 Mt        4.685 Mt   100.000 % 	 ../DeferredRLInitialization

		      50.442 Mt     0.205 %         1 x       50.442 Mt        0.000 Mt        2.279 Mt     4.518 % 	 ../RayTracingRLInitialization

		      48.163 Mt    95.482 %         1 x       48.163 Mt        0.000 Mt       48.163 Mt   100.000 % 	 .../PipelineBuild

		       3.542 Mt     0.014 %         1 x        3.542 Mt        0.000 Mt        3.542 Mt   100.000 % 	 ../ResolveRLInitialization

		     103.202 Mt     0.420 %         1 x      103.202 Mt        0.000 Mt        0.170 Mt     0.165 % 	 ../Initialization

		     103.032 Mt    99.835 %         1 x      103.032 Mt        0.000 Mt        1.912 Mt     1.856 % 	 .../ScenePrep

		      62.401 Mt    60.565 %         1 x       62.401 Mt        0.000 Mt       15.112 Mt    24.217 % 	 ..../SceneLoad

		      39.412 Mt    63.158 %         1 x       39.412 Mt        0.000 Mt        8.383 Mt    21.271 % 	 ...../glTF-LoadScene

		      31.028 Mt    78.729 %         2 x       15.514 Mt        0.000 Mt       31.028 Mt   100.000 % 	 ....../glTF-LoadImageData

		       6.118 Mt     9.805 %         1 x        6.118 Mt        0.000 Mt        6.118 Mt   100.000 % 	 ...../glTF-CreateScene

		       1.759 Mt     2.819 %         1 x        1.759 Mt        0.000 Mt        1.759 Mt   100.000 % 	 ...../SceneDesc-CreateScene

		       0.266 Mt     0.258 %         1 x        0.266 Mt        0.000 Mt        0.001 Mt     0.338 % 	 ..../ShadowMapRLPrep

		       0.265 Mt    99.662 %         1 x        0.265 Mt        0.000 Mt        0.242 Mt    91.365 % 	 ...../PrepareForRendering

		       0.023 Mt     8.635 %         1 x        0.023 Mt        0.000 Mt        0.023 Mt   100.000 % 	 ....../PrepareDrawBundle

		       0.776 Mt     0.753 %         1 x        0.776 Mt        0.000 Mt        0.001 Mt     0.129 % 	 ..../TexturedRLPrep

		       0.775 Mt    99.871 %         1 x        0.775 Mt        0.000 Mt        0.768 Mt    99.020 % 	 ...../PrepareForRendering

		       0.003 Mt     0.361 %         1 x        0.003 Mt        0.000 Mt        0.003 Mt   100.000 % 	 ....../PrepareMaterials

		       0.005 Mt     0.619 %         1 x        0.005 Mt        0.000 Mt        0.005 Mt   100.000 % 	 ....../PrepareDrawBundle

		       1.476 Mt     1.433 %         1 x        1.476 Mt        0.000 Mt        0.000 Mt     0.027 % 	 ..../DeferredRLPrep

		       1.476 Mt    99.973 %         1 x        1.476 Mt        0.000 Mt        1.161 Mt    78.656 % 	 ...../PrepareForRendering

		       0.001 Mt     0.081 %         1 x        0.001 Mt        0.000 Mt        0.001 Mt   100.000 % 	 ....../PrepareMaterials

		       0.310 Mt    21.026 %         1 x        0.310 Mt        0.000 Mt        0.310 Mt   100.000 % 	 ....../BuildMaterialCache

		       0.004 Mt     0.237 %         1 x        0.004 Mt        0.000 Mt        0.004 Mt   100.000 % 	 ....../PrepareDrawBundle

		      24.757 Mt    24.029 %         1 x       24.757 Mt        0.000 Mt        2.540 Mt    10.260 % 	 ..../RayTracingRLPrep

		       0.046 Mt     0.185 %         1 x        0.046 Mt        0.000 Mt        0.046 Mt   100.000 % 	 ...../PrepareAccelerationStructures

		       0.001 Mt     0.003 %         1 x        0.001 Mt        0.000 Mt        0.001 Mt   100.000 % 	 ...../PrepareCache

		      12.683 Mt    51.228 %         1 x       12.683 Mt        0.000 Mt       12.683 Mt   100.000 % 	 ...../PipelineBuild

		       0.350 Mt     1.414 %         1 x        0.350 Mt        0.000 Mt        0.350 Mt   100.000 % 	 ...../BuildCache

		       7.771 Mt    31.387 %         1 x        7.771 Mt        0.000 Mt        0.001 Mt     0.006 % 	 ...../BuildAccelerationStructures

		       7.770 Mt    99.994 %         1 x        7.770 Mt        0.000 Mt        6.023 Mt    77.514 % 	 ....../AccelerationStructureBuild

		       0.930 Mt    11.969 %         1 x        0.930 Mt        0.000 Mt        0.930 Mt   100.000 % 	 ......./BuildBottomLevelAS

		       0.817 Mt    10.517 %         1 x        0.817 Mt        0.000 Mt        0.817 Mt   100.000 % 	 ......./BuildTopLevelAS

		       1.367 Mt     5.523 %         1 x        1.367 Mt        0.000 Mt        1.367 Mt   100.000 % 	 ...../BuildShaderTables

		      11.443 Mt    11.106 %         1 x       11.443 Mt        0.000 Mt       11.443 Mt   100.000 % 	 ..../GpuSidePrep

		   24132.965 Mt    98.156 %     91791 x        0.263 Mt       29.380 Mt      389.483 Mt     1.614 % 	 ../MainLoop

		     189.335 Mt     0.785 %      1446 x        0.131 Mt        0.099 Mt      188.988 Mt    99.817 % 	 .../Update

		       0.347 Mt     0.183 %         1 x        0.347 Mt        0.000 Mt        0.347 Mt   100.000 % 	 ..../GuiModelApply

		   23554.147 Mt    97.602 %      2343 x       10.053 Mt        9.130 Mt      274.645 Mt     1.166 % 	 .../Render

		     431.678 Mt     1.833 %      2343 x        0.184 Mt        0.259 Mt       16.871 Mt     3.908 % 	 ..../RayTracing

		     195.589 Mt    45.309 %      2343 x        0.083 Mt        0.118 Mt      193.488 Mt    98.926 % 	 ...../Deferred

		       2.101 Mt     1.074 %      2343 x        0.001 Mt        0.001 Mt        2.101 Mt   100.000 % 	 ....../DeferredRLPrep

		     152.236 Mt    35.266 %      2343 x        0.065 Mt        0.095 Mt      150.843 Mt    99.085 % 	 ...../RayCasting

		       1.394 Mt     0.915 %      2343 x        0.001 Mt        0.001 Mt        1.394 Mt   100.000 % 	 ....../RayTracingRLPrep

		      66.982 Mt    15.517 %      2343 x        0.029 Mt        0.035 Mt       66.982 Mt   100.000 % 	 ...../Resolve

		   22847.824 Mt    97.001 %      2343 x        9.752 Mt        8.683 Mt    22847.824 Mt   100.000 % 	 ..../Present

	WindowThread:

		   24585.996 Mt   100.000 %         1 x    24585.996 Mt    24585.996 Mt    24585.996 Mt   100.000 % 	 /

	GpuThread:

		   21560.218 Mt   100.000 %      2343 x        9.202 Mt        7.684 Mt      132.861 Mt     0.616 % 	 /

		       0.320 Mt     0.001 %         1 x        0.320 Mt        0.000 Mt        0.006 Mt     1.752 % 	 ./ScenePrep

		       0.314 Mt    98.248 %         1 x        0.314 Mt        0.000 Mt        0.001 Mt     0.255 % 	 ../AccelerationStructureBuild

		       0.254 Mt    80.736 %         1 x        0.254 Mt        0.000 Mt        0.254 Mt   100.000 % 	 .../BuildBottomLevelASGpu

		       0.060 Mt    19.010 %         1 x        0.060 Mt        0.000 Mt        0.060 Mt   100.000 % 	 .../BuildTopLevelASGpu

		   21319.205 Mt    98.882 %      2343 x        9.099 Mt        7.590 Mt        5.078 Mt     0.024 % 	 ./RayTracingGpu

		     301.202 Mt     1.413 %      2343 x        0.129 Mt        0.073 Mt      301.202 Mt   100.000 % 	 ../DeferredRLGpu

		    4629.339 Mt    21.714 %      2343 x        1.976 Mt        0.665 Mt     4629.339 Mt   100.000 % 	 ../RayTracingRLGpu

		   16383.586 Mt    76.849 %      2343 x        6.993 Mt        6.851 Mt    16383.586 Mt   100.000 % 	 ../ResolveRLGpu

		     107.832 Mt     0.500 %      2343 x        0.046 Mt        0.094 Mt      107.832 Mt   100.000 % 	 ./Present


	============================


