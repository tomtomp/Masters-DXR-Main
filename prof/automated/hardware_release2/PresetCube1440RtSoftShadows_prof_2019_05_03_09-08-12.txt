Configuration: 
Command line parameters  = E:\programming\FIT-DIP\Masters-DXR-Main-git\build\Measurement\x64\Release\Measurement.exe --scene Cube/Cube.gltf --profile-automation --profile-camera-track Cube/Cube.trk --rt-mode --width 2560 --height 1440 --profile-output PresetCube1440RtSoftShadows --quality-preset SoftShadows 
Build type               = Release
Device name              = NVIDIA GeForce RTX 2080 Ti
Device memory MB         = 11049
Ray Tracing backend      = Hardware
Window width             = 2560
Window height            = 1421
Window title             = Direct3D App
Back-buffer count        = 3
Fill Triangles           = enabled
Triangles Font Clock-Wise= enabled
Fast Build AS            = disabled
Target FPS               = 0
Target UPS               = 60
Tearing                  = enabled
VSync                    = disabled
GUI rendering            = disabled
Debug layer              = disabled
GPU validation           = disabled
GPU profiling            = enabled
Render mode              = Ray Tracing
Loaded scene file        = Cube/Cube.gltf
Using testing scene      = disabled
Duplication              = 1
Duplication in cube      = disabled
Duplication offset       = 10
BLAS duplication         = disabled
Rays per pixel           = 4
Quality preset           = SoftShadows
Rasterization Quality    : 
  AO                     = disabled
  AO kernel size         = 1
  Shadows                = enabled
  Shadow map resolution  = 4096
  Shadow PCF kernel size = 4
  Reflections            = disabled
Ray Tracing Quality      : 
  AO                     = disabled
  AO rays                = 0
  AO kernel size         = 4
  Shadows                = enabled
  Shadow rays            = 4
  Shadow kernel size     = 4
  Reflections            = disabled

ProfilingAggregator: 
Render	,	2.8089	,	3.0543	,	3.0743	,	3.1935	,	3.1213	,	3.4184	,	3.8078	,	3.9777	,	4.2886	,	3.9772	,	3.4614	,	3.5598	,	3.227	,	3.0869	,	3.2371	,	3.4556	,	3.6741	,	3.9367	,	4.0652	,	4.0437	,	3.7162	,	3.193	,	3.3879	,	3.0953	,	2.6234	,	2.6188	,	2.5037	,	2.4126	,	2.3635	,	2.2975
Update	,	0.0721	,	0.0729	,	0.0827	,	0.0721	,	0.0712	,	0.0719	,	0.0702	,	0.0746	,	0.0731	,	0.0726	,	0.0909	,	0.0893	,	0.072	,	0.0683	,	0.0692	,	0.0699	,	0.0709	,	0.0717	,	0.0764	,	0.0725	,	0.0715	,	0.0722	,	0.0704	,	0.0721	,	0.0637	,	0.0742	,	0.0721	,	0.0713	,	0.0757	,	0.0748
RayTracing	,	0.1711	,	0.1706	,	0.1894	,	0.171	,	0.1716	,	0.171	,	0.1729	,	0.2	,	0.1872	,	0.1723	,	0.1913	,	0.1893	,	0.174	,	0.1709	,	0.172	,	0.1698	,	0.1714	,	0.1709	,	0.1696	,	0.1682	,	0.1663	,	0.168	,	0.1712	,	0.1742	,	0.1455	,	0.1736	,	0.2088	,	0.2066	,	0.2019	,	0.1899
RayTracingGpu	,	2.0167	,	2.27792	,	2.33949	,	2.35306	,	2.41248	,	2.64621	,	2.90659	,	3.09341	,	3.40397	,	3.12061	,	2.63405	,	2.7201	,	2.42378	,	2.30989	,	2.4624	,	2.66906	,	2.90339	,	3.16365	,	3.25571	,	3.23635	,	2.95523	,	2.43037	,	2.57699	,	2.14406	,	1.88624	,	1.77382	,	1.65158	,	1.59674	,	1.54643	,	1.55114
DeferredRLGpu	,	0.180768	,	0.24352	,	0.255072	,	0.261344	,	0.273344	,	0.33488	,	0.309824	,	0.352032	,	0.411104	,	0.370144	,	0.31904	,	0.348672	,	0.278496	,	0.251744	,	0.285216	,	0.333344	,	0.392128	,	0.463328	,	0.485824	,	0.485376	,	0.408448	,	0.285152	,	0.233248	,	0.182368	,	0.151424	,	0.118432	,	0.090592	,	0.073952	,	0.063936	,	0.06368
RayTracingRLGpu	,	0.376736	,	0.491104	,	0.51456	,	0.523712	,	0.549856	,	0.671488	,	0.617952	,	0.713184	,	1.0345	,	0.950048	,	0.663712	,	0.72688	,	0.580192	,	0.523328	,	0.595968	,	0.697728	,	0.820928	,	0.967584	,	1.01184	,	1.01104	,	0.868032	,	0.605568	,	0.492352	,	0.388896	,	0.327744	,	0.266144	,	0.215712	,	0.181536	,	0.165088	,	0.16512
ResolveRLGpu	,	1.45594	,	1.54112	,	1.56662	,	1.56477	,	1.5857	,	1.63754	,	1.97686	,	2.02618	,	1.9561	,	1.79888	,	1.64826	,	1.64259	,	1.56314	,	1.53251	,	1.57805	,	1.63597	,	1.68819	,	1.73066	,	1.75603	,	1.73747	,	1.67581	,	1.53658	,	1.84944	,	1.57123	,	1.40541	,	1.38637	,	1.34355	,	1.33962	,	1.31565	,	1.32038
Index	,	0	,	1	,	2	,	3	,	4	,	5	,	6	,	7	,	8	,	9	,	10	,	11	,	12	,	13	,	14	,	15	,	16	,	17	,	18	,	19	,	20	,	21	,	22	,	23	,	24	,	25	,	26	,	27	,	28	,	29

ProfilingBuildAggregator: 
BuildBottomLevelAS	,	0.9397
BuildBottomLevelASGpu	,	0.018112
BuildTopLevelAS	,	0.9641
BuildTopLevelASGpu	,	0.088256
Duplication	,	1
Triangles	,	12
Meshes	,	1
BottomLevels	,	1
TopLevelSize	,	64
BottomLevelsSize	,	1152
Index	,	0

FpsAggregator: 
FPS	,	259	,	305	,	301	,	304	,	293	,	278	,	276	,	280	,	254	,	249	,	285	,	261	,	276	,	305	,	293	,	280	,	261	,	243	,	231	,	228	,	233	,	277	,	300	,	322	,	363	,	364	,	378	,	391	,	400	,	400
UPS	,	59	,	60	,	61	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	61	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	61	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60
FrameTime	,	3.86387	,	3.28381	,	3.33242	,	3.29963	,	3.41788	,	3.60293	,	3.63435	,	3.57333	,	3.94729	,	4.0189	,	3.51802	,	3.8408	,	3.62697	,	3.28539	,	3.41978	,	3.57684	,	3.83279	,	4.1313	,	4.34643	,	4.38726	,	4.29556	,	3.61082	,	3.34175	,	3.11497	,	2.75599	,	2.74988	,	2.65006	,	2.55889	,	2.50252	,	2.50208
GigaRays/s	,	3.76593	,	4.43114	,	4.36651	,	4.4099	,	4.25733	,	4.03867	,	4.00376	,	4.07212	,	3.68634	,	3.62065	,	4.13614	,	3.78855	,	4.0119	,	4.42901	,	4.25496	,	4.06813	,	3.79646	,	3.52214	,	3.34781	,	3.31666	,	3.38746	,	4.02985	,	4.35432	,	4.67133	,	5.27978	,	5.29152	,	5.49083	,	5.68646	,	5.81456	,	5.81558
Index	,	0	,	1	,	2	,	3	,	4	,	5	,	6	,	7	,	8	,	9	,	10	,	11	,	12	,	13	,	14	,	15	,	16	,	17	,	18	,	19	,	20	,	21	,	22	,	23	,	24	,	25	,	26	,	27	,	28	,	29


AccelerationStructure: 
Ray tracing acceleration structure building statistics: 
	Bottom Level Acceleration Structure: 
		Maximum [B]: 1152
		Minimum [B]: 1152
		Currently Allocated [B]: 1152
		Currently Created [num]: 1
		Current Average [B]: 1152
		Maximum Triangles [num]: 12
		Minimum Triangles [num]: 12
		Total Triangles [num]: 12
		Maximum Meshes [num]: 1
		Minimum Meshes [num]: 1
		Total Meshes [num]: 1
	Top Level Acceleration Structure: 
		Maximum [B]: 64
		Minimum [B]: 64
		Currently Allocated [B]: 64
		Total Allocated [B]: 64
		Total Created [num]: 1
		Average Allocated [B]: 64
		Maximum Instances [num]: 1
		Minimum Instances [num]: 1
		Total Instances [num]: 1
	Scratch Buffer: 
		Maximum [B]: 896
		Minimum [B]: 896
		Currently Allocated [B]: 896
		Total Allocated [B]: 896
		Total Created [num]: 1
		Average Allocated [B]: 896
ProfilingStack: 
[PROF] Profiling statistics: 
	Scopes entered : 146488
	Scopes exited : 146487
	Overhead per scope [ticks] : 113.7
	Threads entered : 3
	Threads exited : 0
	============================
		         Sum[Mt]   Parent[%]  Samples[num]       Avg[Mt]   Last/Curr[Mt]        Self[Mt]     Self[%] 	 Name

	main:

		   30589.298 Mt   100.000 %         1 x    30589.298 Mt    30589.297 Mt        0.440 Mt     0.001 % 	 /

		   30588.879 Mt    99.999 %         1 x    30588.880 Mt    30588.878 Mt      273.008 Mt     0.893 % 	 ./Main application loop

		       2.775 Mt     0.009 %         1 x        2.775 Mt        0.000 Mt        2.775 Mt   100.000 % 	 ../TexturedRLInitialization

		       1.165 Mt     0.004 %         1 x        1.165 Mt        0.000 Mt        1.165 Mt   100.000 % 	 ../ShadowMapRLInitialization

		       4.327 Mt     0.014 %         1 x        4.327 Mt        0.000 Mt        4.327 Mt   100.000 % 	 ../RasterResolveRLInitialization

		       4.148 Mt     0.014 %         1 x        4.148 Mt        0.000 Mt        4.148 Mt   100.000 % 	 ../AmbientOcclusionRLInitialization

		       4.164 Mt     0.014 %         1 x        4.164 Mt        0.000 Mt        4.164 Mt   100.000 % 	 ../DeferredRLInitialization

		      75.269 Mt     0.246 %         1 x       75.269 Mt        0.000 Mt        2.220 Mt     2.949 % 	 ../RayTracingRLInitialization

		      73.050 Mt    97.051 %         1 x       73.050 Mt        0.000 Mt       73.050 Mt   100.000 % 	 .../PipelineBuild

		       3.632 Mt     0.012 %         1 x        3.632 Mt        0.000 Mt        3.632 Mt   100.000 % 	 ../ResolveRLInitialization

		     125.075 Mt     0.409 %         1 x      125.075 Mt        0.000 Mt        0.612 Mt     0.489 % 	 ../Initialization

		     124.463 Mt    99.511 %         1 x      124.463 Mt        0.000 Mt        1.773 Mt     1.425 % 	 .../ScenePrep

		      49.133 Mt    39.476 %         1 x       49.133 Mt        0.000 Mt       11.698 Mt    23.809 % 	 ..../SceneLoad

		      11.913 Mt    24.246 %         1 x       11.913 Mt        0.000 Mt        3.723 Mt    31.252 % 	 ...../glTF-LoadScene

		       8.190 Mt    68.748 %         2 x        4.095 Mt        0.000 Mt        8.190 Mt   100.000 % 	 ....../glTF-LoadImageData

		       9.279 Mt    18.886 %         1 x        9.279 Mt        0.000 Mt        9.279 Mt   100.000 % 	 ...../glTF-CreateScene

		      16.243 Mt    33.059 %         1 x       16.243 Mt        0.000 Mt       16.243 Mt   100.000 % 	 ...../SceneDesc-CreateScene

		       0.803 Mt     0.645 %         1 x        0.803 Mt        0.000 Mt        0.000 Mt     0.050 % 	 ..../ShadowMapRLPrep

		       0.802 Mt    99.950 %         1 x        0.802 Mt        0.000 Mt        0.793 Mt    98.791 % 	 ...../PrepareForRendering

		       0.010 Mt     1.209 %         1 x        0.010 Mt        0.000 Mt        0.010 Mt   100.000 % 	 ....../PrepareDrawBundle

		       2.445 Mt     1.965 %         1 x        2.445 Mt        0.000 Mt        0.001 Mt     0.020 % 	 ..../TexturedRLPrep

		       2.445 Mt    99.980 %         1 x        2.445 Mt        0.000 Mt        2.440 Mt    99.804 % 	 ...../PrepareForRendering

		       0.002 Mt     0.061 %         1 x        0.002 Mt        0.000 Mt        0.002 Mt   100.000 % 	 ....../PrepareMaterials

		       0.003 Mt     0.135 %         1 x        0.003 Mt        0.000 Mt        0.003 Mt   100.000 % 	 ....../PrepareDrawBundle

		       4.847 Mt     3.894 %         1 x        4.847 Mt        0.000 Mt        0.000 Mt     0.010 % 	 ..../DeferredRLPrep

		       4.846 Mt    99.990 %         1 x        4.846 Mt        0.000 Mt        3.991 Mt    82.365 % 	 ...../PrepareForRendering

		       0.001 Mt     0.019 %         1 x        0.001 Mt        0.000 Mt        0.001 Mt   100.000 % 	 ....../PrepareMaterials

		       0.849 Mt    17.517 %         1 x        0.849 Mt        0.000 Mt        0.849 Mt   100.000 % 	 ....../BuildMaterialCache

		       0.005 Mt     0.099 %         1 x        0.005 Mt        0.000 Mt        0.005 Mt   100.000 % 	 ....../PrepareDrawBundle

		      52.521 Mt    42.198 %         1 x       52.521 Mt        0.000 Mt        3.930 Mt     7.483 % 	 ..../RayTracingRLPrep

		       0.059 Mt     0.113 %         1 x        0.059 Mt        0.000 Mt        0.059 Mt   100.000 % 	 ...../PrepareAccelerationStructures

		       0.001 Mt     0.003 %         1 x        0.001 Mt        0.000 Mt        0.001 Mt   100.000 % 	 ...../PrepareCache

		      35.426 Mt    67.451 %         1 x       35.426 Mt        0.000 Mt       35.426 Mt   100.000 % 	 ...../PipelineBuild

		       0.677 Mt     1.289 %         1 x        0.677 Mt        0.000 Mt        0.677 Mt   100.000 % 	 ...../BuildCache

		       6.056 Mt    11.531 %         1 x        6.056 Mt        0.000 Mt        0.001 Mt     0.013 % 	 ...../BuildAccelerationStructures

		       6.055 Mt    99.987 %         1 x        6.055 Mt        0.000 Mt        4.151 Mt    68.559 % 	 ....../AccelerationStructureBuild

		       0.940 Mt    15.519 %         1 x        0.940 Mt        0.000 Mt        0.940 Mt   100.000 % 	 ......./BuildBottomLevelAS

		       0.964 Mt    15.922 %         1 x        0.964 Mt        0.000 Mt        0.964 Mt   100.000 % 	 ......./BuildTopLevelAS

		       6.371 Mt    12.130 %         1 x        6.371 Mt        0.000 Mt        6.371 Mt   100.000 % 	 ...../BuildShaderTables

		      12.941 Mt    10.397 %         1 x       12.941 Mt        0.000 Mt       12.941 Mt   100.000 % 	 ..../GpuSidePrep

		   30095.315 Mt    98.386 %     29057 x        1.036 Mt       55.037 Mt     1431.698 Mt     4.757 % 	 ../MainLoop

		     224.563 Mt     0.746 %      1803 x        0.125 Mt        0.092 Mt      223.941 Mt    99.723 % 	 .../Update

		       0.622 Mt     0.277 %         1 x        0.622 Mt        0.000 Mt        0.622 Mt   100.000 % 	 ..../GuiModelApply

		   28439.054 Mt    94.497 %      8891 x        3.199 Mt        2.587 Mt      998.391 Mt     3.511 % 	 .../Render

		    1572.989 Mt     5.531 %      8891 x        0.177 Mt        0.297 Mt       57.226 Mt     3.638 % 	 ..../RayTracing

		     722.278 Mt    45.918 %      8891 x        0.081 Mt        0.150 Mt      712.804 Mt    98.688 % 	 ...../Deferred

		       9.473 Mt     1.312 %      8891 x        0.001 Mt        0.001 Mt        9.473 Mt   100.000 % 	 ....../DeferredRLPrep

		     558.639 Mt    35.514 %      8891 x        0.063 Mt        0.101 Mt      553.459 Mt    99.073 % 	 ...../RayCasting

		       5.179 Mt     0.927 %      8891 x        0.001 Mt        0.001 Mt        5.179 Mt   100.000 % 	 ....../RayTracingRLPrep

		     234.847 Mt    14.930 %      8891 x        0.026 Mt        0.035 Mt      234.847 Mt   100.000 % 	 ...../Resolve

		   25867.675 Mt    90.958 %      8891 x        2.909 Mt        2.100 Mt    25867.675 Mt   100.000 % 	 ..../Present

	WindowThread:

		   30587.856 Mt   100.000 %         1 x    30587.856 Mt    30587.856 Mt    30587.856 Mt   100.000 % 	 /

	GpuThread:

		   21541.910 Mt   100.000 %      8891 x        2.423 Mt        1.539 Mt      147.212 Mt     0.683 % 	 /

		       0.114 Mt     0.001 %         1 x        0.114 Mt        0.000 Mt        0.006 Mt     5.576 % 	 ./ScenePrep

		       0.107 Mt    94.424 %         1 x        0.107 Mt        0.000 Mt        0.001 Mt     0.865 % 	 ../AccelerationStructureBuild

		       0.018 Mt    16.880 %         1 x        0.018 Mt        0.000 Mt        0.018 Mt   100.000 % 	 .../BuildBottomLevelASGpu

		       0.088 Mt    82.255 %         1 x        0.088 Mt        0.000 Mt        0.088 Mt   100.000 % 	 .../BuildTopLevelASGpu

		   21284.652 Mt    98.806 %      8891 x        2.394 Mt        1.536 Mt       26.160 Mt     0.123 % 	 ./RayTracingGpu

		    2309.029 Mt    10.848 %      8891 x        0.260 Mt        0.064 Mt     2309.029 Mt   100.000 % 	 ../DeferredRLGpu

		    4921.238 Mt    23.121 %      8891 x        0.554 Mt        0.163 Mt     4921.238 Mt   100.000 % 	 ../RayTracingRLGpu

		   14028.225 Mt    65.908 %      8891 x        1.578 Mt        1.308 Mt    14028.225 Mt   100.000 % 	 ../ResolveRLGpu

		     109.932 Mt     0.510 %      8891 x        0.012 Mt        0.003 Mt      109.932 Mt   100.000 % 	 ./Present


	============================


