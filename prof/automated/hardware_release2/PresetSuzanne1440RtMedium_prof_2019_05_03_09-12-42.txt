Configuration: 
Command line parameters  = E:\programming\FIT-DIP\Masters-DXR-Main-git\build\Measurement\x64\Release\Measurement.exe --scene Suzanne/Suzanne.gltf --profile-automation --profile-camera-track Suzanne/Suzanne.trk --rt-mode --width 2560 --height 1440 --profile-output PresetSuzanne1440RtMedium --quality-preset Medium 
Build type               = Release
Device name              = NVIDIA GeForce RTX 2080 Ti
Device memory MB         = 11049
Ray Tracing backend      = Hardware
Window width             = 2560
Window height            = 1421
Window title             = Direct3D App
Back-buffer count        = 3
Fill Triangles           = enabled
Triangles Font Clock-Wise= enabled
Fast Build AS            = disabled
Target FPS               = 0
Target UPS               = 60
Tearing                  = enabled
VSync                    = disabled
GUI rendering            = disabled
Debug layer              = disabled
GPU validation           = disabled
GPU profiling            = enabled
Render mode              = Ray Tracing
Loaded scene file        = Suzanne/Suzanne.gltf
Using testing scene      = disabled
Duplication              = 1
Duplication in cube      = disabled
Duplication offset       = 10
BLAS duplication         = disabled
Rays per pixel           = 8
Quality preset           = Medium
Rasterization Quality    : 
  AO                     = enabled
  AO kernel size         = 4
  Shadows                = enabled
  Shadow map resolution  = 4096
  Shadow PCF kernel size = 4
  Reflections            = disabled
Ray Tracing Quality      : 
  AO                     = enabled
  AO rays                = 4
  AO kernel size         = 4
  Shadows                = enabled
  Shadow rays            = 4
  Shadow kernel size     = 4
  Reflections            = disabled

ProfilingAggregator: 
Render	,	3.587	,	3.7933	,	4.097	,	4.3059	,	3.8283	,	3.72	,	3.3982	,	3.5584	,	3.1999	,	3.5434	,	3.5896	,	3.3068	,	2.7918	,	3.121	,	3.3897	,	4.1772	,	4.6959	,	6.2594	,	8.034	,	5.4226	,	4.4049	,	3.4961	,	3.1982	,	3.1155
Update	,	0.0707	,	0.0687	,	0.072	,	0.0633	,	0.0688	,	0.0691	,	0.0689	,	0.0686	,	0.069	,	0.0721	,	0.0708	,	0.0704	,	0.0558	,	0.0705	,	0.071	,	0.0706	,	0.069	,	0.0679	,	0.0688	,	0.0706	,	0.0713	,	0.07	,	0.0689	,	0.0676
RayTracing	,	0.1697	,	0.167	,	0.1711	,	0.1691	,	0.174	,	0.1718	,	0.1772	,	0.171	,	0.1759	,	0.1708	,	0.1693	,	0.1816	,	0.1358	,	0.1744	,	0.1712	,	0.1714	,	0.1707	,	0.1675	,	0.1706	,	0.1726	,	0.1713	,	0.1713	,	0.1967	,	0.199
RayTracingGpu	,	2.74611	,	3.10806	,	3.35523	,	3.38454	,	2.96362	,	2.74218	,	2.42093	,	2.57254	,	2.38784	,	2.67408	,	2.82234	,	2.5737	,	2.33302	,	2.33798	,	2.57738	,	3.35981	,	3.95722	,	5.26253	,	7.08643	,	4.61242	,	3.64899	,	2.7287	,	2.42378	,	2.36029
DeferredRLGpu	,	0.139488	,	0.183744	,	0.198688	,	0.15376	,	0.100928	,	0.070432	,	0.053664	,	0.04608	,	0.0632	,	0.095776	,	0.104736	,	0.079424	,	0.054752	,	0.05536	,	0.087392	,	0.185088	,	0.230272	,	0.260192	,	0.41216	,	0.326592	,	0.266176	,	0.1352	,	0.081632	,	0.071008
RayTracingRLGpu	,	0.530336	,	0.807968	,	0.998688	,	0.841056	,	0.520352	,	0.316352	,	0.240128	,	0.20432	,	0.293824	,	0.50368	,	0.62368	,	0.43808	,	0.25232	,	0.253248	,	0.425376	,	1.00602	,	1.50784	,	2.59034	,	4.16656	,	2.00029	,	1.19434	,	0.51008	,	0.298848	,	0.26
ResolveRLGpu	,	2.07414	,	2.11334	,	2.15466	,	2.38672	,	2.34109	,	2.35405	,	2.12598	,	2.3209	,	2.02886	,	2.07155	,	2.09216	,	2.05338	,	2.02442	,	2.02733	,	2.06301	,	2.16653	,	2.21706	,	2.41005	,	2.50614	,	2.28358	,	2.18653	,	2.08048	,	2.04157	,	2.02768
Index	,	0	,	1	,	2	,	3	,	4	,	5	,	6	,	7	,	8	,	9	,	10	,	11	,	12	,	13	,	14	,	15	,	16	,	17	,	18	,	19	,	20	,	21	,	22	,	23

ProfilingBuildAggregator: 
BuildBottomLevelAS	,	0.9365
BuildBottomLevelASGpu	,	0.25344
BuildTopLevelAS	,	0.8379
BuildTopLevelASGpu	,	0.062848
Duplication	,	1
Triangles	,	3936
Meshes	,	1
BottomLevels	,	1
TopLevelSize	,	64
BottomLevelsSize	,	222592
Index	,	0

FpsAggregator: 
FPS	,	227	,	254	,	236	,	235	,	261	,	287	,	305	,	312	,	310	,	291	,	268	,	274	,	309	,	321	,	307	,	258	,	215	,	189	,	147	,	154	,	193	,	246	,	289	,	304
UPS	,	59	,	61	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	61	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	61	,	60	,	60	,	60	,	60	,	61	,	60
FrameTime	,	4.41961	,	3.94847	,	4.25308	,	4.263	,	3.83369	,	3.49008	,	3.2853	,	3.2052	,	3.23533	,	3.44426	,	3.73984	,	3.65298	,	3.24451	,	3.11728	,	3.2675	,	3.88221	,	4.66113	,	5.32089	,	6.83279	,	6.52371	,	5.19235	,	4.07037	,	3.46831	,	3.29457
GigaRays/s	,	6.58477	,	7.37047	,	6.84259	,	6.82666	,	7.59114	,	8.33851	,	8.85827	,	9.07965	,	8.99509	,	8.44945	,	7.78164	,	7.96666	,	8.96964	,	9.33572	,	8.90652	,	7.49627	,	6.24357	,	5.4694	,	4.25918	,	4.46097	,	5.6048	,	7.14974	,	8.39086	,	8.83334
Index	,	0	,	1	,	2	,	3	,	4	,	5	,	6	,	7	,	8	,	9	,	10	,	11	,	12	,	13	,	14	,	15	,	16	,	17	,	18	,	19	,	20	,	21	,	22	,	23


AccelerationStructure: 
Ray tracing acceleration structure building statistics: 
	Bottom Level Acceleration Structure: 
		Maximum [B]: 222592
		Minimum [B]: 222592
		Currently Allocated [B]: 222592
		Currently Created [num]: 1
		Current Average [B]: 222592
		Maximum Triangles [num]: 3936
		Minimum Triangles [num]: 3936
		Total Triangles [num]: 3936
		Maximum Meshes [num]: 1
		Minimum Meshes [num]: 1
		Total Meshes [num]: 1
	Top Level Acceleration Structure: 
		Maximum [B]: 64
		Minimum [B]: 64
		Currently Allocated [B]: 64
		Total Allocated [B]: 64
		Total Created [num]: 1
		Average Allocated [B]: 64
		Maximum Instances [num]: 1
		Minimum Instances [num]: 1
		Total Instances [num]: 1
	Scratch Buffer: 
		Maximum [B]: 32896
		Minimum [B]: 32896
		Currently Allocated [B]: 32896
		Total Allocated [B]: 32896
		Total Created [num]: 1
		Average Allocated [B]: 32896
ProfilingStack: 
[PROF] Profiling statistics: 
	Scopes entered : 176964
	Scopes exited : 176963
	Overhead per scope [ticks] : 101.7
	Threads entered : 3
	Threads exited : 0
	============================
		         Sum[Mt]   Parent[%]  Samples[num]       Avg[Mt]   Last/Curr[Mt]        Self[Mt]     Self[%] 	 Name

	main:

		   24522.095 Mt   100.000 %         1 x    24522.096 Mt    24522.095 Mt        0.418 Mt     0.002 % 	 /

		   24521.704 Mt    99.998 %         1 x    24521.705 Mt    24521.704 Mt      273.021 Mt     1.113 % 	 ./Main application loop

		       2.905 Mt     0.012 %         1 x        2.905 Mt        0.000 Mt        2.905 Mt   100.000 % 	 ../TexturedRLInitialization

		       1.171 Mt     0.005 %         1 x        1.171 Mt        0.000 Mt        1.171 Mt   100.000 % 	 ../ShadowMapRLInitialization

		       4.592 Mt     0.019 %         1 x        4.592 Mt        0.000 Mt        4.592 Mt   100.000 % 	 ../RasterResolveRLInitialization

		       2.667 Mt     0.011 %         1 x        2.667 Mt        0.000 Mt        2.667 Mt   100.000 % 	 ../AmbientOcclusionRLInitialization

		       4.520 Mt     0.018 %         1 x        4.520 Mt        0.000 Mt        4.520 Mt   100.000 % 	 ../DeferredRLInitialization

		      51.204 Mt     0.209 %         1 x       51.204 Mt        0.000 Mt        2.462 Mt     4.807 % 	 ../RayTracingRLInitialization

		      48.743 Mt    95.193 %         1 x       48.743 Mt        0.000 Mt       48.743 Mt   100.000 % 	 .../PipelineBuild

		       3.695 Mt     0.015 %         1 x        3.695 Mt        0.000 Mt        3.695 Mt   100.000 % 	 ../ResolveRLInitialization

		     100.671 Mt     0.411 %         1 x      100.671 Mt        0.000 Mt        0.166 Mt     0.164 % 	 ../Initialization

		     100.505 Mt    99.836 %         1 x      100.505 Mt        0.000 Mt        1.048 Mt     1.043 % 	 .../ScenePrep

		      60.611 Mt    60.307 %         1 x       60.611 Mt        0.000 Mt       12.966 Mt    21.392 % 	 ..../SceneLoad

		      39.581 Mt    65.304 %         1 x       39.581 Mt        0.000 Mt        8.667 Mt    21.897 % 	 ...../glTF-LoadScene

		      30.914 Mt    78.103 %         2 x       15.457 Mt        0.000 Mt       30.914 Mt   100.000 % 	 ....../glTF-LoadImageData

		       6.311 Mt    10.412 %         1 x        6.311 Mt        0.000 Mt        6.311 Mt   100.000 % 	 ...../glTF-CreateScene

		       1.753 Mt     2.892 %         1 x        1.753 Mt        0.000 Mt        1.753 Mt   100.000 % 	 ...../SceneDesc-CreateScene

		       0.992 Mt     0.987 %         1 x        0.992 Mt        0.000 Mt        0.000 Mt     0.040 % 	 ..../ShadowMapRLPrep

		       0.992 Mt    99.960 %         1 x        0.992 Mt        0.000 Mt        0.983 Mt    99.103 % 	 ...../PrepareForRendering

		       0.009 Mt     0.897 %         1 x        0.009 Mt        0.000 Mt        0.009 Mt   100.000 % 	 ....../PrepareDrawBundle

		       0.712 Mt     0.709 %         1 x        0.712 Mt        0.000 Mt        0.000 Mt     0.056 % 	 ..../TexturedRLPrep

		       0.712 Mt    99.944 %         1 x        0.712 Mt        0.000 Mt        0.707 Mt    99.368 % 	 ...../PrepareForRendering

		       0.001 Mt     0.183 %         1 x        0.001 Mt        0.000 Mt        0.001 Mt   100.000 % 	 ....../PrepareMaterials

		       0.003 Mt     0.450 %         1 x        0.003 Mt        0.000 Mt        0.003 Mt   100.000 % 	 ....../PrepareDrawBundle

		       1.483 Mt     1.476 %         1 x        1.483 Mt        0.000 Mt        0.000 Mt     0.020 % 	 ..../DeferredRLPrep

		       1.483 Mt    99.980 %         1 x        1.483 Mt        0.000 Mt        1.165 Mt    78.559 % 	 ...../PrepareForRendering

		       0.001 Mt     0.047 %         1 x        0.001 Mt        0.000 Mt        0.001 Mt   100.000 % 	 ....../PrepareMaterials

		       0.314 Mt    21.171 %         1 x        0.314 Mt        0.000 Mt        0.314 Mt   100.000 % 	 ....../BuildMaterialCache

		       0.003 Mt     0.223 %         1 x        0.003 Mt        0.000 Mt        0.003 Mt   100.000 % 	 ....../PrepareDrawBundle

		      24.501 Mt    24.378 %         1 x       24.501 Mt        0.000 Mt        2.552 Mt    10.415 % 	 ..../RayTracingRLPrep

		       0.048 Mt     0.196 %         1 x        0.048 Mt        0.000 Mt        0.048 Mt   100.000 % 	 ...../PrepareAccelerationStructures

		       0.001 Mt     0.003 %         1 x        0.001 Mt        0.000 Mt        0.001 Mt   100.000 % 	 ...../PrepareCache

		      12.567 Mt    51.291 %         1 x       12.567 Mt        0.000 Mt       12.567 Mt   100.000 % 	 ...../PipelineBuild

		       0.324 Mt     1.322 %         1 x        0.324 Mt        0.000 Mt        0.324 Mt   100.000 % 	 ...../BuildCache

		       7.703 Mt    31.438 %         1 x        7.703 Mt        0.000 Mt        0.000 Mt     0.005 % 	 ...../BuildAccelerationStructures

		       7.702 Mt    99.995 %         1 x        7.702 Mt        0.000 Mt        5.928 Mt    76.962 % 	 ....../AccelerationStructureBuild

		       0.936 Mt    12.159 %         1 x        0.936 Mt        0.000 Mt        0.936 Mt   100.000 % 	 ......./BuildBottomLevelAS

		       0.838 Mt    10.879 %         1 x        0.838 Mt        0.000 Mt        0.838 Mt   100.000 % 	 ......./BuildTopLevelAS

		       1.307 Mt     5.336 %         1 x        1.307 Mt        0.000 Mt        1.307 Mt   100.000 % 	 ...../BuildShaderTables

		      11.157 Mt    11.101 %         1 x       11.157 Mt        0.000 Mt       11.157 Mt   100.000 % 	 ..../GpuSidePrep

		   24077.260 Mt    98.188 %     94953 x        0.254 Mt        3.317 Mt      398.972 Mt     1.657 % 	 ../MainLoop

		     179.332 Mt     0.745 %      1444 x        0.124 Mt        0.088 Mt      178.650 Mt    99.620 % 	 .../Update

		       0.682 Mt     0.380 %         1 x        0.682 Mt        0.000 Mt        0.682 Mt   100.000 % 	 ..../GuiModelApply

		   23498.955 Mt    97.598 %      6194 x        3.794 Mt        3.225 Mt      667.260 Mt     2.840 % 	 .../Render

		    1069.542 Mt     4.551 %      6194 x        0.173 Mt        0.225 Mt       41.074 Mt     3.840 % 	 ..../RayTracing

		     476.162 Mt    44.520 %      6194 x        0.077 Mt        0.101 Mt      469.279 Mt    98.554 % 	 ...../Deferred

		       6.883 Mt     1.446 %      6194 x        0.001 Mt        0.001 Mt        6.883 Mt   100.000 % 	 ....../DeferredRLPrep

		     388.421 Mt    36.317 %      6194 x        0.063 Mt        0.085 Mt      384.991 Mt    99.117 % 	 ...../RayCasting

		       3.429 Mt     0.883 %      6194 x        0.001 Mt        0.001 Mt        3.429 Mt   100.000 % 	 ....../RayTracingRLPrep

		     163.885 Mt    15.323 %      6194 x        0.026 Mt        0.032 Mt      163.885 Mt   100.000 % 	 ...../Resolve

		   21762.153 Mt    92.609 %      6194 x        3.513 Mt        2.856 Mt    21762.153 Mt   100.000 % 	 ..../Present

	WindowThread:

		   24520.653 Mt   100.000 %         1 x    24520.653 Mt    24520.653 Mt    24520.653 Mt   100.000 % 	 /

	GpuThread:

		   18612.853 Mt   100.000 %      6194 x        3.005 Mt        2.366 Mt      126.641 Mt     0.680 % 	 /

		       0.324 Mt     0.002 %         1 x        0.324 Mt        0.000 Mt        0.007 Mt     2.044 % 	 ./ScenePrep

		       0.317 Mt    97.956 %         1 x        0.317 Mt        0.000 Mt        0.001 Mt     0.373 % 	 ../AccelerationStructureBuild

		       0.253 Mt    79.831 %         1 x        0.253 Mt        0.000 Mt        0.253 Mt   100.000 % 	 .../BuildBottomLevelASGpu

		       0.063 Mt    19.796 %         1 x        0.063 Mt        0.000 Mt        0.063 Mt   100.000 % 	 .../BuildTopLevelASGpu

		   18381.592 Mt    98.758 %      6194 x        2.968 Mt        2.362 Mt       14.128 Mt     0.077 % 	 ./RayTracingGpu

		     789.931 Mt     4.297 %      6194 x        0.128 Mt        0.071 Mt      789.931 Mt   100.000 % 	 ../DeferredRLGpu

		    4337.408 Mt    23.596 %      6194 x        0.700 Mt        0.261 Mt     4337.408 Mt   100.000 % 	 ../RayTracingRLGpu

		   13240.124 Mt    72.029 %      6194 x        2.138 Mt        2.030 Mt    13240.124 Mt   100.000 % 	 ../ResolveRLGpu

		     104.296 Mt     0.560 %      6194 x        0.017 Mt        0.003 Mt      104.296 Mt   100.000 % 	 ./Present


	============================


