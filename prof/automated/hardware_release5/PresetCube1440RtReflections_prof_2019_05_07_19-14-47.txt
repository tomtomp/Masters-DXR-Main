Configuration: 
Command line parameters  = E:\programming\FIT-DIP\Masters-DXR-Main-git\build\Measurement\x64\Release\Measurement.exe --scene Cube/Cube.gltf --profile-automation --profile-camera-track Cube/Cube.trk --rt-mode --width 2560 --height 1440 --profile-output PresetCube1440RtReflections --quality-preset Reflections 
Build type               = Release
Device name              = NVIDIA GeForce RTX 2080 Ti
Device memory MB         = 11049
Ray Tracing backend      = Hardware
Window width             = 2560
Window height            = 1421
Window title             = Direct3D App
Back-buffer count        = 3
Fill Triangles           = enabled
Triangles Font Clock-Wise= enabled
Fast Build AS            = disabled
Target FPS               = 0
Target UPS               = 60
Tearing                  = enabled
VSync                    = disabled
GUI rendering            = disabled
Debug layer              = disabled
GPU validation           = disabled
GPU profiling            = enabled
Render mode              = Ray Tracing
Loaded scene file        = Cube/Cube.gltf
Using testing scene      = disabled
Duplication              = 1
Duplication in cube      = disabled
Duplication offset       = 10
BLAS duplication         = disabled
Rays per pixel           = 1
Quality preset           = Reflections
Rasterization Quality    : 
  AO                     = disabled
  AO kernel size         = 1
  Shadows                = disabled
  Shadow map resolution  = 1
  Shadow PCF kernel size = 0
  Reflections            = enabled
Ray Tracing Quality      : 
  AO                     = disabled
  AO rays                = 0
  AO kernel size         = 4
  Shadows                = disabled
  Shadow rays            = 0
  Shadow kernel size     = 4
  Reflections            = enabled

ProfilingAggregator: 
Render	,	0.8094	,	1.7208	,	1.9381	,	1.9511	,	1.9172	,	1.7019	,	1.7337	,	1.8097	,	1.7494	,	1.5467	,	1.0438	,	1.4383	,	1.1438	,	1.1418	,	1.3912	,	1.3617	,	1.5389	,	1.7214	,	1.8073	,	1.9047	,	1.5859	,	1.2005	,	1.0313	,	0.9452	,	0.9111	,	0.634	,	0.6988	,	0.6423	,	0.5718	,	0.6538
Update	,	0.0704	,	0.0763	,	0.0428	,	0.0532	,	0.0582	,	0.0342	,	0.0708	,	0.061	,	0.0788	,	0.0694	,	0.0723	,	0.08	,	0.064	,	0.0461	,	0.0814	,	0.0454	,	0.078	,	0.0785	,	0.08	,	0.0699	,	0.0793	,	0.0681	,	0.0701	,	0.0692	,	0.0684	,	0.0621	,	0.0573	,	0.046	,	0.0441	,	0.0236
RayTracing	,	0.0751	,	0.0816	,	0.0658	,	0.1013	,	0.1163	,	0.0757	,	0.0946	,	0.1259	,	0.1757	,	0.1704	,	0.0717	,	0.1693	,	0.1563	,	0.172	,	0.1709	,	0.1275	,	0.1037	,	0.1202	,	0.1736	,	0.1708	,	0.1563	,	0.15	,	0.1024	,	0.1414	,	0.1462	,	0.0632	,	0.1165	,	0.1354	,	0.0835	,	0.1203
RayTracingGpu	,	0.490144	,	0.61952	,	0.651264	,	0.637344	,	0.620448	,	0.735296	,	0.687488	,	0.768608	,	0.886368	,	0.811264	,	0.700256	,	0.7632	,	0.632096	,	0.581248	,	0.645824	,	0.73472	,	0.846816	,	0.98656	,	1.03331	,	1.03136	,	0.911424	,	0.669408	,	0.57104	,	0.469984	,	0.410784	,	0.350752	,	0.2976	,	0.271776	,	0.25472	,	0.253024
DeferredRLGpu	,	0.181824	,	0.2424	,	0.257536	,	0.264096	,	0.27792	,	0.33792	,	0.312992	,	0.356832	,	0.416992	,	0.375776	,	0.32	,	0.35312	,	0.283808	,	0.253792	,	0.288832	,	0.336832	,	0.395872	,	0.466176	,	0.491744	,	0.491104	,	0.423392	,	0.289504	,	0.237664	,	0.185152	,	0.153888	,	0.12112	,	0.09168	,	0.0752	,	0.064864	,	0.064256
RayTracingRLGpu	,	0.14176	,	0.165408	,	0.170144	,	0.159936	,	0.14752	,	0.163584	,	0.15744	,	0.168832	,	0.18832	,	0.174368	,	0.158144	,	0.168256	,	0.148928	,	0.144224	,	0.152032	,	0.16448	,	0.18208	,	0.206912	,	0.215136	,	0.21568	,	0.19744	,	0.164832	,	0.148192	,	0.13248	,	0.12544	,	0.117216	,	0.111168	,	0.109024	,	0.106464	,	0.106816
ResolveRLGpu	,	0.164928	,	0.207488	,	0.2192	,	0.210944	,	0.191808	,	0.230432	,	0.214336	,	0.240544	,	0.278016	,	0.25872	,	0.219968	,	0.239712	,	0.197344	,	0.181216	,	0.202848	,	0.231168	,	0.266304	,	0.308832	,	0.324032	,	0.322624	,	0.28768	,	0.213088	,	0.18304	,	0.150304	,	0.129472	,	0.110432	,	0.093376	,	0.084736	,	0.080352	,	0.07968
Index	,	0	,	1	,	2	,	3	,	4	,	5	,	6	,	7	,	8	,	9	,	10	,	11	,	12	,	13	,	14	,	15	,	16	,	17	,	18	,	19	,	20	,	21	,	22	,	23	,	24	,	25	,	26	,	27	,	28	,	29

ProfilingBuildAggregator: 
BuildBottomLevelAS	,	0.8271
BuildBottomLevelASGpu	,	0.128224
BuildTopLevelAS	,	0.8316
BuildTopLevelASGpu	,	0.060256
Duplication	,	1
Triangles	,	12
Meshes	,	1
BottomLevels	,	1
TopLevelSize	,	64
BottomLevelsSize	,	4480
Index	,	0

FpsAggregator: 
FPS	,	924	,	945	,	834	,	841	,	856	,	770	,	749	,	771	,	636	,	617	,	788	,	711	,	756	,	903	,	855	,	778	,	680	,	592	,	547	,	538	,	553	,	737	,	841	,	950	,	1106	,	1193	,	1438	,	1586	,	1644	,	1688
UPS	,	59	,	60	,	60	,	61	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	61	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60
FrameTime	,	1.08288	,	1.05833	,	1.19966	,	1.19007	,	1.16893	,	1.29982	,	1.33615	,	1.29904	,	1.57503	,	1.62325	,	1.27006	,	1.40795	,	1.32283	,	1.10818	,	1.16999	,	1.28625	,	1.47094	,	1.69084	,	1.83089	,	1.85976	,	1.81021	,	1.35815	,	1.18923	,	1.05272	,	0.904789	,	0.838272	,	0.695693	,	0.630896	,	0.608551	,	0.592468
GigaRays/s	,	3.35935	,	3.43725	,	3.03232	,	3.05676	,	3.11205	,	2.79867	,	2.72256	,	2.80034	,	2.30964	,	2.24103	,	2.86425	,	2.58374	,	2.74999	,	3.28265	,	3.10923	,	2.8282	,	2.47309	,	2.15145	,	1.98688	,	1.95604	,	2.00958	,	2.67847	,	3.05893	,	3.45557	,	4.02056	,	4.33959	,	5.22898	,	5.76602	,	5.97774	,	6.14001
Index	,	0	,	1	,	2	,	3	,	4	,	5	,	6	,	7	,	8	,	9	,	10	,	11	,	12	,	13	,	14	,	15	,	16	,	17	,	18	,	19	,	20	,	21	,	22	,	23	,	24	,	25	,	26	,	27	,	28	,	29


AccelerationStructure: 
Ray tracing acceleration structure building statistics: 
	Bottom Level Acceleration Structure: 
		Maximum [B]: 4480
		Minimum [B]: 4480
		Currently Allocated [B]: 4480
		Currently Created [num]: 1
		Current Average [B]: 4480
		Maximum Triangles [num]: 12
		Minimum Triangles [num]: 12
		Total Triangles [num]: 12
		Maximum Meshes [num]: 1
		Minimum Meshes [num]: 1
		Total Meshes [num]: 1
	Top Level Acceleration Structure: 
		Maximum [B]: 64
		Minimum [B]: 64
		Currently Allocated [B]: 64
		Total Allocated [B]: 64
		Total Created [num]: 1
		Average Allocated [B]: 64
		Maximum Instances [num]: 1
		Minimum Instances [num]: 1
		Total Instances [num]: 1
	Scratch Buffer: 
		Maximum [B]: 896
		Minimum [B]: 896
		Currently Allocated [B]: 896
		Total Allocated [B]: 896
		Total Created [num]: 1
		Average Allocated [B]: 896
ProfilingStack: 
[PROF] Profiling statistics: 
	Scopes entered : 399634
	Scopes exited : 399633
	Overhead per scope [ticks] : 133.6
	Threads entered : 3
	Threads exited : 0
	============================
		         Sum[Mt]   Parent[%]  Samples[num]       Avg[Mt]   Last/Curr[Mt]        Self[Mt]     Self[%] 	 Name

	main:

		   30416.564 Mt   100.000 %         1 x    30416.564 Mt    30416.564 Mt        0.412 Mt     0.001 % 	 /

		   30416.161 Mt    99.999 %         1 x    30416.162 Mt    30416.161 Mt      261.907 Mt     0.861 % 	 ./Main application loop

		       2.267 Mt     0.007 %         1 x        2.267 Mt        0.000 Mt        2.267 Mt   100.000 % 	 ../TexturedRLInitialization

		       1.343 Mt     0.004 %         1 x        1.343 Mt        0.000 Mt        1.343 Mt   100.000 % 	 ../ShadowMapRLInitialization

		       5.505 Mt     0.018 %         1 x        5.505 Mt        0.000 Mt        5.505 Mt   100.000 % 	 ../RasterResolveRLInitialization

		       3.750 Mt     0.012 %         1 x        3.750 Mt        0.000 Mt        3.750 Mt   100.000 % 	 ../ResolveRLInitialization

		   30017.979 Mt    98.691 %     48971 x        0.613 Mt        0.499 Mt      417.883 Mt     1.392 % 	 ../MainLoop

		     200.157 Mt     0.667 %      1802 x        0.111 Mt        0.028 Mt      199.842 Mt    99.843 % 	 .../Update

		       0.315 Mt     0.157 %         1 x        0.315 Mt        0.000 Mt        0.315 Mt   100.000 % 	 ..../GuiModelApply

		   29399.939 Mt    97.941 %     26832 x        1.096 Mt        0.471 Mt     1752.937 Mt     5.962 % 	 .../Render

		    3122.639 Mt    10.621 %     26832 x        0.116 Mt        0.040 Mt      108.639 Mt     3.479 % 	 ..../RayTracing

		    1388.725 Mt    44.473 %     26832 x        0.052 Mt        0.017 Mt     1371.582 Mt    98.766 % 	 ...../Deferred

		      17.143 Mt     1.234 %     26832 x        0.001 Mt        0.000 Mt       17.143 Mt   100.000 % 	 ....../DeferredRLPrep

		    1131.138 Mt    36.224 %     26832 x        0.042 Mt        0.015 Mt     1118.636 Mt    98.895 % 	 ...../RayCasting

		      12.502 Mt     1.105 %     26832 x        0.000 Mt        0.000 Mt       12.502 Mt   100.000 % 	 ....../RayTracingRLPrep

		     494.137 Mt    15.824 %     26832 x        0.018 Mt        0.006 Mt      494.137 Mt   100.000 % 	 ...../Resolve

		   24524.363 Mt    83.416 %     26832 x        0.914 Mt        0.411 Mt    24524.363 Mt   100.000 % 	 ..../Present

		       2.935 Mt     0.010 %         1 x        2.935 Mt        0.000 Mt        2.935 Mt   100.000 % 	 ../AmbientOcclusionRLInitialization

		       5.401 Mt     0.018 %         1 x        5.401 Mt        0.000 Mt        5.401 Mt   100.000 % 	 ../DeferredRLInitialization

		      51.076 Mt     0.168 %         1 x       51.076 Mt        0.000 Mt        2.842 Mt     5.564 % 	 ../RayTracingRLInitialization

		      48.234 Mt    94.436 %         1 x       48.234 Mt        0.000 Mt       48.234 Mt   100.000 % 	 .../PipelineBuild

		      64.000 Mt     0.210 %         1 x       64.000 Mt        0.000 Mt        0.190 Mt     0.297 % 	 ../Initialization

		      63.810 Mt    99.703 %         1 x       63.810 Mt        0.000 Mt        0.944 Mt     1.479 % 	 .../ScenePrep

		      22.381 Mt    35.075 %         1 x       22.381 Mt        0.000 Mt        6.110 Mt    27.300 % 	 ..../SceneLoad

		      10.694 Mt    47.781 %         1 x       10.694 Mt        0.000 Mt        2.733 Mt    25.556 % 	 ...../glTF-LoadScene

		       7.961 Mt    74.444 %         2 x        3.981 Mt        0.000 Mt        7.961 Mt   100.000 % 	 ....../glTF-LoadImageData

		       3.854 Mt    17.220 %         1 x        3.854 Mt        0.000 Mt        3.854 Mt   100.000 % 	 ...../glTF-CreateScene

		       1.723 Mt     7.699 %         1 x        1.723 Mt        0.000 Mt        1.723 Mt   100.000 % 	 ...../SceneDesc-CreateScene

		       3.595 Mt     5.635 %         1 x        3.595 Mt        0.000 Mt        0.000 Mt     0.011 % 	 ..../ShadowMapRLPrep

		       3.595 Mt    99.989 %         1 x        3.595 Mt        0.000 Mt        3.588 Mt    99.797 % 	 ...../PrepareForRendering

		       0.007 Mt     0.203 %         1 x        0.007 Mt        0.000 Mt        0.007 Mt   100.000 % 	 ....../PrepareDrawBundle

		       1.478 Mt     2.316 %         1 x        1.478 Mt        0.000 Mt        0.000 Mt     0.020 % 	 ..../TexturedRLPrep

		       1.478 Mt    99.980 %         1 x        1.478 Mt        0.000 Mt        1.473 Mt    99.716 % 	 ...../PrepareForRendering

		       0.001 Mt     0.068 %         1 x        0.001 Mt        0.000 Mt        0.001 Mt   100.000 % 	 ....../PrepareMaterials

		       0.003 Mt     0.217 %         1 x        0.003 Mt        0.000 Mt        0.003 Mt   100.000 % 	 ....../PrepareDrawBundle

		       1.316 Mt     2.063 %         1 x        1.316 Mt        0.000 Mt        0.000 Mt     0.030 % 	 ..../DeferredRLPrep

		       1.316 Mt    99.970 %         1 x        1.316 Mt        0.000 Mt        1.021 Mt    77.576 % 	 ...../PrepareForRendering

		       0.001 Mt     0.046 %         1 x        0.001 Mt        0.000 Mt        0.001 Mt   100.000 % 	 ....../PrepareMaterials

		       0.292 Mt    22.158 %         1 x        0.292 Mt        0.000 Mt        0.292 Mt   100.000 % 	 ....../BuildMaterialCache

		       0.003 Mt     0.220 %         1 x        0.003 Mt        0.000 Mt        0.003 Mt   100.000 % 	 ....../PrepareDrawBundle

		      22.938 Mt    35.948 %         1 x       22.938 Mt        0.000 Mt        2.499 Mt    10.894 % 	 ..../RayTracingRLPrep

		       0.055 Mt     0.238 %         1 x        0.055 Mt        0.000 Mt        0.055 Mt   100.000 % 	 ...../PrepareAccelerationStructures

		       0.001 Mt     0.003 %         1 x        0.001 Mt        0.000 Mt        0.001 Mt   100.000 % 	 ...../PrepareCache

		      12.919 Mt    56.320 %         1 x       12.919 Mt        0.000 Mt       12.919 Mt   100.000 % 	 ...../PipelineBuild

		       0.323 Mt     1.410 %         1 x        0.323 Mt        0.000 Mt        0.323 Mt   100.000 % 	 ...../BuildCache

		       5.795 Mt    25.262 %         1 x        5.795 Mt        0.000 Mt        0.000 Mt     0.007 % 	 ...../BuildAccelerationStructures

		       5.794 Mt    99.993 %         1 x        5.794 Mt        0.000 Mt        4.136 Mt    71.373 % 	 ....../AccelerationStructureBuild

		       0.827 Mt    14.275 %         1 x        0.827 Mt        0.000 Mt        0.827 Mt   100.000 % 	 ......./BuildBottomLevelAS

		       0.832 Mt    14.352 %         1 x        0.832 Mt        0.000 Mt        0.832 Mt   100.000 % 	 ......./BuildTopLevelAS

		       1.347 Mt     5.873 %         1 x        1.347 Mt        0.000 Mt        1.347 Mt   100.000 % 	 ...../BuildShaderTables

		      11.157 Mt    17.485 %         1 x       11.157 Mt        0.000 Mt       11.157 Mt   100.000 % 	 ..../GpuSidePrep

	WindowThread:

		   30414.260 Mt   100.000 %         1 x    30414.261 Mt    30414.260 Mt    30414.260 Mt   100.000 % 	 /

	GpuThread:

		   15696.008 Mt   100.000 %     26832 x        0.585 Mt        0.253 Mt      140.373 Mt     0.894 % 	 /

		       0.195 Mt     0.001 %         1 x        0.195 Mt        0.000 Mt        0.006 Mt     3.178 % 	 ./ScenePrep

		       0.189 Mt    96.822 %         1 x        0.189 Mt        0.000 Mt        0.001 Mt     0.338 % 	 ../AccelerationStructureBuild

		       0.128 Mt    67.800 %         1 x        0.128 Mt        0.000 Mt        0.128 Mt   100.000 % 	 .../BuildBottomLevelASGpu

		       0.060 Mt    31.861 %         1 x        0.060 Mt        0.000 Mt        0.060 Mt   100.000 % 	 .../BuildTopLevelASGpu

		   15492.242 Mt    98.702 %     26832 x        0.577 Mt        0.251 Mt       65.554 Mt     0.423 % 	 ./RayTracingGpu

		    6516.468 Mt    42.063 %     26832 x        0.243 Mt        0.065 Mt     6516.468 Mt   100.000 % 	 ../DeferredRLGpu

		    3979.400 Mt    25.686 %     26832 x        0.148 Mt        0.106 Mt     3979.400 Mt   100.000 % 	 ../RayTracingRLGpu

		    4930.820 Mt    31.828 %     26832 x        0.184 Mt        0.080 Mt     4930.820 Mt   100.000 % 	 ../ResolveRLGpu

		      63.198 Mt     0.403 %     26832 x        0.002 Mt        0.002 Mt       63.198 Mt   100.000 % 	 ./Present


	============================


