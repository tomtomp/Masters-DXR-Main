Configuration: 
Command line parameters  = E:\programming\FIT-DIP\Masters-DXR-Main-git\build\Measurement\x64\Release\Measurement.exe --scene Suzanne/Suzanne.gltf --profile-automation --profile-camera-track Suzanne/Suzanne.trk --width 2560 --height 1440 --profile-output PresetSuzanne1440RaHardShadows --quality-preset HardShadows 
Build type               = Release
Device name              = NVIDIA GeForce RTX 2080 Ti
Device memory MB         = 11049
Ray Tracing backend      = Hardware
Window width             = 2560
Window height            = 1421
Window title             = Direct3D App
Back-buffer count        = 3
Fill Triangles           = enabled
Triangles Font Clock-Wise= enabled
Fast Build AS            = disabled
Target FPS               = 0
Target UPS               = 60
Tearing                  = enabled
VSync                    = disabled
GUI rendering            = disabled
Debug layer              = disabled
GPU validation           = disabled
GPU profiling            = enabled
Render mode              = Rasterization
Loaded scene file        = Suzanne/Suzanne.gltf
Using testing scene      = disabled
Duplication              = 1
Duplication in cube      = disabled
Duplication offset       = 10
BLAS duplication         = disabled
Rays per pixel           = 1
Quality preset           = HardShadows
Rasterization Quality    : 
  AO                     = disabled
  AO kernel size         = 1
  Shadows                = enabled
  Shadow map resolution  = 2048
  Shadow PCF kernel size = 0
  Reflections            = disabled
Ray Tracing Quality      : 
  AO                     = disabled
  AO rays                = 0
  AO kernel size         = 4
  Shadows                = enabled
  Shadow rays            = 1
  Shadow kernel size     = 4
  Reflections            = disabled

ProfilingAggregator: 
Render	,	0.4453	,	0.5253	,	0.7414	,	0.545	,	0.4141	,	0.6012	,	0.5073	,	2.0389	,	1.5031	,	1.6474	,	1.6748	,	1.8074	,	2.409	,	1.6812	,	1.4673	,	1.5361	,	2.1092	,	1.4784	,	1.2495	,	1.5468	,	1.85	,	2.0325	,	1.9264	,	0.3426
Update	,	0.0291	,	0.0242	,	0.0636	,	0.0342	,	0.0215	,	0.0695	,	0.0587	,	0.0241	,	0.0655	,	0.0637	,	0.0471	,	0.0225	,	0.0345	,	0.0549	,	0.024	,	0.0685	,	0.0723	,	0.0426	,	0.0543	,	0.0429	,	0.0388	,	0.0273	,	0.0281	,	0.027
DeferredRLGpu	,	0.137792	,	0.185408	,	0.202784	,	0.154016	,	0.101088	,	0.070336	,	0.05328	,	0.045376	,	0.06384	,	0.092	,	0.105856	,	0.080096	,	0.054208	,	0.054368	,	0.086688	,	0.18704	,	0.226976	,	0.259136	,	0.40976	,	0.317824	,	0.274144	,	0.137248	,	0.083424	,	0.071392
Index	,	0	,	1	,	2	,	3	,	4	,	5	,	6	,	7	,	8	,	9	,	10	,	11	,	12	,	13	,	14	,	15	,	16	,	17	,	18	,	19	,	20	,	21	,	22	,	23

ProfilingBuildAggregator: 
BuildBottomLevelAS	,	0.842
BuildBottomLevelASGpu	,	0.447232
BuildTopLevelAS	,	0.8053
BuildTopLevelASGpu	,	0.060448
Duplication	,	1
Triangles	,	3936
Meshes	,	1
BottomLevels	,	1
TopLevelSize	,	64
BottomLevelsSize	,	282112
Index	,	0

FpsAggregator: 
FPS	,	1480	,	1582	,	1460	,	1488	,	1755	,	1996	,	2489	,	2397	,	2292	,	2083	,	1891	,	1943	,	2131	,	2371	,	2068	,	1779	,	1340	,	1308	,	1030	,	1031	,	1221	,	1649	,	2287	,	2598
UPS	,	59	,	61	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60
FrameTime	,	0.675923	,	0.632314	,	0.685048	,	0.67223	,	0.569939	,	0.501077	,	0.401875	,	0.417314	,	0.436533	,	0.480419	,	0.529133	,	0.515014	,	0.469703	,	0.421951	,	0.483862	,	0.562526	,	0.747208	,	0.764858	,	0.971136	,	0.971056	,	0.819467	,	0.606842	,	0.437503	,	0.384919
GigaRays/s	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0
Index	,	0	,	1	,	2	,	3	,	4	,	5	,	6	,	7	,	8	,	9	,	10	,	11	,	12	,	13	,	14	,	15	,	16	,	17	,	18	,	19	,	20	,	21	,	22	,	23


AccelerationStructure: 
Ray tracing acceleration structure building statistics: 
	Bottom Level Acceleration Structure: 
		Maximum [B]: 282112
		Minimum [B]: 282112
		Currently Allocated [B]: 282112
		Currently Created [num]: 1
		Current Average [B]: 282112
		Maximum Triangles [num]: 3936
		Minimum Triangles [num]: 3936
		Total Triangles [num]: 3936
		Maximum Meshes [num]: 1
		Minimum Meshes [num]: 1
		Total Meshes [num]: 1
	Top Level Acceleration Structure: 
		Maximum [B]: 64
		Minimum [B]: 64
		Currently Allocated [B]: 64
		Total Allocated [B]: 64
		Total Created [num]: 1
		Average Allocated [B]: 64
		Maximum Instances [num]: 1
		Minimum Instances [num]: 1
		Total Instances [num]: 1
	Scratch Buffer: 
		Maximum [B]: 82944
		Minimum [B]: 82944
		Currently Allocated [B]: 82944
		Total Allocated [B]: 82944
		Total Created [num]: 1
		Average Allocated [B]: 82944
ProfilingStack: 
[PROF] Profiling statistics: 
	Scopes entered : 568896
	Scopes exited : 568895
	Overhead per scope [ticks] : 103.4
	Threads entered : 3
	Threads exited : 0
	============================
		         Sum[Mt]   Parent[%]  Samples[num]       Avg[Mt]   Last/Curr[Mt]        Self[Mt]     Self[%] 	 Name

	main:

		   24460.657 Mt   100.000 %         1 x    24460.657 Mt    24460.656 Mt        0.369 Mt     0.002 % 	 /

		   24460.300 Mt    99.999 %         1 x    24460.300 Mt    24460.300 Mt      269.549 Mt     1.102 % 	 ./Main application loop

		       2.310 Mt     0.009 %         1 x        2.310 Mt        0.000 Mt        2.310 Mt   100.000 % 	 ../TexturedRLInitialization

		       1.326 Mt     0.005 %         1 x        1.326 Mt        0.000 Mt        1.326 Mt   100.000 % 	 ../ShadowMapRLInitialization

		       4.693 Mt     0.019 %         1 x        4.693 Mt        0.000 Mt        4.693 Mt   100.000 % 	 ../RasterResolveRLInitialization

		       3.570 Mt     0.015 %         1 x        3.570 Mt        0.000 Mt        3.570 Mt   100.000 % 	 ../ResolveRLInitialization

		   24012.536 Mt    98.169 %    130700 x        0.184 Mt        0.505 Mt      321.143 Mt     1.337 % 	 ../MainLoop

		     142.306 Mt     0.593 %      1441 x        0.099 Mt        0.039 Mt      141.716 Mt    99.585 % 	 .../Update

		       0.590 Mt     0.415 %         1 x        0.590 Mt        0.000 Mt        0.590 Mt   100.000 % 	 ..../GuiModelApply

		   23549.087 Mt    98.070 %     43671 x        0.539 Mt        0.462 Mt     1381.554 Mt     5.867 % 	 .../Render

		    2442.466 Mt    10.372 %     43671 x        0.056 Mt        0.066 Mt     2416.415 Mt    98.933 % 	 ..../Rasterization

		      14.323 Mt     0.586 %     43671 x        0.000 Mt        0.000 Mt       14.323 Mt   100.000 % 	 ...../DeferredRLPrep

		      11.728 Mt     0.480 %     43671 x        0.000 Mt        0.000 Mt       11.728 Mt   100.000 % 	 ...../ShadowMapRLPrep

		   19725.067 Mt    83.761 %     43671 x        0.452 Mt        0.344 Mt    19725.067 Mt   100.000 % 	 ..../Present

		       2.774 Mt     0.011 %         1 x        2.774 Mt        0.000 Mt        2.774 Mt   100.000 % 	 ../AmbientOcclusionRLInitialization

		       5.008 Mt     0.020 %         1 x        5.008 Mt        0.000 Mt        5.008 Mt   100.000 % 	 ../DeferredRLInitialization

		      52.158 Mt     0.213 %         1 x       52.158 Mt        0.000 Mt        2.325 Mt     4.458 % 	 ../RayTracingRLInitialization

		      49.833 Mt    95.542 %         1 x       49.833 Mt        0.000 Mt       49.833 Mt   100.000 % 	 .../PipelineBuild

		     106.375 Mt     0.435 %         1 x      106.375 Mt        0.000 Mt        0.183 Mt     0.172 % 	 ../Initialization

		     106.192 Mt    99.828 %         1 x      106.192 Mt        0.000 Mt        1.784 Mt     1.680 % 	 .../ScenePrep

		      62.064 Mt    58.446 %         1 x       62.064 Mt        0.000 Mt       15.113 Mt    24.350 % 	 ..../SceneLoad

		      39.021 Mt    62.872 %         1 x       39.021 Mt        0.000 Mt        8.340 Mt    21.374 % 	 ...../glTF-LoadScene

		      30.680 Mt    78.626 %         2 x       15.340 Mt        0.000 Mt       30.680 Mt   100.000 % 	 ....../glTF-LoadImageData

		       6.124 Mt     9.868 %         1 x        6.124 Mt        0.000 Mt        6.124 Mt   100.000 % 	 ...../glTF-CreateScene

		       1.806 Mt     2.911 %         1 x        1.806 Mt        0.000 Mt        1.806 Mt   100.000 % 	 ...../SceneDesc-CreateScene

		       0.310 Mt     0.292 %         1 x        0.310 Mt        0.000 Mt        0.000 Mt     0.097 % 	 ..../ShadowMapRLPrep

		       0.310 Mt    99.903 %         1 x        0.310 Mt        0.000 Mt        0.302 Mt    97.419 % 	 ...../PrepareForRendering

		       0.008 Mt     2.581 %         1 x        0.008 Mt        0.000 Mt        0.008 Mt   100.000 % 	 ....../PrepareDrawBundle

		       1.445 Mt     1.361 %         1 x        1.445 Mt        0.000 Mt        0.000 Mt     0.028 % 	 ..../TexturedRLPrep

		       1.445 Mt    99.972 %         1 x        1.445 Mt        0.000 Mt        1.441 Mt    99.723 % 	 ...../PrepareForRendering

		       0.001 Mt     0.055 %         1 x        0.001 Mt        0.000 Mt        0.001 Mt   100.000 % 	 ....../PrepareMaterials

		       0.003 Mt     0.221 %         1 x        0.003 Mt        0.000 Mt        0.003 Mt   100.000 % 	 ....../PrepareDrawBundle

		       1.357 Mt     1.278 %         1 x        1.357 Mt        0.000 Mt        0.000 Mt     0.029 % 	 ..../DeferredRLPrep

		       1.357 Mt    99.971 %         1 x        1.357 Mt        0.000 Mt        1.060 Mt    78.105 % 	 ...../PrepareForRendering

		       0.001 Mt     0.037 %         1 x        0.001 Mt        0.000 Mt        0.001 Mt   100.000 % 	 ....../PrepareMaterials

		       0.293 Mt    21.579 %         1 x        0.293 Mt        0.000 Mt        0.293 Mt   100.000 % 	 ....../BuildMaterialCache

		       0.004 Mt     0.280 %         1 x        0.004 Mt        0.000 Mt        0.004 Mt   100.000 % 	 ....../PrepareDrawBundle

		      24.573 Mt    23.140 %         1 x       24.573 Mt        0.000 Mt        2.522 Mt    10.262 % 	 ..../RayTracingRLPrep

		       0.042 Mt     0.173 %         1 x        0.042 Mt        0.000 Mt        0.042 Mt   100.000 % 	 ...../PrepareAccelerationStructures

		       0.001 Mt     0.003 %         1 x        0.001 Mt        0.000 Mt        0.001 Mt   100.000 % 	 ...../PrepareCache

		      16.212 Mt    65.976 %         1 x       16.212 Mt        0.000 Mt       16.212 Mt   100.000 % 	 ...../PipelineBuild

		       0.319 Mt     1.298 %         1 x        0.319 Mt        0.000 Mt        0.319 Mt   100.000 % 	 ...../BuildCache

		       4.139 Mt    16.845 %         1 x        4.139 Mt        0.000 Mt        0.000 Mt     0.010 % 	 ...../BuildAccelerationStructures

		       4.139 Mt    99.990 %         1 x        4.139 Mt        0.000 Mt        2.492 Mt    60.200 % 	 ....../AccelerationStructureBuild

		       0.842 Mt    20.344 %         1 x        0.842 Mt        0.000 Mt        0.842 Mt   100.000 % 	 ......./BuildBottomLevelAS

		       0.805 Mt    19.457 %         1 x        0.805 Mt        0.000 Mt        0.805 Mt   100.000 % 	 ......./BuildTopLevelAS

		       1.338 Mt     5.444 %         1 x        1.338 Mt        0.000 Mt        1.338 Mt   100.000 % 	 ...../BuildShaderTables

		      14.658 Mt    13.803 %         1 x       14.658 Mt        0.000 Mt       14.658 Mt   100.000 % 	 ..../GpuSidePrep

	WindowThread:

		   24458.671 Mt   100.000 %         1 x    24458.671 Mt    24458.671 Mt    24458.671 Mt   100.000 % 	 /

	GpuThread:

		   10418.411 Mt   100.000 %     43671 x        0.239 Mt        0.145 Mt      154.978 Mt     1.488 % 	 /

		       0.513 Mt     0.005 %         1 x        0.513 Mt        0.000 Mt        0.005 Mt     0.991 % 	 ./ScenePrep

		       0.508 Mt    99.009 %         1 x        0.508 Mt        0.000 Mt        0.001 Mt     0.120 % 	 ../AccelerationStructureBuild

		       0.447 Mt    87.988 %         1 x        0.447 Mt        0.000 Mt        0.447 Mt   100.000 % 	 .../BuildBottomLevelASGpu

		       0.060 Mt    11.892 %         1 x        0.060 Mt        0.000 Mt        0.060 Mt   100.000 % 	 .../BuildTopLevelASGpu

		   10186.436 Mt    97.773 %     43671 x        0.233 Mt        0.142 Mt       94.401 Mt     0.927 % 	 ./RasterizationGpu

		    5415.511 Mt    53.164 %     43671 x        0.124 Mt        0.072 Mt     5415.511 Mt   100.000 % 	 ../DeferredRLGpu

		     225.686 Mt     2.216 %     43671 x        0.005 Mt        0.005 Mt      225.686 Mt   100.000 % 	 ../ShadowMapRLGpu

		    4450.839 Mt    43.694 %     43671 x        0.102 Mt        0.064 Mt     4450.839 Mt   100.000 % 	 ../RasterResolveRLGpu

		      76.484 Mt     0.734 %     43671 x        0.002 Mt        0.002 Mt       76.484 Mt   100.000 % 	 ./Present


	============================


