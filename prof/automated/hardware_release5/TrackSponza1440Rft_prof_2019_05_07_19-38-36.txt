Configuration: 
Command line parameters  = E:\programming\FIT-DIP\Masters-DXR-Main-git\build\Measurement\x64\Release\Measurement.exe --scene Sponza/Sponza.gltf --quality-preset Low --profile-automation --profile-camera-track Sponza/Sponza.trk --rt-mode --width 2560 --height 1440 --profile-output TrackSponza1440Rft --fast-build-as 
Build type               = Release
Device name              = NVIDIA GeForce RTX 2080 Ti
Device memory MB         = 11049
Ray Tracing backend      = Hardware
Window width             = 2560
Window height            = 1421
Window title             = Direct3D App
Back-buffer count        = 3
Fill Triangles           = enabled
Triangles Font Clock-Wise= enabled
Fast Build AS            = enabled
Target FPS               = 0
Target UPS               = 60
Tearing                  = enabled
VSync                    = disabled
GUI rendering            = disabled
Debug layer              = disabled
GPU validation           = disabled
GPU profiling            = enabled
Render mode              = Ray Tracing
Loaded scene file        = Sponza/Sponza.gltf
Using testing scene      = disabled
Duplication              = 1
Duplication in cube      = disabled
Duplication offset       = 10
BLAS duplication         = disabled
Rays per pixel           = 5
Quality preset           = Low
Rasterization Quality    : 
  AO                     = enabled
  AO kernel size         = 4
  Shadows                = enabled
  Shadow map resolution  = 2048
  Shadow PCF kernel size = 0
  Reflections            = disabled
Ray Tracing Quality      : 
  AO                     = enabled
  AO rays                = 4
  AO kernel size         = 4
  Shadows                = enabled
  Shadow rays            = 1
  Shadow kernel size     = 4
  Reflections            = disabled

ProfilingAggregator: 
Render	,	9.2696	,	9.98	,	9.2652	,	9.3044	,	9.2957	,	8.9625	,	8.6114	,	7.2827	,	7.7554	,	8.6197	,	9.8422	,	10.2171	,	9.9207	,	9.5662	,	8.7562	,	9.3338	,	9.0937	,	8.6393	,	9.3022	,	10.3663	,	13.2491	,	12.2801	,	11.1782	,	9.8797	,	9.7916	,	9.7212
Update	,	0.0299	,	0.0775	,	0.0742	,	0.079	,	0.0822	,	0.0783	,	0.0838	,	0.0777	,	0.071	,	0.0792	,	0.0783	,	0.0805	,	0.0794	,	0.0762	,	0.08	,	0.0797	,	0.0814	,	0.0797	,	0.082	,	0.0787	,	0.0712	,	0.078	,	0.0784	,	0.0861	,	0.0768	,	0.0807
RayTracing	,	0.076	,	0.1715	,	0.1785	,	0.1762	,	0.1778	,	0.1731	,	0.1861	,	0.1764	,	0.1765	,	0.1748	,	0.1743	,	0.1777	,	0.1824	,	0.1715	,	0.1777	,	0.1758	,	0.1764	,	0.1731	,	0.1736	,	0.176	,	0.2044	,	0.2123	,	0.1742	,	0.1766	,	0.1703	,	0.1763
RayTracingGpu	,	8.40176	,	9.14371	,	8.39766	,	8.32538	,	8.30221	,	7.79059	,	7.67347	,	6.38138	,	6.92861	,	7.84739	,	8.9121	,	9.16342	,	9.11472	,	8.57539	,	7.95619	,	8.31194	,	8.31542	,	7.82851	,	8.55453	,	9.46566	,	12.4431	,	11.3696	,	10.1933	,	9.04838	,	8.73677	,	8.7993
DeferredRLGpu	,	3.21037	,	3.59901	,	3.18141	,	3.18451	,	2.87283	,	2.63392	,	2.37891	,	1.5809	,	1.92346	,	2.56912	,	3.50234	,	3.68051	,	3.86621	,	3.41894	,	3.12192	,	2.91808	,	3.04717	,	2.61504	,	3.28752	,	3.52336	,	4.22352	,	4.2793	,	4.44592	,	3.68448	,	3.92208	,	3.71146
RayTracingRLGpu	,	2.71034	,	3.016	,	2.8359	,	2.78224	,	2.68557	,	2.66416	,	2.91958	,	2.44826	,	2.61757	,	2.51014	,	2.83325	,	2.97309	,	2.77187	,	2.81808	,	2.50083	,	2.89501	,	2.80051	,	2.74067	,	2.6519	,	3.57958	,	5.7024	,	4.80403	,	3.47738	,	2.69933	,	2.47126	,	2.59382
ResolveRLGpu	,	2.47958	,	2.52694	,	2.3783	,	2.35546	,	2.74173	,	2.48989	,	2.37341	,	2.35008	,	2.38454	,	2.76595	,	2.5735	,	2.50723	,	2.47491	,	2.3311	,	2.33027	,	2.49722	,	2.46579	,	2.47117	,	2.61312	,	2.35888	,	2.51536	,	2.28426	,	2.26829	,	2.66186	,	2.33946	,	2.49232
Index	,	0	,	1	,	2	,	3	,	4	,	5	,	6	,	7	,	8	,	9	,	10	,	11	,	12	,	13	,	14	,	15	,	16	,	17	,	18	,	19	,	20	,	21	,	22	,	23	,	24	,	25

ProfilingBuildAggregator: 
BuildBottomLevelAS	,	1.4128
BuildBottomLevelASGpu	,	1.76922
BuildTopLevelAS	,	0.7533
BuildTopLevelASGpu	,	0.087328
Duplication	,	1
Triangles	,	262267
Meshes	,	103
BottomLevels	,	1
TopLevelSize	,	64
BottomLevelsSize	,	14622208
Index	,	0

FpsAggregator: 
FPS	,	76	,	103	,	103	,	104	,	106	,	109	,	117	,	122	,	125	,	122	,	108	,	102	,	101	,	104	,	107	,	109	,	111	,	117	,	116	,	106	,	79	,	78	,	84	,	95	,	104	,	105
UPS	,	59	,	61	,	60	,	60	,	60	,	60	,	61	,	60	,	60	,	61	,	60	,	60	,	60	,	60	,	61	,	60	,	60	,	60	,	60	,	61	,	60	,	61	,	61	,	60	,	61	,	60
FrameTime	,	13.1999	,	9.78102	,	9.73884	,	9.63812	,	9.5203	,	9.17462	,	8.6029	,	8.23451	,	8.0212	,	8.23551	,	9.29404	,	9.82703	,	9.93855	,	9.62657	,	9.41751	,	9.23709	,	9.03193	,	8.54917	,	8.633	,	9.52878	,	12.8069	,	12.9184	,	12.0358	,	10.6252	,	9.68541	,	9.52822
GigaRays/s	,	1.37795	,	1.8596	,	1.86766	,	1.88717	,	1.91053	,	1.98251	,	2.11426	,	2.20885	,	2.26759	,	2.20858	,	1.95704	,	1.85089	,	1.83013	,	1.88944	,	1.93138	,	1.96911	,	2.01383	,	2.12755	,	2.10689	,	1.90883	,	1.42023	,	1.40797	,	1.51123	,	1.71185	,	1.87796	,	1.90894
Index	,	0	,	1	,	2	,	3	,	4	,	5	,	6	,	7	,	8	,	9	,	10	,	11	,	12	,	13	,	14	,	15	,	16	,	17	,	18	,	19	,	20	,	21	,	22	,	23	,	24	,	25


AccelerationStructure: 
Ray tracing acceleration structure building statistics: 
	Bottom Level Acceleration Structure: 
		Maximum [B]: 14622208
		Minimum [B]: 14622208
		Currently Allocated [B]: 14622208
		Currently Created [num]: 1
		Current Average [B]: 14622208
		Maximum Triangles [num]: 262267
		Minimum Triangles [num]: 262267
		Total Triangles [num]: 262267
		Maximum Meshes [num]: 103
		Minimum Meshes [num]: 103
		Total Meshes [num]: 103
	Top Level Acceleration Structure: 
		Maximum [B]: 64
		Minimum [B]: 64
		Currently Allocated [B]: 64
		Total Allocated [B]: 64
		Total Created [num]: 1
		Average Allocated [B]: 64
		Maximum Instances [num]: 1
		Minimum Instances [num]: 1
		Total Instances [num]: 1
	Scratch Buffer: 
		Maximum [B]: 11657856
		Minimum [B]: 11657856
		Currently Allocated [B]: 11657856
		Total Allocated [B]: 11657856
		Total Created [num]: 1
		Average Allocated [B]: 11657856
ProfilingStack: 
[PROF] Profiling statistics: 
	Scopes entered : 96944
	Scopes exited : 96943
	Overhead per scope [ticks] : 103.4
	Threads entered : 3
	Threads exited : 0
	============================
		         Sum[Mt]   Parent[%]  Samples[num]       Avg[Mt]   Last/Curr[Mt]        Self[Mt]     Self[%] 	 Name

	main:

		   30003.929 Mt   100.000 %         1 x    30003.930 Mt    30003.929 Mt        0.372 Mt     0.001 % 	 /

		   30003.589 Mt    99.999 %         1 x    30003.590 Mt    30003.589 Mt      638.997 Mt     2.130 % 	 ./Main application loop

		       2.545 Mt     0.008 %         1 x        2.545 Mt        0.000 Mt        2.545 Mt   100.000 % 	 ../TexturedRLInitialization

		       1.994 Mt     0.007 %         1 x        1.994 Mt        0.000 Mt        1.994 Mt   100.000 % 	 ../ShadowMapRLInitialization

		       3.448 Mt     0.011 %         1 x        3.448 Mt        0.000 Mt        3.448 Mt   100.000 % 	 ../RasterResolveRLInitialization

		       4.065 Mt     0.014 %         1 x        4.065 Mt        0.000 Mt        4.065 Mt   100.000 % 	 ../ResolveRLInitialization

		   26162.491 Mt    87.198 %     59968 x        0.436 Mt       10.048 Mt      453.814 Mt     1.735 % 	 ../MainLoop

		     211.160 Mt     0.807 %      1569 x        0.135 Mt        0.047 Mt      210.551 Mt    99.711 % 	 .../Update

		       0.609 Mt     0.289 %         1 x        0.609 Mt        0.000 Mt        0.609 Mt   100.000 % 	 ..../GuiModelApply

		   25497.517 Mt    97.458 %      2715 x        9.391 Mt        9.900 Mt      317.976 Mt     1.247 % 	 .../Render

		     496.791 Mt     1.948 %      2715 x        0.183 Mt        0.228 Mt       19.000 Mt     3.825 % 	 ..../RayTracing

		     209.570 Mt    42.185 %      2715 x        0.077 Mt        0.093 Mt      206.946 Mt    98.748 % 	 ...../Deferred

		       2.623 Mt     1.252 %      2715 x        0.001 Mt        0.001 Mt        2.623 Mt   100.000 % 	 ....../DeferredRLPrep

		     178.302 Mt    35.891 %      2715 x        0.066 Mt        0.085 Mt      176.678 Mt    99.089 % 	 ...../RayCasting

		       1.624 Mt     0.911 %      2715 x        0.001 Mt        0.001 Mt        1.624 Mt   100.000 % 	 ....../RayTracingRLPrep

		      89.919 Mt    18.100 %      2715 x        0.033 Mt        0.041 Mt       89.919 Mt   100.000 % 	 ...../Resolve

		   24682.750 Mt    96.805 %      2715 x        9.091 Mt        9.514 Mt    24682.750 Mt   100.000 % 	 ..../Present

		       4.238 Mt     0.014 %         1 x        4.238 Mt        0.000 Mt        4.238 Mt   100.000 % 	 ../AmbientOcclusionRLInitialization

		       4.839 Mt     0.016 %         1 x        4.839 Mt        0.000 Mt        4.839 Mt   100.000 % 	 ../DeferredRLInitialization

		     281.206 Mt     0.937 %         1 x      281.206 Mt        0.000 Mt        2.501 Mt     0.889 % 	 ../RayTracingRLInitialization

		     278.706 Mt    99.111 %         1 x      278.706 Mt        0.000 Mt      278.706 Mt   100.000 % 	 .../PipelineBuild

		    2899.766 Mt     9.665 %         1 x     2899.766 Mt        0.000 Mt        0.212 Mt     0.007 % 	 ../Initialization

		    2899.555 Mt    99.993 %         1 x     2899.555 Mt        0.000 Mt        2.174 Mt     0.075 % 	 .../ScenePrep

		    2833.481 Mt    97.721 %         1 x     2833.481 Mt        0.000 Mt      264.833 Mt     9.347 % 	 ..../SceneLoad

		    2261.799 Mt    79.824 %         1 x     2261.799 Mt        0.000 Mt      746.310 Mt    32.996 % 	 ...../glTF-LoadScene

		    1515.489 Mt    67.004 %        69 x       21.964 Mt        0.000 Mt     1515.489 Mt   100.000 % 	 ....../glTF-LoadImageData

		     245.043 Mt     8.648 %         1 x      245.043 Mt        0.000 Mt      245.043 Mt   100.000 % 	 ...../glTF-CreateScene

		      61.806 Mt     2.181 %         1 x       61.806 Mt        0.000 Mt       61.806 Mt   100.000 % 	 ...../SceneDesc-CreateScene

		       0.955 Mt     0.033 %         1 x        0.955 Mt        0.000 Mt        0.001 Mt     0.063 % 	 ..../ShadowMapRLPrep

		       0.955 Mt    99.937 %         1 x        0.955 Mt        0.000 Mt        0.914 Mt    95.726 % 	 ...../PrepareForRendering

		       0.041 Mt     4.274 %         1 x        0.041 Mt        0.000 Mt        0.041 Mt   100.000 % 	 ....../PrepareDrawBundle

		       3.271 Mt     0.113 %         1 x        3.271 Mt        0.000 Mt        0.001 Mt     0.018 % 	 ..../TexturedRLPrep

		       3.270 Mt    99.982 %         1 x        3.270 Mt        0.000 Mt        3.234 Mt    98.896 % 	 ...../PrepareForRendering

		       0.002 Mt     0.052 %         1 x        0.002 Mt        0.000 Mt        0.002 Mt   100.000 % 	 ....../PrepareMaterials

		       0.034 Mt     1.052 %         1 x        0.034 Mt        0.000 Mt        0.034 Mt   100.000 % 	 ....../PrepareDrawBundle

		       2.855 Mt     0.098 %         1 x        2.855 Mt        0.000 Mt        0.001 Mt     0.035 % 	 ..../DeferredRLPrep

		       2.854 Mt    99.965 %         1 x        2.854 Mt        0.000 Mt        2.331 Mt    81.673 % 	 ...../PrepareForRendering

		       0.001 Mt     0.021 %         1 x        0.001 Mt        0.000 Mt        0.001 Mt   100.000 % 	 ....../PrepareMaterials

		       0.311 Mt    10.888 %         1 x        0.311 Mt        0.000 Mt        0.311 Mt   100.000 % 	 ....../BuildMaterialCache

		       0.212 Mt     7.418 %         1 x        0.212 Mt        0.000 Mt        0.212 Mt   100.000 % 	 ....../PrepareDrawBundle

		      26.826 Mt     0.925 %         1 x       26.826 Mt        0.000 Mt        2.710 Mt    10.103 % 	 ..../RayTracingRLPrep

		       0.172 Mt     0.643 %         1 x        0.172 Mt        0.000 Mt        0.172 Mt   100.000 % 	 ...../PrepareAccelerationStructures

		       0.003 Mt     0.010 %         1 x        0.003 Mt        0.000 Mt        0.003 Mt   100.000 % 	 ...../PrepareCache

		      13.296 Mt    49.564 %         1 x       13.296 Mt        0.000 Mt       13.296 Mt   100.000 % 	 ...../PipelineBuild

		       0.623 Mt     2.323 %         1 x        0.623 Mt        0.000 Mt        0.623 Mt   100.000 % 	 ...../BuildCache

		       5.020 Mt    18.713 %         1 x        5.020 Mt        0.000 Mt        0.000 Mt     0.008 % 	 ...../BuildAccelerationStructures

		       5.020 Mt    99.992 %         1 x        5.020 Mt        0.000 Mt        2.853 Mt    56.847 % 	 ....../AccelerationStructureBuild

		       1.413 Mt    28.146 %         1 x        1.413 Mt        0.000 Mt        1.413 Mt   100.000 % 	 ......./BuildBottomLevelAS

		       0.753 Mt    15.007 %         1 x        0.753 Mt        0.000 Mt        0.753 Mt   100.000 % 	 ......./BuildTopLevelAS

		       5.002 Mt    18.645 %         1 x        5.002 Mt        0.000 Mt        5.002 Mt   100.000 % 	 ...../BuildShaderTables

		      29.994 Mt     1.034 %         1 x       29.994 Mt        0.000 Mt       29.994 Mt   100.000 % 	 ..../GpuSidePrep

	WindowThread:

		   30002.874 Mt   100.000 %         1 x    30002.874 Mt    30002.874 Mt    30002.874 Mt   100.000 % 	 /

	GpuThread:

		   23238.621 Mt   100.000 %      2715 x        8.559 Mt        8.976 Mt      140.201 Mt     0.603 % 	 /

		       1.864 Mt     0.008 %         1 x        1.864 Mt        0.000 Mt        0.006 Mt     0.330 % 	 ./ScenePrep

		       1.858 Mt    99.670 %         1 x        1.858 Mt        0.000 Mt        0.001 Mt     0.067 % 	 ../AccelerationStructureBuild

		       1.769 Mt    95.232 %         1 x        1.769 Mt        0.000 Mt        1.769 Mt   100.000 % 	 .../BuildBottomLevelASGpu

		       0.087 Mt     4.701 %         1 x        0.087 Mt        0.000 Mt        0.087 Mt   100.000 % 	 .../BuildTopLevelASGpu

		   22989.511 Mt    98.928 %      2715 x        8.468 Mt        8.882 Mt        7.157 Mt     0.031 % 	 ./RayTracingGpu

		    8436.522 Mt    36.697 %      2715 x        3.107 Mt        3.778 Mt     8436.522 Mt   100.000 % 	 ../DeferredRLGpu

		    7898.247 Mt    34.356 %      2715 x        2.909 Mt        2.601 Mt     7898.247 Mt   100.000 % 	 ../RayTracingRLGpu

		    6647.586 Mt    28.916 %      2715 x        2.448 Mt        2.501 Mt     6647.586 Mt   100.000 % 	 ../ResolveRLGpu

		     107.044 Mt     0.461 %      2715 x        0.039 Mt        0.094 Mt      107.044 Mt   100.000 % 	 ./Present


	============================


