Configuration: 
Command line parameters  = E:\programming\FIT-DIP\Masters-DXR-Main-git\build\Measurement\x64\Release\Measurement.exe --scene Sponza/Sponza.gltf --duplication-cube --duplication-offset 0 --profile-triangles --profile-camera-track Sponza/SponzaFast.trk --profile-max-duplication 5 --rt-mode --width 2560 --height 1440 --profile-output TrianglesSponzaNoOffset1440Rt 
Build type               = Release
Device name              = NVIDIA GeForce RTX 2080 Ti
Device memory MB         = 11049
Ray Tracing backend      = Hardware
Window width             = 2560
Window height            = 1421
Window title             = Direct3D App
Back-buffer count        = 3
Fill Triangles           = enabled
Triangles Font Clock-Wise= enabled
Fast Build AS            = disabled
Target FPS               = 0
Target UPS               = 60
Tearing                  = enabled
VSync                    = disabled
GUI rendering            = disabled
Debug layer              = disabled
GPU validation           = disabled
GPU profiling            = enabled
Render mode              = Ray Tracing
Loaded scene file        = Sponza/Sponza.gltf
Using testing scene      = disabled
Duplication              = 5
Duplication in cube      = enabled
Duplication offset       = 0
BLAS duplication         = enabled
Rays per pixel           = 13
Quality preset           = High
Rasterization Quality    : 
  AO                     = enabled
  AO kernel size         = 4
  Shadows                = enabled
  Shadow map resolution  = 4096
  Shadow PCF kernel size = 4
  Reflections            = enabled
Ray Tracing Quality      : 
  AO                     = enabled
  AO rays                = 4
  AO kernel size         = 4
  Shadows                = enabled
  Shadow rays            = 4
  Shadow kernel size     = 4
  Reflections            = enabled

ProfilingAggregator: 
Render	,	9.4305	,	9.4305	,	10.1104	,	10.1104	,	9.5968	,	9.5968	,	9.8755	,	9.8755	,	11.0374	,	11.0374	,	10.4459	,	39.956	,	39.956	,	40.3924	,	40.3924	,	33.6056	,	33.6056	,	45.0467	,	45.0467	,	48.1547	,	48.1547	,	40.8558	,	121.478	,	121.478	,	120.15	,	120.15	,	97.6785	,	97.6785	,	171.263	,	171.263	,	141.19	,	141.19	,	127.476	,	309.764	,	309.764	,	255.661	,	255.661	,	265.273	,	265.273	,	403.543	,	403.543	,	302.419	,	519.166	,	519.166	,	781.077	,	781.077	,	666.045	,	666.045	,	621.452
Update	,	0.0667	,	0.0667	,	0.0553	,	0.0553	,	0.066	,	0.066	,	0.0665	,	0.0665	,	0.0668	,	0.0668	,	0.0647	,	0.0378	,	0.0378	,	0.0432	,	0.0432	,	0.0441	,	0.0441	,	0.0468	,	0.0468	,	0.0363	,	0.0363	,	0.0436	,	0.0337	,	0.0337	,	0.0337	,	0.0337	,	0.0338	,	0.0338	,	0.0335	,	0.0335	,	0.0336	,	0.0336	,	0.0368	,	0.0345	,	0.0345	,	0.0337	,	0.0337	,	0.0343	,	0.0343	,	0.0337	,	0.0337	,	0.0332	,	0.0348	,	0.0348	,	0.0345	,	0.0345	,	0.0338	,	0.0338	,	0.0344
RayTracing	,	0.1449	,	0.1449	,	0.15	,	0.15	,	0.1479	,	0.1479	,	0.1468	,	0.1468	,	0.1524	,	0.1524	,	0.1463	,	0.1615	,	0.1615	,	0.1654	,	0.1654	,	0.1607	,	0.1607	,	0.165	,	0.165	,	0.142	,	0.142	,	0.1685	,	0.1743	,	0.1743	,	0.1835	,	0.1835	,	0.1749	,	0.1749	,	0.169	,	0.169	,	0.168	,	0.168	,	0.1866	,	0.2023	,	0.2023	,	0.2013	,	0.2013	,	0.1844	,	0.1844	,	0.2102	,	0.2102	,	0.2075	,	0.2398	,	0.2398	,	0.2166	,	0.2166	,	0.2197	,	0.2197	,	0.2214
RayTracingGpu	,	8.83955	,	8.83955	,	9.54931	,	9.54931	,	9.00576	,	9.00576	,	9.35683	,	9.35683	,	10.4505	,	10.4505	,	9.86451	,	39.3366	,	39.3366	,	39.7674	,	39.7674	,	32.9643	,	32.9643	,	44.4384	,	44.4384	,	47.5564	,	47.5564	,	40.1868	,	120.852	,	120.852	,	119.448	,	119.448	,	97.0049	,	97.0049	,	170.606	,	170.606	,	140.511	,	140.511	,	126.729	,	309.024	,	309.024	,	254.879	,	254.879	,	264.592	,	264.592	,	402.763	,	402.763	,	301.674	,	518.329	,	518.329	,	780.239	,	780.239	,	665.221	,	665.221	,	620.628
DeferredRLGpu	,	2.41843	,	2.41843	,	3.40419	,	3.40419	,	2.73549	,	2.73549	,	3.31366	,	3.31366	,	4.1944	,	4.1944	,	3.29376	,	17.4725	,	17.4725	,	21.038	,	21.038	,	16.6344	,	16.6344	,	24.095	,	24.095	,	24.9798	,	24.9798	,	21.9582	,	62.11	,	62.11	,	67.322	,	67.322	,	49.9506	,	49.9506	,	78.636	,	78.636	,	81.4475	,	81.4475	,	72.0706	,	176.574	,	176.574	,	150.554	,	150.554	,	165.629	,	165.629	,	214.284	,	214.284	,	170.455	,	293.277	,	293.277	,	376.148	,	376.148	,	375.53	,	375.53	,	332.005
RayTracingRLGpu	,	3.97869	,	3.97869	,	3.76125	,	3.76125	,	3.89405	,	3.89405	,	3.63402	,	3.63402	,	3.9305	,	3.9305	,	4.16083	,	19.4267	,	19.4267	,	16.3577	,	16.3577	,	13.9694	,	13.9694	,	17.9361	,	17.9361	,	20.2484	,	20.2484	,	15.8245	,	56.317	,	56.317	,	49.7485	,	49.7485	,	44.667	,	44.667	,	89.6155	,	89.6155	,	56.692	,	56.692	,	52.2585	,	130.064	,	130.064	,	101.945	,	101.945	,	96.5387	,	96.5387	,	186.168	,	186.168	,	128.817	,	222.677	,	222.677	,	401.676	,	401.676	,	287.29	,	287.29	,	286.214
ResolveRLGpu	,	2.43974	,	2.43974	,	2.38134	,	2.38134	,	2.37219	,	2.37219	,	2.4065	,	2.4065	,	2.32221	,	2.32221	,	2.40742	,	2.4345	,	2.4345	,	2.36973	,	2.36973	,	2.3569	,	2.3569	,	2.404	,	2.404	,	2.32659	,	2.32659	,	2.40237	,	2.42317	,	2.42317	,	2.37542	,	2.37542	,	2.38493	,	2.38493	,	2.35283	,	2.35283	,	2.3695	,	2.3695	,	2.39782	,	2.38352	,	2.38352	,	2.37632	,	2.37632	,	2.4217	,	2.4217	,	2.30928	,	2.30928	,	2.40061	,	2.37229	,	2.37229	,	2.41302	,	2.41302	,	2.39936	,	2.39936	,	2.40707
Index	,	0	,	1	,	2	,	3	,	4	,	5	,	6	,	7	,	8	,	9	,	10	,	11	,	12	,	13	,	14	,	15	,	16	,	17	,	18	,	19	,	20	,	21	,	22	,	23	,	24	,	25	,	26	,	27	,	28	,	29	,	30	,	31	,	32	,	33	,	34	,	35	,	36	,	37	,	38	,	39	,	40	,	41	,	42	,	43	,	44	,	45	,	46	,	47	,	48

ProfilingBuildAggregator: 
BuildBottomLevelAS	,	1.6513	,	9.3385	,	31.4519	,	73.083	,	155.115
BuildBottomLevelASGpu	,	2.35738	,	14.1337	,	47.6308	,	113.689	,	221.478
BuildTopLevelAS	,	0.6943	,	0.9242	,	0.854	,	1.3916	,	1.2401
BuildTopLevelASGpu	,	0.088704	,	0.080928	,	0.167072	,	0.275456	,	0.349024
Duplication	,	1	,	2	,	3	,	4	,	5
Triangles	,	262267	,	2098136	,	7081209	,	16785088	,	32783375
Meshes	,	103	,	824	,	2781	,	6592	,	12875
BottomLevels	,	1	,	8	,	27	,	64	,	125
TopLevelSize	,	64	,	512	,	1728	,	4096	,	8000
BottomLevelsSize	,	18390656	,	147125248	,	496547712	,	1177001984	,	2298832000
Index	,	0	,	1	,	2	,	3	,	4

FpsAggregator: 
FPS	,	106	,	106	,	98	,	98	,	102	,	102	,	104	,	104	,	92	,	92	,	94	,	29	,	29	,	24	,	24	,	28	,	28	,	30	,	30	,	20	,	20	,	23	,	10	,	10	,	8	,	8	,	10	,	10	,	9	,	9	,	7	,	7	,	8	,	4	,	4	,	4	,	4	,	5	,	5	,	3	,	3	,	4	,	2	,	2	,	3	,	3	,	2	,	2	,	2
UPS	,	61	,	61	,	60	,	60	,	61	,	61	,	60	,	60	,	60	,	60	,	61	,	60	,	60	,	61	,	61	,	61	,	61	,	62	,	62	,	63	,	63	,	61	,	66	,	66	,	62	,	62	,	66	,	66	,	60	,	60	,	70	,	70	,	65	,	59	,	59	,	72	,	72	,	71	,	71	,	65	,	65	,	83	,	77	,	77	,	89	,	89	,	99	,	99	,	78
FrameTime	,	9.51371	,	9.51371	,	10.2775	,	10.2775	,	9.84156	,	9.84156	,	9.67077	,	9.67077	,	10.9119	,	10.9119	,	10.7238	,	34.9449	,	34.9449	,	42.2262	,	42.2262	,	36.2957	,	36.2957	,	34.7617	,	34.7617	,	52.3463	,	52.3463	,	43.8588	,	109.098	,	109.098	,	130.011	,	130.011	,	107.523	,	107.523	,	119.726	,	119.726	,	162.568	,	162.568	,	132.349	,	271.467	,	271.467	,	284.983	,	284.983	,	241.283	,	241.283	,	402.805	,	402.805	,	323.913	,	585.455	,	585.455	,	582.811	,	582.811	,	764.959	,	764.959	,	627.262
GigaRays/s	,	4.97082	,	4.97082	,	4.60138	,	4.60138	,	4.80522	,	4.80522	,	4.89009	,	4.89009	,	4.33386	,	4.33386	,	4.4099	,	1.3533	,	1.3533	,	1.11994	,	1.11994	,	1.30293	,	1.30293	,	1.36043	,	1.36043	,	0.903423	,	0.903423	,	1.07825	,	0.433471	,	0.433471	,	0.363744	,	0.363744	,	0.439822	,	0.439822	,	0.394993	,	0.394993	,	0.290899	,	0.290899	,	0.357318	,	0.174205	,	0.174205	,	0.165943	,	0.165943	,	0.195998	,	0.195998	,	0.117404	,	0.117404	,	0.145999	,	0.0807762	,	0.0807762	,	0.0811427	,	0.0811427	,	0.0618214	,	0.0618214	,	0.0753926
Duplication	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	2	,	2	,	2	,	2	,	2	,	2	,	2	,	2	,	2	,	2	,	2	,	3	,	3	,	3	,	3	,	3	,	3	,	3	,	3	,	3	,	3	,	3	,	4	,	4	,	4	,	4	,	4	,	4	,	4	,	4	,	4	,	5	,	5	,	5	,	5	,	5	,	5	,	5
Index	,	0	,	1	,	2	,	3	,	4	,	5	,	6	,	7	,	8	,	9	,	10	,	11	,	12	,	13	,	14	,	15	,	16	,	17	,	18	,	19	,	20	,	21	,	22	,	23	,	24	,	25	,	26	,	27	,	28	,	29	,	30	,	31	,	32	,	33	,	34	,	35	,	36	,	37	,	38	,	39	,	40	,	41	,	42	,	43	,	44	,	45	,	46	,	47	,	48


AccelerationStructure: 
Ray tracing acceleration structure building statistics: 
	Bottom Level Acceleration Structure: 
		Maximum [B]: 18390656
		Minimum [B]: 18390656
		Currently Allocated [B]: 2298832000
		Currently Created [num]: 125
		Current Average [B]: 18390656
		Maximum Triangles [num]: 262267
		Minimum Triangles [num]: 262267
		Total Triangles [num]: 32783375
		Maximum Meshes [num]: 103
		Minimum Meshes [num]: 103
		Total Meshes [num]: 12875
	Top Level Acceleration Structure: 
		Maximum [B]: 8000
		Minimum [B]: 8000
		Currently Allocated [B]: 8000
		Total Allocated [B]: 8000
		Total Created [num]: 1
		Average Allocated [B]: 8000
		Maximum Instances [num]: 125
		Minimum Instances [num]: 125
		Total Instances [num]: 125
	Scratch Buffer: 
		Maximum [B]: 14995072
		Minimum [B]: 14995072
		Currently Allocated [B]: 14995072
		Total Allocated [B]: 14995072
		Total Created [num]: 1
		Average Allocated [B]: 14995072
ProfilingStack: 
[PROF] Profiling statistics: 
	Scopes entered : 104556
	Scopes exited : 104555
	Overhead per scope [ticks] : 102.9
	Threads entered : 3
	Threads exited : 0
	============================
		         Sum[Mt]   Parent[%]  Samples[num]       Avg[Mt]   Last/Curr[Mt]        Self[Mt]     Self[%] 	 Name

	main:

		   50903.842 Mt   100.000 %         1 x    50903.843 Mt    50903.842 Mt        0.258 Mt     0.001 % 	 /

		   50903.615 Mt   100.000 %         1 x    50903.616 Mt    50903.615 Mt      200.748 Mt     0.394 % 	 ./Main application loop

		       1.878 Mt     0.004 %         1 x        1.878 Mt        0.000 Mt        1.878 Mt   100.000 % 	 ../TexturedRLInitialization

		       0.956 Mt     0.002 %         1 x        0.956 Mt        0.000 Mt        0.956 Mt   100.000 % 	 ../ShadowMapRLInitialization

		       2.951 Mt     0.006 %         1 x        2.951 Mt        0.000 Mt        2.951 Mt   100.000 % 	 ../RasterResolveRLInitialization

		       3.604 Mt     0.007 %         1 x        3.604 Mt        0.000 Mt        3.604 Mt   100.000 % 	 ../ResolveRLInitialization

		   47883.356 Mt    94.067 %     86190 x        0.556 Mt      637.923 Mt      589.204 Mt     1.230 % 	 ../MainLoop

		     202.518 Mt     0.423 %      2834 x        0.071 Mt        0.021 Mt      202.077 Mt    99.782 % 	 .../Update

		       0.441 Mt     0.218 %         1 x        0.441 Mt        0.000 Mt        0.441 Mt   100.000 % 	 ..../GuiModelApply

		   47091.633 Mt    98.347 %      1180 x       39.908 Mt      624.183 Mt       97.536 Mt     0.207 % 	 .../Render

		     820.293 Mt     1.742 %      1180 x        0.695 Mt        0.220 Mt        5.832 Mt     0.711 % 	 ..../RayTracing

		     148.463 Mt    18.099 %      1180 x        0.126 Mt        0.106 Mt       77.532 Mt    52.224 % 	 ...../Deferred

		      70.930 Mt    47.776 %      1180 x        0.060 Mt        0.000 Mt        0.599 Mt     0.845 % 	 ....../DeferredRLPrep

		      70.331 Mt    99.155 %         5 x       14.066 Mt        0.000 Mt       31.218 Mt    44.387 % 	 ......./PrepareForRendering

		       0.031 Mt     0.044 %         5 x        0.006 Mt        0.000 Mt        0.031 Mt   100.000 % 	 ......../PrepareMaterials

		       0.001 Mt     0.002 %         5 x        0.000 Mt        0.000 Mt        0.001 Mt   100.000 % 	 ......../BuildMaterialCache

		      39.081 Mt    55.567 %         5 x        7.816 Mt        0.000 Mt       39.081 Mt   100.000 % 	 ......../PrepareDrawBundle

		     634.140 Mt    77.306 %      1180 x        0.537 Mt        0.073 Mt       60.142 Mt     9.484 % 	 ...../RayCasting

		     573.997 Mt    90.516 %      1180 x        0.486 Mt        0.000 Mt       15.620 Mt     2.721 % 	 ....../RayTracingRLPrep

		      62.732 Mt    10.929 %         5 x       12.546 Mt        0.000 Mt       62.732 Mt   100.000 % 	 ......./PrepareAccelerationStructures

		       0.025 Mt     0.004 %         5 x        0.005 Mt        0.000 Mt        0.025 Mt   100.000 % 	 ......./PrepareCache

		     127.706 Mt    22.249 %         5 x       25.541 Mt        0.000 Mt      127.706 Mt   100.000 % 	 ......./PipelineBuild

		       0.001 Mt     0.000 %         5 x        0.000 Mt        0.000 Mt        0.001 Mt   100.000 % 	 ......./BuildCache

		     308.338 Mt    53.718 %         5 x       61.668 Mt        0.000 Mt        0.003 Mt     0.001 % 	 ......./BuildAccelerationStructures

		     308.335 Mt    99.999 %         5 x       61.667 Mt        0.000 Mt       23.851 Mt     7.735 % 	 ......../AccelerationStructureBuild

		     279.145 Mt    90.533 %         5 x       55.829 Mt        0.000 Mt      279.145 Mt   100.000 % 	 ........./BuildBottomLevelAS

		       5.339 Mt     1.732 %         5 x        1.068 Mt        0.000 Mt        5.339 Mt   100.000 % 	 ........./BuildTopLevelAS

		      59.575 Mt    10.379 %         5 x       11.915 Mt        0.000 Mt       59.575 Mt   100.000 % 	 ......./BuildShaderTables

		      31.859 Mt     3.884 %      1180 x        0.027 Mt        0.034 Mt       31.859 Mt   100.000 % 	 ...../Resolve

		   46173.804 Mt    98.051 %      1180 x       39.130 Mt      623.831 Mt    46173.804 Mt   100.000 % 	 ..../Present

		       2.239 Mt     0.004 %         1 x        2.239 Mt        0.000 Mt        2.239 Mt   100.000 % 	 ../AmbientOcclusionRLInitialization

		       3.580 Mt     0.007 %         1 x        3.580 Mt        0.000 Mt        3.580 Mt   100.000 % 	 ../DeferredRLInitialization

		      43.961 Mt     0.086 %         1 x       43.961 Mt        0.000 Mt        2.055 Mt     4.675 % 	 ../RayTracingRLInitialization

		      41.906 Mt    95.325 %         1 x       41.906 Mt        0.000 Mt       41.906 Mt   100.000 % 	 .../PipelineBuild

		    2760.342 Mt     5.423 %         1 x     2760.342 Mt        0.000 Mt        0.287 Mt     0.010 % 	 ../Initialization

		    2760.055 Mt    99.990 %         1 x     2760.055 Mt        0.000 Mt        1.450 Mt     0.053 % 	 .../ScenePrep

		    2700.642 Mt    97.847 %         1 x     2700.642 Mt        0.000 Mt      234.955 Mt     8.700 % 	 ..../SceneLoad

		    2127.624 Mt    78.782 %         1 x     2127.624 Mt        0.000 Mt      583.877 Mt    27.443 % 	 ...../glTF-LoadScene

		    1543.747 Mt    72.557 %        69 x       22.373 Mt        0.000 Mt     1543.747 Mt   100.000 % 	 ....../glTF-LoadImageData

		     277.609 Mt    10.279 %         1 x      277.609 Mt        0.000 Mt      277.609 Mt   100.000 % 	 ...../glTF-CreateScene

		      60.454 Mt     2.239 %         1 x       60.454 Mt        0.000 Mt       60.454 Mt   100.000 % 	 ...../SceneDesc-CreateScene

		       0.764 Mt     0.028 %         1 x        0.764 Mt        0.000 Mt        0.000 Mt     0.065 % 	 ..../ShadowMapRLPrep

		       0.764 Mt    99.935 %         1 x        0.764 Mt        0.000 Mt        0.722 Mt    94.580 % 	 ...../PrepareForRendering

		       0.041 Mt     5.420 %         1 x        0.041 Mt        0.000 Mt        0.041 Mt   100.000 % 	 ....../PrepareDrawBundle

		       3.167 Mt     0.115 %         1 x        3.167 Mt        0.000 Mt        0.000 Mt     0.013 % 	 ..../TexturedRLPrep

		       3.167 Mt    99.987 %         1 x        3.167 Mt        0.000 Mt        3.130 Mt    98.854 % 	 ...../PrepareForRendering

		       0.001 Mt     0.041 %         1 x        0.001 Mt        0.000 Mt        0.001 Mt   100.000 % 	 ....../PrepareMaterials

		       0.035 Mt     1.105 %         1 x        0.035 Mt        0.000 Mt        0.035 Mt   100.000 % 	 ....../PrepareDrawBundle

		       2.484 Mt     0.090 %         1 x        2.484 Mt        0.000 Mt        0.000 Mt     0.020 % 	 ..../DeferredRLPrep

		       2.483 Mt    99.980 %         1 x        2.483 Mt        0.000 Mt        2.084 Mt    83.916 % 	 ...../PrepareForRendering

		       0.001 Mt     0.024 %         1 x        0.001 Mt        0.000 Mt        0.001 Mt   100.000 % 	 ....../PrepareMaterials

		       0.321 Mt    12.915 %         1 x        0.321 Mt        0.000 Mt        0.321 Mt   100.000 % 	 ....../BuildMaterialCache

		       0.078 Mt     3.145 %         1 x        0.078 Mt        0.000 Mt        0.078 Mt   100.000 % 	 ....../PrepareDrawBundle

		      29.209 Mt     1.058 %         1 x       29.209 Mt        0.000 Mt        1.924 Mt     6.586 % 	 ..../RayTracingRLPrep

		       0.077 Mt     0.263 %         1 x        0.077 Mt        0.000 Mt        0.077 Mt   100.000 % 	 ...../PrepareAccelerationStructures

		       0.001 Mt     0.003 %         1 x        0.001 Mt        0.000 Mt        0.001 Mt   100.000 % 	 ...../PrepareCache

		      16.788 Mt    57.477 %         1 x       16.788 Mt        0.000 Mt       16.788 Mt   100.000 % 	 ...../PipelineBuild

		       0.778 Mt     2.664 %         1 x        0.778 Mt        0.000 Mt        0.778 Mt   100.000 % 	 ...../BuildCache

		       5.556 Mt    19.022 %         1 x        5.556 Mt        0.000 Mt        0.000 Mt     0.009 % 	 ...../BuildAccelerationStructures

		       5.556 Mt    99.991 %         1 x        5.556 Mt        0.000 Mt        3.210 Mt    57.780 % 	 ....../AccelerationStructureBuild

		       1.651 Mt    29.723 %         1 x        1.651 Mt        0.000 Mt        1.651 Mt   100.000 % 	 ......./BuildBottomLevelAS

		       0.694 Mt    12.497 %         1 x        0.694 Mt        0.000 Mt        0.694 Mt   100.000 % 	 ......./BuildTopLevelAS

		       4.085 Mt    13.985 %         1 x        4.085 Mt        0.000 Mt        4.085 Mt   100.000 % 	 ...../BuildShaderTables

		      22.339 Mt     0.809 %         1 x       22.339 Mt        0.000 Mt       22.339 Mt   100.000 % 	 ..../GpuSidePrep

	WindowThread:

		   50903.380 Mt   100.000 %         1 x    50903.380 Mt    50903.380 Mt    50903.380 Mt   100.000 % 	 /

	GpuThread:

		   45877.310 Mt   100.000 %      1180 x       38.879 Mt      623.374 Mt      115.076 Mt     0.251 % 	 /

		       2.457 Mt     0.005 %         1 x        2.457 Mt        0.000 Mt        0.009 Mt     0.350 % 	 ./ScenePrep

		       2.448 Mt    99.650 %         1 x        2.448 Mt        0.000 Mt        0.002 Mt     0.085 % 	 ../AccelerationStructureBuild

		       2.357 Mt    96.292 %         1 x        2.357 Mt        0.000 Mt        2.357 Mt   100.000 % 	 .../BuildBottomLevelASGpu

		       0.089 Mt     3.623 %         1 x        0.089 Mt        0.000 Mt        0.089 Mt   100.000 % 	 .../BuildTopLevelASGpu

		   45756.981 Mt    99.738 %      1180 x       38.777 Mt      623.287 Mt        2.805 Mt     0.006 % 	 ./RayTracingGpu

		   21384.715 Mt    46.735 %      1180 x       18.123 Mt      334.775 Mt    21384.715 Mt   100.000 % 	 ../DeferredRLGpu

		   21114.531 Mt    46.145 %      1180 x       17.894 Mt      286.025 Mt    21114.531 Mt   100.000 % 	 ../RayTracingRLGpu

		    2842.890 Mt     6.213 %      1180 x        2.409 Mt        2.486 Mt     2842.890 Mt   100.000 % 	 ../ResolveRLGpu

		     412.040 Mt     0.900 %         5 x       82.408 Mt        0.000 Mt        0.009 Mt     0.002 % 	 ../AccelerationStructureBuild

		     411.076 Mt    99.766 %         5 x       82.215 Mt        0.000 Mt      411.076 Mt   100.000 % 	 .../BuildBottomLevelASGpu

		       0.954 Mt     0.232 %         5 x        0.191 Mt        0.000 Mt        0.954 Mt   100.000 % 	 .../BuildTopLevelASGpu

		       2.797 Mt     0.006 %      1180 x        0.002 Mt        0.087 Mt        2.797 Mt   100.000 % 	 ./Present


	============================


