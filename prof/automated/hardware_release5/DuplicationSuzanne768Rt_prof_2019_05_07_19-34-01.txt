Configuration: 
Command line parameters  = E:\programming\FIT-DIP\Masters-DXR-Main-git\build\Measurement\x64\Release\Measurement.exe --scene Suzanne/Suzanne.gltf --duplication-cube --profile-duplication --profile-max-duplication 10 --rt-mode --width 1024 --height 768 --profile-output DuplicationSuzanne768Rt 
Build type               = Release
Device name              = NVIDIA GeForce RTX 2080 Ti
Device memory MB         = 11049
Ray Tracing backend      = Hardware
Window width             = 1024
Window height            = 768
Window title             = Direct3D App
Back-buffer count        = 3
Fill Triangles           = enabled
Triangles Font Clock-Wise= enabled
Fast Build AS            = disabled
Target FPS               = 60
Target UPS               = 60
Tearing                  = disabled
VSync                    = disabled
GUI rendering            = disabled
Debug layer              = disabled
GPU validation           = disabled
GPU profiling            = enabled
Render mode              = Ray Tracing
Loaded scene file        = Suzanne/Suzanne.gltf
Using testing scene      = disabled
Duplication              = 10
Duplication in cube      = enabled
Duplication offset       = 10
BLAS duplication         = enabled
Rays per pixel           = 13
Quality preset           = High
Rasterization Quality    : 
  AO                     = enabled
  AO kernel size         = 4
  Shadows                = enabled
  Shadow map resolution  = 4096
  Shadow PCF kernel size = 4
  Reflections            = enabled
Ray Tracing Quality      : 
  AO                     = enabled
  AO rays                = 4
  AO kernel size         = 4
  Shadows                = enabled
  Shadow rays            = 4
  Shadow kernel size     = 4
  Reflections            = enabled

ProfilingAggregator: 
Render	,	1.1534	,	1.1508	,	1.195	,	1.1718	,	1.1876	,	1.1568	,	1.2899	,	1.21	,	1.0481	,	1.3197	,	1.2333	,	1.4699	,	1.1612	,	1.1184	,	1.2624	,	1.3161	,	603.024	,	1.5028	,	1.7342	,	1.765
Update	,	0.04	,	0.0283	,	0.0232	,	0.0183	,	0.0189	,	0.0287	,	0.0207	,	0.0333	,	0.0298	,	0.0341	,	0.0375	,	0.0427	,	0.0274	,	0.0269	,	0.0231	,	0.0246	,	0.0324	,	0.0231	,	0.0626	,	0.0328
RayTracing	,	0.0824	,	0.0755	,	0.0675	,	0.0786	,	0.0825	,	0.0844	,	0.0868	,	0.077	,	0.0906	,	0.0872	,	0.0857	,	0.0975	,	0.088	,	0.0547	,	0.0533	,	0.0683	,	292.264	,	0.0581	,	0.0902	,	0.0576
RayTracingGpu	,	0.725312	,	0.722208	,	0.745216	,	0.743136	,	0.765088	,	0.769088	,	0.818304	,	0.812064	,	0.666944	,	0.886304	,	0.711392	,	0.710816	,	0.80832	,	0.809664	,	0.943616	,	0.939424	,	309.757	,	1.10234	,	1.30566	,	1.33062
DeferredRLGpu	,	0.014656	,	0.01488	,	0.02192	,	0.019776	,	0.028576	,	0.02832	,	0.062656	,	0.062752	,	0.08496	,	0.11712	,	0.138912	,	0.137824	,	0.218272	,	0.219872	,	0.327008	,	0.327904	,	0.468672	,	0.464512	,	0.634592	,	0.637888
RayTracingRLGpu	,	0.085504	,	0.081344	,	0.093344	,	0.096352	,	0.105408	,	0.108704	,	0.12432	,	0.11904	,	0.107904	,	0.139232	,	0.117792	,	0.118848	,	0.137984	,	0.137888	,	0.160064	,	0.157312	,	0.187776	,	0.183072	,	0.215168	,	0.226944
ResolveRLGpu	,	0.622848	,	0.623936	,	0.627776	,	0.624512	,	0.625984	,	0.62736	,	0.627232	,	0.626304	,	0.47184	,	0.628	,	0.451232	,	0.450976	,	0.44976	,	0.45008	,	0.4536	,	0.451872	,	0.453728	,	0.45168	,	0.453632	,	0.463232
Index	,	0	,	1	,	2	,	3	,	4	,	5	,	6	,	7	,	8	,	9	,	10	,	11	,	12	,	13	,	14	,	15	,	16	,	17	,	18	,	19

ProfilingBuildAggregator: 
BuildBottomLevelAS	,	1.2116	,	6.9267	,	14.9145	,	29.5961	,	48.1097	,	72.9702	,	100.348	,	147.042	,	216.013
BuildBottomLevelASGpu	,	0.63424	,	16.5825	,	40.595	,	75.581	,	91.8901	,	147.017	,	217.454	,	308.462	,	422.238
BuildTopLevelAS	,	0.9428	,	0.8113	,	1.0283	,	0.4999	,	0.6088	,	0.5127	,	0.7199	,	0.5946	,	0.6432
BuildTopLevelASGpu	,	0.085536	,	0.14528	,	0.140832	,	0.132064	,	0.148768	,	0.155136	,	0.134016	,	0.178656	,	0.189952
Duplication	,	1	,	3	,	4	,	5	,	6	,	7	,	8	,	9	,	10
Triangles	,	3936	,	106272	,	251904	,	492000	,	850176	,	1350048	,	2015232	,	2869344	,	3936000
Meshes	,	1	,	27	,	64	,	125	,	216	,	343	,	512	,	729	,	1000
BottomLevels	,	1	,	27	,	64	,	125	,	216	,	343	,	512	,	729	,	1000
TopLevelSize	,	64	,	1728	,	4096	,	8000	,	13824	,	21952	,	32768	,	46656	,	64000
BottomLevelsSize	,	282112	,	7617024	,	18055168	,	35264000	,	60936192	,	96764416	,	144441344	,	205659648	,	282112000
Index	,	0	,	1	,	2	,	3	,	4	,	5	,	6	,	7	,	8

FpsAggregator: 
FPS	,	54	,	56	,	56	,	58	,	54	,	58	,	49	,	58	,	42	,	59	,	34	,	58	,	24	,	58	,	7	,	58	,	3	,	59	,	14	,	59
UPS	,	59	,	60	,	61	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	35	,	96	,	60	,	60
FrameTime	,	18.5185	,	17.8571	,	17.8576	,	17.2414	,	18.5185	,	17.2415	,	20.4082	,	17.2415	,	23.8095	,	16.9492	,	29.4119	,	17.2415	,	41.6667	,	17.2414	,	142.857	,	17.2414	,	399.446	,	16.9705	,	71.4286	,	16.9492
GigaRays/s	,	0.552075	,	0.572522	,	0.572509	,	0.59297	,	0.552075	,	0.592966	,	0.500957	,	0.592967	,	0.429392	,	0.603193	,	0.347602	,	0.592967	,	0.245367	,	0.59297	,	0.0715653	,	0.59297	,	0.0255945	,	0.602433	,	0.143131	,	0.603193
Index	,	0	,	1	,	2	,	3	,	4	,	5	,	6	,	7	,	8	,	9	,	10	,	11	,	12	,	13	,	14	,	15	,	16	,	17	,	18	,	19


AccelerationStructure: 
Ray tracing acceleration structure building statistics: 
	Bottom Level Acceleration Structure: 
		Maximum [B]: 282112
		Minimum [B]: 282112
		Currently Allocated [B]: 282112000
		Currently Created [num]: 1000
		Current Average [B]: 282112
		Maximum Triangles [num]: 3936
		Minimum Triangles [num]: 3936
		Total Triangles [num]: 3936000
		Maximum Meshes [num]: 1
		Minimum Meshes [num]: 1
		Total Meshes [num]: 1000
	Top Level Acceleration Structure: 
		Maximum [B]: 64000
		Minimum [B]: 64000
		Currently Allocated [B]: 64000
		Total Allocated [B]: 64000
		Total Created [num]: 1
		Average Allocated [B]: 64000
		Maximum Instances [num]: 1000
		Minimum Instances [num]: 1000
		Total Instances [num]: 1000
	Scratch Buffer: 
		Maximum [B]: 82944
		Minimum [B]: 82944
		Currently Allocated [B]: 82944
		Total Allocated [B]: 82944
		Total Created [num]: 1
		Average Allocated [B]: 82944
ProfilingStack: 
[PROF] Profiling statistics: 
	Scopes entered : 72380591
	Scopes exited : 72380590
	Overhead per scope [ticks] : 103.5
	Threads entered : 3
	Threads exited : 0
	============================
		         Sum[Mt]   Parent[%]  Samples[num]       Avg[Mt]   Last/Curr[Mt]        Self[Mt]     Self[%] 	 Name

	main:

		   23205.048 Mt   100.000 %         1 x    23205.049 Mt    23205.048 Mt        0.404 Mt     0.002 % 	 /

		   23204.653 Mt    99.998 %         1 x    23204.654 Mt    23204.653 Mt     3343.206 Mt    14.407 % 	 ./Main application loop

		       2.138 Mt     0.009 %         1 x        2.138 Mt        0.000 Mt        2.138 Mt   100.000 % 	 ../TexturedRLInitialization

		       1.216 Mt     0.005 %         1 x        1.216 Mt        0.000 Mt        1.216 Mt   100.000 % 	 ../ShadowMapRLInitialization

		       4.810 Mt     0.021 %         1 x        4.810 Mt        0.000 Mt        4.810 Mt   100.000 % 	 ../RasterResolveRLInitialization

		       3.529 Mt     0.015 %         1 x        3.529 Mt        0.000 Mt        3.529 Mt   100.000 % 	 ../ResolveRLInitialization

		   19612.879 Mt    84.521 %  72367130 x        0.000 Mt        2.022 Mt    13753.239 Mt    70.124 % 	 ../MainLoop

		     131.045 Mt     0.668 %      1213 x        0.108 Mt        0.028 Mt      130.782 Mt    99.800 % 	 .../Update

		       0.263 Mt     0.200 %         1 x        0.263 Mt        0.000 Mt        0.263 Mt   100.000 % 	 ..../GuiModelApply

		    5728.595 Mt    29.208 %       919 x        6.234 Mt        1.908 Mt       43.918 Mt     0.767 % 	 .../Render

		    2264.792 Mt    39.535 %       919 x        2.464 Mt        0.135 Mt        2.297 Mt     0.101 % 	 ..../RayTracing

		      46.945 Mt     2.073 %       919 x        0.051 Mt        0.065 Mt       32.556 Mt    69.350 % 	 ...../Deferred

		      14.389 Mt    30.650 %       919 x        0.016 Mt        0.000 Mt        0.262 Mt     1.819 % 	 ....../DeferredRLPrep

		      14.127 Mt    98.181 %        16 x        0.883 Mt        0.000 Mt        9.270 Mt    65.622 % 	 ......./PrepareForRendering

		       0.339 Mt     2.399 %        16 x        0.021 Mt        0.000 Mt        0.339 Mt   100.000 % 	 ......../PrepareMaterials

		       0.002 Mt     0.016 %        16 x        0.000 Mt        0.000 Mt        0.002 Mt   100.000 % 	 ......../BuildMaterialCache

		       4.515 Mt    31.963 %        16 x        0.282 Mt        0.000 Mt        4.515 Mt   100.000 % 	 ......../PrepareDrawBundle

		    2203.335 Mt    97.286 %       919 x        2.398 Mt        0.046 Mt       29.336 Mt     1.331 % 	 ...../RayCasting

		    2173.999 Mt    98.669 %       919 x        2.366 Mt        0.000 Mt       29.296 Mt     1.348 % 	 ....../RayTracingRLPrep

		     587.025 Mt    27.002 %        16 x       36.689 Mt        0.000 Mt      587.025 Mt   100.000 % 	 ......./PrepareAccelerationStructures

		       0.189 Mt     0.009 %        16 x        0.012 Mt        0.000 Mt        0.189 Mt   100.000 % 	 ......./PrepareCache

		     356.356 Mt    16.392 %        16 x       22.272 Mt        0.000 Mt      356.356 Mt   100.000 % 	 ......./PipelineBuild

		       0.003 Mt     0.000 %        16 x        0.000 Mt        0.000 Mt        0.003 Mt   100.000 % 	 ......./BuildCache

		    1167.411 Mt    53.699 %        16 x       72.963 Mt        0.000 Mt        0.012 Mt     0.001 % 	 ......./BuildAccelerationStructures

		    1167.399 Mt    99.999 %        16 x       72.962 Mt        0.000 Mt       71.355 Mt     6.112 % 	 ......../AccelerationStructureBuild

		    1084.520 Mt    92.901 %        16 x       67.782 Mt        0.000 Mt     1084.520 Mt   100.000 % 	 ........./BuildBottomLevelAS

		      11.525 Mt     0.987 %        16 x        0.720 Mt        0.000 Mt       11.525 Mt   100.000 % 	 ........./BuildTopLevelAS

		      33.718 Mt     1.551 %        16 x        2.107 Mt        0.000 Mt       33.718 Mt   100.000 % 	 ......./BuildShaderTables

		      12.216 Mt     0.539 %       919 x        0.013 Mt        0.020 Mt       12.216 Mt   100.000 % 	 ...../Resolve

		    3419.885 Mt    59.698 %       919 x        3.721 Mt        1.684 Mt     3419.885 Mt   100.000 % 	 ..../Present

		       2.698 Mt     0.012 %         1 x        2.698 Mt        0.000 Mt        2.698 Mt   100.000 % 	 ../AmbientOcclusionRLInitialization

		       5.133 Mt     0.022 %         1 x        5.133 Mt        0.000 Mt        5.133 Mt   100.000 % 	 ../DeferredRLInitialization

		      70.937 Mt     0.306 %         1 x       70.937 Mt        0.000 Mt        2.990 Mt     4.215 % 	 ../RayTracingRLInitialization

		      67.947 Mt    95.785 %         1 x       67.947 Mt        0.000 Mt       67.947 Mt   100.000 % 	 .../PipelineBuild

		     158.106 Mt     0.681 %         1 x      158.106 Mt        0.000 Mt        0.534 Mt     0.338 % 	 ../Initialization

		     157.572 Mt    99.662 %         1 x      157.572 Mt        0.000 Mt        1.627 Mt     1.033 % 	 .../ScenePrep

		      75.419 Mt    47.863 %         1 x       75.419 Mt        0.000 Mt       18.069 Mt    23.957 % 	 ..../SceneLoad

		      40.374 Mt    53.533 %         1 x       40.374 Mt        0.000 Mt        9.187 Mt    22.756 % 	 ...../glTF-LoadScene

		      31.186 Mt    77.244 %         2 x       15.593 Mt        0.000 Mt       31.186 Mt   100.000 % 	 ....../glTF-LoadImageData

		       8.528 Mt    11.308 %         1 x        8.528 Mt        0.000 Mt        8.528 Mt   100.000 % 	 ...../glTF-CreateScene

		       8.448 Mt    11.202 %         1 x        8.448 Mt        0.000 Mt        8.448 Mt   100.000 % 	 ...../SceneDesc-CreateScene

		       0.757 Mt     0.481 %         1 x        0.757 Mt        0.000 Mt        0.001 Mt     0.079 % 	 ..../ShadowMapRLPrep

		       0.757 Mt    99.921 %         1 x        0.757 Mt        0.000 Mt        0.748 Mt    98.837 % 	 ...../PrepareForRendering

		       0.009 Mt     1.163 %         1 x        0.009 Mt        0.000 Mt        0.009 Mt   100.000 % 	 ....../PrepareDrawBundle

		       3.182 Mt     2.019 %         1 x        3.182 Mt        0.000 Mt        0.000 Mt     0.009 % 	 ..../TexturedRLPrep

		       3.181 Mt    99.991 %         1 x        3.181 Mt        0.000 Mt        3.174 Mt    99.780 % 	 ...../PrepareForRendering

		       0.003 Mt     0.094 %         1 x        0.003 Mt        0.000 Mt        0.003 Mt   100.000 % 	 ....../PrepareMaterials

		       0.004 Mt     0.126 %         1 x        0.004 Mt        0.000 Mt        0.004 Mt   100.000 % 	 ....../PrepareDrawBundle

		       4.528 Mt     2.874 %         1 x        4.528 Mt        0.000 Mt        0.001 Mt     0.015 % 	 ..../DeferredRLPrep

		       4.528 Mt    99.985 %         1 x        4.528 Mt        0.000 Mt        2.098 Mt    46.340 % 	 ...../PrepareForRendering

		       0.001 Mt     0.013 %         1 x        0.001 Mt        0.000 Mt        0.001 Mt   100.000 % 	 ....../PrepareMaterials

		       2.424 Mt    53.532 %         1 x        2.424 Mt        0.000 Mt        2.424 Mt   100.000 % 	 ....../BuildMaterialCache

		       0.005 Mt     0.115 %         1 x        0.005 Mt        0.000 Mt        0.005 Mt   100.000 % 	 ....../PrepareDrawBundle

		      57.726 Mt    36.634 %         1 x       57.726 Mt        0.000 Mt        3.320 Mt     5.751 % 	 ..../RayTracingRLPrep

		       0.071 Mt     0.124 %         1 x        0.071 Mt        0.000 Mt        0.071 Mt   100.000 % 	 ...../PrepareAccelerationStructures

		       0.001 Mt     0.002 %         1 x        0.001 Mt        0.000 Mt        0.001 Mt   100.000 % 	 ...../PrepareCache

		      40.279 Mt    69.778 %         1 x       40.279 Mt        0.000 Mt       40.279 Mt   100.000 % 	 ...../PipelineBuild

		       0.837 Mt     1.450 %         1 x        0.837 Mt        0.000 Mt        0.837 Mt   100.000 % 	 ...../BuildCache

		       7.147 Mt    12.381 %         1 x        7.147 Mt        0.000 Mt        0.001 Mt     0.014 % 	 ...../BuildAccelerationStructures

		       7.146 Mt    99.986 %         1 x        7.146 Mt        0.000 Mt        4.992 Mt    69.851 % 	 ....../AccelerationStructureBuild

		       1.212 Mt    16.955 %         1 x        1.212 Mt        0.000 Mt        1.212 Mt   100.000 % 	 ......./BuildBottomLevelAS

		       0.943 Mt    13.194 %         1 x        0.943 Mt        0.000 Mt        0.943 Mt   100.000 % 	 ......./BuildTopLevelAS

		       6.070 Mt    10.515 %         1 x        6.070 Mt        0.000 Mt        6.070 Mt   100.000 % 	 ...../BuildShaderTables

		      14.333 Mt     9.096 %         1 x       14.333 Mt        0.000 Mt       14.333 Mt   100.000 % 	 ..../GpuSidePrep

	WindowThread:

		   23196.639 Mt   100.000 %         1 x    23196.639 Mt    23196.639 Mt    23196.639 Mt   100.000 % 	 /

	GpuThread:

		    3233.998 Mt   100.000 %       919 x        3.519 Mt        1.311 Mt       93.469 Mt     2.890 % 	 /

		       0.727 Mt     0.022 %         1 x        0.727 Mt        0.000 Mt        0.006 Mt     0.876 % 	 ./ScenePrep

		       0.721 Mt    99.124 %         1 x        0.721 Mt        0.000 Mt        0.001 Mt     0.151 % 	 ../AccelerationStructureBuild

		       0.634 Mt    87.983 %         1 x        0.634 Mt        0.000 Mt        0.634 Mt   100.000 % 	 .../BuildBottomLevelASGpu

		       0.086 Mt    11.866 %         1 x        0.086 Mt        0.000 Mt        0.086 Mt   100.000 % 	 .../BuildTopLevelASGpu

		    3137.992 Mt    97.031 %       919 x        3.415 Mt        1.309 Mt        2.927 Mt     0.093 % 	 ./RayTracingGpu

		     150.803 Mt     4.806 %       919 x        0.164 Mt        0.638 Mt      150.803 Mt   100.000 % 	 ../DeferredRLGpu

		     116.443 Mt     3.711 %       919 x        0.127 Mt        0.215 Mt      116.443 Mt   100.000 % 	 ../RayTracingRLGpu

		     497.796 Mt    15.864 %       919 x        0.542 Mt        0.454 Mt      497.796 Mt   100.000 % 	 ../ResolveRLGpu

		    2370.022 Mt    75.527 %        16 x      148.126 Mt        0.000 Mt        0.037 Mt     0.002 % 	 ../AccelerationStructureBuild

		    2367.542 Mt    99.895 %        16 x      147.971 Mt        0.000 Mt     2367.542 Mt   100.000 % 	 .../BuildBottomLevelASGpu

		       2.442 Mt     0.103 %        16 x        0.153 Mt        0.000 Mt        2.442 Mt   100.000 % 	 .../BuildTopLevelASGpu

		       1.810 Mt     0.056 %       919 x        0.002 Mt        0.002 Mt        1.810 Mt   100.000 % 	 ./Present


	============================


