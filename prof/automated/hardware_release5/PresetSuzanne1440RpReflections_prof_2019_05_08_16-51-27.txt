Configuration: 
Command line parameters  = E:\programming\FIT-DIP\Masters-DXR-Main-git\build\Measurement\x64\Release\Measurement.exe --scene Suzanne/Suzanne.gltf --profile-automation --profile-camera-track Suzanne/Suzanne.trk --rt-mode-pure --width 2560 --height 1440 --profile-output PresetSuzanne1440RpReflections --quality-preset Reflections 
Build type               = Release
Device name              = NVIDIA GeForce RTX 2080 Ti
Device memory MB         = 11049
Ray Tracing backend      = Hardware
Window width             = 2560
Window height            = 1421
Window title             = Direct3D App
Back-buffer count        = 3
Fill Triangles           = enabled
Triangles Font Clock-Wise= enabled
Fast Build AS            = disabled
Target FPS               = 0
Target UPS               = 60
Tearing                  = enabled
VSync                    = disabled
GUI rendering            = disabled
Debug layer              = disabled
GPU validation           = disabled
GPU profiling            = enabled
Render mode              = Ray Tracing
Loaded scene file        = Suzanne/Suzanne.gltf
Using testing scene      = disabled
Duplication              = 1
Duplication in cube      = disabled
Duplication offset       = 10
BLAS duplication         = disabled
Rays per pixel           = 2
Quality preset           = Reflections
Rasterization Quality    : 
  AO                     = disabled
  AO kernel size         = 1
  Shadows                = disabled
  Shadow map resolution  = 1
  Shadow PCF kernel size = 0
  Reflections            = enabled
Ray Tracing Quality      : 
  AO                     = disabled
  AO rays                = 0
  AO kernel size         = 4
  Shadows                = disabled
  Shadow rays            = 0
  Shadow kernel size     = 4
  Reflections            = enabled

ProfilingAggregator: 
Render	,	0.5743	,	0.6767	,	0.633	,	0.5596	,	0.7103	,	0.5414	,	0.5067	,	0.5549	,	0.5054	,	0.6774	,	0.7125	,	0.5391	,	0.5064	,	0.7045	,	0.5234	,	0.6641	,	0.7597	,	0.822	,	0.8623	,	0.7811	,	0.6191	,	0.5638	,	0.6859	,	0.5061
Update	,	0.0808	,	0.0731	,	0.0425	,	0.0341	,	0.0384	,	0.0365	,	0.0685	,	0.0399	,	0.0333	,	0.0718	,	0.0337	,	0.0732	,	0.0339	,	0.0348	,	0.0683	,	0.0727	,	0.0343	,	0.0745	,	0.0375	,	0.0398	,	0.0333	,	0.0335	,	0.0322	,	0.0334
RayTracing	,	0.0226	,	0.0564	,	0.0286	,	0.0203	,	0.0695	,	0.0229	,	0.0204	,	0.0294	,	0.0197	,	0.0537	,	0.0552	,	0.0219	,	0.0198	,	0.0543	,	0.02	,	0.0623	,	0.0553	,	0.0623	,	0.0588	,	0.0547	,	0.0201	,	0.0196	,	0.0596	,	0.0203
RayTracingGpu	,	0.368064	,	0.394272	,	0.404992	,	0.385696	,	0.364416	,	0.338912	,	0.327104	,	0.323488	,	0.333952	,	0.355264	,	0.365056	,	0.346848	,	0.329952	,	0.329312	,	0.35056	,	0.388928	,	0.426688	,	0.470592	,	0.51824	,	0.463936	,	0.42464	,	0.363936	,	0.335456	,	0.331072
RayTracingRLGpu	,	0.367392	,	0.393472	,	0.40352	,	0.384096	,	0.363744	,	0.33824	,	0.326464	,	0.322816	,	0.33328	,	0.354592	,	0.363552	,	0.345216	,	0.328448	,	0.328672	,	0.349088	,	0.387808	,	0.426016	,	0.46976	,	0.5176	,	0.463264	,	0.423168	,	0.362432	,	0.334528	,	0.33024
Index	,	0	,	1	,	2	,	3	,	4	,	5	,	6	,	7	,	8	,	9	,	10	,	11	,	12	,	13	,	14	,	15	,	16	,	17	,	18	,	19	,	20	,	21	,	22	,	23

ProfilingBuildAggregator: 
BuildBottomLevelAS	,	1.022
BuildBottomLevelASGpu	,	0.44128
BuildTopLevelAS	,	0.9095
BuildTopLevelASGpu	,	0.062048
Duplication	,	1
Triangles	,	3936
Meshes	,	1
BottomLevels	,	1
TopLevelSize	,	64
BottomLevelsSize	,	282112
Index	,	0

FpsAggregator: 
FPS	,	1378	,	1470	,	1410	,	1470	,	1503	,	1605	,	1651	,	1652	,	1653	,	1634	,	1587	,	1615	,	1667	,	1656	,	1643	,	1551	,	1438	,	1310	,	1222	,	1216	,	1305	,	1475	,	1613	,	1653
UPS	,	59	,	61	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60
FrameTime	,	0.725836	,	0.680646	,	0.709549	,	0.680553	,	0.665756	,	0.623345	,	0.605835	,	0.605426	,	0.605044	,	0.612267	,	0.630319	,	0.619319	,	0.599997	,	0.603999	,	0.608645	,	0.645115	,	0.695413	,	0.763393	,	0.81863	,	0.822386	,	0.766723	,	0.678258	,	0.620332	,	0.605209
GigaRays/s	,	10.0236	,	10.6891	,	10.2537	,	10.6906	,	10.9282	,	11.6717	,	12.0091	,	12.0172	,	12.0248	,	11.8829	,	11.5426	,	11.7476	,	12.1259	,	12.0456	,	11.9536	,	11.2779	,	10.4622	,	9.5305	,	8.88744	,	8.84684	,	9.48911	,	10.7268	,	11.7284	,	12.0215
Index	,	0	,	1	,	2	,	3	,	4	,	5	,	6	,	7	,	8	,	9	,	10	,	11	,	12	,	13	,	14	,	15	,	16	,	17	,	18	,	19	,	20	,	21	,	22	,	23


AccelerationStructure: 
Ray tracing acceleration structure building statistics: 
	Bottom Level Acceleration Structure: 
		Maximum [B]: 282112
		Minimum [B]: 282112
		Currently Allocated [B]: 282112
		Currently Created [num]: 1
		Current Average [B]: 282112
		Maximum Triangles [num]: 3936
		Minimum Triangles [num]: 3936
		Total Triangles [num]: 3936
		Maximum Meshes [num]: 1
		Minimum Meshes [num]: 1
		Total Meshes [num]: 1
	Top Level Acceleration Structure: 
		Maximum [B]: 64
		Minimum [B]: 64
		Currently Allocated [B]: 64
		Total Allocated [B]: 64
		Total Created [num]: 1
		Average Allocated [B]: 64
		Maximum Instances [num]: 1
		Minimum Instances [num]: 1
		Total Instances [num]: 1
	Scratch Buffer: 
		Maximum [B]: 82944
		Minimum [B]: 82944
		Currently Allocated [B]: 82944
		Total Allocated [B]: 82944
		Total Created [num]: 1
		Average Allocated [B]: 82944
ProfilingStack: 
[PROF] Profiling statistics: 
	Scopes entered : 411532
	Scopes exited : 411531
	Overhead per scope [ticks] : 100.6
	Threads entered : 3
	Threads exited : 0
	============================
		         Sum[Mt]   Parent[%]  Samples[num]       Avg[Mt]   Last/Curr[Mt]        Self[Mt]     Self[%] 	 Name

	main:

		   24480.605 Mt   100.000 %         1 x    24480.606 Mt    24480.605 Mt        0.329 Mt     0.001 % 	 /

		   24480.285 Mt    99.999 %         1 x    24480.286 Mt    24480.285 Mt      281.618 Mt     1.150 % 	 ./Main application loop

		       2.402 Mt     0.010 %         1 x        2.402 Mt        0.000 Mt        2.402 Mt   100.000 % 	 ../TexturedRLInitialization

		       1.276 Mt     0.005 %         1 x        1.276 Mt        0.000 Mt        1.276 Mt   100.000 % 	 ../ShadowMapRLInitialization

		       4.493 Mt     0.018 %         1 x        4.493 Mt        0.000 Mt        4.493 Mt   100.000 % 	 ../RasterResolveRLInitialization

		       3.922 Mt     0.016 %         1 x        3.922 Mt        0.000 Mt        3.922 Mt   100.000 % 	 ../ResolveRLInitialization

		   24007.818 Mt    98.070 %    118998 x        0.202 Mt        0.612 Mt      193.369 Mt     0.805 % 	 ../MainLoop

		     129.636 Mt     0.540 %      1441 x        0.090 Mt        0.049 Mt      129.401 Mt    99.819 % 	 .../Update

		       0.235 Mt     0.181 %         1 x        0.235 Mt        0.000 Mt        0.235 Mt   100.000 % 	 ..../GuiModelApply

		   23684.813 Mt    98.655 %     36381 x        0.651 Mt        0.562 Mt     1151.107 Mt     4.860 % 	 .../Render

		    1168.276 Mt     4.933 %     36381 x        0.032 Mt        0.033 Mt       67.867 Mt     5.809 % 	 ..../RayTracing

		    1100.409 Mt    94.191 %     36381 x        0.030 Mt        0.031 Mt     1089.102 Mt    98.972 % 	 ...../RayCasting

		      11.307 Mt     1.028 %     36381 x        0.000 Mt        0.000 Mt       11.307 Mt   100.000 % 	 ....../RayTracingRLPrep

		   21365.429 Mt    90.207 %     36381 x        0.587 Mt        0.495 Mt    21365.429 Mt   100.000 % 	 ..../Present

		       3.073 Mt     0.013 %         1 x        3.073 Mt        0.000 Mt        3.073 Mt   100.000 % 	 ../AmbientOcclusionRLInitialization

		       4.906 Mt     0.020 %         1 x        4.906 Mt        0.000 Mt        4.906 Mt   100.000 % 	 ../DeferredRLInitialization

		      55.366 Mt     0.226 %         1 x       55.366 Mt        0.000 Mt        2.927 Mt     5.286 % 	 ../RayTracingRLInitialization

		      52.439 Mt    94.714 %         1 x       52.439 Mt        0.000 Mt       52.439 Mt   100.000 % 	 .../PipelineBuild

		     115.411 Mt     0.471 %         1 x      115.411 Mt        0.000 Mt        0.192 Mt     0.166 % 	 ../Initialization

		     115.219 Mt    99.834 %         1 x      115.219 Mt        0.000 Mt        1.090 Mt     0.946 % 	 .../ScenePrep

		      64.443 Mt    55.931 %         1 x       64.443 Mt        0.000 Mt       16.961 Mt    26.319 % 	 ..../SceneLoad

		      39.225 Mt    60.867 %         1 x       39.225 Mt        0.000 Mt        8.450 Mt    21.544 % 	 ...../glTF-LoadScene

		      30.774 Mt    78.456 %         2 x       15.387 Mt        0.000 Mt       30.774 Mt   100.000 % 	 ....../glTF-LoadImageData

		       6.263 Mt     9.719 %         1 x        6.263 Mt        0.000 Mt        6.263 Mt   100.000 % 	 ...../glTF-CreateScene

		       1.994 Mt     3.094 %         1 x        1.994 Mt        0.000 Mt        1.994 Mt   100.000 % 	 ...../SceneDesc-CreateScene

		       0.308 Mt     0.268 %         1 x        0.308 Mt        0.000 Mt        0.001 Mt     0.162 % 	 ..../ShadowMapRLPrep

		       0.308 Mt    99.838 %         1 x        0.308 Mt        0.000 Mt        0.299 Mt    97.175 % 	 ...../PrepareForRendering

		       0.009 Mt     2.825 %         1 x        0.009 Mt        0.000 Mt        0.009 Mt   100.000 % 	 ....../PrepareDrawBundle

		       1.613 Mt     1.400 %         1 x        1.613 Mt        0.000 Mt        0.000 Mt     0.031 % 	 ..../TexturedRLPrep

		       1.613 Mt    99.969 %         1 x        1.613 Mt        0.000 Mt        1.608 Mt    99.709 % 	 ...../PrepareForRendering

		       0.001 Mt     0.062 %         1 x        0.001 Mt        0.000 Mt        0.001 Mt   100.000 % 	 ....../PrepareMaterials

		       0.004 Mt     0.229 %         1 x        0.004 Mt        0.000 Mt        0.004 Mt   100.000 % 	 ....../PrepareDrawBundle

		       1.472 Mt     1.278 %         1 x        1.472 Mt        0.000 Mt        0.000 Mt     0.027 % 	 ..../DeferredRLPrep

		       1.472 Mt    99.973 %         1 x        1.472 Mt        0.000 Mt        1.116 Mt    75.822 % 	 ...../PrepareForRendering

		       0.001 Mt     0.034 %         1 x        0.001 Mt        0.000 Mt        0.001 Mt   100.000 % 	 ....../PrepareMaterials

		       0.352 Mt    23.947 %         1 x        0.352 Mt        0.000 Mt        0.352 Mt   100.000 % 	 ....../BuildMaterialCache

		       0.003 Mt     0.197 %         1 x        0.003 Mt        0.000 Mt        0.003 Mt   100.000 % 	 ....../PrepareDrawBundle

		      33.506 Mt    29.080 %         1 x       33.506 Mt        0.000 Mt        6.779 Mt    20.231 % 	 ..../RayTracingRLPrep

		       0.053 Mt     0.158 %         1 x        0.053 Mt        0.000 Mt        0.053 Mt   100.000 % 	 ...../PrepareAccelerationStructures

		       0.001 Mt     0.002 %         1 x        0.001 Mt        0.000 Mt        0.001 Mt   100.000 % 	 ...../PrepareCache

		      19.918 Mt    59.445 %         1 x       19.918 Mt        0.000 Mt       19.918 Mt   100.000 % 	 ...../PipelineBuild

		       0.368 Mt     1.099 %         1 x        0.368 Mt        0.000 Mt        0.368 Mt   100.000 % 	 ...../BuildCache

		       4.770 Mt    14.237 %         1 x        4.770 Mt        0.000 Mt        0.001 Mt     0.010 % 	 ...../BuildAccelerationStructures

		       4.770 Mt    99.990 %         1 x        4.770 Mt        0.000 Mt        2.838 Mt    59.506 % 	 ....../AccelerationStructureBuild

		       1.022 Mt    21.426 %         1 x        1.022 Mt        0.000 Mt        1.022 Mt   100.000 % 	 ......./BuildBottomLevelAS

		       0.909 Mt    19.067 %         1 x        0.909 Mt        0.000 Mt        0.909 Mt   100.000 % 	 ......./BuildTopLevelAS

		       1.618 Mt     4.828 %         1 x        1.618 Mt        0.000 Mt        1.618 Mt   100.000 % 	 ...../BuildShaderTables

		      12.786 Mt    11.097 %         1 x       12.786 Mt        0.000 Mt       12.786 Mt   100.000 % 	 ..../GpuSidePrep

	WindowThread:

		   24478.987 Mt   100.000 %         1 x    24478.987 Mt    24478.986 Mt    24478.987 Mt   100.000 % 	 /

	GpuThread:

		   13752.936 Mt   100.000 %     36381 x        0.378 Mt        0.330 Mt      143.277 Mt     1.042 % 	 /

		       0.511 Mt     0.004 %         1 x        0.511 Mt        0.000 Mt        0.006 Mt     1.247 % 	 ./ScenePrep

		       0.504 Mt    98.753 %         1 x        0.504 Mt        0.000 Mt        0.001 Mt     0.178 % 	 ../AccelerationStructureBuild

		       0.441 Mt    87.517 %         1 x        0.441 Mt        0.000 Mt        0.441 Mt   100.000 % 	 .../BuildBottomLevelASGpu

		       0.062 Mt    12.306 %         1 x        0.062 Mt        0.000 Mt        0.062 Mt   100.000 % 	 .../BuildTopLevelASGpu

		   13542.841 Mt    98.472 %     36381 x        0.372 Mt        0.328 Mt       45.026 Mt     0.332 % 	 ./RayTracingGpu

		   13497.815 Mt    99.668 %     36381 x        0.371 Mt        0.327 Mt    13497.815 Mt   100.000 % 	 ../RayTracingRLGpu

		      66.307 Mt     0.482 %     36381 x        0.002 Mt        0.002 Mt       66.307 Mt   100.000 % 	 ./Present


	============================


