Configuration: 
Command line parameters  = E:\programming\FIT-DIP\Masters-DXR-Main-git\build\Measurement\x64\Release\Measurement.exe --scene Sponza/Sponza.gltf --profile-automation --profile-camera-track Sponza/SponzaPreset.trk --rt-mode --width 2560 --height 1440 --profile-output PresetSponza1440RtReflections --quality-preset Reflections 
Build type               = Release
Device name              = NVIDIA GeForce RTX 2080 Ti
Device memory MB         = 11049
Ray Tracing backend      = Hardware
Window width             = 2560
Window height            = 1421
Window title             = Direct3D App
Back-buffer count        = 3
Fill Triangles           = enabled
Triangles Font Clock-Wise= enabled
Fast Build AS            = disabled
Target FPS               = 0
Target UPS               = 60
Tearing                  = enabled
VSync                    = disabled
GUI rendering            = disabled
Debug layer              = disabled
GPU validation           = disabled
GPU profiling            = enabled
Render mode              = Ray Tracing
Loaded scene file        = Sponza/Sponza.gltf
Using testing scene      = disabled
Duplication              = 1
Duplication in cube      = disabled
Duplication offset       = 10
BLAS duplication         = disabled
Rays per pixel           = 1
Quality preset           = Reflections
Rasterization Quality    : 
  AO                     = disabled
  AO kernel size         = 1
  Shadows                = disabled
  Shadow map resolution  = 1
  Shadow PCF kernel size = 0
  Reflections            = enabled
Ray Tracing Quality      : 
  AO                     = disabled
  AO rays                = 0
  AO kernel size         = 4
  Shadows                = disabled
  Shadow rays            = 0
  Shadow kernel size     = 4
  Reflections            = enabled

ProfilingAggregator: 
Render	,	4.6563	,	4.3738	,	3.9692	,	3.6333	,	3.6829	,	4.3501	,	3.9392	,	4.6835	,	4.3657	,	3.8992	,	3.6768	,	4.0369	,	4.3396	,	4.5604	,	4.0678	,	3.9803	,	5.2567	,	4.8977	,	3.4703	,	4.5739	,	5.7127	,	5.5502	,	5.6015
Update	,	0.074	,	0.0757	,	0.0759	,	0.0741	,	0.0774	,	0.0758	,	0.0756	,	0.076	,	0.0737	,	0.0775	,	0.0754	,	0.0764	,	0.0754	,	0.0755	,	0.0768	,	0.0755	,	0.0735	,	0.0762	,	0.0745	,	0.0768	,	0.0743	,	0.0744	,	0.0759
RayTracing	,	0.1743	,	0.1713	,	0.1728	,	0.1723	,	0.1796	,	0.1748	,	0.1731	,	0.178	,	0.1695	,	0.1705	,	0.1823	,	0.172	,	0.1716	,	0.1734	,	0.1737	,	0.1915	,	0.169	,	0.1686	,	0.1708	,	0.2241	,	0.185	,	0.1701	,	0.1704
RayTracingGpu	,	3.82093	,	3.5311	,	3.18237	,	2.92256	,	2.52851	,	3.35206	,	3.08829	,	3.81306	,	3.54045	,	3.06227	,	2.8271	,	3.23402	,	3.58506	,	3.7824	,	3.27325	,	3.18614	,	4.5488	,	4.00704	,	2.61926	,	3.66026	,	4.83754	,	4.81907	,	4.85706
DeferredRLGpu	,	2.66618	,	2.44835	,	2.13802	,	1.48512	,	1.4336	,	1.96102	,	1.96086	,	2.37728	,	2.0687	,	1.51827	,	1.34362	,	1.7393	,	2.12643	,	2.41664	,	2.15738	,	1.58765	,	2.14694	,	2.09856	,	1.49085	,	2.67203	,	3.86832	,	3.8511	,	3.88707
RayTracingRLGpu	,	0.688576	,	0.615648	,	0.581984	,	0.951584	,	0.547072	,	0.811168	,	0.65296	,	0.969504	,	1.00362	,	1.07101	,	1.01603	,	1.0264	,	0.986464	,	0.89344	,	0.6472	,	0.945664	,	1.90765	,	1.42768	,	0.662528	,	0.515328	,	0.50416	,	0.502528	,	0.503008
ResolveRLGpu	,	0.463328	,	0.46496	,	0.459936	,	0.483808	,	0.54624	,	0.578272	,	0.47024	,	0.464096	,	0.466144	,	0.469952	,	0.464352	,	0.466176	,	0.469792	,	0.4704	,	0.4664	,	0.6512	,	0.492608	,	0.478656	,	0.462848	,	0.470304	,	0.463104	,	0.463264	,	0.46304
Index	,	0	,	1	,	2	,	3	,	4	,	5	,	6	,	7	,	8	,	9	,	10	,	11	,	12	,	13	,	14	,	15	,	16	,	17	,	18	,	19	,	20	,	21	,	22

ProfilingBuildAggregator: 
BuildBottomLevelAS	,	1.6707
BuildBottomLevelASGpu	,	2.36403
BuildTopLevelAS	,	0.8164
BuildTopLevelASGpu	,	0.09024
Duplication	,	1
Triangles	,	262267
Meshes	,	103
BottomLevels	,	1
TopLevelSize	,	64
BottomLevelsSize	,	18390656
Index	,	0

FpsAggregator: 
FPS	,	146	,	205	,	232	,	255	,	301	,	280	,	278	,	209	,	214	,	232	,	261	,	250	,	226	,	212	,	215	,	273	,	236	,	210	,	247	,	277	,	184	,	171	,	171
UPS	,	59	,	61	,	60	,	60	,	60	,	60	,	60	,	60	,	61	,	60	,	60	,	60	,	60	,	60	,	60	,	61	,	60	,	60	,	60	,	60	,	60	,	60	,	61
FrameTime	,	6.86931	,	4.88559	,	4.32648	,	3.93358	,	3.32346	,	3.57931	,	3.60674	,	4.80657	,	4.67766	,	4.31151	,	3.83898	,	4.0143	,	4.44007	,	4.72777	,	4.65585	,	3.67462	,	4.24152	,	4.7744	,	4.0537	,	3.61737	,	5.44626	,	5.87699	,	5.8734
GigaRays/s	,	0.529567	,	0.74459	,	0.840813	,	0.924796	,	1.09457	,	1.01633	,	1.0086	,	0.75683	,	0.777689	,	0.843732	,	0.947585	,	0.906201	,	0.819303	,	0.769446	,	0.781331	,	0.989968	,	0.857655	,	0.76193	,	0.897391	,	1.00564	,	0.667937	,	0.618983	,	0.619362
Index	,	0	,	1	,	2	,	3	,	4	,	5	,	6	,	7	,	8	,	9	,	10	,	11	,	12	,	13	,	14	,	15	,	16	,	17	,	18	,	19	,	20	,	21	,	22


AccelerationStructure: 
Ray tracing acceleration structure building statistics: 
	Bottom Level Acceleration Structure: 
		Maximum [B]: 18390656
		Minimum [B]: 18390656
		Currently Allocated [B]: 18390656
		Currently Created [num]: 1
		Current Average [B]: 18390656
		Maximum Triangles [num]: 262267
		Minimum Triangles [num]: 262267
		Total Triangles [num]: 262267
		Maximum Meshes [num]: 103
		Minimum Meshes [num]: 103
		Total Meshes [num]: 103
	Top Level Acceleration Structure: 
		Maximum [B]: 64
		Minimum [B]: 64
		Currently Allocated [B]: 64
		Total Allocated [B]: 64
		Total Created [num]: 1
		Average Allocated [B]: 64
		Maximum Instances [num]: 1
		Minimum Instances [num]: 1
		Total Instances [num]: 1
	Scratch Buffer: 
		Maximum [B]: 14995072
		Minimum [B]: 14995072
		Currently Allocated [B]: 14995072
		Total Allocated [B]: 14995072
		Total Created [num]: 1
		Average Allocated [B]: 14995072
ProfilingStack: 
[PROF] Profiling statistics: 
	Scopes entered : 128529
	Scopes exited : 128528
	Overhead per scope [ticks] : 103.2
	Threads entered : 3
	Threads exited : 0
	============================
		         Sum[Mt]   Parent[%]  Samples[num]       Avg[Mt]   Last/Curr[Mt]        Self[Mt]     Self[%] 	 Name

	main:

		   25974.281 Mt   100.000 %         1 x    25974.282 Mt    25974.281 Mt        0.375 Mt     0.001 % 	 /

		   25973.937 Mt    99.999 %         1 x    25973.938 Mt    25973.937 Mt      252.190 Mt     0.971 % 	 ./Main application loop

		       2.333 Mt     0.009 %         1 x        2.333 Mt        0.000 Mt        2.333 Mt   100.000 % 	 ../TexturedRLInitialization

		       1.273 Mt     0.005 %         1 x        1.273 Mt        0.000 Mt        1.273 Mt   100.000 % 	 ../ShadowMapRLInitialization

		       5.346 Mt     0.021 %         1 x        5.346 Mt        0.000 Mt        5.346 Mt   100.000 % 	 ../RasterResolveRLInitialization

		       3.766 Mt     0.014 %         1 x        3.766 Mt        0.000 Mt        3.766 Mt   100.000 % 	 ../ResolveRLInitialization

		   23077.228 Mt    88.848 %     58302 x        0.396 Mt        6.362 Mt      430.874 Mt     1.867 % 	 ../MainLoop

		     193.925 Mt     0.840 %      1384 x        0.140 Mt        0.092 Mt      193.536 Mt    99.800 % 	 .../Update

		       0.389 Mt     0.200 %         1 x        0.389 Mt        0.000 Mt        0.389 Mt   100.000 % 	 ..../GuiModelApply

		   22452.429 Mt    97.293 %      5287 x        4.247 Mt        6.266 Mt      590.419 Mt     2.630 % 	 .../Render

		     938.967 Mt     4.182 %      5287 x        0.178 Mt        0.225 Mt       35.811 Mt     3.814 % 	 ..../RayTracing

		     398.612 Mt    42.452 %      5287 x        0.075 Mt        0.087 Mt      392.546 Mt    98.478 % 	 ...../Deferred

		       6.066 Mt     1.522 %      5287 x        0.001 Mt        0.001 Mt        6.066 Mt   100.000 % 	 ....../DeferredRLPrep

		     330.830 Mt    35.233 %      5287 x        0.063 Mt        0.081 Mt      327.729 Mt    99.063 % 	 ...../RayCasting

		       3.101 Mt     0.937 %      5287 x        0.001 Mt        0.001 Mt        3.101 Mt   100.000 % 	 ....../RayTracingRLPrep

		     173.713 Mt    18.500 %      5287 x        0.033 Mt        0.040 Mt      173.713 Mt   100.000 % 	 ...../Resolve

		   20923.043 Mt    93.188 %      5287 x        3.957 Mt        5.891 Mt    20923.043 Mt   100.000 % 	 ..../Present

		       3.136 Mt     0.012 %         1 x        3.136 Mt        0.000 Mt        3.136 Mt   100.000 % 	 ../AmbientOcclusionRLInitialization

		       5.374 Mt     0.021 %         1 x        5.374 Mt        0.000 Mt        5.374 Mt   100.000 % 	 ../DeferredRLInitialization

		      50.941 Mt     0.196 %         1 x       50.941 Mt        0.000 Mt        2.377 Mt     4.666 % 	 ../RayTracingRLInitialization

		      48.564 Mt    95.334 %         1 x       48.564 Mt        0.000 Mt       48.564 Mt   100.000 % 	 .../PipelineBuild

		    2572.350 Mt     9.904 %         1 x     2572.350 Mt        0.000 Mt        0.231 Mt     0.009 % 	 ../Initialization

		    2572.119 Mt    99.991 %         1 x     2572.119 Mt        0.000 Mt        2.076 Mt     0.081 % 	 .../ScenePrep

		    2503.236 Mt    97.322 %         1 x     2503.236 Mt        0.000 Mt      233.251 Mt     9.318 % 	 ..../SceneLoad

		    1990.215 Mt    79.506 %         1 x     1990.215 Mt        0.000 Mt      586.183 Mt    29.453 % 	 ...../glTF-LoadScene

		    1404.031 Mt    70.547 %        69 x       20.348 Mt        0.000 Mt     1404.031 Mt   100.000 % 	 ....../glTF-LoadImageData

		     214.389 Mt     8.564 %         1 x      214.389 Mt        0.000 Mt      214.389 Mt   100.000 % 	 ...../glTF-CreateScene

		      65.380 Mt     2.612 %         1 x       65.380 Mt        0.000 Mt       65.380 Mt   100.000 % 	 ...../SceneDesc-CreateScene

		       1.111 Mt     0.043 %         1 x        1.111 Mt        0.000 Mt        0.001 Mt     0.099 % 	 ..../ShadowMapRLPrep

		       1.109 Mt    99.901 %         1 x        1.109 Mt        0.000 Mt        1.069 Mt    96.314 % 	 ...../PrepareForRendering

		       0.041 Mt     3.686 %         1 x        0.041 Mt        0.000 Mt        0.041 Mt   100.000 % 	 ....../PrepareDrawBundle

		       3.458 Mt     0.134 %         1 x        3.458 Mt        0.000 Mt        0.001 Mt     0.017 % 	 ..../TexturedRLPrep

		       3.458 Mt    99.983 %         1 x        3.458 Mt        0.000 Mt        3.421 Mt    98.939 % 	 ...../PrepareForRendering

		       0.001 Mt     0.026 %         1 x        0.001 Mt        0.000 Mt        0.001 Mt   100.000 % 	 ....../PrepareMaterials

		       0.036 Mt     1.035 %         1 x        0.036 Mt        0.000 Mt        0.036 Mt   100.000 % 	 ....../PrepareDrawBundle

		       3.110 Mt     0.121 %         1 x        3.110 Mt        0.000 Mt        0.001 Mt     0.023 % 	 ..../DeferredRLPrep

		       3.110 Mt    99.977 %         1 x        3.110 Mt        0.000 Mt        2.452 Mt    78.869 % 	 ...../PrepareForRendering

		       0.001 Mt     0.023 %         1 x        0.001 Mt        0.000 Mt        0.001 Mt   100.000 % 	 ....../PrepareMaterials

		       0.446 Mt    14.330 %         1 x        0.446 Mt        0.000 Mt        0.446 Mt   100.000 % 	 ....../BuildMaterialCache

		       0.211 Mt     6.779 %         1 x        0.211 Mt        0.000 Mt        0.211 Mt   100.000 % 	 ....../PrepareDrawBundle

		      27.303 Mt     1.062 %         1 x       27.303 Mt        0.000 Mt        2.644 Mt     9.684 % 	 ..../RayTracingRLPrep

		       0.176 Mt     0.643 %         1 x        0.176 Mt        0.000 Mt        0.176 Mt   100.000 % 	 ...../PrepareAccelerationStructures

		       0.003 Mt     0.009 %         1 x        0.003 Mt        0.000 Mt        0.003 Mt   100.000 % 	 ...../PrepareCache

		      13.228 Mt    48.448 %         1 x       13.228 Mt        0.000 Mt       13.228 Mt   100.000 % 	 ...../PipelineBuild

		       0.674 Mt     2.469 %         1 x        0.674 Mt        0.000 Mt        0.674 Mt   100.000 % 	 ...../BuildCache

		       9.123 Mt    33.413 %         1 x        9.123 Mt        0.000 Mt        0.001 Mt     0.007 % 	 ...../BuildAccelerationStructures

		       9.122 Mt    99.993 %         1 x        9.122 Mt        0.000 Mt        6.635 Mt    72.736 % 	 ....../AccelerationStructureBuild

		       1.671 Mt    18.314 %         1 x        1.671 Mt        0.000 Mt        1.671 Mt   100.000 % 	 ......./BuildBottomLevelAS

		       0.816 Mt     8.949 %         1 x        0.816 Mt        0.000 Mt        0.816 Mt   100.000 % 	 ......./BuildTopLevelAS

		       1.456 Mt     5.333 %         1 x        1.456 Mt        0.000 Mt        1.456 Mt   100.000 % 	 ...../BuildShaderTables

		      31.825 Mt     1.237 %         1 x       31.825 Mt        0.000 Mt       31.825 Mt   100.000 % 	 ..../GpuSidePrep

	WindowThread:

		   25972.994 Mt   100.000 %         1 x    25972.994 Mt    25972.994 Mt    25972.994 Mt   100.000 % 	 /

	GpuThread:

		   18203.982 Mt   100.000 %      5287 x        3.443 Mt        5.075 Mt      143.253 Mt     0.787 % 	 /

		       2.463 Mt     0.014 %         1 x        2.463 Mt        0.000 Mt        0.007 Mt     0.296 % 	 ./ScenePrep

		       2.456 Mt    99.704 %         1 x        2.456 Mt        0.000 Mt        0.001 Mt     0.051 % 	 ../AccelerationStructureBuild

		       2.364 Mt    96.274 %         1 x        2.364 Mt        0.000 Mt        2.364 Mt   100.000 % 	 .../BuildBottomLevelASGpu

		       0.090 Mt     3.675 %         1 x        0.090 Mt        0.000 Mt        0.090 Mt   100.000 % 	 .../BuildTopLevelASGpu

		   18025.386 Mt    99.019 %      5287 x        3.409 Mt        5.072 Mt       14.753 Mt     0.082 % 	 ./RayTracingGpu

		   11093.973 Mt    61.546 %      5287 x        2.098 Mt        4.093 Mt    11093.973 Mt   100.000 % 	 ../DeferredRLGpu

		    4377.363 Mt    24.284 %      5287 x        0.828 Mt        0.505 Mt     4377.363 Mt   100.000 % 	 ../RayTracingRLGpu

		    2539.297 Mt    14.087 %      5287 x        0.480 Mt        0.473 Mt     2539.297 Mt   100.000 % 	 ../ResolveRLGpu

		      32.879 Mt     0.181 %      5287 x        0.006 Mt        0.003 Mt       32.879 Mt   100.000 % 	 ./Present


	============================


