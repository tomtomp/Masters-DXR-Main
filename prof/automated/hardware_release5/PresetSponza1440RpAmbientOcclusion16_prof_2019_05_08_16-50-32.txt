Configuration: 
Command line parameters  = E:\programming\FIT-DIP\Masters-DXR-Main-git\build\Measurement\x64\Release\Measurement.exe --scene Sponza/Sponza.gltf --profile-automation --profile-camera-track Sponza/SponzaPreset.trk --rt-mode-pure --width 2560 --height 1440 --profile-output PresetSponza1440RpAmbientOcclusion16 --quality-preset AmbientOcclusion16 
Build type               = Release
Device name              = NVIDIA GeForce RTX 2080 Ti
Device memory MB         = 11049
Ray Tracing backend      = Hardware
Window width             = 2560
Window height            = 1421
Window title             = Direct3D App
Back-buffer count        = 3
Fill Triangles           = enabled
Triangles Font Clock-Wise= enabled
Fast Build AS            = disabled
Target FPS               = 0
Target UPS               = 60
Tearing                  = enabled
VSync                    = disabled
GUI rendering            = disabled
Debug layer              = disabled
GPU validation           = disabled
GPU profiling            = enabled
Render mode              = Ray Tracing
Loaded scene file        = Sponza/Sponza.gltf
Using testing scene      = disabled
Duplication              = 1
Duplication in cube      = disabled
Duplication offset       = 10
BLAS duplication         = disabled
Rays per pixel           = 17
Quality preset           = AmbientOcclusion16
Rasterization Quality    : 
  AO                     = enabled
  AO kernel size         = 16
  Shadows                = disabled
  Shadow map resolution  = 1
  Shadow PCF kernel size = 0
  Reflections            = disabled
Ray Tracing Quality      : 
  AO                     = enabled
  AO rays                = 16
  AO kernel size         = 4
  Shadows                = disabled
  Shadow rays            = 0
  Shadow kernel size     = 4
  Reflections            = disabled

ProfilingAggregator: 
Render	,	8.6355	,	9.3575	,	8.938	,	7.8105	,	8.5519	,	8.7222	,	7.9916	,	8.1142	,	7.9023	,	7.6918	,	7.5428	,	7.5438	,	8.57	,	8.4704	,	7.6159	,	8.7722	,	9.9124	,	9.8646	,	8.923	,	8.5831	,	8.1076	,	7.7242	,	7.9215
Update	,	0.0423	,	0.0415	,	0.0812	,	0.0805	,	0.0436	,	0.0447	,	0.0447	,	0.0807	,	0.0437	,	0.0816	,	0.1152	,	0.0439	,	0.0815	,	0.0828	,	0.0726	,	0.0783	,	0.0414	,	0.0424	,	0.0788	,	0.0823	,	0.0379	,	0.0886	,	0.0467
RayTracing	,	0.0498	,	0.0881	,	0.0972	,	0.1119	,	0.0601	,	0.0704	,	0.0982	,	0.0813	,	0.0467	,	0.0857	,	0.0726	,	0.0931	,	0.0875	,	0.0457	,	0.0396	,	0.0842	,	0.0938	,	0.079	,	0.0908	,	0.0442	,	0.0385	,	0.0985	,	0.0599
RayTracingGpu	,	8.21686	,	8.70192	,	8.2081	,	7.30166	,	7.88429	,	8.0777	,	7.21725	,	7.38682	,	7.28003	,	7.09158	,	6.98346	,	6.85731	,	7.9503	,	8.08419	,	7.12173	,	8.1601	,	9.43312	,	9.28416	,	8.16394	,	7.86301	,	7.66493	,	7.13056	,	7.43987
RayTracingRLGpu	,	8.2159	,	8.70122	,	8.20256	,	7.30016	,	7.88234	,	8.07568	,	7.21626	,	7.38589	,	7.27824	,	7.09008	,	6.98259	,	6.85542	,	7.94851	,	8.08349	,	7.11994	,	8.15914	,	9.43226	,	9.28	,	8.16243	,	7.86115	,	7.66342	,	7.12957	,	7.43885
Index	,	0	,	1	,	2	,	3	,	4	,	5	,	6	,	7	,	8	,	9	,	10	,	11	,	12	,	13	,	14	,	15	,	16	,	17	,	18	,	19	,	20	,	21	,	22

ProfilingBuildAggregator: 
BuildBottomLevelAS	,	1.8408
BuildBottomLevelASGpu	,	2.41002
BuildTopLevelAS	,	0.8464
BuildTopLevelASGpu	,	0.088928
Duplication	,	1
Triangles	,	262267
Meshes	,	103
BottomLevels	,	1
TopLevelSize	,	64
BottomLevelsSize	,	18390656
Index	,	0

FpsAggregator: 
FPS	,	84	,	109	,	114	,	117	,	125	,	109	,	126	,	121	,	128	,	133	,	134	,	134	,	127	,	121	,	122	,	121	,	105	,	102	,	105	,	118	,	123	,	126	,	126
UPS	,	59	,	60	,	60	,	60	,	61	,	60	,	60	,	61	,	60	,	60	,	60	,	60	,	61	,	60	,	61	,	60	,	60	,	60	,	61	,	60	,	60	,	61	,	60
FrameTime	,	11.9568	,	9.1889	,	8.7783	,	8.55525	,	8.0339	,	9.22011	,	7.98102	,	8.28832	,	7.81895	,	7.53169	,	7.51203	,	7.49683	,	7.91539	,	8.32521	,	8.25366	,	8.28718	,	9.61179	,	9.81462	,	9.58535	,	8.5288	,	8.17924	,	7.97266	,	7.96761
GigaRays/s	,	5.1721	,	6.73007	,	7.04487	,	7.22854	,	7.69762	,	6.70729	,	7.74863	,	7.46134	,	7.90923	,	8.21089	,	8.23238	,	8.24908	,	7.81287	,	7.42827	,	7.49267	,	7.46236	,	6.43396	,	6.301	,	6.45171	,	7.25095	,	7.56084	,	7.75675	,	7.76166
Index	,	0	,	1	,	2	,	3	,	4	,	5	,	6	,	7	,	8	,	9	,	10	,	11	,	12	,	13	,	14	,	15	,	16	,	17	,	18	,	19	,	20	,	21	,	22


AccelerationStructure: 
Ray tracing acceleration structure building statistics: 
	Bottom Level Acceleration Structure: 
		Maximum [B]: 18390656
		Minimum [B]: 18390656
		Currently Allocated [B]: 18390656
		Currently Created [num]: 1
		Current Average [B]: 18390656
		Maximum Triangles [num]: 262267
		Minimum Triangles [num]: 262267
		Total Triangles [num]: 262267
		Maximum Meshes [num]: 103
		Minimum Meshes [num]: 103
		Total Meshes [num]: 103
	Top Level Acceleration Structure: 
		Maximum [B]: 64
		Minimum [B]: 64
		Currently Allocated [B]: 64
		Total Allocated [B]: 64
		Total Created [num]: 1
		Average Allocated [B]: 64
		Maximum Instances [num]: 1
		Minimum Instances [num]: 1
		Total Instances [num]: 1
	Scratch Buffer: 
		Maximum [B]: 14995072
		Minimum [B]: 14995072
		Currently Allocated [B]: 14995072
		Total Allocated [B]: 14995072
		Total Created [num]: 1
		Average Allocated [B]: 14995072
ProfilingStack: 
[PROF] Profiling statistics: 
	Scopes entered : 97154
	Scopes exited : 97153
	Overhead per scope [ticks] : 104.7
	Threads entered : 3
	Threads exited : 0
	============================
		         Sum[Mt]   Parent[%]  Samples[num]       Avg[Mt]   Last/Curr[Mt]        Self[Mt]     Self[%] 	 Name

	main:

		   26621.006 Mt   100.000 %         1 x    26621.007 Mt    26621.006 Mt        0.498 Mt     0.002 % 	 /

		   26620.528 Mt    99.998 %         1 x    26620.529 Mt    26620.528 Mt      292.264 Mt     1.098 % 	 ./Main application loop

		       2.370 Mt     0.009 %         1 x        2.370 Mt        0.000 Mt        2.370 Mt   100.000 % 	 ../TexturedRLInitialization

		       1.201 Mt     0.005 %         1 x        1.201 Mt        0.000 Mt        1.201 Mt   100.000 % 	 ../ShadowMapRLInitialization

		       5.650 Mt     0.021 %         1 x        5.650 Mt        0.000 Mt        5.650 Mt   100.000 % 	 ../RasterResolveRLInitialization

		       4.100 Mt     0.015 %         1 x        4.100 Mt        0.000 Mt        4.100 Mt   100.000 % 	 ../ResolveRLInitialization

		   23121.266 Mt    86.855 %     73800 x        0.313 Mt        8.390 Mt      197.254 Mt     0.853 % 	 ../MainLoop

		     158.365 Mt     0.685 %      1386 x        0.114 Mt        0.058 Mt      158.108 Mt    99.838 % 	 .../Update

		       0.256 Mt     0.162 %         1 x        0.256 Mt        0.000 Mt        0.256 Mt   100.000 % 	 ..../GuiModelApply

		   22765.647 Mt    98.462 %      2732 x        8.333 Mt        8.330 Mt      222.858 Mt     0.979 % 	 .../Render

		     188.126 Mt     0.826 %      2732 x        0.069 Mt        0.086 Mt       11.773 Mt     6.258 % 	 ..../RayTracing

		     176.353 Mt    93.742 %      2732 x        0.065 Mt        0.082 Mt      174.830 Mt    99.136 % 	 ...../RayCasting

		       1.523 Mt     0.864 %      2732 x        0.001 Mt        0.000 Mt        1.523 Mt   100.000 % 	 ....../RayTracingRLPrep

		   22354.664 Mt    98.195 %      2732 x        8.183 Mt        8.144 Mt    22354.664 Mt   100.000 % 	 ..../Present

		       2.719 Mt     0.010 %         1 x        2.719 Mt        0.000 Mt        2.719 Mt   100.000 % 	 ../AmbientOcclusionRLInitialization

		       6.127 Mt     0.023 %         1 x        6.127 Mt        0.000 Mt        6.127 Mt   100.000 % 	 ../DeferredRLInitialization

		      68.077 Mt     0.256 %         1 x       68.077 Mt        0.000 Mt        2.793 Mt     4.103 % 	 ../RayTracingRLInitialization

		      65.284 Mt    95.897 %         1 x       65.284 Mt        0.000 Mt       65.284 Mt   100.000 % 	 .../PipelineBuild

		    3116.753 Mt    11.708 %         1 x     3116.753 Mt        0.000 Mt        0.481 Mt     0.015 % 	 ../Initialization

		    3116.272 Mt    99.985 %         1 x     3116.272 Mt        0.000 Mt        3.329 Mt     0.107 % 	 .../ScenePrep

		    3030.957 Mt    97.262 %         1 x     3030.957 Mt        0.000 Mt      652.607 Mt    21.531 % 	 ..../SceneLoad

		    2056.913 Mt    67.863 %         1 x     2056.913 Mt        0.000 Mt      592.794 Mt    28.820 % 	 ...../glTF-LoadScene

		    1464.119 Mt    71.180 %        69 x       21.219 Mt        0.000 Mt     1464.119 Mt   100.000 % 	 ....../glTF-LoadImageData

		     243.579 Mt     8.036 %         1 x      243.579 Mt        0.000 Mt      243.579 Mt   100.000 % 	 ...../glTF-CreateScene

		      77.858 Mt     2.569 %         1 x       77.858 Mt        0.000 Mt       77.858 Mt   100.000 % 	 ...../SceneDesc-CreateScene

		       1.309 Mt     0.042 %         1 x        1.309 Mt        0.000 Mt        0.002 Mt     0.115 % 	 ..../ShadowMapRLPrep

		       1.307 Mt    99.885 %         1 x        1.307 Mt        0.000 Mt        1.259 Mt    96.321 % 	 ...../PrepareForRendering

		       0.048 Mt     3.679 %         1 x        0.048 Mt        0.000 Mt        0.048 Mt   100.000 % 	 ....../PrepareDrawBundle

		       3.837 Mt     0.123 %         1 x        3.837 Mt        0.000 Mt        0.000 Mt     0.013 % 	 ..../TexturedRLPrep

		       3.837 Mt    99.987 %         1 x        3.837 Mt        0.000 Mt        3.797 Mt    98.968 % 	 ...../PrepareForRendering

		       0.001 Mt     0.034 %         1 x        0.001 Mt        0.000 Mt        0.001 Mt   100.000 % 	 ....../PrepareMaterials

		       0.038 Mt     0.998 %         1 x        0.038 Mt        0.000 Mt        0.038 Mt   100.000 % 	 ....../PrepareDrawBundle

		       6.162 Mt     0.198 %         1 x        6.162 Mt        0.000 Mt        0.001 Mt     0.016 % 	 ..../DeferredRLPrep

		       6.161 Mt    99.984 %         1 x        6.161 Mt        0.000 Mt        5.218 Mt    84.688 % 	 ...../PrepareForRendering

		       0.001 Mt     0.015 %         1 x        0.001 Mt        0.000 Mt        0.001 Mt   100.000 % 	 ....../PrepareMaterials

		       0.867 Mt    14.069 %         1 x        0.867 Mt        0.000 Mt        0.867 Mt   100.000 % 	 ....../BuildMaterialCache

		       0.076 Mt     1.229 %         1 x        0.076 Mt        0.000 Mt        0.076 Mt   100.000 % 	 ....../PrepareDrawBundle

		      32.751 Mt     1.051 %         1 x       32.751 Mt        0.000 Mt        2.981 Mt     9.103 % 	 ..../RayTracingRLPrep

		       0.067 Mt     0.203 %         1 x        0.067 Mt        0.000 Mt        0.067 Mt   100.000 % 	 ...../PrepareAccelerationStructures

		       0.001 Mt     0.003 %         1 x        0.001 Mt        0.000 Mt        0.001 Mt   100.000 % 	 ...../PrepareCache

		      19.203 Mt    58.633 %         1 x       19.203 Mt        0.000 Mt       19.203 Mt   100.000 % 	 ...../PipelineBuild

		       0.762 Mt     2.326 %         1 x        0.762 Mt        0.000 Mt        0.762 Mt   100.000 % 	 ...../BuildCache

		       6.486 Mt    19.803 %         1 x        6.486 Mt        0.000 Mt        0.000 Mt     0.008 % 	 ...../BuildAccelerationStructures

		       6.485 Mt    99.992 %         1 x        6.485 Mt        0.000 Mt        3.798 Mt    58.563 % 	 ....../AccelerationStructureBuild

		       1.841 Mt    28.386 %         1 x        1.841 Mt        0.000 Mt        1.841 Mt   100.000 % 	 ......./BuildBottomLevelAS

		       0.846 Mt    13.052 %         1 x        0.846 Mt        0.000 Mt        0.846 Mt   100.000 % 	 ......./BuildTopLevelAS

		       3.252 Mt     9.929 %         1 x        3.252 Mt        0.000 Mt        3.252 Mt   100.000 % 	 ...../BuildShaderTables

		      37.927 Mt     1.217 %         1 x       37.927 Mt        0.000 Mt       37.927 Mt   100.000 % 	 ..../GpuSidePrep

	WindowThread:

		   26618.527 Mt   100.000 %         1 x    26618.527 Mt    26618.527 Mt    26618.527 Mt   100.000 % 	 /

	GpuThread:

		   21387.517 Mt   100.000 %      2732 x        7.829 Mt        7.537 Mt      147.431 Mt     0.689 % 	 /

		       2.507 Mt     0.012 %         1 x        2.507 Mt        0.000 Mt        0.006 Mt     0.254 % 	 ./ScenePrep

		       2.500 Mt    99.746 %         1 x        2.500 Mt        0.000 Mt        0.001 Mt     0.049 % 	 ../AccelerationStructureBuild

		       2.410 Mt    96.394 %         1 x        2.410 Mt        0.000 Mt        2.410 Mt   100.000 % 	 .../BuildBottomLevelASGpu

		       0.089 Mt     3.557 %         1 x        0.089 Mt        0.000 Mt        0.089 Mt   100.000 % 	 .../BuildTopLevelASGpu

		   21232.610 Mt    99.276 %      2732 x        7.772 Mt        7.528 Mt        3.856 Mt     0.018 % 	 ./RayTracingGpu

		   21228.754 Mt    99.982 %      2732 x        7.770 Mt        7.524 Mt    21228.754 Mt   100.000 % 	 ../RayTracingRLGpu

		       4.969 Mt     0.023 %      2732 x        0.002 Mt        0.006 Mt        4.969 Mt   100.000 % 	 ./Present


	============================


