Configuration: 
Command line parameters  = E:\programming\FIT-DIP\Masters-DXR-Main-git\build\Measurement\x64\Release\Measurement.exe --scene Sponza/Sponza.gltf --profile-automation --profile-camera-track Sponza/SponzaPreset.trk --rt-mode-pure --width 2560 --height 1440 --profile-output PresetSponza1440RpHigh --quality-preset High 
Build type               = Release
Device name              = NVIDIA GeForce RTX 2080 Ti
Device memory MB         = 11049
Ray Tracing backend      = Hardware
Window width             = 2560
Window height            = 1421
Window title             = Direct3D App
Back-buffer count        = 3
Fill Triangles           = enabled
Triangles Font Clock-Wise= enabled
Fast Build AS            = disabled
Target FPS               = 0
Target UPS               = 60
Tearing                  = enabled
VSync                    = disabled
GUI rendering            = disabled
Debug layer              = disabled
GPU validation           = disabled
GPU profiling            = enabled
Render mode              = Ray Tracing
Loaded scene file        = Sponza/Sponza.gltf
Using testing scene      = disabled
Duplication              = 1
Duplication in cube      = disabled
Duplication offset       = 10
BLAS duplication         = disabled
Rays per pixel           = 14
Quality preset           = High
Rasterization Quality    : 
  AO                     = enabled
  AO kernel size         = 4
  Shadows                = enabled
  Shadow map resolution  = 4096
  Shadow PCF kernel size = 4
  Reflections            = enabled
Ray Tracing Quality      : 
  AO                     = enabled
  AO rays                = 4
  AO kernel size         = 4
  Shadows                = enabled
  Shadow rays            = 4
  Shadow kernel size     = 4
  Reflections            = enabled

ProfilingAggregator: 
Render	,	7.4231	,	7.2605	,	6.6847	,	6.271	,	6.1952	,	7.1184	,	7.1843	,	8.1093	,	8.4887	,	8.1898	,	8.1	,	8.1764	,	8.2672	,	8.4884	,	7.0576	,	6.0333	,	8.6083	,	8.3458	,	6.0725	,	6.2769	,	6.573	,	6.2482	,	6.1214
Update	,	0.0812	,	0.081	,	0.0786	,	0.0785	,	0.0416	,	0.0865	,	0.0798	,	0.0803	,	0.079	,	0.0823	,	0.0445	,	0.0795	,	0.0847	,	0.0453	,	0.0471	,	0.0362	,	0.0414	,	0.0807	,	0.0425	,	0.043	,	0.0805	,	0.0437	,	0.0759
RayTracing	,	0.0843	,	0.0892	,	0.0804	,	0.0358	,	0.0384	,	0.0919	,	0.0973	,	0.097	,	0.0806	,	0.0687	,	0.065	,	0.0584	,	0.106	,	0.0862	,	0.0889	,	0.0301	,	0.0447	,	0.0701	,	0.0326	,	0.0807	,	0.0803	,	0.0374	,	0.0401
RayTracingGpu	,	6.70931	,	6.54374	,	6.12688	,	5.94362	,	5.70534	,	6.46694	,	6.38134	,	7.45034	,	7.87389	,	7.67389	,	7.56848	,	7.58518	,	7.47155	,	7.82189	,	6.44054	,	5.77526	,	8.09232	,	7.73315	,	5.69354	,	5.7433	,	5.90288	,	5.72992	,	5.58618
RayTracingRLGpu	,	6.70864	,	6.54304	,	6.12525	,	5.94195	,	5.70058	,	6.46544	,	6.37984	,	7.44829	,	7.8729	,	7.6729	,	7.56778	,	7.58234	,	7.47082	,	7.82122	,	6.43987	,	5.7703	,	8.09037	,	7.73114	,	5.6919	,	5.74141	,	5.90122	,	5.72813	,	5.58438
Index	,	0	,	1	,	2	,	3	,	4	,	5	,	6	,	7	,	8	,	9	,	10	,	11	,	12	,	13	,	14	,	15	,	16	,	17	,	18	,	19	,	20	,	21	,	22

ProfilingBuildAggregator: 
BuildBottomLevelAS	,	1.5728
BuildBottomLevelASGpu	,	2.36352
BuildTopLevelAS	,	1.0259
BuildTopLevelASGpu	,	0.089824
Duplication	,	1
Triangles	,	262267
Meshes	,	103
BottomLevels	,	1
TopLevelSize	,	64
BottomLevelsSize	,	18390656
Index	,	0

FpsAggregator: 
FPS	,	100	,	132	,	143	,	147	,	157	,	150	,	151	,	128	,	121	,	122	,	121	,	122	,	121	,	122	,	130	,	165	,	132	,	114	,	142	,	174	,	156	,	160	,	160
UPS	,	59	,	60	,	61	,	60	,	60	,	60	,	60	,	60	,	61	,	60	,	61	,	60	,	60	,	60	,	60	,	61	,	60	,	60	,	60	,	60	,	61	,	60	,	60
FrameTime	,	10.0324	,	7.58265	,	7.02406	,	6.81492	,	6.40769	,	6.69651	,	6.62831	,	7.83314	,	8.32979	,	8.26196	,	8.28072	,	8.22396	,	8.27613	,	8.2513	,	7.69544	,	6.07614	,	7.63505	,	8.81062	,	7.06894	,	5.74763	,	6.41795	,	6.28585	,	6.2725
GigaRays/s	,	5.07644	,	6.71647	,	7.2506	,	7.47311	,	7.94805	,	7.60525	,	7.68351	,	6.50169	,	6.11404	,	6.16423	,	6.15027	,	6.19271	,	6.15368	,	6.17219	,	6.61803	,	8.38175	,	6.67037	,	5.78037	,	7.20456	,	8.86081	,	7.93535	,	8.10211	,	8.11936
Index	,	0	,	1	,	2	,	3	,	4	,	5	,	6	,	7	,	8	,	9	,	10	,	11	,	12	,	13	,	14	,	15	,	16	,	17	,	18	,	19	,	20	,	21	,	22


AccelerationStructure: 
Ray tracing acceleration structure building statistics: 
	Bottom Level Acceleration Structure: 
		Maximum [B]: 18390656
		Minimum [B]: 18390656
		Currently Allocated [B]: 18390656
		Currently Created [num]: 1
		Current Average [B]: 18390656
		Maximum Triangles [num]: 262267
		Minimum Triangles [num]: 262267
		Total Triangles [num]: 262267
		Maximum Meshes [num]: 103
		Minimum Meshes [num]: 103
		Total Meshes [num]: 103
	Top Level Acceleration Structure: 
		Maximum [B]: 64
		Minimum [B]: 64
		Currently Allocated [B]: 64
		Total Allocated [B]: 64
		Total Created [num]: 1
		Average Allocated [B]: 64
		Maximum Instances [num]: 1
		Minimum Instances [num]: 1
		Total Instances [num]: 1
	Scratch Buffer: 
		Maximum [B]: 14995072
		Minimum [B]: 14995072
		Currently Allocated [B]: 14995072
		Total Allocated [B]: 14995072
		Total Created [num]: 1
		Average Allocated [B]: 14995072
ProfilingStack: 
[PROF] Profiling statistics: 
	Scopes entered : 104448
	Scopes exited : 104447
	Overhead per scope [ticks] : 102.4
	Threads entered : 3
	Threads exited : 0
	============================
		         Sum[Mt]   Parent[%]  Samples[num]       Avg[Mt]   Last/Curr[Mt]        Self[Mt]     Self[%] 	 Name

	main:

		   26483.731 Mt   100.000 %         1 x    26483.732 Mt    26483.731 Mt        0.514 Mt     0.002 % 	 /

		   26483.242 Mt    99.998 %         1 x    26483.244 Mt    26483.242 Mt      320.061 Mt     1.209 % 	 ./Main application loop

		       2.280 Mt     0.009 %         1 x        2.280 Mt        0.000 Mt        2.280 Mt   100.000 % 	 ../TexturedRLInitialization

		       1.304 Mt     0.005 %         1 x        1.304 Mt        0.000 Mt        1.304 Mt   100.000 % 	 ../ShadowMapRLInitialization

		       4.106 Mt     0.016 %         1 x        4.106 Mt        0.000 Mt        4.106 Mt   100.000 % 	 ../RasterResolveRLInitialization

		       3.727 Mt     0.014 %         1 x        3.727 Mt        0.000 Mt        3.727 Mt   100.000 % 	 ../ResolveRLInitialization

		   23104.429 Mt    87.242 %     77574 x        0.298 Mt        6.499 Mt      206.755 Mt     0.895 % 	 ../MainLoop

		     157.755 Mt     0.683 %      1386 x        0.114 Mt        0.041 Mt      157.511 Mt    99.845 % 	 .../Update

		       0.245 Mt     0.155 %         1 x        0.245 Mt        0.000 Mt        0.245 Mt   100.000 % 	 ..../GuiModelApply

		   22739.919 Mt    98.422 %      3172 x        7.169 Mt        6.374 Mt      247.851 Mt     1.090 % 	 .../Render

		     206.406 Mt     0.908 %      3172 x        0.065 Mt        0.106 Mt       13.526 Mt     6.553 % 	 ..../RayTracing

		     192.880 Mt    93.447 %      3172 x        0.061 Mt        0.100 Mt      190.708 Mt    98.874 % 	 ...../RayCasting

		       2.172 Mt     1.126 %      3172 x        0.001 Mt        0.001 Mt        2.172 Mt   100.000 % 	 ....../RayTracingRLPrep

		   22285.662 Mt    98.002 %      3172 x        7.026 Mt        6.144 Mt    22285.662 Mt   100.000 % 	 ..../Present

		       3.740 Mt     0.014 %         1 x        3.740 Mt        0.000 Mt        3.740 Mt   100.000 % 	 ../AmbientOcclusionRLInitialization

		       5.125 Mt     0.019 %         1 x        5.125 Mt        0.000 Mt        5.125 Mt   100.000 % 	 ../DeferredRLInitialization

		      51.335 Mt     0.194 %         1 x       51.335 Mt        0.000 Mt        2.223 Mt     4.331 % 	 ../RayTracingRLInitialization

		      49.111 Mt    95.669 %         1 x       49.111 Mt        0.000 Mt       49.111 Mt   100.000 % 	 .../PipelineBuild

		    2987.136 Mt    11.279 %         1 x     2987.136 Mt        0.000 Mt        0.226 Mt     0.008 % 	 ../Initialization

		    2986.910 Mt    99.992 %         1 x     2986.910 Mt        0.000 Mt        2.530 Mt     0.085 % 	 .../ScenePrep

		    2914.754 Mt    97.584 %         1 x     2914.754 Mt        0.000 Mt      570.845 Mt    19.585 % 	 ..../SceneLoad

		    2042.364 Mt    70.070 %         1 x     2042.364 Mt        0.000 Mt      580.961 Mt    28.446 % 	 ...../glTF-LoadScene

		    1461.403 Mt    71.554 %        69 x       21.180 Mt        0.000 Mt     1461.403 Mt   100.000 % 	 ....../glTF-LoadImageData

		     220.971 Mt     7.581 %         1 x      220.971 Mt        0.000 Mt      220.971 Mt   100.000 % 	 ...../glTF-CreateScene

		      80.574 Mt     2.764 %         1 x       80.574 Mt        0.000 Mt       80.574 Mt   100.000 % 	 ...../SceneDesc-CreateScene

		       1.150 Mt     0.039 %         1 x        1.150 Mt        0.000 Mt        0.001 Mt     0.078 % 	 ..../ShadowMapRLPrep

		       1.149 Mt    99.922 %         1 x        1.149 Mt        0.000 Mt        1.111 Mt    96.633 % 	 ...../PrepareForRendering

		       0.039 Mt     3.367 %         1 x        0.039 Mt        0.000 Mt        0.039 Mt   100.000 % 	 ....../PrepareDrawBundle

		       3.630 Mt     0.122 %         1 x        3.630 Mt        0.000 Mt        0.001 Mt     0.019 % 	 ..../TexturedRLPrep

		       3.630 Mt    99.981 %         1 x        3.630 Mt        0.000 Mt        3.591 Mt    98.926 % 	 ...../PrepareForRendering

		       0.001 Mt     0.039 %         1 x        0.001 Mt        0.000 Mt        0.001 Mt   100.000 % 	 ....../PrepareMaterials

		       0.038 Mt     1.036 %         1 x        0.038 Mt        0.000 Mt        0.038 Mt   100.000 % 	 ....../PrepareDrawBundle

		       2.621 Mt     0.088 %         1 x        2.621 Mt        0.000 Mt        0.000 Mt     0.015 % 	 ..../DeferredRLPrep

		       2.620 Mt    99.985 %         1 x        2.620 Mt        0.000 Mt        2.036 Mt    77.696 % 	 ...../PrepareForRendering

		       0.001 Mt     0.023 %         1 x        0.001 Mt        0.000 Mt        0.001 Mt   100.000 % 	 ....../PrepareMaterials

		       0.520 Mt    19.850 %         1 x        0.520 Mt        0.000 Mt        0.520 Mt   100.000 % 	 ....../BuildMaterialCache

		       0.064 Mt     2.431 %         1 x        0.064 Mt        0.000 Mt        0.064 Mt   100.000 % 	 ....../PrepareDrawBundle

		      28.600 Mt     0.958 %         1 x       28.600 Mt        0.000 Mt        3.384 Mt    11.831 % 	 ..../RayTracingRLPrep

		       0.077 Mt     0.270 %         1 x        0.077 Mt        0.000 Mt        0.077 Mt   100.000 % 	 ...../PrepareAccelerationStructures

		       0.001 Mt     0.005 %         1 x        0.001 Mt        0.000 Mt        0.001 Mt   100.000 % 	 ...../PrepareCache

		      16.174 Mt    56.553 %         1 x       16.174 Mt        0.000 Mt       16.174 Mt   100.000 % 	 ...../PipelineBuild

		       0.626 Mt     2.189 %         1 x        0.626 Mt        0.000 Mt        0.626 Mt   100.000 % 	 ...../BuildCache

		       6.353 Mt    22.214 %         1 x        6.353 Mt        0.000 Mt        0.001 Mt     0.009 % 	 ...../BuildAccelerationStructures

		       6.353 Mt    99.991 %         1 x        6.353 Mt        0.000 Mt        3.754 Mt    59.093 % 	 ....../AccelerationStructureBuild

		       1.573 Mt    24.758 %         1 x        1.573 Mt        0.000 Mt        1.573 Mt   100.000 % 	 ......./BuildBottomLevelAS

		       1.026 Mt    16.149 %         1 x        1.026 Mt        0.000 Mt        1.026 Mt   100.000 % 	 ......./BuildTopLevelAS

		       1.984 Mt     6.937 %         1 x        1.984 Mt        0.000 Mt        1.984 Mt   100.000 % 	 ...../BuildShaderTables

		      33.624 Mt     1.126 %         1 x       33.624 Mt        0.000 Mt       33.624 Mt   100.000 % 	 ..../GpuSidePrep

	WindowThread:

		   26481.087 Mt   100.000 %         1 x    26481.087 Mt    26481.087 Mt    26481.087 Mt   100.000 % 	 /

	GpuThread:

		   21177.946 Mt   100.000 %      3172 x        6.677 Mt        5.692 Mt      143.724 Mt     0.679 % 	 /

		       2.461 Mt     0.012 %         1 x        2.461 Mt        0.000 Mt        0.006 Mt     0.259 % 	 ./ScenePrep

		       2.455 Mt    99.741 %         1 x        2.455 Mt        0.000 Mt        0.001 Mt     0.050 % 	 ../AccelerationStructureBuild

		       2.364 Mt    96.291 %         1 x        2.364 Mt        0.000 Mt        2.364 Mt   100.000 % 	 .../BuildBottomLevelASGpu

		       0.090 Mt     3.659 %         1 x        0.090 Mt        0.000 Mt        0.090 Mt   100.000 % 	 .../BuildTopLevelASGpu

		   21025.728 Mt    99.281 %      3172 x        6.629 Mt        5.689 Mt        4.471 Mt     0.021 % 	 ./RayTracingGpu

		   21021.257 Mt    99.979 %      3172 x        6.627 Mt        5.688 Mt    21021.257 Mt   100.000 % 	 ../RayTracingRLGpu

		       6.033 Mt     0.028 %      3172 x        0.002 Mt        0.002 Mt        6.033 Mt   100.000 % 	 ./Present


	============================


