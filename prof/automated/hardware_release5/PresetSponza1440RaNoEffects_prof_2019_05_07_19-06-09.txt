Configuration: 
Command line parameters  = E:\programming\FIT-DIP\Masters-DXR-Main-git\build\Measurement\x64\Release\Measurement.exe --scene Sponza/Sponza.gltf --profile-automation --profile-camera-track Sponza/SponzaPreset.trk --width 2560 --height 1440 --profile-output PresetSponza1440RaNoEffects --quality-preset NoEffects 
Build type               = Release
Device name              = NVIDIA GeForce RTX 2080 Ti
Device memory MB         = 11049
Ray Tracing backend      = Hardware
Window width             = 2560
Window height            = 1421
Window title             = Direct3D App
Back-buffer count        = 3
Fill Triangles           = enabled
Triangles Font Clock-Wise= enabled
Fast Build AS            = disabled
Target FPS               = 0
Target UPS               = 60
Tearing                  = enabled
VSync                    = disabled
GUI rendering            = disabled
Debug layer              = disabled
GPU validation           = disabled
GPU profiling            = enabled
Render mode              = Rasterization
Loaded scene file        = Sponza/Sponza.gltf
Using testing scene      = disabled
Duplication              = 1
Duplication in cube      = disabled
Duplication offset       = 10
BLAS duplication         = disabled
Rays per pixel           = 0
Quality preset           = NoEffects
Rasterization Quality    : 
  AO                     = disabled
  AO kernel size         = 1
  Shadows                = disabled
  Shadow map resolution  = 1
  Shadow PCF kernel size = 0
  Reflections            = disabled
Ray Tracing Quality      : 
  AO                     = disabled
  AO rays                = 0
  AO kernel size         = 4
  Shadows                = disabled
  Shadow rays            = 0
  Shadow kernel size     = 4
  Reflections            = disabled

ProfilingAggregator: 
Render	,	4.4155	,	3.6072	,	3.6568	,	2.7497	,	2.626	,	2.8512	,	3.2047	,	3.6144	,	3.2261	,	2.6777	,	2.5752	,	2.8976	,	3.2274	,	3.6038	,	3.3623	,	2.6978	,	3.1593	,	3.0524	,	2.6329	,	4.0655	,	5.9722	,	5.5258	,	5.5239
Update	,	0.0793	,	0.0791	,	0.0793	,	0.0772	,	0.0805	,	0.0796	,	0.0786	,	0.0777	,	0.0805	,	0.071	,	0.0788	,	0.0783	,	0.079	,	0.0764	,	0.0806	,	0.0775	,	0.0803	,	0.08	,	0.0792	,	0.0659	,	0.0757	,	0.0775	,	0.0774
DeferredRLGpu	,	2.76134	,	2.50253	,	2.53789	,	1.4689	,	1.34531	,	1.58947	,	1.94854	,	2.38531	,	2.0681	,	1.52221	,	1.34138	,	1.73341	,	2.0967	,	2.38218	,	2.18131	,	1.52422	,	1.9425	,	1.87722	,	1.52074	,	3.0519	,	4.32278	,	4.2384	,	4.06758
Index	,	0	,	1	,	2	,	3	,	4	,	5	,	6	,	7	,	8	,	9	,	10	,	11	,	12	,	13	,	14	,	15	,	16	,	17	,	18	,	19	,	20	,	21	,	22

ProfilingBuildAggregator: 
BuildBottomLevelAS	,	1.5615
BuildBottomLevelASGpu	,	2.37501
BuildTopLevelAS	,	0.9882
BuildTopLevelASGpu	,	0.088896
Duplication	,	1
Triangles	,	262267
Meshes	,	103
BottomLevels	,	1
TopLevelSize	,	64
BottomLevelsSize	,	18390656
Index	,	0

FpsAggregator: 
FPS	,	172	,	244	,	282	,	314	,	381	,	365	,	345	,	269	,	282	,	342	,	381	,	357	,	307	,	280	,	267	,	343	,	323	,	307	,	332	,	344	,	211	,	187	,	188
UPS	,	59	,	60	,	60	,	61	,	60	,	60	,	60	,	60	,	60	,	60	,	61	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60
FrameTime	,	5.81859	,	4.09954	,	3.55491	,	3.18839	,	2.63078	,	2.74069	,	2.90748	,	3.73041	,	3.55754	,	2.92706	,	2.63113	,	2.80585	,	3.2579	,	3.57585	,	3.75474	,	2.91994	,	3.09624	,	3.26059	,	3.01812	,	2.91099	,	4.75306	,	5.35494	,	5.32931
GigaRays/s	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0
Index	,	0	,	1	,	2	,	3	,	4	,	5	,	6	,	7	,	8	,	9	,	10	,	11	,	12	,	13	,	14	,	15	,	16	,	17	,	18	,	19	,	20	,	21	,	22


AccelerationStructure: 
Ray tracing acceleration structure building statistics: 
	Bottom Level Acceleration Structure: 
		Maximum [B]: 18390656
		Minimum [B]: 18390656
		Currently Allocated [B]: 18390656
		Currently Created [num]: 1
		Current Average [B]: 18390656
		Maximum Triangles [num]: 262267
		Minimum Triangles [num]: 262267
		Total Triangles [num]: 262267
		Maximum Meshes [num]: 103
		Minimum Meshes [num]: 103
		Total Meshes [num]: 103
	Top Level Acceleration Structure: 
		Maximum [B]: 64
		Minimum [B]: 64
		Currently Allocated [B]: 64
		Total Allocated [B]: 64
		Total Created [num]: 1
		Average Allocated [B]: 64
		Maximum Instances [num]: 1
		Minimum Instances [num]: 1
		Total Instances [num]: 1
	Scratch Buffer: 
		Maximum [B]: 14995072
		Minimum [B]: 14995072
		Currently Allocated [B]: 14995072
		Total Allocated [B]: 14995072
		Total Created [num]: 1
		Average Allocated [B]: 14995072
ProfilingStack: 
[PROF] Profiling statistics: 
	Scopes entered : 160270
	Scopes exited : 160269
	Overhead per scope [ticks] : 115.9
	Threads entered : 3
	Threads exited : 0
	============================
		         Sum[Mt]   Parent[%]  Samples[num]       Avg[Mt]   Last/Curr[Mt]        Self[Mt]     Self[%] 	 Name

	main:

		   25955.459 Mt   100.000 %         1 x    25955.460 Mt    25955.459 Mt        0.434 Mt     0.002 % 	 /

		   25955.057 Mt    99.998 %         1 x    25955.059 Mt    25955.057 Mt      266.069 Mt     1.025 % 	 ./Main application loop

		       2.268 Mt     0.009 %         1 x        2.268 Mt        0.000 Mt        2.268 Mt   100.000 % 	 ../TexturedRLInitialization

		       1.356 Mt     0.005 %         1 x        1.356 Mt        0.000 Mt        1.356 Mt   100.000 % 	 ../ShadowMapRLInitialization

		       4.430 Mt     0.017 %         1 x        4.430 Mt        0.000 Mt        4.430 Mt   100.000 % 	 ../RasterResolveRLInitialization

		       3.809 Mt     0.015 %         1 x        3.809 Mt        0.000 Mt        3.809 Mt   100.000 % 	 ../ResolveRLInitialization

		   23050.500 Mt    88.809 %     97360 x        0.237 Mt       18.476 Mt      394.291 Mt     1.711 % 	 ../MainLoop

		     212.862 Mt     0.923 %      1382 x        0.154 Mt        0.096 Mt      212.259 Mt    99.717 % 	 .../Update

		       0.603 Mt     0.283 %         1 x        0.603 Mt        0.000 Mt        0.603 Mt   100.000 % 	 ..../GuiModelApply

		   22443.346 Mt    97.366 %      6824 x        3.289 Mt        5.291 Mt      738.526 Mt     3.291 % 	 .../Render

		     860.394 Mt     3.834 %      6824 x        0.126 Mt        0.194 Mt      849.123 Mt    98.690 % 	 ..../Rasterization

		       7.296 Mt     0.848 %      6824 x        0.001 Mt        0.001 Mt        7.296 Mt   100.000 % 	 ...../DeferredRLPrep

		       3.974 Mt     0.462 %      6824 x        0.001 Mt        0.001 Mt        3.974 Mt   100.000 % 	 ...../ShadowMapRLPrep

		   20844.426 Mt    92.876 %      6824 x        3.055 Mt        4.935 Mt    20844.426 Mt   100.000 % 	 ..../Present

		       2.830 Mt     0.011 %         1 x        2.830 Mt        0.000 Mt        2.830 Mt   100.000 % 	 ../AmbientOcclusionRLInitialization

		       4.964 Mt     0.019 %         1 x        4.964 Mt        0.000 Mt        4.964 Mt   100.000 % 	 ../DeferredRLInitialization

		      55.132 Mt     0.212 %         1 x       55.132 Mt        0.000 Mt        2.382 Mt     4.321 % 	 ../RayTracingRLInitialization

		      52.749 Mt    95.679 %         1 x       52.749 Mt        0.000 Mt       52.749 Mt   100.000 % 	 .../PipelineBuild

		    2563.701 Mt     9.877 %         1 x     2563.701 Mt        0.000 Mt        0.244 Mt     0.010 % 	 ../Initialization

		    2563.457 Mt    99.990 %         1 x     2563.457 Mt        0.000 Mt        2.116 Mt     0.083 % 	 .../ScenePrep

		    2494.151 Mt    97.296 %         1 x     2494.151 Mt        0.000 Mt      239.671 Mt     9.609 % 	 ..../SceneLoad

		    2012.407 Mt    80.685 %         1 x     2012.407 Mt        0.000 Mt      562.078 Mt    27.931 % 	 ...../glTF-LoadScene

		    1450.329 Mt    72.069 %        69 x       21.019 Mt        0.000 Mt     1450.329 Mt   100.000 % 	 ....../glTF-LoadImageData

		     175.443 Mt     7.034 %         1 x      175.443 Mt        0.000 Mt      175.443 Mt   100.000 % 	 ...../glTF-CreateScene

		      66.630 Mt     2.671 %         1 x       66.630 Mt        0.000 Mt       66.630 Mt   100.000 % 	 ...../SceneDesc-CreateScene

		       1.147 Mt     0.045 %         1 x        1.147 Mt        0.000 Mt        0.001 Mt     0.070 % 	 ..../ShadowMapRLPrep

		       1.146 Mt    99.930 %         1 x        1.146 Mt        0.000 Mt        1.110 Mt    96.893 % 	 ...../PrepareForRendering

		       0.036 Mt     3.107 %         1 x        0.036 Mt        0.000 Mt        0.036 Mt   100.000 % 	 ....../PrepareDrawBundle

		       3.537 Mt     0.138 %         1 x        3.537 Mt        0.000 Mt        0.001 Mt     0.014 % 	 ..../TexturedRLPrep

		       3.536 Mt    99.986 %         1 x        3.536 Mt        0.000 Mt        3.499 Mt    98.954 % 	 ...../PrepareForRendering

		       0.001 Mt     0.031 %         1 x        0.001 Mt        0.000 Mt        0.001 Mt   100.000 % 	 ....../PrepareMaterials

		       0.036 Mt     1.015 %         1 x        0.036 Mt        0.000 Mt        0.036 Mt   100.000 % 	 ....../PrepareDrawBundle

		       3.888 Mt     0.152 %         1 x        3.888 Mt        0.000 Mt        0.001 Mt     0.015 % 	 ..../DeferredRLPrep

		       3.887 Mt    99.985 %         1 x        3.887 Mt        0.000 Mt        3.386 Mt    87.121 % 	 ...../PrepareForRendering

		       0.001 Mt     0.015 %         1 x        0.001 Mt        0.000 Mt        0.001 Mt   100.000 % 	 ....../PrepareMaterials

		       0.448 Mt    11.528 %         1 x        0.448 Mt        0.000 Mt        0.448 Mt   100.000 % 	 ....../BuildMaterialCache

		       0.052 Mt     1.335 %         1 x        0.052 Mt        0.000 Mt        0.052 Mt   100.000 % 	 ....../PrepareDrawBundle

		      26.673 Mt     1.041 %         1 x       26.673 Mt        0.000 Mt        2.465 Mt     9.241 % 	 ..../RayTracingRLPrep

		       0.062 Mt     0.234 %         1 x        0.062 Mt        0.000 Mt        0.062 Mt   100.000 % 	 ...../PrepareAccelerationStructures

		       0.001 Mt     0.003 %         1 x        0.001 Mt        0.000 Mt        0.001 Mt   100.000 % 	 ...../PrepareCache

		      12.833 Mt    48.110 %         1 x       12.833 Mt        0.000 Mt       12.833 Mt   100.000 % 	 ...../PipelineBuild

		       0.659 Mt     2.472 %         1 x        0.659 Mt        0.000 Mt        0.659 Mt   100.000 % 	 ...../BuildCache

		       9.260 Mt    34.718 %         1 x        9.260 Mt        0.000 Mt        0.001 Mt     0.005 % 	 ...../BuildAccelerationStructures

		       9.260 Mt    99.995 %         1 x        9.260 Mt        0.000 Mt        6.710 Mt    72.465 % 	 ....../AccelerationStructureBuild

		       1.562 Mt    16.863 %         1 x        1.562 Mt        0.000 Mt        1.562 Mt   100.000 % 	 ......./BuildBottomLevelAS

		       0.988 Mt    10.672 %         1 x        0.988 Mt        0.000 Mt        0.988 Mt   100.000 % 	 ......./BuildTopLevelAS

		       1.393 Mt     5.222 %         1 x        1.393 Mt        0.000 Mt        1.393 Mt   100.000 % 	 ...../BuildShaderTables

		      31.946 Mt     1.246 %         1 x       31.946 Mt        0.000 Mt       31.946 Mt   100.000 % 	 ..../GpuSidePrep

	WindowThread:

		   25954.027 Mt   100.000 %         1 x    25954.027 Mt    25954.027 Mt    25954.027 Mt   100.000 % 	 /

	GpuThread:

		   17521.973 Mt   100.000 %      6824 x        2.568 Mt        4.320 Mt      161.963 Mt     0.924 % 	 /

		       2.473 Mt     0.014 %         1 x        2.473 Mt        0.000 Mt        0.008 Mt     0.318 % 	 ./ScenePrep

		       2.465 Mt    99.682 %         1 x        2.465 Mt        0.000 Mt        0.001 Mt     0.052 % 	 ../AccelerationStructureBuild

		       2.375 Mt    96.342 %         1 x        2.375 Mt        0.000 Mt        2.375 Mt   100.000 % 	 .../BuildBottomLevelASGpu

		       0.089 Mt     3.606 %         1 x        0.089 Mt        0.000 Mt        0.089 Mt   100.000 % 	 .../BuildTopLevelASGpu

		   17309.705 Mt    98.789 %      6824 x        2.537 Mt        4.317 Mt       13.387 Mt     0.077 % 	 ./RasterizationGpu

		   14082.828 Mt    81.358 %      6824 x        2.064 Mt        3.852 Mt    14082.828 Mt   100.000 % 	 ../DeferredRLGpu

		    3213.490 Mt    18.565 %      6824 x        0.471 Mt        0.464 Mt     3213.490 Mt   100.000 % 	 ../RasterResolveRLGpu

		      47.833 Mt     0.273 %      6824 x        0.007 Mt        0.003 Mt       47.833 Mt   100.000 % 	 ./Present


	============================


