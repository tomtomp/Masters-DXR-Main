Configuration: 
Command line parameters  = E:\programming\FIT-DIP\Masters-DXR-Main-git\build\Measurement\x64\Release\Measurement.exe --scene Cube/Cube.gltf --profile-automation --profile-camera-track Cube/Cube.trk --rt-mode-pure --width 2560 --height 1440 --profile-output PresetCube1440RpSoftShadows8 --quality-preset SoftShadows8 
Build type               = Release
Device name              = NVIDIA GeForce RTX 2080 Ti
Device memory MB         = 11049
Ray Tracing backend      = Hardware
Window width             = 2560
Window height            = 1421
Window title             = Direct3D App
Back-buffer count        = 3
Fill Triangles           = enabled
Triangles Font Clock-Wise= enabled
Fast Build AS            = disabled
Target FPS               = 0
Target UPS               = 60
Tearing                  = enabled
VSync                    = disabled
GUI rendering            = disabled
Debug layer              = disabled
GPU validation           = disabled
GPU profiling            = enabled
Render mode              = Ray Tracing
Loaded scene file        = Cube/Cube.gltf
Using testing scene      = disabled
Duplication              = 1
Duplication in cube      = disabled
Duplication offset       = 10
BLAS duplication         = disabled
Rays per pixel           = 9
Quality preset           = SoftShadows8
Rasterization Quality    : 
  AO                     = disabled
  AO kernel size         = 1
  Shadows                = enabled
  Shadow map resolution  = 8192
  Shadow PCF kernel size = 8
  Reflections            = disabled
Ray Tracing Quality      : 
  AO                     = disabled
  AO rays                = 0
  AO kernel size         = 8
  Shadows                = enabled
  Shadow rays            = 8
  Shadow kernel size     = 8
  Reflections            = disabled

ProfilingAggregator: 
Render	,	1.4926	,	1.6594	,	1.8371	,	1.9102	,	2.2119	,	2.4217	,	2.3182	,	2.5341	,	2.8238	,	2.5574	,	2.2819	,	2.4356	,	2.0589	,	1.8774	,	1.8585	,	2.8075	,	2.8223	,	3.4919	,	3.2635	,	3.3107	,	3.2437	,	2.2681	,	2.045	,	1.6539	,	1.271	,	0.9676	,	0.9865	,	0.8773	,	0.8343	,	0.8072
Update	,	0.0611	,	0.0746	,	0.0468	,	0.0812	,	0.0802	,	0.0817	,	0.0639	,	0.0778	,	0.0914	,	0.0811	,	0.0811	,	0.0831	,	0.0828	,	0.0721	,	0.041	,	0.0438	,	0.041	,	0.0811	,	0.0795	,	0.0803	,	0.0556	,	0.0806	,	0.083	,	0.0416	,	0.0792	,	0.0754	,	0.0752	,	0.0754	,	0.0761	,	0.0382
RayTracing	,	0.0748	,	0.0308	,	0.0367	,	0.0517	,	0.0733	,	0.0774	,	0.0745	,	0.075	,	0.0898	,	0.0787	,	0.0736	,	0.0775	,	0.0779	,	0.0746	,	0.032	,	0.0351	,	0.0445	,	0.1058	,	0.0925	,	0.0774	,	0.0482	,	0.0719	,	0.0765	,	0.0741	,	0.0349	,	0.0264	,	0.0571	,	0.0537	,	0.056	,	0.0571
RayTracingGpu	,	1.04621	,	1.3623	,	1.42541	,	1.4744	,	1.54736	,	1.89283	,	1.73683	,	1.99568	,	2.3079	,	2.06688	,	1.80566	,	1.97827	,	1.59904	,	1.41952	,	1.59914	,	2.09981	,	2.39133	,	2.81725	,	2.72496	,	2.7184	,	2.55894	,	1.69379	,	1.40765	,	1.11334	,	0.9376	,	0.754784	,	0.616768	,	0.5384	,	0.494048	,	0.49248
RayTracingRLGpu	,	1.04547	,	1.36074	,	1.42454	,	1.47286	,	1.5465	,	1.8921	,	1.736	,	1.99491	,	2.30717	,	2.06538	,	1.80496	,	1.9767	,	1.59814	,	1.41786	,	1.59763	,	2.09853	,	2.38947	,	2.81654	,	2.72221	,	2.71766	,	2.55795	,	1.69312	,	1.40678	,	1.11254	,	0.936768	,	0.753952	,	0.616096	,	0.537568	,	0.492544	,	0.491008
Index	,	0	,	1	,	2	,	3	,	4	,	5	,	6	,	7	,	8	,	9	,	10	,	11	,	12	,	13	,	14	,	15	,	16	,	17	,	18	,	19	,	20	,	21	,	22	,	23	,	24	,	25	,	26	,	27	,	28	,	29

ProfilingBuildAggregator: 
BuildBottomLevelAS	,	1.2057
BuildBottomLevelASGpu	,	0.128288
BuildTopLevelAS	,	1.1045
BuildTopLevelASGpu	,	0.060896
Duplication	,	1
Triangles	,	12
Meshes	,	1
BottomLevels	,	1
TopLevelSize	,	64
BottomLevelsSize	,	4480
Index	,	0

FpsAggregator: 
FPS	,	649	,	561	,	500	,	511	,	484	,	432	,	412	,	428	,	357	,	352	,	446	,	389	,	428	,	514	,	501	,	440	,	384	,	336	,	304	,	301	,	307	,	412	,	477	,	563	,	659	,	750	,	922	,	1070	,	1166	,	1220
UPS	,	59	,	61	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	61	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60
FrameTime	,	1.54174	,	1.78511	,	2.00181	,	1.95922	,	2.06929	,	2.31745	,	2.42766	,	2.3369	,	2.80562	,	2.8479	,	2.24671	,	2.57286	,	2.33995	,	1.94725	,	1.99894	,	2.27831	,	2.60889	,	2.98396	,	3.29027	,	3.33013	,	3.25828	,	2.43092	,	2.09838	,	1.77789	,	1.51828	,	1.3344	,	1.08461	,	0.934943	,	0.857749	,	0.820087
GigaRays/s	,	21.2356	,	18.3406	,	16.3551	,	16.7106	,	15.8218	,	14.1275	,	13.4862	,	14.0099	,	11.6694	,	11.4961	,	14.5724	,	12.7251	,	13.9917	,	16.8134	,	16.3786	,	14.3702	,	12.5493	,	10.9719	,	9.95051	,	9.83139	,	10.0482	,	13.4681	,	15.6025	,	18.415	,	21.5637	,	24.5353	,	30.1857	,	35.018	,	38.1695	,	39.9224
Index	,	0	,	1	,	2	,	3	,	4	,	5	,	6	,	7	,	8	,	9	,	10	,	11	,	12	,	13	,	14	,	15	,	16	,	17	,	18	,	19	,	20	,	21	,	22	,	23	,	24	,	25	,	26	,	27	,	28	,	29


AccelerationStructure: 
Ray tracing acceleration structure building statistics: 
	Bottom Level Acceleration Structure: 
		Maximum [B]: 4480
		Minimum [B]: 4480
		Currently Allocated [B]: 4480
		Currently Created [num]: 1
		Current Average [B]: 4480
		Maximum Triangles [num]: 12
		Minimum Triangles [num]: 12
		Total Triangles [num]: 12
		Maximum Meshes [num]: 1
		Minimum Meshes [num]: 1
		Total Meshes [num]: 1
	Top Level Acceleration Structure: 
		Maximum [B]: 64
		Minimum [B]: 64
		Currently Allocated [B]: 64
		Total Allocated [B]: 64
		Total Created [num]: 1
		Average Allocated [B]: 64
		Maximum Instances [num]: 1
		Minimum Instances [num]: 1
		Total Instances [num]: 1
	Scratch Buffer: 
		Maximum [B]: 896
		Minimum [B]: 896
		Currently Allocated [B]: 896
		Total Allocated [B]: 896
		Total Created [num]: 1
		Average Allocated [B]: 896
ProfilingStack: 
[PROF] Profiling statistics: 
	Scopes entered : 233822
	Scopes exited : 233821
	Overhead per scope [ticks] : 102.9
	Threads entered : 3
	Threads exited : 0
	============================
		         Sum[Mt]   Parent[%]  Samples[num]       Avg[Mt]   Last/Curr[Mt]        Self[Mt]     Self[%] 	 Name

	main:

		   30489.078 Mt   100.000 %         1 x    30489.078 Mt    30489.078 Mt        0.522 Mt     0.002 % 	 /

		   30488.566 Mt    99.998 %         1 x    30488.566 Mt    30488.566 Mt      298.335 Mt     0.979 % 	 ./Main application loop

		       2.087 Mt     0.007 %         1 x        2.087 Mt        0.000 Mt        2.087 Mt   100.000 % 	 ../TexturedRLInitialization

		       1.149 Mt     0.004 %         1 x        1.149 Mt        0.000 Mt        1.149 Mt   100.000 % 	 ../ShadowMapRLInitialization

		       3.949 Mt     0.013 %         1 x        3.949 Mt        0.000 Mt        3.949 Mt   100.000 % 	 ../RasterResolveRLInitialization

		       3.703 Mt     0.012 %         1 x        3.703 Mt        0.000 Mt        3.703 Mt   100.000 % 	 ../ResolveRLInitialization

		   30033.657 Mt    98.508 %    101767 x        0.295 Mt        8.895 Mt      220.258 Mt     0.733 % 	 ../MainLoop

		     181.595 Mt     0.605 %      1802 x        0.101 Mt        0.041 Mt      181.358 Mt    99.870 % 	 .../Update

		       0.237 Mt     0.130 %         1 x        0.237 Mt        0.000 Mt        0.237 Mt   100.000 % 	 ..../GuiModelApply

		   29631.804 Mt    98.662 %     16276 x        1.821 Mt        0.852 Mt     1000.132 Mt     3.375 % 	 .../Render

		     867.583 Mt     2.928 %     16276 x        0.053 Mt        0.055 Mt       59.059 Mt     6.807 % 	 ..../RayTracing

		     808.524 Mt    93.193 %     16276 x        0.050 Mt        0.052 Mt      799.347 Mt    98.865 % 	 ...../RayCasting

		       9.177 Mt     1.135 %     16276 x        0.001 Mt        0.000 Mt        9.177 Mt   100.000 % 	 ....../RayTracingRLPrep

		   27764.088 Mt    93.697 %     16276 x        1.706 Mt        0.735 Mt    27764.088 Mt   100.000 % 	 ..../Present

		       2.737 Mt     0.009 %         1 x        2.737 Mt        0.000 Mt        2.737 Mt   100.000 % 	 ../AmbientOcclusionRLInitialization

		       5.557 Mt     0.018 %         1 x        5.557 Mt        0.000 Mt        5.557 Mt   100.000 % 	 ../DeferredRLInitialization

		      67.429 Mt     0.221 %         1 x       67.429 Mt        0.000 Mt        2.706 Mt     4.013 % 	 ../RayTracingRLInitialization

		      64.723 Mt    95.987 %         1 x       64.723 Mt        0.000 Mt       64.723 Mt   100.000 % 	 .../PipelineBuild

		      69.963 Mt     0.229 %         1 x       69.963 Mt        0.000 Mt        0.534 Mt     0.763 % 	 ../Initialization

		      69.429 Mt    99.237 %         1 x       69.429 Mt        0.000 Mt        1.202 Mt     1.732 % 	 .../ScenePrep

		      23.872 Mt    34.384 %         1 x       23.872 Mt        0.000 Mt        8.409 Mt    35.225 % 	 ..../SceneLoad

		      10.600 Mt    44.403 %         1 x       10.600 Mt        0.000 Mt        2.605 Mt    24.574 % 	 ...../glTF-LoadScene

		       7.995 Mt    75.426 %         2 x        3.998 Mt        0.000 Mt        7.995 Mt   100.000 % 	 ....../glTF-LoadImageData

		       2.893 Mt    12.116 %         1 x        2.893 Mt        0.000 Mt        2.893 Mt   100.000 % 	 ...../glTF-CreateScene

		       1.971 Mt     8.256 %         1 x        1.971 Mt        0.000 Mt        1.971 Mt   100.000 % 	 ...../SceneDesc-CreateScene

		       0.346 Mt     0.498 %         1 x        0.346 Mt        0.000 Mt        0.001 Mt     0.174 % 	 ..../ShadowMapRLPrep

		       0.345 Mt    99.826 %         1 x        0.345 Mt        0.000 Mt        0.337 Mt    97.596 % 	 ...../PrepareForRendering

		       0.008 Mt     2.404 %         1 x        0.008 Mt        0.000 Mt        0.008 Mt   100.000 % 	 ....../PrepareDrawBundle

		       1.821 Mt     2.624 %         1 x        1.821 Mt        0.000 Mt        0.000 Mt     0.027 % 	 ..../TexturedRLPrep

		       1.821 Mt    99.973 %         1 x        1.821 Mt        0.000 Mt        1.815 Mt    99.671 % 	 ...../PrepareForRendering

		       0.001 Mt     0.049 %         1 x        0.001 Mt        0.000 Mt        0.001 Mt   100.000 % 	 ....../PrepareMaterials

		       0.005 Mt     0.280 %         1 x        0.005 Mt        0.000 Mt        0.005 Mt   100.000 % 	 ....../PrepareDrawBundle

		       1.623 Mt     2.338 %         1 x        1.623 Mt        0.000 Mt        0.001 Mt     0.037 % 	 ..../DeferredRLPrep

		       1.623 Mt    99.963 %         1 x        1.623 Mt        0.000 Mt        1.268 Mt    78.139 % 	 ...../PrepareForRendering

		       0.001 Mt     0.043 %         1 x        0.001 Mt        0.000 Mt        0.001 Mt   100.000 % 	 ....../PrepareMaterials

		       0.350 Mt    21.596 %         1 x        0.350 Mt        0.000 Mt        0.350 Mt   100.000 % 	 ....../BuildMaterialCache

		       0.004 Mt     0.222 %         1 x        0.004 Mt        0.000 Mt        0.004 Mt   100.000 % 	 ....../PrepareDrawBundle

		      27.726 Mt    39.934 %         1 x       27.726 Mt        0.000 Mt        3.076 Mt    11.093 % 	 ..../RayTracingRLPrep

		       0.048 Mt     0.174 %         1 x        0.048 Mt        0.000 Mt        0.048 Mt   100.000 % 	 ...../PrepareAccelerationStructures

		       0.001 Mt     0.003 %         1 x        0.001 Mt        0.000 Mt        0.001 Mt   100.000 % 	 ...../PrepareCache

		      15.874 Mt    57.254 %         1 x       15.874 Mt        0.000 Mt       15.874 Mt   100.000 % 	 ...../PipelineBuild

		       0.449 Mt     1.620 %         1 x        0.449 Mt        0.000 Mt        0.449 Mt   100.000 % 	 ...../BuildCache

		       5.353 Mt    19.306 %         1 x        5.353 Mt        0.000 Mt        0.001 Mt     0.022 % 	 ...../BuildAccelerationStructures

		       5.351 Mt    99.978 %         1 x        5.351 Mt        0.000 Mt        3.041 Mt    56.831 % 	 ....../AccelerationStructureBuild

		       1.206 Mt    22.530 %         1 x        1.206 Mt        0.000 Mt        1.206 Mt   100.000 % 	 ......./BuildBottomLevelAS

		       1.105 Mt    20.639 %         1 x        1.105 Mt        0.000 Mt        1.105 Mt   100.000 % 	 ......./BuildTopLevelAS

		       2.925 Mt    10.551 %         1 x        2.925 Mt        0.000 Mt        2.925 Mt   100.000 % 	 ...../BuildShaderTables

		      12.838 Mt    18.491 %         1 x       12.838 Mt        0.000 Mt       12.838 Mt   100.000 % 	 ..../GpuSidePrep

	WindowThread:

		   30487.304 Mt   100.000 %         1 x    30487.304 Mt    30487.303 Mt    30487.304 Mt   100.000 % 	 /

	GpuThread:

		   22523.556 Mt   100.000 %     16276 x        1.384 Mt        0.493 Mt      134.351 Mt     0.596 % 	 /

		       0.196 Mt     0.001 %         1 x        0.196 Mt        0.000 Mt        0.006 Mt     3.022 % 	 ./ScenePrep

		       0.190 Mt    96.978 %         1 x        0.190 Mt        0.000 Mt        0.001 Mt     0.421 % 	 ../AccelerationStructureBuild

		       0.128 Mt    67.526 %         1 x        0.128 Mt        0.000 Mt        0.128 Mt   100.000 % 	 .../BuildBottomLevelASGpu

		       0.061 Mt    32.053 %         1 x        0.061 Mt        0.000 Mt        0.061 Mt   100.000 % 	 .../BuildTopLevelASGpu

		   22359.555 Mt    99.272 %     16276 x        1.374 Mt        0.491 Mt       20.970 Mt     0.094 % 	 ./RayTracingGpu

		   22338.586 Mt    99.906 %     16276 x        1.372 Mt        0.490 Mt    22338.586 Mt   100.000 % 	 ../RayTracingRLGpu

		      29.454 Mt     0.131 %     16276 x        0.002 Mt        0.002 Mt       29.454 Mt   100.000 % 	 ./Present


	============================


