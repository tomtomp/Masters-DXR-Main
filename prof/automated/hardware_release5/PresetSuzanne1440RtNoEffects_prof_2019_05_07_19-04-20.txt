Configuration: 
Command line parameters  = E:\programming\FIT-DIP\Masters-DXR-Main-git\build\Measurement\x64\Release\Measurement.exe --scene Suzanne/Suzanne.gltf --profile-automation --profile-camera-track Suzanne/Suzanne.trk --rt-mode --width 2560 --height 1440 --profile-output PresetSuzanne1440RtNoEffects --quality-preset NoEffects 
Build type               = Release
Device name              = NVIDIA GeForce RTX 2080 Ti
Device memory MB         = 11049
Ray Tracing backend      = Hardware
Window width             = 2560
Window height            = 1421
Window title             = Direct3D App
Back-buffer count        = 3
Fill Triangles           = enabled
Triangles Font Clock-Wise= enabled
Fast Build AS            = disabled
Target FPS               = 0
Target UPS               = 60
Tearing                  = enabled
VSync                    = disabled
GUI rendering            = disabled
Debug layer              = disabled
GPU validation           = disabled
GPU profiling            = enabled
Render mode              = Ray Tracing
Loaded scene file        = Suzanne/Suzanne.gltf
Using testing scene      = disabled
Duplication              = 1
Duplication in cube      = disabled
Duplication offset       = 10
BLAS duplication         = disabled
Rays per pixel           = 0
Quality preset           = NoEffects
Rasterization Quality    : 
  AO                     = disabled
  AO kernel size         = 1
  Shadows                = disabled
  Shadow map resolution  = 1
  Shadow PCF kernel size = 0
  Reflections            = disabled
Ray Tracing Quality      : 
  AO                     = disabled
  AO rays                = 0
  AO kernel size         = 4
  Shadows                = disabled
  Shadow rays            = 0
  Shadow kernel size     = 4
  Reflections            = disabled

ProfilingAggregator: 
Render	,	0.7634	,	0.8162	,	0.826	,	0.8113	,	0.7403	,	0.4757	,	0.5238	,	0.6043	,	0.6299	,	0.5223	,	0.7244	,	0.6752	,	0.4477	,	1.6014	,	1.3917	,	1.9641	,	1.781	,	1.6516	,	2.3078	,	1.2629	,	2.0408	,	1.6384	,	1.7136	,	1.5712
Update	,	0.0613	,	0.0635	,	0.0661	,	0.0508	,	0.0647	,	0.0225	,	0.0611	,	0.0213	,	0.0596	,	0.0205	,	0.047	,	0.0601	,	0.0202	,	0.059	,	0.0368	,	0.0538	,	0.0715	,	0.07	,	0.0785	,	0.0733	,	0.0561	,	0.0482	,	0.0206	,	0.0598
RayTracing	,	0.1204	,	0.0814	,	0.0799	,	0.1054	,	0.1241	,	0.0447	,	0.0789	,	0.1167	,	0.1166	,	0.0442	,	0.1029	,	0.1158	,	0.0406	,	0.0404	,	0.0536	,	0.1128	,	0.109	,	0.1116	,	0.1688	,	0.1353	,	0.114	,	0.0953	,	0.039	,	0.0552
RayTracingGpu	,	0.34688	,	0.432832	,	0.480192	,	0.412192	,	0.304864	,	0.242656	,	0.216576	,	0.20368	,	0.236576	,	0.295808	,	0.331104	,	0.272992	,	0.222976	,	0.221152	,	0.27856	,	0.459968	,	0.567968	,	0.68576	,	1.12797	,	0.731488	,	0.57968	,	0.345344	,	0.25824	,	0.233376
DeferredRLGpu	,	0.139136	,	0.1824	,	0.200672	,	0.153184	,	0.10064	,	0.069536	,	0.052928	,	0.045504	,	0.063424	,	0.09184	,	0.106496	,	0.079584	,	0.054688	,	0.05456	,	0.0864	,	0.184096	,	0.227872	,	0.260256	,	0.407488	,	0.318656	,	0.267488	,	0.136384	,	0.08464	,	0.05472
RayTracingRLGpu	,	0.11584	,	0.131808	,	0.142176	,	0.1344	,	0.115104	,	0.106336	,	0.10336	,	0.10176	,	0.107072	,	0.115968	,	0.123488	,	0.113056	,	0.105696	,	0.104992	,	0.1112	,	0.14064	,	0.16256	,	0.196608	,	0.274016	,	0.18976	,	0.154336	,	0.117088	,	0.10704	,	0.106272
ResolveRLGpu	,	0.089344	,	0.116544	,	0.134336	,	0.121344	,	0.086944	,	0.065216	,	0.05888	,	0.053344	,	0.064384	,	0.085504	,	0.099424	,	0.07856	,	0.0608	,	0.059712	,	0.077504	,	0.131808	,	0.175264	,	0.22656	,	0.44448	,	0.220768	,	0.153152	,	0.088928	,	0.064736	,	0.059648
Index	,	0	,	1	,	2	,	3	,	4	,	5	,	6	,	7	,	8	,	9	,	10	,	11	,	12	,	13	,	14	,	15	,	16	,	17	,	18	,	19	,	20	,	21	,	22	,	23

ProfilingBuildAggregator: 
BuildBottomLevelAS	,	1.0835
BuildBottomLevelASGpu	,	0.435808
BuildTopLevelAS	,	0.9516
BuildTopLevelASGpu	,	0.0616
Duplication	,	1
Triangles	,	3936
Meshes	,	1
BottomLevels	,	1
TopLevelSize	,	64
BottomLevelsSize	,	282112
Index	,	0

FpsAggregator: 
FPS	,	1248	,	1233	,	1103	,	1060	,	1310	,	1584	,	1805	,	1804	,	1824	,	1666	,	1504	,	1556	,	1706	,	1819	,	1711	,	1335	,	976	,	849	,	619	,	638	,	774	,	1068	,	1501	,	1706
UPS	,	59	,	60	,	60	,	61	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60
FrameTime	,	0.801463	,	0.811221	,	0.906863	,	0.943853	,	0.763725	,	0.631485	,	0.554176	,	0.554637	,	0.548311	,	0.60027	,	0.665203	,	0.643	,	0.586377	,	0.549898	,	0.584923	,	0.750015	,	1.02562	,	1.17828	,	1.6179	,	1.56907	,	1.29202	,	0.936426	,	0.666434	,	0.586627
GigaRays/s	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0
Index	,	0	,	1	,	2	,	3	,	4	,	5	,	6	,	7	,	8	,	9	,	10	,	11	,	12	,	13	,	14	,	15	,	16	,	17	,	18	,	19	,	20	,	21	,	22	,	23


AccelerationStructure: 
Ray tracing acceleration structure building statistics: 
	Bottom Level Acceleration Structure: 
		Maximum [B]: 282112
		Minimum [B]: 282112
		Currently Allocated [B]: 282112
		Currently Created [num]: 1
		Current Average [B]: 282112
		Maximum Triangles [num]: 3936
		Minimum Triangles [num]: 3936
		Total Triangles [num]: 3936
		Maximum Meshes [num]: 1
		Minimum Meshes [num]: 1
		Total Meshes [num]: 1
	Top Level Acceleration Structure: 
		Maximum [B]: 64
		Minimum [B]: 64
		Currently Allocated [B]: 64
		Total Allocated [B]: 64
		Total Created [num]: 1
		Average Allocated [B]: 64
		Maximum Instances [num]: 1
		Minimum Instances [num]: 1
		Total Instances [num]: 1
	Scratch Buffer: 
		Maximum [B]: 82944
		Minimum [B]: 82944
		Currently Allocated [B]: 82944
		Total Allocated [B]: 82944
		Total Created [num]: 1
		Average Allocated [B]: 82944
ProfilingStack: 
[PROF] Profiling statistics: 
	Scopes entered : 496716
	Scopes exited : 496715
	Overhead per scope [ticks] : 122.5
	Threads entered : 3
	Threads exited : 0
	============================
		         Sum[Mt]   Parent[%]  Samples[num]       Avg[Mt]   Last/Curr[Mt]        Self[Mt]     Self[%] 	 Name

	main:

		   24577.447 Mt   100.000 %         1 x    24577.448 Mt    24577.447 Mt        0.552 Mt     0.002 % 	 /

		   24576.905 Mt    99.998 %         1 x    24576.905 Mt    24576.905 Mt      279.797 Mt     1.138 % 	 ./Main application loop

		       2.300 Mt     0.009 %         1 x        2.300 Mt        0.000 Mt        2.300 Mt   100.000 % 	 ../TexturedRLInitialization

		       1.223 Mt     0.005 %         1 x        1.223 Mt        0.000 Mt        1.223 Mt   100.000 % 	 ../ShadowMapRLInitialization

		       4.625 Mt     0.019 %         1 x        4.625 Mt        0.000 Mt        4.625 Mt   100.000 % 	 ../RasterResolveRLInitialization

		       3.736 Mt     0.015 %         1 x        3.736 Mt        0.000 Mt        3.736 Mt   100.000 % 	 ../ResolveRLInitialization

		   24025.623 Mt    97.757 %     74016 x        0.325 Mt        0.586 Mt      740.972 Mt     3.084 % 	 ../MainLoop

		     150.665 Mt     0.627 %      1442 x        0.104 Mt        0.012 Mt      150.057 Mt    99.597 % 	 .../Update

		       0.607 Mt     0.403 %         1 x        0.607 Mt        0.000 Mt        0.607 Mt   100.000 % 	 ..../GuiModelApply

		   23133.986 Mt    96.289 %     32401 x        0.714 Mt        0.540 Mt     1362.334 Mt     5.889 % 	 .../Render

		    2671.488 Mt    11.548 %     32401 x        0.082 Mt        0.055 Mt       88.136 Mt     3.299 % 	 ..../RayTracing

		    1191.165 Mt    44.588 %     32401 x        0.037 Mt        0.024 Mt     1177.846 Mt    98.882 % 	 ...../Deferred

		      13.319 Mt     1.118 %     32401 x        0.000 Mt        0.000 Mt       13.319 Mt   100.000 % 	 ....../DeferredRLPrep

		     956.612 Mt    35.808 %     32401 x        0.030 Mt        0.021 Mt      946.143 Mt    98.906 % 	 ...../RayCasting

		      10.469 Mt     1.094 %     32401 x        0.000 Mt        0.000 Mt       10.469 Mt   100.000 % 	 ....../RayTracingRLPrep

		     435.575 Mt    16.305 %     32401 x        0.013 Mt        0.009 Mt      435.575 Mt   100.000 % 	 ...../Resolve

		   19100.164 Mt    82.563 %     32401 x        0.589 Mt        0.454 Mt    19100.164 Mt   100.000 % 	 ..../Present

		       2.572 Mt     0.010 %         1 x        2.572 Mt        0.000 Mt        2.572 Mt   100.000 % 	 ../AmbientOcclusionRLInitialization

		       4.545 Mt     0.018 %         1 x        4.545 Mt        0.000 Mt        4.545 Mt   100.000 % 	 ../DeferredRLInitialization

		      64.339 Mt     0.262 %         1 x       64.339 Mt        0.000 Mt        3.278 Mt     5.094 % 	 ../RayTracingRLInitialization

		      61.061 Mt    94.906 %         1 x       61.061 Mt        0.000 Mt       61.061 Mt   100.000 % 	 .../PipelineBuild

		     188.144 Mt     0.766 %         1 x      188.144 Mt        0.000 Mt        0.563 Mt     0.299 % 	 ../Initialization

		     187.580 Mt    99.701 %         1 x      187.580 Mt        0.000 Mt        1.738 Mt     0.927 % 	 .../ScenePrep

		     107.158 Mt    57.126 %         1 x      107.158 Mt        0.000 Mt       18.665 Mt    17.418 % 	 ..../SceneLoad

		      63.246 Mt    59.021 %         1 x       63.246 Mt        0.000 Mt       11.718 Mt    18.528 % 	 ...../glTF-LoadScene

		      51.528 Mt    81.472 %         2 x       25.764 Mt        0.000 Mt       51.528 Mt   100.000 % 	 ....../glTF-LoadImageData

		      10.355 Mt     9.663 %         1 x       10.355 Mt        0.000 Mt       10.355 Mt   100.000 % 	 ...../glTF-CreateScene

		      14.892 Mt    13.897 %         1 x       14.892 Mt        0.000 Mt       14.892 Mt   100.000 % 	 ...../SceneDesc-CreateScene

		       0.797 Mt     0.425 %         1 x        0.797 Mt        0.000 Mt        0.001 Mt     0.100 % 	 ..../ShadowMapRLPrep

		       0.796 Mt    99.900 %         1 x        0.796 Mt        0.000 Mt        0.787 Mt    98.807 % 	 ...../PrepareForRendering

		       0.009 Mt     1.193 %         1 x        0.009 Mt        0.000 Mt        0.009 Mt   100.000 % 	 ....../PrepareDrawBundle

		       2.966 Mt     1.581 %         1 x        2.966 Mt        0.000 Mt        0.001 Mt     0.017 % 	 ..../TexturedRLPrep

		       2.966 Mt    99.983 %         1 x        2.966 Mt        0.000 Mt        2.961 Mt    99.845 % 	 ...../PrepareForRendering

		       0.001 Mt     0.034 %         1 x        0.001 Mt        0.000 Mt        0.001 Mt   100.000 % 	 ....../PrepareMaterials

		       0.004 Mt     0.121 %         1 x        0.004 Mt        0.000 Mt        0.004 Mt   100.000 % 	 ....../PrepareDrawBundle

		       2.762 Mt     1.472 %         1 x        2.762 Mt        0.000 Mt        0.001 Mt     0.025 % 	 ..../DeferredRLPrep

		       2.761 Mt    99.975 %         1 x        2.761 Mt        0.000 Mt        1.955 Mt    70.796 % 	 ...../PrepareForRendering

		       0.001 Mt     0.025 %         1 x        0.001 Mt        0.000 Mt        0.001 Mt   100.000 % 	 ....../PrepareMaterials

		       0.803 Mt    29.077 %         1 x        0.803 Mt        0.000 Mt        0.803 Mt   100.000 % 	 ....../BuildMaterialCache

		       0.003 Mt     0.101 %         1 x        0.003 Mt        0.000 Mt        0.003 Mt   100.000 % 	 ....../PrepareDrawBundle

		      58.295 Mt    31.077 %         1 x       58.295 Mt        0.000 Mt        4.034 Mt     6.919 % 	 ..../RayTracingRLPrep

		       0.052 Mt     0.089 %         1 x        0.052 Mt        0.000 Mt        0.052 Mt   100.000 % 	 ...../PrepareAccelerationStructures

		       0.001 Mt     0.001 %         1 x        0.001 Mt        0.000 Mt        0.001 Mt   100.000 % 	 ...../PrepareCache

		      41.685 Mt    71.507 %         1 x       41.685 Mt        0.000 Mt       41.685 Mt   100.000 % 	 ...../PipelineBuild

		       0.754 Mt     1.293 %         1 x        0.754 Mt        0.000 Mt        0.754 Mt   100.000 % 	 ...../BuildCache

		       5.725 Mt     9.820 %         1 x        5.725 Mt        0.000 Mt        0.001 Mt     0.023 % 	 ...../BuildAccelerationStructures

		       5.723 Mt    99.977 %         1 x        5.723 Mt        0.000 Mt        3.688 Mt    64.442 % 	 ....../AccelerationStructureBuild

		       1.083 Mt    18.931 %         1 x        1.083 Mt        0.000 Mt        1.083 Mt   100.000 % 	 ......./BuildBottomLevelAS

		       0.952 Mt    16.626 %         1 x        0.952 Mt        0.000 Mt        0.952 Mt   100.000 % 	 ......./BuildTopLevelAS

		       6.045 Mt    10.370 %         1 x        6.045 Mt        0.000 Mt        6.045 Mt   100.000 % 	 ...../BuildShaderTables

		      13.865 Mt     7.391 %         1 x       13.865 Mt        0.000 Mt       13.865 Mt   100.000 % 	 ..../GpuSidePrep

	WindowThread:

		   24571.489 Mt   100.000 %         1 x    24571.489 Mt    24571.489 Mt    24571.489 Mt   100.000 % 	 /

	GpuThread:

		   11175.473 Mt   100.000 %     32401 x        0.345 Mt        0.238 Mt      136.762 Mt     1.224 % 	 /

		       0.505 Mt     0.005 %         1 x        0.505 Mt        0.000 Mt        0.006 Mt     1.268 % 	 ./ScenePrep

		       0.498 Mt    98.732 %         1 x        0.498 Mt        0.000 Mt        0.001 Mt     0.199 % 	 ../AccelerationStructureBuild

		       0.436 Mt    87.441 %         1 x        0.436 Mt        0.000 Mt        0.436 Mt   100.000 % 	 .../BuildBottomLevelASGpu

		       0.062 Mt    12.360 %         1 x        0.062 Mt        0.000 Mt        0.062 Mt   100.000 % 	 .../BuildTopLevelASGpu

		   10979.224 Mt    98.244 %     32401 x        0.339 Mt        0.236 Mt       76.152 Mt     0.694 % 	 ./RayTracingGpu

		    3846.523 Mt    35.035 %     32401 x        0.119 Mt        0.071 Mt     3846.523 Mt   100.000 % 	 ../DeferredRLGpu

		    3965.108 Mt    36.115 %     32401 x        0.122 Mt        0.104 Mt     3965.108 Mt   100.000 % 	 ../RayTracingRLGpu

		    3091.440 Mt    28.157 %     32401 x        0.095 Mt        0.060 Mt     3091.440 Mt   100.000 % 	 ../ResolveRLGpu

		      58.982 Mt     0.528 %     32401 x        0.002 Mt        0.001 Mt       58.982 Mt   100.000 % 	 ./Present


	============================


