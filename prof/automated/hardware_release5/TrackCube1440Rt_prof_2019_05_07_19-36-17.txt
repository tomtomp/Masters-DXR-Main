Configuration: 
Command line parameters  = E:\programming\FIT-DIP\Masters-DXR-Main-git\build\Measurement\x64\Release\Measurement.exe --scene Cube/Cube.gltf --quality-preset Low --profile-automation --profile-camera-track Cube/Cube.trk --rt-mode --width 2560 --height 1440 --profile-output TrackCube1440Rt 
Build type               = Release
Device name              = NVIDIA GeForce RTX 2080 Ti
Device memory MB         = 11049
Ray Tracing backend      = Hardware
Window width             = 2560
Window height            = 1421
Window title             = Direct3D App
Back-buffer count        = 3
Fill Triangles           = enabled
Triangles Font Clock-Wise= enabled
Fast Build AS            = disabled
Target FPS               = 0
Target UPS               = 60
Tearing                  = enabled
VSync                    = disabled
GUI rendering            = disabled
Debug layer              = disabled
GPU validation           = disabled
GPU profiling            = enabled
Render mode              = Ray Tracing
Loaded scene file        = Cube/Cube.gltf
Using testing scene      = disabled
Duplication              = 1
Duplication in cube      = disabled
Duplication offset       = 10
BLAS duplication         = disabled
Rays per pixel           = 5
Quality preset           = Low
Rasterization Quality    : 
  AO                     = enabled
  AO kernel size         = 4
  Shadows                = enabled
  Shadow map resolution  = 2048
  Shadow PCF kernel size = 0
  Reflections            = disabled
Ray Tracing Quality      : 
  AO                     = enabled
  AO rays                = 4
  AO kernel size         = 4
  Shadows                = enabled
  Shadow rays            = 1
  Shadow kernel size     = 4
  Reflections            = disabled

ProfilingAggregator: 
Render	,	3.5358	,	3.8468	,	4.3312	,	4.16	,	4.0985	,	4.3269	,	4.1066	,	4.3953	,	4.6266	,	4.433	,	4.3627	,	4.7985	,	4.5615	,	3.9709	,	4.0954	,	4.2994	,	4.6251	,	4.9503	,	5.396	,	5.7571	,	4.9297	,	3.6827	,	3.7057	,	3.6277	,	3.4515	,	3.2531	,	3.1226	,	3.1176	,	3.0552
Update	,	0.0776	,	0.077	,	0.0766	,	0.0767	,	0.0714	,	0.0767	,	0.0753	,	0.0762	,	0.0769	,	0.0775	,	0.0766	,	0.0761	,	0.077	,	0.0779	,	0.0808	,	0.0782	,	0.0794	,	0.0773	,	0.0783	,	0.0689	,	0.0781	,	0.0956	,	0.078	,	0.0759	,	0.077	,	0.0794	,	0.076	,	0.0766	,	0.0772
RayTracing	,	0.1741	,	0.1693	,	0.1695	,	0.1769	,	0.1767	,	0.1732	,	0.1731	,	0.1718	,	0.17	,	0.1686	,	0.1711	,	0.1729	,	0.1808	,	0.1738	,	0.1675	,	0.1698	,	0.1734	,	0.174	,	0.1733	,	0.1686	,	0.1692	,	0.068	,	0.1532	,	0.177	,	0.1691	,	0.1732	,	0.1726	,	0.1737	,	0.1711
RayTracingGpu	,	2.77242	,	3.05133	,	3.22198	,	3.34458	,	3.22419	,	3.47984	,	3.3473	,	3.55747	,	3.82758	,	3.62314	,	3.67034	,	3.69619	,	3.5119	,	3.13082	,	3.29018	,	3.52509	,	3.80832	,	4.12214	,	4.6031	,	4.8463	,	4.08042	,	3.24835	,	3.00656	,	2.77978	,	2.63296	,	2.48842	,	2.3849	,	2.31008	,	2.27277
DeferredRLGpu	,	0.179584	,	0.241568	,	0.254624	,	0.261152	,	0.274208	,	0.334432	,	0.31024	,	0.353376	,	0.410816	,	0.365056	,	0.318976	,	0.348032	,	0.27888	,	0.251776	,	0.28656	,	0.334624	,	0.393216	,	0.463104	,	0.486048	,	0.484512	,	0.404448	,	0.283072	,	0.232832	,	0.18096	,	0.151008	,	0.1176	,	0.089568	,	0.073568	,	0.064128
RayTracingRLGpu	,	0.448544	,	0.58816	,	0.621504	,	0.635744	,	0.671808	,	0.828352	,	0.762688	,	0.877344	,	1.02368	,	0.905312	,	0.842656	,	0.918944	,	0.8912	,	0.654496	,	0.754944	,	0.882176	,	1.0425	,	1.23462	,	1.29376	,	1.65738	,	1.29693	,	0.743552	,	0.591264	,	0.465184	,	0.38896	,	0.30816	,	0.2432	,	0.208512	,	0.183616
ResolveRLGpu	,	2.14141	,	2.21936	,	2.34288	,	2.44461	,	2.27482	,	2.31402	,	2.27242	,	2.32483	,	2.39107	,	2.35075	,	2.5056	,	2.42752	,	2.33875	,	2.22259	,	2.24675	,	2.30493	,	2.37062	,	2.42106	,	2.82096	,	2.70234	,	2.37709	,	2.21901	,	2.18077	,	2.13059	,	2.09072	,	2.05978	,	2.04922	,	2.02534	,	2.02342
Index	,	0	,	1	,	2	,	3	,	4	,	5	,	6	,	7	,	8	,	9	,	10	,	11	,	12	,	13	,	14	,	15	,	16	,	17	,	18	,	19	,	20	,	21	,	22	,	23	,	24	,	25	,	26	,	27	,	28

ProfilingBuildAggregator: 
BuildBottomLevelAS	,	0.8919
BuildBottomLevelASGpu	,	0.127424
BuildTopLevelAS	,	2.1953
BuildTopLevelASGpu	,	0.05984
Duplication	,	1
Triangles	,	12
Meshes	,	1
BottomLevels	,	1
TopLevelSize	,	64
BottomLevelsSize	,	4480
Index	,	0

FpsAggregator: 
FPS	,	224	,	239	,	240	,	235	,	232	,	221	,	219	,	223	,	202	,	202	,	227	,	211	,	222	,	236	,	231	,	221	,	207	,	195	,	184	,	184	,	186	,	222	,	250	,	254	,	266	,	277	,	287	,	296	,	299
UPS	,	59	,	61	,	60	,	60	,	60	,	60	,	60	,	61	,	60	,	60	,	60	,	60	,	60	,	60	,	61	,	60	,	60	,	60	,	60	,	61	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60
FrameTime	,	4.46483	,	4.20003	,	4.18017	,	4.27059	,	4.32527	,	4.53289	,	4.57962	,	4.49806	,	4.95402	,	4.96427	,	4.42257	,	4.74841	,	4.51954	,	4.24412	,	4.34699	,	4.53666	,	4.84144	,	5.14323	,	5.46257	,	5.45496	,	5.38439	,	4.50729	,	4.00316	,	3.94316	,	3.7684	,	3.61183	,	3.48557	,	3.38033	,	3.3469
GigaRays/s	,	4.07379	,	4.33064	,	4.35121	,	4.25908	,	4.20524	,	4.01263	,	3.97168	,	4.0437	,	3.67152	,	3.66394	,	4.11272	,	3.83051	,	4.02448	,	4.28565	,	4.18423	,	4.00929	,	3.7569	,	3.53646	,	3.32972	,	3.33436	,	3.37806	,	4.03542	,	4.54361	,	4.61275	,	4.82666	,	5.0359	,	5.21831	,	5.38078	,	5.43451
Index	,	0	,	1	,	2	,	3	,	4	,	5	,	6	,	7	,	8	,	9	,	10	,	11	,	12	,	13	,	14	,	15	,	16	,	17	,	18	,	19	,	20	,	21	,	22	,	23	,	24	,	25	,	26	,	27	,	28


AccelerationStructure: 
Ray tracing acceleration structure building statistics: 
	Bottom Level Acceleration Structure: 
		Maximum [B]: 4480
		Minimum [B]: 4480
		Currently Allocated [B]: 4480
		Currently Created [num]: 1
		Current Average [B]: 4480
		Maximum Triangles [num]: 12
		Minimum Triangles [num]: 12
		Total Triangles [num]: 12
		Maximum Meshes [num]: 1
		Minimum Meshes [num]: 1
		Total Meshes [num]: 1
	Top Level Acceleration Structure: 
		Maximum [B]: 64
		Minimum [B]: 64
		Currently Allocated [B]: 64
		Total Allocated [B]: 64
		Total Created [num]: 1
		Average Allocated [B]: 64
		Maximum Instances [num]: 1
		Minimum Instances [num]: 1
		Total Instances [num]: 1
	Scratch Buffer: 
		Maximum [B]: 896
		Minimum [B]: 896
		Currently Allocated [B]: 896
		Total Allocated [B]: 896
		Total Created [num]: 1
		Average Allocated [B]: 896
ProfilingStack: 
[PROF] Profiling statistics: 
	Scopes entered : 149418
	Scopes exited : 149417
	Overhead per scope [ticks] : 100.6
	Threads entered : 3
	Threads exited : 0
	============================
		         Sum[Mt]   Parent[%]  Samples[num]       Avg[Mt]   Last/Curr[Mt]        Self[Mt]     Self[%] 	 Name

	main:

		   29591.055 Mt   100.000 %         1 x    29591.056 Mt    29591.055 Mt        0.466 Mt     0.002 % 	 /

		   29590.620 Mt    99.999 %         1 x    29590.621 Mt    29590.620 Mt      284.301 Mt     0.961 % 	 ./Main application loop

		       2.293 Mt     0.008 %         1 x        2.293 Mt        0.000 Mt        2.293 Mt   100.000 % 	 ../TexturedRLInitialization

		       1.265 Mt     0.004 %         1 x        1.265 Mt        0.000 Mt        1.265 Mt   100.000 % 	 ../ShadowMapRLInitialization

		       4.290 Mt     0.014 %         1 x        4.290 Mt        0.000 Mt        4.290 Mt   100.000 % 	 ../RasterResolveRLInitialization

		       3.604 Mt     0.012 %         1 x        3.604 Mt        0.000 Mt        3.604 Mt   100.000 % 	 ../ResolveRLInitialization

		   29112.578 Mt    98.384 %     60605 x        0.480 Mt        3.328 Mt     1382.881 Mt     4.750 % 	 ../MainLoop

		     215.440 Mt     0.740 %      1746 x        0.123 Mt        0.035 Mt      214.787 Mt    99.697 % 	 .../Update

		       0.653 Mt     0.303 %         1 x        0.653 Mt        0.000 Mt        0.653 Mt   100.000 % 	 ..../GuiModelApply

		   27514.257 Mt    94.510 %      6694 x        4.110 Mt        3.168 Mt      732.646 Mt     2.663 % 	 .../Render

		    1170.060 Mt     4.253 %      6694 x        0.175 Mt        0.211 Mt       44.106 Mt     3.770 % 	 ..../RayTracing

		     522.816 Mt    44.683 %      6694 x        0.078 Mt        0.095 Mt      514.962 Mt    98.498 % 	 ...../Deferred

		       7.854 Mt     1.502 %      6694 x        0.001 Mt        0.001 Mt        7.854 Mt   100.000 % 	 ....../DeferredRLPrep

		     410.099 Mt    35.049 %      6694 x        0.061 Mt        0.078 Mt      406.224 Mt    99.055 % 	 ...../RayCasting

		       3.875 Mt     0.945 %      6694 x        0.001 Mt        0.001 Mt        3.875 Mt   100.000 % 	 ....../RayTracingRLPrep

		     193.039 Mt    16.498 %      6694 x        0.029 Mt        0.032 Mt      193.039 Mt   100.000 % 	 ...../Resolve

		   25611.551 Mt    93.085 %      6694 x        3.826 Mt        2.829 Mt    25611.551 Mt   100.000 % 	 ..../Present

		       2.622 Mt     0.009 %         1 x        2.622 Mt        0.000 Mt        2.622 Mt   100.000 % 	 ../AmbientOcclusionRLInitialization

		       4.872 Mt     0.016 %         1 x        4.872 Mt        0.000 Mt        4.872 Mt   100.000 % 	 ../DeferredRLInitialization

		      60.332 Mt     0.204 %         1 x       60.332 Mt        0.000 Mt        2.290 Mt     3.795 % 	 ../RayTracingRLInitialization

		      58.042 Mt    96.205 %         1 x       58.042 Mt        0.000 Mt       58.042 Mt   100.000 % 	 .../PipelineBuild

		     114.463 Mt     0.387 %         1 x      114.463 Mt        0.000 Mt        0.288 Mt     0.252 % 	 ../Initialization

		     114.174 Mt    99.748 %         1 x      114.174 Mt        0.000 Mt        1.785 Mt     1.563 % 	 .../ScenePrep

		      47.130 Mt    41.279 %         1 x       47.130 Mt        0.000 Mt       12.117 Mt    25.709 % 	 ..../SceneLoad

		      11.501 Mt    24.404 %         1 x       11.501 Mt        0.000 Mt        3.514 Mt    30.556 % 	 ...../glTF-LoadScene

		       7.987 Mt    69.444 %         2 x        3.994 Mt        0.000 Mt        7.987 Mt   100.000 % 	 ....../glTF-LoadImageData

		       9.454 Mt    20.060 %         1 x        9.454 Mt        0.000 Mt        9.454 Mt   100.000 % 	 ...../glTF-CreateScene

		      14.057 Mt    29.827 %         1 x       14.057 Mt        0.000 Mt       14.057 Mt   100.000 % 	 ...../SceneDesc-CreateScene

		       2.815 Mt     2.466 %         1 x        2.815 Mt        0.000 Mt        0.000 Mt     0.014 % 	 ..../ShadowMapRLPrep

		       2.815 Mt    99.986 %         1 x        2.815 Mt        0.000 Mt        2.807 Mt    99.719 % 	 ...../PrepareForRendering

		       0.008 Mt     0.281 %         1 x        0.008 Mt        0.000 Mt        0.008 Mt   100.000 % 	 ....../PrepareDrawBundle

		       2.732 Mt     2.393 %         1 x        2.732 Mt        0.000 Mt        0.000 Mt     0.015 % 	 ..../TexturedRLPrep

		       2.732 Mt    99.985 %         1 x        2.732 Mt        0.000 Mt        2.727 Mt    99.824 % 	 ...../PrepareForRendering

		       0.001 Mt     0.040 %         1 x        0.001 Mt        0.000 Mt        0.001 Mt   100.000 % 	 ....../PrepareMaterials

		       0.004 Mt     0.135 %         1 x        0.004 Mt        0.000 Mt        0.004 Mt   100.000 % 	 ....../PrepareDrawBundle

		       2.607 Mt     2.283 %         1 x        2.607 Mt        0.000 Mt        0.000 Mt     0.019 % 	 ..../DeferredRLPrep

		       2.606 Mt    99.981 %         1 x        2.606 Mt        0.000 Mt        1.831 Mt    70.266 % 	 ...../PrepareForRendering

		       0.001 Mt     0.023 %         1 x        0.001 Mt        0.000 Mt        0.001 Mt   100.000 % 	 ....../PrepareMaterials

		       0.770 Mt    29.558 %         1 x        0.770 Mt        0.000 Mt        0.770 Mt   100.000 % 	 ....../BuildMaterialCache

		       0.004 Mt     0.153 %         1 x        0.004 Mt        0.000 Mt        0.004 Mt   100.000 % 	 ....../PrepareDrawBundle

		      43.786 Mt    38.350 %         1 x       43.786 Mt        0.000 Mt        3.489 Mt     7.968 % 	 ..../RayTracingRLPrep

		       0.062 Mt     0.141 %         1 x        0.062 Mt        0.000 Mt        0.062 Mt   100.000 % 	 ...../PrepareAccelerationStructures

		       0.001 Mt     0.002 %         1 x        0.001 Mt        0.000 Mt        0.001 Mt   100.000 % 	 ...../PrepareCache

		      30.683 Mt    70.075 %         1 x       30.683 Mt        0.000 Mt       30.683 Mt   100.000 % 	 ...../PipelineBuild

		       0.685 Mt     1.566 %         1 x        0.685 Mt        0.000 Mt        0.685 Mt   100.000 % 	 ...../BuildCache

		       6.621 Mt    15.122 %         1 x        6.621 Mt        0.000 Mt        0.000 Mt     0.008 % 	 ...../BuildAccelerationStructures

		       6.621 Mt    99.992 %         1 x        6.621 Mt        0.000 Mt        3.534 Mt    53.371 % 	 ....../AccelerationStructureBuild

		       0.892 Mt    13.471 %         1 x        0.892 Mt        0.000 Mt        0.892 Mt   100.000 % 	 ......./BuildBottomLevelAS

		       2.195 Mt    33.158 %         1 x        2.195 Mt        0.000 Mt        2.195 Mt   100.000 % 	 ......./BuildTopLevelAS

		       2.245 Mt     5.127 %         1 x        2.245 Mt        0.000 Mt        2.245 Mt   100.000 % 	 ...../BuildShaderTables

		      13.320 Mt    11.666 %         1 x       13.320 Mt        0.000 Mt       13.320 Mt   100.000 % 	 ..../GpuSidePrep

	WindowThread:

		   29583.218 Mt   100.000 %         1 x    29583.218 Mt    29583.218 Mt    29583.218 Mt   100.000 % 	 /

	GpuThread:

		   22136.848 Mt   100.000 %      6694 x        3.307 Mt        2.275 Mt      122.692 Mt     0.554 % 	 /

		       0.195 Mt     0.001 %         1 x        0.195 Mt        0.000 Mt        0.007 Mt     3.435 % 	 ./ScenePrep

		       0.188 Mt    96.565 %         1 x        0.188 Mt        0.000 Mt        0.001 Mt     0.391 % 	 ../AccelerationStructureBuild

		       0.127 Mt    67.779 %         1 x        0.127 Mt        0.000 Mt        0.127 Mt   100.000 % 	 .../BuildBottomLevelASGpu

		       0.060 Mt    31.830 %         1 x        0.060 Mt        0.000 Mt        0.060 Mt   100.000 % 	 .../BuildTopLevelASGpu

		   21853.940 Mt    98.722 %      6694 x        3.265 Mt        2.272 Mt       17.019 Mt     0.078 % 	 ./RayTracingGpu

		    1810.528 Mt     8.285 %      6694 x        0.270 Mt        0.064 Mt     1810.528 Mt   100.000 % 	 ../DeferredRLGpu

		    4756.500 Mt    21.765 %      6694 x        0.711 Mt        0.182 Mt     4756.500 Mt   100.000 % 	 ../RayTracingRLGpu

		   15269.893 Mt    69.872 %      6694 x        2.281 Mt        2.026 Mt    15269.893 Mt   100.000 % 	 ../ResolveRLGpu

		     160.021 Mt     0.723 %      6694 x        0.024 Mt        0.003 Mt      160.021 Mt   100.000 % 	 ./Present


	============================


