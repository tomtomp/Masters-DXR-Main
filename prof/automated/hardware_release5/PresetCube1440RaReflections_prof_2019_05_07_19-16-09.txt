Configuration: 
Command line parameters  = E:\programming\FIT-DIP\Masters-DXR-Main-git\build\Measurement\x64\Release\Measurement.exe --scene Cube/Cube.gltf --profile-automation --profile-camera-track Cube/Cube.trk --width 2560 --height 1440 --profile-output PresetCube1440RaReflections --quality-preset Reflections 
Build type               = Release
Device name              = NVIDIA GeForce RTX 2080 Ti
Device memory MB         = 11049
Ray Tracing backend      = Hardware
Window width             = 2560
Window height            = 1421
Window title             = Direct3D App
Back-buffer count        = 3
Fill Triangles           = enabled
Triangles Font Clock-Wise= enabled
Fast Build AS            = disabled
Target FPS               = 0
Target UPS               = 60
Tearing                  = enabled
VSync                    = disabled
GUI rendering            = disabled
Debug layer              = disabled
GPU validation           = disabled
GPU profiling            = enabled
Render mode              = Rasterization
Loaded scene file        = Cube/Cube.gltf
Using testing scene      = disabled
Duplication              = 1
Duplication in cube      = disabled
Duplication offset       = 10
BLAS duplication         = disabled
Rays per pixel           = 1
Quality preset           = Reflections
Rasterization Quality    : 
  AO                     = disabled
  AO kernel size         = 1
  Shadows                = disabled
  Shadow map resolution  = 1
  Shadow PCF kernel size = 0
  Reflections            = enabled
Ray Tracing Quality      : 
  AO                     = disabled
  AO rays                = 0
  AO kernel size         = 4
  Shadows                = disabled
  Shadow rays            = 0
  Shadow kernel size     = 4
  Reflections            = enabled

ProfilingAggregator: 
Render	,	0.83	,	0.8761	,	0.9086	,	0.8688	,	0.9109	,	1.0329	,	0.9769	,	1.0149	,	1.2196	,	1.0702	,	0.8756	,	1.0141	,	0.8355	,	0.7151	,	0.8706	,	0.9879	,	1.0879	,	1.1814	,	1.3178	,	1.3558	,	1.0732	,	0.8654	,	0.8194	,	0.5175	,	0.5259	,	0.4562	,	0.3914	,	0.3336	,	0.4642	,	0.4924
Update	,	0.0466	,	0.075	,	0.0428	,	0.0785	,	0.0701	,	0.0574	,	0.066	,	0.0397	,	0.0685	,	0.07	,	0.0736	,	0.065	,	0.0651	,	0.0381	,	0.0359	,	0.067	,	0.0449	,	0.0562	,	0.0556	,	0.0633	,	0.03	,	0.0499	,	0.0578	,	0.0204	,	0.0444	,	0.0315	,	0.0216	,	0.0622	,	0.0342	,	0.06
DeferredRLGpu	,	0.180704	,	0.243392	,	0.2576	,	0.26208	,	0.276928	,	0.336864	,	0.312192	,	0.354624	,	0.41456	,	0.373984	,	0.318496	,	0.351424	,	0.281568	,	0.252512	,	0.287264	,	0.334784	,	0.394976	,	0.465248	,	0.488448	,	0.487872	,	0.421024	,	0.288512	,	0.237152	,	0.184704	,	0.153376	,	0.12144	,	0.092096	,	0.075296	,	0.064512	,	0.063808
Index	,	0	,	1	,	2	,	3	,	4	,	5	,	6	,	7	,	8	,	9	,	10	,	11	,	12	,	13	,	14	,	15	,	16	,	17	,	18	,	19	,	20	,	21	,	22	,	23	,	24	,	25	,	26	,	27	,	28	,	29

ProfilingBuildAggregator: 
BuildBottomLevelAS	,	0.7792
BuildBottomLevelASGpu	,	0.128416
BuildTopLevelAS	,	0.7949
BuildTopLevelASGpu	,	0.06
Duplication	,	1
Triangles	,	12
Meshes	,	1
BottomLevels	,	1
TopLevelSize	,	64
BottomLevelsSize	,	4480
Index	,	0

FpsAggregator: 
FPS	,	1318	,	1237	,	1083	,	1094	,	1083	,	991	,	959	,	1012	,	850	,	806	,	1007	,	920	,	999	,	1162	,	1183	,	1015	,	913	,	822	,	751	,	708	,	708	,	1098	,	1115	,	1358	,	1596	,	1744	,	1984	,	2071	,	2287	,	2223
UPS	,	59	,	60	,	60	,	61	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60
FrameTime	,	0.758928	,	0.808794	,	0.923399	,	0.914672	,	0.923384	,	1.00909	,	1.04292	,	0.988995	,	1.17689	,	1.24192	,	0.993563	,	1.08724	,	1.0014	,	0.860688	,	0.845899	,	0.985703	,	1.09552	,	1.21757	,	1.3333	,	1.41417	,	1.41332	,	0.911211	,	0.897562	,	0.736655	,	0.626792	,	0.573405	,	0.504115	,	0.482978	,	0.437369	,	0.449946
GigaRays/s	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0
Index	,	0	,	1	,	2	,	3	,	4	,	5	,	6	,	7	,	8	,	9	,	10	,	11	,	12	,	13	,	14	,	15	,	16	,	17	,	18	,	19	,	20	,	21	,	22	,	23	,	24	,	25	,	26	,	27	,	28	,	29


AccelerationStructure: 
Ray tracing acceleration structure building statistics: 
	Bottom Level Acceleration Structure: 
		Maximum [B]: 4480
		Minimum [B]: 4480
		Currently Allocated [B]: 4480
		Currently Created [num]: 1
		Current Average [B]: 4480
		Maximum Triangles [num]: 12
		Minimum Triangles [num]: 12
		Total Triangles [num]: 12
		Maximum Meshes [num]: 1
		Minimum Meshes [num]: 1
		Total Meshes [num]: 1
	Top Level Acceleration Structure: 
		Maximum [B]: 64
		Minimum [B]: 64
		Currently Allocated [B]: 64
		Total Allocated [B]: 64
		Total Created [num]: 1
		Average Allocated [B]: 64
		Maximum Instances [num]: 1
		Minimum Instances [num]: 1
		Total Instances [num]: 1
	Scratch Buffer: 
		Maximum [B]: 896
		Minimum [B]: 896
		Currently Allocated [B]: 896
		Total Allocated [B]: 896
		Total Created [num]: 1
		Average Allocated [B]: 896
ProfilingStack: 
[PROF] Profiling statistics: 
	Scopes entered : 452446
	Scopes exited : 452445
	Overhead per scope [ticks] : 103
	Threads entered : 3
	Threads exited : 0
	============================
		         Sum[Mt]   Parent[%]  Samples[num]       Avg[Mt]   Last/Curr[Mt]        Self[Mt]     Self[%] 	 Name

	main:

		   30425.821 Mt   100.000 %         1 x    30425.821 Mt    30425.821 Mt        0.391 Mt     0.001 % 	 /

		   30425.439 Mt    99.999 %         1 x    30425.439 Mt    30425.438 Mt      283.128 Mt     0.931 % 	 ./Main application loop

		       2.310 Mt     0.008 %         1 x        2.310 Mt        0.000 Mt        2.310 Mt   100.000 % 	 ../TexturedRLInitialization

		       2.888 Mt     0.009 %         1 x        2.888 Mt        0.000 Mt        2.888 Mt   100.000 % 	 ../ShadowMapRLInitialization

		       5.726 Mt     0.019 %         1 x        5.726 Mt        0.000 Mt        5.726 Mt   100.000 % 	 ../RasterResolveRLInitialization

		       3.425 Mt     0.011 %         1 x        3.425 Mt        0.000 Mt        3.425 Mt   100.000 % 	 ../ResolveRLInitialization

		   30006.358 Mt    98.623 %    125709 x        0.239 Mt        0.445 Mt      388.596 Mt     1.295 % 	 ../MainLoop

		     177.742 Mt     0.592 %      1801 x        0.099 Mt        0.031 Mt      177.141 Mt    99.662 % 	 .../Update

		       0.601 Mt     0.338 %         1 x        0.601 Mt        0.000 Mt        0.601 Mt   100.000 % 	 ..../GuiModelApply

		   29440.020 Mt    98.113 %     36099 x        0.816 Mt        0.413 Mt     1811.348 Mt     6.153 % 	 .../Render

		    2576.930 Mt     8.753 %     36099 x        0.071 Mt        0.032 Mt     2542.669 Mt    98.670 % 	 ..../Rasterization

		      19.620 Mt     0.761 %     36099 x        0.001 Mt        0.000 Mt       19.620 Mt   100.000 % 	 ...../DeferredRLPrep

		      14.642 Mt     0.568 %     36099 x        0.000 Mt        0.000 Mt       14.642 Mt   100.000 % 	 ...../ShadowMapRLPrep

		   25051.742 Mt    85.094 %     36099 x        0.694 Mt        0.358 Mt    25051.742 Mt   100.000 % 	 ..../Present

		       2.716 Mt     0.009 %         1 x        2.716 Mt        0.000 Mt        2.716 Mt   100.000 % 	 ../AmbientOcclusionRLInitialization

		       5.490 Mt     0.018 %         1 x        5.490 Mt        0.000 Mt        5.490 Mt   100.000 % 	 ../DeferredRLInitialization

		      50.739 Mt     0.167 %         1 x       50.739 Mt        0.000 Mt        2.240 Mt     4.416 % 	 ../RayTracingRLInitialization

		      48.499 Mt    95.584 %         1 x       48.499 Mt        0.000 Mt       48.499 Mt   100.000 % 	 .../PipelineBuild

		      62.657 Mt     0.206 %         1 x       62.657 Mt        0.000 Mt        0.183 Mt     0.292 % 	 ../Initialization

		      62.475 Mt    99.708 %         1 x       62.475 Mt        0.000 Mt        1.182 Mt     1.892 % 	 .../ScenePrep

		      22.591 Mt    36.160 %         1 x       22.591 Mt        0.000 Mt        6.202 Mt    27.452 % 	 ..../SceneLoad

		      10.475 Mt    46.367 %         1 x       10.475 Mt        0.000 Mt        2.496 Mt    23.828 % 	 ...../glTF-LoadScene

		       7.979 Mt    76.172 %         2 x        3.989 Mt        0.000 Mt        7.979 Mt   100.000 % 	 ....../glTF-LoadImageData

		       2.207 Mt     9.769 %         1 x        2.207 Mt        0.000 Mt        2.207 Mt   100.000 % 	 ...../glTF-CreateScene

		       3.708 Mt    16.412 %         1 x        3.708 Mt        0.000 Mt        3.708 Mt   100.000 % 	 ...../SceneDesc-CreateScene

		       0.888 Mt     1.422 %         1 x        0.888 Mt        0.000 Mt        0.001 Mt     0.090 % 	 ..../ShadowMapRLPrep

		       0.888 Mt    99.910 %         1 x        0.888 Mt        0.000 Mt        0.879 Mt    99.031 % 	 ...../PrepareForRendering

		       0.009 Mt     0.969 %         1 x        0.009 Mt        0.000 Mt        0.009 Mt   100.000 % 	 ....../PrepareDrawBundle

		       1.443 Mt     2.309 %         1 x        1.443 Mt        0.000 Mt        0.000 Mt     0.021 % 	 ..../TexturedRLPrep

		       1.442 Mt    99.979 %         1 x        1.442 Mt        0.000 Mt        1.438 Mt    99.681 % 	 ...../PrepareForRendering

		       0.001 Mt     0.090 %         1 x        0.001 Mt        0.000 Mt        0.001 Mt   100.000 % 	 ....../PrepareMaterials

		       0.003 Mt     0.229 %         1 x        0.003 Mt        0.000 Mt        0.003 Mt   100.000 % 	 ....../PrepareDrawBundle

		       1.405 Mt     2.249 %         1 x        1.405 Mt        0.000 Mt        0.000 Mt     0.036 % 	 ..../DeferredRLPrep

		       1.405 Mt    99.964 %         1 x        1.405 Mt        0.000 Mt        1.109 Mt    78.958 % 	 ...../PrepareForRendering

		       0.001 Mt     0.036 %         1 x        0.001 Mt        0.000 Mt        0.001 Mt   100.000 % 	 ....../PrepareMaterials

		       0.292 Mt    20.807 %         1 x        0.292 Mt        0.000 Mt        0.292 Mt   100.000 % 	 ....../BuildMaterialCache

		       0.003 Mt     0.199 %         1 x        0.003 Mt        0.000 Mt        0.003 Mt   100.000 % 	 ....../PrepareDrawBundle

		      24.099 Mt    38.573 %         1 x       24.099 Mt        0.000 Mt        2.550 Mt    10.582 % 	 ..../RayTracingRLPrep

		       0.046 Mt     0.191 %         1 x        0.046 Mt        0.000 Mt        0.046 Mt   100.000 % 	 ...../PrepareAccelerationStructures

		       0.001 Mt     0.003 %         1 x        0.001 Mt        0.000 Mt        0.001 Mt   100.000 % 	 ...../PrepareCache

		      12.728 Mt    52.817 %         1 x       12.728 Mt        0.000 Mt       12.728 Mt   100.000 % 	 ...../PipelineBuild

		       0.318 Mt     1.319 %         1 x        0.318 Mt        0.000 Mt        0.318 Mt   100.000 % 	 ...../BuildCache

		       3.848 Mt    15.967 %         1 x        3.848 Mt        0.000 Mt        0.000 Mt     0.010 % 	 ...../BuildAccelerationStructures

		       3.847 Mt    99.990 %         1 x        3.847 Mt        0.000 Mt        2.273 Mt    59.087 % 	 ....../AccelerationStructureBuild

		       0.779 Mt    20.253 %         1 x        0.779 Mt        0.000 Mt        0.779 Mt   100.000 % 	 ......./BuildBottomLevelAS

		       0.795 Mt    20.661 %         1 x        0.795 Mt        0.000 Mt        0.795 Mt   100.000 % 	 ......./BuildTopLevelAS

		       4.608 Mt    19.122 %         1 x        4.608 Mt        0.000 Mt        4.608 Mt   100.000 % 	 ...../BuildShaderTables

		      10.867 Mt    17.394 %         1 x       10.867 Mt        0.000 Mt       10.867 Mt   100.000 % 	 ..../GpuSidePrep

	WindowThread:

		   30423.750 Mt   100.000 %         1 x    30423.750 Mt    30423.750 Mt    30423.750 Mt   100.000 % 	 /

	GpuThread:

		   15109.503 Mt   100.000 %     36099 x        0.419 Mt        0.126 Mt      142.638 Mt     0.944 % 	 /

		       0.195 Mt     0.001 %         1 x        0.195 Mt        0.000 Mt        0.006 Mt     2.954 % 	 ./ScenePrep

		       0.189 Mt    97.046 %         1 x        0.189 Mt        0.000 Mt        0.001 Mt     0.423 % 	 ../AccelerationStructureBuild

		       0.128 Mt    67.867 %         1 x        0.128 Mt        0.000 Mt        0.128 Mt   100.000 % 	 .../BuildBottomLevelASGpu

		       0.060 Mt    31.710 %         1 x        0.060 Mt        0.000 Mt        0.060 Mt   100.000 % 	 .../BuildTopLevelASGpu

		   14892.123 Mt    98.561 %     36099 x        0.413 Mt        0.124 Mt       62.243 Mt     0.418 % 	 ./RasterizationGpu

		    8661.749 Mt    58.163 %     36099 x        0.240 Mt        0.064 Mt     8661.749 Mt   100.000 % 	 ../DeferredRLGpu

		    6168.131 Mt    41.419 %     36099 x        0.171 Mt        0.059 Mt     6168.131 Mt   100.000 % 	 ../RasterResolveRLGpu

		      74.548 Mt     0.493 %     36099 x        0.002 Mt        0.002 Mt       74.548 Mt   100.000 % 	 ./Present


	============================


