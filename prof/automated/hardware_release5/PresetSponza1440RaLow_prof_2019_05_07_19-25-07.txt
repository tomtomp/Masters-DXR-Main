Configuration: 
Command line parameters  = E:\programming\FIT-DIP\Masters-DXR-Main-git\build\Measurement\x64\Release\Measurement.exe --scene Sponza/Sponza.gltf --profile-automation --profile-camera-track Sponza/SponzaPreset.trk --width 2560 --height 1440 --profile-output PresetSponza1440RaLow --quality-preset Low 
Build type               = Release
Device name              = NVIDIA GeForce RTX 2080 Ti
Device memory MB         = 11049
Ray Tracing backend      = Hardware
Window width             = 2560
Window height            = 1421
Window title             = Direct3D App
Back-buffer count        = 3
Fill Triangles           = enabled
Triangles Font Clock-Wise= enabled
Fast Build AS            = disabled
Target FPS               = 0
Target UPS               = 60
Tearing                  = enabled
VSync                    = disabled
GUI rendering            = disabled
Debug layer              = disabled
GPU validation           = disabled
GPU profiling            = enabled
Render mode              = Rasterization
Loaded scene file        = Sponza/Sponza.gltf
Using testing scene      = disabled
Duplication              = 1
Duplication in cube      = disabled
Duplication offset       = 10
BLAS duplication         = disabled
Rays per pixel           = 5
Quality preset           = Low
Rasterization Quality    : 
  AO                     = enabled
  AO kernel size         = 4
  Shadows                = enabled
  Shadow map resolution  = 2048
  Shadow PCF kernel size = 0
  Reflections            = disabled
Ray Tracing Quality      : 
  AO                     = enabled
  AO rays                = 4
  AO kernel size         = 4
  Shadows                = enabled
  Shadow rays            = 1
  Shadow kernel size     = 4
  Reflections            = disabled

ProfilingAggregator: 
Render	,	4.6803	,	4.7367	,	4.3601	,	3.5063	,	3.3114	,	3.4918	,	3.9775	,	4.2975	,	3.9046	,	3.4632	,	3.2733	,	3.6526	,	4.0285	,	5.2649	,	4.4289	,	3.3647	,	4.0212	,	3.8569	,	3.4715	,	4.6755	,	5.7768	,	6.0914	,	6.1227
Update	,	0.0765	,	0.0787	,	0.0768	,	0.0771	,	0.0773	,	0.0749	,	0.0753	,	0.0775	,	0.0768	,	0.0698	,	0.0766	,	0.0766	,	0.0734	,	0.0771	,	0.0769	,	0.0799	,	0.0758	,	0.076	,	0.0762	,	0.0762	,	0.0763	,	0.0757	,	0.0751
DeferredRLGpu	,	2.71779	,	2.49869	,	2.27731	,	1.47248	,	1.3568	,	1.59824	,	1.96413	,	2.37056	,	2.04662	,	1.51862	,	1.34387	,	1.71328	,	2.15264	,	2.80595	,	2.29158	,	1.47664	,	1.94771	,	1.88995	,	1.49158	,	2.69757	,	3.82858	,	3.87974	,	3.86838
Index	,	0	,	1	,	2	,	3	,	4	,	5	,	6	,	7	,	8	,	9	,	10	,	11	,	12	,	13	,	14	,	15	,	16	,	17	,	18	,	19	,	20	,	21	,	22

ProfilingBuildAggregator: 
BuildBottomLevelAS	,	1.69
BuildBottomLevelASGpu	,	2.6361
BuildTopLevelAS	,	0.7516
BuildTopLevelASGpu	,	0.0904
Duplication	,	1
Triangles	,	262267
Meshes	,	103
BottomLevels	,	1
TopLevelSize	,	64
BottomLevelsSize	,	18390656
Index	,	0

FpsAggregator: 
FPS	,	147	,	206	,	229	,	251	,	294	,	285	,	273	,	221	,	229	,	258	,	292	,	280	,	247	,	226	,	220	,	269	,	259	,	244	,	263	,	271	,	186	,	166	,	165
UPS	,	59	,	60	,	61	,	60	,	60	,	60	,	60	,	60	,	60	,	61	,	60	,	60	,	60	,	60	,	60	,	60	,	61	,	60	,	60	,	60	,	60	,	60	,	60
FrameTime	,	6.8118	,	4.87035	,	4.38107	,	3.99609	,	3.40488	,	3.51655	,	3.67034	,	4.52634	,	4.38259	,	3.87956	,	3.43184	,	3.57711	,	4.06145	,	4.4411	,	4.55625	,	3.72021	,	3.87195	,	4.11282	,	3.80462	,	3.69936	,	5.39288	,	6.05878	,	6.06531
GigaRays/s	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0
Index	,	0	,	1	,	2	,	3	,	4	,	5	,	6	,	7	,	8	,	9	,	10	,	11	,	12	,	13	,	14	,	15	,	16	,	17	,	18	,	19	,	20	,	21	,	22


AccelerationStructure: 
Ray tracing acceleration structure building statistics: 
	Bottom Level Acceleration Structure: 
		Maximum [B]: 18390656
		Minimum [B]: 18390656
		Currently Allocated [B]: 18390656
		Currently Created [num]: 1
		Current Average [B]: 18390656
		Maximum Triangles [num]: 262267
		Minimum Triangles [num]: 262267
		Total Triangles [num]: 262267
		Maximum Meshes [num]: 103
		Minimum Meshes [num]: 103
		Total Meshes [num]: 103
	Top Level Acceleration Structure: 
		Maximum [B]: 64
		Minimum [B]: 64
		Currently Allocated [B]: 64
		Total Allocated [B]: 64
		Total Created [num]: 1
		Average Allocated [B]: 64
		Maximum Instances [num]: 1
		Minimum Instances [num]: 1
		Total Instances [num]: 1
	Scratch Buffer: 
		Maximum [B]: 14995072
		Minimum [B]: 14995072
		Currently Allocated [B]: 14995072
		Total Allocated [B]: 14995072
		Total Created [num]: 1
		Average Allocated [B]: 14995072
ProfilingStack: 
[PROF] Profiling statistics: 
	Scopes entered : 121306
	Scopes exited : 121305
	Overhead per scope [ticks] : 114.1
	Threads entered : 3
	Threads exited : 0
	============================
		         Sum[Mt]   Parent[%]  Samples[num]       Avg[Mt]   Last/Curr[Mt]        Self[Mt]     Self[%] 	 Name

	main:

		   26086.385 Mt   100.000 %         1 x    26086.386 Mt    26086.385 Mt        0.426 Mt     0.002 % 	 /

		   26085.991 Mt    99.998 %         1 x    26085.992 Mt    26085.991 Mt      259.440 Mt     0.995 % 	 ./Main application loop

		       3.578 Mt     0.014 %         1 x        3.578 Mt        0.000 Mt        3.578 Mt   100.000 % 	 ../TexturedRLInitialization

		       1.333 Mt     0.005 %         1 x        1.333 Mt        0.000 Mt        1.333 Mt   100.000 % 	 ../ShadowMapRLInitialization

		       5.051 Mt     0.019 %         1 x        5.051 Mt        0.000 Mt        5.051 Mt   100.000 % 	 ../RasterResolveRLInitialization

		       3.582 Mt     0.014 %         1 x        3.582 Mt        0.000 Mt        3.582 Mt   100.000 % 	 ../ResolveRLInitialization

		   23067.673 Mt    88.429 %     59509 x        0.388 Mt       19.805 Mt      413.263 Mt     1.792 % 	 ../MainLoop

		     195.377 Mt     0.847 %      1383 x        0.141 Mt        0.087 Mt      194.759 Mt    99.683 % 	 .../Update

		       0.619 Mt     0.317 %         1 x        0.619 Mt        0.000 Mt        0.619 Mt   100.000 % 	 ..../GuiModelApply

		   22459.032 Mt    97.362 %      5482 x        4.097 Mt        6.397 Mt      614.025 Mt     2.734 % 	 .../Render

		     917.824 Mt     4.087 %      5482 x        0.167 Mt        0.204 Mt      908.852 Mt    99.022 % 	 ..../Rasterization

		       5.809 Mt     0.633 %      5482 x        0.001 Mt        0.001 Mt        5.809 Mt   100.000 % 	 ...../DeferredRLPrep

		       3.163 Mt     0.345 %      5482 x        0.001 Mt        0.001 Mt        3.163 Mt   100.000 % 	 ...../ShadowMapRLPrep

		   20927.183 Mt    93.179 %      5482 x        3.817 Mt        6.046 Mt    20927.183 Mt   100.000 % 	 ..../Present

		       2.611 Mt     0.010 %         1 x        2.611 Mt        0.000 Mt        2.611 Mt   100.000 % 	 ../AmbientOcclusionRLInitialization

		       4.946 Mt     0.019 %         1 x        4.946 Mt        0.000 Mt        4.946 Mt   100.000 % 	 ../DeferredRLInitialization

		      51.281 Mt     0.197 %         1 x       51.281 Mt        0.000 Mt        2.525 Mt     4.925 % 	 ../RayTracingRLInitialization

		      48.756 Mt    95.075 %         1 x       48.756 Mt        0.000 Mt       48.756 Mt   100.000 % 	 .../PipelineBuild

		    2686.496 Mt    10.299 %         1 x     2686.496 Mt        0.000 Mt        0.337 Mt     0.013 % 	 ../Initialization

		    2686.159 Mt    99.987 %         1 x     2686.159 Mt        0.000 Mt        2.648 Mt     0.099 % 	 .../ScenePrep

		    2612.612 Mt    97.262 %         1 x     2612.612 Mt        0.000 Mt      279.744 Mt    10.707 % 	 ..../SceneLoad

		    2048.480 Mt    78.407 %         1 x     2048.480 Mt        0.000 Mt      583.250 Mt    28.472 % 	 ...../glTF-LoadScene

		    1465.230 Mt    71.528 %        69 x       21.235 Mt        0.000 Mt     1465.230 Mt   100.000 % 	 ....../glTF-LoadImageData

		     216.630 Mt     8.292 %         1 x      216.630 Mt        0.000 Mt      216.630 Mt   100.000 % 	 ...../glTF-CreateScene

		      67.758 Mt     2.593 %         1 x       67.758 Mt        0.000 Mt       67.758 Mt   100.000 % 	 ...../SceneDesc-CreateScene

		       1.771 Mt     0.066 %         1 x        1.771 Mt        0.000 Mt        0.001 Mt     0.040 % 	 ..../ShadowMapRLPrep

		       1.770 Mt    99.960 %         1 x        1.770 Mt        0.000 Mt        1.696 Mt    95.803 % 	 ...../PrepareForRendering

		       0.074 Mt     4.197 %         1 x        0.074 Mt        0.000 Mt        0.074 Mt   100.000 % 	 ....../PrepareDrawBundle

		       4.885 Mt     0.182 %         1 x        4.885 Mt        0.000 Mt        0.001 Mt     0.023 % 	 ..../TexturedRLPrep

		       4.884 Mt    99.977 %         1 x        4.884 Mt        0.000 Mt        4.814 Mt    98.559 % 	 ...../PrepareForRendering

		       0.002 Mt     0.037 %         1 x        0.002 Mt        0.000 Mt        0.002 Mt   100.000 % 	 ....../PrepareMaterials

		       0.069 Mt     1.405 %         1 x        0.069 Mt        0.000 Mt        0.069 Mt   100.000 % 	 ....../PrepareDrawBundle

		       4.949 Mt     0.184 %         1 x        4.949 Mt        0.000 Mt        0.001 Mt     0.014 % 	 ..../DeferredRLPrep

		       4.948 Mt    99.986 %         1 x        4.948 Mt        0.000 Mt        4.069 Mt    82.233 % 	 ...../PrepareForRendering

		       0.001 Mt     0.012 %         1 x        0.001 Mt        0.000 Mt        0.001 Mt   100.000 % 	 ....../PrepareMaterials

		       0.726 Mt    14.667 %         1 x        0.726 Mt        0.000 Mt        0.726 Mt   100.000 % 	 ....../BuildMaterialCache

		       0.153 Mt     3.088 %         1 x        0.153 Mt        0.000 Mt        0.153 Mt   100.000 % 	 ....../PrepareDrawBundle

		      28.346 Mt     1.055 %         1 x       28.346 Mt        0.000 Mt        2.078 Mt     7.332 % 	 ..../RayTracingRLPrep

		       0.148 Mt     0.523 %         1 x        0.148 Mt        0.000 Mt        0.148 Mt   100.000 % 	 ...../PrepareAccelerationStructures

		       0.002 Mt     0.008 %         1 x        0.002 Mt        0.000 Mt        0.002 Mt   100.000 % 	 ...../PrepareCache

		      20.325 Mt    71.704 %         1 x       20.325 Mt        0.000 Mt       20.325 Mt   100.000 % 	 ...../PipelineBuild

		       0.599 Mt     2.115 %         1 x        0.599 Mt        0.000 Mt        0.599 Mt   100.000 % 	 ...../BuildCache

		       4.364 Mt    15.397 %         1 x        4.364 Mt        0.000 Mt        0.000 Mt     0.011 % 	 ...../BuildAccelerationStructures

		       4.364 Mt    99.989 %         1 x        4.364 Mt        0.000 Mt        1.922 Mt    44.049 % 	 ....../AccelerationStructureBuild

		       1.690 Mt    38.728 %         1 x        1.690 Mt        0.000 Mt        1.690 Mt   100.000 % 	 ......./BuildBottomLevelAS

		       0.752 Mt    17.224 %         1 x        0.752 Mt        0.000 Mt        0.752 Mt   100.000 % 	 ......./BuildTopLevelAS

		       0.828 Mt     2.922 %         1 x        0.828 Mt        0.000 Mt        0.828 Mt   100.000 % 	 ...../BuildShaderTables

		      30.948 Mt     1.152 %         1 x       30.948 Mt        0.000 Mt       30.948 Mt   100.000 % 	 ..../GpuSidePrep

	WindowThread:

		   26084.896 Mt   100.000 %         1 x    26084.896 Mt    26084.896 Mt    26084.896 Mt   100.000 % 	 /

	GpuThread:

		   18164.299 Mt   100.000 %      5482 x        3.313 Mt        5.502 Mt      150.179 Mt     0.827 % 	 /

		       2.735 Mt     0.015 %         1 x        2.735 Mt        0.000 Mt        0.007 Mt     0.262 % 	 ./ScenePrep

		       2.728 Mt    99.738 %         1 x        2.728 Mt        0.000 Mt        0.001 Mt     0.053 % 	 ../AccelerationStructureBuild

		       2.636 Mt    96.633 %         1 x        2.636 Mt        0.000 Mt        2.636 Mt   100.000 % 	 .../BuildBottomLevelASGpu

		       0.090 Mt     3.314 %         1 x        0.090 Mt        0.000 Mt        0.090 Mt   100.000 % 	 .../BuildTopLevelASGpu

		   17962.761 Mt    98.890 %      5482 x        3.277 Mt        5.500 Mt       20.261 Mt     0.113 % 	 ./RasterizationGpu

		   11439.991 Mt    63.687 %      5482 x        2.087 Mt        3.871 Mt    11439.991 Mt   100.000 % 	 ../DeferredRLGpu

		     391.251 Mt     2.178 %      5482 x        0.071 Mt        0.058 Mt      391.251 Mt   100.000 % 	 ../ShadowMapRLGpu

		    3485.742 Mt    19.405 %      5482 x        0.636 Mt        0.739 Mt     3485.742 Mt   100.000 % 	 ../AmbientOcclusionRLGpu

		    2625.516 Mt    14.616 %      5482 x        0.479 Mt        0.830 Mt     2625.516 Mt   100.000 % 	 ../RasterResolveRLGpu

		      48.625 Mt     0.268 %      5482 x        0.009 Mt        0.002 Mt       48.625 Mt   100.000 % 	 ./Present


	============================


