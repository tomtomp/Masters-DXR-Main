Configuration: 
Command line parameters  = E:\programming\FIT-DIP\Masters-DXR-Main-git\build\Measurement\x64\Release\Measurement.exe --scene Sponza/Sponza.gltf --quality-preset Low --profile-automation --profile-camera-track Sponza/Sponza.trk --width 2560 --height 1440 --profile-output TrackSponza1440Ra 
Build type               = Release
Device name              = NVIDIA GeForce RTX 2080 Ti
Device memory MB         = 11049
Ray Tracing backend      = Hardware
Window width             = 2560
Window height            = 1421
Window title             = Direct3D App
Back-buffer count        = 3
Fill Triangles           = enabled
Triangles Font Clock-Wise= enabled
Fast Build AS            = disabled
Target FPS               = 0
Target UPS               = 60
Tearing                  = enabled
VSync                    = disabled
GUI rendering            = disabled
Debug layer              = disabled
GPU validation           = disabled
GPU profiling            = enabled
Render mode              = Rasterization
Loaded scene file        = Sponza/Sponza.gltf
Using testing scene      = disabled
Duplication              = 1
Duplication in cube      = disabled
Duplication offset       = 10
BLAS duplication         = disabled
Rays per pixel           = 5
Quality preset           = Low
Rasterization Quality    : 
  AO                     = enabled
  AO kernel size         = 4
  Shadows                = enabled
  Shadow map resolution  = 2048
  Shadow PCF kernel size = 0
  Reflections            = disabled
Ray Tracing Quality      : 
  AO                     = enabled
  AO rays                = 4
  AO kernel size         = 4
  Shadows                = enabled
  Shadow rays            = 1
  Shadow kernel size     = 4
  Reflections            = disabled

ProfilingAggregator: 
Render	,	5.2546	,	5.2697	,	5.14	,	5.0949	,	4.7333	,	4.6082	,	4.6682	,	4.1545	,	4.0983	,	4.5404	,	5.4048	,	5.5692	,	5.4302	,	5.0684	,	5.7196	,	5.4974	,	4.9417	,	4.1643	,	4.8877	,	5.5758	,	5.8231	,	6.2057	,	6.1025	,	5.8445	,	5.881	,	5.6786
Update	,	0.0781	,	0.0781	,	0.0757	,	0.0756	,	0.0786	,	0.0756	,	0.0753	,	0.0765	,	0.0755	,	0.0753	,	0.0763	,	0.0756	,	0.0756	,	0.0771	,	0.0765	,	0.0769	,	0.0692	,	0.076	,	0.0765	,	0.0766	,	0.0758	,	0.076	,	0.0806	,	0.0775	,	0.0768	,	0.0774
DeferredRLGpu	,	3.18099	,	3.17069	,	3.19747	,	3.1217	,	2.87677	,	2.6416	,	2.3088	,	1.94762	,	2.09856	,	2.53616	,	3.44918	,	3.69062	,	3.53024	,	3.24637	,	3.26157	,	3.3089	,	2.74134	,	2.17206	,	2.88461	,	3.56944	,	3.85258	,	4.32522	,	4.12803	,	3.69018	,	3.70698	,	3.29981
Index	,	0	,	1	,	2	,	3	,	4	,	5	,	6	,	7	,	8	,	9	,	10	,	11	,	12	,	13	,	14	,	15	,	16	,	17	,	18	,	19	,	20	,	21	,	22	,	23	,	24	,	25

ProfilingBuildAggregator: 
BuildBottomLevelAS	,	1.8222
BuildBottomLevelASGpu	,	2.62192
BuildTopLevelAS	,	0.8977
BuildTopLevelASGpu	,	0.08976
Duplication	,	1
Triangles	,	262267
Meshes	,	103
BottomLevels	,	1
TopLevelSize	,	64
BottomLevelsSize	,	18390656
Index	,	0

FpsAggregator: 
FPS	,	132	,	180	,	185	,	187	,	191	,	201	,	222	,	246	,	260	,	242	,	191	,	174	,	174	,	180	,	186	,	192	,	200	,	222	,	217	,	183	,	168	,	157	,	152	,	168	,	172	,	176
UPS	,	59	,	60	,	61	,	60	,	60	,	60	,	60	,	61	,	60	,	60	,	60	,	60	,	60	,	60	,	61	,	60	,	60	,	60	,	60	,	60	,	61	,	60	,	60	,	60	,	60	,	60
FrameTime	,	6.61403	,	5.55624	,	5.42883	,	5.36853	,	5.23858	,	4.97738	,	4.52303	,	4.07741	,	3.85253	,	4.15019	,	5.25099	,	5.75975	,	5.75057	,	5.58197	,	5.40021	,	5.22711	,	5.00334	,	4.51864	,	4.61685	,	5.47908	,	5.97303	,	6.38028	,	6.59189	,	5.97761	,	5.81554	,	5.68859
GigaRays/s	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0
Index	,	0	,	1	,	2	,	3	,	4	,	5	,	6	,	7	,	8	,	9	,	10	,	11	,	12	,	13	,	14	,	15	,	16	,	17	,	18	,	19	,	20	,	21	,	22	,	23	,	24	,	25


AccelerationStructure: 
Ray tracing acceleration structure building statistics: 
	Bottom Level Acceleration Structure: 
		Maximum [B]: 18390656
		Minimum [B]: 18390656
		Currently Allocated [B]: 18390656
		Currently Created [num]: 1
		Current Average [B]: 18390656
		Maximum Triangles [num]: 262267
		Minimum Triangles [num]: 262267
		Total Triangles [num]: 262267
		Maximum Meshes [num]: 103
		Minimum Meshes [num]: 103
		Total Meshes [num]: 103
	Top Level Acceleration Structure: 
		Maximum [B]: 64
		Minimum [B]: 64
		Currently Allocated [B]: 64
		Total Allocated [B]: 64
		Total Created [num]: 1
		Average Allocated [B]: 64
		Maximum Instances [num]: 1
		Minimum Instances [num]: 1
		Total Instances [num]: 1
	Scratch Buffer: 
		Maximum [B]: 14995072
		Minimum [B]: 14995072
		Currently Allocated [B]: 14995072
		Total Allocated [B]: 14995072
		Total Created [num]: 1
		Average Allocated [B]: 14995072
ProfilingStack: 
[PROF] Profiling statistics: 
	Scopes entered : 96605
	Scopes exited : 96604
	Overhead per scope [ticks] : 100.6
	Threads entered : 3
	Threads exited : 0
	============================
		         Sum[Mt]   Parent[%]  Samples[num]       Avg[Mt]   Last/Curr[Mt]        Self[Mt]     Self[%] 	 Name

	main:

		   29139.131 Mt   100.000 %         1 x    29139.132 Mt    29139.131 Mt        0.446 Mt     0.002 % 	 /

		   29138.718 Mt    99.999 %         1 x    29138.719 Mt    29138.717 Mt      263.296 Mt     0.904 % 	 ./Main application loop

		       2.268 Mt     0.008 %         1 x        2.268 Mt        0.000 Mt        2.268 Mt   100.000 % 	 ../TexturedRLInitialization

		       1.287 Mt     0.004 %         1 x        1.287 Mt        0.000 Mt        1.287 Mt   100.000 % 	 ../ShadowMapRLInitialization

		       4.826 Mt     0.017 %         1 x        4.826 Mt        0.000 Mt        4.826 Mt   100.000 % 	 ../RasterResolveRLInitialization

		       3.770 Mt     0.013 %         1 x        3.770 Mt        0.000 Mt        3.770 Mt   100.000 % 	 ../ResolveRLInitialization

		   26082.074 Mt    89.510 %     40380 x        0.646 Mt       19.951 Mt      462.445 Mt     1.773 % 	 ../MainLoop

		     214.291 Mt     0.822 %      1564 x        0.137 Mt        0.083 Mt      213.652 Mt    99.702 % 	 .../Update

		       0.639 Mt     0.298 %         1 x        0.639 Mt        0.000 Mt        0.639 Mt   100.000 % 	 ..../GuiModelApply

		   25405.337 Mt    97.405 %      4959 x        5.123 Mt        6.202 Mt      554.305 Mt     2.182 % 	 .../Render

		     834.717 Mt     3.286 %      4959 x        0.168 Mt        0.201 Mt      826.792 Mt    99.051 % 	 ..../Rasterization

		       5.038 Mt     0.604 %      4959 x        0.001 Mt        0.001 Mt        5.038 Mt   100.000 % 	 ...../DeferredRLPrep

		       2.887 Mt     0.346 %      4959 x        0.001 Mt        0.001 Mt        2.887 Mt   100.000 % 	 ...../ShadowMapRLPrep

		   24016.315 Mt    94.533 %      4959 x        4.843 Mt        5.859 Mt    24016.315 Mt   100.000 % 	 ..../Present

		       2.718 Mt     0.009 %         1 x        2.718 Mt        0.000 Mt        2.718 Mt   100.000 % 	 ../AmbientOcclusionRLInitialization

		       5.026 Mt     0.017 %         1 x        5.026 Mt        0.000 Mt        5.026 Mt   100.000 % 	 ../DeferredRLInitialization

		      50.938 Mt     0.175 %         1 x       50.938 Mt        0.000 Mt        2.497 Mt     4.903 % 	 ../RayTracingRLInitialization

		      48.441 Mt    95.097 %         1 x       48.441 Mt        0.000 Mt       48.441 Mt   100.000 % 	 .../PipelineBuild

		    2722.516 Mt     9.343 %         1 x     2722.516 Mt        0.000 Mt        0.515 Mt     0.019 % 	 ../Initialization

		    2722.001 Mt    99.981 %         1 x     2722.001 Mt        0.000 Mt        2.311 Mt     0.085 % 	 .../ScenePrep

		    2634.975 Mt    96.803 %         1 x     2634.975 Mt        0.000 Mt      256.084 Mt     9.719 % 	 ..../SceneLoad

		    2062.686 Mt    78.281 %         1 x     2062.686 Mt        0.000 Mt      618.106 Mt    29.966 % 	 ...../glTF-LoadScene

		    1444.580 Mt    70.034 %        69 x       20.936 Mt        0.000 Mt     1444.580 Mt   100.000 % 	 ....../glTF-LoadImageData

		     251.901 Mt     9.560 %         1 x      251.901 Mt        0.000 Mt      251.901 Mt   100.000 % 	 ...../glTF-CreateScene

		      64.303 Mt     2.440 %         1 x       64.303 Mt        0.000 Mt       64.303 Mt   100.000 % 	 ...../SceneDesc-CreateScene

		       1.375 Mt     0.051 %         1 x        1.375 Mt        0.000 Mt        0.001 Mt     0.058 % 	 ..../ShadowMapRLPrep

		       1.374 Mt    99.942 %         1 x        1.374 Mt        0.000 Mt        1.309 Mt    95.291 % 	 ...../PrepareForRendering

		       0.065 Mt     4.709 %         1 x        0.065 Mt        0.000 Mt        0.065 Mt   100.000 % 	 ....../PrepareDrawBundle

		       4.893 Mt     0.180 %         1 x        4.893 Mt        0.000 Mt        0.001 Mt     0.014 % 	 ..../TexturedRLPrep

		       4.892 Mt    99.986 %         1 x        4.892 Mt        0.000 Mt        4.814 Mt    98.404 % 	 ...../PrepareForRendering

		       0.002 Mt     0.035 %         1 x        0.002 Mt        0.000 Mt        0.002 Mt   100.000 % 	 ....../PrepareMaterials

		       0.076 Mt     1.562 %         1 x        0.076 Mt        0.000 Mt        0.076 Mt   100.000 % 	 ....../PrepareDrawBundle

		       5.604 Mt     0.206 %         1 x        5.604 Mt        0.000 Mt        0.001 Mt     0.016 % 	 ..../DeferredRLPrep

		       5.603 Mt    99.984 %         1 x        5.603 Mt        0.000 Mt        4.302 Mt    76.789 % 	 ...../PrepareForRendering

		       0.001 Mt     0.023 %         1 x        0.001 Mt        0.000 Mt        0.001 Mt   100.000 % 	 ....../PrepareMaterials

		       1.161 Mt    20.719 %         1 x        1.161 Mt        0.000 Mt        1.161 Mt   100.000 % 	 ....../BuildMaterialCache

		       0.138 Mt     2.468 %         1 x        0.138 Mt        0.000 Mt        0.138 Mt   100.000 % 	 ....../PrepareDrawBundle

		      38.394 Mt     1.411 %         1 x       38.394 Mt        0.000 Mt        2.891 Mt     7.530 % 	 ..../RayTracingRLPrep

		       0.141 Mt     0.368 %         1 x        0.141 Mt        0.000 Mt        0.141 Mt   100.000 % 	 ...../PrepareAccelerationStructures

		       0.002 Mt     0.005 %         1 x        0.002 Mt        0.000 Mt        0.002 Mt   100.000 % 	 ...../PrepareCache

		      24.403 Mt    63.558 %         1 x       24.403 Mt        0.000 Mt       24.403 Mt   100.000 % 	 ...../PipelineBuild

		       0.809 Mt     2.106 %         1 x        0.809 Mt        0.000 Mt        0.809 Mt   100.000 % 	 ...../BuildCache

		       8.662 Mt    22.560 %         1 x        8.662 Mt        0.000 Mt        0.001 Mt     0.008 % 	 ...../BuildAccelerationStructures

		       8.661 Mt    99.992 %         1 x        8.661 Mt        0.000 Mt        5.941 Mt    68.597 % 	 ....../AccelerationStructureBuild

		       1.822 Mt    21.039 %         1 x        1.822 Mt        0.000 Mt        1.822 Mt   100.000 % 	 ......./BuildBottomLevelAS

		       0.898 Mt    10.365 %         1 x        0.898 Mt        0.000 Mt        0.898 Mt   100.000 % 	 ......./BuildTopLevelAS

		       1.486 Mt     3.872 %         1 x        1.486 Mt        0.000 Mt        1.486 Mt   100.000 % 	 ...../BuildShaderTables

		      34.450 Mt     1.266 %         1 x       34.450 Mt        0.000 Mt       34.450 Mt   100.000 % 	 ..../GpuSidePrep

	WindowThread:

		   29137.581 Mt   100.000 %         1 x    29137.582 Mt    29137.581 Mt    29137.581 Mt   100.000 % 	 /

	GpuThread:

		   21479.347 Mt   100.000 %      4959 x        4.331 Mt        5.141 Mt      159.622 Mt     0.743 % 	 /

		       2.719 Mt     0.013 %         1 x        2.719 Mt        0.000 Mt        0.006 Mt     0.235 % 	 ./ScenePrep

		       2.713 Mt    99.765 %         1 x        2.713 Mt        0.000 Mt        0.001 Mt     0.045 % 	 ../AccelerationStructureBuild

		       2.622 Mt    96.647 %         1 x        2.622 Mt        0.000 Mt        2.622 Mt   100.000 % 	 .../BuildBottomLevelASGpu

		       0.090 Mt     3.309 %         1 x        0.090 Mt        0.000 Mt        0.090 Mt   100.000 % 	 .../BuildTopLevelASGpu

		   21255.340 Mt    98.957 %      4959 x        4.286 Mt        5.047 Mt       19.017 Mt     0.089 % 	 ./RasterizationGpu

		   15308.106 Mt    72.020 %      4959 x        3.087 Mt        3.427 Mt    15308.106 Mt   100.000 % 	 ../DeferredRLGpu

		     377.503 Mt     1.776 %      4959 x        0.076 Mt        0.412 Mt      377.503 Mt   100.000 % 	 ../ShadowMapRLGpu

		    3134.466 Mt    14.747 %      4959 x        0.632 Mt        0.645 Mt     3134.466 Mt   100.000 % 	 ../AmbientOcclusionRLGpu

		    2416.248 Mt    11.368 %      4959 x        0.487 Mt        0.561 Mt     2416.248 Mt   100.000 % 	 ../RasterResolveRLGpu

		      61.665 Mt     0.287 %      4959 x        0.012 Mt        0.094 Mt       61.665 Mt   100.000 % 	 ./Present


	============================


