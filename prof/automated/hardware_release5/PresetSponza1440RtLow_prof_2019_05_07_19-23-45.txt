Configuration: 
Command line parameters  = E:\programming\FIT-DIP\Masters-DXR-Main-git\build\Measurement\x64\Release\Measurement.exe --scene Sponza/Sponza.gltf --profile-automation --profile-camera-track Sponza/SponzaPreset.trk --rt-mode --width 2560 --height 1440 --profile-output PresetSponza1440RtLow --quality-preset Low 
Build type               = Release
Device name              = NVIDIA GeForce RTX 2080 Ti
Device memory MB         = 11049
Ray Tracing backend      = Hardware
Window width             = 2560
Window height            = 1421
Window title             = Direct3D App
Back-buffer count        = 3
Fill Triangles           = enabled
Triangles Font Clock-Wise= enabled
Fast Build AS            = disabled
Target FPS               = 0
Target UPS               = 60
Tearing                  = enabled
VSync                    = disabled
GUI rendering            = disabled
Debug layer              = disabled
GPU validation           = disabled
GPU profiling            = enabled
Render mode              = Ray Tracing
Loaded scene file        = Sponza/Sponza.gltf
Using testing scene      = disabled
Duplication              = 1
Duplication in cube      = disabled
Duplication offset       = 10
BLAS duplication         = disabled
Rays per pixel           = 5
Quality preset           = Low
Rasterization Quality    : 
  AO                     = enabled
  AO kernel size         = 4
  Shadows                = enabled
  Shadow map resolution  = 2048
  Shadow PCF kernel size = 0
  Reflections            = disabled
Ray Tracing Quality      : 
  AO                     = enabled
  AO rays                = 4
  AO kernel size         = 4
  Shadows                = enabled
  Shadow rays            = 1
  Shadow kernel size     = 4
  Reflections            = disabled

ProfilingAggregator: 
Render	,	8.3292	,	8.869	,	8.4622	,	6.8359	,	7.5721	,	7.2587	,	7.5043	,	7.7778	,	7.4347	,	6.8609	,	7.2639	,	7.7904	,	8.4446	,	8.647	,	7.5577	,	7.0628	,	7.8352	,	7.7025	,	7.7351	,	8.8542	,	9.2386	,	9.6589	,	9.844
Update	,	0.076	,	0.0778	,	0.0773	,	0.0765	,	0.0783	,	0.0776	,	0.0766	,	0.0737	,	0.0764	,	0.0783	,	0.0764	,	0.0773	,	0.0779	,	0.0773	,	0.0792	,	0.0785	,	0.0821	,	0.0783	,	0.0763	,	0.0755	,	0.0738	,	0.0762	,	0.0755
RayTracing	,	0.181	,	0.1732	,	0.1746	,	0.1774	,	0.1723	,	0.1762	,	0.1737	,	0.1725	,	0.1777	,	0.1751	,	0.177	,	0.1762	,	0.1751	,	0.1846	,	0.1779	,	0.1739	,	0.1746	,	0.2192	,	0.1702	,	0.1729	,	0.1774	,	0.1727	,	0.1728
RayTracingGpu	,	7.45712	,	7.8729	,	7.45363	,	5.91722	,	6.81936	,	6.38154	,	6.63002	,	6.93632	,	6.60355	,	6.05661	,	6.02646	,	6.90304	,	7.50842	,	7.98195	,	6.68512	,	6.18822	,	6.95027	,	6.82957	,	6.75757	,	7.70131	,	8.32634	,	8.72166	,	8.9527
DeferredRLGpu	,	2.70525	,	2.51222	,	2.1513	,	1.47264	,	1.71046	,	1.59562	,	2.02845	,	2.37616	,	2.07664	,	1.53312	,	1.33696	,	1.74781	,	2.21709	,	2.83622	,	2.1809	,	1.48653	,	1.95088	,	1.8896	,	1.52208	,	2.86189	,	3.83869	,	3.85053	,	3.88003
RayTracingRLGpu	,	2.35427	,	2.84192	,	2.77782	,	2.08413	,	2.43984	,	2.36397	,	2.17075	,	2.17709	,	2.11274	,	2.07699	,	2.12874	,	2.49776	,	2.64627	,	2.53728	,	2.10259	,	2.26771	,	2.5361	,	2.47875	,	2.6487	,	2.51734	,	2.12179	,	2.1241	,	2.4817
ResolveRLGpu	,	2.3945	,	2.5161	,	2.51965	,	2.3583	,	2.66704	,	2.41962	,	2.42845	,	2.38109	,	2.4111	,	2.44451	,	2.55773	,	2.65552	,	2.64294	,	2.60646	,	2.39898	,	2.43184	,	2.4609	,	2.4576	,	2.5839	,	2.31565	,	2.36262	,	2.74509	,	2.58886
Index	,	0	,	1	,	2	,	3	,	4	,	5	,	6	,	7	,	8	,	9	,	10	,	11	,	12	,	13	,	14	,	15	,	16	,	17	,	18	,	19	,	20	,	21	,	22

ProfilingBuildAggregator: 
BuildBottomLevelAS	,	1.6246
BuildBottomLevelASGpu	,	2.34275
BuildTopLevelAS	,	0.8202
BuildTopLevelASGpu	,	0.088864
Duplication	,	1
Triangles	,	262267
Meshes	,	103
BottomLevels	,	1
TopLevelSize	,	64
BottomLevelsSize	,	18390656
Index	,	0

FpsAggregator: 
FPS	,	81	,	113	,	120	,	129	,	142	,	131	,	136	,	120	,	125	,	134	,	140	,	138	,	129	,	121	,	121	,	136	,	126	,	122	,	129	,	136	,	108	,	104	,	103
UPS	,	59	,	61	,	60	,	60	,	60	,	60	,	61	,	60	,	60	,	60	,	60	,	60	,	60	,	61	,	60	,	60	,	60	,	60	,	61	,	60	,	60	,	61	,	60
FrameTime	,	12.4354	,	8.91844	,	8.33474	,	7.77814	,	7.04384	,	7.66591	,	7.36637	,	8.35929	,	8.03129	,	7.47163	,	7.16699	,	7.2617	,	7.76658	,	8.2828	,	8.29394	,	7.35719	,	7.96465	,	8.2342	,	7.81214	,	7.37515	,	9.30408	,	9.69949	,	9.72131
GigaRays/s	,	1.46266	,	2.03946	,	2.18229	,	2.33845	,	2.58223	,	2.37269	,	2.46917	,	2.17588	,	2.26474	,	2.43438	,	2.53786	,	2.50476	,	2.34193	,	2.19597	,	2.19302	,	2.47225	,	2.28369	,	2.20893	,	2.32827	,	2.46623	,	1.95493	,	1.87523	,	1.87102
Index	,	0	,	1	,	2	,	3	,	4	,	5	,	6	,	7	,	8	,	9	,	10	,	11	,	12	,	13	,	14	,	15	,	16	,	17	,	18	,	19	,	20	,	21	,	22


AccelerationStructure: 
Ray tracing acceleration structure building statistics: 
	Bottom Level Acceleration Structure: 
		Maximum [B]: 18390656
		Minimum [B]: 18390656
		Currently Allocated [B]: 18390656
		Currently Created [num]: 1
		Current Average [B]: 18390656
		Maximum Triangles [num]: 262267
		Minimum Triangles [num]: 262267
		Total Triangles [num]: 262267
		Maximum Meshes [num]: 103
		Minimum Meshes [num]: 103
		Total Meshes [num]: 103
	Top Level Acceleration Structure: 
		Maximum [B]: 64
		Minimum [B]: 64
		Currently Allocated [B]: 64
		Total Allocated [B]: 64
		Total Created [num]: 1
		Average Allocated [B]: 64
		Maximum Instances [num]: 1
		Minimum Instances [num]: 1
		Total Instances [num]: 1
	Scratch Buffer: 
		Maximum [B]: 14995072
		Minimum [B]: 14995072
		Currently Allocated [B]: 14995072
		Total Allocated [B]: 14995072
		Total Created [num]: 1
		Average Allocated [B]: 14995072
ProfilingStack: 
[PROF] Profiling statistics: 
	Scopes entered : 71580
	Scopes exited : 71579
	Overhead per scope [ticks] : 101.4
	Threads entered : 3
	Threads exited : 0
	============================
		         Sum[Mt]   Parent[%]  Samples[num]       Avg[Mt]   Last/Curr[Mt]        Self[Mt]     Self[%] 	 Name

	main:

		   26075.489 Mt   100.000 %         1 x    26075.490 Mt    26075.488 Mt        0.450 Mt     0.002 % 	 /

		   26075.070 Mt    99.998 %         1 x    26075.072 Mt    26075.070 Mt      241.561 Mt     0.926 % 	 ./Main application loop

		       2.247 Mt     0.009 %         1 x        2.247 Mt        0.000 Mt        2.247 Mt   100.000 % 	 ../TexturedRLInitialization

		       1.278 Mt     0.005 %         1 x        1.278 Mt        0.000 Mt        1.278 Mt   100.000 % 	 ../ShadowMapRLInitialization

		       5.364 Mt     0.021 %         1 x        5.364 Mt        0.000 Mt        5.364 Mt   100.000 % 	 ../RasterResolveRLInitialization

		       3.876 Mt     0.015 %         1 x        3.876 Mt        0.000 Mt        3.876 Mt   100.000 % 	 ../ResolveRLInitialization

		   23112.887 Mt    88.640 %     33084 x        0.699 Mt       10.466 Mt      420.596 Mt     1.820 % 	 ../MainLoop

		     204.940 Mt     0.887 %      1386 x        0.148 Mt        0.047 Mt      204.599 Mt    99.833 % 	 .../Update

		       0.341 Mt     0.167 %         1 x        0.341 Mt        0.000 Mt        0.341 Mt   100.000 % 	 ..../GuiModelApply

		   22487.350 Mt    97.294 %      2846 x        7.901 Mt       10.323 Mt      325.760 Mt     1.449 % 	 .../Render

		     512.115 Mt     2.277 %      2846 x        0.180 Mt        0.223 Mt       20.152 Mt     3.935 % 	 ..../RayTracing

		     215.307 Mt    42.043 %      2846 x        0.076 Mt        0.091 Mt      212.531 Mt    98.711 % 	 ...../Deferred

		       2.776 Mt     1.289 %      2846 x        0.001 Mt        0.001 Mt        2.776 Mt   100.000 % 	 ....../DeferredRLPrep

		     183.575 Mt    35.847 %      2846 x        0.065 Mt        0.085 Mt      181.861 Mt    99.066 % 	 ...../RayCasting

		       1.715 Mt     0.934 %      2846 x        0.001 Mt        0.001 Mt        1.715 Mt   100.000 % 	 ....../RayTracingRLPrep

		      93.081 Mt    18.176 %      2846 x        0.033 Mt        0.037 Mt       93.081 Mt   100.000 % 	 ...../Resolve

		   21649.475 Mt    96.274 %      2846 x        7.607 Mt        9.942 Mt    21649.475 Mt   100.000 % 	 ..../Present

		       2.755 Mt     0.011 %         1 x        2.755 Mt        0.000 Mt        2.755 Mt   100.000 % 	 ../AmbientOcclusionRLInitialization

		       5.099 Mt     0.020 %         1 x        5.099 Mt        0.000 Mt        5.099 Mt   100.000 % 	 ../DeferredRLInitialization

		      52.315 Mt     0.201 %         1 x       52.315 Mt        0.000 Mt        2.375 Mt     4.539 % 	 ../RayTracingRLInitialization

		      49.940 Mt    95.461 %         1 x       49.940 Mt        0.000 Mt       49.940 Mt   100.000 % 	 .../PipelineBuild

		    2647.690 Mt    10.154 %         1 x     2647.690 Mt        0.000 Mt        0.529 Mt     0.020 % 	 ../Initialization

		    2647.161 Mt    99.980 %         1 x     2647.161 Mt        0.000 Mt        2.144 Mt     0.081 % 	 .../ScenePrep

		    2574.522 Mt    97.256 %         1 x     2574.522 Mt        0.000 Mt      250.300 Mt     9.722 % 	 ..../SceneLoad

		    2048.436 Mt    79.566 %         1 x     2048.436 Mt        0.000 Mt      584.158 Mt    28.517 % 	 ...../glTF-LoadScene

		    1464.278 Mt    71.483 %        69 x       21.221 Mt        0.000 Mt     1464.278 Mt   100.000 % 	 ....../glTF-LoadImageData

		     209.665 Mt     8.144 %         1 x      209.665 Mt        0.000 Mt      209.665 Mt   100.000 % 	 ...../glTF-CreateScene

		      66.120 Mt     2.568 %         1 x       66.120 Mt        0.000 Mt       66.120 Mt   100.000 % 	 ...../SceneDesc-CreateScene

		       1.154 Mt     0.044 %         1 x        1.154 Mt        0.000 Mt        0.001 Mt     0.052 % 	 ..../ShadowMapRLPrep

		       1.153 Mt    99.948 %         1 x        1.153 Mt        0.000 Mt        1.116 Mt    96.749 % 	 ...../PrepareForRendering

		       0.037 Mt     3.251 %         1 x        0.037 Mt        0.000 Mt        0.037 Mt   100.000 % 	 ....../PrepareDrawBundle

		       3.466 Mt     0.131 %         1 x        3.466 Mt        0.000 Mt        0.000 Mt     0.012 % 	 ..../TexturedRLPrep

		       3.466 Mt    99.988 %         1 x        3.466 Mt        0.000 Mt        3.430 Mt    98.973 % 	 ...../PrepareForRendering

		       0.001 Mt     0.029 %         1 x        0.001 Mt        0.000 Mt        0.001 Mt   100.000 % 	 ....../PrepareMaterials

		       0.035 Mt     0.998 %         1 x        0.035 Mt        0.000 Mt        0.035 Mt   100.000 % 	 ....../PrepareDrawBundle

		       4.531 Mt     0.171 %         1 x        4.531 Mt        0.000 Mt        0.000 Mt     0.011 % 	 ..../DeferredRLPrep

		       4.531 Mt    99.989 %         1 x        4.531 Mt        0.000 Mt        3.994 Mt    88.150 % 	 ...../PrepareForRendering

		       0.001 Mt     0.013 %         1 x        0.001 Mt        0.000 Mt        0.001 Mt   100.000 % 	 ....../PrepareMaterials

		       0.447 Mt     9.864 %         1 x        0.447 Mt        0.000 Mt        0.447 Mt   100.000 % 	 ....../BuildMaterialCache

		       0.089 Mt     1.973 %         1 x        0.089 Mt        0.000 Mt        0.089 Mt   100.000 % 	 ....../PrepareDrawBundle

		      26.738 Mt     1.010 %         1 x       26.738 Mt        0.000 Mt        2.487 Mt     9.302 % 	 ..../RayTracingRLPrep

		       0.062 Mt     0.232 %         1 x        0.062 Mt        0.000 Mt        0.062 Mt   100.000 % 	 ...../PrepareAccelerationStructures

		       0.001 Mt     0.003 %         1 x        0.001 Mt        0.000 Mt        0.001 Mt   100.000 % 	 ...../PrepareCache

		      12.824 Mt    47.962 %         1 x       12.824 Mt        0.000 Mt       12.824 Mt   100.000 % 	 ...../PipelineBuild

		       0.664 Mt     2.483 %         1 x        0.664 Mt        0.000 Mt        0.664 Mt   100.000 % 	 ...../BuildCache

		       9.297 Mt    34.771 %         1 x        9.297 Mt        0.000 Mt        0.001 Mt     0.009 % 	 ...../BuildAccelerationStructures

		       9.296 Mt    99.991 %         1 x        9.296 Mt        0.000 Mt        6.852 Mt    73.701 % 	 ....../AccelerationStructureBuild

		       1.625 Mt    17.476 %         1 x        1.625 Mt        0.000 Mt        1.625 Mt   100.000 % 	 ......./BuildBottomLevelAS

		       0.820 Mt     8.823 %         1 x        0.820 Mt        0.000 Mt        0.820 Mt   100.000 % 	 ......./BuildTopLevelAS

		       1.403 Mt     5.246 %         1 x        1.403 Mt        0.000 Mt        1.403 Mt   100.000 % 	 ...../BuildShaderTables

		      34.607 Mt     1.307 %         1 x       34.607 Mt        0.000 Mt       34.607 Mt   100.000 % 	 ..../GpuSidePrep

	WindowThread:

		   26074.115 Mt   100.000 %         1 x    26074.115 Mt    26074.115 Mt    26074.115 Mt   100.000 % 	 /

	GpuThread:

		   20151.130 Mt   100.000 %      2846 x        7.081 Mt        9.266 Mt      155.323 Mt     0.771 % 	 /

		       2.440 Mt     0.012 %         1 x        2.440 Mt        0.000 Mt        0.007 Mt     0.295 % 	 ./ScenePrep

		       2.433 Mt    99.705 %         1 x        2.433 Mt        0.000 Mt        0.001 Mt     0.060 % 	 ../AccelerationStructureBuild

		       2.343 Mt    96.287 %         1 x        2.343 Mt        0.000 Mt        2.343 Mt   100.000 % 	 .../BuildBottomLevelASGpu

		       0.089 Mt     3.652 %         1 x        0.089 Mt        0.000 Mt        0.089 Mt   100.000 % 	 .../BuildTopLevelASGpu

		   19900.940 Mt    98.758 %      2846 x        6.993 Mt        9.171 Mt       10.061 Mt     0.051 % 	 ./RayTracingGpu

		    6119.402 Mt    30.749 %      2846 x        2.150 Mt        4.079 Mt     6119.402 Mt   100.000 % 	 ../DeferredRLGpu

		    6673.214 Mt    33.532 %      2846 x        2.345 Mt        2.601 Mt     6673.214 Mt   100.000 % 	 ../RayTracingRLGpu

		    7098.264 Mt    35.668 %      2846 x        2.494 Mt        2.489 Mt     7098.264 Mt   100.000 % 	 ../ResolveRLGpu

		      92.427 Mt     0.459 %      2846 x        0.032 Mt        0.094 Mt       92.427 Mt   100.000 % 	 ./Present


	============================


