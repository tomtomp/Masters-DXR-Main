Configuration: 
Command line parameters  = E:\programming\FIT-DIP\Masters-DXR-Main-git\build\Measurement\x64\Release\Measurement.exe --scene Suzanne/Suzanne.gltf --profile-automation --profile-camera-track Suzanne/Suzanne.trk --rt-mode-pure --width 2560 --height 1440 --profile-output PresetSuzanne1440RpSoftShadows8 --quality-preset SoftShadows8 
Build type               = Release
Device name              = NVIDIA GeForce RTX 2080 Ti
Device memory MB         = 11049
Ray Tracing backend      = Hardware
Window width             = 2560
Window height            = 1421
Window title             = Direct3D App
Back-buffer count        = 3
Fill Triangles           = enabled
Triangles Font Clock-Wise= enabled
Fast Build AS            = disabled
Target FPS               = 0
Target UPS               = 60
Tearing                  = enabled
VSync                    = disabled
GUI rendering            = disabled
Debug layer              = disabled
GPU validation           = disabled
GPU profiling            = enabled
Render mode              = Ray Tracing
Loaded scene file        = Suzanne/Suzanne.gltf
Using testing scene      = disabled
Duplication              = 1
Duplication in cube      = disabled
Duplication offset       = 10
BLAS duplication         = disabled
Rays per pixel           = 9
Quality preset           = SoftShadows8
Rasterization Quality    : 
  AO                     = disabled
  AO kernel size         = 1
  Shadows                = enabled
  Shadow map resolution  = 8192
  Shadow PCF kernel size = 8
  Reflections            = disabled
Ray Tracing Quality      : 
  AO                     = disabled
  AO rays                = 0
  AO kernel size         = 8
  Shadows                = enabled
  Shadow rays            = 8
  Shadow kernel size     = 8
  Reflections            = disabled

ProfilingAggregator: 
Render	,	0.9889	,	1.4674	,	1.8625	,	1.4055	,	1.1948	,	0.8645	,	0.6577	,	0.7558	,	0.6837	,	1.1302	,	1.2667	,	0.9845	,	0.8099	,	0.8053	,	0.9746	,	1.7196	,	2.09	,	2.5807	,	3.9781	,	2.7473	,	2.0656	,	1.0599	,	0.8755	,	0.682
Update	,	0.0787	,	0.0424	,	0.0815	,	0.04	,	0.0799	,	0.0352	,	0.0714	,	0.0371	,	0.0754	,	0.0789	,	0.0721	,	0.0744	,	0.0737	,	0.0692	,	0.0327	,	0.0807	,	0.0574	,	0.077	,	0.0866	,	0.0818	,	0.0385	,	0.0395	,	0.0345	,	0.0383
RayTracing	,	0.0251	,	0.0657	,	0.079	,	0.0305	,	0.0748	,	0.0564	,	0.0227	,	0.0545	,	0.0207	,	0.0723	,	0.0701	,	0.0557	,	0.0563	,	0.0542	,	0.054	,	0.0717	,	0.0842	,	0.0324	,	0.0782	,	0.0353	,	0.0716	,	0.034	,	0.0578	,	0.0208
RayTracingGpu	,	0.785184	,	1.06227	,	1.22784	,	1.0609	,	0.749376	,	0.536128	,	0.45344	,	0.418336	,	0.507648	,	0.710016	,	0.82032	,	0.637952	,	0.468352	,	0.466688	,	0.63376	,	1.1575	,	1.61645	,	2.22922	,	3.47286	,	2.34438	,	1.51366	,	0.778528	,	0.535552	,	0.486432
RayTracingRLGpu	,	0.78448	,	1.06061	,	1.22714	,	1.05926	,	0.748672	,	0.534464	,	0.451936	,	0.416832	,	0.50608	,	0.708544	,	0.818752	,	0.637248	,	0.467552	,	0.465856	,	0.633056	,	1.15578	,	1.61494	,	2.22723	,	3.47101	,	2.34352	,	1.51293	,	0.777664	,	0.534752	,	0.4856
Index	,	0	,	1	,	2	,	3	,	4	,	5	,	6	,	7	,	8	,	9	,	10	,	11	,	12	,	13	,	14	,	15	,	16	,	17	,	18	,	19	,	20	,	21	,	22	,	23

ProfilingBuildAggregator: 
BuildBottomLevelAS	,	0.9692
BuildBottomLevelASGpu	,	0.445888
BuildTopLevelAS	,	0.9888
BuildTopLevelASGpu	,	0.062624
Duplication	,	1
Triangles	,	3936
Meshes	,	1
BottomLevels	,	1
TopLevelSize	,	64
BottomLevelsSize	,	282112
Index	,	0

FpsAggregator: 
FPS	,	825	,	708	,	583	,	598	,	723	,	981	,	1215	,	1325	,	1299	,	1039	,	821	,	857	,	1127	,	1322	,	1130	,	771	,	524	,	414	,	295	,	292	,	416	,	629	,	998	,	1218
UPS	,	59	,	60	,	60	,	61	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60
FrameTime	,	1.2127	,	1.41378	,	1.71529	,	1.67311	,	1.38358	,	1.02015	,	0.823422	,	0.754956	,	0.769928	,	0.963496	,	1.21827	,	1.16732	,	0.887967	,	0.756453	,	0.885331	,	1.29876	,	1.91169	,	2.41712	,	3.39524	,	3.42664	,	2.40814	,	1.59133	,	1.00207	,	0.821054
GigaRays/s	,	26.9974	,	23.1576	,	19.0871	,	19.5682	,	23.6631	,	32.0931	,	39.7607	,	43.3665	,	42.5233	,	33.9803	,	26.874	,	28.047	,	36.8706	,	43.2807	,	36.9803	,	25.2085	,	17.1261	,	13.545	,	9.64287	,	9.55451	,	13.5955	,	20.5739	,	32.6723	,	39.8754
Index	,	0	,	1	,	2	,	3	,	4	,	5	,	6	,	7	,	8	,	9	,	10	,	11	,	12	,	13	,	14	,	15	,	16	,	17	,	18	,	19	,	20	,	21	,	22	,	23


AccelerationStructure: 
Ray tracing acceleration structure building statistics: 
	Bottom Level Acceleration Structure: 
		Maximum [B]: 282112
		Minimum [B]: 282112
		Currently Allocated [B]: 282112
		Currently Created [num]: 1
		Current Average [B]: 282112
		Maximum Triangles [num]: 3936
		Minimum Triangles [num]: 3936
		Total Triangles [num]: 3936
		Maximum Meshes [num]: 1
		Minimum Meshes [num]: 1
		Total Meshes [num]: 1
	Top Level Acceleration Structure: 
		Maximum [B]: 64
		Minimum [B]: 64
		Currently Allocated [B]: 64
		Total Allocated [B]: 64
		Total Created [num]: 1
		Average Allocated [B]: 64
		Maximum Instances [num]: 1
		Minimum Instances [num]: 1
		Total Instances [num]: 1
	Scratch Buffer: 
		Maximum [B]: 82944
		Minimum [B]: 82944
		Currently Allocated [B]: 82944
		Total Allocated [B]: 82944
		Total Created [num]: 1
		Average Allocated [B]: 82944
ProfilingStack: 
[PROF] Profiling statistics: 
	Scopes entered : 266969
	Scopes exited : 266968
	Overhead per scope [ticks] : 103.9
	Threads entered : 3
	Threads exited : 0
	============================
		         Sum[Mt]   Parent[%]  Samples[num]       Avg[Mt]   Last/Curr[Mt]        Self[Mt]     Self[%] 	 Name

	main:

		   24473.535 Mt   100.000 %         1 x    24473.535 Mt    24473.535 Mt        0.454 Mt     0.002 % 	 /

		   24473.090 Mt    99.998 %         1 x    24473.091 Mt    24473.090 Mt      274.891 Mt     1.123 % 	 ./Main application loop

		       2.049 Mt     0.008 %         1 x        2.049 Mt        0.000 Mt        2.049 Mt   100.000 % 	 ../TexturedRLInitialization

		       1.046 Mt     0.004 %         1 x        1.046 Mt        0.000 Mt        1.046 Mt   100.000 % 	 ../ShadowMapRLInitialization

		       4.003 Mt     0.016 %         1 x        4.003 Mt        0.000 Mt        4.003 Mt   100.000 % 	 ../RasterResolveRLInitialization

		       3.890 Mt     0.016 %         1 x        3.890 Mt        0.000 Mt        3.890 Mt   100.000 % 	 ../ResolveRLInitialization

		   24017.500 Mt    98.138 %    104587 x        0.230 Mt        1.000 Mt      198.205 Mt     0.825 % 	 ../MainLoop

		     148.660 Mt     0.619 %      1441 x        0.103 Mt        0.062 Mt      148.429 Mt    99.845 % 	 .../Update

		       0.231 Mt     0.155 %         1 x        0.231 Mt        0.000 Mt        0.231 Mt   100.000 % 	 ..../GuiModelApply

		   23670.635 Mt    98.556 %     20112 x        1.177 Mt        0.936 Mt     1015.986 Mt     4.292 % 	 .../Render

		     936.947 Mt     3.958 %     20112 x        0.047 Mt        0.098 Mt       60.074 Mt     6.412 % 	 ..../RayTracing

		     876.873 Mt    93.588 %     20112 x        0.044 Mt        0.093 Mt      868.164 Mt    99.007 % 	 ...../RayCasting

		       8.709 Mt     0.993 %     20112 x        0.000 Mt        0.000 Mt        8.709 Mt   100.000 % 	 ....../RayTracingRLPrep

		   21717.703 Mt    91.750 %     20112 x        1.080 Mt        0.761 Mt    21717.703 Mt   100.000 % 	 ..../Present

		       2.485 Mt     0.010 %         1 x        2.485 Mt        0.000 Mt        2.485 Mt   100.000 % 	 ../AmbientOcclusionRLInitialization

		       4.660 Mt     0.019 %         1 x        4.660 Mt        0.000 Mt        4.660 Mt   100.000 % 	 ../DeferredRLInitialization

		      52.568 Mt     0.215 %         1 x       52.568 Mt        0.000 Mt        3.729 Mt     7.094 % 	 ../RayTracingRLInitialization

		      48.839 Mt    92.906 %         1 x       48.839 Mt        0.000 Mt       48.839 Mt   100.000 % 	 .../PipelineBuild

		     109.999 Mt     0.449 %         1 x      109.999 Mt        0.000 Mt        0.488 Mt     0.443 % 	 ../Initialization

		     109.511 Mt    99.557 %         1 x      109.511 Mt        0.000 Mt        1.036 Mt     0.946 % 	 .../ScenePrep

		      64.976 Mt    59.332 %         1 x       64.976 Mt        0.000 Mt       16.937 Mt    26.067 % 	 ..../SceneLoad

		      39.986 Mt    61.539 %         1 x       39.986 Mt        0.000 Mt        8.630 Mt    21.584 % 	 ...../glTF-LoadScene

		      31.355 Mt    78.416 %         2 x       15.678 Mt        0.000 Mt       31.355 Mt   100.000 % 	 ....../glTF-LoadImageData

		       6.035 Mt     9.288 %         1 x        6.035 Mt        0.000 Mt        6.035 Mt   100.000 % 	 ...../glTF-CreateScene

		       2.018 Mt     3.106 %         1 x        2.018 Mt        0.000 Mt        2.018 Mt   100.000 % 	 ...../SceneDesc-CreateScene

		       0.309 Mt     0.282 %         1 x        0.309 Mt        0.000 Mt        0.000 Mt     0.129 % 	 ..../ShadowMapRLPrep

		       0.308 Mt    99.871 %         1 x        0.308 Mt        0.000 Mt        0.300 Mt    97.245 % 	 ...../PrepareForRendering

		       0.009 Mt     2.755 %         1 x        0.009 Mt        0.000 Mt        0.009 Mt   100.000 % 	 ....../PrepareDrawBundle

		       1.595 Mt     1.457 %         1 x        1.595 Mt        0.000 Mt        0.001 Mt     0.038 % 	 ..../TexturedRLPrep

		       1.595 Mt    99.962 %         1 x        1.595 Mt        0.000 Mt        1.585 Mt    99.367 % 	 ...../PrepareForRendering

		       0.001 Mt     0.056 %         1 x        0.001 Mt        0.000 Mt        0.001 Mt   100.000 % 	 ....../PrepareMaterials

		       0.009 Mt     0.577 %         1 x        0.009 Mt        0.000 Mt        0.009 Mt   100.000 % 	 ....../PrepareDrawBundle

		       1.766 Mt     1.613 %         1 x        1.766 Mt        0.000 Mt        0.001 Mt     0.062 % 	 ..../DeferredRLPrep

		       1.765 Mt    99.938 %         1 x        1.765 Mt        0.000 Mt        1.423 Mt    80.618 % 	 ...../PrepareForRendering

		       0.002 Mt     0.113 %         1 x        0.002 Mt        0.000 Mt        0.002 Mt   100.000 % 	 ....../PrepareMaterials

		       0.337 Mt    19.071 %         1 x        0.337 Mt        0.000 Mt        0.337 Mt   100.000 % 	 ....../BuildMaterialCache

		       0.004 Mt     0.198 %         1 x        0.004 Mt        0.000 Mt        0.004 Mt   100.000 % 	 ....../PrepareDrawBundle

		      27.181 Mt    24.820 %         1 x       27.181 Mt        0.000 Mt        3.006 Mt    11.060 % 	 ..../RayTracingRLPrep

		       0.050 Mt     0.185 %         1 x        0.050 Mt        0.000 Mt        0.050 Mt   100.000 % 	 ...../PrepareAccelerationStructures

		       0.001 Mt     0.003 %         1 x        0.001 Mt        0.000 Mt        0.001 Mt   100.000 % 	 ...../PrepareCache

		      15.861 Mt    58.355 %         1 x       15.861 Mt        0.000 Mt       15.861 Mt   100.000 % 	 ...../PipelineBuild

		       0.369 Mt     1.356 %         1 x        0.369 Mt        0.000 Mt        0.369 Mt   100.000 % 	 ...../BuildCache

		       4.875 Mt    17.936 %         1 x        4.875 Mt        0.000 Mt        0.000 Mt     0.004 % 	 ...../BuildAccelerationStructures

		       4.875 Mt    99.996 %         1 x        4.875 Mt        0.000 Mt        2.917 Mt    59.835 % 	 ....../AccelerationStructureBuild

		       0.969 Mt    19.881 %         1 x        0.969 Mt        0.000 Mt        0.969 Mt   100.000 % 	 ......./BuildBottomLevelAS

		       0.989 Mt    20.283 %         1 x        0.989 Mt        0.000 Mt        0.989 Mt   100.000 % 	 ......./BuildTopLevelAS

		       3.019 Mt    11.105 %         1 x        3.019 Mt        0.000 Mt        3.019 Mt   100.000 % 	 ...../BuildShaderTables

		      12.648 Mt    11.549 %         1 x       12.648 Mt        0.000 Mt       12.648 Mt   100.000 % 	 ..../GpuSidePrep

	WindowThread:

		   24472.004 Mt   100.000 %         1 x    24472.004 Mt    24472.004 Mt    24472.004 Mt   100.000 % 	 /

	GpuThread:

		   16151.984 Mt   100.000 %     20112 x        0.803 Mt        0.485 Mt      135.880 Mt     0.841 % 	 /

		       0.515 Mt     0.003 %         1 x        0.515 Mt        0.000 Mt        0.006 Mt     1.161 % 	 ./ScenePrep

		       0.509 Mt    98.839 %         1 x        0.509 Mt        0.000 Mt        0.001 Mt     0.182 % 	 ../AccelerationStructureBuild

		       0.446 Mt    87.525 %         1 x        0.446 Mt        0.000 Mt        0.446 Mt   100.000 % 	 .../BuildBottomLevelASGpu

		       0.063 Mt    12.293 %         1 x        0.063 Mt        0.000 Mt        0.063 Mt   100.000 % 	 .../BuildTopLevelASGpu

		   15979.415 Mt    98.932 %     20112 x        0.795 Mt        0.484 Mt       25.378 Mt     0.159 % 	 ./RayTracingGpu

		   15954.037 Mt    99.841 %     20112 x        0.793 Mt        0.483 Mt    15954.037 Mt   100.000 % 	 ../RayTracingRLGpu

		      36.175 Mt     0.224 %     20112 x        0.002 Mt        0.002 Mt       36.175 Mt   100.000 % 	 ./Present


	============================


