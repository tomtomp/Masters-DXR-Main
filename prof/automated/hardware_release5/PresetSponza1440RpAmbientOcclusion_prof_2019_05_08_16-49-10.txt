Configuration: 
Command line parameters  = E:\programming\FIT-DIP\Masters-DXR-Main-git\build\Measurement\x64\Release\Measurement.exe --scene Sponza/Sponza.gltf --profile-automation --profile-camera-track Sponza/SponzaPreset.trk --rt-mode-pure --width 2560 --height 1440 --profile-output PresetSponza1440RpAmbientOcclusion --quality-preset AmbientOcclusion 
Build type               = Release
Device name              = NVIDIA GeForce RTX 2080 Ti
Device memory MB         = 11049
Ray Tracing backend      = Hardware
Window width             = 2560
Window height            = 1421
Window title             = Direct3D App
Back-buffer count        = 3
Fill Triangles           = enabled
Triangles Font Clock-Wise= enabled
Fast Build AS            = disabled
Target FPS               = 0
Target UPS               = 60
Tearing                  = enabled
VSync                    = disabled
GUI rendering            = disabled
Debug layer              = disabled
GPU validation           = disabled
GPU profiling            = enabled
Render mode              = Ray Tracing
Loaded scene file        = Sponza/Sponza.gltf
Using testing scene      = disabled
Duplication              = 1
Duplication in cube      = disabled
Duplication offset       = 10
BLAS duplication         = disabled
Rays per pixel           = 5
Quality preset           = AmbientOcclusion
Rasterization Quality    : 
  AO                     = enabled
  AO kernel size         = 4
  Shadows                = disabled
  Shadow map resolution  = 1
  Shadow PCF kernel size = 0
  Reflections            = disabled
Ray Tracing Quality      : 
  AO                     = enabled
  AO rays                = 4
  AO kernel size         = 4
  Shadows                = disabled
  Shadow rays            = 0
  Shadow kernel size     = 4
  Reflections            = disabled

ProfilingAggregator: 
Render	,	4.1077	,	3.8434	,	3.5247	,	3.437	,	3.232	,	3.3373	,	3.1254	,	3.6054	,	3.3911	,	3.3348	,	3.0095	,	3.3273	,	3.3013	,	3.4714	,	3.3736	,	3.8608	,	3.9271	,	3.7633	,	3.6291	,	3.7421	,	3.9056	,	3.8653	,	3.7155
Update	,	0.078	,	0.081	,	0.0433	,	0.0411	,	0.0417	,	0.0672	,	0.0776	,	0.0401	,	0.0399	,	0.0405	,	0.0767	,	0.0786	,	0.0391	,	0.0836	,	0.0916	,	0.0825	,	0.0814	,	0.0445	,	0.082	,	0.0443	,	0.0846	,	0.0834	,	0.081
RayTracing	,	0.1066	,	0.0458	,	0.0401	,	0.0826	,	0.0397	,	0.0318	,	0.0315	,	0.0822	,	0.0737	,	0.0731	,	0.0701	,	0.0338	,	0.0292	,	0.0338	,	0.0731	,	0.0727	,	0.0368	,	0.047	,	0.0551	,	0.0614	,	0.0594	,	0.0814	,	0.0387
RayTracingGpu	,	3.43795	,	3.30621	,	3.04922	,	2.91597	,	2.84579	,	2.95843	,	2.85642	,	3.03014	,	2.85882	,	2.67178	,	2.57946	,	2.9911	,	3.04912	,	3.17162	,	2.93248	,	3.256	,	3.4729	,	3.24374	,	3.11146	,	3.17405	,	3.2864	,	3.27046	,	3.28672
RayTracingRLGpu	,	3.43728	,	3.30506	,	3.04826	,	2.91507	,	2.84509	,	2.95674	,	2.85491	,	3.02931	,	2.85728	,	2.67094	,	2.57789	,	2.99037	,	3.04842	,	3.17091	,	2.93162	,	3.25526	,	3.47219	,	3.24189	,	3.1097	,	3.17309	,	3.28525	,	3.26957	,	3.28582
Index	,	0	,	1	,	2	,	3	,	4	,	5	,	6	,	7	,	8	,	9	,	10	,	11	,	12	,	13	,	14	,	15	,	16	,	17	,	18	,	19	,	20	,	21	,	22

ProfilingBuildAggregator: 
BuildBottomLevelAS	,	1.7774
BuildBottomLevelASGpu	,	2.33981
BuildTopLevelAS	,	0.6374
BuildTopLevelASGpu	,	0.089376
Duplication	,	1
Triangles	,	262267
Meshes	,	103
BottomLevels	,	1
TopLevelSize	,	64
BottomLevelsSize	,	18390656
Index	,	0

FpsAggregator: 
FPS	,	199	,	253	,	273	,	279	,	293	,	276	,	296	,	274	,	285	,	298	,	311	,	304	,	285	,	272	,	266	,	287	,	276	,	273	,	280	,	289	,	255	,	255	,	254
UPS	,	59	,	60	,	61	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	61	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	61	,	60	,	60
FrameTime	,	5.03137	,	3.95457	,	3.67193	,	3.59411	,	3.41629	,	3.63496	,	3.38865	,	3.65358	,	3.50894	,	3.35669	,	3.22053	,	3.29174	,	3.51905	,	3.67787	,	3.76267	,	3.49445	,	3.63191	,	3.672	,	3.58258	,	3.47158	,	3.93192	,	3.93219	,	3.93982
GigaRays/s	,	3.61508	,	4.59944	,	4.95347	,	5.06073	,	5.32414	,	5.00385	,	5.36757	,	4.97835	,	5.18356	,	5.41867	,	5.64777	,	5.52558	,	5.16867	,	4.94547	,	4.83401	,	5.20506	,	5.00806	,	4.95337	,	5.07701	,	5.23934	,	4.62594	,	4.62562	,	4.61666
Index	,	0	,	1	,	2	,	3	,	4	,	5	,	6	,	7	,	8	,	9	,	10	,	11	,	12	,	13	,	14	,	15	,	16	,	17	,	18	,	19	,	20	,	21	,	22


AccelerationStructure: 
Ray tracing acceleration structure building statistics: 
	Bottom Level Acceleration Structure: 
		Maximum [B]: 18390656
		Minimum [B]: 18390656
		Currently Allocated [B]: 18390656
		Currently Created [num]: 1
		Current Average [B]: 18390656
		Maximum Triangles [num]: 262267
		Minimum Triangles [num]: 262267
		Total Triangles [num]: 262267
		Maximum Meshes [num]: 103
		Minimum Meshes [num]: 103
		Total Meshes [num]: 103
	Top Level Acceleration Structure: 
		Maximum [B]: 64
		Minimum [B]: 64
		Currently Allocated [B]: 64
		Total Allocated [B]: 64
		Total Created [num]: 1
		Average Allocated [B]: 64
		Maximum Instances [num]: 1
		Minimum Instances [num]: 1
		Total Instances [num]: 1
	Scratch Buffer: 
		Maximum [B]: 14995072
		Minimum [B]: 14995072
		Currently Allocated [B]: 14995072
		Total Allocated [B]: 14995072
		Total Created [num]: 1
		Average Allocated [B]: 14995072
ProfilingStack: 
[PROF] Profiling statistics: 
	Scopes entered : 138225
	Scopes exited : 138224
	Overhead per scope [ticks] : 101.6
	Threads entered : 3
	Threads exited : 0
	============================
		         Sum[Mt]   Parent[%]  Samples[num]       Avg[Mt]   Last/Curr[Mt]        Self[Mt]     Self[%] 	 Name

	main:

		   26520.828 Mt   100.000 %         1 x    26520.829 Mt    26520.828 Mt        0.382 Mt     0.001 % 	 /

		   26520.477 Mt    99.999 %         1 x    26520.478 Mt    26520.477 Mt      266.980 Mt     1.007 % 	 ./Main application loop

		       2.401 Mt     0.009 %         1 x        2.401 Mt        0.000 Mt        2.401 Mt   100.000 % 	 ../TexturedRLInitialization

		       1.277 Mt     0.005 %         1 x        1.277 Mt        0.000 Mt        1.277 Mt   100.000 % 	 ../ShadowMapRLInitialization

		       5.323 Mt     0.020 %         1 x        5.323 Mt        0.000 Mt        5.323 Mt   100.000 % 	 ../RasterResolveRLInitialization

		       3.787 Mt     0.014 %         1 x        3.787 Mt        0.000 Mt        3.787 Mt   100.000 % 	 ../ResolveRLInitialization

		   23055.154 Mt    86.933 %     86050 x        0.268 Mt        3.861 Mt      203.311 Mt     0.882 % 	 ../MainLoop

		     160.417 Mt     0.696 %      1383 x        0.116 Mt        0.068 Mt      160.152 Mt    99.835 % 	 .../Update

		       0.265 Mt     0.165 %         1 x        0.265 Mt        0.000 Mt        0.265 Mt   100.000 % 	 ..../GuiModelApply

		   22691.426 Mt    98.422 %      6335 x        3.582 Mt        3.791 Mt      443.037 Mt     1.952 % 	 .../Render

		     372.785 Mt     1.643 %      6335 x        0.059 Mt        0.082 Mt       25.655 Mt     6.882 % 	 ..../RayTracing

		     347.130 Mt    93.118 %      6335 x        0.055 Mt        0.078 Mt      343.439 Mt    98.937 % 	 ...../RayCasting

		       3.691 Mt     1.063 %      6335 x        0.001 Mt        0.000 Mt        3.691 Mt   100.000 % 	 ....../RayTracingRLPrep

		   21875.604 Mt    96.405 %      6335 x        3.453 Mt        3.609 Mt    21875.604 Mt   100.000 % 	 ..../Present

		       2.815 Mt     0.011 %         1 x        2.815 Mt        0.000 Mt        2.815 Mt   100.000 % 	 ../AmbientOcclusionRLInitialization

		       6.881 Mt     0.026 %         1 x        6.881 Mt        0.000 Mt        6.881 Mt   100.000 % 	 ../DeferredRLInitialization

		      68.723 Mt     0.259 %         1 x       68.723 Mt        0.000 Mt        2.372 Mt     3.452 % 	 ../RayTracingRLInitialization

		      66.351 Mt    96.548 %         1 x       66.351 Mt        0.000 Mt       66.351 Mt   100.000 % 	 .../PipelineBuild

		    3107.135 Mt    11.716 %         1 x     3107.135 Mt        0.000 Mt        0.222 Mt     0.007 % 	 ../Initialization

		    3106.913 Mt    99.993 %         1 x     3106.913 Mt        0.000 Mt        2.442 Mt     0.079 % 	 .../ScenePrep

		    3038.870 Mt    97.810 %         1 x     3038.870 Mt        0.000 Mt      756.760 Mt    24.903 % 	 ..../SceneLoad

		    2007.233 Mt    66.052 %         1 x     2007.233 Mt        0.000 Mt      560.455 Mt    27.922 % 	 ...../glTF-LoadScene

		    1446.778 Mt    72.078 %        69 x       20.968 Mt        0.000 Mt     1446.778 Mt   100.000 % 	 ....../glTF-LoadImageData

		     202.381 Mt     6.660 %         1 x      202.381 Mt        0.000 Mt      202.381 Mt   100.000 % 	 ...../glTF-CreateScene

		      72.496 Mt     2.386 %         1 x       72.496 Mt        0.000 Mt       72.496 Mt   100.000 % 	 ...../SceneDesc-CreateScene

		       0.873 Mt     0.028 %         1 x        0.873 Mt        0.000 Mt        0.001 Mt     0.069 % 	 ..../ShadowMapRLPrep

		       0.872 Mt    99.931 %         1 x        0.872 Mt        0.000 Mt        0.836 Mt    95.885 % 	 ...../PrepareForRendering

		       0.036 Mt     4.115 %         1 x        0.036 Mt        0.000 Mt        0.036 Mt   100.000 % 	 ....../PrepareDrawBundle

		       3.841 Mt     0.124 %         1 x        3.841 Mt        0.000 Mt        0.001 Mt     0.016 % 	 ..../TexturedRLPrep

		       3.841 Mt    99.984 %         1 x        3.841 Mt        0.000 Mt        3.737 Mt    97.303 % 	 ...../PrepareForRendering

		       0.001 Mt     0.029 %         1 x        0.001 Mt        0.000 Mt        0.001 Mt   100.000 % 	 ....../PrepareMaterials

		       0.102 Mt     2.669 %         1 x        0.102 Mt        0.000 Mt        0.102 Mt   100.000 % 	 ....../PrepareDrawBundle

		       2.898 Mt     0.093 %         1 x        2.898 Mt        0.000 Mt        0.001 Mt     0.038 % 	 ..../DeferredRLPrep

		       2.897 Mt    99.962 %         1 x        2.897 Mt        0.000 Mt        2.340 Mt    80.763 % 	 ...../PrepareForRendering

		       0.001 Mt     0.035 %         1 x        0.001 Mt        0.000 Mt        0.001 Mt   100.000 % 	 ....../PrepareMaterials

		       0.351 Mt    12.109 %         1 x        0.351 Mt        0.000 Mt        0.351 Mt   100.000 % 	 ....../BuildMaterialCache

		       0.205 Mt     7.094 %         1 x        0.205 Mt        0.000 Mt        0.205 Mt   100.000 % 	 ....../PrepareDrawBundle

		      27.157 Mt     0.874 %         1 x       27.157 Mt        0.000 Mt        2.844 Mt    10.472 % 	 ..../RayTracingRLPrep

		       0.178 Mt     0.654 %         1 x        0.178 Mt        0.000 Mt        0.178 Mt   100.000 % 	 ...../PrepareAccelerationStructures

		       0.003 Mt     0.010 %         1 x        0.003 Mt        0.000 Mt        0.003 Mt   100.000 % 	 ...../PrepareCache

		      13.112 Mt    48.284 %         1 x       13.112 Mt        0.000 Mt       13.112 Mt   100.000 % 	 ...../PipelineBuild

		       1.296 Mt     4.772 %         1 x        1.296 Mt        0.000 Mt        1.296 Mt   100.000 % 	 ...../BuildCache

		       4.872 Mt    17.940 %         1 x        4.872 Mt        0.000 Mt        0.001 Mt     0.025 % 	 ...../BuildAccelerationStructures

		       4.871 Mt    99.975 %         1 x        4.871 Mt        0.000 Mt        2.456 Mt    50.423 % 	 ....../AccelerationStructureBuild

		       1.777 Mt    36.491 %         1 x        1.777 Mt        0.000 Mt        1.777 Mt   100.000 % 	 ......./BuildBottomLevelAS

		       0.637 Mt    13.086 %         1 x        0.637 Mt        0.000 Mt        0.637 Mt   100.000 % 	 ......./BuildTopLevelAS

		       4.852 Mt    17.868 %         1 x        4.852 Mt        0.000 Mt        4.852 Mt   100.000 % 	 ...../BuildShaderTables

		      30.832 Mt     0.992 %         1 x       30.832 Mt        0.000 Mt       30.832 Mt   100.000 % 	 ..../GpuSidePrep

	WindowThread:

		   26519.403 Mt   100.000 %         1 x    26519.403 Mt    26519.403 Mt    26519.403 Mt   100.000 % 	 /

	GpuThread:

		   19708.997 Mt   100.000 %      6335 x        3.111 Mt        3.257 Mt      146.711 Mt     0.744 % 	 /

		       2.437 Mt     0.012 %         1 x        2.437 Mt        0.000 Mt        0.006 Mt     0.243 % 	 ./ScenePrep

		       2.431 Mt    99.757 %         1 x        2.431 Mt        0.000 Mt        0.001 Mt     0.058 % 	 ../AccelerationStructureBuild

		       2.340 Mt    96.265 %         1 x        2.340 Mt        0.000 Mt        2.340 Mt   100.000 % 	 .../BuildBottomLevelASGpu

		       0.089 Mt     3.677 %         1 x        0.089 Mt        0.000 Mt        0.089 Mt   100.000 % 	 .../BuildTopLevelASGpu

		   19548.772 Mt    99.187 %      6335 x        3.086 Mt        3.255 Mt        8.077 Mt     0.041 % 	 ./RayTracingGpu

		   19540.695 Mt    99.959 %      6335 x        3.085 Mt        3.255 Mt    19540.695 Mt   100.000 % 	 ../RayTracingRLGpu

		      11.076 Mt     0.056 %      6335 x        0.002 Mt        0.002 Mt       11.076 Mt   100.000 % 	 ./Present


	============================


