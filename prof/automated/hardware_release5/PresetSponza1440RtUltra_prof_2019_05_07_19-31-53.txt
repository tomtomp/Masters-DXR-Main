Configuration: 
Command line parameters  = E:\programming\FIT-DIP\Masters-DXR-Main-git\build\Measurement\x64\Release\Measurement.exe --scene Sponza/Sponza.gltf --profile-automation --profile-camera-track Sponza/SponzaPreset.trk --rt-mode --width 2560 --height 1440 --profile-output PresetSponza1440RtUltra --quality-preset Ultra 
Build type               = Release
Device name              = NVIDIA GeForce RTX 2080 Ti
Device memory MB         = 11049
Ray Tracing backend      = Hardware
Window width             = 2560
Window height            = 1421
Window title             = Direct3D App
Back-buffer count        = 3
Fill Triangles           = enabled
Triangles Font Clock-Wise= enabled
Fast Build AS            = disabled
Target FPS               = 0
Target UPS               = 60
Tearing                  = enabled
VSync                    = disabled
GUI rendering            = disabled
Debug layer              = disabled
GPU validation           = disabled
GPU profiling            = enabled
Render mode              = Ray Tracing
Loaded scene file        = Sponza/Sponza.gltf
Using testing scene      = disabled
Duplication              = 1
Duplication in cube      = disabled
Duplication offset       = 10
BLAS duplication         = disabled
Rays per pixel           = 33
Quality preset           = Ultra
Rasterization Quality    : 
  AO                     = enabled
  AO kernel size         = 16
  Shadows                = enabled
  Shadow map resolution  = 8192
  Shadow PCF kernel size = 8
  Reflections            = enabled
Ray Tracing Quality      : 
  AO                     = enabled
  AO rays                = 16
  AO kernel size         = 8
  Shadows                = enabled
  Shadow rays            = 8
  Shadow kernel size     = 8
  Reflections            = enabled

ProfilingAggregator: 
Render	,	24.9007	,	24.8256	,	24.2383	,	21.8166	,	21.8771	,	24.0144	,	24.3351	,	25.2092	,	24.951	,	24.9016	,	24.722	,	24.8812	,	25.0547	,	24.9964	,	22.7493	,	23.3198	,	27.3588	,	25.7251	,	21.8963	,	23.0014	,	23.3383	,	23.0017	,	23.2114
Update	,	0.0392	,	0.0486	,	0.0781	,	0.0735	,	0.0802	,	0.048	,	0.0478	,	0.0484	,	0.0476	,	0.0947	,	0.053	,	0.081	,	0.0782	,	0.0792	,	0.047	,	0.0482	,	0.0494	,	0.0816	,	0.0844	,	0.0908	,	0.0819	,	0.0465	,	0.0808
RayTracing	,	0.1927	,	0.1907	,	0.182	,	0.2045	,	0.1904	,	0.1877	,	0.2187	,	0.21	,	0.2136	,	0.2314	,	0.1833	,	0.1955	,	0.1846	,	0.1845	,	0.1833	,	0.222	,	0.1823	,	0.2081	,	0.2003	,	0.2621	,	0.1888	,	0.1719	,	0.2211
RayTracingGpu	,	23.8731	,	23.9033	,	23.3357	,	20.6031	,	20.5805	,	23.0059	,	23.1804	,	24.061	,	23.8338	,	23.658	,	23.671	,	23.8724	,	24.0606	,	24.0252	,	21.7435	,	22.1821	,	26.2542	,	24.5129	,	20.5759	,	21.8796	,	22.4292	,	22.0972	,	21.8226
DeferredRLGpu	,	2.81821	,	2.65542	,	2.29517	,	1.47741	,	1.35696	,	1.5528	,	2.13843	,	2.3415	,	2.0335	,	1.47754	,	1.38483	,	1.81059	,	2.1519	,	2.44899	,	2.08218	,	1.6103	,	2.06317	,	1.85408	,	1.45773	,	3.07891	,	4.25171	,	3.90227	,	3.85629
RayTracingRLGpu	,	13.4644	,	13.7708	,	13.3634	,	11.6568	,	11.68	,	13.7197	,	13.3915	,	14.2929	,	14.3111	,	14.5783	,	14.5717	,	14.5892	,	14.4854	,	14.1111	,	12.0224	,	12.8385	,	16.4893	,	14.8739	,	11.5127	,	11.1325	,	10.6183	,	10.744	,	10.4599
ResolveRLGpu	,	7.588	,	7.47478	,	7.6751	,	7.46701	,	7.5409	,	7.73088	,	7.64877	,	7.42502	,	7.48701	,	7.5993	,	7.71094	,	7.47053	,	7.42122	,	7.46218	,	7.63597	,	7.73136	,	7.69984	,	7.78307	,	7.60342	,	7.66621	,	7.5559	,	7.4496	,	7.50486
Index	,	0	,	1	,	2	,	3	,	4	,	5	,	6	,	7	,	8	,	9	,	10	,	11	,	12	,	13	,	14	,	15	,	16	,	17	,	18	,	19	,	20	,	21	,	22

ProfilingBuildAggregator: 
BuildBottomLevelAS	,	1.6793
BuildBottomLevelASGpu	,	2.36621
BuildTopLevelAS	,	0.7962
BuildTopLevelASGpu	,	0.089344
Duplication	,	1
Triangles	,	262267
Meshes	,	103
BottomLevels	,	1
TopLevelSize	,	64
BottomLevelsSize	,	18390656
Index	,	0

FpsAggregator: 
FPS	,	27	,	39	,	41	,	43	,	46	,	43	,	44	,	40	,	40	,	40	,	40	,	40	,	40	,	39	,	42	,	46	,	39	,	38	,	43	,	46	,	43	,	43	,	43
UPS	,	59	,	61	,	60	,	61	,	61	,	62	,	61	,	61	,	61	,	60	,	61	,	60	,	61	,	60	,	62	,	61	,	61	,	61	,	61	,	61	,	61	,	61	,	60
FrameTime	,	30.58	,	26.1163	,	24.4063	,	23.6135	,	22.1918	,	23.7613	,	23.1616	,	25.4432	,	25.4837	,	25.1089	,	25.2025	,	25.1755	,	25.444	,	25.6502	,	24.305	,	22.1148	,	26.2411	,	26.9409	,	23.6583	,	22.0073	,	23.5808	,	23.463	,	23.522
GigaRays/s	,	3.19441	,	4.59659	,	4.91864	,	5.08379	,	5.40947	,	5.05216	,	5.18298	,	4.71819	,	4.7107	,	4.78102	,	4.76327	,	4.76837	,	4.71805	,	4.68012	,	4.93915	,	5.42831	,	4.57473	,	4.45591	,	5.07417	,	5.45482	,	5.09083	,	5.11641	,	5.10357
Index	,	0	,	1	,	2	,	3	,	4	,	5	,	6	,	7	,	8	,	9	,	10	,	11	,	12	,	13	,	14	,	15	,	16	,	17	,	18	,	19	,	20	,	21	,	22


AccelerationStructure: 
Ray tracing acceleration structure building statistics: 
	Bottom Level Acceleration Structure: 
		Maximum [B]: 18390656
		Minimum [B]: 18390656
		Currently Allocated [B]: 18390656
		Currently Created [num]: 1
		Current Average [B]: 18390656
		Maximum Triangles [num]: 262267
		Minimum Triangles [num]: 262267
		Total Triangles [num]: 262267
		Maximum Meshes [num]: 103
		Minimum Meshes [num]: 103
		Total Meshes [num]: 103
	Top Level Acceleration Structure: 
		Maximum [B]: 64
		Minimum [B]: 64
		Currently Allocated [B]: 64
		Total Allocated [B]: 64
		Total Created [num]: 1
		Average Allocated [B]: 64
		Maximum Instances [num]: 1
		Minimum Instances [num]: 1
		Total Instances [num]: 1
	Scratch Buffer: 
		Maximum [B]: 14995072
		Minimum [B]: 14995072
		Currently Allocated [B]: 14995072
		Total Allocated [B]: 14995072
		Total Created [num]: 1
		Average Allocated [B]: 14995072
ProfilingStack: 
[PROF] Profiling statistics: 
	Scopes entered : 84991
	Scopes exited : 84990
	Overhead per scope [ticks] : 110.6
	Threads entered : 3
	Threads exited : 0
	============================
		         Sum[Mt]   Parent[%]  Samples[num]       Avg[Mt]   Last/Curr[Mt]        Self[Mt]     Self[%] 	 Name

	main:

		   26454.243 Mt   100.000 %         1 x    26454.243 Mt    26454.242 Mt        0.253 Mt     0.001 % 	 /

		   26454.022 Mt    99.999 %         1 x    26454.024 Mt    26454.022 Mt      276.353 Mt     1.045 % 	 ./Main application loop

		       2.283 Mt     0.009 %         1 x        2.283 Mt        0.000 Mt        2.283 Mt   100.000 % 	 ../TexturedRLInitialization

		       1.244 Mt     0.005 %         1 x        1.244 Mt        0.000 Mt        1.244 Mt   100.000 % 	 ../ShadowMapRLInitialization

		       4.770 Mt     0.018 %         1 x        4.770 Mt        0.000 Mt        4.770 Mt   100.000 % 	 ../RasterResolveRLInitialization

		       3.739 Mt     0.014 %         1 x        3.739 Mt        0.000 Mt        3.739 Mt   100.000 % 	 ../ResolveRLInitialization

		   23370.867 Mt    88.345 %     71181 x        0.328 Mt       37.943 Mt      434.468 Mt     1.859 % 	 ../MainLoop

		     188.789 Mt     0.808 %      1400 x        0.135 Mt        0.033 Mt      188.179 Mt    99.677 % 	 .../Update

		       0.610 Mt     0.323 %         1 x        0.610 Mt        0.000 Mt        0.610 Mt   100.000 % 	 ..../GuiModelApply

		   22747.610 Mt    97.333 %       946 x       24.046 Mt       23.466 Mt      121.600 Mt     0.535 % 	 .../Render

		     192.640 Mt     0.847 %       946 x        0.204 Mt        0.235 Mt        6.716 Mt     3.486 % 	 ..../RayTracing

		      83.243 Mt    43.212 %       946 x        0.088 Mt        0.096 Mt       82.661 Mt    99.301 % 	 ...../Deferred

		       0.582 Mt     0.699 %       946 x        0.001 Mt        0.001 Mt        0.582 Mt   100.000 % 	 ....../DeferredRLPrep

		      69.384 Mt    36.018 %       946 x        0.073 Mt        0.091 Mt       68.800 Mt    99.158 % 	 ...../RayCasting

		       0.584 Mt     0.842 %       946 x        0.001 Mt        0.001 Mt        0.584 Mt   100.000 % 	 ....../RayTracingRLPrep

		      33.297 Mt    17.285 %       946 x        0.035 Mt        0.039 Mt       33.297 Mt   100.000 % 	 ...../Resolve

		   22433.371 Mt    98.619 %       946 x       23.714 Mt       23.065 Mt    22433.371 Mt   100.000 % 	 ..../Present

		       2.733 Mt     0.010 %         1 x        2.733 Mt        0.000 Mt        2.733 Mt   100.000 % 	 ../AmbientOcclusionRLInitialization

		       4.582 Mt     0.017 %         1 x        4.582 Mt        0.000 Mt        4.582 Mt   100.000 % 	 ../DeferredRLInitialization

		      51.307 Mt     0.194 %         1 x       51.307 Mt        0.000 Mt        2.730 Mt     5.322 % 	 ../RayTracingRLInitialization

		      48.576 Mt    94.678 %         1 x       48.576 Mt        0.000 Mt       48.576 Mt   100.000 % 	 .../PipelineBuild

		    2736.145 Mt    10.343 %         1 x     2736.145 Mt        0.000 Mt        0.218 Mt     0.008 % 	 ../Initialization

		    2735.927 Mt    99.992 %         1 x     2735.927 Mt        0.000 Mt        2.110 Mt     0.077 % 	 .../ScenePrep

		    2667.653 Mt    97.505 %         1 x     2667.653 Mt        0.000 Mt      272.939 Mt    10.231 % 	 ..../SceneLoad

		    2102.648 Mt    78.820 %         1 x     2102.648 Mt        0.000 Mt      612.621 Mt    29.136 % 	 ...../glTF-LoadScene

		    1490.027 Mt    70.864 %        69 x       21.595 Mt        0.000 Mt     1490.027 Mt   100.000 % 	 ....../glTF-LoadImageData

		     229.248 Mt     8.594 %         1 x      229.248 Mt        0.000 Mt      229.248 Mt   100.000 % 	 ...../glTF-CreateScene

		      62.817 Mt     2.355 %         1 x       62.817 Mt        0.000 Mt       62.817 Mt   100.000 % 	 ...../SceneDesc-CreateScene

		       1.101 Mt     0.040 %         1 x        1.101 Mt        0.000 Mt        0.000 Mt     0.045 % 	 ..../ShadowMapRLPrep

		       1.101 Mt    99.955 %         1 x        1.101 Mt        0.000 Mt        1.059 Mt    96.212 % 	 ...../PrepareForRendering

		       0.042 Mt     3.788 %         1 x        0.042 Mt        0.000 Mt        0.042 Mt   100.000 % 	 ....../PrepareDrawBundle

		       3.723 Mt     0.136 %         1 x        3.723 Mt        0.000 Mt        0.000 Mt     0.011 % 	 ..../TexturedRLPrep

		       3.722 Mt    99.989 %         1 x        3.722 Mt        0.000 Mt        3.684 Mt    98.960 % 	 ...../PrepareForRendering

		       0.001 Mt     0.035 %         1 x        0.001 Mt        0.000 Mt        0.001 Mt   100.000 % 	 ....../PrepareMaterials

		       0.037 Mt     1.005 %         1 x        0.037 Mt        0.000 Mt        0.037 Mt   100.000 % 	 ....../PrepareDrawBundle

		       3.130 Mt     0.114 %         1 x        3.130 Mt        0.000 Mt        0.001 Mt     0.026 % 	 ..../DeferredRLPrep

		       3.130 Mt    99.974 %         1 x        3.130 Mt        0.000 Mt        2.422 Mt    77.389 % 	 ...../PrepareForRendering

		       0.001 Mt     0.016 %         1 x        0.001 Mt        0.000 Mt        0.001 Mt   100.000 % 	 ....../PrepareMaterials

		       0.486 Mt    15.542 %         1 x        0.486 Mt        0.000 Mt        0.486 Mt   100.000 % 	 ....../BuildMaterialCache

		       0.221 Mt     7.052 %         1 x        0.221 Mt        0.000 Mt        0.221 Mt   100.000 % 	 ....../PrepareDrawBundle

		      27.249 Mt     0.996 %         1 x       27.249 Mt        0.000 Mt        2.622 Mt     9.620 % 	 ..../RayTracingRLPrep

		       0.185 Mt     0.680 %         1 x        0.185 Mt        0.000 Mt        0.185 Mt   100.000 % 	 ...../PrepareAccelerationStructures

		       0.003 Mt     0.009 %         1 x        0.003 Mt        0.000 Mt        0.003 Mt   100.000 % 	 ...../PrepareCache

		      13.280 Mt    48.736 %         1 x       13.280 Mt        0.000 Mt       13.280 Mt   100.000 % 	 ...../PipelineBuild

		       0.664 Mt     2.435 %         1 x        0.664 Mt        0.000 Mt        0.664 Mt   100.000 % 	 ...../BuildCache

		       9.060 Mt    33.248 %         1 x        9.060 Mt        0.000 Mt        0.000 Mt     0.004 % 	 ...../BuildAccelerationStructures

		       9.059 Mt    99.996 %         1 x        9.059 Mt        0.000 Mt        6.584 Mt    72.675 % 	 ....../AccelerationStructureBuild

		       1.679 Mt    18.537 %         1 x        1.679 Mt        0.000 Mt        1.679 Mt   100.000 % 	 ......./BuildBottomLevelAS

		       0.796 Mt     8.789 %         1 x        0.796 Mt        0.000 Mt        0.796 Mt   100.000 % 	 ......./BuildTopLevelAS

		       1.436 Mt     5.271 %         1 x        1.436 Mt        0.000 Mt        1.436 Mt   100.000 % 	 ...../BuildShaderTables

		      30.960 Mt     1.132 %         1 x       30.960 Mt        0.000 Mt       30.960 Mt   100.000 % 	 ..../GpuSidePrep

	WindowThread:

		   26453.473 Mt   100.000 %         1 x    26453.473 Mt    26453.473 Mt    26453.473 Mt   100.000 % 	 /

	GpuThread:

		   21907.328 Mt   100.000 %       946 x       23.158 Mt       22.252 Mt      142.107 Mt     0.649 % 	 /

		       2.464 Mt     0.011 %         1 x        2.464 Mt        0.000 Mt        0.006 Mt     0.257 % 	 ./ScenePrep

		       2.457 Mt    99.743 %         1 x        2.457 Mt        0.000 Mt        0.002 Mt     0.072 % 	 ../AccelerationStructureBuild

		       2.366 Mt    96.293 %         1 x        2.366 Mt        0.000 Mt        2.366 Mt   100.000 % 	 .../BuildBottomLevelASGpu

		       0.089 Mt     3.636 %         1 x        0.089 Mt        0.000 Mt        0.089 Mt   100.000 % 	 .../BuildTopLevelASGpu

		   21697.922 Mt    99.044 %       946 x       22.936 Mt       21.885 Mt        2.541 Mt     0.012 % 	 ./RayTracingGpu

		    2086.548 Mt     9.616 %       946 x        2.206 Mt        3.866 Mt     2086.548 Mt   100.000 % 	 ../DeferredRLGpu

		   12396.424 Mt    57.132 %       946 x       13.104 Mt       10.572 Mt    12396.424 Mt   100.000 % 	 ../RayTracingRLGpu

		    7212.409 Mt    33.240 %       946 x        7.624 Mt        7.445 Mt     7212.409 Mt   100.000 % 	 ../ResolveRLGpu

		      64.835 Mt     0.296 %       946 x        0.069 Mt        0.368 Mt       64.835 Mt   100.000 % 	 ./Present


	============================


