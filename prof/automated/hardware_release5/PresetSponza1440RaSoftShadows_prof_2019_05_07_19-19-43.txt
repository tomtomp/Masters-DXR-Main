Configuration: 
Command line parameters  = E:\programming\FIT-DIP\Masters-DXR-Main-git\build\Measurement\x64\Release\Measurement.exe --scene Sponza/Sponza.gltf --profile-automation --profile-camera-track Sponza/SponzaPreset.trk --width 2560 --height 1440 --profile-output PresetSponza1440RaSoftShadows --quality-preset SoftShadows 
Build type               = Release
Device name              = NVIDIA GeForce RTX 2080 Ti
Device memory MB         = 11049
Ray Tracing backend      = Hardware
Window width             = 2560
Window height            = 1421
Window title             = Direct3D App
Back-buffer count        = 3
Fill Triangles           = enabled
Triangles Font Clock-Wise= enabled
Fast Build AS            = disabled
Target FPS               = 0
Target UPS               = 60
Tearing                  = enabled
VSync                    = disabled
GUI rendering            = disabled
Debug layer              = disabled
GPU validation           = disabled
GPU profiling            = enabled
Render mode              = Rasterization
Loaded scene file        = Sponza/Sponza.gltf
Using testing scene      = disabled
Duplication              = 1
Duplication in cube      = disabled
Duplication offset       = 10
BLAS duplication         = disabled
Rays per pixel           = 4
Quality preset           = SoftShadows
Rasterization Quality    : 
  AO                     = disabled
  AO kernel size         = 1
  Shadows                = enabled
  Shadow map resolution  = 4096
  Shadow PCF kernel size = 4
  Reflections            = disabled
Ray Tracing Quality      : 
  AO                     = disabled
  AO rays                = 0
  AO kernel size         = 4
  Shadows                = enabled
  Shadow rays            = 4
  Shadow kernel size     = 4
  Reflections            = disabled

ProfilingAggregator: 
Render	,	5.3817	,	5.1282	,	4.7347	,	3.9421	,	3.8051	,	4.034	,	4.9293	,	5.8222	,	5.2996	,	4.6008	,	3.9687	,	4.9553	,	4.7565	,	5.0091	,	4.8163	,	3.6393	,	4.4444	,	4.3938	,	3.9868	,	5.1828	,	6.49	,	6.4195	,	6.4444
Update	,	0.0765	,	0.0782	,	0.0777	,	0.0787	,	0.0775	,	0.0771	,	0.0777	,	0.0757	,	0.0791	,	0.0777	,	0.0777	,	0.0758	,	0.076	,	0.0747	,	0.0769	,	0.0377	,	0.0768	,	0.0772	,	0.0766	,	0.078	,	0.0756	,	0.0762	,	0.0745
DeferredRLGpu	,	2.73926	,	2.48198	,	2.17798	,	1.46858	,	1.34176	,	1.58768	,	1.99494	,	2.78074	,	2.46624	,	1.7441	,	1.33011	,	2.12813	,	2.14803	,	2.41629	,	2.18109	,	1.4817	,	1.94576	,	1.89216	,	1.49277	,	2.67891	,	3.87427	,	3.84714	,	3.88614
Index	,	0	,	1	,	2	,	3	,	4	,	5	,	6	,	7	,	8	,	9	,	10	,	11	,	12	,	13	,	14	,	15	,	16	,	17	,	18	,	19	,	20	,	21	,	22

ProfilingBuildAggregator: 
BuildBottomLevelAS	,	1.6818
BuildBottomLevelASGpu	,	2.3648
BuildTopLevelAS	,	0.8421
BuildTopLevelASGpu	,	0.089376
Duplication	,	1
Triangles	,	262267
Meshes	,	103
BottomLevels	,	1
TopLevelSize	,	64
BottomLevelsSize	,	18390656
Index	,	0

FpsAggregator: 
FPS	,	121	,	178	,	199	,	216	,	248	,	245	,	233	,	193	,	200	,	221	,	247	,	236	,	214	,	198	,	192	,	231	,	224	,	216	,	228	,	230	,	161	,	148	,	148
UPS	,	59	,	60	,	61	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	61	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	61
FrameTime	,	8.29086	,	5.6196	,	5.03268	,	4.64135	,	4.04729	,	4.09114	,	4.30228	,	5.19494	,	5.00324	,	4.53344	,	4.05181	,	4.23971	,	4.68104	,	5.06861	,	5.21576	,	4.33347	,	4.47937	,	4.63068	,	4.39475	,	4.35217	,	6.22115	,	6.76267	,	6.76276
GigaRays/s	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0
Index	,	0	,	1	,	2	,	3	,	4	,	5	,	6	,	7	,	8	,	9	,	10	,	11	,	12	,	13	,	14	,	15	,	16	,	17	,	18	,	19	,	20	,	21	,	22


AccelerationStructure: 
Ray tracing acceleration structure building statistics: 
	Bottom Level Acceleration Structure: 
		Maximum [B]: 18390656
		Minimum [B]: 18390656
		Currently Allocated [B]: 18390656
		Currently Created [num]: 1
		Current Average [B]: 18390656
		Maximum Triangles [num]: 262267
		Minimum Triangles [num]: 262267
		Total Triangles [num]: 262267
		Maximum Meshes [num]: 103
		Minimum Meshes [num]: 103
		Total Meshes [num]: 103
	Top Level Acceleration Structure: 
		Maximum [B]: 64
		Minimum [B]: 64
		Currently Allocated [B]: 64
		Total Allocated [B]: 64
		Total Created [num]: 1
		Average Allocated [B]: 64
		Maximum Instances [num]: 1
		Minimum Instances [num]: 1
		Total Instances [num]: 1
	Scratch Buffer: 
		Maximum [B]: 14995072
		Minimum [B]: 14995072
		Currently Allocated [B]: 14995072
		Total Allocated [B]: 14995072
		Total Created [num]: 1
		Average Allocated [B]: 14995072
ProfilingStack: 
[PROF] Profiling statistics: 
	Scopes entered : 82291
	Scopes exited : 82290
	Overhead per scope [ticks] : 101.6
	Threads entered : 3
	Threads exited : 0
	============================
		         Sum[Mt]   Parent[%]  Samples[num]       Avg[Mt]   Last/Curr[Mt]        Self[Mt]     Self[%] 	 Name

	main:

		   26111.137 Mt   100.000 %         1 x    26111.138 Mt    26111.137 Mt        0.475 Mt     0.002 % 	 /

		   26110.694 Mt    99.998 %         1 x    26110.696 Mt    26110.694 Mt      265.832 Mt     1.018 % 	 ./Main application loop

		       2.272 Mt     0.009 %         1 x        2.272 Mt        0.000 Mt        2.272 Mt   100.000 % 	 ../TexturedRLInitialization

		       1.298 Mt     0.005 %         1 x        1.298 Mt        0.000 Mt        1.298 Mt   100.000 % 	 ../ShadowMapRLInitialization

		       5.112 Mt     0.020 %         1 x        5.112 Mt        0.000 Mt        5.112 Mt   100.000 % 	 ../RasterResolveRLInitialization

		       3.549 Mt     0.014 %         1 x        3.549 Mt        0.000 Mt        3.549 Mt   100.000 % 	 ../ResolveRLInitialization

		   23062.634 Mt    88.326 %     33506 x        0.688 Mt        7.562 Mt      418.539 Mt     1.815 % 	 ../MainLoop

		     197.189 Mt     0.855 %      1383 x        0.143 Mt        0.085 Mt      196.593 Mt    99.698 % 	 .../Update

		       0.596 Mt     0.302 %         1 x        0.596 Mt        0.000 Mt        0.596 Mt   100.000 % 	 ..../GuiModelApply

		   22446.907 Mt    97.330 %      4729 x        4.747 Mt        7.474 Mt      522.776 Mt     2.329 % 	 .../Render

		     698.649 Mt     3.112 %      4729 x        0.148 Mt        0.168 Mt      691.228 Mt    98.938 % 	 ..../Rasterization

		       4.752 Mt     0.680 %      4729 x        0.001 Mt        0.001 Mt        4.752 Mt   100.000 % 	 ...../DeferredRLPrep

		       2.670 Mt     0.382 %      4729 x        0.001 Mt        0.001 Mt        2.670 Mt   100.000 % 	 ...../ShadowMapRLPrep

		   21225.481 Mt    94.559 %      4729 x        4.488 Mt        7.164 Mt    21225.481 Mt   100.000 % 	 ..../Present

		       2.846 Mt     0.011 %         1 x        2.846 Mt        0.000 Mt        2.846 Mt   100.000 % 	 ../AmbientOcclusionRLInitialization

		       5.429 Mt     0.021 %         1 x        5.429 Mt        0.000 Mt        5.429 Mt   100.000 % 	 ../DeferredRLInitialization

		      51.678 Mt     0.198 %         1 x       51.678 Mt        0.000 Mt        2.388 Mt     4.621 % 	 ../RayTracingRLInitialization

		      49.290 Mt    95.379 %         1 x       49.290 Mt        0.000 Mt       49.290 Mt   100.000 % 	 .../PipelineBuild

		    2710.044 Mt    10.379 %         1 x     2710.044 Mt        0.000 Mt        0.529 Mt     0.020 % 	 ../Initialization

		    2709.515 Mt    99.980 %         1 x     2709.515 Mt        0.000 Mt        2.119 Mt     0.078 % 	 .../ScenePrep

		    2636.746 Mt    97.314 %         1 x     2636.746 Mt        0.000 Mt      256.930 Mt     9.744 % 	 ..../SceneLoad

		    2083.291 Mt    79.010 %         1 x     2083.291 Mt        0.000 Mt      610.787 Mt    29.318 % 	 ...../glTF-LoadScene

		    1472.505 Mt    70.682 %        69 x       21.341 Mt        0.000 Mt     1472.505 Mt   100.000 % 	 ....../glTF-LoadImageData

		     227.901 Mt     8.643 %         1 x      227.901 Mt        0.000 Mt      227.901 Mt   100.000 % 	 ...../glTF-CreateScene

		      68.624 Mt     2.603 %         1 x       68.624 Mt        0.000 Mt       68.624 Mt   100.000 % 	 ...../SceneDesc-CreateScene

		       1.130 Mt     0.042 %         1 x        1.130 Mt        0.000 Mt        0.000 Mt     0.035 % 	 ..../ShadowMapRLPrep

		       1.129 Mt    99.965 %         1 x        1.129 Mt        0.000 Mt        1.088 Mt    96.361 % 	 ...../PrepareForRendering

		       0.041 Mt     3.639 %         1 x        0.041 Mt        0.000 Mt        0.041 Mt   100.000 % 	 ....../PrepareDrawBundle

		       3.626 Mt     0.134 %         1 x        3.626 Mt        0.000 Mt        0.001 Mt     0.017 % 	 ..../TexturedRLPrep

		       3.625 Mt    99.983 %         1 x        3.625 Mt        0.000 Mt        3.590 Mt    99.021 % 	 ...../PrepareForRendering

		       0.001 Mt     0.033 %         1 x        0.001 Mt        0.000 Mt        0.001 Mt   100.000 % 	 ....../PrepareMaterials

		       0.034 Mt     0.946 %         1 x        0.034 Mt        0.000 Mt        0.034 Mt   100.000 % 	 ....../PrepareDrawBundle

		       4.870 Mt     0.180 %         1 x        4.870 Mt        0.000 Mt        0.001 Mt     0.012 % 	 ..../DeferredRLPrep

		       4.870 Mt    99.988 %         1 x        4.870 Mt        0.000 Mt        4.377 Mt    89.879 % 	 ...../PrepareForRendering

		       0.001 Mt     0.018 %         1 x        0.001 Mt        0.000 Mt        0.001 Mt   100.000 % 	 ....../PrepareMaterials

		       0.439 Mt     9.006 %         1 x        0.439 Mt        0.000 Mt        0.439 Mt   100.000 % 	 ....../BuildMaterialCache

		       0.053 Mt     1.097 %         1 x        0.053 Mt        0.000 Mt        0.053 Mt   100.000 % 	 ....../PrepareDrawBundle

		      26.942 Mt     0.994 %         1 x       26.942 Mt        0.000 Mt        2.546 Mt     9.448 % 	 ..../RayTracingRLPrep

		       0.074 Mt     0.274 %         1 x        0.074 Mt        0.000 Mt        0.074 Mt   100.000 % 	 ...../PrepareAccelerationStructures

		       0.001 Mt     0.004 %         1 x        0.001 Mt        0.000 Mt        0.001 Mt   100.000 % 	 ...../PrepareCache

		      12.967 Mt    48.129 %         1 x       12.967 Mt        0.000 Mt       12.967 Mt   100.000 % 	 ...../PipelineBuild

		       0.664 Mt     2.465 %         1 x        0.664 Mt        0.000 Mt        0.664 Mt   100.000 % 	 ...../BuildCache

		       9.232 Mt    34.268 %         1 x        9.232 Mt        0.000 Mt        0.001 Mt     0.006 % 	 ...../BuildAccelerationStructures

		       9.232 Mt    99.994 %         1 x        9.232 Mt        0.000 Mt        6.708 Mt    72.661 % 	 ....../AccelerationStructureBuild

		       1.682 Mt    18.218 %         1 x        1.682 Mt        0.000 Mt        1.682 Mt   100.000 % 	 ......./BuildBottomLevelAS

		       0.842 Mt     9.122 %         1 x        0.842 Mt        0.000 Mt        0.842 Mt   100.000 % 	 ......./BuildTopLevelAS

		       1.458 Mt     5.413 %         1 x        1.458 Mt        0.000 Mt        1.458 Mt   100.000 % 	 ...../BuildShaderTables

		      34.082 Mt     1.258 %         1 x       34.082 Mt        0.000 Mt       34.082 Mt   100.000 % 	 ..../GpuSidePrep

	WindowThread:

		   26109.737 Mt   100.000 %         1 x    26109.737 Mt    26109.737 Mt    26109.737 Mt   100.000 % 	 /

	GpuThread:

		   18756.572 Mt   100.000 %      4729 x        3.966 Mt        6.656 Mt      156.321 Mt     0.833 % 	 /

		       2.463 Mt     0.013 %         1 x        2.463 Mt        0.000 Mt        0.007 Mt     0.290 % 	 ./ScenePrep

		       2.456 Mt    99.710 %         1 x        2.456 Mt        0.000 Mt        0.001 Mt     0.057 % 	 ../AccelerationStructureBuild

		       2.365 Mt    96.303 %         1 x        2.365 Mt        0.000 Mt        2.365 Mt   100.000 % 	 .../BuildBottomLevelASGpu

		       0.089 Mt     3.640 %         1 x        0.089 Mt        0.000 Mt        0.089 Mt   100.000 % 	 .../BuildTopLevelASGpu

		   18500.978 Mt    98.637 %      4729 x        3.912 Mt        6.561 Mt       16.947 Mt     0.092 % 	 ./RasterizationGpu

		    9912.322 Mt    53.577 %      4729 x        2.096 Mt        4.409 Mt     9912.322 Mt   100.000 % 	 ../DeferredRLGpu

		     747.552 Mt     4.041 %      4729 x        0.158 Mt        0.239 Mt      747.552 Mt   100.000 % 	 ../ShadowMapRLGpu

		    7824.157 Mt    42.291 %      4729 x        1.655 Mt        1.911 Mt     7824.157 Mt   100.000 % 	 ../RasterResolveRLGpu

		      96.811 Mt     0.516 %      4729 x        0.020 Mt        0.094 Mt       96.811 Mt   100.000 % 	 ./Present


	============================


