Configuration: 
Command line parameters  = E:\programming\FIT-DIP\Masters-DXR-Main-git\build\Measurement\x64\Release\Measurement.exe --scene Cube/Cube.gltf --quality-preset Low --profile-automation --profile-camera-track Cube/Cube.trk --width 1024 --height 768 --profile-output TrackCube768Ra 
Build type               = Release
Device name              = NVIDIA GeForce RTX 2080 Ti
Device memory MB         = 11049
Ray Tracing backend      = Hardware
Window width             = 1024
Window height            = 768
Window title             = Direct3D App
Back-buffer count        = 3
Fill Triangles           = enabled
Triangles Font Clock-Wise= enabled
Fast Build AS            = disabled
Target FPS               = 0
Target UPS               = 60
Tearing                  = enabled
VSync                    = disabled
GUI rendering            = disabled
Debug layer              = disabled
GPU validation           = disabled
GPU profiling            = enabled
Render mode              = Rasterization
Loaded scene file        = Cube/Cube.gltf
Using testing scene      = disabled
Duplication              = 1
Duplication in cube      = disabled
Duplication offset       = 10
BLAS duplication         = disabled
Rays per pixel           = 5
Quality preset           = Low
Rasterization Quality    : 
  AO                     = enabled
  AO kernel size         = 4
  Shadows                = enabled
  Shadow map resolution  = 2048
  Shadow PCF kernel size = 0
  Reflections            = disabled
Ray Tracing Quality      : 
  AO                     = enabled
  AO rays                = 4
  AO kernel size         = 4
  Shadows                = enabled
  Shadow rays            = 1
  Shadow kernel size     = 4
  Reflections            = disabled

ProfilingAggregator: 
Render	,	0.4268	,	0.453	,	0.4588	,	0.4372	,	0.7187	,	0.7096	,	0.7597	,	0.7551	,	0.7908	,	0.7948	,	0.4885	,	0.5422	,	0.6537	,	0.4397	,	0.4522	,	0.6701	,	0.6945	,	0.5228	,	0.6385	,	0.6116	,	0.5862	,	0.4589	,	0.5181	,	0.5138	,	0.4123	,	0.4624	,	0.5341	,	0.448	,	0.3958	,	0.4992
Update	,	0.0225	,	0.0246	,	0.0243	,	0.0216	,	0.061	,	0.0251	,	0.0219	,	0.0244	,	0.069	,	0.0252	,	0.0266	,	0.0278	,	0.0684	,	0.0243	,	0.0262	,	0.0585	,	0.019	,	0.0184	,	0.0594	,	0.0433	,	0.0397	,	0.0194	,	0.0227	,	0.0397	,	0.0603	,	0.0601	,	0.0605	,	0.0197	,	0.0645	,	0.0572
DeferredRLGpu	,	0.053888	,	0.070592	,	0.074752	,	0.07664	,	0.080192	,	0.0976	,	0.088544	,	0.097184	,	0.104768	,	0.102912	,	0.094112	,	0.097344	,	0.082304	,	0.074848	,	0.084448	,	0.097568	,	0.107136	,	0.108672	,	0.108288	,	0.10816	,	0.09264	,	0.06816	,	0.061056	,	0.053312	,	0.04544	,	0.036576	,	0.027808	,	0.02256	,	0.019904	,	0.019776
Index	,	0	,	1	,	2	,	3	,	4	,	5	,	6	,	7	,	8	,	9	,	10	,	11	,	12	,	13	,	14	,	15	,	16	,	17	,	18	,	19	,	20	,	21	,	22	,	23	,	24	,	25	,	26	,	27	,	28	,	29

ProfilingBuildAggregator: 
BuildBottomLevelAS	,	0.5641
BuildBottomLevelASGpu	,	0.132128
BuildTopLevelAS	,	0.5354
BuildTopLevelASGpu	,	0.060608
Duplication	,	1
Triangles	,	12
Meshes	,	1
BottomLevels	,	1
TopLevelSize	,	64
BottomLevelsSize	,	4480
Index	,	0

FpsAggregator: 
FPS	,	2011	,	2097	,	2022	,	2052	,	2012	,	1853	,	1884	,	1916	,	1796	,	1860	,	1890	,	1876	,	1870	,	2094	,	1980	,	1912	,	1687	,	1790	,	1650	,	1666	,	1666	,	1861	,	1923	,	1940	,	2014	,	2142	,	2110	,	2246	,	2410	,	2330
UPS	,	59	,	61	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60
FrameTime	,	0.497441	,	0.477085	,	0.494771	,	0.48753	,	0.497066	,	0.539751	,	0.531116	,	0.522123	,	0.556953	,	0.538014	,	0.529137	,	0.533182	,	0.53481	,	0.477723	,	0.505095	,	0.523138	,	0.592984	,	0.558745	,	0.606219	,	0.600489	,	0.600584	,	0.537372	,	0.52029	,	0.515675	,	0.496674	,	0.466862	,	0.474143	,	0.44532	,	0.414996	,	0.429219
GigaRays/s	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0
Index	,	0	,	1	,	2	,	3	,	4	,	5	,	6	,	7	,	8	,	9	,	10	,	11	,	12	,	13	,	14	,	15	,	16	,	17	,	18	,	19	,	20	,	21	,	22	,	23	,	24	,	25	,	26	,	27	,	28	,	29


AccelerationStructure: 
Ray tracing acceleration structure building statistics: 
	Bottom Level Acceleration Structure: 
		Maximum [B]: 4480
		Minimum [B]: 4480
		Currently Allocated [B]: 4480
		Currently Created [num]: 1
		Current Average [B]: 4480
		Maximum Triangles [num]: 12
		Minimum Triangles [num]: 12
		Total Triangles [num]: 12
		Maximum Meshes [num]: 1
		Minimum Meshes [num]: 1
		Total Meshes [num]: 1
	Top Level Acceleration Structure: 
		Maximum [B]: 64
		Minimum [B]: 64
		Currently Allocated [B]: 64
		Total Allocated [B]: 64
		Total Created [num]: 1
		Average Allocated [B]: 64
		Maximum Instances [num]: 1
		Minimum Instances [num]: 1
		Total Instances [num]: 1
	Scratch Buffer: 
		Maximum [B]: 896
		Minimum [B]: 896
		Currently Allocated [B]: 896
		Total Allocated [B]: 896
		Total Created [num]: 1
		Average Allocated [B]: 896
ProfilingStack: 
[PROF] Profiling statistics: 
	Scopes entered : 795035
	Scopes exited : 795034
	Overhead per scope [ticks] : 103.3
	Threads entered : 3
	Threads exited : 0
	============================
		         Sum[Mt]   Parent[%]  Samples[num]       Avg[Mt]   Last/Curr[Mt]        Self[Mt]     Self[%] 	 Name

	main:

		   30398.103 Mt   100.000 %         1 x    30398.104 Mt    30398.103 Mt        0.290 Mt     0.001 % 	 /

		   30397.823 Mt    99.999 %         1 x    30397.823 Mt    30397.823 Mt      265.817 Mt     0.874 % 	 ./Main application loop

		       2.764 Mt     0.009 %         1 x        2.764 Mt        0.000 Mt        2.764 Mt   100.000 % 	 ../TexturedRLInitialization

		       1.239 Mt     0.004 %         1 x        1.239 Mt        0.000 Mt        1.239 Mt   100.000 % 	 ../ShadowMapRLInitialization

		       3.979 Mt     0.013 %         1 x        3.979 Mt        0.000 Mt        3.979 Mt   100.000 % 	 ../RasterResolveRLInitialization

		       3.513 Mt     0.012 %         1 x        3.513 Mt        0.000 Mt        3.513 Mt   100.000 % 	 ../ResolveRLInitialization

		   30007.310 Mt    98.715 %    149007 x        0.201 Mt        0.374 Mt      318.603 Mt     1.062 % 	 ../MainLoop

		     115.499 Mt     0.385 %      1801 x        0.064 Mt        0.026 Mt      115.241 Mt    99.776 % 	 .../Update

		       0.259 Mt     0.224 %         1 x        0.259 Mt        0.000 Mt        0.259 Mt   100.000 % 	 ..../GuiModelApply

		   29573.208 Mt    98.553 %     58562 x        0.505 Mt        0.347 Mt     1648.482 Mt     5.574 % 	 .../Render

		    3324.450 Mt    11.241 %     58562 x        0.057 Mt        0.041 Mt     3293.134 Mt    99.058 % 	 ..../Rasterization

		      17.058 Mt     0.513 %     58562 x        0.000 Mt        0.000 Mt       17.058 Mt   100.000 % 	 ...../DeferredRLPrep

		      14.258 Mt     0.429 %     58562 x        0.000 Mt        0.000 Mt       14.258 Mt   100.000 % 	 ...../ShadowMapRLPrep

		   24600.276 Mt    83.184 %     58562 x        0.420 Mt        0.284 Mt    24600.276 Mt   100.000 % 	 ..../Present

		       2.946 Mt     0.010 %         1 x        2.946 Mt        0.000 Mt        2.946 Mt   100.000 % 	 ../AmbientOcclusionRLInitialization

		       3.974 Mt     0.013 %         1 x        3.974 Mt        0.000 Mt        3.974 Mt   100.000 % 	 ../DeferredRLInitialization

		      52.609 Mt     0.173 %         1 x       52.609 Mt        0.000 Mt        1.913 Mt     3.636 % 	 ../RayTracingRLInitialization

		      50.696 Mt    96.364 %         1 x       50.696 Mt        0.000 Mt       50.696 Mt   100.000 % 	 .../PipelineBuild

		      53.672 Mt     0.177 %         1 x       53.672 Mt        0.000 Mt        0.182 Mt     0.339 % 	 ../Initialization

		      53.490 Mt    99.661 %         1 x       53.490 Mt        0.000 Mt        0.628 Mt     1.175 % 	 .../ScenePrep

		      21.482 Mt    40.160 %         1 x       21.482 Mt        0.000 Mt        8.722 Mt    40.602 % 	 ..../SceneLoad

		      10.567 Mt    49.193 %         1 x       10.567 Mt        0.000 Mt        2.549 Mt    24.119 % 	 ...../glTF-LoadScene

		       8.019 Mt    75.881 %         2 x        4.009 Mt        0.000 Mt        8.019 Mt   100.000 % 	 ....../glTF-LoadImageData

		       1.405 Mt     6.539 %         1 x        1.405 Mt        0.000 Mt        1.405 Mt   100.000 % 	 ...../glTF-CreateScene

		       0.788 Mt     3.666 %         1 x        0.788 Mt        0.000 Mt        0.788 Mt   100.000 % 	 ...../SceneDesc-CreateScene

		       0.161 Mt     0.302 %         1 x        0.161 Mt        0.000 Mt        0.000 Mt     0.248 % 	 ..../ShadowMapRLPrep

		       0.161 Mt    99.752 %         1 x        0.161 Mt        0.000 Mt        0.153 Mt    95.028 % 	 ...../PrepareForRendering

		       0.008 Mt     4.972 %         1 x        0.008 Mt        0.000 Mt        0.008 Mt   100.000 % 	 ....../PrepareDrawBundle

		       0.986 Mt     1.844 %         1 x        0.986 Mt        0.000 Mt        0.000 Mt     0.041 % 	 ..../TexturedRLPrep

		       0.986 Mt    99.959 %         1 x        0.986 Mt        0.000 Mt        0.982 Mt    99.574 % 	 ...../PrepareForRendering

		       0.001 Mt     0.091 %         1 x        0.001 Mt        0.000 Mt        0.001 Mt   100.000 % 	 ....../PrepareMaterials

		       0.003 Mt     0.335 %         1 x        0.003 Mt        0.000 Mt        0.003 Mt   100.000 % 	 ....../PrepareDrawBundle

		       0.956 Mt     1.787 %         1 x        0.956 Mt        0.000 Mt        0.001 Mt     0.052 % 	 ..../DeferredRLPrep

		       0.955 Mt    99.948 %         1 x        0.955 Mt        0.000 Mt        0.792 Mt    82.965 % 	 ...../PrepareForRendering

		       0.001 Mt     0.052 %         1 x        0.001 Mt        0.000 Mt        0.001 Mt   100.000 % 	 ....../PrepareMaterials

		       0.159 Mt    16.668 %         1 x        0.159 Mt        0.000 Mt        0.159 Mt   100.000 % 	 ....../BuildMaterialCache

		       0.003 Mt     0.314 %         1 x        0.003 Mt        0.000 Mt        0.003 Mt   100.000 % 	 ....../PrepareDrawBundle

		      21.238 Mt    39.704 %         1 x       21.238 Mt        0.000 Mt        1.608 Mt     7.569 % 	 ..../RayTracingRLPrep

		       0.043 Mt     0.204 %         1 x        0.043 Mt        0.000 Mt        0.043 Mt   100.000 % 	 ...../PrepareAccelerationStructures

		       0.001 Mt     0.003 %         1 x        0.001 Mt        0.000 Mt        0.001 Mt   100.000 % 	 ...../PrepareCache

		      16.796 Mt    79.087 %         1 x       16.796 Mt        0.000 Mt       16.796 Mt   100.000 % 	 ...../PipelineBuild

		       0.151 Mt     0.712 %         1 x        0.151 Mt        0.000 Mt        0.151 Mt   100.000 % 	 ...../BuildCache

		       2.038 Mt     9.597 %         1 x        2.038 Mt        0.000 Mt        0.001 Mt     0.025 % 	 ...../BuildAccelerationStructures

		       2.038 Mt    99.975 %         1 x        2.038 Mt        0.000 Mt        0.938 Mt    46.039 % 	 ....../AccelerationStructureBuild

		       0.564 Mt    27.685 %         1 x        0.564 Mt        0.000 Mt        0.564 Mt   100.000 % 	 ......./BuildBottomLevelAS

		       0.535 Mt    26.276 %         1 x        0.535 Mt        0.000 Mt        0.535 Mt   100.000 % 	 ......./BuildTopLevelAS

		       0.601 Mt     2.828 %         1 x        0.601 Mt        0.000 Mt        0.601 Mt   100.000 % 	 ...../BuildShaderTables

		       8.039 Mt    15.030 %         1 x        8.039 Mt        0.000 Mt        8.039 Mt   100.000 % 	 ..../GpuSidePrep

	WindowThread:

		   30396.614 Mt   100.000 %         1 x    30396.614 Mt    30396.614 Mt    30396.614 Mt   100.000 % 	 /

	GpuThread:

		   15518.459 Mt   100.000 %     58562 x        0.265 Mt        0.151 Mt      128.937 Mt     0.831 % 	 /

		       0.200 Mt     0.001 %         1 x        0.200 Mt        0.000 Mt        0.006 Mt     3.109 % 	 ./ScenePrep

		       0.193 Mt    96.891 %         1 x        0.193 Mt        0.000 Mt        0.001 Mt     0.380 % 	 ../AccelerationStructureBuild

		       0.132 Mt    68.293 %         1 x        0.132 Mt        0.000 Mt        0.132 Mt   100.000 % 	 .../BuildBottomLevelASGpu

		       0.061 Mt    31.326 %         1 x        0.061 Mt        0.000 Mt        0.061 Mt   100.000 % 	 .../BuildTopLevelASGpu

		   15286.689 Mt    98.506 %     58562 x        0.261 Mt        0.149 Mt      160.213 Mt     1.048 % 	 ./RasterizationGpu

		    4290.231 Mt    28.065 %     58562 x        0.073 Mt        0.019 Mt     4290.231 Mt   100.000 % 	 ../DeferredRLGpu

		     230.599 Mt     1.508 %     58562 x        0.004 Mt        0.004 Mt      230.599 Mt   100.000 % 	 ../ShadowMapRLGpu

		    7496.487 Mt    49.039 %     58562 x        0.128 Mt        0.104 Mt     7496.487 Mt   100.000 % 	 ../AmbientOcclusionRLGpu

		    3109.159 Mt    20.339 %     58562 x        0.053 Mt        0.021 Mt     3109.159 Mt   100.000 % 	 ../RasterResolveRLGpu

		     102.633 Mt     0.661 %     58562 x        0.002 Mt        0.002 Mt      102.633 Mt   100.000 % 	 ./Present


	============================


