Configuration: 
Command line parameters  = E:\programming\FIT-DIP\Masters-DXR-Main-git\build\Measurement\x64\Release\Measurement.exe --scene Sponza/Sponza.gltf --profile-automation --profile-camera-track Sponza/SponzaPreset.trk --rt-mode --width 2560 --height 1440 --profile-output PresetSponza1440RtHigh --quality-preset High 
Build type               = Release
Device name              = NVIDIA GeForce RTX 2080 Ti
Device memory MB         = 11049
Ray Tracing backend      = Hardware
Window width             = 2560
Window height            = 1421
Window title             = Direct3D App
Back-buffer count        = 3
Fill Triangles           = enabled
Triangles Font Clock-Wise= enabled
Fast Build AS            = disabled
Target FPS               = 0
Target UPS               = 60
Tearing                  = enabled
VSync                    = disabled
GUI rendering            = disabled
Debug layer              = disabled
GPU validation           = disabled
GPU profiling            = enabled
Render mode              = Ray Tracing
Loaded scene file        = Sponza/Sponza.gltf
Using testing scene      = disabled
Duplication              = 1
Duplication in cube      = disabled
Duplication offset       = 10
BLAS duplication         = disabled
Rays per pixel           = 13
Quality preset           = High
Rasterization Quality    : 
  AO                     = enabled
  AO kernel size         = 4
  Shadows                = enabled
  Shadow map resolution  = 4096
  Shadow PCF kernel size = 4
  Reflections            = enabled
Ray Tracing Quality      : 
  AO                     = enabled
  AO rays                = 4
  AO kernel size         = 4
  Shadows                = enabled
  Shadow rays            = 4
  Shadow kernel size     = 4
  Reflections            = enabled

ProfilingAggregator: 
Render	,	11.8888	,	10.8194	,	10.2435	,	9.9137	,	9.649	,	10.736	,	10.5998	,	12.0164	,	11.9265	,	11.6051	,	10.966	,	11.3626	,	12.1053	,	11.4732	,	10.815	,	9.243	,	12.3887	,	11.7258	,	8.9248	,	10.4852	,	11.0439	,	11.7095	,	10.8651
Update	,	0.0771	,	0.0758	,	0.0776	,	0.0773	,	0.0763	,	0.0745	,	0.0761	,	0.0756	,	0.0736	,	0.0741	,	0.0761	,	0.0786	,	0.075	,	0.0723	,	0.0674	,	0.0776	,	0.0757	,	0.0759	,	0.066	,	0.0801	,	0.0758	,	0.0754	,	0.0774
RayTracing	,	0.1741	,	0.1761	,	0.1747	,	0.1744	,	0.1883	,	0.1711	,	0.1745	,	0.1741	,	0.1739	,	0.1715	,	0.1726	,	0.1764	,	0.1735	,	0.1876	,	0.1718	,	0.205	,	0.1783	,	0.1866	,	0.1561	,	0.1754	,	0.2027	,	0.1746	,	0.1896
RayTracingGpu	,	10.8977	,	9.97261	,	9.39734	,	9.0769	,	8.63821	,	9.79843	,	9.72973	,	10.7667	,	10.6844	,	10.7131	,	10.0674	,	10.4588	,	11.1753	,	10.6328	,	9.78598	,	8.29318	,	11.5092	,	10.6659	,	8.17622	,	9.5272	,	10.1348	,	10.7571	,	10.137
DeferredRLGpu	,	2.81283	,	2.48672	,	2.14166	,	1.48106	,	1.35526	,	1.57997	,	2.00314	,	2.37606	,	2.07587	,	1.52701	,	1.34131	,	1.74131	,	2.17373	,	2.43702	,	2.18771	,	1.50285	,	2.08086	,	2.07802	,	1.48138	,	2.80698	,	3.83968	,	3.84621	,	3.84384
RayTracingRLGpu	,	5.53085	,	5.06413	,	4.82403	,	4.95312	,	4.67165	,	5.5208	,	5.21776	,	5.86931	,	6.03795	,	6.4833	,	6.22109	,	6.22525	,	6.32656	,	5.74656	,	5.02307	,	4.26387	,	6.90813	,	6.04032	,	4.18518	,	3.94048	,	3.86848	,	4.22861	,	3.86666
ResolveRLGpu	,	2.55142	,	2.41962	,	2.42826	,	2.64035	,	2.60966	,	2.69536	,	2.50621	,	2.51971	,	2.5689	,	2.70019	,	2.50298	,	2.48979	,	2.67258	,	2.444	,	2.5727	,	2.52406	,	2.51821	,	2.54554	,	2.50752	,	2.77747	,	2.42454	,	2.68	,	2.42403
Index	,	0	,	1	,	2	,	3	,	4	,	5	,	6	,	7	,	8	,	9	,	10	,	11	,	12	,	13	,	14	,	15	,	16	,	17	,	18	,	19	,	20	,	21	,	22

ProfilingBuildAggregator: 
BuildBottomLevelAS	,	1.7152
BuildBottomLevelASGpu	,	2.40787
BuildTopLevelAS	,	1.073
BuildTopLevelASGpu	,	0.089408
Duplication	,	1
Triangles	,	262267
Meshes	,	103
BottomLevels	,	1
TopLevelSize	,	64
BottomLevelsSize	,	18390656
Index	,	0

FpsAggregator: 
FPS	,	59	,	84	,	91	,	97	,	105	,	100	,	99	,	84	,	82	,	85	,	87	,	86	,	85	,	83	,	87	,	105	,	88	,	80	,	95	,	109	,	89	,	87	,	87
UPS	,	59	,	61	,	60	,	60	,	60	,	61	,	60	,	60	,	60	,	61	,	60	,	60	,	61	,	60	,	61	,	60	,	60	,	60	,	61	,	60	,	60	,	61	,	61
FrameTime	,	17.0965	,	11.9946	,	10.9935	,	10.3858	,	9.54073	,	10.0173	,	10.159	,	12.0032	,	12.2102	,	11.8158	,	11.5578	,	11.6595	,	11.8866	,	12.1643	,	11.5892	,	9.58809	,	11.3742	,	12.505	,	10.5462	,	9.24182	,	11.3486	,	11.6031	,	11.5871
GigaRays/s	,	2.76612	,	3.94268	,	4.3017	,	4.55343	,	4.95674	,	4.72092	,	4.65507	,	3.93986	,	3.87307	,	4.00235	,	4.09167	,	4.05598	,	3.97849	,	3.88767	,	4.08059	,	4.93225	,	4.15772	,	3.78174	,	4.48414	,	5.11705	,	4.1671	,	4.07573	,	4.08133
Index	,	0	,	1	,	2	,	3	,	4	,	5	,	6	,	7	,	8	,	9	,	10	,	11	,	12	,	13	,	14	,	15	,	16	,	17	,	18	,	19	,	20	,	21	,	22


AccelerationStructure: 
Ray tracing acceleration structure building statistics: 
	Bottom Level Acceleration Structure: 
		Maximum [B]: 18390656
		Minimum [B]: 18390656
		Currently Allocated [B]: 18390656
		Currently Created [num]: 1
		Current Average [B]: 18390656
		Maximum Triangles [num]: 262267
		Minimum Triangles [num]: 262267
		Total Triangles [num]: 262267
		Maximum Meshes [num]: 103
		Minimum Meshes [num]: 103
		Total Meshes [num]: 103
	Top Level Acceleration Structure: 
		Maximum [B]: 64
		Minimum [B]: 64
		Currently Allocated [B]: 64
		Total Allocated [B]: 64
		Total Created [num]: 1
		Average Allocated [B]: 64
		Maximum Instances [num]: 1
		Minimum Instances [num]: 1
		Total Instances [num]: 1
	Scratch Buffer: 
		Maximum [B]: 14995072
		Minimum [B]: 14995072
		Currently Allocated [B]: 14995072
		Total Allocated [B]: 14995072
		Total Created [num]: 1
		Average Allocated [B]: 14995072
ProfilingStack: 
[PROF] Profiling statistics: 
	Scopes entered : 101875
	Scopes exited : 101874
	Overhead per scope [ticks] : 105
	Threads entered : 3
	Threads exited : 0
	============================
		         Sum[Mt]   Parent[%]  Samples[num]       Avg[Mt]   Last/Curr[Mt]        Self[Mt]     Self[%] 	 Name

	main:

		   26067.773 Mt   100.000 %         1 x    26067.773 Mt    26067.772 Mt        0.421 Mt     0.002 % 	 /

		   26067.377 Mt    99.998 %         1 x    26067.379 Mt    26067.377 Mt      239.727 Mt     0.920 % 	 ./Main application loop

		       2.233 Mt     0.009 %         1 x        2.233 Mt        0.000 Mt        2.233 Mt   100.000 % 	 ../TexturedRLInitialization

		       1.199 Mt     0.005 %         1 x        1.199 Mt        0.000 Mt        1.199 Mt   100.000 % 	 ../ShadowMapRLInitialization

		       4.911 Mt     0.019 %         1 x        4.911 Mt        0.000 Mt        4.911 Mt   100.000 % 	 ../RasterResolveRLInitialization

		       3.604 Mt     0.014 %         1 x        3.604 Mt        0.000 Mt        3.604 Mt   100.000 % 	 ../ResolveRLInitialization

		   23160.345 Mt    88.848 %     73646 x        0.314 Mt       11.949 Mt      433.049 Mt     1.870 % 	 ../MainLoop

		     194.677 Mt     0.841 %      1389 x        0.140 Mt        0.046 Mt      194.267 Mt    99.789 % 	 .../Update

		       0.410 Mt     0.211 %         1 x        0.410 Mt        0.000 Mt        0.410 Mt   100.000 % 	 ..../GuiModelApply

		   22532.619 Mt    97.290 %      2056 x       10.959 Mt       11.819 Mt      243.825 Mt     1.082 % 	 .../Render

		     376.574 Mt     1.671 %      2056 x        0.183 Mt        0.205 Mt       13.955 Mt     3.706 % 	 ..../RayTracing

		     160.092 Mt    42.513 %      2056 x        0.078 Mt        0.081 Mt      158.374 Mt    98.926 % 	 ...../Deferred

		       1.719 Mt     1.074 %      2056 x        0.001 Mt        0.001 Mt        1.719 Mt   100.000 % 	 ....../DeferredRLPrep

		     134.985 Mt    35.846 %      2056 x        0.066 Mt        0.079 Mt      133.837 Mt    99.149 % 	 ...../RayCasting

		       1.148 Mt     0.851 %      2056 x        0.001 Mt        0.000 Mt        1.148 Mt   100.000 % 	 ....../RayTracingRLPrep

		      67.542 Mt    17.936 %      2056 x        0.033 Mt        0.037 Mt       67.542 Mt   100.000 % 	 ...../Resolve

		   21912.220 Mt    97.247 %      2056 x       10.658 Mt       11.476 Mt    21912.220 Mt   100.000 % 	 ..../Present

		       2.571 Mt     0.010 %         1 x        2.571 Mt        0.000 Mt        2.571 Mt   100.000 % 	 ../AmbientOcclusionRLInitialization

		       5.425 Mt     0.021 %         1 x        5.425 Mt        0.000 Mt        5.425 Mt   100.000 % 	 ../DeferredRLInitialization

		      53.139 Mt     0.204 %         1 x       53.139 Mt        0.000 Mt        2.665 Mt     5.016 % 	 ../RayTracingRLInitialization

		      50.473 Mt    94.984 %         1 x       50.473 Mt        0.000 Mt       50.473 Mt   100.000 % 	 .../PipelineBuild

		    2594.223 Mt     9.952 %         1 x     2594.223 Mt        0.000 Mt        0.211 Mt     0.008 % 	 ../Initialization

		    2594.011 Mt    99.992 %         1 x     2594.011 Mt        0.000 Mt        2.291 Mt     0.088 % 	 .../ScenePrep

		    2524.288 Mt    97.312 %         1 x     2524.288 Mt        0.000 Mt      234.763 Mt     9.300 % 	 ..../SceneLoad

		    2001.397 Mt    79.286 %         1 x     2001.397 Mt        0.000 Mt      596.449 Mt    29.802 % 	 ...../glTF-LoadScene

		    1404.948 Mt    70.198 %        69 x       20.362 Mt        0.000 Mt     1404.948 Mt   100.000 % 	 ....../glTF-LoadImageData

		     222.463 Mt     8.813 %         1 x      222.463 Mt        0.000 Mt      222.463 Mt   100.000 % 	 ...../glTF-CreateScene

		      65.665 Mt     2.601 %         1 x       65.665 Mt        0.000 Mt       65.665 Mt   100.000 % 	 ...../SceneDesc-CreateScene

		       1.146 Mt     0.044 %         1 x        1.146 Mt        0.000 Mt        0.001 Mt     0.052 % 	 ..../ShadowMapRLPrep

		       1.146 Mt    99.948 %         1 x        1.146 Mt        0.000 Mt        1.103 Mt    96.238 % 	 ...../PrepareForRendering

		       0.043 Mt     3.762 %         1 x        0.043 Mt        0.000 Mt        0.043 Mt   100.000 % 	 ....../PrepareDrawBundle

		       3.672 Mt     0.142 %         1 x        3.672 Mt        0.000 Mt        0.001 Mt     0.019 % 	 ..../TexturedRLPrep

		       3.671 Mt    99.981 %         1 x        3.671 Mt        0.000 Mt        3.633 Mt    98.968 % 	 ...../PrepareForRendering

		       0.002 Mt     0.044 %         1 x        0.002 Mt        0.000 Mt        0.002 Mt   100.000 % 	 ....../PrepareMaterials

		       0.036 Mt     0.989 %         1 x        0.036 Mt        0.000 Mt        0.036 Mt   100.000 % 	 ....../PrepareDrawBundle

		       3.497 Mt     0.135 %         1 x        3.497 Mt        0.000 Mt        0.001 Mt     0.031 % 	 ..../DeferredRLPrep

		       3.496 Mt    99.969 %         1 x        3.496 Mt        0.000 Mt        2.974 Mt    85.079 % 	 ...../PrepareForRendering

		       0.002 Mt     0.051 %         1 x        0.002 Mt        0.000 Mt        0.002 Mt   100.000 % 	 ....../PrepareMaterials

		       0.449 Mt    12.839 %         1 x        0.449 Mt        0.000 Mt        0.449 Mt   100.000 % 	 ....../BuildMaterialCache

		       0.071 Mt     2.031 %         1 x        0.071 Mt        0.000 Mt        0.071 Mt   100.000 % 	 ....../PrepareDrawBundle

		      27.287 Mt     1.052 %         1 x       27.287 Mt        0.000 Mt        2.466 Mt     9.039 % 	 ..../RayTracingRLPrep

		       0.065 Mt     0.237 %         1 x        0.065 Mt        0.000 Mt        0.065 Mt   100.000 % 	 ...../PrepareAccelerationStructures

		       0.001 Mt     0.003 %         1 x        0.001 Mt        0.000 Mt        0.001 Mt   100.000 % 	 ...../PrepareCache

		      12.889 Mt    47.236 %         1 x       12.889 Mt        0.000 Mt       12.889 Mt   100.000 % 	 ...../PipelineBuild

		       0.653 Mt     2.395 %         1 x        0.653 Mt        0.000 Mt        0.653 Mt   100.000 % 	 ...../BuildCache

		       9.630 Mt    35.292 %         1 x        9.630 Mt        0.000 Mt        0.000 Mt     0.004 % 	 ...../BuildAccelerationStructures

		       9.630 Mt    99.996 %         1 x        9.630 Mt        0.000 Mt        6.841 Mt    71.046 % 	 ....../AccelerationStructureBuild

		       1.715 Mt    17.812 %         1 x        1.715 Mt        0.000 Mt        1.715 Mt   100.000 % 	 ......./BuildBottomLevelAS

		       1.073 Mt    11.143 %         1 x        1.073 Mt        0.000 Mt        1.073 Mt   100.000 % 	 ......./BuildTopLevelAS

		       1.582 Mt     5.799 %         1 x        1.582 Mt        0.000 Mt        1.582 Mt   100.000 % 	 ...../BuildShaderTables

		      31.831 Mt     1.227 %         1 x       31.831 Mt        0.000 Mt       31.831 Mt   100.000 % 	 ..../GpuSidePrep

	WindowThread:

		   26066.249 Mt   100.000 %         1 x    26066.249 Mt    26066.249 Mt    26066.249 Mt   100.000 % 	 /

	GpuThread:

		   20800.614 Mt   100.000 %      2056 x       10.117 Mt       11.059 Mt      139.174 Mt     0.669 % 	 /

		       2.505 Mt     0.012 %         1 x        2.505 Mt        0.000 Mt        0.006 Mt     0.254 % 	 ./ScenePrep

		       2.498 Mt    99.746 %         1 x        2.498 Mt        0.000 Mt        0.001 Mt     0.049 % 	 ../AccelerationStructureBuild

		       2.408 Mt    96.373 %         1 x        2.408 Mt        0.000 Mt        2.408 Mt   100.000 % 	 .../BuildBottomLevelASGpu

		       0.089 Mt     3.578 %         1 x        0.089 Mt        0.000 Mt        0.089 Mt   100.000 % 	 .../BuildTopLevelASGpu

		   20572.226 Mt    98.902 %      2056 x       10.006 Mt       10.967 Mt        5.625 Mt     0.027 % 	 ./RayTracingGpu

		    4476.618 Mt    21.760 %      2056 x        2.177 Mt        4.334 Mt     4476.618 Mt   100.000 % 	 ../DeferredRLGpu

		   10784.349 Mt    52.422 %      2056 x        5.245 Mt        4.058 Mt    10784.349 Mt   100.000 % 	 ../RayTracingRLGpu

		    5305.633 Mt    25.790 %      2056 x        2.581 Mt        2.573 Mt     5305.633 Mt   100.000 % 	 ../ResolveRLGpu

		      86.709 Mt     0.417 %      2056 x        0.042 Mt        0.092 Mt       86.709 Mt   100.000 % 	 ./Present


	============================


