Configuration: 
Command line parameters  = E:\programming\FIT-DIP\Masters-DXR-Main-git\build\Measurement\x64\Release\Measurement.exe --scene Sponza/Sponza.gltf --duplication-cube --profile-duplication --profile-max-duplication 5 --rt-mode --width 1024 --height 768 --profile-output DuplicationSponza768Rt 
Build type               = Release
Device name              = NVIDIA GeForce RTX 2080 Ti
Device memory MB         = 11049
Ray Tracing backend      = Hardware
Window width             = 1024
Window height            = 768
Window title             = Direct3D App
Back-buffer count        = 3
Fill Triangles           = enabled
Triangles Font Clock-Wise= enabled
Fast Build AS            = disabled
Target FPS               = 60
Target UPS               = 60
Tearing                  = disabled
VSync                    = disabled
GUI rendering            = disabled
Debug layer              = disabled
GPU validation           = disabled
GPU profiling            = enabled
Render mode              = Ray Tracing
Loaded scene file        = Sponza/Sponza.gltf
Using testing scene      = disabled
Duplication              = 5
Duplication in cube      = enabled
Duplication offset       = 10
BLAS duplication         = enabled
Rays per pixel           = 13
Quality preset           = High
Rasterization Quality    : 
  AO                     = enabled
  AO kernel size         = 4
  Shadows                = enabled
  Shadow map resolution  = 4096
  Shadow PCF kernel size = 4
  Reflections            = enabled
Ray Tracing Quality      : 
  AO                     = enabled
  AO rays                = 4
  AO kernel size         = 4
  Shadows                = enabled
  Shadow rays            = 4
  Shadow kernel size     = 4
  Reflections            = enabled

ProfilingAggregator: 
Render	,	3.9113	,	3.8541	,	6.9744	,	6.916	,	9.5615	,	9.6602	,	13.5981	,	13.8074	,	17.4713	,	17.5519
Update	,	0.0237	,	0.0342	,	0.0197	,	0.0214	,	0.0363	,	0.0388	,	0.0863	,	0.0984	,	0.0958	,	0.0936
RayTracing	,	0.0811	,	0.087	,	0.073	,	0.0593	,	0.1207	,	0.1593	,	0.2308	,	0.2459	,	0.2191	,	0.2172
RayTracingGpu	,	3.36317	,	3.34858	,	6.69584	,	6.54224	,	8.98125	,	8.90294	,	12.7148	,	12.744	,	16.5781	,	16.6622
DeferredRLGpu	,	0.889792	,	0.89024	,	4.27011	,	3.86211	,	7.03104	,	6.95754	,	10.6858	,	10.7165	,	14.5495	,	14.6103
RayTracingRLGpu	,	1.76317	,	1.75325	,	1.76096	,	1.94989	,	1.42461	,	1.42182	,	1.5079	,	1.5049	,	1.50614	,	1.52381
ResolveRLGpu	,	0.70816	,	0.70208	,	0.662624	,	0.726208	,	0.522592	,	0.519744	,	0.518592	,	0.518656	,	0.519904	,	0.523488
Index	,	0	,	1	,	2	,	3	,	4	,	5	,	6	,	7	,	8	,	9

ProfilingBuildAggregator: 
BuildBottomLevelAS	,	1.2806	,	7.6742	,	24.0139	,	64.6694	,	112.781
BuildBottomLevelASGpu	,	2.64298	,	20.0909	,	61.802	,	113.055	,	219.426
BuildTopLevelAS	,	0.5678	,	0.5807	,	0.5843	,	1.4045	,	0.6979
BuildTopLevelASGpu	,	0.088704	,	0.100384	,	0.128416	,	0.108288	,	0.136288
Duplication	,	1	,	2	,	3	,	4	,	5
Triangles	,	262267	,	2098136	,	7081209	,	16785088	,	32783375
Meshes	,	103	,	824	,	2781	,	6592	,	12875
BottomLevels	,	1	,	8	,	27	,	64	,	125
TopLevelSize	,	64	,	512	,	1728	,	4096	,	8000
BottomLevelsSize	,	18390656	,	147125248	,	496547712	,	1177001984	,	2298832000
Index	,	0	,	1	,	2	,	3	,	4

FpsAggregator: 
FPS	,	54	,	59	,	54	,	60	,	42	,	59	,	27	,	59	,	31	,	57
UPS	,	59	,	60	,	60	,	61	,	60	,	60	,	60	,	60	,	60	,	61
FrameTime	,	18.5185	,	16.9492	,	18.6256	,	16.7788	,	23.8095	,	16.9492	,	37.4753	,	16.9504	,	32.2802	,	17.8491
GigaRays/s	,	0.552075	,	0.603193	,	0.548901	,	0.609316	,	0.429392	,	0.603193	,	0.272809	,	0.603148	,	0.316715	,	0.57278
Index	,	0	,	1	,	2	,	3	,	4	,	5	,	6	,	7	,	8	,	9


AccelerationStructure: 
Ray tracing acceleration structure building statistics: 
	Bottom Level Acceleration Structure: 
		Maximum [B]: 18390656
		Minimum [B]: 18390656
		Currently Allocated [B]: 2298832000
		Currently Created [num]: 125
		Current Average [B]: 18390656
		Maximum Triangles [num]: 262267
		Minimum Triangles [num]: 262267
		Total Triangles [num]: 32783375
		Maximum Meshes [num]: 103
		Minimum Meshes [num]: 103
		Total Meshes [num]: 12875
	Top Level Acceleration Structure: 
		Maximum [B]: 8000
		Minimum [B]: 8000
		Currently Allocated [B]: 8000
		Total Allocated [B]: 8000
		Total Created [num]: 1
		Average Allocated [B]: 8000
		Maximum Instances [num]: 125
		Minimum Instances [num]: 125
		Total Instances [num]: 125
	Scratch Buffer: 
		Maximum [B]: 14995072
		Minimum [B]: 14995072
		Currently Allocated [B]: 14995072
		Total Allocated [B]: 14995072
		Total Created [num]: 1
		Average Allocated [B]: 14995072
ProfilingStack: 
[PROF] Profiling statistics: 
	Scopes entered : 15874737
	Scopes exited : 15874736
	Overhead per scope [ticks] : 100.8
	Threads entered : 3
	Threads exited : 0
	============================
		         Sum[Mt]   Parent[%]  Samples[num]       Avg[Mt]   Last/Curr[Mt]        Self[Mt]     Self[%] 	 Name

	main:

		   13747.123 Mt   100.000 %         1 x    13747.123 Mt    13747.122 Mt        0.406 Mt     0.003 % 	 /

		   13746.748 Mt    99.997 %         1 x    13746.750 Mt    13746.748 Mt      993.288 Mt     7.226 % 	 ./Main application loop

		       2.404 Mt     0.017 %         1 x        2.404 Mt        0.000 Mt        2.404 Mt   100.000 % 	 ../TexturedRLInitialization

		       1.191 Mt     0.009 %         1 x        1.191 Mt        0.000 Mt        1.191 Mt   100.000 % 	 ../ShadowMapRLInitialization

		       4.259 Mt     0.031 %         1 x        4.259 Mt        0.000 Mt        4.259 Mt   100.000 % 	 ../RasterResolveRLInitialization

		       3.577 Mt     0.026 %         1 x        3.577 Mt        0.000 Mt        3.577 Mt   100.000 % 	 ../ResolveRLInitialization

		    9923.028 Mt    72.185 %  15867372 x        0.001 Mt       33.059 Mt     3478.817 Mt    35.058 % 	 ../MainLoop

		      89.889 Mt     0.906 %       602 x        0.149 Mt        0.101 Mt       89.624 Mt    99.705 % 	 .../Update

		       0.265 Mt     0.295 %         1 x        0.265 Mt        0.000 Mt        0.265 Mt   100.000 % 	 ..../GuiModelApply

		    6354.323 Mt    64.036 %       503 x       12.633 Mt       17.957 Mt       44.267 Mt     0.697 % 	 .../Render

		     890.381 Mt    14.012 %       503 x        1.770 Mt        0.282 Mt        2.471 Mt     0.277 % 	 ..../RayTracing

		     112.646 Mt    12.651 %       503 x        0.224 Mt        0.140 Mt       29.336 Mt    26.042 % 	 ...../Deferred

		      83.310 Mt    73.958 %       503 x        0.166 Mt        0.001 Mt        0.228 Mt     0.274 % 	 ....../DeferredRLPrep

		      83.082 Mt    99.726 %         7 x       11.869 Mt        0.000 Mt       41.120 Mt    49.493 % 	 ......./PrepareForRendering

		       0.033 Mt     0.040 %         7 x        0.005 Mt        0.000 Mt        0.033 Mt   100.000 % 	 ......../PrepareMaterials

		       0.001 Mt     0.001 %         7 x        0.000 Mt        0.000 Mt        0.001 Mt   100.000 % 	 ......../BuildMaterialCache

		      41.928 Mt    50.465 %         7 x        5.990 Mt        0.000 Mt       41.928 Mt   100.000 % 	 ......../PrepareDrawBundle

		     763.985 Mt    85.804 %       503 x        1.519 Mt        0.093 Mt       26.052 Mt     3.410 % 	 ...../RayCasting

		     737.933 Mt    96.590 %       503 x        1.467 Mt        0.001 Mt       11.424 Mt     1.548 % 	 ....../RayTracingRLPrep

		     203.862 Mt    27.626 %         7 x       29.123 Mt        0.000 Mt      203.862 Mt   100.000 % 	 ......./PrepareAccelerationStructures

		       0.018 Mt     0.002 %         7 x        0.003 Mt        0.000 Mt        0.018 Mt   100.000 % 	 ......./PrepareCache

		      96.600 Mt    13.091 %         7 x       13.800 Mt        0.000 Mt       96.600 Mt   100.000 % 	 ......./PipelineBuild

		       0.002 Mt     0.000 %         7 x        0.000 Mt        0.000 Mt        0.002 Mt   100.000 % 	 ......./BuildCache

		     368.090 Mt    49.881 %         7 x       52.584 Mt        0.000 Mt        0.006 Mt     0.002 % 	 ......./BuildAccelerationStructures

		     368.084 Mt    99.998 %         7 x       52.583 Mt        0.000 Mt       34.678 Mt     9.421 % 	 ......../AccelerationStructureBuild

		     326.627 Mt    88.737 %         7 x       46.661 Mt        0.000 Mt      326.627 Mt   100.000 % 	 ........./BuildBottomLevelAS

		       6.779 Mt     1.842 %         7 x        0.968 Mt        0.000 Mt        6.779 Mt   100.000 % 	 ........./BuildTopLevelAS

		      57.937 Mt     7.851 %         7 x        8.277 Mt        0.000 Mt       57.937 Mt   100.000 % 	 ......./BuildShaderTables

		      11.279 Mt     1.267 %       503 x        0.022 Mt        0.040 Mt       11.279 Mt   100.000 % 	 ...../Resolve

		    5419.674 Mt    85.291 %       503 x       10.775 Mt       17.496 Mt     5419.674 Mt   100.000 % 	 ..../Present

		       2.352 Mt     0.017 %         1 x        2.352 Mt        0.000 Mt        2.352 Mt   100.000 % 	 ../AmbientOcclusionRLInitialization

		       3.772 Mt     0.027 %         1 x        3.772 Mt        0.000 Mt        3.772 Mt   100.000 % 	 ../DeferredRLInitialization

		      53.271 Mt     0.388 %         1 x       53.271 Mt        0.000 Mt        2.242 Mt     4.210 % 	 ../RayTracingRLInitialization

		      51.028 Mt    95.790 %         1 x       51.028 Mt        0.000 Mt       51.028 Mt   100.000 % 	 .../PipelineBuild

		    2759.605 Mt    20.075 %         1 x     2759.605 Mt        0.000 Mt        0.345 Mt     0.012 % 	 ../Initialization

		    2759.260 Mt    99.988 %         1 x     2759.260 Mt        0.000 Mt        2.208 Mt     0.080 % 	 .../ScenePrep

		    2705.929 Mt    98.067 %         1 x     2705.929 Mt        0.000 Mt      225.390 Mt     8.330 % 	 ..../SceneLoad

		    2039.784 Mt    75.382 %         1 x     2039.784 Mt        0.000 Mt      582.424 Mt    28.553 % 	 ...../glTF-LoadScene

		    1457.360 Mt    71.447 %        69 x       21.121 Mt        0.000 Mt     1457.360 Mt   100.000 % 	 ....../glTF-LoadImageData

		     386.392 Mt    14.279 %         1 x      386.392 Mt        0.000 Mt      386.392 Mt   100.000 % 	 ...../glTF-CreateScene

		      54.362 Mt     2.009 %         1 x       54.362 Mt        0.000 Mt       54.362 Mt   100.000 % 	 ...../SceneDesc-CreateScene

		       0.884 Mt     0.032 %         1 x        0.884 Mt        0.000 Mt        0.001 Mt     0.113 % 	 ..../ShadowMapRLPrep

		       0.883 Mt    99.887 %         1 x        0.883 Mt        0.000 Mt        0.846 Mt    95.798 % 	 ...../PrepareForRendering

		       0.037 Mt     4.202 %         1 x        0.037 Mt        0.000 Mt        0.037 Mt   100.000 % 	 ....../PrepareDrawBundle

		       3.231 Mt     0.117 %         1 x        3.231 Mt        0.000 Mt        0.000 Mt     0.015 % 	 ..../TexturedRLPrep

		       3.230 Mt    99.985 %         1 x        3.230 Mt        0.000 Mt        3.191 Mt    98.790 % 	 ...../PrepareForRendering

		       0.001 Mt     0.037 %         1 x        0.001 Mt        0.000 Mt        0.001 Mt   100.000 % 	 ....../PrepareMaterials

		       0.038 Mt     1.173 %         1 x        0.038 Mt        0.000 Mt        0.038 Mt   100.000 % 	 ....../PrepareDrawBundle

		       2.262 Mt     0.082 %         1 x        2.262 Mt        0.000 Mt        0.000 Mt     0.022 % 	 ..../DeferredRLPrep

		       2.261 Mt    99.978 %         1 x        2.261 Mt        0.000 Mt        1.907 Mt    84.327 % 	 ...../PrepareForRendering

		       0.001 Mt     0.027 %         1 x        0.001 Mt        0.000 Mt        0.001 Mt   100.000 % 	 ....../PrepareMaterials

		       0.281 Mt    12.436 %         1 x        0.281 Mt        0.000 Mt        0.281 Mt   100.000 % 	 ....../BuildMaterialCache

		       0.073 Mt     3.211 %         1 x        0.073 Mt        0.000 Mt        0.073 Mt   100.000 % 	 ....../PrepareDrawBundle

		      19.086 Mt     0.692 %         1 x       19.086 Mt        0.000 Mt        1.721 Mt     9.019 % 	 ..../RayTracingRLPrep

		       0.093 Mt     0.488 %         1 x        0.093 Mt        0.000 Mt        0.093 Mt   100.000 % 	 ...../PrepareAccelerationStructures

		       0.001 Mt     0.004 %         1 x        0.001 Mt        0.000 Mt        0.001 Mt   100.000 % 	 ...../PrepareCache

		      11.766 Mt    61.650 %         1 x       11.766 Mt        0.000 Mt       11.766 Mt   100.000 % 	 ...../PipelineBuild

		       0.526 Mt     2.754 %         1 x        0.526 Mt        0.000 Mt        0.526 Mt   100.000 % 	 ...../BuildCache

		       3.905 Mt    20.459 %         1 x        3.905 Mt        0.000 Mt        0.001 Mt     0.020 % 	 ...../BuildAccelerationStructures

		       3.904 Mt    99.980 %         1 x        3.904 Mt        0.000 Mt        2.056 Mt    52.654 % 	 ....../AccelerationStructureBuild

		       1.281 Mt    32.802 %         1 x        1.281 Mt        0.000 Mt        1.281 Mt   100.000 % 	 ......./BuildBottomLevelAS

		       0.568 Mt    14.544 %         1 x        0.568 Mt        0.000 Mt        0.568 Mt   100.000 % 	 ......./BuildTopLevelAS

		       1.074 Mt     5.625 %         1 x        1.074 Mt        0.000 Mt        1.074 Mt   100.000 % 	 ...../BuildShaderTables

		      25.661 Mt     0.930 %         1 x       25.661 Mt        0.000 Mt       25.661 Mt   100.000 % 	 ..../GpuSidePrep

	WindowThread:

		   13741.219 Mt   100.000 %         1 x    13741.219 Mt    13741.219 Mt    13741.219 Mt   100.000 % 	 /

	GpuThread:

		    5323.166 Mt   100.000 %       503 x       10.583 Mt       16.857 Mt       99.961 Mt     1.878 % 	 /

		       2.740 Mt     0.051 %         1 x        2.740 Mt        0.000 Mt        0.007 Mt     0.239 % 	 ./ScenePrep

		       2.733 Mt    99.761 %         1 x        2.733 Mt        0.000 Mt        0.001 Mt     0.054 % 	 ../AccelerationStructureBuild

		       2.643 Mt    96.701 %         1 x        2.643 Mt        0.000 Mt        2.643 Mt   100.000 % 	 .../BuildBottomLevelASGpu

		       0.089 Mt     3.245 %         1 x        0.089 Mt        0.000 Mt        0.089 Mt   100.000 % 	 .../BuildTopLevelASGpu

		    5219.300 Mt    98.049 %       503 x       10.376 Mt       16.854 Mt        1.720 Mt     0.033 % 	 ./RayTracingGpu

		    3469.146 Mt    66.468 %       503 x        6.897 Mt       14.777 Mt     3469.146 Mt   100.000 % 	 ../DeferredRLGpu

		     835.201 Mt    16.002 %       503 x        1.660 Mt        1.545 Mt      835.201 Mt   100.000 % 	 ../RayTracingRLGpu

		     301.916 Mt     5.785 %       503 x        0.600 Mt        0.531 Mt      301.916 Mt   100.000 % 	 ../ResolveRLGpu

		     611.318 Mt    11.713 %         7 x       87.331 Mt        0.000 Mt        0.017 Mt     0.003 % 	 ../AccelerationStructureBuild

		     610.489 Mt    99.864 %         7 x       87.213 Mt        0.000 Mt      610.489 Mt   100.000 % 	 .../BuildBottomLevelASGpu

		       0.812 Mt     0.133 %         7 x        0.116 Mt        0.000 Mt        0.812 Mt   100.000 % 	 .../BuildTopLevelASGpu

		       1.165 Mt     0.022 %       503 x        0.002 Mt        0.003 Mt        1.165 Mt   100.000 % 	 ./Present


	============================


