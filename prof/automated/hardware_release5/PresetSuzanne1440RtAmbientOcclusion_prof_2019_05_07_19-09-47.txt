Configuration: 
Command line parameters  = E:\programming\FIT-DIP\Masters-DXR-Main-git\build\Measurement\x64\Release\Measurement.exe --scene Suzanne/Suzanne.gltf --profile-automation --profile-camera-track Suzanne/Suzanne.trk --rt-mode --width 2560 --height 1440 --profile-output PresetSuzanne1440RtAmbientOcclusion --quality-preset AmbientOcclusion 
Build type               = Release
Device name              = NVIDIA GeForce RTX 2080 Ti
Device memory MB         = 11049
Ray Tracing backend      = Hardware
Window width             = 2560
Window height            = 1421
Window title             = Direct3D App
Back-buffer count        = 3
Fill Triangles           = enabled
Triangles Font Clock-Wise= enabled
Fast Build AS            = disabled
Target FPS               = 0
Target UPS               = 60
Tearing                  = enabled
VSync                    = disabled
GUI rendering            = disabled
Debug layer              = disabled
GPU validation           = disabled
GPU profiling            = enabled
Render mode              = Ray Tracing
Loaded scene file        = Suzanne/Suzanne.gltf
Using testing scene      = disabled
Duplication              = 1
Duplication in cube      = disabled
Duplication offset       = 10
BLAS duplication         = disabled
Rays per pixel           = 4
Quality preset           = AmbientOcclusion
Rasterization Quality    : 
  AO                     = enabled
  AO kernel size         = 4
  Shadows                = disabled
  Shadow map resolution  = 1
  Shadow PCF kernel size = 0
  Reflections            = disabled
Ray Tracing Quality      : 
  AO                     = enabled
  AO rays                = 4
  AO kernel size         = 4
  Shadows                = disabled
  Shadow rays            = 0
  Shadow kernel size     = 4
  Reflections            = disabled

ProfilingAggregator: 
Render	,	1.8993	,	2.1533	,	2.2331	,	2.1186	,	1.8849	,	1.7473	,	1.7361	,	1.5559	,	1.8065	,	1.7933	,	1.9465	,	2.3142	,	1.9013	,	1.7355	,	1.6609	,	2.5657	,	2.8483	,	2.8625	,	3.3821	,	2.6554	,	2.2774	,	1.8203	,	1.7739	,	1.8424
Update	,	0.0804	,	0.0847	,	0.0792	,	0.0783	,	0.074	,	0.0776	,	0.0798	,	0.0794	,	0.0785	,	0.0918	,	0.0806	,	0.0774	,	0.0775	,	0.0805	,	0.0692	,	0.0798	,	0.0817	,	0.0852	,	0.0772	,	0.0768	,	0.0786	,	0.078	,	0.0777	,	0.0514
RayTracing	,	0.1526	,	0.1755	,	0.1713	,	0.1724	,	0.1614	,	0.1461	,	0.1726	,	0.173	,	0.2033	,	0.1441	,	0.1715	,	0.1712	,	0.1293	,	0.1382	,	0.1198	,	0.1702	,	0.1734	,	0.1586	,	0.1739	,	0.1846	,	0.1726	,	0.1403	,	0.1837	,	0.1744
RayTracingGpu	,	1.17437	,	1.32317	,	1.42221	,	1.31798	,	1.13469	,	1.02723	,	0.974336	,	0.974496	,	1.01226	,	1.11085	,	1.19165	,	1.42928	,	0.981024	,	0.98224	,	1.07834	,	1.74589	,	1.71683	,	2.22474	,	2.53043	,	1.85117	,	1.55619	,	1.16595	,	1.02726	,	1.00106
DeferredRLGpu	,	0.139808	,	0.183712	,	0.199616	,	0.157792	,	0.09968	,	0.077856	,	0.057536	,	0.046016	,	0.069728	,	0.093376	,	0.125984	,	0.07984	,	0.055392	,	0.060576	,	0.089568	,	0.185888	,	0.229824	,	0.258656	,	0.41648	,	0.31968	,	0.268032	,	0.137472	,	0.0824	,	0.07152
RayTracingRLGpu	,	0.241344	,	0.330816	,	0.401152	,	0.351104	,	0.244384	,	0.17264	,	0.145952	,	0.159712	,	0.164384	,	0.228192	,	0.270496	,	0.209216	,	0.152416	,	0.147168	,	0.204512	,	0.38704	,	0.52752	,	0.732384	,	1.17472	,	0.652416	,	0.454752	,	0.237696	,	0.169024	,	0.15408
ResolveRLGpu	,	0.790944	,	0.806432	,	0.818368	,	0.805664	,	0.788864	,	0.775136	,	0.767936	,	0.766048	,	0.776224	,	0.787616	,	0.793408	,	1.13814	,	0.76896	,	0.771456	,	0.782176	,	1.17091	,	0.95776	,	1.23178	,	0.935904	,	0.877152	,	0.831328	,	0.7888	,	0.774112	,	0.773888
Index	,	0	,	1	,	2	,	3	,	4	,	5	,	6	,	7	,	8	,	9	,	10	,	11	,	12	,	13	,	14	,	15	,	16	,	17	,	18	,	19	,	20	,	21	,	22	,	23

ProfilingBuildAggregator: 
BuildBottomLevelAS	,	0.9256
BuildBottomLevelASGpu	,	0.439072
BuildTopLevelAS	,	0.7397
BuildTopLevelASGpu	,	0.06224
Duplication	,	1
Triangles	,	3936
Meshes	,	1
BottomLevels	,	1
TopLevelSize	,	64
BottomLevelsSize	,	282112
Index	,	0

FpsAggregator: 
FPS	,	416	,	466	,	445	,	445	,	483	,	528	,	560	,	595	,	573	,	536	,	497	,	512	,	542	,	578	,	550	,	484	,	422	,	391	,	322	,	335	,	383	,	456	,	524	,	551
UPS	,	59	,	60	,	61	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	61	,	60	,	60	,	60	,	60	,	60
FrameTime	,	2.40438	,	2.14631	,	2.2518	,	2.25164	,	2.07339	,	1.89676	,	1.78694	,	1.683	,	1.74545	,	1.8682	,	2.0133	,	1.95715	,	1.847	,	1.73211	,	1.81979	,	2.06659	,	2.37266	,	2.55773	,	3.11448	,	2.99157	,	2.61498	,	2.19599	,	1.9115	,	1.81676
GigaRays/s	,	6.0519	,	6.77955	,	6.46195	,	6.46242	,	7.01798	,	7.67153	,	8.14298	,	8.6459	,	8.33655	,	7.7888	,	7.22745	,	7.43481	,	7.8782	,	8.40077	,	7.99599	,	7.04108	,	6.13279	,	5.68904	,	4.67205	,	4.86402	,	5.56449	,	6.62619	,	7.61235	,	8.00935
Index	,	0	,	1	,	2	,	3	,	4	,	5	,	6	,	7	,	8	,	9	,	10	,	11	,	12	,	13	,	14	,	15	,	16	,	17	,	18	,	19	,	20	,	21	,	22	,	23


AccelerationStructure: 
Ray tracing acceleration structure building statistics: 
	Bottom Level Acceleration Structure: 
		Maximum [B]: 282112
		Minimum [B]: 282112
		Currently Allocated [B]: 282112
		Currently Created [num]: 1
		Current Average [B]: 282112
		Maximum Triangles [num]: 3936
		Minimum Triangles [num]: 3936
		Total Triangles [num]: 3936
		Maximum Meshes [num]: 1
		Minimum Meshes [num]: 1
		Total Meshes [num]: 1
	Top Level Acceleration Structure: 
		Maximum [B]: 64
		Minimum [B]: 64
		Currently Allocated [B]: 64
		Total Allocated [B]: 64
		Total Created [num]: 1
		Average Allocated [B]: 64
		Maximum Instances [num]: 1
		Minimum Instances [num]: 1
		Total Instances [num]: 1
	Scratch Buffer: 
		Maximum [B]: 82944
		Minimum [B]: 82944
		Currently Allocated [B]: 82944
		Total Allocated [B]: 82944
		Total Created [num]: 1
		Average Allocated [B]: 82944
ProfilingStack: 
[PROF] Profiling statistics: 
	Scopes entered : 254200
	Scopes exited : 254199
	Overhead per scope [ticks] : 100.2
	Threads entered : 3
	Threads exited : 0
	============================
		         Sum[Mt]   Parent[%]  Samples[num]       Avg[Mt]   Last/Curr[Mt]        Self[Mt]     Self[%] 	 Name

	main:

		   24517.912 Mt   100.000 %         1 x    24517.913 Mt    24517.912 Mt        0.612 Mt     0.002 % 	 /

		   24517.329 Mt    99.998 %         1 x    24517.331 Mt    24517.329 Mt      305.066 Mt     1.244 % 	 ./Main application loop

		       2.315 Mt     0.009 %         1 x        2.315 Mt        0.000 Mt        2.315 Mt   100.000 % 	 ../TexturedRLInitialization

		       1.267 Mt     0.005 %         1 x        1.267 Mt        0.000 Mt        1.267 Mt   100.000 % 	 ../ShadowMapRLInitialization

		       4.836 Mt     0.020 %         1 x        4.836 Mt        0.000 Mt        4.836 Mt   100.000 % 	 ../RasterResolveRLInitialization

		       3.648 Mt     0.015 %         1 x        3.648 Mt        0.000 Mt        3.648 Mt   100.000 % 	 ../ResolveRLInitialization

		   24036.711 Mt    98.040 %    101965 x        0.236 Mt        1.816 Mt      398.935 Mt     1.660 % 	 ../MainLoop

		     195.182 Mt     0.812 %      1442 x        0.135 Mt        0.089 Mt      194.583 Mt    99.693 % 	 .../Update

		       0.599 Mt     0.307 %         1 x        0.599 Mt        0.000 Mt        0.599 Mt   100.000 % 	 ..../GuiModelApply

		   23442.593 Mt    97.528 %     11596 x        2.022 Mt        1.725 Mt     1202.992 Mt     5.132 % 	 .../Render

		    1905.163 Mt     8.127 %     11596 x        0.164 Mt        0.177 Mt       72.976 Mt     3.830 % 	 ..../RayTracing

		     848.254 Mt    44.524 %     11596 x        0.073 Mt        0.081 Mt      835.271 Mt    98.469 % 	 ...../Deferred

		      12.983 Mt     1.531 %     11596 x        0.001 Mt        0.001 Mt       12.983 Mt   100.000 % 	 ....../DeferredRLPrep

		     679.194 Mt    35.650 %     11596 x        0.059 Mt        0.064 Mt      672.372 Mt    98.995 % 	 ...../RayCasting

		       6.823 Mt     1.005 %     11596 x        0.001 Mt        0.001 Mt        6.823 Mt   100.000 % 	 ....../RayTracingRLPrep

		     304.739 Mt    15.995 %     11596 x        0.026 Mt        0.027 Mt      304.739 Mt   100.000 % 	 ...../Resolve

		   20334.438 Mt    86.741 %     11596 x        1.754 Mt        1.436 Mt    20334.438 Mt   100.000 % 	 ..../Present

		       2.752 Mt     0.011 %         1 x        2.752 Mt        0.000 Mt        2.752 Mt   100.000 % 	 ../AmbientOcclusionRLInitialization

		       5.135 Mt     0.021 %         1 x        5.135 Mt        0.000 Mt        5.135 Mt   100.000 % 	 ../DeferredRLInitialization

		      50.692 Mt     0.207 %         1 x       50.692 Mt        0.000 Mt        2.392 Mt     4.719 % 	 ../RayTracingRLInitialization

		      48.300 Mt    95.281 %         1 x       48.300 Mt        0.000 Mt       48.300 Mt   100.000 % 	 .../PipelineBuild

		     104.908 Mt     0.428 %         1 x      104.908 Mt        0.000 Mt        0.193 Mt     0.184 % 	 ../Initialization

		     104.715 Mt    99.816 %         1 x      104.715 Mt        0.000 Mt        1.062 Mt     1.014 % 	 .../ScenePrep

		      63.768 Mt    60.897 %         1 x       63.768 Mt        0.000 Mt       16.263 Mt    25.504 % 	 ..../SceneLoad

		      39.294 Mt    61.620 %         1 x       39.294 Mt        0.000 Mt        8.376 Mt    21.316 % 	 ...../glTF-LoadScene

		      30.918 Mt    78.684 %         2 x       15.459 Mt        0.000 Mt       30.918 Mt   100.000 % 	 ....../glTF-LoadImageData

		       6.446 Mt    10.108 %         1 x        6.446 Mt        0.000 Mt        6.446 Mt   100.000 % 	 ...../glTF-CreateScene

		       1.765 Mt     2.768 %         1 x        1.765 Mt        0.000 Mt        1.765 Mt   100.000 % 	 ...../SceneDesc-CreateScene

		       0.281 Mt     0.269 %         1 x        0.281 Mt        0.000 Mt        0.000 Mt     0.142 % 	 ..../ShadowMapRLPrep

		       0.281 Mt    99.858 %         1 x        0.281 Mt        0.000 Mt        0.273 Mt    97.012 % 	 ...../PrepareForRendering

		       0.008 Mt     2.988 %         1 x        0.008 Mt        0.000 Mt        0.008 Mt   100.000 % 	 ....../PrepareDrawBundle

		       1.437 Mt     1.372 %         1 x        1.437 Mt        0.000 Mt        0.000 Mt     0.021 % 	 ..../TexturedRLPrep

		       1.437 Mt    99.979 %         1 x        1.437 Mt        0.000 Mt        1.432 Mt    99.715 % 	 ...../PrepareForRendering

		       0.001 Mt     0.063 %         1 x        0.001 Mt        0.000 Mt        0.001 Mt   100.000 % 	 ....../PrepareMaterials

		       0.003 Mt     0.223 %         1 x        0.003 Mt        0.000 Mt        0.003 Mt   100.000 % 	 ....../PrepareDrawBundle

		       1.359 Mt     1.298 %         1 x        1.359 Mt        0.000 Mt        0.000 Mt     0.037 % 	 ..../DeferredRLPrep

		       1.358 Mt    99.963 %         1 x        1.358 Mt        0.000 Mt        1.062 Mt    78.162 % 	 ...../PrepareForRendering

		       0.001 Mt     0.037 %         1 x        0.001 Mt        0.000 Mt        0.001 Mt   100.000 % 	 ....../PrepareMaterials

		       0.293 Mt    21.587 %         1 x        0.293 Mt        0.000 Mt        0.293 Mt   100.000 % 	 ....../BuildMaterialCache

		       0.003 Mt     0.214 %         1 x        0.003 Mt        0.000 Mt        0.003 Mt   100.000 % 	 ....../PrepareDrawBundle

		      25.844 Mt    24.680 %         1 x       25.844 Mt        0.000 Mt        2.447 Mt     9.469 % 	 ..../RayTracingRLPrep

		       0.050 Mt     0.194 %         1 x        0.050 Mt        0.000 Mt        0.050 Mt   100.000 % 	 ...../PrepareAccelerationStructures

		       0.001 Mt     0.003 %         1 x        0.001 Mt        0.000 Mt        0.001 Mt   100.000 % 	 ...../PrepareCache

		      17.630 Mt    68.219 %         1 x       17.630 Mt        0.000 Mt       17.630 Mt   100.000 % 	 ...../PipelineBuild

		       0.316 Mt     1.222 %         1 x        0.316 Mt        0.000 Mt        0.316 Mt   100.000 % 	 ...../BuildCache

		       4.102 Mt    15.872 %         1 x        4.102 Mt        0.000 Mt        0.001 Mt     0.017 % 	 ...../BuildAccelerationStructures

		       4.101 Mt    99.983 %         1 x        4.101 Mt        0.000 Mt        2.436 Mt    59.394 % 	 ....../AccelerationStructureBuild

		       0.926 Mt    22.570 %         1 x        0.926 Mt        0.000 Mt        0.926 Mt   100.000 % 	 ......./BuildBottomLevelAS

		       0.740 Mt    18.037 %         1 x        0.740 Mt        0.000 Mt        0.740 Mt   100.000 % 	 ......./BuildTopLevelAS

		       1.298 Mt     5.021 %         1 x        1.298 Mt        0.000 Mt        1.298 Mt   100.000 % 	 ...../BuildShaderTables

		      10.964 Mt    10.470 %         1 x       10.964 Mt        0.000 Mt       10.964 Mt   100.000 % 	 ..../GpuSidePrep

	WindowThread:

		   24514.497 Mt   100.000 %         1 x    24514.498 Mt    24514.497 Mt    24514.497 Mt   100.000 % 	 /

	GpuThread:

		   14635.930 Mt   100.000 %     11596 x        1.262 Mt        0.998 Mt      128.927 Mt     0.881 % 	 /

		       0.508 Mt     0.003 %         1 x        0.508 Mt        0.000 Mt        0.006 Mt     1.153 % 	 ./ScenePrep

		       0.502 Mt    98.847 %         1 x        0.502 Mt        0.000 Mt        0.001 Mt     0.147 % 	 ../AccelerationStructureBuild

		       0.439 Mt    87.456 %         1 x        0.439 Mt        0.000 Mt        0.439 Mt   100.000 % 	 .../BuildBottomLevelASGpu

		       0.062 Mt    12.397 %         1 x        0.062 Mt        0.000 Mt        0.062 Mt   100.000 % 	 .../BuildTopLevelASGpu

		   14402.167 Mt    98.403 %     11596 x        1.242 Mt        0.996 Mt       27.581 Mt     0.192 % 	 ./RayTracingGpu

		    1519.452 Mt    10.550 %     11596 x        0.131 Mt        0.072 Mt     1519.452 Mt   100.000 % 	 ../DeferredRLGpu

		    3437.553 Mt    23.868 %     11596 x        0.296 Mt        0.154 Mt     3437.553 Mt   100.000 % 	 ../RayTracingRLGpu

		    9417.581 Mt    65.390 %     11596 x        0.812 Mt        0.769 Mt     9417.581 Mt   100.000 % 	 ../ResolveRLGpu

		     104.328 Mt     0.713 %     11596 x        0.009 Mt        0.002 Mt      104.328 Mt   100.000 % 	 ./Present


	============================


