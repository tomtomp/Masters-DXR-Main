Configuration: 
Command line parameters  = E:\programming\FIT-DIP\Masters-DXR-Main-git\build\Measurement\x64\Release\Measurement.exe --scene Sponza/Sponza.gltf --quality-preset Low --profile-automation --profile-camera-track Sponza/Sponza.trk --rt-mode --width 1024 --height 768 --profile-output TrackSponza768Rt 
Build type               = Release
Device name              = NVIDIA GeForce RTX 2080 Ti
Device memory MB         = 11049
Ray Tracing backend      = Hardware
Window width             = 1024
Window height            = 768
Window title             = Direct3D App
Back-buffer count        = 3
Fill Triangles           = enabled
Triangles Font Clock-Wise= enabled
Fast Build AS            = disabled
Target FPS               = 0
Target UPS               = 60
Tearing                  = enabled
VSync                    = disabled
GUI rendering            = disabled
Debug layer              = disabled
GPU validation           = disabled
GPU profiling            = enabled
Render mode              = Ray Tracing
Loaded scene file        = Sponza/Sponza.gltf
Using testing scene      = disabled
Duplication              = 1
Duplication in cube      = disabled
Duplication offset       = 10
BLAS duplication         = disabled
Rays per pixel           = 5
Quality preset           = Low
Rasterization Quality    : 
  AO                     = enabled
  AO kernel size         = 4
  Shadows                = enabled
  Shadow map resolution  = 2048
  Shadow PCF kernel size = 0
  Reflections            = disabled
Ray Tracing Quality      : 
  AO                     = enabled
  AO rays                = 4
  AO kernel size         = 4
  Shadows                = enabled
  Shadow rays            = 1
  Shadow kernel size     = 4
  Reflections            = disabled

ProfilingAggregator: 
Render	,	3.1312	,	3.2637	,	3.2678	,	3.0149	,	2.9562	,	2.5841	,	2.7801	,	2.3277	,	2.5051	,	2.7953	,	2.916	,	3.0668	,	3.0046	,	2.9875	,	2.9482	,	2.867	,	2.857	,	2.6027	,	2.8704	,	3.013	,	3.1455	,	3.2829	,	3.1437	,	3.0534	,	2.9682	,	2.8648
Update	,	0.0747	,	0.0844	,	0.0757	,	0.0729	,	0.0758	,	0.0523	,	0.0767	,	0.0758	,	0.0748	,	0.0874	,	0.0747	,	0.0715	,	0.0746	,	0.0754	,	0.0748	,	0.0756	,	0.0746	,	0.075	,	0.0747	,	0.0748	,	0.0751	,	0.0738	,	0.0683	,	0.0761	,	0.0825	,	0.059
RayTracing	,	0.1736	,	0.172	,	0.1699	,	0.1765	,	0.1897	,	0.0744	,	0.1761	,	0.1723	,	0.1712	,	0.1721	,	0.17	,	0.1702	,	0.1746	,	0.1713	,	0.1737	,	0.1725	,	0.1698	,	0.1811	,	0.1729	,	0.172	,	0.1702	,	0.1956	,	0.1743	,	0.1694	,	0.1696	,	0.0986
RayTracingGpu	,	2.2385	,	2.46806	,	2.36208	,	2.16675	,	2.0527	,	2.1737	,	2.06032	,	1.50458	,	1.74019	,	1.9553	,	2.12061	,	2.31754	,	2.26541	,	2.18493	,	2.13808	,	2.1175	,	2.0583	,	1.83635	,	2.02163	,	2.20506	,	2.33248	,	2.46864	,	2.32774	,	2.24685	,	2.23197	,	2.37859
DeferredRLGpu	,	1.09843	,	1.28762	,	1.10723	,	1.07888	,	0.998368	,	0.9328	,	0.811968	,	0.520736	,	0.688416	,	0.894336	,	1.11286	,	1.28509	,	1.24867	,	1.17757	,	1.1535	,	1.1151	,	1.04125	,	0.841888	,	1.02573	,	1.20192	,	1.2591	,	1.40726	,	1.29619	,	1.23187	,	1.22752	,	1.18019
RayTracingRLGpu	,	0.526432	,	0.563616	,	0.547936	,	0.565056	,	0.534528	,	0.717536	,	0.716704	,	0.466144	,	0.520384	,	0.524992	,	0.493216	,	0.51952	,	0.505888	,	0.499104	,	0.474752	,	0.489376	,	0.49568	,	0.478368	,	0.480096	,	0.478848	,	0.560256	,	0.551712	,	0.524768	,	0.506752	,	0.492896	,	0.49872
ResolveRLGpu	,	0.61008	,	0.6152	,	0.7048	,	0.5184	,	0.51696	,	0.521248	,	0.529696	,	0.515616	,	0.527264	,	0.532576	,	0.511712	,	0.510784	,	0.50784	,	0.50512	,	0.507808	,	0.510944	,	0.515616	,	0.513984	,	0.512608	,	0.521376	,	0.510688	,	0.506976	,	0.503136	,	0.505664	,	0.508832	,	0.694848
Index	,	0	,	1	,	2	,	3	,	4	,	5	,	6	,	7	,	8	,	9	,	10	,	11	,	12	,	13	,	14	,	15	,	16	,	17	,	18	,	19	,	20	,	21	,	22	,	23	,	24	,	25

ProfilingBuildAggregator: 
BuildBottomLevelAS	,	1.7463
BuildBottomLevelASGpu	,	2.6183
BuildTopLevelAS	,	0.8547
BuildTopLevelASGpu	,	0.089408
Duplication	,	1
Triangles	,	262267
Meshes	,	103
BottomLevels	,	1
TopLevelSize	,	64
BottomLevelsSize	,	18390656
Index	,	0

FpsAggregator: 
FPS	,	302	,	322	,	333	,	329	,	334	,	348	,	362	,	394	,	401	,	373	,	340	,	318	,	324	,	326	,	328	,	334	,	337	,	363	,	360	,	334	,	311	,	304	,	305	,	325	,	331	,	330
UPS	,	59	,	61	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	61	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	61
FrameTime	,	3.32085	,	3.11117	,	3.00307	,	3.04601	,	2.99604	,	2.87497	,	2.76372	,	2.54322	,	2.4995	,	2.68274	,	2.94945	,	3.14526	,	3.08688	,	3.06916	,	3.05252	,	2.99698	,	2.97549	,	2.75751	,	2.78224	,	2.99995	,	3.21802	,	3.29979	,	3.28559	,	3.07866	,	3.02892	,	3.0376
GigaRays/s	,	1.18408	,	1.26388	,	1.30938	,	1.29092	,	1.31245	,	1.36772	,	1.42278	,	1.54613	,	1.57318	,	1.46573	,	1.33318	,	1.25018	,	1.27383	,	1.28118	,	1.28817	,	1.31204	,	1.32152	,	1.42598	,	1.41331	,	1.31074	,	1.22192	,	1.19164	,	1.19679	,	1.27723	,	1.29821	,	1.29449
Index	,	0	,	1	,	2	,	3	,	4	,	5	,	6	,	7	,	8	,	9	,	10	,	11	,	12	,	13	,	14	,	15	,	16	,	17	,	18	,	19	,	20	,	21	,	22	,	23	,	24	,	25


AccelerationStructure: 
Ray tracing acceleration structure building statistics: 
	Bottom Level Acceleration Structure: 
		Maximum [B]: 18390656
		Minimum [B]: 18390656
		Currently Allocated [B]: 18390656
		Currently Created [num]: 1
		Current Average [B]: 18390656
		Maximum Triangles [num]: 262267
		Minimum Triangles [num]: 262267
		Total Triangles [num]: 262267
		Maximum Meshes [num]: 103
		Minimum Meshes [num]: 103
		Total Meshes [num]: 103
	Top Level Acceleration Structure: 
		Maximum [B]: 64
		Minimum [B]: 64
		Currently Allocated [B]: 64
		Total Allocated [B]: 64
		Total Created [num]: 1
		Average Allocated [B]: 64
		Maximum Instances [num]: 1
		Minimum Instances [num]: 1
		Total Instances [num]: 1
	Scratch Buffer: 
		Maximum [B]: 14995072
		Minimum [B]: 14995072
		Currently Allocated [B]: 14995072
		Total Allocated [B]: 14995072
		Total Created [num]: 1
		Average Allocated [B]: 14995072
ProfilingStack: 
[PROF] Profiling statistics: 
	Scopes entered : 172741
	Scopes exited : 172740
	Overhead per scope [ticks] : 100.9
	Threads entered : 3
	Threads exited : 0
	============================
		         Sum[Mt]   Parent[%]  Samples[num]       Avg[Mt]   Last/Curr[Mt]        Self[Mt]     Self[%] 	 Name

	main:

		   29370.535 Mt   100.000 %         1 x    29370.536 Mt    29370.535 Mt        0.422 Mt     0.001 % 	 /

		   29370.143 Mt    99.999 %         1 x    29370.144 Mt    29370.143 Mt      326.437 Mt     1.111 % 	 ./Main application loop

		       3.360 Mt     0.011 %         1 x        3.360 Mt        0.000 Mt        3.360 Mt   100.000 % 	 ../TexturedRLInitialization

		       1.692 Mt     0.006 %         1 x        1.692 Mt        0.000 Mt        1.692 Mt   100.000 % 	 ../ShadowMapRLInitialization

		       4.069 Mt     0.014 %         1 x        4.069 Mt        0.000 Mt        4.069 Mt   100.000 % 	 ../RasterResolveRLInitialization

		       3.785 Mt     0.013 %         1 x        3.785 Mt        0.000 Mt        3.785 Mt   100.000 % 	 ../ResolveRLInitialization

		   26048.966 Mt    88.692 %     57056 x        0.457 Mt        3.093 Mt      386.772 Mt     1.485 % 	 ../MainLoop

		     192.090 Mt     0.737 %      1563 x        0.123 Mt        0.083 Mt      191.522 Mt    99.704 % 	 .../Update

		       0.568 Mt     0.296 %         1 x        0.568 Mt        0.000 Mt        0.568 Mt   100.000 % 	 ..../GuiModelApply

		   25470.104 Mt    97.778 %      8770 x        2.904 Mt        3.007 Mt      946.205 Mt     3.715 % 	 .../Render

		    1499.947 Mt     5.889 %      8770 x        0.171 Mt        0.192 Mt       58.426 Mt     3.895 % 	 ..../RayTracing

		     618.817 Mt    41.256 %      8770 x        0.071 Mt        0.077 Mt      609.713 Mt    98.529 % 	 ...../Deferred

		       9.103 Mt     1.471 %      8770 x        0.001 Mt        0.001 Mt        9.103 Mt   100.000 % 	 ....../DeferredRLPrep

		     547.630 Mt    36.510 %      8770 x        0.062 Mt        0.075 Mt      542.707 Mt    99.101 % 	 ...../RayCasting

		       4.923 Mt     0.899 %      8770 x        0.001 Mt        0.000 Mt        4.923 Mt   100.000 % 	 ....../RayTracingRLPrep

		     275.075 Mt    18.339 %      8770 x        0.031 Mt        0.034 Mt      275.075 Mt   100.000 % 	 ...../Resolve

		   23023.952 Mt    90.396 %      8770 x        2.625 Mt        2.701 Mt    23023.952 Mt   100.000 % 	 ..../Present

		       2.890 Mt     0.010 %         1 x        2.890 Mt        0.000 Mt        2.890 Mt   100.000 % 	 ../AmbientOcclusionRLInitialization

		       4.938 Mt     0.017 %         1 x        4.938 Mt        0.000 Mt        4.938 Mt   100.000 % 	 ../DeferredRLInitialization

		      61.343 Mt     0.209 %         1 x       61.343 Mt        0.000 Mt        2.335 Mt     3.806 % 	 ../RayTracingRLInitialization

		      59.008 Mt    96.194 %         1 x       59.008 Mt        0.000 Mt       59.008 Mt   100.000 % 	 .../PipelineBuild

		    2912.663 Mt     9.917 %         1 x     2912.663 Mt        0.000 Mt        0.241 Mt     0.008 % 	 ../Initialization

		    2912.423 Mt    99.992 %         1 x     2912.423 Mt        0.000 Mt        2.343 Mt     0.080 % 	 .../ScenePrep

		    2839.109 Mt    97.483 %         1 x     2839.109 Mt        0.000 Mt      263.640 Mt     9.286 % 	 ..../SceneLoad

		    1996.215 Mt    70.311 %         1 x     1996.215 Mt        0.000 Mt      566.724 Mt    28.390 % 	 ...../glTF-LoadScene

		    1429.492 Mt    71.610 %        69 x       20.717 Mt        0.000 Mt     1429.492 Mt   100.000 % 	 ....../glTF-LoadImageData

		     484.232 Mt    17.056 %         1 x      484.232 Mt        0.000 Mt      484.232 Mt   100.000 % 	 ...../glTF-CreateScene

		      95.021 Mt     3.347 %         1 x       95.021 Mt        0.000 Mt       95.021 Mt   100.000 % 	 ...../SceneDesc-CreateScene

		       0.986 Mt     0.034 %         1 x        0.986 Mt        0.000 Mt        0.001 Mt     0.051 % 	 ..../ShadowMapRLPrep

		       0.985 Mt    99.949 %         1 x        0.985 Mt        0.000 Mt        0.944 Mt    95.858 % 	 ...../PrepareForRendering

		       0.041 Mt     4.142 %         1 x        0.041 Mt        0.000 Mt        0.041 Mt   100.000 % 	 ....../PrepareDrawBundle

		       3.266 Mt     0.112 %         1 x        3.266 Mt        0.000 Mt        0.000 Mt     0.012 % 	 ..../TexturedRLPrep

		       3.266 Mt    99.988 %         1 x        3.266 Mt        0.000 Mt        3.229 Mt    98.879 % 	 ...../PrepareForRendering

		       0.001 Mt     0.037 %         1 x        0.001 Mt        0.000 Mt        0.001 Mt   100.000 % 	 ....../PrepareMaterials

		       0.035 Mt     1.084 %         1 x        0.035 Mt        0.000 Mt        0.035 Mt   100.000 % 	 ....../PrepareDrawBundle

		       2.598 Mt     0.089 %         1 x        2.598 Mt        0.000 Mt        0.001 Mt     0.023 % 	 ..../DeferredRLPrep

		       2.597 Mt    99.977 %         1 x        2.597 Mt        0.000 Mt        2.220 Mt    85.477 % 	 ...../PrepareForRendering

		       0.001 Mt     0.023 %         1 x        0.001 Mt        0.000 Mt        0.001 Mt   100.000 % 	 ....../PrepareMaterials

		       0.319 Mt    12.275 %         1 x        0.319 Mt        0.000 Mt        0.319 Mt   100.000 % 	 ....../BuildMaterialCache

		       0.058 Mt     2.225 %         1 x        0.058 Mt        0.000 Mt        0.058 Mt   100.000 % 	 ....../PrepareDrawBundle

		      32.685 Mt     1.122 %         1 x       32.685 Mt        0.000 Mt        2.793 Mt     8.545 % 	 ..../RayTracingRLPrep

		       0.068 Mt     0.207 %         1 x        0.068 Mt        0.000 Mt        0.068 Mt   100.000 % 	 ...../PrepareAccelerationStructures

		       0.001 Mt     0.003 %         1 x        0.001 Mt        0.000 Mt        0.001 Mt   100.000 % 	 ...../PrepareCache

		      17.646 Mt    53.988 %         1 x       17.646 Mt        0.000 Mt       17.646 Mt   100.000 % 	 ...../PipelineBuild

		       0.720 Mt     2.203 %         1 x        0.720 Mt        0.000 Mt        0.720 Mt   100.000 % 	 ...../BuildCache

		       6.173 Mt    18.887 %         1 x        6.173 Mt        0.000 Mt        0.001 Mt     0.011 % 	 ...../BuildAccelerationStructures

		       6.173 Mt    99.989 %         1 x        6.173 Mt        0.000 Mt        3.572 Mt    57.861 % 	 ....../AccelerationStructureBuild

		       1.746 Mt    28.292 %         1 x        1.746 Mt        0.000 Mt        1.746 Mt   100.000 % 	 ......./BuildBottomLevelAS

		       0.855 Mt    13.847 %         1 x        0.855 Mt        0.000 Mt        0.855 Mt   100.000 % 	 ......./BuildTopLevelAS

		       5.284 Mt    16.168 %         1 x        5.284 Mt        0.000 Mt        5.284 Mt   100.000 % 	 ...../BuildShaderTables

		      31.436 Mt     1.079 %         1 x       31.436 Mt        0.000 Mt       31.436 Mt   100.000 % 	 ..../GpuSidePrep

	WindowThread:

		   29363.478 Mt   100.000 %         1 x    29363.478 Mt    29363.478 Mt    29363.478 Mt   100.000 % 	 /

	GpuThread:

		   18909.498 Mt   100.000 %      8770 x        2.156 Mt        2.192 Mt      132.146 Mt     0.699 % 	 /

		       2.715 Mt     0.014 %         1 x        2.715 Mt        0.000 Mt        0.006 Mt     0.225 % 	 ./ScenePrep

		       2.709 Mt    99.775 %         1 x        2.709 Mt        0.000 Mt        0.001 Mt     0.047 % 	 ../AccelerationStructureBuild

		       2.618 Mt    96.652 %         1 x        2.618 Mt        0.000 Mt        2.618 Mt   100.000 % 	 .../BuildBottomLevelASGpu

		       0.089 Mt     3.300 %         1 x        0.089 Mt        0.000 Mt        0.089 Mt   100.000 % 	 .../BuildTopLevelASGpu

		   18729.112 Mt    99.046 %      8770 x        2.136 Mt        2.189 Mt       27.294 Mt     0.146 % 	 ./RayTracingGpu

		    9496.896 Mt    50.707 %      8770 x        1.083 Mt        1.176 Mt     9496.896 Mt   100.000 % 	 ../DeferredRLGpu

		    4592.009 Mt    24.518 %      8770 x        0.524 Mt        0.498 Mt     4592.009 Mt   100.000 % 	 ../RayTracingRLGpu

		    4612.914 Mt    24.630 %      8770 x        0.526 Mt        0.513 Mt     4612.914 Mt   100.000 % 	 ../ResolveRLGpu

		      45.524 Mt     0.241 %      8770 x        0.005 Mt        0.002 Mt       45.524 Mt   100.000 % 	 ./Present


	============================


