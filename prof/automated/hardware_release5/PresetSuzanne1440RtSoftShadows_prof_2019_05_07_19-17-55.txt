Configuration: 
Command line parameters  = E:\programming\FIT-DIP\Masters-DXR-Main-git\build\Measurement\x64\Release\Measurement.exe --scene Suzanne/Suzanne.gltf --profile-automation --profile-camera-track Suzanne/Suzanne.trk --rt-mode --width 2560 --height 1440 --profile-output PresetSuzanne1440RtSoftShadows --quality-preset SoftShadows 
Build type               = Release
Device name              = NVIDIA GeForce RTX 2080 Ti
Device memory MB         = 11049
Ray Tracing backend      = Hardware
Window width             = 2560
Window height            = 1421
Window title             = Direct3D App
Back-buffer count        = 3
Fill Triangles           = enabled
Triangles Font Clock-Wise= enabled
Fast Build AS            = disabled
Target FPS               = 0
Target UPS               = 60
Tearing                  = enabled
VSync                    = disabled
GUI rendering            = disabled
Debug layer              = disabled
GPU validation           = disabled
GPU profiling            = enabled
Render mode              = Ray Tracing
Loaded scene file        = Suzanne/Suzanne.gltf
Using testing scene      = disabled
Duplication              = 1
Duplication in cube      = disabled
Duplication offset       = 10
BLAS duplication         = disabled
Rays per pixel           = 4
Quality preset           = SoftShadows
Rasterization Quality    : 
  AO                     = disabled
  AO kernel size         = 1
  Shadows                = enabled
  Shadow map resolution  = 4096
  Shadow PCF kernel size = 4
  Reflections            = disabled
Ray Tracing Quality      : 
  AO                     = disabled
  AO rays                = 0
  AO kernel size         = 4
  Shadows                = enabled
  Shadow rays            = 4
  Shadow kernel size     = 4
  Reflections            = disabled

ProfilingAggregator: 
Render	,	2.5004	,	2.7335	,	2.471	,	2.7226	,	2.5133	,	2.3013	,	2.3197	,	2.2725	,	2.3594	,	2.4606	,	2.4272	,	2.1505	,	2.366	,	2.1639	,	2.3385	,	2.7025	,	3.5622	,	3.5677	,	4.9762	,	3.7102	,	3.097	,	2.5118	,	2.342	,	2.3447
Update	,	0.0763	,	0.0754	,	0.0375	,	0.0774	,	0.0763	,	0.076	,	0.0758	,	0.0754	,	0.0772	,	0.0524	,	0.0769	,	0.0701	,	0.0762	,	0.0761	,	0.0761	,	0.0755	,	0.0772	,	0.0753	,	0.0791	,	0.0757	,	0.0758	,	0.0756	,	0.075	,	0.073
RayTracing	,	0.1729	,	0.1724	,	0.0723	,	0.1719	,	0.1738	,	0.1696	,	0.1702	,	0.1704	,	0.1697	,	0.0882	,	0.1697	,	0.1478	,	0.1702	,	0.1703	,	0.1709	,	0.1702	,	0.1688	,	0.1714	,	0.1694	,	0.1718	,	0.1702	,	0.1721	,	0.1692	,	0.17
RayTracingGpu	,	1.75043	,	1.93616	,	2.05123	,	1.91997	,	1.69597	,	1.55312	,	1.50326	,	1.46608	,	1.536	,	1.67728	,	1.73859	,	1.61894	,	1.50102	,	1.50477	,	1.64474	,	2.01914	,	2.42458	,	2.68138	,	3.93418	,	2.80493	,	2.27728	,	1.74003	,	1.56522	,	1.53818
DeferredRLGpu	,	0.139424	,	0.185696	,	0.200192	,	0.154656	,	0.100416	,	0.06992	,	0.053184	,	0.04576	,	0.062752	,	0.092512	,	0.105184	,	0.07968	,	0.054624	,	0.054784	,	0.100448	,	0.184224	,	0.227712	,	0.262912	,	0.414848	,	0.32256	,	0.272384	,	0.13744	,	0.083392	,	0.071136
RayTracingRLGpu	,	0.281888	,	0.392416	,	0.468416	,	0.403072	,	0.270464	,	0.184576	,	0.154816	,	0.140224	,	0.174784	,	0.256352	,	0.304768	,	0.227904	,	0.162304	,	0.158272	,	0.221504	,	0.4448	,	0.644864	,	0.925472	,	1.6929	,	0.973856	,	0.591968	,	0.278656	,	0.184416	,	0.167168
ResolveRLGpu	,	1.32608	,	1.35478	,	1.38096	,	1.35398	,	1.3233	,	1.296	,	1.29347	,	1.27754	,	1.29683	,	1.32531	,	1.32563	,	1.30992	,	1.28234	,	1.28995	,	1.32112	,	1.38826	,	1.55037	,	1.49085	,	1.82435	,	1.50653	,	1.41101	,	1.3209	,	1.29578	,	1.29798
Index	,	0	,	1	,	2	,	3	,	4	,	5	,	6	,	7	,	8	,	9	,	10	,	11	,	12	,	13	,	14	,	15	,	16	,	17	,	18	,	19	,	20	,	21	,	22	,	23

ProfilingBuildAggregator: 
BuildBottomLevelAS	,	0.8512
BuildBottomLevelASGpu	,	0.441696
BuildTopLevelAS	,	0.8206
BuildTopLevelASGpu	,	0.061664
Duplication	,	1
Triangles	,	3936
Meshes	,	1
BottomLevels	,	1
TopLevelSize	,	64
BottomLevelsSize	,	282112
Index	,	0

FpsAggregator: 
FPS	,	314	,	362	,	347	,	351	,	371	,	404	,	417	,	422	,	419	,	405	,	383	,	392	,	420	,	421	,	414	,	372	,	325	,	296	,	248	,	247	,	286	,	351	,	403	,	418
UPS	,	59	,	60	,	61	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	61	,	60	,	60	,	60	,	60	,	60	,	60	,	60
FrameTime	,	3.18515	,	2.76857	,	2.88879	,	2.8545	,	2.69822	,	2.47767	,	2.40047	,	2.37005	,	2.38937	,	2.47432	,	2.61586	,	2.5521	,	2.38312	,	2.3756	,	2.42051	,	2.68864	,	3.08537	,	3.38756	,	4.03693	,	4.05973	,	3.50666	,	2.84985	,	2.48258	,	2.39293
GigaRays/s	,	4.5684	,	5.2558	,	5.03707	,	5.09758	,	5.39283	,	5.87287	,	6.06174	,	6.13955	,	6.0899	,	5.88082	,	5.56262	,	5.7016	,	6.10589	,	6.12521	,	6.01156	,	5.41205	,	4.71615	,	4.29544	,	3.60449	,	3.58424	,	4.14954	,	5.10589	,	5.86126	,	6.08085
Index	,	0	,	1	,	2	,	3	,	4	,	5	,	6	,	7	,	8	,	9	,	10	,	11	,	12	,	13	,	14	,	15	,	16	,	17	,	18	,	19	,	20	,	21	,	22	,	23


AccelerationStructure: 
Ray tracing acceleration structure building statistics: 
	Bottom Level Acceleration Structure: 
		Maximum [B]: 282112
		Minimum [B]: 282112
		Currently Allocated [B]: 282112
		Currently Created [num]: 1
		Current Average [B]: 282112
		Maximum Triangles [num]: 3936
		Minimum Triangles [num]: 3936
		Total Triangles [num]: 3936
		Maximum Meshes [num]: 1
		Minimum Meshes [num]: 1
		Total Meshes [num]: 1
	Top Level Acceleration Structure: 
		Maximum [B]: 64
		Minimum [B]: 64
		Currently Allocated [B]: 64
		Total Allocated [B]: 64
		Total Created [num]: 1
		Average Allocated [B]: 64
		Maximum Instances [num]: 1
		Minimum Instances [num]: 1
		Total Instances [num]: 1
	Scratch Buffer: 
		Maximum [B]: 82944
		Minimum [B]: 82944
		Currently Allocated [B]: 82944
		Total Allocated [B]: 82944
		Total Created [num]: 1
		Average Allocated [B]: 82944
ProfilingStack: 
[PROF] Profiling statistics: 
	Scopes entered : 215010
	Scopes exited : 215009
	Overhead per scope [ticks] : 104.3
	Threads entered : 3
	Threads exited : 0
	============================
		         Sum[Mt]   Parent[%]  Samples[num]       Avg[Mt]   Last/Curr[Mt]        Self[Mt]     Self[%] 	 Name

	main:

		   24483.390 Mt   100.000 %         1 x    24483.391 Mt    24483.389 Mt        0.278 Mt     0.001 % 	 /

		   24483.144 Mt    99.999 %         1 x    24483.145 Mt    24483.144 Mt      254.611 Mt     1.040 % 	 ./Main application loop

		       3.730 Mt     0.015 %         1 x        3.730 Mt        0.000 Mt        3.730 Mt   100.000 % 	 ../TexturedRLInitialization

		       1.839 Mt     0.008 %         1 x        1.839 Mt        0.000 Mt        1.839 Mt   100.000 % 	 ../ShadowMapRLInitialization

		       6.226 Mt     0.025 %         1 x        6.226 Mt        0.000 Mt        6.226 Mt   100.000 % 	 ../RasterResolveRLInitialization

		       3.788 Mt     0.015 %         1 x        3.788 Mt        0.000 Mt        3.788 Mt   100.000 % 	 ../ResolveRLInitialization

		   24042.342 Mt    98.200 %     99253 x        0.242 Mt        2.468 Mt      408.492 Mt     1.699 % 	 ../MainLoop

		     195.572 Mt     0.813 %      1442 x        0.136 Mt        0.082 Mt      194.969 Mt    99.692 % 	 .../Update

		       0.603 Mt     0.308 %         1 x        0.603 Mt        0.000 Mt        0.603 Mt   100.000 % 	 ..../GuiModelApply

		   23438.278 Mt    97.487 %      8790 x        2.666 Mt        2.384 Mt      943.173 Mt     4.024 % 	 .../Render

		    1514.023 Mt     6.460 %      8790 x        0.172 Mt        0.187 Mt       59.109 Mt     3.904 % 	 ..../RayTracing

		     671.217 Mt    44.333 %      8790 x        0.076 Mt        0.084 Mt      661.657 Mt    98.576 % 	 ...../Deferred

		       9.560 Mt     1.424 %      8790 x        0.001 Mt        0.001 Mt        9.560 Mt   100.000 % 	 ....../DeferredRLPrep

		     539.133 Mt    35.609 %      8790 x        0.061 Mt        0.068 Mt      534.341 Mt    99.111 % 	 ...../RayCasting

		       4.792 Mt     0.889 %      8790 x        0.001 Mt        0.001 Mt        4.792 Mt   100.000 % 	 ....../RayTracingRLPrep

		     244.564 Mt    16.153 %      8790 x        0.028 Mt        0.029 Mt      244.564 Mt   100.000 % 	 ...../Resolve

		   20981.082 Mt    89.516 %      8790 x        2.387 Mt        2.080 Mt    20981.082 Mt   100.000 % 	 ..../Present

		       3.666 Mt     0.015 %         1 x        3.666 Mt        0.000 Mt        3.666 Mt   100.000 % 	 ../AmbientOcclusionRLInitialization

		       5.423 Mt     0.022 %         1 x        5.423 Mt        0.000 Mt        5.423 Mt   100.000 % 	 ../DeferredRLInitialization

		      51.061 Mt     0.209 %         1 x       51.061 Mt        0.000 Mt        2.424 Mt     4.747 % 	 ../RayTracingRLInitialization

		      48.637 Mt    95.253 %         1 x       48.637 Mt        0.000 Mt       48.637 Mt   100.000 % 	 .../PipelineBuild

		     110.458 Mt     0.451 %         1 x      110.458 Mt        0.000 Mt        0.181 Mt     0.164 % 	 ../Initialization

		     110.276 Mt    99.836 %         1 x      110.276 Mt        0.000 Mt        1.157 Mt     1.050 % 	 .../ScenePrep

		      63.458 Mt    57.544 %         1 x       63.458 Mt        0.000 Mt       16.120 Mt    25.403 % 	 ..../SceneLoad

		      39.262 Mt    61.871 %         1 x       39.262 Mt        0.000 Mt        8.427 Mt    21.463 % 	 ...../glTF-LoadScene

		      30.835 Mt    78.537 %         2 x       15.418 Mt        0.000 Mt       30.835 Mt   100.000 % 	 ....../glTF-LoadImageData

		       6.284 Mt     9.903 %         1 x        6.284 Mt        0.000 Mt        6.284 Mt   100.000 % 	 ...../glTF-CreateScene

		       1.792 Mt     2.824 %         1 x        1.792 Mt        0.000 Mt        1.792 Mt   100.000 % 	 ...../SceneDesc-CreateScene

		       0.301 Mt     0.273 %         1 x        0.301 Mt        0.000 Mt        0.001 Mt     0.166 % 	 ..../ShadowMapRLPrep

		       0.300 Mt    99.834 %         1 x        0.300 Mt        0.000 Mt        0.292 Mt    97.202 % 	 ...../PrepareForRendering

		       0.008 Mt     2.798 %         1 x        0.008 Mt        0.000 Mt        0.008 Mt   100.000 % 	 ....../PrepareDrawBundle

		       1.406 Mt     1.275 %         1 x        1.406 Mt        0.000 Mt        0.000 Mt     0.021 % 	 ..../TexturedRLPrep

		       1.406 Mt    99.979 %         1 x        1.406 Mt        0.000 Mt        1.401 Mt    99.694 % 	 ...../PrepareForRendering

		       0.001 Mt     0.050 %         1 x        0.001 Mt        0.000 Mt        0.001 Mt   100.000 % 	 ....../PrepareMaterials

		       0.004 Mt     0.256 %         1 x        0.004 Mt        0.000 Mt        0.004 Mt   100.000 % 	 ....../PrepareDrawBundle

		       1.375 Mt     1.247 %         1 x        1.375 Mt        0.000 Mt        0.000 Mt     0.036 % 	 ..../DeferredRLPrep

		       1.375 Mt    99.964 %         1 x        1.375 Mt        0.000 Mt        1.079 Mt    78.478 % 	 ...../PrepareForRendering

		       0.000 Mt     0.029 %         1 x        0.000 Mt        0.000 Mt        0.000 Mt   100.000 % 	 ....../PrepareMaterials

		       0.293 Mt    21.282 %         1 x        0.293 Mt        0.000 Mt        0.293 Mt   100.000 % 	 ....../BuildMaterialCache

		       0.003 Mt     0.211 %         1 x        0.003 Mt        0.000 Mt        0.003 Mt   100.000 % 	 ....../PrepareDrawBundle

		      31.404 Mt    28.477 %         1 x       31.404 Mt        0.000 Mt        2.627 Mt     8.365 % 	 ..../RayTracingRLPrep

		       0.046 Mt     0.146 %         1 x        0.046 Mt        0.000 Mt        0.046 Mt   100.000 % 	 ...../PrepareAccelerationStructures

		       0.001 Mt     0.003 %         1 x        0.001 Mt        0.000 Mt        0.001 Mt   100.000 % 	 ...../PrepareCache

		      22.952 Mt    73.086 %         1 x       22.952 Mt        0.000 Mt       22.952 Mt   100.000 % 	 ...../PipelineBuild

		       0.309 Mt     0.985 %         1 x        0.309 Mt        0.000 Mt        0.309 Mt   100.000 % 	 ...../BuildCache

		       4.060 Mt    12.929 %         1 x        4.060 Mt        0.000 Mt        0.001 Mt     0.012 % 	 ...../BuildAccelerationStructures

		       4.060 Mt    99.988 %         1 x        4.060 Mt        0.000 Mt        2.388 Mt    58.819 % 	 ....../AccelerationStructureBuild

		       0.851 Mt    20.968 %         1 x        0.851 Mt        0.000 Mt        0.851 Mt   100.000 % 	 ......./BuildBottomLevelAS

		       0.821 Mt    20.214 %         1 x        0.821 Mt        0.000 Mt        0.821 Mt   100.000 % 	 ......./BuildTopLevelAS

		       1.409 Mt     4.488 %         1 x        1.409 Mt        0.000 Mt        1.409 Mt   100.000 % 	 ...../BuildShaderTables

		      11.175 Mt    10.134 %         1 x       11.175 Mt        0.000 Mt       11.175 Mt   100.000 % 	 ..../GpuSidePrep

	WindowThread:

		   24482.143 Mt   100.000 %         1 x    24482.143 Mt    24482.143 Mt    24482.143 Mt   100.000 % 	 /

	GpuThread:

		   16542.151 Mt   100.000 %      8790 x        1.882 Mt        1.545 Mt      126.970 Mt     0.768 % 	 /

		       0.511 Mt     0.003 %         1 x        0.511 Mt        0.000 Mt        0.007 Mt     1.339 % 	 ./ScenePrep

		       0.504 Mt    98.661 %         1 x        0.504 Mt        0.000 Mt        0.001 Mt     0.209 % 	 ../AccelerationStructureBuild

		       0.442 Mt    87.566 %         1 x        0.442 Mt        0.000 Mt        0.442 Mt   100.000 % 	 .../BuildBottomLevelASGpu

		       0.062 Mt    12.225 %         1 x        0.062 Mt        0.000 Mt        0.062 Mt   100.000 % 	 .../BuildTopLevelASGpu

		   16302.020 Mt    98.548 %      8790 x        1.855 Mt        1.541 Mt       21.377 Mt     0.131 % 	 ./RayTracingGpu

		    1157.086 Mt     7.098 %      8790 x        0.132 Mt        0.072 Mt     1157.086 Mt   100.000 % 	 ../DeferredRLGpu

		    3107.572 Mt    19.062 %      8790 x        0.354 Mt        0.167 Mt     3107.572 Mt   100.000 % 	 ../RayTracingRLGpu

		   12015.984 Mt    73.709 %      8790 x        1.367 Mt        1.302 Mt    12015.984 Mt   100.000 % 	 ../ResolveRLGpu

		     112.650 Mt     0.681 %      8790 x        0.013 Mt        0.002 Mt      112.650 Mt   100.000 % 	 ./Present


	============================


