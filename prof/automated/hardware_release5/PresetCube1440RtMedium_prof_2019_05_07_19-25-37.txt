Configuration: 
Command line parameters  = E:\programming\FIT-DIP\Masters-DXR-Main-git\build\Measurement\x64\Release\Measurement.exe --scene Cube/Cube.gltf --profile-automation --profile-camera-track Cube/Cube.trk --rt-mode --width 2560 --height 1440 --profile-output PresetCube1440RtMedium --quality-preset Medium 
Build type               = Release
Device name              = NVIDIA GeForce RTX 2080 Ti
Device memory MB         = 11049
Ray Tracing backend      = Hardware
Window width             = 2560
Window height            = 1421
Window title             = Direct3D App
Back-buffer count        = 3
Fill Triangles           = enabled
Triangles Font Clock-Wise= enabled
Fast Build AS            = disabled
Target FPS               = 0
Target UPS               = 60
Tearing                  = enabled
VSync                    = disabled
GUI rendering            = disabled
Debug layer              = disabled
GPU validation           = disabled
GPU profiling            = enabled
Render mode              = Ray Tracing
Loaded scene file        = Cube/Cube.gltf
Using testing scene      = disabled
Duplication              = 1
Duplication in cube      = disabled
Duplication offset       = 10
BLAS duplication         = disabled
Rays per pixel           = 8
Quality preset           = Medium
Rasterization Quality    : 
  AO                     = enabled
  AO kernel size         = 4
  Shadows                = enabled
  Shadow map resolution  = 4096
  Shadow PCF kernel size = 4
  Reflections            = disabled
Ray Tracing Quality      : 
  AO                     = enabled
  AO rays                = 4
  AO kernel size         = 4
  Shadows                = enabled
  Shadow rays            = 4
  Shadow kernel size     = 4
  Reflections            = disabled

ProfilingAggregator: 
Render	,	3.9239	,	4.3486	,	4.3434	,	4.3936	,	4.3068	,	4.7283	,	4.9971	,	5.0164	,	5.3937	,	5.0178	,	4.8478	,	4.9303	,	4.8474	,	4.7007	,	5.0974	,	4.9748	,	5.9411	,	5.7378	,	5.9687	,	5.8505	,	5.2711	,	4.4839	,	4.5891	,	4.337	,	3.9769	,	3.6217	,	3.3539	,	3.2154	,	3.1288
Update	,	0.0771	,	0.0707	,	0.0763	,	0.0771	,	0.0772	,	0.0825	,	0.0753	,	0.0763	,	0.0689	,	0.074	,	0.0769	,	0.0773	,	0.0679	,	0.0752	,	0.076	,	0.0755	,	0.0779	,	0.0751	,	0.0753	,	0.0723	,	0.0742	,	0.0765	,	0.0774	,	0.0751	,	0.0772	,	0.0772	,	0.0754	,	0.0763	,	0.0808
RayTracing	,	0.1773	,	0.1713	,	0.1721	,	0.1762	,	0.173	,	0.1735	,	0.232	,	0.1735	,	0.1758	,	0.169	,	0.173	,	0.1986	,	0.1691	,	0.1703	,	0.1715	,	0.1726	,	0.1752	,	0.1722	,	0.1754	,	0.1689	,	0.1764	,	0.1749	,	0.1727	,	0.1711	,	0.1736	,	0.172	,	0.1714	,	0.1801	,	0.1737
RayTracingGpu	,	3.04835	,	3.43357	,	3.53434	,	3.56918	,	3.65357	,	4.01526	,	4.03888	,	4.11498	,	4.48698	,	4.20067	,	4.01136	,	4.16384	,	4.06435	,	3.89949	,	4.32083	,	4.07181	,	4.96602	,	4.88618	,	5.03875	,	5.03165	,	4.53037	,	3.71043	,	3.72128	,	3.05446	,	2.98365	,	2.6624	,	2.49306	,	2.40682	,	2.34669
DeferredRLGpu	,	0.181824	,	0.24464	,	0.257536	,	0.264736	,	0.27744	,	0.339328	,	0.31312	,	0.359296	,	0.417376	,	0.369984	,	0.323872	,	0.352096	,	0.281792	,	0.255168	,	0.290656	,	0.339264	,	0.398656	,	0.470752	,	0.492192	,	0.49152	,	0.410912	,	0.287872	,	0.236192	,	0.184384	,	0.164224	,	0.119616	,	0.091488	,	0.074656	,	0.064544
RayTracingRLGpu	,	0.693728	,	0.93232	,	0.98512	,	1.00995	,	1.07152	,	1.32266	,	1.42198	,	1.40397	,	1.64269	,	1.44765	,	1.32675	,	1.4497	,	1.14342	,	1.03046	,	1.53802	,	1.39616	,	2.03357	,	1.95741	,	2.0503	,	2.04934	,	1.71773	,	1.16723	,	0.925472	,	0.71328	,	0.587584	,	0.452352	,	0.338336	,	0.276608	,	0.240672
ResolveRLGpu	,	2.16934	,	2.2545	,	2.2896	,	2.29254	,	2.30166	,	2.35101	,	2.30179	,	2.34979	,	2.42464	,	2.38093	,	2.35866	,	2.35738	,	2.6368	,	2.61168	,	2.48995	,	2.3343	,	2.53219	,	2.45594	,	2.49302	,	2.48838	,	2.39984	,	2.2529	,	2.55766	,	2.15376	,	2.23024	,	2.08822	,	2.06109	,	2.05379	,	2.03894
Index	,	0	,	1	,	2	,	3	,	4	,	5	,	6	,	7	,	8	,	9	,	10	,	11	,	12	,	13	,	14	,	15	,	16	,	17	,	18	,	19	,	20	,	21	,	22	,	23	,	24	,	25	,	26	,	27	,	28

ProfilingBuildAggregator: 
BuildBottomLevelAS	,	0.825
BuildBottomLevelASGpu	,	0.127488
BuildTopLevelAS	,	0.8055
BuildTopLevelASGpu	,	0.060992
Duplication	,	1
Triangles	,	12
Meshes	,	1
BottomLevels	,	1
TopLevelSize	,	64
BottomLevelsSize	,	4480
Index	,	0

FpsAggregator: 
FPS	,	207	,	231	,	222	,	223	,	217	,	204	,	205	,	206	,	184	,	180	,	205	,	191	,	203	,	222	,	216	,	204	,	192	,	175	,	165	,	164	,	167	,	201	,	220	,	239	,	257	,	267	,	288	,	298	,	303
UPS	,	59	,	61	,	60	,	60	,	60	,	60	,	61	,	60	,	60	,	60	,	60	,	60	,	61	,	60	,	60	,	60	,	60	,	60	,	60	,	61	,	60	,	60	,	60	,	60	,	60	,	60	,	61	,	60	,	60
FrameTime	,	4.84405	,	4.34287	,	4.52079	,	4.50281	,	4.61552	,	4.91535	,	4.90261	,	4.87401	,	5.45108	,	5.57731	,	4.87916	,	5.25017	,	4.94438	,	4.50692	,	4.64857	,	4.9109	,	5.20913	,	5.73333	,	6.07541	,	6.12073	,	6.01905	,	4.98069	,	4.55325	,	4.18844	,	3.90394	,	3.74881	,	3.47395	,	3.36316	,	3.30793
GigaRays/s	,	6.0078	,	6.70112	,	6.43738	,	6.4631	,	6.30526	,	5.92066	,	5.93603	,	5.97087	,	5.33877	,	5.21794	,	5.96456	,	5.54308	,	5.8859	,	6.45719	,	6.26044	,	5.92602	,	5.58675	,	5.07595	,	4.79014	,	4.75468	,	4.83499	,	5.84298	,	6.3915	,	6.94819	,	7.45455	,	7.76301	,	8.37724	,	8.6532	,	8.79767
Index	,	0	,	1	,	2	,	3	,	4	,	5	,	6	,	7	,	8	,	9	,	10	,	11	,	12	,	13	,	14	,	15	,	16	,	17	,	18	,	19	,	20	,	21	,	22	,	23	,	24	,	25	,	26	,	27	,	28


AccelerationStructure: 
Ray tracing acceleration structure building statistics: 
	Bottom Level Acceleration Structure: 
		Maximum [B]: 4480
		Minimum [B]: 4480
		Currently Allocated [B]: 4480
		Currently Created [num]: 1
		Current Average [B]: 4480
		Maximum Triangles [num]: 12
		Minimum Triangles [num]: 12
		Total Triangles [num]: 12
		Maximum Meshes [num]: 1
		Minimum Meshes [num]: 1
		Total Meshes [num]: 1
	Top Level Acceleration Structure: 
		Maximum [B]: 64
		Minimum [B]: 64
		Currently Allocated [B]: 64
		Total Allocated [B]: 64
		Total Created [num]: 1
		Average Allocated [B]: 64
		Maximum Instances [num]: 1
		Minimum Instances [num]: 1
		Total Instances [num]: 1
	Scratch Buffer: 
		Maximum [B]: 896
		Minimum [B]: 896
		Currently Allocated [B]: 896
		Total Allocated [B]: 896
		Total Created [num]: 1
		Average Allocated [B]: 896
ProfilingStack: 
[PROF] Profiling statistics: 
	Scopes entered : 176067
	Scopes exited : 176066
	Overhead per scope [ticks] : 105.2
	Threads entered : 3
	Threads exited : 0
	============================
		         Sum[Mt]   Parent[%]  Samples[num]       Avg[Mt]   Last/Curr[Mt]        Self[Mt]     Self[%] 	 Name

	main:

		   29498.014 Mt   100.000 %         1 x    29498.014 Mt    29498.013 Mt        0.440 Mt     0.001 % 	 /

		   29497.606 Mt    99.999 %         1 x    29497.607 Mt    29497.606 Mt      250.516 Mt     0.849 % 	 ./Main application loop

		       3.379 Mt     0.011 %         1 x        3.379 Mt        0.000 Mt        3.379 Mt   100.000 % 	 ../TexturedRLInitialization

		       1.667 Mt     0.006 %         1 x        1.667 Mt        0.000 Mt        1.667 Mt   100.000 % 	 ../ShadowMapRLInitialization

		       6.321 Mt     0.021 %         1 x        6.321 Mt        0.000 Mt        6.321 Mt   100.000 % 	 ../RasterResolveRLInitialization

		       3.611 Mt     0.012 %         1 x        3.611 Mt        0.000 Mt        3.611 Mt   100.000 % 	 ../ResolveRLInitialization

		   29091.830 Mt    98.624 %     92923 x        0.313 Mt        3.285 Mt      445.243 Mt     1.530 % 	 ../MainLoop

		     222.154 Mt     0.764 %      1745 x        0.127 Mt        0.091 Mt      221.801 Mt    99.841 % 	 .../Update

		       0.353 Mt     0.159 %         1 x        0.353 Mt        0.000 Mt        0.353 Mt   100.000 % 	 ..../GuiModelApply

		   28424.433 Mt    97.706 %      6258 x        4.542 Mt        3.190 Mt      689.784 Mt     2.427 % 	 .../Render

		    1099.912 Mt     3.870 %      6258 x        0.176 Mt        0.244 Mt       42.188 Mt     3.836 % 	 ..../RayTracing

		     494.976 Mt    45.001 %      6258 x        0.079 Mt        0.118 Mt      488.027 Mt    98.596 % 	 ...../Deferred

		       6.949 Mt     1.404 %      6258 x        0.001 Mt        0.001 Mt        6.949 Mt   100.000 % 	 ....../DeferredRLPrep

		     386.929 Mt    35.178 %      6258 x        0.062 Mt        0.087 Mt      383.268 Mt    99.054 % 	 ...../RayCasting

		       3.661 Mt     0.946 %      6258 x        0.001 Mt        0.001 Mt        3.661 Mt   100.000 % 	 ....../RayTracingRLPrep

		     175.820 Mt    15.985 %      6258 x        0.028 Mt        0.032 Mt      175.820 Mt   100.000 % 	 ...../Resolve

		   26634.737 Mt    93.704 %      6258 x        4.256 Mt        2.821 Mt    26634.737 Mt   100.000 % 	 ..../Present

		       3.495 Mt     0.012 %         1 x        3.495 Mt        0.000 Mt        3.495 Mt   100.000 % 	 ../AmbientOcclusionRLInitialization

		       7.695 Mt     0.026 %         1 x        7.695 Mt        0.000 Mt        7.695 Mt   100.000 % 	 ../DeferredRLInitialization

		      63.376 Mt     0.215 %         1 x       63.376 Mt        0.000 Mt        2.367 Mt     3.735 % 	 ../RayTracingRLInitialization

		      61.009 Mt    96.265 %         1 x       61.009 Mt        0.000 Mt       61.009 Mt   100.000 % 	 .../PipelineBuild

		      65.715 Mt     0.223 %         1 x       65.715 Mt        0.000 Mt        0.185 Mt     0.281 % 	 ../Initialization

		      65.530 Mt    99.719 %         1 x       65.530 Mt        0.000 Mt        4.606 Mt     7.029 % 	 .../ScenePrep

		      22.710 Mt    34.656 %         1 x       22.710 Mt        0.000 Mt        6.431 Mt    28.316 % 	 ..../SceneLoad

		      10.796 Mt    47.540 %         1 x       10.796 Mt        0.000 Mt        2.670 Mt    24.734 % 	 ...../glTF-LoadScene

		       8.126 Mt    75.266 %         2 x        4.063 Mt        0.000 Mt        8.126 Mt   100.000 % 	 ....../glTF-LoadImageData

		       3.747 Mt    16.500 %         1 x        3.747 Mt        0.000 Mt        3.747 Mt   100.000 % 	 ...../glTF-CreateScene

		       1.736 Mt     7.643 %         1 x        1.736 Mt        0.000 Mt        1.736 Mt   100.000 % 	 ...../SceneDesc-CreateScene

		       0.283 Mt     0.432 %         1 x        0.283 Mt        0.000 Mt        0.000 Mt     0.106 % 	 ..../ShadowMapRLPrep

		       0.283 Mt    99.894 %         1 x        0.283 Mt        0.000 Mt        0.276 Mt    97.489 % 	 ...../PrepareForRendering

		       0.007 Mt     2.511 %         1 x        0.007 Mt        0.000 Mt        0.007 Mt   100.000 % 	 ....../PrepareDrawBundle

		       1.415 Mt     2.160 %         1 x        1.415 Mt        0.000 Mt        0.001 Mt     0.057 % 	 ..../TexturedRLPrep

		       1.415 Mt    99.943 %         1 x        1.415 Mt        0.000 Mt        1.410 Mt    99.703 % 	 ...../PrepareForRendering

		       0.001 Mt     0.064 %         1 x        0.001 Mt        0.000 Mt        0.001 Mt   100.000 % 	 ....../PrepareMaterials

		       0.003 Mt     0.233 %         1 x        0.003 Mt        0.000 Mt        0.003 Mt   100.000 % 	 ....../PrepareDrawBundle

		       1.341 Mt     2.046 %         1 x        1.341 Mt        0.000 Mt        0.000 Mt     0.030 % 	 ..../DeferredRLPrep

		       1.341 Mt    99.970 %         1 x        1.341 Mt        0.000 Mt        1.042 Mt    77.734 % 	 ...../PrepareForRendering

		       0.001 Mt     0.037 %         1 x        0.001 Mt        0.000 Mt        0.001 Mt   100.000 % 	 ....../PrepareMaterials

		       0.295 Mt    22.013 %         1 x        0.295 Mt        0.000 Mt        0.295 Mt   100.000 % 	 ....../BuildMaterialCache

		       0.003 Mt     0.216 %         1 x        0.003 Mt        0.000 Mt        0.003 Mt   100.000 % 	 ....../PrepareDrawBundle

		      24.048 Mt    36.698 %         1 x       24.048 Mt        0.000 Mt        2.445 Mt    10.168 % 	 ..../RayTracingRLPrep

		       0.056 Mt     0.234 %         1 x        0.056 Mt        0.000 Mt        0.056 Mt   100.000 % 	 ...../PrepareAccelerationStructures

		       0.001 Mt     0.003 %         1 x        0.001 Mt        0.000 Mt        0.001 Mt   100.000 % 	 ...../PrepareCache

		      12.590 Mt    52.353 %         1 x       12.590 Mt        0.000 Mt       12.590 Mt   100.000 % 	 ...../PipelineBuild

		       0.315 Mt     1.310 %         1 x        0.315 Mt        0.000 Mt        0.315 Mt   100.000 % 	 ...../BuildCache

		       3.941 Mt    16.386 %         1 x        3.941 Mt        0.000 Mt        0.000 Mt     0.010 % 	 ...../BuildAccelerationStructures

		       3.940 Mt    99.990 %         1 x        3.940 Mt        0.000 Mt        2.310 Mt    58.618 % 	 ....../AccelerationStructureBuild

		       0.825 Mt    20.939 %         1 x        0.825 Mt        0.000 Mt        0.825 Mt   100.000 % 	 ......./BuildBottomLevelAS

		       0.805 Mt    20.444 %         1 x        0.805 Mt        0.000 Mt        0.805 Mt   100.000 % 	 ......./BuildTopLevelAS

		       4.700 Mt    19.546 %         1 x        4.700 Mt        0.000 Mt        4.700 Mt   100.000 % 	 ...../BuildShaderTables

		      11.126 Mt    16.978 %         1 x       11.126 Mt        0.000 Mt       11.126 Mt   100.000 % 	 ..../GpuSidePrep

	WindowThread:

		   29496.345 Mt   100.000 %         1 x    29496.345 Mt    29496.345 Mt    29496.345 Mt   100.000 % 	 /

	GpuThread:

		   23359.809 Mt   100.000 %      6258 x        3.733 Mt        2.353 Mt      127.590 Mt     0.546 % 	 /

		       0.196 Mt     0.001 %         1 x        0.196 Mt        0.000 Mt        0.006 Mt     3.282 % 	 ./ScenePrep

		       0.190 Mt    96.718 %         1 x        0.190 Mt        0.000 Mt        0.001 Mt     0.574 % 	 ../AccelerationStructureBuild

		       0.127 Mt    67.252 %         1 x        0.127 Mt        0.000 Mt        0.127 Mt   100.000 % 	 .../BuildBottomLevelASGpu

		       0.061 Mt    32.174 %         1 x        0.061 Mt        0.000 Mt        0.061 Mt   100.000 % 	 .../BuildTopLevelASGpu

		   23079.653 Mt    98.801 %      6258 x        3.688 Mt        2.350 Mt       14.900 Mt     0.065 % 	 ./RayTracingGpu

		    1691.410 Mt     7.329 %      6258 x        0.270 Mt        0.064 Mt     1691.410 Mt   100.000 % 	 ../DeferredRLGpu

		    6905.139 Mt    29.919 %      6258 x        1.103 Mt        0.239 Mt     6905.139 Mt   100.000 % 	 ../RayTracingRLGpu

		   14468.203 Mt    62.688 %      6258 x        2.312 Mt        2.045 Mt    14468.203 Mt   100.000 % 	 ../ResolveRLGpu

		     152.369 Mt     0.652 %      6258 x        0.024 Mt        0.003 Mt      152.369 Mt   100.000 % 	 ./Present


	============================


