Configuration: 
Command line parameters  = E:\programming\FIT-DIP\Masters-DXR-Main-git\build\Measurement\x64\Release\Measurement.exe --scene Suzanne/Suzanne.gltf --profile-automation --profile-camera-track Suzanne/Suzanne.trk --rt-mode-pure --width 2560 --height 1440 --profile-output PresetSuzanne1440RpAmbientOcclusion --quality-preset AmbientOcclusion 
Build type               = Release
Device name              = NVIDIA GeForce RTX 2080 Ti
Device memory MB         = 11049
Ray Tracing backend      = Hardware
Window width             = 2560
Window height            = 1421
Window title             = Direct3D App
Back-buffer count        = 3
Fill Triangles           = enabled
Triangles Font Clock-Wise= enabled
Fast Build AS            = disabled
Target FPS               = 0
Target UPS               = 60
Tearing                  = enabled
VSync                    = disabled
GUI rendering            = disabled
Debug layer              = disabled
GPU validation           = disabled
GPU profiling            = enabled
Render mode              = Ray Tracing
Loaded scene file        = Suzanne/Suzanne.gltf
Using testing scene      = disabled
Duplication              = 1
Duplication in cube      = disabled
Duplication offset       = 10
BLAS duplication         = disabled
Rays per pixel           = 5
Quality preset           = AmbientOcclusion
Rasterization Quality    : 
  AO                     = enabled
  AO kernel size         = 4
  Shadows                = disabled
  Shadow map resolution  = 1
  Shadow PCF kernel size = 0
  Reflections            = disabled
Ray Tracing Quality      : 
  AO                     = enabled
  AO rays                = 4
  AO kernel size         = 4
  Shadows                = disabled
  Shadow rays            = 0
  Shadow kernel size     = 4
  Reflections            = disabled

ProfilingAggregator: 
Render	,	0.7707	,	1.1012	,	1.097	,	0.9596	,	0.7248	,	0.76	,	0.7302	,	0.6316	,	0.6244	,	0.8591	,	0.9728	,	0.6714	,	0.5996	,	0.7125	,	0.8031	,	1.0447	,	1.5645	,	1.868	,	2.2608	,	1.7618	,	1.3718	,	0.9392	,	0.7492	,	0.5987
Update	,	0.0719	,	0.0324	,	0.077	,	0.0396	,	0.0345	,	0.0366	,	0.0735	,	0.033	,	0.0368	,	0.0378	,	0.0772	,	0.0389	,	0.0347	,	0.0322	,	0.0747	,	0.0788	,	0.0408	,	0.0518	,	0.0384	,	0.0758	,	0.0753	,	0.054	,	0.0324	,	0.032
RayTracing	,	0.0343	,	0.0562	,	0.0752	,	0.0292	,	0.0217	,	0.0575	,	0.0583	,	0.0292	,	0.0247	,	0.0578	,	0.0586	,	0.0242	,	0.0239	,	0.0577	,	0.0579	,	0.0255	,	0.0803	,	0.0738	,	0.034	,	0.0805	,	0.0844	,	0.0622	,	0.059	,	0.0214
RayTracingGpu	,	0.542656	,	0.693568	,	0.787744	,	0.698368	,	0.534144	,	0.426496	,	0.383904	,	0.366176	,	0.411232	,	0.51264	,	0.588544	,	0.483968	,	0.391552	,	0.39056	,	0.479104	,	0.753088	,	0.960256	,	1.28954	,	1.94592	,	1.23136	,	0.896992	,	0.539872	,	0.422816	,	0.399008
RayTracingRLGpu	,	0.541984	,	0.692896	,	0.787104	,	0.696864	,	0.532704	,	0.424992	,	0.383104	,	0.365504	,	0.40976	,	0.511968	,	0.587904	,	0.483008	,	0.39088	,	0.389888	,	0.477504	,	0.751552	,	0.959552	,	1.28781	,	1.94426	,	1.23053	,	0.89536	,	0.53824	,	0.421184	,	0.398336
Index	,	0	,	1	,	2	,	3	,	4	,	5	,	6	,	7	,	8	,	9	,	10	,	11	,	12	,	13	,	14	,	15	,	16	,	17	,	18	,	19	,	20	,	21	,	22	,	23

ProfilingBuildAggregator: 
BuildBottomLevelAS	,	1.0027
BuildBottomLevelASGpu	,	0.4376
BuildTopLevelAS	,	1.1883
BuildTopLevelASGpu	,	0.061152
Duplication	,	1
Triangles	,	3936
Meshes	,	1
BottomLevels	,	1
TopLevelSize	,	64
BottomLevelsSize	,	282112
Index	,	0

FpsAggregator: 
FPS	,	1057	,	1001	,	848	,	839	,	995	,	1231	,	1419	,	1517	,	1500	,	1287	,	1079	,	1147	,	1340	,	1499	,	1353	,	1061	,	766	,	619	,	462	,	485	,	625	,	919	,	1251	,	1407
UPS	,	59	,	60	,	60	,	61	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60
FrameTime	,	0.946334	,	0.999096	,	1.1801	,	1.193	,	1.00564	,	0.812377	,	0.70475	,	0.65924	,	0.666921	,	0.777338	,	0.92749	,	0.872225	,	0.746697	,	0.66715	,	0.739226	,	0.94342	,	1.30667	,	1.61776	,	2.16908	,	2.06298	,	1.60141	,	1.08886	,	0.799934	,	0.711075
GigaRays/s	,	19.2203	,	18.2053	,	15.4129	,	15.2463	,	18.0867	,	22.3896	,	25.8089	,	27.5905	,	27.2728	,	23.3988	,	19.6108	,	20.8533	,	24.359	,	27.2634	,	24.6052	,	19.2796	,	13.92	,	11.2432	,	8.38551	,	8.81674	,	11.358	,	16.7044	,	22.7379	,	25.5793
Index	,	0	,	1	,	2	,	3	,	4	,	5	,	6	,	7	,	8	,	9	,	10	,	11	,	12	,	13	,	14	,	15	,	16	,	17	,	18	,	19	,	20	,	21	,	22	,	23


AccelerationStructure: 
Ray tracing acceleration structure building statistics: 
	Bottom Level Acceleration Structure: 
		Maximum [B]: 282112
		Minimum [B]: 282112
		Currently Allocated [B]: 282112
		Currently Created [num]: 1
		Current Average [B]: 282112
		Maximum Triangles [num]: 3936
		Minimum Triangles [num]: 3936
		Total Triangles [num]: 3936
		Maximum Meshes [num]: 1
		Minimum Meshes [num]: 1
		Total Meshes [num]: 1
	Top Level Acceleration Structure: 
		Maximum [B]: 64
		Minimum [B]: 64
		Currently Allocated [B]: 64
		Total Allocated [B]: 64
		Total Created [num]: 1
		Average Allocated [B]: 64
		Maximum Instances [num]: 1
		Minimum Instances [num]: 1
		Total Instances [num]: 1
	Scratch Buffer: 
		Maximum [B]: 82944
		Minimum [B]: 82944
		Currently Allocated [B]: 82944
		Total Allocated [B]: 82944
		Total Created [num]: 1
		Average Allocated [B]: 82944
ProfilingStack: 
[PROF] Profiling statistics: 
	Scopes entered : 318879
	Scopes exited : 318878
	Overhead per scope [ticks] : 104.2
	Threads entered : 3
	Threads exited : 0
	============================
		         Sum[Mt]   Parent[%]  Samples[num]       Avg[Mt]   Last/Curr[Mt]        Self[Mt]     Self[%] 	 Name

	main:

		   24474.704 Mt   100.000 %         1 x    24474.704 Mt    24474.704 Mt        0.633 Mt     0.003 % 	 /

		   24474.086 Mt    99.997 %         1 x    24474.086 Mt    24474.085 Mt      276.420 Mt     1.129 % 	 ./Main application loop

		       2.576 Mt     0.011 %         1 x        2.576 Mt        0.000 Mt        2.576 Mt   100.000 % 	 ../TexturedRLInitialization

		       1.469 Mt     0.006 %         1 x        1.469 Mt        0.000 Mt        1.469 Mt   100.000 % 	 ../ShadowMapRLInitialization

		       7.945 Mt     0.032 %         1 x        7.945 Mt        0.000 Mt        7.945 Mt   100.000 % 	 ../RasterResolveRLInitialization

		       3.905 Mt     0.016 %         1 x        3.905 Mt        0.000 Mt        3.905 Mt   100.000 % 	 ../ResolveRLInitialization

		   24015.020 Mt    98.124 %    111721 x        0.215 Mt        0.678 Mt      194.802 Mt     0.811 % 	 ../MainLoop

		     145.487 Mt     0.606 %      1441 x        0.101 Mt        0.033 Mt      145.264 Mt    99.846 % 	 .../Update

		       0.223 Mt     0.154 %         1 x        0.223 Mt        0.000 Mt        0.223 Mt   100.000 % 	 ..../GuiModelApply

		   23674.731 Mt    98.583 %     25709 x        0.921 Mt        0.643 Mt     1125.927 Mt     4.756 % 	 .../Render

		    1108.256 Mt     4.681 %     25709 x        0.043 Mt        0.030 Mt       64.914 Mt     5.857 % 	 ..../RayTracing

		    1043.342 Mt    94.143 %     25709 x        0.041 Mt        0.028 Mt     1032.406 Mt    98.952 % 	 ...../RayCasting

		      10.936 Mt     1.048 %     25709 x        0.000 Mt        0.000 Mt       10.936 Mt   100.000 % 	 ....../RayTracingRLPrep

		   21440.548 Mt    90.563 %     25709 x        0.834 Mt        0.586 Mt    21440.548 Mt   100.000 % 	 ..../Present

		       2.701 Mt     0.011 %         1 x        2.701 Mt        0.000 Mt        2.701 Mt   100.000 % 	 ../AmbientOcclusionRLInitialization

		       4.230 Mt     0.017 %         1 x        4.230 Mt        0.000 Mt        4.230 Mt   100.000 % 	 ../DeferredRLInitialization

		      50.342 Mt     0.206 %         1 x       50.342 Mt        0.000 Mt        3.589 Mt     7.129 % 	 ../RayTracingRLInitialization

		      46.753 Mt    92.871 %         1 x       46.753 Mt        0.000 Mt       46.753 Mt   100.000 % 	 .../PipelineBuild

		     109.477 Mt     0.447 %         1 x      109.477 Mt        0.000 Mt        0.195 Mt     0.178 % 	 ../Initialization

		     109.282 Mt    99.822 %         1 x      109.282 Mt        0.000 Mt        1.281 Mt     1.173 % 	 .../ScenePrep

		      63.066 Mt    57.709 %         1 x       63.066 Mt        0.000 Mt       14.365 Mt    22.778 % 	 ..../SceneLoad

		      40.086 Mt    63.563 %         1 x       40.086 Mt        0.000 Mt        8.895 Mt    22.189 % 	 ...../glTF-LoadScene

		      31.192 Mt    77.811 %         2 x       15.596 Mt        0.000 Mt       31.192 Mt   100.000 % 	 ....../glTF-LoadImageData

		       6.629 Mt    10.511 %         1 x        6.629 Mt        0.000 Mt        6.629 Mt   100.000 % 	 ...../glTF-CreateScene

		       1.985 Mt     3.148 %         1 x        1.985 Mt        0.000 Mt        1.985 Mt   100.000 % 	 ...../SceneDesc-CreateScene

		       0.338 Mt     0.309 %         1 x        0.338 Mt        0.000 Mt        0.001 Mt     0.178 % 	 ..../ShadowMapRLPrep

		       0.337 Mt    99.822 %         1 x        0.337 Mt        0.000 Mt        0.329 Mt    97.507 % 	 ...../PrepareForRendering

		       0.008 Mt     2.493 %         1 x        0.008 Mt        0.000 Mt        0.008 Mt   100.000 % 	 ....../PrepareDrawBundle

		       1.774 Mt     1.623 %         1 x        1.774 Mt        0.000 Mt        0.000 Mt     0.017 % 	 ..../TexturedRLPrep

		       1.773 Mt    99.983 %         1 x        1.773 Mt        0.000 Mt        1.768 Mt    99.724 % 	 ...../PrepareForRendering

		       0.001 Mt     0.068 %         1 x        0.001 Mt        0.000 Mt        0.001 Mt   100.000 % 	 ....../PrepareMaterials

		       0.004 Mt     0.209 %         1 x        0.004 Mt        0.000 Mt        0.004 Mt   100.000 % 	 ....../PrepareDrawBundle

		       1.628 Mt     1.489 %         1 x        1.628 Mt        0.000 Mt        0.000 Mt     0.031 % 	 ..../DeferredRLPrep

		       1.627 Mt    99.969 %         1 x        1.627 Mt        0.000 Mt        1.270 Mt    78.022 % 	 ...../PrepareForRendering

		       0.001 Mt     0.037 %         1 x        0.001 Mt        0.000 Mt        0.001 Mt   100.000 % 	 ....../PrepareMaterials

		       0.354 Mt    21.756 %         1 x        0.354 Mt        0.000 Mt        0.354 Mt   100.000 % 	 ....../BuildMaterialCache

		       0.003 Mt     0.184 %         1 x        0.003 Mt        0.000 Mt        0.003 Mt   100.000 % 	 ....../PrepareDrawBundle

		      28.626 Mt    26.194 %         1 x       28.626 Mt        0.000 Mt        3.245 Mt    11.334 % 	 ..../RayTracingRLPrep

		       0.072 Mt     0.252 %         1 x        0.072 Mt        0.000 Mt        0.072 Mt   100.000 % 	 ...../PrepareAccelerationStructures

		       0.002 Mt     0.006 %         1 x        0.002 Mt        0.000 Mt        0.002 Mt   100.000 % 	 ...../PrepareCache

		      16.658 Mt    58.191 %         1 x       16.658 Mt        0.000 Mt       16.658 Mt   100.000 % 	 ...../PipelineBuild

		       0.382 Mt     1.335 %         1 x        0.382 Mt        0.000 Mt        0.382 Mt   100.000 % 	 ...../BuildCache

		       5.228 Mt    18.261 %         1 x        5.228 Mt        0.000 Mt        0.001 Mt     0.011 % 	 ...../BuildAccelerationStructures

		       5.227 Mt    99.989 %         1 x        5.227 Mt        0.000 Mt        3.036 Mt    58.082 % 	 ....../AccelerationStructureBuild

		       1.003 Mt    19.183 %         1 x        1.003 Mt        0.000 Mt        1.003 Mt   100.000 % 	 ......./BuildBottomLevelAS

		       1.188 Mt    22.734 %         1 x        1.188 Mt        0.000 Mt        1.188 Mt   100.000 % 	 ......./BuildTopLevelAS

		       3.040 Mt    10.621 %         1 x        3.040 Mt        0.000 Mt        3.040 Mt   100.000 % 	 ...../BuildShaderTables

		      12.570 Mt    11.503 %         1 x       12.570 Mt        0.000 Mt       12.570 Mt   100.000 % 	 ..../GpuSidePrep

	WindowThread:

		   24471.142 Mt   100.000 %         1 x    24471.142 Mt    24471.142 Mt    24471.142 Mt   100.000 % 	 /

	GpuThread:

		   14979.999 Mt   100.000 %     25709 x        0.583 Mt        0.398 Mt      138.086 Mt     0.922 % 	 /

		       0.505 Mt     0.003 %         1 x        0.505 Mt        0.000 Mt        0.005 Mt     0.964 % 	 ./ScenePrep

		       0.500 Mt    99.036 %         1 x        0.500 Mt        0.000 Mt        0.001 Mt     0.192 % 	 ../AccelerationStructureBuild

		       0.438 Mt    87.570 %         1 x        0.438 Mt        0.000 Mt        0.438 Mt   100.000 % 	 .../BuildBottomLevelASGpu

		       0.061 Mt    12.237 %         1 x        0.061 Mt        0.000 Mt        0.061 Mt   100.000 % 	 .../BuildTopLevelASGpu

		   14796.557 Mt    98.775 %     25709 x        0.576 Mt        0.397 Mt       31.742 Mt     0.215 % 	 ./RayTracingGpu

		   14764.814 Mt    99.785 %     25709 x        0.574 Mt        0.396 Mt    14764.814 Mt   100.000 % 	 ../RayTracingRLGpu

		      44.852 Mt     0.299 %     25709 x        0.002 Mt        0.002 Mt       44.852 Mt   100.000 % 	 ./Present


	============================


