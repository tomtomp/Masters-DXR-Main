Configuration: 
Command line parameters  = E:\programming\FIT-DIP\Masters-DXR-Main-git\build\Measurement\x64\Release\Measurement.exe --scene Sponza/Sponza.gltf --profile-automation --profile-camera-track Sponza/SponzaPreset.trk --width 2560 --height 1440 --profile-output PresetSponza1440RaAmbientOcclusion16 --quality-preset AmbientOcclusion16 
Build type               = Release
Device name              = NVIDIA GeForce RTX 2080 Ti
Device memory MB         = 11049
Ray Tracing backend      = Hardware
Window width             = 2560
Window height            = 1421
Window title             = Direct3D App
Back-buffer count        = 3
Fill Triangles           = enabled
Triangles Font Clock-Wise= enabled
Fast Build AS            = disabled
Target FPS               = 0
Target UPS               = 60
Tearing                  = enabled
VSync                    = disabled
GUI rendering            = disabled
Debug layer              = disabled
GPU validation           = disabled
GPU profiling            = enabled
Render mode              = Rasterization
Loaded scene file        = Sponza/Sponza.gltf
Using testing scene      = disabled
Duplication              = 1
Duplication in cube      = disabled
Duplication offset       = 10
BLAS duplication         = disabled
Rays per pixel           = 16
Quality preset           = AmbientOcclusion16
Rasterization Quality    : 
  AO                     = enabled
  AO kernel size         = 16
  Shadows                = disabled
  Shadow map resolution  = 1
  Shadow PCF kernel size = 0
  Reflections            = disabled
Ray Tracing Quality      : 
  AO                     = enabled
  AO rays                = 16
  AO kernel size         = 4
  Shadows                = disabled
  Shadow rays            = 0
  Shadow kernel size     = 4
  Reflections            = disabled

ProfilingAggregator: 
Render	,	11.9078	,	11.5708	,	10.7704	,	10.1636	,	10.5292	,	10.2304	,	10.796	,	11.0858	,	11.2579	,	10.9313	,	10.671	,	10.8209	,	11.0502	,	11.0792	,	11.1544	,	10.4856	,	11.3531	,	10.7673	,	10.7895	,	11.7843	,	12.3736	,	12.2673	,	12.7807
Update	,	0.097	,	0.0737	,	0.0763	,	0.0751	,	0.0697	,	0.0735	,	0.0769	,	0.0759	,	0.0777	,	0.0766	,	0.0773	,	0.0771	,	0.0781	,	0.076	,	0.0766	,	0.076	,	0.0755	,	0.079	,	0.0818	,	0.0696	,	0.076	,	0.0774	,	0.0739
DeferredRLGpu	,	2.70787	,	2.50195	,	2.2399	,	1.49251	,	1.5455	,	1.65437	,	2.06614	,	2.39373	,	2.15005	,	1.66182	,	1.49488	,	1.80234	,	2.20531	,	2.43443	,	2.20051	,	1.58816	,	2.25027	,	1.98573	,	1.57171	,	2.89094	,	3.95427	,	3.89795	,	3.9664
Index	,	0	,	1	,	2	,	3	,	4	,	5	,	6	,	7	,	8	,	9	,	10	,	11	,	12	,	13	,	14	,	15	,	16	,	17	,	18	,	19	,	20	,	21	,	22

ProfilingBuildAggregator: 
BuildBottomLevelAS	,	1.6686
BuildBottomLevelASGpu	,	2.36467
BuildTopLevelAS	,	0.8502
BuildTopLevelASGpu	,	0.089824
Duplication	,	1
Triangles	,	262267
Meshes	,	103
BottomLevels	,	1
TopLevelSize	,	64
BottomLevelsSize	,	18390656
Index	,	0

FpsAggregator: 
FPS	,	60	,	84	,	90	,	93	,	98	,	95	,	94	,	87	,	89	,	91	,	94	,	93	,	90	,	88	,	88	,	94	,	90	,	90	,	92	,	93	,	81	,	78	,	78
UPS	,	59	,	61	,	60	,	61	,	60	,	60	,	60	,	60	,	61	,	60	,	60	,	60	,	60	,	61	,	60	,	61	,	60	,	60	,	60	,	61	,	60	,	60	,	60
FrameTime	,	16.8196	,	11.9366	,	11.2295	,	10.8385	,	10.2483	,	10.566	,	10.6459	,	11.5359	,	11.3419	,	11.0037	,	10.6482	,	10.7661	,	11.1466	,	11.4102	,	11.4621	,	10.7113	,	11.112	,	11.1766	,	10.9534	,	10.784	,	12.411	,	12.8768	,	12.9005
GigaRays/s	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0
Index	,	0	,	1	,	2	,	3	,	4	,	5	,	6	,	7	,	8	,	9	,	10	,	11	,	12	,	13	,	14	,	15	,	16	,	17	,	18	,	19	,	20	,	21	,	22


AccelerationStructure: 
Ray tracing acceleration structure building statistics: 
	Bottom Level Acceleration Structure: 
		Maximum [B]: 18390656
		Minimum [B]: 18390656
		Currently Allocated [B]: 18390656
		Currently Created [num]: 1
		Current Average [B]: 18390656
		Maximum Triangles [num]: 262267
		Minimum Triangles [num]: 262267
		Total Triangles [num]: 262267
		Maximum Meshes [num]: 103
		Minimum Meshes [num]: 103
		Total Meshes [num]: 103
	Top Level Acceleration Structure: 
		Maximum [B]: 64
		Minimum [B]: 64
		Currently Allocated [B]: 64
		Total Allocated [B]: 64
		Total Created [num]: 1
		Average Allocated [B]: 64
		Maximum Instances [num]: 1
		Minimum Instances [num]: 1
		Total Instances [num]: 1
	Scratch Buffer: 
		Maximum [B]: 14995072
		Minimum [B]: 14995072
		Currently Allocated [B]: 14995072
		Total Allocated [B]: 14995072
		Total Created [num]: 1
		Average Allocated [B]: 14995072
ProfilingStack: 
[PROF] Profiling statistics: 
	Scopes entered : 91129
	Scopes exited : 91128
	Overhead per scope [ticks] : 102.1
	Threads entered : 3
	Threads exited : 0
	============================
		         Sum[Mt]   Parent[%]  Samples[num]       Avg[Mt]   Last/Curr[Mt]        Self[Mt]     Self[%] 	 Name

	main:

		   26066.180 Mt   100.000 %         1 x    26066.181 Mt    26066.180 Mt        0.434 Mt     0.002 % 	 /

		   26065.829 Mt    99.999 %         1 x    26065.831 Mt    26065.829 Mt      246.846 Mt     0.947 % 	 ./Main application loop

		       2.382 Mt     0.009 %         1 x        2.382 Mt        0.000 Mt        2.382 Mt   100.000 % 	 ../TexturedRLInitialization

		       1.276 Mt     0.005 %         1 x        1.276 Mt        0.000 Mt        1.276 Mt   100.000 % 	 ../ShadowMapRLInitialization

		       4.615 Mt     0.018 %         1 x        4.615 Mt        0.000 Mt        4.615 Mt   100.000 % 	 ../RasterResolveRLInitialization

		       3.684 Mt     0.014 %         1 x        3.684 Mt        0.000 Mt        3.684 Mt   100.000 % 	 ../ResolveRLInitialization

		   23135.661 Mt    88.759 %     69321 x        0.334 Mt       27.326 Mt      407.878 Mt     1.763 % 	 ../MainLoop

		     199.861 Mt     0.864 %      1386 x        0.144 Mt        0.107 Mt      199.255 Mt    99.697 % 	 .../Update

		       0.606 Mt     0.303 %         1 x        0.606 Mt        0.000 Mt        0.606 Mt   100.000 % 	 ..../GuiModelApply

		   22527.921 Mt    97.373 %      2031 x       11.092 Mt       12.337 Mt      238.794 Mt     1.060 % 	 .../Render

		     309.174 Mt     1.372 %      2031 x        0.152 Mt        0.222 Mt      306.297 Mt    99.069 % 	 ..../Rasterization

		       1.723 Mt     0.557 %      2031 x        0.001 Mt        0.001 Mt        1.723 Mt   100.000 % 	 ...../DeferredRLPrep

		       1.154 Mt     0.373 %      2031 x        0.001 Mt        0.001 Mt        1.154 Mt   100.000 % 	 ...../ShadowMapRLPrep

		   21979.953 Mt    97.568 %      2031 x       10.822 Mt       11.951 Mt    21979.953 Mt   100.000 % 	 ..../Present

		       2.755 Mt     0.011 %         1 x        2.755 Mt        0.000 Mt        2.755 Mt   100.000 % 	 ../AmbientOcclusionRLInitialization

		       5.210 Mt     0.020 %         1 x        5.210 Mt        0.000 Mt        5.210 Mt   100.000 % 	 ../DeferredRLInitialization

		      51.096 Mt     0.196 %         1 x       51.096 Mt        0.000 Mt        2.612 Mt     5.111 % 	 ../RayTracingRLInitialization

		      48.484 Mt    94.889 %         1 x       48.484 Mt        0.000 Mt       48.484 Mt   100.000 % 	 .../PipelineBuild

		    2612.305 Mt    10.022 %         1 x     2612.305 Mt        0.000 Mt        0.251 Mt     0.010 % 	 ../Initialization

		    2612.054 Mt    99.990 %         1 x     2612.054 Mt        0.000 Mt        2.122 Mt     0.081 % 	 .../ScenePrep

		    2542.072 Mt    97.321 %         1 x     2542.072 Mt        0.000 Mt      260.363 Mt    10.242 % 	 ..../SceneLoad

		    1997.172 Mt    78.565 %         1 x     1997.172 Mt        0.000 Mt      595.573 Mt    29.821 % 	 ...../glTF-LoadScene

		    1401.599 Mt    70.179 %        69 x       20.313 Mt        0.000 Mt     1401.599 Mt   100.000 % 	 ....../glTF-LoadImageData

		     231.362 Mt     9.101 %         1 x      231.362 Mt        0.000 Mt      231.362 Mt   100.000 % 	 ...../glTF-CreateScene

		      53.175 Mt     2.092 %         1 x       53.175 Mt        0.000 Mt       53.175 Mt   100.000 % 	 ...../SceneDesc-CreateScene

		       1.161 Mt     0.044 %         1 x        1.161 Mt        0.000 Mt        0.000 Mt     0.043 % 	 ..../ShadowMapRLPrep

		       1.161 Mt    99.957 %         1 x        1.161 Mt        0.000 Mt        1.125 Mt    96.925 % 	 ...../PrepareForRendering

		       0.036 Mt     3.075 %         1 x        0.036 Mt        0.000 Mt        0.036 Mt   100.000 % 	 ....../PrepareDrawBundle

		       3.556 Mt     0.136 %         1 x        3.556 Mt        0.000 Mt        0.000 Mt     0.014 % 	 ..../TexturedRLPrep

		       3.556 Mt    99.986 %         1 x        3.556 Mt        0.000 Mt        3.518 Mt    98.940 % 	 ...../PrepareForRendering

		       0.001 Mt     0.031 %         1 x        0.001 Mt        0.000 Mt        0.001 Mt   100.000 % 	 ....../PrepareMaterials

		       0.037 Mt     1.029 %         1 x        0.037 Mt        0.000 Mt        0.037 Mt   100.000 % 	 ....../PrepareDrawBundle

		       4.593 Mt     0.176 %         1 x        4.593 Mt        0.000 Mt        0.001 Mt     0.017 % 	 ..../DeferredRLPrep

		       4.592 Mt    99.983 %         1 x        4.592 Mt        0.000 Mt        4.057 Mt    88.341 % 	 ...../PrepareForRendering

		       0.001 Mt     0.011 %         1 x        0.001 Mt        0.000 Mt        0.001 Mt   100.000 % 	 ....../PrepareMaterials

		       0.446 Mt     9.706 %         1 x        0.446 Mt        0.000 Mt        0.446 Mt   100.000 % 	 ....../BuildMaterialCache

		       0.089 Mt     1.943 %         1 x        0.089 Mt        0.000 Mt        0.089 Mt   100.000 % 	 ....../PrepareDrawBundle

		      26.771 Mt     1.025 %         1 x       26.771 Mt        0.000 Mt        2.511 Mt     9.378 % 	 ..../RayTracingRLPrep

		       0.065 Mt     0.243 %         1 x        0.065 Mt        0.000 Mt        0.065 Mt   100.000 % 	 ...../PrepareAccelerationStructures

		       0.001 Mt     0.003 %         1 x        0.001 Mt        0.000 Mt        0.001 Mt   100.000 % 	 ...../PrepareCache

		      12.966 Mt    48.431 %         1 x       12.966 Mt        0.000 Mt       12.966 Mt   100.000 % 	 ...../PipelineBuild

		       0.663 Mt     2.475 %         1 x        0.663 Mt        0.000 Mt        0.663 Mt   100.000 % 	 ...../BuildCache

		       9.028 Mt    33.723 %         1 x        9.028 Mt        0.000 Mt        0.000 Mt     0.004 % 	 ...../BuildAccelerationStructures

		       9.027 Mt    99.996 %         1 x        9.027 Mt        0.000 Mt        6.509 Mt    72.099 % 	 ....../AccelerationStructureBuild

		       1.669 Mt    18.484 %         1 x        1.669 Mt        0.000 Mt        1.669 Mt   100.000 % 	 ......./BuildBottomLevelAS

		       0.850 Mt     9.418 %         1 x        0.850 Mt        0.000 Mt        0.850 Mt   100.000 % 	 ......./BuildTopLevelAS

		       1.539 Mt     5.747 %         1 x        1.539 Mt        0.000 Mt        1.539 Mt   100.000 % 	 ...../BuildShaderTables

		      31.779 Mt     1.217 %         1 x       31.779 Mt        0.000 Mt       31.779 Mt   100.000 % 	 ..../GpuSidePrep

	WindowThread:

		   26064.742 Mt   100.000 %         1 x    26064.743 Mt    26064.742 Mt    26064.742 Mt   100.000 % 	 /

	GpuThread:

		   21013.392 Mt   100.000 %      2031 x       10.346 Mt       11.366 Mt      149.168 Mt     0.710 % 	 /

		       2.462 Mt     0.012 %         1 x        2.462 Mt        0.000 Mt        0.006 Mt     0.252 % 	 ./ScenePrep

		       2.456 Mt    99.748 %         1 x        2.456 Mt        0.000 Mt        0.001 Mt     0.051 % 	 ../AccelerationStructureBuild

		       2.365 Mt    96.291 %         1 x        2.365 Mt        0.000 Mt        2.365 Mt   100.000 % 	 .../BuildBottomLevelASGpu

		       0.090 Mt     3.658 %         1 x        0.090 Mt        0.000 Mt        0.090 Mt   100.000 % 	 .../BuildTopLevelASGpu

		   20850.392 Mt    99.224 %      2031 x       10.266 Mt       11.363 Mt        5.434 Mt     0.026 % 	 ./RasterizationGpu

		    4512.678 Mt    21.643 %      2031 x        2.222 Mt        3.914 Mt     4512.678 Mt   100.000 % 	 ../DeferredRLGpu

		   15239.418 Mt    73.089 %      2031 x        7.503 Mt        6.985 Mt    15239.418 Mt   100.000 % 	 ../AmbientOcclusionRLGpu

		    1092.862 Mt     5.241 %      2031 x        0.538 Mt        0.463 Mt     1092.862 Mt   100.000 % 	 ../RasterResolveRLGpu

		      11.369 Mt     0.054 %      2031 x        0.006 Mt        0.002 Mt       11.369 Mt   100.000 % 	 ./Present


	============================


