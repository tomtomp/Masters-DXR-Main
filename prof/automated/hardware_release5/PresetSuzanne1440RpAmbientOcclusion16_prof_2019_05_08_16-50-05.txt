Configuration: 
Command line parameters  = E:\programming\FIT-DIP\Masters-DXR-Main-git\build\Measurement\x64\Release\Measurement.exe --scene Suzanne/Suzanne.gltf --profile-automation --profile-camera-track Suzanne/Suzanne.trk --rt-mode-pure --width 2560 --height 1440 --profile-output PresetSuzanne1440RpAmbientOcclusion16 --quality-preset AmbientOcclusion16 
Build type               = Release
Device name              = NVIDIA GeForce RTX 2080 Ti
Device memory MB         = 11049
Ray Tracing backend      = Hardware
Window width             = 2560
Window height            = 1421
Window title             = Direct3D App
Back-buffer count        = 3
Fill Triangles           = enabled
Triangles Font Clock-Wise= enabled
Fast Build AS            = disabled
Target FPS               = 0
Target UPS               = 60
Tearing                  = enabled
VSync                    = disabled
GUI rendering            = disabled
Debug layer              = disabled
GPU validation           = disabled
GPU profiling            = enabled
Render mode              = Ray Tracing
Loaded scene file        = Suzanne/Suzanne.gltf
Using testing scene      = disabled
Duplication              = 1
Duplication in cube      = disabled
Duplication offset       = 10
BLAS duplication         = disabled
Rays per pixel           = 17
Quality preset           = AmbientOcclusion16
Rasterization Quality    : 
  AO                     = enabled
  AO kernel size         = 16
  Shadows                = disabled
  Shadow map resolution  = 1
  Shadow PCF kernel size = 0
  Reflections            = disabled
Ray Tracing Quality      : 
  AO                     = enabled
  AO rays                = 16
  AO kernel size         = 4
  Shadows                = disabled
  Shadow rays            = 0
  Shadow kernel size     = 4
  Reflections            = disabled

ProfilingAggregator: 
Render	,	1.6275	,	1.9799	,	2.2045	,	2.0127	,	1.409	,	1.0503	,	0.9622	,	0.729	,	0.9555	,	1.3462	,	1.7449	,	1.2072	,	0.8603	,	0.9967	,	1.1964	,	2.0329	,	2.8501	,	3.7602	,	5.8226	,	3.3759	,	2.2781	,	1.2687	,	1.0334	,	0.7835
Update	,	0.0774	,	0.0433	,	0.0447	,	0.0822	,	0.0792	,	0.0755	,	0.0716	,	0.073	,	0.078	,	0.0393	,	0.0735	,	0.0798	,	0.0758	,	0.04	,	0.0375	,	0.0401	,	0.0461	,	0.085	,	0.0779	,	0.0816	,	0.0393	,	0.0416	,	0.0711	,	0.0742
RayTracing	,	0.0634	,	0.0737	,	0.0331	,	0.0768	,	0.0309	,	0.0272	,	0.0755	,	0.0207	,	0.0721	,	0.0274	,	0.071	,	0.0272	,	0.0563	,	0.0684	,	0.0548	,	0.0286	,	0.0715	,	0.0438	,	0.0805	,	0.0686	,	0.0301	,	0.0338	,	0.0635	,	0.0199
RayTracingGpu	,	1.02912	,	1.47533	,	1.7879	,	1.53856	,	1.02506	,	0.68112	,	0.547712	,	0.48864	,	0.634976	,	0.956352	,	1.13533	,	0.849088	,	0.574432	,	0.570048	,	0.847104	,	1.69482	,	2.35434	,	3.27683	,	5.24509	,	2.97216	,	2.04397	,	1.01498	,	0.65952	,	0.589472
RayTracingRLGpu	,	1.02758	,	1.4745	,	1.78717	,	1.53786	,	1.02419	,	0.679456	,	0.54608	,	0.487136	,	0.634272	,	0.954528	,	1.13382	,	0.84736	,	0.572928	,	0.568416	,	0.845632	,	1.69414	,	2.35331	,	3.27514	,	5.24438	,	2.97126	,	2.04246	,	1.01392	,	0.658048	,	0.58864
Index	,	0	,	1	,	2	,	3	,	4	,	5	,	6	,	7	,	8	,	9	,	10	,	11	,	12	,	13	,	14	,	15	,	16	,	17	,	18	,	19	,	20	,	21	,	22	,	23

ProfilingBuildAggregator: 
BuildBottomLevelAS	,	1.1057
BuildBottomLevelASGpu	,	0.453792
BuildTopLevelAS	,	0.9926
BuildTopLevelASGpu	,	0.062208
Duplication	,	1
Triangles	,	3936
Meshes	,	1
BottomLevels	,	1
TopLevelSize	,	64
BottomLevelsSize	,	282112
Index	,	0

FpsAggregator: 
FPS	,	681	,	561	,	455	,	444	,	561	,	790	,	1002	,	1169	,	1120	,	847	,	626	,	666	,	917	,	1133	,	944	,	595	,	390	,	305	,	202	,	224	,	331	,	510	,	845	,	1021
UPS	,	59	,	60	,	61	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	61	,	60	,	60	,	60	,	60
FrameTime	,	1.46882	,	1.78393	,	2.2027	,	2.25578	,	1.78339	,	1.26665	,	0.998331	,	0.856035	,	0.893418	,	1.18101	,	1.59834	,	1.50177	,	1.09107	,	0.882855	,	1.05996	,	1.68101	,	2.5683	,	3.2819	,	4.9783	,	4.47614	,	3.02373	,	1.96101	,	1.18385	,	0.979866
GigaRays/s	,	42.103	,	34.6662	,	28.0756	,	27.4149	,	34.6765	,	48.8233	,	61.9453	,	72.2423	,	69.2195	,	52.3636	,	38.6913	,	41.1794	,	56.6803	,	70.0477	,	58.3438	,	36.7885	,	24.079	,	18.8433	,	12.4223	,	13.8159	,	20.4522	,	31.5358	,	52.2381	,	63.1126
Index	,	0	,	1	,	2	,	3	,	4	,	5	,	6	,	7	,	8	,	9	,	10	,	11	,	12	,	13	,	14	,	15	,	16	,	17	,	18	,	19	,	20	,	21	,	22	,	23


AccelerationStructure: 
Ray tracing acceleration structure building statistics: 
	Bottom Level Acceleration Structure: 
		Maximum [B]: 282112
		Minimum [B]: 282112
		Currently Allocated [B]: 282112
		Currently Created [num]: 1
		Current Average [B]: 282112
		Maximum Triangles [num]: 3936
		Minimum Triangles [num]: 3936
		Total Triangles [num]: 3936
		Maximum Meshes [num]: 1
		Minimum Meshes [num]: 1
		Total Meshes [num]: 1
	Top Level Acceleration Structure: 
		Maximum [B]: 64
		Minimum [B]: 64
		Currently Allocated [B]: 64
		Total Allocated [B]: 64
		Total Created [num]: 1
		Average Allocated [B]: 64
		Maximum Instances [num]: 1
		Minimum Instances [num]: 1
		Total Instances [num]: 1
	Scratch Buffer: 
		Maximum [B]: 82944
		Minimum [B]: 82944
		Currently Allocated [B]: 82944
		Total Allocated [B]: 82944
		Total Created [num]: 1
		Average Allocated [B]: 82944
ProfilingStack: 
[PROF] Profiling statistics: 
	Scopes entered : 228246
	Scopes exited : 228245
	Overhead per scope [ticks] : 102.5
	Threads entered : 3
	Threads exited : 0
	============================
		         Sum[Mt]   Parent[%]  Samples[num]       Avg[Mt]   Last/Curr[Mt]        Self[Mt]     Self[%] 	 Name

	main:

		   24526.656 Mt   100.000 %         1 x    24526.656 Mt    24526.656 Mt        0.340 Mt     0.001 % 	 /

		   24526.325 Mt    99.999 %         1 x    24526.326 Mt    24526.325 Mt      292.742 Mt     1.194 % 	 ./Main application loop

		       2.217 Mt     0.009 %         1 x        2.217 Mt        0.000 Mt        2.217 Mt   100.000 % 	 ../TexturedRLInitialization

		       1.116 Mt     0.005 %         1 x        1.116 Mt        0.000 Mt        1.116 Mt   100.000 % 	 ../ShadowMapRLInitialization

		       4.070 Mt     0.017 %         1 x        4.070 Mt        0.000 Mt        4.070 Mt   100.000 % 	 ../RasterResolveRLInitialization

		       3.826 Mt     0.016 %         1 x        3.826 Mt        0.000 Mt        3.826 Mt   100.000 % 	 ../ResolveRLInitialization

		   24026.799 Mt    97.963 %     96031 x        0.250 Mt        0.939 Mt      190.584 Mt     0.793 % 	 ../MainLoop

		     153.190 Mt     0.638 %      1442 x        0.106 Mt        0.037 Mt      152.972 Mt    99.858 % 	 .../Update

		       0.218 Mt     0.142 %         1 x        0.218 Mt        0.000 Mt        0.218 Mt   100.000 % 	 ..../GuiModelApply

		   23683.024 Mt    98.569 %     16341 x        1.449 Mt        0.901 Mt      900.756 Mt     3.803 % 	 .../Render

		     795.602 Mt     3.359 %     16341 x        0.049 Mt        0.041 Mt       51.445 Mt     6.466 % 	 ..../RayTracing

		     744.157 Mt    93.534 %     16341 x        0.046 Mt        0.038 Mt      735.739 Mt    98.869 % 	 ...../RayCasting

		       8.418 Mt     1.131 %     16341 x        0.001 Mt        0.000 Mt        8.418 Mt   100.000 % 	 ....../RayTracingRLPrep

		   21986.666 Mt    92.837 %     16341 x        1.345 Mt        0.808 Mt    21986.666 Mt   100.000 % 	 ..../Present

		       2.580 Mt     0.011 %         1 x        2.580 Mt        0.000 Mt        2.580 Mt   100.000 % 	 ../AmbientOcclusionRLInitialization

		       5.926 Mt     0.024 %         1 x        5.926 Mt        0.000 Mt        5.926 Mt   100.000 % 	 ../DeferredRLInitialization

		      60.531 Mt     0.247 %         1 x       60.531 Mt        0.000 Mt        2.524 Mt     4.169 % 	 ../RayTracingRLInitialization

		      58.008 Mt    95.831 %         1 x       58.008 Mt        0.000 Mt       58.008 Mt   100.000 % 	 .../PipelineBuild

		     126.519 Mt     0.516 %         1 x      126.519 Mt        0.000 Mt        0.198 Mt     0.156 % 	 ../Initialization

		     126.322 Mt    99.844 %         1 x      126.322 Mt        0.000 Mt        1.244 Mt     0.985 % 	 .../ScenePrep

		      78.211 Mt    61.914 %         1 x       78.211 Mt        0.000 Mt       15.618 Mt    19.969 % 	 ..../SceneLoad

		      53.639 Mt    68.582 %         1 x       53.639 Mt        0.000 Mt       14.764 Mt    27.526 % 	 ...../glTF-LoadScene

		      38.875 Mt    72.474 %         2 x       19.437 Mt        0.000 Mt       38.875 Mt   100.000 % 	 ....../glTF-LoadImageData

		       6.734 Mt     8.610 %         1 x        6.734 Mt        0.000 Mt        6.734 Mt   100.000 % 	 ...../glTF-CreateScene

		       2.220 Mt     2.838 %         1 x        2.220 Mt        0.000 Mt        2.220 Mt   100.000 % 	 ...../SceneDesc-CreateScene

		       0.321 Mt     0.254 %         1 x        0.321 Mt        0.000 Mt        0.000 Mt     0.125 % 	 ..../ShadowMapRLPrep

		       0.320 Mt    99.875 %         1 x        0.320 Mt        0.000 Mt        0.312 Mt    97.376 % 	 ...../PrepareForRendering

		       0.008 Mt     2.624 %         1 x        0.008 Mt        0.000 Mt        0.008 Mt   100.000 % 	 ....../PrepareDrawBundle

		       1.951 Mt     1.544 %         1 x        1.951 Mt        0.000 Mt        0.000 Mt     0.021 % 	 ..../TexturedRLPrep

		       1.950 Mt    99.979 %         1 x        1.950 Mt        0.000 Mt        1.944 Mt    99.656 % 	 ...../PrepareForRendering

		       0.002 Mt     0.118 %         1 x        0.002 Mt        0.000 Mt        0.002 Mt   100.000 % 	 ....../PrepareMaterials

		       0.004 Mt     0.226 %         1 x        0.004 Mt        0.000 Mt        0.004 Mt   100.000 % 	 ....../PrepareDrawBundle

		       1.643 Mt     1.300 %         1 x        1.643 Mt        0.000 Mt        0.000 Mt     0.024 % 	 ..../DeferredRLPrep

		       1.642 Mt    99.976 %         1 x        1.642 Mt        0.000 Mt        1.289 Mt    78.495 % 	 ...../PrepareForRendering

		       0.001 Mt     0.049 %         1 x        0.001 Mt        0.000 Mt        0.001 Mt   100.000 % 	 ....../PrepareMaterials

		       0.348 Mt    21.219 %         1 x        0.348 Mt        0.000 Mt        0.348 Mt   100.000 % 	 ....../BuildMaterialCache

		       0.004 Mt     0.237 %         1 x        0.004 Mt        0.000 Mt        0.004 Mt   100.000 % 	 ....../PrepareDrawBundle

		      30.101 Mt    23.829 %         1 x       30.101 Mt        0.000 Mt        3.014 Mt    10.014 % 	 ..../RayTracingRLPrep

		       0.050 Mt     0.166 %         1 x        0.050 Mt        0.000 Mt        0.050 Mt   100.000 % 	 ...../PrepareAccelerationStructures

		       0.001 Mt     0.002 %         1 x        0.001 Mt        0.000 Mt        0.001 Mt   100.000 % 	 ...../PrepareCache

		      16.836 Mt    55.931 %         1 x       16.836 Mt        0.000 Mt       16.836 Mt   100.000 % 	 ...../PipelineBuild

		       0.427 Mt     1.420 %         1 x        0.427 Mt        0.000 Mt        0.427 Mt   100.000 % 	 ...../BuildCache

		       8.099 Mt    26.908 %         1 x        8.099 Mt        0.000 Mt        0.000 Mt     0.006 % 	 ...../BuildAccelerationStructures

		       8.099 Mt    99.994 %         1 x        8.099 Mt        0.000 Mt        6.001 Mt    74.092 % 	 ....../AccelerationStructureBuild

		       1.106 Mt    13.652 %         1 x        1.106 Mt        0.000 Mt        1.106 Mt   100.000 % 	 ......./BuildBottomLevelAS

		       0.993 Mt    12.256 %         1 x        0.993 Mt        0.000 Mt        0.993 Mt   100.000 % 	 ......./BuildTopLevelAS

		       1.673 Mt     5.558 %         1 x        1.673 Mt        0.000 Mt        1.673 Mt   100.000 % 	 ...../BuildShaderTables

		      12.852 Mt    10.174 %         1 x       12.852 Mt        0.000 Mt       12.852 Mt   100.000 % 	 ..../GpuSidePrep

	WindowThread:

		   24524.490 Mt   100.000 %         1 x    24524.490 Mt    24524.490 Mt    24524.490 Mt   100.000 % 	 /

	GpuThread:

		   17190.227 Mt   100.000 %     16341 x        1.052 Mt        0.589 Mt      132.193 Mt     0.769 % 	 /

		       0.523 Mt     0.003 %         1 x        0.523 Mt        0.000 Mt        0.006 Mt     1.090 % 	 ./ScenePrep

		       0.517 Mt    98.910 %         1 x        0.517 Mt        0.000 Mt        0.001 Mt     0.210 % 	 ../AccelerationStructureBuild

		       0.454 Mt    87.759 %         1 x        0.454 Mt        0.000 Mt        0.454 Mt   100.000 % 	 .../BuildBottomLevelASGpu

		       0.062 Mt    12.030 %         1 x        0.062 Mt        0.000 Mt        0.062 Mt   100.000 % 	 .../BuildTopLevelASGpu

		   17028.125 Mt    99.057 %     16341 x        1.042 Mt        0.587 Mt       20.954 Mt     0.123 % 	 ./RayTracingGpu

		   17007.172 Mt    99.877 %     16341 x        1.041 Mt        0.586 Mt    17007.172 Mt   100.000 % 	 ../RayTracingRLGpu

		      29.386 Mt     0.171 %     16341 x        0.002 Mt        0.002 Mt       29.386 Mt   100.000 % 	 ./Present


	============================


