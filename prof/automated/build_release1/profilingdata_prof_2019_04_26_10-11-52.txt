Configuration: 
Command line parameters  = 
Window width             = 1186
Window height            = 1219
Window title             = Direct3D App
Back-buffer count        = 3
Fill Triangles           = enabled
Triangles Font Clock-Wise= disabled
Fast Build AS            = disabled
Target FPS               = 0
Target UPS               = 60
Tearing                  = enabled
VSync                    = disabled
GUI rendering            = disabled
Debug layer              = enabled
GPU validation           = disabled
GPU profiling            = enabled
Render mode              = Ray Tracing
Loaded scene file        = 
Using testing scene      = disabled
BLAS duplication         = disabled
Rays per pixel           = 14
Quality preset           = High
Rasterization Quality    : 
  AO                     = enabled
  AO kernel size         = 4
  Shadows                = enabled
  Shadow map resolution  = 4096
  Shadow PCF kernel size = 4
  Reflections            = enabled
Ray Tracing Quality      : 
  AO                     = enabled
  AO rays                = 4
  AO kernel size         = 4
  Shadows                = enabled
  Shadow rays            = 4
  Shadow kernel size     = 4
  Reflections            = enabled

ProfilingAggregator: 
Render	,	60.0809	,	6.0221	,	5.4902	,	5.2218	,	5.9389	,	5.0414	,	4.8068	,	4.7562	,	5.6544	,	6.0627	,	5.7031	,	6.6475	,	5.6377
Update	,	0.0153	,	0.0134	,	0.0592	,	0.0554	,	0.0171	,	0.0562	,	0.0723	,	0.0548	,	0.0136	,	0.0562	,	0.0565	,	0.0555	,	0.0559
RayTracing	,	27.2388	,	2.5717	,	2.3609	,	2.3272	,	2.5013	,	2.1752	,	2.0081	,	1.9923	,	2.0021	,	2.2705	,	1.994	,	2.0226	,	1.9784
RayTracingGpu	,	8.81898	,	0.395232	,	1.58954	,	1.44816	,	1.34493	,	1.34182	,	1.36294	,	1.36694	,	2.23622	,	2.25437	,	2.23795	,	2.61072	,	2.25888
DeferredRLGpu	,	0.028256	,	0.037984	,	0.28096	,	0.260288	,	0.221536	,	0.221184	,	0.256704	,	0.256768	,	0.393952	,	0.384512	,	0.381568	,	0.379072	,	0.394432
RayTracingRLGpu	,	0.038368	,	0.147936	,	1.07098	,	0.949728	,	0.8864	,	0.883296	,	0.868128	,	0.870336	,	1.44918	,	1.47546	,	1.46794	,	1.82112	,	1.46634
ResolveRLGpu	,	0.180576	,	0.208	,	0.236192	,	0.23648	,	0.234112	,	0.23552	,	0.2368	,	0.238464	,	0.390464	,	0.392608	,	0.386784	,	0.403232	,	0.39504
Index	,	0	,	1	,	2	,	3	,	4	,	5	,	6	,	7	,	8	,	9	,	10	,	11	,	12

ProfilingBuildAggregator: 
BuildBottomLevelAS	,	15.8118
BuildBottomLevelASGpu	,	8.43379
BuildTopLevelAS	,	0.4198
BuildTopLevelASGpu	,	0.131136
Triangles	,	281182
Meshes	,	52
BottomLevels	,	43
TopLevelSize	,	2752
BottomLevelsSize	,	15869344
Index	,	0

FpsAggregator: 
FPS	,	1	,	1	,	55	,	60	,	60	,	38	,	35	,	40	,	28	,	35	,	38	,	4	,	60
UPS	,	39	,	46	,	130	,	61	,	61	,	37	,	120	,	61	,	60	,	61	,	62	,	61	,	62
FrameTime	,	1433.11	,	1182.4	,	18.2722	,	16.8396	,	16.6737	,	42.6587	,	28.969	,	25.8361	,	35.7396	,	29.0681	,	27.192	,	251.812	,	16.8067
GigaRays/s	,	0.00768265	,	0.00931164	,	0.602558	,	0.653818	,	0.660323	,	0.258096	,	0.380064	,	0.42615	,	0.525688	,	0.64634	,	0.690934	,	0.0746106	,	1.11788
Index	,	0	,	1	,	2	,	3	,	4	,	5	,	6	,	7	,	8	,	9	,	10	,	11	,	12


AccelerationStructure: 
Ray tracing acceleration structure building statistics: 
	Bottom Level Acceleration Structure: 
		Maximum [B]: 2940416
		Minimum [B]: 6256
		Currently Allocated [B]: 15869344
		Currently Created [num]: 43
		Current Average [B]: 369054
		Maximum Triangles [num]: 52674
		Minimum Triangles [num]: 27
		Total Triangles [num]: 281182
		Maximum Meshes [num]: 2
		Minimum Meshes [num]: 1
		Total Meshes [num]: 52
	Top Level Acceleration Structure: 
		Maximum [B]: 2752
		Minimum [B]: 2752
		Currently Allocated [B]: 2752
		Total Allocated [B]: 2752
		Total Created [num]: 1
		Average Allocated [B]: 2752
		Maximum Instances [num]: 43
		Minimum Instances [num]: 43
		Total Instances [num]: 43
	Scratch Buffer: 
		Maximum [B]: 2344448
		Minimum [B]: 2344448
		Currently Allocated [B]: 2344448
		Total Allocated [B]: 2344448
		Total Created [num]: 1
		Average Allocated [B]: 2344448
ProfilingStack: 
[PROF] Profiling statistics: 
	Scopes entered : 6746
	Scopes exited : 6746
	Overhead per scope [ticks] : 164.4
	Threads entered : 3
	Threads exited : 0
	============================
		         Sum[Mt]   Parent[%]  Samples[num]       Avg[Mt]   Last/Curr[Mt]        Self[Mt]     Self[%] 	 Name

	main:

		   15224.258 Mt   100.000 %         1 x    15224.259 Mt    15224.258 Mt    11476.463 Mt    75.383 % 	 /

		      62.219 Mt     0.409 %         1 x       62.219 Mt        0.000 Mt        0.685 Mt     1.101 % 	 ./Initialize

		       2.441 Mt     3.923 %         1 x        2.441 Mt        0.000 Mt        2.441 Mt   100.000 % 	 ../TexturedRLInitialization

		       1.670 Mt     2.683 %         1 x        1.670 Mt        0.000 Mt        1.670 Mt   100.000 % 	 ../ShadowMapRLInitialization

		       6.620 Mt    10.641 %         1 x        6.620 Mt        0.000 Mt        6.620 Mt   100.000 % 	 ../AmbientOcclusionRLInitialization

		       4.204 Mt     6.757 %         1 x        4.204 Mt        0.000 Mt        4.204 Mt   100.000 % 	 ../RasterResolveRLInitialization

		       3.852 Mt     6.192 %         1 x        3.852 Mt        0.000 Mt        3.852 Mt   100.000 % 	 ../DeferredRLInitialization

		      38.944 Mt    62.592 %         1 x       38.944 Mt        0.000 Mt        1.245 Mt     3.197 % 	 ../RayTracingRLInitialization

		      37.699 Mt    96.803 %         1 x       37.699 Mt        0.000 Mt       37.699 Mt   100.000 % 	 .../PipelineBuild

		       3.802 Mt     6.110 %         1 x        3.802 Mt        0.000 Mt        3.802 Mt   100.000 % 	 ../ResolveRLInitialization

		    3685.576 Mt    24.209 %       495 x        7.446 Mt        5.918 Mt       17.461 Mt     0.474 % 	 ./Run

		     880.874 Mt    23.901 %       902 x        0.977 Mt        0.057 Mt      299.449 Mt    33.995 % 	 ../Update

		     508.523 Mt    57.729 %         1 x      508.523 Mt        0.000 Mt      503.644 Mt    99.040 % 	 .../ScenePrep

		       4.880 Mt     0.960 %         1 x        4.880 Mt        0.000 Mt        4.880 Mt   100.000 % 	 ..../SceneDesc-CreateScene

		      72.901 Mt     8.276 %        62 x        1.176 Mt        0.000 Mt       28.679 Mt    39.340 % 	 .../AccelerationStructureBuild

		      18.420 Mt    25.267 %        62 x        0.297 Mt        0.000 Mt       18.420 Mt   100.000 % 	 ..../BuildBottomLevelAS

		      25.802 Mt    35.394 %        62 x        0.416 Mt        0.000 Mt       25.802 Mt   100.000 % 	 ..../BuildTopLevelAS

		    2787.241 Mt    75.626 %       495 x        5.631 Mt        5.858 Mt        0.553 Mt     0.020 % 	 ../Render

		    2786.688 Mt    99.980 %       495 x        5.630 Mt        5.857 Mt     1683.082 Mt    60.397 % 	 .../Render

		    1103.606 Mt    39.603 %       495 x        2.230 Mt        1.989 Mt     1078.253 Mt    97.703 % 	 ..../RayTracing

		       3.418 Mt     0.310 %       495 x        0.007 Mt        0.001 Mt        0.473 Mt    13.826 % 	 ...../DeferredRLPrep

		       2.946 Mt    86.174 %         1 x        2.946 Mt        0.000 Mt        1.602 Mt    54.398 % 	 ....../PrepareForRendering

		       0.062 Mt     2.091 %         1 x        0.062 Mt        0.000 Mt        0.062 Mt   100.000 % 	 ......./PrepareMaterials

		       0.254 Mt     8.606 %         1 x        0.254 Mt        0.000 Mt        0.254 Mt   100.000 % 	 ......./BuildMaterialCache

		       1.028 Mt    34.904 %         1 x        1.028 Mt        0.000 Mt        1.028 Mt   100.000 % 	 ......./PrepareDrawBundle

		      21.935 Mt     1.988 %       495 x        0.044 Mt        0.001 Mt        2.541 Mt    11.584 % 	 ...../RayTracingRLPrep

		       1.360 Mt     6.202 %         1 x        1.360 Mt        0.000 Mt        1.360 Mt   100.000 % 	 ....../PrepareAccelerationStructures

		       0.050 Mt     0.226 %         1 x        0.050 Mt        0.000 Mt        0.050 Mt   100.000 % 	 ....../PrepareCache

		       0.492 Mt     2.245 %         1 x        0.492 Mt        0.000 Mt        0.492 Mt   100.000 % 	 ....../BuildCache

		      17.095 Mt    77.935 %         1 x       17.095 Mt        0.000 Mt        0.001 Mt     0.006 % 	 ....../BuildAccelerationStructures

		      17.094 Mt    99.994 %         1 x       17.094 Mt        0.000 Mt        0.862 Mt     5.043 % 	 ......./AccelerationStructureBuild

		      15.812 Mt    92.501 %         1 x       15.812 Mt        0.000 Mt       15.812 Mt   100.000 % 	 ......../BuildBottomLevelAS

		       0.420 Mt     2.456 %         1 x        0.420 Mt        0.000 Mt        0.420 Mt   100.000 % 	 ......../BuildTopLevelAS

		       0.397 Mt     1.809 %         1 x        0.397 Mt        0.000 Mt        0.397 Mt   100.000 % 	 ....../BuildShaderTables

		       0.001 Mt     0.000 %         1 x        0.001 Mt        0.001 Mt        0.001 Mt   100.000 % 	 ./Destroy

	WindowThread:

		   15222.530 Mt   100.000 %         1 x    15222.530 Mt    15222.530 Mt    15222.530 Mt   100.000 % 	 /

	GpuThread:

		     919.803 Mt   100.000 %       556 x        1.654 Mt        2.482 Mt        0.085 Mt     0.009 % 	 /

		      20.876 Mt     2.270 %        62 x        0.337 Mt        0.000 Mt        0.054 Mt     0.258 % 	 ./AccelerationStructureBuild

		      17.305 Mt    82.894 %        62 x        0.279 Mt        0.000 Mt       17.305 Mt   100.000 % 	 ../BuildBottomLevelASGpu

		       3.517 Mt    16.848 %        62 x        0.057 Mt        0.000 Mt        3.517 Mt   100.000 % 	 ../BuildTopLevelASGpu

		     895.944 Mt    97.406 %       495 x        1.810 Mt        2.479 Mt        1.061 Mt     0.118 % 	 ./RayTracingGpu

		     146.766 Mt    16.381 %       495 x        0.296 Mt        0.398 Mt      146.766 Mt   100.000 % 	 ../DeferredRLGpu

		       8.566 Mt     0.956 %         1 x        8.566 Mt        0.000 Mt        0.001 Mt     0.010 % 	 ../AccelerationStructureBuild

		       8.434 Mt    98.459 %         1 x        8.434 Mt        0.000 Mt        8.434 Mt   100.000 % 	 .../BuildBottomLevelASGpu

		       0.131 Mt     1.531 %         1 x        0.131 Mt        0.000 Mt        0.131 Mt   100.000 % 	 .../BuildTopLevelASGpu

		     587.366 Mt    65.558 %       495 x        1.187 Mt        1.683 Mt      587.366 Mt   100.000 % 	 ../RayTracingRLGpu

		     152.185 Mt    16.986 %       495 x        0.307 Mt        0.395 Mt      152.185 Mt   100.000 % 	 ../ResolveRLGpu

		       2.898 Mt     0.315 %       495 x        0.006 Mt        0.001 Mt        2.898 Mt   100.000 % 	 ./Present


	============================


