Configuration: 
Command line parameters  = 
Build type               = Release
Device name              = NVIDIA GeForce RTX 2080 Ti
Device memory MB         = 11049
Ray Tracing backend      = Hardware
Window width             = 1161
Window height            = 1219
Window title             = Direct3D App
Back-buffer count        = 3
Fill Triangles           = enabled
Triangles Font Clock-Wise= disabled
Fast Build AS            = disabled
Target FPS               = 0
Target UPS               = 60
Tearing                  = enabled
VSync                    = disabled
GUI rendering            = disabled
Debug layer              = disabled
GPU validation           = disabled
GPU profiling            = enabled
Render mode              = Ray Tracing
Loaded scene file        = 
Using testing scene      = disabled
BLAS duplication         = disabled
Rays per pixel           = 8
Quality preset           = Medium
Rasterization Quality    : 
  AO                     = enabled
  AO kernel size         = 4
  Shadows                = enabled
  Shadow map resolution  = 4096
  Shadow PCF kernel size = 4
  Reflections            = disabled
Ray Tracing Quality      : 
  AO                     = enabled
  AO rays                = 4
  AO kernel size         = 4
  Shadows                = enabled
  Shadow rays            = 4
  Shadow kernel size     = 4
  Reflections            = disabled

ProfilingAggregator: 
Render	,	3.7406	,	3.2708
Update	,	0.0896	,	0.268
RayTracing	,	0.1606	,	0.1285
RayTracingGpu	,	2.96397	,	2.68614
DeferredRLGpu	,	0.326816	,	0.228512
RayTracingRLGpu	,	1.44384	,	1.68096
ResolveRLGpu	,	1.18966	,	0.773216
Index	,	0	,	1

ProfilingBuildAggregator: 
BuildBottomLevelAS	,	0.2764
BuildBottomLevelASGpu	,	0.136448
BuildTopLevelAS	,	0.4373
BuildTopLevelASGpu	,	0.078464
Triangles	,	112112
Meshes	,	1702
BottomLevels	,	1686
TopLevelSize	,	107904
BottomLevelsSize	,	14168480
Index	,	0

FpsAggregator: 
FPS	,	3	,	58
UPS	,	12	,	279
FrameTime	,	1280.22	,	18.1034
GigaRays/s	,	0.00802793	,	0.567712
Index	,	0	,	1


AccelerationStructure: 
Ray tracing acceleration structure building statistics: 
	Bottom Level Acceleration Structure: 
		Maximum [B]: 239488
		Minimum [B]: 2688
		Currently Allocated [B]: 14168480
		Currently Created [num]: 1686
		Current Average [B]: 8403
		Maximum Triangles [num]: 3348
		Minimum Triangles [num]: 2
		Total Triangles [num]: 112112
		Maximum Meshes [num]: 2
		Minimum Meshes [num]: 1
		Total Meshes [num]: 1702
	Top Level Acceleration Structure: 
		Maximum [B]: 107904
		Minimum [B]: 107904
		Currently Allocated [B]: 107904
		Total Allocated [B]: 107904
		Total Created [num]: 1
		Average Allocated [B]: 107904
		Maximum Instances [num]: 1686
		Minimum Instances [num]: 1686
		Total Instances [num]: 1686
	Scratch Buffer: 
		Maximum [B]: 131584
		Minimum [B]: 131584
		Currently Allocated [B]: 131584
		Total Allocated [B]: 131584
		Total Created [num]: 1
		Average Allocated [B]: 131584
ProfilingStack: 
[PROF] Profiling statistics: 
	Scopes entered : 24130
	Scopes exited : 24130
	Overhead per scope [ticks] : 162.8
	Threads entered : 3
	Threads exited : 0
	============================
		         Sum[Mt]   Parent[%]  Samples[num]       Avg[Mt]   Last/Curr[Mt]        Self[Mt]     Self[%] 	 Name

	main:

		   14069.122 Mt   100.000 %         1 x    14069.123 Mt    14069.122 Mt     5833.271 Mt    41.462 % 	 /

		      64.884 Mt     0.461 %         1 x       64.884 Mt        0.000 Mt        0.683 Mt     1.053 % 	 ./Initialize

		       1.437 Mt     2.214 %         1 x        1.437 Mt        0.000 Mt        1.437 Mt   100.000 % 	 ../ShadowMapRLInitialization

		       6.147 Mt     9.474 %         1 x        6.147 Mt        0.000 Mt        6.147 Mt   100.000 % 	 ../AmbientOcclusionRLInitialization

		       3.867 Mt     5.960 %         1 x        3.867 Mt        0.000 Mt        3.867 Mt   100.000 % 	 ../RasterResolveRLInitialization

		       3.896 Mt     6.004 %         1 x        3.896 Mt        0.000 Mt        3.896 Mt   100.000 % 	 ../DeferredRLInitialization

		      45.277 Mt    69.781 %         1 x       45.277 Mt        0.000 Mt        1.740 Mt     3.843 % 	 ../RayTracingRLInitialization

		      43.537 Mt    96.157 %         1 x       43.537 Mt        0.000 Mt       43.537 Mt   100.000 % 	 .../PipelineBuild

		       3.578 Mt     5.514 %         1 x        3.578 Mt        0.000 Mt        3.578 Mt   100.000 % 	 ../ResolveRLInitialization

		    8170.966 Mt    58.077 %       225 x       36.315 Mt        3.879 Mt        7.525 Mt     0.092 % 	 ./Run

		    7456.616 Mt    91.257 %       834 x        8.941 Mt        0.267 Mt     2626.525 Mt    35.224 % 	 ../Update

		    1628.006 Mt    21.833 %         2 x      814.003 Mt        0.000 Mt       45.653 Mt     2.804 % 	 .../ScenePrep

		      32.103 Mt     1.972 %         2 x       16.052 Mt        0.000 Mt       32.103 Mt   100.000 % 	 ..../SceneDesc-CreateScene

		       4.458 Mt     0.274 %         2 x        2.229 Mt        0.000 Mt        0.002 Mt     0.054 % 	 ..../ShadowMapRLPrep

		       4.456 Mt    99.946 %         2 x        2.228 Mt        0.000 Mt        1.777 Mt    39.878 % 	 ...../PrepareForRendering

		       2.679 Mt    60.122 %         2 x        1.339 Mt        0.000 Mt        2.679 Mt   100.000 % 	 ....../PrepareDrawBundle

		      10.403 Mt     0.639 %         2 x        5.202 Mt        0.000 Mt        0.002 Mt     0.022 % 	 ..../DeferredRLPrep

		      10.401 Mt    99.978 %         2 x        5.201 Mt        0.000 Mt        4.401 Mt    42.314 % 	 ...../PrepareForRendering

		       0.697 Mt     6.705 %         2 x        0.349 Mt        0.000 Mt        0.697 Mt   100.000 % 	 ....../PrepareMaterials

		       0.777 Mt     7.472 %         2 x        0.389 Mt        0.000 Mt        0.777 Mt   100.000 % 	 ....../BuildMaterialCache

		       4.525 Mt    43.508 %         2 x        2.263 Mt        0.000 Mt        4.525 Mt   100.000 % 	 ....../PrepareDrawBundle

		    1011.165 Mt    62.111 %         2 x      505.583 Mt        0.000 Mt      142.112 Mt    14.054 % 	 ..../RayTracingRLPrep

		      82.809 Mt     8.189 %         2 x       41.404 Mt        0.000 Mt       82.809 Mt   100.000 % 	 ...../PrepareAccelerationStructures

		       0.702 Mt     0.069 %         2 x        0.351 Mt        0.000 Mt        0.702 Mt   100.000 % 	 ...../PrepareCache

		      18.226 Mt     1.802 %         2 x        9.113 Mt        0.000 Mt       18.226 Mt   100.000 % 	 ...../PipelineBuild

		       2.298 Mt     0.227 %         2 x        1.149 Mt        0.000 Mt        2.298 Mt   100.000 % 	 ...../BuildCache

		     755.535 Mt    74.719 %         2 x      377.768 Mt        0.000 Mt        0.002 Mt     0.000 % 	 ...../BuildAccelerationStructures

		     755.534 Mt   100.000 %         2 x      377.767 Mt        0.000 Mt        6.638 Mt     0.879 % 	 ....../AccelerationStructureBuild

		     747.707 Mt    98.964 %         2 x      373.854 Mt        0.000 Mt      747.707 Mt   100.000 % 	 ......./BuildBottomLevelAS

		       1.188 Mt     0.157 %         2 x        0.594 Mt        0.000 Mt        1.188 Mt   100.000 % 	 ......./BuildTopLevelAS

		       9.484 Mt     0.938 %         2 x        4.742 Mt        0.000 Mt        9.484 Mt   100.000 % 	 ...../BuildShaderTables

		     524.223 Mt    32.200 %         2 x      262.111 Mt        0.000 Mt      524.223 Mt   100.000 % 	 ..../GpuSidePrep

		    3202.085 Mt    42.943 %      3438 x        0.931 Mt        0.000 Mt     1133.666 Mt    35.404 % 	 .../AccelerationStructureBuild

		     763.466 Mt    23.843 %      3438 x        0.222 Mt        0.000 Mt      763.466 Mt   100.000 % 	 ..../BuildBottomLevelAS

		    1304.953 Mt    40.753 %      3438 x        0.380 Mt        0.000 Mt     1304.953 Mt   100.000 % 	 ..../BuildTopLevelAS

		     706.825 Mt     8.650 %       225 x        3.141 Mt        3.609 Mt        0.157 Mt     0.022 % 	 ../Render

		     706.668 Mt    99.978 %       225 x        3.141 Mt        3.608 Mt       19.812 Mt     2.804 % 	 .../Render

		     657.896 Mt    93.098 %       225 x        2.924 Mt        3.389 Mt      657.896 Mt   100.000 % 	 ..../Present

		      28.960 Mt     4.098 %       212 x        0.137 Mt        0.130 Mt       28.702 Mt    99.107 % 	 ..../RayTracing

		       0.148 Mt     0.513 %       212 x        0.001 Mt        0.000 Mt        0.148 Mt   100.000 % 	 ...../DeferredRLPrep

		       0.110 Mt     0.380 %       212 x        0.001 Mt        0.001 Mt        0.110 Mt   100.000 % 	 ...../RayTracingRLPrep

		       0.001 Mt     0.000 %         1 x        0.001 Mt        0.001 Mt        0.001 Mt   100.000 % 	 ./Destroy

	WindowThread:

		   14067.440 Mt   100.000 %         1 x    14067.440 Mt    14067.440 Mt    14067.440 Mt   100.000 % 	 /

	GpuThread:

		    1418.180 Mt   100.000 %      3661 x        0.387 Mt        2.413 Mt       11.631 Mt     0.820 % 	 /

		       1.543 Mt     0.109 %       225 x        0.007 Mt        0.001 Mt        1.543 Mt   100.000 % 	 ./Present

		     515.231 Mt    36.330 %         2 x      257.615 Mt        0.000 Mt        0.007 Mt     0.001 % 	 ./ScenePrep

		     515.223 Mt    99.999 %         2 x      257.612 Mt        0.000 Mt        0.003 Mt     0.001 % 	 ../AccelerationStructureBuild

		     514.711 Mt    99.900 %         2 x      257.355 Mt        0.000 Mt      514.711 Mt   100.000 % 	 .../BuildBottomLevelASGpu

		       0.510 Mt     0.099 %         2 x        0.255 Mt        0.000 Mt        0.510 Mt   100.000 % 	 .../BuildTopLevelASGpu

		     372.694 Mt    26.280 %      3438 x        0.108 Mt        0.000 Mt        3.845 Mt     1.032 % 	 ./AccelerationStructureBuild

		     148.780 Mt    39.920 %      3438 x        0.043 Mt        0.000 Mt      148.780 Mt   100.000 % 	 ../BuildBottomLevelASGpu

		     220.069 Mt    59.048 %      3438 x        0.064 Mt        0.000 Mt      220.069 Mt   100.000 % 	 ../BuildTopLevelASGpu

		     517.081 Mt    36.461 %       212 x        2.439 Mt        2.411 Mt        0.840 Mt     0.162 % 	 ./RayTracingGpu

		      50.924 Mt     9.848 %       212 x        0.240 Mt        0.232 Mt       50.924 Mt   100.000 % 	 ../DeferredRLGpu

		     275.700 Mt    53.319 %       212 x        1.300 Mt        1.406 Mt      275.700 Mt   100.000 % 	 ../RayTracingRLGpu

		     189.617 Mt    36.671 %       212 x        0.894 Mt        0.768 Mt      189.617 Mt   100.000 % 	 ../ResolveRLGpu


	============================


