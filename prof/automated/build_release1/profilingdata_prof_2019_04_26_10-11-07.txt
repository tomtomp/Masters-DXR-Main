Configuration: 
Command line parameters  = 
Window width             = 1186
Window height            = 1219
Window title             = Direct3D App
Back-buffer count        = 3
Fill Triangles           = enabled
Triangles Font Clock-Wise= disabled
Fast Build AS            = disabled
Target FPS               = 0
Target UPS               = 60
Tearing                  = enabled
VSync                    = disabled
GUI rendering            = disabled
Debug layer              = enabled
GPU validation           = disabled
GPU profiling            = enabled
Render mode              = Ray Tracing
Loaded scene file        = 
Using testing scene      = disabled
BLAS duplication         = disabled
Rays per pixel           = 14
Quality preset           = High
Rasterization Quality    : 
  AO                     = enabled
  AO kernel size         = 4
  Shadows                = enabled
  Shadow map resolution  = 4096
  Shadow PCF kernel size = 4
  Reflections            = enabled
Ray Tracing Quality      : 
  AO                     = enabled
  AO rays                = 4
  AO kernel size         = 4
  Shadows                = enabled
  Shadow rays            = 4
  Shadow kernel size     = 4
  Reflections            = enabled

ProfilingAggregator: 
Render	,	5.0141	,	5.6216	,	7.8102	,	5.9602	,	5.8175	,	5.9868	,	6.0337	,	5.9799	,	5.4783	,	4.9753
Update	,	0.0057	,	0.0064	,	0.0451	,	0.0402	,	0.0059	,	0.0062	,	0.0417	,	0.0058	,	0.0471	,	0.0433
RayTracing	,	2.3885	,	2.0114	,	4.0221	,	2.2762	,	2.1433	,	2.2775	,	2.3035	,	2.3805	,	2.3426	,	1.9892
RayTracingGpu	,	0.63264	,	2.21715	,	2.19686	,	2.20026	,	2.2136	,	2.19709	,	2.21491	,	2.02518	,	1.5303	,	1.51952
DeferredRLGpu	,	0.03056	,	0.189248	,	0.189152	,	0.187584	,	0.192096	,	0.188832	,	0.190048	,	0.29568	,	0.155072	,	0.156512
RayTracingRLGpu	,	0.122112	,	1.63792	,	1.6201	,	1.62432	,	1.63283	,	1.62109	,	1.63376	,	1.36131	,	0.99776	,	0.993472
ResolveRLGpu	,	0.476224	,	0.387552	,	0.386176	,	0.38608	,	0.38752	,	0.384544	,	0.389568	,	0.365888	,	0.376288	,	0.367072
Index	,	0	,	1	,	2	,	3	,	4	,	5	,	6	,	7	,	8	,	9

ProfilingBuildAggregator: 
BuildBottomLevelAS	,	16.0827
BuildBottomLevelASGpu	,	12.8563
BuildTopLevelAS	,	0.4377
BuildTopLevelASGpu	,	0.18336
Triangles	,	281182
Meshes	,	52
BottomLevels	,	43
TopLevelSize	,	2752
BottomLevelsSize	,	15869344
Index	,	0

FpsAggregator: 
FPS	,	2	,	54	,	59	,	56	,	57	,	58	,	61	,	60	,	60	,	59
UPS	,	109	,	84	,	61	,	60	,	62	,	60	,	60	,	61	,	60	,	61
FrameTime	,	615.737	,	18.6273	,	17.1953	,	17.9853	,	17.7787	,	17.2483	,	16.6544	,	16.7007	,	16.7419	,	17.1823
GigaRays/s	,	0.0305128	,	1.00862	,	1.09261	,	1.04462	,	1.05676	,	1.08926	,	1.1281	,	1.12498	,	1.1222	,	1.09344
Index	,	0	,	1	,	2	,	3	,	4	,	5	,	6	,	7	,	8	,	9


AccelerationStructure: 
Ray tracing acceleration structure building statistics: 
	Bottom Level Acceleration Structure: 
		Maximum [B]: 2940416
		Minimum [B]: 6256
		Currently Allocated [B]: 15869344
		Currently Created [num]: 43
		Current Average [B]: 369054
		Maximum Triangles [num]: 52674
		Minimum Triangles [num]: 27
		Total Triangles [num]: 281182
		Maximum Meshes [num]: 2
		Minimum Meshes [num]: 1
		Total Meshes [num]: 52
	Top Level Acceleration Structure: 
		Maximum [B]: 2752
		Minimum [B]: 2752
		Currently Allocated [B]: 2752
		Total Allocated [B]: 2752
		Total Created [num]: 1
		Average Allocated [B]: 2752
		Maximum Instances [num]: 43
		Minimum Instances [num]: 43
		Total Instances [num]: 43
	Scratch Buffer: 
		Maximum [B]: 2344448
		Minimum [B]: 2344448
		Currently Allocated [B]: 2344448
		Total Allocated [B]: 2344448
		Total Created [num]: 1
		Average Allocated [B]: 2344448
ProfilingStack: 
[PROF] Profiling statistics: 
	Scopes entered : 7199
	Scopes exited : 7199
	Overhead per scope [ticks] : 162.7
	Threads entered : 3
	Threads exited : 0
	============================
		         Sum[Mt]   Parent[%]  Samples[num]       Avg[Mt]   Last/Curr[Mt]        Self[Mt]     Self[%] 	 Name

	main:

		   12120.898 Mt   100.000 %         1 x    12120.899 Mt    12120.898 Mt     7764.626 Mt    64.060 % 	 /

		     187.615 Mt     1.548 %         1 x      187.615 Mt        0.000 Mt        0.992 Mt     0.529 % 	 ./Initialize

		       2.574 Mt     1.372 %         1 x        2.574 Mt        0.000 Mt        2.574 Mt   100.000 % 	 ../TexturedRLInitialization

		       1.448 Mt     0.772 %         1 x        1.448 Mt        0.000 Mt        1.448 Mt   100.000 % 	 ../ShadowMapRLInitialization

		       6.081 Mt     3.241 %         1 x        6.081 Mt        0.000 Mt        6.081 Mt   100.000 % 	 ../AmbientOcclusionRLInitialization

		       4.229 Mt     2.254 %         1 x        4.229 Mt        0.000 Mt        4.229 Mt   100.000 % 	 ../RasterResolveRLInitialization

		       3.652 Mt     1.946 %         1 x        3.652 Mt        0.000 Mt        3.652 Mt   100.000 % 	 ../DeferredRLInitialization

		     163.828 Mt    87.322 %         1 x      163.828 Mt        0.000 Mt        1.390 Mt     0.848 % 	 ../RayTracingRLInitialization

		     162.439 Mt    99.152 %         1 x      162.439 Mt        0.000 Mt      162.439 Mt   100.000 % 	 .../PipelineBuild

		       4.811 Mt     2.564 %         1 x        4.811 Mt        0.000 Mt        4.811 Mt   100.000 % 	 ../ResolveRLInitialization

		    4168.657 Mt    34.392 %       554 x        7.525 Mt        5.848 Mt       14.670 Mt     0.352 % 	 ./Run

		     791.257 Mt    18.981 %       706 x        1.121 Mt        0.042 Mt      173.570 Mt    21.936 % 	 ../Update

		     554.368 Mt    70.062 %         1 x      554.368 Mt        0.000 Mt      548.920 Mt    99.017 % 	 .../ScenePrep

		       5.448 Mt     0.983 %         1 x        5.448 Mt        0.000 Mt        5.448 Mt   100.000 % 	 ..../SceneDesc-CreateScene

		      63.319 Mt     8.002 %        62 x        1.021 Mt        0.000 Mt       21.959 Mt    34.679 % 	 .../AccelerationStructureBuild

		      17.074 Mt    26.965 %        62 x        0.275 Mt        0.000 Mt       17.074 Mt   100.000 % 	 ..../BuildBottomLevelAS

		      24.287 Mt    38.356 %        62 x        0.392 Mt        0.000 Mt       24.287 Mt   100.000 % 	 ..../BuildTopLevelAS

		    3362.730 Mt    80.667 %       554 x        6.070 Mt        5.803 Mt        0.754 Mt     0.022 % 	 ../Render

		    3361.976 Mt    99.978 %       554 x        6.069 Mt        5.801 Mt     2053.653 Mt    61.085 % 	 .../Render

		    1308.323 Mt    38.915 %       554 x        2.362 Mt        2.713 Mt     1283.136 Mt    98.075 % 	 ..../RayTracing

		       3.320 Mt     0.254 %       554 x        0.006 Mt        0.001 Mt        0.567 Mt    17.070 % 	 ...../DeferredRLPrep

		       2.753 Mt    82.930 %         1 x        2.753 Mt        0.000 Mt        1.431 Mt    51.983 % 	 ....../PrepareForRendering

		       0.064 Mt     2.306 %         1 x        0.064 Mt        0.000 Mt        0.064 Mt   100.000 % 	 ......./PrepareMaterials

		       0.257 Mt     9.345 %         1 x        0.257 Mt        0.000 Mt        0.257 Mt   100.000 % 	 ......./BuildMaterialCache

		       1.001 Mt    36.365 %         1 x        1.001 Mt        0.000 Mt        1.001 Mt   100.000 % 	 ......./PrepareDrawBundle

		      21.867 Mt     1.671 %       554 x        0.039 Mt        0.001 Mt        2.569 Mt    11.746 % 	 ...../RayTracingRLPrep

		       1.320 Mt     6.038 %         1 x        1.320 Mt        0.000 Mt        1.320 Mt   100.000 % 	 ....../PrepareAccelerationStructures

		       0.048 Mt     0.218 %         1 x        0.048 Mt        0.000 Mt        0.048 Mt   100.000 % 	 ....../PrepareCache

		       0.481 Mt     2.201 %         1 x        0.481 Mt        0.000 Mt        0.481 Mt   100.000 % 	 ....../BuildCache

		      17.050 Mt    77.968 %         1 x       17.050 Mt        0.000 Mt        0.001 Mt     0.006 % 	 ....../BuildAccelerationStructures

		      17.049 Mt    99.994 %         1 x       17.049 Mt        0.000 Mt        0.528 Mt     3.098 % 	 ......./AccelerationStructureBuild

		      16.083 Mt    94.335 %         1 x       16.083 Mt        0.000 Mt       16.083 Mt   100.000 % 	 ......../BuildBottomLevelAS

		       0.438 Mt     2.567 %         1 x        0.438 Mt        0.000 Mt        0.438 Mt   100.000 % 	 ......../BuildTopLevelAS

		       0.400 Mt     1.829 %         1 x        0.400 Mt        0.000 Mt        0.400 Mt   100.000 % 	 ....../BuildShaderTables

		       0.001 Mt     0.000 %         1 x        0.001 Mt        0.001 Mt        0.001 Mt   100.000 % 	 ./Destroy

	WindowThread:

		   12119.093 Mt   100.000 %         1 x    12119.093 Mt    12119.093 Mt    12119.093 Mt   100.000 % 	 /

	GpuThread:

		    1220.240 Mt   100.000 %       615 x        1.984 Mt        1.524 Mt        0.022 Mt     0.002 % 	 /

		      29.713 Mt     2.435 %        62 x        0.479 Mt        0.000 Mt        0.077 Mt     0.258 % 	 ./AccelerationStructureBuild

		      24.545 Mt    82.609 %        62 x        0.396 Mt        0.000 Mt       24.545 Mt   100.000 % 	 ../BuildBottomLevelASGpu

		       5.091 Mt    17.133 %        62 x        0.082 Mt        0.000 Mt        5.091 Mt   100.000 % 	 ../BuildTopLevelASGpu

		    1187.582 Mt    97.324 %       554 x        2.144 Mt        1.522 Mt        1.104 Mt     0.093 % 	 ./RayTracingGpu

		     110.104 Mt     9.271 %       554 x        0.199 Mt        0.157 Mt      110.104 Mt   100.000 % 	 ../DeferredRLGpu

		      13.041 Mt     1.098 %         1 x       13.041 Mt        0.000 Mt        0.001 Mt     0.010 % 	 ../AccelerationStructureBuild

		      12.856 Mt    98.584 %         1 x       12.856 Mt        0.000 Mt       12.856 Mt   100.000 % 	 .../BuildBottomLevelASGpu

		       0.183 Mt     1.406 %         1 x        0.183 Mt        0.000 Mt        0.183 Mt   100.000 % 	 .../BuildTopLevelASGpu

		     848.052 Mt    71.410 %       554 x        1.531 Mt        0.993 Mt      848.052 Mt   100.000 % 	 ../RayTracingRLGpu

		     215.281 Mt    18.128 %       554 x        0.389 Mt        0.369 Mt      215.281 Mt   100.000 % 	 ../ResolveRLGpu

		       2.923 Mt     0.240 %       554 x        0.005 Mt        0.002 Mt        2.923 Mt   100.000 % 	 ./Present


	============================


