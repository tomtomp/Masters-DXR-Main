Configuration: 
Command line parameters  = 
Build type               = Release
Device name              = NVIDIA GeForce RTX 2080 Ti
Device memory MB         = 11049
Ray Tracing backend      = Hardware
Window width             = 1161
Window height            = 1219
Window title             = Direct3D App
Back-buffer count        = 3
Fill Triangles           = enabled
Triangles Font Clock-Wise= disabled
Fast Build AS            = disabled
Target FPS               = 0
Target UPS               = 60
Tearing                  = enabled
VSync                    = disabled
GUI rendering            = disabled
Debug layer              = disabled
GPU validation           = disabled
GPU profiling            = enabled
Render mode              = Ray Tracing
Loaded scene file        = 
Using testing scene      = disabled
BLAS duplication         = disabled
Rays per pixel           = 8
Quality preset           = Medium
Rasterization Quality    : 
  AO                     = enabled
  AO kernel size         = 4
  Shadows                = enabled
  Shadow map resolution  = 4096
  Shadow PCF kernel size = 4
  Reflections            = disabled
Ray Tracing Quality      : 
  AO                     = enabled
  AO rays                = 4
  AO kernel size         = 4
  Shadows                = enabled
  Shadow rays            = 4
  Shadow kernel size     = 4
  Reflections            = disabled

ProfilingAggregator: 
Render	,	3.8291	,	3.3976
Update	,	0.0762	,	0.0786
RayTracing	,	0.137	,	0.1294
RayTracingGpu	,	3.04982	,	2.86115
DeferredRLGpu	,	0.45424	,	0.452672
RayTracingRLGpu	,	1.79731	,	1.61088
ResolveRLGpu	,	0.79296	,	0.792416
Index	,	0	,	1

ProfilingBuildAggregator: 
BuildBottomLevelAS	,	0.2864
BuildBottomLevelASGpu	,	0.43072
BuildTopLevelAS	,	0.3743
BuildTopLevelASGpu	,	0.078784
Triangles	,	32515
Meshes	,	78
BottomLevels	,	60
TopLevelSize	,	3840
BottomLevelsSize	,	2524832
Index	,	0

FpsAggregator: 
FPS	,	42	,	57
UPS	,	169	,	60
FrameTime	,	23.8875	,	17.5509
GigaRays/s	,	0.430247	,	0.585582
Index	,	0	,	1


AccelerationStructure: 
Ray tracing acceleration structure building statistics: 
	Bottom Level Acceleration Structure: 
		Maximum [B]: 583552
		Minimum [B]: 5216
		Currently Allocated [B]: 2524832
		Currently Created [num]: 60
		Current Average [B]: 42080
		Maximum Triangles [num]: 8232
		Minimum Triangles [num]: 24
		Total Triangles [num]: 32515
		Maximum Meshes [num]: 3
		Minimum Meshes [num]: 1
		Total Meshes [num]: 78
	Top Level Acceleration Structure: 
		Maximum [B]: 3840
		Minimum [B]: 3840
		Currently Allocated [B]: 3840
		Total Allocated [B]: 3840
		Total Created [num]: 1
		Average Allocated [B]: 3840
		Maximum Instances [num]: 60
		Minimum Instances [num]: 60
		Total Instances [num]: 60
	Scratch Buffer: 
		Maximum [B]: 471552
		Minimum [B]: 471552
		Currently Allocated [B]: 471552
		Total Allocated [B]: 471552
		Total Created [num]: 1
		Average Allocated [B]: 471552
ProfilingStack: 
[PROF] Profiling statistics: 
	Scopes entered : 2443
	Scopes exited : 2443
	Overhead per scope [ticks] : 162.9
	Threads entered : 3
	Threads exited : 0
	============================
		         Sum[Mt]   Parent[%]  Samples[num]       Avg[Mt]   Last/Curr[Mt]        Self[Mt]     Self[%] 	 Name

	main:

		    4751.588 Mt   100.000 %         1 x     4751.588 Mt     4751.588 Mt     3867.033 Mt    81.384 % 	 /

		      58.693 Mt     1.235 %         1 x       58.693 Mt        0.000 Mt        0.724 Mt     1.234 % 	 ./Initialize

		       1.421 Mt     2.421 %         1 x        1.421 Mt        0.000 Mt        1.421 Mt   100.000 % 	 ../ShadowMapRLInitialization

		       5.819 Mt     9.913 %         1 x        5.819 Mt        0.000 Mt        5.819 Mt   100.000 % 	 ../AmbientOcclusionRLInitialization

		       3.483 Mt     5.935 %         1 x        3.483 Mt        0.000 Mt        3.483 Mt   100.000 % 	 ../RasterResolveRLInitialization

		       3.918 Mt     6.675 %         1 x        3.918 Mt        0.000 Mt        3.918 Mt   100.000 % 	 ../DeferredRLInitialization

		      39.692 Mt    67.626 %         1 x       39.692 Mt        0.000 Mt        1.800 Mt     4.534 % 	 ../RayTracingRLInitialization

		      37.892 Mt    95.466 %         1 x       37.892 Mt        0.000 Mt       37.892 Mt   100.000 % 	 .../PipelineBuild

		       3.636 Mt     6.196 %         1 x        3.636 Mt        0.000 Mt        3.636 Mt   100.000 % 	 ../ResolveRLInitialization

		     825.861 Mt    17.381 %       135 x        6.117 Mt        3.440 Mt        3.711 Mt     0.449 % 	 ./Run

		     318.789 Mt    38.601 %       275 x        1.159 Mt        0.075 Mt      128.529 Mt    40.318 % 	 ../Update

		      95.536 Mt    29.968 %         1 x       95.536 Mt        0.000 Mt       29.591 Mt    30.974 % 	 .../ScenePrep

		       5.392 Mt     5.644 %         1 x        5.392 Mt        0.000 Mt        5.392 Mt   100.000 % 	 ..../SceneDesc-CreateScene

		       1.008 Mt     1.056 %         1 x        1.008 Mt        0.000 Mt        0.001 Mt     0.099 % 	 ..../ShadowMapRLPrep

		       1.007 Mt    99.901 %         1 x        1.007 Mt        0.000 Mt        0.939 Mt    93.200 % 	 ...../PrepareForRendering

		       0.069 Mt     6.800 %         1 x        0.069 Mt        0.000 Mt        0.069 Mt   100.000 % 	 ....../PrepareDrawBundle

		       3.535 Mt     3.700 %         1 x        3.535 Mt        0.000 Mt        0.001 Mt     0.031 % 	 ..../DeferredRLPrep

		       3.534 Mt    99.969 %         1 x        3.534 Mt        0.000 Mt        3.146 Mt    89.034 % 	 ...../PrepareForRendering

		       0.065 Mt     1.834 %         1 x        0.065 Mt        0.000 Mt        0.065 Mt   100.000 % 	 ....../PrepareMaterials

		       0.200 Mt     5.654 %         1 x        0.200 Mt        0.000 Mt        0.200 Mt   100.000 % 	 ....../BuildMaterialCache

		       0.123 Mt     3.478 %         1 x        0.123 Mt        0.000 Mt        0.123 Mt   100.000 % 	 ....../PrepareDrawBundle

		      30.135 Mt    31.543 %         1 x       30.135 Mt        0.000 Mt        1.674 Mt     5.557 % 	 ..../RayTracingRLPrep

		       1.558 Mt     5.170 %         1 x        1.558 Mt        0.000 Mt        1.558 Mt   100.000 % 	 ...../PrepareAccelerationStructures

		       0.054 Mt     0.179 %         1 x        0.054 Mt        0.000 Mt        0.054 Mt   100.000 % 	 ...../PrepareCache

		       8.892 Mt    29.508 %         1 x        8.892 Mt        0.000 Mt        8.892 Mt   100.000 % 	 ...../PipelineBuild

		       0.957 Mt     3.174 %         1 x        0.957 Mt        0.000 Mt        0.957 Mt   100.000 % 	 ...../BuildCache

		      16.579 Mt    55.017 %         1 x       16.579 Mt        0.000 Mt        0.001 Mt     0.008 % 	 ...../BuildAccelerationStructures

		      16.578 Mt    99.992 %         1 x       16.578 Mt        0.000 Mt        0.549 Mt     3.315 % 	 ....../AccelerationStructureBuild

		      15.645 Mt    94.375 %         1 x       15.645 Mt        0.000 Mt       15.645 Mt   100.000 % 	 ......./BuildBottomLevelAS

		       0.383 Mt     2.310 %         1 x        0.383 Mt        0.000 Mt        0.383 Mt   100.000 % 	 ......./BuildTopLevelAS

		       0.420 Mt     1.395 %         1 x        0.420 Mt        0.000 Mt        0.420 Mt   100.000 % 	 ...../BuildShaderTables

		      25.874 Mt    27.083 %         1 x       25.874 Mt        0.000 Mt       25.874 Mt   100.000 % 	 ..../GpuSidePrep

		      94.725 Mt    29.714 %        88 x        1.076 Mt        0.000 Mt       33.577 Mt    35.447 % 	 .../AccelerationStructureBuild

		      24.560 Mt    25.928 %        88 x        0.279 Mt        0.000 Mt       24.560 Mt   100.000 % 	 ..../BuildBottomLevelAS

		      36.588 Mt    38.625 %        88 x        0.416 Mt        0.000 Mt       36.588 Mt   100.000 % 	 ..../BuildTopLevelAS

		     503.361 Mt    60.950 %       135 x        3.729 Mt        3.363 Mt        0.101 Mt     0.020 % 	 ../Render

		     503.260 Mt    99.980 %       135 x        3.728 Mt        3.362 Mt       12.426 Mt     2.469 % 	 .../Render

		     472.212 Mt    93.831 %       135 x        3.498 Mt        3.158 Mt      472.212 Mt   100.000 % 	 ..../Present

		      18.622 Mt     3.700 %       133 x        0.140 Mt        0.122 Mt       18.480 Mt    99.239 % 	 ..../RayTracing

		       0.069 Mt     0.373 %       133 x        0.001 Mt        0.000 Mt        0.069 Mt   100.000 % 	 ...../DeferredRLPrep

		       0.072 Mt     0.389 %       133 x        0.001 Mt        0.001 Mt        0.072 Mt   100.000 % 	 ...../RayTracingRLPrep

		       0.001 Mt     0.000 %         1 x        0.001 Mt        0.001 Mt        0.001 Mt   100.000 % 	 ./Destroy

	WindowThread:

		    4749.987 Mt   100.000 %         1 x     4749.987 Mt     4749.987 Mt     4749.987 Mt   100.000 % 	 /

	GpuThread:

		     460.551 Mt   100.000 %       222 x        2.075 Mt        2.858 Mt        6.032 Mt     1.310 % 	 /

		       0.571 Mt     0.124 %       135 x        0.004 Mt        0.002 Mt        0.571 Mt   100.000 % 	 ./Present

		      18.736 Mt     4.068 %         1 x       18.736 Mt        0.000 Mt        0.008 Mt     0.041 % 	 ./ScenePrep

		      18.729 Mt    99.959 %         1 x       18.729 Mt        0.000 Mt        0.002 Mt     0.008 % 	 ../AccelerationStructureBuild

		      18.559 Mt    99.096 %         1 x       18.559 Mt        0.000 Mt       18.559 Mt   100.000 % 	 .../BuildBottomLevelASGpu

		       0.168 Mt     0.896 %         1 x        0.168 Mt        0.000 Mt        0.168 Mt   100.000 % 	 .../BuildTopLevelASGpu

		      26.394 Mt     5.731 %        88 x        0.300 Mt        0.000 Mt        0.122 Mt     0.461 % 	 ./AccelerationStructureBuild

		      19.382 Mt    73.435 %        88 x        0.220 Mt        0.000 Mt       19.382 Mt   100.000 % 	 ../BuildBottomLevelASGpu

		       6.890 Mt    26.104 %        88 x        0.078 Mt        0.000 Mt        6.890 Mt   100.000 % 	 ../BuildTopLevelASGpu

		     408.818 Mt    88.767 %       133 x        3.074 Mt        2.856 Mt        0.552 Mt     0.135 % 	 ./RayTracingGpu

		      61.281 Mt    14.990 %       133 x        0.461 Mt        0.450 Mt       61.281 Mt   100.000 % 	 ../DeferredRLGpu

		     236.301 Mt    57.801 %       133 x        1.777 Mt        1.610 Mt      236.301 Mt   100.000 % 	 ../RayTracingRLGpu

		     110.684 Mt    27.074 %       133 x        0.832 Mt        0.792 Mt      110.684 Mt   100.000 % 	 ../ResolveRLGpu


	============================


