Configuration: 
Command line parameters  = 
Window width             = 1024
Window height            = 768
Window title             = Direct3D App
Back-buffer count        = 3
Fill Triangles           = enabled
Triangles Font Clock-Wise= disabled
Fast Build AS            = disabled
Target FPS               = 0
Target UPS               = 60
Tearing                  = enabled
VSync                    = disabled
GUI rendering            = disabled
Debug layer              = enabled
GPU validation           = disabled
GPU profiling            = enabled
Render mode              = Ray Tracing
Loaded scene file        = 
Using testing scene      = disabled
BLAS duplication         = disabled
Rays per pixel           = 14
Quality preset           = Medium
Rasterization Quality    : 
  AO                     = enabled
  AO kernel size         = 4
  Shadows                = enabled
  Shadow map resolution  = 4096
  Shadow PCF kernel size = 4
  Reflections            = disabled
Ray Tracing Quality      : 
  AO                     = enabled
  AO rays                = 4
  AO kernel size         = 4
  Shadows                = enabled
  Shadow rays            = 4
  Shadow kernel size     = 4
  Reflections            = disabled

ProfilingAggregator: 
Update	,	377.34	,	379.253	,	381.311	,	383.375	,	385.753	,	387.721	,	389.497	,	391.943	,	393.661	,	395.32	,	397.28	,	398.905	,	400.55	,	402.252	,	403.956	,	405.612	,	407.867	,	410.173	,	412.075	,	414.046	,	416.023	,	418.078	,	419.882	,	421.593	,	423.427	,	425.434	,	427.389	,	429.944	,	432.128	,	434.068	,	436.333	,	438.41	,	440.271	,	442.482	,	444.567	,	446.401	,	448.245	,	450.056	,	451.845	,	453.779	,	455.917	,	458.263	,	460.199	,	462.432	,	464.58	,	466.955	,	469.617	,	471.826	,	474.165	,	477.707	,	480.754	,	482.984	,	485.374	,	487.545	,	489.855	,	493.099	,	495.602	,	498.06	,	500.114	,	502.103	,	503.947
Index	,	0	,	1	,	2	,	3	,	4	,	5	,	6	,	7	,	8	,	9	,	10	,	11	,	12	,	13	,	14	,	15	,	16	,	17	,	18	,	19	,	20	,	21	,	22	,	23	,	24	,	25	,	26	,	27	,	28	,	29	,	30	,	31	,	32	,	33	,	34	,	35	,	36	,	37	,	38	,	39	,	40	,	41	,	42	,	43	,	44	,	45	,	46	,	47	,	48	,	49	,	50	,	51	,	52	,	53	,	54	,	55	,	56	,	57	,	58	,	59	,	60

ProfilingBuildAggregator: 
BuildBottomLevelAS	,	0.2469	,	0.2982	,	0.2609	,	0.2853	,	0.3483	,	0.275	,	0.221	,	0.3418	,	0.2209	,	0.2115	,	0.2894	,	0.2152	,	0.216	,	0.2194	,	0.2177	,	0.2177	,	0.3697	,	0.3251	,	0.2365	,	0.2775	,	0.2716	,	0.2475	,	0.2225	,	0.2484	,	0.2302	,	0.2576	,	0.2406	,	0.2909	,	0.2577	,	0.2448	,	0.2954	,	0.2349	,	0.2187	,	0.2392	,	0.252	,	0.2344	,	0.2195	,	0.2395	,	0.2377	,	0.267	,	0.2327	,	0.3325	,	0.2998	,	0.2755	,	0.2524	,	0.3324	,	0.3729	,	0.2491	,	0.2552	,	0.4696	,	0.438	,	0.2648	,	0.3142	,	0.2859	,	0.2899	,	0.4044	,	0.3054	,	0.3624	,	0.2767	,	0.2822	,	0.2281
BuildBottomLevelASGpu	,	0.087936	,	0.231136	,	0.323264	,	0.398528	,	0.33088	,	0.239328	,	0.235936	,	0.431456	,	0.1672	,	0.172928	,	0.140736	,	0.096064	,	0.138112	,	0.097632	,	0.164	,	0.139072	,	0.395936	,	0.445216	,	0.242784	,	0.254752	,	0.269312	,	0.233024	,	0.228992	,	0.128736	,	0.256736	,	0.301184	,	0.173856	,	0.126848	,	0.100544	,	0.105856	,	0.13456	,	0.126208	,	0.116128	,	0.1392	,	0.161408	,	0.141888	,	0.169696	,	0.1	,	0.106336	,	0.11584	,	0.122944	,	0.199744	,	0.20816	,	0.429824	,	0.513376	,	0.44464	,	0.705248	,	0.473728	,	0.581184	,	0.544128	,	0.60464	,	0.468256	,	0.634336	,	0.548576	,	0.629056	,	0.385408	,	0.236192	,	0.35344	,	0.386912	,	0.378336	,	0.283424
BuildTopLevelAS	,	0.3882	,	0.3554	,	0.3551	,	0.362	,	0.4322	,	0.4463	,	0.3663	,	0.3692	,	0.3561	,	0.3578	,	0.3982	,	0.3565	,	0.3594	,	0.3894	,	0.3656	,	0.362	,	0.4115	,	0.3859	,	0.3826	,	0.3892	,	0.389	,	0.3816	,	0.3752	,	0.3628	,	0.3634	,	0.354	,	0.4143	,	0.5153	,	0.4165	,	0.3993	,	0.4461	,	0.4117	,	0.433	,	0.5105	,	0.3926	,	0.3972	,	0.4332	,	0.3999	,	0.3929	,	0.4194	,	0.3668	,	0.5549	,	0.3717	,	0.4051	,	0.4207	,	0.358	,	0.4683	,	0.3651	,	0.4193	,	0.9644	,	0.5389	,	0.3777	,	0.3786	,	0.3617	,	0.3612	,	0.5322	,	0.4396	,	0.4233	,	0.3693	,	0.3818	,	0.3653
BuildTopLevelASGpu	,	0.055392	,	0.054848	,	0.055328	,	0.058624	,	0.054624	,	0.054656	,	0.055392	,	0.057152	,	0.054784	,	0.054656	,	0.054656	,	0.054528	,	0.055328	,	0.05536	,	0.055648	,	0.054656	,	0.055808	,	0.061472	,	0.055584	,	0.05552	,	0.05584	,	0.055744	,	0.055424	,	0.055328	,	0.054912	,	0.054752	,	0.055456	,	0.059264	,	0.05456	,	0.054656	,	0.060224	,	0.059808	,	0.055456	,	0.059712	,	0.054528	,	0.0544	,	0.055392	,	0.05456	,	0.055904	,	0.055488	,	0.059008	,	0.05936	,	0.055424	,	0.058528	,	0.055776	,	0.056224	,	0.055936	,	0.055392	,	0.058848	,	0.060416	,	0.05568	,	0.055488	,	0.055424	,	0.055424	,	0.056192	,	0.057728	,	0.056448	,	0.060544	,	0.055584	,	0.055744	,	0.055616
Triangles	,	32	,	4120	,	15584	,	45330	,	20655	,	7167	,	5600	,	46560	,	288	,	282	,	143	,	28	,	75	,	27	,	132	,	108	,	39389	,	52674	,	1812	,	2934	,	2305	,	1290	,	1919	,	122	,	8917	,	19389	,	281	,	47	,	34	,	46	,	63	,	52	,	50	,	102	,	142	,	125	,	183	,	34	,	33	,	48	,	34	,	1658	,	1368	,	7000	,	8584	,	7794	,	37536	,	6246	,	14409	,	23123	,	23437	,	9929	,	29460	,	23399	,	29275	,	1620	,	192	,	1572	,	1362	,	1999	,	306
Meshes	,	1	,	1	,	2	,	2	,	2	,	1	,	1	,	2	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	2	,	2	,	2	,	2	,	2	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1
BottomLevels	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1
TopLevelSize	,	64	,	64	,	64	,	64	,	64	,	64	,	64	,	64	,	64	,	64	,	64	,	64	,	64	,	64	,	64	,	64	,	64	,	64	,	64	,	64	,	64	,	64	,	64	,	64	,	64	,	64	,	64	,	64	,	64	,	64	,	64	,	64	,	64	,	64	,	64	,	64	,	64	,	64	,	64	,	64	,	64	,	64	,	64	,	64	,	64	,	64	,	64	,	64	,	64	,	64	,	64	,	64	,	64	,	64	,	64	,	64	,	64	,	64	,	64	,	64	,	64
BottomLevelsSize	,	6528	,	233856	,	872832	,	2531072	,	1155712	,	403712	,	316672	,	2599424	,	20992	,	19840	,	12544	,	6256	,	8496	,	6256	,	12304	,	10752	,	2199808	,	2940416	,	105600	,	167936	,	132864	,	76032	,	111104	,	11392	,	501632	,	1085056	,	19840	,	7168	,	6800	,	7168	,	8320	,	7888	,	7888	,	10144	,	12544	,	11392	,	14976	,	6800	,	6800	,	7680	,	6800	,	97152	,	80896	,	498560	,	606720	,	555392	,	2637440	,	442240	,	1015680	,	1625856	,	1647488	,	700288	,	2071040	,	1646976	,	2056960	,	120832	,	18432	,	115712	,	100352	,	147456	,	26112
Index	,	0	,	1	,	2	,	3	,	4	,	5	,	6	,	7	,	8	,	9	,	10	,	11	,	12	,	13	,	14	,	15	,	16	,	17	,	18	,	19	,	20	,	21	,	22	,	23	,	24	,	25	,	26	,	27	,	28	,	29	,	30	,	31	,	32	,	33	,	34	,	35	,	36	,	37	,	38	,	39	,	40	,	41	,	42	,	43	,	44	,	45	,	46	,	47	,	48	,	49	,	50	,	51	,	52	,	53	,	54	,	55	,	56	,	57	,	58	,	59	,	60

FpsAggregator: 
FPS	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0
UPS	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0
FrameTime	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0
GigaRays/s	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0
Index	,	0	,	1	,	2	,	3	,	4	,	5	,	6	,	7	,	8	,	9	,	10	,	11	,	12	,	13	,	14	,	15	,	16	,	17	,	18	,	19	,	20	,	21	,	22	,	23	,	24	,	25	,	26	,	27	,	28	,	29	,	30	,	31	,	32	,	33	,	34	,	35	,	36	,	37	,	38	,	39	,	40	,	41	,	42	,	43	,	44	,	45	,	46	,	47	,	48	,	49	,	50	,	51	,	52	,	53	,	54	,	55	,	56	,	57	,	58	,	59	,	60


AccelerationStructure: 
Ray tracing acceleration structure building statistics: 
	Bottom Level Acceleration Structure: 
		Maximum [B]: 0
		Minimum [B]: 18446744073709551615
		Currently Allocated [B]: 0
		Currently Created [num]: 0
		Current Average [B]: 0
		Maximum Triangles [num]: 0
		Minimum Triangles [num]: 18446744073709551615
		Total Triangles [num]: 0
		Maximum Meshes [num]: 0
		Minimum Meshes [num]: 18446744073709551615
		Total Meshes [num]: 0
	Top Level Acceleration Structure: 
		Maximum [B]: 0
		Minimum [B]: 18446744073709551615
		Currently Allocated [B]: 0
		Total Allocated [B]: 0
		Total Created [num]: 0
		Average Allocated [B]: 0
		Maximum Instances [num]: 0
		Minimum Instances [num]: 18446744073709551615
		Total Instances [num]: 0
	Scratch Buffer: 
		Maximum [B]: 0
		Minimum [B]: 18446744073709551615
		Currently Allocated [B]: 0
		Total Allocated [B]: 0
		Total Created [num]: 0
		Average Allocated [B]: 0
ProfilingStack: 
[PROF] Profiling statistics: 
	Scopes entered : 385
	Scopes exited : 383
	Overhead per scope [ticks] : 163
	Threads entered : 3
	Threads exited : 0
	============================
		         Sum[Mt]   Parent[%]  Samples[num]       Avg[Mt]   Last/Curr[Mt]        Self[Mt]     Self[%] 	 Name

	main:

		    1669.517 Mt   100.000 %         1 x     1669.517 Mt     1669.517 Mt     1090.264 Mt    65.304 % 	 /

		      74.645 Mt     4.471 %         1 x       74.645 Mt       74.645 Mt        1.097 Mt     1.469 % 	 ./Initialize

		       3.329 Mt     4.460 %         1 x        3.329 Mt        3.329 Mt        3.329 Mt   100.000 % 	 ../TexturedRLInitialization

		       1.571 Mt     2.104 %         1 x        1.571 Mt        1.571 Mt        1.571 Mt   100.000 % 	 ../ShadowMapRLInitialization

		       6.144 Mt     8.231 %         1 x        6.144 Mt        6.144 Mt        6.144 Mt   100.000 % 	 ../AmbientOcclusionRLInitialization

		       4.998 Mt     6.696 %         1 x        4.998 Mt        4.998 Mt        4.998 Mt   100.000 % 	 ../RasterResolveRLInitialization

		       4.242 Mt     5.683 %         1 x        4.242 Mt        4.242 Mt        4.242 Mt   100.000 % 	 ../DeferredRLInitialization

		      48.078 Mt    64.410 %         1 x       48.078 Mt       48.078 Mt        1.475 Mt     3.068 % 	 ../RayTracingRLInitialization

		      46.603 Mt    96.932 %         1 x       46.603 Mt       46.603 Mt       46.603 Mt   100.000 % 	 .../PipelineBuild

		       5.185 Mt     6.946 %         1 x        5.185 Mt        5.185 Mt        5.185 Mt   100.000 % 	 ../ResolveRLInitialization

		     504.672 Mt    30.229 %         1 x      504.673 Mt      504.672 Mt        0.002 Mt     0.000 % 	 ./Run

		     504.677 Mt   100.001 %         1 x      504.677 Mt      504.677 Mt       76.983 Mt    15.254 % 	 ../Update

		     360.735 Mt    71.478 %         1 x      360.735 Mt      360.735 Mt      353.102 Mt    97.884 % 	 .../ScenePrep

		       7.633 Mt     2.116 %         1 x        7.633 Mt        7.633 Mt        7.633 Mt   100.000 % 	 ..../SceneDesc-CreateScene

		      66.959 Mt    13.268 %        62 x        1.080 Mt        0.900 Mt       23.397 Mt    34.943 % 	 .../AccelerationStructureBuild

		      17.795 Mt    26.576 %        62 x        0.287 Mt        0.228 Mt       17.795 Mt   100.000 % 	 ..../BuildBottomLevelAS

		      25.767 Mt    38.481 %        62 x        0.416 Mt        0.365 Mt       25.767 Mt   100.000 % 	 ..../BuildTopLevelAS

	WindowThread:

		    1667.359 Mt   100.000 %         1 x     1667.359 Mt     1667.359 Mt     1667.359 Mt   100.000 % 	 /

	GpuThread:

		      20.584 Mt   100.000 %        61 x        0.337 Mt        0.340 Mt       -0.144 Mt    -0.701 % 	 /

		      20.729 Mt   100.701 %        62 x        0.334 Mt        0.340 Mt        0.054 Mt     0.262 % 	 ./AccelerationStructureBuild

		      17.189 Mt    82.922 %        62 x        0.277 Mt        0.283 Mt       17.189 Mt   100.000 % 	 ../BuildBottomLevelASGpu

		       3.486 Mt    16.815 %        62 x        0.056 Mt        0.056 Mt        3.486 Mt   100.000 % 	 ../BuildTopLevelASGpu


	============================


