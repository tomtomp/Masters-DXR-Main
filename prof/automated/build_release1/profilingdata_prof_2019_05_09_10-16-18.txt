Configuration: 
Command line parameters  = 
Build type               = Release
Device name              = NVIDIA GeForce RTX 2080 Ti
Device memory MB         = 11049
Ray Tracing backend      = Hardware
Window width             = 1161
Window height            = 1219
Window title             = Direct3D App
Back-buffer count        = 3
Fill Triangles           = enabled
Triangles Font Clock-Wise= disabled
Fast Build AS            = disabled
Target FPS               = 0
Target UPS               = 60
Tearing                  = enabled
VSync                    = disabled
GUI rendering            = disabled
Debug layer              = disabled
GPU validation           = disabled
GPU profiling            = enabled
Render mode              = Ray Tracing
Loaded scene file        = 
Using testing scene      = disabled
BLAS duplication         = disabled
Rays per pixel           = 8
Quality preset           = Medium
Rasterization Quality    : 
  AO                     = enabled
  AO kernel size         = 4
  Shadows                = enabled
  Shadow map resolution  = 4096
  Shadow PCF kernel size = 4
  Reflections            = disabled
Ray Tracing Quality      : 
  AO                     = enabled
  AO rays                = 4
  AO kernel size         = 4
  Shadows                = enabled
  Shadow rays            = 4
  Shadow kernel size     = 4
  Reflections            = disabled

ProfilingAggregator: 
Render	,	2.0323	,	2.3811	,	2.7036	,	2.6785
Update	,	0.0758	,	0.2644	,	0.2686	,	0.0807
RayTracing	,	0.8246	,	0.1251	,	0.1242	,	0.1331
RayTracingGpu	,	0.692896	,	1.82829	,	2.02083	,	2.14186
DeferredRLGpu	,	0.032	,	0.223264	,	0.2304	,	0.227808
RayTracingRLGpu	,	0.048288	,	0.83584	,	1.02541	,	1.14768
ResolveRLGpu	,	0.606944	,	0.765056	,	0.761056	,	0.762816
Index	,	0	,	1	,	2	,	3

ProfilingBuildAggregator: 
BuildBottomLevelAS	,	0.2196
BuildBottomLevelASGpu	,	0.136288
BuildTopLevelAS	,	0.3414
BuildTopLevelASGpu	,	0.078816
Triangles	,	112112
Meshes	,	1702
BottomLevels	,	1686
TopLevelSize	,	107904
BottomLevelsSize	,	14168480
Index	,	0

FpsAggregator: 
FPS	,	12	,	56	,	59	,	59
UPS	,	140	,	279	,	63	,	60
FrameTime	,	364.783	,	18.6795	,	16.9625	,	16.9574
GigaRays/s	,	0.0172471	,	0.550204	,	0.605895	,	0.60608
Index	,	0	,	1	,	2	,	3


AccelerationStructure: 
Ray tracing acceleration structure building statistics: 
	Bottom Level Acceleration Structure: 
		Maximum [B]: 239488
		Minimum [B]: 2688
		Currently Allocated [B]: 14168480
		Currently Created [num]: 1686
		Current Average [B]: 8403
		Maximum Triangles [num]: 3348
		Minimum Triangles [num]: 2
		Total Triangles [num]: 112112
		Maximum Meshes [num]: 2
		Minimum Meshes [num]: 1
		Total Meshes [num]: 1702
	Top Level Acceleration Structure: 
		Maximum [B]: 107904
		Minimum [B]: 107904
		Currently Allocated [B]: 107904
		Total Allocated [B]: 107904
		Total Created [num]: 1
		Average Allocated [B]: 107904
		Maximum Instances [num]: 1686
		Minimum Instances [num]: 1686
		Total Instances [num]: 1686
	Scratch Buffer: 
		Maximum [B]: 131584
		Minimum [B]: 131584
		Currently Allocated [B]: 131584
		Total Allocated [B]: 131584
		Total Created [num]: 1
		Average Allocated [B]: 131584
ProfilingStack: 
[PROF] Profiling statistics: 
	Scopes entered : 13365
	Scopes exited : 13365
	Overhead per scope [ticks] : 161.9
	Threads entered : 3
	Threads exited : 0
	============================
		         Sum[Mt]   Parent[%]  Samples[num]       Avg[Mt]   Last/Curr[Mt]        Self[Mt]     Self[%] 	 Name

	main:

		    9753.690 Mt   100.000 %         1 x     9753.691 Mt     9753.690 Mt     5274.257 Mt    54.074 % 	 /

		      61.494 Mt     0.630 %         1 x       61.494 Mt        0.000 Mt        0.741 Mt     1.205 % 	 ./Initialize

		       1.687 Mt     2.743 %         1 x        1.687 Mt        0.000 Mt        1.687 Mt   100.000 % 	 ../ShadowMapRLInitialization

		       6.012 Mt     9.776 %         1 x        6.012 Mt        0.000 Mt        6.012 Mt   100.000 % 	 ../AmbientOcclusionRLInitialization

		       3.806 Mt     6.190 %         1 x        3.806 Mt        0.000 Mt        3.806 Mt   100.000 % 	 ../RasterResolveRLInitialization

		       3.770 Mt     6.131 %         1 x        3.770 Mt        0.000 Mt        3.770 Mt   100.000 % 	 ../DeferredRLInitialization

		      41.911 Mt    68.154 %         1 x       41.911 Mt        0.000 Mt        1.703 Mt     4.063 % 	 ../RayTracingRLInitialization

		      40.208 Mt    95.937 %         1 x       40.208 Mt        0.000 Mt       40.208 Mt   100.000 % 	 .../PipelineBuild

		       3.567 Mt     5.801 %         1 x        3.567 Mt        0.000 Mt        3.567 Mt   100.000 % 	 ../ResolveRLInitialization

		    4417.938 Mt    45.295 %       211 x       20.938 Mt        3.867 Mt        4.982 Mt     0.113 % 	 ./Run

		    3764.891 Mt    85.218 %       576 x        6.536 Mt        0.283 Mt     1307.796 Mt    34.737 % 	 ../Update

		     892.168 Mt    23.697 %         1 x      892.168 Mt        0.000 Mt       25.886 Mt     2.902 % 	 .../ScenePrep

		      16.060 Mt     1.800 %         1 x       16.060 Mt        0.000 Mt       16.060 Mt   100.000 % 	 ..../SceneDesc-CreateScene

		       2.613 Mt     0.293 %         1 x        2.613 Mt        0.000 Mt        0.001 Mt     0.050 % 	 ..../ShadowMapRLPrep

		       2.612 Mt    99.950 %         1 x        2.612 Mt        0.000 Mt        1.218 Mt    46.636 % 	 ...../PrepareForRendering

		       1.394 Mt    53.364 %         1 x        1.394 Mt        0.000 Mt        1.394 Mt   100.000 % 	 ....../PrepareDrawBundle

		       6.851 Mt     0.768 %         1 x        6.851 Mt        0.000 Mt        0.001 Mt     0.018 % 	 ..../DeferredRLPrep

		       6.850 Mt    99.982 %         1 x        6.850 Mt        0.000 Mt        3.782 Mt    55.221 % 	 ...../PrepareForRendering

		       0.370 Mt     5.403 %         1 x        0.370 Mt        0.000 Mt        0.370 Mt   100.000 % 	 ....../PrepareMaterials

		       0.427 Mt     6.232 %         1 x        0.427 Mt        0.000 Mt        0.427 Mt   100.000 % 	 ....../BuildMaterialCache

		       2.270 Mt    33.144 %         1 x        2.270 Mt        0.000 Mt        2.270 Mt   100.000 % 	 ....../PrepareDrawBundle

		     525.996 Mt    58.957 %         1 x      525.996 Mt        0.000 Mt        2.493 Mt     0.474 % 	 ..../RayTracingRLPrep

		      72.465 Mt    13.777 %         1 x       72.465 Mt        0.000 Mt       72.465 Mt   100.000 % 	 ...../PrepareAccelerationStructures

		       0.387 Mt     0.073 %         1 x        0.387 Mt        0.000 Mt        0.387 Mt   100.000 % 	 ...../PrepareCache

		      10.498 Mt     1.996 %         1 x       10.498 Mt        0.000 Mt       10.498 Mt   100.000 % 	 ...../PipelineBuild

		       1.163 Mt     0.221 %         1 x        1.163 Mt        0.000 Mt        1.163 Mt   100.000 % 	 ...../BuildCache

		     434.434 Mt    82.593 %         1 x      434.434 Mt        0.000 Mt        0.001 Mt     0.000 % 	 ...../BuildAccelerationStructures

		     434.433 Mt   100.000 %         1 x      434.433 Mt        0.000 Mt        3.073 Mt     0.707 % 	 ....../AccelerationStructureBuild

		     430.841 Mt    99.173 %         1 x      430.841 Mt        0.000 Mt      430.841 Mt   100.000 % 	 ......./BuildBottomLevelAS

		       0.518 Mt     0.119 %         1 x        0.518 Mt        0.000 Mt        0.518 Mt   100.000 % 	 ......./BuildTopLevelAS

		       4.557 Mt     0.866 %         1 x        4.557 Mt        0.000 Mt        4.557 Mt   100.000 % 	 ...../BuildShaderTables

		     314.762 Mt    35.281 %         1 x      314.762 Mt        0.000 Mt      314.762 Mt   100.000 % 	 ..../GpuSidePrep

		    1564.927 Mt    41.566 %      1719 x        0.910 Mt        0.000 Mt      554.771 Mt    35.450 % 	 .../AccelerationStructureBuild

		     374.299 Mt    23.918 %      1719 x        0.218 Mt        0.000 Mt      374.299 Mt   100.000 % 	 ..../BuildBottomLevelAS

		     635.857 Mt    40.632 %      1719 x        0.370 Mt        0.000 Mt      635.857 Mt   100.000 % 	 ..../BuildTopLevelAS

		     648.064 Mt    14.669 %       211 x        3.071 Mt        3.581 Mt        0.177 Mt     0.027 % 	 ../Render

		     647.887 Mt    99.973 %       211 x        3.071 Mt        3.580 Mt       18.652 Mt     2.879 % 	 .../Render

		     601.917 Mt    92.905 %       211 x        2.853 Mt        3.362 Mt      601.917 Mt   100.000 % 	 ..../Present

		      27.318 Mt     4.216 %       198 x        0.138 Mt        0.133 Mt       27.087 Mt    99.154 % 	 ..../RayTracing

		       0.138 Mt     0.504 %       198 x        0.001 Mt        0.001 Mt        0.138 Mt   100.000 % 	 ...../DeferredRLPrep

		       0.093 Mt     0.342 %       198 x        0.000 Mt        0.001 Mt        0.093 Mt   100.000 % 	 ...../RayTracingRLPrep

		       0.001 Mt     0.000 %         1 x        0.001 Mt        0.001 Mt        0.001 Mt   100.000 % 	 ./Destroy

	WindowThread:

		    9752.330 Mt   100.000 %         1 x     9752.330 Mt     9752.330 Mt     9752.330 Mt   100.000 % 	 /

	GpuThread:

		     975.810 Mt   100.000 %      1929 x        0.506 Mt        2.388 Mt        6.589 Mt     0.675 % 	 /

		       1.405 Mt     0.144 %       211 x        0.007 Mt        0.002 Mt        1.405 Mt   100.000 % 	 ./Present

		     307.577 Mt    31.520 %         1 x      307.577 Mt        0.000 Mt        0.007 Mt     0.002 % 	 ./ScenePrep

		     307.570 Mt    99.998 %         1 x      307.570 Mt        0.000 Mt        0.001 Mt     0.000 % 	 ../AccelerationStructureBuild

		     307.325 Mt    99.920 %         1 x      307.325 Mt        0.000 Mt      307.325 Mt   100.000 % 	 .../BuildBottomLevelASGpu

		       0.244 Mt     0.079 %         1 x        0.244 Mt        0.000 Mt        0.244 Mt   100.000 % 	 .../BuildTopLevelASGpu

		     187.092 Mt    19.173 %      1719 x        0.109 Mt        0.000 Mt        1.701 Mt     0.909 % 	 ./AccelerationStructureBuild

		      74.468 Mt    39.803 %      1719 x        0.043 Mt        0.000 Mt       74.468 Mt   100.000 % 	 ../BuildBottomLevelASGpu

		     110.923 Mt    59.288 %      1719 x        0.065 Mt        0.000 Mt      110.923 Mt   100.000 % 	 ../BuildTopLevelASGpu

		     473.147 Mt    48.488 %       198 x        2.390 Mt        2.385 Mt        0.683 Mt     0.144 % 	 ./RayTracingGpu

		      47.243 Mt     9.985 %       198 x        0.239 Mt        0.231 Mt       47.243 Mt   100.000 % 	 ../DeferredRLGpu

		     265.809 Mt    56.179 %       198 x        1.342 Mt        1.384 Mt      265.809 Mt   100.000 % 	 ../RayTracingRLGpu

		     159.412 Mt    33.692 %       198 x        0.805 Mt        0.765 Mt      159.412 Mt   100.000 % 	 ../ResolveRLGpu


	============================


