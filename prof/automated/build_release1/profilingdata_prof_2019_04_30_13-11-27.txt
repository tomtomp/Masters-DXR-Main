Configuration: 
Command line parameters  = 
Window width             = 1024
Window height            = 768
Window title             = Direct3D App
Back-buffer count        = 3
Fill Triangles           = enabled
Triangles Font Clock-Wise= disabled
Fast Build AS            = disabled
Target FPS               = 0
Target UPS               = 60
Tearing                  = enabled
VSync                    = disabled
GUI rendering            = disabled
Debug layer              = enabled
GPU validation           = disabled
GPU profiling            = enabled
Render mode              = Ray Tracing
Loaded scene file        = 
Using testing scene      = disabled
BLAS duplication         = disabled
Rays per pixel           = 9
Quality preset           = Medium
Rasterization Quality    : 
  AO                     = enabled
  AO kernel size         = 4
  Shadows                = enabled
  Shadow map resolution  = 4096
  Shadow PCF kernel size = 4
  Reflections            = disabled
Ray Tracing Quality      : 
  AO                     = enabled
  AO rays                = 4
  AO kernel size         = 4
  Shadows                = enabled
  Shadow rays            = 4
  Shadow kernel size     = 4
  Reflections            = disabled

ProfilingAggregator: 
Render	,	0.7349	,	0.7349	,	0.7349	,	0.7349	,	0.7349	,	0.7349	,	0.7349	,	0.7349	,	0.7349	,	0.7349	,	0.7349	,	0.7349	,	0.7349	,	0.7349	,	0.7349	,	0.7349	,	0.7349	,	0.7349	,	0.7349	,	0.7349	,	0.7349	,	0.7349	,	0.7349	,	0.7349	,	0.7349	,	0.7349	,	0.7349	,	0.7349	,	0.7349	,	0.7349	,	0.7349	,	0.7349	,	0.7349	,	0.7349	,	0.7349	,	0.7349	,	0.7349	,	0.7349	,	0.7349	,	0.7349	,	0.7349	,	0.7349	,	0.7349	,	0.7349	,	0.7349	,	0.7349	,	0.7349	,	0.7349	,	0.7349	,	0.7349	,	0.7349	,	0.7349	,	0.7349	,	0.7349	,	0.7349	,	0.7349	,	0.7349	,	0.7349	,	0.7349	,	0.7349	,	0.7349	,	0.7349
Update	,	0.0569	,	62.3038	,	64.2921	,	66.5425	,	69.6077	,	71.9827	,	74.1314	,	76.2054	,	78.7615	,	80.7989	,	83.1306	,	85.9655	,	87.778	,	89.5338	,	91.4214	,	93.4804	,	95.408	,	97.9273	,	102.229	,	105.502	,	108.055	,	110.229	,	112.176	,	114.98	,	117.534	,	120.255	,	122.863	,	125.009	,	126.854	,	129.182	,	131.098	,	132.911	,	135.013	,	137.237	,	139.31	,	141.383	,	143.319	,	145.362	,	147.341	,	149.332	,	151.679	,	153.782	,	155.811	,	157.757	,	160.083	,	163.236	,	166.181	,	172.011	,	174.556	,	177.271	,	179.766	,	182.504	,	185.415	,	189.213	,	192.201	,	195.09	,	197.356	,	199.218	,	201.484	,	203.933	,	206.095	,	208.214
Index	,	0	,	1	,	2	,	3	,	4	,	5	,	6	,	7	,	8	,	9	,	10	,	11	,	12	,	13	,	14	,	15	,	16	,	17	,	18	,	19	,	20	,	21	,	22	,	23	,	24	,	25	,	26	,	27	,	28	,	29	,	30	,	31	,	32	,	33	,	34	,	35	,	36	,	37	,	38	,	39	,	40	,	41	,	42	,	43	,	44	,	45	,	46	,	47	,	48	,	49	,	50	,	51	,	52	,	53	,	54	,	55	,	56	,	57	,	58	,	59	,	60	,	61

ProfilingBuildAggregator: 
BuildBottomLevelAS	,	0.2439	,	0.2886	,	0.2752	,	0.4831	,	0.2765	,	0.2493	,	0.2534	,	0.3531	,	0.2366	,	0.2831	,	0.3267	,	0.2293	,	0.2238	,	0.2504	,	0.2588	,	0.2476	,	0.3756	,	0.5378	,	0.3935	,	0.3287	,	0.2429	,	0.2299	,	0.4155	,	0.287	,	0.2999	,	0.2846	,	0.2394	,	0.2337	,	0.3599	,	0.2559	,	0.2445	,	0.2375	,	0.2655	,	0.2797	,	0.2487	,	0.2493	,	0.2474	,	0.2603	,	0.2403	,	0.3387	,	0.2232	,	0.2831	,	0.2472	,	0.2739	,	0.4013	,	0.3745	,	0.3686	,	0.2969	,	0.2903	,	0.2865	,	0.2928	,	0.297	,	0.4438	,	0.3143	,	0.314	,	0.2516	,	0.2341	,	0.2407	,	0.2583	,	0.2809	,	0.2282
BuildBottomLevelASGpu	,	0.120512	,	0.325216	,	0.464768	,	0.610368	,	0.472256	,	0.351552	,	0.341888	,	0.61152	,	0.23824	,	0.245792	,	0.200704	,	0.136768	,	0.196192	,	0.139296	,	0.234432	,	0.197408	,	0.549056	,	0.64016	,	0.348832	,	0.3632	,	0.381408	,	0.329728	,	0.343456	,	0.196896	,	0.379168	,	0.446016	,	0.2592	,	0.165088	,	0.143552	,	0.150912	,	0.175904	,	0.163712	,	0.164896	,	0.180192	,	0.229184	,	0.200512	,	0.24176	,	0.142784	,	0.151296	,	0.169312	,	0.159488	,	0.2712	,	0.296	,	0.609216	,	0.734432	,	0.636576	,	0.966592	,	0.670048	,	0.755872	,	0.796032	,	0.906208	,	0.668896	,	0.947168	,	0.82208	,	0.925248	,	0.540704	,	0.335648	,	0.482432	,	0.551104	,	0.53808	,	0.402656
BuildTopLevelAS	,	0.4074	,	0.368	,	0.3655	,	0.4296	,	0.3963	,	0.4077	,	0.3821	,	0.3678	,	0.3696	,	0.491	,	0.531	,	0.3671	,	0.3685	,	0.4874	,	0.4037	,	0.388	,	0.3934	,	0.6249	,	0.4858	,	0.4184	,	0.385	,	0.3813	,	0.5124	,	0.4391	,	0.4604	,	0.4082	,	0.4008	,	0.406	,	0.5104	,	0.3996	,	0.3752	,	0.4087	,	0.4681	,	0.4114	,	0.3877	,	0.3935	,	0.4132	,	0.4213	,	0.4076	,	0.5231	,	0.3961	,	0.4324	,	0.3691	,	0.3973	,	0.5654	,	0.5504	,	0.3894	,	0.3811	,	0.3998	,	0.3597	,	0.4321	,	0.4802	,	0.5071	,	0.4293	,	0.3677	,	0.3644	,	0.3589	,	0.4602	,	0.3982	,	0.3699	,	0.4489
BuildTopLevelASGpu	,	0.077536	,	0.078528	,	0.07888	,	0.08336	,	0.077536	,	0.077568	,	0.07792	,	0.082752	,	0.078304	,	0.078112	,	0.078528	,	0.078528	,	0.07776	,	0.077568	,	0.078496	,	0.077568	,	0.07872	,	0.082944	,	0.07872	,	0.078304	,	0.07776	,	0.077536	,	0.078144	,	0.083008	,	0.08336	,	0.083168	,	0.083168	,	0.078496	,	0.07776	,	0.077536	,	0.077344	,	0.077728	,	0.07776	,	0.078304	,	0.078304	,	0.078304	,	0.078304	,	0.07792	,	0.077344	,	0.077952	,	0.077728	,	0.077536	,	0.078144	,	0.082016	,	0.079072	,	0.07872	,	0.083936	,	0.079456	,	0.078688	,	0.079456	,	0.079456	,	0.082176	,	0.353984	,	0.084896	,	0.079424	,	0.079648	,	0.07872	,	0.07872	,	0.07872	,	0.079488	,	0.079296
Triangles	,	32	,	4120	,	15584	,	45330	,	20655	,	5600	,	7167	,	46560	,	288	,	282	,	143	,	28	,	75	,	27	,	132	,	108	,	39389	,	52674	,	1812	,	2934	,	2305	,	1290	,	1919	,	122	,	8917	,	19389	,	281	,	47	,	34	,	46	,	63	,	52	,	50	,	102	,	142	,	125	,	183	,	34	,	33	,	48	,	34	,	1658	,	1368	,	7000	,	8584	,	7794	,	37536	,	6246	,	14409	,	23123	,	23437	,	9929	,	29460	,	23399	,	29275	,	1620	,	192	,	1572	,	1362	,	1999	,	306
Meshes	,	1	,	1	,	2	,	2	,	2	,	1	,	1	,	2	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	2	,	2	,	2	,	2	,	2	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1
BottomLevels	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1
TopLevelSize	,	64	,	64	,	64	,	64	,	64	,	64	,	64	,	64	,	64	,	64	,	64	,	64	,	64	,	64	,	64	,	64	,	64	,	64	,	64	,	64	,	64	,	64	,	64	,	64	,	64	,	64	,	64	,	64	,	64	,	64	,	64	,	64	,	64	,	64	,	64	,	64	,	64	,	64	,	64	,	64	,	64	,	64	,	64	,	64	,	64	,	64	,	64	,	64	,	64	,	64	,	64	,	64	,	64	,	64	,	64	,	64	,	64	,	64	,	64	,	64	,	64
BottomLevelsSize	,	6528	,	233856	,	872832	,	2531072	,	1155712	,	316672	,	403712	,	2599424	,	20992	,	19840	,	12544	,	6256	,	8496	,	6256	,	12304	,	10752	,	2199808	,	2940416	,	105600	,	167936	,	132864	,	76032	,	111104	,	11392	,	501632	,	1085056	,	19840	,	7168	,	6800	,	7168	,	8320	,	7888	,	7888	,	10144	,	12544	,	11392	,	14976	,	6800	,	6800	,	7680	,	6800	,	97152	,	80896	,	498560	,	606720	,	555392	,	2637440	,	442240	,	1015680	,	1625856	,	1647488	,	700288	,	2071040	,	1646976	,	2056960	,	120832	,	18432	,	115712	,	100352	,	147456	,	26112
Index	,	0	,	1	,	2	,	3	,	4	,	5	,	6	,	7	,	8	,	9	,	10	,	11	,	12	,	13	,	14	,	15	,	16	,	17	,	18	,	19	,	20	,	21	,	22	,	23	,	24	,	25	,	26	,	27	,	28	,	29	,	30	,	31	,	32	,	33	,	34	,	35	,	36	,	37	,	38	,	39	,	40	,	41	,	42	,	43	,	44	,	45	,	46	,	47	,	48	,	49	,	50	,	51	,	52	,	53	,	54	,	55	,	56	,	57	,	58	,	59	,	60

FpsAggregator: 
FPS	,	2	,	2	,	2	,	2	,	2	,	2	,	2	,	2	,	2	,	2	,	2	,	2	,	2	,	2	,	2	,	2	,	2	,	2	,	2	,	2	,	2	,	2	,	2	,	2	,	2	,	2	,	2	,	2	,	2	,	2	,	2	,	2	,	2	,	2	,	2	,	2	,	2	,	2	,	2	,	2	,	2	,	2	,	2	,	2	,	2	,	2	,	2	,	2	,	2	,	2	,	2	,	2	,	2	,	2	,	2	,	2	,	2	,	2	,	2	,	2	,	2	,	2
UPS	,	15	,	15	,	15	,	15	,	15	,	15	,	15	,	15	,	15	,	15	,	15	,	15	,	15	,	15	,	15	,	15	,	15	,	15	,	15	,	15	,	15	,	15	,	15	,	15	,	15	,	15	,	15	,	15	,	15	,	15	,	15	,	15	,	15	,	15	,	15	,	15	,	15	,	15	,	15	,	15	,	15	,	15	,	15	,	15	,	15	,	15	,	15	,	15	,	15	,	15	,	15	,	15	,	15	,	15	,	15	,	15	,	15	,	15	,	15	,	15	,	15	,	15
FrameTime	,	1038.75	,	1038.75	,	1038.75	,	1038.75	,	1038.75	,	1038.75	,	1038.75	,	1038.75	,	1038.75	,	1038.75	,	1038.75	,	1038.75	,	1038.75	,	1038.75	,	1038.75	,	1038.75	,	1038.75	,	1038.75	,	1038.75	,	1038.75	,	1038.75	,	1038.75	,	1038.75	,	1038.75	,	1038.75	,	1038.75	,	1038.75	,	1038.75	,	1038.75	,	1038.75	,	1038.75	,	1038.75	,	1038.75	,	1038.75	,	1038.75	,	1038.75	,	1038.75	,	1038.75	,	1038.75	,	1038.75	,	1038.75	,	1038.75	,	1038.75	,	1038.75	,	1038.75	,	1038.75	,	1038.75	,	1038.75	,	1038.75	,	1038.75	,	1038.75	,	1038.75	,	1038.75	,	1038.75	,	1038.75	,	1038.75	,	1038.75	,	1038.75	,	1038.75	,	1038.75	,	1038.75	,	1038.75
GigaRays/s	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0
Index	,	0	,	1	,	2	,	3	,	4	,	5	,	6	,	7	,	8	,	9	,	10	,	11	,	12	,	13	,	14	,	15	,	16	,	17	,	18	,	19	,	20	,	21	,	22	,	23	,	24	,	25	,	26	,	27	,	28	,	29	,	30	,	31	,	32	,	33	,	34	,	35	,	36	,	37	,	38	,	39	,	40	,	41	,	42	,	43	,	44	,	45	,	46	,	47	,	48	,	49	,	50	,	51	,	52	,	53	,	54	,	55	,	56	,	57	,	58	,	59	,	60	,	61


AccelerationStructure: 
Ray tracing acceleration structure building statistics: 
	Bottom Level Acceleration Structure: 
		Maximum [B]: 0
		Minimum [B]: 18446744073709551615
		Currently Allocated [B]: 0
		Currently Created [num]: 0
		Current Average [B]: 0
		Maximum Triangles [num]: 0
		Minimum Triangles [num]: 18446744073709551615
		Total Triangles [num]: 0
		Maximum Meshes [num]: 0
		Minimum Meshes [num]: 18446744073709551615
		Total Meshes [num]: 0
	Top Level Acceleration Structure: 
		Maximum [B]: 0
		Minimum [B]: 18446744073709551615
		Currently Allocated [B]: 0
		Total Allocated [B]: 0
		Total Created [num]: 0
		Average Allocated [B]: 0
		Maximum Instances [num]: 0
		Minimum Instances [num]: 18446744073709551615
		Total Instances [num]: 0
	Scratch Buffer: 
		Maximum [B]: 0
		Minimum [B]: 18446744073709551615
		Currently Allocated [B]: 0
		Total Allocated [B]: 0
		Total Created [num]: 0
		Average Allocated [B]: 0
ProfilingStack: 
[PROF] Profiling statistics: 
	Scopes entered : 408
	Scopes exited : 406
	Overhead per scope [ticks] : 162.2
	Threads entered : 3
	Threads exited : 0
	============================
		         Sum[Mt]   Parent[%]  Samples[num]       Avg[Mt]   Last/Curr[Mt]        Self[Mt]     Self[%] 	 Name

	main:

		    2465.296 Mt   100.000 %         1 x     2465.296 Mt     2465.296 Mt     2179.766 Mt    88.418 % 	 /

		      60.326 Mt     2.447 %         1 x       60.326 Mt        0.000 Mt        0.611 Mt     1.013 % 	 ./Initialize

		       2.737 Mt     4.537 %         1 x        2.737 Mt        0.000 Mt        2.737 Mt   100.000 % 	 ../TexturedRLInitialization

		       1.325 Mt     2.197 %         1 x        1.325 Mt        0.000 Mt        1.325 Mt   100.000 % 	 ../ShadowMapRLInitialization

		       6.398 Mt    10.605 %         1 x        6.398 Mt        0.000 Mt        6.398 Mt   100.000 % 	 ../AmbientOcclusionRLInitialization

		       5.276 Mt     8.746 %         1 x        5.276 Mt        0.000 Mt        5.276 Mt   100.000 % 	 ../RasterResolveRLInitialization

		       3.991 Mt     6.616 %         1 x        3.991 Mt        0.000 Mt        3.991 Mt   100.000 % 	 ../DeferredRLInitialization

		      35.951 Mt    59.595 %         1 x       35.951 Mt        0.000 Mt        1.304 Mt     3.628 % 	 ../RayTracingRLInitialization

		      34.647 Mt    96.372 %         1 x       34.647 Mt        0.000 Mt       34.647 Mt   100.000 % 	 .../PipelineBuild

		       4.036 Mt     6.691 %         1 x        4.036 Mt        0.000 Mt        4.036 Mt   100.000 % 	 ../ResolveRLInitialization

		     225.274 Mt     9.138 %         3 x       75.091 Mt      209.957 Mt        0.920 Mt     0.408 % 	 ./Run

		     218.006 Mt    96.774 %        16 x       13.625 Mt      209.059 Mt       94.571 Mt    43.380 % 	 ../Update

		      51.586 Mt    23.663 %         1 x       51.586 Mt       51.586 Mt       46.413 Mt    89.972 % 	 .../ScenePrep

		       5.173 Mt    10.028 %         1 x        5.173 Mt        5.173 Mt        5.173 Mt   100.000 % 	 ..../SceneDesc-CreateScene

		      71.850 Mt    32.958 %        62 x        1.159 Mt        1.052 Mt       26.744 Mt    37.222 % 	 .../AccelerationStructureBuild

		      18.655 Mt    25.964 %        62 x        0.301 Mt        0.228 Mt       18.655 Mt   100.000 % 	 ..../BuildBottomLevelAS

		      26.451 Mt    36.814 %        62 x        0.427 Mt        0.449 Mt       26.451 Mt   100.000 % 	 ..../BuildTopLevelAS

		       6.354 Mt     2.821 %         2 x        3.177 Mt        0.000 Mt        0.002 Mt     0.028 % 	 ../Render

		       6.352 Mt    99.972 %         2 x        3.176 Mt        0.000 Mt        6.352 Mt   100.000 % 	 .../Render

	WindowThread:

		    2463.425 Mt   100.000 %         1 x     2463.425 Mt     2463.425 Mt     2463.425 Mt   100.000 % 	 /

	GpuThread:

		      29.612 Mt   100.000 %        63 x        0.470 Mt        0.484 Mt       -0.199 Mt    -0.674 % 	 /

		       0.002 Mt     0.006 %         2 x        0.001 Mt        0.000 Mt        0.002 Mt   100.000 % 	 ./Present

		      29.809 Mt   100.668 %        62 x        0.481 Mt        0.484 Mt        0.086 Mt     0.290 % 	 ./AccelerationStructureBuild

		      24.539 Mt    82.321 %        62 x        0.396 Mt        0.403 Mt       24.539 Mt   100.000 % 	 ../BuildBottomLevelASGpu

		       5.184 Mt    17.389 %        62 x        0.084 Mt        0.079 Mt        5.184 Mt   100.000 % 	 ../BuildTopLevelASGpu


	============================


