Configuration: 
Command line parameters  = 
Build type               = Release
Device name              = NVIDIA GeForce RTX 2080 Ti
Device memory MB         = 11049
Ray Tracing backend      = Hardware
Window width             = 1024
Window height            = 768
Window title             = Direct3D App
Back-buffer count        = 3
Fill Triangles           = enabled
Triangles Font Clock-Wise= disabled
Fast Build AS            = disabled
Target FPS               = 0
Target UPS               = 60
Tearing                  = enabled
VSync                    = disabled
GUI rendering            = disabled
Debug layer              = disabled
GPU validation           = disabled
GPU profiling            = enabled
Render mode              = Ray Tracing
Loaded scene file        = 
Using testing scene      = disabled
BLAS duplication         = disabled
Rays per pixel           = 8
Quality preset           = Medium
Rasterization Quality    : 
  AO                     = enabled
  AO kernel size         = 4
  Shadows                = enabled
  Shadow map resolution  = 4096
  Shadow PCF kernel size = 4
  Reflections            = disabled
Ray Tracing Quality      : 
  AO                     = enabled
  AO rays                = 4
  AO kernel size         = 4
  Shadows                = enabled
  Shadow rays            = 4
  Shadow kernel size     = 4
  Reflections            = disabled

ProfilingAggregator: 
Render	,	0.9902	,	0.9902
Update	,	30.2337	,	32.8173
Index	,	0	,	1

ProfilingBuildAggregator: 
BuildBottomLevelAS	,	0.3653	,	0.4522
BuildBottomLevelASGpu	,	0.014688	,	0.252672
BuildTopLevelAS	,	0.5933	,	0.4804
BuildTopLevelASGpu	,	0.05632	,	0.06048
Triangles	,	32	,	2764
Meshes	,	1	,	1
BottomLevels	,	1	,	1
TopLevelSize	,	64	,	64
BottomLevelsSize	,	2048	,	157312
Index	,	0	,	1

FpsAggregator: 
FPS	,	0	,	0
UPS	,	0	,	0
FrameTime	,	0	,	0
GigaRays/s	,	0	,	0
Index	,	0	,	1


AccelerationStructure: 
Ray tracing acceleration structure building statistics: 
	Bottom Level Acceleration Structure: 
		Maximum [B]: 195840
		Minimum [B]: 6528
		Currently Allocated [B]: 202368
		Currently Created [num]: 2
		Current Average [B]: 101184
		Maximum Triangles [num]: 2764
		Minimum Triangles [num]: 32
		Total Triangles [num]: 2796
		Maximum Meshes [num]: 1
		Minimum Meshes [num]: 1
		Total Meshes [num]: 2
	Top Level Acceleration Structure: 
		Maximum [B]: 128
		Minimum [B]: 128
		Currently Allocated [B]: 128
		Total Allocated [B]: 128
		Total Created [num]: 1
		Average Allocated [B]: 128
		Maximum Instances [num]: 2
		Minimum Instances [num]: 2
		Total Instances [num]: 2
	Scratch Buffer: 
		Maximum [B]: 58368
		Minimum [B]: 58368
		Currently Allocated [B]: 58368
		Total Allocated [B]: 58368
		Total Created [num]: 1
		Average Allocated [B]: 58368
ProfilingStack: 
[PROF] Profiling statistics: 
	Scopes entered : 71
	Scopes exited : 69
	Overhead per scope [ticks] : 166.4
	Threads entered : 3
	Threads exited : 0
	============================
		         Sum[Mt]   Parent[%]  Samples[num]       Avg[Mt]   Last/Curr[Mt]        Self[Mt]     Self[%] 	 Name

	main:

		     361.333 Mt   100.000 %         1 x      361.334 Mt      361.333 Mt      244.430 Mt    67.647 % 	 /

		      68.037 Mt    18.829 %         1 x       68.037 Mt       68.037 Mt        0.712 Mt     1.046 % 	 ./Initialize

		       1.396 Mt     2.052 %         1 x        1.396 Mt        1.396 Mt        1.396 Mt   100.000 % 	 ../ShadowMapRLInitialization

		       6.495 Mt     9.546 %         1 x        6.495 Mt        6.495 Mt        6.495 Mt   100.000 % 	 ../AmbientOcclusionRLInitialization

		       3.956 Mt     5.814 %         1 x        3.956 Mt        3.956 Mt        3.956 Mt   100.000 % 	 ../RasterResolveRLInitialization

		       4.407 Mt     6.477 %         1 x        4.407 Mt        4.407 Mt        4.407 Mt   100.000 % 	 ../DeferredRLInitialization

		      47.249 Mt    69.446 %         1 x       47.249 Mt       47.249 Mt        1.618 Mt     3.425 % 	 ../RayTracingRLInitialization

		      45.630 Mt    96.575 %         1 x       45.630 Mt       45.630 Mt       45.630 Mt   100.000 % 	 .../PipelineBuild

		       3.822 Mt     5.617 %         1 x        3.822 Mt        3.822 Mt        3.822 Mt   100.000 % 	 ../ResolveRLInitialization

		      48.948 Mt    13.547 %         3 x       16.316 Mt       33.131 Mt        0.014 Mt     0.028 % 	 ./Run

		      40.137 Mt    82.000 %         9 x        4.460 Mt       33.136 Mt       11.114 Mt    27.691 % 	 ../Update

		      24.131 Mt    60.122 %         1 x       24.131 Mt       24.131 Mt        3.127 Mt    12.958 % 	 .../ScenePrep

		       0.507 Mt     2.099 %         1 x        0.507 Mt        0.507 Mt        0.507 Mt   100.000 % 	 ..../SceneDesc-CreateScene

		       0.069 Mt     0.288 %         1 x        0.069 Mt        0.069 Mt        0.001 Mt     0.865 % 	 ..../ShadowMapRLPrep

		       0.069 Mt    99.135 %         1 x        0.069 Mt        0.069 Mt        0.061 Mt    88.808 % 	 ...../PrepareForRendering

		       0.008 Mt    11.192 %         1 x        0.008 Mt        0.008 Mt        0.008 Mt   100.000 % 	 ....../PrepareDrawBundle

		       1.439 Mt     5.963 %         1 x        1.439 Mt        1.439 Mt        0.001 Mt     0.069 % 	 ..../DeferredRLPrep

		       1.438 Mt    99.931 %         1 x        1.438 Mt        1.438 Mt        1.256 Mt    87.316 % 	 ...../PrepareForRendering

		       0.006 Mt     0.396 %         1 x        0.006 Mt        0.006 Mt        0.006 Mt   100.000 % 	 ....../PrepareMaterials

		       0.162 Mt    11.266 %         1 x        0.162 Mt        0.162 Mt        0.162 Mt   100.000 % 	 ....../BuildMaterialCache

		       0.015 Mt     1.022 %         1 x        0.015 Mt        0.015 Mt        0.015 Mt   100.000 % 	 ....../PrepareDrawBundle

		      13.472 Mt    55.826 %         1 x       13.472 Mt       13.472 Mt        1.358 Mt    10.083 % 	 ..../RayTracingRLPrep

		       0.087 Mt     0.647 %         1 x        0.087 Mt        0.087 Mt        0.087 Mt   100.000 % 	 ...../PrepareAccelerationStructures

		       0.005 Mt     0.039 %         1 x        0.005 Mt        0.005 Mt        0.005 Mt   100.000 % 	 ...../PrepareCache

		      10.313 Mt    76.551 %         1 x       10.313 Mt       10.313 Mt       10.313 Mt   100.000 % 	 ...../PipelineBuild

		       0.072 Mt     0.537 %         1 x        0.072 Mt        0.072 Mt        0.072 Mt   100.000 % 	 ...../BuildCache

		       1.397 Mt    10.371 %         1 x        1.397 Mt        1.397 Mt        0.001 Mt     0.072 % 	 ...../BuildAccelerationStructures

		       1.396 Mt    99.928 %         1 x        1.396 Mt        1.396 Mt        0.352 Mt    25.183 % 	 ....../AccelerationStructureBuild

		       0.687 Mt    49.191 %         1 x        0.687 Mt        0.687 Mt        0.687 Mt   100.000 % 	 ......./BuildBottomLevelAS

		       0.358 Mt    25.627 %         1 x        0.358 Mt        0.358 Mt        0.358 Mt   100.000 % 	 ......./BuildTopLevelAS

		       0.239 Mt     1.772 %         1 x        0.239 Mt        0.239 Mt        0.239 Mt   100.000 % 	 ...../BuildShaderTables

		       5.518 Mt    22.865 %         1 x        5.518 Mt        5.518 Mt        5.518 Mt   100.000 % 	 ..../GpuSidePrep

		       4.892 Mt    12.187 %         3 x        1.631 Mt        1.343 Mt        2.038 Mt    41.665 % 	 .../AccelerationStructureBuild

		       1.166 Mt    23.839 %         3 x        0.389 Mt        0.452 Mt        1.166 Mt   100.000 % 	 ..../BuildBottomLevelAS

		       1.687 Mt    34.496 %         3 x        0.562 Mt        0.480 Mt        1.687 Mt   100.000 % 	 ..../BuildTopLevelAS

		       8.804 Mt    17.987 %         2 x        4.402 Mt        0.990 Mt        0.002 Mt     0.025 % 	 ../Render

		       8.802 Mt    99.975 %         2 x        4.401 Mt        0.989 Mt        0.813 Mt     9.240 % 	 .../Render

		       7.989 Mt    90.760 %         2 x        3.994 Mt        0.909 Mt        7.989 Mt   100.000 % 	 ..../Present

	WindowThread:

		     359.755 Mt   100.000 %         1 x      359.755 Mt      359.755 Mt      359.755 Mt   100.000 % 	 /

	GpuThread:

		       8.038 Mt   100.000 %         4 x        2.009 Mt        0.314 Mt        6.959 Mt    86.585 % 	 /

		       0.002 Mt     0.030 %         2 x        0.001 Mt        0.001 Mt        0.002 Mt   100.000 % 	 ./Present

		       0.617 Mt     7.680 %         1 x        0.617 Mt        0.617 Mt        0.007 Mt     1.177 % 	 ./ScenePrep

		       0.610 Mt    98.823 %         1 x        0.610 Mt        0.610 Mt        0.001 Mt     0.147 % 	 ../AccelerationStructureBuild

		       0.545 Mt    89.362 %         1 x        0.545 Mt        0.545 Mt        0.545 Mt   100.000 % 	 .../BuildBottomLevelASGpu

		       0.064 Mt    10.492 %         1 x        0.064 Mt        0.064 Mt        0.064 Mt   100.000 % 	 .../BuildTopLevelASGpu

		       0.458 Mt     5.704 %         3 x        0.153 Mt        0.314 Mt        0.003 Mt     0.726 % 	 ./AccelerationStructureBuild

		       0.282 Mt    61.516 %         3 x        0.094 Mt        0.253 Mt        0.282 Mt   100.000 % 	 ../BuildBottomLevelASGpu

		       0.173 Mt    37.758 %         3 x        0.058 Mt        0.060 Mt        0.173 Mt   100.000 % 	 ../BuildTopLevelASGpu


	============================


