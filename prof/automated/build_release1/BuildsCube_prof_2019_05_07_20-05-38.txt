Configuration: 
Command line parameters  = E:\programming\FIT-DIP\Masters-DXR-Main-git\build\Measurement\x64\Release\Measurement.exe --scene Cube/Cube.gltf --profile-builds --profile-output BuildsCube 
Build type               = Release
Device name              = NVIDIA GeForce RTX 2080 Ti
Device memory MB         = 11049
Ray Tracing backend      = Hardware
Window width             = 1024
Window height            = 768
Window title             = Direct3D App
Back-buffer count        = 3
Fill Triangles           = enabled
Triangles Font Clock-Wise= enabled
Fast Build AS            = disabled
Target FPS               = 60
Target UPS               = 60
Tearing                  = disabled
VSync                    = disabled
GUI rendering            = enabled
Debug layer              = disabled
GPU validation           = disabled
GPU profiling            = enabled
Render mode              = Rasterization
Loaded scene file        = Cube/Cube.gltf
Using testing scene      = disabled
Duplication              = 1
Duplication in cube      = disabled
Duplication offset       = 10
BLAS duplication         = disabled
Rays per pixel           = 13
Quality preset           = High
Rasterization Quality    : 
  AO                     = enabled
  AO kernel size         = 4
  Shadows                = enabled
  Shadow map resolution  = 4096
  Shadow PCF kernel size = 4
  Reflections            = enabled
Ray Tracing Quality      : 
  AO                     = enabled
  AO rays                = 4
  AO kernel size         = 4
  Shadows                = enabled
  Shadow rays            = 4
  Shadow kernel size     = 4
  Reflections            = enabled

ProfilingAggregator: 

ProfilingBuildAggregator: 
BuildBottomLevelAS	,	1.6638	,	1.6638
BuildBottomLevelASGpu	,	0.01312	,	0.118112
BuildTopLevelAS	,	0.9942	,	0.9942
BuildTopLevelASGpu	,	0.061728	,	0.058816
Duplication	,	1	,	1
Triangles	,	12	,	12
Meshes	,	1	,	1
BottomLevels	,	1	,	1
TopLevelSize	,	64	,	64
BottomLevelsSize	,	1152	,	4480
Index	,	0	,	1

FpsAggregator: 
FPS	,	0	,	0
UPS	,	0	,	0
FrameTime	,	0	,	0
GigaRays/s	,	0	,	0
Index	,	0	,	1


AccelerationStructure: 
Ray tracing acceleration structure building statistics: 
	Bottom Level Acceleration Structure: 
		Maximum [B]: 4480
		Minimum [B]: 4480
		Currently Allocated [B]: 4480
		Currently Created [num]: 1
		Current Average [B]: 4480
		Maximum Triangles [num]: 12
		Minimum Triangles [num]: 12
		Total Triangles [num]: 12
		Maximum Meshes [num]: 1
		Minimum Meshes [num]: 1
		Total Meshes [num]: 1
	Top Level Acceleration Structure: 
		Maximum [B]: 64
		Minimum [B]: 64
		Currently Allocated [B]: 64
		Total Allocated [B]: 64
		Total Created [num]: 1
		Average Allocated [B]: 64
		Maximum Instances [num]: 1
		Minimum Instances [num]: 1
		Total Instances [num]: 1
	Scratch Buffer: 
		Maximum [B]: 896
		Minimum [B]: 896
		Currently Allocated [B]: 896
		Total Allocated [B]: 896
		Total Created [num]: 1
		Average Allocated [B]: 896
ProfilingStack: 
[PROF] Profiling statistics: 
	Scopes entered : 62
	Scopes exited : 61
	Overhead per scope [ticks] : 133.9
	Threads entered : 3
	Threads exited : 0
	============================
		         Sum[Mt]   Parent[%]  Samples[num]       Avg[Mt]   Last/Curr[Mt]        Self[Mt]     Self[%] 	 Name

	main:

		     550.894 Mt   100.000 %         1 x      550.895 Mt      550.894 Mt        0.477 Mt     0.087 % 	 /

		     550.462 Mt    99.922 %         1 x      550.463 Mt      550.462 Mt      257.614 Mt    46.800 % 	 ./Main application loop

		       2.281 Mt     0.414 %         1 x        2.281 Mt        2.281 Mt        2.281 Mt   100.000 % 	 ../TexturedRLInitialization

		       1.246 Mt     0.226 %         1 x        1.246 Mt        1.246 Mt        1.246 Mt   100.000 % 	 ../ShadowMapRLInitialization

		       4.945 Mt     0.898 %         1 x        4.945 Mt        4.945 Mt        4.945 Mt   100.000 % 	 ../RasterResolveRLInitialization

		       3.965 Mt     0.720 %         1 x        3.965 Mt        3.965 Mt        3.965 Mt   100.000 % 	 ../ResolveRLInitialization

		      26.946 Mt     4.895 %         3 x        8.982 Mt        9.115 Mt       12.997 Mt    48.233 % 	 ../AccelerationStructureBuild

		       7.279 Mt    27.013 %         3 x        2.426 Mt        0.535 Mt        7.279 Mt   100.000 % 	 .../BuildBottomLevelAS

		       6.670 Mt    24.754 %         3 x        2.223 Mt        4.729 Mt        6.670 Mt   100.000 % 	 .../BuildTopLevelAS

		       2.616 Mt     0.475 %         1 x        2.616 Mt        2.616 Mt        2.616 Mt   100.000 % 	 ../AmbientOcclusionRLInitialization

		       4.498 Mt     0.817 %         1 x        4.498 Mt        4.498 Mt        4.498 Mt   100.000 % 	 ../DeferredRLInitialization

		     101.358 Mt    18.413 %         1 x      101.358 Mt      101.358 Mt        2.355 Mt     2.323 % 	 ../RayTracingRLInitialization

		      99.003 Mt    97.677 %         1 x       99.003 Mt       99.003 Mt       99.003 Mt   100.000 % 	 .../PipelineBuild

		     144.992 Mt    26.340 %         1 x      144.992 Mt      144.992 Mt        0.564 Mt     0.389 % 	 ../Initialization

		     144.429 Mt    99.611 %         1 x      144.429 Mt      144.429 Mt        1.906 Mt     1.320 % 	 .../ScenePrep

		      51.546 Mt    35.690 %         1 x       51.546 Mt       51.546 Mt       13.693 Mt    26.565 % 	 ..../SceneLoad

		      12.069 Mt    23.413 %         1 x       12.069 Mt       12.069 Mt        3.860 Mt    31.987 % 	 ...../glTF-LoadScene

		       8.208 Mt    68.013 %         2 x        4.104 Mt        1.129 Mt        8.208 Mt   100.000 % 	 ....../glTF-LoadImageData

		       9.566 Mt    18.558 %         1 x        9.566 Mt        9.566 Mt        9.566 Mt   100.000 % 	 ...../glTF-CreateScene

		      16.218 Mt    31.464 %         1 x       16.218 Mt       16.218 Mt       16.218 Mt   100.000 % 	 ...../SceneDesc-CreateScene

		       1.135 Mt     0.786 %         1 x        1.135 Mt        1.135 Mt        0.000 Mt     0.035 % 	 ..../ShadowMapRLPrep

		       1.135 Mt    99.965 %         1 x        1.135 Mt        1.135 Mt        1.124 Mt    99.030 % 	 ...../PrepareForRendering

		       0.011 Mt     0.970 %         1 x        0.011 Mt        0.011 Mt        0.011 Mt   100.000 % 	 ....../PrepareDrawBundle

		       5.967 Mt     4.132 %         1 x        5.967 Mt        5.967 Mt        0.001 Mt     0.010 % 	 ..../TexturedRLPrep

		       5.967 Mt    99.990 %         1 x        5.967 Mt        5.967 Mt        5.960 Mt    99.884 % 	 ...../PrepareForRendering

		       0.001 Mt     0.023 %         1 x        0.001 Mt        0.001 Mt        0.001 Mt   100.000 % 	 ....../PrepareMaterials

		       0.005 Mt     0.092 %         1 x        0.005 Mt        0.005 Mt        0.005 Mt   100.000 % 	 ....../PrepareDrawBundle

		       3.956 Mt     2.739 %         1 x        3.956 Mt        3.956 Mt        0.001 Mt     0.018 % 	 ..../DeferredRLPrep

		       3.955 Mt    99.982 %         1 x        3.955 Mt        3.955 Mt        2.511 Mt    63.495 % 	 ...../PrepareForRendering

		       0.001 Mt     0.023 %         1 x        0.001 Mt        0.001 Mt        0.001 Mt   100.000 % 	 ....../PrepareMaterials

		       1.438 Mt    36.349 %         1 x        1.438 Mt        1.438 Mt        1.438 Mt   100.000 % 	 ....../BuildMaterialCache

		       0.005 Mt     0.134 %         1 x        0.005 Mt        0.005 Mt        0.005 Mt   100.000 % 	 ....../PrepareDrawBundle

		      68.493 Mt    47.423 %         1 x       68.493 Mt       68.493 Mt        3.604 Mt     5.261 % 	 ..../RayTracingRLPrep

		       0.095 Mt     0.139 %         1 x        0.095 Mt        0.095 Mt        0.095 Mt   100.000 % 	 ...../PrepareAccelerationStructures

		       0.002 Mt     0.002 %         1 x        0.002 Mt        0.002 Mt        0.002 Mt   100.000 % 	 ...../PrepareCache

		      49.816 Mt    72.732 %         1 x       49.816 Mt       49.816 Mt       49.816 Mt   100.000 % 	 ...../PipelineBuild

		       1.046 Mt     1.527 %         1 x        1.046 Mt        1.046 Mt        1.046 Mt   100.000 % 	 ...../BuildCache

		      11.595 Mt    16.928 %         1 x       11.595 Mt       11.595 Mt        0.001 Mt     0.007 % 	 ...../BuildAccelerationStructures

		      11.594 Mt    99.993 %         1 x       11.594 Mt       11.594 Mt        8.936 Mt    77.074 % 	 ....../AccelerationStructureBuild

		       1.664 Mt    14.351 %         1 x        1.664 Mt        1.664 Mt        1.664 Mt   100.000 % 	 ......./BuildBottomLevelAS

		       0.994 Mt     8.575 %         1 x        0.994 Mt        0.994 Mt        0.994 Mt   100.000 % 	 ......./BuildTopLevelAS

		       2.336 Mt     3.410 %         1 x        2.336 Mt        2.336 Mt        2.336 Mt   100.000 % 	 ...../BuildShaderTables

		      11.425 Mt     7.910 %         1 x       11.425 Mt       11.425 Mt       11.425 Mt   100.000 % 	 ..../GpuSidePrep

	WindowThread:

		     549.604 Mt   100.000 %         1 x      549.604 Mt      549.604 Mt      549.604 Mt   100.000 % 	 /

	GpuThread:

		      34.906 Mt   100.000 %         2 x       17.453 Mt        0.178 Mt       34.376 Mt    98.480 % 	 /

		       0.201 Mt     0.577 %         1 x        0.201 Mt        0.201 Mt        0.006 Mt     2.956 % 	 ./ScenePrep

		       0.195 Mt    97.044 %         1 x        0.195 Mt        0.195 Mt        0.001 Mt     0.753 % 	 ../AccelerationStructureBuild

		       0.132 Mt    67.704 %         1 x        0.132 Mt        0.132 Mt        0.132 Mt   100.000 % 	 .../BuildBottomLevelASGpu

		       0.062 Mt    31.543 %         1 x        0.062 Mt        0.062 Mt        0.062 Mt   100.000 % 	 .../BuildTopLevelASGpu

		       0.329 Mt     0.943 %         3 x        0.110 Mt        0.178 Mt        0.002 Mt     0.749 % 	 ./AccelerationStructureBuild

		       0.144 Mt    43.864 %         3 x        0.048 Mt        0.118 Mt        0.144 Mt   100.000 % 	 ../BuildBottomLevelASGpu

		       0.182 Mt    55.387 %         3 x        0.061 Mt        0.059 Mt        0.182 Mt   100.000 % 	 ../BuildTopLevelASGpu


	============================


