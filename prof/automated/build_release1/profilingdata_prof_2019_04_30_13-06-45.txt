Configuration: 
Command line parameters  = 
Window width             = 1186
Window height            = 1219
Window title             = Direct3D App
Back-buffer count        = 3
Fill Triangles           = enabled
Triangles Font Clock-Wise= disabled
Fast Build AS            = disabled
Target FPS               = 0
Target UPS               = 60
Tearing                  = enabled
VSync                    = disabled
GUI rendering            = disabled
Debug layer              = enabled
GPU validation           = disabled
GPU profiling            = enabled
Render mode              = Ray Tracing
Loaded scene file        = 
Using testing scene      = disabled
BLAS duplication         = disabled
Rays per pixel           = 34
Quality preset           = Ultra
Rasterization Quality    : 
  AO                     = enabled
  AO kernel size         = 16
  Shadows                = enabled
  Shadow map resolution  = 8192
  Shadow PCF kernel size = 8
  Reflections            = enabled
Ray Tracing Quality      : 
  AO                     = enabled
  AO rays                = 16
  AO kernel size         = 8
  Shadows                = enabled
  Shadow rays            = 8
  Shadow kernel size     = 8
  Reflections            = enabled

ProfilingAggregator: 
Render	,	22.2979	,	19.6694	,	20.6995	,	20.0153	,	20.6847	,	21.7622	,	19.2977
Update	,	0.0242	,	0.0243	,	0.0345	,	0.0248	,	0.0897	,	0.081	,	0.024
RayTracing	,	7.5748	,	7.109	,	7.7249	,	7.0454	,	8.0822	,	7.1371	,	7.0799
RayTracingGpu	,	6.2488	,	6.14512	,	6.13978	,	6.39018	,	6.08829	,	6.40947	,	6.07894
DeferredRLGpu	,	0.257056	,	0.267328	,	0.23424	,	0.245024	,	0.250112	,	0.243168	,	0.24608
RayTracingRLGpu	,	3.17085	,	3.10381	,	2.99587	,	3.23546	,	3.05546	,	3.27779	,	3.05568
ResolveRLGpu	,	2.81712	,	2.76922	,	2.89981	,	2.90826	,	2.77955	,	2.88611	,	2.77414
Index	,	0	,	1	,	2	,	3	,	4	,	5	,	6

ProfilingBuildAggregator: 
BuildBottomLevelAS	,	11.5305
BuildBottomLevelASGpu	,	20.3492
BuildTopLevelAS	,	0.4336
BuildTopLevelASGpu	,	0.280352
Triangles	,	281182
Meshes	,	52
BottomLevels	,	43
TopLevelSize	,	2752
BottomLevelsSize	,	15869344
Index	,	0

FpsAggregator: 
FPS	,	21	,	29	,	33	,	34	,	33	,	19	,	28
UPS	,	169	,	61	,	60	,	62	,	61	,	36	,	475
FrameTime	,	47.9225	,	35.1151	,	30.404	,	30.2571	,	31.1329	,	395.627	,	35.8278
GigaRays/s	,	0.952114	,	1.29938	,	1.50071	,	1.508	,	1.46558	,	0.11533	,	1.27353
Index	,	0	,	1	,	2	,	3	,	4	,	5	,	6


AccelerationStructure: 
Ray tracing acceleration structure building statistics: 
	Bottom Level Acceleration Structure: 
		Maximum [B]: 2940416
		Minimum [B]: 6256
		Currently Allocated [B]: 15869344
		Currently Created [num]: 43
		Current Average [B]: 369054
		Maximum Triangles [num]: 52674
		Minimum Triangles [num]: 27
		Total Triangles [num]: 281182
		Maximum Meshes [num]: 2
		Minimum Meshes [num]: 1
		Total Meshes [num]: 52
	Top Level Acceleration Structure: 
		Maximum [B]: 2752
		Minimum [B]: 2752
		Currently Allocated [B]: 2752
		Total Allocated [B]: 2752
		Total Created [num]: 1
		Average Allocated [B]: 2752
		Maximum Instances [num]: 43
		Minimum Instances [num]: 43
		Total Instances [num]: 43
	Scratch Buffer: 
		Maximum [B]: 2344448
		Minimum [B]: 2344448
		Currently Allocated [B]: 2344448
		Total Allocated [B]: 2344448
		Total Created [num]: 1
		Average Allocated [B]: 2344448
ProfilingStack: 
[PROF] Profiling statistics: 
	Scopes entered : 3696
	Scopes exited : 3696
	Overhead per scope [ticks] : 162.1
	Threads entered : 3
	Threads exited : 0
	============================
		         Sum[Mt]   Parent[%]  Samples[num]       Avg[Mt]   Last/Curr[Mt]        Self[Mt]     Self[%] 	 Name

	main:

		   16351.880 Mt   100.000 %         1 x    16351.881 Mt    16351.880 Mt    11355.790 Mt    69.446 % 	 /

		      64.310 Mt     0.393 %         1 x       64.310 Mt        0.000 Mt        0.610 Mt     0.948 % 	 ./Initialize

		       2.865 Mt     4.454 %         1 x        2.865 Mt        0.000 Mt        2.865 Mt   100.000 % 	 ../TexturedRLInitialization

		       1.319 Mt     2.051 %         1 x        1.319 Mt        0.000 Mt        1.319 Mt   100.000 % 	 ../ShadowMapRLInitialization

		       6.302 Mt     9.800 %         1 x        6.302 Mt        0.000 Mt        6.302 Mt   100.000 % 	 ../AmbientOcclusionRLInitialization

		       5.274 Mt     8.201 %         1 x        5.274 Mt        0.000 Mt        5.274 Mt   100.000 % 	 ../RasterResolveRLInitialization

		       4.023 Mt     6.255 %         1 x        4.023 Mt        0.000 Mt        4.023 Mt   100.000 % 	 ../DeferredRLInitialization

		      39.553 Mt    61.504 %         1 x       39.553 Mt        0.000 Mt        1.477 Mt     3.735 % 	 ../RayTracingRLInitialization

		      38.076 Mt    96.265 %         1 x       38.076 Mt        0.000 Mt       38.076 Mt   100.000 % 	 .../PipelineBuild

		       4.365 Mt     6.788 %         1 x        4.365 Mt        0.000 Mt        4.365 Mt   100.000 % 	 ../ResolveRLInitialization

		    4931.779 Mt    30.160 %       213 x       23.154 Mt       20.675 Mt       13.567 Mt     0.275 % 	 ./Run

		     320.065 Mt     6.490 %       968 x        0.331 Mt        0.024 Mt      181.678 Mt    56.763 % 	 ../Update

		      58.455 Mt    18.264 %         1 x       58.455 Mt        0.000 Mt       54.464 Mt    93.171 % 	 .../ScenePrep

		       3.992 Mt     6.829 %         1 x        3.992 Mt        0.000 Mt        3.992 Mt   100.000 % 	 ..../SceneDesc-CreateScene

		      79.933 Mt    24.974 %        62 x        1.289 Mt        0.000 Mt       31.076 Mt    38.878 % 	 .../AccelerationStructureBuild

		      21.015 Mt    26.291 %        62 x        0.339 Mt        0.000 Mt       21.015 Mt   100.000 % 	 ..../BuildBottomLevelAS

		      27.841 Mt    34.831 %        62 x        0.449 Mt        0.000 Mt       27.841 Mt   100.000 % 	 ..../BuildTopLevelAS

		    4598.147 Mt    93.235 %       213 x       21.588 Mt       20.566 Mt        0.257 Mt     0.006 % 	 ../Render

		    4597.890 Mt    99.994 %       213 x       21.586 Mt       20.565 Mt     2886.372 Mt    62.776 % 	 .../Render

		    1711.518 Mt    37.224 %       211 x        8.111 Mt        6.942 Mt     1680.966 Mt    98.215 % 	 ..../RayTracing

		       8.233 Mt     0.481 %       211 x        0.039 Mt        0.002 Mt        0.315 Mt     3.827 % 	 ...../DeferredRLPrep

		       7.918 Mt    96.173 %         1 x        7.918 Mt        0.000 Mt        3.383 Mt    42.724 % 	 ....../PrepareForRendering

		       0.052 Mt     0.663 %         1 x        0.052 Mt        0.000 Mt        0.052 Mt   100.000 % 	 ......./PrepareMaterials

		       0.463 Mt     5.842 %         1 x        0.463 Mt        0.000 Mt        0.463 Mt   100.000 % 	 ......./BuildMaterialCache

		       4.020 Mt    50.770 %         1 x        4.020 Mt        0.000 Mt        4.020 Mt   100.000 % 	 ......./PrepareDrawBundle

		      22.319 Mt     1.304 %       211 x        0.106 Mt        0.001 Mt        7.176 Mt    32.152 % 	 ...../RayTracingRLPrep

		       1.532 Mt     6.863 %         1 x        1.532 Mt        0.000 Mt        1.532 Mt   100.000 % 	 ....../PrepareAccelerationStructures

		       0.060 Mt     0.267 %         1 x        0.060 Mt        0.000 Mt        0.060 Mt   100.000 % 	 ....../PrepareCache

		       0.527 Mt     2.363 %         1 x        0.527 Mt        0.000 Mt        0.527 Mt   100.000 % 	 ....../BuildCache

		      12.648 Mt    56.670 %         1 x       12.648 Mt        0.000 Mt        0.001 Mt     0.009 % 	 ....../BuildAccelerationStructures

		      12.647 Mt    99.991 %         1 x       12.647 Mt        0.000 Mt        0.683 Mt     5.401 % 	 ......./AccelerationStructureBuild

		      11.530 Mt    91.170 %         1 x       11.530 Mt        0.000 Mt       11.530 Mt   100.000 % 	 ......../BuildBottomLevelAS

		       0.434 Mt     3.428 %         1 x        0.434 Mt        0.000 Mt        0.434 Mt   100.000 % 	 ......../BuildTopLevelAS

		       0.376 Mt     1.685 %         1 x        0.376 Mt        0.000 Mt        0.376 Mt   100.000 % 	 ....../BuildShaderTables

		       0.001 Mt     0.000 %         1 x        0.001 Mt        0.001 Mt        0.001 Mt   100.000 % 	 ./Destroy

	WindowThread:

		   16349.831 Mt   100.000 %         1 x    16349.831 Mt    16349.831 Mt    16349.831 Mt   100.000 % 	 /

	GpuThread:

		    1472.155 Mt   100.000 %       274 x        5.373 Mt        6.462 Mt       -0.261 Mt    -0.018 % 	 /

		      13.244 Mt     0.900 %       213 x        0.062 Mt        0.108 Mt       13.244 Mt   100.000 % 	 ./Present

		      49.198 Mt     3.342 %        62 x        0.794 Mt        0.000 Mt        0.124 Mt     0.253 % 	 ./AccelerationStructureBuild

		      41.397 Mt    84.144 %        62 x        0.668 Mt        0.000 Mt       41.397 Mt   100.000 % 	 ../BuildBottomLevelASGpu

		       7.676 Mt    15.603 %        62 x        0.124 Mt        0.000 Mt        7.676 Mt   100.000 % 	 ../BuildTopLevelASGpu

		    1409.973 Mt    95.776 %       211 x        6.682 Mt        6.354 Mt        0.784 Mt     0.056 % 	 ./RayTracingGpu

		      59.816 Mt     4.242 %       211 x        0.283 Mt        0.246 Mt       59.816 Mt   100.000 % 	 ../DeferredRLGpu

		      20.631 Mt     1.463 %         1 x       20.631 Mt        0.000 Mt        0.002 Mt     0.009 % 	 ../AccelerationStructureBuild

		      20.349 Mt    98.632 %         1 x       20.349 Mt        0.000 Mt       20.349 Mt   100.000 % 	 .../BuildBottomLevelASGpu

		       0.280 Mt     1.359 %         1 x        0.280 Mt        0.000 Mt        0.280 Mt   100.000 % 	 .../BuildTopLevelASGpu

		     697.306 Mt    49.455 %       211 x        3.305 Mt        3.170 Mt      697.306 Mt   100.000 % 	 ../RayTracingRLGpu

		     631.436 Mt    44.784 %       211 x        2.993 Mt        2.934 Mt      631.436 Mt   100.000 % 	 ../ResolveRLGpu


	============================


