Configuration: 
Command line parameters  = 
Window width             = 1186
Window height            = 1219
Window title             = Direct3D App
Back-buffer count        = 3
Fill Triangles           = enabled
Triangles Font Clock-Wise= disabled
Fast Build AS            = disabled
Target FPS               = 0
Target UPS               = 60
Tearing                  = enabled
VSync                    = disabled
GUI rendering            = disabled
Debug layer              = enabled
GPU validation           = disabled
GPU profiling            = enabled
Render mode              = Ray Tracing
Loaded scene file        = 
Using testing scene      = disabled
BLAS duplication         = disabled
Rays per pixel           = 9
Quality preset           = Medium
Rasterization Quality    : 
  AO                     = enabled
  AO kernel size         = 4
  Shadows                = enabled
  Shadow map resolution  = 4096
  Shadow PCF kernel size = 4
  Reflections            = disabled
Ray Tracing Quality      : 
  AO                     = enabled
  AO rays                = 4
  AO kernel size         = 4
  Shadows                = enabled
  Shadow rays            = 4
  Shadow kernel size     = 4
  Reflections            = disabled

ProfilingAggregator: 
Render	,	75.5131	,	49.538	,	18.2076	,	15.7067	,	15.8324	,	16.7767
Update	,	0.0153	,	0.0152	,	0.0156	,	0.0225	,	0.0187	,	0.0713
RayTracing	,	40.4191	,	37.3569	,	9.4941	,	8.2129	,	8.3412	,	8.2025
RayTracingGpu	,	15.1194	,	3.30566	,	1.96122	,	1.69542	,	1.21562	,	1.56714
DeferredRLGpu	,	0.03392	,	0.390688	,	0.214176	,	0.135968	,	0.066432	,	0.097472
RayTracingRLGpu	,	0.05168	,	1.52365	,	0.870816	,	0.68944	,	0.306464	,	0.504544
ResolveRLGpu	,	0.82928	,	1.38867	,	0.87392	,	0.867904	,	0.840448	,	0.962752
Index	,	0	,	1	,	2	,	3	,	4	,	5

ProfilingBuildAggregator: 
BuildBottomLevelAS	,	16.1302
BuildBottomLevelASGpu	,	14.0291
BuildTopLevelAS	,	0.7238
BuildTopLevelASGpu	,	0.165728
Triangles	,	281182
Meshes	,	52
BottomLevels	,	43
TopLevelSize	,	2752
BottomLevelsSize	,	15869344
Index	,	0

FpsAggregator: 
FPS	,	1	,	1	,	42	,	43	,	41	,	41
UPS	,	46	,	48	,	170	,	60	,	61	,	60
FrameTime	,	1575.57	,	1845.17	,	23.8858	,	23.2973	,	24.9063	,	24.5664
GigaRays/s	,	0.00449226	,	0.0065457	,	0.505653	,	0.518425	,	0.484934	,	0.491643
Index	,	0	,	1	,	2	,	3	,	4	,	5


AccelerationStructure: 
Ray tracing acceleration structure building statistics: 
	Bottom Level Acceleration Structure: 
		Maximum [B]: 2940416
		Minimum [B]: 6256
		Currently Allocated [B]: 15869344
		Currently Created [num]: 43
		Current Average [B]: 369054
		Maximum Triangles [num]: 52674
		Minimum Triangles [num]: 27
		Total Triangles [num]: 281182
		Maximum Meshes [num]: 2
		Minimum Meshes [num]: 1
		Total Meshes [num]: 52
	Top Level Acceleration Structure: 
		Maximum [B]: 2752
		Minimum [B]: 2752
		Currently Allocated [B]: 2752
		Total Allocated [B]: 2752
		Total Created [num]: 1
		Average Allocated [B]: 2752
		Maximum Instances [num]: 43
		Minimum Instances [num]: 43
		Total Instances [num]: 43
	Scratch Buffer: 
		Maximum [B]: 2344448
		Minimum [B]: 2344448
		Currently Allocated [B]: 2344448
		Total Allocated [B]: 2344448
		Total Created [num]: 1
		Average Allocated [B]: 2344448
ProfilingStack: 
[PROF] Profiling statistics: 
	Scopes entered : 3132
	Scopes exited : 3132
	Overhead per scope [ticks] : 161.6
	Threads entered : 3
	Threads exited : 0
	============================
		         Sum[Mt]   Parent[%]  Samples[num]       Avg[Mt]   Last/Curr[Mt]        Self[Mt]     Self[%] 	 Name

	main:

		    8622.530 Mt   100.000 %         1 x     8622.531 Mt     8622.530 Mt     4388.214 Mt    50.892 % 	 /

		      73.657 Mt     0.854 %         1 x       73.657 Mt        0.000 Mt        1.052 Mt     1.428 % 	 ./Initialize

		       3.103 Mt     4.213 %         1 x        3.103 Mt        0.000 Mt        3.103 Mt   100.000 % 	 ../TexturedRLInitialization

		       1.538 Mt     2.088 %         1 x        1.538 Mt        0.000 Mt        1.538 Mt   100.000 % 	 ../ShadowMapRLInitialization

		       6.315 Mt     8.574 %         1 x        6.315 Mt        0.000 Mt        6.315 Mt   100.000 % 	 ../AmbientOcclusionRLInitialization

		       5.435 Mt     7.379 %         1 x        5.435 Mt        0.000 Mt        5.435 Mt   100.000 % 	 ../RasterResolveRLInitialization

		       5.092 Mt     6.913 %         1 x        5.092 Mt        0.000 Mt        5.092 Mt   100.000 % 	 ../DeferredRLInitialization

		      46.563 Mt    63.216 %         1 x       46.563 Mt        0.000 Mt        1.445 Mt     3.104 % 	 ../RayTracingRLInitialization

		      45.117 Mt    96.896 %         1 x       45.117 Mt        0.000 Mt       45.117 Mt   100.000 % 	 .../PipelineBuild

		       4.559 Mt     6.190 %         1 x        4.559 Mt        0.000 Mt        4.559 Mt   100.000 % 	 ../ResolveRLInitialization

		    4160.659 Mt    48.253 %       203 x       20.496 Mt       18.465 Mt       11.171 Mt     0.268 % 	 ./Run

		     750.451 Mt    18.037 %       500 x        1.501 Mt        0.081 Mt      163.055 Mt    21.728 % 	 ../Update

		     516.298 Mt    68.798 %         1 x      516.298 Mt        0.000 Mt      510.411 Mt    98.860 % 	 .../ScenePrep

		       5.887 Mt     1.140 %         1 x        5.887 Mt        0.000 Mt        5.887 Mt   100.000 % 	 ..../SceneDesc-CreateScene

		      71.098 Mt     9.474 %        62 x        1.147 Mt        0.000 Mt       24.617 Mt    34.624 % 	 .../AccelerationStructureBuild

		      20.247 Mt    28.477 %        62 x        0.327 Mt        0.000 Mt       20.247 Mt   100.000 % 	 ..../BuildBottomLevelAS

		      26.235 Mt    36.899 %        62 x        0.423 Mt        0.000 Mt       26.235 Mt   100.000 % 	 ..../BuildTopLevelAS

		    3399.037 Mt    81.695 %       203 x       16.744 Mt       18.381 Mt        0.285 Mt     0.008 % 	 ../Render

		    3398.752 Mt    99.992 %       203 x       16.743 Mt       18.379 Mt     1709.536 Mt    50.299 % 	 .../Render

		    1689.216 Mt    49.701 %       203 x        8.321 Mt        9.558 Mt     1656.397 Mt    98.057 % 	 ..../RayTracing

		       5.107 Mt     0.302 %       203 x        0.025 Mt        0.001 Mt        0.287 Mt     5.625 % 	 ...../DeferredRLPrep

		       4.820 Mt    94.375 %         1 x        4.820 Mt        0.000 Mt        2.481 Mt    51.465 % 	 ....../PrepareForRendering

		       0.065 Mt     1.346 %         1 x        0.065 Mt        0.000 Mt        0.065 Mt   100.000 % 	 ......./PrepareMaterials

		       0.273 Mt     5.668 %         1 x        0.273 Mt        0.000 Mt        0.273 Mt   100.000 % 	 ......./BuildMaterialCache

		       2.001 Mt    41.521 %         1 x        2.001 Mt        0.000 Mt        2.001 Mt   100.000 % 	 ......./PrepareDrawBundle

		      27.712 Mt     1.640 %       203 x        0.137 Mt        0.001 Mt        7.390 Mt    26.668 % 	 ...../RayTracingRLPrep

		       1.380 Mt     4.980 %         1 x        1.380 Mt        0.000 Mt        1.380 Mt   100.000 % 	 ....../PrepareAccelerationStructures

		       0.049 Mt     0.178 %         1 x        0.049 Mt        0.000 Mt        0.049 Mt   100.000 % 	 ....../PrepareCache

		       0.795 Mt     2.870 %         1 x        0.795 Mt        0.000 Mt        0.795 Mt   100.000 % 	 ....../BuildCache

		      17.623 Mt    63.594 %         1 x       17.623 Mt        0.000 Mt        0.002 Mt     0.011 % 	 ....../BuildAccelerationStructures

		      17.621 Mt    99.989 %         1 x       17.621 Mt        0.000 Mt        0.767 Mt     4.352 % 	 ......./AccelerationStructureBuild

		      16.130 Mt    91.541 %         1 x       16.130 Mt        0.000 Mt       16.130 Mt   100.000 % 	 ......../BuildBottomLevelAS

		       0.724 Mt     4.108 %         1 x        0.724 Mt        0.000 Mt        0.724 Mt   100.000 % 	 ......../BuildTopLevelAS

		       0.474 Mt     1.711 %         1 x        0.474 Mt        0.000 Mt        0.474 Mt   100.000 % 	 ....../BuildShaderTables

		       0.001 Mt     0.000 %         1 x        0.001 Mt        0.001 Mt        0.001 Mt   100.000 % 	 ./Destroy

	WindowThread:

		    8620.698 Mt   100.000 %         1 x     8620.698 Mt     8620.698 Mt     8620.698 Mt   100.000 % 	 /

	GpuThread:

		     413.502 Mt   100.000 %       264 x        1.566 Mt        1.766 Mt       -0.055 Mt    -0.013 % 	 /

		      20.808 Mt     5.032 %        62 x        0.336 Mt        0.000 Mt        0.055 Mt     0.265 % 	 ./AccelerationStructureBuild

		      17.241 Mt    82.855 %        62 x        0.278 Mt        0.000 Mt       17.241 Mt   100.000 % 	 ../BuildBottomLevelASGpu

		       3.512 Mt    16.879 %        62 x        0.057 Mt        0.000 Mt        3.512 Mt   100.000 % 	 ../BuildTopLevelASGpu

		     387.491 Mt    93.709 %       203 x        1.909 Mt        1.614 Mt        0.407 Mt     0.105 % 	 ./RayTracingGpu

		      31.324 Mt     8.084 %       203 x        0.154 Mt        0.098 Mt       31.324 Mt   100.000 % 	 ../DeferredRLGpu

		      14.196 Mt     3.664 %         1 x       14.196 Mt        0.000 Mt        0.001 Mt     0.009 % 	 ../AccelerationStructureBuild

		      14.029 Mt    98.823 %         1 x       14.029 Mt        0.000 Mt       14.029 Mt   100.000 % 	 .../BuildBottomLevelASGpu

		       0.166 Mt     1.167 %         1 x        0.166 Mt        0.000 Mt        0.166 Mt   100.000 % 	 .../BuildTopLevelASGpu

		     150.550 Mt    38.852 %       203 x        0.742 Mt        0.503 Mt      150.550 Mt   100.000 % 	 ../RayTracingRLGpu

		     191.014 Mt    49.295 %       203 x        0.941 Mt        1.010 Mt      191.014 Mt   100.000 % 	 ../ResolveRLGpu

		       5.258 Mt     1.272 %       203 x        0.026 Mt        0.152 Mt        5.258 Mt   100.000 % 	 ./Present


	============================


