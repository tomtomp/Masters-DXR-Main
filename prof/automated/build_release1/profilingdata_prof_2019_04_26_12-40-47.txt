Configuration: 
Command line parameters  = 
Window width             = 1186
Window height            = 1219
Window title             = Direct3D App
Back-buffer count        = 3
Fill Triangles           = enabled
Triangles Font Clock-Wise= disabled
Fast Build AS            = disabled
Target FPS               = 0
Target UPS               = 60
Tearing                  = enabled
VSync                    = disabled
GUI rendering            = disabled
Debug layer              = enabled
GPU validation           = disabled
GPU profiling            = enabled
Render mode              = Ray Tracing
Loaded scene file        = 
Using testing scene      = disabled
BLAS duplication         = disabled
Rays per pixel           = 14
Quality preset           = High
Rasterization Quality    : 
  AO                     = enabled
  AO kernel size         = 4
  Shadows                = enabled
  Shadow map resolution  = 4096
  Shadow PCF kernel size = 4
  Reflections            = enabled
Ray Tracing Quality      : 
  AO                     = enabled
  AO rays                = 4
  AO kernel size         = 4
  Shadows                = enabled
  Shadow rays            = 4
  Shadow kernel size     = 4
  Reflections            = enabled

ProfilingAggregator: 
Render	,	16212.3	,	23.3061	,	15.0487
Update	,	0.0049	,	0.0109	,	0.0399
RayTracing	,	16099.3	,	10.7111	,	6.8123
RayTracingGpu	,	106.293	,	5.42874	,	2.21898
DeferredRLGpu	,	0.551296	,	0.43664	,	0.187744
RayTracingRLGpu	,	0.839968	,	2.26413	,	1.64323
ResolveRLGpu	,	2.35008	,	2.65578	,	0.385632
Index	,	0	,	1	,	2

ProfilingBuildAggregator: 
BuildBottomLevelAS	,	31.3231
BuildBottomLevelASGpu	,	101
BuildTopLevelAS	,	0.7923
BuildTopLevelASGpu	,	1.46851
Triangles	,	281182
Meshes	,	52
BottomLevels	,	43
TopLevelSize	,	2752
BottomLevelsSize	,	15869344
Index	,	0

FpsAggregator: 
FPS	,	1	,	1	,	36
UPS	,	58	,	992	,	230
FrameTime	,	17506.2	,	2852.9	,	27.8004
GigaRays/s	,	0.000628924	,	0.00658552	,	0.675813
Index	,	0	,	1	,	2


AccelerationStructure: 
Ray tracing acceleration structure building statistics: 
	Bottom Level Acceleration Structure: 
		Maximum [B]: 2940416
		Minimum [B]: 6256
		Currently Allocated [B]: 15869344
		Currently Created [num]: 43
		Current Average [B]: 369054
		Maximum Triangles [num]: 52674
		Minimum Triangles [num]: 27
		Total Triangles [num]: 281182
		Maximum Meshes [num]: 2
		Minimum Meshes [num]: 1
		Total Meshes [num]: 52
	Top Level Acceleration Structure: 
		Maximum [B]: 2752
		Minimum [B]: 2752
		Currently Allocated [B]: 2752
		Total Allocated [B]: 2752
		Total Created [num]: 1
		Average Allocated [B]: 2752
		Maximum Instances [num]: 43
		Minimum Instances [num]: 43
		Total Instances [num]: 43
	Scratch Buffer: 
		Maximum [B]: 2344448
		Minimum [B]: 2344448
		Currently Allocated [B]: 2344448
		Total Allocated [B]: 2344448
		Total Created [num]: 1
		Average Allocated [B]: 2344448
ProfilingStack: 
[PROF] Profiling statistics: 
	Scopes entered : 2158
	Scopes exited : 2158
	Overhead per scope [ticks] : 163.8
	Threads entered : 3
	Threads exited : 0
	============================
		         Sum[Mt]   Parent[%]  Samples[num]       Avg[Mt]   Last/Curr[Mt]        Self[Mt]     Self[%] 	 Name

	main:

		   21702.168 Mt   100.000 %         1 x    21702.168 Mt    21702.168 Mt     4118.724 Mt    18.978 % 	 /

		     297.138 Mt     1.369 %         1 x      297.138 Mt        0.000 Mt        1.114 Mt     0.375 % 	 ./Initialize

		       6.235 Mt     2.098 %         1 x        6.235 Mt        0.000 Mt        6.235 Mt   100.000 % 	 ../TexturedRLInitialization

		       2.084 Mt     0.701 %         1 x        2.084 Mt        0.000 Mt        2.084 Mt   100.000 % 	 ../ShadowMapRLInitialization

		       8.225 Mt     2.768 %         1 x        8.225 Mt        0.000 Mt        8.225 Mt   100.000 % 	 ../AmbientOcclusionRLInitialization

		      10.245 Mt     3.448 %         1 x       10.245 Mt        0.000 Mt       10.245 Mt   100.000 % 	 ../RasterResolveRLInitialization

		       4.965 Mt     1.671 %         1 x        4.965 Mt        0.000 Mt        4.965 Mt   100.000 % 	 ../DeferredRLInitialization

		     253.033 Mt    85.157 %         1 x      253.033 Mt        0.000 Mt        1.715 Mt     0.678 % 	 ../RayTracingRLInitialization

		     251.318 Mt    99.322 %         1 x      251.318 Mt        0.000 Mt      251.318 Mt   100.000 % 	 .../PipelineBuild

		      11.237 Mt     3.782 %         1 x       11.237 Mt        0.000 Mt       11.237 Mt   100.000 % 	 ../ResolveRLInitialization

		   17286.306 Mt    79.652 %        43 x      402.007 Mt       15.444 Mt        9.019 Mt     0.052 % 	 ./Run

		     308.061 Mt     1.782 %      1286 x        0.240 Mt        0.042 Mt      176.784 Mt    57.386 % 	 ../Update

		      62.121 Mt    20.165 %         1 x       62.121 Mt        0.000 Mt       49.881 Mt    80.297 % 	 .../ScenePrep

		      12.240 Mt    19.703 %         1 x       12.240 Mt        0.000 Mt       12.240 Mt   100.000 % 	 ..../SceneDesc-CreateScene

		      69.156 Mt    22.449 %        62 x        1.115 Mt        0.000 Mt       24.400 Mt    35.282 % 	 .../AccelerationStructureBuild

		      18.892 Mt    27.319 %        62 x        0.305 Mt        0.000 Mt       18.892 Mt   100.000 % 	 ..../BuildBottomLevelAS

		      25.864 Mt    37.399 %        62 x        0.417 Mt        0.000 Mt       25.864 Mt   100.000 % 	 ..../BuildTopLevelAS

		   16969.226 Mt    98.166 %        43 x      394.633 Mt       15.399 Mt        0.064 Mt     0.000 % 	 ../Render

		   16969.162 Mt   100.000 %        43 x      394.632 Mt       15.398 Mt      557.276 Mt     3.284 % 	 .../Render

		   16411.886 Mt    96.716 %        43 x      381.672 Mt        7.118 Mt      320.507 Mt     1.953 % 	 ..../RayTracing

		    3399.526 Mt    20.714 %        43 x       79.059 Mt        0.001 Mt        0.058 Mt     0.002 % 	 ...../DeferredRLPrep

		    3399.468 Mt    99.998 %         1 x     3399.468 Mt        0.000 Mt        3.300 Mt     0.097 % 	 ....../PrepareForRendering

		       0.063 Mt     0.002 %         1 x        0.063 Mt        0.000 Mt        0.063 Mt   100.000 % 	 ......./PrepareMaterials

		    3390.931 Mt    99.749 %         1 x     3390.931 Mt        0.000 Mt     3390.931 Mt   100.000 % 	 ......./BuildMaterialCache

		       5.174 Mt     0.152 %         1 x        5.174 Mt        0.000 Mt        5.174 Mt   100.000 % 	 ......./PrepareDrawBundle

		   12691.854 Mt    77.333 %        43 x      295.159 Mt        0.001 Mt    11198.374 Mt    88.233 % 	 ...../RayTracingRLPrep

		       1.337 Mt     0.011 %         1 x        1.337 Mt        0.000 Mt        1.337 Mt   100.000 % 	 ....../PrepareAccelerationStructures

		       0.052 Mt     0.000 %         1 x        0.052 Mt        0.000 Mt        0.052 Mt   100.000 % 	 ....../PrepareCache

		    1447.241 Mt    11.403 %         1 x     1447.241 Mt        0.000 Mt     1447.241 Mt   100.000 % 	 ....../BuildCache

		      44.088 Mt     0.347 %         1 x       44.088 Mt        0.000 Mt        0.003 Mt     0.007 % 	 ....../BuildAccelerationStructures

		      44.084 Mt    99.993 %         1 x       44.084 Mt        0.000 Mt       11.969 Mt    27.150 % 	 ......./AccelerationStructureBuild

		      31.323 Mt    71.053 %         1 x       31.323 Mt        0.000 Mt       31.323 Mt   100.000 % 	 ......../BuildBottomLevelAS

		       0.792 Mt     1.797 %         1 x        0.792 Mt        0.000 Mt        0.792 Mt   100.000 % 	 ......../BuildTopLevelAS

		       0.762 Mt     0.006 %         1 x        0.762 Mt        0.000 Mt        0.762 Mt   100.000 % 	 ....../BuildShaderTables

		       0.001 Mt     0.000 %         1 x        0.001 Mt        0.001 Mt        0.001 Mt   100.000 % 	 ./Destroy

	WindowThread:

		   21699.922 Mt   100.000 %         1 x    21699.923 Mt    21699.922 Mt    21699.922 Mt   100.000 % 	 /

	GpuThread:

		     292.329 Mt   100.000 %       104 x        2.811 Mt        2.416 Mt       -0.124 Mt    -0.042 % 	 /

		      20.827 Mt     7.124 %        62 x        0.336 Mt        0.000 Mt        0.056 Mt     0.270 % 	 ./AccelerationStructureBuild

		      17.268 Mt    82.914 %        62 x        0.279 Mt        0.000 Mt       17.268 Mt   100.000 % 	 ../BuildBottomLevelASGpu

		       3.502 Mt    16.816 %        62 x        0.056 Mt        0.000 Mt        3.502 Mt   100.000 % 	 ../BuildTopLevelASGpu

		     268.131 Mt    91.722 %        43 x        6.236 Mt        2.414 Mt        0.283 Mt     0.105 % 	 ./RayTracingGpu

		      18.381 Mt     6.855 %        43 x        0.427 Mt        0.189 Mt       18.381 Mt   100.000 % 	 ../DeferredRLGpu

		     102.471 Mt    38.217 %         1 x      102.471 Mt        0.000 Mt        0.002 Mt     0.002 % 	 ../AccelerationStructureBuild

		     101.000 Mt    98.565 %         1 x      101.000 Mt        0.000 Mt      101.000 Mt   100.000 % 	 .../BuildBottomLevelASGpu

		       1.469 Mt     1.433 %         1 x        1.469 Mt        0.000 Mt        1.469 Mt   100.000 % 	 .../BuildTopLevelASGpu

		     113.269 Mt    42.244 %        43 x        2.634 Mt        1.840 Mt      113.269 Mt   100.000 % 	 ../RayTracingRLGpu

		      33.728 Mt    12.579 %        43 x        0.784 Mt        0.384 Mt       33.728 Mt   100.000 % 	 ../ResolveRLGpu

		       3.495 Mt     1.196 %        43 x        0.081 Mt        0.002 Mt        3.495 Mt   100.000 % 	 ./Present


	============================


