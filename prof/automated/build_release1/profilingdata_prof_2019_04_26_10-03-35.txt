Configuration: 
Command line parameters  = 
Window width             = 1186
Window height            = 1219
Window title             = Direct3D App
Back-buffer count        = 3
Fill Triangles           = enabled
Triangles Font Clock-Wise= disabled
Fast Build AS            = disabled
Target FPS               = 0
Target UPS               = 60
Tearing                  = enabled
VSync                    = disabled
GUI rendering            = disabled
Debug layer              = enabled
GPU validation           = disabled
GPU profiling            = enabled
Render mode              = Ray Tracing
Loaded scene file        = 
Using testing scene      = disabled
BLAS duplication         = disabled
Rays per pixel           = 14
Quality preset           = High
Rasterization Quality    : 
  AO                     = enabled
  AO kernel size         = 4
  Shadows                = enabled
  Shadow map resolution  = 4096
  Shadow PCF kernel size = 4
  Reflections            = enabled
Ray Tracing Quality      : 
  AO                     = enabled
  AO rays                = 4
  AO kernel size         = 4
  Shadows                = enabled
  Shadow rays            = 4
  Shadow kernel size     = 4
  Reflections            = enabled

ProfilingAggregator: 
Render	,	6.9947	,	6.2189	,	5.8386	,	6.1891	,	5.6576
Update	,	0.0072	,	0.0431	,	0.0408	,	0.0425	,	0.0391
RayTracing	,	1.9912	,	2.398	,	2.0019	,	2.2162	,	1.9931
RayTracingGpu	,	3.22355	,	2.21603	,	2.4265	,	2.31424	,	2.2192
DeferredRLGpu	,	0.5056	,	0.360576	,	0.363104	,	0.3584	,	0.360864
RayTracingRLGpu	,	2.19872	,	1.46522	,	1.66842	,	1.46016	,	1.46883
ResolveRLGpu	,	0.514528	,	0.38704	,	0.391904	,	0.491232	,	0.386048
Index	,	0	,	1	,	2	,	3	,	4

ProfilingBuildAggregator: 
BuildBottomLevelAS	,	16.5786
BuildBottomLevelASGpu	,	13.0712
BuildTopLevelAS	,	0.4618
BuildTopLevelASGpu	,	0.184704
Triangles	,	281182
Meshes	,	52
BottomLevels	,	43
TopLevelSize	,	2752
BottomLevelsSize	,	15869344
Index	,	0

FpsAggregator: 
FPS	,	3	,	53	,	58	,	60	,	60
UPS	,	304	,	63	,	60	,	61	,	60
FrameTime	,	341.242	,	19.3164	,	17.2699	,	16.8381	,	16.763
GigaRays/s	,	0.0550574	,	0.972638	,	1.0879	,	1.11579	,	1.12079
Index	,	0	,	1	,	2	,	3	,	4


AccelerationStructure: 
Ray tracing acceleration structure building statistics: 
	Bottom Level Acceleration Structure: 
		Maximum [B]: 2940416
		Minimum [B]: 6256
		Currently Allocated [B]: 15869344
		Currently Created [num]: 43
		Current Average [B]: 369054
		Maximum Triangles [num]: 52674
		Minimum Triangles [num]: 27
		Total Triangles [num]: 281182
		Maximum Meshes [num]: 2
		Minimum Meshes [num]: 1
		Total Meshes [num]: 52
	Top Level Acceleration Structure: 
		Maximum [B]: 2752
		Minimum [B]: 2752
		Currently Allocated [B]: 2752
		Total Allocated [B]: 2752
		Total Created [num]: 1
		Average Allocated [B]: 2752
		Maximum Instances [num]: 43
		Minimum Instances [num]: 43
		Total Instances [num]: 43
	Scratch Buffer: 
		Maximum [B]: 2344448
		Minimum [B]: 2344448
		Currently Allocated [B]: 2344448
		Total Allocated [B]: 2344448
		Total Created [num]: 1
		Average Allocated [B]: 2344448
ProfilingStack: 
[PROF] Profiling statistics: 
	Scopes entered : 3605
	Scopes exited : 3605
	Overhead per scope [ticks] : 165.1
	Threads entered : 3
	Threads exited : 0
	============================
		         Sum[Mt]   Parent[%]  Samples[num]       Avg[Mt]   Last/Curr[Mt]        Self[Mt]     Self[%] 	 Name

	main:

		    9588.925 Mt   100.000 %         1 x     9588.925 Mt     9588.925 Mt     7020.118 Mt    73.211 % 	 /

		      69.437 Mt     0.724 %         1 x       69.437 Mt        0.000 Mt        0.977 Mt     1.406 % 	 ./Initialize

		       2.455 Mt     3.536 %         1 x        2.455 Mt        0.000 Mt        2.455 Mt   100.000 % 	 ../TexturedRLInitialization

		       1.353 Mt     1.949 %         1 x        1.353 Mt        0.000 Mt        1.353 Mt   100.000 % 	 ../ShadowMapRLInitialization

		       6.652 Mt     9.580 %         1 x        6.652 Mt        0.000 Mt        6.652 Mt   100.000 % 	 ../AmbientOcclusionRLInitialization

		       4.837 Mt     6.966 %         1 x        4.837 Mt        0.000 Mt        4.837 Mt   100.000 % 	 ../RasterResolveRLInitialization

		       4.035 Mt     5.812 %         1 x        4.035 Mt        0.000 Mt        4.035 Mt   100.000 % 	 ../DeferredRLInitialization

		      44.568 Mt    64.185 %         1 x       44.568 Mt        0.000 Mt        1.490 Mt     3.343 % 	 ../RayTracingRLInitialization

		      43.078 Mt    96.657 %         1 x       43.078 Mt        0.000 Mt       43.078 Mt   100.000 % 	 .../PipelineBuild

		       4.560 Mt     6.567 %         1 x        4.560 Mt        0.000 Mt        4.560 Mt   100.000 % 	 ../ResolveRLInitialization

		    2499.369 Mt    26.065 %       241 x       10.371 Mt        5.972 Mt        7.805 Mt     0.312 % 	 ./Run

		     904.405 Mt    36.185 %       555 x        1.630 Mt        0.042 Mt      289.863 Mt    32.050 % 	 ../Update

		     535.467 Mt    59.207 %         1 x      535.467 Mt        0.000 Mt      529.676 Mt    98.919 % 	 .../ScenePrep

		       5.791 Mt     1.081 %         1 x        5.791 Mt        0.000 Mt        5.791 Mt   100.000 % 	 ..../SceneDesc-CreateScene

		      79.076 Mt     8.743 %        62 x        1.275 Mt        0.000 Mt       27.412 Mt    34.665 % 	 .../AccelerationStructureBuild

		      22.112 Mt    27.964 %        62 x        0.357 Mt        0.000 Mt       22.112 Mt   100.000 % 	 ..../BuildBottomLevelAS

		      29.552 Mt    37.371 %        62 x        0.477 Mt        0.000 Mt       29.552 Mt   100.000 % 	 ..../BuildTopLevelAS

		    1587.159 Mt    63.502 %       241 x        6.586 Mt        5.927 Mt        0.297 Mt     0.019 % 	 ../Render

		    1586.862 Mt    99.981 %       241 x        6.584 Mt        5.926 Mt     1016.133 Mt    64.034 % 	 .../Render

		     570.730 Mt    35.966 %       241 x        2.368 Mt        2.034 Mt      545.072 Mt    95.504 % 	 ..../RayTracing

		       3.092 Mt     0.542 %       241 x        0.013 Mt        0.001 Mt        0.240 Mt     7.769 % 	 ...../DeferredRLPrep

		       2.852 Mt    92.231 %         1 x        2.852 Mt        0.000 Mt        1.608 Mt    56.381 % 	 ....../PrepareForRendering

		       0.065 Mt     2.269 %         1 x        0.065 Mt        0.000 Mt        0.065 Mt   100.000 % 	 ......./PrepareMaterials

		       0.262 Mt     9.202 %         1 x        0.262 Mt        0.000 Mt        0.262 Mt   100.000 % 	 ......./BuildMaterialCache

		       0.917 Mt    32.148 %         1 x        0.917 Mt        0.000 Mt        0.917 Mt   100.000 % 	 ......./PrepareDrawBundle

		      22.566 Mt     3.954 %       241 x        0.094 Mt        0.001 Mt        2.420 Mt    10.725 % 	 ...../RayTracingRLPrep

		       1.272 Mt     5.637 %         1 x        1.272 Mt        0.000 Mt        1.272 Mt   100.000 % 	 ....../PrepareAccelerationStructures

		       0.049 Mt     0.218 %         1 x        0.049 Mt        0.000 Mt        0.049 Mt   100.000 % 	 ....../PrepareCache

		       0.608 Mt     2.694 %         1 x        0.608 Mt        0.000 Mt        0.608 Mt   100.000 % 	 ....../BuildCache

		      17.811 Mt    78.930 %         1 x       17.811 Mt        0.000 Mt        0.002 Mt     0.011 % 	 ....../BuildAccelerationStructures

		      17.809 Mt    99.989 %         1 x       17.809 Mt        0.000 Mt        0.769 Mt     4.317 % 	 ......./AccelerationStructureBuild

		      16.579 Mt    93.090 %         1 x       16.579 Mt        0.000 Mt       16.579 Mt   100.000 % 	 ......../BuildBottomLevelAS

		       0.462 Mt     2.593 %         1 x        0.462 Mt        0.000 Mt        0.462 Mt   100.000 % 	 ......../BuildTopLevelAS

		       0.405 Mt     1.796 %         1 x        0.405 Mt        0.000 Mt        0.405 Mt   100.000 % 	 ....../BuildShaderTables

		       0.001 Mt     0.000 %         1 x        0.001 Mt        0.001 Mt        0.001 Mt   100.000 % 	 ./Destroy

	WindowThread:

		    9586.944 Mt   100.000 %         1 x     9586.944 Mt     9586.944 Mt     9586.944 Mt   100.000 % 	 /

	GpuThread:

		     692.424 Mt   100.000 %       302 x        2.293 Mt        2.420 Mt       -0.506 Mt    -0.073 % 	 /

		     104.274 Mt    15.059 %        62 x        1.682 Mt        0.000 Mt        0.222 Mt     0.213 % 	 ./AccelerationStructureBuild

		      85.196 Mt    81.704 %        62 x        1.374 Mt        0.000 Mt       85.196 Mt   100.000 % 	 ../BuildBottomLevelASGpu

		      18.857 Mt    18.084 %        62 x        0.304 Mt        0.000 Mt       18.857 Mt   100.000 % 	 ../BuildTopLevelASGpu

		     586.935 Mt    84.765 %       241 x        2.435 Mt        2.418 Mt        0.883 Mt     0.150 % 	 ./RayTracingGpu

		      87.638 Mt    14.932 %       241 x        0.364 Mt        0.364 Mt       87.638 Mt   100.000 % 	 ../DeferredRLGpu

		      13.257 Mt     2.259 %         1 x       13.257 Mt        0.000 Mt        0.001 Mt     0.010 % 	 ../AccelerationStructureBuild

		      13.071 Mt    98.597 %         1 x       13.071 Mt        0.000 Mt       13.071 Mt   100.000 % 	 .../BuildBottomLevelASGpu

		       0.185 Mt     1.393 %         1 x        0.185 Mt        0.000 Mt        0.185 Mt   100.000 % 	 .../BuildTopLevelASGpu

		     388.349 Mt    66.166 %       241 x        1.611 Mt        1.666 Mt      388.349 Mt   100.000 % 	 ../RayTracingRLGpu

		      96.808 Mt    16.494 %       241 x        0.402 Mt        0.386 Mt       96.808 Mt   100.000 % 	 ../ResolveRLGpu

		       1.721 Mt     0.248 %       241 x        0.007 Mt        0.002 Mt        1.721 Mt   100.000 % 	 ./Present


	============================


