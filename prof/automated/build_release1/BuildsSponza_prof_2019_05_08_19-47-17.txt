Configuration: 
Command line parameters  = E:\programming\FIT-DIP\Masters-DXR-Main-git\build\Measurement\x64\Release\Measurement.exe --scene Sponza/Sponza.gltf --profile-builds --profile-output BuildsSponza 
Build type               = Release
Device name              = NVIDIA GeForce RTX 2080 Ti
Device memory MB         = 11049
Ray Tracing backend      = Hardware
Window width             = 1024
Window height            = 768
Window title             = Direct3D App
Back-buffer count        = 3
Fill Triangles           = enabled
Triangles Font Clock-Wise= enabled
Fast Build AS            = disabled
Target FPS               = 60
Target UPS               = 60
Tearing                  = disabled
VSync                    = disabled
GUI rendering            = enabled
Debug layer              = disabled
GPU validation           = disabled
GPU profiling            = enabled
Render mode              = Rasterization
Loaded scene file        = Sponza/Sponza.gltf
Using testing scene      = disabled
Duplication              = 1
Duplication in cube      = disabled
Duplication offset       = 10
BLAS duplication         = disabled
Rays per pixel           = 13
Quality preset           = High
Rasterization Quality    : 
  AO                     = enabled
  AO kernel size         = 4
  Shadows                = enabled
  Shadow map resolution  = 4096
  Shadow PCF kernel size = 4
  Reflections            = enabled
Ray Tracing Quality      : 
  AO                     = enabled
  AO rays                = 4
  AO kernel size         = 4
  Shadows                = enabled
  Shadow rays            = 4
  Shadow kernel size     = 4
  Reflections            = enabled

ProfilingAggregator: 

ProfilingBuildAggregator: 
BuildBottomLevelAS	,	1.9483	,	1.9483	,	1.9483	,	1.9483	,	1.9483	,	1.9483	,	1.9483	,	1.9483	,	1.9483	,	1.9483	,	1.9483	,	1.9483	,	1.9483	,	1.9483	,	1.9483	,	1.9483	,	1.9483	,	1.9483	,	1.9483	,	1.9483	,	1.9483	,	1.9483	,	1.9483	,	1.9483	,	1.9483	,	1.9483	,	1.9483	,	1.9483	,	1.9483	,	1.9483	,	1.9483	,	1.9483	,	1.9483	,	1.9483	,	1.9483	,	1.9483	,	1.9483	,	1.9483	,	1.9483	,	1.9483	,	1.9483	,	1.9483	,	1.9483	,	1.9483	,	1.9483	,	1.9483	,	1.9483	,	1.9483	,	1.9483	,	1.9483	,	1.9483	,	1.9483	,	1.9483	,	1.9483	,	1.9483	,	1.9483	,	1.9483	,	1.9483	,	1.9483	,	1.9483	,	1.9483	,	1.9483	,	1.9483	,	1.9483	,	1.9483	,	1.9483	,	1.9483	,	1.9483	,	1.9483	,	1.9483	,	1.9483	,	1.9483	,	1.9483	,	1.9483	,	1.9483	,	1.9483	,	1.9483	,	1.9483	,	1.9483	,	1.9483	,	1.9483	,	1.9483	,	1.9483	,	1.9483	,	1.9483	,	1.9483	,	1.9483	,	1.9483	,	1.9483	,	1.9483	,	1.9483	,	1.9483	,	1.9483	,	1.9483	,	1.9483	,	1.9483	,	1.9483	,	1.9483	,	1.9483	,	1.9483	,	1.9483	,	1.9483	,	1.9483	,	1.9483
BuildBottomLevelASGpu	,	1.49302	,	0.6008	,	0.3816	,	0.494656	,	0.408128	,	0.249344	,	0.307904	,	0.44064	,	0.432736	,	0.484192	,	0.163872	,	0.442656	,	0.40224	,	0.495392	,	0.304704	,	0.286176	,	0.29776	,	0.25872	,	0.233664	,	0.278912	,	0.230848	,	0.258304	,	0.230432	,	0.277024	,	0.232224	,	0.260352	,	0.249344	,	0.280704	,	0.232256	,	0.257856	,	0.24816	,	0.26176	,	0.231232	,	0.257856	,	0.236352	,	0.33296	,	0.202528	,	0.29024	,	0.254048	,	0.340704	,	0.18464	,	0.242624	,	0.2056	,	0.374528	,	0.208832	,	0.507776	,	0.228992	,	0.14128	,	0.428256	,	0.521024	,	0.698784	,	0.16816	,	0.237312	,	0.566752	,	0.811392	,	0.556064	,	0.555872	,	0.580736	,	0.556096	,	0.564768	,	0.567616	,	0.559136	,	0.549376	,	0.591392	,	0.518048	,	0.508896	,	0.51696	,	0.536992	,	0.499904	,	0.527616	,	0.525152	,	0.51296	,	0.505248	,	0.517184	,	0.163264	,	0.59024	,	0.143712	,	0.59328	,	0.163264	,	0.58704	,	0.143744	,	0.605792	,	0.389984	,	0.46288	,	0.376928	,	0.46288	,	0.384096	,	0.47872	,	0.392032	,	0.475872	,	0.373728	,	0.461024	,	0.391808	,	0.483648	,	0.374752	,	0.452864	,	0.608672	,	0.438848	,	0.414464	,	0.487072	,	0.40016	,	0.188896	,	0.61248	,	0.16304
BuildTopLevelAS	,	1.0912	,	1.0912	,	1.0912	,	1.0912	,	1.0912	,	1.0912	,	1.0912	,	1.0912	,	1.0912	,	1.0912	,	1.0912	,	1.0912	,	1.0912	,	1.0912	,	1.0912	,	1.0912	,	1.0912	,	1.0912	,	1.0912	,	1.0912	,	1.0912	,	1.0912	,	1.0912	,	1.0912	,	1.0912	,	1.0912	,	1.0912	,	1.0912	,	1.0912	,	1.0912	,	1.0912	,	1.0912	,	1.0912	,	1.0912	,	1.0912	,	1.0912	,	1.0912	,	1.0912	,	1.0912	,	1.0912	,	1.0912	,	1.0912	,	1.0912	,	1.0912	,	1.0912	,	1.0912	,	1.0912	,	1.0912	,	1.0912	,	1.0912	,	1.0912	,	1.0912	,	1.0912	,	1.0912	,	1.0912	,	1.0912	,	1.0912	,	1.0912	,	1.0912	,	1.0912	,	1.0912	,	1.0912	,	1.0912	,	1.0912	,	1.0912	,	1.0912	,	1.0912	,	1.0912	,	1.0912	,	1.0912	,	1.0912	,	1.0912	,	1.0912	,	1.0912	,	1.0912	,	1.0912	,	1.0912	,	1.0912	,	1.0912	,	1.0912	,	1.0912	,	1.0912	,	1.0912	,	1.0912	,	1.0912	,	1.0912	,	1.0912	,	1.0912	,	1.0912	,	1.0912	,	1.0912	,	1.0912	,	1.0912	,	1.0912	,	1.0912	,	1.0912	,	1.0912	,	1.0912	,	1.0912	,	1.0912	,	1.0912	,	1.0912	,	1.0912	,	1.0912
BuildTopLevelASGpu	,	0.086176	,	0.079232	,	0.078624	,	0.082496	,	0.077664	,	0.078688	,	0.083264	,	0.078432	,	0.077824	,	0.082272	,	0.078016	,	0.077632	,	0.08272	,	0.078432	,	0.078656	,	0.078432	,	0.083104	,	0.077824	,	0.078624	,	0.082496	,	0.078432	,	0.078016	,	0.077824	,	0.082272	,	0.078048	,	0.078048	,	0.077824	,	0.08272	,	0.078656	,	0.077824	,	0.082496	,	0.096064	,	0.078592	,	0.07824	,	0.08208	,	0.08208	,	0.0776	,	0.077632	,	0.08208	,	0.078016	,	0.078048	,	0.078016	,	0.08352	,	0.078432	,	0.077792	,	0.078048	,	0.082688	,	0.077792	,	0.078848	,	0.079424	,	0.08208	,	0.078848	,	0.078432	,	0.082944	,	0.079232	,	0.0776	,	0.079232	,	0.081888	,	0.0776	,	0.078944	,	0.081696	,	0.07824	,	0.077792	,	0.08208	,	0.077632	,	0.078336	,	0.078432	,	0.082112	,	0.07744	,	0.077824	,	0.08208	,	0.0776	,	0.07744	,	0.077632	,	0.083296	,	0.078432	,	0.077824	,	0.0784	,	0.083712	,	0.07904	,	0.077824	,	0.08208	,	0.079456	,	0.077632	,	0.077824	,	0.082304	,	0.077664	,	0.077632	,	0.081696	,	0.078016	,	0.078432	,	0.077632	,	0.082304	,	0.079264	,	0.077664	,	0.0776	,	0.081888	,	0.078624	,	0.078432	,	0.082496	,	0.0776	,	0.077824	,	0.079232	,	0.082848
Duplication	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1
Triangles	,	262267	,	3640	,	468	,	2211	,	1362	,	108	,	280	,	2332	,	1456	,	2896	,	16	,	2332	,	1096	,	3680	,	204	,	204	,	148	,	160	,	112	,	160	,	112	,	160	,	112	,	160	,	112	,	160	,	112	,	160	,	112	,	160	,	112	,	160	,	112	,	160	,	112	,	440	,	32	,	280	,	54	,	242	,	24	,	82	,	24	,	556	,	50	,	3408	,	50	,	5	,	1360	,	3736	,	23208	,	18	,	64	,	5456	,	27796	,	5504	,	5504	,	5504	,	5504	,	5504	,	5504	,	5504	,	5504	,	11040	,	4864	,	4608	,	4864	,	4864	,	4608	,	4864	,	4608	,	4864	,	4608	,	4864	,	8	,	4957	,	8	,	4957	,	8	,	4957	,	8	,	4957	,	468	,	2211	,	468	,	2211	,	468	,	2211	,	400	,	2211	,	400	,	2211	,	400	,	2211	,	400	,	2211	,	9184	,	1521	,	1362	,	1521	,	1362	,	28	,	14484	,	10
Meshes	,	103	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1
BottomLevels	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1
TopLevelSize	,	64	,	64	,	64	,	64	,	64	,	64	,	64	,	64	,	64	,	64	,	64	,	64	,	64	,	64	,	64	,	64	,	64	,	64	,	64	,	64	,	64	,	64	,	64	,	64	,	64	,	64	,	64	,	64	,	64	,	64	,	64	,	64	,	64	,	64	,	64	,	64	,	64	,	64	,	64	,	64	,	64	,	64	,	64	,	64	,	64	,	64	,	64	,	64	,	64	,	64	,	64	,	64	,	64	,	64	,	64	,	64	,	64	,	64	,	64	,	64	,	64	,	64	,	64	,	64	,	64	,	64	,	64	,	64	,	64	,	64	,	64	,	64	,	64	,	64	,	64	,	64	,	64	,	64	,	64	,	64	,	64	,	64	,	64	,	64	,	64	,	64	,	64	,	64	,	64	,	64	,	64	,	64	,	64	,	64	,	64	,	64	,	64	,	64	,	64	,	64	,	64	,	64	,	64	,	64
BottomLevelsSize	,	14622208	,	260736	,	37504	,	158080	,	99072	,	11904	,	23168	,	168192	,	104832	,	206080	,	4864	,	168192	,	82560	,	261376	,	17792	,	17792	,	13952	,	15616	,	12032	,	15616	,	12032	,	15616	,	12032	,	15616	,	12032	,	15616	,	12032	,	15616	,	12032	,	15616	,	12032	,	15616	,	12032	,	15616	,	12032	,	35712	,	6528	,	23168	,	8192	,	21120	,	5216	,	10112	,	5216	,	43264	,	6784	,	246016	,	6784	,	3712	,	99072	,	267904	,	1631872	,	4864	,	8448	,	389504	,	1954560	,	390272	,	390272	,	390272	,	390272	,	390272	,	390272	,	390272	,	390272	,	777984	,	346880	,	326144	,	346880	,	346880	,	326144	,	346880	,	326144	,	346880	,	326144	,	346880	,	3840	,	353792	,	3840	,	353792	,	3840	,	353792	,	3840	,	353792	,	37504	,	158080	,	37504	,	158080	,	37504	,	158080	,	32256	,	158080	,	32256	,	158080	,	32256	,	158080	,	32256	,	158080	,	648576	,	109952	,	99072	,	109952	,	99072	,	6400	,	1021440	,	4096
Index	,	0	,	1	,	2	,	3	,	4	,	5	,	6	,	7	,	8	,	9	,	10	,	11	,	12	,	13	,	14	,	15	,	16	,	17	,	18	,	19	,	20	,	21	,	22	,	23	,	24	,	25	,	26	,	27	,	28	,	29	,	30	,	31	,	32	,	33	,	34	,	35	,	36	,	37	,	38	,	39	,	40	,	41	,	42	,	43	,	44	,	45	,	46	,	47	,	48	,	49	,	50	,	51	,	52	,	53	,	54	,	55	,	56	,	57	,	58	,	59	,	60	,	61	,	62	,	63	,	64	,	65	,	66	,	67	,	68	,	69	,	70	,	71	,	72	,	73	,	74	,	75	,	76	,	77	,	78	,	79	,	80	,	81	,	82	,	83	,	84	,	85	,	86	,	87	,	88	,	89	,	90	,	91	,	92	,	93	,	94	,	95	,	96	,	97	,	98	,	99	,	100	,	101	,	102	,	103

FpsAggregator: 
FPS	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0
UPS	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0
FrameTime	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0
GigaRays/s	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0
Index	,	0	,	1	,	2	,	3	,	4	,	5	,	6	,	7	,	8	,	9	,	10	,	11	,	12	,	13	,	14	,	15	,	16	,	17	,	18	,	19	,	20	,	21	,	22	,	23	,	24	,	25	,	26	,	27	,	28	,	29	,	30	,	31	,	32	,	33	,	34	,	35	,	36	,	37	,	38	,	39	,	40	,	41	,	42	,	43	,	44	,	45	,	46	,	47	,	48	,	49	,	50	,	51	,	52	,	53	,	54	,	55	,	56	,	57	,	58	,	59	,	60	,	61	,	62	,	63	,	64	,	65	,	66	,	67	,	68	,	69	,	70	,	71	,	72	,	73	,	74	,	75	,	76	,	77	,	78	,	79	,	80	,	81	,	82	,	83	,	84	,	85	,	86	,	87	,	88	,	89	,	90	,	91	,	92	,	93	,	94	,	95	,	96	,	97	,	98	,	99	,	100	,	101	,	102	,	103


AccelerationStructure: 
Ray tracing acceleration structure building statistics: 
	Bottom Level Acceleration Structure: 
		Maximum [B]: 18390656
		Minimum [B]: 18390656
		Currently Allocated [B]: 18390656
		Currently Created [num]: 1
		Current Average [B]: 18390656
		Maximum Triangles [num]: 262267
		Minimum Triangles [num]: 262267
		Total Triangles [num]: 262267
		Maximum Meshes [num]: 103
		Minimum Meshes [num]: 103
		Total Meshes [num]: 103
	Top Level Acceleration Structure: 
		Maximum [B]: 64
		Minimum [B]: 64
		Currently Allocated [B]: 64
		Total Allocated [B]: 64
		Total Created [num]: 1
		Average Allocated [B]: 64
		Maximum Instances [num]: 1
		Minimum Instances [num]: 1
		Total Instances [num]: 1
	Scratch Buffer: 
		Maximum [B]: 14995072
		Minimum [B]: 14995072
		Currently Allocated [B]: 14995072
		Total Allocated [B]: 14995072
		Total Created [num]: 1
		Average Allocated [B]: 14995072
ProfilingStack: 
[PROF] Profiling statistics: 
	Scopes entered : 741
	Scopes exited : 740
	Overhead per scope [ticks] : 102.7
	Threads entered : 3
	Threads exited : 0
	============================
		         Sum[Mt]   Parent[%]  Samples[num]       Avg[Mt]   Last/Curr[Mt]        Self[Mt]     Self[%] 	 Name

	main:

		    6953.105 Mt   100.000 %         1 x     6953.105 Mt     6953.105 Mt        0.318 Mt     0.005 % 	 /

		    6952.801 Mt    99.996 %         1 x     6952.801 Mt     6952.801 Mt      390.730 Mt     5.620 % 	 ./Main application loop

		       2.132 Mt     0.031 %         1 x        2.132 Mt        2.132 Mt        2.132 Mt   100.000 % 	 ../TexturedRLInitialization

		       1.183 Mt     0.017 %         1 x        1.183 Mt        1.183 Mt        1.183 Mt   100.000 % 	 ../ShadowMapRLInitialization

		       4.321 Mt     0.062 %         1 x        4.321 Mt        4.321 Mt        4.321 Mt   100.000 % 	 ../RasterResolveRLInitialization

		       3.762 Mt     0.054 %         1 x        3.762 Mt        3.762 Mt        3.762 Mt   100.000 % 	 ../ResolveRLInitialization

		     379.481 Mt     5.458 %       105 x        3.614 Mt        4.493 Mt      248.611 Mt    65.514 % 	 ../AccelerationStructureBuild

		      61.032 Mt    16.083 %       105 x        0.581 Mt        2.148 Mt       61.032 Mt   100.000 % 	 .../BuildBottomLevelAS

		      69.838 Mt    18.403 %       105 x        0.665 Mt        0.518 Mt       69.838 Mt   100.000 % 	 .../BuildTopLevelAS

		       2.601 Mt     0.037 %         1 x        2.601 Mt        2.601 Mt        2.601 Mt   100.000 % 	 ../AmbientOcclusionRLInitialization

		       4.794 Mt     0.069 %         1 x        4.794 Mt        4.794 Mt        4.794 Mt   100.000 % 	 ../DeferredRLInitialization

		      65.988 Mt     0.949 %         1 x       65.988 Mt       65.988 Mt        2.538 Mt     3.846 % 	 ../RayTracingRLInitialization

		      63.450 Mt    96.154 %         1 x       63.450 Mt       63.450 Mt       63.450 Mt   100.000 % 	 .../PipelineBuild

		    6097.808 Mt    87.703 %         1 x     6097.808 Mt     6097.808 Mt        0.243 Mt     0.004 % 	 ../Initialization

		    6097.565 Mt    99.996 %         1 x     6097.565 Mt     6097.565 Mt        2.251 Mt     0.037 % 	 .../ScenePrep

		    6013.161 Mt    98.616 %         1 x     6013.161 Mt     6013.161 Mt     2938.699 Mt    48.871 % 	 ..../SceneLoad

		    2313.673 Mt    38.477 %         1 x     2313.673 Mt     2313.673 Mt      785.460 Mt    33.949 % 	 ...../glTF-LoadScene

		    1528.213 Mt    66.051 %        69 x       22.148 Mt        0.006 Mt     1528.213 Mt   100.000 % 	 ....../glTF-LoadImageData

		     483.993 Mt     8.049 %         1 x      483.993 Mt      483.993 Mt      483.993 Mt   100.000 % 	 ...../glTF-CreateScene

		     276.796 Mt     4.603 %         1 x      276.796 Mt      276.796 Mt      276.796 Mt   100.000 % 	 ...../SceneDesc-CreateScene

		       2.111 Mt     0.035 %         1 x        2.111 Mt        2.111 Mt        0.001 Mt     0.028 % 	 ..../ShadowMapRLPrep

		       2.110 Mt    99.972 %         1 x        2.110 Mt        2.110 Mt        2.073 Mt    98.209 % 	 ...../PrepareForRendering

		       0.038 Mt     1.791 %         1 x        0.038 Mt        0.038 Mt        0.038 Mt   100.000 % 	 ....../PrepareDrawBundle

		       5.459 Mt     0.090 %         1 x        5.459 Mt        5.459 Mt        0.001 Mt     0.015 % 	 ..../TexturedRLPrep

		       5.459 Mt    99.985 %         1 x        5.459 Mt        5.459 Mt        5.420 Mt    99.286 % 	 ...../PrepareForRendering

		       0.003 Mt     0.055 %         1 x        0.003 Mt        0.003 Mt        0.003 Mt   100.000 % 	 ....../PrepareMaterials

		       0.036 Mt     0.660 %         1 x        0.036 Mt        0.036 Mt        0.036 Mt   100.000 % 	 ....../PrepareDrawBundle

		       5.093 Mt     0.084 %         1 x        5.093 Mt        5.093 Mt        0.001 Mt     0.022 % 	 ..../DeferredRLPrep

		       5.091 Mt    99.978 %         1 x        5.091 Mt        5.091 Mt        3.699 Mt    72.648 % 	 ...../PrepareForRendering

		       0.002 Mt     0.039 %         1 x        0.002 Mt        0.002 Mt        0.002 Mt   100.000 % 	 ....../PrepareMaterials

		       1.315 Mt    25.834 %         1 x        1.315 Mt        1.315 Mt        1.315 Mt   100.000 % 	 ....../BuildMaterialCache

		       0.075 Mt     1.479 %         1 x        0.075 Mt        0.075 Mt        0.075 Mt   100.000 % 	 ....../PrepareDrawBundle

		      40.599 Mt     0.666 %         1 x       40.599 Mt       40.599 Mt        2.550 Mt     6.281 % 	 ..../RayTracingRLPrep

		       0.068 Mt     0.167 %         1 x        0.068 Mt        0.068 Mt        0.068 Mt   100.000 % 	 ...../PrepareAccelerationStructures

		       0.001 Mt     0.002 %         1 x        0.001 Mt        0.001 Mt        0.001 Mt   100.000 % 	 ...../PrepareCache

		      28.780 Mt    70.888 %         1 x       28.780 Mt       28.780 Mt       28.780 Mt   100.000 % 	 ...../PipelineBuild

		       0.984 Mt     2.423 %         1 x        0.984 Mt        0.984 Mt        0.984 Mt   100.000 % 	 ...../BuildCache

		       5.887 Mt    14.501 %         1 x        5.887 Mt        5.887 Mt        0.000 Mt     0.008 % 	 ...../BuildAccelerationStructures

		       5.887 Mt    99.992 %         1 x        5.887 Mt        5.887 Mt        2.847 Mt    48.367 % 	 ....../AccelerationStructureBuild

		       1.948 Mt    33.097 %         1 x        1.948 Mt        1.948 Mt        1.948 Mt   100.000 % 	 ......./BuildBottomLevelAS

		       1.091 Mt    18.537 %         1 x        1.091 Mt        1.091 Mt        1.091 Mt   100.000 % 	 ......./BuildTopLevelAS

		       2.329 Mt     5.737 %         1 x        2.329 Mt        2.329 Mt        2.329 Mt   100.000 % 	 ...../BuildShaderTables

		      28.892 Mt     0.474 %         1 x       28.892 Mt       28.892 Mt       28.892 Mt   100.000 % 	 ..../GpuSidePrep

	WindowThread:

		    6949.620 Mt   100.000 %         1 x     6949.620 Mt     6949.620 Mt     6949.620 Mt   100.000 % 	 /

	GpuThread:

		     109.877 Mt   100.000 %       104 x        1.057 Mt        0.248 Mt       55.752 Mt    50.740 % 	 /

		       2.463 Mt     2.241 %         1 x        2.463 Mt        2.463 Mt        0.006 Mt     0.247 % 	 ./ScenePrep

		       2.457 Mt    99.753 %         1 x        2.457 Mt        2.457 Mt        0.001 Mt     0.052 % 	 ../AccelerationStructureBuild

		       2.366 Mt    96.336 %         1 x        2.366 Mt        2.366 Mt        2.366 Mt   100.000 % 	 .../BuildBottomLevelASGpu

		       0.089 Mt     3.612 %         1 x        0.089 Mt        0.089 Mt        0.089 Mt   100.000 % 	 .../BuildTopLevelASGpu

		      51.662 Mt    47.018 %       105 x        0.492 Mt        0.248 Mt        0.134 Mt     0.259 % 	 ./AccelerationStructureBuild

		      43.156 Mt    83.536 %       105 x        0.411 Mt        0.163 Mt       43.156 Mt   100.000 % 	 ../BuildBottomLevelASGpu

		       8.372 Mt    16.205 %       105 x        0.080 Mt        0.083 Mt        8.372 Mt   100.000 % 	 ../BuildTopLevelASGpu


	============================


