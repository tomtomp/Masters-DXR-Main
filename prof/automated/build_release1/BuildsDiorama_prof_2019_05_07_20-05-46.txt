Configuration: 
Command line parameters  = E:\programming\FIT-DIP\Masters-DXR-Main-git\build\Measurement\x64\Release\Measurement.exe --scene Diorama/Diorama.glb --profile-builds --profile-output BuildsDiorama 
Build type               = Release
Device name              = NVIDIA GeForce RTX 2080 Ti
Device memory MB         = 11049
Ray Tracing backend      = Hardware
Window width             = 1024
Window height            = 768
Window title             = Direct3D App
Back-buffer count        = 3
Fill Triangles           = enabled
Triangles Font Clock-Wise= enabled
Fast Build AS            = disabled
Target FPS               = 60
Target UPS               = 60
Tearing                  = disabled
VSync                    = disabled
GUI rendering            = enabled
Debug layer              = disabled
GPU validation           = disabled
GPU profiling            = enabled
Render mode              = Rasterization
Loaded scene file        = Diorama/Diorama.glb
Using testing scene      = disabled
Duplication              = 1
Duplication in cube      = disabled
Duplication offset       = 10
BLAS duplication         = disabled
Rays per pixel           = 13
Quality preset           = High
Rasterization Quality    : 
  AO                     = enabled
  AO kernel size         = 4
  Shadows                = enabled
  Shadow map resolution  = 4096
  Shadow PCF kernel size = 4
  Reflections            = enabled
Ray Tracing Quality      : 
  AO                     = enabled
  AO rays                = 4
  AO kernel size         = 4
  Shadows                = enabled
  Shadow rays            = 4
  Shadow kernel size     = 4
  Reflections            = enabled

ProfilingAggregator: 

ProfilingBuildAggregator: 
BuildBottomLevelAS	,	6.2901	,	6.2901	,	6.2901	,	6.2901	,	6.2901	,	6.2901	,	6.2901	,	6.2901	,	6.2901	,	6.2901	,	6.2901	,	6.2901	,	6.2901	,	6.2901	,	6.2901	,	6.2901	,	6.2901	,	6.2901	,	6.2901	,	6.2901	,	6.2901	,	6.2901	,	6.2901	,	6.2901	,	6.2901	,	6.2901	,	6.2901	,	6.2901	,	6.2901	,	6.2901	,	6.2901	,	6.2901	,	6.2901	,	6.2901	,	6.2901	,	6.2901
BuildBottomLevelASGpu	,	0.178368	,	0.160416	,	0.177728	,	0.175904	,	0.189696	,	0.169088	,	0.175072	,	0.162464	,	0.16496	,	0.1672	,	0.170528	,	0.168672	,	0.176736	,	0.166624	,	0.16288	,	0.410688	,	0.384192	,	0.546528	,	0.269472	,	0.229376	,	0.259968	,	0.25664	,	0.262016	,	0.251712	,	0.245664	,	0.231232	,	0.231424	,	0.262048	,	0.241344	,	0.232064	,	0.25664	,	0.250272	,	0.241984	,	0.68448	,	0.620064	,	0.990912
BuildTopLevelAS	,	1.1795	,	1.1795	,	1.1795	,	1.1795	,	1.1795	,	1.1795	,	1.1795	,	1.1795	,	1.1795	,	1.1795	,	1.1795	,	1.1795	,	1.1795	,	1.1795	,	1.1795	,	1.1795	,	1.1795	,	1.1795	,	1.1795	,	1.1795	,	1.1795	,	1.1795	,	1.1795	,	1.1795	,	1.1795	,	1.1795	,	1.1795	,	1.1795	,	1.1795	,	1.1795	,	1.1795	,	1.1795	,	1.1795	,	1.1795	,	1.1795	,	1.1795
BuildTopLevelASGpu	,	0.079392	,	0.07632	,	0.078176	,	0.076352	,	0.078592	,	0.07632	,	0.07696	,	0.076128	,	0.07712	,	0.076544	,	0.077536	,	0.07632	,	0.079232	,	0.078944	,	0.07632	,	0.07696	,	0.078368	,	0.077536	,	0.082496	,	0.078176	,	0.078176	,	0.078176	,	0.078176	,	0.078176	,	0.080032	,	0.078176	,	0.079008	,	0.079616	,	0.079008	,	0.078176	,	0.078176	,	0.079232	,	0.079424	,	0.082912	,	0.0784	,	0.08368
Duplication	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1
Triangles	,	70	,	43	,	56	,	63	,	70	,	46	,	55	,	46	,	42	,	46	,	51	,	46	,	46	,	56	,	46	,	18310	,	5192	,	47168	,	70	,	43	,	56	,	63	,	70	,	46	,	55	,	46	,	42	,	46	,	51	,	46	,	46	,	56	,	46	,	18310	,	5192	,	47168
Meshes	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1
BottomLevels	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1
TopLevelSize	,	64	,	64	,	64	,	64	,	64	,	64	,	64	,	64	,	64	,	64	,	64	,	64	,	64	,	64	,	64	,	64	,	64	,	64	,	64	,	64	,	64	,	64	,	64	,	64	,	64	,	64	,	64	,	64	,	64	,	64	,	64	,	64	,	64	,	64	,	64	,	64
BottomLevelsSize	,	6912	,	5376	,	6016	,	6784	,	6912	,	5888	,	6016	,	5888	,	5376	,	5888	,	6016	,	5888	,	5888	,	6016	,	5888	,	1023616	,	292736	,	2632320	,	8448	,	6656	,	8320	,	8448	,	8448	,	6656	,	8320	,	6656	,	6656	,	6656	,	6784	,	6656	,	6656	,	8320	,	6656	,	1287552	,	368768	,	3311360
Index	,	0	,	1	,	2	,	3	,	4	,	5	,	6	,	7	,	8	,	9	,	10	,	11	,	12	,	13	,	14	,	15	,	16	,	17	,	18	,	19	,	20	,	21	,	22	,	23	,	24	,	25	,	26	,	27	,	28	,	29	,	30	,	31	,	32	,	33	,	34	,	35

FpsAggregator: 
FPS	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0
UPS	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0
FrameTime	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0
GigaRays/s	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0
Index	,	0	,	1	,	2	,	3	,	4	,	5	,	6	,	7	,	8	,	9	,	10	,	11	,	12	,	13	,	14	,	15	,	16	,	17	,	18	,	19	,	20	,	21	,	22	,	23	,	24	,	25	,	26	,	27	,	28	,	29	,	30	,	31	,	32	,	33	,	34	,	35


AccelerationStructure: 
Ray tracing acceleration structure building statistics: 
	Bottom Level Acceleration Structure: 
		Maximum [B]: 3311360
		Minimum [B]: 6656
		Currently Allocated [B]: 5078016
		Currently Created [num]: 18
		Current Average [B]: 282112
		Maximum Triangles [num]: 47168
		Minimum Triangles [num]: 42
		Total Triangles [num]: 71452
		Maximum Meshes [num]: 1
		Minimum Meshes [num]: 1
		Total Meshes [num]: 18
	Top Level Acceleration Structure: 
		Maximum [B]: 1152
		Minimum [B]: 1152
		Currently Allocated [B]: 1152
		Total Allocated [B]: 1152
		Total Created [num]: 1
		Average Allocated [B]: 1152
		Maximum Instances [num]: 18
		Minimum Instances [num]: 18
		Total Instances [num]: 18
	Scratch Buffer: 
		Maximum [B]: 990848
		Minimum [B]: 990848
		Currently Allocated [B]: 990848
		Total Allocated [B]: 990848
		Total Created [num]: 1
		Average Allocated [B]: 990848
ProfilingStack: 
[PROF] Profiling statistics: 
	Scopes entered : 276
	Scopes exited : 275
	Overhead per scope [ticks] : 102.5
	Threads entered : 3
	Threads exited : 0
	============================
		         Sum[Mt]   Parent[%]  Samples[num]       Avg[Mt]   Last/Curr[Mt]        Self[Mt]     Self[%] 	 Name

	main:

		    1758.352 Mt   100.000 %         1 x     1758.352 Mt     1758.352 Mt        0.389 Mt     0.022 % 	 /

		    1758.008 Mt    99.980 %         1 x     1758.009 Mt     1758.008 Mt      295.337 Mt    16.800 % 	 ./Main application loop

		       2.064 Mt     0.117 %         1 x        2.064 Mt        2.064 Mt        2.064 Mt   100.000 % 	 ../TexturedRLInitialization

		       1.210 Mt     0.069 %         1 x        1.210 Mt        1.210 Mt        1.210 Mt   100.000 % 	 ../ShadowMapRLInitialization

		       4.216 Mt     0.240 %         1 x        4.216 Mt        4.216 Mt        4.216 Mt   100.000 % 	 ../RasterResolveRLInitialization

		       3.641 Mt     0.207 %         1 x        3.641 Mt        3.641 Mt        3.641 Mt   100.000 % 	 ../ResolveRLInitialization

		     287.123 Mt    16.332 %        37 x        7.760 Mt        9.349 Mt      192.498 Mt    67.044 % 	 ../AccelerationStructureBuild

		      40.870 Mt    14.234 %        37 x        1.105 Mt        1.400 Mt       40.870 Mt   100.000 % 	 .../BuildBottomLevelAS

		      53.754 Mt    18.722 %        37 x        1.453 Mt        1.134 Mt       53.754 Mt   100.000 % 	 .../BuildTopLevelAS

		       2.616 Mt     0.149 %         1 x        2.616 Mt        2.616 Mt        2.616 Mt   100.000 % 	 ../AmbientOcclusionRLInitialization

		       4.308 Mt     0.245 %         1 x        4.308 Mt        4.308 Mt        4.308 Mt   100.000 % 	 ../DeferredRLInitialization

		      58.236 Mt     3.313 %         1 x       58.236 Mt       58.236 Mt        2.267 Mt     3.893 % 	 ../RayTracingRLInitialization

		      55.969 Mt    96.107 %         1 x       55.969 Mt       55.969 Mt       55.969 Mt   100.000 % 	 .../PipelineBuild

		    1099.258 Mt    62.529 %         1 x     1099.258 Mt     1099.258 Mt        0.258 Mt     0.023 % 	 ../Initialization

		    1099.000 Mt    99.977 %         1 x     1099.000 Mt     1099.000 Mt        2.104 Mt     0.191 % 	 .../ScenePrep

		    1026.922 Mt    93.441 %         1 x     1026.922 Mt     1026.922 Mt      107.886 Mt    10.506 % 	 ..../SceneLoad

		     678.268 Mt    66.049 %         1 x      678.268 Mt      678.268 Mt      153.104 Mt    22.573 % 	 ...../glTF-LoadScene

		     525.164 Mt    77.427 %        12 x       43.764 Mt       72.320 Mt      525.164 Mt   100.000 % 	 ....../glTF-LoadImageData

		     182.665 Mt    17.788 %         1 x      182.665 Mt      182.665 Mt      182.665 Mt   100.000 % 	 ...../glTF-CreateScene

		      58.103 Mt     5.658 %         1 x       58.103 Mt       58.103 Mt       58.103 Mt   100.000 % 	 ...../SceneDesc-CreateScene

		       1.380 Mt     0.126 %         1 x        1.380 Mt        1.380 Mt        0.001 Mt     0.043 % 	 ..../ShadowMapRLPrep

		       1.380 Mt    99.957 %         1 x        1.380 Mt        1.380 Mt        1.341 Mt    97.231 % 	 ...../PrepareForRendering

		       0.038 Mt     2.769 %         1 x        0.038 Mt        0.038 Mt        0.038 Mt   100.000 % 	 ....../PrepareDrawBundle

		       2.489 Mt     0.227 %         1 x        2.489 Mt        2.489 Mt        0.001 Mt     0.028 % 	 ..../TexturedRLPrep

		       2.489 Mt    99.972 %         1 x        2.489 Mt        2.489 Mt        2.438 Mt    97.947 % 	 ...../PrepareForRendering

		       0.021 Mt     0.856 %         1 x        0.021 Mt        0.021 Mt        0.021 Mt   100.000 % 	 ....../PrepareMaterials

		       0.030 Mt     1.197 %         1 x        0.030 Mt        0.030 Mt        0.030 Mt   100.000 % 	 ....../PrepareDrawBundle

		       2.327 Mt     0.212 %         1 x        2.327 Mt        2.327 Mt        0.001 Mt     0.026 % 	 ..../DeferredRLPrep

		       2.326 Mt    99.974 %         1 x        2.326 Mt        2.326 Mt        1.762 Mt    75.744 % 	 ...../PrepareForRendering

		       0.009 Mt     0.383 %         1 x        0.009 Mt        0.009 Mt        0.009 Mt   100.000 % 	 ....../PrepareMaterials

		       0.515 Mt    22.146 %         1 x        0.515 Mt        0.515 Mt        0.515 Mt   100.000 % 	 ....../BuildMaterialCache

		       0.040 Mt     1.728 %         1 x        0.040 Mt        0.040 Mt        0.040 Mt   100.000 % 	 ....../PrepareDrawBundle

		      41.395 Mt     3.767 %         1 x       41.395 Mt       41.395 Mt        3.913 Mt     9.452 % 	 ..../RayTracingRLPrep

		       0.471 Mt     1.139 %         1 x        0.471 Mt        0.471 Mt        0.471 Mt   100.000 % 	 ...../PrepareAccelerationStructures

		       0.009 Mt     0.022 %         1 x        0.009 Mt        0.009 Mt        0.009 Mt   100.000 % 	 ...../PrepareCache

		      19.944 Mt    48.179 %         1 x       19.944 Mt       19.944 Mt       19.944 Mt   100.000 % 	 ...../PipelineBuild

		       1.151 Mt     2.781 %         1 x        1.151 Mt        1.151 Mt        1.151 Mt   100.000 % 	 ...../BuildCache

		      12.537 Mt    30.285 %         1 x       12.537 Mt       12.537 Mt        0.000 Mt     0.004 % 	 ...../BuildAccelerationStructures

		      12.536 Mt    99.996 %         1 x       12.536 Mt       12.536 Mt        5.067 Mt    40.416 % 	 ....../AccelerationStructureBuild

		       6.290 Mt    50.175 %         1 x        6.290 Mt        6.290 Mt        6.290 Mt   100.000 % 	 ......./BuildBottomLevelAS

		       1.179 Mt     9.409 %         1 x        1.179 Mt        1.179 Mt        1.179 Mt   100.000 % 	 ......./BuildTopLevelAS

		       3.370 Mt     8.141 %         1 x        3.370 Mt        3.370 Mt        3.370 Mt   100.000 % 	 ...../BuildShaderTables

		      22.382 Mt     2.037 %         1 x       22.382 Mt       22.382 Mt       22.382 Mt   100.000 % 	 ..../GpuSidePrep

	WindowThread:

		    1754.835 Mt   100.000 %         1 x     1754.835 Mt     1754.834 Mt     1754.835 Mt   100.000 % 	 /

	GpuThread:

		      53.723 Mt   100.000 %        36 x        1.492 Mt        1.076 Mt       34.240 Mt    63.734 % 	 /

		       6.431 Mt    11.971 %         1 x        6.431 Mt        6.431 Mt        0.006 Mt     0.096 % 	 ./ScenePrep

		       6.425 Mt    99.904 %         1 x        6.425 Mt        6.425 Mt        0.001 Mt     0.018 % 	 ../AccelerationStructureBuild

		       6.292 Mt    97.929 %         1 x        6.292 Mt        6.292 Mt        6.292 Mt   100.000 % 	 .../BuildBottomLevelASGpu

		       0.132 Mt     2.053 %         1 x        0.132 Mt        0.132 Mt        0.132 Mt   100.000 % 	 .../BuildTopLevelASGpu

		      13.052 Mt    24.295 %        37 x        0.353 Mt        1.076 Mt        0.047 Mt     0.359 % 	 ./AccelerationStructureBuild

		      10.103 Mt    77.409 %        37 x        0.273 Mt        0.991 Mt       10.103 Mt   100.000 % 	 ../BuildBottomLevelASGpu

		       2.902 Mt    22.232 %        37 x        0.078 Mt        0.084 Mt        2.902 Mt   100.000 % 	 ../BuildTopLevelASGpu


	============================


