Configuration: 
Command line parameters  = 
Build type               = Release
Device name              = NVIDIA GeForce RTX 2080 Ti
Device memory MB         = 11049
Ray Tracing backend      = Hardware
Window width             = 1024
Window height            = 768
Window title             = Direct3D App
Back-buffer count        = 3
Fill Triangles           = enabled
Triangles Font Clock-Wise= disabled
Fast Build AS            = disabled
Target FPS               = 0
Target UPS               = 60
Tearing                  = enabled
VSync                    = disabled
GUI rendering            = disabled
Debug layer              = disabled
GPU validation           = disabled
GPU profiling            = enabled
Render mode              = Ray Tracing
Loaded scene file        = 
Using testing scene      = disabled
BLAS duplication         = disabled
Rays per pixel           = 6
Quality preset           = Medium
Rasterization Quality    : 
  AO                     = enabled
  AO kernel size         = 4
  Shadows                = enabled
  Shadow map resolution  = 4096
  Shadow PCF kernel size = 4
  Reflections            = disabled
Ray Tracing Quality      : 
  AO                     = enabled
  AO rays                = 2
  AO kernel size         = 4
  Shadows                = enabled
  Shadow rays            = 4
  Shadow kernel size     = 4
  Reflections            = disabled

ProfilingAggregator: 
Render	,	1.073	,	1.073	,	1.073	,	1.073	,	1.073	,	1.073	,	1.073	,	1.073	,	1.073	,	1.073	,	1.073	,	1.073	,	1.073	,	1.073	,	1.073	,	1.073	,	1.073	,	1.073	,	1.073	,	1.073	,	1.073	,	1.073	,	1.073	,	1.073	,	1.073
Update	,	0.0912	,	62.9525	,	64.6719	,	66.2684	,	67.9627	,	69.5786	,	71.4128	,	73.5055	,	75.4997	,	77.4365	,	79.1807	,	80.9531	,	82.668	,	84.3821	,	86.1451	,	88.2985	,	90.4841	,	92.5383	,	94.2906	,	96.1016	,	97.8204	,	99.4099	,	100.994	,	103.058	,	105.204
Index	,	0	,	1	,	2	,	3	,	4	,	5	,	6	,	7	,	8	,	9	,	10	,	11	,	12	,	13	,	14	,	15	,	16	,	17	,	18	,	19	,	20	,	21	,	22	,	23	,	24

ProfilingBuildAggregator: 
BuildBottomLevelAS	,	0.208	,	0.2145	,	0.227	,	0.2864	,	0.2588	,	0.2494	,	0.3312	,	0.2716	,	0.2344	,	0.254	,	0.2581	,	0.218	,	0.2258	,	0.2537	,	0.2548	,	0.3044	,	0.3161	,	0.2509	,	0.2187	,	0.2344	,	0.213	,	0.2343	,	0.2861	,	0.2829
BuildBottomLevelASGpu	,	0.020288	,	0.02208	,	0.02128	,	0.157312	,	0.14912	,	0.14992	,	0.161792	,	0.14976	,	0.14896	,	0.21776	,	0.264192	,	0.264576	,	0.215136	,	0.280096	,	0.25056	,	0.265824	,	0.245216	,	0.243808	,	0.25296	,	0.24288	,	0.178464	,	0.136096	,	0.169888	,	0.13824
BuildTopLevelAS	,	0.4148	,	0.3704	,	0.3424	,	0.3421	,	0.3276	,	0.4305	,	0.4268	,	0.3543	,	0.3581	,	0.3825	,	0.3613	,	0.3415	,	0.3498	,	0.3448	,	0.4499	,	0.4195	,	0.3659	,	0.342	,	0.42	,	0.3324	,	0.3467	,	0.3626	,	0.537	,	0.4098
BuildTopLevelASGpu	,	0.0784	,	0.078816	,	0.07936	,	0.07744	,	0.076288	,	0.07728	,	0.08096	,	0.07648	,	0.076448	,	0.076448	,	0.077248	,	0.076256	,	0.077248	,	0.076448	,	0.077248	,	0.080736	,	0.077824	,	0.07648	,	0.07648	,	0.076288	,	0.081312	,	0.078208	,	0.078048	,	0.079008
Triangles	,	32	,	28	,	28	,	60	,	60	,	60	,	60	,	60	,	60	,	232	,	776	,	874	,	232	,	1504	,	1341	,	1341	,	1341	,	1341	,	1341	,	1341	,	24	,	4	,	24	,	4
Meshes	,	1	,	2	,	2	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1
BottomLevels	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1
TopLevelSize	,	64	,	64	,	64	,	64	,	64	,	64	,	64	,	64	,	64	,	64	,	64	,	64	,	64	,	64	,	64	,	64	,	64	,	64	,	64	,	64	,	64	,	64	,	64	,	64
BottomLevelsSize	,	2048	,	1920	,	1920	,	6208	,	6208	,	6208	,	6208	,	6208	,	6208	,	16000	,	46208	,	51968	,	16000	,	87040	,	77952	,	77952	,	77952	,	77952	,	77952	,	77952	,	5216	,	3712	,	5216	,	3712
Index	,	0	,	1	,	2	,	3	,	4	,	5	,	6	,	7	,	8	,	9	,	10	,	11	,	12	,	13	,	14	,	15	,	16	,	17	,	18	,	19	,	20	,	21	,	22	,	23

FpsAggregator: 
FPS	,	2	,	2	,	2	,	2	,	2	,	2	,	2	,	2	,	2	,	2	,	2	,	2	,	2	,	2	,	2	,	2	,	2	,	2	,	2	,	2	,	2	,	2	,	2	,	2	,	2
UPS	,	9	,	9	,	9	,	9	,	9	,	9	,	9	,	9	,	9	,	9	,	9	,	9	,	9	,	9	,	9	,	9	,	9	,	9	,	9	,	9	,	9	,	9	,	9	,	9	,	9
FrameTime	,	1141.12	,	1141.12	,	1141.12	,	1141.12	,	1141.12	,	1141.12	,	1141.12	,	1141.12	,	1141.12	,	1141.12	,	1141.12	,	1141.12	,	1141.12	,	1141.12	,	1141.12	,	1141.12	,	1141.12	,	1141.12	,	1141.12	,	1141.12	,	1141.12	,	1141.12	,	1141.12	,	1141.12	,	1141.12
GigaRays/s	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0
Index	,	0	,	1	,	2	,	3	,	4	,	5	,	6	,	7	,	8	,	9	,	10	,	11	,	12	,	13	,	14	,	15	,	16	,	17	,	18	,	19	,	20	,	21	,	22	,	23	,	24


AccelerationStructure: 
Ray tracing acceleration structure building statistics: 
	Bottom Level Acceleration Structure: 
		Maximum [B]: 109696
		Minimum [B]: 6400
		Currently Allocated [B]: 939904
		Currently Created [num]: 20
		Current Average [B]: 46995
		Maximum Triangles [num]: 1504
		Minimum Triangles [num]: 28
		Total Triangles [num]: 12112
		Maximum Meshes [num]: 2
		Minimum Meshes [num]: 1
		Total Meshes [num]: 22
	Top Level Acceleration Structure: 
		Maximum [B]: 1280
		Minimum [B]: 1280
		Currently Allocated [B]: 1280
		Total Allocated [B]: 1280
		Total Created [num]: 1
		Average Allocated [B]: 1280
		Maximum Instances [num]: 20
		Minimum Instances [num]: 20
		Total Instances [num]: 20
	Scratch Buffer: 
		Maximum [B]: 31872
		Minimum [B]: 31872
		Currently Allocated [B]: 31872
		Total Allocated [B]: 31872
		Total Created [num]: 1
		Average Allocated [B]: 31872
ProfilingStack: 
[PROF] Profiling statistics: 
	Scopes entered : 204
	Scopes exited : 202
	Overhead per scope [ticks] : 165.3
	Threads entered : 3
	Threads exited : 0
	============================
		         Sum[Mt]   Parent[%]  Samples[num]       Avg[Mt]   Last/Curr[Mt]        Self[Mt]     Self[%] 	 Name

	main:

		    2524.045 Mt   100.000 %         1 x     2524.046 Mt     2524.045 Mt     2342.075 Mt    92.791 % 	 /

		      61.283 Mt     2.428 %         1 x       61.283 Mt        0.000 Mt        0.763 Mt     1.245 % 	 ./Initialize

		       1.608 Mt     2.624 %         1 x        1.608 Mt        0.000 Mt        1.608 Mt   100.000 % 	 ../ShadowMapRLInitialization

		       6.097 Mt     9.949 %         1 x        6.097 Mt        0.000 Mt        6.097 Mt   100.000 % 	 ../AmbientOcclusionRLInitialization

		       3.586 Mt     5.851 %         1 x        3.586 Mt        0.000 Mt        3.586 Mt   100.000 % 	 ../RasterResolveRLInitialization

		       3.772 Mt     6.156 %         1 x        3.772 Mt        0.000 Mt        3.772 Mt   100.000 % 	 ../DeferredRLInitialization

		      41.494 Mt    67.709 %         1 x       41.494 Mt        0.000 Mt        1.795 Mt     4.326 % 	 ../RayTracingRLInitialization

		      39.699 Mt    95.674 %         1 x       39.699 Mt        0.000 Mt       39.699 Mt   100.000 % 	 .../PipelineBuild

		       3.963 Mt     6.466 %         1 x        3.963 Mt        0.000 Mt        3.963 Mt   100.000 % 	 ../ResolveRLInitialization

		     120.755 Mt     4.784 %         3 x       40.252 Mt      106.436 Mt        0.712 Mt     0.590 % 	 ./Run

		     112.449 Mt    93.121 %        10 x       11.245 Mt      105.743 Mt       29.279 Mt    26.038 % 	 ../Update

		      59.361 Mt    52.789 %         1 x       59.361 Mt       59.361 Mt       23.996 Mt    40.423 % 	 .../ScenePrep

		       1.260 Mt     2.123 %         1 x        1.260 Mt        1.260 Mt        1.260 Mt   100.000 % 	 ..../SceneDesc-CreateScene

		       0.723 Mt     1.218 %         1 x        0.723 Mt        0.723 Mt        0.001 Mt     0.166 % 	 ..../ShadowMapRLPrep

		       0.722 Mt    99.834 %         1 x        0.722 Mt        0.722 Mt        0.694 Mt    96.106 % 	 ...../PrepareForRendering

		       0.028 Mt     3.894 %         1 x        0.028 Mt        0.028 Mt        0.028 Mt   100.000 % 	 ....../PrepareDrawBundle

		       1.889 Mt     3.182 %         1 x        1.889 Mt        1.889 Mt        0.001 Mt     0.048 % 	 ..../DeferredRLPrep

		       1.888 Mt    99.952 %         1 x        1.888 Mt        1.888 Mt        1.732 Mt    91.722 % 	 ...../PrepareForRendering

		       0.025 Mt     1.303 %         1 x        0.025 Mt        0.025 Mt        0.025 Mt   100.000 % 	 ....../PrepareMaterials

		       0.087 Mt     4.618 %         1 x        0.087 Mt        0.087 Mt        0.087 Mt   100.000 % 	 ....../BuildMaterialCache

		       0.044 Mt     2.357 %         1 x        0.044 Mt        0.044 Mt        0.044 Mt   100.000 % 	 ....../PrepareDrawBundle

		      18.708 Mt    31.516 %         1 x       18.708 Mt       18.708 Mt        1.327 Mt     7.095 % 	 ..../RayTracingRLPrep

		       0.586 Mt     3.134 %         1 x        0.586 Mt        0.586 Mt        0.586 Mt   100.000 % 	 ...../PrepareAccelerationStructures

		       0.021 Mt     0.110 %         1 x        0.021 Mt        0.021 Mt        0.021 Mt   100.000 % 	 ...../PrepareCache

		      10.457 Mt    55.897 %         1 x       10.457 Mt       10.457 Mt       10.457 Mt   100.000 % 	 ...../PipelineBuild

		       0.147 Mt     0.788 %         1 x        0.147 Mt        0.147 Mt        0.147 Mt   100.000 % 	 ...../BuildCache

		       5.867 Mt    31.362 %         1 x        5.867 Mt        5.867 Mt        0.001 Mt     0.015 % 	 ...../BuildAccelerationStructures

		       5.866 Mt    99.985 %         1 x        5.866 Mt        5.866 Mt        0.561 Mt     9.558 % 	 ....../AccelerationStructureBuild

		       4.912 Mt    83.738 %         1 x        4.912 Mt        4.912 Mt        4.912 Mt   100.000 % 	 ......./BuildBottomLevelAS

		       0.393 Mt     6.704 %         1 x        0.393 Mt        0.393 Mt        0.393 Mt   100.000 % 	 ......./BuildTopLevelAS

		       0.302 Mt     1.614 %         1 x        0.302 Mt        0.302 Mt        0.302 Mt   100.000 % 	 ...../BuildShaderTables

		      12.785 Mt    21.538 %         1 x       12.785 Mt       12.785 Mt       12.785 Mt   100.000 % 	 ..../GpuSidePrep

		      23.809 Mt    21.173 %        25 x        0.952 Mt        1.100 Mt        7.977 Mt    33.503 % 	 .../AccelerationStructureBuild

		       6.319 Mt    26.538 %        25 x        0.253 Mt        0.283 Mt        6.319 Mt   100.000 % 	 ..../BuildBottomLevelAS

		       9.514 Mt    39.959 %        25 x        0.381 Mt        0.410 Mt        9.514 Mt   100.000 % 	 ..../BuildTopLevelAS

		       7.602 Mt     6.295 %         2 x        3.801 Mt        0.000 Mt        0.002 Mt     0.028 % 	 ../Render

		       7.599 Mt    99.972 %         2 x        3.800 Mt        0.000 Mt        0.761 Mt    10.009 % 	 .../Render

		       6.839 Mt    89.991 %         2 x        3.419 Mt        0.000 Mt        6.839 Mt   100.000 % 	 ..../Present

	WindowThread:

		    2522.614 Mt   100.000 %         1 x     2522.614 Mt     2522.614 Mt     2522.614 Mt   100.000 % 	 /

	GpuThread:

		      17.605 Mt   100.000 %        26 x        0.677 Mt        0.219 Mt        4.618 Mt    26.229 % 	 /

		       0.002 Mt     0.014 %         2 x        0.001 Mt        0.000 Mt        0.002 Mt   100.000 % 	 ./Present

		       6.638 Mt    37.706 %         1 x        6.638 Mt        6.638 Mt        0.007 Mt     0.100 % 	 ./ScenePrep

		       6.632 Mt    99.900 %         1 x        6.632 Mt        6.632 Mt        0.001 Mt     0.017 % 	 ../AccelerationStructureBuild

		       6.485 Mt    97.782 %         1 x        6.485 Mt        6.485 Mt        6.485 Mt   100.000 % 	 .../BuildBottomLevelASGpu

		       0.146 Mt     2.200 %         1 x        0.146 Mt        0.146 Mt        0.146 Mt   100.000 % 	 .../BuildTopLevelASGpu

		       6.347 Mt    36.051 %        25 x        0.254 Mt        0.219 Mt        0.035 Mt     0.555 % 	 ./AccelerationStructureBuild

		       4.366 Mt    68.798 %        25 x        0.175 Mt        0.138 Mt        4.366 Mt   100.000 % 	 ../BuildBottomLevelASGpu

		       1.945 Mt    30.648 %        25 x        0.078 Mt        0.079 Mt        1.945 Mt   100.000 % 	 ../BuildTopLevelASGpu


	============================


