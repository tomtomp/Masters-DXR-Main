Configuration: 
Command line parameters  = 
Build type               = Release
Device name              = NVIDIA GeForce RTX 2080 Ti
Device memory MB         = 11049
Ray Tracing backend      = Hardware
Window width             = 1161
Window height            = 1219
Window title             = Direct3D App
Back-buffer count        = 3
Fill Triangles           = enabled
Triangles Font Clock-Wise= disabled
Fast Build AS            = disabled
Target FPS               = 0
Target UPS               = 60
Tearing                  = enabled
VSync                    = disabled
GUI rendering            = disabled
Debug layer              = disabled
GPU validation           = disabled
GPU profiling            = enabled
Render mode              = Ray Tracing
Loaded scene file        = 
Using testing scene      = disabled
BLAS duplication         = disabled
Rays per pixel           = 6
Quality preset           = Medium
Rasterization Quality    : 
  AO                     = enabled
  AO kernel size         = 4
  Shadows                = enabled
  Shadow map resolution  = 4096
  Shadow PCF kernel size = 4
  Reflections            = disabled
Ray Tracing Quality      : 
  AO                     = enabled
  AO rays                = 2
  AO kernel size         = 4
  Shadows                = enabled
  Shadow rays            = 4
  Shadow kernel size     = 4
  Reflections            = disabled

ProfilingAggregator: 
Render	,	2.1541	,	1.923	,	1.8532	,	1.8805
Update	,	0.0648	,	0.0643	,	0.0648	,	0.0218
RayTracing	,	0.1292	,	0.1242	,	0.1255	,	0.1466
RayTracingGpu	,	1.6247	,	1.42342	,	1.34189	,	1.34118
DeferredRLGpu	,	0.241632	,	0.171744	,	0.165184	,	0.1608
RayTracingRLGpu	,	0.622944	,	0.484128	,	0.433088	,	0.432832
ResolveRLGpu	,	0.756736	,	0.764864	,	0.7408	,	0.744064
Index	,	0	,	1	,	2	,	3

ProfilingBuildAggregator: 
BuildBottomLevelAS	,	0.1949
BuildBottomLevelASGpu	,	0.096128
BuildTopLevelAS	,	0.4015
BuildTopLevelASGpu	,	0.056608
Triangles	,	12112
Meshes	,	22
BottomLevels	,	20
TopLevelSize	,	1280
BottomLevelsSize	,	939904
Index	,	0

FpsAggregator: 
FPS	,	50	,	58	,	61	,	61
UPS	,	60	,	61	,	60	,	61
FrameTime	,	20.01	,	17.3164	,	16.6596	,	16.6663
GigaRays/s	,	0.385215	,	0.445136	,	0.462685	,	0.462499
Index	,	0	,	1	,	2	,	3


AccelerationStructure: 
Ray tracing acceleration structure building statistics: 
	Bottom Level Acceleration Structure: 
		Maximum [B]: 109696
		Minimum [B]: 6400
		Currently Allocated [B]: 939904
		Currently Created [num]: 20
		Current Average [B]: 46995
		Maximum Triangles [num]: 1504
		Minimum Triangles [num]: 28
		Total Triangles [num]: 12112
		Maximum Meshes [num]: 2
		Minimum Meshes [num]: 1
		Total Meshes [num]: 22
	Top Level Acceleration Structure: 
		Maximum [B]: 1280
		Minimum [B]: 1280
		Currently Allocated [B]: 1280
		Total Allocated [B]: 1280
		Total Created [num]: 1
		Average Allocated [B]: 1280
		Maximum Instances [num]: 20
		Minimum Instances [num]: 20
		Total Instances [num]: 20
	Scratch Buffer: 
		Maximum [B]: 31872
		Minimum [B]: 31872
		Currently Allocated [B]: 31872
		Total Allocated [B]: 31872
		Total Created [num]: 1
		Average Allocated [B]: 31872
ProfilingStack: 
[PROF] Profiling statistics: 
	Scopes entered : 8902
	Scopes exited : 8902
	Overhead per scope [ticks] : 165.3
	Threads entered : 3
	Threads exited : 0
	============================
		         Sum[Mt]   Parent[%]  Samples[num]       Avg[Mt]   Last/Curr[Mt]        Self[Mt]     Self[%] 	 Name

	main:

		   13458.209 Mt   100.000 %         1 x    13458.210 Mt    13458.209 Mt    11528.544 Mt    85.662 % 	 /

		      61.283 Mt     0.455 %         1 x       61.283 Mt        0.000 Mt        0.763 Mt     1.245 % 	 ./Initialize

		       1.608 Mt     2.624 %         1 x        1.608 Mt        0.000 Mt        1.608 Mt   100.000 % 	 ../ShadowMapRLInitialization

		       6.097 Mt     9.949 %         1 x        6.097 Mt        0.000 Mt        6.097 Mt   100.000 % 	 ../AmbientOcclusionRLInitialization

		       3.586 Mt     5.851 %         1 x        3.586 Mt        0.000 Mt        3.586 Mt   100.000 % 	 ../RasterResolveRLInitialization

		       3.772 Mt     6.156 %         1 x        3.772 Mt        0.000 Mt        3.772 Mt   100.000 % 	 ../DeferredRLInitialization

		      41.494 Mt    67.709 %         1 x       41.494 Mt        0.000 Mt        1.795 Mt     4.326 % 	 ../RayTracingRLInitialization

		      39.699 Mt    95.674 %         1 x       39.699 Mt        0.000 Mt       39.699 Mt   100.000 % 	 .../PipelineBuild

		       3.963 Mt     6.466 %         1 x        3.963 Mt        0.000 Mt        3.963 Mt   100.000 % 	 ../ResolveRLInitialization

		    1868.382 Mt    13.883 %       632 x        2.956 Mt        1.938 Mt       14.315 Mt     0.766 % 	 ./Run

		     404.972 Mt    21.675 %       798 x        0.507 Mt        0.018 Mt      190.048 Mt    46.929 % 	 ../Update

		     145.317 Mt    35.883 %         3 x       48.439 Mt        0.000 Mt       55.434 Mt    38.147 % 	 .../ScenePrep

		       4.449 Mt     3.062 %         3 x        1.483 Mt        0.000 Mt        4.449 Mt   100.000 % 	 ..../SceneDesc-CreateScene

		       1.120 Mt     0.771 %         3 x        0.373 Mt        0.000 Mt        0.003 Mt     0.304 % 	 ..../ShadowMapRLPrep

		       1.117 Mt    99.696 %         3 x        0.372 Mt        0.000 Mt        1.035 Mt    92.666 % 	 ...../PrepareForRendering

		       0.082 Mt     7.334 %         3 x        0.027 Mt        0.000 Mt        0.082 Mt   100.000 % 	 ....../PrepareDrawBundle

		       3.130 Mt     2.154 %         3 x        1.043 Mt        0.000 Mt        0.003 Mt     0.083 % 	 ..../DeferredRLPrep

		       3.127 Mt    99.917 %         3 x        1.042 Mt        0.000 Mt        2.698 Mt    86.287 % 	 ...../PrepareForRendering

		       0.051 Mt     1.647 %         3 x        0.017 Mt        0.000 Mt        0.051 Mt   100.000 % 	 ....../PrepareMaterials

		       0.249 Mt     7.960 %         3 x        0.083 Mt        0.000 Mt        0.249 Mt   100.000 % 	 ....../BuildMaterialCache

		       0.128 Mt     4.106 %         3 x        0.043 Mt        0.000 Mt        0.128 Mt   100.000 % 	 ....../PrepareDrawBundle

		      55.494 Mt    38.188 %         3 x       18.498 Mt        0.000 Mt        8.182 Mt    14.743 % 	 ..../RayTracingRLPrep

		       1.758 Mt     3.169 %         3 x        0.586 Mt        0.000 Mt        1.758 Mt   100.000 % 	 ...../PrepareAccelerationStructures

		       0.046 Mt     0.083 %         3 x        0.015 Mt        0.000 Mt        0.046 Mt   100.000 % 	 ...../PrepareCache

		      27.983 Mt    50.425 %         3 x        9.328 Mt        0.000 Mt       27.983 Mt   100.000 % 	 ...../PipelineBuild

		       0.474 Mt     0.854 %         3 x        0.158 Mt        0.000 Mt        0.474 Mt   100.000 % 	 ...../BuildCache

		      16.158 Mt    29.116 %         3 x        5.386 Mt        0.000 Mt        0.003 Mt     0.019 % 	 ...../BuildAccelerationStructures

		      16.155 Mt    99.981 %         3 x        5.385 Mt        0.000 Mt        1.510 Mt     9.350 % 	 ....../AccelerationStructureBuild

		      13.520 Mt    83.694 %         3 x        4.507 Mt        0.000 Mt       13.520 Mt   100.000 % 	 ......./BuildBottomLevelAS

		       1.124 Mt     6.956 %         3 x        0.375 Mt        0.000 Mt        1.124 Mt   100.000 % 	 ......./BuildTopLevelAS

		       0.893 Mt     1.610 %         3 x        0.298 Mt        0.000 Mt        0.893 Mt   100.000 % 	 ...../BuildShaderTables

		      25.690 Mt    17.678 %         3 x        8.563 Mt        0.000 Mt       25.690 Mt   100.000 % 	 ..../GpuSidePrep

		      69.607 Mt    17.188 %        75 x        0.928 Mt        0.000 Mt       23.925 Mt    34.371 % 	 .../AccelerationStructureBuild

		      18.246 Mt    26.213 %        75 x        0.243 Mt        0.000 Mt       18.246 Mt   100.000 % 	 ..../BuildBottomLevelAS

		      27.437 Mt    39.416 %        75 x        0.366 Mt        0.000 Mt       27.437 Mt   100.000 % 	 ..../BuildTopLevelAS

		    1449.095 Mt    77.559 %       632 x        2.293 Mt        1.852 Mt        0.460 Mt     0.032 % 	 ../Render

		    1448.635 Mt    99.968 %       632 x        2.292 Mt        1.851 Mt       54.233 Mt     3.744 % 	 .../Render

		    1309.042 Mt    90.364 %       632 x        2.071 Mt        1.644 Mt     1309.042 Mt   100.000 % 	 ..../Present

		      85.361 Mt     5.892 %       630 x        0.135 Mt        0.126 Mt       84.588 Mt    99.095 % 	 ..../RayTracing

		       0.420 Mt     0.492 %       630 x        0.001 Mt        0.001 Mt        0.420 Mt   100.000 % 	 ...../DeferredRLPrep

		       0.353 Mt     0.413 %       630 x        0.001 Mt        0.001 Mt        0.353 Mt   100.000 % 	 ...../RayTracingRLPrep

		       0.001 Mt     0.000 %         1 x        0.001 Mt        0.001 Mt        0.001 Mt   100.000 % 	 ./Destroy

	WindowThread:

		   13456.797 Mt   100.000 %         1 x    13456.797 Mt    13456.797 Mt    13456.797 Mt   100.000 % 	 /

	GpuThread:

		    1088.235 Mt   100.000 %       704 x        1.546 Mt        1.345 Mt       13.479 Mt     1.239 % 	 /

		       8.214 Mt     0.755 %       632 x        0.013 Mt        0.002 Mt        8.214 Mt   100.000 % 	 ./Present

		      16.004 Mt     1.471 %         3 x        5.335 Mt        0.000 Mt        0.010 Mt     0.061 % 	 ./ScenePrep

		      15.995 Mt    99.939 %         3 x        5.332 Mt        0.000 Mt        0.003 Mt     0.016 % 	 ../AccelerationStructureBuild

		      15.635 Mt    97.753 %         3 x        5.212 Mt        0.000 Mt       15.635 Mt   100.000 % 	 .../BuildBottomLevelASGpu

		       0.357 Mt     2.231 %         3 x        0.119 Mt        0.000 Mt        0.357 Mt   100.000 % 	 .../BuildTopLevelASGpu

		      15.345 Mt     1.410 %        75 x        0.205 Mt        0.000 Mt        0.084 Mt     0.549 % 	 ./AccelerationStructureBuild

		      10.540 Mt    68.685 %        75 x        0.141 Mt        0.000 Mt       10.540 Mt   100.000 % 	 ../BuildBottomLevelASGpu

		       4.721 Mt    30.766 %        75 x        0.063 Mt        0.000 Mt        4.721 Mt   100.000 % 	 ../BuildTopLevelASGpu

		    1035.192 Mt    95.126 %       630 x        1.643 Mt        1.343 Mt        2.087 Mt     0.202 % 	 ./RayTracingGpu

		     140.169 Mt    13.540 %       630 x        0.222 Mt        0.163 Mt      140.169 Mt   100.000 % 	 ../DeferredRLGpu

		     391.691 Mt    37.838 %       630 x        0.622 Mt        0.433 Mt      391.691 Mt   100.000 % 	 ../RayTracingRLGpu

		     501.246 Mt    48.421 %       630 x        0.796 Mt        0.743 Mt      501.246 Mt   100.000 % 	 ../ResolveRLGpu


	============================


