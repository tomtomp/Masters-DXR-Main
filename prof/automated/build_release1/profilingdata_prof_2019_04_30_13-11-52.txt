Configuration: 
Command line parameters  = 
Window width             = 1186
Window height            = 1219
Window title             = Direct3D App
Back-buffer count        = 3
Fill Triangles           = enabled
Triangles Font Clock-Wise= disabled
Fast Build AS            = disabled
Target FPS               = 0
Target UPS               = 60
Tearing                  = enabled
VSync                    = disabled
GUI rendering            = disabled
Debug layer              = enabled
GPU validation           = disabled
GPU profiling            = enabled
Render mode              = Ray Tracing
Loaded scene file        = 
Using testing scene      = disabled
BLAS duplication         = disabled
Rays per pixel           = 9
Quality preset           = Medium
Rasterization Quality    : 
  AO                     = enabled
  AO kernel size         = 4
  Shadows                = enabled
  Shadow map resolution  = 4096
  Shadow PCF kernel size = 4
  Reflections            = disabled
Ray Tracing Quality      : 
  AO                     = enabled
  AO rays                = 4
  AO kernel size         = 4
  Shadows                = enabled
  Shadow rays            = 4
  Shadow kernel size     = 4
  Reflections            = disabled

ProfilingAggregator: 
Render	,	16.2851	,	15.2441	,	16.7633	,	16.4418
Update	,	0.0825	,	0.0243	,	0.0916	,	0.0836
RayTracing	,	7.815	,	6.9832	,	8.1433	,	7.484
RayTracingGpu	,	2.79958	,	2.67773	,	2.46874	,	2.70157
DeferredRLGpu	,	0.260384	,	0.251936	,	0.249632	,	0.254016
RayTracingRLGpu	,	1.63322	,	1.50909	,	1.30522	,	1.5344
ResolveRLGpu	,	0.904	,	0.914528	,	0.912	,	0.91056
Index	,	0	,	1	,	2	,	3

ProfilingBuildAggregator: 
BuildBottomLevelAS	,	12.2172
BuildBottomLevelASGpu	,	15.1346
BuildTopLevelAS	,	0.4266
BuildTopLevelASGpu	,	0.218432
Triangles	,	281182
Meshes	,	52
BottomLevels	,	43
TopLevelSize	,	2752
BottomLevelsSize	,	15869344
Index	,	0

FpsAggregator: 
FPS	,	25	,	36	,	38	,	38
UPS	,	158	,	61	,	60	,	60
FrameTime	,	40.3356	,	27.9492	,	26.3353	,	26.7122
GigaRays/s	,	0.299436	,	0.432138	,	0.45862	,	0.45215
Index	,	0	,	1	,	2	,	3


AccelerationStructure: 
Ray tracing acceleration structure building statistics: 
	Bottom Level Acceleration Structure: 
		Maximum [B]: 2940416
		Minimum [B]: 6256
		Currently Allocated [B]: 15869344
		Currently Created [num]: 43
		Current Average [B]: 369054
		Maximum Triangles [num]: 52674
		Minimum Triangles [num]: 27
		Total Triangles [num]: 281182
		Maximum Meshes [num]: 2
		Minimum Meshes [num]: 1
		Total Meshes [num]: 52
	Top Level Acceleration Structure: 
		Maximum [B]: 2752
		Minimum [B]: 2752
		Currently Allocated [B]: 2752
		Total Allocated [B]: 2752
		Total Created [num]: 1
		Average Allocated [B]: 2752
		Maximum Instances [num]: 43
		Minimum Instances [num]: 43
		Total Instances [num]: 43
	Scratch Buffer: 
		Maximum [B]: 2344448
		Minimum [B]: 2344448
		Currently Allocated [B]: 2344448
		Total Allocated [B]: 2344448
		Total Created [num]: 1
		Average Allocated [B]: 2344448
ProfilingStack: 
[PROF] Profiling statistics: 
	Scopes entered : 2734
	Scopes exited : 2734
	Overhead per scope [ticks] : 161.6
	Threads entered : 3
	Threads exited : 0
	============================
		         Sum[Mt]   Parent[%]  Samples[num]       Avg[Mt]   Last/Curr[Mt]        Self[Mt]     Self[%] 	 Name

	main:

		    7117.454 Mt   100.000 %         1 x     7117.455 Mt     7117.454 Mt     3823.291 Mt    53.717 % 	 /

		      82.745 Mt     1.163 %         1 x       82.745 Mt        0.000 Mt        0.654 Mt     0.790 % 	 ./Initialize

		       2.852 Mt     3.446 %         1 x        2.852 Mt        0.000 Mt        2.852 Mt   100.000 % 	 ../TexturedRLInitialization

		       2.252 Mt     2.722 %         1 x        2.252 Mt        0.000 Mt        2.252 Mt   100.000 % 	 ../ShadowMapRLInitialization

		      13.808 Mt    16.688 %         1 x       13.808 Mt        0.000 Mt       13.808 Mt   100.000 % 	 ../AmbientOcclusionRLInitialization

		      14.270 Mt    17.246 %         1 x       14.270 Mt        0.000 Mt       14.270 Mt   100.000 % 	 ../RasterResolveRLInitialization

		       7.039 Mt     8.507 %         1 x        7.039 Mt        0.000 Mt        7.039 Mt   100.000 % 	 ../DeferredRLInitialization

		      37.399 Mt    45.198 %         1 x       37.399 Mt        0.000 Mt        1.315 Mt     3.516 % 	 ../RayTracingRLInitialization

		      36.084 Mt    96.484 %         1 x       36.084 Mt        0.000 Mt       36.084 Mt   100.000 % 	 .../PipelineBuild

		       4.471 Mt     5.404 %         1 x        4.471 Mt        0.000 Mt        4.471 Mt   100.000 % 	 ../ResolveRLInitialization

		    3211.418 Mt    45.120 %       176 x       18.247 Mt       17.298 Mt        6.250 Mt     0.195 % 	 ./Run

		     308.503 Mt     9.606 %       413 x        0.747 Mt        0.090 Mt      174.106 Mt    56.436 % 	 ../Update

		      57.417 Mt    18.611 %         1 x       57.417 Mt        0.000 Mt       53.131 Mt    92.535 % 	 .../ScenePrep

		       4.286 Mt     7.465 %         1 x        4.286 Mt        0.000 Mt        4.286 Mt   100.000 % 	 ..../SceneDesc-CreateScene

		      76.980 Mt    24.953 %        62 x        1.242 Mt        0.000 Mt       26.558 Mt    34.500 % 	 .../AccelerationStructureBuild

		      19.624 Mt    25.492 %        62 x        0.317 Mt        0.000 Mt       19.624 Mt   100.000 % 	 ..../BuildBottomLevelAS

		      30.798 Mt    40.008 %        62 x        0.497 Mt        0.000 Mt       30.798 Mt   100.000 % 	 ..../BuildTopLevelAS

		    2896.666 Mt    90.199 %       176 x       16.458 Mt       17.204 Mt        0.208 Mt     0.007 % 	 ../Render

		    2896.458 Mt    99.993 %       176 x       16.457 Mt       17.203 Mt     1564.752 Mt    54.023 % 	 .../Render

		    1331.706 Mt    45.977 %       174 x        7.653 Mt        9.061 Mt     1305.667 Mt    98.045 % 	 ..../RayTracing

		       4.633 Mt     0.348 %       174 x        0.027 Mt        0.001 Mt        0.250 Mt     5.392 % 	 ...../DeferredRLPrep

		       4.383 Mt    94.608 %         1 x        4.383 Mt        0.000 Mt        2.312 Mt    52.760 % 	 ....../PrepareForRendering

		       0.061 Mt     1.399 %         1 x        0.061 Mt        0.000 Mt        0.061 Mt   100.000 % 	 ......./PrepareMaterials

		       0.252 Mt     5.738 %         1 x        0.252 Mt        0.000 Mt        0.252 Mt   100.000 % 	 ......./BuildMaterialCache

		       1.758 Mt    40.104 %         1 x        1.758 Mt        0.000 Mt        1.758 Mt   100.000 % 	 ......./PrepareDrawBundle

		      21.406 Mt     1.607 %       174 x        0.123 Mt        0.003 Mt        6.151 Mt    28.734 % 	 ...../RayTracingRLPrep

		       1.157 Mt     5.406 %         1 x        1.157 Mt        0.000 Mt        1.157 Mt   100.000 % 	 ....../PrepareAccelerationStructures

		       0.041 Mt     0.193 %         1 x        0.041 Mt        0.000 Mt        0.041 Mt   100.000 % 	 ....../PrepareCache

		       0.446 Mt     2.085 %         1 x        0.446 Mt        0.000 Mt        0.446 Mt   100.000 % 	 ....../BuildCache

		      13.251 Mt    61.902 %         1 x       13.251 Mt        0.000 Mt        0.002 Mt     0.012 % 	 ....../BuildAccelerationStructures

		      13.249 Mt    99.988 %         1 x       13.249 Mt        0.000 Mt        0.606 Mt     4.571 % 	 ......./AccelerationStructureBuild

		      12.217 Mt    92.209 %         1 x       12.217 Mt        0.000 Mt       12.217 Mt   100.000 % 	 ......../BuildBottomLevelAS

		       0.427 Mt     3.220 %         1 x        0.427 Mt        0.000 Mt        0.427 Mt   100.000 % 	 ......../BuildTopLevelAS

		       0.359 Mt     1.679 %         1 x        0.359 Mt        0.000 Mt        0.359 Mt   100.000 % 	 ....../BuildShaderTables

		       0.001 Mt     0.000 %         1 x        0.001 Mt        0.001 Mt        0.001 Mt   100.000 % 	 ./Destroy

	WindowThread:

		    7115.831 Mt   100.000 %         1 x     7115.831 Mt     7115.831 Mt     7115.831 Mt   100.000 % 	 /

	GpuThread:

		     503.352 Mt   100.000 %       237 x        2.124 Mt        2.486 Mt       -0.109 Mt    -0.022 % 	 /

		       1.840 Mt     0.365 %       176 x        0.010 Mt        0.001 Mt        1.840 Mt   100.000 % 	 ./Present

		      38.024 Mt     7.554 %        62 x        0.613 Mt        0.000 Mt        0.086 Mt     0.225 % 	 ./AccelerationStructureBuild

		      32.114 Mt    84.459 %        62 x        0.518 Mt        0.000 Mt       32.114 Mt   100.000 % 	 ../BuildBottomLevelASGpu

		       5.824 Mt    15.316 %        62 x        0.094 Mt        0.000 Mt        5.824 Mt   100.000 % 	 ../BuildTopLevelASGpu

		     463.598 Mt    92.102 %       174 x        2.664 Mt        2.484 Mt        0.460 Mt     0.099 % 	 ./RayTracingGpu

		      44.515 Mt     9.602 %       174 x        0.256 Mt        0.255 Mt       44.515 Mt   100.000 % 	 ../DeferredRLGpu

		      15.354 Mt     3.312 %         1 x       15.354 Mt        0.000 Mt        0.001 Mt     0.008 % 	 ../AccelerationStructureBuild

		      15.135 Mt    98.569 %         1 x       15.135 Mt        0.000 Mt       15.135 Mt   100.000 % 	 .../BuildBottomLevelASGpu

		       0.218 Mt     1.423 %         1 x        0.218 Mt        0.000 Mt        0.218 Mt   100.000 % 	 .../BuildTopLevelASGpu

		     240.003 Mt    51.770 %       174 x        1.379 Mt        1.309 Mt      240.003 Mt   100.000 % 	 ../RayTracingRLGpu

		     163.267 Mt    35.217 %       174 x        0.938 Mt        0.917 Mt      163.267 Mt   100.000 % 	 ../ResolveRLGpu


	============================


