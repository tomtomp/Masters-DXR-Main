Configuration: 
Command line parameters  = E:\programming\FIT-DIP\Masters-DXR-Main-git\build\Measurement\x64\Debug\Measurement.exe --scene Suzanne/Suzanne.gltf --profile-builds --profile-output BuildsSuzanne 
Window width             = 1024
Window height            = 768
Window title             = Direct3D App
Back-buffer count        = 3
Fill Triangles           = enabled
Triangles Font Clock-Wise= enabled
Fast Build AS            = disabled
Target FPS               = 60
Target UPS               = 60
Tearing                  = disabled
VSync                    = disabled
GUI rendering            = enabled
Debug layer              = enabled
GPU validation           = disabled
GPU profiling            = enabled
Render mode              = Rasterization
Loaded scene file        = Suzanne/Suzanne.gltf
Using testing scene      = disabled
Duplication              = 1
Duplication in cube      = disabled
Duplication offset       = 10
BLAS duplication         = disabled
Rays per pixel           = 10

ProfilingAggregator: 

ProfilingBuildAggregator: 
BuildBottomLevelAS	,	2.6415	,	1.1672
BuildBottomLevelASGpu	,	0.352064	,	0.61968
BuildTopLevelAS	,	1.6797	,	1.1211
BuildTopLevelASGpu	,	0.08368	,	0.084096
Duplication	,	1	,	1
Triangles	,	3936	,	3936
Meshes	,	1	,	1
BottomLevels	,	1	,	1
Index	,	0	,	1

FpsAggregator: 
FPS	,	0	,	0
UPS	,	0	,	0
FrameTime	,	0	,	0
GigaRays/s	,	0	,	0
Index	,	0	,	1


AccelerationStructure: 
Ray tracing acceleration structure building statistics: 
	Bottom Level Acceleration Structure: 
		Maximum [B]: 224128
		Minimum [B]: 224128
		Currently Allocated [B]: 224128
		Currently Created [num]: 1
		Current Average [B]: 224128
		Maximum Triangles [num]: 3936
		Minimum Triangles [num]: 3936
		Total Triangles [num]: 3936
		Maximum Meshes [num]: 1
		Minimum Meshes [num]: 1
		Total Meshes [num]: 1
	Top Level Acceleration Structure: 
		Maximum [B]: 64
		Minimum [B]: 64
		Currently Allocated [B]: 64
		Total Allocated [B]: 64
		Total Created [num]: 1
		Average Allocated [B]: 64
		Maximum Instances [num]: 1
		Minimum Instances [num]: 1
		Total Instances [num]: 1
	Scratch Buffer: 
		Maximum [B]: 34304
		Minimum [B]: 34304
		Currently Allocated [B]: 34304
		Total Allocated [B]: 34304
		Total Created [num]: 1
		Average Allocated [B]: 34304
ProfilingStack: 
[PROF] Profiling statistics: 
	Scopes entered : 55
	Scopes exited : 54
	Overhead per scope [ticks] : 1041.9
	Threads entered : 3
	Threads exited : 0
	============================
		         Sum[Mt]   Parent[%]  Samples[num]       Avg[Mt]   Last/Curr[Mt]        Self[Mt]     Self[%] 	 Name

	main:

		     821.011 Mt   100.000 %         1 x      821.013 Mt      821.010 Mt        0.745 Mt     0.091 % 	 /

		     820.326 Mt    99.917 %         1 x      820.328 Mt      820.326 Mt      347.929 Mt    42.413 % 	 ./Main application loop

		       3.025 Mt     0.369 %         1 x        3.025 Mt        3.025 Mt        3.025 Mt   100.000 % 	 ../TexturedRLInitialization

		       6.153 Mt     0.750 %         1 x        6.153 Mt        6.153 Mt        6.153 Mt   100.000 % 	 ../DeferredRLInitialization

		      82.044 Mt    10.001 %         1 x       82.044 Mt       82.044 Mt        2.565 Mt     3.126 % 	 ../RayTracingRLInitialization

		      79.479 Mt    96.874 %         1 x       79.479 Mt       79.479 Mt       79.479 Mt   100.000 % 	 .../PipelineBuild

		       4.731 Mt     0.577 %         1 x        4.731 Mt        4.731 Mt        4.731 Mt   100.000 % 	 ../ResolveRLInitialization

		     325.164 Mt    39.638 %         1 x      325.164 Mt      325.164 Mt        9.102 Mt     2.799 % 	 ../Initialization

		     316.063 Mt    97.201 %         1 x      316.063 Mt      316.063 Mt        3.837 Mt     1.214 % 	 .../ScenePrep

		     259.584 Mt    82.130 %         1 x      259.584 Mt      259.584 Mt       28.387 Mt    10.935 % 	 ..../SceneLoad

		     189.788 Mt    73.112 %         1 x      189.788 Mt      189.788 Mt       41.389 Mt    21.808 % 	 ...../glTF-LoadScene

		     148.399 Mt    78.192 %         2 x       74.199 Mt       67.956 Mt      148.399 Mt   100.000 % 	 ....../glTF-LoadImageData

		      23.922 Mt     9.216 %         1 x       23.922 Mt       23.922 Mt       23.922 Mt   100.000 % 	 ...../glTF-CreateScene

		      17.486 Mt     6.736 %         1 x       17.486 Mt       17.486 Mt       17.486 Mt   100.000 % 	 ...../SceneDesc-CreateScene

		       3.428 Mt     1.085 %         1 x        3.428 Mt        3.428 Mt        0.016 Mt     0.464 % 	 ..../TexturedRLPrep

		       3.412 Mt    99.536 %         1 x        3.412 Mt        3.412 Mt        1.087 Mt    31.850 % 	 ...../PrepareForRendering

		       1.405 Mt    41.170 %         1 x        1.405 Mt        1.405 Mt        1.405 Mt   100.000 % 	 ....../PrepareMaterials

		       0.921 Mt    26.980 %         1 x        0.921 Mt        0.921 Mt        0.921 Mt   100.000 % 	 ....../PrepareDrawBundle

		       7.827 Mt     2.476 %         1 x        7.827 Mt        7.827 Mt        0.015 Mt     0.198 % 	 ..../DeferredRLPrep

		       7.812 Mt    99.802 %         1 x        7.812 Mt        7.812 Mt        5.647 Mt    72.286 % 	 ...../PrepareForRendering

		       0.057 Mt     0.725 %         1 x        0.057 Mt        0.057 Mt        0.057 Mt   100.000 % 	 ....../PrepareMaterials

		       1.247 Mt    15.962 %         1 x        1.247 Mt        1.247 Mt        1.247 Mt   100.000 % 	 ....../BuildMaterialCache

		       0.861 Mt    11.027 %         1 x        0.861 Mt        0.861 Mt        0.861 Mt   100.000 % 	 ....../PrepareDrawBundle

		      26.781 Mt     8.473 %         1 x       26.781 Mt       26.781 Mt        9.221 Mt    34.431 % 	 ..../RayTracingRLPrep

		       0.741 Mt     2.766 %         1 x        0.741 Mt        0.741 Mt        0.741 Mt   100.000 % 	 ...../PrepareAccelerationStructures

		       0.078 Mt     0.291 %         1 x        0.078 Mt        0.078 Mt        0.078 Mt   100.000 % 	 ...../PrepareCache

		       2.042 Mt     7.625 %         1 x        2.042 Mt        2.042 Mt        2.042 Mt   100.000 % 	 ...../BuildCache

		       8.176 Mt    30.528 %         1 x        8.176 Mt        8.176 Mt        0.011 Mt     0.138 % 	 ...../BuildAccelerationStructures

		       8.165 Mt    99.862 %         1 x        8.165 Mt        8.165 Mt        4.932 Mt    60.410 % 	 ....../AccelerationStructureBuild

		       1.657 Mt    20.301 %         1 x        1.657 Mt        1.657 Mt        1.657 Mt   100.000 % 	 ......./BuildBottomLevelAS

		       1.575 Mt    19.289 %         1 x        1.575 Mt        1.575 Mt        1.575 Mt   100.000 % 	 ......./BuildTopLevelAS

		       6.523 Mt    24.357 %         1 x        6.523 Mt        6.523 Mt        6.523 Mt   100.000 % 	 ...../BuildShaderTables

		      14.606 Mt     4.621 %         1 x       14.606 Mt       14.606 Mt       14.606 Mt   100.000 % 	 ..../GpuSidePrep

		      51.280 Mt     6.251 %         3 x       17.093 Mt       10.443 Mt       39.158 Mt    76.361 % 	 ../AccelerationStructureBuild

		       6.391 Mt    12.463 %         3 x        2.130 Mt        1.167 Mt        6.391 Mt   100.000 % 	 .../BuildBottomLevelAS

		       5.731 Mt    11.175 %         3 x        1.910 Mt        1.121 Mt        5.731 Mt   100.000 % 	 .../BuildTopLevelAS

	WindowThread:

		     811.700 Mt   100.000 %         1 x      811.701 Mt      811.700 Mt      811.700 Mt   100.000 % 	 /

	GpuThread:

		      82.123 Mt   100.000 %         2 x       41.062 Mt        0.705 Mt       80.097 Mt    97.532 % 	 /

		       0.447 Mt     0.545 %         1 x        0.447 Mt        0.447 Mt        0.006 Mt     1.452 % 	 ./ScenePrep

		       0.441 Mt    98.548 %         1 x        0.441 Mt        0.441 Mt        0.001 Mt     0.334 % 	 ../AccelerationStructureBuild

		       0.355 Mt    80.450 %         1 x        0.355 Mt        0.355 Mt        0.355 Mt   100.000 % 	 .../BuildBottomLevelASGpu

		       0.085 Mt    19.216 %         1 x        0.085 Mt        0.085 Mt        0.085 Mt   100.000 % 	 .../BuildTopLevelASGpu

		       1.579 Mt     1.923 %         3 x        0.526 Mt        0.705 Mt        0.004 Mt     0.249 % 	 ./AccelerationStructureBuild

		       1.324 Mt    83.828 %         3 x        0.441 Mt        0.620 Mt        1.324 Mt   100.000 % 	 ../BuildBottomLevelASGpu

		       0.251 Mt    15.923 %         3 x        0.084 Mt        0.084 Mt        0.251 Mt   100.000 % 	 ../BuildTopLevelASGpu


	============================


