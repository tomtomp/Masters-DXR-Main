Configuration: 
Command line parameters  = Measurement.exe --scene Dragon/Dragon.glb --profile-builds --profile-output BuildsDragon 
Window width             = 1024
Window height            = 768
Window title             = Direct3D App
Back-buffer count        = 3
Fill Triangles           = enabled
Triangles Font Clock-Wise= enabled
Fast Build AS            = disabled
Target FPS               = 60
Target UPS               = 60
Tearing                  = disabled
VSync                    = disabled
GUI rendering            = enabled
Debug layer              = enabled
GPU validation           = disabled
GPU profiling            = enabled
Render mode              = Rasterization
Loaded scene file        = Dragon/Dragon.glb
Using testing scene      = disabled
Duplication              = 1
Duplication in cube      = disabled
Duplication offset       = 10
BLAS duplication         = disabled
Rays per pixel           = 10

ProfilingAggregator: 

ProfilingBuildAggregator: 
BuildBottomLevelAS	,	1.0459	,	0.8914	,	0.8204	,	1.3472
BuildBottomLevelASGpu	,	0.561536	,	0.520256	,	0.870176	,	0.820928
BuildTopLevelAS	,	0.887	,	0.7508	,	0.7533	,	1.4451
BuildTopLevelASGpu	,	0.077888	,	0.083072	,	0.08208	,	0.08464
Duplication	,	1	,	1	,	1	,	1
Triangles	,	34852	,	11757	,	34852	,	11757
Meshes	,	1	,	1	,	1	,	1
BottomLevels	,	1	,	1	,	1	,	1
Index	,	0	,	1	,	2	,	3

FpsAggregator: 
FPS	,	0	,	0	,	0	,	0
UPS	,	0	,	0	,	0	,	0
FrameTime	,	0	,	0	,	0	,	0
GigaRays/s	,	0	,	0	,	0	,	0
Index	,	0	,	1	,	2	,	3


AccelerationStructure: 
Ray tracing acceleration structure building statistics: 
	Bottom Level Acceleration Structure: 
		Maximum [B]: 1947136
		Minimum [B]: 659968
		Currently Allocated [B]: 2607104
		Currently Created [num]: 2
		Current Average [B]: 1303552
		Maximum Triangles [num]: 34852
		Minimum Triangles [num]: 11757
		Total Triangles [num]: 46609
		Maximum Meshes [num]: 1
		Minimum Meshes [num]: 1
		Total Meshes [num]: 2
	Top Level Acceleration Structure: 
		Maximum [B]: 128
		Minimum [B]: 128
		Currently Allocated [B]: 128
		Total Allocated [B]: 128
		Total Created [num]: 1
		Average Allocated [B]: 128
		Maximum Instances [num]: 2
		Minimum Instances [num]: 2
		Total Instances [num]: 2
	Scratch Buffer: 
		Maximum [B]: 290304
		Minimum [B]: 290304
		Currently Allocated [B]: 290304
		Total Allocated [B]: 290304
		Total Created [num]: 1
		Average Allocated [B]: 290304
ProfilingStack: 
[PROF] Profiling statistics: 
	Scopes entered : 73
	Scopes exited : 72
	Overhead per scope [ticks] : 1003.3
	Threads entered : 3
	Threads exited : 0
	============================
		         Sum[Mt]   Parent[%]  Samples[num]       Avg[Mt]   Last/Curr[Mt]        Self[Mt]     Self[%] 	 Name

	main:

		    2633.419 Mt   100.000 %         1 x     2633.421 Mt     2633.418 Mt        0.755 Mt     0.029 % 	 /

		    2632.760 Mt    99.975 %         1 x     2632.763 Mt     2632.759 Mt      344.997 Mt    13.104 % 	 ./Main application loop

		       2.822 Mt     0.107 %         1 x        2.822 Mt        2.822 Mt        2.822 Mt   100.000 % 	 ../TexturedRLInitialization

		       6.594 Mt     0.250 %         1 x        6.594 Mt        6.594 Mt        6.594 Mt   100.000 % 	 ../DeferredRLInitialization

		      48.717 Mt     1.850 %         1 x       48.717 Mt       48.717 Mt        2.363 Mt     4.850 % 	 ../RayTracingRLInitialization

		      46.354 Mt    95.150 %         1 x       46.354 Mt       46.354 Mt       46.354 Mt   100.000 % 	 .../PipelineBuild

		       3.237 Mt     0.123 %         1 x        3.237 Mt        3.237 Mt        3.237 Mt   100.000 % 	 ../ResolveRLInitialization

		    2186.743 Mt    83.059 %         1 x     2186.743 Mt     2186.743 Mt        2.544 Mt     0.116 % 	 ../Initialization

		    2184.198 Mt    99.884 %         1 x     2184.198 Mt     2184.198 Mt        1.947 Mt     0.089 % 	 .../ScenePrep

		    2156.214 Mt    98.719 %         1 x     2156.214 Mt     2156.214 Mt       83.752 Mt     3.884 % 	 ..../SceneLoad

		    1980.679 Mt    91.859 %         1 x     1980.679 Mt     1980.679 Mt      154.326 Mt     7.792 % 	 ...../glTF-LoadScene

		    1826.353 Mt    92.208 %         8 x      228.294 Mt      365.124 Mt     1826.353 Mt   100.000 % 	 ....../glTF-LoadImageData

		      84.089 Mt     3.900 %         1 x       84.089 Mt       84.089 Mt       84.089 Mt   100.000 % 	 ...../glTF-CreateScene

		       7.694 Mt     0.357 %         1 x        7.694 Mt        7.694 Mt        7.694 Mt   100.000 % 	 ...../SceneDesc-CreateScene

		       0.998 Mt     0.046 %         1 x        0.998 Mt        0.998 Mt        0.005 Mt     0.471 % 	 ..../TexturedRLPrep

		       0.993 Mt    99.529 %         1 x        0.993 Mt        0.993 Mt        0.281 Mt    28.300 % 	 ...../PrepareForRendering

		       0.381 Mt    38.347 %         1 x        0.381 Mt        0.381 Mt        0.381 Mt   100.000 % 	 ....../PrepareMaterials

		       0.331 Mt    33.353 %         1 x        0.331 Mt        0.331 Mt        0.331 Mt   100.000 % 	 ....../PrepareDrawBundle

		       1.980 Mt     0.091 %         1 x        1.980 Mt        1.980 Mt        0.004 Mt     0.227 % 	 ..../DeferredRLPrep

		       1.976 Mt    99.773 %         1 x        1.976 Mt        1.976 Mt        1.353 Mt    68.507 % 	 ...../PrepareForRendering

		       0.029 Mt     1.448 %         1 x        0.029 Mt        0.029 Mt        0.029 Mt   100.000 % 	 ....../PrepareMaterials

		       0.307 Mt    15.544 %         1 x        0.307 Mt        0.307 Mt        0.307 Mt   100.000 % 	 ....../BuildMaterialCache

		       0.286 Mt    14.501 %         1 x        0.286 Mt        0.286 Mt        0.286 Mt   100.000 % 	 ....../PrepareDrawBundle

		      10.911 Mt     0.500 %         1 x       10.911 Mt       10.911 Mt        3.883 Mt    35.591 % 	 ..../RayTracingRLPrep

		       0.402 Mt     3.685 %         1 x        0.402 Mt        0.402 Mt        0.402 Mt   100.000 % 	 ...../PrepareAccelerationStructures

		       0.036 Mt     0.334 %         1 x        0.036 Mt        0.036 Mt        0.036 Mt   100.000 % 	 ...../PrepareCache

		       0.424 Mt     3.891 %         1 x        0.424 Mt        0.424 Mt        0.424 Mt   100.000 % 	 ...../BuildCache

		       4.911 Mt    45.010 %         1 x        4.911 Mt        4.911 Mt        0.005 Mt     0.092 % 	 ...../BuildAccelerationStructures

		       4.906 Mt    99.908 %         1 x        4.906 Mt        4.906 Mt        2.174 Mt    44.307 % 	 ....../AccelerationStructureBuild

		       1.837 Mt    37.435 %         1 x        1.837 Mt        1.837 Mt        1.837 Mt   100.000 % 	 ......./BuildBottomLevelAS

		       0.896 Mt    18.258 %         1 x        0.896 Mt        0.896 Mt        0.896 Mt   100.000 % 	 ......./BuildTopLevelAS

		       1.254 Mt    11.490 %         1 x        1.254 Mt        1.254 Mt        1.254 Mt   100.000 % 	 ...../BuildShaderTables

		      12.149 Mt     0.556 %         1 x       12.149 Mt       12.149 Mt       12.149 Mt   100.000 % 	 ..../GpuSidePrep

		      39.651 Mt     1.506 %         5 x        7.930 Mt       11.397 Mt       27.938 Mt    70.460 % 	 ../AccelerationStructureBuild

		       6.411 Mt    16.168 %         5 x        1.282 Mt        1.347 Mt        6.411 Mt   100.000 % 	 .../BuildBottomLevelAS

		       5.302 Mt    13.372 %         5 x        1.060 Mt        1.445 Mt        5.302 Mt   100.000 % 	 .../BuildTopLevelAS

	WindowThread:

		    2630.521 Mt   100.000 %         1 x     2630.522 Mt     2630.520 Mt     2630.521 Mt   100.000 % 	 /

	GpuThread:

		      52.222 Mt   100.000 %         4 x       13.056 Mt        0.907 Mt       46.953 Mt    89.910 % 	 /

		       1.523 Mt     2.917 %         1 x        1.523 Mt        1.523 Mt        0.007 Mt     0.437 % 	 ./ScenePrep

		       1.517 Mt    99.563 %         1 x        1.517 Mt        1.517 Mt        0.001 Mt     0.082 % 	 ../AccelerationStructureBuild

		       1.305 Mt    86.042 %         1 x        1.305 Mt        1.305 Mt        1.305 Mt   100.000 % 	 .../BuildBottomLevelASGpu

		       0.210 Mt    13.875 %         1 x        0.210 Mt        0.210 Mt        0.210 Mt   100.000 % 	 .../BuildTopLevelASGpu

		       3.746 Mt     7.173 %         5 x        0.749 Mt        0.907 Mt        0.006 Mt     0.162 % 	 ./AccelerationStructureBuild

		       3.334 Mt    89.011 %         5 x        0.667 Mt        0.821 Mt        3.334 Mt   100.000 % 	 ../BuildBottomLevelASGpu

		       0.406 Mt    10.826 %         5 x        0.081 Mt        0.085 Mt        0.406 Mt   100.000 % 	 ../BuildTopLevelASGpu


	============================


