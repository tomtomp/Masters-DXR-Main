Configuration: 
Command line parameters  = E:\programming\FIT-DIP\Masters-DXR-Main-git\build\Measurement\x64\Debug\Measurement.exe --scene Dragon/Dragon.glb --profile-builds --profile-output BuildsDragon 
Window width             = 1024
Window height            = 768
Window title             = Direct3D App
Back-buffer count        = 3
Fill Triangles           = enabled
Triangles Font Clock-Wise= enabled
Fast Build AS            = disabled
Target FPS               = 60
Target UPS               = 60
Tearing                  = disabled
VSync                    = disabled
GUI rendering            = enabled
Debug layer              = enabled
GPU validation           = disabled
GPU profiling            = enabled
Render mode              = Rasterization
Loaded scene file        = Dragon/Dragon.glb
Using testing scene      = disabled
Duplication              = 1
Duplication in cube      = disabled
Duplication offset       = 10
BLAS duplication         = disabled
Rays per pixel           = 10

ProfilingAggregator: 

ProfilingBuildAggregator: 
BuildBottomLevelAS	,	1.7737	,	0.8537	,	0.9396	,	0.9185
BuildBottomLevelASGpu	,	1.80208	,	1.55245	,	3.38275	,	2.36995
BuildTopLevelAS	,	1.4819	,	1.1202	,	4.0743	,	3.6486
BuildTopLevelASGpu	,	0.237824	,	0.237024	,	0.260352	,	0.240896
Duplication	,	1	,	1	,	1	,	1
Triangles	,	34852	,	11757	,	34852	,	11757
Meshes	,	1	,	1	,	1	,	1
BottomLevels	,	1	,	1	,	1	,	1
Index	,	0	,	1	,	2	,	3

FpsAggregator: 
FPS	,	0	,	0	,	0	,	0
UPS	,	0	,	0	,	0	,	0
FrameTime	,	0	,	0	,	0	,	0
GigaRays/s	,	0	,	0	,	0	,	0
Index	,	0	,	1	,	2	,	3


AccelerationStructure: 
Ray tracing acceleration structure building statistics: 
	Bottom Level Acceleration Structure: 
		Maximum [B]: 1947136
		Minimum [B]: 659968
		Currently Allocated [B]: 2607104
		Currently Created [num]: 2
		Current Average [B]: 1303552
		Maximum Triangles [num]: 34852
		Minimum Triangles [num]: 11757
		Total Triangles [num]: 46609
		Maximum Meshes [num]: 1
		Minimum Meshes [num]: 1
		Total Meshes [num]: 2
	Top Level Acceleration Structure: 
		Maximum [B]: 128
		Minimum [B]: 128
		Currently Allocated [B]: 128
		Total Allocated [B]: 128
		Total Created [num]: 1
		Average Allocated [B]: 128
		Maximum Instances [num]: 2
		Minimum Instances [num]: 2
		Total Instances [num]: 2
	Scratch Buffer: 
		Maximum [B]: 290304
		Minimum [B]: 290304
		Currently Allocated [B]: 290304
		Total Allocated [B]: 290304
		Total Created [num]: 1
		Average Allocated [B]: 290304
ProfilingStack: 
[PROF] Profiling statistics: 
	Scopes entered : 73
	Scopes exited : 72
	Overhead per scope [ticks] : 1016.8
	Threads entered : 3
	Threads exited : 0
	============================
		         Sum[Mt]   Parent[%]  Samples[num]       Avg[Mt]   Last/Curr[Mt]        Self[Mt]     Self[%] 	 Name

	main:

		    2793.108 Mt   100.000 %         1 x     2793.112 Mt     2793.107 Mt        1.185 Mt     0.042 % 	 /

		    2792.106 Mt    99.964 %         1 x     2792.111 Mt     2792.105 Mt      434.407 Mt    15.558 % 	 ./Main application loop

		       3.022 Mt     0.108 %         1 x        3.022 Mt        3.022 Mt        3.022 Mt   100.000 % 	 ../TexturedRLInitialization

		       6.161 Mt     0.221 %         1 x        6.161 Mt        6.161 Mt        6.161 Mt   100.000 % 	 ../DeferredRLInitialization

		      57.733 Mt     2.068 %         1 x       57.733 Mt       57.733 Mt        2.610 Mt     4.520 % 	 ../RayTracingRLInitialization

		      55.123 Mt    95.480 %         1 x       55.123 Mt       55.123 Mt       55.123 Mt   100.000 % 	 .../PipelineBuild

		       3.297 Mt     0.118 %         1 x        3.297 Mt        3.297 Mt        3.297 Mt   100.000 % 	 ../ResolveRLInitialization

		    2236.617 Mt    80.105 %         1 x     2236.617 Mt     2236.617 Mt        8.853 Mt     0.396 % 	 ../Initialization

		    2227.765 Mt    99.604 %         1 x     2227.765 Mt     2227.765 Mt        1.815 Mt     0.081 % 	 .../ScenePrep

		    2176.867 Mt    97.715 %         1 x     2176.867 Mt     2176.867 Mt       81.495 Mt     3.744 % 	 ..../SceneLoad

		    1967.484 Mt    90.381 %         1 x     1967.484 Mt     1967.484 Mt      153.351 Mt     7.794 % 	 ...../glTF-LoadScene

		    1814.133 Mt    92.206 %         8 x      226.767 Mt      366.410 Mt     1814.133 Mt   100.000 % 	 ....../glTF-LoadImageData

		      98.911 Mt     4.544 %         1 x       98.911 Mt       98.911 Mt       98.911 Mt   100.000 % 	 ...../glTF-CreateScene

		      28.976 Mt     1.331 %         1 x       28.976 Mt       28.976 Mt       28.976 Mt   100.000 % 	 ...../SceneDesc-CreateScene

		       3.120 Mt     0.140 %         1 x        3.120 Mt        3.120 Mt        0.005 Mt     0.163 % 	 ..../TexturedRLPrep

		       3.115 Mt    99.837 %         1 x        3.115 Mt        3.115 Mt        1.167 Mt    37.467 % 	 ...../PrepareForRendering

		       1.595 Mt    51.191 %         1 x        1.595 Mt        1.595 Mt        1.595 Mt   100.000 % 	 ....../PrepareMaterials

		       0.353 Mt    11.342 %         1 x        0.353 Mt        0.353 Mt        0.353 Mt   100.000 % 	 ....../PrepareDrawBundle

		       3.733 Mt     0.168 %         1 x        3.733 Mt        3.733 Mt        0.005 Mt     0.126 % 	 ..../DeferredRLPrep

		       3.728 Mt    99.874 %         1 x        3.728 Mt        3.728 Mt        2.435 Mt    65.315 % 	 ...../PrepareForRendering

		       0.032 Mt     0.866 %         1 x        0.032 Mt        0.032 Mt        0.032 Mt   100.000 % 	 ....../PrepareMaterials

		       0.962 Mt    25.804 %         1 x        0.962 Mt        0.962 Mt        0.962 Mt   100.000 % 	 ....../BuildMaterialCache

		       0.299 Mt     8.015 %         1 x        0.299 Mt        0.299 Mt        0.299 Mt   100.000 % 	 ....../PrepareDrawBundle

		      19.800 Mt     0.889 %         1 x       19.800 Mt       19.800 Mt        5.317 Mt    26.851 % 	 ..../RayTracingRLPrep

		       0.412 Mt     2.080 %         1 x        0.412 Mt        0.412 Mt        0.412 Mt   100.000 % 	 ...../PrepareAccelerationStructures

		       0.037 Mt     0.185 %         1 x        0.037 Mt        0.037 Mt        0.037 Mt   100.000 % 	 ...../PrepareCache

		       1.500 Mt     7.576 %         1 x        1.500 Mt        1.500 Mt        1.500 Mt   100.000 % 	 ...../BuildCache

		       9.676 Mt    48.870 %         1 x        9.676 Mt        9.676 Mt        0.011 Mt     0.118 % 	 ...../BuildAccelerationStructures

		       9.665 Mt    99.882 %         1 x        9.665 Mt        9.665 Mt        6.664 Mt    68.949 % 	 ....../AccelerationStructureBuild

		       2.144 Mt    22.186 %         1 x        2.144 Mt        2.144 Mt        2.144 Mt   100.000 % 	 ......./BuildBottomLevelAS

		       0.857 Mt     8.864 %         1 x        0.857 Mt        0.857 Mt        0.857 Mt   100.000 % 	 ......./BuildTopLevelAS

		       2.859 Mt    14.438 %         1 x        2.859 Mt        2.859 Mt        2.859 Mt   100.000 % 	 ...../BuildShaderTables

		      22.430 Mt     1.007 %         1 x       22.430 Mt       22.430 Mt       22.430 Mt   100.000 % 	 ..../GpuSidePrep

		      50.869 Mt     1.822 %         5 x       10.174 Mt        6.915 Mt       31.029 Mt    60.998 % 	 ../AccelerationStructureBuild

		       7.613 Mt    14.967 %         5 x        1.523 Mt        0.918 Mt        7.613 Mt   100.000 % 	 .../BuildBottomLevelAS

		      12.227 Mt    24.035 %         5 x        2.445 Mt        3.649 Mt       12.227 Mt   100.000 % 	 .../BuildTopLevelAS

	WindowThread:

		    2779.204 Mt   100.000 %         1 x     2779.206 Mt     2779.204 Mt     2779.204 Mt   100.000 % 	 /

	GpuThread:

		      95.134 Mt   100.000 %         4 x       23.783 Mt        2.614 Mt       79.150 Mt    83.199 % 	 /

		       3.843 Mt     4.039 %         1 x        3.843 Mt        3.843 Mt        0.018 Mt     0.475 % 	 ./ScenePrep

		       3.825 Mt    99.525 %         1 x        3.825 Mt        3.825 Mt        0.005 Mt     0.141 % 	 ../AccelerationStructureBuild

		       3.549 Mt    92.794 %         1 x        3.549 Mt        3.549 Mt        3.549 Mt   100.000 % 	 .../BuildBottomLevelASGpu

		       0.270 Mt     7.064 %         1 x        0.270 Mt        0.270 Mt        0.270 Mt   100.000 % 	 .../BuildTopLevelASGpu

		      12.141 Mt    12.762 %         5 x        2.428 Mt        2.614 Mt        0.017 Mt     0.144 % 	 ./AccelerationStructureBuild

		      10.909 Mt    89.858 %         5 x        2.182 Mt        2.370 Mt       10.909 Mt   100.000 % 	 ../BuildBottomLevelASGpu

		       1.214 Mt     9.999 %         5 x        0.243 Mt        0.241 Mt        1.214 Mt   100.000 % 	 ../BuildTopLevelASGpu


	============================


