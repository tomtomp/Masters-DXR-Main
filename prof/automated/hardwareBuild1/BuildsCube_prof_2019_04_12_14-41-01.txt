Configuration: 
Command line parameters  = E:\programming\FIT-DIP\Masters-DXR-Main-git\build\Measurement\x64\Debug\Measurement.exe --scene Cube/Cube.gltf --profile-builds --profile-output BuildsCube 
Window width             = 1024
Window height            = 768
Window title             = Direct3D App
Back-buffer count        = 3
Fill Triangles           = enabled
Triangles Font Clock-Wise= enabled
Fast Build AS            = disabled
Target FPS               = 60
Target UPS               = 60
Tearing                  = disabled
VSync                    = disabled
GUI rendering            = enabled
Debug layer              = enabled
GPU validation           = disabled
GPU profiling            = enabled
Render mode              = Rasterization
Loaded scene file        = Cube/Cube.gltf
Using testing scene      = disabled
Duplication              = 1
Duplication in cube      = disabled
Duplication offset       = 10
BLAS duplication         = disabled
Rays per pixel           = 10

ProfilingAggregator: 

ProfilingBuildAggregator: 
BuildBottomLevelAS	,	1.3349	,	0.7032
BuildBottomLevelASGpu	,	0.11952	,	0.160672
BuildTopLevelAS	,	1.5514	,	0.8516
BuildTopLevelASGpu	,	0.082528	,	0.081344
Duplication	,	1	,	1
Triangles	,	12	,	12
Meshes	,	1	,	1
BottomLevels	,	1	,	1
Index	,	0	,	1

FpsAggregator: 
FPS	,	0	,	0
UPS	,	0	,	0
FrameTime	,	0	,	0
GigaRays/s	,	0	,	0
Index	,	0	,	1


AccelerationStructure: 
Ray tracing acceleration structure building statistics: 
	Bottom Level Acceleration Structure: 
		Maximum [B]: 5168
		Minimum [B]: 5168
		Currently Allocated [B]: 5168
		Currently Created [num]: 1
		Current Average [B]: 5168
		Maximum Triangles [num]: 12
		Minimum Triangles [num]: 12
		Total Triangles [num]: 12
		Maximum Meshes [num]: 1
		Minimum Meshes [num]: 1
		Total Meshes [num]: 1
	Top Level Acceleration Structure: 
		Maximum [B]: 64
		Minimum [B]: 64
		Currently Allocated [B]: 64
		Total Allocated [B]: 64
		Total Created [num]: 1
		Average Allocated [B]: 64
		Maximum Instances [num]: 1
		Minimum Instances [num]: 1
		Total Instances [num]: 1
	Scratch Buffer: 
		Maximum [B]: 3584
		Minimum [B]: 3584
		Currently Allocated [B]: 3584
		Total Allocated [B]: 3584
		Total Created [num]: 1
		Average Allocated [B]: 3584
ProfilingStack: 
[PROF] Profiling statistics: 
	Scopes entered : 55
	Scopes exited : 54
	Overhead per scope [ticks] : 1025
	Threads entered : 3
	Threads exited : 0
	============================
		         Sum[Mt]   Parent[%]  Samples[num]       Avg[Mt]   Last/Curr[Mt]        Self[Mt]     Self[%] 	 Name

	main:

		     624.046 Mt   100.000 %         1 x      624.048 Mt      624.045 Mt        0.789 Mt     0.126 % 	 /

		     623.329 Mt    99.885 %         1 x      623.332 Mt      623.328 Mt      355.946 Mt    57.104 % 	 ./Main application loop

		       3.140 Mt     0.504 %         1 x        3.140 Mt        3.140 Mt        3.140 Mt   100.000 % 	 ../TexturedRLInitialization

		       6.271 Mt     1.006 %         1 x        6.271 Mt        6.271 Mt        6.271 Mt   100.000 % 	 ../DeferredRLInitialization

		      67.865 Mt    10.887 %         1 x       67.865 Mt       67.865 Mt        2.149 Mt     3.166 % 	 ../RayTracingRLInitialization

		      65.716 Mt    96.834 %         1 x       65.716 Mt       65.716 Mt       65.716 Mt   100.000 % 	 .../PipelineBuild

		       3.327 Mt     0.534 %         1 x        3.327 Mt        3.327 Mt        3.327 Mt   100.000 % 	 ../ResolveRLInitialization

		     147.653 Mt    23.688 %         1 x      147.653 Mt      147.653 Mt        3.641 Mt     2.466 % 	 ../Initialization

		     144.012 Mt    97.534 %         1 x      144.012 Mt      144.012 Mt        2.063 Mt     1.432 % 	 .../ScenePrep

		     104.697 Mt    72.700 %         1 x      104.697 Mt      104.697 Mt       16.047 Mt    15.327 % 	 ..../SceneLoad

		      58.706 Mt    56.073 %         1 x       58.706 Mt       58.706 Mt       13.642 Mt    23.238 % 	 ...../glTF-LoadScene

		      45.064 Mt    76.762 %         2 x       22.532 Mt        2.371 Mt       45.064 Mt   100.000 % 	 ....../glTF-LoadImageData

		      13.236 Mt    12.642 %         1 x       13.236 Mt       13.236 Mt       13.236 Mt   100.000 % 	 ...../glTF-CreateScene

		      16.708 Mt    15.958 %         1 x       16.708 Mt       16.708 Mt       16.708 Mt   100.000 % 	 ...../SceneDesc-CreateScene

		       2.576 Mt     1.789 %         1 x        2.576 Mt        2.576 Mt        0.005 Mt     0.194 % 	 ..../TexturedRLPrep

		       2.571 Mt    99.806 %         1 x        2.571 Mt        2.571 Mt        1.170 Mt    45.521 % 	 ...../PrepareForRendering

		       1.089 Mt    42.346 %         1 x        1.089 Mt        1.089 Mt        1.089 Mt   100.000 % 	 ....../PrepareMaterials

		       0.312 Mt    12.133 %         1 x        0.312 Mt        0.312 Mt        0.312 Mt   100.000 % 	 ....../PrepareDrawBundle

		       3.344 Mt     2.322 %         1 x        3.344 Mt        3.344 Mt        0.005 Mt     0.153 % 	 ..../DeferredRLPrep

		       3.338 Mt    99.847 %         1 x        3.338 Mt        3.338 Mt        2.148 Mt    64.345 % 	 ...../PrepareForRendering

		       0.017 Mt     0.500 %         1 x        0.017 Mt        0.017 Mt        0.017 Mt   100.000 % 	 ....../PrepareMaterials

		       0.898 Mt    26.896 %         1 x        0.898 Mt        0.898 Mt        0.898 Mt   100.000 % 	 ....../BuildMaterialCache

		       0.276 Mt     8.258 %         1 x        0.276 Mt        0.276 Mt        0.276 Mt   100.000 % 	 ....../PrepareDrawBundle

		      19.924 Mt    13.835 %         1 x       19.924 Mt       19.924 Mt        4.865 Mt    24.416 % 	 ..../RayTracingRLPrep

		       0.253 Mt     1.268 %         1 x        0.253 Mt        0.253 Mt        0.253 Mt   100.000 % 	 ...../PrepareAccelerationStructures

		       0.024 Mt     0.120 %         1 x        0.024 Mt        0.024 Mt        0.024 Mt   100.000 % 	 ...../PrepareCache

		       0.985 Mt     4.943 %         1 x        0.985 Mt        0.985 Mt        0.985 Mt   100.000 % 	 ...../BuildCache

		      11.151 Mt    55.965 %         1 x       11.151 Mt       11.151 Mt        0.004 Mt     0.038 % 	 ...../BuildAccelerationStructures

		      11.146 Mt    99.962 %         1 x       11.146 Mt       11.146 Mt        9.034 Mt    81.051 % 	 ....../AccelerationStructureBuild

		       1.113 Mt     9.988 %         1 x        1.113 Mt        1.113 Mt        1.113 Mt   100.000 % 	 ......./BuildBottomLevelAS

		       0.999 Mt     8.961 %         1 x        0.999 Mt        0.999 Mt        0.999 Mt   100.000 % 	 ......./BuildTopLevelAS

		       2.648 Mt    13.288 %         1 x        2.648 Mt        2.648 Mt        2.648 Mt   100.000 % 	 ...../BuildShaderTables

		      11.410 Mt     7.923 %         1 x       11.410 Mt       11.410 Mt       11.410 Mt   100.000 % 	 ..../GpuSidePrep

		      39.127 Mt     6.277 %         3 x       13.042 Mt        4.198 Mt       29.586 Mt    75.615 % 	 ../AccelerationStructureBuild

		       4.036 Mt    10.314 %         3 x        1.345 Mt        0.703 Mt        4.036 Mt   100.000 % 	 .../BuildBottomLevelAS

		       5.506 Mt    14.071 %         3 x        1.835 Mt        0.852 Mt        5.506 Mt   100.000 % 	 .../BuildTopLevelAS

	WindowThread:

		     620.638 Mt   100.000 %         1 x      620.638 Mt      620.637 Mt      620.638 Mt   100.000 % 	 /

	GpuThread:

		      68.808 Mt   100.000 %         2 x       34.404 Mt        0.243 Mt       67.946 Mt    98.747 % 	 /

		       0.212 Mt     0.308 %         1 x        0.212 Mt        0.212 Mt        0.006 Mt     2.986 % 	 ./ScenePrep

		       0.206 Mt    97.014 %         1 x        0.206 Mt        0.206 Mt        0.001 Mt     0.606 % 	 ../AccelerationStructureBuild

		       0.121 Mt    58.853 %         1 x        0.121 Mt        0.121 Mt        0.121 Mt   100.000 % 	 .../BuildBottomLevelASGpu

		       0.083 Mt    40.541 %         1 x        0.083 Mt        0.083 Mt        0.083 Mt   100.000 % 	 .../BuildTopLevelASGpu

		       0.650 Mt     0.944 %         3 x        0.217 Mt        0.243 Mt        0.004 Mt     0.571 % 	 ./AccelerationStructureBuild

		       0.400 Mt    61.511 %         3 x        0.133 Mt        0.161 Mt        0.400 Mt   100.000 % 	 ../BuildBottomLevelASGpu

		       0.246 Mt    37.918 %         3 x        0.082 Mt        0.081 Mt        0.246 Mt   100.000 % 	 ../BuildTopLevelASGpu


	============================


