Configuration: 
Command line parameters  = E:\programming\FIT-DIP\Masters-DXR-Main-git\build\Measurement\x64\Debug\Measurement.exe --scene Suzanne/Suzanne.gltf --profile-builds --profile-output BuildsSuzanne 
Window width             = 1024
Window height            = 768
Window title             = Direct3D App
Back-buffer count        = 3
Fill Triangles           = enabled
Triangles Font Clock-Wise= enabled
Fast Build AS            = disabled
Target FPS               = 60
Target UPS               = 60
Tearing                  = disabled
VSync                    = disabled
GUI rendering            = enabled
Debug layer              = enabled
GPU validation           = disabled
GPU profiling            = enabled
Render mode              = Rasterization
Loaded scene file        = Suzanne/Suzanne.gltf
Using testing scene      = disabled
Duplication              = 1
Duplication in cube      = disabled
Duplication offset       = 10
BLAS duplication         = disabled
Rays per pixel           = 10

ProfilingAggregator: 

ProfilingBuildAggregator: 
BuildBottomLevelAS	,	1.3503	,	0.6967
BuildBottomLevelASGpu	,	0.352384	,	0.598368
BuildTopLevelAS	,	1.9948	,	0.7109
BuildTopLevelASGpu	,	0.083872	,	0.082272
Duplication	,	1	,	1
Triangles	,	3936	,	3936
Meshes	,	1	,	1
BottomLevels	,	1	,	1
Index	,	0	,	1

FpsAggregator: 
FPS	,	0	,	0
UPS	,	0	,	0
FrameTime	,	0	,	0
GigaRays/s	,	0	,	0
Index	,	0	,	1


AccelerationStructure: 
Ray tracing acceleration structure building statistics: 
	Bottom Level Acceleration Structure: 
		Maximum [B]: 224128
		Minimum [B]: 224128
		Currently Allocated [B]: 224128
		Currently Created [num]: 1
		Current Average [B]: 224128
		Maximum Triangles [num]: 3936
		Minimum Triangles [num]: 3936
		Total Triangles [num]: 3936
		Maximum Meshes [num]: 1
		Minimum Meshes [num]: 1
		Total Meshes [num]: 1
	Top Level Acceleration Structure: 
		Maximum [B]: 64
		Minimum [B]: 64
		Currently Allocated [B]: 64
		Total Allocated [B]: 64
		Total Created [num]: 1
		Average Allocated [B]: 64
		Maximum Instances [num]: 1
		Minimum Instances [num]: 1
		Total Instances [num]: 1
	Scratch Buffer: 
		Maximum [B]: 34304
		Minimum [B]: 34304
		Currently Allocated [B]: 34304
		Total Allocated [B]: 34304
		Total Created [num]: 1
		Average Allocated [B]: 34304
ProfilingStack: 
[PROF] Profiling statistics: 
	Scopes entered : 55
	Scopes exited : 54
	Overhead per scope [ticks] : 1027.1
	Threads entered : 3
	Threads exited : 0
	============================
		         Sum[Mt]   Parent[%]  Samples[num]       Avg[Mt]   Last/Curr[Mt]        Self[Mt]     Self[%] 	 Name

	main:

		     787.139 Mt   100.000 %         1 x      787.141 Mt      787.138 Mt        0.738 Mt     0.094 % 	 /

		     786.457 Mt    99.913 %         1 x      786.459 Mt      786.457 Mt      356.110 Mt    45.280 % 	 ./Main application loop

		       2.873 Mt     0.365 %         1 x        2.873 Mt        2.873 Mt        2.873 Mt   100.000 % 	 ../TexturedRLInitialization

		       8.724 Mt     1.109 %         1 x        8.724 Mt        8.724 Mt        8.724 Mt   100.000 % 	 ../DeferredRLInitialization

		      73.795 Mt     9.383 %         1 x       73.795 Mt       73.795 Mt        2.852 Mt     3.865 % 	 ../RayTracingRLInitialization

		      70.942 Mt    96.135 %         1 x       70.942 Mt       70.942 Mt       70.942 Mt   100.000 % 	 .../PipelineBuild

		       3.466 Mt     0.441 %         1 x        3.466 Mt        3.466 Mt        3.466 Mt   100.000 % 	 ../ResolveRLInitialization

		     298.595 Mt    37.967 %         1 x      298.595 Mt      298.595 Mt        8.309 Mt     2.783 % 	 ../Initialization

		     290.286 Mt    97.217 %         1 x      290.286 Mt      290.286 Mt        2.856 Mt     0.984 % 	 .../ScenePrep

		     241.866 Mt    83.320 %         1 x      241.866 Mt      241.866 Mt       24.023 Mt     9.932 % 	 ..../SceneLoad

		     185.844 Mt    76.838 %         1 x      185.844 Mt      185.844 Mt       37.840 Mt    20.361 % 	 ...../glTF-LoadScene

		     148.004 Mt    79.639 %         2 x       74.002 Mt       68.234 Mt      148.004 Mt   100.000 % 	 ....../glTF-LoadImageData

		      15.883 Mt     6.567 %         1 x       15.883 Mt       15.883 Mt       15.883 Mt   100.000 % 	 ...../glTF-CreateScene

		      16.115 Mt     6.663 %         1 x       16.115 Mt       16.115 Mt       16.115 Mt   100.000 % 	 ...../SceneDesc-CreateScene

		       2.488 Mt     0.857 %         1 x        2.488 Mt        2.488 Mt        0.005 Mt     0.201 % 	 ..../TexturedRLPrep

		       2.483 Mt    99.799 %         1 x        2.483 Mt        2.483 Mt        0.908 Mt    36.567 % 	 ...../PrepareForRendering

		       1.277 Mt    51.438 %         1 x        1.277 Mt        1.277 Mt        1.277 Mt   100.000 % 	 ....../PrepareMaterials

		       0.298 Mt    11.996 %         1 x        0.298 Mt        0.298 Mt        0.298 Mt   100.000 % 	 ....../PrepareDrawBundle

		       5.484 Mt     1.889 %         1 x        5.484 Mt        5.484 Mt        0.006 Mt     0.104 % 	 ..../DeferredRLPrep

		       5.479 Mt    99.896 %         1 x        5.479 Mt        5.479 Mt        3.428 Mt    62.570 % 	 ...../PrepareForRendering

		       0.017 Mt     0.308 %         1 x        0.017 Mt        0.017 Mt        0.017 Mt   100.000 % 	 ....../PrepareMaterials

		       1.167 Mt    21.304 %         1 x        1.167 Mt        1.167 Mt        1.167 Mt   100.000 % 	 ....../BuildMaterialCache

		       0.867 Mt    15.818 %         1 x        0.867 Mt        0.867 Mt        0.867 Mt   100.000 % 	 ....../PrepareDrawBundle

		      20.501 Mt     7.062 %         1 x       20.501 Mt       20.501 Mt        6.529 Mt    31.846 % 	 ..../RayTracingRLPrep

		       0.745 Mt     3.634 %         1 x        0.745 Mt        0.745 Mt        0.745 Mt   100.000 % 	 ...../PrepareAccelerationStructures

		       0.075 Mt     0.368 %         1 x        0.075 Mt        0.075 Mt        0.075 Mt   100.000 % 	 ...../PrepareCache

		       1.751 Mt     8.541 %         1 x        1.751 Mt        1.751 Mt        1.751 Mt   100.000 % 	 ...../BuildCache

		       8.473 Mt    41.330 %         1 x        8.473 Mt        8.473 Mt        0.005 Mt     0.063 % 	 ...../BuildAccelerationStructures

		       8.468 Mt    99.937 %         1 x        8.468 Mt        8.468 Mt        5.915 Mt    69.852 % 	 ....../AccelerationStructureBuild

		       1.377 Mt    16.266 %         1 x        1.377 Mt        1.377 Mt        1.377 Mt   100.000 % 	 ......./BuildBottomLevelAS

		       1.175 Mt    13.882 %         1 x        1.175 Mt        1.175 Mt        1.175 Mt   100.000 % 	 ......./BuildTopLevelAS

		       2.928 Mt    14.281 %         1 x        2.928 Mt        2.928 Mt        2.928 Mt   100.000 % 	 ...../BuildShaderTables

		      17.091 Mt     5.888 %         1 x       17.091 Mt       17.091 Mt       17.091 Mt   100.000 % 	 ..../GpuSidePrep

		      42.894 Mt     5.454 %         3 x       14.298 Mt        6.340 Mt       31.490 Mt    73.413 % 	 ../AccelerationStructureBuild

		       6.836 Mt    15.938 %         3 x        2.279 Mt        0.697 Mt        6.836 Mt   100.000 % 	 .../BuildBottomLevelAS

		       4.568 Mt    10.649 %         3 x        1.523 Mt        0.711 Mt        4.568 Mt   100.000 % 	 .../BuildTopLevelAS

	WindowThread:

		     774.670 Mt   100.000 %         1 x      774.670 Mt      774.670 Mt      774.670 Mt   100.000 % 	 /

	GpuThread:

		      80.312 Mt   100.000 %         2 x       40.156 Mt        0.682 Mt       78.307 Mt    97.502 % 	 /

		       0.448 Mt     0.558 %         1 x        0.448 Mt        0.448 Mt        0.007 Mt     1.527 % 	 ./ScenePrep

		       0.441 Mt    98.473 %         1 x        0.441 Mt        0.441 Mt        0.001 Mt     0.290 % 	 ../AccelerationStructureBuild

		       0.357 Mt    80.879 %         1 x        0.357 Mt        0.357 Mt        0.357 Mt   100.000 % 	 .../BuildBottomLevelASGpu

		       0.083 Mt    18.832 %         1 x        0.083 Mt        0.083 Mt        0.083 Mt   100.000 % 	 .../BuildTopLevelASGpu

		       1.558 Mt     1.939 %         3 x        0.519 Mt        0.682 Mt        0.004 Mt     0.281 % 	 ./AccelerationStructureBuild

		       1.303 Mt    83.667 %         3 x        0.434 Mt        0.598 Mt        1.303 Mt   100.000 % 	 ../BuildBottomLevelASGpu

		       0.250 Mt    16.052 %         3 x        0.083 Mt        0.082 Mt        0.250 Mt   100.000 % 	 ../BuildTopLevelASGpu


	============================


