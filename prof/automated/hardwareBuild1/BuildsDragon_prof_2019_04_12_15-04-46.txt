Configuration: 
Command line parameters  = E:\programming\FIT-DIP\Masters-DXR-Main-git\build\Measurement\x64\Debug\Measurement.exe --scene Dragon/Dragon.glb --profile-builds --profile-output BuildsDragon 
Window width             = 1024
Window height            = 768
Window title             = Direct3D App
Back-buffer count        = 3
Fill Triangles           = enabled
Triangles Font Clock-Wise= enabled
Fast Build AS            = disabled
Target FPS               = 60
Target UPS               = 60
Tearing                  = disabled
VSync                    = disabled
GUI rendering            = enabled
Debug layer              = enabled
GPU validation           = disabled
GPU profiling            = enabled
Render mode              = Rasterization
Loaded scene file        = Dragon/Dragon.glb
Using testing scene      = disabled
Duplication              = 1
Duplication in cube      = disabled
Duplication offset       = 10
BLAS duplication         = disabled
Rays per pixel           = 10

ProfilingAggregator: 

ProfilingBuildAggregator: 
BuildBottomLevelAS	,	1.2851	,	1.2489	,	1.4298	,	1.1142
BuildBottomLevelASGpu	,	0.56864	,	0.52352	,	0.892128	,	0.806528
BuildTopLevelAS	,	0.7917	,	0.9593	,	1.0054	,	0.9552
BuildTopLevelASGpu	,	0.083232	,	0.083232	,	0.08384	,	0.079264
Duplication	,	1	,	1	,	1	,	1
Triangles	,	34852	,	11757	,	34852	,	11757
Meshes	,	1	,	1	,	1	,	1
BottomLevels	,	1	,	1	,	1	,	1
Index	,	0	,	1	,	2	,	3

FpsAggregator: 
FPS	,	0	,	0	,	0	,	0
UPS	,	0	,	0	,	0	,	0
FrameTime	,	0	,	0	,	0	,	0
GigaRays/s	,	0	,	0	,	0	,	0
Index	,	0	,	1	,	2	,	3


AccelerationStructure: 
Ray tracing acceleration structure building statistics: 
	Bottom Level Acceleration Structure: 
		Maximum [B]: 1947136
		Minimum [B]: 659968
		Currently Allocated [B]: 2607104
		Currently Created [num]: 2
		Current Average [B]: 1303552
		Maximum Triangles [num]: 34852
		Minimum Triangles [num]: 11757
		Total Triangles [num]: 46609
		Maximum Meshes [num]: 1
		Minimum Meshes [num]: 1
		Total Meshes [num]: 2
	Top Level Acceleration Structure: 
		Maximum [B]: 128
		Minimum [B]: 128
		Currently Allocated [B]: 128
		Total Allocated [B]: 128
		Total Created [num]: 1
		Average Allocated [B]: 128
		Maximum Instances [num]: 2
		Minimum Instances [num]: 2
		Total Instances [num]: 2
	Scratch Buffer: 
		Maximum [B]: 290304
		Minimum [B]: 290304
		Currently Allocated [B]: 290304
		Total Allocated [B]: 290304
		Total Created [num]: 1
		Average Allocated [B]: 290304
ProfilingStack: 
[PROF] Profiling statistics: 
	Scopes entered : 73
	Scopes exited : 72
	Overhead per scope [ticks] : 1003
	Threads entered : 3
	Threads exited : 0
	============================
		         Sum[Mt]   Parent[%]  Samples[num]       Avg[Mt]   Last/Curr[Mt]        Self[Mt]     Self[%] 	 Name

	main:

		    2692.108 Mt   100.000 %         1 x     2692.111 Mt     2692.107 Mt        1.149 Mt     0.043 % 	 /

		    2691.082 Mt    99.962 %         1 x     2691.086 Mt     2691.081 Mt      369.903 Mt    13.746 % 	 ./Main application loop

		       2.860 Mt     0.106 %         1 x        2.860 Mt        2.860 Mt        2.860 Mt   100.000 % 	 ../TexturedRLInitialization

		       5.691 Mt     0.211 %         1 x        5.691 Mt        5.691 Mt        5.691 Mt   100.000 % 	 ../DeferredRLInitialization

		      54.863 Mt     2.039 %         1 x       54.863 Mt       54.863 Mt        2.649 Mt     4.828 % 	 ../RayTracingRLInitialization

		      52.214 Mt    95.172 %         1 x       52.214 Mt       52.214 Mt       52.214 Mt   100.000 % 	 .../PipelineBuild

		       3.448 Mt     0.128 %         1 x        3.448 Mt        3.448 Mt        3.448 Mt   100.000 % 	 ../ResolveRLInitialization

		    2207.508 Mt    82.031 %         1 x     2207.508 Mt     2207.508 Mt        2.777 Mt     0.126 % 	 ../Initialization

		    2204.731 Mt    99.874 %         1 x     2204.731 Mt     2204.731 Mt        2.538 Mt     0.115 % 	 .../ScenePrep

		    2164.133 Mt    98.159 %         1 x     2164.133 Mt     2164.133 Mt       84.149 Mt     3.888 % 	 ..../SceneLoad

		    1957.066 Mt    90.432 %         1 x     1957.066 Mt     1957.066 Mt      153.683 Mt     7.853 % 	 ...../glTF-LoadScene

		    1803.383 Mt    92.147 %         8 x      225.423 Mt      364.456 Mt     1803.383 Mt   100.000 % 	 ....../glTF-LoadImageData

		      95.611 Mt     4.418 %         1 x       95.611 Mt       95.611 Mt       95.611 Mt   100.000 % 	 ...../glTF-CreateScene

		      27.307 Mt     1.262 %         1 x       27.307 Mt       27.307 Mt       27.307 Mt   100.000 % 	 ...../SceneDesc-CreateScene

		       2.448 Mt     0.111 %         1 x        2.448 Mt        2.448 Mt        0.005 Mt     0.212 % 	 ..../TexturedRLPrep

		       2.443 Mt    99.788 %         1 x        2.443 Mt        2.443 Mt        0.873 Mt    35.755 % 	 ...../PrepareForRendering

		       1.240 Mt    50.737 %         1 x        1.240 Mt        1.240 Mt        1.240 Mt   100.000 % 	 ....../PrepareMaterials

		       0.330 Mt    13.508 %         1 x        0.330 Mt        0.330 Mt        0.330 Mt   100.000 % 	 ....../PrepareDrawBundle

		       3.372 Mt     0.153 %         1 x        3.372 Mt        3.372 Mt        0.004 Mt     0.130 % 	 ..../DeferredRLPrep

		       3.368 Mt    99.870 %         1 x        3.368 Mt        3.368 Mt        2.246 Mt    66.699 % 	 ...../PrepareForRendering

		       0.028 Mt     0.840 %         1 x        0.028 Mt        0.028 Mt        0.028 Mt   100.000 % 	 ....../PrepareMaterials

		       0.817 Mt    24.244 %         1 x        0.817 Mt        0.817 Mt        0.817 Mt   100.000 % 	 ....../BuildMaterialCache

		       0.277 Mt     8.216 %         1 x        0.277 Mt        0.277 Mt        0.277 Mt   100.000 % 	 ....../PrepareDrawBundle

		      17.603 Mt     0.798 %         1 x       17.603 Mt       17.603 Mt        4.930 Mt    28.008 % 	 ..../RayTracingRLPrep

		       0.406 Mt     2.305 %         1 x        0.406 Mt        0.406 Mt        0.406 Mt   100.000 % 	 ...../PrepareAccelerationStructures

		       0.036 Mt     0.206 %         1 x        0.036 Mt        0.036 Mt        0.036 Mt   100.000 % 	 ...../PrepareCache

		       0.916 Mt     5.202 %         1 x        0.916 Mt        0.916 Mt        0.916 Mt   100.000 % 	 ...../BuildCache

		       8.870 Mt    50.390 %         1 x        8.870 Mt        8.870 Mt        0.004 Mt     0.047 % 	 ...../BuildAccelerationStructures

		       8.866 Mt    99.953 %         1 x        8.866 Mt        8.866 Mt        6.326 Mt    71.348 % 	 ....../AccelerationStructureBuild

		       1.639 Mt    18.487 %         1 x        1.639 Mt        1.639 Mt        1.639 Mt   100.000 % 	 ......./BuildBottomLevelAS

		       0.901 Mt    10.165 %         1 x        0.901 Mt        0.901 Mt        0.901 Mt   100.000 % 	 ......./BuildTopLevelAS

		       2.445 Mt    13.889 %         1 x        2.445 Mt        2.445 Mt        2.445 Mt   100.000 % 	 ...../BuildShaderTables

		      14.637 Mt     0.664 %         1 x       14.637 Mt       14.637 Mt       14.637 Mt   100.000 % 	 ..../GpuSidePrep

		      46.807 Mt     1.739 %         5 x        9.361 Mt        9.382 Mt       35.301 Mt    75.417 % 	 ../AccelerationStructureBuild

		       6.771 Mt    14.465 %         5 x        1.354 Mt        1.114 Mt        6.771 Mt   100.000 % 	 .../BuildBottomLevelAS

		       4.736 Mt    10.118 %         5 x        0.947 Mt        0.955 Mt        4.736 Mt   100.000 % 	 .../BuildTopLevelAS

	WindowThread:

		    2677.757 Mt   100.000 %         1 x     2677.758 Mt     2677.756 Mt     2677.757 Mt   100.000 % 	 /

	GpuThread:

		      56.013 Mt   100.000 %         4 x       14.003 Mt        0.887 Mt       51.050 Mt    91.140 % 	 /

		       1.184 Mt     2.113 %         1 x        1.184 Mt        1.184 Mt        0.006 Mt     0.549 % 	 ./ScenePrep

		       1.177 Mt    99.451 %         1 x        1.177 Mt        1.177 Mt        0.001 Mt     0.079 % 	 ../AccelerationStructureBuild

		       1.090 Mt    92.582 %         1 x        1.090 Mt        1.090 Mt        1.090 Mt   100.000 % 	 .../BuildBottomLevelASGpu

		       0.086 Mt     7.340 %         1 x        0.086 Mt        0.086 Mt        0.086 Mt   100.000 % 	 .../BuildTopLevelASGpu

		       3.779 Mt     6.747 %         5 x        0.756 Mt        0.887 Mt        0.007 Mt     0.185 % 	 ./AccelerationStructureBuild

		       3.359 Mt    88.893 %         5 x        0.672 Mt        0.807 Mt        3.359 Mt   100.000 % 	 ../BuildBottomLevelASGpu

		       0.413 Mt    10.923 %         5 x        0.083 Mt        0.079 Mt        0.413 Mt   100.000 % 	 ../BuildTopLevelASGpu


	============================


