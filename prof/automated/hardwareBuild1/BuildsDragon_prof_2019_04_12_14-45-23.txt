Configuration: 
Command line parameters  = Measurement.exe --scene Dragon/Dragon.glb --profile-builds --profile-output BuildsDragon 
Window width             = 1024
Window height            = 768
Window title             = Direct3D App
Back-buffer count        = 3
Fill Triangles           = enabled
Triangles Font Clock-Wise= enabled
Fast Build AS            = disabled
Target FPS               = 60
Target UPS               = 60
Tearing                  = disabled
VSync                    = disabled
GUI rendering            = enabled
Debug layer              = enabled
GPU validation           = disabled
GPU profiling            = enabled
Render mode              = Rasterization
Loaded scene file        = Dragon/Dragon.glb
Using testing scene      = disabled
Duplication              = 1
Duplication in cube      = disabled
Duplication offset       = 10
BLAS duplication         = disabled
Rays per pixel           = 10

ProfilingAggregator: 

ProfilingBuildAggregator: 
BuildBottomLevelAS	,	1.2021	,	0.7871	,	1.9739	,	1.395
BuildBottomLevelASGpu	,	3.33069	,	1.85142	,	3.70141	,	2.85034
BuildTopLevelAS	,	1.4424	,	1.1705	,	1.8121	,	1.5918
BuildTopLevelASGpu	,	0.741184	,	0.351264	,	0.760096	,	2.75718
Duplication	,	1	,	1	,	1	,	1
Triangles	,	34852	,	11757	,	34852	,	11757
Meshes	,	1	,	1	,	1	,	1
BottomLevels	,	1	,	1	,	1	,	1
Index	,	0	,	1	,	2	,	3

FpsAggregator: 
FPS	,	0	,	0	,	0	,	0
UPS	,	0	,	0	,	0	,	0
FrameTime	,	0	,	0	,	0	,	0
GigaRays/s	,	0	,	0	,	0	,	0
Index	,	0	,	1	,	2	,	3


AccelerationStructure: 
Ray tracing acceleration structure building statistics: 
	Bottom Level Acceleration Structure: 
		Maximum [B]: 1947136
		Minimum [B]: 659968
		Currently Allocated [B]: 2607104
		Currently Created [num]: 2
		Current Average [B]: 1303552
		Maximum Triangles [num]: 34852
		Minimum Triangles [num]: 11757
		Total Triangles [num]: 46609
		Maximum Meshes [num]: 1
		Minimum Meshes [num]: 1
		Total Meshes [num]: 2
	Top Level Acceleration Structure: 
		Maximum [B]: 128
		Minimum [B]: 128
		Currently Allocated [B]: 128
		Total Allocated [B]: 128
		Total Created [num]: 1
		Average Allocated [B]: 128
		Maximum Instances [num]: 2
		Minimum Instances [num]: 2
		Total Instances [num]: 2
	Scratch Buffer: 
		Maximum [B]: 290304
		Minimum [B]: 290304
		Currently Allocated [B]: 290304
		Total Allocated [B]: 290304
		Total Created [num]: 1
		Average Allocated [B]: 290304
ProfilingStack: 
[PROF] Profiling statistics: 
	Scopes entered : 73
	Scopes exited : 72
	Overhead per scope [ticks] : 1004.7
	Threads entered : 3
	Threads exited : 0
	============================
		         Sum[Mt]   Parent[%]  Samples[num]       Avg[Mt]   Last/Curr[Mt]        Self[Mt]     Self[%] 	 Name

	main:

		    2829.418 Mt   100.000 %         1 x     2829.423 Mt     2829.417 Mt        1.169 Mt     0.041 % 	 /

		    2828.432 Mt    99.965 %         1 x     2828.437 Mt     2828.431 Mt      430.255 Mt    15.212 % 	 ./Main application loop

		       4.133 Mt     0.146 %         1 x        4.133 Mt        4.133 Mt        4.133 Mt   100.000 % 	 ../TexturedRLInitialization

		       7.870 Mt     0.278 %         1 x        7.870 Mt        7.870 Mt        7.870 Mt   100.000 % 	 ../DeferredRLInitialization

		      59.278 Mt     2.096 %         1 x       59.278 Mt       59.278 Mt        2.148 Mt     3.624 % 	 ../RayTracingRLInitialization

		      57.129 Mt    96.376 %         1 x       57.129 Mt       57.129 Mt       57.129 Mt   100.000 % 	 .../PipelineBuild

		       3.394 Mt     0.120 %         1 x        3.394 Mt        3.394 Mt        3.394 Mt   100.000 % 	 ../ResolveRLInitialization

		    2280.484 Mt    80.627 %         1 x     2280.484 Mt     2280.484 Mt        8.825 Mt     0.387 % 	 ../Initialization

		    2271.659 Mt    99.613 %         1 x     2271.659 Mt     2271.659 Mt        2.767 Mt     0.122 % 	 .../ScenePrep

		    2223.166 Mt    97.865 %         1 x     2223.166 Mt     2223.166 Mt      106.388 Mt     4.785 % 	 ..../SceneLoad

		    2002.644 Mt    90.081 %         1 x     2002.644 Mt     2002.644 Mt      152.782 Mt     7.629 % 	 ...../glTF-LoadScene

		    1849.862 Mt    92.371 %         8 x      231.233 Mt      372.161 Mt     1849.862 Mt   100.000 % 	 ....../glTF-LoadImageData

		     106.325 Mt     4.783 %         1 x      106.325 Mt      106.325 Mt      106.325 Mt   100.000 % 	 ...../glTF-CreateScene

		       7.808 Mt     0.351 %         1 x        7.808 Mt        7.808 Mt        7.808 Mt   100.000 % 	 ...../SceneDesc-CreateScene

		       1.119 Mt     0.049 %         1 x        1.119 Mt        1.119 Mt        0.005 Mt     0.438 % 	 ..../TexturedRLPrep

		       1.114 Mt    99.562 %         1 x        1.114 Mt        1.114 Mt        0.332 Mt    29.813 % 	 ...../PrepareForRendering

		       0.449 Mt    40.329 %         1 x        0.449 Mt        0.449 Mt        0.449 Mt   100.000 % 	 ....../PrepareMaterials

		       0.333 Mt    29.858 %         1 x        0.333 Mt        0.333 Mt        0.333 Mt   100.000 % 	 ....../PrepareDrawBundle

		       2.367 Mt     0.104 %         1 x        2.367 Mt        2.367 Mt        0.005 Mt     0.194 % 	 ..../DeferredRLPrep

		       2.362 Mt    99.806 %         1 x        2.362 Mt        2.362 Mt        1.697 Mt    71.864 % 	 ...../PrepareForRendering

		       0.028 Mt     1.194 %         1 x        0.028 Mt        0.028 Mt        0.028 Mt   100.000 % 	 ....../PrepareMaterials

		       0.361 Mt    15.304 %         1 x        0.361 Mt        0.361 Mt        0.361 Mt   100.000 % 	 ....../BuildMaterialCache

		       0.275 Mt    11.638 %         1 x        0.275 Mt        0.275 Mt        0.275 Mt   100.000 % 	 ....../PrepareDrawBundle

		      17.909 Mt     0.788 %         1 x       17.909 Mt       17.909 Mt        6.401 Mt    35.743 % 	 ..../RayTracingRLPrep

		       0.407 Mt     2.272 %         1 x        0.407 Mt        0.407 Mt        0.407 Mt   100.000 % 	 ...../PrepareAccelerationStructures

		       0.036 Mt     0.200 %         1 x        0.036 Mt        0.036 Mt        0.036 Mt   100.000 % 	 ...../PrepareCache

		       0.423 Mt     2.362 %         1 x        0.423 Mt        0.423 Mt        0.423 Mt   100.000 % 	 ...../BuildCache

		       8.726 Mt    48.726 %         1 x        8.726 Mt        8.726 Mt        0.004 Mt     0.048 % 	 ...../BuildAccelerationStructures

		       8.722 Mt    99.952 %         1 x        8.722 Mt        8.722 Mt        5.827 Mt    66.811 % 	 ....../AccelerationStructureBuild

		       1.629 Mt    18.681 %         1 x        1.629 Mt        1.629 Mt        1.629 Mt   100.000 % 	 ......./BuildBottomLevelAS

		       1.265 Mt    14.508 %         1 x        1.265 Mt        1.265 Mt        1.265 Mt   100.000 % 	 ......./BuildTopLevelAS

		       1.916 Mt    10.696 %         1 x        1.916 Mt        1.916 Mt        1.916 Mt   100.000 % 	 ...../BuildShaderTables

		      24.332 Mt     1.071 %         1 x       24.332 Mt       24.332 Mt       24.332 Mt   100.000 % 	 ..../GpuSidePrep

		      43.019 Mt     1.521 %         5 x        8.604 Mt        8.227 Mt       28.294 Mt    65.771 % 	 ../AccelerationStructureBuild

		       7.253 Mt    16.860 %         5 x        1.451 Mt        1.395 Mt        7.253 Mt   100.000 % 	 .../BuildBottomLevelAS

		       7.472 Mt    17.369 %         5 x        1.494 Mt        1.592 Mt        7.472 Mt   100.000 % 	 .../BuildTopLevelAS

	WindowThread:

		    2821.111 Mt   100.000 %         1 x     2821.112 Mt     2821.110 Mt     2821.111 Mt   100.000 % 	 /

	GpuThread:

		      91.692 Mt   100.000 %         4 x       22.923 Mt        5.612 Mt       67.102 Mt    73.182 % 	 /

		       4.156 Mt     4.532 %         1 x        4.156 Mt        4.156 Mt        0.032 Mt     0.761 % 	 ./ScenePrep

		       4.124 Mt    99.239 %         1 x        4.124 Mt        4.124 Mt        0.003 Mt     0.081 % 	 ../AccelerationStructureBuild

		       3.730 Mt    90.436 %         1 x        3.730 Mt        3.730 Mt        3.730 Mt   100.000 % 	 .../BuildBottomLevelASGpu

		       0.391 Mt     9.482 %         1 x        0.391 Mt        0.391 Mt        0.391 Mt   100.000 % 	 .../BuildTopLevelASGpu

		      20.435 Mt    22.286 %         5 x        4.087 Mt        5.612 Mt        0.019 Mt     0.094 % 	 ./AccelerationStructureBuild

		      15.065 Mt    73.720 %         5 x        3.013 Mt        2.850 Mt       15.065 Mt   100.000 % 	 ../BuildBottomLevelASGpu

		       5.351 Mt    26.185 %         5 x        1.070 Mt        2.757 Mt        5.351 Mt   100.000 % 	 ../BuildTopLevelASGpu


	============================


