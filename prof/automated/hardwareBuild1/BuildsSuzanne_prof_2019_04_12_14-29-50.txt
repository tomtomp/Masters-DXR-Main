Configuration: 
Command line parameters  = E:\programming\FIT-DIP\Masters-DXR-Main-git\build\Measurement\x64\Debug\Measurement.exe --scene Suzanne/Suzanne.gltf --profile-builds --profile-output BuildsSuzanne 
Window width             = 1024
Window height            = 768
Window title             = Direct3D App
Back-buffer count        = 3
Fill Triangles           = enabled
Triangles Font Clock-Wise= enabled
Fast Build AS            = disabled
Target FPS               = 60
Target UPS               = 60
Tearing                  = disabled
VSync                    = disabled
GUI rendering            = enabled
Debug layer              = enabled
GPU validation           = disabled
GPU profiling            = enabled
Render mode              = Rasterization
Loaded scene file        = Suzanne/Suzanne.gltf
Using testing scene      = disabled
Duplication              = 1
Duplication in cube      = disabled
Duplication offset       = 10
BLAS duplication         = disabled
Rays per pixel           = 10

ProfilingAggregator: 

ProfilingBuildAggregator: 
BuildBottomLevelAS	,	2.4214	,	0.7192
BuildBottomLevelASGpu	,	0.34768	,	0.609792
BuildTopLevelAS	,	1.1092	,	0.9059
BuildTopLevelASGpu	,	0.082816	,	0.081376
Duplication	,	1	,	1
Triangles	,	3936	,	3936
Meshes	,	1	,	1
BottomLevels	,	1	,	1
Index	,	0	,	1

FpsAggregator: 
FPS	,	0	,	0
UPS	,	0	,	0
FrameTime	,	0	,	0
GigaRays/s	,	0	,	0
Index	,	0	,	1


AccelerationStructure: 
Ray tracing acceleration structure building statistics: 
	Bottom Level Acceleration Structure: 
		Maximum [B]: 224128
		Minimum [B]: 224128
		Currently Allocated [B]: 224128
		Currently Created [num]: 1
		Current Average [B]: 224128
		Maximum Triangles [num]: 3936
		Minimum Triangles [num]: 3936
		Total Triangles [num]: 3936
		Maximum Meshes [num]: 1
		Minimum Meshes [num]: 1
		Total Meshes [num]: 1
	Top Level Acceleration Structure: 
		Maximum [B]: 64
		Minimum [B]: 64
		Currently Allocated [B]: 64
		Total Allocated [B]: 64
		Total Created [num]: 1
		Average Allocated [B]: 64
		Maximum Instances [num]: 1
		Minimum Instances [num]: 1
		Total Instances [num]: 1
	Scratch Buffer: 
		Maximum [B]: 34304
		Minimum [B]: 34304
		Currently Allocated [B]: 34304
		Total Allocated [B]: 34304
		Total Created [num]: 1
		Average Allocated [B]: 34304
ProfilingStack: 
[PROF] Profiling statistics: 
	Scopes entered : 55
	Scopes exited : 54
	Overhead per scope [ticks] : 1015.1
	Threads entered : 3
	Threads exited : 0
	============================
		         Sum[Mt]   Parent[%]  Samples[num]       Avg[Mt]   Last/Curr[Mt]        Self[Mt]     Self[%] 	 Name

	main:

		     810.722 Mt   100.000 %         1 x      810.723 Mt      810.720 Mt        0.989 Mt     0.122 % 	 /

		     809.791 Mt    99.885 %         1 x      809.793 Mt      809.791 Mt      414.331 Mt    51.165 % 	 ./Main application loop

		       3.055 Mt     0.377 %         1 x        3.055 Mt        3.055 Mt        3.055 Mt   100.000 % 	 ../TexturedRLInitialization

		       8.172 Mt     1.009 %         1 x        8.172 Mt        8.172 Mt        8.172 Mt   100.000 % 	 ../DeferredRLInitialization

		      66.801 Mt     8.249 %         1 x       66.801 Mt       66.801 Mt        4.154 Mt     6.218 % 	 ../RayTracingRLInitialization

		      62.647 Mt    93.782 %         1 x       62.647 Mt       62.647 Mt       62.647 Mt   100.000 % 	 .../PipelineBuild

		       7.481 Mt     0.924 %         1 x        7.481 Mt        7.481 Mt        7.481 Mt   100.000 % 	 ../ResolveRLInitialization

		     283.256 Mt    34.979 %         1 x      283.256 Mt      283.256 Mt        6.353 Mt     2.243 % 	 ../Initialization

		     276.903 Mt    97.757 %         1 x      276.903 Mt      276.903 Mt        2.892 Mt     1.044 % 	 .../ScenePrep

		     233.450 Mt    84.307 %         1 x      233.450 Mt      233.450 Mt       23.883 Mt    10.231 % 	 ..../SceneLoad

		     180.114 Mt    77.153 %         1 x      180.114 Mt      180.114 Mt       33.305 Mt    18.491 % 	 ...../glTF-LoadScene

		     146.809 Mt    81.509 %         2 x       73.405 Mt       68.042 Mt      146.809 Mt   100.000 % 	 ....../glTF-LoadImageData

		      15.746 Mt     6.745 %         1 x       15.746 Mt       15.746 Mt       15.746 Mt   100.000 % 	 ...../glTF-CreateScene

		      13.706 Mt     5.871 %         1 x       13.706 Mt       13.706 Mt       13.706 Mt   100.000 % 	 ...../SceneDesc-CreateScene

		       2.984 Mt     1.078 %         1 x        2.984 Mt        2.984 Mt        0.016 Mt     0.533 % 	 ..../TexturedRLPrep

		       2.968 Mt    99.467 %         1 x        2.968 Mt        2.968 Mt        0.947 Mt    31.911 % 	 ...../PrepareForRendering

		       1.084 Mt    36.524 %         1 x        1.084 Mt        1.084 Mt        1.084 Mt   100.000 % 	 ....../PrepareMaterials

		       0.937 Mt    31.564 %         1 x        0.937 Mt        0.937 Mt        0.937 Mt   100.000 % 	 ....../PrepareDrawBundle

		       5.150 Mt     1.860 %         1 x        5.150 Mt        5.150 Mt        0.015 Mt     0.295 % 	 ..../DeferredRLPrep

		       5.135 Mt    99.705 %         1 x        5.135 Mt        5.135 Mt        3.231 Mt    62.929 % 	 ...../PrepareForRendering

		       0.054 Mt     1.044 %         1 x        0.054 Mt        0.054 Mt        0.054 Mt   100.000 % 	 ....../PrepareMaterials

		       0.980 Mt    19.088 %         1 x        0.980 Mt        0.980 Mt        0.980 Mt   100.000 % 	 ....../BuildMaterialCache

		       0.870 Mt    16.940 %         1 x        0.870 Mt        0.870 Mt        0.870 Mt   100.000 % 	 ....../PrepareDrawBundle

		      17.995 Mt     6.499 %         1 x       17.995 Mt       17.995 Mt        8.320 Mt    46.235 % 	 ..../RayTracingRLPrep

		       0.745 Mt     4.138 %         1 x        0.745 Mt        0.745 Mt        0.745 Mt   100.000 % 	 ...../PrepareAccelerationStructures

		       0.078 Mt     0.431 %         1 x        0.078 Mt        0.078 Mt        0.078 Mt   100.000 % 	 ...../PrepareCache

		       1.094 Mt     6.079 %         1 x        1.094 Mt        1.094 Mt        1.094 Mt   100.000 % 	 ...../BuildCache

		       5.754 Mt    31.975 %         1 x        5.754 Mt        5.754 Mt        0.013 Mt     0.221 % 	 ...../BuildAccelerationStructures

		       5.741 Mt    99.779 %         1 x        5.741 Mt        5.741 Mt        3.331 Mt    58.019 % 	 ....../AccelerationStructureBuild

		       1.298 Mt    22.615 %         1 x        1.298 Mt        1.298 Mt        1.298 Mt   100.000 % 	 ......./BuildBottomLevelAS

		       1.112 Mt    19.365 %         1 x        1.112 Mt        1.112 Mt        1.112 Mt   100.000 % 	 ......./BuildTopLevelAS

		       2.005 Mt    11.141 %         1 x        2.005 Mt        2.005 Mt        2.005 Mt   100.000 % 	 ...../BuildShaderTables

		      14.432 Mt     5.212 %         1 x       14.432 Mt       14.432 Mt       14.432 Mt   100.000 % 	 ..../GpuSidePrep

		      26.697 Mt     3.297 %         3 x        8.899 Mt        4.396 Mt       15.738 Mt    58.949 % 	 ../AccelerationStructureBuild

		       6.837 Mt    25.611 %         3 x        2.279 Mt        0.719 Mt        6.837 Mt   100.000 % 	 .../BuildBottomLevelAS

		       4.122 Mt    15.439 %         3 x        1.374 Mt        0.906 Mt        4.122 Mt   100.000 % 	 .../BuildTopLevelAS

	WindowThread:

		     801.099 Mt   100.000 %         1 x      801.100 Mt      801.099 Mt      801.099 Mt   100.000 % 	 /

	GpuThread:

		      53.026 Mt   100.000 %         2 x       26.513 Mt        0.692 Mt       51.021 Mt    96.220 % 	 /

		       0.448 Mt     0.845 %         1 x        0.448 Mt        0.448 Mt        0.007 Mt     1.499 % 	 ./ScenePrep

		       0.441 Mt    98.501 %         1 x        0.441 Mt        0.441 Mt        0.001 Mt     0.290 % 	 ../AccelerationStructureBuild

		       0.357 Mt    80.879 %         1 x        0.357 Mt        0.357 Mt        0.357 Mt   100.000 % 	 .../BuildBottomLevelASGpu

		       0.083 Mt    18.832 %         1 x        0.083 Mt        0.083 Mt        0.083 Mt   100.000 % 	 .../BuildTopLevelASGpu

		       1.556 Mt     2.935 %         3 x        0.519 Mt        0.692 Mt        0.004 Mt     0.257 % 	 ./AccelerationStructureBuild

		       1.305 Mt    83.870 %         3 x        0.435 Mt        0.610 Mt        1.305 Mt   100.000 % 	 ../BuildBottomLevelASGpu

		       0.247 Mt    15.873 %         3 x        0.082 Mt        0.081 Mt        0.247 Mt   100.000 % 	 ../BuildTopLevelASGpu


	============================


