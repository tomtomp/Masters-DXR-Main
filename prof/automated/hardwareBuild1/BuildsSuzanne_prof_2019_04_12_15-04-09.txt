Configuration: 
Command line parameters  = E:\programming\FIT-DIP\Masters-DXR-Main-git\build\Measurement\x64\Debug\Measurement.exe --scene Suzanne/Suzanne.gltf --profile-builds --profile-output BuildsSuzanne 
Window width             = 1024
Window height            = 768
Window title             = Direct3D App
Back-buffer count        = 3
Fill Triangles           = enabled
Triangles Font Clock-Wise= enabled
Fast Build AS            = disabled
Target FPS               = 60
Target UPS               = 60
Tearing                  = disabled
VSync                    = disabled
GUI rendering            = enabled
Debug layer              = enabled
GPU validation           = disabled
GPU profiling            = enabled
Render mode              = Rasterization
Loaded scene file        = Suzanne/Suzanne.gltf
Using testing scene      = disabled
Duplication              = 1
Duplication in cube      = disabled
Duplication offset       = 10
BLAS duplication         = disabled
Rays per pixel           = 10

ProfilingAggregator: 

ProfilingBuildAggregator: 
BuildBottomLevelAS	,	0.724	,	1.0715
BuildBottomLevelASGpu	,	0.349344	,	0.611232
BuildTopLevelAS	,	3.2891	,	3.4382
BuildTopLevelASGpu	,	0.083424	,	0.08384
Duplication	,	1	,	1
Triangles	,	3936	,	3936
Meshes	,	1	,	1
BottomLevels	,	1	,	1
Index	,	0	,	1

FpsAggregator: 
FPS	,	0	,	0
UPS	,	0	,	0
FrameTime	,	0	,	0
GigaRays/s	,	0	,	0
Index	,	0	,	1


AccelerationStructure: 
Ray tracing acceleration structure building statistics: 
	Bottom Level Acceleration Structure: 
		Maximum [B]: 224128
		Minimum [B]: 224128
		Currently Allocated [B]: 224128
		Currently Created [num]: 1
		Current Average [B]: 224128
		Maximum Triangles [num]: 3936
		Minimum Triangles [num]: 3936
		Total Triangles [num]: 3936
		Maximum Meshes [num]: 1
		Minimum Meshes [num]: 1
		Total Meshes [num]: 1
	Top Level Acceleration Structure: 
		Maximum [B]: 64
		Minimum [B]: 64
		Currently Allocated [B]: 64
		Total Allocated [B]: 64
		Total Created [num]: 1
		Average Allocated [B]: 64
		Maximum Instances [num]: 1
		Minimum Instances [num]: 1
		Total Instances [num]: 1
	Scratch Buffer: 
		Maximum [B]: 34304
		Minimum [B]: 34304
		Currently Allocated [B]: 34304
		Total Allocated [B]: 34304
		Total Created [num]: 1
		Average Allocated [B]: 34304
ProfilingStack: 
[PROF] Profiling statistics: 
	Scopes entered : 55
	Scopes exited : 54
	Overhead per scope [ticks] : 1005
	Threads entered : 3
	Threads exited : 0
	============================
		         Sum[Mt]   Parent[%]  Samples[num]       Avg[Mt]   Last/Curr[Mt]        Self[Mt]     Self[%] 	 Name

	main:

		     725.567 Mt   100.000 %         1 x      725.569 Mt      725.567 Mt        0.889 Mt     0.123 % 	 /

		     724.749 Mt    99.887 %         1 x      724.752 Mt      724.749 Mt      375.436 Mt    51.802 % 	 ./Main application loop

		       3.001 Mt     0.414 %         1 x        3.001 Mt        3.001 Mt        3.001 Mt   100.000 % 	 ../TexturedRLInitialization

		       6.041 Mt     0.833 %         1 x        6.041 Mt        6.041 Mt        6.041 Mt   100.000 % 	 ../DeferredRLInitialization

		      56.351 Mt     7.775 %         1 x       56.351 Mt       56.351 Mt        2.398 Mt     4.256 % 	 ../RayTracingRLInitialization

		      53.953 Mt    95.744 %         1 x       53.953 Mt       53.953 Mt       53.953 Mt   100.000 % 	 .../PipelineBuild

		       3.186 Mt     0.440 %         1 x        3.186 Mt        3.186 Mt        3.186 Mt   100.000 % 	 ../ResolveRLInitialization

		     249.020 Mt    34.359 %         1 x      249.020 Mt      249.020 Mt        3.417 Mt     1.372 % 	 ../Initialization

		     245.602 Mt    98.628 %         1 x      245.602 Mt      245.602 Mt        1.687 Mt     0.687 % 	 .../ScenePrep

		     211.133 Mt    85.965 %         1 x      211.133 Mt      211.133 Mt       18.778 Mt     8.894 % 	 ..../SceneLoad

		     164.315 Mt    77.825 %         1 x      164.315 Mt      164.315 Mt       19.939 Mt    12.135 % 	 ...../glTF-LoadScene

		     144.375 Mt    87.865 %         2 x       72.188 Mt       67.268 Mt      144.375 Mt   100.000 % 	 ....../glTF-LoadImageData

		      15.222 Mt     7.210 %         1 x       15.222 Mt       15.222 Mt       15.222 Mt   100.000 % 	 ...../glTF-CreateScene

		      12.818 Mt     6.071 %         1 x       12.818 Mt       12.818 Mt       12.818 Mt   100.000 % 	 ...../SceneDesc-CreateScene

		       3.735 Mt     1.521 %         1 x        3.735 Mt        3.735 Mt        0.006 Mt     0.155 % 	 ..../TexturedRLPrep

		       3.729 Mt    99.845 %         1 x        3.729 Mt        3.729 Mt        0.917 Mt    24.586 % 	 ...../PrepareForRendering

		       2.522 Mt    67.635 %         1 x        2.522 Mt        2.522 Mt        2.522 Mt   100.000 % 	 ....../PrepareMaterials

		       0.290 Mt     7.780 %         1 x        0.290 Mt        0.290 Mt        0.290 Mt   100.000 % 	 ....../PrepareDrawBundle

		       2.823 Mt     1.149 %         1 x        2.823 Mt        2.823 Mt        0.005 Mt     0.163 % 	 ..../DeferredRLPrep

		       2.818 Mt    99.837 %         1 x        2.818 Mt        2.818 Mt        1.848 Mt    65.571 % 	 ...../PrepareForRendering

		       0.017 Mt     0.607 %         1 x        0.017 Mt        0.017 Mt        0.017 Mt   100.000 % 	 ....../PrepareMaterials

		       0.694 Mt    24.618 %         1 x        0.694 Mt        0.694 Mt        0.694 Mt   100.000 % 	 ....../BuildMaterialCache

		       0.259 Mt     9.204 %         1 x        0.259 Mt        0.259 Mt        0.259 Mt   100.000 % 	 ....../PrepareDrawBundle

		      14.842 Mt     6.043 %         1 x       14.842 Mt       14.842 Mt        4.439 Mt    29.911 % 	 ..../RayTracingRLPrep

		       0.234 Mt     1.579 %         1 x        0.234 Mt        0.234 Mt        0.234 Mt   100.000 % 	 ...../PrepareAccelerationStructures

		       0.025 Mt     0.166 %         1 x        0.025 Mt        0.025 Mt        0.025 Mt   100.000 % 	 ...../PrepareCache

		       0.701 Mt     4.723 %         1 x        0.701 Mt        0.701 Mt        0.701 Mt   100.000 % 	 ...../BuildCache

		       7.234 Mt    48.736 %         1 x        7.234 Mt        7.234 Mt        0.004 Mt     0.053 % 	 ...../BuildAccelerationStructures

		       7.230 Mt    99.947 %         1 x        7.230 Mt        7.230 Mt        3.591 Mt    49.668 % 	 ....../AccelerationStructureBuild

		       1.017 Mt    14.067 %         1 x        1.017 Mt        1.017 Mt        1.017 Mt   100.000 % 	 ......./BuildBottomLevelAS

		       2.622 Mt    36.265 %         1 x        2.622 Mt        2.622 Mt        2.622 Mt   100.000 % 	 ......./BuildTopLevelAS

		       2.209 Mt    14.886 %         1 x        2.209 Mt        2.209 Mt        2.209 Mt   100.000 % 	 ...../BuildShaderTables

		      11.382 Mt     4.634 %         1 x       11.382 Mt       11.382 Mt       11.382 Mt   100.000 % 	 ..../GpuSidePrep

		      31.714 Mt     4.376 %         3 x       10.571 Mt        8.349 Mt       17.818 Mt    56.184 % 	 ../AccelerationStructureBuild

		       4.031 Mt    12.710 %         3 x        1.344 Mt        1.071 Mt        4.031 Mt   100.000 % 	 .../BuildBottomLevelAS

		       9.865 Mt    31.106 %         3 x        3.288 Mt        3.438 Mt        9.865 Mt   100.000 % 	 .../BuildTopLevelAS

	WindowThread:

		     709.156 Mt   100.000 %         1 x      709.157 Mt      709.156 Mt      709.156 Mt   100.000 % 	 /

	GpuThread:

		      53.755 Mt   100.000 %         2 x       26.878 Mt        0.697 Mt       51.737 Mt    96.246 % 	 /

		       0.453 Mt     0.843 %         1 x        0.453 Mt        0.453 Mt        0.007 Mt     1.632 % 	 ./ScenePrep

		       0.446 Mt    98.368 %         1 x        0.446 Mt        0.446 Mt        0.002 Mt     0.366 % 	 ../AccelerationStructureBuild

		       0.359 Mt    80.592 %         1 x        0.359 Mt        0.359 Mt        0.359 Mt   100.000 % 	 .../BuildBottomLevelASGpu

		       0.085 Mt    19.042 %         1 x        0.085 Mt        0.085 Mt        0.085 Mt   100.000 % 	 .../BuildTopLevelASGpu

		       1.565 Mt     2.911 %         3 x        0.522 Mt        0.697 Mt        0.004 Mt     0.276 % 	 ./AccelerationStructureBuild

		       1.310 Mt    83.705 %         3 x        0.437 Mt        0.611 Mt        1.310 Mt   100.000 % 	 ../BuildBottomLevelASGpu

		       0.251 Mt    16.019 %         3 x        0.084 Mt        0.084 Mt        0.251 Mt   100.000 % 	 ../BuildTopLevelASGpu


	============================


