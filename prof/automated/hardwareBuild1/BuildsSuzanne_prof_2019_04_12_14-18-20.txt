Configuration: 
Command line parameters  = E:\programming\FIT-DIP\Masters-DXR-Main-git\build\Measurement\x64\Debug\Measurement.exe --scene Suzanne/Suzanne.gltf --profile-builds --profile-output BuildsSuzanne 
Window width             = 1024
Window height            = 768
Window title             = Direct3D App
Back-buffer count        = 3
Fill Triangles           = enabled
Triangles Font Clock-Wise= enabled
Fast Build AS            = disabled
Target FPS               = 60
Target UPS               = 60
Tearing                  = disabled
VSync                    = disabled
GUI rendering            = enabled
Debug layer              = enabled
GPU validation           = disabled
GPU profiling            = enabled
Render mode              = Rasterization
Loaded scene file        = Suzanne/Suzanne.gltf
Using testing scene      = disabled
Duplication              = 1
Duplication in cube      = disabled
Duplication offset       = 10
BLAS duplication         = disabled
Rays per pixel           = 10

ProfilingAggregator: 

ProfilingBuildAggregator: 
BuildBottomLevelAS	,	3.4888	,	1.3777
BuildBottomLevelASGpu	,	0.351264	,	0.613152
BuildTopLevelAS	,	1.1452	,	1.4036
BuildTopLevelASGpu	,	0.082304	,	0.08464
Duplication	,	1	,	1
Triangles	,	3936	,	3936
Meshes	,	1	,	1
BottomLevels	,	1	,	1
Index	,	0	,	1

FpsAggregator: 
FPS	,	0	,	0
UPS	,	0	,	0
FrameTime	,	0	,	0
GigaRays/s	,	0	,	0
Index	,	0	,	1


AccelerationStructure: 
Ray tracing acceleration structure building statistics: 
	Bottom Level Acceleration Structure: 
		Maximum [B]: 224128
		Minimum [B]: 224128
		Currently Allocated [B]: 224128
		Currently Created [num]: 1
		Current Average [B]: 224128
		Maximum Triangles [num]: 3936
		Minimum Triangles [num]: 3936
		Total Triangles [num]: 3936
		Maximum Meshes [num]: 1
		Minimum Meshes [num]: 1
		Total Meshes [num]: 1
	Top Level Acceleration Structure: 
		Maximum [B]: 64
		Minimum [B]: 64
		Currently Allocated [B]: 64
		Total Allocated [B]: 64
		Total Created [num]: 1
		Average Allocated [B]: 64
		Maximum Instances [num]: 1
		Minimum Instances [num]: 1
		Total Instances [num]: 1
	Scratch Buffer: 
		Maximum [B]: 34304
		Minimum [B]: 34304
		Currently Allocated [B]: 34304
		Total Allocated [B]: 34304
		Total Created [num]: 1
		Average Allocated [B]: 34304
ProfilingStack: 
[PROF] Profiling statistics: 
	Scopes entered : 55
	Scopes exited : 54
	Overhead per scope [ticks] : 1018.5
	Threads entered : 3
	Threads exited : 0
	============================
		         Sum[Mt]   Parent[%]  Samples[num]       Avg[Mt]   Last/Curr[Mt]        Self[Mt]     Self[%] 	 Name

	main:

		     795.152 Mt   100.000 %         1 x      795.155 Mt      795.151 Mt        1.163 Mt     0.146 % 	 /

		     794.092 Mt    99.867 %         1 x      794.095 Mt      794.091 Mt      382.574 Mt    48.177 % 	 ./Main application loop

		       2.876 Mt     0.362 %         1 x        2.876 Mt        2.876 Mt        2.876 Mt   100.000 % 	 ../TexturedRLInitialization

		       6.034 Mt     0.760 %         1 x        6.034 Mt        6.034 Mt        6.034 Mt   100.000 % 	 ../DeferredRLInitialization

		      60.876 Mt     7.666 %         1 x       60.876 Mt       60.876 Mt        2.848 Mt     4.678 % 	 ../RayTracingRLInitialization

		      58.028 Mt    95.322 %         1 x       58.028 Mt       58.028 Mt       58.028 Mt   100.000 % 	 .../PipelineBuild

		       3.346 Mt     0.421 %         1 x        3.346 Mt        3.346 Mt        3.346 Mt   100.000 % 	 ../ResolveRLInitialization

		     298.080 Mt    37.537 %         1 x      298.080 Mt      298.080 Mt        8.907 Mt     2.988 % 	 ../Initialization

		     289.173 Mt    97.012 %         1 x      289.173 Mt      289.173 Mt        2.154 Mt     0.745 % 	 .../ScenePrep

		     243.002 Mt    84.033 %         1 x      243.002 Mt      243.002 Mt       21.506 Mt     8.850 % 	 ..../SceneLoad

		     185.615 Mt    76.384 %         1 x      185.615 Mt      185.615 Mt       38.213 Mt    20.587 % 	 ...../glTF-LoadScene

		     147.403 Mt    79.413 %         2 x       73.701 Mt       67.879 Mt      147.403 Mt   100.000 % 	 ....../glTF-LoadImageData

		      17.791 Mt     7.322 %         1 x       17.791 Mt       17.791 Mt       17.791 Mt   100.000 % 	 ...../glTF-CreateScene

		      18.089 Mt     7.444 %         1 x       18.089 Mt       18.089 Mt       18.089 Mt   100.000 % 	 ...../SceneDesc-CreateScene

		       2.635 Mt     0.911 %         1 x        2.635 Mt        2.635 Mt        0.005 Mt     0.190 % 	 ..../TexturedRLPrep

		       2.630 Mt    99.810 %         1 x        2.630 Mt        2.630 Mt        0.977 Mt    37.152 % 	 ...../PrepareForRendering

		       1.371 Mt    52.120 %         1 x        1.371 Mt        1.371 Mt        1.371 Mt   100.000 % 	 ....../PrepareMaterials

		       0.282 Mt    10.729 %         1 x        0.282 Mt        0.282 Mt        0.282 Mt   100.000 % 	 ....../PrepareDrawBundle

		       5.749 Mt     1.988 %         1 x        5.749 Mt        5.749 Mt        0.005 Mt     0.087 % 	 ..../DeferredRLPrep

		       5.744 Mt    99.913 %         1 x        5.744 Mt        5.744 Mt        2.736 Mt    47.627 % 	 ...../PrepareForRendering

		       0.016 Mt     0.277 %         1 x        0.016 Mt        0.016 Mt        0.016 Mt   100.000 % 	 ....../PrepareMaterials

		       2.730 Mt    47.519 %         1 x        2.730 Mt        2.730 Mt        2.730 Mt   100.000 % 	 ....../BuildMaterialCache

		       0.263 Mt     4.577 %         1 x        0.263 Mt        0.263 Mt        0.263 Mt   100.000 % 	 ....../PrepareDrawBundle

		      23.275 Mt     8.049 %         1 x       23.275 Mt       23.275 Mt        9.570 Mt    41.118 % 	 ..../RayTracingRLPrep

		       0.243 Mt     1.046 %         1 x        0.243 Mt        0.243 Mt        0.243 Mt   100.000 % 	 ...../PrepareAccelerationStructures

		       0.024 Mt     0.104 %         1 x        0.024 Mt        0.024 Mt        0.024 Mt   100.000 % 	 ...../PrepareCache

		       1.345 Mt     5.777 %         1 x        1.345 Mt        1.345 Mt        1.345 Mt   100.000 % 	 ...../BuildCache

		       9.211 Mt    39.574 %         1 x        9.211 Mt        9.211 Mt        0.004 Mt     0.045 % 	 ...../BuildAccelerationStructures

		       9.207 Mt    99.955 %         1 x        9.207 Mt        9.207 Mt        4.889 Mt    53.103 % 	 ....../AccelerationStructureBuild

		       2.995 Mt    32.532 %         1 x        2.995 Mt        2.995 Mt        2.995 Mt   100.000 % 	 ......./BuildBottomLevelAS

		       1.323 Mt    14.365 %         1 x        1.323 Mt        1.323 Mt        1.323 Mt   100.000 % 	 ......./BuildTopLevelAS

		       2.881 Mt    12.380 %         1 x        2.881 Mt        2.881 Mt        2.881 Mt   100.000 % 	 ...../BuildShaderTables

		      12.359 Mt     4.274 %         1 x       12.359 Mt       12.359 Mt       12.359 Mt   100.000 % 	 ..../GpuSidePrep

		      40.307 Mt     5.076 %         3 x       13.436 Mt       11.820 Mt       28.651 Mt    71.084 % 	 ../AccelerationStructureBuild

		       7.586 Mt    18.822 %         3 x        2.529 Mt        1.378 Mt        7.586 Mt   100.000 % 	 .../BuildBottomLevelAS

		       4.069 Mt    10.095 %         3 x        1.356 Mt        1.404 Mt        4.069 Mt   100.000 % 	 .../BuildTopLevelAS

	WindowThread:

		     779.718 Mt   100.000 %         1 x      779.719 Mt      779.718 Mt      779.718 Mt   100.000 % 	 /

	GpuThread:

		      72.346 Mt   100.000 %         2 x       36.173 Mt        0.699 Mt       70.328 Mt    97.210 % 	 /

		       0.450 Mt     0.622 %         1 x        0.450 Mt        0.450 Mt        0.007 Mt     1.459 % 	 ./ScenePrep

		       0.443 Mt    98.541 %         1 x        0.443 Mt        0.443 Mt        0.001 Mt     0.282 % 	 ../AccelerationStructureBuild

		       0.359 Mt    80.960 %         1 x        0.359 Mt        0.359 Mt        0.359 Mt   100.000 % 	 .../BuildBottomLevelASGpu

		       0.083 Mt    18.758 %         1 x        0.083 Mt        0.083 Mt        0.083 Mt   100.000 % 	 .../BuildTopLevelASGpu

		       1.569 Mt     2.169 %         3 x        0.523 Mt        0.699 Mt        0.004 Mt     0.255 % 	 ./AccelerationStructureBuild

		       1.316 Mt    83.859 %         3 x        0.439 Mt        0.613 Mt        1.316 Mt   100.000 % 	 ../BuildBottomLevelASGpu

		       0.249 Mt    15.887 %         3 x        0.083 Mt        0.085 Mt        0.249 Mt   100.000 % 	 ../BuildTopLevelASGpu


	============================


