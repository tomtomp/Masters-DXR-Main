Configuration: 
Command line parameters  = E:\programming\FIT-DIP\Masters-DXR-Main-git\build\Measurement\x64\Debug\Measurement.exe --scene Cube/Cube.gltf --profile-builds --profile-output BuildsCube 
Window width             = 1024
Window height            = 768
Window title             = Direct3D App
Back-buffer count        = 3
Fill Triangles           = enabled
Triangles Font Clock-Wise= enabled
Fast Build AS            = disabled
Target FPS               = 60
Target UPS               = 60
Tearing                  = disabled
VSync                    = disabled
GUI rendering            = enabled
Debug layer              = enabled
GPU validation           = disabled
GPU profiling            = enabled
Render mode              = Rasterization
Loaded scene file        = Cube/Cube.gltf
Using testing scene      = disabled
Duplication              = 1
Duplication in cube      = disabled
Duplication offset       = 10
BLAS duplication         = disabled
Rays per pixel           = 10

ProfilingAggregator: 

ProfilingBuildAggregator: 
BuildBottomLevelAS	,	1.3323	,	0.5848
BuildBottomLevelASGpu	,	0.119648	,	0.160544
BuildTopLevelAS	,	1.3127	,	0.8502
BuildTopLevelASGpu	,	0.082784	,	0.081152
Duplication	,	1	,	1
Triangles	,	12	,	12
Meshes	,	1	,	1
BottomLevels	,	1	,	1
Index	,	0	,	1

FpsAggregator: 
FPS	,	0	,	0
UPS	,	0	,	0
FrameTime	,	0	,	0
GigaRays/s	,	0	,	0
Index	,	0	,	1


AccelerationStructure: 
Ray tracing acceleration structure building statistics: 
	Bottom Level Acceleration Structure: 
		Maximum [B]: 5168
		Minimum [B]: 5168
		Currently Allocated [B]: 5168
		Currently Created [num]: 1
		Current Average [B]: 5168
		Maximum Triangles [num]: 12
		Minimum Triangles [num]: 12
		Total Triangles [num]: 12
		Maximum Meshes [num]: 1
		Minimum Meshes [num]: 1
		Total Meshes [num]: 1
	Top Level Acceleration Structure: 
		Maximum [B]: 64
		Minimum [B]: 64
		Currently Allocated [B]: 64
		Total Allocated [B]: 64
		Total Created [num]: 1
		Average Allocated [B]: 64
		Maximum Instances [num]: 1
		Minimum Instances [num]: 1
		Total Instances [num]: 1
	Scratch Buffer: 
		Maximum [B]: 3584
		Minimum [B]: 3584
		Currently Allocated [B]: 3584
		Total Allocated [B]: 3584
		Total Created [num]: 1
		Average Allocated [B]: 3584
ProfilingStack: 
[PROF] Profiling statistics: 
	Scopes entered : 55
	Scopes exited : 54
	Overhead per scope [ticks] : 1006.2
	Threads entered : 3
	Threads exited : 0
	============================
		         Sum[Mt]   Parent[%]  Samples[num]       Avg[Mt]   Last/Curr[Mt]        Self[Mt]     Self[%] 	 Name

	main:

		     679.528 Mt   100.000 %         1 x      679.530 Mt      679.527 Mt        1.114 Mt     0.164 % 	 /

		     678.477 Mt    99.845 %         1 x      678.479 Mt      678.476 Mt      392.618 Mt    57.868 % 	 ./Main application loop

		       2.844 Mt     0.419 %         1 x        2.844 Mt        2.844 Mt        2.844 Mt   100.000 % 	 ../TexturedRLInitialization

		       6.225 Mt     0.918 %         1 x        6.225 Mt        6.225 Mt        6.225 Mt   100.000 % 	 ../DeferredRLInitialization

		      56.849 Mt     8.379 %         1 x       56.849 Mt       56.849 Mt        2.190 Mt     3.852 % 	 ../RayTracingRLInitialization

		      54.659 Mt    96.148 %         1 x       54.659 Mt       54.659 Mt       54.659 Mt   100.000 % 	 .../PipelineBuild

		       3.266 Mt     0.481 %         1 x        3.266 Mt        3.266 Mt        3.266 Mt   100.000 % 	 ../ResolveRLInitialization

		     182.666 Mt    26.923 %         1 x      182.666 Mt      182.666 Mt        4.219 Mt     2.310 % 	 ../Initialization

		     178.448 Mt    97.690 %         1 x      178.448 Mt      178.448 Mt        3.176 Mt     1.780 % 	 .../ScenePrep

		     135.657 Mt    76.021 %         1 x      135.657 Mt      135.657 Mt       21.738 Mt    16.024 % 	 ..../SceneLoad

		      84.468 Mt    62.266 %         1 x       84.468 Mt       84.468 Mt       34.191 Mt    40.478 % 	 ...../glTF-LoadScene

		      50.277 Mt    59.522 %         2 x       25.139 Mt        2.354 Mt       50.277 Mt   100.000 % 	 ....../glTF-LoadImageData

		      14.263 Mt    10.514 %         1 x       14.263 Mt       14.263 Mt       14.263 Mt   100.000 % 	 ...../glTF-CreateScene

		      15.188 Mt    11.196 %         1 x       15.188 Mt       15.188 Mt       15.188 Mt   100.000 % 	 ...../SceneDesc-CreateScene

		       2.992 Mt     1.677 %         1 x        2.992 Mt        2.992 Mt        0.016 Mt     0.535 % 	 ..../TexturedRLPrep

		       2.976 Mt    99.465 %         1 x        2.976 Mt        2.976 Mt        1.006 Mt    33.796 % 	 ...../PrepareForRendering

		       1.011 Mt    33.977 %         1 x        1.011 Mt        1.011 Mt        1.011 Mt   100.000 % 	 ....../PrepareMaterials

		       0.959 Mt    32.227 %         1 x        0.959 Mt        0.959 Mt        0.959 Mt   100.000 % 	 ....../PrepareDrawBundle

		       5.188 Mt     2.907 %         1 x        5.188 Mt        5.188 Mt        0.016 Mt     0.301 % 	 ..../DeferredRLPrep

		       5.172 Mt    99.699 %         1 x        5.172 Mt        5.172 Mt        3.309 Mt    63.969 % 	 ...../PrepareForRendering

		       0.055 Mt     1.069 %         1 x        0.055 Mt        0.055 Mt        0.055 Mt   100.000 % 	 ....../PrepareMaterials

		       0.923 Mt    17.853 %         1 x        0.923 Mt        0.923 Mt        0.923 Mt   100.000 % 	 ....../BuildMaterialCache

		       0.885 Mt    17.109 %         1 x        0.885 Mt        0.885 Mt        0.885 Mt   100.000 % 	 ....../PrepareDrawBundle

		      19.400 Mt    10.872 %         1 x       19.400 Mt       19.400 Mt        7.937 Mt    40.913 % 	 ..../RayTracingRLPrep

		       0.645 Mt     3.323 %         1 x        0.645 Mt        0.645 Mt        0.645 Mt   100.000 % 	 ...../PrepareAccelerationStructures

		       0.065 Mt     0.332 %         1 x        0.065 Mt        0.065 Mt        0.065 Mt   100.000 % 	 ...../PrepareCache

		       0.931 Mt     4.797 %         1 x        0.931 Mt        0.931 Mt        0.931 Mt   100.000 % 	 ...../BuildCache

		       6.779 Mt    34.943 %         1 x        6.779 Mt        6.779 Mt        0.006 Mt     0.086 % 	 ...../BuildAccelerationStructures

		       6.773 Mt    99.914 %         1 x        6.773 Mt        6.773 Mt        3.563 Mt    52.606 % 	 ....../AccelerationStructureBuild

		       1.750 Mt    25.830 %         1 x        1.750 Mt        1.750 Mt        1.750 Mt   100.000 % 	 ......./BuildBottomLevelAS

		       1.461 Mt    21.564 %         1 x        1.461 Mt        1.461 Mt        1.461 Mt   100.000 % 	 ......./BuildTopLevelAS

		       3.044 Mt    15.692 %         1 x        3.044 Mt        3.044 Mt        3.044 Mt   100.000 % 	 ...../BuildShaderTables

		      12.034 Mt     6.744 %         1 x       12.034 Mt       12.034 Mt       12.034 Mt   100.000 % 	 ..../GpuSidePrep

		      34.008 Mt     5.012 %         3 x       11.336 Mt        5.172 Mt       22.092 Mt    64.962 % 	 ../AccelerationStructureBuild

		       7.338 Mt    21.577 %         3 x        2.446 Mt        0.585 Mt        7.338 Mt   100.000 % 	 .../BuildBottomLevelAS

		       4.578 Mt    13.461 %         3 x        1.526 Mt        0.850 Mt        4.578 Mt   100.000 % 	 .../BuildTopLevelAS

	WindowThread:

		     668.292 Mt   100.000 %         1 x      668.293 Mt      668.291 Mt      668.292 Mt   100.000 % 	 /

	GpuThread:

		      63.991 Mt   100.000 %         2 x       31.996 Mt        0.243 Mt       63.130 Mt    98.654 % 	 /

		       0.211 Mt     0.329 %         1 x        0.211 Mt        0.211 Mt        0.007 Mt     3.233 % 	 ./ScenePrep

		       0.204 Mt    96.767 %         1 x        0.204 Mt        0.204 Mt        0.001 Mt     0.424 % 	 ../AccelerationStructureBuild

		       0.120 Mt    59.059 %         1 x        0.120 Mt        0.120 Mt        0.120 Mt   100.000 % 	 .../BuildBottomLevelASGpu

		       0.083 Mt    40.518 %         1 x        0.083 Mt        0.083 Mt        0.083 Mt   100.000 % 	 .../BuildTopLevelASGpu

		       0.650 Mt     1.016 %         3 x        0.217 Mt        0.243 Mt        0.004 Mt     0.586 % 	 ./AccelerationStructureBuild

		       0.400 Mt    61.479 %         3 x        0.133 Mt        0.161 Mt        0.400 Mt   100.000 % 	 ../BuildBottomLevelASGpu

		       0.247 Mt    37.935 %         3 x        0.082 Mt        0.081 Mt        0.247 Mt   100.000 % 	 ../BuildTopLevelASGpu


	============================


