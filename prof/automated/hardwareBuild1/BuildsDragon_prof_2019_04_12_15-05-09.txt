Configuration: 
Command line parameters  = E:\programming\FIT-DIP\Masters-DXR-Main-git\build\Measurement\x64\Debug\Measurement.exe --scene Dragon/Dragon.glb --profile-builds --profile-output BuildsDragon 
Window width             = 1024
Window height            = 768
Window title             = Direct3D App
Back-buffer count        = 3
Fill Triangles           = enabled
Triangles Font Clock-Wise= enabled
Fast Build AS            = disabled
Target FPS               = 60
Target UPS               = 60
Tearing                  = disabled
VSync                    = disabled
GUI rendering            = enabled
Debug layer              = enabled
GPU validation           = disabled
GPU profiling            = enabled
Render mode              = Rasterization
Loaded scene file        = Dragon/Dragon.glb
Using testing scene      = disabled
Duplication              = 1
Duplication in cube      = disabled
Duplication offset       = 10
BLAS duplication         = disabled
Rays per pixel           = 10

ProfilingAggregator: 

ProfilingBuildAggregator: 
BuildBottomLevelAS	,	1.3338	,	1.0363	,	1.2521	,	0.8809
BuildBottomLevelASGpu	,	0.567264	,	0.514144	,	0.886688	,	0.793664
BuildTopLevelAS	,	2.8859	,	2.8313	,	0.8352	,	0.813
BuildTopLevelASGpu	,	0.082656	,	0.082816	,	0.083808	,	0.079424
Duplication	,	1	,	1	,	1	,	1
Triangles	,	34852	,	11757	,	34852	,	11757
Meshes	,	1	,	1	,	1	,	1
BottomLevels	,	1	,	1	,	1	,	1
Index	,	0	,	1	,	2	,	3

FpsAggregator: 
FPS	,	0	,	0	,	0	,	0
UPS	,	0	,	0	,	0	,	0
FrameTime	,	0	,	0	,	0	,	0
GigaRays/s	,	0	,	0	,	0	,	0
Index	,	0	,	1	,	2	,	3


AccelerationStructure: 
Ray tracing acceleration structure building statistics: 
	Bottom Level Acceleration Structure: 
		Maximum [B]: 1947136
		Minimum [B]: 659968
		Currently Allocated [B]: 2607104
		Currently Created [num]: 2
		Current Average [B]: 1303552
		Maximum Triangles [num]: 34852
		Minimum Triangles [num]: 11757
		Total Triangles [num]: 46609
		Maximum Meshes [num]: 1
		Minimum Meshes [num]: 1
		Total Meshes [num]: 2
	Top Level Acceleration Structure: 
		Maximum [B]: 128
		Minimum [B]: 128
		Currently Allocated [B]: 128
		Total Allocated [B]: 128
		Total Created [num]: 1
		Average Allocated [B]: 128
		Maximum Instances [num]: 2
		Minimum Instances [num]: 2
		Total Instances [num]: 2
	Scratch Buffer: 
		Maximum [B]: 290304
		Minimum [B]: 290304
		Currently Allocated [B]: 290304
		Total Allocated [B]: 290304
		Total Created [num]: 1
		Average Allocated [B]: 290304
ProfilingStack: 
[PROF] Profiling statistics: 
	Scopes entered : 73
	Scopes exited : 72
	Overhead per scope [ticks] : 1004.8
	Threads entered : 3
	Threads exited : 0
	============================
		         Sum[Mt]   Parent[%]  Samples[num]       Avg[Mt]   Last/Curr[Mt]        Self[Mt]     Self[%] 	 Name

	main:

		    2664.838 Mt   100.000 %         1 x     2664.840 Mt     2664.837 Mt        0.759 Mt     0.028 % 	 /

		    2664.135 Mt    99.974 %         1 x     2664.137 Mt     2664.135 Mt      335.946 Mt    12.610 % 	 ./Main application loop

		       2.812 Mt     0.106 %         1 x        2.812 Mt        2.812 Mt        2.812 Mt   100.000 % 	 ../TexturedRLInitialization

		       6.448 Mt     0.242 %         1 x        6.448 Mt        6.448 Mt        6.448 Mt   100.000 % 	 ../DeferredRLInitialization

		      61.639 Mt     2.314 %         1 x       61.639 Mt       61.639 Mt        1.960 Mt     3.179 % 	 ../RayTracingRLInitialization

		      59.679 Mt    96.821 %         1 x       59.679 Mt       59.679 Mt       59.679 Mt   100.000 % 	 .../PipelineBuild

		       3.092 Mt     0.116 %         1 x        3.092 Mt        3.092 Mt        3.092 Mt   100.000 % 	 ../ResolveRLInitialization

		    2210.655 Mt    82.978 %         1 x     2210.655 Mt     2210.655 Mt        8.372 Mt     0.379 % 	 ../Initialization

		    2202.283 Mt    99.621 %         1 x     2202.283 Mt     2202.283 Mt        2.943 Mt     0.134 % 	 .../ScenePrep

		    2164.914 Mt    98.303 %         1 x     2164.914 Mt     2164.914 Mt       81.656 Mt     3.772 % 	 ..../SceneLoad

		    1963.251 Mt    90.685 %         1 x     1963.251 Mt     1963.251 Mt      153.075 Mt     7.797 % 	 ...../glTF-LoadScene

		    1810.176 Mt    92.203 %         8 x      226.272 Mt      365.759 Mt     1810.176 Mt   100.000 % 	 ....../glTF-LoadImageData

		      94.901 Mt     4.384 %         1 x       94.901 Mt       94.901 Mt       94.901 Mt   100.000 % 	 ...../glTF-CreateScene

		      25.105 Mt     1.160 %         1 x       25.105 Mt       25.105 Mt       25.105 Mt   100.000 % 	 ...../SceneDesc-CreateScene

		       2.398 Mt     0.109 %         1 x        2.398 Mt        2.398 Mt        0.008 Mt     0.334 % 	 ..../TexturedRLPrep

		       2.390 Mt    99.666 %         1 x        2.390 Mt        2.390 Mt        0.760 Mt    31.802 % 	 ...../PrepareForRendering

		       1.107 Mt    46.316 %         1 x        1.107 Mt        1.107 Mt        1.107 Mt   100.000 % 	 ....../PrepareMaterials

		       0.523 Mt    21.882 %         1 x        0.523 Mt        0.523 Mt        0.523 Mt   100.000 % 	 ....../PrepareDrawBundle

		       3.572 Mt     0.162 %         1 x        3.572 Mt        3.572 Mt        0.008 Mt     0.216 % 	 ..../DeferredRLPrep

		       3.564 Mt    99.784 %         1 x        3.564 Mt        3.564 Mt        2.288 Mt    64.208 % 	 ...../PrepareForRendering

		       0.049 Mt     1.375 %         1 x        0.049 Mt        0.049 Mt        0.049 Mt   100.000 % 	 ....../PrepareMaterials

		       0.745 Mt    20.888 %         1 x        0.745 Mt        0.745 Mt        0.745 Mt   100.000 % 	 ....../BuildMaterialCache

		       0.482 Mt    13.529 %         1 x        0.482 Mt        0.482 Mt        0.482 Mt   100.000 % 	 ....../PrepareDrawBundle

		      15.464 Mt     0.702 %         1 x       15.464 Mt       15.464 Mt        4.883 Mt    31.579 % 	 ..../RayTracingRLPrep

		       0.713 Mt     4.608 %         1 x        0.713 Mt        0.713 Mt        0.713 Mt   100.000 % 	 ...../PrepareAccelerationStructures

		       0.061 Mt     0.395 %         1 x        0.061 Mt        0.061 Mt        0.061 Mt   100.000 % 	 ...../PrepareCache

		       0.915 Mt     5.917 %         1 x        0.915 Mt        0.915 Mt        0.915 Mt   100.000 % 	 ...../BuildCache

		       5.456 Mt    35.284 %         1 x        5.456 Mt        5.456 Mt        0.006 Mt     0.114 % 	 ...../BuildAccelerationStructures

		       5.450 Mt    99.886 %         1 x        5.450 Mt        5.450 Mt        3.134 Mt    57.496 % 	 ....../AccelerationStructureBuild

		       1.515 Mt    27.790 %         1 x        1.515 Mt        1.515 Mt        1.515 Mt   100.000 % 	 ......./BuildBottomLevelAS

		       0.802 Mt    14.713 %         1 x        0.802 Mt        0.802 Mt        0.802 Mt   100.000 % 	 ......./BuildTopLevelAS

		       3.436 Mt    22.217 %         1 x        3.436 Mt        3.436 Mt        3.436 Mt   100.000 % 	 ...../BuildShaderTables

		      12.992 Mt     0.590 %         1 x       12.992 Mt       12.992 Mt       12.992 Mt   100.000 % 	 ..../GpuSidePrep

		      43.544 Mt     1.634 %         5 x        8.709 Mt        4.921 Mt       26.831 Mt    61.619 % 	 ../AccelerationStructureBuild

		       7.213 Mt    16.566 %         5 x        1.443 Mt        0.881 Mt        7.213 Mt   100.000 % 	 .../BuildBottomLevelAS

		       9.499 Mt    21.815 %         5 x        1.900 Mt        0.813 Mt        9.499 Mt   100.000 % 	 .../BuildTopLevelAS

	WindowThread:

		    2654.421 Mt   100.000 %         1 x     2654.421 Mt     2654.421 Mt     2654.421 Mt   100.000 % 	 /

	GpuThread:

		      71.646 Mt   100.000 %         4 x       17.912 Mt        0.874 Mt       66.717 Mt    93.120 % 	 /

		       1.182 Mt     1.650 %         1 x        1.182 Mt        1.182 Mt        0.007 Mt     0.552 % 	 ./ScenePrep

		       1.176 Mt    99.448 %         1 x        1.176 Mt        1.176 Mt        0.001 Mt     0.090 % 	 ../AccelerationStructureBuild

		       1.088 Mt    92.536 %         1 x        1.088 Mt        1.088 Mt        1.088 Mt   100.000 % 	 .../BuildBottomLevelASGpu

		       0.087 Mt     7.374 %         1 x        0.087 Mt        0.087 Mt        0.087 Mt   100.000 % 	 .../BuildTopLevelASGpu

		       3.747 Mt     5.230 %         5 x        0.749 Mt        0.874 Mt        0.007 Mt     0.174 % 	 ./AccelerationStructureBuild

		       3.329 Mt    88.847 %         5 x        0.666 Mt        0.794 Mt        3.329 Mt   100.000 % 	 ../BuildBottomLevelASGpu

		       0.411 Mt    10.979 %         5 x        0.082 Mt        0.079 Mt        0.411 Mt   100.000 % 	 ../BuildTopLevelASGpu


	============================


