Configuration: 
Command line parameters  = E:\programming\FIT-DIP\Masters-DXR-Main-git\build\Measurement\x64\Debug\Measurement.exe --scene Suzanne/Suzanne.gltf --profile-builds --profile-output BuildsSuzanne 
Window width             = 1024
Window height            = 768
Window title             = Direct3D App
Back-buffer count        = 3
Fill Triangles           = enabled
Triangles Font Clock-Wise= enabled
Fast Build AS            = disabled
Target FPS               = 60
Target UPS               = 60
Tearing                  = disabled
VSync                    = disabled
GUI rendering            = enabled
Debug layer              = enabled
GPU validation           = disabled
GPU profiling            = enabled
Render mode              = Rasterization
Loaded scene file        = Suzanne/Suzanne.gltf
Using testing scene      = disabled
Duplication              = 1
Duplication in cube      = disabled
Duplication offset       = 10
BLAS duplication         = disabled
Rays per pixel           = 10

ProfilingAggregator: 

ProfilingBuildAggregator: 
BuildBottomLevelAS	,	1.425	,	1.0312
BuildBottomLevelASGpu	,	0.349344	,	0.604032
BuildTopLevelAS	,	1.5531	,	1.1137
BuildTopLevelASGpu	,	0.083008	,	0.08336
Duplication	,	1	,	1
Triangles	,	3936	,	3936
Meshes	,	1	,	1
BottomLevels	,	1	,	1
Index	,	0	,	1

FpsAggregator: 
FPS	,	0	,	0
UPS	,	0	,	0
FrameTime	,	0	,	0
GigaRays/s	,	0	,	0
Index	,	0	,	1


AccelerationStructure: 
Ray tracing acceleration structure building statistics: 
	Bottom Level Acceleration Structure: 
		Maximum [B]: 224128
		Minimum [B]: 224128
		Currently Allocated [B]: 224128
		Currently Created [num]: 1
		Current Average [B]: 224128
		Maximum Triangles [num]: 3936
		Minimum Triangles [num]: 3936
		Total Triangles [num]: 3936
		Maximum Meshes [num]: 1
		Minimum Meshes [num]: 1
		Total Meshes [num]: 1
	Top Level Acceleration Structure: 
		Maximum [B]: 64
		Minimum [B]: 64
		Currently Allocated [B]: 64
		Total Allocated [B]: 64
		Total Created [num]: 1
		Average Allocated [B]: 64
		Maximum Instances [num]: 1
		Minimum Instances [num]: 1
		Total Instances [num]: 1
	Scratch Buffer: 
		Maximum [B]: 34304
		Minimum [B]: 34304
		Currently Allocated [B]: 34304
		Total Allocated [B]: 34304
		Total Created [num]: 1
		Average Allocated [B]: 34304
ProfilingStack: 
[PROF] Profiling statistics: 
	Scopes entered : 55
	Scopes exited : 54
	Overhead per scope [ticks] : 1009.2
	Threads entered : 3
	Threads exited : 0
	============================
		         Sum[Mt]   Parent[%]  Samples[num]       Avg[Mt]   Last/Curr[Mt]        Self[Mt]     Self[%] 	 Name

	main:

		     748.708 Mt   100.000 %         1 x      748.710 Mt      748.707 Mt        0.879 Mt     0.117 % 	 /

		     747.929 Mt    99.896 %         1 x      747.932 Mt      747.928 Mt      361.581 Mt    48.344 % 	 ./Main application loop

		       3.023 Mt     0.404 %         1 x        3.023 Mt        3.023 Mt        3.023 Mt   100.000 % 	 ../TexturedRLInitialization

		       7.122 Mt     0.952 %         1 x        7.122 Mt        7.122 Mt        7.122 Mt   100.000 % 	 ../DeferredRLInitialization

		      57.391 Mt     7.673 %         1 x       57.391 Mt       57.391 Mt        2.594 Mt     4.520 % 	 ../RayTracingRLInitialization

		      54.796 Mt    95.480 %         1 x       54.796 Mt       54.796 Mt       54.796 Mt   100.000 % 	 .../PipelineBuild

		       3.689 Mt     0.493 %         1 x        3.689 Mt        3.689 Mt        3.689 Mt   100.000 % 	 ../ResolveRLInitialization

		     270.386 Mt    36.151 %         1 x      270.386 Mt      270.386 Mt        8.706 Mt     3.220 % 	 ../Initialization

		     261.680 Mt    96.780 %         1 x      261.680 Mt      261.680 Mt        2.483 Mt     0.949 % 	 .../ScenePrep

		     221.508 Mt    84.649 %         1 x      221.508 Mt      221.508 Mt       21.431 Mt     9.675 % 	 ..../SceneLoad

		     166.083 Mt    74.978 %         1 x      166.083 Mt      166.083 Mt       20.328 Mt    12.240 % 	 ...../glTF-LoadScene

		     145.755 Mt    87.760 %         2 x       72.877 Mt       67.807 Mt      145.755 Mt   100.000 % 	 ....../glTF-LoadImageData

		      17.386 Mt     7.849 %         1 x       17.386 Mt       17.386 Mt       17.386 Mt   100.000 % 	 ...../glTF-CreateScene

		      16.608 Mt     7.498 %         1 x       16.608 Mt       16.608 Mt       16.608 Mt   100.000 % 	 ...../SceneDesc-CreateScene

		       2.318 Mt     0.886 %         1 x        2.318 Mt        2.318 Mt        0.005 Mt     0.216 % 	 ..../TexturedRLPrep

		       2.313 Mt    99.784 %         1 x        2.313 Mt        2.313 Mt        1.039 Mt    44.941 % 	 ...../PrepareForRendering

		       0.981 Mt    42.407 %         1 x        0.981 Mt        0.981 Mt        0.981 Mt   100.000 % 	 ....../PrepareMaterials

		       0.293 Mt    12.652 %         1 x        0.293 Mt        0.293 Mt        0.293 Mt   100.000 % 	 ....../PrepareDrawBundle

		       4.154 Mt     1.588 %         1 x        4.154 Mt        4.154 Mt        0.006 Mt     0.142 % 	 ..../DeferredRLPrep

		       4.148 Mt    99.858 %         1 x        4.148 Mt        4.148 Mt        2.459 Mt    59.282 % 	 ...../PrepareForRendering

		       0.016 Mt     0.383 %         1 x        0.016 Mt        0.016 Mt        0.016 Mt   100.000 % 	 ....../PrepareMaterials

		       0.823 Mt    19.832 %         1 x        0.823 Mt        0.823 Mt        0.823 Mt   100.000 % 	 ....../BuildMaterialCache

		       0.851 Mt    20.502 %         1 x        0.851 Mt        0.851 Mt        0.851 Mt   100.000 % 	 ....../PrepareDrawBundle

		      17.715 Mt     6.770 %         1 x       17.715 Mt       17.715 Mt        5.914 Mt    33.383 % 	 ..../RayTracingRLPrep

		       0.682 Mt     3.850 %         1 x        0.682 Mt        0.682 Mt        0.682 Mt   100.000 % 	 ...../PrepareAccelerationStructures

		       0.068 Mt     0.383 %         1 x        0.068 Mt        0.068 Mt        0.068 Mt   100.000 % 	 ...../PrepareCache

		       0.971 Mt     5.482 %         1 x        0.971 Mt        0.971 Mt        0.971 Mt   100.000 % 	 ...../BuildCache

		       7.560 Mt    42.674 %         1 x        7.560 Mt        7.560 Mt        0.004 Mt     0.050 % 	 ...../BuildAccelerationStructures

		       7.556 Mt    99.950 %         1 x        7.556 Mt        7.556 Mt        4.744 Mt    62.783 % 	 ....../AccelerationStructureBuild

		       1.272 Mt    16.829 %         1 x        1.272 Mt        1.272 Mt        1.272 Mt   100.000 % 	 ......./BuildBottomLevelAS

		       1.540 Mt    20.388 %         1 x        1.540 Mt        1.540 Mt        1.540 Mt   100.000 % 	 ......./BuildTopLevelAS

		       2.520 Mt    14.227 %         1 x        2.520 Mt        2.520 Mt        2.520 Mt   100.000 % 	 ...../BuildShaderTables

		      13.502 Mt     5.160 %         1 x       13.502 Mt       13.502 Mt       13.502 Mt   100.000 % 	 ..../GpuSidePrep

		      44.738 Mt     5.982 %         3 x       14.913 Mt        8.876 Mt       34.076 Mt    76.168 % 	 ../AccelerationStructureBuild

		       5.310 Mt    11.869 %         3 x        1.770 Mt        1.031 Mt        5.310 Mt   100.000 % 	 .../BuildBottomLevelAS

		       5.352 Mt    11.963 %         3 x        1.784 Mt        1.114 Mt        5.352 Mt   100.000 % 	 .../BuildTopLevelAS

	WindowThread:

		     735.261 Mt   100.000 %         1 x      735.262 Mt      735.261 Mt      735.261 Mt   100.000 % 	 /

	GpuThread:

		      79.867 Mt   100.000 %         2 x       39.933 Mt        0.689 Mt       77.864 Mt    97.493 % 	 /

		       0.446 Mt     0.559 %         1 x        0.446 Mt        0.446 Mt        0.006 Mt     1.426 % 	 ./ScenePrep

		       0.440 Mt    98.574 %         1 x        0.440 Mt        0.440 Mt        0.002 Mt     0.385 % 	 ../AccelerationStructureBuild

		       0.349 Mt    79.293 %         1 x        0.349 Mt        0.349 Mt        0.349 Mt   100.000 % 	 .../BuildBottomLevelASGpu

		       0.089 Mt    20.321 %         1 x        0.089 Mt        0.089 Mt        0.089 Mt   100.000 % 	 .../BuildTopLevelASGpu

		       1.556 Mt     1.948 %         3 x        0.519 Mt        0.689 Mt        0.004 Mt     0.249 % 	 ./AccelerationStructureBuild

		       1.303 Mt    83.724 %         3 x        0.434 Mt        0.604 Mt        1.303 Mt   100.000 % 	 ../BuildBottomLevelASGpu

		       0.249 Mt    16.027 %         3 x        0.083 Mt        0.083 Mt        0.249 Mt   100.000 % 	 ../BuildTopLevelASGpu


	============================


