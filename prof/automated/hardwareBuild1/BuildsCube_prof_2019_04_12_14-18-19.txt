Configuration: 
Command line parameters  = E:\programming\FIT-DIP\Masters-DXR-Main-git\build\Measurement\x64\Debug\Measurement.exe --scene Cube/Cube.gltf --profile-builds --profile-output BuildsCube 
Window width             = 1024
Window height            = 768
Window title             = Direct3D App
Back-buffer count        = 3
Fill Triangles           = enabled
Triangles Font Clock-Wise= enabled
Fast Build AS            = disabled
Target FPS               = 60
Target UPS               = 60
Tearing                  = disabled
VSync                    = disabled
GUI rendering            = enabled
Debug layer              = enabled
GPU validation           = disabled
GPU profiling            = enabled
Render mode              = Rasterization
Loaded scene file        = Cube/Cube.gltf
Using testing scene      = disabled
Duplication              = 1
Duplication in cube      = disabled
Duplication offset       = 10
BLAS duplication         = disabled
Rays per pixel           = 10

ProfilingAggregator: 

ProfilingBuildAggregator: 
BuildBottomLevelAS	,	1.532	,	1.4659
BuildBottomLevelASGpu	,	0.119072	,	0.160576
BuildTopLevelAS	,	0.9368	,	0.946
BuildTopLevelASGpu	,	0.083264	,	0.080768
Duplication	,	1	,	1
Triangles	,	12	,	12
Meshes	,	1	,	1
BottomLevels	,	1	,	1
Index	,	0	,	1

FpsAggregator: 
FPS	,	0	,	0
UPS	,	0	,	0
FrameTime	,	0	,	0
GigaRays/s	,	0	,	0
Index	,	0	,	1


AccelerationStructure: 
Ray tracing acceleration structure building statistics: 
	Bottom Level Acceleration Structure: 
		Maximum [B]: 5168
		Minimum [B]: 5168
		Currently Allocated [B]: 5168
		Currently Created [num]: 1
		Current Average [B]: 5168
		Maximum Triangles [num]: 12
		Minimum Triangles [num]: 12
		Total Triangles [num]: 12
		Maximum Meshes [num]: 1
		Minimum Meshes [num]: 1
		Total Meshes [num]: 1
	Top Level Acceleration Structure: 
		Maximum [B]: 64
		Minimum [B]: 64
		Currently Allocated [B]: 64
		Total Allocated [B]: 64
		Total Created [num]: 1
		Average Allocated [B]: 64
		Maximum Instances [num]: 1
		Minimum Instances [num]: 1
		Total Instances [num]: 1
	Scratch Buffer: 
		Maximum [B]: 3584
		Minimum [B]: 3584
		Currently Allocated [B]: 3584
		Total Allocated [B]: 3584
		Total Created [num]: 1
		Average Allocated [B]: 3584
ProfilingStack: 
[PROF] Profiling statistics: 
	Scopes entered : 55
	Scopes exited : 54
	Overhead per scope [ticks] : 1009.3
	Threads entered : 3
	Threads exited : 0
	============================
		         Sum[Mt]   Parent[%]  Samples[num]       Avg[Mt]   Last/Curr[Mt]        Self[Mt]     Self[%] 	 Name

	main:

		     651.266 Mt   100.000 %         1 x      651.269 Mt      651.265 Mt        0.750 Mt     0.115 % 	 /

		     650.619 Mt    99.901 %         1 x      650.623 Mt      650.618 Mt      341.403 Mt    52.474 % 	 ./Main application loop

		       2.940 Mt     0.452 %         1 x        2.940 Mt        2.940 Mt        2.940 Mt   100.000 % 	 ../TexturedRLInitialization

		       6.027 Mt     0.926 %         1 x        6.027 Mt        6.027 Mt        6.027 Mt   100.000 % 	 ../DeferredRLInitialization

		     103.670 Mt    15.934 %         1 x      103.670 Mt      103.670 Mt        3.450 Mt     3.327 % 	 ../RayTracingRLInitialization

		     100.221 Mt    96.673 %         1 x      100.221 Mt      100.221 Mt      100.221 Mt   100.000 % 	 .../PipelineBuild

		       4.257 Mt     0.654 %         1 x        4.257 Mt        4.257 Mt        4.257 Mt   100.000 % 	 ../ResolveRLInitialization

		     158.426 Mt    24.350 %         1 x      158.426 Mt      158.426 Mt        5.760 Mt     3.636 % 	 ../Initialization

		     152.665 Mt    96.364 %         1 x      152.665 Mt      152.665 Mt        2.999 Mt     1.964 % 	 .../ScenePrep

		     108.003 Mt    70.745 %         1 x      108.003 Mt      108.003 Mt       14.180 Mt    13.129 % 	 ..../SceneLoad

		      59.897 Mt    55.459 %         1 x       59.897 Mt       59.897 Mt       14.690 Mt    24.525 % 	 ...../glTF-LoadScene

		      45.208 Mt    75.475 %         2 x       22.604 Mt        2.350 Mt       45.208 Mt   100.000 % 	 ....../glTF-LoadImageData

		      15.449 Mt    14.305 %         1 x       15.449 Mt       15.449 Mt       15.449 Mt   100.000 % 	 ...../glTF-CreateScene

		      18.477 Mt    17.108 %         1 x       18.477 Mt       18.477 Mt       18.477 Mt   100.000 % 	 ...../SceneDesc-CreateScene

		       5.244 Mt     3.435 %         1 x        5.244 Mt        5.244 Mt        0.016 Mt     0.305 % 	 ..../TexturedRLPrep

		       5.228 Mt    99.695 %         1 x        5.228 Mt        5.228 Mt        2.908 Mt    55.622 % 	 ...../PrepareForRendering

		       1.357 Mt    25.962 %         1 x        1.357 Mt        1.357 Mt        1.357 Mt   100.000 % 	 ....../PrepareMaterials

		       0.963 Mt    18.416 %         1 x        0.963 Mt        0.963 Mt        0.963 Mt   100.000 % 	 ....../PrepareDrawBundle

		       4.075 Mt     2.669 %         1 x        4.075 Mt        4.075 Mt        0.015 Mt     0.366 % 	 ..../DeferredRLPrep

		       4.060 Mt    99.634 %         1 x        4.060 Mt        4.060 Mt        2.650 Mt    65.283 % 	 ...../PrepareForRendering

		       0.053 Mt     1.318 %         1 x        0.053 Mt        0.053 Mt        0.053 Mt   100.000 % 	 ....../PrepareMaterials

		       1.023 Mt    25.204 %         1 x        1.023 Mt        1.023 Mt        1.023 Mt   100.000 % 	 ....../BuildMaterialCache

		       0.333 Mt     8.195 %         1 x        0.333 Mt        0.333 Mt        0.333 Mt   100.000 % 	 ....../PrepareDrawBundle

		      19.836 Mt    12.993 %         1 x       19.836 Mt       19.836 Mt        5.067 Mt    25.543 % 	 ..../RayTracingRLPrep

		       0.298 Mt     1.502 %         1 x        0.298 Mt        0.298 Mt        0.298 Mt   100.000 % 	 ...../PrepareAccelerationStructures

		       0.030 Mt     0.150 %         1 x        0.030 Mt        0.030 Mt        0.030 Mt   100.000 % 	 ...../PrepareCache

		       1.113 Mt     5.610 %         1 x        1.113 Mt        1.113 Mt        1.113 Mt   100.000 % 	 ...../BuildCache

		       7.165 Mt    36.121 %         1 x        7.165 Mt        7.165 Mt        0.005 Mt     0.068 % 	 ...../BuildAccelerationStructures

		       7.160 Mt    99.932 %         1 x        7.160 Mt        7.160 Mt        4.782 Mt    66.792 % 	 ....../AccelerationStructureBuild

		       1.287 Mt    17.970 %         1 x        1.287 Mt        1.287 Mt        1.287 Mt   100.000 % 	 ......./BuildBottomLevelAS

		       1.091 Mt    15.237 %         1 x        1.091 Mt        1.091 Mt        1.091 Mt   100.000 % 	 ......./BuildTopLevelAS

		       6.164 Mt    31.074 %         1 x        6.164 Mt        6.164 Mt        6.164 Mt   100.000 % 	 ...../BuildShaderTables

		      12.509 Mt     8.194 %         1 x       12.509 Mt       12.509 Mt       12.509 Mt   100.000 % 	 ..../GpuSidePrep

		      33.896 Mt     5.210 %         3 x       11.299 Mt        6.505 Mt       24.294 Mt    71.673 % 	 ../AccelerationStructureBuild

		       5.383 Mt    15.881 %         3 x        1.794 Mt        1.466 Mt        5.383 Mt   100.000 % 	 .../BuildBottomLevelAS

		       4.218 Mt    12.446 %         3 x        1.406 Mt        0.946 Mt        4.218 Mt   100.000 % 	 .../BuildTopLevelAS

	WindowThread:

		     647.672 Mt   100.000 %         1 x      647.673 Mt      647.671 Mt      647.672 Mt   100.000 % 	 /

	GpuThread:

		      63.107 Mt   100.000 %         2 x       31.554 Mt        0.243 Mt       62.241 Mt    98.627 % 	 /

		       0.217 Mt     0.343 %         1 x        0.217 Mt        0.217 Mt        0.007 Mt     3.015 % 	 ./ScenePrep

		       0.210 Mt    96.985 %         1 x        0.210 Mt        0.210 Mt        0.002 Mt     0.869 % 	 ../AccelerationStructureBuild

		       0.121 Mt    57.787 %         1 x        0.121 Mt        0.121 Mt        0.121 Mt   100.000 % 	 .../BuildBottomLevelASGpu

		       0.087 Mt    41.344 %         1 x        0.087 Mt        0.087 Mt        0.087 Mt   100.000 % 	 .../BuildTopLevelASGpu

		       0.650 Mt     1.030 %         3 x        0.217 Mt        0.243 Mt        0.004 Mt     0.615 % 	 ./AccelerationStructureBuild

		       0.399 Mt    61.340 %         3 x        0.133 Mt        0.161 Mt        0.399 Mt   100.000 % 	 ../BuildBottomLevelASGpu

		       0.247 Mt    38.045 %         3 x        0.082 Mt        0.081 Mt        0.247 Mt   100.000 % 	 ../BuildTopLevelASGpu


	============================


