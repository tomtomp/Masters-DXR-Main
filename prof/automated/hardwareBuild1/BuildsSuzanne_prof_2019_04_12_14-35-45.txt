Configuration: 
Command line parameters  = E:\programming\FIT-DIP\Masters-DXR-Main-git\build\Measurement\x64\Debug\Measurement.exe --scene Suzanne/Suzanne.gltf --profile-builds --profile-output BuildsSuzanne 
Window width             = 1024
Window height            = 768
Window title             = Direct3D App
Back-buffer count        = 3
Fill Triangles           = enabled
Triangles Font Clock-Wise= enabled
Fast Build AS            = disabled
Target FPS               = 60
Target UPS               = 60
Tearing                  = disabled
VSync                    = disabled
GUI rendering            = enabled
Debug layer              = enabled
GPU validation           = disabled
GPU profiling            = enabled
Render mode              = Rasterization
Loaded scene file        = Suzanne/Suzanne.gltf
Using testing scene      = disabled
Duplication              = 1
Duplication in cube      = disabled
Duplication offset       = 10
BLAS duplication         = disabled
Rays per pixel           = 10

ProfilingAggregator: 

ProfilingBuildAggregator: 
BuildBottomLevelAS	,	3.5034	,	0.6573
BuildBottomLevelASGpu	,	0.347936	,	0.595136
BuildTopLevelAS	,	0.8643	,	1.4951
BuildTopLevelASGpu	,	0.083072	,	0.080896
Duplication	,	1	,	1
Triangles	,	3936	,	3936
Meshes	,	1	,	1
BottomLevels	,	1	,	1
Index	,	0	,	1

FpsAggregator: 
FPS	,	0	,	0
UPS	,	0	,	0
FrameTime	,	0	,	0
GigaRays/s	,	0	,	0
Index	,	0	,	1


AccelerationStructure: 
Ray tracing acceleration structure building statistics: 
	Bottom Level Acceleration Structure: 
		Maximum [B]: 224128
		Minimum [B]: 224128
		Currently Allocated [B]: 224128
		Currently Created [num]: 1
		Current Average [B]: 224128
		Maximum Triangles [num]: 3936
		Minimum Triangles [num]: 3936
		Total Triangles [num]: 3936
		Maximum Meshes [num]: 1
		Minimum Meshes [num]: 1
		Total Meshes [num]: 1
	Top Level Acceleration Structure: 
		Maximum [B]: 64
		Minimum [B]: 64
		Currently Allocated [B]: 64
		Total Allocated [B]: 64
		Total Created [num]: 1
		Average Allocated [B]: 64
		Maximum Instances [num]: 1
		Minimum Instances [num]: 1
		Total Instances [num]: 1
	Scratch Buffer: 
		Maximum [B]: 34304
		Minimum [B]: 34304
		Currently Allocated [B]: 34304
		Total Allocated [B]: 34304
		Total Created [num]: 1
		Average Allocated [B]: 34304
ProfilingStack: 
[PROF] Profiling statistics: 
	Scopes entered : 55
	Scopes exited : 54
	Overhead per scope [ticks] : 1019.6
	Threads entered : 3
	Threads exited : 0
	============================
		         Sum[Mt]   Parent[%]  Samples[num]       Avg[Mt]   Last/Curr[Mt]        Self[Mt]     Self[%] 	 Name

	main:

		     742.797 Mt   100.000 %         1 x      742.799 Mt      742.796 Mt        0.767 Mt     0.103 % 	 /

		     742.104 Mt    99.907 %         1 x      742.106 Mt      742.104 Mt      352.411 Mt    47.488 % 	 ./Main application loop

		       2.942 Mt     0.396 %         1 x        2.942 Mt        2.942 Mt        2.942 Mt   100.000 % 	 ../TexturedRLInitialization

		       9.139 Mt     1.232 %         1 x        9.139 Mt        9.139 Mt        9.139 Mt   100.000 % 	 ../DeferredRLInitialization

		      69.199 Mt     9.325 %         1 x       69.199 Mt       69.199 Mt        2.512 Mt     3.630 % 	 ../RayTracingRLInitialization

		      66.687 Mt    96.370 %         1 x       66.687 Mt       66.687 Mt       66.687 Mt   100.000 % 	 .../PipelineBuild

		       3.374 Mt     0.455 %         1 x        3.374 Mt        3.374 Mt        3.374 Mt   100.000 % 	 ../ResolveRLInitialization

		     276.003 Mt    37.192 %         1 x      276.003 Mt      276.003 Mt        3.953 Mt     1.432 % 	 ../Initialization

		     272.050 Mt    98.568 %         1 x      272.050 Mt      272.050 Mt        1.861 Mt     0.684 % 	 .../ScenePrep

		     227.685 Mt    83.692 %         1 x      227.685 Mt      227.685 Mt       20.875 Mt     9.168 % 	 ..../SceneLoad

		     166.065 Mt    72.937 %         1 x      166.065 Mt      166.065 Mt       20.787 Mt    12.517 % 	 ...../glTF-LoadScene

		     145.278 Mt    87.483 %         2 x       72.639 Mt       67.753 Mt      145.278 Mt   100.000 % 	 ....../glTF-LoadImageData

		      24.410 Mt    10.721 %         1 x       24.410 Mt       24.410 Mt       24.410 Mt   100.000 % 	 ...../glTF-CreateScene

		      16.335 Mt     7.174 %         1 x       16.335 Mt       16.335 Mt       16.335 Mt   100.000 % 	 ...../SceneDesc-CreateScene

		       3.630 Mt     1.334 %         1 x        3.630 Mt        3.630 Mt        0.005 Mt     0.132 % 	 ..../TexturedRLPrep

		       3.625 Mt    99.868 %         1 x        3.625 Mt        3.625 Mt        0.959 Mt    26.464 % 	 ...../PrepareForRendering

		       2.373 Mt    65.470 %         1 x        2.373 Mt        2.373 Mt        2.373 Mt   100.000 % 	 ....../PrepareMaterials

		       0.292 Mt     8.066 %         1 x        0.292 Mt        0.292 Mt        0.292 Mt   100.000 % 	 ....../PrepareDrawBundle

		       4.663 Mt     1.714 %         1 x        4.663 Mt        4.663 Mt        0.006 Mt     0.120 % 	 ..../DeferredRLPrep

		       4.657 Mt    99.880 %         1 x        4.657 Mt        4.657 Mt        2.772 Mt    59.519 % 	 ...../PrepareForRendering

		       0.016 Mt     0.350 %         1 x        0.016 Mt        0.016 Mt        0.016 Mt   100.000 % 	 ....../PrepareMaterials

		       0.989 Mt    21.245 %         1 x        0.989 Mt        0.989 Mt        0.989 Mt   100.000 % 	 ....../BuildMaterialCache

		       0.879 Mt    18.886 %         1 x        0.879 Mt        0.879 Mt        0.879 Mt   100.000 % 	 ....../PrepareDrawBundle

		      21.085 Mt     7.750 %         1 x       21.085 Mt       21.085 Mt        5.930 Mt    28.125 % 	 ..../RayTracingRLPrep

		       0.755 Mt     3.581 %         1 x        0.755 Mt        0.755 Mt        0.755 Mt   100.000 % 	 ...../PrepareAccelerationStructures

		       0.076 Mt     0.361 %         1 x        0.076 Mt        0.076 Mt        0.076 Mt   100.000 % 	 ...../PrepareCache

		       1.012 Mt     4.800 %         1 x        1.012 Mt        1.012 Mt        1.012 Mt   100.000 % 	 ...../BuildCache

		       7.102 Mt    33.681 %         1 x        7.102 Mt        7.102 Mt        0.004 Mt     0.055 % 	 ...../BuildAccelerationStructures

		       7.098 Mt    99.945 %         1 x        7.098 Mt        7.098 Mt        4.779 Mt    67.329 % 	 ....../AccelerationStructureBuild

		       1.281 Mt    18.049 %         1 x        1.281 Mt        1.281 Mt        1.281 Mt   100.000 % 	 ......./BuildBottomLevelAS

		       1.038 Mt    14.621 %         1 x        1.038 Mt        1.038 Mt        1.038 Mt   100.000 % 	 ......./BuildTopLevelAS

		       6.210 Mt    29.452 %         1 x        6.210 Mt        6.210 Mt        6.210 Mt   100.000 % 	 ...../BuildShaderTables

		      13.127 Mt     4.825 %         1 x       13.127 Mt       13.127 Mt       13.127 Mt   100.000 % 	 ..../GpuSidePrep

		      29.037 Mt     3.913 %         3 x        9.679 Mt        4.446 Mt       18.055 Mt    62.179 % 	 ../AccelerationStructureBuild

		       6.903 Mt    23.774 %         3 x        2.301 Mt        0.657 Mt        6.903 Mt   100.000 % 	 .../BuildBottomLevelAS

		       4.079 Mt    14.047 %         3 x        1.360 Mt        1.495 Mt        4.079 Mt   100.000 % 	 .../BuildTopLevelAS

	WindowThread:

		     733.032 Mt   100.000 %         1 x      733.033 Mt      733.032 Mt      733.032 Mt   100.000 % 	 /

	GpuThread:

		      59.969 Mt   100.000 %         2 x       29.985 Mt        0.677 Mt       57.980 Mt    96.683 % 	 /

		       0.447 Mt     0.746 %         1 x        0.447 Mt        0.447 Mt        0.006 Mt     1.445 % 	 ./ScenePrep

		       0.441 Mt    98.555 %         1 x        0.441 Mt        0.441 Mt        0.001 Mt     0.334 % 	 ../AccelerationStructureBuild

		       0.353 Mt    79.997 %         1 x        0.353 Mt        0.353 Mt        0.353 Mt   100.000 % 	 .../BuildBottomLevelASGpu

		       0.087 Mt    19.669 %         1 x        0.087 Mt        0.087 Mt        0.087 Mt   100.000 % 	 .../BuildTopLevelASGpu

		       1.542 Mt     2.571 %         3 x        0.514 Mt        0.677 Mt        0.004 Mt     0.257 % 	 ./AccelerationStructureBuild

		       1.291 Mt    83.722 %         3 x        0.430 Mt        0.595 Mt        1.291 Mt   100.000 % 	 ../BuildBottomLevelASGpu

		       0.247 Mt    16.021 %         3 x        0.082 Mt        0.081 Mt        0.247 Mt   100.000 % 	 ../BuildTopLevelASGpu


	============================


