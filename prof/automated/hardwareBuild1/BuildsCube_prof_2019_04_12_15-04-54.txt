Configuration: 
Command line parameters  = E:\programming\FIT-DIP\Masters-DXR-Main-git\build\Measurement\x64\Debug\Measurement.exe --scene Cube/Cube.gltf --profile-builds --profile-output BuildsCube 
Window width             = 1024
Window height            = 768
Window title             = Direct3D App
Back-buffer count        = 3
Fill Triangles           = enabled
Triangles Font Clock-Wise= enabled
Fast Build AS            = disabled
Target FPS               = 60
Target UPS               = 60
Tearing                  = disabled
VSync                    = disabled
GUI rendering            = enabled
Debug layer              = enabled
GPU validation           = disabled
GPU profiling            = enabled
Render mode              = Rasterization
Loaded scene file        = Cube/Cube.gltf
Using testing scene      = disabled
Duplication              = 1
Duplication in cube      = disabled
Duplication offset       = 10
BLAS duplication         = disabled
Rays per pixel           = 10

ProfilingAggregator: 

ProfilingBuildAggregator: 
BuildBottomLevelAS	,	0.6848	,	0.6163
BuildBottomLevelASGpu	,	0.10608	,	0.17216
BuildTopLevelAS	,	1.1458	,	0.8858
BuildTopLevelASGpu	,	0.078016	,	0.084672
Duplication	,	1	,	1
Triangles	,	12	,	12
Meshes	,	1	,	1
BottomLevels	,	1	,	1
Index	,	0	,	1

FpsAggregator: 
FPS	,	0	,	0
UPS	,	0	,	0
FrameTime	,	0	,	0
GigaRays/s	,	0	,	0
Index	,	0	,	1


AccelerationStructure: 
Ray tracing acceleration structure building statistics: 
	Bottom Level Acceleration Structure: 
		Maximum [B]: 5168
		Minimum [B]: 5168
		Currently Allocated [B]: 5168
		Currently Created [num]: 1
		Current Average [B]: 5168
		Maximum Triangles [num]: 12
		Minimum Triangles [num]: 12
		Total Triangles [num]: 12
		Maximum Meshes [num]: 1
		Minimum Meshes [num]: 1
		Total Meshes [num]: 1
	Top Level Acceleration Structure: 
		Maximum [B]: 64
		Minimum [B]: 64
		Currently Allocated [B]: 64
		Total Allocated [B]: 64
		Total Created [num]: 1
		Average Allocated [B]: 64
		Maximum Instances [num]: 1
		Minimum Instances [num]: 1
		Total Instances [num]: 1
	Scratch Buffer: 
		Maximum [B]: 3584
		Minimum [B]: 3584
		Currently Allocated [B]: 3584
		Total Allocated [B]: 3584
		Total Created [num]: 1
		Average Allocated [B]: 3584
ProfilingStack: 
[PROF] Profiling statistics: 
	Scopes entered : 55
	Scopes exited : 54
	Overhead per scope [ticks] : 1005.6
	Threads entered : 3
	Threads exited : 0
	============================
		         Sum[Mt]   Parent[%]  Samples[num]       Avg[Mt]   Last/Curr[Mt]        Self[Mt]     Self[%] 	 Name

	main:

		     561.855 Mt   100.000 %         1 x      561.858 Mt      561.854 Mt        0.838 Mt     0.149 % 	 /

		     561.114 Mt    99.868 %         1 x      561.117 Mt      561.114 Mt      328.720 Mt    58.583 % 	 ./Main application loop

		       2.899 Mt     0.517 %         1 x        2.899 Mt        2.899 Mt        2.899 Mt   100.000 % 	 ../TexturedRLInitialization

		       5.526 Mt     0.985 %         1 x        5.526 Mt        5.526 Mt        5.526 Mt   100.000 % 	 ../DeferredRLInitialization

		      54.985 Mt     9.799 %         1 x       54.985 Mt       54.985 Mt        2.123 Mt     3.861 % 	 ../RayTracingRLInitialization

		      52.862 Mt    96.139 %         1 x       52.862 Mt       52.862 Mt       52.862 Mt   100.000 % 	 .../PipelineBuild

		       3.563 Mt     0.635 %         1 x        3.563 Mt        3.563 Mt        3.563 Mt   100.000 % 	 ../ResolveRLInitialization

		     141.604 Mt    25.236 %         1 x      141.604 Mt      141.604 Mt        3.637 Mt     2.569 % 	 ../Initialization

		     137.967 Mt    97.431 %         1 x      137.967 Mt      137.967 Mt        3.103 Mt     2.249 % 	 .../ScenePrep

		     102.591 Mt    74.359 %         1 x      102.591 Mt      102.591 Mt       16.188 Mt    15.779 % 	 ..../SceneLoad

		      58.167 Mt    56.698 %         1 x       58.167 Mt       58.167 Mt       13.404 Mt    23.044 % 	 ...../glTF-LoadScene

		      44.763 Mt    76.956 %         2 x       22.381 Mt        2.349 Mt       44.763 Mt   100.000 % 	 ....../glTF-LoadImageData

		      12.533 Mt    12.216 %         1 x       12.533 Mt       12.533 Mt       12.533 Mt   100.000 % 	 ...../glTF-CreateScene

		      15.703 Mt    15.307 %         1 x       15.703 Mt       15.703 Mt       15.703 Mt   100.000 % 	 ...../SceneDesc-CreateScene

		       2.432 Mt     1.762 %         1 x        2.432 Mt        2.432 Mt        0.005 Mt     0.214 % 	 ..../TexturedRLPrep

		       2.426 Mt    99.786 %         1 x        2.426 Mt        2.426 Mt        0.870 Mt    35.843 % 	 ...../PrepareForRendering

		       1.249 Mt    51.484 %         1 x        1.249 Mt        1.249 Mt        1.249 Mt   100.000 % 	 ....../PrepareMaterials

		       0.307 Mt    12.673 %         1 x        0.307 Mt        0.307 Mt        0.307 Mt   100.000 % 	 ....../PrepareDrawBundle

		       3.872 Mt     2.807 %         1 x        3.872 Mt        3.872 Mt        0.005 Mt     0.127 % 	 ..../DeferredRLPrep

		       3.867 Mt    99.873 %         1 x        3.867 Mt        3.867 Mt        2.632 Mt    68.055 % 	 ...../PrepareForRendering

		       0.016 Mt     0.421 %         1 x        0.016 Mt        0.016 Mt        0.016 Mt   100.000 % 	 ....../PrepareMaterials

		       0.936 Mt    24.213 %         1 x        0.936 Mt        0.936 Mt        0.936 Mt   100.000 % 	 ....../BuildMaterialCache

		       0.283 Mt     7.310 %         1 x        0.283 Mt        0.283 Mt        0.283 Mt   100.000 % 	 ....../PrepareDrawBundle

		      14.686 Mt    10.645 %         1 x       14.686 Mt       14.686 Mt        4.670 Mt    31.800 % 	 ..../RayTracingRLPrep

		       0.237 Mt     1.616 %         1 x        0.237 Mt        0.237 Mt        0.237 Mt   100.000 % 	 ...../PrepareAccelerationStructures

		       0.024 Mt     0.161 %         1 x        0.024 Mt        0.024 Mt        0.024 Mt   100.000 % 	 ...../PrepareCache

		       0.921 Mt     6.272 %         1 x        0.921 Mt        0.921 Mt        0.921 Mt   100.000 % 	 ...../BuildCache

		       6.378 Mt    43.427 %         1 x        6.378 Mt        6.378 Mt        0.005 Mt     0.072 % 	 ...../BuildAccelerationStructures

		       6.373 Mt    99.928 %         1 x        6.373 Mt        6.373 Mt        4.199 Mt    65.887 % 	 ....../AccelerationStructureBuild

		       1.230 Mt    19.293 %         1 x        1.230 Mt        1.230 Mt        1.230 Mt   100.000 % 	 ......./BuildBottomLevelAS

		       0.945 Mt    14.820 %         1 x        0.945 Mt        0.945 Mt        0.945 Mt   100.000 % 	 ......./BuildTopLevelAS

		       2.456 Mt    16.722 %         1 x        2.456 Mt        2.456 Mt        2.456 Mt   100.000 % 	 ...../BuildShaderTables

		      11.283 Mt     8.178 %         1 x       11.283 Mt       11.283 Mt       11.283 Mt   100.000 % 	 ..../GpuSidePrep

		      23.817 Mt     4.245 %         3 x        7.939 Mt        6.999 Mt       16.331 Mt    68.569 % 	 ../AccelerationStructureBuild

		       4.121 Mt    17.304 %         3 x        1.374 Mt        0.616 Mt        4.121 Mt   100.000 % 	 .../BuildBottomLevelAS

		       3.365 Mt    14.127 %         3 x        1.122 Mt        0.886 Mt        3.365 Mt   100.000 % 	 .../BuildTopLevelAS

	WindowThread:

		     559.039 Mt   100.000 %         1 x      559.039 Mt      559.038 Mt      559.039 Mt   100.000 % 	 /

	GpuThread:

		      47.358 Mt   100.000 %         2 x       23.679 Mt        0.258 Mt       46.517 Mt    98.225 % 	 /

		       0.211 Mt     0.447 %         1 x        0.211 Mt        0.211 Mt        0.007 Mt     3.193 % 	 ./ScenePrep

		       0.205 Mt    96.807 %         1 x        0.205 Mt        0.205 Mt        0.001 Mt     0.422 % 	 ../AccelerationStructureBuild

		       0.121 Mt    59.059 %         1 x        0.121 Mt        0.121 Mt        0.121 Mt   100.000 % 	 .../BuildBottomLevelASGpu

		       0.083 Mt    40.519 %         1 x        0.083 Mt        0.083 Mt        0.083 Mt   100.000 % 	 .../BuildTopLevelASGpu

		       0.629 Mt     1.328 %         3 x        0.210 Mt        0.258 Mt        0.004 Mt     0.626 % 	 ./AccelerationStructureBuild

		       0.384 Mt    61.104 %         3 x        0.128 Mt        0.172 Mt        0.384 Mt   100.000 % 	 ../BuildBottomLevelASGpu

		       0.241 Mt    38.270 %         3 x        0.080 Mt        0.085 Mt        0.241 Mt   100.000 % 	 ../BuildTopLevelASGpu


	============================


