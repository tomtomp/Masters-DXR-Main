Configuration: 
Command line parameters  = 
Window width             = 1186
Window height            = 1219
Window title             = Direct3D App
Back-buffer count        = 3
Fill Triangles           = enabled
Triangles Font Clock-Wise= disabled
Fast Build AS            = disabled
Target FPS               = 0
Target UPS               = 60
Tearing                  = enabled
VSync                    = disabled
GUI rendering            = disabled
Debug layer              = enabled
GPU validation           = disabled
GPU profiling            = enabled
Render mode              = Ray Tracing
Loaded scene file        = 
Using testing scene      = disabled
BLAS duplication         = disabled
Rays per pixel           = 9
Quality preset           = Medium
Rasterization Quality    : 
  AO                     = enabled
  AO kernel size         = 4
  Shadows                = enabled
  Shadow map resolution  = 4096
  Shadow PCF kernel size = 4
  Reflections            = disabled
Ray Tracing Quality      : 
  AO                     = enabled
  AO rays                = 4
  AO kernel size         = 4
  Shadows                = enabled
  Shadow rays            = 4
  Shadow kernel size     = 4
  Reflections            = disabled

ProfilingAggregator: 
Render	,	15.034	,	15.088	,	17.7085	,	15.2329	,	15.6794	,	15.9292	,	15.1399	,	15.5138	,	15.8339	,	26.5558	,	15.799	,	17.4812	,	21.6854	,	15.6739	,	16.3442	,	14.9245	,	15.9367	,	15.3112	,	15.0381	,	17.1223	,	21.1916	,	15.4861	,	14.8837	,	15.3141	,	15.552	,	15.176	,	15.7597	,	14.9935	,	15.5697	,	15.2091	,	15.642
Update	,	0.0827	,	0.0811	,	0.0243	,	0.0248	,	0.0827	,	0.0835	,	0.0819	,	0.0847	,	0.0237	,	0.0827	,	0.0256	,	0.0993	,	0.0831	,	0.0246	,	0.0264	,	0.0248	,	0.0227	,	0.0821	,	0.0832	,	0.0829	,	0.0803	,	0.0799	,	0.0815	,	0.0852	,	0.0809	,	0.0244	,	0.0798	,	0.0793	,	0.0988	,	0.081	,	0.0833
RayTracing	,	6.9101	,	6.9951	,	9.4006	,	6.8452	,	7.5936	,	7.6829	,	6.9983	,	6.995	,	7.2475	,	13.6213	,	7.2747	,	8.7878	,	11.5893	,	6.8898	,	8.1126	,	6.8941	,	7.7805	,	7.1201	,	6.9651	,	8.4093	,	10.6589	,	6.9939	,	7.0139	,	6.912	,	7.2521	,	7.0746	,	7.706	,	7.0153	,	6.8413	,	7.0602	,	7.5148
RayTracingGpu	,	2.39418	,	2.38208	,	2.26989	,	2.4105	,	2.37498	,	1.90842	,	2.30554	,	2.90992	,	2.71926	,	2.41872	,	2.65389	,	2.26304	,	2.25312	,	2.26515	,	2.25514	,	2.24304	,	2.27584	,	2.47222	,	2.23488	,	2.39664	,	2.54	,	2.39603	,	2.15104	,	2.1584	,	2.16499	,	2.41226	,	2.1592	,	2.16256	,	2.41747	,	2.16256	,	2.3095
DeferredRLGpu	,	0.233024	,	0.272608	,	0.258208	,	0.265824	,	0.253504	,	0.159712	,	0.294624	,	0.387296	,	0.368288	,	0.328096	,	0.263264	,	0.243968	,	0.238976	,	0.238368	,	0.23824	,	0.236672	,	0.24112	,	0.239136	,	0.2352	,	0.305472	,	0.295936	,	0.228672	,	0.22544	,	0.228736	,	0.228128	,	0.23024	,	0.232032	,	0.23136	,	0.226816	,	0.228352	,	0.226688
RayTracingRLGpu	,	1.09859	,	1.21053	,	1.11555	,	1.24269	,	1.20922	,	0.871616	,	1.12934	,	1.61779	,	1.44035	,	1.18662	,	1.48234	,	1.11184	,	1.11613	,	1.11878	,	1.11222	,	1.1113	,	1.12445	,	1.3327	,	1.09683	,	1.19613	,	1.34058	,	1.03978	,	1.03056	,	1.03907	,	1.03706	,	1.27834	,	1.03315	,	1.03507	,	1.28298	,	1.03763	,	1.03078
ResolveRLGpu	,	1.05936	,	0.8952	,	0.892448	,	0.899264	,	0.909984	,	0.87504	,	0.87936	,	0.903136	,	0.907008	,	0.900704	,	0.905888	,	0.90496	,	0.894688	,	0.905248	,	0.901824	,	0.892512	,	0.907072	,	0.897824	,	0.900864	,	0.891968	,	0.900064	,	1.12448	,	0.892576	,	0.888672	,	0.896384	,	0.896192	,	0.892096	,	0.894816	,	0.900608	,	0.894464	,	1.04986
Index	,	0	,	1	,	2	,	3	,	4	,	5	,	6	,	7	,	8	,	9	,	10	,	11	,	12	,	13	,	14	,	15	,	16	,	17	,	18	,	19	,	20	,	21	,	22	,	23	,	24	,	25	,	26	,	27	,	28	,	29	,	30

ProfilingBuildAggregator: 
BuildBottomLevelAS	,	12.9951
BuildBottomLevelASGpu	,	24.5647
BuildTopLevelAS	,	0.6194
BuildTopLevelASGpu	,	0.311648
Triangles	,	281182
Meshes	,	52
BottomLevels	,	43
TopLevelSize	,	2752
BottomLevelsSize	,	15869344
Index	,	0

FpsAggregator: 
FPS	,	23	,	37	,	35	,	40	,	39	,	38	,	39	,	37	,	36	,	36	,	38	,	37	,	40	,	42	,	39	,	38	,	37	,	39	,	40	,	39	,	39	,	37	,	41	,	39	,	40	,	37	,	34	,	41	,	39	,	37	,	35
UPS	,	196	,	61	,	61	,	62	,	61	,	61	,	60	,	61	,	62	,	59	,	62	,	60	,	61	,	62	,	60	,	60	,	62	,	60	,	61	,	60	,	61	,	61	,	60	,	61	,	61	,	61	,	60	,	60	,	62	,	60	,	62
FrameTime	,	43.9772	,	27.0812	,	29.2763	,	25.5505	,	26.3032	,	26.6768	,	26.1267	,	27.358	,	27.9659	,	27.8684	,	26.8256	,	27.1716	,	25.7084	,	24.3003	,	25.8169	,	26.4241	,	27.5505	,	25.7777	,	25.3265	,	25.7906	,	26.0446	,	27.3257	,	24.5202	,	25.8129	,	25.6606	,	27.1421	,	29.7269	,	24.5268	,	26.1281	,	27.4093	,	29.1195
GigaRays/s	,	0.27464	,	0.445989	,	0.412549	,	0.472707	,	0.45918	,	0.452749	,	0.462282	,	0.441476	,	0.43188	,	0.43339	,	0.450238	,	0.444505	,	0.469804	,	0.497028	,	0.467829	,	0.457078	,	0.438392	,	0.46854	,	0.476888	,	0.468307	,	0.463739	,	0.441999	,	0.492569	,	0.467902	,	0.470679	,	0.444988	,	0.406296	,	0.492437	,	0.462258	,	0.44065	,	0.41477
Index	,	0	,	1	,	2	,	3	,	4	,	5	,	6	,	7	,	8	,	9	,	10	,	11	,	12	,	13	,	14	,	15	,	16	,	17	,	18	,	19	,	20	,	21	,	22	,	23	,	24	,	25	,	26	,	27	,	28	,	29	,	30


AccelerationStructure: 
Ray tracing acceleration structure building statistics: 
	Bottom Level Acceleration Structure: 
		Maximum [B]: 2940416
		Minimum [B]: 6256
		Currently Allocated [B]: 15869344
		Currently Created [num]: 43
		Current Average [B]: 369054
		Maximum Triangles [num]: 52674
		Minimum Triangles [num]: 27
		Total Triangles [num]: 281182
		Maximum Meshes [num]: 2
		Minimum Meshes [num]: 1
		Total Meshes [num]: 52
	Top Level Acceleration Structure: 
		Maximum [B]: 2752
		Minimum [B]: 2752
		Currently Allocated [B]: 2752
		Total Allocated [B]: 2752
		Total Created [num]: 1
		Average Allocated [B]: 2752
		Maximum Instances [num]: 43
		Minimum Instances [num]: 43
		Total Instances [num]: 43
	Scratch Buffer: 
		Maximum [B]: 2344448
		Minimum [B]: 2344448
		Currently Allocated [B]: 2344448
		Total Allocated [B]: 2344448
		Total Created [num]: 1
		Average Allocated [B]: 2344448
ProfilingStack: 
[PROF] Profiling statistics: 
	Scopes entered : 15750
	Scopes exited : 15750
	Overhead per scope [ticks] : 161.4
	Threads entered : 3
	Threads exited : 0
	============================
		         Sum[Mt]   Parent[%]  Samples[num]       Avg[Mt]   Last/Curr[Mt]        Self[Mt]     Self[%] 	 Name

	main:

		   35008.018 Mt   100.000 %         1 x    35008.019 Mt    35008.018 Mt    14422.069 Mt    41.196 % 	 /

		      61.791 Mt     0.177 %         1 x       61.791 Mt        0.000 Mt        0.611 Mt     0.989 % 	 ./Initialize

		       2.568 Mt     4.155 %         1 x        2.568 Mt        0.000 Mt        2.568 Mt   100.000 % 	 ../TexturedRLInitialization

		       1.286 Mt     2.081 %         1 x        1.286 Mt        0.000 Mt        1.286 Mt   100.000 % 	 ../ShadowMapRLInitialization

		       7.490 Mt    12.121 %         1 x        7.490 Mt        0.000 Mt        7.490 Mt   100.000 % 	 ../AmbientOcclusionRLInitialization

		       4.853 Mt     7.853 %         1 x        4.853 Mt        0.000 Mt        4.853 Mt   100.000 % 	 ../RasterResolveRLInitialization

		       4.038 Mt     6.534 %         1 x        4.038 Mt        0.000 Mt        4.038 Mt   100.000 % 	 ../DeferredRLInitialization

		      36.853 Mt    59.642 %         1 x       36.853 Mt        0.000 Mt        1.254 Mt     3.403 % 	 ../RayTracingRLInitialization

		      35.599 Mt    96.597 %         1 x       35.599 Mt        0.000 Mt       35.599 Mt   100.000 % 	 .../PipelineBuild

		       4.093 Mt     6.624 %         1 x        4.093 Mt        0.000 Mt        4.093 Mt   100.000 % 	 ../ResolveRLInitialization

		   20524.159 Mt    58.627 %      1207 x       17.004 Mt       16.331 Mt       50.288 Mt     0.245 % 	 ./Run

		     453.628 Mt     2.210 %      2088 x        0.217 Mt        0.027 Mt      303.122 Mt    66.822 % 	 ../Update

		      78.277 Mt    17.256 %         1 x       78.277 Mt        0.000 Mt       51.804 Mt    66.181 % 	 .../ScenePrep

		      26.472 Mt    33.819 %         1 x       26.472 Mt        0.000 Mt       26.472 Mt   100.000 % 	 ..../SceneDesc-CreateScene

		      72.230 Mt    15.923 %        62 x        1.165 Mt        0.000 Mt       23.905 Mt    33.096 % 	 .../AccelerationStructureBuild

		      18.795 Mt    26.021 %        62 x        0.303 Mt        0.000 Mt       18.795 Mt   100.000 % 	 ..../BuildBottomLevelAS

		      29.529 Mt    40.882 %        62 x        0.476 Mt        0.000 Mt       29.529 Mt   100.000 % 	 ..../BuildTopLevelAS

		   20020.243 Mt    97.545 %      1207 x       16.587 Mt       16.183 Mt        1.493 Mt     0.007 % 	 ../Render

		   20018.749 Mt    99.993 %      1207 x       16.586 Mt       16.182 Mt    10494.372 Mt    52.423 % 	 .../Render

		    9524.377 Mt    47.577 %      1205 x        7.904 Mt        7.854 Mt     9491.527 Mt    99.655 % 	 ..../RayTracing

		       6.534 Mt     0.069 %      1205 x        0.005 Mt        0.001 Mt        1.852 Mt    28.351 % 	 ...../DeferredRLPrep

		       4.681 Mt    71.649 %         1 x        4.681 Mt        0.000 Mt        2.381 Mt    50.864 % 	 ....../PrepareForRendering

		       0.061 Mt     1.309 %         1 x        0.061 Mt        0.000 Mt        0.061 Mt   100.000 % 	 ......./PrepareMaterials

		       0.272 Mt     5.817 %         1 x        0.272 Mt        0.000 Mt        0.272 Mt   100.000 % 	 ......./BuildMaterialCache

		       1.967 Mt    42.010 %         1 x        1.967 Mt        0.000 Mt        1.967 Mt   100.000 % 	 ......./PrepareDrawBundle

		      26.316 Mt     0.276 %      1205 x        0.022 Mt        0.002 Mt        9.072 Mt    34.474 % 	 ...../RayTracingRLPrep

		       1.346 Mt     5.115 %         1 x        1.346 Mt        0.000 Mt        1.346 Mt   100.000 % 	 ....../PrepareAccelerationStructures

		       0.051 Mt     0.195 %         1 x        0.051 Mt        0.000 Mt        0.051 Mt   100.000 % 	 ....../PrepareCache

		       0.472 Mt     1.795 %         1 x        0.472 Mt        0.000 Mt        0.472 Mt   100.000 % 	 ....../BuildCache

		      14.529 Mt    55.210 %         1 x       14.529 Mt        0.000 Mt        0.001 Mt     0.009 % 	 ....../BuildAccelerationStructures

		      14.528 Mt    99.991 %         1 x       14.528 Mt        0.000 Mt        0.913 Mt     6.287 % 	 ......./AccelerationStructureBuild

		      12.995 Mt    89.450 %         1 x       12.995 Mt        0.000 Mt       12.995 Mt   100.000 % 	 ......../BuildBottomLevelAS

		       0.619 Mt     4.264 %         1 x        0.619 Mt        0.000 Mt        0.619 Mt   100.000 % 	 ......../BuildTopLevelAS

		       0.845 Mt     3.212 %         1 x        0.845 Mt        0.000 Mt        0.845 Mt   100.000 % 	 ....../BuildShaderTables

		       0.000 Mt     0.000 %         1 x        0.000 Mt        0.000 Mt        0.000 Mt   100.000 % 	 ./Destroy

	WindowThread:

		   35005.802 Mt   100.000 %         1 x    35005.802 Mt    35005.802 Mt    35005.802 Mt   100.000 % 	 /

	GpuThread:

		    2944.680 Mt   100.000 %      1268 x        2.322 Mt        2.154 Mt        0.240 Mt     0.008 % 	 /

		      15.265 Mt     0.518 %      1207 x        0.013 Mt        0.001 Mt       15.265 Mt   100.000 % 	 ./Present

		      54.063 Mt     1.836 %        62 x        0.872 Mt        0.000 Mt        0.107 Mt     0.198 % 	 ./AccelerationStructureBuild

		      45.340 Mt    83.864 %        62 x        0.731 Mt        0.000 Mt       45.340 Mt   100.000 % 	 ../BuildBottomLevelASGpu

		       8.616 Mt    15.937 %        62 x        0.139 Mt        0.000 Mt        8.616 Mt   100.000 % 	 ../BuildTopLevelASGpu

		    2875.112 Mt    97.638 %      1205 x        2.386 Mt        2.151 Mt        3.433 Mt     0.119 % 	 ./RayTracingGpu

		     306.417 Mt    10.658 %      1205 x        0.254 Mt        0.229 Mt      306.417 Mt   100.000 % 	 ../DeferredRLGpu

		      24.878 Mt     0.865 %         1 x       24.878 Mt        0.000 Mt        0.002 Mt     0.008 % 	 ../AccelerationStructureBuild

		      24.565 Mt    98.740 %         1 x       24.565 Mt        0.000 Mt       24.565 Mt   100.000 % 	 .../BuildBottomLevelASGpu

		       0.312 Mt     1.253 %         1 x        0.312 Mt        0.000 Mt        0.312 Mt   100.000 % 	 .../BuildTopLevelASGpu

		    1437.327 Mt    49.992 %      1205 x        1.193 Mt        1.029 Mt     1437.327 Mt   100.000 % 	 ../RayTracingRLGpu

		    1103.058 Mt    38.366 %      1205 x        0.915 Mt        0.892 Mt     1103.058 Mt   100.000 % 	 ../ResolveRLGpu


	============================


