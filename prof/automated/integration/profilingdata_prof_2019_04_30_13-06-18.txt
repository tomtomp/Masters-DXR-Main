Configuration: 
Command line parameters  = 
Window width             = 1186
Window height            = 1219
Window title             = Direct3D App
Back-buffer count        = 3
Fill Triangles           = enabled
Triangles Font Clock-Wise= disabled
Fast Build AS            = disabled
Target FPS               = 0
Target UPS               = 60
Tearing                  = enabled
VSync                    = disabled
GUI rendering            = disabled
Debug layer              = enabled
GPU validation           = disabled
GPU profiling            = enabled
Render mode              = Ray Tracing
Loaded scene file        = 
Using testing scene      = disabled
BLAS duplication         = disabled
Rays per pixel           = 34
Quality preset           = Ultra
Rasterization Quality    : 
  AO                     = enabled
  AO kernel size         = 16
  Shadows                = enabled
  Shadow map resolution  = 8192
  Shadow PCF kernel size = 8
  Reflections            = enabled
Ray Tracing Quality      : 
  AO                     = enabled
  AO rays                = 16
  AO kernel size         = 8
  Shadows                = enabled
  Shadow rays            = 8
  Shadow kernel size     = 8
  Reflections            = enabled

ProfilingAggregator: 
Render	,	19.8033	,	20.9164	,	19.296	,	19.5007	,	18.8407	,	19.5489	,	20.3095	,	19.637	,	19.1131	,	18.8215	,	20.7836
Update	,	0.025	,	0.0986	,	0.0832	,	0.0822	,	0.0244	,	0.0241	,	0.0259	,	0.0245	,	0.0244	,	0.0852	,	0.0825
RayTracing	,	7.1356	,	8.1597	,	6.9419	,	7.3255	,	6.9446	,	7.3479	,	7.4408	,	6.9476	,	7.0651	,	7.0145	,	8.5248
RayTracingGpu	,	5.92682	,	5.82739	,	6.0407	,	5.62659	,	5.60822	,	5.71651	,	5.58035	,	5.56947	,	5.5287	,	5.52262	,	5.52925
DeferredRLGpu	,	0.188384	,	0.194144	,	0.21024	,	0.188064	,	0.195424	,	0.193664	,	0.195328	,	0.196544	,	0.1944	,	0.192896	,	0.194272
RayTracingRLGpu	,	2.72989	,	2.52957	,	2.91107	,	2.53232	,	2.58963	,	2.57584	,	2.56874	,	2.57219	,	2.55491	,	2.54938	,	2.54835
ResolveRLGpu	,	3.00547	,	3.10208	,	2.91779	,	2.9024	,	2.82109	,	2.94214	,	2.81402	,	2.79866	,	2.77581	,	2.77802	,	2.78435
Index	,	0	,	1	,	2	,	3	,	4	,	5	,	6	,	7	,	8	,	9	,	10

ProfilingBuildAggregator: 
BuildBottomLevelAS	,	13.192
BuildBottomLevelASGpu	,	23.3171
BuildTopLevelAS	,	0.483
BuildTopLevelASGpu	,	0.938816
Triangles	,	281182
Meshes	,	52
BottomLevels	,	43
TopLevelSize	,	2752
BottomLevelsSize	,	15869344
Index	,	0

FpsAggregator: 
FPS	,	22	,	31	,	33	,	34	,	34	,	34	,	11	,	30	,	30	,	30	,	30
UPS	,	170	,	60	,	60	,	62	,	62	,	62	,	23	,	761	,	61	,	61	,	61
FrameTime	,	46.3209	,	32.6083	,	30.4641	,	30.1277	,	30.6632	,	30.1098	,	1095.47	,	34.5629	,	33.4196	,	33.9389	,	34.0416
GigaRays/s	,	0.985033	,	1.39926	,	1.49775	,	1.51448	,	1.48803	,	1.51538	,	0.0416512	,	1.32013	,	1.3653	,	1.34441	,	1.34035
Index	,	0	,	1	,	2	,	3	,	4	,	5	,	6	,	7	,	8	,	9	,	10


AccelerationStructure: 
Ray tracing acceleration structure building statistics: 
	Bottom Level Acceleration Structure: 
		Maximum [B]: 2940416
		Minimum [B]: 6256
		Currently Allocated [B]: 15869344
		Currently Created [num]: 43
		Current Average [B]: 369054
		Maximum Triangles [num]: 52674
		Minimum Triangles [num]: 27
		Total Triangles [num]: 281182
		Maximum Meshes [num]: 2
		Minimum Meshes [num]: 1
		Total Meshes [num]: 52
	Top Level Acceleration Structure: 
		Maximum [B]: 2752
		Minimum [B]: 2752
		Currently Allocated [B]: 2752
		Total Allocated [B]: 2752
		Total Created [num]: 1
		Average Allocated [B]: 2752
		Maximum Instances [num]: 43
		Minimum Instances [num]: 43
		Total Instances [num]: 43
	Scratch Buffer: 
		Maximum [B]: 2344448
		Minimum [B]: 2344448
		Currently Allocated [B]: 2344448
		Total Allocated [B]: 2344448
		Total Created [num]: 1
		Average Allocated [B]: 2344448
ProfilingStack: 
[PROF] Profiling statistics: 
	Scopes entered : 5565
	Scopes exited : 5565
	Overhead per scope [ticks] : 162
	Threads entered : 3
	Threads exited : 0
	============================
		         Sum[Mt]   Parent[%]  Samples[num]       Avg[Mt]   Last/Curr[Mt]        Self[Mt]     Self[%] 	 Name

	main:

		   24966.258 Mt   100.000 %         1 x    24966.259 Mt    24966.258 Mt    17551.757 Mt    70.302 % 	 /

		      66.885 Mt     0.268 %         1 x       66.885 Mt        0.000 Mt        0.675 Mt     1.009 % 	 ./Initialize

		       3.115 Mt     4.658 %         1 x        3.115 Mt        0.000 Mt        3.115 Mt   100.000 % 	 ../TexturedRLInitialization

		       1.597 Mt     2.388 %         1 x        1.597 Mt        0.000 Mt        1.597 Mt   100.000 % 	 ../ShadowMapRLInitialization

		       6.705 Mt    10.025 %         1 x        6.705 Mt        0.000 Mt        6.705 Mt   100.000 % 	 ../AmbientOcclusionRLInitialization

		       5.258 Mt     7.862 %         1 x        5.258 Mt        0.000 Mt        5.258 Mt   100.000 % 	 ../RasterResolveRLInitialization

		       4.577 Mt     6.844 %         1 x        4.577 Mt        0.000 Mt        4.577 Mt   100.000 % 	 ../DeferredRLInitialization

		      40.538 Mt    60.608 %         1 x       40.538 Mt        0.000 Mt        1.255 Mt     3.096 % 	 ../RayTracingRLInitialization

		      39.283 Mt    96.904 %         1 x       39.283 Mt        0.000 Mt       39.283 Mt   100.000 % 	 .../PipelineBuild

		       4.419 Mt     6.607 %         1 x        4.419 Mt        0.000 Mt        4.419 Mt   100.000 % 	 ../ResolveRLInitialization

		    7347.616 Mt    29.430 %       336 x       21.868 Mt       22.216 Mt       20.287 Mt     0.276 % 	 ./Run

		     345.878 Mt     4.707 %      1484 x        0.233 Mt        0.029 Mt      203.809 Mt    58.925 % 	 ../Update

		      71.817 Mt    20.764 %         1 x       71.817 Mt        0.000 Mt       51.008 Mt    71.024 % 	 .../ScenePrep

		      20.809 Mt    28.976 %         1 x       20.809 Mt        0.000 Mt       20.809 Mt   100.000 % 	 ..../SceneDesc-CreateScene

		      70.251 Mt    20.311 %        62 x        1.133 Mt        0.000 Mt       25.375 Mt    36.120 % 	 .../AccelerationStructureBuild

		      18.788 Mt    26.744 %        62 x        0.303 Mt        0.000 Mt       18.788 Mt   100.000 % 	 ..../BuildBottomLevelAS

		      26.089 Mt    37.136 %        62 x        0.421 Mt        0.000 Mt       26.089 Mt   100.000 % 	 ..../BuildTopLevelAS

		    6981.451 Mt    95.017 %       336 x       20.778 Mt       22.095 Mt        0.382 Mt     0.005 % 	 ../Render

		    6981.069 Mt    99.995 %       336 x       20.777 Mt       22.094 Mt     4298.913 Mt    61.580 % 	 .../Render

		    2682.156 Mt    38.420 %       334 x        8.030 Mt        8.474 Mt     2653.401 Mt    98.928 % 	 ..../RayTracing

		       5.489 Mt     0.205 %       334 x        0.016 Mt        0.001 Mt        0.461 Mt     8.396 % 	 ...../DeferredRLPrep

		       5.028 Mt    91.604 %         1 x        5.028 Mt        0.000 Mt        2.686 Mt    53.426 % 	 ....../PrepareForRendering

		       0.061 Mt     1.207 %         1 x        0.061 Mt        0.000 Mt        0.061 Mt   100.000 % 	 ......./PrepareMaterials

		       0.259 Mt     5.149 %         1 x        0.259 Mt        0.000 Mt        0.259 Mt   100.000 % 	 ......./BuildMaterialCache

		       2.022 Mt    40.218 %         1 x        2.022 Mt        0.000 Mt        2.022 Mt   100.000 % 	 ......./PrepareDrawBundle

		      23.266 Mt     0.867 %       334 x        0.070 Mt        0.001 Mt        6.686 Mt    28.736 % 	 ...../RayTracingRLPrep

		       1.276 Mt     5.486 %         1 x        1.276 Mt        0.000 Mt        1.276 Mt   100.000 % 	 ....../PrepareAccelerationStructures

		       0.050 Mt     0.213 %         1 x        0.050 Mt        0.000 Mt        0.050 Mt   100.000 % 	 ....../PrepareCache

		       0.534 Mt     2.297 %         1 x        0.534 Mt        0.000 Mt        0.534 Mt   100.000 % 	 ....../BuildCache

		      14.263 Mt    61.304 %         1 x       14.263 Mt        0.000 Mt        0.002 Mt     0.011 % 	 ....../BuildAccelerationStructures

		      14.262 Mt    99.989 %         1 x       14.262 Mt        0.000 Mt        0.587 Mt     4.114 % 	 ......./AccelerationStructureBuild

		      13.192 Mt    92.499 %         1 x       13.192 Mt        0.000 Mt       13.192 Mt   100.000 % 	 ......../BuildBottomLevelAS

		       0.483 Mt     3.387 %         1 x        0.483 Mt        0.000 Mt        0.483 Mt   100.000 % 	 ......../BuildTopLevelAS

		       0.457 Mt     1.963 %         1 x        0.457 Mt        0.000 Mt        0.457 Mt   100.000 % 	 ....../BuildShaderTables

		       0.001 Mt     0.000 %         1 x        0.001 Mt        0.001 Mt        0.001 Mt   100.000 % 	 ./Destroy

	WindowThread:

		   24964.047 Mt   100.000 %         1 x    24964.047 Mt    24964.047 Mt    24964.047 Mt   100.000 % 	 /

	GpuThread:

		    2043.285 Mt   100.000 %       397 x        5.147 Mt        5.520 Mt       -0.184 Mt    -0.009 % 	 /

		      19.265 Mt     0.943 %       336 x        0.057 Mt        0.001 Mt       19.265 Mt   100.000 % 	 ./Present

		      51.728 Mt     2.532 %        62 x        0.834 Mt        0.000 Mt        0.122 Mt     0.236 % 	 ./AccelerationStructureBuild

		      42.589 Mt    82.333 %        62 x        0.687 Mt        0.000 Mt       42.589 Mt   100.000 % 	 ../BuildBottomLevelASGpu

		       9.017 Mt    17.431 %        62 x        0.145 Mt        0.000 Mt        9.017 Mt   100.000 % 	 ../BuildTopLevelASGpu

		    1972.476 Mt    96.535 %       334 x        5.906 Mt        5.518 Mt        0.874 Mt     0.044 % 	 ./RayTracingGpu

		      65.861 Mt     3.339 %       334 x        0.197 Mt        0.192 Mt       65.861 Mt   100.000 % 	 ../DeferredRLGpu

		      24.258 Mt     1.230 %         1 x       24.258 Mt        0.000 Mt        0.002 Mt     0.008 % 	 ../AccelerationStructureBuild

		      23.317 Mt    96.122 %         1 x       23.317 Mt        0.000 Mt       23.317 Mt   100.000 % 	 .../BuildBottomLevelASGpu

		       0.939 Mt     3.870 %         1 x        0.939 Mt        0.000 Mt        0.939 Mt   100.000 % 	 .../BuildTopLevelASGpu

		     899.335 Mt    45.594 %       334 x        2.693 Mt        2.553 Mt      899.335 Mt   100.000 % 	 ../RayTracingRLGpu

		     982.148 Mt    49.793 %       334 x        2.941 Mt        2.770 Mt      982.148 Mt   100.000 % 	 ../ResolveRLGpu


	============================


