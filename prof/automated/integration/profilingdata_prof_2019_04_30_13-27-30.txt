Configuration: 
Command line parameters  = 
Window width             = 1186
Window height            = 1219
Window title             = Direct3D App
Back-buffer count        = 3
Fill Triangles           = enabled
Triangles Font Clock-Wise= disabled
Fast Build AS            = disabled
Target FPS               = 0
Target UPS               = 60
Tearing                  = enabled
VSync                    = disabled
GUI rendering            = disabled
Debug layer              = enabled
GPU validation           = disabled
GPU profiling            = enabled
Render mode              = Ray Tracing
Loaded scene file        = 
Using testing scene      = disabled
BLAS duplication         = disabled
Rays per pixel           = 9
Quality preset           = Medium
Rasterization Quality    : 
  AO                     = enabled
  AO kernel size         = 4
  Shadows                = enabled
  Shadow map resolution  = 4096
  Shadow PCF kernel size = 4
  Reflections            = disabled
Ray Tracing Quality      : 
  AO                     = enabled
  AO rays                = 4
  AO kernel size         = 4
  Shadows                = enabled
  Shadow rays            = 4
  Shadow kernel size     = 4
  Reflections            = disabled

ProfilingAggregator: 
Render	,	15.7911	,	26.6894	,	16.1011	,	16.05	,	16.5729	,	15.724	,	17.3938	,	17.183	,	17.9934	,	15.4564	,	15.6394	,	15.5406	,	16.9926	,	15.1816	,	16.7442	,	15.2047	,	17.8474	,	17.6026	,	22.5529	,	15.4135
Update	,	0.0246	,	0.1393	,	0.0822	,	0.0224	,	0.0822	,	0.0824	,	0.0275	,	0.0422	,	0.0821	,	0.0245	,	0.024	,	0.0249	,	0.0287	,	0.0797	,	0.0267	,	0.0811	,	0.0242	,	0.0243	,	0.1034	,	0.0804
RayTracing	,	6.9541	,	16.1659	,	7.4598	,	7.2152	,	7.6582	,	7.3482	,	8.3963	,	8.3319	,	7.368	,	7.0654	,	7.174	,	7.1814	,	8.3377	,	7.1045	,	8.1086	,	6.9494	,	7.1661	,	8.352	,	11.5826	,	7.0143
RayTracingGpu	,	2.35341	,	2.33357	,	2.39872	,	2.3279	,	2.3976	,	2.3761	,	2.59395	,	2.55923	,	2.5985	,	2.13117	,	2.41142	,	2.18307	,	1.95981	,	2.22579	,	2.2335	,	2.3799	,	2.22723	,	2.55002	,	2.41818	,	2.3288
DeferredRLGpu	,	0.235712	,	0.234432	,	0.262592	,	0.25184	,	0.273728	,	0.271808	,	0.271808	,	0.256928	,	0.235872	,	0.204032	,	0.22384	,	0.201632	,	0.209952	,	0.2192	,	0.205152	,	0.208096	,	0.208384	,	0.2264	,	0.288544	,	0.265888
RayTracingRLGpu	,	1.20122	,	1.19344	,	1.22835	,	1.16054	,	1.2135	,	1.19904	,	1.4103	,	1.38339	,	1.45987	,	1.02227	,	1.26762	,	1.07066	,	0.873632	,	1.09712	,	1.12768	,	1.15827	,	1.10368	,	1.42282	,	1.22682	,	1.14614
ResolveRLGpu	,	0.914592	,	0.9032	,	0.905632	,	0.913344	,	0.90752	,	0.90448	,	0.905888	,	0.916192	,	0.901248	,	0.903232	,	0.91824	,	0.907136	,	0.8736	,	0.906848	,	0.897408	,	1.0105	,	0.912864	,	0.8976	,	0.901056	,	0.913984
Index	,	0	,	1	,	2	,	3	,	4	,	5	,	6	,	7	,	8	,	9	,	10	,	11	,	12	,	13	,	14	,	15	,	16	,	17	,	18	,	19

ProfilingBuildAggregator: 
BuildBottomLevelAS	,	12.9684
BuildBottomLevelASGpu	,	29.2092
BuildTopLevelAS	,	0.458
BuildTopLevelASGpu	,	0.331776
Triangles	,	281182
Meshes	,	52
BottomLevels	,	43
TopLevelSize	,	2752
BottomLevelsSize	,	15869344
Index	,	0

FpsAggregator: 
FPS	,	20	,	17	,	35	,	30	,	16	,	39	,	38	,	39	,	41	,	38	,	37	,	37	,	39	,	41	,	42	,	40	,	41	,	40	,	38	,	38
UPS	,	178	,	47	,	299	,	61	,	25	,	103	,	63	,	61	,	60	,	62	,	60	,	61	,	60	,	60	,	61	,	60	,	61	,	61	,	59	,	61
FrameTime	,	50.5662	,	278.708	,	29.2993	,	33.6883	,	70.3462	,	26.6086	,	26.7354	,	26.1274	,	24.7643	,	26.5849	,	27.2161	,	27.4292	,	25.6812	,	24.4074	,	24.286	,	25.0541	,	24.576	,	25.5445	,	26.3212	,	26.407
GigaRays/s	,	0.238854	,	0.0433354	,	0.412226	,	0.35852	,	0.171692	,	0.45391	,	0.451757	,	0.46227	,	0.487715	,	0.454315	,	0.443779	,	0.44033	,	0.470302	,	0.494846	,	0.497319	,	0.482072	,	0.491452	,	0.472819	,	0.458866	,	0.457375
Index	,	0	,	1	,	2	,	3	,	4	,	5	,	6	,	7	,	8	,	9	,	10	,	11	,	12	,	13	,	14	,	15	,	16	,	17	,	18	,	19


AccelerationStructure: 
Ray tracing acceleration structure building statistics: 
	Bottom Level Acceleration Structure: 
		Maximum [B]: 2940416
		Minimum [B]: 6256
		Currently Allocated [B]: 15869344
		Currently Created [num]: 43
		Current Average [B]: 369054
		Maximum Triangles [num]: 52674
		Minimum Triangles [num]: 27
		Total Triangles [num]: 281182
		Maximum Meshes [num]: 2
		Minimum Meshes [num]: 1
		Total Meshes [num]: 52
	Top Level Acceleration Structure: 
		Maximum [B]: 2752
		Minimum [B]: 2752
		Currently Allocated [B]: 2752
		Total Allocated [B]: 2752
		Total Created [num]: 1
		Average Allocated [B]: 2752
		Maximum Instances [num]: 43
		Minimum Instances [num]: 43
		Total Instances [num]: 43
	Scratch Buffer: 
		Maximum [B]: 2344448
		Minimum [B]: 2344448
		Currently Allocated [B]: 2344448
		Total Allocated [B]: 2344448
		Total Created [num]: 1
		Average Allocated [B]: 2344448
ProfilingStack: 
[PROF] Profiling statistics: 
	Scopes entered : 10067
	Scopes exited : 10067
	Overhead per scope [ticks] : 160.9
	Threads entered : 3
	Threads exited : 0
	============================
		         Sum[Mt]   Parent[%]  Samples[num]       Avg[Mt]   Last/Curr[Mt]        Self[Mt]     Self[%] 	 Name

	main:

		   27209.222 Mt   100.000 %         1 x    27209.223 Mt    27209.222 Mt    14114.596 Mt    51.874 % 	 /

		      75.802 Mt     0.279 %         1 x       75.802 Mt        0.000 Mt        0.696 Mt     0.919 % 	 ./Initialize

		       3.001 Mt     3.959 %         1 x        3.001 Mt        0.000 Mt        3.001 Mt   100.000 % 	 ../TexturedRLInitialization

		       1.411 Mt     1.861 %         1 x        1.411 Mt        0.000 Mt        1.411 Mt   100.000 % 	 ../ShadowMapRLInitialization

		       7.564 Mt     9.978 %         1 x        7.564 Mt        0.000 Mt        7.564 Mt   100.000 % 	 ../AmbientOcclusionRLInitialization

		       5.704 Mt     7.525 %         1 x        5.704 Mt        0.000 Mt        5.704 Mt   100.000 % 	 ../RasterResolveRLInitialization

		       4.131 Mt     5.449 %         1 x        4.131 Mt        0.000 Mt        4.131 Mt   100.000 % 	 ../DeferredRLInitialization

		      47.058 Mt    62.080 %         1 x       47.058 Mt        0.000 Mt        1.622 Mt     3.446 % 	 ../RayTracingRLInitialization

		      45.437 Mt    96.554 %         1 x       45.437 Mt        0.000 Mt       45.437 Mt   100.000 % 	 .../PipelineBuild

		       6.238 Mt     8.229 %         1 x        6.238 Mt        0.000 Mt        6.238 Mt   100.000 % 	 ../ResolveRLInitialization

		   13018.824 Mt    47.847 %       733 x       17.761 Mt       17.539 Mt       37.607 Mt     0.289 % 	 ./Run

		     461.916 Mt     3.548 %      1619 x        0.285 Mt        0.029 Mt      294.116 Mt    63.673 % 	 ../Update

		      83.940 Mt    18.172 %         1 x       83.940 Mt        0.000 Mt       56.881 Mt    67.763 % 	 .../ScenePrep

		      27.059 Mt    32.237 %         1 x       27.059 Mt        0.000 Mt       27.059 Mt   100.000 % 	 ..../SceneDesc-CreateScene

		      83.860 Mt    18.155 %        62 x        1.353 Mt        0.000 Mt       33.086 Mt    39.454 % 	 .../AccelerationStructureBuild

		      21.289 Mt    25.386 %        62 x        0.343 Mt        0.000 Mt       21.289 Mt   100.000 % 	 ..../BuildBottomLevelAS

		      29.485 Mt    35.160 %        62 x        0.476 Mt        0.000 Mt       29.485 Mt   100.000 % 	 ..../BuildTopLevelAS

		   12519.300 Mt    96.163 %       733 x       17.080 Mt       17.415 Mt        0.849 Mt     0.007 % 	 ../Render

		   12518.452 Mt    99.993 %       733 x       17.078 Mt       17.414 Mt     6756.036 Mt    53.969 % 	 .../Render

		    5762.416 Mt    46.031 %       731 x        7.883 Mt        8.133 Mt     5730.479 Mt    99.446 % 	 ..../RayTracing

		       7.146 Mt     0.124 %       731 x        0.010 Mt        0.001 Mt        1.010 Mt    14.129 % 	 ...../DeferredRLPrep

		       6.136 Mt    85.871 %         1 x        6.136 Mt        0.000 Mt        3.506 Mt    57.132 % 	 ....../PrepareForRendering

		       0.065 Mt     1.053 %         1 x        0.065 Mt        0.000 Mt        0.065 Mt   100.000 % 	 ......./PrepareMaterials

		       0.455 Mt     7.410 %         1 x        0.455 Mt        0.000 Mt        0.455 Mt   100.000 % 	 ......./BuildMaterialCache

		       2.111 Mt    34.405 %         1 x        2.111 Mt        0.000 Mt        2.111 Mt   100.000 % 	 ......./PrepareDrawBundle

		      24.791 Mt     0.430 %       731 x        0.034 Mt        0.001 Mt        8.269 Mt    33.353 % 	 ...../RayTracingRLPrep

		       1.400 Mt     5.646 %         1 x        1.400 Mt        0.000 Mt        1.400 Mt   100.000 % 	 ....../PrepareAccelerationStructures

		       0.053 Mt     0.212 %         1 x        0.053 Mt        0.000 Mt        0.053 Mt   100.000 % 	 ....../PrepareCache

		       0.552 Mt     2.226 %         1 x        0.552 Mt        0.000 Mt        0.552 Mt   100.000 % 	 ....../BuildCache

		      14.169 Mt    57.152 %         1 x       14.169 Mt        0.000 Mt        0.002 Mt     0.011 % 	 ....../BuildAccelerationStructures

		      14.167 Mt    99.989 %         1 x       14.167 Mt        0.000 Mt        0.741 Mt     5.230 % 	 ......./AccelerationStructureBuild

		      12.968 Mt    91.538 %         1 x       12.968 Mt        0.000 Mt       12.968 Mt   100.000 % 	 ......../BuildBottomLevelAS

		       0.458 Mt     3.233 %         1 x        0.458 Mt        0.000 Mt        0.458 Mt   100.000 % 	 ......../BuildTopLevelAS

		       0.350 Mt     1.411 %         1 x        0.350 Mt        0.000 Mt        0.350 Mt   100.000 % 	 ....../BuildShaderTables

		       0.001 Mt     0.000 %         1 x        0.001 Mt        0.001 Mt        0.001 Mt   100.000 % 	 ./Destroy

	WindowThread:

		   27207.057 Mt   100.000 %         1 x    27207.057 Mt    27207.057 Mt    27207.057 Mt   100.000 % 	 /

	GpuThread:

		    1882.593 Mt   100.000 %       794 x        2.371 Mt        2.309 Mt       -0.089 Mt    -0.005 % 	 /

		      13.694 Mt     0.727 %       733 x        0.019 Mt        0.002 Mt       13.694 Mt   100.000 % 	 ./Present

		      65.002 Mt     3.453 %        62 x        1.048 Mt        0.000 Mt        0.198 Mt     0.304 % 	 ./AccelerationStructureBuild

		      54.253 Mt    83.464 %        62 x        0.875 Mt        0.000 Mt       54.253 Mt   100.000 % 	 ../BuildBottomLevelASGpu

		      10.551 Mt    16.232 %        62 x        0.170 Mt        0.000 Mt       10.551 Mt   100.000 % 	 ../BuildTopLevelASGpu

		    1803.986 Mt    95.825 %       731 x        2.468 Mt        2.307 Mt        1.934 Mt     0.107 % 	 ./RayTracingGpu

		     186.223 Mt    10.323 %       731 x        0.255 Mt        0.265 Mt      186.223 Mt   100.000 % 	 ../DeferredRLGpu

		      29.545 Mt     1.638 %         1 x       29.545 Mt        0.000 Mt        0.004 Mt     0.014 % 	 ../AccelerationStructureBuild

		      29.209 Mt    98.863 %         1 x       29.209 Mt        0.000 Mt       29.209 Mt   100.000 % 	 .../BuildBottomLevelASGpu

		       0.332 Mt     1.123 %         1 x        0.332 Mt        0.000 Mt        0.332 Mt   100.000 % 	 .../BuildTopLevelASGpu

		     898.491 Mt    49.806 %       731 x        1.229 Mt        1.138 Mt      898.491 Mt   100.000 % 	 ../RayTracingRLGpu

		     687.793 Mt    38.126 %       731 x        0.941 Mt        0.902 Mt      687.793 Mt   100.000 % 	 ../ResolveRLGpu


	============================


