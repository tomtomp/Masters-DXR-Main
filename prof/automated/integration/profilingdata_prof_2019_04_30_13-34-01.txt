Configuration: 
Command line parameters  = 
Window width             = 1186
Window height            = 1219
Window title             = Direct3D App
Back-buffer count        = 3
Fill Triangles           = enabled
Triangles Font Clock-Wise= disabled
Fast Build AS            = disabled
Target FPS               = 0
Target UPS               = 60
Tearing                  = enabled
VSync                    = disabled
GUI rendering            = disabled
Debug layer              = enabled
GPU validation           = disabled
GPU profiling            = enabled
Render mode              = Ray Tracing
Loaded scene file        = 
Using testing scene      = disabled
BLAS duplication         = disabled
Rays per pixel           = 9
Quality preset           = Medium
Rasterization Quality    : 
  AO                     = enabled
  AO kernel size         = 4
  Shadows                = enabled
  Shadow map resolution  = 4096
  Shadow PCF kernel size = 4
  Reflections            = disabled
Ray Tracing Quality      : 
  AO                     = enabled
  AO rays                = 4
  AO kernel size         = 4
  Shadows                = enabled
  Shadow rays            = 4
  Shadow kernel size     = 4
  Reflections            = disabled

ProfilingAggregator: 
Render	,	15.3343	,	19.5335	,	15.4623	,	16.5592	,	15.6076	,	15.6831	,	16.6639	,	17.1247	,	15.128	,	15.6857	,	15.8868	,	17.6162	,	17.498	,	17.6618	,	15.6975	,	16.8849	,	16.1232	,	15.85	,	15.9327	,	22.5476	,	15.4914	,	15.6664	,	16.4696	,	15.3908
Update	,	0.0834	,	0.098	,	0.0243	,	0.1144	,	0.0246	,	0.0248	,	0.0894	,	0.0904	,	0.0816	,	0.0809	,	0.0246	,	0.085	,	0.0808	,	0.0913	,	0.0823	,	0.0287	,	0.0249	,	0.1342	,	0.0241	,	0.1041	,	0.0821	,	0.0827	,	0.0245	,	0.0245
RayTracing	,	7.0131	,	10.023	,	6.9205	,	7.8769	,	6.9877	,	7.1748	,	8.2243	,	8.0681	,	6.9171	,	7.1337	,	7.0967	,	8.9148	,	8.9213	,	8.5722	,	7.0026	,	8.1708	,	7.1854	,	6.9777	,	7.1918	,	11.5355	,	7.0152	,	6.9954	,	7.096	,	7.0604
RayTracingGpu	,	2.51594	,	2.52611	,	2.30198	,	2.53962	,	2.16426	,	2.43834	,	2.38099	,	2.59664	,	2.45213	,	2.45251	,	2.70365	,	2.46464	,	2.44493	,	2.45069	,	2.4505	,	2.72048	,	2.54739	,	2.46547	,	2.45034	,	2.74323	,	2.45568	,	2.47686	,	2.45725	,	2.45117
DeferredRLGpu	,	0.237536	,	0.239712	,	0.258432	,	0.233472	,	0.20512	,	0.223424	,	0.221696	,	0.25312	,	0.246944	,	0.245888	,	0.247936	,	0.254624	,	0.248128	,	0.249472	,	0.248384	,	0.247168	,	0.243392	,	0.247168	,	0.244992	,	0.251136	,	0.247872	,	0.25024	,	0.249536	,	0.252224
RayTracingRLGpu	,	1.37674	,	1.37002	,	1.15302	,	1.26355	,	1.07315	,	1.30429	,	1.2601	,	1.31494	,	1.29408	,	1.29619	,	1.54963	,	1.29965	,	1.29526	,	1.29242	,	1.29574	,	1.56208	,	1.29376	,	1.3024	,	1.29264	,	1.57523	,	1.30211	,	1.3119	,	1.30093	,	1.29325
ResolveRLGpu	,	0.898848	,	0.913216	,	0.88704	,	1.03914	,	0.883296	,	0.908544	,	0.894144	,	1.02614	,	0.908928	,	0.90576	,	0.90368	,	0.908128	,	0.899328	,	0.905056	,	0.902848	,	0.907424	,	1.00816	,	0.912672	,	0.908512	,	0.909728	,	0.903104	,	0.911232	,	0.905824	,	0.90352
Index	,	0	,	1	,	2	,	3	,	4	,	5	,	6	,	7	,	8	,	9	,	10	,	11	,	12	,	13	,	14	,	15	,	16	,	17	,	18	,	19	,	20	,	21	,	22	,	23

ProfilingBuildAggregator: 
BuildBottomLevelAS	,	12.3704
BuildBottomLevelASGpu	,	29.4259
BuildTopLevelAS	,	0.4506
BuildTopLevelASGpu	,	0.317888
Triangles	,	281182
Meshes	,	52
BottomLevels	,	43
TopLevelSize	,	2752
BottomLevelsSize	,	15869344
Index	,	0

FpsAggregator: 
FPS	,	23	,	37	,	38	,	39	,	37	,	38	,	37	,	35	,	38	,	37	,	39	,	38	,	41	,	39	,	38	,	38	,	38	,	37	,	40	,	39	,	38	,	39	,	10	,	32
UPS	,	202	,	61	,	61	,	59	,	62	,	60	,	60	,	61	,	60	,	61	,	61	,	63	,	62	,	61	,	60	,	61	,	61	,	60	,	62	,	60	,	59	,	63	,	15	,	2215
FrameTime	,	43.6173	,	27.4976	,	26.3418	,	25.6499	,	27.5627	,	26.5637	,	27.274	,	28.9619	,	26.4066	,	27.3507	,	27.1563	,	26.8436	,	25.001	,	25.7563	,	26.4252	,	26.5779	,	26.7413	,	27.2556	,	25.5087	,	26.1221	,	26.39	,	25.9647	,	3614.87	,	31.859
GigaRays/s	,	0.276906	,	0.439234	,	0.458507	,	0.470875	,	0.438198	,	0.454677	,	0.442837	,	0.417028	,	0.457382	,	0.441595	,	0.444755	,	0.449936	,	0.483097	,	0.46893	,	0.45706	,	0.454434	,	0.451658	,	0.443136	,	0.473481	,	0.462363	,	0.45767	,	0.465167	,	0.00334118	,	0.379105
Index	,	0	,	1	,	2	,	3	,	4	,	5	,	6	,	7	,	8	,	9	,	10	,	11	,	12	,	13	,	14	,	15	,	16	,	17	,	18	,	19	,	20	,	21	,	22	,	23


AccelerationStructure: 
Ray tracing acceleration structure building statistics: 
	Bottom Level Acceleration Structure: 
		Maximum [B]: 2940416
		Minimum [B]: 6256
		Currently Allocated [B]: 15869344
		Currently Created [num]: 43
		Current Average [B]: 369054
		Maximum Triangles [num]: 52674
		Minimum Triangles [num]: 27
		Total Triangles [num]: 281182
		Maximum Meshes [num]: 2
		Minimum Meshes [num]: 1
		Total Meshes [num]: 52
	Top Level Acceleration Structure: 
		Maximum [B]: 2752
		Minimum [B]: 2752
		Currently Allocated [B]: 2752
		Total Allocated [B]: 2752
		Total Created [num]: 1
		Average Allocated [B]: 2752
		Maximum Instances [num]: 43
		Minimum Instances [num]: 43
		Total Instances [num]: 43
	Scratch Buffer: 
		Maximum [B]: 2344448
		Minimum [B]: 2344448
		Currently Allocated [B]: 2344448
		Total Allocated [B]: 2344448
		Total Created [num]: 1
		Average Allocated [B]: 2344448
ProfilingStack: 
[PROF] Profiling statistics: 
	Scopes entered : 13823
	Scopes exited : 13823
	Overhead per scope [ticks] : 162.7
	Threads entered : 3
	Threads exited : 0
	============================
		         Sum[Mt]   Parent[%]  Samples[num]       Avg[Mt]   Last/Curr[Mt]        Self[Mt]     Self[%] 	 Name

	main:

		   62744.652 Mt   100.000 %         1 x    62744.653 Mt    62744.652 Mt    47243.707 Mt    75.295 % 	 /

		      67.287 Mt     0.107 %         1 x       67.287 Mt        0.000 Mt        0.624 Mt     0.927 % 	 ./Initialize

		       3.245 Mt     4.823 %         1 x        3.245 Mt        0.000 Mt        3.245 Mt   100.000 % 	 ../TexturedRLInitialization

		       1.566 Mt     2.328 %         1 x        1.566 Mt        0.000 Mt        1.566 Mt   100.000 % 	 ../ShadowMapRLInitialization

		       6.981 Mt    10.375 %         1 x        6.981 Mt        0.000 Mt        6.981 Mt   100.000 % 	 ../AmbientOcclusionRLInitialization

		       4.884 Mt     7.259 %         1 x        4.884 Mt        0.000 Mt        4.884 Mt   100.000 % 	 ../RasterResolveRLInitialization

		       4.182 Mt     6.215 %         1 x        4.182 Mt        0.000 Mt        4.182 Mt   100.000 % 	 ../DeferredRLInitialization

		      41.504 Mt    61.682 %         1 x       41.504 Mt        0.000 Mt        1.263 Mt     3.042 % 	 ../RayTracingRLInitialization

		      40.241 Mt    96.958 %         1 x       40.241 Mt        0.000 Mt       40.241 Mt   100.000 % 	 .../PipelineBuild

		       4.301 Mt     6.392 %         1 x        4.301 Mt        0.000 Mt        4.301 Mt   100.000 % 	 ../ResolveRLInitialization

		   15433.658 Mt    24.598 %       881 x       17.518 Mt       15.524 Mt       43.978 Mt     0.285 % 	 ./Run

		     444.890 Mt     2.883 %      3747 x        0.119 Mt        0.024 Mt      305.572 Mt    68.685 % 	 ../Update

		      59.674 Mt    13.413 %         1 x       59.674 Mt        0.000 Mt       54.254 Mt    90.918 % 	 .../ScenePrep

		       5.419 Mt     9.082 %         1 x        5.419 Mt        0.000 Mt        5.419 Mt   100.000 % 	 ..../SceneDesc-CreateScene

		      79.645 Mt    17.902 %        62 x        1.285 Mt        0.000 Mt       31.582 Mt    39.654 % 	 .../AccelerationStructureBuild

		      20.589 Mt    25.851 %        62 x        0.332 Mt        0.000 Mt       20.589 Mt   100.000 % 	 ..../BuildBottomLevelAS

		      27.474 Mt    34.495 %        62 x        0.443 Mt        0.000 Mt       27.474 Mt   100.000 % 	 ..../BuildTopLevelAS

		   14944.789 Mt    96.832 %       881 x       16.963 Mt       15.416 Mt        1.180 Mt     0.008 % 	 ../Render

		   14943.609 Mt    99.992 %       881 x       16.962 Mt       15.415 Mt     8043.738 Mt    53.827 % 	 .../Render

		    6899.871 Mt    46.173 %       879 x        7.850 Mt        6.970 Mt     6870.106 Mt    99.569 % 	 ..../RayTracing

		       5.949 Mt     0.086 %       879 x        0.007 Mt        0.001 Mt        1.189 Mt    19.988 % 	 ...../DeferredRLPrep

		       4.760 Mt    80.012 %         1 x        4.760 Mt        0.000 Mt        2.395 Mt    50.318 % 	 ....../PrepareForRendering

		       0.059 Mt     1.233 %         1 x        0.059 Mt        0.000 Mt        0.059 Mt   100.000 % 	 ......./PrepareMaterials

		       0.264 Mt     5.538 %         1 x        0.264 Mt        0.000 Mt        0.264 Mt   100.000 % 	 ......./BuildMaterialCache

		       2.042 Mt    42.910 %         1 x        2.042 Mt        0.000 Mt        2.042 Mt   100.000 % 	 ......./PrepareDrawBundle

		      23.817 Mt     0.345 %       879 x        0.027 Mt        0.002 Mt        7.875 Mt    33.066 % 	 ...../RayTracingRLPrep

		       1.520 Mt     6.382 %         1 x        1.520 Mt        0.000 Mt        1.520 Mt   100.000 % 	 ....../PrepareAccelerationStructures

		       0.066 Mt     0.276 %         1 x        0.066 Mt        0.000 Mt        0.066 Mt   100.000 % 	 ....../PrepareCache

		       0.480 Mt     2.016 %         1 x        0.480 Mt        0.000 Mt        0.480 Mt   100.000 % 	 ....../BuildCache

		      13.530 Mt    56.809 %         1 x       13.530 Mt        0.000 Mt        0.001 Mt     0.010 % 	 ....../BuildAccelerationStructures

		      13.529 Mt    99.990 %         1 x       13.529 Mt        0.000 Mt        0.708 Mt     5.233 % 	 ......./AccelerationStructureBuild

		      12.370 Mt    91.437 %         1 x       12.370 Mt        0.000 Mt       12.370 Mt   100.000 % 	 ......../BuildBottomLevelAS

		       0.451 Mt     3.331 %         1 x        0.451 Mt        0.000 Mt        0.451 Mt   100.000 % 	 ......../BuildTopLevelAS

		       0.345 Mt     1.451 %         1 x        0.345 Mt        0.000 Mt        0.345 Mt   100.000 % 	 ....../BuildShaderTables

		       0.001 Mt     0.000 %         1 x        0.001 Mt        0.001 Mt        0.001 Mt   100.000 % 	 ./Destroy

	WindowThread:

		   62742.934 Mt   100.000 %         1 x    62742.935 Mt    62742.934 Mt    62742.934 Mt   100.000 % 	 /

	GpuThread:

		    2305.265 Mt   100.000 %       942 x        2.447 Mt        2.452 Mt        0.055 Mt     0.002 % 	 /

		      11.137 Mt     0.483 %       881 x        0.013 Mt        0.001 Mt       11.137 Mt   100.000 % 	 ./Present

		      55.408 Mt     2.404 %        62 x        0.894 Mt        0.000 Mt        0.123 Mt     0.222 % 	 ./AccelerationStructureBuild

		      46.429 Mt    83.795 %        62 x        0.749 Mt        0.000 Mt       46.429 Mt   100.000 % 	 ../BuildBottomLevelASGpu

		       8.856 Mt    15.984 %        62 x        0.143 Mt        0.000 Mt        8.856 Mt   100.000 % 	 ../BuildTopLevelASGpu

		    2238.665 Mt    97.111 %       879 x        2.547 Mt        2.451 Mt        2.922 Mt     0.131 % 	 ./RayTracingGpu

		     218.063 Mt     9.741 %       879 x        0.248 Mt        0.250 Mt      218.063 Mt   100.000 % 	 ../DeferredRLGpu

		      29.746 Mt     1.329 %         1 x       29.746 Mt        0.000 Mt        0.002 Mt     0.006 % 	 ../AccelerationStructureBuild

		      29.426 Mt    98.925 %         1 x       29.426 Mt        0.000 Mt       29.426 Mt   100.000 % 	 .../BuildBottomLevelASGpu

		       0.318 Mt     1.069 %         1 x        0.318 Mt        0.000 Mt        0.318 Mt   100.000 % 	 .../BuildTopLevelASGpu

		    1171.958 Mt    52.351 %       879 x        1.333 Mt        1.293 Mt     1171.958 Mt   100.000 % 	 ../RayTracingRLGpu

		     815.976 Mt    36.449 %       879 x        0.928 Mt        0.907 Mt      815.976 Mt   100.000 % 	 ../ResolveRLGpu


	============================


