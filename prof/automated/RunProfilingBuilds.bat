@echo off

cd "..\..\build\Measurement\x64\Release\"



start /wait Measurement.exe --scene Cube/Cube.gltf --profile-builds --profile-output BuildsCube
copy BuildsCube*.txt ..\..\..\..\prof\automated\

start /wait Measurement.exe --scene Suzanne/Suzanne.gltf --profile-builds --profile-output BuildsSuzanne
copy BuildsSuzanne*.txt ..\..\..\..\prof\automated\

start /wait Measurement.exe --scene Sponza/Sponza.gltf --profile-builds --profile-output BuildsSponza
copy BuildsSponza*.txt ..\..\..\..\prof\automated\

start /wait Measurement.exe --scene Dragon/Dragon.glb --profile-builds --profile-output BuildsDragon
copy BuildsDragon*.txt ..\..\..\..\prof\automated\

start /wait Measurement.exe --scene Diorama/Diorama.glb --profile-builds --profile-output BuildsDiorama
copy BuildsDiorama*.txt ..\..\..\..\prof\automated\



exit