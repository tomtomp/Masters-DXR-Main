Configuration: 
Command line parameters  = E:\programming\FIT-DIP\Masters-DXR-Main-git\build\Measurement\x64\Debug\Measurement.exe --scene Sponza/Sponza.gltf --profile-track Sponza/Sponza.trk --width 2560 --height 1440 --profile-output TrackSponza1440Ra 
Window width             = 2560
Window height            = 1421
Window title             = Direct3D App
Back-buffer count        = 3
Target FPS               = 0
Target UPS               = 60
Tearing                  = enabled
VSync                    = disabled
GUI rendering            = disabled
Debug layer              = enabled
GPU validation           = disabled
GPU profiling            = enabled
Render mode              = Rasterization
Loaded scene file        = Sponza/Sponza.gltf
Using testing scene      = disabled
Duplication              = 1
Duplication in cube      = disabled
Duplication offset       = 10
BLAS duplication         = disabled
Rays per pixel           = 9

ProfilingBuildAggregator: 
BuildBottomLevelAS	,	3.2396
BuildBottomLevelASGpu	,	24.1725
BuildTopLevelAS	,	1.1659
BuildTopLevelASGpu	,	0.644864

ProfilingAggregator: 
Render	,	1.8348	,	1.9387	,	2.3559	,	1.996	,	2.0035	,	1.9635	,	2.3101	,	1.808	,	1.8806	,	2.0467	,	2.1638	,	2.1468	,	6.0704	,	2.3947	,	2.4674	,	2.3614	,	2.6931	,	4.8941	,	2.3047	,	6.0494	,	5.2071	,	2.8293	,	3.9497	,	2.5568	,	2.1774	,	3.3683
Update	,	0.2976	,	0.2934	,	0.4844	,	0.294	,	0.2905	,	0.3378	,	0.2969	,	0.2981	,	0.2994	,	0.2956	,	0.292	,	0.3054	,	0.299	,	0.2946	,	0.2974	,	0.2771	,	0.2921	,	0.2933	,	0.2975	,	0.299	,	0.3044	,	0.565	,	0.2944	,	0.2959	,	0.7928	,	0.4528

FpsAggregator: 
FPS	,	397	,	417	,	402	,	401	,	386	,	386	,	394	,	408	,	420	,	419	,	384	,	365	,	372	,	381	,	383	,	386	,	392	,	408	,	404	,	384	,	371	,	356	,	352	,	354	,	361	,	382
UPS	,	59	,	61	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	61	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	61	,	60
FrameTime	,	2.52348	,	2.40091	,	2.49083	,	2.49525	,	2.59171	,	2.59443	,	2.54357	,	2.45304	,	2.38188	,	2.38723	,	2.60817	,	2.74056	,	2.70419	,	2.63077	,	2.61306	,	2.59165	,	2.55159	,	2.45638	,	2.47961	,	2.60921	,	2.69642	,	2.81144	,	2.85185	,	2.8321	,	2.77357	,	2.61909
GigaRays/s	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0

AccelerationStructure: 
Ray tracing acceleration structure building statistics: 
	Bottom Level Acceleration Structure: 
		Maximum [B]: 18391808
		Minimum [B]: 18391808
		Currently Allocated [B]: 18391808
		Currently Created [num]: 1
		Current Average [B]: 18391808
		Maximum Triangles [num]: 262267
		Minimum Triangles [num]: 262267
		Total Triangles [num]: 262267
		Maximum Meshes [num]: 103
		Minimum Meshes [num]: 103
		Total Meshes [num]: 103
	Top Level Acceleration Structure: 
		Maximum [B]: 64
		Minimum [B]: 64
		Currently Allocated [B]: 64
		Total Allocated [B]: 64
		Total Created [num]: 1
		Average Allocated [B]: 64
		Maximum Instances [num]: 1
		Minimum Instances [num]: 1
		Total Instances [num]: 1
	Scratch Buffer: 
		Maximum [B]: 14998528
		Minimum [B]: 14998528
		Currently Allocated [B]: 14998528
		Total Allocated [B]: 14998528
		Total Created [num]: 1
		Average Allocated [B]: 14998528
ProfilingStack: 
[PROF] Profiling statistics: 
	Scopes entered : 55075
	Scopes exited : 55074
	Overhead per scope [ticks] : 1017.9
	Threads entered : 3
	Threads exited : 0
	============================
		         Sum[Mt]   Parent[%]  Samples[num]       Avg[Mt]   Last/Curr[Mt]        Self[Mt]     Self[%] 	 Name

	main:

		   35744.452 Mt   100.000 %         1 x    35744.456 Mt    35744.451 Mt        1.124 Mt     0.003 % 	 /

		   35743.472 Mt    99.997 %         1 x    35743.478 Mt    35743.471 Mt      426.192 Mt     1.192 % 	 ./Main application loop

		       6.811 Mt     0.019 %         1 x        6.811 Mt        0.000 Mt        6.811 Mt   100.000 % 	 ../TexturedRLInitialization

		       8.200 Mt     0.023 %         1 x        8.200 Mt        0.000 Mt        8.200 Mt   100.000 % 	 ../DeferredRLInitialization

		      53.484 Mt     0.150 %         1 x       53.484 Mt        0.000 Mt        2.627 Mt     4.912 % 	 ../RayTracingRLInitialization

		      50.857 Mt    95.088 %         1 x       50.857 Mt        0.000 Mt       50.857 Mt   100.000 % 	 .../PipelineBuild

		      11.380 Mt     0.032 %         1 x       11.380 Mt        0.000 Mt       11.380 Mt   100.000 % 	 ../ResolveRLInitialization

		    9228.827 Mt    25.820 %         1 x     9228.827 Mt        0.000 Mt        8.431 Mt     0.091 % 	 ../Initialization

		    9220.397 Mt    99.909 %         1 x     9220.397 Mt        0.000 Mt        3.361 Mt     0.036 % 	 .../ScenePrep

		    9084.635 Mt    98.528 %         1 x     9084.635 Mt        0.000 Mt      330.894 Mt     3.642 % 	 ..../SceneLoad

		    8288.113 Mt    91.232 %         1 x     8288.113 Mt        0.000 Mt     1126.218 Mt    13.588 % 	 ...../glTF-LoadScene

		    7161.896 Mt    86.412 %        69 x      103.796 Mt        0.000 Mt     7161.896 Mt   100.000 % 	 ....../glTF-LoadImageData

		     286.088 Mt     3.149 %         1 x      286.088 Mt        0.000 Mt      286.088 Mt   100.000 % 	 ...../glTF-CreateScene

		     179.540 Mt     1.976 %         1 x      179.540 Mt        0.000 Mt      179.540 Mt   100.000 % 	 ...../SceneDesc-CreateScene

		      16.713 Mt     0.181 %         1 x       16.713 Mt        0.000 Mt        0.013 Mt     0.075 % 	 ..../TexturedRLPrep

		      16.701 Mt    99.925 %         1 x       16.701 Mt        0.000 Mt        2.414 Mt    14.451 % 	 ...../PrepareForRendering

		       6.697 Mt    40.101 %         1 x        6.697 Mt        0.000 Mt        6.697 Mt   100.000 % 	 ....../PrepareMaterials

		       7.590 Mt    45.448 %         1 x        7.590 Mt        0.000 Mt        7.590 Mt   100.000 % 	 ....../PrepareDrawBundle

		      14.152 Mt     0.153 %         1 x       14.152 Mt        0.000 Mt        0.012 Mt     0.082 % 	 ..../DeferredRLPrep

		      14.141 Mt    99.918 %         1 x       14.141 Mt        0.000 Mt        5.354 Mt    37.861 % 	 ...../PrepareForRendering

		       0.053 Mt     0.373 %         1 x        0.053 Mt        0.000 Mt        0.053 Mt   100.000 % 	 ....../PrepareMaterials

		       4.309 Mt    30.474 %         1 x        4.309 Mt        0.000 Mt        4.309 Mt   100.000 % 	 ....../BuildMaterialCache

		       4.425 Mt    31.291 %         1 x        4.425 Mt        0.000 Mt        4.425 Mt   100.000 % 	 ....../PrepareDrawBundle

		      32.039 Mt     0.347 %         1 x       32.039 Mt        0.000 Mt        7.875 Mt    24.579 % 	 ..../RayTracingRLPrep

		       1.007 Mt     3.142 %         1 x        1.007 Mt        0.000 Mt        1.007 Mt   100.000 % 	 ...../PrepareAccelerationStructures

		       0.045 Mt     0.140 %         1 x        0.045 Mt        0.000 Mt        0.045 Mt   100.000 % 	 ...../PrepareCache

		       5.961 Mt    18.605 %         1 x        5.961 Mt        0.000 Mt        5.961 Mt   100.000 % 	 ...../BuildCache

		       9.504 Mt    29.664 %         1 x        9.504 Mt        0.000 Mt        0.009 Mt     0.100 % 	 ...../BuildAccelerationStructures

		       9.495 Mt    99.900 %         1 x        9.495 Mt        0.000 Mt        5.089 Mt    53.600 % 	 ....../AccelerationStructureBuild

		       3.240 Mt    34.120 %         1 x        3.240 Mt        0.000 Mt        3.240 Mt   100.000 % 	 ......./BuildBottomLevelAS

		       1.166 Mt    12.280 %         1 x        1.166 Mt        0.000 Mt        1.166 Mt   100.000 % 	 ......./BuildTopLevelAS

		       7.648 Mt    23.870 %         1 x        7.648 Mt        0.000 Mt        7.648 Mt   100.000 % 	 ...../BuildShaderTables

		      69.497 Mt     0.754 %         1 x       69.497 Mt        0.000 Mt       69.497 Mt   100.000 % 	 ..../GpuSidePrep

		   26008.578 Mt    72.765 %     13140 x        1.979 Mt        3.069 Mt      301.277 Mt     1.158 % 	 ../MainLoop

		     716.975 Mt     2.757 %      1563 x        0.459 Mt        0.301 Mt      716.975 Mt   100.000 % 	 .../Update

		   24990.326 Mt    96.085 %     10067 x        2.482 Mt        2.757 Mt    24946.940 Mt    99.826 % 	 .../Render

		      43.386 Mt     0.174 %     10067 x        0.004 Mt        0.003 Mt       43.386 Mt   100.000 % 	 ..../TexturedRLPrep

	WindowThread:

		   35735.033 Mt   100.000 %         1 x    35735.034 Mt    35735.032 Mt    35735.033 Mt   100.000 % 	 /

	GpuThread:

		    4212.576 Mt   100.000 %     10067 x        0.418 Mt        0.426 Mt      166.282 Mt     3.947 % 	 /

		      24.870 Mt     0.590 %         1 x       24.870 Mt        0.000 Mt        0.050 Mt     0.201 % 	 ./ScenePrep

		      24.820 Mt    99.799 %         1 x       24.820 Mt        0.000 Mt        0.003 Mt     0.012 % 	 ../AccelerationStructureBuild

		      24.173 Mt    97.390 %         1 x       24.173 Mt        0.000 Mt       24.173 Mt   100.000 % 	 .../BuildBottomLevelASGpu

		       0.645 Mt     2.598 %         1 x        0.645 Mt        0.000 Mt        0.645 Mt   100.000 % 	 .../BuildTopLevelASGpu

		    3993.337 Mt    94.796 %     10067 x        0.397 Mt        0.423 Mt     3993.337 Mt   100.000 % 	 ./Textured

		      28.086 Mt     0.667 %     10067 x        0.003 Mt        0.003 Mt       28.086 Mt   100.000 % 	 ./Present


	============================


