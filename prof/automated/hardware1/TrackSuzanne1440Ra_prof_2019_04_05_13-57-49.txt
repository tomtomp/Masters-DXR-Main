Configuration: 
Command line parameters  = E:\programming\FIT-DIP\Masters-DXR-Main-git\build\Measurement\x64\Debug\Measurement.exe --scene Suzanne/Suzanne.gltf --profile-track Suzanne/Suzanne.trk --width 2560 --height 1440 --profile-output TrackSuzanne1440Ra 
Window width             = 2560
Window height            = 1421
Window title             = Direct3D App
Back-buffer count        = 3
Target FPS               = 0
Target UPS               = 60
Tearing                  = enabled
VSync                    = disabled
GUI rendering            = disabled
Debug layer              = enabled
GPU validation           = disabled
GPU profiling            = enabled
Render mode              = Rasterization
Loaded scene file        = Suzanne/Suzanne.gltf
Using testing scene      = disabled
Duplication              = 1
Duplication in cube      = disabled
Duplication offset       = 10
BLAS duplication         = disabled
Rays per pixel           = 9

ProfilingBuildAggregator: 
BuildBottomLevelAS	,	2.9131
BuildBottomLevelASGpu	,	0.624224
BuildTopLevelAS	,	1.9323
BuildTopLevelASGpu	,	0.084608

ProfilingAggregator: 
Render	,	1.9738	,	0.8879	,	0.6721	,	0.649	,	1.4881	,	0.7764	,	1.3801	,	1.1328	,	1.5856	,	1.7961	,	1.4477	,	0.9827	,	1.2751	,	0.6165	,	1.5013	,	0.6637	,	0.6839	,	0.6851	,	1.6769	,	1.1278	,	1.4703	,	2.6666	,	0.6344	,	2.2797
Update	,	0.8545	,	0.3018	,	0.3018	,	0.2938	,	0.2966	,	0.3142	,	0.834	,	0.2975	,	0.2961	,	0.3021	,	0.2939	,	0.296	,	0.6742	,	0.3145	,	0.8484	,	0.296	,	0.2997	,	0.2989	,	0.2918	,	0.4693	,	0.3022	,	0.2984	,	0.2939	,	0.2877

FpsAggregator: 
FPS	,	1078	,	1272	,	1259	,	1238	,	1210	,	1235	,	1230	,	1208	,	1206	,	1170	,	1146	,	1210	,	1195	,	1188	,	1157	,	1116	,	1143	,	1124	,	1109	,	1082	,	1113	,	1145	,	1169	,	1123
UPS	,	59	,	61	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60
FrameTime	,	0.929223	,	0.786712	,	0.794351	,	0.808183	,	0.826575	,	0.809976	,	0.813749	,	0.827845	,	0.830328	,	0.855028	,	0.872609	,	0.826585	,	0.837332	,	0.842171	,	0.86513	,	0.896167	,	0.875173	,	0.889728	,	0.901802	,	0.924913	,	0.898531	,	0.874749	,	0.855856	,	0.890947
GigaRays/s	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0

AccelerationStructure: 
Ray tracing acceleration structure building statistics: 
	Bottom Level Acceleration Structure: 
		Maximum [B]: 283264
		Minimum [B]: 283264
		Currently Allocated [B]: 283264
		Currently Created [num]: 1
		Current Average [B]: 283264
		Maximum Triangles [num]: 3936
		Minimum Triangles [num]: 3936
		Total Triangles [num]: 3936
		Maximum Meshes [num]: 1
		Minimum Meshes [num]: 1
		Total Meshes [num]: 1
	Top Level Acceleration Structure: 
		Maximum [B]: 64
		Minimum [B]: 64
		Currently Allocated [B]: 64
		Total Allocated [B]: 64
		Total Created [num]: 1
		Average Allocated [B]: 64
		Maximum Instances [num]: 1
		Minimum Instances [num]: 1
		Total Instances [num]: 1
	Scratch Buffer: 
		Maximum [B]: 83968
		Minimum [B]: 83968
		Currently Allocated [B]: 83968
		Total Allocated [B]: 83968
		Total Created [num]: 1
		Average Allocated [B]: 83968
ProfilingStack: 
[PROF] Profiling statistics: 
	Scopes entered : 146694
	Scopes exited : 146693
	Overhead per scope [ticks] : 1002
	Threads entered : 3
	Threads exited : 0
	============================
		         Sum[Mt]   Parent[%]  Samples[num]       Avg[Mt]   Last/Curr[Mt]        Self[Mt]     Self[%] 	 Name

	main:

		   24725.763 Mt   100.000 %         1 x    24725.765 Mt    24725.763 Mt        1.054 Mt     0.004 % 	 /

		   24724.753 Mt    99.996 %         1 x    24724.754 Mt    24724.753 Mt      452.700 Mt     1.831 % 	 ./Main application loop

		       2.899 Mt     0.012 %         1 x        2.899 Mt        0.000 Mt        2.899 Mt   100.000 % 	 ../TexturedRLInitialization

		       4.864 Mt     0.020 %         1 x        4.864 Mt        0.000 Mt        4.864 Mt   100.000 % 	 ../DeferredRLInitialization

		      47.319 Mt     0.191 %         1 x       47.319 Mt        0.000 Mt        2.473 Mt     5.227 % 	 ../RayTracingRLInitialization

		      44.846 Mt    94.773 %         1 x       44.846 Mt        0.000 Mt       44.846 Mt   100.000 % 	 .../PipelineBuild

		      10.921 Mt     0.044 %         1 x       10.921 Mt        0.000 Mt       10.921 Mt   100.000 % 	 ../ResolveRLInitialization

		     267.443 Mt     1.082 %         1 x      267.443 Mt        0.000 Mt        2.480 Mt     0.927 % 	 ../Initialization

		     264.964 Mt    99.073 %         1 x      264.964 Mt        0.000 Mt        2.543 Mt     0.960 % 	 .../ScenePrep

		     213.685 Mt    80.647 %         1 x      213.685 Mt        0.000 Mt       27.381 Mt    12.814 % 	 ..../SceneLoad

		     164.860 Mt    77.151 %         1 x      164.860 Mt        0.000 Mt       19.987 Mt    12.123 % 	 ...../glTF-LoadScene

		     144.873 Mt    87.877 %         2 x       72.437 Mt        0.000 Mt      144.873 Mt   100.000 % 	 ....../glTF-LoadImageData

		      16.217 Mt     7.589 %         1 x       16.217 Mt        0.000 Mt       16.217 Mt   100.000 % 	 ...../glTF-CreateScene

		       5.227 Mt     2.446 %         1 x        5.227 Mt        0.000 Mt        5.227 Mt   100.000 % 	 ...../SceneDesc-CreateScene

		       3.504 Mt     1.323 %         1 x        3.504 Mt        0.000 Mt        0.016 Mt     0.445 % 	 ..../TexturedRLPrep

		       3.489 Mt    99.555 %         1 x        3.489 Mt        0.000 Mt        0.803 Mt    23.015 % 	 ...../PrepareForRendering

		       1.773 Mt    50.811 %         1 x        1.773 Mt        0.000 Mt        1.773 Mt   100.000 % 	 ....../PrepareMaterials

		       0.913 Mt    26.174 %         1 x        0.913 Mt        0.000 Mt        0.913 Mt   100.000 % 	 ....../PrepareDrawBundle

		       5.061 Mt     1.910 %         1 x        5.061 Mt        0.000 Mt        0.015 Mt     0.296 % 	 ..../DeferredRLPrep

		       5.046 Mt    99.704 %         1 x        5.046 Mt        0.000 Mt        3.259 Mt    64.578 % 	 ...../PrepareForRendering

		       0.052 Mt     1.031 %         1 x        0.052 Mt        0.000 Mt        0.052 Mt   100.000 % 	 ....../PrepareMaterials

		       0.893 Mt    17.689 %         1 x        0.893 Mt        0.000 Mt        0.893 Mt   100.000 % 	 ....../BuildMaterialCache

		       0.843 Mt    16.702 %         1 x        0.843 Mt        0.000 Mt        0.843 Mt   100.000 % 	 ....../PrepareDrawBundle

		      22.146 Mt     8.358 %         1 x       22.146 Mt        0.000 Mt        7.709 Mt    34.808 % 	 ..../RayTracingRLPrep

		       0.769 Mt     3.473 %         1 x        0.769 Mt        0.000 Mt        0.769 Mt   100.000 % 	 ...../PrepareAccelerationStructures

		       0.075 Mt     0.341 %         1 x        0.075 Mt        0.000 Mt        0.075 Mt   100.000 % 	 ...../PrepareCache

		       1.137 Mt     5.135 %         1 x        1.137 Mt        0.000 Mt        1.137 Mt   100.000 % 	 ...../BuildCache

		       9.967 Mt    45.005 %         1 x        9.967 Mt        0.000 Mt        0.021 Mt     0.212 % 	 ...../BuildAccelerationStructures

		       9.946 Mt    99.788 %         1 x        9.946 Mt        0.000 Mt        5.100 Mt    51.282 % 	 ....../AccelerationStructureBuild

		       2.913 Mt    29.290 %         1 x        2.913 Mt        0.000 Mt        2.913 Mt   100.000 % 	 ......./BuildBottomLevelAS

		       1.932 Mt    19.428 %         1 x        1.932 Mt        0.000 Mt        1.932 Mt   100.000 % 	 ......./BuildTopLevelAS

		       2.489 Mt    11.238 %         1 x        2.489 Mt        0.000 Mt        2.489 Mt   100.000 % 	 ...../BuildShaderTables

		      18.024 Mt     6.802 %         1 x       18.024 Mt        0.000 Mt       18.024 Mt   100.000 % 	 ..../GpuSidePrep

		   23938.606 Mt    96.820 %     32704 x        0.732 Mt        1.534 Mt      295.186 Mt     1.233 % 	 ../MainLoop

		     592.783 Mt     2.476 %      1441 x        0.411 Mt        0.294 Mt      592.783 Mt   100.000 % 	 .../Update

		   23050.637 Mt    96.291 %     28128 x        0.819 Mt        1.232 Mt    22952.216 Mt    99.573 % 	 .../Render

		      98.420 Mt     0.427 %     28128 x        0.003 Mt        0.003 Mt       98.420 Mt   100.000 % 	 ..../TexturedRLPrep

	WindowThread:

		   24714.805 Mt   100.000 %         1 x    24714.805 Mt    24714.804 Mt    24714.805 Mt   100.000 % 	 /

	GpuThread:

		     725.986 Mt   100.000 %     28128 x        0.026 Mt        0.008 Mt      160.084 Mt    22.051 % 	 /

		       0.717 Mt     0.099 %         1 x        0.717 Mt        0.000 Mt        0.007 Mt     1.008 % 	 ./ScenePrep

		       0.710 Mt    98.992 %         1 x        0.710 Mt        0.000 Mt        0.001 Mt     0.176 % 	 ../AccelerationStructureBuild

		       0.624 Mt    87.909 %         1 x        0.624 Mt        0.000 Mt        0.624 Mt   100.000 % 	 .../BuildBottomLevelASGpu

		       0.085 Mt    11.915 %         1 x        0.085 Mt        0.000 Mt        0.085 Mt   100.000 % 	 .../BuildTopLevelASGpu

		     486.906 Mt    67.068 %     28128 x        0.017 Mt        0.004 Mt      486.906 Mt   100.000 % 	 ./Textured

		      78.279 Mt    10.782 %     28128 x        0.003 Mt        0.003 Mt       78.279 Mt   100.000 % 	 ./Present


	============================


