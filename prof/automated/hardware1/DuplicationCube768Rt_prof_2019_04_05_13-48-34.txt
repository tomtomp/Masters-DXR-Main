Configuration: 
Command line parameters  = E:\programming\FIT-DIP\Masters-DXR-Main-git\build\Measurement\x64\Debug\Measurement.exe --scene Cube/Cube.gltf --duplication-cube --profile-duplication 10 --rt-mode --width 1024 --height 768 --profile-output DuplicationCube768Rt 
Window width             = 1024
Window height            = 768
Window title             = Direct3D App
Back-buffer count        = 3
Target FPS               = 60
Target UPS               = 60
Tearing                  = disabled
VSync                    = disabled
GUI rendering            = disabled
Debug layer              = enabled
GPU validation           = disabled
GPU profiling            = enabled
Render mode              = Ray Tracing
Loaded scene file        = Cube/Cube.gltf
Using testing scene      = disabled
Duplication              = 10
Duplication in cube      = enabled
Duplication offset       = 10
BLAS duplication         = enabled
Rays per pixel           = 9

ProfilingBuildAggregator: 
BuildBottomLevelAS	,	4.6758	,	23.4835	,	47.7812	,	200.397	,	293.205	,	484.322	,	665.698	,	856.677	,	1398.2
BuildBottomLevelASGpu	,	0.176704	,	4.17347	,	34.298	,	88.2583	,	91.7932	,	99.1672	,	126.571	,	89.1922	,	289.083
BuildTopLevelAS	,	1.5947	,	0.7878	,	2.0173	,	2.9508	,	1.3022	,	1.3955	,	0.9584	,	1.2883	,	1.4886
BuildTopLevelASGpu	,	0.089376	,	0.131392	,	0.400736	,	0.524256	,	0.457984	,	0.292832	,	0.280448	,	0.17776	,	0.279744

ProfilingAggregator: 
Render	,	4.5211	,	3.631	,	7.2332	,	10.8245	,	6.2809	,	7.1799	,	7.3865	,	990.652	,	1235.03	,	2066.16
Update	,	0.4833	,	0.3225	,	0.5543	,	1.0689	,	0.4353	,	0.5278	,	1.4934	,	0.7266	,	0.9187	,	1.1801
RayTracing	,	0.921504	,	0.927008	,	2.67619	,	4.71094	,	2.49344	,	2.81334	,	2.57357	,	130.031	,	90.423	,	290.811
RayTracingRL	,	0.096576	,	0.107776	,	0.522368	,	1.51021	,	0.609792	,	0.682368	,	0.694016	,	1.1903	,	0.152736	,	0.211168

FpsAggregator: 
FPS	,	55	,	54	,	55	,	47	,	29	,	21	,	4	,	1	,	1	,	1
UPS	,	59	,	61	,	59	,	61	,	60	,	60	,	60	,	4	,	70	,	97
FrameTime	,	18.1819	,	18.5246	,	18.2365	,	21.2831	,	34.4828	,	47.9229	,	250.002	,	1236.03	,	1620.3	,	2582.97
GigaRays/s	,	0.389283	,	0.382081	,	0.388117	,	0.332559	,	0.205259	,	0.147693	,	0.0283114	,	0.0057263	,	0.00436825	,	0.00274021

AccelerationStructure: 
Ray tracing acceleration structure building statistics: 
	Bottom Level Acceleration Structure: 
		Maximum [B]: 6208
		Minimum [B]: 6208
		Currently Allocated [B]: 6208000
		Currently Created [num]: 1000
		Current Average [B]: 6208
		Maximum Triangles [num]: 12
		Minimum Triangles [num]: 12
		Total Triangles [num]: 12000
		Maximum Meshes [num]: 1
		Minimum Meshes [num]: 1
		Total Meshes [num]: 1000
	Top Level Acceleration Structure: 
		Maximum [B]: 64000
		Minimum [B]: 64000
		Currently Allocated [B]: 64000
		Total Allocated [B]: 64000
		Total Created [num]: 1
		Average Allocated [B]: 64000
		Maximum Instances [num]: 1000
		Minimum Instances [num]: 1000
		Total Instances [num]: 1000
	Scratch Buffer: 
		Maximum [B]: 79360
		Minimum [B]: 79360
		Currently Allocated [B]: 79360
		Total Allocated [B]: 79360
		Total Created [num]: 1
		Average Allocated [B]: 79360
ProfilingStack: 
[PROF] Profiling statistics: 
	Scopes entered : 615668
	Scopes exited : 615667
	Overhead per scope [ticks] : 1002.5
	Threads entered : 3
	Threads exited : 0
	============================
		         Sum[Mt]   Parent[%]  Samples[num]       Avg[Mt]   Last/Curr[Mt]        Self[Mt]     Self[%] 	 Name

	main:

		   13478.864 Mt   100.000 %         1 x    13478.867 Mt    13478.862 Mt        1.033 Mt     0.008 % 	 /

		   13477.960 Mt    99.993 %         1 x    13477.965 Mt    13477.960 Mt     1888.291 Mt    14.010 % 	 ./Main application loop

		       2.693 Mt     0.020 %         1 x        2.693 Mt        0.000 Mt        2.693 Mt   100.000 % 	 ../TexturedRLInitialization

		       5.739 Mt     0.043 %         1 x        5.739 Mt        0.000 Mt        5.739 Mt   100.000 % 	 ../DeferredRLInitialization

		      71.608 Mt     0.531 %         1 x       71.608 Mt        0.000 Mt        2.412 Mt     3.368 % 	 ../RayTracingRLInitialization

		      69.196 Mt    96.632 %         1 x       69.196 Mt        0.000 Mt       69.196 Mt   100.000 % 	 .../PipelineBuild

		      10.955 Mt     0.081 %         1 x       10.955 Mt        0.000 Mt       10.955 Mt   100.000 % 	 ../ResolveRLInitialization

		     173.996 Mt     1.291 %         1 x      173.996 Mt        0.000 Mt        8.444 Mt     4.853 % 	 ../Initialization

		     165.552 Mt    95.147 %         1 x      165.552 Mt        0.000 Mt        2.439 Mt     1.473 % 	 .../ScenePrep

		     116.856 Mt    70.586 %         1 x      116.856 Mt        0.000 Mt       13.066 Mt    11.181 % 	 ..../SceneLoad

		      59.177 Mt    50.641 %         1 x       59.177 Mt        0.000 Mt       14.309 Mt    24.180 % 	 ...../glTF-LoadScene

		      44.868 Mt    75.820 %         2 x       22.434 Mt        0.000 Mt       44.868 Mt   100.000 % 	 ....../glTF-LoadImageData

		      20.159 Mt    17.251 %         1 x       20.159 Mt        0.000 Mt       20.159 Mt   100.000 % 	 ...../glTF-CreateScene

		      24.455 Mt    20.927 %         1 x       24.455 Mt        0.000 Mt       24.455 Mt   100.000 % 	 ...../SceneDesc-CreateScene

		       2.021 Mt     1.221 %         1 x        2.021 Mt        0.000 Mt        0.007 Mt     0.331 % 	 ..../TexturedRLPrep

		       2.015 Mt    99.669 %         1 x        2.015 Mt        0.000 Mt        0.726 Mt    36.027 % 	 ...../PrepareForRendering

		       0.917 Mt    45.523 %         1 x        0.917 Mt        0.000 Mt        0.917 Mt   100.000 % 	 ....../PrepareMaterials

		       0.372 Mt    18.450 %         1 x        0.372 Mt        0.000 Mt        0.372 Mt   100.000 % 	 ....../PrepareDrawBundle

		       5.444 Mt     3.288 %         1 x        5.444 Mt        0.000 Mt        0.006 Mt     0.110 % 	 ..../DeferredRLPrep

		       5.438 Mt    99.890 %         1 x        5.438 Mt        0.000 Mt        4.185 Mt    76.969 % 	 ...../PrepareForRendering

		       0.019 Mt     0.357 %         1 x        0.019 Mt        0.000 Mt        0.019 Mt   100.000 % 	 ....../PrepareMaterials

		       0.843 Mt    15.511 %         1 x        0.843 Mt        0.000 Mt        0.843 Mt   100.000 % 	 ....../BuildMaterialCache

		       0.390 Mt     7.163 %         1 x        0.390 Mt        0.000 Mt        0.390 Mt   100.000 % 	 ....../PrepareDrawBundle

		      28.045 Mt    16.941 %         1 x       28.045 Mt        0.000 Mt        5.411 Mt    19.293 % 	 ..../RayTracingRLPrep

		       0.327 Mt     1.166 %         1 x        0.327 Mt        0.000 Mt        0.327 Mt   100.000 % 	 ...../PrepareAccelerationStructures

		       0.034 Mt     0.122 %         1 x        0.034 Mt        0.000 Mt        0.034 Mt   100.000 % 	 ...../PrepareCache

		       1.465 Mt     5.224 %         1 x        1.465 Mt        0.000 Mt        1.465 Mt   100.000 % 	 ...../BuildCache

		      13.329 Mt    47.527 %         1 x       13.329 Mt        0.000 Mt        0.010 Mt     0.076 % 	 ...../BuildAccelerationStructures

		      13.319 Mt    99.924 %         1 x       13.319 Mt        0.000 Mt        7.048 Mt    52.920 % 	 ....../AccelerationStructureBuild

		       4.676 Mt    35.107 %         1 x        4.676 Mt        0.000 Mt        4.676 Mt   100.000 % 	 ......./BuildBottomLevelAS

		       1.595 Mt    11.973 %         1 x        1.595 Mt        0.000 Mt        1.595 Mt   100.000 % 	 ......./BuildTopLevelAS

		       7.479 Mt    26.668 %         1 x        7.479 Mt        0.000 Mt        7.479 Mt   100.000 % 	 ...../BuildShaderTables

		      10.746 Mt     6.491 %         1 x       10.746 Mt        0.000 Mt       10.746 Mt   100.000 % 	 ..../GpuSidePrep

		   11324.679 Mt    84.024 %    612583 x        0.018 Mt      243.480 Mt     1705.409 Mt    15.059 % 	 ../MainLoop

		    1803.615 Mt    15.926 %       746 x        2.418 Mt        1.175 Mt     1803.615 Mt   100.000 % 	 .../Update

		    7815.655 Mt    69.014 %       269 x       29.054 Mt       11.809 Mt     2593.053 Mt    33.178 % 	 .../Render

		      86.014 Mt     1.101 %       269 x        0.320 Mt        0.004 Mt        1.392 Mt     1.618 % 	 ..../DeferredRLPrep

		      84.622 Mt    98.382 %        10 x        8.462 Mt        0.000 Mt        6.468 Mt     7.643 % 	 ...../PrepareForRendering

		       8.462 Mt     9.999 %        10 x        0.846 Mt        0.000 Mt        8.462 Mt   100.000 % 	 ....../PrepareMaterials

		       0.009 Mt     0.011 %        10 x        0.001 Mt        0.000 Mt        0.009 Mt   100.000 % 	 ....../BuildMaterialCache

		      69.683 Mt    82.346 %        10 x        6.968 Mt        0.000 Mt       69.683 Mt   100.000 % 	 ....../PrepareDrawBundle

		    5136.588 Mt    65.722 %       269 x       19.095 Mt        0.005 Mt       42.381 Mt     0.825 % 	 ..../RayTracingRLPrep

		     868.819 Mt    16.914 %        10 x       86.882 Mt        0.000 Mt      868.819 Mt   100.000 % 	 ...../PrepareAccelerationStructures

		       8.086 Mt     0.157 %        10 x        0.809 Mt        0.000 Mt        8.086 Mt   100.000 % 	 ...../PrepareCache

		       0.007 Mt     0.000 %        10 x        0.001 Mt        0.000 Mt        0.007 Mt   100.000 % 	 ...../BuildCache

		    4094.002 Mt    79.703 %        10 x      409.400 Mt        0.000 Mt        0.030 Mt     0.001 % 	 ...../BuildAccelerationStructures

		    4093.971 Mt    99.999 %        10 x      409.397 Mt        0.000 Mt       69.911 Mt     1.708 % 	 ....../AccelerationStructureBuild

		    4009.600 Mt    97.939 %        10 x      400.960 Mt        0.000 Mt     4009.600 Mt   100.000 % 	 ......./BuildBottomLevelAS

		      14.460 Mt     0.353 %        10 x        1.446 Mt        0.000 Mt       14.460 Mt   100.000 % 	 ......./BuildTopLevelAS

		     123.292 Mt     2.400 %        10 x       12.329 Mt        0.000 Mt      123.292 Mt   100.000 % 	 ...../BuildShaderTables

	WindowThread:

		   13469.688 Mt   100.000 %         1 x    13469.690 Mt    13469.687 Mt    13469.688 Mt   100.000 % 	 /

	GpuThread:

		    1547.416 Mt   100.000 %       269 x        5.752 Mt        0.749 Mt      132.297 Mt     8.550 % 	 /

		       0.274 Mt     0.018 %         1 x        0.274 Mt        0.000 Mt        0.006 Mt     2.219 % 	 ./ScenePrep

		       0.268 Mt    97.781 %         1 x        0.268 Mt        0.000 Mt        0.002 Mt     0.669 % 	 ../AccelerationStructureBuild

		       0.177 Mt    65.966 %         1 x        0.177 Mt        0.000 Mt        0.177 Mt   100.000 % 	 .../BuildBottomLevelASGpu

		       0.089 Mt    33.365 %         1 x        0.089 Mt        0.000 Mt        0.089 Mt   100.000 % 	 .../BuildTopLevelASGpu

		    1414.317 Mt    91.399 %       269 x        5.258 Mt        0.748 Mt        1.813 Mt     0.128 % 	 ./RayTracing

		      26.901 Mt     1.902 %       269 x        0.100 Mt        0.031 Mt       26.901 Mt   100.000 % 	 ../DeferredRL

		     123.992 Mt     8.767 %       269 x        0.461 Mt        0.152 Mt      123.992 Mt   100.000 % 	 ../RayTracingRL

		     430.552 Mt    30.442 %       269 x        1.601 Mt        0.561 Mt      430.552 Mt   100.000 % 	 ../ResolveRL

		     831.060 Mt    58.761 %        10 x       83.106 Mt        0.000 Mt        0.028 Mt     0.003 % 	 ../AccelerationStructureBuild

		     828.262 Mt    99.663 %        10 x       82.826 Mt        0.000 Mt      828.262 Mt   100.000 % 	 .../BuildBottomLevelASGpu

		       2.770 Mt     0.333 %        10 x        0.277 Mt        0.000 Mt        2.770 Mt   100.000 % 	 .../BuildTopLevelASGpu

		       0.528 Mt     0.034 %       269 x        0.002 Mt        0.000 Mt        0.528 Mt   100.000 % 	 ./Present


	============================


