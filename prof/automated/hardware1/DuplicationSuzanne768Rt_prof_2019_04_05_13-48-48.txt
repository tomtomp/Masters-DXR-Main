Configuration: 
Command line parameters  = E:\programming\FIT-DIP\Masters-DXR-Main-git\build\Measurement\x64\Debug\Measurement.exe --scene Suzanne/Suzanne.gltf --duplication-cube --profile-duplication 10 --rt-mode --width 1024 --height 768 --profile-output DuplicationSuzanne768Rt 
Window width             = 1024
Window height            = 768
Window title             = Direct3D App
Back-buffer count        = 3
Target FPS               = 60
Target UPS               = 60
Tearing                  = disabled
VSync                    = disabled
GUI rendering            = disabled
Debug layer              = enabled
GPU validation           = disabled
GPU profiling            = enabled
Render mode              = Ray Tracing
Loaded scene file        = Suzanne/Suzanne.gltf
Using testing scene      = disabled
Duplication              = 10
Duplication in cube      = enabled
Duplication offset       = 10
BLAS duplication         = enabled
Rays per pixel           = 9

ProfilingBuildAggregator: 
BuildBottomLevelAS	,	1.5744	,	30.3049	,	83.304	,	156.312	,	255.452	,	411.522	,	536.722	,	840.814	,	1203.47
BuildBottomLevelASGpu	,	0.429856	,	17.3408	,	87.3638	,	78.8129	,	97.7715	,	157.236	,	230.791	,	329.941	,	677.88
BuildTopLevelAS	,	1.2447	,	1.1288	,	1.7684	,	0.8558	,	2.8527	,	2.5968	,	1.1147	,	1.4264	,	1.3319
BuildTopLevelASGpu	,	0.059872	,	0.127232	,	0.27056	,	0.164448	,	0.156928	,	0.245856	,	0.131904	,	0.182528	,	0.207008

ProfilingAggregator: 
Render	,	3.4661	,	3.5623	,	5.6381	,	5.079	,	4.3184	,	453.257	,	5.6996	,	944.229	,	1412.95	,	2205.18
Update	,	0.2422	,	0.2726	,	0.2785	,	0.3408	,	0.416	,	0.3989	,	0.6415	,	0.693	,	0.9218	,	1.1736
RayTracing	,	0.913184	,	0.92176	,	2.676	,	1.97645	,	0.880544	,	99.0014	,	0.921184	,	232.195	,	331.666	,	681.608
RayTracingRL	,	0.092608	,	0.100736	,	0.504352	,	0.566496	,	0.132832	,	0.155648	,	0.160192	,	0.385888	,	0.223328	,	0.247456

FpsAggregator: 
FPS	,	57	,	57	,	51	,	33	,	19	,	2	,	8	,	1	,	1	,	1
UPS	,	59	,	61	,	60	,	60	,	60	,	33	,	91	,	6	,	62	,	105
FrameTime	,	17.5879	,	17.5439	,	19.6079	,	30.4468	,	52.6317	,	537.099	,	125.247	,	1122.98	,	1748.31	,	2677.78
GigaRays/s	,	0.402429	,	0.403439	,	0.360972	,	0.232467	,	0.13448	,	0.013178	,	0.0565116	,	0.00630279	,	0.00404841	,	0.00264319

AccelerationStructure: 
Ray tracing acceleration structure building statistics: 
	Bottom Level Acceleration Structure: 
		Maximum [B]: 283264
		Minimum [B]: 283264
		Currently Allocated [B]: 283264000
		Currently Created [num]: 1000
		Current Average [B]: 283264
		Maximum Triangles [num]: 3936
		Minimum Triangles [num]: 3936
		Total Triangles [num]: 3936000
		Maximum Meshes [num]: 1
		Minimum Meshes [num]: 1
		Total Meshes [num]: 1000
	Top Level Acceleration Structure: 
		Maximum [B]: 64000
		Minimum [B]: 64000
		Currently Allocated [B]: 64000
		Total Allocated [B]: 64000
		Total Created [num]: 1
		Average Allocated [B]: 64000
		Maximum Instances [num]: 1000
		Minimum Instances [num]: 1000
		Total Instances [num]: 1000
	Scratch Buffer: 
		Maximum [B]: 83968
		Minimum [B]: 83968
		Currently Allocated [B]: 83968
		Total Allocated [B]: 83968
		Total Created [num]: 1
		Average Allocated [B]: 83968
ProfilingStack: 
[PROF] Profiling statistics: 
	Scopes entered : 677456
	Scopes exited : 677455
	Overhead per scope [ticks] : 1008.7
	Threads entered : 3
	Threads exited : 0
	============================
		         Sum[Mt]   Parent[%]  Samples[num]       Avg[Mt]   Last/Curr[Mt]        Self[Mt]     Self[%] 	 Name

	main:

		   13673.078 Mt   100.000 %         1 x    13673.079 Mt    13673.077 Mt        0.886 Mt     0.006 % 	 /

		   13672.237 Mt    99.994 %         1 x    13672.238 Mt    13672.237 Mt     1835.211 Mt    13.423 % 	 ./Main application loop

		       2.580 Mt     0.019 %         1 x        2.580 Mt        0.000 Mt        2.580 Mt   100.000 % 	 ../TexturedRLInitialization

		       5.203 Mt     0.038 %         1 x        5.203 Mt        0.000 Mt        5.203 Mt   100.000 % 	 ../DeferredRLInitialization

		      56.813 Mt     0.416 %         1 x       56.813 Mt        0.000 Mt        1.966 Mt     3.460 % 	 ../RayTracingRLInitialization

		      54.847 Mt    96.540 %         1 x       54.847 Mt        0.000 Mt       54.847 Mt   100.000 % 	 .../PipelineBuild

		      11.190 Mt     0.082 %         1 x       11.190 Mt        0.000 Mt       11.190 Mt   100.000 % 	 ../ResolveRLInitialization

		     238.087 Mt     1.741 %         1 x      238.087 Mt        0.000 Mt        7.684 Mt     3.228 % 	 ../Initialization

		     230.403 Mt    96.772 %         1 x      230.403 Mt        0.000 Mt        1.409 Mt     0.611 % 	 .../ScenePrep

		     196.135 Mt    85.127 %         1 x      196.135 Mt        0.000 Mt       16.929 Mt     8.631 % 	 ..../SceneLoad

		     164.917 Mt    84.083 %         1 x      164.917 Mt        0.000 Mt       20.161 Mt    12.225 % 	 ...../glTF-LoadScene

		     144.756 Mt    87.775 %         2 x       72.378 Mt        0.000 Mt      144.756 Mt   100.000 % 	 ....../glTF-LoadImageData

		       8.301 Mt     4.232 %         1 x        8.301 Mt        0.000 Mt        8.301 Mt   100.000 % 	 ...../glTF-CreateScene

		       5.989 Mt     3.053 %         1 x        5.989 Mt        0.000 Mt        5.989 Mt   100.000 % 	 ...../SceneDesc-CreateScene

		       1.631 Mt     0.708 %         1 x        1.631 Mt        0.000 Mt        0.005 Mt     0.288 % 	 ..../TexturedRLPrep

		       1.626 Mt    99.712 %         1 x        1.626 Mt        0.000 Mt        0.651 Mt    40.011 % 	 ...../PrepareForRendering

		       0.672 Mt    41.315 %         1 x        0.672 Mt        0.000 Mt        0.672 Mt   100.000 % 	 ....../PrepareMaterials

		       0.304 Mt    18.674 %         1 x        0.304 Mt        0.000 Mt        0.304 Mt   100.000 % 	 ....../PrepareDrawBundle

		       3.163 Mt     1.373 %         1 x        3.163 Mt        0.000 Mt        0.004 Mt     0.136 % 	 ..../DeferredRLPrep

		       3.159 Mt    99.864 %         1 x        3.159 Mt        0.000 Mt        2.352 Mt    74.459 % 	 ...../PrepareForRendering

		       0.018 Mt     0.557 %         1 x        0.018 Mt        0.000 Mt        0.018 Mt   100.000 % 	 ....../PrepareMaterials

		       0.513 Mt    16.234 %         1 x        0.513 Mt        0.000 Mt        0.513 Mt   100.000 % 	 ....../BuildMaterialCache

		       0.276 Mt     8.750 %         1 x        0.276 Mt        0.000 Mt        0.276 Mt   100.000 % 	 ....../PrepareDrawBundle

		      17.125 Mt     7.433 %         1 x       17.125 Mt        0.000 Mt        4.137 Mt    24.156 % 	 ..../RayTracingRLPrep

		       0.238 Mt     1.389 %         1 x        0.238 Mt        0.000 Mt        0.238 Mt   100.000 % 	 ...../PrepareAccelerationStructures

		       0.025 Mt     0.147 %         1 x        0.025 Mt        0.000 Mt        0.025 Mt   100.000 % 	 ...../PrepareCache

		       0.688 Mt     4.020 %         1 x        0.688 Mt        0.000 Mt        0.688 Mt   100.000 % 	 ...../BuildCache

		       9.684 Mt    56.545 %         1 x        9.684 Mt        0.000 Mt        0.007 Mt     0.071 % 	 ...../BuildAccelerationStructures

		       9.677 Mt    99.929 %         1 x        9.677 Mt        0.000 Mt        6.858 Mt    70.867 % 	 ....../AccelerationStructureBuild

		       1.574 Mt    16.270 %         1 x        1.574 Mt        0.000 Mt        1.574 Mt   100.000 % 	 ......./BuildBottomLevelAS

		       1.245 Mt    12.863 %         1 x        1.245 Mt        0.000 Mt        1.245 Mt   100.000 % 	 ......./BuildTopLevelAS

		       2.354 Mt    13.743 %         1 x        2.354 Mt        0.000 Mt        2.354 Mt   100.000 % 	 ...../BuildShaderTables

		      10.940 Mt     4.748 %         1 x       10.940 Mt        0.000 Mt       10.940 Mt   100.000 % 	 ..../GpuSidePrep

		   11523.153 Mt    84.281 %    674619 x        0.017 Mt      268.432 Mt     1393.071 Mt    12.089 % 	 ../MainLoop

		    1735.314 Mt    15.059 %       757 x        2.292 Mt        1.175 Mt     1735.314 Mt   100.000 % 	 .../Update

		    8394.768 Mt    72.851 %       231 x       36.341 Mt       12.097 Mt     3085.749 Mt    36.758 % 	 .../Render

		      97.182 Mt     1.158 %       231 x        0.421 Mt        0.004 Mt        0.833 Mt     0.858 % 	 ..../DeferredRLPrep

		      96.349 Mt    99.142 %        13 x        7.411 Mt        0.000 Mt        5.794 Mt     6.013 % 	 ...../PrepareForRendering

		       9.330 Mt     9.684 %        13 x        0.718 Mt        0.000 Mt        9.330 Mt   100.000 % 	 ....../PrepareMaterials

		       0.011 Mt     0.012 %        13 x        0.001 Mt        0.000 Mt        0.011 Mt   100.000 % 	 ....../BuildMaterialCache

		      81.213 Mt    84.291 %        13 x        6.247 Mt        0.000 Mt       81.213 Mt   100.000 % 	 ....../PrepareDrawBundle

		    5211.837 Mt    62.084 %       231 x       22.562 Mt        0.005 Mt       43.509 Mt     0.835 % 	 ..../RayTracingRLPrep

		     888.383 Mt    17.045 %        13 x       68.337 Mt        0.000 Mt      888.383 Mt   100.000 % 	 ...../PrepareAccelerationStructures

		       9.166 Mt     0.176 %        13 x        0.705 Mt        0.000 Mt        9.166 Mt   100.000 % 	 ...../PrepareCache

		       0.009 Mt     0.000 %        13 x        0.001 Mt        0.000 Mt        0.009 Mt   100.000 % 	 ...../BuildCache

		    4157.604 Mt    79.772 %        13 x      319.816 Mt        0.000 Mt        0.035 Mt     0.001 % 	 ...../BuildAccelerationStructures

		    4157.569 Mt    99.999 %        13 x      319.813 Mt        0.000 Mt       91.799 Mt     2.208 % 	 ....../AccelerationStructureBuild

		    4044.158 Mt    97.272 %        13 x      311.089 Mt        0.000 Mt     4044.158 Mt   100.000 % 	 ......./BuildBottomLevelAS

		      21.612 Mt     0.520 %        13 x        1.662 Mt        0.000 Mt       21.612 Mt   100.000 % 	 ......./BuildTopLevelAS

		     113.165 Mt     2.171 %        13 x        8.705 Mt        0.000 Mt      113.165 Mt   100.000 % 	 ...../BuildShaderTables

	WindowThread:

		   13660.642 Mt   100.000 %         1 x    13660.642 Mt    13660.642 Mt    13660.642 Mt   100.000 % 	 /

	GpuThread:

		    2450.043 Mt   100.000 %       231 x       10.606 Mt        1.414 Mt      119.496 Mt     4.877 % 	 /

		       0.495 Mt     0.020 %         1 x        0.495 Mt        0.000 Mt        0.005 Mt     0.956 % 	 ./ScenePrep

		       0.490 Mt    99.044 %         1 x        0.490 Mt        0.000 Mt        0.001 Mt     0.150 % 	 ../AccelerationStructureBuild

		       0.430 Mt    87.643 %         1 x        0.430 Mt        0.000 Mt        0.430 Mt   100.000 % 	 .../BuildBottomLevelASGpu

		       0.060 Mt    12.207 %         1 x        0.060 Mt        0.000 Mt        0.060 Mt   100.000 % 	 .../BuildTopLevelASGpu

		    2329.664 Mt    95.087 %       231 x       10.085 Mt        1.414 Mt        0.740 Mt     0.032 % 	 ./RayTracing

		      18.398 Mt     0.790 %       231 x        0.080 Mt        0.608 Mt       18.398 Mt   100.000 % 	 ../DeferredRL

		      56.884 Mt     2.442 %       231 x        0.246 Mt        0.250 Mt       56.884 Mt   100.000 % 	 ../RayTracingRL

		     228.966 Mt     9.828 %       231 x        0.991 Mt        0.554 Mt      228.966 Mt   100.000 % 	 ../ResolveRL

		    2024.676 Mt    86.908 %        13 x      155.744 Mt        0.000 Mt        0.024 Mt     0.001 % 	 ../AccelerationStructureBuild

		    2022.350 Mt    99.885 %        13 x      155.565 Mt        0.000 Mt     2022.350 Mt   100.000 % 	 .../BuildBottomLevelASGpu

		       2.302 Mt     0.114 %        13 x        0.177 Mt        0.000 Mt        2.302 Mt   100.000 % 	 .../BuildTopLevelASGpu

		       0.388 Mt     0.016 %       231 x        0.002 Mt        0.000 Mt        0.388 Mt   100.000 % 	 ./Present


	============================


