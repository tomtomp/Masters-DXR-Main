##
# @file DuplicationGraph.R
# @author Tomas Polasek
# @brief Script used for generating the duplication graphs
##

library(stringr)
library(gridExtra)
library(grid)
library(ggplot2)
library(ggpubr)
library(scales)
library(reshape)

### Configuration

# TODO - Load from files...

dataDirectory <- "preset_graph2"
sceneNames <- c("Cube", "Suzanne", "Sponza")

###

dataPath <- paste("../", dataDirectory, sep = "")

cubeFiles <- list.files(dataPath, "PresetCube.*Rt.*", full.names = TRUE)
suzanneFiles <- list.files(dataPath, "PresetSuzanne.*Rt.*", full.names = TRUE)
sponzaFiles <- list.files(dataPath, "PresetSponza.*Rt.*", full.names = TRUE)

cubeNameList <- str_extract(cubeFiles, "(?<=1440Rt).*?(?=_prof)")
suzanneNameList <- str_extract(suzanneFiles, "(?<=1440Rt).*?(?=_prof)")
sponzaNameList <- str_extract(sponzaFiles, "(?<=1440Rt).*?(?=_prof)")
cubeNames <- unique(str_extract(cubeFiles, "(?<=Preset).*?(?=[0-9])"))
suzanneNames <- unique(str_extract(suzanneFiles, "(?<=Preset).*?(?=[0-9])"))
sponzaNames <- unique(str_extract(sponzaFiles, "(?<=Preset).*?(?=[0-9])"))

cubeFileLines <- lapply(cubeFiles, readLines)
suzanneFileLines <- lapply(suzanneFiles, readLines)
sponzaFileLines <- lapply(sponzaFiles, readLines)

cubeFileLineNumbers <- lapply(lapply(cubeFileLines, grep, pattern = "FpsAggregator"), "+", c(1, 2, 3, 4))
suzanneFileLineNumbers <- lapply(lapply(suzanneFileLines, grep, pattern = "FpsAggregator"), "+", c(1, 2, 3, 4))
sponzaFileLineNumbers <- lapply(lapply(sponzaFileLines, grep, pattern = "FpsAggregator"), "+", c(1, 2, 3, 4))

cubeData <- list()
cubeFramesTmp <- list()
cubeFrames <- list()
for (i in 1:length(cubeFiles)) {
  data <- read.csv(text = cubeFileLines[[i]][cubeFileLineNumbers[[i]]], header = FALSE, strip.white = TRUE)
  cubeData[[i]] <- as.data.frame(t(data[, -1]))
  colnames(cubeData[[i]]) <- paste(cubeNameList[i], t(data)[1,])
  cubeFramesTmp[[i]] <- length(cubeData[[i]][[1]])
}
cubeFrames[[1]] <- c(1:cubeFramesTmp[[2]], 1:cubeFramesTmp[[4]], 1:cubeFramesTmp[[6]])
cubeFrames[[2]] <- c(1:cubeFramesTmp[[1]], 1:cubeFramesTmp[[3]], 1:cubeFramesTmp[[5]])

suzanneData <- list()
suzanneFramesTmp <- list()
suzanneFrames <- list()
for (i in 1:length(suzanneFiles)) {
  data <- read.csv(text = suzanneFileLines[[i]][suzanneFileLineNumbers[[i]]], header = FALSE, strip.white = TRUE)
  suzanneData[[i]] <- as.data.frame(t(data[, -1]))
  colnames(suzanneData[[i]]) <- paste(suzanneNameList[i], t(data)[1,])
  suzanneFramesTmp[[i]] <- length(suzanneData[[i]][[1]])
}
suzanneFrames[[1]] <- c(1:suzanneFramesTmp[[2]], 1:suzanneFramesTmp[[4]], 1:suzanneFramesTmp[[6]])
suzanneFrames[[2]] <- c(1:suzanneFramesTmp[[1]], 1:suzanneFramesTmp[[3]], 1:suzanneFramesTmp[[5]])

sponzaData <- list()
sponzaFramesTmp <- list()
sponzaFrames <- list()
for (i in 1:length(sponzaFiles)) {
  data <- read.csv(text = sponzaFileLines[[i]][sponzaFileLineNumbers[[i]]], header = FALSE, strip.white = TRUE)
  sponzaData[[i]] <- as.data.frame(t(data[, -1]))
  colnames(sponzaData[[i]]) <- paste(sponzaNameList[i], t(data)[1,])
  sponzaFramesTmp[[i]] <- length(sponzaData[[i]][[1]])
}
sponzaFrames[[1]] <- c(1:sponzaFramesTmp[[2]], 1:sponzaFramesTmp[[4]], 1:sponzaFramesTmp[[6]])
sponzaFrames[[2]] <- c(1:sponzaFramesTmp[[1]], 1:sponzaFramesTmp[[3]], 1:sponzaFramesTmp[[5]])

#ggplot(data = melt(rtData), aes(x = c(rep(1:9, 4), rep(1:7, 4), rep(1:9, 4)), y = value, color = variable)) + geom_point()
#ggplot(data = melt(rtData), aes(x = c(rep(1:9, 4), rep(1:7, 4), rep(1:9, 4)), y = value, color = variable)) + geom_line() + scale_y_sqrt()
#ggsave("test-ggsave.pdf")

#ggplot(data = subset(melt(rtData), grepl("Gpu", variable)), 
#       aes(x = c(rep(1:9, 2), rep(1:7, 2), rep(1:9, 2)), y = value, color = variable)) + 
#  geom_line(size = 1.5) + scale_y_continuous(trans = log2_trans())

#ggplot(data = subset(melt(rtData), !grepl("Gpu", variable)), 
#       aes(x = c(rep(1:9, 2), rep(1:7, 2), rep(1:9, 2)), y = value, color = variable)) + 
#  geom_line(size = 1.5) + scale_y_continuous(trans = log2_trans())

giga_mult <- function(trans, format = scientific_format()) {
  if (is.character(trans)) 
    trans <- match.fun(trans)
  force(format)
  function(x) {
    x <- trans(x * 1000000000)
    format(x)
  }
}

##############################################################################################################################

cubeGigaRays <- ggplot(data = subset(melt(cubeData), grepl("*GigaRays", variable)), 
                       aes(x = rep(1:cubeFramesTmp[[1]], length(cubeData)), y = value, 
                           color = factor(variable, labels = cubeNameList), shape = factor(variable, labels = cubeNameList))) + 
  geom_line(size = 1.2) + 
  geom_point(size = 2.5) + 
  scale_y_continuous(trans = log10_trans(), 
                     breaks = scales::trans_breaks("log10", function(x) 10^x),
                     labels = giga_mult("log10", scales::math_format(10^.x))) + 
  labs(title = "Cube", colour = "Scenes", shape = "Scenes", x = "Frame [idx]", y = "Performance [Rays/s]") + 
  theme_bw() + 
  theme(plot.title = element_text(size = 24, face = "bold"), 
        axis.text=element_text(size = 16), 
        axis.title=element_text(size = 18, face = "bold"), 
        legend.text = element_text(size = 20), 
        legend.title = element_text(size = 22, face = "bold"))

suzanneGigaRays <- ggplot(data = subset(melt(suzanneData), grepl("*GigaRays", variable)), 
                       aes(x = rep(1:suzanneFramesTmp[[1]], length(suzanneData)), y = value, 
                           color = factor(variable, labels = suzanneNameList), shape = factor(variable, labels = suzanneNameList))) + 
  geom_line(size = 1.2) + 
  geom_point(size = 2.5) + 
  scale_y_continuous(trans = log10_trans(), 
                     breaks = scales::trans_breaks("log10", function(x) 10^x),
                     labels = giga_mult("log10", scales::math_format(10^.x))) + 
  labs(title = "Suzanne", colour = "Scenes", shape = "Scenes", x = "Frame [idx]", y = "Performance [Rays/s]") + 
  theme_bw() + 
  theme(plot.title = element_text(size = 24, face = "bold"), 
        axis.text=element_text(size = 16), 
        axis.title=element_text(size = 18, face = "bold"), 
        legend.text = element_text(size = 20), 
        legend.title = element_text(size = 22, face = "bold"))

sponzaGigaRays <- ggplot(data = subset(melt(sponzaData), grepl("*GigaRays", variable)), 
                          aes(x = rep(1:sponzaFramesTmp[[1]], length(sponzaData)), y = value, 
                              color = factor(variable, labels = sponzaNameList), shape = factor(variable, labels = sponzaNameList))) + 
  geom_line(size = 1.2) + 
  geom_point(size = 2.5) + 
  scale_y_continuous(trans = log10_trans(), 
                     breaks = scales::trans_breaks("log10", function(x) 10^x),
                     labels = giga_mult("log10", scales::math_format(10^.x))) + 
  labs(title = "Sponza", colour = "Scenes", shape = "Scenes", x = "Frame [idx]", y = "Performance [Rays/s]") + 
  theme_bw() + 
  theme(plot.title = element_text(size = 24, face = "bold"), 
        axis.text=element_text(size = 16), 
        axis.title=element_text(size = 18, face = "bold"), 
        legend.text = element_text(size = 20), 
        legend.title = element_text(size = 22, face = "bold"))

ggarrange(cubeGigaRays, suzanneGigaRays, sponzaGigaRays, ncol = 2, nrow = 2, common.legend = TRUE, legend = "bottom")

ggsave(paste(dataDirectory, "Preset1440GigaRays.pdf", sep = "_"))

##############################################################################################################################

cubeFps <- ggplot(data = subset(melt(cubeData), grepl("*FPS", variable)), 
                  aes(x = rep(1:cubeFramesTmp[[1]], length(cubeData)), y = value, 
                      color = factor(variable, labels = cubeNameList), shape = factor(variable, labels = cubeNameList))) + 
  geom_line(size = 1.2) + 
  geom_point(size = 2.5) + 
  scale_y_continuous(limits = c(0, NA)) + 
  labs(title = "Cube", colour = "Scenes", shape = "Scenes", x = "Frame [idx]", y = "Performance [Frames/s]") + 
  theme_bw() + 
  theme(plot.title = element_text(size = 24, face = "bold"), 
        axis.text=element_text(size = 16), 
        axis.title=element_text(size = 18, face = "bold"), 
        legend.text = element_text(size = 20), 
        legend.title = element_text(size = 22, face = "bold"))

suzanneFps <- ggplot(data = subset(melt(suzanneData), grepl("*FPS", variable)), 
                  aes(x = rep(1:suzanneFramesTmp[[1]], length(suzanneData)), y = value, 
                      color = factor(variable, labels = suzanneNameList), shape = factor(variable, labels = suzanneNameList))) + 
  geom_line(size = 1.2) + 
  geom_point(size = 2.5) + 
  scale_y_continuous(limits = c(0, NA)) + 
  labs(title = "Suzanne", colour = "Scenes", shape = "Scenes", x = "Frame [idx]", y = "Performance [Frames/s]") + 
  theme_bw() + 
  theme(plot.title = element_text(size = 24, face = "bold"), 
        axis.text=element_text(size = 16), 
        axis.title=element_text(size = 18, face = "bold"), 
        legend.text = element_text(size = 20), 
        legend.title = element_text(size = 22, face = "bold"))

sponzaFps <- ggplot(data = subset(melt(sponzaData), grepl("*FPS", variable)), 
                     aes(x = rep(1:sponzaFramesTmp[[1]], length(sponzaData)), y = value, 
                         color = factor(variable, labels = sponzaNameList), shape = factor(variable, labels = sponzaNameList))) + 
  geom_line(size = 1.2) + 
  geom_point(size = 2.5) + 
  scale_y_continuous(limits = c(0, NA)) + 
  labs(title = "Sponza", colour = "Scenes", shape = "Scenes", x = "Frame [idx]", y = "Performance [Frames/s]") + 
  theme_bw() + 
  theme(plot.title = element_text(size = 24, face = "bold"), 
        axis.text=element_text(size = 16), 
        axis.title=element_text(size = 18, face = "bold"), 
        legend.text = element_text(size = 20), 
        legend.title = element_text(size = 22, face = "bold"))

ggarrange(cubeFps, suzanneFps, sponzaFps, ncol = 2, nrow = 2, common.legend = TRUE, legend = "bottom")

ggsave(paste(dataDirectory, "Preset1440Fps.pdf", sep = "_"))

##############################################################################################################################
