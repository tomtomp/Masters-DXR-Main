##
# @file DuplicationGraph.R
# @author Tomas Polasek
# @brief Script used for generating the duplication graphs
##

library(stringr)
library(gridExtra)
library(grid)
library(ggplot2)
library(ggpubr)
library(scales)
library(reshape)

### Configuration

# TODO - Load from files...
# Counts in order - Cube, Sponza, Suzanne
triangleCounts <- c(12, 262267, 3936)
meshCounts <- c(1, 103, 1)
cubeMeasurements <- 9
sponzaMeasurements <- 6
suzanneMeasurements <- 9
dataDirectory <- "hardware_release5"

###

dataPath <- paste("../", dataDirectory, sep = "")

rtFiles <- list.files(dataPath, "Duplication.*Rt.*", full.names = TRUE)
rfFiles <- list.files(dataPath, "Duplication.*Rf.*", full.names = TRUE)

rtNames <- str_extract(rtFiles, "(?<=Duplication).*?(?=[0-9])")
rfNames <- str_extract(rfFiles, "(?<=Duplication).*?(?=[0-9])")

rtFileLines <- lapply(rtFiles, readLines)
rfFileLines <- lapply(rfFiles, readLines)

rtFileLineNumbers <- lapply(lapply(rtFileLines, grep, pattern = "ProfilingBuildAggregator"), "+", c(1:11))
rfFileLineNumbers <- lapply(lapply(rfFileLines, grep, pattern = "ProfilingBuildAggregator"), "+", c(1:11))

rtData <- list()
for (i in 1:length(rtFiles)) {
  data <- read.csv(text = rtFileLines[[i]][rtFileLineNumbers[[i]]], header = FALSE, strip.white = TRUE)
  rtData[[i]] <- as.data.frame(t(data[, -1]))
  rtData[[i]] <- cbind(rtData[[i]], TotalTime=apply(rtData[[i]][1:4], 1, sum, na.rm = TRUE))
  rtData[[i]] <- cbind(rtData[[i]], TotalSize=apply(rtData[[i]][9:10], 1, sum, na.rm = TRUE))
  colnames(rtData[[i]]) <- paste(rtNames[i], c(t(data)[1,], "TotalTime", "TotalSize"))
}

rtTriangles <- subset(melt(rtData), grepl("Triangles", variable))$value
rtMeshes <- subset(melt(rtData), grepl("Meshes", variable))$value
rtBottomLevels <- subset(melt(rtData), grepl("BottomLevels", variable) & !grepl("BottomLevelsSize", variable))$value
rtTopLevelSize <- subset(melt(rtData), grepl("TopLevelSize", variable))$value
rtBottomLevelSize <- subset(melt(rtData), grepl("BottomLevelsSize", variable))$value

rfData <- list()
for (i in 1:length(rfFiles)) {
  data <- read.csv(text = rfFileLines[[i]][rfFileLineNumbers[[i]]], header = FALSE, strip.white = TRUE)
  rfData[[i]] <- as.data.frame(t(data[, -1]))
  rfData[[i]] <- cbind(rfData[[i]], TotalTime=apply(rfData[[i]][1:4], 1, sum, na.rm = TRUE))
  rfData[[i]] <- cbind(rfData[[i]], TotalTime=apply(rfData[[i]][9:10], 1, sum, na.rm = TRUE))
  colnames(rfData[[i]]) <- paste(rfNames[i], c(t(data)[1,], "TotalTime", "TotalSize"))
}

rfTriangles <- subset(melt(rfData), grepl("Triangles", variable))$value
rfMeshes <- subset(melt(rfData), grepl("Meshes", variable))$value
rfBottomLevels <- subset(melt(rfData), grepl("BottomLevels", variable))$value
rfTopLevelSize <- subset(melt(rfData), grepl("TopLevelSize", variable))$value
rfBottomLevelSize <- subset(melt(rfData), grepl("BottomLevelsSize", variable))$value

#ggplot(data = melt(rtData), aes(x = c(rep(1:9, 4), rep(1:7, 4), rep(1:9, 4)), y = value, color = variable)) + geom_point()
#ggplot(data = melt(rtData), aes(x = c(rep(1:9, 4), rep(1:7, 4), rep(1:9, 4)), y = value, color = variable)) + geom_line() + scale_y_sqrt()
#ggsave("test-ggsave.pdf")

#ggplot(data = subset(melt(rtData), grepl("Gpu", variable)), 
#       aes(x = c(rep(1:9, 2), rep(1:7, 2), rep(1:9, 2)), y = value, color = variable)) + 
#  geom_line(size = 1.5) + scale_y_continuous(trans = log2_trans())

#ggplot(data = subset(melt(rtData), !grepl("Gpu", variable)), 
#       aes(x = c(rep(1:9, 2), rep(1:7, 2), rep(1:9, 2)), y = value, color = variable)) + 
#  geom_line(size = 1.5) + scale_y_continuous(trans = log2_trans())

##############################################################################################################################

bottomGpu <- ggplot(data = subset(melt(rtData), grepl("Bottom.*Gpu", variable)), 
                    aes(x = rtTriangles, y = value, 
                        color = factor(variable, labels = rtNames), shape = factor(variable, labels = rtNames))) + 
  geom_line(size = 1.2) + 
  geom_point(size = 2.5) + 
  scale_x_continuous(trans = log10_trans(), 
                     breaks = scales::trans_breaks("log10", function(x) 10^x), 
                     labels = scales::trans_format("log10", scales::math_format(10^.x))) + 
  scale_y_continuous(trans = log10_trans(), 
                     breaks = scales::trans_breaks("log10", function(x) 10^x), 
                     labels = scales::trans_format("log10", scales::math_format(10^.x))) + 
  labs(title = "Spodní GPU", colour = "Scenes", shape = "Scenes", x = "Triangles [n]", y = "Time [ms]") + 
  theme_bw() + 
  theme(plot.title = element_text(size = 24, face = "bold"), 
        axis.text=element_text(size = 16), 
        axis.title=element_text(size = 18, face = "bold"), 
        legend.text = element_text(size = 20), 
        legend.title = element_text(size = 22, face = "bold"))

topGpu <- ggplot(data = subset(melt(rtData), grepl("Top.*Gpu", variable)), 
                 aes(x = rtTriangles, y = value, 
                     color = factor(variable, labels = rtNames), shape = factor(variable, labels = rtNames))) + 
  geom_line(size = 1.2) + 
  geom_point(size = 2.5) + 
  scale_x_continuous(trans = log10_trans(), 
                     breaks = scales::trans_breaks("log10", function(x) 10^x), 
                     labels = scales::trans_format("log10", scales::math_format(10^.x))) + 
  scale_y_continuous(trans = log10_trans(), expand = expand_scale(mult = 2), 
                     breaks = scales::trans_breaks("log10", function(x) 10^x), 
                     labels = scales::trans_format("log10", scales::math_format(10^.x))) + 
  labs(title = "Vrchní GPU", colour = "Scenes", shape = "Scenes", x = "Triangles [n]", y = "Time [ms]") + 
  theme_bw() + 
  theme(plot.title = element_text(size = 24, face = "bold"), 
        axis.text=element_text(size = 16), 
        axis.title=element_text(size = 18, face = "bold"), 
        legend.text = element_text(size = 20), 
        legend.title = element_text(size = 22, face = "bold"))

bottomCpu <- ggplot(data = subset(melt(rtData), grepl("BottomLevelAS$", variable)), 
                    aes(x = rtTriangles, y = value, 
                        color = factor(variable, labels = rtNames), shape = factor(variable, labels = rtNames))) + 
  geom_line(size = 1.2) + 
  geom_point(size = 2.5) + 
  scale_x_continuous(trans = log10_trans(), 
                     breaks = scales::trans_breaks("log10", function(x) 10^x), 
                     labels = scales::trans_format("log10", scales::math_format(10^.x))) + 
  scale_y_continuous(trans = log10_trans(), 
                     breaks = scales::trans_breaks("log10", function(x) 10^x), 
                     labels = scales::trans_format("log10", scales::math_format(10^.x))) + 
  labs(title = "Spodní CPU", colour = "Scenes", shape = "Scenes", x = "Triangles [n]", y = "Time [ms]") + 
  theme_bw() + 
  theme(plot.title = element_text(size = 24, face = "bold"), 
        axis.text=element_text(size = 16), 
        axis.title=element_text(size = 18, face = "bold"), 
        legend.text = element_text(size = 20), 
        legend.title = element_text(size = 22, face = "bold"))

topCpu <- ggplot(data = subset(melt(rtData), grepl("TopLevelAS$", variable)), 
                 aes(x = rtTriangles, y = value, 
                        color = factor(variable, labels = rtNames), shape = factor(variable, labels = rtNames))) + 
  geom_line(size = 1.2) + 
  geom_point(size = 2.5) + 
  scale_x_continuous(trans = log10_trans(), 
                     breaks = scales::trans_breaks("log10", function(x) 10^x), 
                     labels = scales::trans_format("log10", scales::math_format(10^.x))) + 
  scale_y_continuous(trans = log10_trans(), expand = expand_scale(mult = 2), 
                     breaks = scales::trans_breaks("log10", function(x) 10^x), 
                     labels = scales::trans_format("log10", scales::math_format(10^.x))) + 
  labs(title = "Vrchní CPU", colour = "Scenes", shape = "Scenes", x = "Triangles [n]", y = "Time [ms]") + 
  theme_bw() + 
  theme(plot.title = element_text(size = 24, face = "bold"), 
        axis.text=element_text(size = 16), 
        axis.title=element_text(size = 18, face = "bold"), 
        legend.text = element_text(size = 20), 
        legend.title = element_text(size = 22, face = "bold"))

ggarrange(bottomGpu, topGpu, bottomCpu, topCpu, ncol = 2, nrow = 2, common.legend = TRUE, legend = "bottom")

ggsave(paste(dataDirectory, "DuplicationTrianglesCs.pdf", sep = "_"))

##############################################################################################################################

bottomGpu <- ggplot(data = subset(melt(rtData), grepl("Bottom.*Gpu", variable)), 
                    aes(x = rtMeshes, y = value, 
                        color = factor(variable, labels = rtNames), shape = factor(variable, labels = rtNames))) + 
  geom_line(size = 1.2) + 
  geom_point(size = 2.5) + 
  scale_x_continuous(trans = log10_trans(), 
                     breaks = scales::trans_breaks("log10", function(x) 10^x), 
                     labels = scales::trans_format("log10", scales::math_format(10^.x))) + 
  scale_y_continuous(trans = log10_trans(), 
                     breaks = scales::trans_breaks("log10", function(x) 10^x), 
                     labels = scales::trans_format("log10", scales::math_format(10^.x))) + 
  labs(title = "GPU Bottom", colour = "Scenes", shape = "Scenes", x = "Meshes [n]", y = "Time [ms]") + 
  theme_bw() + 
  theme(plot.title = element_text(size = 24, face = "bold"), 
        axis.text=element_text(size = 16), 
        axis.title=element_text(size = 18, face = "bold"), 
        legend.text = element_text(size = 20), 
        legend.title = element_text(size = 22, face = "bold"))

topGpu <- ggplot(data = subset(melt(rtData), grepl("Top.*Gpu", variable)), 
                 aes(x = rtMeshes, y = value, 
                     color = factor(variable, labels = rtNames), shape = factor(variable, labels = rtNames))) + 
  geom_line(size = 1.2) + 
  geom_point(size = 2.5) + 
  scale_x_continuous(trans = log10_trans(), 
                     breaks = scales::trans_breaks("log10", function(x) 10^x), 
                     labels = scales::trans_format("log10", scales::math_format(10^.x))) + 
  scale_y_continuous(trans = log10_trans(), expand = expand_scale(mult = 2), 
                     breaks = scales::trans_breaks("log10", function(x) 10^x), 
                     labels = scales::trans_format("log10", scales::math_format(10^.x))) + 
  labs(title = "GPU Top", colour = "Scenes", shape = "Scenes", x = "Meshes [n]", y = "Time [ms]") + 
  theme_bw() + 
  theme(plot.title = element_text(size = 24, face = "bold"), 
        axis.text=element_text(size = 16), 
        axis.title=element_text(size = 18, face = "bold"), 
        legend.text = element_text(size = 20), 
        legend.title = element_text(size = 22, face = "bold"))

bottomCpu <- ggplot(data = subset(melt(rtData), grepl("BottomLevelAS$", variable)), 
                    aes(x = rtMeshes, y = value, 
                        color = factor(variable, labels = rtNames), shape = factor(variable, labels = rtNames))) + 
  geom_line(size = 1.2) + 
  geom_point(size = 2.5) + 
  scale_x_continuous(trans = log10_trans(), 
                     breaks = scales::trans_breaks("log10", function(x) 10^x), 
                     labels = scales::trans_format("log10", scales::math_format(10^.x))) + 
  scale_y_continuous(trans = log10_trans(), 
                     breaks = scales::trans_breaks("log10", function(x) 10^x), 
                     labels = scales::trans_format("log10", scales::math_format(10^.x))) + 
  labs(title = "CPU Bottom", colour = "Scenes", shape = "Scenes", x = "Meshes [n]", y = "Time [ms]") + 
  theme_bw() + 
  theme(plot.title = element_text(size = 24, face = "bold"), 
        axis.text=element_text(size = 16), 
        axis.title=element_text(size = 18, face = "bold"), 
        legend.text = element_text(size = 20), 
        legend.title = element_text(size = 22, face = "bold"))

topCpu <- ggplot(data = subset(melt(rtData), grepl("TopLevelAS$", variable)), 
                 aes(x = rtMeshes, y = value, 
                     color = factor(variable, labels = rtNames), shape = factor(variable, labels = rtNames))) + 
  geom_line(size = 1.2) + 
  geom_point(size = 2.5) + 
  scale_x_continuous(trans = log10_trans(), 
                     breaks = scales::trans_breaks("log10", function(x) 10^x), 
                     labels = scales::trans_format("log10", scales::math_format(10^.x))) + 
  scale_y_continuous(trans = log10_trans(), expand = expand_scale(mult = 2), 
                     breaks = scales::trans_breaks("log10", function(x) 10^x), 
                     labels = scales::trans_format("log10", scales::math_format(10^.x))) + 
  labs(title = "CPU Top", colour = "Scenes", shape = "Scenes", x = "Meshes [n]", y = "Time [ms]") + 
  theme_bw() + 
  theme(plot.title = element_text(size = 24, face = "bold"), 
        axis.text=element_text(size = 16), 
        axis.title=element_text(size = 18, face = "bold"), 
        legend.text = element_text(size = 20), 
        legend.title = element_text(size = 22, face = "bold"))

ggarrange(bottomGpu, topGpu, bottomCpu, topCpu, ncol = 2, nrow = 2, common.legend = TRUE, legend = "bottom")

ggsave(paste(dataDirectory, "DuplicationMeshes.pdf", sep = "_"))

##############################################################################################################################

bottomGpu <- ggplot(data = subset(melt(rtData), grepl("Bottom.*Gpu", variable)), 
                    aes(x = rtBottomLevels, y = value, 
                        color = factor(variable, labels = rtNames), shape = factor(variable, labels = rtNames))) + 
  geom_line(size = 1.2) + 
  geom_point(size = 2.5) + 
  scale_x_continuous(trans = log10_trans(), 
                     breaks = scales::trans_breaks("log10", function(x) 10^x), 
                     labels = scales::trans_format("log10", scales::math_format(10^.x))) + 
  scale_y_continuous(trans = log10_trans(), 
                     breaks = scales::trans_breaks("log10", function(x) 10^x), 
                     labels = scales::trans_format("log10", scales::math_format(10^.x))) + 
  labs(title = "GPU Bottom", colour = "Scenes", shape = "Scenes", x = "Instances [n]", y = "Time [ms]") + 
  theme_bw() + 
  theme(plot.title = element_text(size = 24, face = "bold"), 
        axis.text=element_text(size = 16), 
        axis.title=element_text(size = 18, face = "bold"), 
        legend.text = element_text(size = 20), 
        legend.title = element_text(size = 22, face = "bold"))

topGpu <- ggplot(data = subset(melt(rtData), grepl("Top.*Gpu", variable)), 
                 aes(x = rtBottomLevels, y = value, 
                     color = factor(variable, labels = rtNames), shape = factor(variable, labels = rtNames))) + 
  geom_line(size = 1.2) + 
  geom_point(size = 2.5) + 
  scale_x_continuous(trans = log10_trans(), 
                     breaks = scales::trans_breaks("log10", function(x) 10^x), 
                     labels = scales::trans_format("log10", scales::math_format(10^.x))) + 
  scale_y_continuous(trans = log10_trans(), expand = expand_scale(mult = 2), 
                     breaks = scales::trans_breaks("log10", function(x) 10^x), 
                     labels = scales::trans_format("log10", scales::math_format(10^.x))) + 
  labs(title = "GPU Top", colour = "Scenes", shape = "Scenes", x = "Instances [n]", y = "Time [ms]") + 
  theme_bw() + 
  theme(plot.title = element_text(size = 24, face = "bold"), 
        axis.text=element_text(size = 16), 
        axis.title=element_text(size = 18, face = "bold"), 
        legend.text = element_text(size = 20), 
        legend.title = element_text(size = 22, face = "bold"))

bottomCpu <- ggplot(data = subset(melt(rtData), grepl("BottomLevelAS$", variable)), 
                    aes(x = rtBottomLevels, y = value, 
                        color = factor(variable, labels = rtNames), shape = factor(variable, labels = rtNames))) + 
  geom_line(size = 1.2) + 
  geom_point(size = 2.5) + 
  scale_x_continuous(trans = log10_trans(), 
                     breaks = scales::trans_breaks("log10", function(x) 10^x), 
                     labels = scales::trans_format("log10", scales::math_format(10^.x))) + 
  scale_y_continuous(trans = log10_trans(), 
                     breaks = scales::trans_breaks("log10", function(x) 10^x), 
                     labels = scales::trans_format("log10", scales::math_format(10^.x))) + 
  labs(title = "CPU Bottom", colour = "Scenes", shape = "Scenes", x = "Instances [n]", y = "Time [ms]") + 
  theme_bw() + 
  theme(plot.title = element_text(size = 24, face = "bold"), 
        axis.text=element_text(size = 16), 
        axis.title=element_text(size = 18, face = "bold"), 
        legend.text = element_text(size = 20), 
        legend.title = element_text(size = 22, face = "bold"))

topCpu <- ggplot(data = subset(melt(rtData), grepl("TopLevelAS$", variable)), 
                 aes(x = rtBottomLevels, y = value, 
                     color = factor(variable, labels = rtNames), shape = factor(variable, labels = rtNames))) + 
  geom_line(size = 1.2) + 
  geom_point(size = 2.5) + 
  scale_x_continuous(trans = log10_trans(), 
                     breaks = scales::trans_breaks("log10", function(x) 10^x), 
                     labels = scales::trans_format("log10", scales::math_format(10^.x))) + 
  scale_y_continuous(trans = log10_trans(), expand = expand_scale(mult = 2), 
                     breaks = scales::trans_breaks("log10", function(x) 10^x), 
                     labels = scales::trans_format("log10", scales::math_format(10^.x))) + 
  labs(title = "CPU Top", colour = "Scenes", shape = "Scenes", x = "Instances [n]", y = "Time [ms]") + 
  theme_bw() + 
  theme(plot.title = element_text(size = 24, face = "bold"), 
        axis.text=element_text(size = 16), 
        axis.title=element_text(size = 18, face = "bold"), 
        legend.text = element_text(size = 20), 
        legend.title = element_text(size = 22, face = "bold"))

ggarrange(bottomGpu, topGpu, bottomCpu, topCpu, ncol = 2, nrow = 2, common.legend = TRUE, legend = "bottom")

ggsave(paste(dataDirectory, "DuplicationLevels.pdf", sep = "_"))

##############################################################################################################################

fancy_format <- function(l) {
  format(l, nsmall = 1, scientific = FALSE)
}

##############################################################################################################################

normalTotal <- ggplot(data = subset(melt(rtData), grepl("TotalTime", variable)), 
       aes(x = rtTriangles, y = value, 
           color = factor(variable, labels = rtNames), shape = factor(variable, labels = rtNames))) + 
  geom_line(size = 1.2) + 
  geom_point(size = 2.5) + 
  scale_x_continuous(trans = log10_trans(), 
                     breaks = scales::trans_breaks("log10", function(x) 10^x), 
                     labels = scales::trans_format("log10", scales::math_format(10^.x))) + 
  scale_y_continuous(limits = c(0.0, NA), labels = fancy_format, breaks = pretty_breaks(n = 8)) + 
  #scale_y_continuous(trans = log10_trans(), 
                     #breaks = scales::trans_breaks("log10", function(x) 10^x), 
                     #labels = scales::trans_format("log10", scales::math_format(10^.x))) + 
  labs(title = "Normal Build", colour = "Scenes", shape = "Scenes", x = "Triangles [n]", y = "Time [ms]") + 
  theme_bw() + 
  theme(plot.title = element_text(size = 24, face = "bold"), 
        axis.text=element_text(size = 16), 
        axis.title=element_text(size = 18, face = "bold"), 
        legend.text = element_text(size = 20), 
        legend.title = element_text(size = 22, face = "bold"))

fastTotal <- ggplot(data = subset(melt(rfData), grepl("TotalTime", variable)), 
                      aes(x = rfTriangles, y = value, 
                          color = factor(variable, labels = rtNames), shape = factor(variable, labels = rtNames))) + 
  geom_line(size = 1.2) + 
  geom_point(size = 2.5) + 
  scale_x_continuous(trans = log10_trans(), 
                     breaks = scales::trans_breaks("log10", function(x) 10^x), 
                     labels = scales::trans_format("log10", scales::math_format(10^.x))) + 
  scale_y_continuous(limits = c(0.0, NA), labels = fancy_format, breaks = pretty_breaks(n = 8)) + 
  #scale_y_continuous(trans = log10_trans(), 
                     #breaks = scales::trans_breaks("log10", function(x) 10^x), 
                     #labels = scales::trans_format("log10", scales::math_format(10^.x))) + 
  labs(title = "Fast Build", colour = "Scenes", shape = "Scenes", x = "Triangles [n]", y = "Time [ms]") + 
  theme_bw() + 
  theme(plot.title = element_text(size = 24, face = "bold"), 
        axis.text=element_text(size = 16), 
        axis.title=element_text(size = 18, face = "bold"), 
        legend.text = element_text(size = 20), 
        legend.title = element_text(size = 22, face = "bold"))

ggarrange(normalTotal, fastTotal, ncol = 2, nrow = 1, common.legend = TRUE, legend = "bottom")

ggsave(paste(dataDirectory, "DuplicationNormalFast.pdf", sep = "_"), width = 10, height = 5)

##############################################################################################################################

normalSize <- ggplot(data = subset(melt(rtData), grepl("TotalSize", variable)), 
                      aes(x = rtTriangles, y = value, 
                          color = factor(variable, labels = rtNames), shape = factor(variable, labels = rtNames))) + 
  geom_line(size = 1.2) + 
  geom_point(size = 2.5) + 
  scale_x_continuous(trans = log10_trans(), 
                     breaks = scales::trans_breaks("log10", function(x) 10^x), 
                     labels = scales::trans_format("log10", scales::math_format(10^.x))) + 
  scale_y_continuous(trans = log10_trans(), 
                     breaks = scales::trans_breaks("log10", function(x) 10^x), 
                     labels = scales::trans_format("log10", scales::math_format(10^.x))) + 
  labs(title = "Normal Build", colour = "Scenes", shape = "Scenes", x = "Triangles [n]", y = "Total Size [B]") + 
  theme_bw() + 
  theme(plot.title = element_text(size = 24, face = "bold"), 
        axis.text=element_text(size = 16), 
        axis.title=element_text(size = 18, face = "bold"), 
        legend.text = element_text(size = 20), 
        legend.title = element_text(size = 22, face = "bold"))

fastSize <- ggplot(data = subset(melt(rfData), grepl("TotalSize", variable)), 
                    aes(x = rfTriangles, y = value, 
                        color = factor(variable, labels = rtNames), shape = factor(variable, labels = rtNames))) + 
  geom_line(size = 1.2) + 
  geom_point(size = 2.5) + 
  scale_x_continuous(trans = log10_trans(), 
                     breaks = scales::trans_breaks("log10", function(x) 10^x), 
                     labels = scales::trans_format("log10", scales::math_format(10^.x))) + 
  scale_y_continuous(trans = log10_trans(), 
                     breaks = scales::trans_breaks("log10", function(x) 10^x), 
                     labels = scales::trans_format("log10", scales::math_format(10^.x))) + 
  labs(title = "Fast Build", colour = "Scenes", shape = "Scenes", x = "Triangles [n]", y = "Total Size [B]") + 
  theme_bw() + 
  theme(plot.title = element_text(size = 24, face = "bold"), 
        axis.text=element_text(size = 16), 
        axis.title=element_text(size = 18, face = "bold"), 
        legend.text = element_text(size = 20), 
        legend.title = element_text(size = 22, face = "bold"))

ggarrange(normalSize, fastSize, ncol = 2, nrow = 1, common.legend = TRUE, legend = "bottom")

ggsave(paste(dataDirectory, "DuplicationNormalFastSize.pdf", sep = "_"), width = 10, height = 5)

##############################################################################################################################

