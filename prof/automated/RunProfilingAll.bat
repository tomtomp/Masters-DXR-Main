@echo off

start /wait RunProfilingPreset.bat
start /wait RunProfilingDuplication.bat
start /wait RunProfilingTrack.bat
start /wait RunProfilingTriangles.bat
start /wait RunProfilingBuilds.bat


rundll32.exe user32.dll,LockWorkStation

exit