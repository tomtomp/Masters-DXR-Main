Configuration: 
Command line parameters  = E:\programming\FIT-DIP\Masters-DXR-Main-git\build\Measurement\x64\Debug\Measurement.exe --scene Suzanne/Suzanne.gltf --profile-automation --profile-camera-track Suzanne/Suzanne.trk --rt-mode-pure --width 1024 --height 768 --profile-output TrackSuzanne768Rp 
Window width             = 1024
Window height            = 768
Window title             = Direct3D App
Back-buffer count        = 3
Fill Triangles           = enabled
Triangles Font Clock-Wise= enabled
Fast Build AS            = disabled
Target FPS               = 0
Target UPS               = 60
Tearing                  = enabled
VSync                    = disabled
GUI rendering            = disabled
Debug layer              = enabled
GPU validation           = disabled
GPU profiling            = enabled
Render mode              = Ray Tracing
Loaded scene file        = Suzanne/Suzanne.gltf
Using testing scene      = disabled
Duplication              = 1
Duplication in cube      = disabled
Duplication offset       = 10
BLAS duplication         = disabled
Rays per pixel           = 13
Quality preset           = High
Rasterization Quality    : 
  AO                     = enabled
  AO kernel size         = 4
  Shadows                = enabled
  Shadow map resolution  = 4096
  Shadow PCF kernel size = 4
  Reflections            = enabled
Ray Tracing Quality      : 
  AO                     = enabled
  AO rays                = 4
  AO kernel size         = 4
  Shadows                = enabled
  Shadow rays            = 4
  Shadow kernel size     = 4
  Reflections            = enabled

ProfilingAggregator: 
Render	,	2.931	,	1.8145	,	2.2135	,	3.2122	,	2.5547	,	2.0271	,	2.1202	,	2.7955	,	2.8625	,	4.4146	,	3.5008	,	3.842	,	1.9169	,	2.8329	,	3.5386	,	4.6729	,	4.8665	,	2.2322	,	2.7713	,	2.4045	,	2.2659	,	1.8844	,	1.6182	,	1.6369
Update	,	0.7799	,	0.4652	,	0.4718	,	1.0444	,	0.4666	,	0.59	,	0.6648	,	0.4621	,	1.1525	,	1.1362	,	0.5874	,	0.4676	,	0.5731	,	0.8997	,	0.5276	,	1.2967	,	1.1719	,	0.544	,	0.5495	,	1.1977	,	0.7811	,	0.6508	,	0.8283	,	0.5074
RayTracing	,	1.3784	,	0.6495	,	0.6462	,	1.2676	,	1.0714	,	0.6821	,	0.6427	,	0.9177	,	0.739	,	1.2855	,	0.9603	,	1.3925	,	0.6423	,	1.1151	,	1.3214	,	1.7148	,	1.505	,	0.6423	,	0.6403	,	0.6593	,	0.6447	,	0.6424	,	0.642	,	0.6463
RayTracingGpu	,	0.364704	,	0.496736	,	0.82512	,	0.733728	,	0.545984	,	0.415168	,	0.40096	,	0.541184	,	0.684768	,	2.03008	,	1.20102	,	0.848704	,	0.525184	,	0.5248	,	0.857632	,	1.40118	,	1.52608	,	0.829216	,	1.45683	,	1.02058	,	0.748928	,	0.455168	,	0.33712	,	0.353248
RayTracingRLGpu	,	0.363968	,	0.49616	,	0.82384	,	0.732928	,	0.54496	,	0.414176	,	0.399968	,	0.539424	,	0.68304	,	2.02787	,	1.19981	,	0.847776	,	0.524128	,	0.522912	,	0.85568	,	1.39981	,	1.52461	,	0.82832	,	1.45606	,	1.01962	,	0.748064	,	0.454432	,	0.336384	,	0.351968
Index	,	0	,	1	,	2	,	3	,	4	,	5	,	6	,	7	,	8	,	9	,	10	,	11	,	12	,	13	,	14	,	15	,	16	,	17	,	18	,	19	,	20	,	21	,	22	,	23

ProfilingBuildAggregator: 
BuildBottomLevelAS	,	0.8963
BuildBottomLevelASGpu	,	0.357408
BuildTopLevelAS	,	0.9564
BuildTopLevelASGpu	,	0.083232
Duplication	,	1
Triangles	,	3936
Meshes	,	1
BottomLevels	,	1
TopLevelSize	,	64
BottomLevelsSize	,	224128
Index	,	0

FpsAggregator: 
FPS	,	466	,	449	,	378	,	349	,	384	,	445	,	477	,	421	,	382	,	330	,	311	,	297	,	370	,	403	,	381	,	282	,	222	,	277	,	295	,	261	,	345	,	392	,	484	,	507
UPS	,	59	,	61	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	61	,	60	,	60	,	60	,	60	,	60	,	60	,	60
FrameTime	,	2.15126	,	2.23014	,	2.64773	,	2.86646	,	2.60651	,	2.24877	,	2.09969	,	2.37839	,	2.61868	,	3.03087	,	3.21956	,	3.3748	,	2.70297	,	2.48712	,	2.63347	,	3.5507	,	4.53077	,	3.61142	,	3.39312	,	3.83336	,	2.90003	,	2.55126	,	2.06825	,	1.97286
GigaRays/s	,	4.75237	,	4.5843	,	3.86128	,	3.56663	,	3.92233	,	4.54632	,	4.8691	,	4.29855	,	3.90411	,	3.37316	,	3.17547	,	3.0294	,	3.78237	,	4.11062	,	3.88218	,	2.87932	,	2.25649	,	2.83091	,	3.01304	,	2.66701	,	3.52535	,	4.00729	,	4.94313	,	5.18214
Index	,	0	,	1	,	2	,	3	,	4	,	5	,	6	,	7	,	8	,	9	,	10	,	11	,	12	,	13	,	14	,	15	,	16	,	17	,	18	,	19	,	20	,	21	,	22	,	23


AccelerationStructure: 
Ray tracing acceleration structure building statistics: 
	Bottom Level Acceleration Structure: 
		Maximum [B]: 224128
		Minimum [B]: 224128
		Currently Allocated [B]: 224128
		Currently Created [num]: 1
		Current Average [B]: 224128
		Maximum Triangles [num]: 3936
		Minimum Triangles [num]: 3936
		Total Triangles [num]: 3936
		Maximum Meshes [num]: 1
		Minimum Meshes [num]: 1
		Total Meshes [num]: 1
	Top Level Acceleration Structure: 
		Maximum [B]: 64
		Minimum [B]: 64
		Currently Allocated [B]: 64
		Total Allocated [B]: 64
		Total Created [num]: 1
		Average Allocated [B]: 64
		Maximum Instances [num]: 1
		Minimum Instances [num]: 1
		Total Instances [num]: 1
	Scratch Buffer: 
		Maximum [B]: 34304
		Minimum [B]: 34304
		Currently Allocated [B]: 34304
		Total Allocated [B]: 34304
		Total Created [num]: 1
		Average Allocated [B]: 34304
ProfilingStack: 
[PROF] Profiling statistics: 
	Scopes entered : 68997
	Scopes exited : 68996
	Overhead per scope [ticks] : 1001.2
	Threads entered : 3
	Threads exited : 0
	============================
		         Sum[Mt]   Parent[%]  Samples[num]       Avg[Mt]   Last/Curr[Mt]        Self[Mt]     Self[%] 	 Name

	main:

		   24700.223 Mt   100.000 %         1 x    24700.225 Mt    24700.223 Mt        0.733 Mt     0.003 % 	 /

		   24699.534 Mt    99.997 %         1 x    24699.537 Mt    24699.534 Mt      363.265 Mt     1.471 % 	 ./Main application loop

		       2.925 Mt     0.012 %         1 x        2.925 Mt        0.000 Mt        2.925 Mt   100.000 % 	 ../TexturedRLInitialization

		       2.515 Mt     0.010 %         1 x        2.515 Mt        0.000 Mt        2.515 Mt   100.000 % 	 ../ShadowMapRLInitialization

		       3.881 Mt     0.016 %         1 x        3.881 Mt        0.000 Mt        3.881 Mt   100.000 % 	 ../AmbientOcclusionRLInitialization

		      10.290 Mt     0.042 %         1 x       10.290 Mt        0.000 Mt       10.290 Mt   100.000 % 	 ../RasterResolveRLInitialization

		     242.253 Mt     0.981 %         1 x      242.253 Mt        0.000 Mt        4.610 Mt     1.903 % 	 ../Initialization

		     237.643 Mt    98.097 %         1 x      237.643 Mt        0.000 Mt        1.295 Mt     0.545 % 	 .../ScenePrep

		     204.858 Mt    86.204 %         1 x      204.858 Mt        0.000 Mt       18.891 Mt     9.221 % 	 ..../SceneLoad

		     163.564 Mt    79.843 %         1 x      163.564 Mt        0.000 Mt       19.396 Mt    11.858 % 	 ...../glTF-LoadScene

		     144.168 Mt    88.142 %         2 x       72.084 Mt        0.000 Mt      144.168 Mt   100.000 % 	 ....../glTF-LoadImageData

		      14.390 Mt     7.024 %         1 x       14.390 Mt        0.000 Mt       14.390 Mt   100.000 % 	 ...../glTF-CreateScene

		       8.013 Mt     3.912 %         1 x        8.013 Mt        0.000 Mt        8.013 Mt   100.000 % 	 ...../SceneDesc-CreateScene

		       0.389 Mt     0.164 %         1 x        0.389 Mt        0.000 Mt        0.005 Mt     1.158 % 	 ..../ShadowMapRLPrep

		       0.384 Mt    98.842 %         1 x        0.384 Mt        0.000 Mt        0.323 Mt    84.197 % 	 ...../PrepareForRendering

		       0.061 Mt    15.803 %         1 x        0.061 Mt        0.000 Mt        0.061 Mt   100.000 % 	 ....../PrepareDrawBundle

		       0.967 Mt     0.407 %         1 x        0.967 Mt        0.000 Mt        0.004 Mt     0.455 % 	 ..../TexturedRLPrep

		       0.963 Mt    99.545 %         1 x        0.963 Mt        0.000 Mt        0.304 Mt    31.574 % 	 ...../PrepareForRendering

		       0.405 Mt    42.119 %         1 x        0.405 Mt        0.000 Mt        0.405 Mt   100.000 % 	 ....../PrepareMaterials

		       0.253 Mt    26.306 %         1 x        0.253 Mt        0.000 Mt        0.253 Mt   100.000 % 	 ....../PrepareDrawBundle

		       4.152 Mt     1.747 %         1 x        4.152 Mt        0.000 Mt        0.005 Mt     0.123 % 	 ..../DeferredRLPrep

		       4.147 Mt    99.877 %         1 x        4.147 Mt        0.000 Mt        1.670 Mt    40.265 % 	 ...../PrepareForRendering

		       0.015 Mt     0.367 %         1 x        0.015 Mt        0.000 Mt        0.015 Mt   100.000 % 	 ....../PrepareMaterials

		       1.809 Mt    43.634 %         1 x        1.809 Mt        0.000 Mt        1.809 Mt   100.000 % 	 ....../BuildMaterialCache

		       0.652 Mt    15.735 %         1 x        0.652 Mt        0.000 Mt        0.652 Mt   100.000 % 	 ....../PrepareDrawBundle

		      13.261 Mt     5.580 %         1 x       13.261 Mt        0.000 Mt        4.025 Mt    30.354 % 	 ..../RayTracingRLPrep

		       0.571 Mt     4.308 %         1 x        0.571 Mt        0.000 Mt        0.571 Mt   100.000 % 	 ...../PrepareAccelerationStructures

		       0.058 Mt     0.435 %         1 x        0.058 Mt        0.000 Mt        0.058 Mt   100.000 % 	 ...../PrepareCache

		       0.405 Mt     3.057 %         1 x        0.405 Mt        0.000 Mt        0.405 Mt   100.000 % 	 ...../BuildCache

		       6.426 Mt    48.456 %         1 x        6.426 Mt        0.000 Mt        0.005 Mt     0.073 % 	 ...../BuildAccelerationStructures

		       6.421 Mt    99.927 %         1 x        6.421 Mt        0.000 Mt        4.568 Mt    71.145 % 	 ....../AccelerationStructureBuild

		       0.896 Mt    13.959 %         1 x        0.896 Mt        0.000 Mt        0.896 Mt   100.000 % 	 ......./BuildBottomLevelAS

		       0.956 Mt    14.895 %         1 x        0.956 Mt        0.000 Mt        0.956 Mt   100.000 % 	 ......./BuildTopLevelAS

		       1.776 Mt    13.389 %         1 x        1.776 Mt        0.000 Mt        1.776 Mt   100.000 % 	 ...../BuildShaderTables

		      12.722 Mt     5.353 %         1 x       12.722 Mt        0.000 Mt       12.722 Mt   100.000 % 	 ..../GpuSidePrep

		       5.017 Mt     0.020 %         1 x        5.017 Mt        0.000 Mt        5.017 Mt   100.000 % 	 ../DeferredRLInitialization

		      62.341 Mt     0.252 %         1 x       62.341 Mt        0.000 Mt        2.230 Mt     3.577 % 	 ../RayTracingRLInitialization

		      60.111 Mt    96.423 %         1 x       60.111 Mt        0.000 Mt       60.111 Mt   100.000 % 	 .../PipelineBuild

		       5.176 Mt     0.021 %         1 x        5.176 Mt        0.000 Mt        5.176 Mt   100.000 % 	 ../ResolveRLInitialization

		   24001.872 Mt    97.175 %     14051 x        1.708 Mt        2.662 Mt      306.767 Mt     1.278 % 	 ../MainLoop

		    1197.639 Mt     4.990 %      1442 x        0.831 Mt        0.507 Mt     1196.404 Mt    99.897 % 	 .../Update

		       1.234 Mt     0.103 %         1 x        1.234 Mt        0.000 Mt        1.234 Mt   100.000 % 	 ..../GuiModelApply

		   22497.466 Mt    93.732 %      8910 x        2.525 Mt        2.147 Mt    14804.436 Mt    65.805 % 	 .../Render

		    7693.030 Mt    34.195 %      8910 x        0.863 Mt        0.708 Mt     7647.742 Mt    99.411 % 	 ..../RayTracing

		      45.288 Mt     0.589 %      8910 x        0.005 Mt        0.003 Mt       45.288 Mt   100.000 % 	 ...../RayTracingRLPrep

	WindowThread:

		   24695.766 Mt   100.000 %         1 x    24695.766 Mt    24695.765 Mt    24695.766 Mt   100.000 % 	 /

	GpuThread:

		    6395.827 Mt   100.000 %      8910 x        0.718 Mt        0.377 Mt      143.564 Mt     2.245 % 	 /

		       0.451 Mt     0.007 %         1 x        0.451 Mt        0.000 Mt        0.008 Mt     1.854 % 	 ./ScenePrep

		       0.442 Mt    98.146 %         1 x        0.442 Mt        0.000 Mt        0.002 Mt     0.362 % 	 ../AccelerationStructureBuild

		       0.357 Mt    80.818 %         1 x        0.357 Mt        0.000 Mt        0.357 Mt   100.000 % 	 .../BuildBottomLevelASGpu

		       0.083 Mt    18.821 %         1 x        0.083 Mt        0.000 Mt        0.083 Mt   100.000 % 	 .../BuildTopLevelASGpu

		    6228.381 Mt    97.382 %      8910 x        0.699 Mt        0.374 Mt       10.064 Mt     0.162 % 	 ./RayTracingGpu

		    6218.317 Mt    99.838 %      8910 x        0.698 Mt        0.373 Mt     6218.317 Mt   100.000 % 	 ../RayTracingRLGpu

		      23.432 Mt     0.366 %      8910 x        0.003 Mt        0.002 Mt       23.432 Mt   100.000 % 	 ./Present


	============================


