Configuration: 
Command line parameters  = E:\programming\FIT-DIP\Masters-DXR-Main-git\build\Measurement\x64\Debug\Measurement.exe --scene Sponza/Sponza.gltf --profile-automation --profile-camera-track Sponza/SponzaPreset.trk --rt-mode --width 2560 --height 1440 --profile-output PresetSponza1440RtNoEffects --quality-preset NoEffects 
Window width             = 2560
Window height            = 1421
Window title             = Direct3D App
Back-buffer count        = 3
Fill Triangles           = enabled
Triangles Font Clock-Wise= enabled
Fast Build AS            = disabled
Target FPS               = 0
Target UPS               = 60
Tearing                  = enabled
VSync                    = disabled
GUI rendering            = disabled
Debug layer              = enabled
GPU validation           = disabled
GPU profiling            = enabled
Render mode              = Ray Tracing
Loaded scene file        = Sponza/Sponza.gltf
Using testing scene      = disabled
Duplication              = 1
Duplication in cube      = disabled
Duplication offset       = 10
BLAS duplication         = disabled
Rays per pixel           = 1
Quality preset           = NoEffects
Rasterization Quality    : 
  AO                     = disabled
  AO kernel size         = 1
  Shadows                = disabled
  Shadow map resolution  = 1
  Shadow PCF kernel size = 0
  Reflections            = disabled
Ray Tracing Quality      : 
  AO                     = disabled
  AO rays                = 0
  AO kernel size         = 4
  Shadows                = disabled
  Shadow rays            = 0
  Shadow kernel size     = 4
  Reflections            = disabled

ProfilingAggregator: 
Render	,	6.6407	,	6.2944	,	7.3629	,	6.1818	,	6.675	,	6.0657	,	6.7204	,	7.4963	,	5.7613	,	5.4931	,	6.4277	,	6.0371	,	6.1948	,	6.2739	,	6.3101	,	6.5471	,	6.8572	,	6.8309	,	6.438	,	6.795	,	5.9935	,	6.4941	,	6.4039
Update	,	0.7612	,	0.7138	,	0.9209	,	0.4892	,	0.774	,	0.8631	,	0.7649	,	0.832	,	0.6188	,	0.9165	,	0.5565	,	0.8979	,	0.5616	,	1.0915	,	0.978	,	0.5043	,	0.8528	,	0.6178	,	0.9488	,	0.6786	,	0.5658	,	1.078	,	0.5059
RayTracing	,	2.9478	,	2.8125	,	3.5253	,	2.9466	,	3.1096	,	2.5361	,	2.9216	,	3.191	,	2.4973	,	2.4672	,	2.9712	,	2.4773	,	2.7321	,	2.7488	,	2.6768	,	2.5014	,	2.9575	,	2.9616	,	2.9694	,	2.7078	,	2.4841	,	2.7406	,	2.4793
RayTracingGpu	,	2.3056	,	2.26413	,	2.1247	,	1.65066	,	1.77414	,	2.36432	,	2.65786	,	2.65494	,	2.03453	,	1.91437	,	2.11661	,	2.42669	,	2.10538	,	2.24304	,	2.28365	,	2.48602	,	2.40093	,	2.39162	,	2.19094	,	2.9273	,	2.24685	,	2.26387	,	2.592
DeferredRLGpu	,	1.53472	,	1.4903	,	1.3663	,	0.89552	,	1.02675	,	1.31069	,	1.60714	,	1.87082	,	1.27782	,	1.13456	,	1.11715	,	1.41485	,	1.3391	,	1.46733	,	1.48864	,	1.47702	,	1.54854	,	1.53738	,	1.34358	,	1.81706	,	1.44749	,	1.46234	,	1.53158
RayTracingRLGpu	,	0.336256	,	0.337536	,	0.330368	,	0.328544	,	0.325504	,	0.454112	,	0.454304	,	0.351008	,	0.32512	,	0.3384	,	0.42768	,	0.430816	,	0.332032	,	0.334624	,	0.349344	,	0.534816	,	0.374144	,	0.376064	,	0.372832	,	0.4856	,	0.35072	,	0.351392	,	0.35968
ResolveRLGpu	,	0.43312	,	0.43488	,	0.426944	,	0.425184	,	0.420192	,	0.597696	,	0.594368	,	0.42672	,	0.429664	,	0.439584	,	0.570368	,	0.578848	,	0.432352	,	0.439424	,	0.444128	,	0.472864	,	0.476384	,	0.476352	,	0.472256	,	0.62288	,	0.447104	,	0.448256	,	0.698912
Index	,	0	,	1	,	2	,	3	,	4	,	5	,	6	,	7	,	8	,	9	,	10	,	11	,	12	,	13	,	14	,	15	,	16	,	17	,	18	,	19	,	20	,	21	,	22

ProfilingBuildAggregator: 
BuildBottomLevelAS	,	3.8553
BuildBottomLevelASGpu	,	15.9058
BuildTopLevelAS	,	1.0816
BuildTopLevelASGpu	,	0.635136
Duplication	,	1
Triangles	,	262267
Meshes	,	103
BottomLevels	,	1
TopLevelSize	,	64
BottomLevelsSize	,	14623872
Index	,	0

FpsAggregator: 
FPS	,	134	,	144	,	150	,	155	,	165	,	149	,	147	,	147	,	153	,	153	,	153	,	148	,	146	,	150	,	148	,	153	,	148	,	146	,	147	,	145	,	144	,	148	,	148
UPS	,	59	,	61	,	60	,	60	,	60	,	61	,	60	,	60	,	61	,	60	,	60	,	60	,	60	,	60	,	61	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	61
FrameTime	,	7.50708	,	6.96132	,	6.71429	,	6.46965	,	6.07626	,	6.73516	,	6.82107	,	6.85155	,	6.57603	,	6.56054	,	6.53639	,	6.77154	,	6.86768	,	6.69303	,	6.78648	,	6.53653	,	6.7662	,	6.87587	,	6.82377	,	6.9052	,	6.96254	,	6.7627	,	6.80216
GigaRays/s	,	0.484577	,	0.522568	,	0.541794	,	0.562281	,	0.598684	,	0.540115	,	0.533312	,	0.53094	,	0.553185	,	0.554491	,	0.55654	,	0.537213	,	0.529693	,	0.543515	,	0.53603	,	0.556528	,	0.537637	,	0.529062	,	0.533101	,	0.526815	,	0.522476	,	0.537916	,	0.534795
Index	,	0	,	1	,	2	,	3	,	4	,	5	,	6	,	7	,	8	,	9	,	10	,	11	,	12	,	13	,	14	,	15	,	16	,	17	,	18	,	19	,	20	,	21	,	22


AccelerationStructure: 
Ray tracing acceleration structure building statistics: 
	Bottom Level Acceleration Structure: 
		Maximum [B]: 14623872
		Minimum [B]: 14623872
		Currently Allocated [B]: 14623872
		Currently Created [num]: 1
		Current Average [B]: 14623872
		Maximum Triangles [num]: 262267
		Minimum Triangles [num]: 262267
		Total Triangles [num]: 262267
		Maximum Meshes [num]: 103
		Minimum Meshes [num]: 103
		Total Meshes [num]: 103
	Top Level Acceleration Structure: 
		Maximum [B]: 64
		Minimum [B]: 64
		Currently Allocated [B]: 64
		Total Allocated [B]: 64
		Total Created [num]: 1
		Average Allocated [B]: 64
		Maximum Instances [num]: 1
		Minimum Instances [num]: 1
		Total Instances [num]: 1
	Scratch Buffer: 
		Maximum [B]: 11660800
		Minimum [B]: 11660800
		Currently Allocated [B]: 11660800
		Total Allocated [B]: 11660800
		Total Created [num]: 1
		Average Allocated [B]: 11660800
ProfilingStack: 
[PROF] Profiling statistics: 
	Scopes entered : 40191
	Scopes exited : 40190
	Overhead per scope [ticks] : 995.3
	Threads entered : 3
	Threads exited : 0
	============================
		         Sum[Mt]   Parent[%]  Samples[num]       Avg[Mt]   Last/Curr[Mt]        Self[Mt]     Self[%] 	 Name

	main:

		   33090.992 Mt   100.000 %         1 x    33090.993 Mt    33090.991 Mt        0.926 Mt     0.003 % 	 /

		   33090.109 Mt    99.997 %         1 x    33090.112 Mt    33090.109 Mt      366.935 Mt     1.109 % 	 ./Main application loop

		       2.917 Mt     0.009 %         1 x        2.917 Mt        0.000 Mt        2.917 Mt   100.000 % 	 ../TexturedRLInitialization

		       2.285 Mt     0.007 %         1 x        2.285 Mt        0.000 Mt        2.285 Mt   100.000 % 	 ../ShadowMapRLInitialization

		       3.845 Mt     0.012 %         1 x        3.845 Mt        0.000 Mt        3.845 Mt   100.000 % 	 ../AmbientOcclusionRLInitialization

		       5.990 Mt     0.018 %         1 x        5.990 Mt        0.000 Mt        5.990 Mt   100.000 % 	 ../RasterResolveRLInitialization

		    9544.675 Mt    28.844 %         1 x     9544.675 Mt        0.000 Mt       12.752 Mt     0.134 % 	 ../Initialization

		    9531.923 Mt    99.866 %         1 x     9531.923 Mt        0.000 Mt        4.379 Mt     0.046 % 	 .../ScenePrep

		    9396.400 Mt    98.578 %         1 x     9396.400 Mt        0.000 Mt      358.224 Mt     3.812 % 	 ..../SceneLoad

		    8145.222 Mt    86.684 %         1 x     8145.222 Mt        0.000 Mt     1109.711 Mt    13.624 % 	 ...../glTF-LoadScene

		    7035.511 Mt    86.376 %        69 x      101.964 Mt        0.000 Mt     7035.511 Mt   100.000 % 	 ....../glTF-LoadImageData

		     556.917 Mt     5.927 %         1 x      556.917 Mt        0.000 Mt      556.917 Mt   100.000 % 	 ...../glTF-CreateScene

		     336.037 Mt     3.576 %         1 x      336.037 Mt        0.000 Mt      336.037 Mt   100.000 % 	 ...../SceneDesc-CreateScene

		       3.330 Mt     0.035 %         1 x        3.330 Mt        0.000 Mt        0.006 Mt     0.168 % 	 ..../ShadowMapRLPrep

		       3.325 Mt    99.832 %         1 x        3.325 Mt        0.000 Mt        2.627 Mt    79.008 % 	 ...../PrepareForRendering

		       0.698 Mt    20.992 %         1 x        0.698 Mt        0.000 Mt        0.698 Mt   100.000 % 	 ....../PrepareDrawBundle

		      17.669 Mt     0.185 %         1 x       17.669 Mt        0.000 Mt        0.005 Mt     0.028 % 	 ..../TexturedRLPrep

		      17.663 Mt    99.972 %         1 x       17.663 Mt        0.000 Mt        2.172 Mt    12.297 % 	 ...../PrepareForRendering

		       7.407 Mt    41.931 %         1 x        7.407 Mt        0.000 Mt        7.407 Mt   100.000 % 	 ....../PrepareMaterials

		       8.085 Mt    45.772 %         1 x        8.085 Mt        0.000 Mt        8.085 Mt   100.000 % 	 ....../PrepareDrawBundle

		      12.457 Mt     0.131 %         1 x       12.457 Mt        0.000 Mt        0.005 Mt     0.043 % 	 ..../DeferredRLPrep

		      12.451 Mt    99.957 %         1 x       12.451 Mt        0.000 Mt        4.154 Mt    33.361 % 	 ...../PrepareForRendering

		       0.025 Mt     0.203 %         1 x        0.025 Mt        0.000 Mt        0.025 Mt   100.000 % 	 ....../PrepareMaterials

		       5.650 Mt    45.375 %         1 x        5.650 Mt        0.000 Mt        5.650 Mt   100.000 % 	 ....../BuildMaterialCache

		       2.622 Mt    21.061 %         1 x        2.622 Mt        0.000 Mt        2.622 Mt   100.000 % 	 ....../PrepareDrawBundle

		      33.658 Mt     0.353 %         1 x       33.658 Mt        0.000 Mt        9.286 Mt    27.590 % 	 ..../RayTracingRLPrep

		       0.635 Mt     1.887 %         1 x        0.635 Mt        0.000 Mt        0.635 Mt   100.000 % 	 ...../PrepareAccelerationStructures

		       0.027 Mt     0.081 %         1 x        0.027 Mt        0.000 Mt        0.027 Mt   100.000 % 	 ...../PrepareCache

		       5.731 Mt    17.029 %         1 x        5.731 Mt        0.000 Mt        5.731 Mt   100.000 % 	 ...../BuildCache

		      11.552 Mt    34.322 %         1 x       11.552 Mt        0.000 Mt        0.004 Mt     0.036 % 	 ...../BuildAccelerationStructures

		      11.548 Mt    99.964 %         1 x       11.548 Mt        0.000 Mt        6.611 Mt    57.248 % 	 ....../AccelerationStructureBuild

		       3.855 Mt    33.386 %         1 x        3.855 Mt        0.000 Mt        3.855 Mt   100.000 % 	 ......./BuildBottomLevelAS

		       1.082 Mt     9.366 %         1 x        1.082 Mt        0.000 Mt        1.082 Mt   100.000 % 	 ......./BuildTopLevelAS

		       6.426 Mt    19.093 %         1 x        6.426 Mt        0.000 Mt        6.426 Mt   100.000 % 	 ...../BuildShaderTables

		      64.030 Mt     0.672 %         1 x       64.030 Mt        0.000 Mt       64.030 Mt   100.000 % 	 ..../GpuSidePrep

		       4.715 Mt     0.014 %         1 x        4.715 Mt        0.000 Mt        4.715 Mt   100.000 % 	 ../DeferredRLInitialization

		      65.314 Mt     0.197 %         1 x       65.314 Mt        0.000 Mt        2.054 Mt     3.145 % 	 ../RayTracingRLInitialization

		      63.260 Mt    96.855 %         1 x       63.260 Mt        0.000 Mt       63.260 Mt   100.000 % 	 .../PipelineBuild

		       4.853 Mt     0.015 %         1 x        4.853 Mt        0.000 Mt        4.853 Mt   100.000 % 	 ../ResolveRLInitialization

		   23088.580 Mt    69.775 %      7887 x        2.927 Mt        8.417 Mt      392.122 Mt     1.698 % 	 ../MainLoop

		    1063.732 Mt     4.607 %      1386 x        0.767 Mt        0.403 Mt     1062.498 Mt    99.884 % 	 .../Update

		       1.234 Mt     0.116 %         1 x        1.234 Mt        0.000 Mt        1.234 Mt   100.000 % 	 ..../GuiModelApply

		   21632.725 Mt    93.694 %      3423 x        6.320 Mt        7.485 Mt    12305.809 Mt    56.885 % 	 .../Render

		    9326.916 Mt    43.115 %      3423 x        2.725 Mt        2.888 Mt     9301.456 Mt    99.727 % 	 ..../RayTracing

		      14.428 Mt     0.155 %      3423 x        0.004 Mt        0.003 Mt       14.428 Mt   100.000 % 	 ...../DeferredRLPrep

		      11.033 Mt     0.118 %      3423 x        0.003 Mt        0.004 Mt       11.033 Mt   100.000 % 	 ...../RayTracingRLPrep

	WindowThread:

		   33068.954 Mt   100.000 %         1 x    33068.955 Mt    33068.954 Mt    33068.954 Mt   100.000 % 	 /

	GpuThread:

		    7930.173 Mt   100.000 %      3423 x        2.317 Mt        2.692 Mt      183.934 Mt     2.319 % 	 /

		      16.594 Mt     0.209 %         1 x       16.594 Mt        0.000 Mt        0.050 Mt     0.303 % 	 ./ScenePrep

		      16.544 Mt    99.697 %         1 x       16.544 Mt        0.000 Mt        0.003 Mt     0.020 % 	 ../AccelerationStructureBuild

		      15.906 Mt    96.141 %         1 x       15.906 Mt        0.000 Mt       15.906 Mt   100.000 % 	 .../BuildBottomLevelASGpu

		       0.635 Mt     3.839 %         1 x        0.635 Mt        0.000 Mt        0.635 Mt   100.000 % 	 .../BuildTopLevelASGpu

		    7693.375 Mt    97.014 %      3423 x        2.248 Mt        2.689 Mt        6.026 Mt     0.078 % 	 ./RayTracingGpu

		    4741.869 Mt    61.636 %      3423 x        1.385 Mt        1.857 Mt     4741.869 Mt   100.000 % 	 ../DeferredRLGpu

		    1301.647 Mt    16.919 %      3423 x        0.380 Mt        0.367 Mt     1301.647 Mt   100.000 % 	 ../RayTracingRLGpu

		    1643.832 Mt    21.367 %      3423 x        0.480 Mt        0.463 Mt     1643.832 Mt   100.000 % 	 ../ResolveRLGpu

		      36.269 Mt     0.457 %      3423 x        0.011 Mt        0.002 Mt       36.269 Mt   100.000 % 	 ./Present


	============================


