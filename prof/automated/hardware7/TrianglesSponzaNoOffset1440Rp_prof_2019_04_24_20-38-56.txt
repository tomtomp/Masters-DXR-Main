Configuration: 
Command line parameters  = E:\programming\FIT-DIP\Masters-DXR-Main-git\build\Measurement\x64\Debug\Measurement.exe --scene Sponza/Sponza.gltf --duplication-cube --duplication-offset 0 --profile-triangles --profile-camera-track Sponza/SponzaFast.trk --profile-max-duplication 6 --rt-mode-pure --width 2560 --height 1440 --profile-output TrianglesSponzaNoOffset1440Rp 
Window width             = 2560
Window height            = 1421
Window title             = Direct3D App
Back-buffer count        = 3
Fill Triangles           = enabled
Triangles Font Clock-Wise= enabled
Fast Build AS            = disabled
Target FPS               = 0
Target UPS               = 60
Tearing                  = enabled
VSync                    = disabled
GUI rendering            = disabled
Debug layer              = enabled
GPU validation           = disabled
GPU profiling            = enabled
Render mode              = Ray Tracing
Loaded scene file        = Sponza/Sponza.gltf
Using testing scene      = disabled
Duplication              = 6
Duplication in cube      = enabled
Duplication offset       = 0
BLAS duplication         = enabled
Rays per pixel           = 13
Quality preset           = High
Rasterization Quality    : 
  AO                     = enabled
  AO kernel size         = 4
  Shadows                = enabled
  Shadow map resolution  = 4096
  Shadow PCF kernel size = 4
  Reflections            = enabled
Ray Tracing Quality      : 
  AO                     = enabled
  AO rays                = 4
  AO kernel size         = 4
  Shadows                = enabled
  Shadow rays            = 4
  Shadow kernel size     = 4
  Reflections            = enabled

ProfilingAggregator: 
Render	,	8.3657	,	8.3657	,	8.7047	,	8.7047	,	8.2498	,	8.2498	,	8.5122	,	8.5122	,	10.1385	,	10.1385	,	9.17	,	28.9367	,	28.9367	,	32.1003	,	32.1003	,	30.6578	,	30.6578	,	33.2946	,	33.2946	,	48.9256	,	48.9256	,	30.2398	,	93.6556	,	93.6556	,	98.2783	,	98.2783	,	92.9318	,	92.9318	,	206.531	,	206.531	,	128.594	,	128.594	,	96.3226	,	291.341	,	291.341	,	232.934	,	232.934	,	244.812	,	244.812	,	470.889	,	470.889	,	256.832	,	513.829	,	513.829	,	482.796	,	482.796	,	1027.42	,	1027.42	,	557.841	,	955.885	,	955.885	,	1972.86	,	1972.86	,	1051.16
Update	,	0.7027	,	0.7027	,	0.6059	,	0.6059	,	0.8839	,	0.8839	,	0.8331	,	0.8331	,	0.8262	,	0.8262	,	0.5823	,	0.5165	,	0.5165	,	0.5428	,	0.5428	,	0.517	,	0.517	,	0.5112	,	0.5112	,	0.4947	,	0.4947	,	0.5132	,	0.5051	,	0.5051	,	0.5087	,	0.5087	,	0.5087	,	0.5087	,	0.5104	,	0.5104	,	0.5074	,	0.5074	,	0.4988	,	0.5516	,	0.5516	,	0.5527	,	0.5527	,	0.5568	,	0.5568	,	0.5537	,	0.5537	,	0.5427	,	0.559	,	0.559	,	0.6265	,	0.6265	,	0.5913	,	0.5913	,	0.5061	,	0.6307	,	0.6307	,	0.6135	,	0.6135	,	0.5987
RayTracing	,	1.2182	,	1.2182	,	1.2197	,	1.2197	,	1.1924	,	1.1924	,	1.1979	,	1.1979	,	1.22	,	1.22	,	1.2703	,	1.3287	,	1.3287	,	1.4404	,	1.4404	,	1.3364	,	1.3364	,	1.3198	,	1.3198	,	1.3368	,	1.3368	,	1.3252	,	1.3587	,	1.3587	,	1.3696	,	1.3696	,	1.3566	,	1.3566	,	1.3934	,	1.3934	,	1.363	,	1.363	,	1.363	,	1.4589	,	1.4589	,	1.463	,	1.463	,	1.4945	,	1.4945	,	1.4119	,	1.4119	,	1.4271	,	1.2303	,	1.2303	,	1.4265	,	1.4265	,	1.3505	,	1.3505	,	1.2287	,	1.2231	,	1.2231	,	1.2185	,	1.2185	,	1.1411
RayTracingGpu	,	5.56173	,	5.56173	,	6.10813	,	6.10813	,	5.9255	,	5.9255	,	6.24957	,	6.24957	,	7.39254	,	7.39254	,	6.36723	,	25.8946	,	25.8946	,	28.7233	,	28.7233	,	27.5768	,	27.5768	,	30.3062	,	30.3062	,	45.7833	,	45.7833	,	27.2126	,	90.5842	,	90.5842	,	95.0139	,	95.0139	,	89.7691	,	89.7691	,	203.441	,	203.441	,	125.395	,	125.395	,	93.0798	,	287.82	,	287.82	,	229.531	,	229.531	,	241.402	,	241.402	,	467.65	,	467.65	,	253.539	,	510.898	,	510.898	,	479.462	,	479.462	,	1024.2	,	1024.2	,	554.988	,	953.105	,	953.105	,	1969.96	,	1969.96	,	1048.52
RayTracingRLGpu	,	5.56134	,	5.56134	,	6.10774	,	6.10774	,	5.92499	,	5.92499	,	6.24915	,	6.24915	,	7.39187	,	7.39187	,	6.36685	,	25.8939	,	25.8939	,	28.7227	,	28.7227	,	27.5764	,	27.5764	,	30.3057	,	30.3057	,	45.7826	,	45.7826	,	27.2119	,	90.5838	,	90.5838	,	95.0135	,	95.0135	,	89.7684	,	89.7684	,	203.441	,	203.441	,	125.394	,	125.394	,	93.0793	,	287.819	,	287.819	,	229.53	,	229.53	,	241.401	,	241.401	,	467.65	,	467.65	,	253.539	,	510.897	,	510.897	,	479.461	,	479.461	,	1024.2	,	1024.2	,	554.987	,	953.105	,	953.105	,	1969.96	,	1969.96	,	1048.52
Index	,	0	,	1	,	2	,	3	,	4	,	5	,	6	,	7	,	8	,	9	,	10	,	11	,	12	,	13	,	14	,	15	,	16	,	17	,	18	,	19	,	20	,	21	,	22	,	23	,	24	,	25	,	26	,	27	,	28	,	29	,	30	,	31	,	32	,	33	,	34	,	35	,	36	,	37	,	38	,	39	,	40	,	41	,	42	,	43	,	44	,	45	,	46	,	47	,	48	,	49	,	50	,	51	,	52	,	53

ProfilingBuildAggregator: 
BuildBottomLevelAS	,	4.1936	,	9.6497	,	32.6024	,	82.4282	,	154.66	,	259.743
BuildBottomLevelASGpu	,	12.3945	,	8.88237	,	31.1594	,	72.9668	,	141.947	,	244.515
BuildTopLevelAS	,	2.2614	,	0.7471	,	0.748	,	0.9489	,	0.7096	,	0.7553
BuildTopLevelASGpu	,	0.44048	,	0.079328	,	0.164032	,	0.294016	,	0.36608	,	0.410496
Duplication	,	1	,	2	,	3	,	4	,	5	,	6
Triangles	,	262267	,	2098136	,	7081209	,	16785088	,	32783375	,	56649672
Meshes	,	103	,	824	,	2781	,	6592	,	12875	,	22248
BottomLevels	,	1	,	8	,	27	,	64	,	125	,	216
TopLevelSize	,	64	,	512	,	1728	,	4096	,	8000	,	13824
BottomLevelsSize	,	14623872	,	116990976	,	394844544	,	935927808	,	1827984000	,	3158756352
Index	,	0	,	1	,	2	,	3	,	4	,	5

FpsAggregator: 
FPS	,	107	,	107	,	115	,	115	,	112	,	112	,	113	,	113	,	90	,	90	,	102	,	32	,	32	,	32	,	32	,	33	,	33	,	33	,	33	,	17	,	17	,	29	,	11	,	11	,	10	,	10	,	11	,	11	,	10	,	10	,	6	,	6	,	10	,	5	,	5	,	4	,	4	,	4	,	4	,	3	,	3	,	4	,	2	,	2	,	2	,	2	,	2	,	2	,	2	,	1	,	1	,	2	,	2	,	1
UPS	,	60	,	60	,	60	,	60	,	61	,	61	,	60	,	60	,	60	,	60	,	60	,	61	,	61	,	62	,	62	,	61	,	61	,	61	,	61	,	62	,	62	,	62	,	63	,	63	,	62	,	62	,	63	,	63	,	57	,	57	,	72	,	72	,	63	,	73	,	73	,	66	,	66	,	60	,	60	,	61	,	61	,	85	,	72	,	72	,	65	,	65	,	60	,	60	,	101	,	69	,	69	,	116	,	116	,	121
FrameTime	,	9.36296	,	9.36296	,	8.73432	,	8.73432	,	8.94521	,	8.94521	,	8.90356	,	8.90356	,	11.2078	,	11.2078	,	9.81303	,	31.6006	,	31.6006	,	32.2108	,	32.2108	,	30.9423	,	30.9423	,	30.9011	,	30.9011	,	61.5591	,	61.5591	,	34.8812	,	94.984	,	94.984	,	103.995	,	103.995	,	94.8753	,	94.8753	,	105.356	,	105.356	,	188.463	,	188.463	,	100.892	,	257.765	,	257.765	,	260.046	,	260.046	,	253.316	,	253.316	,	414.949	,	414.949	,	299.426	,	592.918	,	592.918	,	525.684	,	525.684	,	767.856	,	767.856	,	613.468	,	1007.51	,	1007.51	,	1471.61	,	1471.61	,	1134.95
GigaRays/s	,	5.05085	,	5.05085	,	5.41437	,	5.41437	,	5.28673	,	5.28673	,	5.31146	,	5.31146	,	4.21947	,	4.21947	,	4.81919	,	1.49652	,	1.49652	,	1.46817	,	1.46817	,	1.52835	,	1.52835	,	1.5304	,	1.5304	,	0.76822	,	0.76822	,	1.35577	,	0.497882	,	0.497882	,	0.454741	,	0.454741	,	0.498453	,	0.498453	,	0.448867	,	0.448867	,	0.250929	,	0.250929	,	0.468726	,	0.183465	,	0.183465	,	0.181856	,	0.181856	,	0.186687	,	0.186687	,	0.113968	,	0.113968	,	0.157938	,	0.0797596	,	0.0797596	,	0.0899606	,	0.0899606	,	0.0615882	,	0.0615882	,	0.0770877	,	0.0469384	,	0.0469384	,	0.0321354	,	0.0321354	,	0.0416679
Duplication	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	2	,	2	,	2	,	2	,	2	,	2	,	2	,	2	,	2	,	2	,	2	,	3	,	3	,	3	,	3	,	3	,	3	,	3	,	3	,	3	,	3	,	3	,	4	,	4	,	4	,	4	,	4	,	4	,	4	,	4	,	4	,	5	,	5	,	5	,	5	,	5	,	5	,	5	,	6	,	6	,	6	,	6	,	6
Index	,	0	,	1	,	2	,	3	,	4	,	5	,	6	,	7	,	8	,	9	,	10	,	11	,	12	,	13	,	14	,	15	,	16	,	17	,	18	,	19	,	20	,	21	,	22	,	23	,	24	,	25	,	26	,	27	,	28	,	29	,	30	,	31	,	32	,	33	,	34	,	35	,	36	,	37	,	38	,	39	,	40	,	41	,	42	,	43	,	44	,	45	,	46	,	47	,	48	,	49	,	50	,	51	,	52	,	53


AccelerationStructure: 
Ray tracing acceleration structure building statistics: 
	Bottom Level Acceleration Structure: 
		Maximum [B]: 14623872
		Minimum [B]: 14623872
		Currently Allocated [B]: 3158756352
		Currently Created [num]: 216
		Current Average [B]: 14623872
		Maximum Triangles [num]: 262267
		Minimum Triangles [num]: 262267
		Total Triangles [num]: 56649672
		Maximum Meshes [num]: 103
		Minimum Meshes [num]: 103
		Total Meshes [num]: 22248
	Top Level Acceleration Structure: 
		Maximum [B]: 13824
		Minimum [B]: 13824
		Currently Allocated [B]: 13824
		Total Allocated [B]: 13824
		Total Created [num]: 1
		Average Allocated [B]: 13824
		Maximum Instances [num]: 216
		Minimum Instances [num]: 216
		Total Instances [num]: 216
	Scratch Buffer: 
		Maximum [B]: 11660800
		Minimum [B]: 11660800
		Currently Allocated [B]: 11660800
		Total Allocated [B]: 11660800
		Total Created [num]: 1
		Average Allocated [B]: 11660800
ProfilingStack: 
[PROF] Profiling statistics: 
	Scopes entered : 17251
	Scopes exited : 17250
	Overhead per scope [ticks] : 1005.6
	Threads entered : 3
	Threads exited : 0
	============================
		         Sum[Mt]   Parent[%]  Samples[num]       Avg[Mt]   Last/Curr[Mt]        Self[Mt]     Self[%] 	 Name

	main:

		   66080.934 Mt   100.000 %         1 x    66080.939 Mt    66080.932 Mt        0.673 Mt     0.001 % 	 /

		   66080.411 Mt    99.999 %         1 x    66080.419 Mt    66080.410 Mt      333.537 Mt     0.505 % 	 ./Main application loop

		       2.851 Mt     0.004 %         1 x        2.851 Mt        0.000 Mt        2.851 Mt   100.000 % 	 ../TexturedRLInitialization

		       2.236 Mt     0.003 %         1 x        2.236 Mt        0.000 Mt        2.236 Mt   100.000 % 	 ../ShadowMapRLInitialization

		       4.099 Mt     0.006 %         1 x        4.099 Mt        0.000 Mt        4.099 Mt   100.000 % 	 ../AmbientOcclusionRLInitialization

		       5.066 Mt     0.008 %         1 x        5.066 Mt        0.000 Mt        5.066 Mt   100.000 % 	 ../RasterResolveRLInitialization

		    9075.326 Mt    13.734 %         1 x     9075.326 Mt        0.000 Mt       12.819 Mt     0.141 % 	 ../Initialization

		    9062.507 Mt    99.859 %         1 x     9062.507 Mt        0.000 Mt        3.486 Mt     0.038 % 	 .../ScenePrep

		    8947.027 Mt    98.726 %         1 x     8947.027 Mt        0.000 Mt      326.787 Mt     3.652 % 	 ..../SceneLoad

		    8184.420 Mt    91.476 %         1 x     8184.420 Mt        0.000 Mt     1103.467 Mt    13.483 % 	 ...../glTF-LoadScene

		    7080.952 Mt    86.517 %        69 x      102.622 Mt        0.000 Mt     7080.952 Mt   100.000 % 	 ....../glTF-LoadImageData

		     291.803 Mt     3.261 %         1 x      291.803 Mt        0.000 Mt      291.803 Mt   100.000 % 	 ...../glTF-CreateScene

		     144.017 Mt     1.610 %         1 x      144.017 Mt        0.000 Mt      144.017 Mt   100.000 % 	 ...../SceneDesc-CreateScene

		       2.436 Mt     0.027 %         1 x        2.436 Mt        0.000 Mt        0.005 Mt     0.213 % 	 ..../ShadowMapRLPrep

		       2.431 Mt    99.787 %         1 x        2.431 Mt        0.000 Mt        1.726 Mt    70.995 % 	 ...../PrepareForRendering

		       0.705 Mt    29.005 %         1 x        0.705 Mt        0.000 Mt        0.705 Mt   100.000 % 	 ....../PrepareDrawBundle

		       5.775 Mt     0.064 %         1 x        5.775 Mt        0.000 Mt        0.005 Mt     0.090 % 	 ..../TexturedRLPrep

		       5.770 Mt    99.910 %         1 x        5.770 Mt        0.000 Mt        0.942 Mt    16.329 % 	 ...../PrepareForRendering

		       2.571 Mt    44.554 %         1 x        2.571 Mt        0.000 Mt        2.571 Mt   100.000 % 	 ....../PrepareMaterials

		       2.257 Mt    39.117 %         1 x        2.257 Mt        0.000 Mt        2.257 Mt   100.000 % 	 ....../PrepareDrawBundle

		       7.656 Mt     0.084 %         1 x        7.656 Mt        0.000 Mt        0.005 Mt     0.063 % 	 ..../DeferredRLPrep

		       7.651 Mt    99.937 %         1 x        7.651 Mt        0.000 Mt        2.978 Mt    38.928 % 	 ...../PrepareForRendering

		       0.021 Mt     0.272 %         1 x        0.021 Mt        0.000 Mt        0.021 Mt   100.000 % 	 ....../PrepareMaterials

		       2.121 Mt    27.722 %         1 x        2.121 Mt        0.000 Mt        2.121 Mt   100.000 % 	 ....../BuildMaterialCache

		       2.531 Mt    33.078 %         1 x        2.531 Mt        0.000 Mt        2.531 Mt   100.000 % 	 ....../PrepareDrawBundle

		      35.835 Mt     0.395 %         1 x       35.835 Mt        0.000 Mt        6.310 Mt    17.608 % 	 ..../RayTracingRLPrep

		       0.599 Mt     1.670 %         1 x        0.599 Mt        0.000 Mt        0.599 Mt   100.000 % 	 ...../PrepareAccelerationStructures

		       0.025 Mt     0.068 %         1 x        0.025 Mt        0.000 Mt        0.025 Mt   100.000 % 	 ...../PrepareCache

		       5.216 Mt    14.555 %         1 x        5.216 Mt        0.000 Mt        5.216 Mt   100.000 % 	 ...../BuildCache

		      12.890 Mt    35.971 %         1 x       12.890 Mt        0.000 Mt        0.005 Mt     0.040 % 	 ...../BuildAccelerationStructures

		      12.885 Mt    99.960 %         1 x       12.885 Mt        0.000 Mt        6.430 Mt    49.902 % 	 ....../AccelerationStructureBuild

		       4.194 Mt    32.547 %         1 x        4.194 Mt        0.000 Mt        4.194 Mt   100.000 % 	 ......./BuildBottomLevelAS

		       2.261 Mt    17.551 %         1 x        2.261 Mt        0.000 Mt        2.261 Mt   100.000 % 	 ......./BuildTopLevelAS

		      10.797 Mt    30.129 %         1 x       10.797 Mt        0.000 Mt       10.797 Mt   100.000 % 	 ...../BuildShaderTables

		      60.292 Mt     0.665 %         1 x       60.292 Mt        0.000 Mt       60.292 Mt   100.000 % 	 ..../GpuSidePrep

		       4.831 Mt     0.007 %         1 x        4.831 Mt        0.000 Mt        4.831 Mt   100.000 % 	 ../DeferredRLInitialization

		      51.586 Mt     0.078 %         1 x       51.586 Mt        0.000 Mt        2.255 Mt     4.372 % 	 ../RayTracingRLInitialization

		      49.331 Mt    95.628 %         1 x       49.331 Mt        0.000 Mt       49.331 Mt   100.000 % 	 .../PipelineBuild

		       5.063 Mt     0.008 %         1 x        5.063 Mt        0.000 Mt        5.063 Mt   100.000 % 	 ../ResolveRLInitialization

		   56595.816 Mt    85.647 %      6006 x        9.423 Mt     1116.354 Mt      304.681 Mt     0.538 % 	 ../MainLoop

		    2393.974 Mt     4.230 %      3329 x        0.719 Mt        0.610 Mt     2392.534 Mt    99.940 % 	 .../Update

		       1.440 Mt     0.060 %         1 x        1.440 Mt        0.000 Mt        1.440 Mt   100.000 % 	 ..../GuiModelApply

		   53897.162 Mt    95.232 %      1288 x       41.846 Mt     1048.334 Mt    50507.410 Mt    93.711 % 	 .../Render

		    3389.752 Mt     6.289 %      1288 x        2.632 Mt        1.189 Mt     1628.211 Mt    48.033 % 	 ..../RayTracing

		    1761.541 Mt    51.967 %      1288 x        1.368 Mt        0.004 Mt       14.670 Mt     0.833 % 	 ...../RayTracingRLPrep

		     291.411 Mt    16.543 %         7 x       41.630 Mt        0.000 Mt      291.411 Mt   100.000 % 	 ....../PrepareAccelerationStructures

		       1.297 Mt     0.074 %         7 x        0.185 Mt        0.000 Mt        1.297 Mt   100.000 % 	 ....../PrepareCache

		       0.006 Mt     0.000 %         7 x        0.001 Mt        0.000 Mt        0.006 Mt   100.000 % 	 ....../BuildCache

		     607.487 Mt    34.486 %         7 x       86.784 Mt        0.000 Mt        0.020 Mt     0.003 % 	 ....../BuildAccelerationStructures

		     607.467 Mt    99.997 %         7 x       86.781 Mt        0.000 Mt       19.215 Mt     3.163 % 	 ......./AccelerationStructureBuild

		     582.694 Mt    95.922 %         7 x       83.242 Mt        0.000 Mt      582.694 Mt   100.000 % 	 ......../BuildBottomLevelAS

		       5.558 Mt     0.915 %         7 x        0.794 Mt        0.000 Mt        5.558 Mt   100.000 % 	 ......../BuildTopLevelAS

		     846.670 Mt    48.064 %         7 x      120.953 Mt        0.000 Mt      846.670 Mt   100.000 % 	 ....../BuildShaderTables

	WindowThread:

		   66081.165 Mt   100.000 %         1 x    66081.167 Mt    66081.164 Mt    66081.165 Mt   100.000 % 	 /

	GpuThread:

		   48983.357 Mt   100.000 %      1288 x       38.031 Mt     1045.633 Mt      254.854 Mt     0.520 % 	 /

		      12.870 Mt     0.026 %         1 x       12.870 Mt        0.000 Mt        0.032 Mt     0.249 % 	 ./ScenePrep

		      12.838 Mt    99.751 %         1 x       12.838 Mt        0.000 Mt        0.003 Mt     0.022 % 	 ../AccelerationStructureBuild

		      12.394 Mt    96.546 %         1 x       12.394 Mt        0.000 Mt       12.394 Mt   100.000 % 	 .../BuildBottomLevelASGpu

		       0.440 Mt     3.431 %         1 x        0.440 Mt        0.000 Mt        0.440 Mt   100.000 % 	 .../BuildTopLevelASGpu

		   48712.796 Mt    99.448 %      1288 x       37.820 Mt     1045.629 Mt        0.705 Mt     0.001 % 	 ./RayTracingGpu

		   48170.610 Mt    98.887 %      1288 x       37.400 Mt     1045.628 Mt    48170.610 Mt   100.000 % 	 ../RayTracingRLGpu

		     541.481 Mt     1.112 %         7 x       77.354 Mt        0.000 Mt        0.005 Mt     0.001 % 	 ../AccelerationStructureBuild

		     539.919 Mt    99.711 %         7 x       77.131 Mt        0.000 Mt      539.919 Mt   100.000 % 	 .../BuildBottomLevelASGpu

		       1.557 Mt     0.288 %         7 x        0.222 Mt        0.000 Mt        1.557 Mt   100.000 % 	 .../BuildTopLevelASGpu

		       2.837 Mt     0.006 %      1288 x        0.002 Mt        0.002 Mt        2.837 Mt   100.000 % 	 ./Present


	============================


