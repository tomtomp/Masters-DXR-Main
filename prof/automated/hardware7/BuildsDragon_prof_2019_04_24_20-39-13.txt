Configuration: 
Command line parameters  = E:\programming\FIT-DIP\Masters-DXR-Main-git\build\Measurement\x64\Debug\Measurement.exe --scene Dragon/Dragon.glb --profile-builds --profile-output BuildsDragon 
Window width             = 1024
Window height            = 768
Window title             = Direct3D App
Back-buffer count        = 3
Fill Triangles           = enabled
Triangles Font Clock-Wise= enabled
Fast Build AS            = disabled
Target FPS               = 60
Target UPS               = 60
Tearing                  = disabled
VSync                    = disabled
GUI rendering            = enabled
Debug layer              = enabled
GPU validation           = disabled
GPU profiling            = enabled
Render mode              = Rasterization
Loaded scene file        = Dragon/Dragon.glb
Using testing scene      = disabled
Duplication              = 1
Duplication in cube      = disabled
Duplication offset       = 10
BLAS duplication         = disabled
Rays per pixel           = 14
Quality preset           = High
Rasterization Quality    : 
  AO                     = enabled
  AO kernel size         = 4
  Shadows                = enabled
  Shadow map resolution  = 4096
  Shadow PCF kernel size = 4
  Reflections            = enabled
Ray Tracing Quality      : 
  AO                     = enabled
  AO rays                = 4
  AO kernel size         = 4
  Shadows                = enabled
  Shadow rays            = 4
  Shadow kernel size     = 4
  Reflections            = enabled

ProfilingAggregator: 

ProfilingBuildAggregator: 
BuildBottomLevelAS	,	1.4909	,	1.137	,	1.5759	,	1.1449
BuildBottomLevelASGpu	,	2.77834	,	2.63088	,	3.79306	,	4.11181
BuildTopLevelAS	,	0.9705	,	0.8933	,	1.2768	,	1.2033
BuildTopLevelASGpu	,	0.358336	,	0.382176	,	0.793952	,	0.392992
Duplication	,	1	,	1	,	1	,	1
Triangles	,	34852	,	11757	,	34852	,	11757
Meshes	,	1	,	1	,	1	,	1
BottomLevels	,	1	,	1	,	1	,	1
TopLevelSize	,	64	,	64	,	64	,	64
BottomLevelsSize	,	1947136	,	659968	,	2450816	,	829568
Index	,	0	,	1	,	2	,	3

FpsAggregator: 
FPS	,	0	,	0	,	0	,	0
UPS	,	0	,	0	,	0	,	0
FrameTime	,	0	,	0	,	0	,	0
GigaRays/s	,	0	,	0	,	0	,	0
Index	,	0	,	1	,	2	,	3


AccelerationStructure: 
Ray tracing acceleration structure building statistics: 
	Bottom Level Acceleration Structure: 
		Maximum [B]: 1947136
		Minimum [B]: 659968
		Currently Allocated [B]: 2607104
		Currently Created [num]: 2
		Current Average [B]: 1303552
		Maximum Triangles [num]: 34852
		Minimum Triangles [num]: 11757
		Total Triangles [num]: 46609
		Maximum Meshes [num]: 1
		Minimum Meshes [num]: 1
		Total Meshes [num]: 2
	Top Level Acceleration Structure: 
		Maximum [B]: 128
		Minimum [B]: 128
		Currently Allocated [B]: 128
		Total Allocated [B]: 128
		Total Created [num]: 1
		Average Allocated [B]: 128
		Maximum Instances [num]: 2
		Minimum Instances [num]: 2
		Total Instances [num]: 2
	Scratch Buffer: 
		Maximum [B]: 290304
		Minimum [B]: 290304
		Currently Allocated [B]: 290304
		Total Allocated [B]: 290304
		Total Created [num]: 1
		Average Allocated [B]: 290304
ProfilingStack: 
[PROF] Profiling statistics: 
	Scopes entered : 79
	Scopes exited : 78
	Overhead per scope [ticks] : 997.6
	Threads entered : 3
	Threads exited : 0
	============================
		         Sum[Mt]   Parent[%]  Samples[num]       Avg[Mt]   Last/Curr[Mt]        Self[Mt]     Self[%] 	 Name

	main:

		    2766.505 Mt   100.000 %         1 x     2766.509 Mt     2766.504 Mt        0.721 Mt     0.026 % 	 /

		    2765.924 Mt    99.979 %         1 x     2765.930 Mt     2765.923 Mt      369.711 Mt    13.367 % 	 ./Main application loop

		       2.908 Mt     0.105 %         1 x        2.908 Mt        2.908 Mt        2.908 Mt   100.000 % 	 ../TexturedRLInitialization

		       2.315 Mt     0.084 %         1 x        2.315 Mt        2.315 Mt        2.315 Mt   100.000 % 	 ../ShadowMapRLInitialization

		       3.964 Mt     0.143 %         1 x        3.964 Mt        3.964 Mt        3.964 Mt   100.000 % 	 ../AmbientOcclusionRLInitialization

		       5.021 Mt     0.182 %         1 x        5.021 Mt        5.021 Mt        5.021 Mt   100.000 % 	 ../RasterResolveRLInitialization

		    2261.214 Mt    81.753 %         1 x     2261.214 Mt     2261.214 Mt        7.215 Mt     0.319 % 	 ../Initialization

		    2253.999 Mt    99.681 %         1 x     2253.999 Mt     2253.999 Mt        2.884 Mt     0.128 % 	 .../ScenePrep

		    2194.728 Mt    97.370 %         1 x     2194.728 Mt     2194.728 Mt       91.112 Mt     4.151 % 	 ..../SceneLoad

		    1976.822 Mt    90.071 %         1 x     1976.822 Mt     1976.822 Mt      153.616 Mt     7.771 % 	 ...../glTF-LoadScene

		    1823.205 Mt    92.229 %         8 x      227.901 Mt      363.897 Mt     1823.205 Mt   100.000 % 	 ....../glTF-LoadImageData

		      97.439 Mt     4.440 %         1 x       97.439 Mt       97.439 Mt       97.439 Mt   100.000 % 	 ...../glTF-CreateScene

		      29.355 Mt     1.338 %         1 x       29.355 Mt       29.355 Mt       29.355 Mt   100.000 % 	 ...../SceneDesc-CreateScene

		       0.869 Mt     0.039 %         1 x        0.869 Mt        0.869 Mt        0.005 Mt     0.575 % 	 ..../ShadowMapRLPrep

		       0.864 Mt    99.425 %         1 x        0.864 Mt        0.864 Mt        0.779 Mt    90.095 % 	 ...../PrepareForRendering

		       0.086 Mt     9.905 %         1 x        0.086 Mt        0.086 Mt        0.086 Mt   100.000 % 	 ....../PrepareDrawBundle

		       3.115 Mt     0.138 %         1 x        3.115 Mt        3.115 Mt        0.004 Mt     0.144 % 	 ..../TexturedRLPrep

		       3.110 Mt    99.856 %         1 x        3.110 Mt        3.110 Mt        1.727 Mt    55.542 % 	 ...../PrepareForRendering

		       1.100 Mt    35.378 %         1 x        1.100 Mt        1.100 Mt        1.100 Mt   100.000 % 	 ....../PrepareMaterials

		       0.282 Mt     9.080 %         1 x        0.282 Mt        0.282 Mt        0.282 Mt   100.000 % 	 ....../PrepareDrawBundle

		       5.862 Mt     0.260 %         1 x        5.862 Mt        5.862 Mt        0.005 Mt     0.090 % 	 ..../DeferredRLPrep

		       5.856 Mt    99.910 %         1 x        5.856 Mt        5.856 Mt        4.769 Mt    81.438 % 	 ...../PrepareForRendering

		       0.029 Mt     0.492 %         1 x        0.029 Mt        0.029 Mt        0.029 Mt   100.000 % 	 ....../PrepareMaterials

		       0.764 Mt    13.042 %         1 x        0.764 Mt        0.764 Mt        0.764 Mt   100.000 % 	 ....../BuildMaterialCache

		       0.294 Mt     5.029 %         1 x        0.294 Mt        0.294 Mt        0.294 Mt   100.000 % 	 ....../PrepareDrawBundle

		      18.409 Mt     0.817 %         1 x       18.409 Mt       18.409 Mt        5.342 Mt    29.018 % 	 ..../RayTracingRLPrep

		       0.419 Mt     2.275 %         1 x        0.419 Mt        0.419 Mt        0.419 Mt   100.000 % 	 ...../PrepareAccelerationStructures

		       0.038 Mt     0.206 %         1 x        0.038 Mt        0.038 Mt        0.038 Mt   100.000 % 	 ...../PrepareCache

		       0.949 Mt     5.156 %         1 x        0.949 Mt        0.949 Mt        0.949 Mt   100.000 % 	 ...../BuildCache

		       9.159 Mt    49.752 %         1 x        9.159 Mt        9.159 Mt        0.004 Mt     0.043 % 	 ...../BuildAccelerationStructures

		       9.155 Mt    99.957 %         1 x        9.155 Mt        9.155 Mt        4.347 Mt    47.484 % 	 ....../AccelerationStructureBuild

		       1.941 Mt    21.198 %         1 x        1.941 Mt        1.941 Mt        1.941 Mt   100.000 % 	 ......./BuildBottomLevelAS

		       2.867 Mt    31.317 %         1 x        2.867 Mt        2.867 Mt        2.867 Mt   100.000 % 	 ......./BuildTopLevelAS

		       2.502 Mt    13.594 %         1 x        2.502 Mt        2.502 Mt        2.502 Mt   100.000 % 	 ...../BuildShaderTables

		      28.133 Mt     1.248 %         1 x       28.133 Mt       28.133 Mt       28.133 Mt   100.000 % 	 ..../GpuSidePrep

		      45.470 Mt     1.644 %         5 x        9.094 Mt        7.531 Mt       29.356 Mt    64.562 % 	 ../AccelerationStructureBuild

		       9.845 Mt    21.653 %         5 x        1.969 Mt        1.145 Mt        9.845 Mt   100.000 % 	 .../BuildBottomLevelAS

		       6.268 Mt    13.786 %         5 x        1.254 Mt        1.203 Mt        6.268 Mt   100.000 % 	 .../BuildTopLevelAS

		       4.380 Mt     0.158 %         1 x        4.380 Mt        4.380 Mt        4.380 Mt   100.000 % 	 ../DeferredRLInitialization

		      65.977 Mt     2.385 %         1 x       65.977 Mt       65.977 Mt        2.261 Mt     3.427 % 	 ../RayTracingRLInitialization

		      63.716 Mt    96.573 %         1 x       63.716 Mt       63.716 Mt       63.716 Mt   100.000 % 	 .../PipelineBuild

		       4.966 Mt     0.180 %         1 x        4.966 Mt        4.966 Mt        4.966 Mt   100.000 % 	 ../ResolveRLInitialization

	WindowThread:

		    2759.481 Mt   100.000 %         1 x     2759.482 Mt     2759.479 Mt     2759.481 Mt   100.000 % 	 /

	GpuThread:

		      87.127 Mt   100.000 %         4 x       21.782 Mt        4.509 Mt       63.720 Mt    73.134 % 	 /

		       5.006 Mt     5.745 %         1 x        5.006 Mt        5.006 Mt        0.046 Mt     0.928 % 	 ./ScenePrep

		       4.959 Mt    99.072 %         1 x        4.959 Mt        4.959 Mt        0.003 Mt     0.057 % 	 ../AccelerationStructureBuild

		       3.767 Mt    75.964 %         1 x        3.767 Mt        3.767 Mt        3.767 Mt   100.000 % 	 .../BuildBottomLevelASGpu

		       1.189 Mt    23.979 %         1 x        1.189 Mt        1.189 Mt        1.189 Mt   100.000 % 	 .../BuildTopLevelASGpu

		      18.402 Mt    21.121 %         5 x        3.680 Mt        4.509 Mt        0.024 Mt     0.130 % 	 ./AccelerationStructureBuild

		      16.092 Mt    87.449 %         5 x        3.218 Mt        4.112 Mt       16.092 Mt   100.000 % 	 ../BuildBottomLevelASGpu

		       2.286 Mt    12.421 %         5 x        0.457 Mt        0.393 Mt        2.286 Mt   100.000 % 	 ../BuildTopLevelASGpu


	============================


