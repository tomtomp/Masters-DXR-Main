Configuration: 
Command line parameters  = E:\programming\FIT-DIP\Masters-DXR-Main-git\build\Measurement\x64\Debug\Measurement.exe --scene Cube/Cube.gltf --duplication-cube --profile-duplication --profile-max-duplication 10 --rt-mode --width 1024 --height 768 --profile-output DuplicationCube768Rt 
Window width             = 1024
Window height            = 768
Window title             = Direct3D App
Back-buffer count        = 3
Fill Triangles           = enabled
Triangles Font Clock-Wise= enabled
Fast Build AS            = disabled
Target FPS               = 60
Target UPS               = 60
Tearing                  = disabled
VSync                    = disabled
GUI rendering            = disabled
Debug layer              = enabled
GPU validation           = disabled
GPU profiling            = enabled
Render mode              = Ray Tracing
Loaded scene file        = Cube/Cube.gltf
Using testing scene      = disabled
Duplication              = 10
Duplication in cube      = enabled
Duplication offset       = 10
BLAS duplication         = enabled
Rays per pixel           = 14
Quality preset           = High
Rasterization Quality    : 
  AO                     = enabled
  AO kernel size         = 4
  Shadows                = enabled
  Shadow map resolution  = 4096
  Shadow PCF kernel size = 4
  Reflections            = enabled
Ray Tracing Quality      : 
  AO                     = enabled
  AO rays                = 4
  AO kernel size         = 4
  Shadows                = enabled
  Shadow rays            = 4
  Shadow kernel size     = 4
  Reflections            = enabled

ProfilingAggregator: 

ProfilingBuildAggregator: 

FpsAggregator: 


AccelerationStructure: 
Ray tracing acceleration structure building statistics: 
	Bottom Level Acceleration Structure: 
		Maximum [B]: 5168
		Minimum [B]: 5168
		Currently Allocated [B]: 5168000
		Currently Created [num]: 1000
		Current Average [B]: 5168
		Maximum Triangles [num]: 12
		Minimum Triangles [num]: 12
		Total Triangles [num]: 12000
		Maximum Meshes [num]: 1
		Minimum Meshes [num]: 1
		Total Meshes [num]: 1000
	Top Level Acceleration Structure: 
		Maximum [B]: 64000
		Minimum [B]: 64000
		Currently Allocated [B]: 64000
		Total Allocated [B]: 64000
		Total Created [num]: 1
		Average Allocated [B]: 64000
		Maximum Instances [num]: 1000
		Minimum Instances [num]: 1000
		Total Instances [num]: 1000
	Scratch Buffer: 
		Maximum [B]: 79360
		Minimum [B]: 79360
		Currently Allocated [B]: 79360
		Total Allocated [B]: 79360
		Total Created [num]: 1
		Average Allocated [B]: 79360
ProfilingStack: 
[PROF] Profiling statistics: 
	Scopes entered : 2586868
	Scopes exited : 2586867
	Overhead per scope [ticks] : 1029.3
	Threads entered : 3
	Threads exited : 0
	============================
		         Sum[Mt]   Parent[%]  Samples[num]       Avg[Mt]   Last/Curr[Mt]        Self[Mt]     Self[%] 	 Name

	main:

		   21862.494 Mt   100.000 %         1 x    21862.496 Mt    21862.493 Mt        0.929 Mt     0.004 % 	 /

		   21861.616 Mt    99.996 %         1 x    21861.619 Mt    21861.615 Mt     4726.427 Mt    21.620 % 	 ./Main application loop

		       2.935 Mt     0.013 %         1 x        2.935 Mt        0.000 Mt        2.935 Mt   100.000 % 	 ../TexturedRLInitialization

		       2.478 Mt     0.011 %         1 x        2.478 Mt        0.000 Mt        2.478 Mt   100.000 % 	 ../ShadowMapRLInitialization

		       5.118 Mt     0.023 %         1 x        5.118 Mt        0.000 Mt        5.118 Mt   100.000 % 	 ../AmbientOcclusionRLInitialization

		       5.120 Mt     0.023 %         1 x        5.120 Mt        0.000 Mt        5.120 Mt   100.000 % 	 ../RasterResolveRLInitialization

		     144.533 Mt     0.661 %         1 x      144.533 Mt        0.000 Mt        4.567 Mt     3.160 % 	 ../Initialization

		     139.965 Mt    96.840 %         1 x      139.965 Mt        0.000 Mt        2.242 Mt     1.602 % 	 .../ScenePrep

		     105.798 Mt    75.589 %         1 x      105.798 Mt        0.000 Mt       13.824 Mt    13.066 % 	 ..../SceneLoad

		      59.062 Mt    55.825 %         1 x       59.062 Mt        0.000 Mt       13.794 Mt    23.355 % 	 ...../glTF-LoadScene

		      45.268 Mt    76.645 %         2 x       22.634 Mt        0.000 Mt       45.268 Mt   100.000 % 	 ....../glTF-LoadImageData

		      18.241 Mt    17.242 %         1 x       18.241 Mt        0.000 Mt       18.241 Mt   100.000 % 	 ...../glTF-CreateScene

		      14.671 Mt    13.867 %         1 x       14.671 Mt        0.000 Mt       14.671 Mt   100.000 % 	 ...../SceneDesc-CreateScene

		       0.909 Mt     0.649 %         1 x        0.909 Mt        0.000 Mt        0.007 Mt     0.737 % 	 ..../ShadowMapRLPrep

		       0.902 Mt    99.263 %         1 x        0.902 Mt        0.000 Mt        0.838 Mt    92.840 % 	 ...../PrepareForRendering

		       0.065 Mt     7.160 %         1 x        0.065 Mt        0.000 Mt        0.065 Mt   100.000 % 	 ....../PrepareDrawBundle

		       2.868 Mt     2.049 %         1 x        2.868 Mt        0.000 Mt        0.005 Mt     0.174 % 	 ..../TexturedRLPrep

		       2.863 Mt    99.826 %         1 x        2.863 Mt        0.000 Mt        1.143 Mt    39.927 % 	 ...../PrepareForRendering

		       1.082 Mt    37.793 %         1 x        1.082 Mt        0.000 Mt        1.082 Mt   100.000 % 	 ....../PrepareMaterials

		       0.638 Mt    22.279 %         1 x        0.638 Mt        0.000 Mt        0.638 Mt   100.000 % 	 ....../PrepareDrawBundle

		       3.355 Mt     2.397 %         1 x        3.355 Mt        0.000 Mt        0.010 Mt     0.313 % 	 ..../DeferredRLPrep

		       3.345 Mt    99.687 %         1 x        3.345 Mt        0.000 Mt        2.085 Mt    62.333 % 	 ...../PrepareForRendering

		       0.039 Mt     1.160 %         1 x        0.039 Mt        0.000 Mt        0.039 Mt   100.000 % 	 ....../PrepareMaterials

		       0.937 Mt    28.026 %         1 x        0.937 Mt        0.000 Mt        0.937 Mt   100.000 % 	 ....../BuildMaterialCache

		       0.284 Mt     8.482 %         1 x        0.284 Mt        0.000 Mt        0.284 Mt   100.000 % 	 ....../PrepareDrawBundle

		      15.769 Mt    11.267 %         1 x       15.769 Mt        0.000 Mt        4.429 Mt    28.088 % 	 ..../RayTracingRLPrep

		       0.261 Mt     1.654 %         1 x        0.261 Mt        0.000 Mt        0.261 Mt   100.000 % 	 ...../PrepareAccelerationStructures

		       0.026 Mt     0.164 %         1 x        0.026 Mt        0.000 Mt        0.026 Mt   100.000 % 	 ...../PrepareCache

		       1.206 Mt     7.647 %         1 x        1.206 Mt        0.000 Mt        1.206 Mt   100.000 % 	 ...../BuildCache

		       5.954 Mt    37.757 %         1 x        5.954 Mt        0.000 Mt        0.007 Mt     0.111 % 	 ...../BuildAccelerationStructures

		       5.947 Mt    99.889 %         1 x        5.947 Mt        0.000 Mt        3.807 Mt    64.016 % 	 ....../AccelerationStructureBuild

		       1.088 Mt    18.299 %         1 x        1.088 Mt        0.000 Mt        1.088 Mt   100.000 % 	 ......./BuildBottomLevelAS

		       1.052 Mt    17.685 %         1 x        1.052 Mt        0.000 Mt        1.052 Mt   100.000 % 	 ......./BuildTopLevelAS

		       3.893 Mt    24.690 %         1 x        3.893 Mt        0.000 Mt        3.893 Mt   100.000 % 	 ...../BuildShaderTables

		       9.024 Mt     6.447 %         1 x        9.024 Mt        0.000 Mt        9.024 Mt   100.000 % 	 ..../GpuSidePrep

		       5.090 Mt     0.023 %         1 x        5.090 Mt        0.000 Mt        5.090 Mt   100.000 % 	 ../DeferredRLInitialization

		      64.819 Mt     0.296 %         1 x       64.819 Mt        0.000 Mt        2.337 Mt     3.606 % 	 ../RayTracingRLInitialization

		      62.482 Mt    96.394 %         1 x       62.482 Mt        0.000 Mt       62.482 Mt   100.000 % 	 .../PipelineBuild

		       4.694 Mt     0.021 %         1 x        4.694 Mt        0.000 Mt        4.694 Mt   100.000 % 	 ../ResolveRLInitialization

		   16900.402 Mt    77.306 %   2577051 x        0.007 Mt       18.645 Mt     4575.653 Mt    27.074 % 	 ../MainLoop

		    2007.568 Mt    11.879 %      1232 x        1.630 Mt        1.622 Mt     2006.347 Mt    99.939 % 	 .../Update

		       1.221 Mt     0.061 %         1 x        1.221 Mt        0.000 Mt        1.221 Mt   100.000 % 	 ..../GuiModelApply

		   10317.180 Mt    61.047 %       924 x       11.166 Mt        0.000 Mt     4868.250 Mt    47.186 % 	 .../Render

		    5448.930 Mt    52.814 %       924 x        5.897 Mt        0.000 Mt     2694.467 Mt    49.449 % 	 ..../RayTracing

		     126.592 Mt     2.323 %       924 x        0.137 Mt        0.000 Mt        3.064 Mt     2.421 % 	 ...../DeferredRLPrep

		     123.527 Mt    97.579 %        15 x        8.235 Mt        0.000 Mt        5.734 Mt     4.642 % 	 ....../PrepareForRendering

		      12.778 Mt    10.344 %        15 x        0.852 Mt        0.000 Mt       12.778 Mt   100.000 % 	 ......./PrepareMaterials

		       0.012 Mt     0.009 %        15 x        0.001 Mt        0.000 Mt        0.012 Mt   100.000 % 	 ......./BuildMaterialCache

		     105.004 Mt    85.005 %        15 x        7.000 Mt        0.000 Mt      105.004 Mt   100.000 % 	 ......./PrepareDrawBundle

		    2627.872 Mt    48.227 %       924 x        2.844 Mt        0.000 Mt       38.393 Mt     1.461 % 	 ...../RayTracingRLPrep

		    1111.682 Mt    42.304 %        15 x       74.112 Mt        0.000 Mt     1111.682 Mt   100.000 % 	 ....../PrepareAccelerationStructures

		      12.867 Mt     0.490 %        15 x        0.858 Mt        0.000 Mt       12.867 Mt   100.000 % 	 ....../PrepareCache

		       0.011 Mt     0.000 %        15 x        0.001 Mt        0.000 Mt        0.011 Mt   100.000 % 	 ....../BuildCache

		    1354.474 Mt    51.543 %        15 x       90.298 Mt        0.000 Mt        0.039 Mt     0.003 % 	 ....../BuildAccelerationStructures

		    1354.435 Mt    99.997 %        15 x       90.296 Mt        0.000 Mt       57.832 Mt     4.270 % 	 ......./AccelerationStructureBuild

		    1284.050 Mt    94.803 %        15 x       85.603 Mt        0.000 Mt     1284.050 Mt   100.000 % 	 ......../BuildBottomLevelAS

		      12.553 Mt     0.927 %        15 x        0.837 Mt        0.000 Mt       12.553 Mt   100.000 % 	 ......../BuildTopLevelAS

		     110.445 Mt     4.203 %        15 x        7.363 Mt        0.000 Mt      110.445 Mt   100.000 % 	 ....../BuildShaderTables

	WindowThread:

		   21858.187 Mt   100.000 %         1 x    21858.188 Mt    21858.187 Mt    21858.187 Mt   100.000 % 	 /

	GpuThread:

		    3499.089 Mt   100.000 %       924 x        3.787 Mt        0.000 Mt      112.380 Mt     3.212 % 	 /

		       0.217 Mt     0.006 %         1 x        0.217 Mt        0.000 Mt        0.006 Mt     2.711 % 	 ./ScenePrep

		       0.211 Mt    97.289 %         1 x        0.211 Mt        0.000 Mt        0.001 Mt     0.575 % 	 ../AccelerationStructureBuild

		       0.125 Mt    58.964 %         1 x        0.125 Mt        0.000 Mt        0.125 Mt   100.000 % 	 .../BuildBottomLevelASGpu

		       0.086 Mt    40.460 %         1 x        0.086 Mt        0.000 Mt        0.086 Mt   100.000 % 	 .../BuildTopLevelASGpu

		    3305.534 Mt    94.468 %       924 x        3.577 Mt        0.000 Mt        4.302 Mt     0.130 % 	 ./RayTracingGpu

		     191.228 Mt     5.785 %       924 x        0.207 Mt        0.000 Mt      191.228 Mt   100.000 % 	 ../DeferredRLGpu

		     563.299 Mt    17.041 %       924 x        0.610 Mt        0.000 Mt      563.299 Mt   100.000 % 	 ../RayTracingRLGpu

		    1500.459 Mt    45.392 %       924 x        1.624 Mt        0.000 Mt     1500.459 Mt   100.000 % 	 ../ResolveRLGpu

		    1046.246 Mt    31.651 %        15 x       69.750 Mt        0.000 Mt        0.040 Mt     0.004 % 	 ../AccelerationStructureBuild

		    1039.383 Mt    99.344 %        15 x       69.292 Mt        0.000 Mt     1039.383 Mt   100.000 % 	 .../BuildBottomLevelASGpu

		       6.823 Mt     0.652 %        15 x        0.455 Mt        0.000 Mt        6.823 Mt   100.000 % 	 .../BuildTopLevelASGpu

		      80.958 Mt     2.314 %       924 x        0.088 Mt        0.000 Mt       80.958 Mt   100.000 % 	 ./Present


	============================


