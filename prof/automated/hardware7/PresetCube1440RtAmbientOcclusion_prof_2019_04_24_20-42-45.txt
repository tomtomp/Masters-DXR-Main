Configuration: 
Command line parameters  = E:\programming\FIT-DIP\Masters-DXR-Main-git\build\Measurement\x64\Debug\Measurement.exe --scene Cube/Cube.gltf --profile-automation --profile-camera-track Cube/Cube.trk --rt-mode --width 2560 --height 1440 --profile-output PresetCube1440RtAmbientOcclusion --quality-preset AmbientOcclusion 
Window width             = 2560
Window height            = 1421
Window title             = Direct3D App
Back-buffer count        = 3
Fill Triangles           = enabled
Triangles Font Clock-Wise= enabled
Fast Build AS            = disabled
Target FPS               = 0
Target UPS               = 60
Tearing                  = enabled
VSync                    = disabled
GUI rendering            = disabled
Debug layer              = enabled
GPU validation           = disabled
GPU profiling            = enabled
Render mode              = Ray Tracing
Loaded scene file        = Cube/Cube.gltf
Using testing scene      = disabled
Duplication              = 1
Duplication in cube      = disabled
Duplication offset       = 10
BLAS duplication         = disabled
Rays per pixel           = 5
Quality preset           = AmbientOcclusion
Rasterization Quality    : 
  AO                     = enabled
  AO kernel size         = 4
  Shadows                = disabled
  Shadow map resolution  = 1
  Shadow PCF kernel size = 0
  Reflections            = disabled
Ray Tracing Quality      : 
  AO                     = enabled
  AO rays                = 4
  AO kernel size         = 4
  Shadows                = disabled
  Shadow rays            = 0
  Shadow kernel size     = 4
  Reflections            = disabled

ProfilingAggregator: 
Render	,	4.6933	,	4.7054	,	9.2555	,	4.6394	,	5.6337	,	4.3421	,	5.0384	,	6.0187	,	5.3409	,	5.7374	,	5.478	,	4.4407	,	4.1009	,	4.8573	,	5.6414	,	4.5775	,	5.0004	,	5.3012	,	5.4513	,	5.3752	,	5.1745	,	4.2738	,	3.9356	,	3.9729	,	4.1241	,	5.2561	,	3.8004	,	3.9036	,	3.9874
Update	,	1.2463	,	0.7855	,	1.5918	,	0.8718	,	1.3258	,	0.4967	,	0.7876	,	1.0687	,	0.6821	,	1.0934	,	0.7345	,	0.6399	,	1.04	,	0.7075	,	1.1774	,	0.4968	,	1.5932	,	0.7299	,	0.5466	,	0.5131	,	0.5219	,	0.4898	,	0.9633	,	0.5295	,	0.7963	,	1.1901	,	0.5305	,	0.5784	,	1.1122
RayTracing	,	1.7105	,	1.6551	,	4.4849	,	1.4223	,	2.0257	,	1.2304	,	1.7739	,	1.9804	,	1.5825	,	2.1453	,	1.9804	,	1.2752	,	1.237	,	1.7435	,	2.2538	,	1.2437	,	1.2569	,	1.2831	,	1.4567	,	1.3433	,	1.476	,	1.266	,	1.2751	,	1.2765	,	1.2527	,	2.3608	,	1.246	,	1.2258	,	1.2279
RayTracingGpu	,	1.62186	,	1.88653	,	1.89754	,	1.98538	,	2.03702	,	2.12886	,	2.0207	,	2.39469	,	2.40144	,	2.1999	,	2.12442	,	2.21718	,	1.9231	,	1.81869	,	1.96074	,	2.46691	,	2.58237	,	2.91264	,	2.79424	,	2.78035	,	2.48294	,	1.98163	,	1.74794	,	1.53827	,	1.61235	,	1.51914	,	1.48259	,	1.47914	,	1.52173
DeferredRLGpu	,	0.211296	,	0.28736	,	0.29392	,	0.302528	,	0.295776	,	0.333728	,	0.306912	,	0.351904	,	0.4096	,	0.357984	,	0.320512	,	0.346144	,	0.27584	,	0.24976	,	0.28432	,	0.332992	,	0.392096	,	0.466336	,	0.487008	,	0.486752	,	0.407936	,	0.285632	,	0.233216	,	0.18096	,	0.177056	,	0.145472	,	0.117504	,	0.103776	,	0.09696
RayTracingRLGpu	,	0.439424	,	0.578464	,	0.592608	,	0.669792	,	0.796928	,	0.894304	,	0.81472	,	1.12557	,	1.06986	,	0.927488	,	0.900192	,	0.960736	,	0.758784	,	0.688736	,	0.788448	,	1.20179	,	1.09792	,	1.5079	,	1.36083	,	1.3593	,	1.16858	,	0.830304	,	0.668768	,	0.522336	,	0.50416	,	0.414016	,	0.350752	,	0.31696	,	0.302752
ResolveRLGpu	,	0.969728	,	1.01926	,	1.00976	,	1.01197	,	0.943136	,	0.899744	,	0.898176	,	0.916192	,	0.92096	,	0.913536	,	0.902912	,	0.909152	,	0.887584	,	0.87904	,	0.886784	,	0.931072	,	1.09114	,	0.937184	,	0.945376	,	0.933248	,	0.905504	,	0.86464	,	0.845152	,	0.833952	,	0.92976	,	0.958336	,	1.01267	,	1.05658	,	1.12029
Index	,	0	,	1	,	2	,	3	,	4	,	5	,	6	,	7	,	8	,	9	,	10	,	11	,	12	,	13	,	14	,	15	,	16	,	17	,	18	,	19	,	20	,	21	,	22	,	23	,	24	,	25	,	26	,	27	,	28

ProfilingBuildAggregator: 
BuildBottomLevelAS	,	1.0542
BuildBottomLevelASGpu	,	0.09296
BuildTopLevelAS	,	0.9409
BuildTopLevelASGpu	,	0.060864
Duplication	,	1
Triangles	,	12
Meshes	,	1
BottomLevels	,	1
TopLevelSize	,	64
BottomLevelsSize	,	5168
Index	,	0

FpsAggregator: 
FPS	,	191	,	186	,	180	,	180	,	179	,	176	,	173	,	179	,	175	,	151	,	181	,	168	,	175	,	189	,	188	,	183	,	164	,	148	,	143	,	142	,	152	,	178	,	195	,	193	,	199	,	198	,	205	,	209	,	205
UPS	,	59	,	61	,	60	,	60	,	60	,	61	,	60	,	60	,	60	,	61	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	61	,	60	,	60	,	60	,	60	,	61	,	60	,	60	,	60	,	60	,	60
FrameTime	,	5.24132	,	5.4037	,	5.59257	,	5.57739	,	5.5975	,	5.70328	,	5.79321	,	5.61737	,	5.74319	,	6.65742	,	5.52785	,	5.95289	,	5.72326	,	5.30223	,	5.34137	,	5.47652	,	6.10128	,	6.77684	,	7.01317	,	7.06265	,	6.61221	,	5.62077	,	5.14726	,	5.20409	,	5.04078	,	5.06323	,	4.87818	,	4.80228	,	4.89292
GigaRays/s	,	3.47027	,	3.36599	,	3.25232	,	3.26117	,	3.24945	,	3.18918	,	3.13968	,	3.23795	,	3.16702	,	2.73211	,	3.29039	,	3.05546	,	3.17805	,	3.4304	,	3.40527	,	3.32123	,	2.98115	,	2.68396	,	2.59352	,	2.57535	,	2.75079	,	3.236	,	3.53369	,	3.4951	,	3.60833	,	3.59233	,	3.7286	,	3.78754	,	3.71737
Index	,	0	,	1	,	2	,	3	,	4	,	5	,	6	,	7	,	8	,	9	,	10	,	11	,	12	,	13	,	14	,	15	,	16	,	17	,	18	,	19	,	20	,	21	,	22	,	23	,	24	,	25	,	26	,	27	,	28


AccelerationStructure: 
Ray tracing acceleration structure building statistics: 
	Bottom Level Acceleration Structure: 
		Maximum [B]: 5168
		Minimum [B]: 5168
		Currently Allocated [B]: 5168
		Currently Created [num]: 1
		Current Average [B]: 5168
		Maximum Triangles [num]: 12
		Minimum Triangles [num]: 12
		Total Triangles [num]: 12
		Maximum Meshes [num]: 1
		Minimum Meshes [num]: 1
		Total Meshes [num]: 1
	Top Level Acceleration Structure: 
		Maximum [B]: 64
		Minimum [B]: 64
		Currently Allocated [B]: 64
		Total Allocated [B]: 64
		Total Created [num]: 1
		Average Allocated [B]: 64
		Maximum Instances [num]: 1
		Minimum Instances [num]: 1
		Total Instances [num]: 1
	Scratch Buffer: 
		Maximum [B]: 3584
		Minimum [B]: 3584
		Currently Allocated [B]: 3584
		Total Allocated [B]: 3584
		Total Created [num]: 1
		Average Allocated [B]: 3584
ProfilingStack: 
[PROF] Profiling statistics: 
	Scopes entered : 59084
	Scopes exited : 59083
	Overhead per scope [ticks] : 996.3
	Threads entered : 3
	Threads exited : 0
	============================
		         Sum[Mt]   Parent[%]  Samples[num]       Avg[Mt]   Last/Curr[Mt]        Self[Mt]     Self[%] 	 Name

	main:

		   29610.218 Mt   100.000 %         1 x    29610.219 Mt    29610.217 Mt        0.753 Mt     0.003 % 	 /

		   29609.509 Mt    99.998 %         1 x    29609.511 Mt    29609.508 Mt      347.711 Mt     1.174 % 	 ./Main application loop

		       3.414 Mt     0.012 %         1 x        3.414 Mt        0.000 Mt        3.414 Mt   100.000 % 	 ../TexturedRLInitialization

		       2.479 Mt     0.008 %         1 x        2.479 Mt        0.000 Mt        2.479 Mt   100.000 % 	 ../ShadowMapRLInitialization

		       3.916 Mt     0.013 %         1 x        3.916 Mt        0.000 Mt        3.916 Mt   100.000 % 	 ../AmbientOcclusionRLInitialization

		       5.951 Mt     0.020 %         1 x        5.951 Mt        0.000 Mt        5.951 Mt   100.000 % 	 ../RasterResolveRLInitialization

		     111.727 Mt     0.377 %         1 x      111.727 Mt        0.000 Mt        3.610 Mt     3.231 % 	 ../Initialization

		     108.117 Mt    96.769 %         1 x      108.117 Mt        0.000 Mt        1.138 Mt     1.052 % 	 .../ScenePrep

		      76.078 Mt    70.367 %         1 x       76.078 Mt        0.000 Mt       11.772 Mt    15.474 % 	 ..../SceneLoad

		      57.381 Mt    75.424 %         1 x       57.381 Mt        0.000 Mt       12.527 Mt    21.831 % 	 ...../glTF-LoadScene

		      44.854 Mt    78.169 %         2 x       22.427 Mt        0.000 Mt       44.854 Mt   100.000 % 	 ....../glTF-LoadImageData

		       3.918 Mt     5.150 %         1 x        3.918 Mt        0.000 Mt        3.918 Mt   100.000 % 	 ...../glTF-CreateScene

		       3.007 Mt     3.953 %         1 x        3.007 Mt        0.000 Mt        3.007 Mt   100.000 % 	 ...../SceneDesc-CreateScene

		       0.448 Mt     0.415 %         1 x        0.448 Mt        0.000 Mt        0.005 Mt     1.048 % 	 ..../ShadowMapRLPrep

		       0.444 Mt    98.952 %         1 x        0.444 Mt        0.000 Mt        0.387 Mt    87.334 % 	 ...../PrepareForRendering

		       0.056 Mt    12.666 %         1 x        0.056 Mt        0.000 Mt        0.056 Mt   100.000 % 	 ....../PrepareDrawBundle

		       1.041 Mt     0.963 %         1 x        1.041 Mt        0.000 Mt        0.005 Mt     0.451 % 	 ..../TexturedRLPrep

		       1.036 Mt    99.549 %         1 x        1.036 Mt        0.000 Mt        0.375 Mt    36.215 % 	 ...../PrepareForRendering

		       0.414 Mt    39.988 %         1 x        0.414 Mt        0.000 Mt        0.414 Mt   100.000 % 	 ....../PrepareMaterials

		       0.247 Mt    23.796 %         1 x        0.247 Mt        0.000 Mt        0.247 Mt   100.000 % 	 ....../PrepareDrawBundle

		       4.409 Mt     4.078 %         1 x        4.409 Mt        0.000 Mt        0.004 Mt     0.100 % 	 ..../DeferredRLPrep

		       4.405 Mt    99.900 %         1 x        4.405 Mt        0.000 Mt        1.626 Mt    36.915 % 	 ...../PrepareForRendering

		       0.015 Mt     0.336 %         1 x        0.015 Mt        0.000 Mt        0.015 Mt   100.000 % 	 ....../PrepareMaterials

		       2.502 Mt    56.803 %         1 x        2.502 Mt        0.000 Mt        2.502 Mt   100.000 % 	 ....../BuildMaterialCache

		       0.262 Mt     5.946 %         1 x        0.262 Mt        0.000 Mt        0.262 Mt   100.000 % 	 ....../PrepareDrawBundle

		      14.376 Mt    13.297 %         1 x       14.376 Mt        0.000 Mt        3.810 Mt    26.505 % 	 ..../RayTracingRLPrep

		       0.228 Mt     1.587 %         1 x        0.228 Mt        0.000 Mt        0.228 Mt   100.000 % 	 ...../PrepareAccelerationStructures

		       0.023 Mt     0.162 %         1 x        0.023 Mt        0.000 Mt        0.023 Mt   100.000 % 	 ...../PrepareCache

		       0.413 Mt     2.874 %         1 x        0.413 Mt        0.000 Mt        0.413 Mt   100.000 % 	 ...../BuildCache

		       8.377 Mt    58.273 %         1 x        8.377 Mt        0.000 Mt        0.004 Mt     0.045 % 	 ...../BuildAccelerationStructures

		       8.373 Mt    99.955 %         1 x        8.373 Mt        0.000 Mt        6.378 Mt    76.174 % 	 ....../AccelerationStructureBuild

		       1.054 Mt    12.590 %         1 x        1.054 Mt        0.000 Mt        1.054 Mt   100.000 % 	 ......./BuildBottomLevelAS

		       0.941 Mt    11.237 %         1 x        0.941 Mt        0.000 Mt        0.941 Mt   100.000 % 	 ......./BuildTopLevelAS

		       1.524 Mt    10.599 %         1 x        1.524 Mt        0.000 Mt        1.524 Mt   100.000 % 	 ...../BuildShaderTables

		      10.627 Mt     9.829 %         1 x       10.627 Mt        0.000 Mt       10.627 Mt   100.000 % 	 ..../GpuSidePrep

		       4.955 Mt     0.017 %         1 x        4.955 Mt        0.000 Mt        4.955 Mt   100.000 % 	 ../DeferredRLInitialization

		      49.789 Mt     0.168 %         1 x       49.789 Mt        0.000 Mt        2.130 Mt     4.278 % 	 ../RayTracingRLInitialization

		      47.659 Mt    95.722 %         1 x       47.659 Mt        0.000 Mt       47.659 Mt   100.000 % 	 .../PipelineBuild

		       4.955 Mt     0.017 %         1 x        4.955 Mt        0.000 Mt        4.955 Mt   100.000 % 	 ../ResolveRLInitialization

		   29074.611 Mt    98.193 %     10621 x        2.737 Mt       23.388 Mt      358.007 Mt     1.231 % 	 ../MainLoop

		    1626.613 Mt     5.595 %      1745 x        0.932 Mt        0.717 Mt     1625.180 Mt    99.912 % 	 .../Update

		       1.433 Mt     0.088 %         1 x        1.433 Mt        0.000 Mt        1.433 Mt   100.000 % 	 ..../GuiModelApply

		   27089.991 Mt    93.174 %      5186 x        5.224 Mt        4.841 Mt    17511.329 Mt    64.641 % 	 .../Render

		    9578.662 Mt    35.359 %      5186 x        1.847 Mt        1.823 Mt     9531.043 Mt    99.503 % 	 ..../RayTracing

		      27.994 Mt     0.292 %      5186 x        0.005 Mt        0.005 Mt       27.994 Mt   100.000 % 	 ...../DeferredRLPrep

		      19.625 Mt     0.205 %      5186 x        0.004 Mt        0.004 Mt       19.625 Mt   100.000 % 	 ...../RayTracingRLPrep

	WindowThread:

		   29605.493 Mt   100.000 %         1 x    29605.494 Mt    29605.493 Mt    29605.493 Mt   100.000 % 	 /

	GpuThread:

		   10562.257 Mt   100.000 %      5186 x        2.037 Mt        1.538 Mt      147.759 Mt     1.399 % 	 /

		       0.161 Mt     0.002 %         1 x        0.161 Mt        0.000 Mt        0.005 Mt     3.146 % 	 ./ScenePrep

		       0.156 Mt    96.854 %         1 x        0.156 Mt        0.000 Mt        0.002 Mt     1.172 % 	 ../AccelerationStructureBuild

		       0.093 Mt    59.725 %         1 x        0.093 Mt        0.000 Mt        0.093 Mt   100.000 % 	 .../BuildBottomLevelASGpu

		       0.061 Mt    39.104 %         1 x        0.061 Mt        0.000 Mt        0.061 Mt   100.000 % 	 .../BuildTopLevelASGpu

		   10356.583 Mt    98.053 %      5186 x        1.997 Mt        1.535 Mt        5.853 Mt     0.057 % 	 ./RayTracingGpu

		    1474.266 Mt    14.235 %      5186 x        0.284 Mt        0.096 Mt     1474.266 Mt   100.000 % 	 ../DeferredRLGpu

		    4021.784 Mt    38.833 %      5186 x        0.776 Mt        0.309 Mt     4021.784 Mt   100.000 % 	 ../RayTracingRLGpu

		    4854.680 Mt    46.875 %      5186 x        0.936 Mt        1.128 Mt     4854.680 Mt   100.000 % 	 ../ResolveRLGpu

		      57.754 Mt     0.547 %      5186 x        0.011 Mt        0.002 Mt       57.754 Mt   100.000 % 	 ./Present


	============================


