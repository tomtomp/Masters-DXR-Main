Configuration: 
Command line parameters  = E:\programming\FIT-DIP\Masters-DXR-Main-git\build\Measurement\x64\Debug\Measurement.exe --scene Sponza/Sponza.gltf --profile-automation --profile-camera-track Sponza/SponzaPreset.trk --rt-mode --width 2560 --height 1440 --profile-output PresetSponza1440RtHardShadows --quality-preset HardShadows 
Window width             = 2560
Window height            = 1421
Window title             = Direct3D App
Back-buffer count        = 3
Fill Triangles           = enabled
Triangles Font Clock-Wise= enabled
Fast Build AS            = disabled
Target FPS               = 0
Target UPS               = 60
Tearing                  = enabled
VSync                    = disabled
GUI rendering            = disabled
Debug layer              = enabled
GPU validation           = disabled
GPU profiling            = enabled
Render mode              = Ray Tracing
Loaded scene file        = Sponza/Sponza.gltf
Using testing scene      = disabled
Duplication              = 1
Duplication in cube      = disabled
Duplication offset       = 10
BLAS duplication         = disabled
Rays per pixel           = 2
Quality preset           = HardShadows
Rasterization Quality    : 
  AO                     = disabled
  AO kernel size         = 1
  Shadows                = enabled
  Shadow map resolution  = 2048
  Shadow PCF kernel size = 0
  Reflections            = disabled
Ray Tracing Quality      : 
  AO                     = disabled
  AO rays                = 0
  AO kernel size         = 4
  Shadows                = enabled
  Shadow rays            = 1
  Shadow kernel size     = 4
  Reflections            = disabled

ProfilingAggregator: 
Render	,	7.7078	,	8.238	,	8.7947	,	7.548	,	7.1489	,	7.8855	,	7.3395	,	8.1957	,	7.3315	,	7.1034	,	7.012	,	7.1732	,	7.1977	,	8.082	,	8.027	,	7.4672	,	8.2435	,	7.8618	,	7.6418	,	8.0464	,	8.1319	,	7.5029	,	8.2534
Update	,	0.4928	,	0.7687	,	0.6758	,	0.6759	,	0.7936	,	0.8126	,	0.5782	,	0.6205	,	0.8722	,	0.5213	,	0.8048	,	0.6993	,	0.8383	,	0.7785	,	0.572	,	0.6796	,	0.6434	,	0.5007	,	0.5589	,	0.8525	,	0.5674	,	0.5503	,	0.6202
RayTracing	,	2.5457	,	3.0218	,	3.5886	,	2.9637	,	2.7536	,	3.0063	,	2.4941	,	3.0052	,	2.482	,	2.4621	,	2.51	,	2.5076	,	2.5096	,	2.9728	,	3.0667	,	2.9809	,	3.0423	,	2.9624	,	3.0439	,	2.9732	,	2.9731	,	2.6129	,	2.9888
RayTracingGpu	,	3.91853	,	3.9159	,	3.71158	,	3.22765	,	3.23152	,	3.44854	,	3.72608	,	3.69424	,	3.50266	,	3.30947	,	3.09728	,	3.36762	,	3.51891	,	3.77632	,	3.66358	,	3.17053	,	3.89187	,	3.45373	,	3.18115	,	3.76147	,	3.86259	,	3.84758	,	3.8528
DeferredRLGpu	,	1.46694	,	1.42384	,	1.25542	,	0.76464	,	0.702592	,	0.876608	,	1.12314	,	1.16358	,	0.997024	,	0.792256	,	0.68032	,	0.903872	,	1.07344	,	1.20938	,	1.24269	,	0.874816	,	1.18426	,	1.02941	,	0.874656	,	1.2991	,	1.37341	,	1.36358	,	1.36288
RayTracingRLGpu	,	0.747328	,	0.79456	,	0.74272	,	0.742656	,	0.81328	,	0.853728	,	0.71792	,	0.672192	,	0.643424	,	0.642752	,	0.69904	,	0.751872	,	0.737056	,	0.84976	,	0.703776	,	0.591712	,	0.90832	,	0.707712	,	0.589696	,	0.750304	,	0.774016	,	0.77104	,	0.773312
ResolveRLGpu	,	1.70336	,	1.69648	,	1.71107	,	1.71926	,	1.71462	,	1.71718	,	1.88394	,	1.85757	,	1.86131	,	1.87354	,	1.71715	,	1.7111	,	1.70739	,	1.71622	,	1.71629	,	1.70294	,	1.79837	,	1.7153	,	1.71571	,	1.71117	,	1.71408	,	1.71206	,	1.71546
Index	,	0	,	1	,	2	,	3	,	4	,	5	,	6	,	7	,	8	,	9	,	10	,	11	,	12	,	13	,	14	,	15	,	16	,	17	,	18	,	19	,	20	,	21	,	22

ProfilingBuildAggregator: 
BuildBottomLevelAS	,	4.3052
BuildBottomLevelASGpu	,	16.0044
BuildTopLevelAS	,	3.1335
BuildTopLevelASGpu	,	0.62928
Duplication	,	1
Triangles	,	262267
Meshes	,	103
BottomLevels	,	1
TopLevelSize	,	64
BottomLevelsSize	,	14623872
Index	,	0

FpsAggregator: 
FPS	,	102	,	116	,	120	,	123	,	131	,	126	,	128	,	121	,	125	,	130	,	132	,	131	,	126	,	121	,	120	,	130	,	127	,	123	,	128	,	130	,	118	,	119	,	119
UPS	,	59	,	60	,	61	,	60	,	60	,	60	,	61	,	60	,	60	,	60	,	60	,	60	,	60	,	61	,	60	,	60	,	60	,	60	,	61	,	60	,	60	,	60	,	61
FrameTime	,	9.84129	,	8.62549	,	8.38162	,	8.14485	,	7.63729	,	7.98486	,	7.8676	,	8.27888	,	8.00806	,	7.69762	,	7.60324	,	7.65741	,	7.97165	,	8.28378	,	8.34952	,	7.70508	,	7.9197	,	8.14919	,	7.84493	,	7.73166	,	8.50319	,	8.4147	,	8.47171
GigaRays/s	,	0.739285	,	0.84349	,	0.868033	,	0.893267	,	0.952631	,	0.911164	,	0.924745	,	0.878805	,	0.908524	,	0.945165	,	0.956897	,	0.950128	,	0.912675	,	0.878285	,	0.871369	,	0.944249	,	0.918661	,	0.892791	,	0.927416	,	0.941004	,	0.855622	,	0.86462	,	0.858801
Index	,	0	,	1	,	2	,	3	,	4	,	5	,	6	,	7	,	8	,	9	,	10	,	11	,	12	,	13	,	14	,	15	,	16	,	17	,	18	,	19	,	20	,	21	,	22


AccelerationStructure: 
Ray tracing acceleration structure building statistics: 
	Bottom Level Acceleration Structure: 
		Maximum [B]: 14623872
		Minimum [B]: 14623872
		Currently Allocated [B]: 14623872
		Currently Created [num]: 1
		Current Average [B]: 14623872
		Maximum Triangles [num]: 262267
		Minimum Triangles [num]: 262267
		Total Triangles [num]: 262267
		Maximum Meshes [num]: 103
		Minimum Meshes [num]: 103
		Total Meshes [num]: 103
	Top Level Acceleration Structure: 
		Maximum [B]: 64
		Minimum [B]: 64
		Currently Allocated [B]: 64
		Total Allocated [B]: 64
		Total Created [num]: 1
		Average Allocated [B]: 64
		Maximum Instances [num]: 1
		Minimum Instances [num]: 1
		Total Instances [num]: 1
	Scratch Buffer: 
		Maximum [B]: 11660800
		Minimum [B]: 11660800
		Currently Allocated [B]: 11660800
		Total Allocated [B]: 11660800
		Total Created [num]: 1
		Average Allocated [B]: 11660800
ProfilingStack: 
[PROF] Profiling statistics: 
	Scopes entered : 34653
	Scopes exited : 34652
	Overhead per scope [ticks] : 1045.3
	Threads entered : 3
	Threads exited : 0
	============================
		         Sum[Mt]   Parent[%]  Samples[num]       Avg[Mt]   Last/Curr[Mt]        Self[Mt]     Self[%] 	 Name

	main:

		   32718.575 Mt   100.000 %         1 x    32718.576 Mt    32718.574 Mt        0.742 Mt     0.002 % 	 /

		   32717.878 Mt    99.998 %         1 x    32717.880 Mt    32717.877 Mt      364.773 Mt     1.115 % 	 ./Main application loop

		       2.923 Mt     0.009 %         1 x        2.923 Mt        0.000 Mt        2.923 Mt   100.000 % 	 ../TexturedRLInitialization

		       2.644 Mt     0.008 %         1 x        2.644 Mt        0.000 Mt        2.644 Mt   100.000 % 	 ../ShadowMapRLInitialization

		       3.941 Mt     0.012 %         1 x        3.941 Mt        0.000 Mt        3.941 Mt   100.000 % 	 ../AmbientOcclusionRLInitialization

		       5.129 Mt     0.016 %         1 x        5.129 Mt        0.000 Mt        5.129 Mt   100.000 % 	 ../RasterResolveRLInitialization

		    9190.595 Mt    28.090 %         1 x     9190.595 Mt        0.000 Mt       12.159 Mt     0.132 % 	 ../Initialization

		    9178.436 Mt    99.868 %         1 x     9178.436 Mt        0.000 Mt        3.582 Mt     0.039 % 	 .../ScenePrep

		    9064.047 Mt    98.754 %         1 x     9064.047 Mt        0.000 Mt      328.412 Mt     3.623 % 	 ..../SceneLoad

		    8243.678 Mt    90.949 %         1 x     8243.678 Mt        0.000 Mt     1112.561 Mt    13.496 % 	 ...../glTF-LoadScene

		    7131.117 Mt    86.504 %        69 x      103.350 Mt        0.000 Mt     7131.117 Mt   100.000 % 	 ....../glTF-LoadImageData

		     380.540 Mt     4.198 %         1 x      380.540 Mt        0.000 Mt      380.540 Mt   100.000 % 	 ...../glTF-CreateScene

		     111.417 Mt     1.229 %         1 x      111.417 Mt        0.000 Mt      111.417 Mt   100.000 % 	 ...../SceneDesc-CreateScene

		       2.469 Mt     0.027 %         1 x        2.469 Mt        0.000 Mt        0.005 Mt     0.211 % 	 ..../ShadowMapRLPrep

		       2.464 Mt    99.789 %         1 x        2.464 Mt        0.000 Mt        1.776 Mt    72.104 % 	 ...../PrepareForRendering

		       0.687 Mt    27.896 %         1 x        0.687 Mt        0.000 Mt        0.687 Mt   100.000 % 	 ....../PrepareDrawBundle

		       5.991 Mt     0.065 %         1 x        5.991 Mt        0.000 Mt        0.005 Mt     0.083 % 	 ..../TexturedRLPrep

		       5.986 Mt    99.917 %         1 x        5.986 Mt        0.000 Mt        1.113 Mt    18.585 % 	 ...../PrepareForRendering

		       2.655 Mt    44.344 %         1 x        2.655 Mt        0.000 Mt        2.655 Mt   100.000 % 	 ....../PrepareMaterials

		       2.219 Mt    37.071 %         1 x        2.219 Mt        0.000 Mt        2.219 Mt   100.000 % 	 ....../PrepareDrawBundle

		       7.764 Mt     0.085 %         1 x        7.764 Mt        0.000 Mt        0.005 Mt     0.062 % 	 ..../DeferredRLPrep

		       7.759 Mt    99.938 %         1 x        7.759 Mt        0.000 Mt        2.986 Mt    38.488 % 	 ...../PrepareForRendering

		       0.021 Mt     0.269 %         1 x        0.021 Mt        0.000 Mt        0.021 Mt   100.000 % 	 ....../PrepareMaterials

		       2.187 Mt    28.190 %         1 x        2.187 Mt        0.000 Mt        2.187 Mt   100.000 % 	 ....../BuildMaterialCache

		       2.564 Mt    33.052 %         1 x        2.564 Mt        0.000 Mt        2.564 Mt   100.000 % 	 ....../PrepareDrawBundle

		      29.431 Mt     0.321 %         1 x       29.431 Mt        0.000 Mt        6.521 Mt    22.157 % 	 ..../RayTracingRLPrep

		       0.645 Mt     2.191 %         1 x        0.645 Mt        0.000 Mt        0.645 Mt   100.000 % 	 ...../PrepareAccelerationStructures

		       0.026 Mt     0.087 %         1 x        0.026 Mt        0.000 Mt        0.026 Mt   100.000 % 	 ...../PrepareCache

		       5.356 Mt    18.199 %         1 x        5.356 Mt        0.000 Mt        5.356 Mt   100.000 % 	 ...../BuildCache

		      12.461 Mt    42.340 %         1 x       12.461 Mt        0.000 Mt        0.004 Mt     0.032 % 	 ...../BuildAccelerationStructures

		      12.457 Mt    99.968 %         1 x       12.457 Mt        0.000 Mt        5.018 Mt    40.286 % 	 ....../AccelerationStructureBuild

		       4.305 Mt    34.560 %         1 x        4.305 Mt        0.000 Mt        4.305 Mt   100.000 % 	 ......./BuildBottomLevelAS

		       3.134 Mt    25.154 %         1 x        3.134 Mt        0.000 Mt        3.134 Mt   100.000 % 	 ......./BuildTopLevelAS

		       4.423 Mt    15.026 %         1 x        4.423 Mt        0.000 Mt        4.423 Mt   100.000 % 	 ...../BuildShaderTables

		      65.152 Mt     0.710 %         1 x       65.152 Mt        0.000 Mt       65.152 Mt   100.000 % 	 ..../GpuSidePrep

		       4.955 Mt     0.015 %         1 x        4.955 Mt        0.000 Mt        4.955 Mt   100.000 % 	 ../DeferredRLInitialization

		      50.092 Mt     0.153 %         1 x       50.092 Mt        0.000 Mt        2.175 Mt     4.343 % 	 ../RayTracingRLInitialization

		      47.917 Mt    95.657 %         1 x       47.917 Mt        0.000 Mt       47.917 Mt   100.000 % 	 .../PipelineBuild

		       5.072 Mt     0.016 %         1 x        5.072 Mt        0.000 Mt        5.072 Mt   100.000 % 	 ../ResolveRLInitialization

		   23087.753 Mt    70.566 %      7524 x        3.069 Mt       10.436 Mt      157.968 Mt     0.684 % 	 ../MainLoop

		    1088.932 Mt     4.716 %      1386 x        0.786 Mt        0.586 Mt     1087.729 Mt    99.889 % 	 .../Update

		       1.203 Mt     0.111 %         1 x        1.203 Mt        0.000 Mt        1.203 Mt   100.000 % 	 ..../GuiModelApply

		   21840.852 Mt    94.599 %      2848 x        7.669 Mt        9.127 Mt    13962.375 Mt    63.928 % 	 .../Render

		    7878.477 Mt    36.072 %      2848 x        2.766 Mt        3.703 Mt     7857.470 Mt    99.733 % 	 ..../RayTracing

		      11.772 Mt     0.149 %      2848 x        0.004 Mt        0.004 Mt       11.772 Mt   100.000 % 	 ...../DeferredRLPrep

		       9.236 Mt     0.117 %      2848 x        0.003 Mt        0.005 Mt        9.236 Mt   100.000 % 	 ...../RayTracingRLPrep

	WindowThread:

		   32713.754 Mt   100.000 %         1 x    32713.754 Mt    32713.754 Mt    32713.754 Mt   100.000 % 	 /

	GpuThread:

		   10413.445 Mt   100.000 %      2848 x        3.656 Mt        3.877 Mt      201.929 Mt     1.939 % 	 /

		      16.687 Mt     0.160 %         1 x       16.687 Mt        0.000 Mt        0.051 Mt     0.305 % 	 ./ScenePrep

		      16.636 Mt    99.695 %         1 x       16.636 Mt        0.000 Mt        0.003 Mt     0.017 % 	 ../AccelerationStructureBuild

		      16.004 Mt    96.201 %         1 x       16.004 Mt        0.000 Mt       16.004 Mt   100.000 % 	 .../BuildBottomLevelASGpu

		       0.629 Mt     3.783 %         1 x        0.629 Mt        0.000 Mt        0.629 Mt   100.000 % 	 .../BuildTopLevelASGpu

		   10134.253 Mt    97.319 %      2848 x        3.558 Mt        3.875 Mt        3.340 Mt     0.033 % 	 ./RayTracingGpu

		    3063.155 Mt    30.226 %      2848 x        1.076 Mt        1.385 Mt     3063.155 Mt   100.000 % 	 ../DeferredRLGpu

		    2121.504 Mt    20.934 %      2848 x        0.745 Mt        0.772 Mt     2121.504 Mt   100.000 % 	 ../RayTracingRLGpu

		    4946.254 Mt    48.807 %      2848 x        1.737 Mt        1.716 Mt     4946.254 Mt   100.000 % 	 ../ResolveRLGpu

		      60.577 Mt     0.582 %      2848 x        0.021 Mt        0.001 Mt       60.577 Mt   100.000 % 	 ./Present


	============================


