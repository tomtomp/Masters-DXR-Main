Configuration: 
Command line parameters  = E:\programming\FIT-DIP\Masters-DXR-Main-git\build\Measurement\x64\Debug\Measurement.exe --scene Sponza/Sponza.gltf --profile-automation --profile-camera-track Sponza/SponzaPreset.trk --rt-mode --width 2560 --height 1440 --profile-output PresetSponza1440RtHigh --quality-preset High 
Window width             = 2560
Window height            = 1421
Window title             = Direct3D App
Back-buffer count        = 3
Fill Triangles           = enabled
Triangles Font Clock-Wise= enabled
Fast Build AS            = disabled
Target FPS               = 0
Target UPS               = 60
Tearing                  = enabled
VSync                    = disabled
GUI rendering            = disabled
Debug layer              = enabled
GPU validation           = disabled
GPU profiling            = enabled
Render mode              = Ray Tracing
Loaded scene file        = Sponza/Sponza.gltf
Using testing scene      = disabled
Duplication              = 1
Duplication in cube      = disabled
Duplication offset       = 10
BLAS duplication         = disabled
Rays per pixel           = 14
Quality preset           = High
Rasterization Quality    : 
  AO                     = enabled
  AO kernel size         = 4
  Shadows                = enabled
  Shadow map resolution  = 4096
  Shadow PCF kernel size = 4
  Reflections            = enabled
Ray Tracing Quality      : 
  AO                     = enabled
  AO rays                = 4
  AO kernel size         = 4
  Shadows                = enabled
  Shadow rays            = 4
  Shadow kernel size     = 4
  Reflections            = enabled

ProfilingAggregator: 
Render	,	14.5536	,	14.2431	,	14.2184	,	13.5911	,	13.7643	,	14.541	,	13.9658	,	14.894	,	14.4174	,	14.6422	,	14.6067	,	15.6232	,	14.9923	,	15.9317	,	13.9772	,	13.1789	,	16.3752	,	15.7552	,	12.8885	,	13.4212	,	13.2827	,	13.5275	,	13.0455
Update	,	0.5813	,	0.6359	,	0.6858	,	0.5857	,	0.9266	,	0.5901	,	0.9582	,	0.5733	,	0.5721	,	0.6698	,	0.8127	,	0.7084	,	0.5657	,	0.6012	,	0.7655	,	0.9251	,	0.5658	,	0.9124	,	1.0499	,	0.8739	,	0.9702	,	1.0365	,	0.8521
RayTracing	,	3.0431	,	3.1498	,	3.0572	,	3.0831	,	3.0524	,	3.0408	,	3.1013	,	3.0497	,	3.1112	,	3.026	,	3.014	,	3.1625	,	3.1084	,	3.1135	,	3.0596	,	3.0824	,	3.0518	,	3.0758	,	3.0106	,	2.9873	,	3.0354	,	3.0404	,	3.0459
RayTracingGpu	,	9.70298	,	9.5856	,	9.62234	,	8.91658	,	9.03686	,	9.91677	,	9.41562	,	10.2365	,	9.87971	,	9.97549	,	10.0014	,	10.6999	,	10.3351	,	11.1618	,	9.30061	,	8.50656	,	12.0457	,	11.2594	,	8.48512	,	8.93005	,	8.6409	,	8.78912	,	8.57805
DeferredRLGpu	,	1.46026	,	1.41914	,	1.23754	,	0.776256	,	0.707008	,	0.880832	,	1.14774	,	1.28848	,	1.01235	,	0.782816	,	0.687328	,	0.927008	,	1.11936	,	1.21712	,	1.22621	,	0.912832	,	1.10522	,	1.19926	,	0.849664	,	1.46333	,	1.38739	,	1.38525	,	1.37059
RayTracingRLGpu	,	5.64515	,	5.65562	,	5.72554	,	5.6273	,	5.64314	,	6.52192	,	5.75763	,	6.41789	,	6.37213	,	6.57091	,	6.80342	,	7.10214	,	6.73248	,	7.29619	,	5.57357	,	5.09456	,	8.42646	,	7.5425	,	5.00938	,	4.80445	,	4.73914	,	4.73248	,	4.70835
ResolveRLGpu	,	2.59648	,	2.50989	,	2.65805	,	2.5119	,	2.6855	,	2.5129	,	2.50877	,	2.52886	,	2.49424	,	2.62064	,	2.5095	,	2.66979	,	2.48051	,	2.64742	,	2.49821	,	2.4975	,	2.5129	,	2.51667	,	2.62442	,	2.66144	,	2.51309	,	2.6703	,	2.49798
Index	,	0	,	1	,	2	,	3	,	4	,	5	,	6	,	7	,	8	,	9	,	10	,	11	,	12	,	13	,	14	,	15	,	16	,	17	,	18	,	19	,	20	,	21	,	22

ProfilingBuildAggregator: 
BuildBottomLevelAS	,	4.4598
BuildBottomLevelASGpu	,	12.9212
BuildTopLevelAS	,	2.534
BuildTopLevelASGpu	,	0.08944
Duplication	,	1
Triangles	,	262267
Meshes	,	103
BottomLevels	,	1
TopLevelSize	,	64
BottomLevelsSize	,	14623872
Index	,	0

FpsAggregator: 
FPS	,	56	,	66	,	68	,	69	,	70	,	67	,	68	,	66	,	64	,	65	,	65	,	64	,	64	,	61	,	64	,	74	,	62	,	59	,	69	,	75	,	71	,	71	,	72
UPS	,	59	,	61	,	61	,	60	,	61	,	60	,	60	,	61	,	61	,	60	,	60	,	61	,	61	,	60	,	61	,	61	,	60	,	61	,	61	,	60	,	61	,	60	,	61
FrameTime	,	18.0597	,	15.3502	,	14.8625	,	14.6018	,	14.3663	,	15.1399	,	14.7062	,	15.3603	,	15.731	,	15.4301	,	15.5486	,	15.8324	,	15.757	,	16.5341	,	15.7758	,	13.6943	,	16.2248	,	17.1431	,	14.6923	,	13.5048	,	14.2764	,	14.1644	,	14.0727
GigaRays/s	,	2.82001	,	3.31779	,	3.42665	,	3.48782	,	3.545	,	3.36387	,	3.46308	,	3.3156	,	3.23748	,	3.30061	,	3.27545	,	3.21674	,	3.23212	,	3.08022	,	3.22827	,	3.71895	,	3.13894	,	2.9708	,	3.46635	,	3.77114	,	3.56732	,	3.59555	,	3.61897
Index	,	0	,	1	,	2	,	3	,	4	,	5	,	6	,	7	,	8	,	9	,	10	,	11	,	12	,	13	,	14	,	15	,	16	,	17	,	18	,	19	,	20	,	21	,	22


AccelerationStructure: 
Ray tracing acceleration structure building statistics: 
	Bottom Level Acceleration Structure: 
		Maximum [B]: 14623872
		Minimum [B]: 14623872
		Currently Allocated [B]: 14623872
		Currently Created [num]: 1
		Current Average [B]: 14623872
		Maximum Triangles [num]: 262267
		Minimum Triangles [num]: 262267
		Total Triangles [num]: 262267
		Maximum Meshes [num]: 103
		Minimum Meshes [num]: 103
		Total Meshes [num]: 103
	Top Level Acceleration Structure: 
		Maximum [B]: 64
		Minimum [B]: 64
		Currently Allocated [B]: 64
		Total Allocated [B]: 64
		Total Created [num]: 1
		Average Allocated [B]: 64
		Maximum Instances [num]: 1
		Minimum Instances [num]: 1
		Total Instances [num]: 1
	Scratch Buffer: 
		Maximum [B]: 11660800
		Minimum [B]: 11660800
		Currently Allocated [B]: 11660800
		Total Allocated [B]: 11660800
		Total Created [num]: 1
		Average Allocated [B]: 11660800
ProfilingStack: 
[PROF] Profiling statistics: 
	Scopes entered : 21617
	Scopes exited : 21616
	Overhead per scope [ticks] : 1035
	Threads entered : 3
	Threads exited : 0
	============================
		         Sum[Mt]   Parent[%]  Samples[num]       Avg[Mt]   Last/Curr[Mt]        Self[Mt]     Self[%] 	 Name

	main:

		   32837.851 Mt   100.000 %         1 x    32837.853 Mt    32837.850 Mt        0.808 Mt     0.002 % 	 /

		   32837.098 Mt    99.998 %         1 x    32837.101 Mt    32837.098 Mt      341.563 Mt     1.040 % 	 ./Main application loop

		       4.489 Mt     0.014 %         1 x        4.489 Mt        0.000 Mt        4.489 Mt   100.000 % 	 ../TexturedRLInitialization

		       3.365 Mt     0.010 %         1 x        3.365 Mt        0.000 Mt        3.365 Mt   100.000 % 	 ../ShadowMapRLInitialization

		       5.234 Mt     0.016 %         1 x        5.234 Mt        0.000 Mt        5.234 Mt   100.000 % 	 ../AmbientOcclusionRLInitialization

		       6.183 Mt     0.019 %         1 x        6.183 Mt        0.000 Mt        6.183 Mt   100.000 % 	 ../RasterResolveRLInitialization

		    9177.416 Mt    27.948 %         1 x     9177.416 Mt        0.000 Mt       11.701 Mt     0.127 % 	 ../Initialization

		    9165.715 Mt    99.873 %         1 x     9165.715 Mt        0.000 Mt        3.464 Mt     0.038 % 	 .../ScenePrep

		    9053.174 Mt    98.772 %         1 x     9053.174 Mt        0.000 Mt      462.280 Mt     5.106 % 	 ..../SceneLoad

		    8176.127 Mt    90.312 %         1 x     8176.127 Mt        0.000 Mt     1115.735 Mt    13.646 % 	 ...../glTF-LoadScene

		    7060.392 Mt    86.354 %        69 x      102.325 Mt        0.000 Mt     7060.392 Mt   100.000 % 	 ....../glTF-LoadImageData

		     304.989 Mt     3.369 %         1 x      304.989 Mt        0.000 Mt      304.989 Mt   100.000 % 	 ...../glTF-CreateScene

		     109.778 Mt     1.213 %         1 x      109.778 Mt        0.000 Mt      109.778 Mt   100.000 % 	 ...../SceneDesc-CreateScene

		       2.550 Mt     0.028 %         1 x        2.550 Mt        0.000 Mt        0.005 Mt     0.212 % 	 ..../ShadowMapRLPrep

		       2.545 Mt    99.788 %         1 x        2.545 Mt        0.000 Mt        1.831 Mt    71.956 % 	 ...../PrepareForRendering

		       0.714 Mt    28.044 %         1 x        0.714 Mt        0.000 Mt        0.714 Mt   100.000 % 	 ....../PrepareDrawBundle

		       6.096 Mt     0.067 %         1 x        6.096 Mt        0.000 Mt        0.007 Mt     0.113 % 	 ..../TexturedRLPrep

		       6.089 Mt    99.887 %         1 x        6.089 Mt        0.000 Mt        1.200 Mt    19.706 % 	 ...../PrepareForRendering

		       2.579 Mt    42.360 %         1 x        2.579 Mt        0.000 Mt        2.579 Mt   100.000 % 	 ....../PrepareMaterials

		       2.310 Mt    37.934 %         1 x        2.310 Mt        0.000 Mt        2.310 Mt   100.000 % 	 ....../PrepareDrawBundle

		       9.556 Mt     0.104 %         1 x        9.556 Mt        0.000 Mt        0.015 Mt     0.157 % 	 ..../DeferredRLPrep

		       9.540 Mt    99.843 %         1 x        9.540 Mt        0.000 Mt        4.662 Mt    48.860 % 	 ...../PrepareForRendering

		       0.068 Mt     0.708 %         1 x        0.068 Mt        0.000 Mt        0.068 Mt   100.000 % 	 ....../PrepareMaterials

		       2.120 Mt    22.224 %         1 x        2.120 Mt        0.000 Mt        2.120 Mt   100.000 % 	 ....../BuildMaterialCache

		       2.691 Mt    28.208 %         1 x        2.691 Mt        0.000 Mt        2.691 Mt   100.000 % 	 ....../PrepareDrawBundle

		      33.069 Mt     0.361 %         1 x       33.069 Mt        0.000 Mt        7.081 Mt    21.412 % 	 ..../RayTracingRLPrep

		       0.612 Mt     1.850 %         1 x        0.612 Mt        0.000 Mt        0.612 Mt   100.000 % 	 ...../PrepareAccelerationStructures

		       0.025 Mt     0.076 %         1 x        0.025 Mt        0.000 Mt        0.025 Mt   100.000 % 	 ...../PrepareCache

		       5.323 Mt    16.097 %         1 x        5.323 Mt        0.000 Mt        5.323 Mt   100.000 % 	 ...../BuildCache

		      12.119 Mt    36.648 %         1 x       12.119 Mt        0.000 Mt        0.005 Mt     0.039 % 	 ...../BuildAccelerationStructures

		      12.114 Mt    99.961 %         1 x       12.114 Mt        0.000 Mt        5.120 Mt    42.268 % 	 ....../AccelerationStructureBuild

		       4.460 Mt    36.814 %         1 x        4.460 Mt        0.000 Mt        4.460 Mt   100.000 % 	 ......./BuildBottomLevelAS

		       2.534 Mt    20.917 %         1 x        2.534 Mt        0.000 Mt        2.534 Mt   100.000 % 	 ......./BuildTopLevelAS

		       7.909 Mt    23.916 %         1 x        7.909 Mt        0.000 Mt        7.909 Mt   100.000 % 	 ...../BuildShaderTables

		      57.807 Mt     0.631 %         1 x       57.807 Mt        0.000 Mt       57.807 Mt   100.000 % 	 ..../GpuSidePrep

		       5.451 Mt     0.017 %         1 x        5.451 Mt        0.000 Mt        5.451 Mt   100.000 % 	 ../DeferredRLInitialization

		      50.474 Mt     0.154 %         1 x       50.474 Mt        0.000 Mt        2.083 Mt     4.127 % 	 ../RayTracingRLInitialization

		      48.390 Mt    95.873 %         1 x       48.390 Mt        0.000 Mt       48.390 Mt   100.000 % 	 .../PipelineBuild

		       4.997 Mt     0.015 %         1 x        4.997 Mt        0.000 Mt        4.997 Mt   100.000 % 	 ../ResolveRLInitialization

		   23237.926 Mt    70.767 %      6334 x        3.669 Mt       27.217 Mt      156.431 Mt     0.673 % 	 ../MainLoop

		    1087.617 Mt     4.680 %      1393 x        0.781 Mt        0.781 Mt     1085.447 Mt    99.801 % 	 .../Update

		       2.169 Mt     0.199 %         1 x        2.169 Mt        0.000 Mt        2.169 Mt   100.000 % 	 ..../GuiModelApply

		   21993.878 Mt    94.646 %      1531 x       14.366 Mt       16.860 Mt    17291.696 Mt    78.620 % 	 .../Render

		    4702.181 Mt    21.380 %      1531 x        3.071 Mt        4.996 Mt     4691.010 Mt    99.762 % 	 ..../RayTracing

		       5.355 Mt     0.114 %      1531 x        0.003 Mt        0.005 Mt        5.355 Mt   100.000 % 	 ...../DeferredRLPrep

		       5.817 Mt     0.124 %      1531 x        0.004 Mt        0.007 Mt        5.817 Mt   100.000 % 	 ...../RayTracingRLPrep

	WindowThread:

		   32833.317 Mt   100.000 %         1 x    32833.318 Mt    32833.317 Mt    32833.317 Mt   100.000 % 	 /

	GpuThread:

		   15187.774 Mt   100.000 %      1531 x        9.920 Mt        9.396 Mt      181.802 Mt     1.197 % 	 /

		      13.062 Mt     0.086 %         1 x       13.062 Mt        0.000 Mt        0.049 Mt     0.376 % 	 ./ScenePrep

		      13.012 Mt    99.624 %         1 x       13.012 Mt        0.000 Mt        0.002 Mt     0.014 % 	 ../AccelerationStructureBuild

		      12.921 Mt    99.299 %         1 x       12.921 Mt        0.000 Mt       12.921 Mt   100.000 % 	 .../BuildBottomLevelASGpu

		       0.089 Mt     0.687 %         1 x        0.089 Mt        0.000 Mt        0.089 Mt   100.000 % 	 .../BuildTopLevelASGpu

		   14901.556 Mt    98.115 %      1531 x        9.733 Mt        9.298 Mt        1.885 Mt     0.013 % 	 ./RayTracingGpu

		    1678.017 Mt    11.261 %      1531 x        1.096 Mt        1.545 Mt     1678.017 Mt   100.000 % 	 ../DeferredRLGpu

		    9261.182 Mt    62.149 %      1531 x        6.049 Mt        5.112 Mt     9261.182 Mt   100.000 % 	 ../RayTracingRLGpu

		    3960.472 Mt    26.578 %      1531 x        2.587 Mt        2.640 Mt     3960.472 Mt   100.000 % 	 ../ResolveRLGpu

		      91.354 Mt     0.601 %      1531 x        0.060 Mt        0.098 Mt       91.354 Mt   100.000 % 	 ./Present


	============================


