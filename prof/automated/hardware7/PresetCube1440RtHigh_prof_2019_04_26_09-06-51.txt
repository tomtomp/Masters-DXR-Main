Configuration: 
Command line parameters  = E:\programming\FIT-DIP\Masters-DXR-Main-git\build\Measurement\x64\Debug\Measurement.exe --scene Cube/Cube.gltf --profile-automation --profile-camera-track Cube/Cube.trk --rt-mode --width 2560 --height 1440 --profile-output PresetCube1440RtHigh --quality-preset High 
Window width             = 2560
Window height            = 1421
Window title             = Direct3D App
Back-buffer count        = 3
Fill Triangles           = enabled
Triangles Font Clock-Wise= enabled
Fast Build AS            = disabled
Target FPS               = 0
Target UPS               = 60
Tearing                  = enabled
VSync                    = disabled
GUI rendering            = disabled
Debug layer              = enabled
GPU validation           = disabled
GPU profiling            = enabled
Render mode              = Ray Tracing
Loaded scene file        = Cube/Cube.gltf
Using testing scene      = disabled
Duplication              = 1
Duplication in cube      = disabled
Duplication offset       = 10
BLAS duplication         = disabled
Rays per pixel           = 14
Quality preset           = High
Rasterization Quality    : 
  AO                     = enabled
  AO kernel size         = 4
  Shadows                = enabled
  Shadow map resolution  = 4096
  Shadow PCF kernel size = 4
  Reflections            = enabled
Ray Tracing Quality      : 
  AO                     = enabled
  AO rays                = 4
  AO kernel size         = 4
  Shadows                = enabled
  Shadow rays            = 4
  Shadow kernel size     = 4
  Reflections            = enabled

ProfilingAggregator: 
Render	,	7.0425	,	9.1853	,	11.4477	,	9.5617	,	6.8903	,	9.6718	,	9.4441	,	8.9323	,	8.5186	,	12.6732	,	9.7217	,	9.2725	,	12.0779	,	7.7127	,	10.1245	,	9.627	,	10.6722	,	11.5995	,	12.389	,	9.2068	,	11.4016	,	9.6729	,	9.1327	,	9.1778	,	15.7436	,	5.9427	,	6.4126	,	5.3949	,	7.9854
Update	,	1.2255	,	1.153	,	0.9418	,	1.3431	,	1.1352	,	0.926	,	1.16	,	0.92	,	0.6583	,	1.5548	,	1.0163	,	1.0319	,	1.6025	,	0.7779	,	0.62	,	0.5467	,	1.2145	,	1.1472	,	1.3757	,	0.6609	,	1.3574	,	1.195	,	1.1644	,	1.2777	,	1.3746	,	0.8104	,	1.1652	,	1.1499	,	0.6096
RayTracing	,	1.9437	,	3.0092	,	4.5508	,	3.523	,	1.3792	,	3.1108	,	2.736	,	2.2964	,	1.953	,	4.9516	,	2.9783	,	2.3379	,	4.4169	,	1.5731	,	3.3347	,	2.1351	,	2.8735	,	2.9552	,	3.5927	,	1.5898	,	3.6356	,	3.0038	,	3.1626	,	3.4249	,	10.26	,	1.5768	,	1.9306	,	1.4886	,	3.2729
RayTracingGpu	,	3.47926	,	3.87837	,	3.86394	,	3.91853	,	4.06842	,	4.47475	,	4.48486	,	4.80109	,	4.97235	,	4.66566	,	4.74301	,	4.99846	,	4.57094	,	4.3696	,	4.48755	,	4.92624	,	5.85811	,	6.41782	,	6.18554	,	6.18973	,	5.53683	,	4.38486	,	3.78707	,	3.43555	,	3.41795	,	3.00982	,	2.80762	,	2.71328	,	2.64426
DeferredRLGpu	,	0.181088	,	0.241856	,	0.254176	,	0.260768	,	0.273472	,	0.333824	,	0.30976	,	0.354304	,	0.414016	,	0.367168	,	0.318336	,	0.349984	,	0.277952	,	0.251488	,	0.286176	,	0.338016	,	0.396864	,	0.467808	,	0.487744	,	0.48864	,	0.401536	,	0.28416	,	0.232096	,	0.179392	,	0.150592	,	0.11632	,	0.08832	,	0.073376	,	0.063616
RayTracingRLGpu	,	0.835104	,	1.12806	,	1.18925	,	1.24602	,	1.3911	,	1.70403	,	1.56614	,	1.99302	,	2.0904	,	1.84803	,	1.99805	,	2.18912	,	1.71066	,	1.54182	,	1.78317	,	2.14112	,	2.84944	,	3.31542	,	3.20707	,	3.21894	,	2.68749	,	1.67917	,	1.17168	,	0.901408	,	0.748416	,	0.568032	,	0.420704	,	0.345376	,	0.298432
ResolveRLGpu	,	2.46208	,	2.50749	,	2.41923	,	2.41091	,	2.40288	,	2.43574	,	2.60704	,	2.4529	,	2.46672	,	2.44918	,	2.42538	,	2.45827	,	2.58118	,	2.57517	,	2.41699	,	2.44618	,	2.61088	,	2.63338	,	2.4896	,	2.48125	,	2.44688	,	2.42035	,	2.3823	,	2.35376	,	2.51782	,	2.32426	,	2.29734	,	2.29354	,	2.28138
Index	,	0	,	1	,	2	,	3	,	4	,	5	,	6	,	7	,	8	,	9	,	10	,	11	,	12	,	13	,	14	,	15	,	16	,	17	,	18	,	19	,	20	,	21	,	22	,	23	,	24	,	25	,	26	,	27	,	28

ProfilingBuildAggregator: 
BuildBottomLevelAS	,	1.0374
BuildBottomLevelASGpu	,	0.08768
BuildTopLevelAS	,	1.0284
BuildTopLevelASGpu	,	0.060832
Duplication	,	1
Triangles	,	12
Meshes	,	1
BottomLevels	,	1
TopLevelSize	,	64
BottomLevelsSize	,	5168
Index	,	0

FpsAggregator: 
FPS	,	116	,	112	,	109	,	106	,	103	,	111	,	101	,	102	,	98	,	95	,	104	,	96	,	96	,	107	,	99	,	99	,	102	,	91	,	86	,	84	,	90	,	98	,	106	,	116	,	123	,	133	,	140	,	150	,	150
UPS	,	59	,	60	,	60	,	61	,	60	,	60	,	61	,	60	,	60	,	60	,	60	,	60	,	61	,	60	,	60	,	61	,	60	,	60	,	60	,	61	,	60	,	60	,	61	,	60	,	60	,	61	,	60	,	60	,	60
FrameTime	,	8.66512	,	8.93826	,	9.21967	,	9.50531	,	9.71556	,	9.0375	,	9.95517	,	9.83796	,	10.2217	,	10.5861	,	9.61752	,	10.4176	,	10.522	,	9.35398	,	10.1536	,	10.1922	,	9.8518	,	11.0053	,	11.6823	,	11.9738	,	11.1445	,	10.2913	,	9.45893	,	8.6553	,	8.21384	,	7.55873	,	7.14286	,	6.70018	,	6.68984
GigaRays/s	,	5.87743	,	5.69783	,	5.52391	,	5.35792	,	5.24197	,	5.63526	,	5.1158	,	5.17675	,	4.98239	,	4.81089	,	5.2954	,	4.8887	,	4.84022	,	5.4446	,	5.01584	,	4.99682	,	5.16947	,	4.62765	,	4.35945	,	4.25335	,	4.56983	,	4.94871	,	5.38419	,	5.8841	,	6.20035	,	6.73772	,	7.13001	,	7.60109	,	7.61284
Index	,	0	,	1	,	2	,	3	,	4	,	5	,	6	,	7	,	8	,	9	,	10	,	11	,	12	,	13	,	14	,	15	,	16	,	17	,	18	,	19	,	20	,	21	,	22	,	23	,	24	,	25	,	26	,	27	,	28


AccelerationStructure: 
Ray tracing acceleration structure building statistics: 
	Bottom Level Acceleration Structure: 
		Maximum [B]: 5168
		Minimum [B]: 5168
		Currently Allocated [B]: 5168
		Currently Created [num]: 1
		Current Average [B]: 5168
		Maximum Triangles [num]: 12
		Minimum Triangles [num]: 12
		Total Triangles [num]: 12
		Maximum Meshes [num]: 1
		Minimum Meshes [num]: 1
		Total Meshes [num]: 1
	Top Level Acceleration Structure: 
		Maximum [B]: 64
		Minimum [B]: 64
		Currently Allocated [B]: 64
		Total Allocated [B]: 64
		Total Created [num]: 1
		Average Allocated [B]: 64
		Maximum Instances [num]: 1
		Minimum Instances [num]: 1
		Total Instances [num]: 1
	Scratch Buffer: 
		Maximum [B]: 3584
		Minimum [B]: 3584
		Currently Allocated [B]: 3584
		Total Allocated [B]: 3584
		Total Created [num]: 1
		Average Allocated [B]: 3584
ProfilingStack: 
[PROF] Profiling statistics: 
	Scopes entered : 34705
	Scopes exited : 34704
	Overhead per scope [ticks] : 1005.6
	Threads entered : 3
	Threads exited : 0
	============================
		         Sum[Mt]   Parent[%]  Samples[num]       Avg[Mt]   Last/Curr[Mt]        Self[Mt]     Self[%] 	 Name

	main:

		   29723.344 Mt   100.000 %         1 x    29723.348 Mt    29723.342 Mt        1.142 Mt     0.004 % 	 /

		   29722.347 Mt    99.997 %         1 x    29722.354 Mt    29722.346 Mt      406.332 Mt     1.367 % 	 ./Main application loop

		       2.964 Mt     0.010 %         1 x        2.964 Mt        0.000 Mt        2.964 Mt   100.000 % 	 ../TexturedRLInitialization

		       2.554 Mt     0.009 %         1 x        2.554 Mt        0.000 Mt        2.554 Mt   100.000 % 	 ../ShadowMapRLInitialization

		       4.753 Mt     0.016 %         1 x        4.753 Mt        0.000 Mt        4.753 Mt   100.000 % 	 ../AmbientOcclusionRLInitialization

		       5.197 Mt     0.017 %         1 x        5.197 Mt        0.000 Mt        5.197 Mt   100.000 % 	 ../RasterResolveRLInitialization

		     113.694 Mt     0.383 %         1 x      113.694 Mt        0.000 Mt        3.617 Mt     3.182 % 	 ../Initialization

		     110.076 Mt    96.818 %         1 x      110.076 Mt        0.000 Mt        1.188 Mt     1.079 % 	 .../ScenePrep

		      77.434 Mt    70.345 %         1 x       77.434 Mt        0.000 Mt       12.461 Mt    16.093 % 	 ..../SceneLoad

		      57.439 Mt    74.179 %         1 x       57.439 Mt        0.000 Mt       12.585 Mt    21.910 % 	 ...../glTF-LoadScene

		      44.855 Mt    78.090 %         2 x       22.427 Mt        0.000 Mt       44.855 Mt   100.000 % 	 ....../glTF-LoadImageData

		       4.154 Mt     5.365 %         1 x        4.154 Mt        0.000 Mt        4.154 Mt   100.000 % 	 ...../glTF-CreateScene

		       3.379 Mt     4.363 %         1 x        3.379 Mt        0.000 Mt        3.379 Mt   100.000 % 	 ...../SceneDesc-CreateScene

		       0.500 Mt     0.454 %         1 x        0.500 Mt        0.000 Mt        0.005 Mt     0.900 % 	 ..../ShadowMapRLPrep

		       0.496 Mt    99.100 %         1 x        0.496 Mt        0.000 Mt        0.441 Mt    88.983 % 	 ...../PrepareForRendering

		       0.055 Mt    11.017 %         1 x        0.055 Mt        0.000 Mt        0.055 Mt   100.000 % 	 ....../PrepareDrawBundle

		       3.313 Mt     3.010 %         1 x        3.313 Mt        0.000 Mt        0.004 Mt     0.133 % 	 ..../TexturedRLPrep

		       3.309 Mt    99.867 %         1 x        3.309 Mt        0.000 Mt        0.393 Mt    11.873 % 	 ...../PrepareForRendering

		       2.648 Mt    80.013 %         1 x        2.648 Mt        0.000 Mt        2.648 Mt   100.000 % 	 ....../PrepareMaterials

		       0.269 Mt     8.114 %         1 x        0.269 Mt        0.000 Mt        0.269 Mt   100.000 % 	 ....../PrepareDrawBundle

		       2.394 Mt     2.175 %         1 x        2.394 Mt        0.000 Mt        0.005 Mt     0.188 % 	 ..../DeferredRLPrep

		       2.390 Mt    99.812 %         1 x        2.390 Mt        0.000 Mt        1.643 Mt    68.774 % 	 ...../PrepareForRendering

		       0.016 Mt     0.686 %         1 x        0.016 Mt        0.000 Mt        0.016 Mt   100.000 % 	 ....../PrepareMaterials

		       0.458 Mt    19.149 %         1 x        0.458 Mt        0.000 Mt        0.458 Mt   100.000 % 	 ....../BuildMaterialCache

		       0.272 Mt    11.391 %         1 x        0.272 Mt        0.000 Mt        0.272 Mt   100.000 % 	 ....../PrepareDrawBundle

		      14.971 Mt    13.600 %         1 x       14.971 Mt        0.000 Mt        4.366 Mt    29.164 % 	 ..../RayTracingRLPrep

		       0.234 Mt     1.566 %         1 x        0.234 Mt        0.000 Mt        0.234 Mt   100.000 % 	 ...../PrepareAccelerationStructures

		       0.024 Mt     0.163 %         1 x        0.024 Mt        0.000 Mt        0.024 Mt   100.000 % 	 ...../PrepareCache

		       0.482 Mt     3.221 %         1 x        0.482 Mt        0.000 Mt        0.482 Mt   100.000 % 	 ...../BuildCache

		       7.885 Mt    52.667 %         1 x        7.885 Mt        0.000 Mt        0.004 Mt     0.046 % 	 ...../BuildAccelerationStructures

		       7.881 Mt    99.954 %         1 x        7.881 Mt        0.000 Mt        5.815 Mt    73.788 % 	 ....../AccelerationStructureBuild

		       1.037 Mt    13.163 %         1 x        1.037 Mt        0.000 Mt        1.037 Mt   100.000 % 	 ......./BuildBottomLevelAS

		       1.028 Mt    13.049 %         1 x        1.028 Mt        0.000 Mt        1.028 Mt   100.000 % 	 ......./BuildTopLevelAS

		       1.979 Mt    13.219 %         1 x        1.979 Mt        0.000 Mt        1.979 Mt   100.000 % 	 ...../BuildShaderTables

		      10.276 Mt     9.336 %         1 x       10.276 Mt        0.000 Mt       10.276 Mt   100.000 % 	 ..../GpuSidePrep

		       5.564 Mt     0.019 %         1 x        5.564 Mt        0.000 Mt        5.564 Mt   100.000 % 	 ../DeferredRLInitialization

		      51.093 Mt     0.172 %         1 x       51.093 Mt        0.000 Mt        2.162 Mt     4.231 % 	 ../RayTracingRLInitialization

		      48.931 Mt    95.769 %         1 x       48.931 Mt        0.000 Mt       48.931 Mt   100.000 % 	 .../PipelineBuild

		       4.957 Mt     0.017 %         1 x        4.957 Mt        0.000 Mt        4.957 Mt   100.000 % 	 ../ResolveRLInitialization

		   29125.241 Mt    97.991 %      4798 x        6.070 Mt       27.185 Mt      424.592 Mt     1.458 % 	 ../MainLoop

		    1933.593 Mt     6.639 %      1747 x        1.107 Mt        0.760 Mt     1929.627 Mt    99.795 % 	 .../Update

		       3.966 Mt     0.205 %         1 x        3.966 Mt        0.000 Mt        3.966 Mt   100.000 % 	 ..../GuiModelApply

		   26767.056 Mt    91.903 %      3124 x        8.568 Mt        6.274 Mt    19088.315 Mt    71.313 % 	 .../Render

		    7678.741 Mt    28.687 %      3124 x        2.458 Mt        1.876 Mt     7642.489 Mt    99.528 % 	 ..../RayTracing

		      19.858 Mt     0.259 %      3124 x        0.006 Mt        0.005 Mt       19.858 Mt   100.000 % 	 ...../DeferredRLPrep

		      16.394 Mt     0.214 %      3124 x        0.005 Mt        0.004 Mt       16.394 Mt   100.000 % 	 ...../RayTracingRLPrep

	WindowThread:

		   29710.904 Mt   100.000 %         1 x    29710.905 Mt    29710.903 Mt    29710.904 Mt   100.000 % 	 /

	GpuThread:

		   13511.111 Mt   100.000 %      3124 x        4.325 Mt        2.649 Mt      170.512 Mt     1.262 % 	 /

		       0.154 Mt     0.001 %         1 x        0.154 Mt        0.000 Mt        0.005 Mt     3.256 % 	 ./ScenePrep

		       0.149 Mt    96.744 %         1 x        0.149 Mt        0.000 Mt        0.001 Mt     0.514 % 	 ../AccelerationStructureBuild

		       0.088 Mt    58.735 %         1 x        0.088 Mt        0.000 Mt        0.088 Mt   100.000 % 	 .../BuildBottomLevelASGpu

		       0.061 Mt    40.750 %         1 x        0.061 Mt        0.000 Mt        0.061 Mt   100.000 % 	 .../BuildTopLevelASGpu

		   13249.858 Mt    98.066 %      3124 x        4.241 Mt        2.646 Mt        3.492 Mt     0.026 % 	 ./RayTracingGpu

		     839.167 Mt     6.333 %      3124 x        0.269 Mt        0.065 Mt      839.167 Mt   100.000 % 	 ../DeferredRLGpu

		    4807.618 Mt    36.284 %      3124 x        1.539 Mt        0.301 Mt     4807.618 Mt   100.000 % 	 ../RayTracingRLGpu

		    7599.581 Mt    57.356 %      3124 x        2.433 Mt        2.278 Mt     7599.581 Mt   100.000 % 	 ../ResolveRLGpu

		      90.586 Mt     0.670 %      3124 x        0.029 Mt        0.002 Mt       90.586 Mt   100.000 % 	 ./Present


	============================


