Configuration: 
Command line parameters  = E:\programming\FIT-DIP\Masters-DXR-Main-git\build\Measurement\x64\Debug\Measurement.exe --scene Sponza/Sponza.gltf --profile-automation --profile-camera-track Sponza/Sponza.trk --width 2560 --height 1440 --profile-output TrackSponza1440Ra 
Window width             = 2560
Window height            = 1421
Window title             = Direct3D App
Back-buffer count        = 3
Fill Triangles           = enabled
Triangles Font Clock-Wise= enabled
Fast Build AS            = disabled
Target FPS               = 0
Target UPS               = 60
Tearing                  = enabled
VSync                    = disabled
GUI rendering            = disabled
Debug layer              = enabled
GPU validation           = disabled
GPU profiling            = enabled
Render mode              = Rasterization
Loaded scene file        = Sponza/Sponza.gltf
Using testing scene      = disabled
Duplication              = 1
Duplication in cube      = disabled
Duplication offset       = 10
BLAS duplication         = disabled
Rays per pixel           = 14
Quality preset           = High
Rasterization Quality    : 
  AO                     = enabled
  AO kernel size         = 4
  Shadows                = enabled
  Shadow map resolution  = 4096
  Shadow PCF kernel size = 4
  Reflections            = enabled
Ray Tracing Quality      : 
  AO                     = enabled
  AO rays                = 4
  AO kernel size         = 4
  Shadows                = enabled
  Shadow rays            = 4
  Shadow kernel size     = 4
  Reflections            = enabled

ProfilingAggregator: 
Render	,	8.517	,	8.3477	,	7.9114	,	8.4609	,	7.9451	,	7.7087	,	8.3604	,	7.8519	,	7.0643	,	7.6004	,	8.095	,	8.6171	,	7.6908	,	8.2013	,	8.1205	,	8.0308	,	7.8098	,	7.5867	,	7.9154	,	8.1625	,	8.7049	,	9.2358	,	8.471	,	8.5502	,	7.9887	,	7.4336
Update	,	1.0461	,	0.7365	,	0.4932	,	0.668	,	0.7022	,	0.6102	,	0.7638	,	0.5166	,	0.4667	,	0.5755	,	0.5381	,	0.488	,	0.4744	,	0.6677	,	0.9188	,	1.1506	,	0.5479	,	1.0085	,	0.4816	,	0.5605	,	0.9015	,	0.564	,	0.6872	,	1.1003	,	1.0181	,	0.608
DeferredRLGpu	,	1.52179	,	1.57008	,	1.57354	,	1.59434	,	1.50784	,	1.44646	,	1.29533	,	0.860992	,	1.08262	,	1.28288	,	1.65232	,	1.96115	,	1.55283	,	1.45946	,	1.42163	,	1.32336	,	1.29402	,	1.01866	,	1.14992	,	1.43533	,	2.22352	,	2.2561	,	1.95622	,	1.52448	,	1.33734	,	1.31741
Index	,	0	,	1	,	2	,	3	,	4	,	5	,	6	,	7	,	8	,	9	,	10	,	11	,	12	,	13	,	14	,	15	,	16	,	17	,	18	,	19	,	20	,	21	,	22	,	23	,	24	,	25

ProfilingBuildAggregator: 
BuildBottomLevelAS	,	5.4507
BuildBottomLevelASGpu	,	15.8974
BuildTopLevelAS	,	2.4817
BuildTopLevelASGpu	,	0.634048
Duplication	,	1
Triangles	,	262267
Meshes	,	103
BottomLevels	,	1
TopLevelSize	,	64
BottomLevelsSize	,	14623872
Index	,	0

FpsAggregator: 
FPS	,	102	,	115	,	118	,	118	,	117	,	118	,	121	,	124	,	130	,	133	,	122	,	121	,	120	,	119	,	119	,	119	,	121	,	125	,	126	,	124	,	113	,	105	,	108	,	114	,	120	,	121
UPS	,	59	,	60	,	60	,	61	,	60	,	60	,	60	,	60	,	61	,	60	,	60	,	60	,	61	,	60	,	60	,	60	,	60	,	61	,	60	,	60	,	61	,	60	,	60	,	60	,	61	,	60
FrameTime	,	9.83543	,	8.69971	,	8.48619	,	8.50999	,	8.59077	,	8.49688	,	8.30173	,	8.07698	,	7.71181	,	7.57005	,	8.20546	,	8.32528	,	8.37988	,	8.4268	,	8.4269	,	8.4157	,	8.29011	,	8.05959	,	7.96583	,	8.08776	,	8.9036	,	9.54123	,	9.28451	,	8.80812	,	8.39425	,	8.29805
GigaRays/s	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0
Index	,	0	,	1	,	2	,	3	,	4	,	5	,	6	,	7	,	8	,	9	,	10	,	11	,	12	,	13	,	14	,	15	,	16	,	17	,	18	,	19	,	20	,	21	,	22	,	23	,	24	,	25


AccelerationStructure: 
Ray tracing acceleration structure building statistics: 
	Bottom Level Acceleration Structure: 
		Maximum [B]: 14623872
		Minimum [B]: 14623872
		Currently Allocated [B]: 14623872
		Currently Created [num]: 1
		Current Average [B]: 14623872
		Maximum Triangles [num]: 262267
		Minimum Triangles [num]: 262267
		Total Triangles [num]: 262267
		Maximum Meshes [num]: 103
		Minimum Meshes [num]: 103
		Total Meshes [num]: 103
	Top Level Acceleration Structure: 
		Maximum [B]: 64
		Minimum [B]: 64
		Currently Allocated [B]: 64
		Total Allocated [B]: 64
		Total Created [num]: 1
		Average Allocated [B]: 64
		Maximum Instances [num]: 1
		Minimum Instances [num]: 1
		Total Instances [num]: 1
	Scratch Buffer: 
		Maximum [B]: 11660800
		Minimum [B]: 11660800
		Currently Allocated [B]: 11660800
		Total Allocated [B]: 11660800
		Total Created [num]: 1
		Average Allocated [B]: 11660800
ProfilingStack: 
[PROF] Profiling statistics: 
	Scopes entered : 40859
	Scopes exited : 40858
	Overhead per scope [ticks] : 996.6
	Threads entered : 3
	Threads exited : 0
	============================
		         Sum[Mt]   Parent[%]  Samples[num]       Avg[Mt]   Last/Curr[Mt]        Self[Mt]     Self[%] 	 Name

	main:

		   35683.779 Mt   100.000 %         1 x    35683.782 Mt    35683.777 Mt        1.043 Mt     0.003 % 	 /

		   35682.867 Mt    99.997 %         1 x    35682.873 Mt    35682.866 Mt      395.371 Mt     1.108 % 	 ./Main application loop

		       2.838 Mt     0.008 %         1 x        2.838 Mt        0.000 Mt        2.838 Mt   100.000 % 	 ../TexturedRLInitialization

		       2.492 Mt     0.007 %         1 x        2.492 Mt        0.000 Mt        2.492 Mt   100.000 % 	 ../ShadowMapRLInitialization

		       4.506 Mt     0.013 %         1 x        4.506 Mt        0.000 Mt        4.506 Mt   100.000 % 	 ../AmbientOcclusionRLInitialization

		       6.187 Mt     0.017 %         1 x        6.187 Mt        0.000 Mt        6.187 Mt   100.000 % 	 ../RasterResolveRLInitialization

		    9107.966 Mt    25.525 %         1 x     9107.966 Mt        0.000 Mt       12.316 Mt     0.135 % 	 ../Initialization

		    9095.650 Mt    99.865 %         1 x     9095.650 Mt        0.000 Mt        3.516 Mt     0.039 % 	 .../ScenePrep

		    8967.938 Mt    98.596 %         1 x     8967.938 Mt        0.000 Mt      342.639 Mt     3.821 % 	 ..../SceneLoad

		    8152.641 Mt    90.909 %         1 x     8152.641 Mt        0.000 Mt     1110.265 Mt    13.618 % 	 ...../glTF-LoadScene

		    7042.376 Mt    86.382 %        69 x      102.063 Mt        0.000 Mt     7042.376 Mt   100.000 % 	 ....../glTF-LoadImageData

		     312.999 Mt     3.490 %         1 x      312.999 Mt        0.000 Mt      312.999 Mt   100.000 % 	 ...../glTF-CreateScene

		     159.660 Mt     1.780 %         1 x      159.660 Mt        0.000 Mt      159.660 Mt   100.000 % 	 ...../SceneDesc-CreateScene

		       2.562 Mt     0.028 %         1 x        2.562 Mt        0.000 Mt        0.005 Mt     0.199 % 	 ..../ShadowMapRLPrep

		       2.557 Mt    99.801 %         1 x        2.557 Mt        0.000 Mt        1.870 Mt    73.122 % 	 ...../PrepareForRendering

		       0.687 Mt    26.878 %         1 x        0.687 Mt        0.000 Mt        0.687 Mt   100.000 % 	 ....../PrepareDrawBundle

		      16.831 Mt     0.185 %         1 x       16.831 Mt        0.000 Mt        0.005 Mt     0.030 % 	 ..../TexturedRLPrep

		      16.826 Mt    99.970 %         1 x       16.826 Mt        0.000 Mt        1.402 Mt     8.335 % 	 ...../PrepareForRendering

		       7.404 Mt    44.005 %         1 x        7.404 Mt        0.000 Mt        7.404 Mt   100.000 % 	 ....../PrepareMaterials

		       8.019 Mt    47.660 %         1 x        8.019 Mt        0.000 Mt        8.019 Mt   100.000 % 	 ....../PrepareDrawBundle

		       8.404 Mt     0.092 %         1 x        8.404 Mt        0.000 Mt        0.005 Mt     0.061 % 	 ..../DeferredRLPrep

		       8.399 Mt    99.939 %         1 x        8.399 Mt        0.000 Mt        3.230 Mt    38.456 % 	 ...../PrepareForRendering

		       0.023 Mt     0.269 %         1 x        0.023 Mt        0.000 Mt        0.023 Mt   100.000 % 	 ....../PrepareMaterials

		       2.487 Mt    29.608 %         1 x        2.487 Mt        0.000 Mt        2.487 Mt   100.000 % 	 ....../BuildMaterialCache

		       2.660 Mt    31.667 %         1 x        2.660 Mt        0.000 Mt        2.660 Mt   100.000 % 	 ....../PrepareDrawBundle

		      33.807 Mt     0.372 %         1 x       33.807 Mt        0.000 Mt        6.510 Mt    19.256 % 	 ..../RayTracingRLPrep

		       0.629 Mt     1.859 %         1 x        0.629 Mt        0.000 Mt        0.629 Mt   100.000 % 	 ...../PrepareAccelerationStructures

		       0.026 Mt     0.077 %         1 x        0.026 Mt        0.000 Mt        0.026 Mt   100.000 % 	 ...../PrepareCache

		       5.319 Mt    15.733 %         1 x        5.319 Mt        0.000 Mt        5.319 Mt   100.000 % 	 ...../BuildCache

		      16.983 Mt    50.234 %         1 x       16.983 Mt        0.000 Mt        0.004 Mt     0.021 % 	 ...../BuildAccelerationStructures

		      16.979 Mt    99.979 %         1 x       16.979 Mt        0.000 Mt        9.047 Mt    53.281 % 	 ....../AccelerationStructureBuild

		       5.451 Mt    32.102 %         1 x        5.451 Mt        0.000 Mt        5.451 Mt   100.000 % 	 ......./BuildBottomLevelAS

		       2.482 Mt    14.616 %         1 x        2.482 Mt        0.000 Mt        2.482 Mt   100.000 % 	 ......./BuildTopLevelAS

		       4.341 Mt    12.841 %         1 x        4.341 Mt        0.000 Mt        4.341 Mt   100.000 % 	 ...../BuildShaderTables

		      62.590 Mt     0.688 %         1 x       62.590 Mt        0.000 Mt       62.590 Mt   100.000 % 	 ..../GpuSidePrep

		       5.146 Mt     0.014 %         1 x        5.146 Mt        0.000 Mt        5.146 Mt   100.000 % 	 ../DeferredRLInitialization

		      52.565 Mt     0.147 %         1 x       52.565 Mt        0.000 Mt        2.249 Mt     4.279 % 	 ../RayTracingRLInitialization

		      50.316 Mt    95.721 %         1 x       50.316 Mt        0.000 Mt       50.316 Mt   100.000 % 	 .../PipelineBuild

		       5.161 Mt     0.014 %         1 x        5.161 Mt        0.000 Mt        5.161 Mt   100.000 % 	 ../ResolveRLInitialization

		   26100.635 Mt    73.146 %      8232 x        3.171 Mt        8.446 Mt      167.223 Mt     0.641 % 	 ../MainLoop

		    1126.887 Mt     4.317 %      1566 x        0.720 Mt        0.525 Mt     1125.580 Mt    99.884 % 	 .../Update

		       1.307 Mt     0.116 %         1 x        1.307 Mt        0.000 Mt        1.307 Mt   100.000 % 	 ..../GuiModelApply

		   24806.525 Mt    95.042 %      3095 x        8.015 Mt        7.911 Mt    17546.061 Mt    70.732 % 	 .../Render

		    7260.464 Mt    29.268 %      3095 x        2.346 Mt        2.310 Mt     7236.935 Mt    99.676 % 	 ..../Rasterization

		      12.679 Mt     0.175 %      3095 x        0.004 Mt        0.004 Mt       12.679 Mt   100.000 % 	 ...../DeferredRLPrep

		      10.851 Mt     0.149 %      3095 x        0.004 Mt        0.005 Mt       10.851 Mt   100.000 % 	 ...../ShadowMapRLPrep

	WindowThread:

		   35673.091 Mt   100.000 %         1 x    35673.093 Mt    35673.090 Mt    35673.091 Mt   100.000 % 	 /

	GpuThread:

		   14493.667 Mt   100.000 %      3095 x        4.683 Mt        4.350 Mt      189.467 Mt     1.307 % 	 /

		      16.584 Mt     0.114 %         1 x       16.584 Mt        0.000 Mt        0.049 Mt     0.298 % 	 ./ScenePrep

		      16.535 Mt    99.702 %         1 x       16.535 Mt        0.000 Mt        0.003 Mt     0.020 % 	 ../AccelerationStructureBuild

		      15.897 Mt    96.145 %         1 x       15.897 Mt        0.000 Mt       15.897 Mt   100.000 % 	 .../BuildBottomLevelASGpu

		       0.634 Mt     3.835 %         1 x        0.634 Mt        0.000 Mt        0.634 Mt   100.000 % 	 .../BuildTopLevelASGpu

		   14207.118 Mt    98.023 %      3095 x        4.590 Mt        4.347 Mt        4.594 Mt     0.032 % 	 ./RasterizationGpu

		    4611.490 Mt    32.459 %      3095 x        1.490 Mt        1.328 Mt     4611.490 Mt   100.000 % 	 ../DeferredRLGpu

		     470.723 Mt     3.313 %      3095 x        0.152 Mt        0.144 Mt      470.723 Mt   100.000 % 	 ../ShadowMapRLGpu

		    1884.490 Mt    13.264 %      3095 x        0.609 Mt        0.596 Mt     1884.490 Mt   100.000 % 	 ../AmbientOcclusionRLGpu

		    7235.822 Mt    50.931 %      3095 x        2.338 Mt        2.277 Mt     7235.822 Mt   100.000 % 	 ../RasterResolveRLGpu

		      80.498 Mt     0.555 %      3095 x        0.026 Mt        0.001 Mt       80.498 Mt   100.000 % 	 ./Present


	============================


