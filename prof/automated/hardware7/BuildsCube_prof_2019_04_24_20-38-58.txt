Configuration: 
Command line parameters  = E:\programming\FIT-DIP\Masters-DXR-Main-git\build\Measurement\x64\Debug\Measurement.exe --scene Cube/Cube.gltf --profile-builds --profile-output BuildsCube 
Window width             = 1024
Window height            = 768
Window title             = Direct3D App
Back-buffer count        = 3
Fill Triangles           = enabled
Triangles Font Clock-Wise= enabled
Fast Build AS            = disabled
Target FPS               = 60
Target UPS               = 60
Tearing                  = disabled
VSync                    = disabled
GUI rendering            = enabled
Debug layer              = enabled
GPU validation           = disabled
GPU profiling            = enabled
Render mode              = Rasterization
Loaded scene file        = Cube/Cube.gltf
Using testing scene      = disabled
Duplication              = 1
Duplication in cube      = disabled
Duplication offset       = 10
BLAS duplication         = disabled
Rays per pixel           = 14
Quality preset           = High
Rasterization Quality    : 
  AO                     = enabled
  AO kernel size         = 4
  Shadows                = enabled
  Shadow map resolution  = 4096
  Shadow PCF kernel size = 4
  Reflections            = enabled
Ray Tracing Quality      : 
  AO                     = enabled
  AO rays                = 4
  AO kernel size         = 4
  Shadows                = enabled
  Shadow rays            = 4
  Shadow kernel size     = 4
  Reflections            = enabled

ProfilingAggregator: 

ProfilingBuildAggregator: 
BuildBottomLevelAS	,	0.5005	,	0.5105
BuildBottomLevelASGpu	,	0.0744	,	0.126592
BuildTopLevelAS	,	0.6929	,	2.0171
BuildTopLevelASGpu	,	0.054944	,	0.0616
Duplication	,	1	,	1
Triangles	,	12	,	12
Meshes	,	1	,	1
BottomLevels	,	1	,	1
TopLevelSize	,	64	,	64
BottomLevelsSize	,	5168	,	6208
Index	,	0	,	1

FpsAggregator: 
FPS	,	0	,	0
UPS	,	0	,	0
FrameTime	,	0	,	0
GigaRays/s	,	0	,	0
Index	,	0	,	1


AccelerationStructure: 
Ray tracing acceleration structure building statistics: 
	Bottom Level Acceleration Structure: 
		Maximum [B]: 5168
		Minimum [B]: 5168
		Currently Allocated [B]: 5168
		Currently Created [num]: 1
		Current Average [B]: 5168
		Maximum Triangles [num]: 12
		Minimum Triangles [num]: 12
		Total Triangles [num]: 12
		Maximum Meshes [num]: 1
		Minimum Meshes [num]: 1
		Total Meshes [num]: 1
	Top Level Acceleration Structure: 
		Maximum [B]: 64
		Minimum [B]: 64
		Currently Allocated [B]: 64
		Total Allocated [B]: 64
		Total Created [num]: 1
		Average Allocated [B]: 64
		Maximum Instances [num]: 1
		Minimum Instances [num]: 1
		Total Instances [num]: 1
	Scratch Buffer: 
		Maximum [B]: 3584
		Minimum [B]: 3584
		Currently Allocated [B]: 3584
		Total Allocated [B]: 3584
		Total Created [num]: 1
		Average Allocated [B]: 3584
ProfilingStack: 
[PROF] Profiling statistics: 
	Scopes entered : 61
	Scopes exited : 60
	Overhead per scope [ticks] : 1016.8
	Threads entered : 3
	Threads exited : 0
	============================
		         Sum[Mt]   Parent[%]  Samples[num]       Avg[Mt]   Last/Curr[Mt]        Self[Mt]     Self[%] 	 Name

	main:

		     586.288 Mt   100.000 %         1 x      586.290 Mt      586.287 Mt        0.766 Mt     0.131 % 	 /

		     585.576 Mt    99.878 %         1 x      585.578 Mt      585.575 Mt      326.136 Mt    55.695 % 	 ./Main application loop

		       3.065 Mt     0.523 %         1 x        3.065 Mt        3.065 Mt        3.065 Mt   100.000 % 	 ../TexturedRLInitialization

		       2.464 Mt     0.421 %         1 x        2.464 Mt        2.464 Mt        2.464 Mt   100.000 % 	 ../ShadowMapRLInitialization

		       4.444 Mt     0.759 %         1 x        4.444 Mt        4.444 Mt        4.444 Mt   100.000 % 	 ../AmbientOcclusionRLInitialization

		      11.667 Mt     1.992 %         1 x       11.667 Mt       11.667 Mt       11.667 Mt   100.000 % 	 ../RasterResolveRLInitialization

		     142.999 Mt    24.420 %         1 x      142.999 Mt      142.999 Mt        5.587 Mt     3.907 % 	 ../Initialization

		     137.411 Mt    96.093 %         1 x      137.411 Mt      137.411 Mt        1.994 Mt     1.451 % 	 .../ScenePrep

		     103.631 Mt    75.417 %         1 x      103.631 Mt      103.631 Mt       15.313 Mt    14.777 % 	 ..../SceneLoad

		      58.485 Mt    56.435 %         1 x       58.485 Mt       58.485 Mt       13.535 Mt    23.142 % 	 ...../glTF-LoadScene

		      44.950 Mt    76.858 %         2 x       22.475 Mt        2.308 Mt       44.950 Mt   100.000 % 	 ....../glTF-LoadImageData

		      13.044 Mt    12.587 %         1 x       13.044 Mt       13.044 Mt       13.044 Mt   100.000 % 	 ...../glTF-CreateScene

		      16.789 Mt    16.201 %         1 x       16.789 Mt       16.789 Mt       16.789 Mt   100.000 % 	 ...../SceneDesc-CreateScene

		       1.378 Mt     1.003 %         1 x        1.378 Mt        1.378 Mt        0.005 Mt     0.334 % 	 ..../ShadowMapRLPrep

		       1.373 Mt    99.666 %         1 x        1.373 Mt        1.373 Mt        1.307 Mt    95.179 % 	 ...../PrepareForRendering

		       0.066 Mt     4.821 %         1 x        0.066 Mt        0.066 Mt        0.066 Mt   100.000 % 	 ....../PrepareDrawBundle

		       2.882 Mt     2.097 %         1 x        2.882 Mt        2.882 Mt        0.004 Mt     0.149 % 	 ..../TexturedRLPrep

		       2.877 Mt    99.851 %         1 x        2.877 Mt        2.877 Mt        1.400 Mt    48.671 % 	 ...../PrepareForRendering

		       1.198 Mt    41.654 %         1 x        1.198 Mt        1.198 Mt        1.198 Mt   100.000 % 	 ....../PrepareMaterials

		       0.278 Mt     9.676 %         1 x        0.278 Mt        0.278 Mt        0.278 Mt   100.000 % 	 ....../PrepareDrawBundle

		       4.688 Mt     3.412 %         1 x        4.688 Mt        4.688 Mt        0.005 Mt     0.100 % 	 ..../DeferredRLPrep

		       4.684 Mt    99.900 %         1 x        4.684 Mt        4.684 Mt        2.279 Mt    48.652 % 	 ...../PrepareForRendering

		       0.017 Mt     0.359 %         1 x        0.017 Mt        0.017 Mt        0.017 Mt   100.000 % 	 ....../PrepareMaterials

		       2.114 Mt    45.127 %         1 x        2.114 Mt        2.114 Mt        2.114 Mt   100.000 % 	 ....../BuildMaterialCache

		       0.275 Mt     5.863 %         1 x        0.275 Mt        0.275 Mt        0.275 Mt   100.000 % 	 ....../PrepareDrawBundle

		      14.209 Mt    10.341 %         1 x       14.209 Mt       14.209 Mt        4.437 Mt    31.223 % 	 ..../RayTracingRLPrep

		       0.244 Mt     1.717 %         1 x        0.244 Mt        0.244 Mt        0.244 Mt   100.000 % 	 ...../PrepareAccelerationStructures

		       0.025 Mt     0.174 %         1 x        0.025 Mt        0.025 Mt        0.025 Mt   100.000 % 	 ...../PrepareCache

		       1.113 Mt     7.832 %         1 x        1.113 Mt        1.113 Mt        1.113 Mt   100.000 % 	 ...../BuildCache

		       6.094 Mt    42.885 %         1 x        6.094 Mt        6.094 Mt        0.004 Mt     0.071 % 	 ...../BuildAccelerationStructures

		       6.089 Mt    99.929 %         1 x        6.089 Mt        6.089 Mt        3.894 Mt    63.943 % 	 ....../AccelerationStructureBuild

		       1.062 Mt    17.436 %         1 x        1.062 Mt        1.062 Mt        1.062 Mt   100.000 % 	 ......./BuildBottomLevelAS

		       1.134 Mt    18.621 %         1 x        1.134 Mt        1.134 Mt        1.134 Mt   100.000 % 	 ......./BuildTopLevelAS

		       2.298 Mt    16.170 %         1 x        2.298 Mt        2.298 Mt        2.298 Mt   100.000 % 	 ...../BuildShaderTables

		       8.629 Mt     6.280 %         1 x        8.629 Mt        8.629 Mt        8.629 Mt   100.000 % 	 ..../GpuSidePrep

		      21.468 Mt     3.666 %         3 x        7.156 Mt        4.576 Mt       14.993 Mt    69.840 % 	 ../AccelerationStructureBuild

		       2.476 Mt    11.532 %         3 x        0.825 Mt        0.510 Mt        2.476 Mt   100.000 % 	 .../BuildBottomLevelAS

		       3.999 Mt    18.629 %         3 x        1.333 Mt        2.017 Mt        3.999 Mt   100.000 % 	 .../BuildTopLevelAS

		       5.324 Mt     0.909 %         1 x        5.324 Mt        5.324 Mt        5.324 Mt   100.000 % 	 ../DeferredRLInitialization

		      63.017 Mt    10.762 %         1 x       63.017 Mt       63.017 Mt        2.351 Mt     3.731 % 	 ../RayTracingRLInitialization

		      60.666 Mt    96.269 %         1 x       60.666 Mt       60.666 Mt       60.666 Mt   100.000 % 	 .../PipelineBuild

		       4.992 Mt     0.852 %         1 x        4.992 Mt        4.992 Mt        4.992 Mt   100.000 % 	 ../ResolveRLInitialization

	WindowThread:

		     582.632 Mt   100.000 %         1 x      582.632 Mt      582.631 Mt      582.632 Mt   100.000 % 	 /

	GpuThread:

		      46.506 Mt   100.000 %         2 x       23.253 Mt        0.189 Mt       45.894 Mt    98.683 % 	 /

		       0.163 Mt     0.350 %         1 x        0.163 Mt        0.163 Mt        0.005 Mt     2.889 % 	 ./ScenePrep

		       0.158 Mt    97.111 %         1 x        0.158 Mt        0.158 Mt        0.001 Mt     0.607 % 	 ../AccelerationStructureBuild

		       0.092 Mt    58.470 %         1 x        0.092 Mt        0.092 Mt        0.092 Mt   100.000 % 	 .../BuildBottomLevelASGpu

		       0.065 Mt    40.923 %         1 x        0.065 Mt        0.065 Mt        0.065 Mt   100.000 % 	 .../BuildTopLevelASGpu

		       0.450 Mt     0.967 %         3 x        0.150 Mt        0.189 Mt        0.003 Mt     0.612 % 	 ./AccelerationStructureBuild

		       0.275 Mt    61.248 %         3 x        0.092 Mt        0.127 Mt        0.275 Mt   100.000 % 	 ../BuildBottomLevelASGpu

		       0.171 Mt    38.140 %         3 x        0.057 Mt        0.062 Mt        0.171 Mt   100.000 % 	 ../BuildTopLevelASGpu


	============================


