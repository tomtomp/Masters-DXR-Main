Configuration: 
Command line parameters  = E:\programming\FIT-DIP\Masters-DXR-Main-git\build\Measurement\x64\Debug\Measurement.exe --scene Sponza/Sponza.gltf --duplication-cube --duplication-offset 0 --profile-triangles --profile-camera-track Sponza/SponzaFast.trk --profile-max-duplication 6 --rt-mode --width 2560 --height 1440 --profile-output TrianglesSponzaNoOffset1440Rt 
Window width             = 2560
Window height            = 1421
Window title             = Direct3D App
Back-buffer count        = 3
Fill Triangles           = enabled
Triangles Font Clock-Wise= enabled
Fast Build AS            = disabled
Target FPS               = 0
Target UPS               = 60
Tearing                  = enabled
VSync                    = disabled
GUI rendering            = disabled
Debug layer              = enabled
GPU validation           = disabled
GPU profiling            = enabled
Render mode              = Ray Tracing
Loaded scene file        = Sponza/Sponza.gltf
Using testing scene      = disabled
Duplication              = 6
Duplication in cube      = enabled
Duplication offset       = 0
BLAS duplication         = enabled
Rays per pixel           = 14
Quality preset           = High
Rasterization Quality    : 
  AO                     = enabled
  AO kernel size         = 4
  Shadows                = enabled
  Shadow map resolution  = 4096
  Shadow PCF kernel size = 4
  Reflections            = enabled
Ray Tracing Quality      : 
  AO                     = enabled
  AO rays                = 4
  AO kernel size         = 4
  Shadows                = enabled
  Shadow rays            = 4
  Shadow kernel size     = 4
  Reflections            = enabled

ProfilingAggregator: 
Render	,	12.6592	,	12.6592	,	13.5813	,	13.5813	,	13.2239	,	13.2239	,	13.3803	,	13.3803	,	15.3973	,	15.3973	,	13.4867	,	36.4131	,	36.4131	,	36.6012	,	36.6012	,	34.8636	,	34.8636	,	39.0673	,	39.0673	,	52.3063	,	52.3063	,	36.219	,	99.7563	,	99.7563	,	100.221	,	100.221	,	97.2974	,	97.2974	,	227.467	,	227.467	,	117.065	,	117.065	,	102.007	,	252.822	,	252.822	,	223.213	,	223.213	,	244.189	,	244.189	,	484.36	,	484.36	,	253.661	,	476.121	,	476.121	,	475.344	,	475.344	,	1037.93	,	1037.93	,	529.868	,	1773.03	,	1773.03	,	977.225
Update	,	0.8819	,	0.8819	,	0.9849	,	0.9849	,	0.9078	,	0.9078	,	0.7143	,	0.7143	,	0.6323	,	0.6323	,	0.5587	,	0.5036	,	0.5036	,	0.497	,	0.497	,	0.499	,	0.499	,	0.4862	,	0.4862	,	0.4984	,	0.4984	,	0.5203	,	0.5066	,	0.5066	,	0.5077	,	0.5077	,	0.5086	,	0.5086	,	0.5083	,	0.5083	,	0.5079	,	0.5079	,	0.4969	,	0.5507	,	0.5507	,	0.5527	,	0.5527	,	0.5629	,	0.5629	,	0.4657	,	0.4657	,	0.5422	,	0.6254	,	0.6254	,	0.5279	,	0.5279	,	0.6267	,	0.6267	,	0.6148	,	0.6127	,	0.6127	,	0.6139
RayTracing	,	3.0524	,	3.0524	,	3.0425	,	3.0425	,	3.0071	,	3.0071	,	3.0179	,	3.0179	,	3.0352	,	3.0352	,	3.0724	,	8.8393	,	8.8393	,	8.8102	,	8.8102	,	8.742	,	8.742	,	8.8675	,	8.8675	,	9.0733	,	9.0733	,	8.8308	,	23.0113	,	23.0113	,	22.9754	,	22.9754	,	23.3867	,	23.3867	,	23.3829	,	23.3829	,	23.6066	,	23.6066	,	23.518	,	46.789	,	46.789	,	47.5317	,	47.5317	,	47.0678	,	47.0678	,	45.0291	,	45.0291	,	47.3297	,	86.6808	,	86.6808	,	86.6856	,	86.6856	,	86.3187	,	86.3187	,	85.5865	,	146.398	,	146.398	,	146.292
RayTracingGpu	,	8.14611	,	8.14611	,	9.06403	,	9.06403	,	8.58243	,	8.58243	,	8.70771	,	8.70771	,	10.8515	,	10.8515	,	8.79162	,	25.6595	,	25.6595	,	25.7754	,	25.7754	,	24.3593	,	24.3593	,	28.1181	,	28.1181	,	40.9658	,	40.9658	,	25.4711	,	74.1219	,	74.1219	,	74.5211	,	74.5211	,	71.299	,	71.299	,	201.458	,	201.458	,	90.857	,	90.857	,	75.8582	,	202.379	,	202.379	,	171.951	,	171.951	,	193.576	,	193.576	,	435.758	,	435.758	,	202.648	,	384.226	,	384.226	,	383.715	,	383.715	,	946.466	,	946.466	,	439.369	,	1619.33	,	1619.33	,	823.659
DeferredRLGpu	,	1.19478	,	1.19478	,	1.48122	,	1.48122	,	1.29485	,	1.29485	,	1.12256	,	1.12256	,	1.99296	,	1.99296	,	1.32205	,	1.70352	,	1.70352	,	1.95805	,	1.95805	,	1.61075	,	1.61075	,	1.65718	,	1.65718	,	2.44035	,	2.44035	,	1.71597	,	3.26454	,	3.26454	,	2.93645	,	2.93645	,	2.24576	,	2.24576	,	3.49683	,	3.49683	,	3.24224	,	3.24224	,	2.75776	,	6.9487	,	6.9487	,	4.52691	,	4.52691	,	3.84125	,	3.84125	,	6.52701	,	6.52701	,	4.74986	,	8.13101	,	8.13101	,	6.01168	,	6.01168	,	9.52397	,	9.52397	,	8.10538	,	13.8455	,	13.8455	,	13.0363
RayTracingRLGpu	,	4.4809	,	4.4809	,	4.98608	,	4.98608	,	4.61232	,	4.61232	,	4.92115	,	4.92115	,	6.22813	,	6.22813	,	4.98096	,	21.3324	,	21.3324	,	21.3324	,	21.3324	,	20.2586	,	20.2586	,	23.9946	,	23.9946	,	36.0382	,	36.0382	,	21.1429	,	68.3847	,	68.3847	,	69.0867	,	69.0867	,	66.5756	,	66.5756	,	195.471	,	195.471	,	85.1238	,	85.1238	,	70.6148	,	192.927	,	192.927	,	164.921	,	164.921	,	187.22	,	187.22	,	426.74	,	426.74	,	195.39	,	373.591	,	373.591	,	375.218	,	375.218	,	934.437	,	934.437	,	428.753	,	1603.01	,	1603.01	,	808.14
ResolveRLGpu	,	2.4695	,	2.4695	,	2.59578	,	2.59578	,	2.6745	,	2.6745	,	2.66285	,	2.66285	,	2.62934	,	2.62934	,	2.48755	,	2.62096	,	2.62096	,	2.48339	,	2.48339	,	2.48746	,	2.48746	,	2.46445	,	2.46445	,	2.48618	,	2.48618	,	2.6112	,	2.46986	,	2.46986	,	2.49696	,	2.49696	,	2.47613	,	2.47613	,	2.48829	,	2.48829	,	2.48941	,	2.48941	,	2.48406	,	2.50189	,	2.50189	,	2.5015	,	2.5015	,	2.51322	,	2.51322	,	2.48874	,	2.48874	,	2.50694	,	2.50253	,	2.50253	,	2.48307	,	2.48307	,	2.50282	,	2.50282	,	2.50877	,	2.47597	,	2.47597	,	2.48083
Index	,	0	,	1	,	2	,	3	,	4	,	5	,	6	,	7	,	8	,	9	,	10	,	11	,	12	,	13	,	14	,	15	,	16	,	17	,	18	,	19	,	20	,	21	,	22	,	23	,	24	,	25	,	26	,	27	,	28	,	29	,	30	,	31	,	32	,	33	,	34	,	35	,	36	,	37	,	38	,	39	,	40	,	41	,	42	,	43	,	44	,	45	,	46	,	47	,	48	,	49	,	50	,	51

ProfilingBuildAggregator: 
BuildBottomLevelAS	,	4.5065	,	9.2508	,	32.0728	,	79.4163	,	161.137	,	261.113
BuildBottomLevelASGpu	,	15.7021	,	8.97072	,	31.2395	,	72.623	,	141.453	,	243.614
BuildTopLevelAS	,	2.5289	,	0.6532	,	0.6589	,	0.7966	,	0.7088	,	0.6768
BuildTopLevelASGpu	,	0.627904	,	0.165728	,	0.174496	,	0.270752	,	0.366304	,	0.41168
Duplication	,	1	,	2	,	3	,	4	,	5	,	6
Triangles	,	262267	,	2098136	,	7081209	,	16785088	,	32783375	,	56649672
Meshes	,	103	,	824	,	2781	,	6592	,	12875	,	22248
BottomLevels	,	1	,	8	,	27	,	64	,	125	,	216
TopLevelSize	,	64	,	512	,	1728	,	4096	,	8000	,	13824
BottomLevelsSize	,	14623872	,	116990976	,	394844544	,	935927808	,	1827984000	,	3158756352
Index	,	0	,	1	,	2	,	3	,	4	,	5

FpsAggregator: 
FPS	,	70	,	70	,	73	,	73	,	73	,	73	,	72	,	72	,	59	,	59	,	69	,	26	,	26	,	27	,	27	,	28	,	28	,	28	,	28	,	15	,	15	,	25	,	10	,	10	,	10	,	10	,	11	,	11	,	9	,	9	,	6	,	6	,	10	,	4	,	4	,	4	,	4	,	5	,	5	,	3	,	3	,	4	,	2	,	2	,	2	,	2	,	2	,	2	,	2	,	1	,	1	,	1
UPS	,	60	,	60	,	61	,	61	,	61	,	61	,	60	,	60	,	60	,	60	,	60	,	62	,	62	,	61	,	61	,	60	,	60	,	62	,	62	,	59	,	59	,	63	,	64	,	64	,	64	,	64	,	67	,	67	,	58	,	58	,	68	,	68	,	65	,	59	,	59	,	65	,	65	,	72	,	72	,	60	,	60	,	86	,	67	,	67	,	61	,	61	,	59	,	59	,	101	,	55	,	55	,	109
FrameTime	,	14.3346	,	14.3346	,	13.8865	,	13.8865	,	13.8719	,	13.8719	,	13.9063	,	13.9063	,	17.031	,	17.031	,	14.6165	,	39.1834	,	39.1834	,	37.7464	,	37.7464	,	35.9664	,	35.9664	,	36.531	,	36.531	,	67.0035	,	67.0035	,	41.2611	,	105.684	,	105.684	,	107.344	,	107.344	,	99.8278	,	99.8278	,	123.833	,	123.833	,	170.258	,	170.258	,	106.056	,	250.327	,	250.327	,	262.911	,	262.911	,	244.299	,	244.299	,	416.378	,	416.378	,	296.758	,	530.453	,	530.453	,	503.554	,	503.554	,	771.168	,	771.168	,	590.419	,	1815.91	,	1815.91	,	1054.2
GigaRays/s	,	3.55286	,	3.55286	,	3.66749	,	3.66749	,	3.67134	,	3.67134	,	3.66227	,	3.66227	,	2.99035	,	2.99035	,	3.48432	,	1.29975	,	1.29975	,	1.34923	,	1.34923	,	1.41601	,	1.41601	,	1.39412	,	1.39412	,	0.760089	,	0.760089	,	1.2343	,	0.481893	,	0.481893	,	0.474444	,	0.474444	,	0.510165	,	0.510165	,	0.411269	,	0.411269	,	0.299126	,	0.299126	,	0.480205	,	0.203448	,	0.203448	,	0.19371	,	0.19371	,	0.208469	,	0.208469	,	0.122313	,	0.122313	,	0.171616	,	0.0960097	,	0.0960097	,	0.101138	,	0.101138	,	0.0660409	,	0.0660409	,	0.0862585	,	0.0280458	,	0.0280458	,	0.0483104
Duplication	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	1	,	2	,	2	,	2	,	2	,	2	,	2	,	2	,	2	,	2	,	2	,	2	,	3	,	3	,	3	,	3	,	3	,	3	,	3	,	3	,	3	,	3	,	3	,	4	,	4	,	4	,	4	,	4	,	4	,	4	,	4	,	4	,	5	,	5	,	5	,	5	,	5	,	5	,	5	,	6	,	6	,	6
Index	,	0	,	1	,	2	,	3	,	4	,	5	,	6	,	7	,	8	,	9	,	10	,	11	,	12	,	13	,	14	,	15	,	16	,	17	,	18	,	19	,	20	,	21	,	22	,	23	,	24	,	25	,	26	,	27	,	28	,	29	,	30	,	31	,	32	,	33	,	34	,	35	,	36	,	37	,	38	,	39	,	40	,	41	,	42	,	43	,	44	,	45	,	46	,	47	,	48	,	49	,	50	,	51


AccelerationStructure: 
Ray tracing acceleration structure building statistics: 
	Bottom Level Acceleration Structure: 
		Maximum [B]: 14623872
		Minimum [B]: 14623872
		Currently Allocated [B]: 3158756352
		Currently Created [num]: 216
		Current Average [B]: 14623872
		Maximum Triangles [num]: 262267
		Minimum Triangles [num]: 262267
		Total Triangles [num]: 56649672
		Maximum Meshes [num]: 103
		Minimum Meshes [num]: 103
		Total Meshes [num]: 22248
	Top Level Acceleration Structure: 
		Maximum [B]: 13824
		Minimum [B]: 13824
		Currently Allocated [B]: 13824
		Total Allocated [B]: 13824
		Total Created [num]: 1
		Average Allocated [B]: 13824
		Maximum Instances [num]: 216
		Minimum Instances [num]: 216
		Total Instances [num]: 216
	Scratch Buffer: 
		Maximum [B]: 11660800
		Minimum [B]: 11660800
		Currently Allocated [B]: 11660800
		Total Allocated [B]: 11660800
		Total Created [num]: 1
		Average Allocated [B]: 11660800
ProfilingStack: 
[PROF] Profiling statistics: 
	Scopes entered : 17943
	Scopes exited : 17942
	Overhead per scope [ticks] : 996.7
	Threads entered : 3
	Threads exited : 0
	============================
		         Sum[Mt]   Parent[%]  Samples[num]       Avg[Mt]   Last/Curr[Mt]        Self[Mt]     Self[%] 	 Name

	main:

		   65602.965 Mt   100.000 %         1 x    65602.969 Mt    65602.963 Mt        0.724 Mt     0.001 % 	 /

		   65602.392 Mt    99.999 %         1 x    65602.400 Mt    65602.391 Mt      339.931 Mt     0.518 % 	 ./Main application loop

		       2.950 Mt     0.004 %         1 x        2.950 Mt        0.000 Mt        2.950 Mt   100.000 % 	 ../TexturedRLInitialization

		       2.467 Mt     0.004 %         1 x        2.467 Mt        0.000 Mt        2.467 Mt   100.000 % 	 ../ShadowMapRLInitialization

		       3.994 Mt     0.006 %         1 x        3.994 Mt        0.000 Mt        3.994 Mt   100.000 % 	 ../AmbientOcclusionRLInitialization

		       5.360 Mt     0.008 %         1 x        5.360 Mt        0.000 Mt        5.360 Mt   100.000 % 	 ../RasterResolveRLInitialization

		    9300.702 Mt    14.177 %         1 x     9300.702 Mt        0.000 Mt       12.861 Mt     0.138 % 	 ../Initialization

		    9287.841 Mt    99.862 %         1 x     9287.841 Mt        0.000 Mt        3.973 Mt     0.043 % 	 .../ScenePrep

		    9166.546 Mt    98.694 %         1 x     9166.546 Mt        0.000 Mt      400.124 Mt     4.365 % 	 ..../SceneLoad

		    8214.786 Mt    89.617 %         1 x     8214.786 Mt        0.000 Mt     1125.718 Mt    13.704 % 	 ...../glTF-LoadScene

		    7089.068 Mt    86.296 %        69 x      102.740 Mt        0.000 Mt     7089.068 Mt   100.000 % 	 ....../glTF-LoadImageData

		     317.492 Mt     3.464 %         1 x      317.492 Mt        0.000 Mt      317.492 Mt   100.000 % 	 ...../glTF-CreateScene

		     234.144 Mt     2.554 %         1 x      234.144 Mt        0.000 Mt      234.144 Mt   100.000 % 	 ...../SceneDesc-CreateScene

		       2.901 Mt     0.031 %         1 x        2.901 Mt        0.000 Mt        0.005 Mt     0.179 % 	 ..../ShadowMapRLPrep

		       2.895 Mt    99.821 %         1 x        2.895 Mt        0.000 Mt        2.161 Mt    74.628 % 	 ...../PrepareForRendering

		       0.735 Mt    25.372 %         1 x        0.735 Mt        0.000 Mt        0.735 Mt   100.000 % 	 ....../PrepareDrawBundle

		       6.269 Mt     0.067 %         1 x        6.269 Mt        0.000 Mt        0.005 Mt     0.081 % 	 ..../TexturedRLPrep

		       6.263 Mt    99.919 %         1 x        6.263 Mt        0.000 Mt        1.233 Mt    19.689 % 	 ...../PrepareForRendering

		       2.744 Mt    43.805 %         1 x        2.744 Mt        0.000 Mt        2.744 Mt   100.000 % 	 ....../PrepareMaterials

		       2.287 Mt    36.506 %         1 x        2.287 Mt        0.000 Mt        2.287 Mt   100.000 % 	 ....../PrepareDrawBundle

		       7.667 Mt     0.083 %         1 x        7.667 Mt        0.000 Mt        0.005 Mt     0.063 % 	 ..../DeferredRLPrep

		       7.662 Mt    99.937 %         1 x        7.662 Mt        0.000 Mt        2.948 Mt    38.475 % 	 ...../PrepareForRendering

		       0.022 Mt     0.286 %         1 x        0.022 Mt        0.000 Mt        0.022 Mt   100.000 % 	 ....../PrepareMaterials

		       2.162 Mt    28.218 %         1 x        2.162 Mt        0.000 Mt        2.162 Mt   100.000 % 	 ....../BuildMaterialCache

		       2.530 Mt    33.021 %         1 x        2.530 Mt        0.000 Mt        2.530 Mt   100.000 % 	 ....../PrepareDrawBundle

		      29.326 Mt     0.316 %         1 x       29.326 Mt        0.000 Mt        7.144 Mt    24.362 % 	 ..../RayTracingRLPrep

		       0.573 Mt     1.953 %         1 x        0.573 Mt        0.000 Mt        0.573 Mt   100.000 % 	 ...../PrepareAccelerationStructures

		       0.025 Mt     0.085 %         1 x        0.025 Mt        0.000 Mt        0.025 Mt   100.000 % 	 ...../PrepareCache

		       5.383 Mt    18.355 %         1 x        5.383 Mt        0.000 Mt        5.383 Mt   100.000 % 	 ...../BuildCache

		      11.817 Mt    40.294 %         1 x       11.817 Mt        0.000 Mt        0.005 Mt     0.040 % 	 ...../BuildAccelerationStructures

		      11.812 Mt    99.960 %         1 x       11.812 Mt        0.000 Mt        4.777 Mt    40.439 % 	 ....../AccelerationStructureBuild

		       4.506 Mt    38.152 %         1 x        4.506 Mt        0.000 Mt        4.506 Mt   100.000 % 	 ......./BuildBottomLevelAS

		       2.529 Mt    21.410 %         1 x        2.529 Mt        0.000 Mt        2.529 Mt   100.000 % 	 ......./BuildTopLevelAS

		       4.385 Mt    14.952 %         1 x        4.385 Mt        0.000 Mt        4.385 Mt   100.000 % 	 ...../BuildShaderTables

		      71.160 Mt     0.766 %         1 x       71.160 Mt        0.000 Mt       71.160 Mt   100.000 % 	 ..../GpuSidePrep

		       4.973 Mt     0.008 %         1 x        4.973 Mt        0.000 Mt        4.973 Mt   100.000 % 	 ../DeferredRLInitialization

		      58.326 Mt     0.089 %         1 x       58.326 Mt        0.000 Mt        2.280 Mt     3.909 % 	 ../RayTracingRLInitialization

		      56.046 Mt    96.091 %         1 x       56.046 Mt        0.000 Mt       56.046 Mt   100.000 % 	 .../PipelineBuild

		       5.066 Mt     0.008 %         1 x        5.066 Mt        0.000 Mt        5.066 Mt   100.000 % 	 ../ResolveRLInitialization

		   55878.623 Mt    85.178 %      5902 x        9.468 Mt     1044.257 Mt      312.248 Mt     0.559 % 	 ../MainLoop

		    2280.058 Mt     4.080 %      3290 x        0.693 Mt        0.606 Mt     2278.836 Mt    99.946 % 	 .../Update

		       1.222 Mt     0.054 %         1 x        1.222 Mt        0.000 Mt        1.222 Mt   100.000 % 	 ..../GuiModelApply

		   53286.317 Mt    95.361 %       950 x       56.091 Mt      978.745 Mt    41285.540 Mt    77.479 % 	 .../Render

		   12000.778 Mt    22.521 %       950 x       12.632 Mt      147.232 Mt     9408.762 Mt    78.401 % 	 ..../RayTracing

		     918.291 Mt     7.652 %       950 x        0.967 Mt        0.005 Mt        3.952 Mt     0.430 % 	 ...../DeferredRLPrep

		     914.338 Mt    99.570 %         6 x      152.390 Mt        0.000 Mt       28.773 Mt     3.147 % 	 ....../PrepareForRendering

		       1.320 Mt     0.144 %         6 x        0.220 Mt        0.000 Mt        1.320 Mt   100.000 % 	 ......./PrepareMaterials

		       0.005 Mt     0.001 %         6 x        0.001 Mt        0.000 Mt        0.005 Mt   100.000 % 	 ......./BuildMaterialCache

		     884.239 Mt    96.708 %         6 x      147.373 Mt        0.000 Mt      884.239 Mt   100.000 % 	 ......./PrepareDrawBundle

		    1673.725 Mt    13.947 %       950 x        1.762 Mt        0.012 Mt       11.671 Mt     0.697 % 	 ...../RayTracingRLPrep

		     260.991 Mt    15.593 %         6 x       43.498 Mt        0.000 Mt      260.991 Mt   100.000 % 	 ....../PrepareAccelerationStructures

		       1.230 Mt     0.073 %         6 x        0.205 Mt        0.000 Mt        1.230 Mt   100.000 % 	 ....../PrepareCache

		       0.005 Mt     0.000 %         6 x        0.001 Mt        0.000 Mt        0.005 Mt   100.000 % 	 ....../BuildCache

		     572.522 Mt    34.206 %         6 x       95.420 Mt        0.000 Mt        0.017 Mt     0.003 % 	 ....../BuildAccelerationStructures

		     572.505 Mt    99.997 %         6 x       95.418 Mt        0.000 Mt       14.636 Mt     2.557 % 	 ......./AccelerationStructureBuild

		     553.556 Mt    96.690 %         6 x       92.259 Mt        0.000 Mt      553.556 Mt   100.000 % 	 ......../BuildBottomLevelAS

		       4.314 Mt     0.753 %         6 x        0.719 Mt        0.000 Mt        4.314 Mt   100.000 % 	 ......../BuildTopLevelAS

		     827.306 Mt    49.429 %         6 x      137.884 Mt        0.000 Mt      827.306 Mt   100.000 % 	 ....../BuildShaderTables

	WindowThread:

		   65603.060 Mt   100.000 %         1 x    65603.062 Mt    65603.059 Mt    65603.060 Mt   100.000 % 	 /

	GpuThread:

		   39682.670 Mt   100.000 %       950 x       41.771 Mt      824.212 Mt      179.149 Mt     0.451 % 	 /

		      16.382 Mt     0.041 %         1 x       16.382 Mt        0.000 Mt        0.049 Mt     0.300 % 	 ./ScenePrep

		      16.333 Mt    99.700 %         1 x       16.333 Mt        0.000 Mt        0.003 Mt     0.020 % 	 ../AccelerationStructureBuild

		      15.702 Mt    96.136 %         1 x       15.702 Mt        0.000 Mt       15.702 Mt   100.000 % 	 .../BuildBottomLevelASGpu

		       0.628 Mt     3.844 %         1 x        0.628 Mt        0.000 Mt        0.628 Mt   100.000 % 	 .../BuildTopLevelASGpu

		   39446.372 Mt    99.405 %       950 x       41.522 Mt      824.125 Mt        1.266 Mt     0.003 % 	 ./RayTracingGpu

		    1889.801 Mt     4.791 %       950 x        1.989 Mt       13.240 Mt     1889.801 Mt   100.000 % 	 ../DeferredRLGpu

		   34632.850 Mt    87.797 %       950 x       36.456 Mt      808.300 Mt    34632.850 Mt   100.000 % 	 ../RayTracingRLGpu

		    2413.828 Mt     6.119 %       950 x        2.541 Mt        2.583 Mt     2413.828 Mt   100.000 % 	 ../ResolveRLGpu

		     508.628 Mt     1.289 %         6 x       84.771 Mt        0.000 Mt        0.012 Mt     0.002 % 	 ../AccelerationStructureBuild

		     507.124 Mt    99.704 %         6 x       84.521 Mt        0.000 Mt      507.124 Mt   100.000 % 	 .../BuildBottomLevelASGpu

		       1.492 Mt     0.293 %         6 x        0.249 Mt        0.000 Mt        1.492 Mt   100.000 % 	 .../BuildTopLevelASGpu

		      40.766 Mt     0.103 %       950 x        0.043 Mt        0.087 Mt       40.766 Mt   100.000 % 	 ./Present


	============================


