Configuration: 
Command line parameters  = E:\programming\FIT-DIP\Masters-DXR-Main-git\build\Measurement\x64\Debug\Measurement.exe --scene Suzanne/Suzanne.gltf --profile-automation --profile-camera-track Suzanne/Suzanne.trk --rt-mode --width 2560 --height 1440 --profile-output PresetSuzanne1440RtReflections --quality-preset Reflections 
Window width             = 2560
Window height            = 1421
Window title             = Direct3D App
Back-buffer count        = 3
Fill Triangles           = enabled
Triangles Font Clock-Wise= enabled
Fast Build AS            = disabled
Target FPS               = 0
Target UPS               = 60
Tearing                  = enabled
VSync                    = disabled
GUI rendering            = disabled
Debug layer              = enabled
GPU validation           = disabled
GPU profiling            = enabled
Render mode              = Ray Tracing
Loaded scene file        = Suzanne/Suzanne.gltf
Using testing scene      = disabled
Duplication              = 1
Duplication in cube      = disabled
Duplication offset       = 10
BLAS duplication         = disabled
Rays per pixel           = 2
Quality preset           = Reflections
Rasterization Quality    : 
  AO                     = disabled
  AO kernel size         = 1
  Shadows                = disabled
  Shadow map resolution  = 1
  Shadow PCF kernel size = 0
  Reflections            = enabled
Ray Tracing Quality      : 
  AO                     = disabled
  AO rays                = 0
  AO kernel size         = 4
  Shadows                = disabled
  Shadow rays            = 0
  Shadow kernel size     = 4
  Reflections            = enabled

ProfilingAggregator: 
Render	,	3.7987	,	3.0442	,	4.9317	,	3.5117	,	3.0599	,	3.3044	,	3.0256	,	3.6817	,	3.3617	,	4.5979	,	5.256	,	5.5222	,	5.351	,	3.5463	,	3.7542	,	6.1827	,	5.1059	,	5.242	,	4.6637	,	4.4407	,	3.8493	,	3.4496	,	3.2584	,	3.045
Update	,	0.853	,	0.4759	,	0.9729	,	0.4908	,	0.5119	,	1.174	,	0.4708	,	0.6034	,	0.8592	,	0.6061	,	0.8903	,	0.9631	,	0.8857	,	0.4667	,	0.6498	,	0.5798	,	0.6549	,	1.1029	,	0.8971	,	0.9347	,	0.8842	,	1.0539	,	0.4719	,	0.5059
RayTracing	,	2.047	,	1.2379	,	2.3455	,	1.3423	,	1.24	,	1.2284	,	1.2281	,	1.5697	,	1.2327	,	1.3942	,	1.4692	,	2.4379	,	2.4102	,	1.367	,	1.4184	,	2.764	,	1.751	,	1.6982	,	1.4545	,	1.7262	,	1.4877	,	1.2638	,	1.2287	,	1.2233
RayTracingGpu	,	0.466528	,	0.832736	,	0.99104	,	0.985344	,	0.890496	,	0.86528	,	0.870816	,	0.926304	,	1.03536	,	1.51578	,	1.58675	,	1.1296	,	1.00125	,	1.00307	,	1.15613	,	1.6215	,	1.69866	,	1.81424	,	2.10205	,	1.49402	,	1.2353	,	0.929376	,	0.881056	,	0.925088
DeferredRLGpu	,	0.147936	,	0.301248	,	0.395296	,	0.368512	,	0.27264	,	0.213344	,	0.184704	,	0.179904	,	0.24656	,	0.371392	,	0.427648	,	0.318976	,	0.221184	,	0.223456	,	0.343488	,	0.723776	,	0.816448	,	0.913056	,	1.13139	,	0.74144	,	0.55024	,	0.297824	,	0.219808	,	0.214496
RayTracingRLGpu	,	0.186528	,	0.308416	,	0.344	,	0.371456	,	0.39488	,	0.4304	,	0.465024	,	0.509728	,	0.530656	,	0.537824	,	0.541248	,	0.536096	,	0.52944	,	0.529856	,	0.536032	,	0.552128	,	0.513056	,	0.493312	,	0.467712	,	0.398432	,	0.39744	,	0.405248	,	0.441504	,	0.4808
ResolveRLGpu	,	0.13056	,	0.22128	,	0.24976	,	0.243104	,	0.220928	,	0.220032	,	0.21936	,	0.234368	,	0.256128	,	0.604544	,	0.616096	,	0.272992	,	0.248128	,	0.247744	,	0.274816	,	0.343616	,	0.36736	,	0.405344	,	0.500896	,	0.352352	,	0.285312	,	0.224288	,	0.217696	,	0.227744
Index	,	0	,	1	,	2	,	3	,	4	,	5	,	6	,	7	,	8	,	9	,	10	,	11	,	12	,	13	,	14	,	15	,	16	,	17	,	18	,	19	,	20	,	21	,	22	,	23

ProfilingBuildAggregator: 
BuildBottomLevelAS	,	0.928
BuildBottomLevelASGpu	,	0.358752
BuildTopLevelAS	,	0.9415
BuildTopLevelASGpu	,	0.084128
Duplication	,	1
Triangles	,	3936
Meshes	,	1
BottomLevels	,	1
TopLevelSize	,	64
BottomLevelsSize	,	224128
Index	,	0

FpsAggregator: 
FPS	,	276	,	292	,	254	,	236	,	245	,	265	,	267	,	251	,	241	,	226	,	212	,	217	,	231	,	235	,	230	,	203	,	191	,	189	,	175	,	187	,	211	,	234	,	251	,	254
UPS	,	59	,	60	,	61	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	61	,	60	,	60	,	60	,	60	,	61	,	60	,	60	,	60	,	61	,	60	,	60	,	60
FrameTime	,	3.62426	,	3.43368	,	3.95025	,	4.2504	,	4.09239	,	3.7776	,	3.74809	,	3.98647	,	4.15947	,	4.43964	,	4.72468	,	4.61896	,	4.34708	,	4.26365	,	4.36129	,	4.95338	,	5.26499	,	5.31285	,	5.72285	,	5.36646	,	4.75687	,	4.27981	,	3.98709	,	3.9407
GigaRays/s	,	2.00745	,	2.11887	,	1.84179	,	1.71173	,	1.77782	,	1.92596	,	1.94113	,	1.82505	,	1.74915	,	1.63876	,	1.5399	,	1.57514	,	1.67366	,	1.70641	,	1.66821	,	1.4688	,	1.38187	,	1.36942	,	1.27131	,	1.35574	,	1.52948	,	1.69996	,	1.82477	,	1.84625
Index	,	0	,	1	,	2	,	3	,	4	,	5	,	6	,	7	,	8	,	9	,	10	,	11	,	12	,	13	,	14	,	15	,	16	,	17	,	18	,	19	,	20	,	21	,	22	,	23


AccelerationStructure: 
Ray tracing acceleration structure building statistics: 
	Bottom Level Acceleration Structure: 
		Maximum [B]: 224128
		Minimum [B]: 224128
		Currently Allocated [B]: 224128
		Currently Created [num]: 1
		Current Average [B]: 224128
		Maximum Triangles [num]: 3936
		Minimum Triangles [num]: 3936
		Total Triangles [num]: 3936
		Maximum Meshes [num]: 1
		Minimum Meshes [num]: 1
		Total Meshes [num]: 1
	Top Level Acceleration Structure: 
		Maximum [B]: 64
		Minimum [B]: 64
		Currently Allocated [B]: 64
		Total Allocated [B]: 64
		Total Created [num]: 1
		Average Allocated [B]: 64
		Maximum Instances [num]: 1
		Minimum Instances [num]: 1
		Total Instances [num]: 1
	Scratch Buffer: 
		Maximum [B]: 34304
		Minimum [B]: 34304
		Currently Allocated [B]: 34304
		Total Allocated [B]: 34304
		Total Created [num]: 1
		Average Allocated [B]: 34304
ProfilingStack: 
[PROF] Profiling statistics: 
	Scopes entered : 62628
	Scopes exited : 62627
	Overhead per scope [ticks] : 1023.2
	Threads entered : 3
	Threads exited : 0
	============================
		         Sum[Mt]   Parent[%]  Samples[num]       Avg[Mt]   Last/Curr[Mt]        Self[Mt]     Self[%] 	 Name

	main:

		   24712.956 Mt   100.000 %         1 x    24712.958 Mt    24712.956 Mt        0.763 Mt     0.003 % 	 /

		   24712.238 Mt    99.997 %         1 x    24712.240 Mt    24712.238 Mt      352.664 Mt     1.427 % 	 ./Main application loop

		       2.884 Mt     0.012 %         1 x        2.884 Mt        0.000 Mt        2.884 Mt   100.000 % 	 ../TexturedRLInitialization

		       2.388 Mt     0.010 %         1 x        2.388 Mt        0.000 Mt        2.388 Mt   100.000 % 	 ../ShadowMapRLInitialization

		       4.700 Mt     0.019 %         1 x        4.700 Mt        0.000 Mt        4.700 Mt   100.000 % 	 ../AmbientOcclusionRLInitialization

		       5.583 Mt     0.023 %         1 x        5.583 Mt        0.000 Mt        5.583 Mt   100.000 % 	 ../RasterResolveRLInitialization

		     229.280 Mt     0.928 %         1 x      229.280 Mt        0.000 Mt        3.630 Mt     1.583 % 	 ../Initialization

		     225.650 Mt    98.417 %         1 x      225.650 Mt        0.000 Mt        1.197 Mt     0.530 % 	 .../ScenePrep

		     192.896 Mt    85.484 %         1 x      192.896 Mt        0.000 Mt       16.567 Mt     8.588 % 	 ..../SceneLoad

		     163.452 Mt    84.736 %         1 x      163.452 Mt        0.000 Mt       19.338 Mt    11.831 % 	 ...../glTF-LoadScene

		     144.115 Mt    88.169 %         2 x       72.057 Mt        0.000 Mt      144.115 Mt   100.000 % 	 ....../glTF-LoadImageData

		       7.094 Mt     3.677 %         1 x        7.094 Mt        0.000 Mt        7.094 Mt   100.000 % 	 ...../glTF-CreateScene

		       5.783 Mt     2.998 %         1 x        5.783 Mt        0.000 Mt        5.783 Mt   100.000 % 	 ...../SceneDesc-CreateScene

		       0.423 Mt     0.188 %         1 x        0.423 Mt        0.000 Mt        0.005 Mt     1.111 % 	 ..../ShadowMapRLPrep

		       0.418 Mt    98.889 %         1 x        0.418 Mt        0.000 Mt        0.358 Mt    85.520 % 	 ...../PrepareForRendering

		       0.061 Mt    14.480 %         1 x        0.061 Mt        0.000 Mt        0.061 Mt   100.000 % 	 ....../PrepareDrawBundle

		       1.019 Mt     0.452 %         1 x        1.019 Mt        0.000 Mt        0.006 Mt     0.559 % 	 ..../TexturedRLPrep

		       1.014 Mt    99.441 %         1 x        1.014 Mt        0.000 Mt        0.332 Mt    32.708 % 	 ...../PrepareForRendering

		       0.423 Mt    41.756 %         1 x        0.423 Mt        0.000 Mt        0.423 Mt   100.000 % 	 ....../PrepareMaterials

		       0.259 Mt    25.535 %         1 x        0.259 Mt        0.000 Mt        0.259 Mt   100.000 % 	 ....../PrepareDrawBundle

		       3.943 Mt     1.748 %         1 x        3.943 Mt        0.000 Mt        0.004 Mt     0.114 % 	 ..../DeferredRLPrep

		       3.939 Mt    99.886 %         1 x        3.939 Mt        0.000 Mt        1.523 Mt    38.670 % 	 ...../PrepareForRendering

		       0.016 Mt     0.399 %         1 x        0.016 Mt        0.000 Mt        0.016 Mt   100.000 % 	 ....../PrepareMaterials

		       2.149 Mt    54.547 %         1 x        2.149 Mt        0.000 Mt        2.149 Mt   100.000 % 	 ....../BuildMaterialCache

		       0.252 Mt     6.385 %         1 x        0.252 Mt        0.000 Mt        0.252 Mt   100.000 % 	 ....../PrepareDrawBundle

		      13.996 Mt     6.202 %         1 x       13.996 Mt        0.000 Mt        3.778 Mt    26.991 % 	 ..../RayTracingRLPrep

		       0.235 Mt     1.681 %         1 x        0.235 Mt        0.000 Mt        0.235 Mt   100.000 % 	 ...../PrepareAccelerationStructures

		       0.024 Mt     0.169 %         1 x        0.024 Mt        0.000 Mt        0.024 Mt   100.000 % 	 ...../PrepareCache

		       0.425 Mt     3.038 %         1 x        0.425 Mt        0.000 Mt        0.425 Mt   100.000 % 	 ...../BuildCache

		       8.011 Mt    57.235 %         1 x        8.011 Mt        0.000 Mt        0.004 Mt     0.046 % 	 ...../BuildAccelerationStructures

		       8.007 Mt    99.954 %         1 x        8.007 Mt        0.000 Mt        6.137 Mt    76.651 % 	 ....../AccelerationStructureBuild

		       0.928 Mt    11.590 %         1 x        0.928 Mt        0.000 Mt        0.928 Mt   100.000 % 	 ......./BuildBottomLevelAS

		       0.942 Mt    11.759 %         1 x        0.942 Mt        0.000 Mt        0.942 Mt   100.000 % 	 ......./BuildTopLevelAS

		       1.524 Mt    10.885 %         1 x        1.524 Mt        0.000 Mt        1.524 Mt   100.000 % 	 ...../BuildShaderTables

		      12.176 Mt     5.396 %         1 x       12.176 Mt        0.000 Mt       12.176 Mt   100.000 % 	 ..../GpuSidePrep

		       5.379 Mt     0.022 %         1 x        5.379 Mt        0.000 Mt        5.379 Mt   100.000 % 	 ../DeferredRLInitialization

		      49.747 Mt     0.201 %         1 x       49.747 Mt        0.000 Mt        2.063 Mt     4.146 % 	 ../RayTracingRLInitialization

		      47.685 Mt    95.854 %         1 x       47.685 Mt        0.000 Mt       47.685 Mt   100.000 % 	 .../PipelineBuild

		       5.143 Mt     0.021 %         1 x        5.143 Mt        0.000 Mt        5.143 Mt   100.000 % 	 ../ResolveRLInitialization

		   24054.470 Mt    97.338 %     10964 x        2.194 Mt        4.238 Mt      301.612 Mt     1.254 % 	 ../MainLoop

		    1254.927 Mt     5.217 %      1445 x        0.868 Mt        0.399 Mt     1250.838 Mt    99.674 % 	 .../Update

		       4.089 Mt     0.326 %         1 x        4.089 Mt        0.000 Mt        4.089 Mt   100.000 % 	 ..../GuiModelApply

		   22497.931 Mt    93.529 %      5575 x        4.036 Mt        3.322 Mt    13505.867 Mt    60.032 % 	 .../Render

		    8992.064 Mt    39.968 %      5575 x        1.613 Mt        1.325 Mt     8946.068 Mt    99.488 % 	 ..../RayTracing

		      28.295 Mt     0.315 %      5575 x        0.005 Mt        0.003 Mt       28.295 Mt   100.000 % 	 ...../DeferredRLPrep

		      17.701 Mt     0.197 %      5575 x        0.003 Mt        0.003 Mt       17.701 Mt   100.000 % 	 ...../RayTracingRLPrep

	WindowThread:

		   24708.225 Mt   100.000 %         1 x    24708.226 Mt    24708.224 Mt    24708.225 Mt   100.000 % 	 /

	GpuThread:

		    6407.827 Mt   100.000 %      5575 x        1.149 Mt        0.930 Mt      176.340 Mt     2.752 % 	 /

		       0.450 Mt     0.007 %         1 x        0.450 Mt        0.000 Mt        0.006 Mt     1.365 % 	 ./ScenePrep

		       0.444 Mt    98.635 %         1 x        0.444 Mt        0.000 Mt        0.001 Mt     0.274 % 	 ../AccelerationStructureBuild

		       0.359 Mt    80.783 %         1 x        0.359 Mt        0.000 Mt        0.359 Mt   100.000 % 	 .../BuildBottomLevelASGpu

		       0.084 Mt    18.944 %         1 x        0.084 Mt        0.000 Mt        0.084 Mt   100.000 % 	 .../BuildTopLevelASGpu

		    6152.847 Mt    96.021 %      5575 x        1.104 Mt        0.926 Mt       11.923 Mt     0.194 % 	 ./RayTracingGpu

		    2101.319 Mt    34.152 %      5575 x        0.377 Mt        0.215 Mt     2101.319 Mt   100.000 % 	 ../DeferredRLGpu

		    2519.678 Mt    40.951 %      5575 x        0.452 Mt        0.480 Mt     2519.678 Mt   100.000 % 	 ../RayTracingRLGpu

		    1519.927 Mt    24.703 %      5575 x        0.273 Mt        0.230 Mt     1519.927 Mt   100.000 % 	 ../ResolveRLGpu

		      78.189 Mt     1.220 %      5575 x        0.014 Mt        0.003 Mt       78.189 Mt   100.000 % 	 ./Present


	============================


