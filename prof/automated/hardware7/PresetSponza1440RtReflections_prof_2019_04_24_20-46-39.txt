Configuration: 
Command line parameters  = E:\programming\FIT-DIP\Masters-DXR-Main-git\build\Measurement\x64\Debug\Measurement.exe --scene Sponza/Sponza.gltf --profile-automation --profile-camera-track Sponza/SponzaPreset.trk --rt-mode --width 2560 --height 1440 --profile-output PresetSponza1440RtReflections --quality-preset Reflections 
Window width             = 2560
Window height            = 1421
Window title             = Direct3D App
Back-buffer count        = 3
Fill Triangles           = enabled
Triangles Font Clock-Wise= enabled
Fast Build AS            = disabled
Target FPS               = 0
Target UPS               = 60
Tearing                  = enabled
VSync                    = disabled
GUI rendering            = disabled
Debug layer              = enabled
GPU validation           = disabled
GPU profiling            = enabled
Render mode              = Ray Tracing
Loaded scene file        = Sponza/Sponza.gltf
Using testing scene      = disabled
Duplication              = 1
Duplication in cube      = disabled
Duplication offset       = 10
BLAS duplication         = disabled
Rays per pixel           = 2
Quality preset           = Reflections
Rasterization Quality    : 
  AO                     = disabled
  AO kernel size         = 1
  Shadows                = disabled
  Shadow map resolution  = 1
  Shadow PCF kernel size = 0
  Reflections            = enabled
Ray Tracing Quality      : 
  AO                     = disabled
  AO rays                = 0
  AO kernel size         = 4
  Shadows                = disabled
  Shadow rays            = 0
  Shadow kernel size     = 4
  Reflections            = enabled

ProfilingAggregator: 
Render	,	7.5549	,	6.7516	,	6.4121	,	5.4891	,	5.5405	,	6.563	,	6.3847	,	7.5005	,	6.518	,	6.6447	,	6.4708	,	6.5208	,	7.6257	,	6.278	,	5.9161	,	5.7435	,	6.9451	,	6.4584	,	6.1315	,	6.6846	,	6.8674	,	6.7417	,	6.0961
Update	,	1.0734	,	0.7696	,	0.8117	,	0.8513	,	0.9979	,	0.7225	,	0.6815	,	1.1586	,	0.4844	,	1.2026	,	0.5029	,	0.4813	,	0.8788	,	0.4769	,	0.777	,	0.5782	,	0.6605	,	0.6719	,	0.7965	,	0.9274	,	0.4844	,	0.9231	,	0.6761
RayTracing	,	3.2595	,	2.9916	,	2.942	,	2.4856	,	2.5035	,	2.964	,	2.7436	,	2.956	,	2.586	,	2.9697	,	2.935	,	2.5943	,	3.1479	,	2.4927	,	2.5157	,	2.5381	,	2.4992	,	2.5324	,	2.7462	,	2.9665	,	2.9509	,	2.9605	,	2.661
RayTracingGpu	,	2.63507	,	2.48467	,	2.27494	,	1.84	,	1.82256	,	2.28202	,	2.44819	,	3.22634	,	2.50694	,	2.32941	,	2.19443	,	2.45507	,	2.88499	,	2.59453	,	2.35034	,	1.98778	,	2.98499	,	2.71866	,	2.00003	,	2.27965	,	2.29584	,	2.29133	,	2.29981
DeferredRLGpu	,	1.49309	,	1.42086	,	1.26845	,	0.782624	,	0.813248	,	0.997152	,	1.20592	,	1.33178	,	1.02288	,	0.805824	,	0.716608	,	0.933728	,	1.0935	,	1.22154	,	1.23498	,	0.863264	,	1.06598	,	1.02525	,	0.873472	,	1.30685	,	1.3567	,	1.34909	,	1.35859
RayTracingRLGpu	,	0.692128	,	0.611808	,	0.563424	,	0.614752	,	0.577984	,	0.835904	,	0.789888	,	1.34352	,	1.03792	,	1.08336	,	1.04179	,	1.08294	,	1.32253	,	0.922144	,	0.66272	,	0.658496	,	1.44291	,	1.21645	,	0.661504	,	0.507328	,	0.486112	,	0.488288	,	0.486624
ResolveRLGpu	,	0.448832	,	0.450976	,	0.441952	,	0.441408	,	0.430336	,	0.447776	,	0.451136	,	0.550144	,	0.445056	,	0.439136	,	0.434592	,	0.437056	,	0.467872	,	0.448384	,	0.45184	,	0.465216	,	0.475008	,	0.475616	,	0.464128	,	0.464128	,	0.45168	,	0.452768	,	0.453408
Index	,	0	,	1	,	2	,	3	,	4	,	5	,	6	,	7	,	8	,	9	,	10	,	11	,	12	,	13	,	14	,	15	,	16	,	17	,	18	,	19	,	20	,	21	,	22

ProfilingBuildAggregator: 
BuildBottomLevelAS	,	4.4734
BuildBottomLevelASGpu	,	15.6817
BuildTopLevelAS	,	2.2307
BuildTopLevelASGpu	,	0.623968
Duplication	,	1
Triangles	,	262267
Meshes	,	103
BottomLevels	,	1
TopLevelSize	,	64
BottomLevelsSize	,	14623872
Index	,	0

FpsAggregator: 
FPS	,	125	,	142	,	149	,	154	,	163	,	151	,	154	,	140	,	143	,	146	,	151	,	148	,	144	,	141	,	142	,	158	,	143	,	135	,	148	,	158	,	147	,	150	,	149
UPS	,	59	,	60	,	60	,	61	,	60	,	60	,	60	,	60	,	60	,	61	,	60	,	60	,	60	,	61	,	60	,	60	,	60	,	61	,	60	,	60	,	60	,	60	,	61
FrameTime	,	8.01141	,	7.0498	,	6.73515	,	6.50169	,	6.15057	,	6.63831	,	6.49641	,	7.18789	,	7.00671	,	6.90018	,	6.63481	,	6.77153	,	6.98459	,	7.13215	,	7.06982	,	6.35147	,	7.02204	,	7.45259	,	6.79683	,	6.33177	,	6.80893	,	6.69185	,	6.71824
GigaRays/s	,	0.908145	,	1.03202	,	1.08023	,	1.11902	,	1.1829	,	1.09599	,	1.11993	,	1.01219	,	1.03837	,	1.0544	,	1.09657	,	1.07443	,	1.04165	,	1.0201	,	1.0291	,	1.14549	,	1.0361	,	0.976241	,	1.07043	,	1.14905	,	1.06853	,	1.08722	,	1.08295
Index	,	0	,	1	,	2	,	3	,	4	,	5	,	6	,	7	,	8	,	9	,	10	,	11	,	12	,	13	,	14	,	15	,	16	,	17	,	18	,	19	,	20	,	21	,	22


AccelerationStructure: 
Ray tracing acceleration structure building statistics: 
	Bottom Level Acceleration Structure: 
		Maximum [B]: 14623872
		Minimum [B]: 14623872
		Currently Allocated [B]: 14623872
		Currently Created [num]: 1
		Current Average [B]: 14623872
		Maximum Triangles [num]: 262267
		Minimum Triangles [num]: 262267
		Total Triangles [num]: 262267
		Maximum Meshes [num]: 103
		Minimum Meshes [num]: 103
		Total Meshes [num]: 103
	Top Level Acceleration Structure: 
		Maximum [B]: 64
		Minimum [B]: 64
		Currently Allocated [B]: 64
		Total Allocated [B]: 64
		Total Created [num]: 1
		Average Allocated [B]: 64
		Maximum Instances [num]: 1
		Minimum Instances [num]: 1
		Total Instances [num]: 1
	Scratch Buffer: 
		Maximum [B]: 11660800
		Minimum [B]: 11660800
		Currently Allocated [B]: 11660800
		Total Allocated [B]: 11660800
		Total Created [num]: 1
		Average Allocated [B]: 11660800
ProfilingStack: 
[PROF] Profiling statistics: 
	Scopes entered : 40296
	Scopes exited : 40295
	Overhead per scope [ticks] : 1006.5
	Threads entered : 3
	Threads exited : 0
	============================
		         Sum[Mt]   Parent[%]  Samples[num]       Avg[Mt]   Last/Curr[Mt]        Self[Mt]     Self[%] 	 Name

	main:

		   32633.609 Mt   100.000 %         1 x    32633.612 Mt    32633.608 Mt        0.689 Mt     0.002 % 	 /

		   32633.034 Mt    99.998 %         1 x    32633.040 Mt    32633.034 Mt      349.014 Mt     1.070 % 	 ./Main application loop

		       2.860 Mt     0.009 %         1 x        2.860 Mt        0.000 Mt        2.860 Mt   100.000 % 	 ../TexturedRLInitialization

		       2.536 Mt     0.008 %         1 x        2.536 Mt        0.000 Mt        2.536 Mt   100.000 % 	 ../ShadowMapRLInitialization

		       4.686 Mt     0.014 %         1 x        4.686 Mt        0.000 Mt        4.686 Mt   100.000 % 	 ../AmbientOcclusionRLInitialization

		       6.017 Mt     0.018 %         1 x        6.017 Mt        0.000 Mt        6.017 Mt   100.000 % 	 ../RasterResolveRLInitialization

		    9133.205 Mt    27.988 %         1 x     9133.205 Mt        0.000 Mt       12.162 Mt     0.133 % 	 ../Initialization

		    9121.043 Mt    99.867 %         1 x     9121.043 Mt        0.000 Mt        9.863 Mt     0.108 % 	 .../ScenePrep

		    9000.751 Mt    98.681 %         1 x     9000.751 Mt        0.000 Mt      328.345 Mt     3.648 % 	 ..../SceneLoad

		    8267.975 Mt    91.859 %         1 x     8267.975 Mt        0.000 Mt     1113.800 Mt    13.471 % 	 ...../glTF-LoadScene

		    7154.175 Mt    86.529 %        69 x      103.684 Mt        0.000 Mt     7154.175 Mt   100.000 % 	 ....../glTF-LoadImageData

		     291.634 Mt     3.240 %         1 x      291.634 Mt        0.000 Mt      291.634 Mt   100.000 % 	 ...../glTF-CreateScene

		     112.796 Mt     1.253 %         1 x      112.796 Mt        0.000 Mt      112.796 Mt   100.000 % 	 ...../SceneDesc-CreateScene

		       2.648 Mt     0.029 %         1 x        2.648 Mt        0.000 Mt        0.005 Mt     0.204 % 	 ..../ShadowMapRLPrep

		       2.642 Mt    99.796 %         1 x        2.642 Mt        0.000 Mt        1.917 Mt    72.550 % 	 ...../PrepareForRendering

		       0.725 Mt    27.450 %         1 x        0.725 Mt        0.000 Mt        0.725 Mt   100.000 % 	 ....../PrepareDrawBundle

		       6.064 Mt     0.066 %         1 x        6.064 Mt        0.000 Mt        0.005 Mt     0.084 % 	 ..../TexturedRLPrep

		       6.059 Mt    99.916 %         1 x        6.059 Mt        0.000 Mt        1.109 Mt    18.297 % 	 ...../PrepareForRendering

		       2.674 Mt    44.136 %         1 x        2.674 Mt        0.000 Mt        2.674 Mt   100.000 % 	 ....../PrepareMaterials

		       2.276 Mt    37.566 %         1 x        2.276 Mt        0.000 Mt        2.276 Mt   100.000 % 	 ....../PrepareDrawBundle

		       8.795 Mt     0.096 %         1 x        8.795 Mt        0.000 Mt        0.005 Mt     0.057 % 	 ..../DeferredRLPrep

		       8.790 Mt    99.943 %         1 x        8.790 Mt        0.000 Mt        3.909 Mt    44.466 % 	 ...../PrepareForRendering

		       0.019 Mt     0.214 %         1 x        0.019 Mt        0.000 Mt        0.019 Mt   100.000 % 	 ....../PrepareMaterials

		       2.224 Mt    25.297 %         1 x        2.224 Mt        0.000 Mt        2.224 Mt   100.000 % 	 ....../BuildMaterialCache

		       2.639 Mt    30.023 %         1 x        2.639 Mt        0.000 Mt        2.639 Mt   100.000 % 	 ....../PrepareDrawBundle

		      29.363 Mt     0.322 %         1 x       29.363 Mt        0.000 Mt        6.504 Mt    22.150 % 	 ..../RayTracingRLPrep

		       0.610 Mt     2.078 %         1 x        0.610 Mt        0.000 Mt        0.610 Mt   100.000 % 	 ...../PrepareAccelerationStructures

		       0.025 Mt     0.084 %         1 x        0.025 Mt        0.000 Mt        0.025 Mt   100.000 % 	 ...../PrepareCache

		       5.314 Mt    18.098 %         1 x        5.314 Mt        0.000 Mt        5.314 Mt   100.000 % 	 ...../BuildCache

		      12.580 Mt    42.843 %         1 x       12.580 Mt        0.000 Mt        0.004 Mt     0.033 % 	 ...../BuildAccelerationStructures

		      12.576 Mt    99.967 %         1 x       12.576 Mt        0.000 Mt        5.872 Mt    46.690 % 	 ....../AccelerationStructureBuild

		       4.473 Mt    35.572 %         1 x        4.473 Mt        0.000 Mt        4.473 Mt   100.000 % 	 ......./BuildBottomLevelAS

		       2.231 Mt    17.738 %         1 x        2.231 Mt        0.000 Mt        2.231 Mt   100.000 % 	 ......./BuildTopLevelAS

		       4.330 Mt    14.747 %         1 x        4.330 Mt        0.000 Mt        4.330 Mt   100.000 % 	 ...../BuildShaderTables

		      63.559 Mt     0.697 %         1 x       63.559 Mt        0.000 Mt       63.559 Mt   100.000 % 	 ..../GpuSidePrep

		       5.151 Mt     0.016 %         1 x        5.151 Mt        0.000 Mt        5.151 Mt   100.000 % 	 ../DeferredRLInitialization

		      51.755 Mt     0.159 %         1 x       51.755 Mt        0.000 Mt        2.063 Mt     3.986 % 	 ../RayTracingRLInitialization

		      49.692 Mt    96.014 %         1 x       49.692 Mt        0.000 Mt       49.692 Mt   100.000 % 	 .../PipelineBuild

		       4.962 Mt     0.015 %         1 x        4.962 Mt        0.000 Mt        4.962 Mt   100.000 % 	 ../ResolveRLInitialization

		   23072.849 Mt    70.704 %      8353 x        2.762 Mt        7.158 Mt      160.958 Mt     0.698 % 	 ../MainLoop

		    1079.723 Mt     4.680 %      1385 x        0.780 Mt        0.515 Mt     1078.320 Mt    99.870 % 	 .../Update

		       1.403 Mt     0.130 %         1 x        1.403 Mt        0.000 Mt        1.403 Mt   100.000 % 	 ..../GuiModelApply

		   21832.168 Mt    94.623 %      3383 x        6.453 Mt        6.635 Mt    12605.354 Mt    57.738 % 	 .../Render

		    9226.814 Mt    42.262 %      3383 x        2.727 Mt        2.868 Mt     9201.691 Mt    99.728 % 	 ..../RayTracing

		      14.433 Mt     0.156 %      3383 x        0.004 Mt        0.003 Mt       14.433 Mt   100.000 % 	 ...../DeferredRLPrep

		      10.689 Mt     0.116 %      3383 x        0.003 Mt        0.004 Mt       10.689 Mt   100.000 % 	 ...../RayTracingRLPrep

	WindowThread:

		   32631.666 Mt   100.000 %         1 x    32631.667 Mt    32631.665 Mt    32631.666 Mt   100.000 % 	 /

	GpuThread:

		    8301.563 Mt   100.000 %      3383 x        2.454 Mt        2.306 Mt      182.220 Mt     2.195 % 	 /

		      16.359 Mt     0.197 %         1 x       16.359 Mt        0.000 Mt        0.050 Mt     0.308 % 	 ./ScenePrep

		      16.309 Mt    99.692 %         1 x       16.309 Mt        0.000 Mt        0.003 Mt     0.020 % 	 ../AccelerationStructureBuild

		      15.682 Mt    96.154 %         1 x       15.682 Mt        0.000 Mt       15.682 Mt   100.000 % 	 .../BuildBottomLevelASGpu

		       0.624 Mt     3.826 %         1 x        0.624 Mt        0.000 Mt        0.624 Mt   100.000 % 	 .../BuildTopLevelASGpu

		    8079.567 Mt    97.326 %      3383 x        2.388 Mt        2.303 Mt        4.724 Mt     0.058 % 	 ./RayTracingGpu

		    3744.323 Mt    46.343 %      3383 x        1.107 Mt        1.359 Mt     3744.323 Mt   100.000 % 	 ../DeferredRLGpu

		    2789.604 Mt    34.527 %      3383 x        0.825 Mt        0.489 Mt     2789.604 Mt   100.000 % 	 ../RayTracingRLGpu

		    1540.916 Mt    19.072 %      3383 x        0.455 Mt        0.452 Mt     1540.916 Mt   100.000 % 	 ../ResolveRLGpu

		      23.418 Mt     0.282 %      3383 x        0.007 Mt        0.002 Mt       23.418 Mt   100.000 % 	 ./Present


	============================


