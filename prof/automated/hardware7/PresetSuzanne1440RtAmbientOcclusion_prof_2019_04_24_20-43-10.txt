Configuration: 
Command line parameters  = E:\programming\FIT-DIP\Masters-DXR-Main-git\build\Measurement\x64\Debug\Measurement.exe --scene Suzanne/Suzanne.gltf --profile-automation --profile-camera-track Suzanne/Suzanne.trk --rt-mode --width 2560 --height 1440 --profile-output PresetSuzanne1440RtAmbientOcclusion --quality-preset AmbientOcclusion 
Window width             = 2560
Window height            = 1421
Window title             = Direct3D App
Back-buffer count        = 3
Fill Triangles           = enabled
Triangles Font Clock-Wise= enabled
Fast Build AS            = disabled
Target FPS               = 0
Target UPS               = 60
Tearing                  = enabled
VSync                    = disabled
GUI rendering            = disabled
Debug layer              = enabled
GPU validation           = disabled
GPU profiling            = enabled
Render mode              = Ray Tracing
Loaded scene file        = Suzanne/Suzanne.gltf
Using testing scene      = disabled
Duplication              = 1
Duplication in cube      = disabled
Duplication offset       = 10
BLAS duplication         = disabled
Rays per pixel           = 5
Quality preset           = AmbientOcclusion
Rasterization Quality    : 
  AO                     = enabled
  AO kernel size         = 4
  Shadows                = disabled
  Shadow map resolution  = 1
  Shadow PCF kernel size = 0
  Reflections            = disabled
Ray Tracing Quality      : 
  AO                     = enabled
  AO rays                = 4
  AO kernel size         = 4
  Shadows                = disabled
  Shadow rays            = 0
  Shadow kernel size     = 4
  Reflections            = disabled

ProfilingAggregator: 
Render	,	4.7323	,	4.9052	,	4.0915	,	4.4172	,	4.6543	,	4.9222	,	4.1986	,	3.9969	,	4.7167	,	4.1855	,	6.6252	,	5.0967	,	4.1214	,	4.0474	,	4.2947	,	4.5814	,	6.7268	,	7.0645	,	8.1116	,	7.3043	,	4.3462	,	3.356	,	5.2132	,	3.3413
Update	,	1.0649	,	1.0711	,	0.9355	,	0.8423	,	1.0365	,	1.2477	,	1.0215	,	0.4811	,	0.7694	,	0.8966	,	0.4795	,	1.1258	,	0.4766	,	0.4818	,	1.1609	,	0.974	,	1.2653	,	1.0683	,	1.2705	,	0.6618	,	0.4851	,	0.5109	,	0.9969	,	0.483
RayTracing	,	1.686	,	1.4447	,	1.2811	,	1.5182	,	1.9578	,	1.7881	,	1.3944	,	1.2775	,	1.6112	,	1.2582	,	2.8156	,	1.916	,	1.4366	,	1.245	,	1.2264	,	1.2409	,	2.8272	,	2.9579	,	3.1118	,	3.0741	,	1.3754	,	1.249	,	2.2029	,	1.2255
RayTracingGpu	,	1.69923	,	1.97962	,	1.86621	,	1.70262	,	1.50675	,	1.604	,	1.3952	,	1.50038	,	1.66742	,	1.83059	,	1.88326	,	1.66845	,	1.51654	,	1.59677	,	1.87632	,	2.10221	,	2.01597	,	2.21376	,	2.88624	,	2.15568	,	1.76627	,	1.24035	,	1.22886	,	1.27283
DeferredRLGpu	,	0.149408	,	0.203616	,	0.204352	,	0.168096	,	0.118528	,	0.088128	,	0.071648	,	0.069472	,	0.098528	,	0.141664	,	0.152864	,	0.115392	,	0.080672	,	0.087488	,	0.142752	,	0.241408	,	0.242784	,	0.26464	,	0.391264	,	0.313184	,	0.2392	,	0.124288	,	0.080128	,	0.073312
RayTracingRLGpu	,	0.471232	,	0.671584	,	0.68416	,	0.574432	,	0.398912	,	0.288992	,	0.25824	,	0.255296	,	0.33312	,	0.472768	,	0.539488	,	0.399328	,	0.281408	,	0.297216	,	0.434464	,	0.713952	,	0.789696	,	1.02038	,	1.55626	,	0.964352	,	0.682592	,	0.326432	,	0.25712	,	0.251072
ResolveRLGpu	,	1.07747	,	1.10269	,	0.976384	,	0.958976	,	0.987904	,	1.22528	,	1.06358	,	1.17373	,	1.23424	,	1.2145	,	1.18925	,	1.15213	,	1.15261	,	1.2105	,	1.29754	,	1.14512	,	0.9824	,	0.927584	,	0.937536	,	0.876864	,	0.843424	,	0.788576	,	0.890368	,	0.947264
Index	,	0	,	1	,	2	,	3	,	4	,	5	,	6	,	7	,	8	,	9	,	10	,	11	,	12	,	13	,	14	,	15	,	16	,	17	,	18	,	19	,	20	,	21	,	22	,	23

ProfilingBuildAggregator: 
BuildBottomLevelAS	,	0.9257
BuildBottomLevelASGpu	,	0.358496
BuildTopLevelAS	,	1.0035
BuildTopLevelASGpu	,	0.082784
Duplication	,	1
Triangles	,	3936
Meshes	,	1
BottomLevels	,	1
TopLevelSize	,	64
BottomLevelsSize	,	224128
Index	,	0

FpsAggregator: 
FPS	,	167	,	183	,	183	,	187	,	194	,	212	,	207	,	198	,	201	,	194	,	187	,	193	,	205	,	200	,	193	,	179	,	175	,	173	,	172	,	172	,	176	,	208	,	231	,	241
UPS	,	59	,	60	,	61	,	60	,	60	,	60	,	60	,	61	,	60	,	60	,	60	,	60	,	60	,	61	,	60	,	60	,	60	,	60	,	61	,	60	,	60	,	60	,	60	,	61
FrameTime	,	5.98933	,	5.47981	,	5.48496	,	5.36912	,	5.17638	,	4.73087	,	4.83881	,	5.06805	,	4.98142	,	5.16531	,	5.37392	,	5.20191	,	4.88291	,	5.01908	,	5.18615	,	5.6066	,	5.75063	,	5.81028	,	5.85121	,	5.83042	,	5.68647	,	4.81477	,	4.34606	,	4.15966
GigaRays/s	,	3.03687	,	3.31924	,	3.31612	,	3.38767	,	3.5138	,	3.8447	,	3.75894	,	3.58892	,	3.65133	,	3.52134	,	3.38464	,	3.49656	,	3.72499	,	3.62393	,	3.50719	,	3.24418	,	3.16292	,	3.13045	,	3.10855	,	3.11964	,	3.19861	,	3.77771	,	4.18513	,	4.37267
Index	,	0	,	1	,	2	,	3	,	4	,	5	,	6	,	7	,	8	,	9	,	10	,	11	,	12	,	13	,	14	,	15	,	16	,	17	,	18	,	19	,	20	,	21	,	22	,	23


AccelerationStructure: 
Ray tracing acceleration structure building statistics: 
	Bottom Level Acceleration Structure: 
		Maximum [B]: 224128
		Minimum [B]: 224128
		Currently Allocated [B]: 224128
		Currently Created [num]: 1
		Current Average [B]: 224128
		Maximum Triangles [num]: 3936
		Minimum Triangles [num]: 3936
		Total Triangles [num]: 3936
		Maximum Meshes [num]: 1
		Minimum Meshes [num]: 1
		Total Meshes [num]: 1
	Top Level Acceleration Structure: 
		Maximum [B]: 64
		Minimum [B]: 64
		Currently Allocated [B]: 64
		Total Allocated [B]: 64
		Total Created [num]: 1
		Average Allocated [B]: 64
		Maximum Instances [num]: 1
		Minimum Instances [num]: 1
		Total Instances [num]: 1
	Scratch Buffer: 
		Maximum [B]: 34304
		Minimum [B]: 34304
		Currently Allocated [B]: 34304
		Total Allocated [B]: 34304
		Total Created [num]: 1
		Average Allocated [B]: 34304
ProfilingStack: 
[PROF] Profiling statistics: 
	Scopes entered : 49327
	Scopes exited : 49326
	Overhead per scope [ticks] : 1004.1
	Threads entered : 3
	Threads exited : 0
	============================
		         Sum[Mt]   Parent[%]  Samples[num]       Avg[Mt]   Last/Curr[Mt]        Self[Mt]     Self[%] 	 Name

	main:

		   24734.612 Mt   100.000 %         1 x    24734.613 Mt    24734.611 Mt        0.708 Mt     0.003 % 	 /

		   24733.947 Mt    99.997 %         1 x    24733.950 Mt    24733.947 Mt      352.278 Mt     1.424 % 	 ./Main application loop

		       2.984 Mt     0.012 %         1 x        2.984 Mt        0.000 Mt        2.984 Mt   100.000 % 	 ../TexturedRLInitialization

		       2.436 Mt     0.010 %         1 x        2.436 Mt        0.000 Mt        2.436 Mt   100.000 % 	 ../ShadowMapRLInitialization

		       4.110 Mt     0.017 %         1 x        4.110 Mt        0.000 Mt        4.110 Mt   100.000 % 	 ../AmbientOcclusionRLInitialization

		       6.382 Mt     0.026 %         1 x        6.382 Mt        0.000 Mt        6.382 Mt   100.000 % 	 ../RasterResolveRLInitialization

		     230.977 Mt     0.934 %         1 x      230.977 Mt        0.000 Mt        3.607 Mt     1.561 % 	 ../Initialization

		     227.370 Mt    98.439 %         1 x      227.370 Mt        0.000 Mt        1.238 Mt     0.545 % 	 .../ScenePrep

		     194.224 Mt    85.422 %         1 x      194.224 Mt        0.000 Mt       16.789 Mt     8.644 % 	 ..../SceneLoad

		     164.197 Mt    84.540 %         1 x      164.197 Mt        0.000 Mt       19.351 Mt    11.785 % 	 ...../glTF-LoadScene

		     144.846 Mt    88.215 %         2 x       72.423 Mt        0.000 Mt      144.846 Mt   100.000 % 	 ....../glTF-LoadImageData

		       7.627 Mt     3.927 %         1 x        7.627 Mt        0.000 Mt        7.627 Mt   100.000 % 	 ...../glTF-CreateScene

		       5.612 Mt     2.889 %         1 x        5.612 Mt        0.000 Mt        5.612 Mt   100.000 % 	 ...../SceneDesc-CreateScene

		       0.567 Mt     0.250 %         1 x        0.567 Mt        0.000 Mt        0.005 Mt     0.828 % 	 ..../ShadowMapRLPrep

		       0.563 Mt    99.172 %         1 x        0.563 Mt        0.000 Mt        0.501 Mt    89.104 % 	 ...../PrepareForRendering

		       0.061 Mt    10.896 %         1 x        0.061 Mt        0.000 Mt        0.061 Mt   100.000 % 	 ....../PrepareDrawBundle

		       1.060 Mt     0.466 %         1 x        1.060 Mt        0.000 Mt        0.005 Mt     0.425 % 	 ..../TexturedRLPrep

		       1.055 Mt    99.575 %         1 x        1.055 Mt        0.000 Mt        0.345 Mt    32.705 % 	 ...../PrepareForRendering

		       0.452 Mt    42.807 %         1 x        0.452 Mt        0.000 Mt        0.452 Mt   100.000 % 	 ....../PrepareMaterials

		       0.258 Mt    24.488 %         1 x        0.258 Mt        0.000 Mt        0.258 Mt   100.000 % 	 ....../PrepareDrawBundle

		       3.791 Mt     1.667 %         1 x        3.791 Mt        0.000 Mt        0.005 Mt     0.119 % 	 ..../DeferredRLPrep

		       3.786 Mt    99.881 %         1 x        3.786 Mt        0.000 Mt        1.763 Mt    46.570 % 	 ...../PrepareForRendering

		       0.016 Mt     0.428 %         1 x        0.016 Mt        0.000 Mt        0.016 Mt   100.000 % 	 ....../PrepareMaterials

		       1.752 Mt    46.264 %         1 x        1.752 Mt        0.000 Mt        1.752 Mt   100.000 % 	 ....../BuildMaterialCache

		       0.255 Mt     6.738 %         1 x        0.255 Mt        0.000 Mt        0.255 Mt   100.000 % 	 ....../PrepareDrawBundle

		      14.285 Mt     6.283 %         1 x       14.285 Mt        0.000 Mt        3.783 Mt    26.483 % 	 ..../RayTracingRLPrep

		       0.244 Mt     1.709 %         1 x        0.244 Mt        0.000 Mt        0.244 Mt   100.000 % 	 ...../PrepareAccelerationStructures

		       0.025 Mt     0.173 %         1 x        0.025 Mt        0.000 Mt        0.025 Mt   100.000 % 	 ...../PrepareCache

		       0.424 Mt     2.969 %         1 x        0.424 Mt        0.000 Mt        0.424 Mt   100.000 % 	 ...../BuildCache

		       8.282 Mt    57.977 %         1 x        8.282 Mt        0.000 Mt        0.004 Mt     0.045 % 	 ...../BuildAccelerationStructures

		       8.278 Mt    99.955 %         1 x        8.278 Mt        0.000 Mt        6.349 Mt    76.696 % 	 ....../AccelerationStructureBuild

		       0.926 Mt    11.182 %         1 x        0.926 Mt        0.000 Mt        0.926 Mt   100.000 % 	 ......./BuildBottomLevelAS

		       1.004 Mt    12.122 %         1 x        1.004 Mt        0.000 Mt        1.004 Mt   100.000 % 	 ......./BuildTopLevelAS

		       1.527 Mt    10.689 %         1 x        1.527 Mt        0.000 Mt        1.527 Mt   100.000 % 	 ...../BuildShaderTables

		      12.205 Mt     5.368 %         1 x       12.205 Mt        0.000 Mt       12.205 Mt   100.000 % 	 ..../GpuSidePrep

		       5.290 Mt     0.021 %         1 x        5.290 Mt        0.000 Mt        5.290 Mt   100.000 % 	 ../DeferredRLInitialization

		      49.929 Mt     0.202 %         1 x       49.929 Mt        0.000 Mt        2.091 Mt     4.188 % 	 ../RayTracingRLInitialization

		      47.838 Mt    95.812 %         1 x       47.838 Mt        0.000 Mt       47.838 Mt   100.000 % 	 .../PipelineBuild

		       5.182 Mt     0.021 %         1 x        5.182 Mt        0.000 Mt        5.182 Mt   100.000 % 	 ../ResolveRLInitialization

		   24074.379 Mt    97.333 %      6141 x        3.920 Mt        9.893 Mt      287.813 Mt     1.196 % 	 ../MainLoop

		    1326.253 Mt     5.509 %      1445 x        0.918 Mt        1.691 Mt     1322.082 Mt    99.686 % 	 .../Update

		       4.171 Mt     0.314 %         1 x        4.171 Mt        0.000 Mt        4.171 Mt   100.000 % 	 ..../GuiModelApply

		   22460.313 Mt    93.296 %      4633 x        4.848 Mt        8.180 Mt    14345.974 Mt    63.873 % 	 .../Render

		    8114.340 Mt    36.127 %      4633 x        1.751 Mt        4.289 Mt     8074.219 Mt    99.506 % 	 ..../RayTracing

		      23.758 Mt     0.293 %      4633 x        0.005 Mt        0.009 Mt       23.758 Mt   100.000 % 	 ...../DeferredRLPrep

		      16.363 Mt     0.202 %      4633 x        0.004 Mt        0.009 Mt       16.363 Mt   100.000 % 	 ...../RayTracingRLPrep

	WindowThread:

		   24729.913 Mt   100.000 %         1 x    24729.914 Mt    24729.913 Mt    24729.913 Mt   100.000 % 	 /

	GpuThread:

		    8374.333 Mt   100.000 %      4633 x        1.808 Mt        1.283 Mt      178.292 Mt     2.129 % 	 /

		       0.449 Mt     0.005 %         1 x        0.449 Mt        0.000 Mt        0.006 Mt     1.355 % 	 ./ScenePrep

		       0.442 Mt    98.645 %         1 x        0.442 Mt        0.000 Mt        0.001 Mt     0.275 % 	 ../AccelerationStructureBuild

		       0.358 Mt    81.017 %         1 x        0.358 Mt        0.000 Mt        0.358 Mt   100.000 % 	 .../BuildBottomLevelASGpu

		       0.083 Mt    18.708 %         1 x        0.083 Mt        0.000 Mt        0.083 Mt   100.000 % 	 .../BuildTopLevelASGpu

		    8125.985 Mt    97.034 %      4633 x        1.754 Mt        1.280 Mt        6.708 Mt     0.083 % 	 ./RayTracingGpu

		     723.863 Mt     8.908 %      4633 x        0.156 Mt        0.076 Mt      723.863 Mt   100.000 % 	 ../DeferredRLGpu

		    2436.465 Mt    29.984 %      4633 x        0.526 Mt        0.256 Mt     2436.465 Mt   100.000 % 	 ../RayTracingRLGpu

		    4958.950 Mt    61.026 %      4633 x        1.070 Mt        0.946 Mt     4958.950 Mt   100.000 % 	 ../ResolveRLGpu

		      69.608 Mt     0.831 %      4633 x        0.015 Mt        0.002 Mt       69.608 Mt   100.000 % 	 ./Present


	============================


