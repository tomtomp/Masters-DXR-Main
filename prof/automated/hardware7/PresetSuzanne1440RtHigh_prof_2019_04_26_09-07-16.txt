Configuration: 
Command line parameters  = E:\programming\FIT-DIP\Masters-DXR-Main-git\build\Measurement\x64\Debug\Measurement.exe --scene Suzanne/Suzanne.gltf --profile-automation --profile-camera-track Suzanne/Suzanne.trk --rt-mode --width 2560 --height 1440 --profile-output PresetSuzanne1440RtHigh --quality-preset High 
Window width             = 2560
Window height            = 1421
Window title             = Direct3D App
Back-buffer count        = 3
Fill Triangles           = enabled
Triangles Font Clock-Wise= enabled
Fast Build AS            = disabled
Target FPS               = 0
Target UPS               = 60
Tearing                  = enabled
VSync                    = disabled
GUI rendering            = disabled
Debug layer              = enabled
GPU validation           = disabled
GPU profiling            = enabled
Render mode              = Ray Tracing
Loaded scene file        = Suzanne/Suzanne.gltf
Using testing scene      = disabled
Duplication              = 1
Duplication in cube      = disabled
Duplication offset       = 10
BLAS duplication         = disabled
Rays per pixel           = 14
Quality preset           = High
Rasterization Quality    : 
  AO                     = enabled
  AO kernel size         = 4
  Shadows                = enabled
  Shadow map resolution  = 4096
  Shadow PCF kernel size = 4
  Reflections            = enabled
Ray Tracing Quality      : 
  AO                     = enabled
  AO rays                = 4
  AO kernel size         = 4
  Shadows                = enabled
  Shadow rays            = 4
  Shadow kernel size     = 4
  Reflections            = enabled

ProfilingAggregator: 
Render	,	8.777	,	6.6241	,	9.0143	,	6.0039	,	7.0006	,	6.8286	,	5.8913	,	6.0434	,	8.3206	,	6.3969	,	7.1935	,	8.24	,	4.8491	,	4.8437	,	6.0255	,	7.8047	,	6.2601	,	7.5684	,	13.1648	,	12.1184	,	11.4142	,	5.5163	,	5.017	,	6.308
Update	,	1.3981	,	0.5673	,	1.2114	,	0.6983	,	1.1474	,	1.2352	,	1.3383	,	1.1405	,	0.5035	,	1.1163	,	1.1528	,	1.2841	,	0.802	,	0.5004	,	0.834	,	1.1718	,	0.5087	,	0.6677	,	1.5076	,	1.5423	,	0.5837	,	0.5159	,	0.5045	,	1.0079
RayTracing	,	3.3947	,	1.5059	,	3.1817	,	1.3245	,	2.3467	,	2.3154	,	1.8903	,	1.9575	,	3.4427	,	1.9166	,	2.6579	,	3.2769	,	1.2454	,	1.258	,	1.8001	,	2.3772	,	1.2313	,	1.4025	,	3.8397	,	4.1332	,	4.5075	,	1.3205	,	1.2987	,	2.097
RayTracingGpu	,	3.02464	,	3.79126	,	3.53894	,	3.29395	,	2.90986	,	2.66173	,	2.57987	,	2.52771	,	2.62736	,	2.87805	,	3.01888	,	2.7976	,	2.58528	,	2.60458	,	2.81021	,	3.46294	,	3.95405	,	4.69069	,	6.61795	,	5.02122	,	3.93126	,	2.98518	,	2.68461	,	2.65258
DeferredRLGpu	,	0.127808	,	0.174176	,	0.18912	,	0.145728	,	0.094752	,	0.065792	,	0.050848	,	0.044448	,	0.060192	,	0.088704	,	0.100704	,	0.076672	,	0.05232	,	0.053728	,	0.084992	,	0.176192	,	0.216672	,	0.25168	,	0.39136	,	0.307328	,	0.238848	,	0.122144	,	0.075584	,	0.063904
RayTracingRLGpu	,	0.607104	,	1.16435	,	1.02506	,	0.840288	,	0.519264	,	0.321888	,	0.250432	,	0.217152	,	0.297792	,	0.501568	,	0.6232	,	0.440416	,	0.267072	,	0.269568	,	0.44	,	0.959872	,	1.38224	,	2.05194	,	3.6328	,	2.13888	,	1.35005	,	0.567744	,	0.335904	,	0.298208
ResolveRLGpu	,	2.28874	,	2.45158	,	2.32365	,	2.30688	,	2.29491	,	2.27296	,	2.2775	,	2.26512	,	2.26813	,	2.28682	,	2.29398	,	2.27939	,	2.26486	,	2.28016	,	2.2841	,	2.32563	,	2.35405	,	2.38592	,	2.59267	,	2.57347	,	2.34125	,	2.29421	,	2.272	,	2.28925
Index	,	0	,	1	,	2	,	3	,	4	,	5	,	6	,	7	,	8	,	9	,	10	,	11	,	12	,	13	,	14	,	15	,	16	,	17	,	18	,	19	,	20	,	21	,	22	,	23

ProfilingBuildAggregator: 
BuildBottomLevelAS	,	1.6023
BuildBottomLevelASGpu	,	0.250176
BuildTopLevelAS	,	1.3326
BuildTopLevelASGpu	,	0.064352
Duplication	,	1
Triangles	,	3936
Meshes	,	1
BottomLevels	,	1
TopLevelSize	,	64
BottomLevelsSize	,	224128
Index	,	0

FpsAggregator: 
FPS	,	118	,	120	,	120	,	120	,	131	,	145	,	149	,	153	,	140	,	136	,	133	,	138	,	147	,	163	,	145	,	127	,	123	,	115	,	91	,	90	,	98	,	128	,	144	,	146
UPS	,	59	,	61	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	61	,	60	,	60	,	60	,	61	,	60	,	60	,	60	,	60	,	61	,	60	,	60	,	61	,	60	,	60
FrameTime	,	8.53471	,	8.3436	,	8.36389	,	8.36035	,	7.64086	,	6.8997	,	6.71994	,	6.55717	,	7.14468	,	7.37379	,	7.53935	,	7.29682	,	6.82335	,	6.1672	,	6.92878	,	7.91624	,	8.15382	,	8.70797	,	11.124	,	11.1289	,	10.2376	,	7.83852	,	6.96045	,	6.86381
GigaRays/s	,	5.96723	,	6.10392	,	6.08911	,	6.09169	,	6.66531	,	7.38128	,	7.57873	,	7.76686	,	7.12819	,	6.90671	,	6.75504	,	6.97957	,	7.46388	,	8.25798	,	7.35031	,	6.43344	,	6.24599	,	5.84851	,	4.57828	,	4.57624	,	4.97466	,	6.49723	,	7.31686	,	7.41988
Index	,	0	,	1	,	2	,	3	,	4	,	5	,	6	,	7	,	8	,	9	,	10	,	11	,	12	,	13	,	14	,	15	,	16	,	17	,	18	,	19	,	20	,	21	,	22	,	23


AccelerationStructure: 
Ray tracing acceleration structure building statistics: 
	Bottom Level Acceleration Structure: 
		Maximum [B]: 224128
		Minimum [B]: 224128
		Currently Allocated [B]: 224128
		Currently Created [num]: 1
		Current Average [B]: 224128
		Maximum Triangles [num]: 3936
		Minimum Triangles [num]: 3936
		Total Triangles [num]: 3936
		Maximum Meshes [num]: 1
		Minimum Meshes [num]: 1
		Total Meshes [num]: 1
	Top Level Acceleration Structure: 
		Maximum [B]: 64
		Minimum [B]: 64
		Currently Allocated [B]: 64
		Total Allocated [B]: 64
		Total Created [num]: 1
		Average Allocated [B]: 64
		Maximum Instances [num]: 1
		Minimum Instances [num]: 1
		Total Instances [num]: 1
	Scratch Buffer: 
		Maximum [B]: 34304
		Minimum [B]: 34304
		Currently Allocated [B]: 34304
		Total Allocated [B]: 34304
		Total Created [num]: 1
		Average Allocated [B]: 34304
ProfilingStack: 
[PROF] Profiling statistics: 
	Scopes entered : 34253
	Scopes exited : 34252
	Overhead per scope [ticks] : 1017
	Threads entered : 3
	Threads exited : 0
	============================
		         Sum[Mt]   Parent[%]  Samples[num]       Avg[Mt]   Last/Curr[Mt]        Self[Mt]     Self[%] 	 Name

	main:

		   24721.647 Mt   100.000 %         1 x    24721.649 Mt    24721.646 Mt        0.796 Mt     0.003 % 	 /

		   24720.895 Mt    99.997 %         1 x    24720.897 Mt    24720.895 Mt      328.933 Mt     1.331 % 	 ./Main application loop

		       2.929 Mt     0.012 %         1 x        2.929 Mt        0.000 Mt        2.929 Mt   100.000 % 	 ../TexturedRLInitialization

		       2.533 Mt     0.010 %         1 x        2.533 Mt        0.000 Mt        2.533 Mt   100.000 % 	 ../ShadowMapRLInitialization

		       3.918 Mt     0.016 %         1 x        3.918 Mt        0.000 Mt        3.918 Mt   100.000 % 	 ../AmbientOcclusionRLInitialization

		       5.483 Mt     0.022 %         1 x        5.483 Mt        0.000 Mt        5.483 Mt   100.000 % 	 ../RasterResolveRLInitialization

		     230.137 Mt     0.931 %         1 x      230.137 Mt        0.000 Mt        3.636 Mt     1.580 % 	 ../Initialization

		     226.501 Mt    98.420 %         1 x      226.501 Mt        0.000 Mt        1.322 Mt     0.584 % 	 .../ScenePrep

		     193.936 Mt    85.623 %         1 x      193.936 Mt        0.000 Mt       16.137 Mt     8.321 % 	 ..../SceneLoad

		     163.597 Mt    84.356 %         1 x      163.597 Mt        0.000 Mt       19.393 Mt    11.854 % 	 ...../glTF-LoadScene

		     144.204 Mt    88.146 %         2 x       72.102 Mt        0.000 Mt      144.204 Mt   100.000 % 	 ....../glTF-LoadImageData

		       8.044 Mt     4.148 %         1 x        8.044 Mt        0.000 Mt        8.044 Mt   100.000 % 	 ...../glTF-CreateScene

		       6.158 Mt     3.175 %         1 x        6.158 Mt        0.000 Mt        6.158 Mt   100.000 % 	 ...../SceneDesc-CreateScene

		       0.583 Mt     0.257 %         1 x        0.583 Mt        0.000 Mt        0.004 Mt     0.772 % 	 ..../ShadowMapRLPrep

		       0.579 Mt    99.228 %         1 x        0.579 Mt        0.000 Mt        0.518 Mt    89.578 % 	 ...../PrepareForRendering

		       0.060 Mt    10.422 %         1 x        0.060 Mt        0.000 Mt        0.060 Mt   100.000 % 	 ....../PrepareDrawBundle

		       1.119 Mt     0.494 %         1 x        1.119 Mt        0.000 Mt        0.004 Mt     0.375 % 	 ..../TexturedRLPrep

		       1.115 Mt    99.625 %         1 x        1.115 Mt        0.000 Mt        0.366 Mt    32.810 % 	 ...../PrepareForRendering

		       0.495 Mt    44.426 %         1 x        0.495 Mt        0.000 Mt        0.495 Mt   100.000 % 	 ....../PrepareMaterials

		       0.254 Mt    22.764 %         1 x        0.254 Mt        0.000 Mt        0.254 Mt   100.000 % 	 ....../PrepareDrawBundle

		       3.757 Mt     1.659 %         1 x        3.757 Mt        0.000 Mt        0.004 Mt     0.117 % 	 ..../DeferredRLPrep

		       3.753 Mt    99.883 %         1 x        3.753 Mt        0.000 Mt        1.613 Mt    42.976 % 	 ...../PrepareForRendering

		       0.016 Mt     0.416 %         1 x        0.016 Mt        0.000 Mt        0.016 Mt   100.000 % 	 ....../PrepareMaterials

		       1.860 Mt    49.571 %         1 x        1.860 Mt        0.000 Mt        1.860 Mt   100.000 % 	 ....../BuildMaterialCache

		       0.264 Mt     7.037 %         1 x        0.264 Mt        0.000 Mt        0.264 Mt   100.000 % 	 ....../PrepareDrawBundle

		      15.175 Mt     6.700 %         1 x       15.175 Mt        0.000 Mt        6.091 Mt    40.139 % 	 ..../RayTracingRLPrep

		       0.232 Mt     1.530 %         1 x        0.232 Mt        0.000 Mt        0.232 Mt   100.000 % 	 ...../PrepareAccelerationStructures

		       0.023 Mt     0.154 %         1 x        0.023 Mt        0.000 Mt        0.023 Mt   100.000 % 	 ...../PrepareCache

		       0.575 Mt     3.791 %         1 x        0.575 Mt        0.000 Mt        0.575 Mt   100.000 % 	 ...../BuildCache

		       6.457 Mt    42.554 %         1 x        6.457 Mt        0.000 Mt        0.004 Mt     0.063 % 	 ...../BuildAccelerationStructures

		       6.453 Mt    99.937 %         1 x        6.453 Mt        0.000 Mt        3.518 Mt    54.521 % 	 ....../AccelerationStructureBuild

		       1.602 Mt    24.829 %         1 x        1.602 Mt        0.000 Mt        1.602 Mt   100.000 % 	 ......./BuildBottomLevelAS

		       1.333 Mt    20.650 %         1 x        1.333 Mt        0.000 Mt        1.333 Mt   100.000 % 	 ......./BuildTopLevelAS

		       1.796 Mt    11.832 %         1 x        1.796 Mt        0.000 Mt        1.796 Mt   100.000 % 	 ...../BuildShaderTables

		      10.608 Mt     4.684 %         1 x       10.608 Mt        0.000 Mt       10.608 Mt   100.000 % 	 ..../GpuSidePrep

		       5.289 Mt     0.021 %         1 x        5.289 Mt        0.000 Mt        5.289 Mt   100.000 % 	 ../DeferredRLInitialization

		      50.414 Mt     0.204 %         1 x       50.414 Mt        0.000 Mt        2.474 Mt     4.907 % 	 ../RayTracingRLInitialization

		      47.940 Mt    95.093 %         1 x       47.940 Mt        0.000 Mt       47.940 Mt   100.000 % 	 .../PipelineBuild

		       4.898 Mt     0.020 %         1 x        4.898 Mt        0.000 Mt        4.898 Mt   100.000 % 	 ../ResolveRLInitialization

		   24086.361 Mt    97.433 %      4665 x        5.163 Mt        6.433 Mt      341.317 Mt     1.417 % 	 ../MainLoop

		    1573.651 Mt     6.533 %      1446 x        1.088 Mt        0.409 Mt     1569.675 Mt    99.747 % 	 .../Update

		       3.977 Mt     0.253 %         1 x        3.977 Mt        0.000 Mt        3.977 Mt   100.000 % 	 ..../GuiModelApply

		   22171.393 Mt    92.050 %      3122 x        7.102 Mt        5.436 Mt    15269.035 Mt    68.868 % 	 .../Render

		    6902.358 Mt    31.132 %      3122 x        2.211 Mt        1.471 Mt     6861.764 Mt    99.412 % 	 ..../RayTracing

		      18.934 Mt     0.274 %      3122 x        0.006 Mt        0.004 Mt       18.934 Mt   100.000 % 	 ...../DeferredRLPrep

		      21.659 Mt     0.314 %      3122 x        0.007 Mt        0.004 Mt       21.659 Mt   100.000 % 	 ...../RayTracingRLPrep

	WindowThread:

		   24716.838 Mt   100.000 %         1 x    24716.838 Mt    24716.837 Mt    24716.838 Mt   100.000 % 	 /

	GpuThread:

		   10248.920 Mt   100.000 %      3122 x        3.283 Mt        2.647 Mt      173.506 Mt     1.693 % 	 /

		       0.322 Mt     0.003 %         1 x        0.322 Mt        0.000 Mt        0.006 Mt     1.890 % 	 ./ScenePrep

		       0.316 Mt    98.110 %         1 x        0.316 Mt        0.000 Mt        0.001 Mt     0.325 % 	 ../AccelerationStructureBuild

		       0.250 Mt    79.282 %         1 x        0.250 Mt        0.000 Mt        0.250 Mt   100.000 % 	 .../BuildBottomLevelASGpu

		       0.064 Mt    20.393 %         1 x        0.064 Mt        0.000 Mt        0.064 Mt   100.000 % 	 .../BuildTopLevelASGpu

		    9996.582 Mt    97.538 %      3122 x        3.202 Mt        2.644 Mt        3.441 Mt     0.034 % 	 ./RayTracingGpu

		     388.253 Mt     3.884 %      3122 x        0.124 Mt        0.067 Mt      388.253 Mt   100.000 % 	 ../DeferredRLGpu

		    2338.234 Mt    23.390 %      3122 x        0.749 Mt        0.300 Mt     2338.234 Mt   100.000 % 	 ../RayTracingRLGpu

		    7266.654 Mt    72.691 %      3122 x        2.328 Mt        2.276 Mt     7266.654 Mt   100.000 % 	 ../ResolveRLGpu

		      78.511 Mt     0.766 %      3122 x        0.025 Mt        0.002 Mt       78.511 Mt   100.000 % 	 ./Present


	============================


