Configuration: 
Command line parameters  = E:\programming\FIT-DIP\Masters-DXR-Main-git\build\Measurement\x64\Debug\Measurement.exe --scene Suzanne/Suzanne.gltf --profile-automation --profile-camera-track Suzanne/Suzanne.trk --rt-mode --width 2560 --height 1440 --profile-output PresetSuzanne1440RtAmbientOcclusion16 --quality-preset AmbientOcclusion16 
Window width             = 2560
Window height            = 1421
Window title             = Direct3D App
Back-buffer count        = 3
Fill Triangles           = enabled
Triangles Font Clock-Wise= enabled
Fast Build AS            = disabled
Target FPS               = 0
Target UPS               = 60
Tearing                  = enabled
VSync                    = disabled
GUI rendering            = disabled
Debug layer              = enabled
GPU validation           = disabled
GPU profiling            = enabled
Render mode              = Ray Tracing
Loaded scene file        = Suzanne/Suzanne.gltf
Using testing scene      = disabled
Duplication              = 1
Duplication in cube      = disabled
Duplication offset       = 10
BLAS duplication         = disabled
Rays per pixel           = 17
Quality preset           = AmbientOcclusion16
Rasterization Quality    : 
  AO                     = enabled
  AO kernel size         = 16
  Shadows                = disabled
  Shadow map resolution  = 1
  Shadow PCF kernel size = 0
  Reflections            = disabled
Ray Tracing Quality      : 
  AO                     = enabled
  AO rays                = 16
  AO kernel size         = 4
  Shadows                = disabled
  Shadow rays            = 0
  Shadow kernel size     = 4
  Reflections            = disabled

ProfilingAggregator: 
Render	,	6.6025	,	7.0504	,	7.5162	,	6.3615	,	5.9442	,	3.6318	,	5.573	,	4.046	,	6.0969	,	4.5546	,	6.9937	,	4.4777	,	5.9031	,	3.9602	,	6.8671	,	5.5248	,	8.2604	,	9.6825	,	12.5075	,	9.78	,	7.442	,	6.5891	,	4.0427	,	4.5323
Update	,	0.9238	,	1.2844	,	0.5255	,	0.9716	,	1.2791	,	0.785	,	0.6936	,	1.2114	,	0.5316	,	0.4861	,	1.0692	,	0.9826	,	0.731	,	0.7762	,	0.9658	,	1.0743	,	1.0047	,	1.1877	,	1.3492	,	1.3248	,	1.2781	,	0.6176	,	0.5888	,	0.9527
RayTracing	,	2.2856	,	2.4477	,	2.6054	,	2.1693	,	2.4101	,	1.2311	,	2.4961	,	1.4064	,	2.6474	,	1.25	,	2.9567	,	1.6252	,	2.6527	,	1.3137	,	2.6459	,	1.4285	,	2.7054	,	2.6317	,	2.8209	,	2.7095	,	2.1217	,	2.7665	,	1.4877	,	1.6528
RayTracingGpu	,	2.63648	,	2.92595	,	3.13296	,	2.6649	,	1.88464	,	1.41206	,	1.41853	,	1.39542	,	1.7641	,	2.13008	,	2.03645	,	1.61766	,	1.37158	,	1.46646	,	2.22848	,	2.86499	,	3.60189	,	5.06576	,	7.43552	,	5.2839	,	3.62298	,	1.97357	,	1.4281	,	1.53178
DeferredRLGpu	,	0.132672	,	0.175616	,	0.190784	,	0.14512	,	0.094112	,	0.065984	,	0.057952	,	0.055488	,	0.078528	,	0.104288	,	0.101472	,	0.075872	,	0.057792	,	0.064288	,	0.102816	,	0.179616	,	0.216832	,	0.254592	,	0.39904	,	0.375808	,	0.23728	,	0.12208	,	0.075264	,	0.069216
RayTracingRLGpu	,	1.58768	,	1.94029	,	2.12013	,	1.71702	,	1.00666	,	0.57408	,	0.488896	,	0.41184	,	0.68544	,	1.10259	,	1.14458	,	0.765408	,	0.468448	,	0.50128	,	1.1816	,	1.84752	,	2.53805	,	3.75718	,	6.09821	,	4.00957	,	2.54643	,	1.06227	,	0.588576	,	0.583712
ResolveRLGpu	,	0.914784	,	0.808832	,	0.821088	,	0.801728	,	0.782784	,	0.770944	,	0.870656	,	0.926976	,	0.99856	,	0.922144	,	0.789312	,	0.7752	,	0.844192	,	0.899808	,	0.942912	,	0.836768	,	0.845856	,	1.05293	,	0.937344	,	0.896928	,	0.838336	,	0.788448	,	0.763488	,	0.877792
Index	,	0	,	1	,	2	,	3	,	4	,	5	,	6	,	7	,	8	,	9	,	10	,	11	,	12	,	13	,	14	,	15	,	16	,	17	,	18	,	19	,	20	,	21	,	22	,	23

ProfilingBuildAggregator: 
BuildBottomLevelAS	,	0.9476
BuildBottomLevelASGpu	,	0.358688
BuildTopLevelAS	,	2.4142
BuildTopLevelASGpu	,	0.085952
Duplication	,	1
Triangles	,	3936
Meshes	,	1
BottomLevels	,	1
TopLevelSize	,	64
BottomLevelsSize	,	224128
Index	,	0

FpsAggregator: 
FPS	,	139	,	154	,	137	,	138	,	170	,	200	,	210	,	215	,	202	,	183	,	178	,	183	,	207	,	223	,	197	,	147	,	125	,	102	,	82	,	87	,	104	,	156	,	199	,	207
UPS	,	59	,	60	,	60	,	61	,	60	,	60	,	60	,	60	,	60	,	61	,	60	,	60	,	60	,	60	,	61	,	60	,	60	,	61	,	60	,	61	,	60	,	60	,	61	,	60
FrameTime	,	7.19535	,	6.50086	,	7.33061	,	7.27862	,	5.89804	,	5.01271	,	4.76415	,	4.66857	,	4.95284	,	5.49206	,	5.63913	,	5.48011	,	4.83958	,	4.49755	,	5.1077	,	6.83987	,	8.03092	,	9.89702	,	12.3288	,	11.5995	,	9.68484	,	6.41206	,	5.03807	,	4.84879
GigaRays/s	,	8.5947	,	9.51289	,	8.43612	,	8.49638	,	10.4852	,	12.337	,	12.9807	,	13.2464	,	12.4862	,	11.2602	,	10.9666	,	11.2848	,	12.7784	,	13.7501	,	12.1076	,	9.04138	,	7.70047	,	6.24854	,	5.01607	,	5.33141	,	6.38544	,	9.64463	,	12.2749	,	12.7541
Index	,	0	,	1	,	2	,	3	,	4	,	5	,	6	,	7	,	8	,	9	,	10	,	11	,	12	,	13	,	14	,	15	,	16	,	17	,	18	,	19	,	20	,	21	,	22	,	23


AccelerationStructure: 
Ray tracing acceleration structure building statistics: 
	Bottom Level Acceleration Structure: 
		Maximum [B]: 224128
		Minimum [B]: 224128
		Currently Allocated [B]: 224128
		Currently Created [num]: 1
		Current Average [B]: 224128
		Maximum Triangles [num]: 3936
		Minimum Triangles [num]: 3936
		Total Triangles [num]: 3936
		Maximum Meshes [num]: 1
		Minimum Meshes [num]: 1
		Total Meshes [num]: 1
	Top Level Acceleration Structure: 
		Maximum [B]: 64
		Minimum [B]: 64
		Currently Allocated [B]: 64
		Total Allocated [B]: 64
		Total Created [num]: 1
		Average Allocated [B]: 64
		Maximum Instances [num]: 1
		Minimum Instances [num]: 1
		Total Instances [num]: 1
	Scratch Buffer: 
		Maximum [B]: 34304
		Minimum [B]: 34304
		Currently Allocated [B]: 34304
		Total Allocated [B]: 34304
		Total Created [num]: 1
		Average Allocated [B]: 34304
ProfilingStack: 
[PROF] Profiling statistics: 
	Scopes entered : 46402
	Scopes exited : 46401
	Overhead per scope [ticks] : 1003.6
	Threads entered : 3
	Threads exited : 0
	============================
		         Sum[Mt]   Parent[%]  Samples[num]       Avg[Mt]   Last/Curr[Mt]        Self[Mt]     Self[%] 	 Name

	main:

		   24738.924 Mt   100.000 %         1 x    24738.925 Mt    24738.923 Mt        0.821 Mt     0.003 % 	 /

		   24738.146 Mt    99.997 %         1 x    24738.149 Mt    24738.146 Mt      339.363 Mt     1.372 % 	 ./Main application loop

		       3.974 Mt     0.016 %         1 x        3.974 Mt        0.000 Mt        3.974 Mt   100.000 % 	 ../TexturedRLInitialization

		       3.188 Mt     0.013 %         1 x        3.188 Mt        0.000 Mt        3.188 Mt   100.000 % 	 ../ShadowMapRLInitialization

		       5.331 Mt     0.022 %         1 x        5.331 Mt        0.000 Mt        5.331 Mt   100.000 % 	 ../AmbientOcclusionRLInitialization

		       5.650 Mt     0.023 %         1 x        5.650 Mt        0.000 Mt        5.650 Mt   100.000 % 	 ../RasterResolveRLInitialization

		     230.214 Mt     0.931 %         1 x      230.214 Mt        0.000 Mt        3.622 Mt     1.573 % 	 ../Initialization

		     226.592 Mt    98.427 %         1 x      226.592 Mt        0.000 Mt        1.316 Mt     0.581 % 	 .../ScenePrep

		     193.602 Mt    85.441 %         1 x      193.602 Mt        0.000 Mt       16.554 Mt     8.551 % 	 ..../SceneLoad

		     163.639 Mt    84.524 %         1 x      163.639 Mt        0.000 Mt       19.327 Mt    11.811 % 	 ...../glTF-LoadScene

		     144.312 Mt    88.189 %         2 x       72.156 Mt        0.000 Mt      144.312 Mt   100.000 % 	 ....../glTF-LoadImageData

		       7.571 Mt     3.911 %         1 x        7.571 Mt        0.000 Mt        7.571 Mt   100.000 % 	 ...../glTF-CreateScene

		       5.837 Mt     3.015 %         1 x        5.837 Mt        0.000 Mt        5.837 Mt   100.000 % 	 ...../SceneDesc-CreateScene

		       0.421 Mt     0.186 %         1 x        0.421 Mt        0.000 Mt        0.005 Mt     1.139 % 	 ..../ShadowMapRLPrep

		       0.417 Mt    98.861 %         1 x        0.417 Mt        0.000 Mt        0.356 Mt    85.526 % 	 ...../PrepareForRendering

		       0.060 Mt    14.474 %         1 x        0.060 Mt        0.000 Mt        0.060 Mt   100.000 % 	 ....../PrepareDrawBundle

		       1.021 Mt     0.451 %         1 x        1.021 Mt        0.000 Mt        0.004 Mt     0.431 % 	 ..../TexturedRLPrep

		       1.017 Mt    99.569 %         1 x        1.017 Mt        0.000 Mt        0.344 Mt    33.855 % 	 ...../PrepareForRendering

		       0.424 Mt    41.743 %         1 x        0.424 Mt        0.000 Mt        0.424 Mt   100.000 % 	 ....../PrepareMaterials

		       0.248 Mt    24.402 %         1 x        0.248 Mt        0.000 Mt        0.248 Mt   100.000 % 	 ....../PrepareDrawBundle

		       4.565 Mt     2.015 %         1 x        4.565 Mt        0.000 Mt        0.005 Mt     0.118 % 	 ..../DeferredRLPrep

		       4.560 Mt    99.882 %         1 x        4.560 Mt        0.000 Mt        1.706 Mt    37.404 % 	 ...../PrepareForRendering

		       0.016 Mt     0.342 %         1 x        0.016 Mt        0.000 Mt        0.016 Mt   100.000 % 	 ....../PrepareMaterials

		       1.984 Mt    43.518 %         1 x        1.984 Mt        0.000 Mt        1.984 Mt   100.000 % 	 ....../BuildMaterialCache

		       0.854 Mt    18.737 %         1 x        0.854 Mt        0.000 Mt        0.854 Mt   100.000 % 	 ....../PrepareDrawBundle

		      13.118 Mt     5.789 %         1 x       13.118 Mt        0.000 Mt        4.307 Mt    32.835 % 	 ..../RayTracingRLPrep

		       0.736 Mt     5.611 %         1 x        0.736 Mt        0.000 Mt        0.736 Mt   100.000 % 	 ...../PrepareAccelerationStructures

		       0.074 Mt     0.561 %         1 x        0.074 Mt        0.000 Mt        0.074 Mt   100.000 % 	 ...../PrepareCache

		       0.495 Mt     3.771 %         1 x        0.495 Mt        0.000 Mt        0.495 Mt   100.000 % 	 ...../BuildCache

		       5.949 Mt    45.347 %         1 x        5.949 Mt        0.000 Mt        0.004 Mt     0.062 % 	 ...../BuildAccelerationStructures

		       5.945 Mt    99.938 %         1 x        5.945 Mt        0.000 Mt        2.583 Mt    43.450 % 	 ....../AccelerationStructureBuild

		       0.948 Mt    15.940 %         1 x        0.948 Mt        0.000 Mt        0.948 Mt   100.000 % 	 ......./BuildBottomLevelAS

		       2.414 Mt    40.610 %         1 x        2.414 Mt        0.000 Mt        2.414 Mt   100.000 % 	 ......./BuildTopLevelAS

		       1.558 Mt    11.875 %         1 x        1.558 Mt        0.000 Mt        1.558 Mt   100.000 % 	 ...../BuildShaderTables

		      12.549 Mt     5.538 %         1 x       12.549 Mt        0.000 Mt       12.549 Mt   100.000 % 	 ..../GpuSidePrep

		       5.089 Mt     0.021 %         1 x        5.089 Mt        0.000 Mt        5.089 Mt   100.000 % 	 ../DeferredRLInitialization

		      50.076 Mt     0.202 %         1 x       50.076 Mt        0.000 Mt        2.061 Mt     4.117 % 	 ../RayTracingRLInitialization

		      48.014 Mt    95.883 %         1 x       48.014 Mt        0.000 Mt       48.014 Mt   100.000 % 	 .../PipelineBuild

		       5.146 Mt     0.021 %         1 x        5.146 Mt        0.000 Mt        5.146 Mt   100.000 % 	 ../ResolveRLInitialization

		   24090.116 Mt    97.380 %      9389 x        2.566 Mt        4.639 Mt      313.371 Mt     1.301 % 	 ../MainLoop

		    1437.402 Mt     5.967 %      1446 x        0.994 Mt        0.518 Mt     1435.383 Mt    99.860 % 	 .../Update

		       2.019 Mt     0.140 %         1 x        2.019 Mt        0.000 Mt        2.019 Mt   100.000 % 	 ..../GuiModelApply

		   22339.343 Mt    92.732 %      3947 x        5.660 Mt        4.112 Mt    14717.501 Mt    65.882 % 	 .../Render

		    7621.842 Mt    34.118 %      3947 x        1.931 Mt        1.369 Mt     7584.451 Mt    99.509 % 	 ..../RayTracing

		      21.698 Mt     0.285 %      3947 x        0.005 Mt        0.003 Mt       21.698 Mt   100.000 % 	 ...../DeferredRLPrep

		      15.693 Mt     0.206 %      3947 x        0.004 Mt        0.003 Mt       15.693 Mt   100.000 % 	 ...../RayTracingRLPrep

	WindowThread:

		   24734.171 Mt   100.000 %         1 x    24734.172 Mt    24734.171 Mt    24734.171 Mt   100.000 % 	 /

	GpuThread:

		    9157.575 Mt   100.000 %      3947 x        2.320 Mt        1.536 Mt      160.314 Mt     1.751 % 	 /

		       0.452 Mt     0.005 %         1 x        0.452 Mt        0.000 Mt        0.006 Mt     1.303 % 	 ./ScenePrep

		       0.446 Mt    98.697 %         1 x        0.446 Mt        0.000 Mt        0.001 Mt     0.323 % 	 ../AccelerationStructureBuild

		       0.359 Mt    80.409 %         1 x        0.359 Mt        0.000 Mt        0.359 Mt   100.000 % 	 .../BuildBottomLevelASGpu

		       0.086 Mt    19.268 %         1 x        0.086 Mt        0.000 Mt        0.086 Mt   100.000 % 	 .../BuildTopLevelASGpu

		    8955.422 Mt    97.793 %      3947 x        2.269 Mt        1.534 Mt        4.735 Mt     0.053 % 	 ./RayTracingGpu

		     472.255 Mt     5.273 %      3947 x        0.120 Mt        0.070 Mt      472.255 Mt   100.000 % 	 ../DeferredRLGpu

		    5066.397 Mt    56.574 %      3947 x        1.284 Mt        0.588 Mt     5066.397 Mt   100.000 % 	 ../RayTracingRLGpu

		    3412.035 Mt    38.100 %      3947 x        0.864 Mt        0.873 Mt     3412.035 Mt   100.000 % 	 ../ResolveRLGpu

		      41.387 Mt     0.452 %      3947 x        0.010 Mt        0.002 Mt       41.387 Mt   100.000 % 	 ./Present


	============================


