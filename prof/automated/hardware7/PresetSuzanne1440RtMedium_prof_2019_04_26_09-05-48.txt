Configuration: 
Command line parameters  = E:\programming\FIT-DIP\Masters-DXR-Main-git\build\Measurement\x64\Debug\Measurement.exe --scene Suzanne/Suzanne.gltf --profile-automation --profile-camera-track Suzanne/Suzanne.trk --rt-mode --width 2560 --height 1440 --profile-output PresetSuzanne1440RtMedium --quality-preset Medium 
Window width             = 2560
Window height            = 1421
Window title             = Direct3D App
Back-buffer count        = 3
Fill Triangles           = enabled
Triangles Font Clock-Wise= enabled
Fast Build AS            = disabled
Target FPS               = 0
Target UPS               = 60
Tearing                  = enabled
VSync                    = disabled
GUI rendering            = disabled
Debug layer              = enabled
GPU validation           = disabled
GPU profiling            = enabled
Render mode              = Ray Tracing
Loaded scene file        = Suzanne/Suzanne.gltf
Using testing scene      = disabled
Duplication              = 1
Duplication in cube      = disabled
Duplication offset       = 10
BLAS duplication         = disabled
Rays per pixel           = 9
Quality preset           = Medium
Rasterization Quality    : 
  AO                     = enabled
  AO kernel size         = 4
  Shadows                = enabled
  Shadow map resolution  = 4096
  Shadow PCF kernel size = 4
  Reflections            = disabled
Ray Tracing Quality      : 
  AO                     = enabled
  AO rays                = 4
  AO kernel size         = 4
  Shadows                = enabled
  Shadow rays            = 4
  Shadow kernel size     = 4
  Reflections            = disabled

ProfilingAggregator: 
Render	,	7.1873	,	7.2449	,	7.1745	,	7.0638	,	5.4414	,	6.2104	,	5.788	,	5.9909	,	7.478	,	8.3696	,	5.5369	,	7.5896	,	6.0878	,	4.8403	,	5.5167	,	8.4554	,	8.8955	,	9.6989	,	12.0856	,	8.1316	,	11.5392	,	7.9811	,	4.9377	,	6.7142
Update	,	1.1015	,	1.1057	,	0.7864	,	1.1928	,	0.4873	,	0.7664	,	1.0514	,	1.2131	,	1.2109	,	1.2105	,	0.5256	,	1.2653	,	1.0795	,	0.5331	,	1.0348	,	1.0858	,	1.2008	,	1.1041	,	1.4236	,	0.7358	,	0.7586	,	0.7341	,	0.4765	,	1.2759
RayTracing	,	2.1545	,	2.264	,	2.0739	,	2.1551	,	1.2775	,	2.0736	,	1.8153	,	2.0957	,	2.9363	,	3.3451	,	1.3342	,	2.8694	,	1.9792	,	1.2946	,	1.4299	,	2.9943	,	3.0317	,	2.8766	,	3.3009	,	1.7872	,	4.5625	,	3.0063	,	1.2632	,	2.542
RayTracingGpu	,	3.05901	,	3.31299	,	3.4767	,	3.24618	,	2.99786	,	2.6015	,	2.48579	,	2.46189	,	2.56464	,	2.8304	,	2.95914	,	2.72208	,	2.51843	,	2.53146	,	2.7545	,	3.4231	,	3.88381	,	4.65936	,	6.38582	,	4.78739	,	3.86038	,	2.90355	,	2.62966	,	2.56093
DeferredRLGpu	,	0.127776	,	0.17456	,	0.19104	,	0.145216	,	0.094208	,	0.065536	,	0.050176	,	0.044512	,	0.059712	,	0.09008	,	0.102304	,	0.075392	,	0.05216	,	0.053824	,	0.085728	,	0.178144	,	0.213856	,	0.254496	,	0.389248	,	0.309408	,	0.237952	,	0.12112	,	0.074432	,	0.064096
RayTracingRLGpu	,	0.600896	,	0.885088	,	1.03133	,	0.840832	,	0.513504	,	0.321088	,	0.249216	,	0.216768	,	0.300864	,	0.505376	,	0.623232	,	0.435552	,	0.266144	,	0.269728	,	0.446208	,	0.971552	,	1.3801	,	2.07565	,	3.62179	,	2.13373	,	1.3353	,	0.567552	,	0.336608	,	0.298144
ResolveRLGpu	,	2.32906	,	2.25219	,	2.25338	,	2.25917	,	2.38922	,	2.21379	,	2.18541	,	2.19946	,	2.20314	,	2.23382	,	2.23264	,	2.20986	,	2.19917	,	2.20678	,	2.2216	,	2.27242	,	2.28867	,	2.32838	,	2.37382	,	2.34314	,	2.28605	,	2.21379	,	2.21734	,	2.1977
Index	,	0	,	1	,	2	,	3	,	4	,	5	,	6	,	7	,	8	,	9	,	10	,	11	,	12	,	13	,	14	,	15	,	16	,	17	,	18	,	19	,	20	,	21	,	22	,	23

ProfilingBuildAggregator: 
BuildBottomLevelAS	,	0.9762
BuildBottomLevelASGpu	,	0.251328
BuildTopLevelAS	,	1.1681
BuildTopLevelASGpu	,	0.05968
Duplication	,	1
Triangles	,	3936
Meshes	,	1
BottomLevels	,	1
TopLevelSize	,	64
BottomLevelsSize	,	224128
Index	,	0

FpsAggregator: 
FPS	,	125	,	128	,	120	,	132	,	131	,	154	,	156	,	147	,	156	,	150	,	139	,	142	,	146	,	164	,	153	,	135	,	110	,	101	,	92	,	93	,	101	,	124	,	149	,	158
UPS	,	59	,	61	,	60	,	60	,	61	,	60	,	60	,	60	,	60	,	61	,	60	,	60	,	60	,	61	,	60	,	60	,	60	,	60	,	60	,	61	,	60	,	60	,	61	,	60
FrameTime	,	8.05235	,	7.85315	,	8.33351	,	7.62919	,	7.67338	,	6.53217	,	6.43729	,	6.83394	,	6.4147	,	6.70978	,	7.21606	,	7.06152	,	6.85704	,	6.11933	,	6.57125	,	7.40866	,	9.11312	,	9.965	,	10.9475	,	10.7826	,	9.99395	,	8.07296	,	6.71275	,	6.362
GigaRays/s	,	4.06587	,	4.16901	,	3.9287	,	4.29139	,	4.26668	,	5.0121	,	5.08597	,	4.79077	,	5.10388	,	4.87942	,	4.53708	,	4.63637	,	4.77463	,	5.35023	,	4.98229	,	4.41913	,	3.5926	,	3.28548	,	2.99062	,	3.03637	,	3.27596	,	4.05549	,	4.87726	,	5.14616
Index	,	0	,	1	,	2	,	3	,	4	,	5	,	6	,	7	,	8	,	9	,	10	,	11	,	12	,	13	,	14	,	15	,	16	,	17	,	18	,	19	,	20	,	21	,	22	,	23


AccelerationStructure: 
Ray tracing acceleration structure building statistics: 
	Bottom Level Acceleration Structure: 
		Maximum [B]: 224128
		Minimum [B]: 224128
		Currently Allocated [B]: 224128
		Currently Created [num]: 1
		Current Average [B]: 224128
		Maximum Triangles [num]: 3936
		Minimum Triangles [num]: 3936
		Total Triangles [num]: 3936
		Maximum Meshes [num]: 1
		Minimum Meshes [num]: 1
		Total Meshes [num]: 1
	Top Level Acceleration Structure: 
		Maximum [B]: 64
		Minimum [B]: 64
		Currently Allocated [B]: 64
		Total Allocated [B]: 64
		Total Created [num]: 1
		Average Allocated [B]: 64
		Maximum Instances [num]: 1
		Minimum Instances [num]: 1
		Total Instances [num]: 1
	Scratch Buffer: 
		Maximum [B]: 34304
		Minimum [B]: 34304
		Currently Allocated [B]: 34304
		Total Allocated [B]: 34304
		Total Created [num]: 1
		Average Allocated [B]: 34304
ProfilingStack: 
[PROF] Profiling statistics: 
	Scopes entered : 39016
	Scopes exited : 39015
	Overhead per scope [ticks] : 1013.4
	Threads entered : 3
	Threads exited : 0
	============================
		         Sum[Mt]   Parent[%]  Samples[num]       Avg[Mt]   Last/Curr[Mt]        Self[Mt]     Self[%] 	 Name

	main:

		   24761.665 Mt   100.000 %         1 x    24761.668 Mt    24761.663 Mt        0.990 Mt     0.004 % 	 /

		   24760.817 Mt    99.997 %         1 x    24760.824 Mt    24760.816 Mt      352.483 Mt     1.424 % 	 ./Main application loop

		       2.978 Mt     0.012 %         1 x        2.978 Mt        0.000 Mt        2.978 Mt   100.000 % 	 ../TexturedRLInitialization

		       2.518 Mt     0.010 %         1 x        2.518 Mt        0.000 Mt        2.518 Mt   100.000 % 	 ../ShadowMapRLInitialization

		       3.874 Mt     0.016 %         1 x        3.874 Mt        0.000 Mt        3.874 Mt   100.000 % 	 ../AmbientOcclusionRLInitialization

		       5.895 Mt     0.024 %         1 x        5.895 Mt        0.000 Mt        5.895 Mt   100.000 % 	 ../RasterResolveRLInitialization

		     231.301 Mt     0.934 %         1 x      231.301 Mt        0.000 Mt        3.609 Mt     1.560 % 	 ../Initialization

		     227.692 Mt    98.440 %         1 x      227.692 Mt        0.000 Mt        1.239 Mt     0.544 % 	 .../ScenePrep

		     194.467 Mt    85.408 %         1 x      194.467 Mt        0.000 Mt       16.637 Mt     8.555 % 	 ..../SceneLoad

		     164.094 Mt    84.381 %         1 x      164.094 Mt        0.000 Mt       19.302 Mt    11.763 % 	 ...../glTF-LoadScene

		     144.792 Mt    88.237 %         2 x       72.396 Mt        0.000 Mt      144.792 Mt   100.000 % 	 ....../glTF-LoadImageData

		       7.835 Mt     4.029 %         1 x        7.835 Mt        0.000 Mt        7.835 Mt   100.000 % 	 ...../glTF-CreateScene

		       5.901 Mt     3.034 %         1 x        5.901 Mt        0.000 Mt        5.901 Mt   100.000 % 	 ...../SceneDesc-CreateScene

		       0.451 Mt     0.198 %         1 x        0.451 Mt        0.000 Mt        0.005 Mt     1.087 % 	 ..../ShadowMapRLPrep

		       0.446 Mt    98.913 %         1 x        0.446 Mt        0.000 Mt        0.384 Mt    86.188 % 	 ...../PrepareForRendering

		       0.062 Mt    13.812 %         1 x        0.062 Mt        0.000 Mt        0.062 Mt   100.000 % 	 ....../PrepareDrawBundle

		       1.124 Mt     0.494 %         1 x        1.124 Mt        0.000 Mt        0.004 Mt     0.374 % 	 ..../TexturedRLPrep

		       1.120 Mt    99.626 %         1 x        1.120 Mt        0.000 Mt        0.368 Mt    32.845 % 	 ...../PrepareForRendering

		       0.496 Mt    44.246 %         1 x        0.496 Mt        0.000 Mt        0.496 Mt   100.000 % 	 ....../PrepareMaterials

		       0.257 Mt    22.909 %         1 x        0.257 Mt        0.000 Mt        0.257 Mt   100.000 % 	 ....../PrepareDrawBundle

		       3.619 Mt     1.590 %         1 x        3.619 Mt        0.000 Mt        0.004 Mt     0.122 % 	 ..../DeferredRLPrep

		       3.615 Mt    99.878 %         1 x        3.615 Mt        0.000 Mt        1.623 Mt    44.905 % 	 ...../PrepareForRendering

		       0.016 Mt     0.432 %         1 x        0.016 Mt        0.000 Mt        0.016 Mt   100.000 % 	 ....../PrepareMaterials

		       1.726 Mt    47.754 %         1 x        1.726 Mt        0.000 Mt        1.726 Mt   100.000 % 	 ....../BuildMaterialCache

		       0.250 Mt     6.910 %         1 x        0.250 Mt        0.000 Mt        0.250 Mt   100.000 % 	 ....../PrepareDrawBundle

		      15.660 Mt     6.878 %         1 x       15.660 Mt        0.000 Mt        4.004 Mt    25.569 % 	 ..../RayTracingRLPrep

		       0.233 Mt     1.486 %         1 x        0.233 Mt        0.000 Mt        0.233 Mt   100.000 % 	 ...../PrepareAccelerationStructures

		       0.024 Mt     0.151 %         1 x        0.024 Mt        0.000 Mt        0.024 Mt   100.000 % 	 ...../PrepareCache

		       0.501 Mt     3.201 %         1 x        0.501 Mt        0.000 Mt        0.501 Mt   100.000 % 	 ...../BuildCache

		       9.082 Mt    57.995 %         1 x        9.082 Mt        0.000 Mt        0.004 Mt     0.040 % 	 ...../BuildAccelerationStructures

		       9.078 Mt    99.960 %         1 x        9.078 Mt        0.000 Mt        6.934 Mt    76.380 % 	 ....../AccelerationStructureBuild

		       0.976 Mt    10.753 %         1 x        0.976 Mt        0.000 Mt        0.976 Mt   100.000 % 	 ......./BuildBottomLevelAS

		       1.168 Mt    12.867 %         1 x        1.168 Mt        0.000 Mt        1.168 Mt   100.000 % 	 ......./BuildTopLevelAS

		       1.816 Mt    11.598 %         1 x        1.816 Mt        0.000 Mt        1.816 Mt   100.000 % 	 ...../BuildShaderTables

		      11.132 Mt     4.889 %         1 x       11.132 Mt        0.000 Mt       11.132 Mt   100.000 % 	 ..../GpuSidePrep

		       5.306 Mt     0.021 %         1 x        5.306 Mt        0.000 Mt        5.306 Mt   100.000 % 	 ../DeferredRLInitialization

		      50.805 Mt     0.205 %         1 x       50.805 Mt        0.000 Mt        2.146 Mt     4.224 % 	 ../RayTracingRLInitialization

		      48.659 Mt    95.776 %         1 x       48.659 Mt        0.000 Mt       48.659 Mt   100.000 % 	 .../PipelineBuild

		       5.011 Mt     0.020 %         1 x        5.011 Mt        0.000 Mt        5.011 Mt   100.000 % 	 ../ResolveRLInitialization

		   24100.648 Mt    97.334 %      8653 x        2.785 Mt        6.492 Mt      335.991 Mt     1.394 % 	 ../MainLoop

		    1511.177 Mt     6.270 %      1447 x        1.044 Mt        0.469 Mt     1507.219 Mt    99.738 % 	 .../Update

		       3.958 Mt     0.262 %         1 x        3.958 Mt        0.000 Mt        3.958 Mt   100.000 % 	 ..../GuiModelApply

		   22253.480 Mt    92.336 %      3208 x        6.937 Mt        5.434 Mt    15255.445 Mt    68.553 % 	 .../Render

		    6998.035 Mt    31.447 %      3208 x        2.181 Mt        1.607 Mt     6963.888 Mt    99.512 % 	 ..../RayTracing

		      19.256 Mt     0.275 %      3208 x        0.006 Mt        0.004 Mt       19.256 Mt   100.000 % 	 ...../DeferredRLPrep

		      14.891 Mt     0.213 %      3208 x        0.005 Mt        0.004 Mt       14.891 Mt   100.000 % 	 ...../RayTracingRLPrep

	WindowThread:

		   24755.159 Mt   100.000 %         1 x    24755.161 Mt    24755.158 Mt    24755.159 Mt   100.000 % 	 /

	GpuThread:

		   10258.125 Mt   100.000 %      3208 x        3.198 Mt        2.553 Mt      170.146 Mt     1.659 % 	 /

		       0.317 Mt     0.003 %         1 x        0.317 Mt        0.000 Mt        0.005 Mt     1.704 % 	 ./ScenePrep

		       0.312 Mt    98.296 %         1 x        0.312 Mt        0.000 Mt        0.001 Mt     0.328 % 	 ../AccelerationStructureBuild

		       0.251 Mt    80.546 %         1 x        0.251 Mt        0.000 Mt        0.251 Mt   100.000 % 	 .../BuildBottomLevelASGpu

		       0.060 Mt    19.126 %         1 x        0.060 Mt        0.000 Mt        0.060 Mt   100.000 % 	 .../BuildTopLevelASGpu

		   10007.207 Mt    97.554 %      3208 x        3.119 Mt        2.551 Mt        3.480 Mt     0.035 % 	 ./RayTracingGpu

		     394.593 Mt     3.943 %      3208 x        0.123 Mt        0.066 Mt      394.593 Mt   100.000 % 	 ../DeferredRLGpu

		    2361.749 Mt    23.600 %      3208 x        0.736 Mt        0.300 Mt     2361.749 Mt   100.000 % 	 ../RayTracingRLGpu

		    7247.385 Mt    72.422 %      3208 x        2.259 Mt        2.183 Mt     7247.385 Mt   100.000 % 	 ../ResolveRLGpu

		      80.454 Mt     0.784 %      3208 x        0.025 Mt        0.001 Mt       80.454 Mt   100.000 % 	 ./Present


	============================


