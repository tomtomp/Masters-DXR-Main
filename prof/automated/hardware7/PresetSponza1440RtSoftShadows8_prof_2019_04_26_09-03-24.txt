Configuration: 
Command line parameters  = E:\programming\FIT-DIP\Masters-DXR-Main-git\build\Measurement\x64\Debug\Measurement.exe --scene Sponza/Sponza.gltf --profile-automation --profile-camera-track Sponza/SponzaPreset.trk --rt-mode --width 2560 --height 1440 --profile-output PresetSponza1440RtSoftShadows8 --quality-preset SoftShadows8 
Window width             = 2560
Window height            = 1421
Window title             = Direct3D App
Back-buffer count        = 3
Fill Triangles           = enabled
Triangles Font Clock-Wise= enabled
Fast Build AS            = disabled
Target FPS               = 0
Target UPS               = 60
Tearing                  = enabled
VSync                    = disabled
GUI rendering            = disabled
Debug layer              = enabled
GPU validation           = disabled
GPU profiling            = enabled
Render mode              = Ray Tracing
Loaded scene file        = Sponza/Sponza.gltf
Using testing scene      = disabled
Duplication              = 1
Duplication in cube      = disabled
Duplication offset       = 10
BLAS duplication         = disabled
Rays per pixel           = 9
Quality preset           = SoftShadows8
Rasterization Quality    : 
  AO                     = disabled
  AO kernel size         = 1
  Shadows                = enabled
  Shadow map resolution  = 8192
  Shadow PCF kernel size = 8
  Reflections            = disabled
Ray Tracing Quality      : 
  AO                     = disabled
  AO rays                = 0
  AO kernel size         = 8
  Shadows                = enabled
  Shadow rays            = 8
  Shadow kernel size     = 8
  Reflections            = disabled

ProfilingAggregator: 
Render	,	16.032	,	16.8525	,	16.0714	,	16.1783	,	16.5829	,	16.9678	,	16.0357	,	15.893	,	15.2193	,	15.5802	,	15.8808	,	16.2906	,	15.9628	,	17.4531	,	15.8032	,	15.3674	,	16.1878	,	15.6512	,	15.0187	,	16.3951	,	16.701	,	16.2108	,	16.4609
Update	,	0.9847	,	0.5933	,	1.033	,	0.6292	,	0.8545	,	0.4834	,	0.8929	,	1.0598	,	0.7046	,	0.5979	,	0.9399	,	0.6241	,	0.7837	,	0.5756	,	0.5771	,	0.8382	,	0.7716	,	0.6578	,	0.9116	,	0.5758	,	0.5824	,	1.0897	,	0.6007
RayTracing	,	3.0517	,	3.1736	,	3.1174	,	3.0543	,	3.0316	,	3.0346	,	3.0804	,	3.026	,	3.0112	,	3.159	,	3.0419	,	3.1161	,	3.0169	,	3.1041	,	3.0455	,	3.1277	,	3.0309	,	3.0335	,	3.1041	,	3.0719	,	3.1522	,	3.1022	,	3.0866
RayTracingGpu	,	11.3802	,	12.0469	,	11.3377	,	11.5837	,	12.0803	,	12.3021	,	11.537	,	11.4946	,	10.63	,	10.6348	,	11.3139	,	11.3977	,	11.5103	,	12.518	,	11.2582	,	10.237	,	11.57	,	11.0633	,	10.3656	,	11.7687	,	11.7505	,	11.515	,	11.642
DeferredRLGpu	,	1.45789	,	1.4072	,	1.2761	,	0.776864	,	0.70672	,	0.864672	,	1.14736	,	1.35949	,	1.00835	,	0.78512	,	0.695296	,	0.919712	,	1.10115	,	1.23267	,	1.22998	,	0.904192	,	1.09354	,	1.03984	,	0.851488	,	1.43859	,	1.3759	,	1.39667	,	1.3791
RayTracingRLGpu	,	4.75078	,	5.33418	,	4.84381	,	5.48589	,	6.04518	,	6.22995	,	5.08886	,	4.91926	,	4.44326	,	4.51558	,	5.26493	,	5.27139	,	5.23949	,	5.98214	,	4.83891	,	3.9895	,	5.28048	,	4.83731	,	4.20022	,	4.96432	,	4.95658	,	4.93101	,	4.95174
ResolveRLGpu	,	5.17072	,	5.30029	,	5.21683	,	5.31994	,	5.32752	,	5.20618	,	5.29962	,	5.21472	,	5.17757	,	5.33197	,	5.35267	,	5.20554	,	5.16851	,	5.30227	,	5.18822	,	5.34246	,	5.19507	,	5.18534	,	5.31235	,	5.36438	,	5.41702	,	5.18643	,	5.31018
Index	,	0	,	1	,	2	,	3	,	4	,	5	,	6	,	7	,	8	,	9	,	10	,	11	,	12	,	13	,	14	,	15	,	16	,	17	,	18	,	19	,	20	,	21	,	22

ProfilingBuildAggregator: 
BuildBottomLevelAS	,	10.112
BuildBottomLevelASGpu	,	12.3128
BuildTopLevelAS	,	2.8164
BuildTopLevelASGpu	,	0.443552
Duplication	,	1
Triangles	,	262267
Meshes	,	103
BottomLevels	,	1
TopLevelSize	,	64
BottomLevelsSize	,	14623872
Index	,	0

FpsAggregator: 
FPS	,	39	,	58	,	60	,	59	,	59	,	55	,	59	,	60	,	60	,	62	,	61	,	60	,	60	,	57	,	58	,	64	,	61	,	60	,	63	,	63	,	59	,	59	,	59
UPS	,	59	,	61	,	61	,	60	,	60	,	61	,	60	,	61	,	60	,	60	,	61	,	60	,	61	,	60	,	61	,	60	,	61	,	61	,	61	,	60	,	61	,	60	,	61
FrameTime	,	26.0543	,	17.4509	,	16.7942	,	17.1385	,	16.9716	,	18.2387	,	17.205	,	16.6962	,	16.7097	,	16.277	,	16.4957	,	16.7707	,	16.9245	,	17.6953	,	17.3888	,	15.7226	,	16.6178	,	16.9249	,	16.0857	,	15.88	,	17.1364	,	17.1394	,	17.065
GigaRays/s	,	1.2566	,	1.87611	,	1.94947	,	1.91031	,	1.92909	,	1.79507	,	1.90292	,	1.96092	,	1.95934	,	2.01142	,	1.98475	,	1.95221	,	1.93446	,	1.8502	,	1.88281	,	2.08235	,	1.97016	,	1.93442	,	2.03533	,	2.0617	,	1.91054	,	1.91021	,	1.91853
Index	,	0	,	1	,	2	,	3	,	4	,	5	,	6	,	7	,	8	,	9	,	10	,	11	,	12	,	13	,	14	,	15	,	16	,	17	,	18	,	19	,	20	,	21	,	22


AccelerationStructure: 
Ray tracing acceleration structure building statistics: 
	Bottom Level Acceleration Structure: 
		Maximum [B]: 14623872
		Minimum [B]: 14623872
		Currently Allocated [B]: 14623872
		Currently Created [num]: 1
		Current Average [B]: 14623872
		Maximum Triangles [num]: 262267
		Minimum Triangles [num]: 262267
		Total Triangles [num]: 262267
		Maximum Meshes [num]: 103
		Minimum Meshes [num]: 103
		Total Meshes [num]: 103
	Top Level Acceleration Structure: 
		Maximum [B]: 64
		Minimum [B]: 64
		Currently Allocated [B]: 64
		Total Allocated [B]: 64
		Total Created [num]: 1
		Average Allocated [B]: 64
		Maximum Instances [num]: 1
		Minimum Instances [num]: 1
		Total Instances [num]: 1
	Scratch Buffer: 
		Maximum [B]: 11660800
		Minimum [B]: 11660800
		Currently Allocated [B]: 11660800
		Total Allocated [B]: 11660800
		Total Created [num]: 1
		Average Allocated [B]: 11660800
ProfilingStack: 
[PROF] Profiling statistics: 
	Scopes entered : 19856
	Scopes exited : 19855
	Overhead per scope [ticks] : 1010.3
	Threads entered : 3
	Threads exited : 0
	============================
		         Sum[Mt]   Parent[%]  Samples[num]       Avg[Mt]   Last/Curr[Mt]        Self[Mt]     Self[%] 	 Name

	main:

		   33092.871 Mt   100.000 %         1 x    33092.875 Mt    33092.869 Mt        1.177 Mt     0.004 % 	 /

		   33091.839 Mt    99.997 %         1 x    33091.846 Mt    33091.838 Mt      364.390 Mt     1.101 % 	 ./Main application loop

		       3.016 Mt     0.009 %         1 x        3.016 Mt        0.000 Mt        3.016 Mt   100.000 % 	 ../TexturedRLInitialization

		       2.386 Mt     0.007 %         1 x        2.386 Mt        0.000 Mt        2.386 Mt   100.000 % 	 ../ShadowMapRLInitialization

		       4.072 Mt     0.012 %         1 x        4.072 Mt        0.000 Mt        4.072 Mt   100.000 % 	 ../AmbientOcclusionRLInitialization

		       6.025 Mt     0.018 %         1 x        6.025 Mt        0.000 Mt        6.025 Mt   100.000 % 	 ../RasterResolveRLInitialization

		    9428.535 Mt    28.492 %         1 x     9428.535 Mt        0.000 Mt       12.971 Mt     0.138 % 	 ../Initialization

		    9415.564 Mt    99.862 %         1 x     9415.564 Mt        0.000 Mt        3.407 Mt     0.036 % 	 .../ScenePrep

		    9297.944 Mt    98.751 %         1 x     9297.944 Mt        0.000 Mt      595.528 Mt     6.405 % 	 ..../SceneLoad

		    8272.837 Mt    88.975 %         1 x     8272.837 Mt        0.000 Mt     1119.541 Mt    13.533 % 	 ...../glTF-LoadScene

		    7153.295 Mt    86.467 %        69 x      103.671 Mt        0.000 Mt     7153.295 Mt   100.000 % 	 ....../glTF-LoadImageData

		     315.851 Mt     3.397 %         1 x      315.851 Mt        0.000 Mt      315.851 Mt   100.000 % 	 ...../glTF-CreateScene

		     113.728 Mt     1.223 %         1 x      113.728 Mt        0.000 Mt      113.728 Mt   100.000 % 	 ...../SceneDesc-CreateScene

		       2.733 Mt     0.029 %         1 x        2.733 Mt        0.000 Mt        0.006 Mt     0.234 % 	 ..../ShadowMapRLPrep

		       2.727 Mt    99.766 %         1 x        2.727 Mt        0.000 Mt        2.003 Mt    73.445 % 	 ...../PrepareForRendering

		       0.724 Mt    26.555 %         1 x        0.724 Mt        0.000 Mt        0.724 Mt   100.000 % 	 ....../PrepareDrawBundle

		       5.951 Mt     0.063 %         1 x        5.951 Mt        0.000 Mt        0.005 Mt     0.086 % 	 ..../TexturedRLPrep

		       5.946 Mt    99.914 %         1 x        5.946 Mt        0.000 Mt        1.117 Mt    18.789 % 	 ...../PrepareForRendering

		       2.602 Mt    43.766 %         1 x        2.602 Mt        0.000 Mt        2.602 Mt   100.000 % 	 ....../PrepareMaterials

		       2.226 Mt    37.445 %         1 x        2.226 Mt        0.000 Mt        2.226 Mt   100.000 % 	 ....../PrepareDrawBundle

		       7.828 Mt     0.083 %         1 x        7.828 Mt        0.000 Mt        0.005 Mt     0.060 % 	 ..../DeferredRLPrep

		       7.823 Mt    99.940 %         1 x        7.823 Mt        0.000 Mt        3.024 Mt    38.658 % 	 ...../PrepareForRendering

		       0.021 Mt     0.274 %         1 x        0.021 Mt        0.000 Mt        0.021 Mt   100.000 % 	 ....../PrepareMaterials

		       2.144 Mt    27.408 %         1 x        2.144 Mt        0.000 Mt        2.144 Mt   100.000 % 	 ....../BuildMaterialCache

		       2.633 Mt    33.660 %         1 x        2.633 Mt        0.000 Mt        2.633 Mt   100.000 % 	 ....../PrepareDrawBundle

		      36.317 Mt     0.386 %         1 x       36.317 Mt        0.000 Mt        6.968 Mt    19.186 % 	 ..../RayTracingRLPrep

		       0.570 Mt     1.569 %         1 x        0.570 Mt        0.000 Mt        0.570 Mt   100.000 % 	 ...../PrepareAccelerationStructures

		       0.026 Mt     0.071 %         1 x        0.026 Mt        0.000 Mt        0.026 Mt   100.000 % 	 ...../PrepareCache

		       5.338 Mt    14.700 %         1 x        5.338 Mt        0.000 Mt        5.338 Mt   100.000 % 	 ...../BuildCache

		      18.740 Mt    51.601 %         1 x       18.740 Mt        0.000 Mt        0.004 Mt     0.022 % 	 ...../BuildAccelerationStructures

		      18.735 Mt    99.978 %         1 x       18.735 Mt        0.000 Mt        5.807 Mt    30.995 % 	 ....../AccelerationStructureBuild

		      10.112 Mt    53.973 %         1 x       10.112 Mt        0.000 Mt       10.112 Mt   100.000 % 	 ......./BuildBottomLevelAS

		       2.816 Mt    15.033 %         1 x        2.816 Mt        0.000 Mt        2.816 Mt   100.000 % 	 ......./BuildTopLevelAS

		       4.675 Mt    12.873 %         1 x        4.675 Mt        0.000 Mt        4.675 Mt   100.000 % 	 ...../BuildShaderTables

		      61.384 Mt     0.652 %         1 x       61.384 Mt        0.000 Mt       61.384 Mt   100.000 % 	 ..../GpuSidePrep

		       4.981 Mt     0.015 %         1 x        4.981 Mt        0.000 Mt        4.981 Mt   100.000 % 	 ../DeferredRLInitialization

		      50.927 Mt     0.154 %         1 x       50.927 Mt        0.000 Mt        2.166 Mt     4.253 % 	 ../RayTracingRLInitialization

		      48.761 Mt    95.747 %         1 x       48.761 Mt        0.000 Mt       48.761 Mt   100.000 % 	 .../PipelineBuild

		       5.326 Mt     0.016 %         1 x        5.326 Mt        0.000 Mt        5.326 Mt   100.000 % 	 ../ResolveRLInitialization

		   23222.180 Mt    70.175 %      6149 x        3.777 Mt       33.753 Mt      158.716 Mt     0.683 % 	 ../MainLoop

		    1155.694 Mt     4.977 %      1392 x        0.830 Mt        1.044 Mt     1154.272 Mt    99.877 % 	 .../Update

		       1.422 Mt     0.123 %         1 x        1.422 Mt        0.000 Mt        1.422 Mt   100.000 % 	 ..../GuiModelApply

		   21907.770 Mt    94.340 %      1356 x       16.156 Mt       21.378 Mt    17712.639 Mt    80.851 % 	 .../Render

		    4195.131 Mt    19.149 %      1356 x        3.094 Mt        6.761 Mt     4185.360 Mt    99.767 % 	 ..../RayTracing

		       4.600 Mt     0.110 %      1356 x        0.003 Mt        0.006 Mt        4.600 Mt   100.000 % 	 ...../DeferredRLPrep

		       5.171 Mt     0.123 %      1356 x        0.004 Mt        0.015 Mt        5.171 Mt   100.000 % 	 ...../RayTracingRLPrep

	WindowThread:

		   33080.281 Mt   100.000 %         1 x    33080.282 Mt    33080.280 Mt    33080.281 Mt   100.000 % 	 /

	GpuThread:

		   15865.873 Mt   100.000 %      1356 x       11.700 Mt       11.645 Mt      256.961 Mt     1.620 % 	 /

		      12.778 Mt     0.081 %         1 x       12.778 Mt        0.000 Mt        0.018 Mt     0.144 % 	 ./ScenePrep

		      12.759 Mt    99.856 %         1 x       12.759 Mt        0.000 Mt        0.003 Mt     0.024 % 	 ../AccelerationStructureBuild

		      12.313 Mt    96.500 %         1 x       12.313 Mt        0.000 Mt       12.313 Mt   100.000 % 	 .../BuildBottomLevelASGpu

		       0.444 Mt     3.476 %         1 x        0.444 Mt        0.000 Mt        0.444 Mt   100.000 % 	 .../BuildTopLevelASGpu

		   15529.048 Mt    97.877 %      1356 x       11.452 Mt       11.557 Mt        7.374 Mt     0.047 % 	 ./RayTracingGpu

		    1536.626 Mt     9.895 %      1356 x        1.133 Mt        1.371 Mt     1536.626 Mt   100.000 % 	 ../DeferredRLGpu

		    6828.340 Mt    43.971 %      1356 x        5.036 Mt        4.918 Mt     6828.340 Mt   100.000 % 	 ../RayTracingRLGpu

		    7156.709 Mt    46.086 %      1356 x        5.278 Mt        5.267 Mt     7156.709 Mt   100.000 % 	 ../ResolveRLGpu

		      67.086 Mt     0.423 %      1356 x        0.049 Mt        0.087 Mt       67.086 Mt   100.000 % 	 ./Present


	============================


