Configuration: 
Command line parameters  = E:\programming\FIT-DIP\Masters-DXR-Main-git\build\Measurement\x64\Debug\Measurement.exe --scene Cube/Cube.gltf --profile-automation --profile-camera-track Cube/Cube.trk --rt-mode --width 2560 --height 1440 --profile-output TrackCube1440Rt 
Window width             = 2560
Window height            = 1421
Window title             = Direct3D App
Back-buffer count        = 3
Fill Triangles           = enabled
Triangles Font Clock-Wise= enabled
Fast Build AS            = disabled
Target FPS               = 0
Target UPS               = 60
Tearing                  = enabled
VSync                    = disabled
GUI rendering            = disabled
Debug layer              = enabled
GPU validation           = disabled
GPU profiling            = enabled
Render mode              = Ray Tracing
Loaded scene file        = Cube/Cube.gltf
Using testing scene      = disabled
Duplication              = 1
Duplication in cube      = disabled
Duplication offset       = 10
BLAS duplication         = disabled
Rays per pixel           = 14
Quality preset           = High
Rasterization Quality    : 
  AO                     = enabled
  AO kernel size         = 4
  Shadows                = enabled
  Shadow map resolution  = 4096
  Shadow PCF kernel size = 4
  Reflections            = enabled
Ray Tracing Quality      : 
  AO                     = enabled
  AO rays                = 4
  AO kernel size         = 4
  Shadows                = enabled
  Shadow rays            = 4
  Shadow kernel size     = 4
  Reflections            = enabled

ProfilingAggregator: 
Render	,	6.9481	,	7.1669	,	9.06	,	7.9944	,	9.8111	,	10.1966	,	9.6442	,	12.0663	,	11.3984	,	9.1098	,	8.7124	,	11.7193	,	10.4049	,	8.9468	,	9.585	,	8.0382	,	11.6555	,	13.7743	,	10.4304	,	9.4501	,	8.6854	,	8.235	,	7.4273	,	9.1963	,	8.4467	,	5.7745	,	6.4196	,	9.7162	,	5.4772
Update	,	1.1086	,	1.1051	,	1.223	,	0.857	,	1.3285	,	1.1609	,	1.2506	,	1.108	,	1.4761	,	1.4057	,	1.0385	,	1.4113	,	1.1762	,	1.1257	,	1.2974	,	0.6874	,	1.3752	,	1.6198	,	0.9412	,	0.991	,	0.7705	,	1.148	,	1.047	,	0.6981	,	1.0867	,	0.9953	,	1.019	,	0.4937	,	0.6874
RayTracing	,	1.9051	,	2.0562	,	3.0427	,	2.3085	,	3.2123	,	3.1418	,	3.1566	,	4.4726	,	3.8322	,	2.3867	,	2.2883	,	3.8883	,	3.3364	,	2.7538	,	2.9933	,	1.6744	,	3.5978	,	4.4858	,	2.5114	,	1.7512	,	1.6447	,	2.2216	,	2.0646	,	3.5225	,	3.1073	,	1.3238	,	1.9129	,	4.3791	,	1.4804
RayTracingGpu	,	3.36544	,	3.75552	,	3.80173	,	3.87309	,	4.44122	,	4.8584	,	4.27114	,	4.5625	,	4.96547	,	4.5943	,	4.75296	,	5.00941	,	4.79344	,	4.16368	,	4.45667	,	4.8784	,	5.79258	,	6.3537	,	6.10771	,	6.13856	,	5.49523	,	4.32384	,	3.75926	,	3.4151	,	3.21363	,	3.08272	,	2.79286	,	2.684	,	2.62269
DeferredRLGpu	,	0.181824	,	0.240448	,	0.251264	,	0.2568	,	0.270208	,	0.332224	,	0.305504	,	0.3504	,	0.408224	,	0.356032	,	0.321984	,	0.343072	,	0.27328	,	0.24864	,	0.28528	,	0.334016	,	0.3928	,	0.461984	,	0.4808	,	0.484864	,	0.395616	,	0.282368	,	0.22848	,	0.177632	,	0.14864	,	0.115648	,	0.087936	,	0.07264	,	0.062496
RayTracingRLGpu	,	0.829472	,	1.1209	,	1.17363	,	1.23082	,	1.6489	,	1.97789	,	1.55578	,	1.78966	,	2.092	,	1.8153	,	2.02029	,	2.14608	,	1.9887	,	1.52915	,	1.77571	,	2.11766	,	2.80762	,	3.29578	,	3.16387	,	3.19923	,	2.64966	,	1.65312	,	1.16493	,	0.89824	,	0.7416	,	0.561344	,	0.41856	,	0.340704	,	0.297312
ResolveRLGpu	,	2.35299	,	2.39296	,	2.37578	,	2.3847	,	2.52102	,	2.54701	,	2.40893	,	2.42141	,	2.46422	,	2.42195	,	2.40976	,	2.51949	,	2.53056	,	2.38454	,	2.39478	,	2.42544	,	2.59123	,	2.59466	,	2.46189	,	2.45338	,	2.44893	,	2.38758	,	2.36461	,	2.33805	,	2.32234	,	2.40454	,	2.28531	,	2.26957	,	2.26186
Index	,	0	,	1	,	2	,	3	,	4	,	5	,	6	,	7	,	8	,	9	,	10	,	11	,	12	,	13	,	14	,	15	,	16	,	17	,	18	,	19	,	20	,	21	,	22	,	23	,	24	,	25	,	26	,	27	,	28

ProfilingBuildAggregator: 
BuildBottomLevelAS	,	0.9003
BuildBottomLevelASGpu	,	0.120672
BuildTopLevelAS	,	0.7382
BuildTopLevelASGpu	,	0.082944
Duplication	,	1
Triangles	,	12
Meshes	,	1
BottomLevels	,	1
TopLevelSize	,	64
BottomLevelsSize	,	5168
Index	,	0

FpsAggregator: 
FPS	,	103	,	105	,	106	,	106	,	103	,	95	,	95	,	96	,	96	,	93	,	95	,	94	,	94	,	100	,	96	,	95	,	92	,	85	,	84	,	84	,	84	,	93	,	103	,	113	,	120	,	126	,	139	,	144	,	146
UPS	,	59	,	60	,	61	,	60	,	61	,	60	,	60	,	60	,	61	,	60	,	61	,	60	,	60	,	60	,	61	,	60	,	60	,	60	,	61	,	60	,	60	,	60	,	61	,	60	,	60	,	61	,	60	,	60	,	61
FrameTime	,	9.71134	,	9.57699	,	9.51597	,	9.4621	,	9.8113	,	10.5427	,	10.5523	,	10.5065	,	10.5476	,	10.7859	,	10.5782	,	10.6856	,	10.666	,	10.0504	,	10.4626	,	10.5614	,	10.9437	,	11.768	,	12.0124	,	11.9625	,	11.9136	,	10.7565	,	9.78944	,	8.91379	,	8.34655	,	7.97762	,	7.24037	,	7.00678	,	6.88329
GigaRays/s	,	5.24424	,	5.31781	,	5.35191	,	5.38238	,	5.19081	,	4.83071	,	4.82632	,	4.84734	,	4.82844	,	4.7218	,	4.81448	,	4.7661	,	4.77486	,	5.06735	,	4.8677	,	4.82216	,	4.65369	,	4.32773	,	4.23966	,	4.25736	,	4.27482	,	4.73467	,	5.20241	,	5.71347	,	6.10176	,	6.38394	,	7.03398	,	7.26848	,	7.39888
Index	,	0	,	1	,	2	,	3	,	4	,	5	,	6	,	7	,	8	,	9	,	10	,	11	,	12	,	13	,	14	,	15	,	16	,	17	,	18	,	19	,	20	,	21	,	22	,	23	,	24	,	25	,	26	,	27	,	28


AccelerationStructure: 
Ray tracing acceleration structure building statistics: 
	Bottom Level Acceleration Structure: 
		Maximum [B]: 5168
		Minimum [B]: 5168
		Currently Allocated [B]: 5168
		Currently Created [num]: 1
		Current Average [B]: 5168
		Maximum Triangles [num]: 12
		Minimum Triangles [num]: 12
		Total Triangles [num]: 12
		Maximum Meshes [num]: 1
		Minimum Meshes [num]: 1
		Total Meshes [num]: 1
	Top Level Acceleration Structure: 
		Maximum [B]: 64
		Minimum [B]: 64
		Currently Allocated [B]: 64
		Total Allocated [B]: 64
		Total Created [num]: 1
		Average Allocated [B]: 64
		Maximum Instances [num]: 1
		Minimum Instances [num]: 1
		Total Instances [num]: 1
	Scratch Buffer: 
		Maximum [B]: 3584
		Minimum [B]: 3584
		Currently Allocated [B]: 3584
		Total Allocated [B]: 3584
		Total Created [num]: 1
		Average Allocated [B]: 3584
ProfilingStack: 
[PROF] Profiling statistics: 
	Scopes entered : 36927
	Scopes exited : 36926
	Overhead per scope [ticks] : 1001
	Threads entered : 3
	Threads exited : 0
	============================
		         Sum[Mt]   Parent[%]  Samples[num]       Avg[Mt]   Last/Curr[Mt]        Self[Mt]     Self[%] 	 Name

	main:

		   29850.693 Mt   100.000 %         1 x    29850.697 Mt    29850.691 Mt        0.741 Mt     0.002 % 	 /

		   29850.107 Mt    99.998 %         1 x    29850.114 Mt    29850.106 Mt      391.273 Mt     1.311 % 	 ./Main application loop

		       2.983 Mt     0.010 %         1 x        2.983 Mt        0.000 Mt        2.983 Mt   100.000 % 	 ../TexturedRLInitialization

		       2.443 Mt     0.008 %         1 x        2.443 Mt        0.000 Mt        2.443 Mt   100.000 % 	 ../ShadowMapRLInitialization

		       4.188 Mt     0.014 %         1 x        4.188 Mt        0.000 Mt        4.188 Mt   100.000 % 	 ../AmbientOcclusionRLInitialization

		       5.838 Mt     0.020 %         1 x        5.838 Mt        0.000 Mt        5.838 Mt   100.000 % 	 ../RasterResolveRLInitialization

		     175.878 Mt     0.589 %         1 x      175.878 Mt        0.000 Mt        5.352 Mt     3.043 % 	 ../Initialization

		     170.526 Mt    96.957 %         1 x      170.526 Mt        0.000 Mt        2.274 Mt     1.334 % 	 .../ScenePrep

		     142.742 Mt    83.707 %         1 x      142.742 Mt        0.000 Mt       25.474 Mt    17.846 % 	 ..../SceneLoad

		      81.665 Mt    57.211 %         1 x       81.665 Mt        0.000 Mt       33.252 Mt    40.718 % 	 ...../glTF-LoadScene

		      48.413 Mt    59.282 %         2 x       24.206 Mt        0.000 Mt       48.413 Mt   100.000 % 	 ....../glTF-LoadImageData

		      10.513 Mt     7.365 %         1 x       10.513 Mt        0.000 Mt       10.513 Mt   100.000 % 	 ...../glTF-CreateScene

		      25.090 Mt    17.577 %         1 x       25.090 Mt        0.000 Mt       25.090 Mt   100.000 % 	 ...../SceneDesc-CreateScene

		       0.724 Mt     0.425 %         1 x        0.724 Mt        0.000 Mt        0.009 Mt     1.243 % 	 ..../ShadowMapRLPrep

		       0.715 Mt    98.757 %         1 x        0.715 Mt        0.000 Mt        0.651 Mt    91.076 % 	 ...../PrepareForRendering

		       0.064 Mt     8.924 %         1 x        0.064 Mt        0.000 Mt        0.064 Mt   100.000 % 	 ....../PrepareDrawBundle

		       1.703 Mt     0.999 %         1 x        1.703 Mt        0.000 Mt        0.005 Mt     0.270 % 	 ..../TexturedRLPrep

		       1.699 Mt    99.730 %         1 x        1.699 Mt        0.000 Mt        0.640 Mt    37.688 % 	 ...../PrepareForRendering

		       0.788 Mt    46.412 %         1 x        0.788 Mt        0.000 Mt        0.788 Mt   100.000 % 	 ....../PrepareMaterials

		       0.270 Mt    15.900 %         1 x        0.270 Mt        0.000 Mt        0.270 Mt   100.000 % 	 ....../PrepareDrawBundle

		       2.973 Mt     1.743 %         1 x        2.973 Mt        0.000 Mt        0.005 Mt     0.158 % 	 ..../DeferredRLPrep

		       2.968 Mt    99.842 %         1 x        2.968 Mt        0.000 Mt        2.104 Mt    70.878 % 	 ...../PrepareForRendering

		       0.016 Mt     0.529 %         1 x        0.016 Mt        0.000 Mt        0.016 Mt   100.000 % 	 ....../PrepareMaterials

		       0.571 Mt    19.231 %         1 x        0.571 Mt        0.000 Mt        0.571 Mt   100.000 % 	 ....../BuildMaterialCache

		       0.278 Mt     9.363 %         1 x        0.278 Mt        0.000 Mt        0.278 Mt   100.000 % 	 ....../PrepareDrawBundle

		      11.466 Mt     6.724 %         1 x       11.466 Mt        0.000 Mt        4.284 Mt    37.362 % 	 ..../RayTracingRLPrep

		       0.235 Mt     2.047 %         1 x        0.235 Mt        0.000 Mt        0.235 Mt   100.000 % 	 ...../PrepareAccelerationStructures

		       0.023 Mt     0.202 %         1 x        0.023 Mt        0.000 Mt        0.023 Mt   100.000 % 	 ...../PrepareCache

		       1.536 Mt    13.397 %         1 x        1.536 Mt        0.000 Mt        1.536 Mt   100.000 % 	 ...../BuildCache

		       3.776 Mt    32.930 %         1 x        3.776 Mt        0.000 Mt        0.005 Mt     0.124 % 	 ...../BuildAccelerationStructures

		       3.771 Mt    99.876 %         1 x        3.771 Mt        0.000 Mt        2.133 Mt    56.551 % 	 ....../AccelerationStructureBuild

		       0.900 Mt    23.874 %         1 x        0.900 Mt        0.000 Mt        0.900 Mt   100.000 % 	 ......./BuildBottomLevelAS

		       0.738 Mt    19.575 %         1 x        0.738 Mt        0.000 Mt        0.738 Mt   100.000 % 	 ......./BuildTopLevelAS

		       1.613 Mt    14.063 %         1 x        1.613 Mt        0.000 Mt        1.613 Mt   100.000 % 	 ...../BuildShaderTables

		       8.643 Mt     5.069 %         1 x        8.643 Mt        0.000 Mt        8.643 Mt   100.000 % 	 ..../GpuSidePrep

		       5.166 Mt     0.017 %         1 x        5.166 Mt        0.000 Mt        5.166 Mt   100.000 % 	 ../DeferredRLInitialization

		      71.335 Mt     0.239 %         1 x       71.335 Mt        0.000 Mt        2.525 Mt     3.540 % 	 ../RayTracingRLInitialization

		      68.810 Mt    96.460 %         1 x       68.810 Mt        0.000 Mt       68.810 Mt   100.000 % 	 .../PipelineBuild

		       5.485 Mt     0.018 %         1 x        5.485 Mt        0.000 Mt        5.485 Mt   100.000 % 	 ../ResolveRLInitialization

		   29185.518 Mt    97.774 %      8249 x        3.538 Mt       12.400 Mt     1182.062 Mt     4.050 % 	 ../MainLoop

		    2012.869 Mt     6.897 %      1751 x        1.150 Mt        1.085 Mt     2008.726 Mt    99.794 % 	 .../Update

		       4.143 Mt     0.206 %         1 x        4.143 Mt        0.000 Mt        4.143 Mt   100.000 % 	 ..../GuiModelApply

		   25990.587 Mt    89.053 %      2987 x        8.701 Mt        8.918 Mt    18390.828 Mt    70.760 % 	 .../Render

		    7599.759 Mt    29.240 %      2987 x        2.544 Mt        3.408 Mt     7562.689 Mt    99.512 % 	 ..../RayTracing

		      20.408 Mt     0.269 %      2987 x        0.007 Mt        0.007 Mt       20.408 Mt   100.000 % 	 ...../DeferredRLPrep

		      16.662 Mt     0.219 %      2987 x        0.006 Mt        0.006 Mt       16.662 Mt   100.000 % 	 ...../RayTracingRLPrep

	WindowThread:

		   29842.681 Mt   100.000 %         1 x    29842.682 Mt    29842.680 Mt    29842.681 Mt   100.000 % 	 /

	GpuThread:

		   12826.603 Mt   100.000 %      2987 x        4.294 Mt        2.936 Mt      158.834 Mt     1.238 % 	 /

		       0.211 Mt     0.002 %         1 x        0.211 Mt        0.000 Mt        0.006 Mt     2.883 % 	 ./ScenePrep

		       0.205 Mt    97.117 %         1 x        0.205 Mt        0.000 Mt        0.001 Mt     0.594 % 	 ../AccelerationStructureBuild

		       0.121 Mt    58.913 %         1 x        0.121 Mt        0.000 Mt        0.121 Mt   100.000 % 	 .../BuildBottomLevelASGpu

		       0.083 Mt    40.494 %         1 x        0.083 Mt        0.000 Mt        0.083 Mt   100.000 % 	 .../BuildTopLevelASGpu

		   12585.841 Mt    98.123 %      2987 x        4.214 Mt        2.732 Mt        3.204 Mt     0.025 % 	 ./RayTracingGpu

		     794.447 Mt     6.312 %      2987 x        0.266 Mt        0.063 Mt      794.447 Mt   100.000 % 	 ../DeferredRLGpu

		    4563.014 Mt    36.255 %      2987 x        1.528 Mt        0.298 Mt     4563.014 Mt   100.000 % 	 ../RayTracingRLGpu

		    7225.176 Mt    57.407 %      2987 x        2.419 Mt        2.370 Mt     7225.176 Mt   100.000 % 	 ../ResolveRLGpu

		      81.717 Mt     0.637 %      2987 x        0.027 Mt        0.203 Mt       81.717 Mt   100.000 % 	 ./Present


	============================


