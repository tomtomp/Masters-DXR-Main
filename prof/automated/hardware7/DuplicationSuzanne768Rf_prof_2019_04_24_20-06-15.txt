Configuration: 
Command line parameters  = E:\programming\FIT-DIP\Masters-DXR-Main-git\build\Measurement\x64\Debug\Measurement.exe --scene Suzanne/Suzanne.gltf --duplication-cube --profile-duplication --profile-max-duplication 10 --rt-mode --width 1024 --height 768 --profile-output DuplicationSuzanne768Rf --fast-build-as 
Window width             = 1024
Window height            = 768
Window title             = Direct3D App
Back-buffer count        = 3
Fill Triangles           = enabled
Triangles Font Clock-Wise= enabled
Fast Build AS            = enabled
Target FPS               = 60
Target UPS               = 60
Tearing                  = disabled
VSync                    = disabled
GUI rendering            = disabled
Debug layer              = enabled
GPU validation           = disabled
GPU profiling            = enabled
Render mode              = Ray Tracing
Loaded scene file        = Suzanne/Suzanne.gltf
Using testing scene      = disabled
Duplication              = 10
Duplication in cube      = enabled
Duplication offset       = 10
BLAS duplication         = enabled
Rays per pixel           = 14
Quality preset           = High
Rasterization Quality    : 
  AO                     = enabled
  AO kernel size         = 4
  Shadows                = enabled
  Shadow map resolution  = 4096
  Shadow PCF kernel size = 4
  Reflections            = enabled
Ray Tracing Quality      : 
  AO                     = enabled
  AO rays                = 4
  AO kernel size         = 4
  Shadows                = enabled
  Shadow rays            = 4
  Shadow kernel size     = 4
  Reflections            = enabled

ProfilingAggregator: 

ProfilingBuildAggregator: 

FpsAggregator: 


AccelerationStructure: 
Ray tracing acceleration structure building statistics: 
	Bottom Level Acceleration Structure: 
		Maximum [B]: 224128
		Minimum [B]: 224128
		Currently Allocated [B]: 224128000
		Currently Created [num]: 1000
		Current Average [B]: 224128
		Maximum Triangles [num]: 3936
		Minimum Triangles [num]: 3936
		Total Triangles [num]: 3936000
		Maximum Meshes [num]: 1
		Minimum Meshes [num]: 1
		Total Meshes [num]: 1000
	Top Level Acceleration Structure: 
		Maximum [B]: 64000
		Minimum [B]: 64000
		Currently Allocated [B]: 64000
		Total Allocated [B]: 64000
		Total Created [num]: 1
		Average Allocated [B]: 64000
		Maximum Instances [num]: 1000
		Minimum Instances [num]: 1000
		Total Instances [num]: 1000
	Scratch Buffer: 
		Maximum [B]: 79360
		Minimum [B]: 79360
		Currently Allocated [B]: 79360
		Total Allocated [B]: 79360
		Total Created [num]: 1
		Average Allocated [B]: 79360
ProfilingStack: 
[PROF] Profiling statistics: 
	Scopes entered : 3029826
	Scopes exited : 3029825
	Overhead per scope [ticks] : 1008.8
	Threads entered : 3
	Threads exited : 0
	============================
		         Sum[Mt]   Parent[%]  Samples[num]       Avg[Mt]   Last/Curr[Mt]        Self[Mt]     Self[%] 	 Name

	main:

		   21731.559 Mt   100.000 %         1 x    21731.561 Mt    21731.558 Mt        0.730 Mt     0.003 % 	 /

		   21730.871 Mt    99.997 %         1 x    21730.875 Mt    21730.871 Mt     5445.143 Mt    25.057 % 	 ./Main application loop

		       2.892 Mt     0.013 %         1 x        2.892 Mt        0.000 Mt        2.892 Mt   100.000 % 	 ../TexturedRLInitialization

		       2.515 Mt     0.012 %         1 x        2.515 Mt        0.000 Mt        2.515 Mt   100.000 % 	 ../ShadowMapRLInitialization

		       3.832 Mt     0.018 %         1 x        3.832 Mt        0.000 Mt        3.832 Mt   100.000 % 	 ../AmbientOcclusionRLInitialization

		       6.303 Mt     0.029 %         1 x        6.303 Mt        0.000 Mt        6.303 Mt   100.000 % 	 ../RasterResolveRLInitialization

		     249.069 Mt     1.146 %         1 x      249.069 Mt        0.000 Mt       12.243 Mt     4.916 % 	 ../Initialization

		     236.826 Mt    95.084 %         1 x      236.826 Mt        0.000 Mt        1.441 Mt     0.608 % 	 .../ScenePrep

		     203.730 Mt    86.026 %         1 x      203.730 Mt        0.000 Mt       18.485 Mt     9.073 % 	 ..../SceneLoad

		     163.539 Mt    80.272 %         1 x      163.539 Mt        0.000 Mt       19.326 Mt    11.817 % 	 ...../glTF-LoadScene

		     144.213 Mt    88.183 %         2 x       72.106 Mt        0.000 Mt      144.213 Mt   100.000 % 	 ....../glTF-LoadImageData

		      14.361 Mt     7.049 %         1 x       14.361 Mt        0.000 Mt       14.361 Mt   100.000 % 	 ...../glTF-CreateScene

		       7.346 Mt     3.606 %         1 x        7.346 Mt        0.000 Mt        7.346 Mt   100.000 % 	 ...../SceneDesc-CreateScene

		       0.647 Mt     0.273 %         1 x        0.647 Mt        0.000 Mt        0.005 Mt     0.696 % 	 ..../ShadowMapRLPrep

		       0.642 Mt    99.304 %         1 x        0.642 Mt        0.000 Mt        0.581 Mt    90.395 % 	 ...../PrepareForRendering

		       0.062 Mt     9.605 %         1 x        0.062 Mt        0.000 Mt        0.062 Mt   100.000 % 	 ....../PrepareDrawBundle

		       1.112 Mt     0.469 %         1 x        1.112 Mt        0.000 Mt        0.004 Mt     0.405 % 	 ..../TexturedRLPrep

		       1.107 Mt    99.595 %         1 x        1.107 Mt        0.000 Mt        0.371 Mt    33.502 % 	 ...../PrepareForRendering

		       0.476 Mt    42.977 %         1 x        0.476 Mt        0.000 Mt        0.476 Mt   100.000 % 	 ....../PrepareMaterials

		       0.260 Mt    23.521 %         1 x        0.260 Mt        0.000 Mt        0.260 Mt   100.000 % 	 ....../PrepareDrawBundle

		       3.331 Mt     1.407 %         1 x        3.331 Mt        0.000 Mt        0.005 Mt     0.156 % 	 ..../DeferredRLPrep

		       3.326 Mt    99.844 %         1 x        3.326 Mt        0.000 Mt        1.629 Mt    48.994 % 	 ...../PrepareForRendering

		       0.016 Mt     0.484 %         1 x        0.016 Mt        0.000 Mt        0.016 Mt   100.000 % 	 ....../PrepareMaterials

		       1.020 Mt    30.677 %         1 x        1.020 Mt        0.000 Mt        1.020 Mt   100.000 % 	 ....../BuildMaterialCache

		       0.660 Mt    19.844 %         1 x        0.660 Mt        0.000 Mt        0.660 Mt   100.000 % 	 ....../PrepareDrawBundle

		      14.424 Mt     6.091 %         1 x       14.424 Mt        0.000 Mt        3.952 Mt    27.400 % 	 ..../RayTracingRLPrep

		       0.563 Mt     3.900 %         1 x        0.563 Mt        0.000 Mt        0.563 Mt   100.000 % 	 ...../PrepareAccelerationStructures

		       0.056 Mt     0.387 %         1 x        0.056 Mt        0.000 Mt        0.056 Mt   100.000 % 	 ...../PrepareCache

		       0.403 Mt     2.795 %         1 x        0.403 Mt        0.000 Mt        0.403 Mt   100.000 % 	 ...../BuildCache

		       7.763 Mt    53.818 %         1 x        7.763 Mt        0.000 Mt        0.006 Mt     0.075 % 	 ...../BuildAccelerationStructures

		       7.757 Mt    99.925 %         1 x        7.757 Mt        0.000 Mt        6.015 Mt    77.548 % 	 ....../AccelerationStructureBuild

		       0.859 Mt    11.078 %         1 x        0.859 Mt        0.000 Mt        0.859 Mt   100.000 % 	 ......./BuildBottomLevelAS

		       0.882 Mt    11.374 %         1 x        0.882 Mt        0.000 Mt        0.882 Mt   100.000 % 	 ......./BuildTopLevelAS

		       1.688 Mt    11.700 %         1 x        1.688 Mt        0.000 Mt        1.688 Mt   100.000 % 	 ...../BuildShaderTables

		      12.141 Mt     5.126 %         1 x       12.141 Mt        0.000 Mt       12.141 Mt   100.000 % 	 ..../GpuSidePrep

		       5.351 Mt     0.025 %         1 x        5.351 Mt        0.000 Mt        5.351 Mt   100.000 % 	 ../DeferredRLInitialization

		      62.720 Mt     0.289 %         1 x       62.720 Mt        0.000 Mt        2.070 Mt     3.301 % 	 ../RayTracingRLInitialization

		      60.650 Mt    96.699 %         1 x       60.650 Mt        0.000 Mt       60.650 Mt   100.000 % 	 .../PipelineBuild

		       5.097 Mt     0.023 %         1 x        5.097 Mt        0.000 Mt        5.097 Mt   100.000 % 	 ../ResolveRLInitialization

		   15947.948 Mt    73.388 %   3019913 x        0.005 Mt       10.832 Mt     5070.984 Mt    31.797 % 	 ../MainLoop

		    1990.607 Mt    12.482 %      1220 x        1.632 Mt        1.645 Mt     1989.385 Mt    99.939 % 	 .../Update

		       1.222 Mt     0.061 %         1 x        1.222 Mt        0.000 Mt        1.222 Mt   100.000 % 	 ..../GuiModelApply

		    8886.357 Mt    55.721 %       941 x        9.444 Mt        0.000 Mt     4538.459 Mt    51.072 % 	 .../Render

		    4347.898 Mt    48.928 %       941 x        4.621 Mt        0.000 Mt     2618.888 Mt    60.233 % 	 ..../RayTracing

		      91.571 Mt     2.106 %       941 x        0.097 Mt        0.000 Mt        2.590 Mt     2.828 % 	 ...../DeferredRLPrep

		      88.981 Mt    97.172 %        12 x        7.415 Mt        0.000 Mt        4.561 Mt     5.125 % 	 ....../PrepareForRendering

		       8.518 Mt     9.572 %        12 x        0.710 Mt        0.000 Mt        8.518 Mt   100.000 % 	 ......./PrepareMaterials

		       0.010 Mt     0.011 %        12 x        0.001 Mt        0.000 Mt        0.010 Mt   100.000 % 	 ......./BuildMaterialCache

		      75.894 Mt    85.292 %        12 x        6.324 Mt        0.000 Mt       75.894 Mt   100.000 % 	 ......./PrepareDrawBundle

		    1637.438 Mt    37.660 %       941 x        1.740 Mt        0.000 Mt       21.848 Mt     1.334 % 	 ...../RayTracingRLPrep

		     693.666 Mt    42.363 %        12 x       57.806 Mt        0.000 Mt      693.666 Mt   100.000 % 	 ....../PrepareAccelerationStructures

		       8.280 Mt     0.506 %        12 x        0.690 Mt        0.000 Mt        8.280 Mt   100.000 % 	 ....../PrepareCache

		       0.008 Mt     0.000 %        12 x        0.001 Mt        0.000 Mt        0.008 Mt   100.000 % 	 ....../BuildCache

		     834.384 Mt    50.957 %        12 x       69.532 Mt        0.000 Mt        0.029 Mt     0.004 % 	 ....../BuildAccelerationStructures

		     834.354 Mt    99.996 %        12 x       69.530 Mt        0.000 Mt       29.525 Mt     3.539 % 	 ......./AccelerationStructureBuild

		     795.716 Mt    95.369 %        12 x       66.310 Mt        0.000 Mt      795.716 Mt   100.000 % 	 ......../BuildBottomLevelAS

		       9.114 Mt     1.092 %        12 x        0.759 Mt        0.000 Mt        9.114 Mt   100.000 % 	 ......../BuildTopLevelAS

		      79.253 Mt     4.840 %        12 x        6.604 Mt        0.000 Mt       79.253 Mt   100.000 % 	 ....../BuildShaderTables

	WindowThread:

		   21727.416 Mt   100.000 %         1 x    21727.416 Mt    21727.416 Mt    21727.416 Mt   100.000 % 	 /

	GpuThread:

		    3423.266 Mt   100.000 %       941 x        3.638 Mt        0.000 Mt      124.091 Mt     3.625 % 	 /

		       0.448 Mt     0.013 %         1 x        0.448 Mt        0.000 Mt        0.006 Mt     1.321 % 	 ./ScenePrep

		       0.442 Mt    98.679 %         1 x        0.442 Mt        0.000 Mt        0.001 Mt     0.318 % 	 ../AccelerationStructureBuild

		       0.356 Mt    80.431 %         1 x        0.356 Mt        0.000 Mt        0.356 Mt   100.000 % 	 .../BuildBottomLevelASGpu

		       0.085 Mt    19.251 %         1 x        0.085 Mt        0.000 Mt        0.085 Mt   100.000 % 	 .../BuildTopLevelASGpu

		    3270.330 Mt    95.532 %       941 x        3.475 Mt        0.000 Mt        2.557 Mt     0.078 % 	 ./RayTracingGpu

		     203.240 Mt     6.215 %       941 x        0.216 Mt        0.000 Mt      203.240 Mt   100.000 % 	 ../DeferredRLGpu

		     285.322 Mt     8.725 %       941 x        0.303 Mt        0.000 Mt      285.322 Mt   100.000 % 	 ../RayTracingRLGpu

		     979.390 Mt    29.948 %       941 x        1.041 Mt        0.000 Mt      979.390 Mt   100.000 % 	 ../ResolveRLGpu

		    1799.822 Mt    55.035 %        12 x      149.985 Mt        0.000 Mt        0.033 Mt     0.002 % 	 ../AccelerationStructureBuild

		    1796.026 Mt    99.789 %        12 x      149.669 Mt        0.000 Mt     1796.026 Mt   100.000 % 	 .../BuildBottomLevelASGpu

		       3.763 Mt     0.209 %        12 x        0.314 Mt        0.000 Mt        3.763 Mt   100.000 % 	 .../BuildTopLevelASGpu

		      28.396 Mt     0.830 %       941 x        0.030 Mt        0.000 Mt       28.396 Mt   100.000 % 	 ./Present


	============================


