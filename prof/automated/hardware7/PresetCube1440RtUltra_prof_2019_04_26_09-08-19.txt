Configuration: 
Command line parameters  = E:\programming\FIT-DIP\Masters-DXR-Main-git\build\Measurement\x64\Debug\Measurement.exe --scene Cube/Cube.gltf --profile-automation --profile-camera-track Cube/Cube.trk --rt-mode --width 2560 --height 1440 --profile-output PresetCube1440RtUltra --quality-preset Ultra 
Window width             = 2560
Window height            = 1421
Window title             = Direct3D App
Back-buffer count        = 3
Fill Triangles           = enabled
Triangles Font Clock-Wise= enabled
Fast Build AS            = disabled
Target FPS               = 0
Target UPS               = 60
Tearing                  = enabled
VSync                    = disabled
GUI rendering            = disabled
Debug layer              = enabled
GPU validation           = disabled
GPU profiling            = enabled
Render mode              = Ray Tracing
Loaded scene file        = Cube/Cube.gltf
Using testing scene      = disabled
Duplication              = 1
Duplication in cube      = disabled
Duplication offset       = 10
BLAS duplication         = disabled
Rays per pixel           = 34
Quality preset           = Ultra
Rasterization Quality    : 
  AO                     = enabled
  AO kernel size         = 16
  Shadows                = enabled
  Shadow map resolution  = 8192
  Shadow PCF kernel size = 8
  Reflections            = enabled
Ray Tracing Quality      : 
  AO                     = enabled
  AO rays                = 16
  AO kernel size         = 8
  Shadows                = enabled
  Shadow rays            = 8
  Shadow kernel size     = 8
  Reflections            = enabled

ProfilingAggregator: 
Render	,	17.1459	,	17.8332	,	16.1058	,	18.7684	,	17.5972	,	17.782	,	20.2203	,	20.6329	,	21.0609	,	21.1831	,	21.3045	,	22.1336	,	19.8495	,	20.0778	,	20.0021	,	21.5793	,	23.2153	,	23.9568	,	24.8508	,	29.0741	,	22.825	,	22.6232	,	17.2718	,	17.9259	,	16.5292	,	16.6608	,	15.1255	,	15.0829	,	14.5551
Update	,	1.4451	,	1.5463	,	1.0179	,	1.387	,	1.2436	,	1.1037	,	1.6819	,	1.6157	,	1.2035	,	1.662	,	1.3466	,	1.638	,	1.5729	,	1.5319	,	1.3949	,	1.5221	,	1.5126	,	1.324	,	1.5192	,	1.9823	,	1.6616	,	1.5431	,	1.4243	,	1.661	,	1.6507	,	1.5477	,	1.4015	,	1.4557	,	1.326
RayTracing	,	4.2493	,	4.2646	,	2.7057	,	4.2214	,	3.1288	,	2.6394	,	4.5395	,	4.1485	,	3.7214	,	4.5652	,	4.1809	,	4.5516	,	4.1765	,	4.6151	,	3.9191	,	4.5258	,	4.5899	,	3.7404	,	4.399	,	7.0531	,	4.6481	,	3.9348	,	3.5181	,	4.5438	,	4.1184	,	4.1928	,	3.7184	,	3.9483	,	3.648
RayTracingGpu	,	9.79539	,	10.7535	,	11.2727	,	11.6338	,	11.9868	,	12.9732	,	12.4751	,	13.4484	,	14.5632	,	13.3628	,	14.0026	,	14.322	,	12.5527	,	12.1219	,	13.1171	,	14.0367	,	15.3322	,	17.33	,	17.2922	,	17.2755	,	14.7322	,	12.436	,	11.044	,	10.1181	,	9.68522	,	9.11203	,	8.49446	,	8.25846	,	8.12365
DeferredRLGpu	,	0.18096	,	0.241312	,	0.252512	,	0.260896	,	0.27536	,	0.335392	,	0.306752	,	0.358752	,	0.414656	,	0.335264	,	0.336352	,	0.34144	,	0.267584	,	0.255584	,	0.291712	,	0.343808	,	0.404224	,	0.477728	,	0.492064	,	0.490208	,	0.353568	,	0.275136	,	0.220704	,	0.172832	,	0.146496	,	0.109728	,	0.08512	,	0.0712	,	0.063648
RayTracingRLGpu	,	2.23802	,	3.06666	,	3.50282	,	3.82646	,	4.23299	,	5.17834	,	4.71453	,	5.56061	,	6.44147	,	5.45216	,	6.00938	,	6.35773	,	4.68237	,	4.45696	,	5.17888	,	6.20083	,	7.41344	,	9.11123	,	9.12842	,	9.14374	,	6.6231	,	4.53744	,	3.35706	,	2.56003	,	2.12666	,	1.5016	,	1.09846	,	0.846464	,	0.739008
ResolveRLGpu	,	7.37562	,	7.44458	,	7.51616	,	7.54557	,	7.47754	,	7.45821	,	7.45254	,	7.5281	,	7.70605	,	7.57389	,	7.65597	,	7.62205	,	7.60134	,	7.40838	,	7.64522	,	7.4911	,	7.5137	,	7.73955	,	7.67075	,	7.64054	,	7.75446	,	7.62182	,	7.46486	,	7.38426	,	7.41094	,	7.49952	,	7.30976	,	7.33968	,	7.32
Index	,	0	,	1	,	2	,	3	,	4	,	5	,	6	,	7	,	8	,	9	,	10	,	11	,	12	,	13	,	14	,	15	,	16	,	17	,	18	,	19	,	20	,	21	,	22	,	23	,	24	,	25	,	26	,	27	,	28

ProfilingBuildAggregator: 
BuildBottomLevelAS	,	1.0052
BuildBottomLevelASGpu	,	0.087936
BuildTopLevelAS	,	1.0639
BuildTopLevelASGpu	,	0.061856
Duplication	,	1
Triangles	,	12
Meshes	,	1
BottomLevels	,	1
TopLevelSize	,	64
BottomLevelsSize	,	5168
Index	,	0

FpsAggregator: 
FPS	,	52	,	52	,	51	,	51	,	49	,	45	,	45	,	45	,	42	,	42	,	45	,	42	,	44	,	48	,	45	,	43	,	41	,	39	,	36	,	36	,	37	,	44	,	49	,	52	,	54	,	58	,	61	,	64	,	64
UPS	,	59	,	61	,	61	,	61	,	60	,	60	,	61	,	60	,	62	,	61	,	61	,	61	,	61	,	61	,	60	,	61	,	61	,	61	,	61	,	61	,	61	,	61	,	61	,	60	,	61	,	60	,	61	,	61	,	61
FrameTime	,	19.4223	,	19.5965	,	19.8405	,	19.94	,	20.5176	,	22.2834	,	22.6174	,	22.2883	,	24.3549	,	24.3208	,	22.4974	,	24.3197	,	22.9764	,	21.1909	,	22.2552	,	23.5794	,	24.9053	,	26.2737	,	28.1239	,	28.426	,	27.3233	,	23.1808	,	20.5272	,	19.2913	,	18.7221	,	17.4214	,	16.5892	,	15.8514	,	15.8656
GigaRays/s	,	6.36814	,	6.31154	,	6.23391	,	6.20279	,	6.0282	,	5.5505	,	5.46852	,	5.54928	,	5.07839	,	5.08552	,	5.4977	,	5.08575	,	5.38308	,	5.83665	,	5.55754	,	5.24541	,	4.96616	,	4.70752	,	4.39782	,	4.35108	,	4.52668	,	5.33562	,	6.02535	,	6.41139	,	6.60631	,	7.09953	,	7.45566	,	7.80271	,	7.79573
Index	,	0	,	1	,	2	,	3	,	4	,	5	,	6	,	7	,	8	,	9	,	10	,	11	,	12	,	13	,	14	,	15	,	16	,	17	,	18	,	19	,	20	,	21	,	22	,	23	,	24	,	25	,	26	,	27	,	28


AccelerationStructure: 
Ray tracing acceleration structure building statistics: 
	Bottom Level Acceleration Structure: 
		Maximum [B]: 5168
		Minimum [B]: 5168
		Currently Allocated [B]: 5168
		Currently Created [num]: 1
		Current Average [B]: 5168
		Maximum Triangles [num]: 12
		Minimum Triangles [num]: 12
		Total Triangles [num]: 12
		Maximum Meshes [num]: 1
		Minimum Meshes [num]: 1
		Total Meshes [num]: 1
	Top Level Acceleration Structure: 
		Maximum [B]: 64
		Minimum [B]: 64
		Currently Allocated [B]: 64
		Total Allocated [B]: 64
		Total Created [num]: 1
		Average Allocated [B]: 64
		Maximum Instances [num]: 1
		Minimum Instances [num]: 1
		Total Instances [num]: 1
	Scratch Buffer: 
		Maximum [B]: 3584
		Minimum [B]: 3584
		Currently Allocated [B]: 3584
		Total Allocated [B]: 3584
		Total Created [num]: 1
		Average Allocated [B]: 3584
ProfilingStack: 
[PROF] Profiling statistics: 
	Scopes entered : 21019
	Scopes exited : 21018
	Overhead per scope [ticks] : 1033.8
	Threads entered : 3
	Threads exited : 0
	============================
		         Sum[Mt]   Parent[%]  Samples[num]       Avg[Mt]   Last/Curr[Mt]        Self[Mt]     Self[%] 	 Name

	main:

		   29941.381 Mt   100.000 %         1 x    29941.385 Mt    29941.379 Mt        0.745 Mt     0.002 % 	 /

		   29940.783 Mt    99.998 %         1 x    29940.790 Mt    29940.782 Mt      333.666 Mt     1.114 % 	 ./Main application loop

		       2.948 Mt     0.010 %         1 x        2.948 Mt        0.000 Mt        2.948 Mt   100.000 % 	 ../TexturedRLInitialization

		       2.338 Mt     0.008 %         1 x        2.338 Mt        0.000 Mt        2.338 Mt   100.000 % 	 ../ShadowMapRLInitialization

		       4.280 Mt     0.014 %         1 x        4.280 Mt        0.000 Mt        4.280 Mt   100.000 % 	 ../AmbientOcclusionRLInitialization

		       6.782 Mt     0.023 %         1 x        6.782 Mt        0.000 Mt        6.782 Mt   100.000 % 	 ../RasterResolveRLInitialization

		     113.543 Mt     0.379 %         1 x      113.543 Mt        0.000 Mt        3.631 Mt     3.198 % 	 ../Initialization

		     109.912 Mt    96.802 %         1 x      109.912 Mt        0.000 Mt        1.202 Mt     1.094 % 	 .../ScenePrep

		      77.179 Mt    70.219 %         1 x       77.179 Mt        0.000 Mt       12.081 Mt    15.654 % 	 ..../SceneLoad

		      57.446 Mt    74.432 %         1 x       57.446 Mt        0.000 Mt       12.419 Mt    21.620 % 	 ...../glTF-LoadScene

		      45.026 Mt    78.380 %         2 x       22.513 Mt        0.000 Mt       45.026 Mt   100.000 % 	 ....../glTF-LoadImageData

		       4.046 Mt     5.242 %         1 x        4.046 Mt        0.000 Mt        4.046 Mt   100.000 % 	 ...../glTF-CreateScene

		       3.606 Mt     4.672 %         1 x        3.606 Mt        0.000 Mt        3.606 Mt   100.000 % 	 ...../SceneDesc-CreateScene

		       0.469 Mt     0.426 %         1 x        0.469 Mt        0.000 Mt        0.005 Mt     1.003 % 	 ..../ShadowMapRLPrep

		       0.464 Mt    98.997 %         1 x        0.464 Mt        0.000 Mt        0.410 Mt    88.319 % 	 ...../PrepareForRendering

		       0.054 Mt    11.681 %         1 x        0.054 Mt        0.000 Mt        0.054 Mt   100.000 % 	 ....../PrepareDrawBundle

		       1.163 Mt     1.058 %         1 x        1.163 Mt        0.000 Mt        0.004 Mt     0.370 % 	 ..../TexturedRLPrep

		       1.159 Mt    99.630 %         1 x        1.159 Mt        0.000 Mt        0.387 Mt    33.431 % 	 ...../PrepareForRendering

		       0.507 Mt    43.763 %         1 x        0.507 Mt        0.000 Mt        0.507 Mt   100.000 % 	 ....../PrepareMaterials

		       0.264 Mt    22.805 %         1 x        0.264 Mt        0.000 Mt        0.264 Mt   100.000 % 	 ....../PrepareDrawBundle

		       4.082 Mt     3.714 %         1 x        4.082 Mt        0.000 Mt        0.004 Mt     0.108 % 	 ..../DeferredRLPrep

		       4.077 Mt    99.892 %         1 x        4.077 Mt        0.000 Mt        1.554 Mt    38.108 % 	 ...../PrepareForRendering

		       0.016 Mt     0.383 %         1 x        0.016 Mt        0.000 Mt        0.016 Mt   100.000 % 	 ....../PrepareMaterials

		       2.235 Mt    54.809 %         1 x        2.235 Mt        0.000 Mt        2.235 Mt   100.000 % 	 ....../BuildMaterialCache

		       0.273 Mt     6.700 %         1 x        0.273 Mt        0.000 Mt        0.273 Mt   100.000 % 	 ....../PrepareDrawBundle

		      15.642 Mt    14.231 %         1 x       15.642 Mt        0.000 Mt        4.139 Mt    26.461 % 	 ..../RayTracingRLPrep

		       0.237 Mt     1.517 %         1 x        0.237 Mt        0.000 Mt        0.237 Mt   100.000 % 	 ...../PrepareAccelerationStructures

		       0.024 Mt     0.156 %         1 x        0.024 Mt        0.000 Mt        0.024 Mt   100.000 % 	 ...../PrepareCache

		       0.488 Mt     3.121 %         1 x        0.488 Mt        0.000 Mt        0.488 Mt   100.000 % 	 ...../BuildCache

		       8.853 Mt    56.602 %         1 x        8.853 Mt        0.000 Mt        0.004 Mt     0.045 % 	 ...../BuildAccelerationStructures

		       8.849 Mt    99.955 %         1 x        8.849 Mt        0.000 Mt        6.780 Mt    76.619 % 	 ....../AccelerationStructureBuild

		       1.005 Mt    11.359 %         1 x        1.005 Mt        0.000 Mt        1.005 Mt   100.000 % 	 ......./BuildBottomLevelAS

		       1.064 Mt    12.022 %         1 x        1.064 Mt        0.000 Mt        1.064 Mt   100.000 % 	 ......./BuildTopLevelAS

		       1.899 Mt    12.144 %         1 x        1.899 Mt        0.000 Mt        1.899 Mt   100.000 % 	 ...../BuildShaderTables

		      10.176 Mt     9.258 %         1 x       10.176 Mt        0.000 Mt       10.176 Mt   100.000 % 	 ..../GpuSidePrep

		       5.188 Mt     0.017 %         1 x        5.188 Mt        0.000 Mt        5.188 Mt   100.000 % 	 ../DeferredRLInitialization

		      50.965 Mt     0.170 %         1 x       50.965 Mt        0.000 Mt        2.204 Mt     4.325 % 	 ../RayTracingRLInitialization

		      48.761 Mt    95.675 %         1 x       48.761 Mt        0.000 Mt       48.761 Mt   100.000 % 	 .../PipelineBuild

		       5.304 Mt     0.018 %         1 x        5.304 Mt        0.000 Mt        5.304 Mt   100.000 % 	 ../ResolveRLInitialization

		   29415.768 Mt    98.246 %      6819 x        4.314 Mt       40.597 Mt      554.412 Mt     1.885 % 	 ../MainLoop

		    2662.103 Mt     9.050 %      1763 x        1.510 Mt        0.966 Mt     2658.163 Mt    99.852 % 	 .../Update

		       3.940 Mt     0.148 %         1 x        3.940 Mt        0.000 Mt        3.940 Mt   100.000 % 	 ..../GuiModelApply

		   26199.253 Mt    89.065 %      1377 x       19.026 Mt       13.110 Mt    20743.031 Mt    79.174 % 	 .../Render

		    5456.222 Mt    20.826 %      1377 x        3.962 Mt        2.547 Mt     5433.309 Mt    99.580 % 	 ..../RayTracing

		      11.545 Mt     0.212 %      1377 x        0.008 Mt        0.007 Mt       11.545 Mt   100.000 % 	 ...../DeferredRLPrep

		      11.368 Mt     0.208 %      1377 x        0.008 Mt        0.006 Mt       11.368 Mt   100.000 % 	 ...../RayTracingRLPrep

	WindowThread:

		   29939.384 Mt   100.000 %         1 x    29939.385 Mt    29939.383 Mt    29939.384 Mt   100.000 % 	 /

	GpuThread:

		   16960.817 Mt   100.000 %      1377 x       12.317 Mt        8.265 Mt      160.348 Mt     0.945 % 	 /

		       0.156 Mt     0.001 %         1 x        0.156 Mt        0.000 Mt        0.005 Mt     3.357 % 	 ./ScenePrep

		       0.151 Mt    96.643 %         1 x        0.151 Mt        0.000 Mt        0.001 Mt     0.868 % 	 ../AccelerationStructureBuild

		       0.088 Mt    58.196 %         1 x        0.088 Mt        0.000 Mt        0.088 Mt   100.000 % 	 .../BuildBottomLevelASGpu

		       0.062 Mt    40.936 %         1 x        0.062 Mt        0.000 Mt        0.062 Mt   100.000 % 	 .../BuildTopLevelASGpu

		   16721.450 Mt    98.589 %      1377 x       12.143 Mt        8.178 Mt        1.636 Mt     0.010 % 	 ./RayTracingGpu

		     365.474 Mt     2.186 %      1377 x        0.265 Mt        0.066 Mt      365.474 Mt   100.000 % 	 ../DeferredRLGpu

		    6005.732 Mt    35.916 %      1377 x        4.361 Mt        0.741 Mt     6005.732 Mt   100.000 % 	 ../RayTracingRLGpu

		   10348.608 Mt    61.888 %      1377 x        7.515 Mt        7.370 Mt    10348.608 Mt   100.000 % 	 ../ResolveRLGpu

		      78.863 Mt     0.465 %      1377 x        0.057 Mt        0.087 Mt       78.863 Mt   100.000 % 	 ./Present


	============================


