Configuration: 
Command line parameters  = E:\programming\FIT-DIP\Masters-DXR-Main-git\build\Measurement\x64\Debug\Measurement.exe --scene Cube/Cube.gltf --duplication-cube --profile-duplication 10 --width 1024 --height 768 --profile-output DuplicationCube768Ra 
Window width             = 1024
Window height            = 768
Window title             = Direct3D App
Back-buffer count        = 3
Target FPS               = 60
Target UPS               = 60
Tearing                  = disabled
VSync                    = disabled
GUI rendering            = disabled
Debug layer              = enabled
GPU validation           = disabled
GPU profiling            = enabled
Render mode              = Rasterization
Loaded scene file        = Cube/Cube.gltf
Using testing scene      = disabled
Duplication              = 10
Duplication in cube      = enabled
Duplication offset       = 10
BLAS duplication         = enabled
Rays per pixel           = 9

ProfilingBuildAggregator: 
BuildBottomLevelAS	,	1.933
BuildBottomLevelASGpu	,	0.126752
BuildTopLevelAS	,	1.2447
BuildTopLevelASGpu	,	0.060864

ProfilingAggregator: 
Render	,	0.9972	,	1.0603	,	1.3047	,	1.6814	,	4.6596	,	3.459	,	3.8397	,	4.9679	,	5.7743	,	7.1731
Update	,	0.2339	,	0.2232	,	0.2697	,	0.3275	,	0.3841	,	0.4769	,	0.5612	,	0.7857	,	1.1088	,	1.2957

FpsAggregator: 
FPS	,	57	,	60	,	60	,	60	,	59	,	57	,	55	,	50	,	46	,	40
UPS	,	59	,	61	,	60	,	59	,	61	,	60	,	59	,	61	,	60	,	60
FrameTime	,	17.5439	,	16.6705	,	16.6675	,	16.6692	,	16.9492	,	17.5448	,	18.1968	,	20	,	21.7391	,	25.0001
GigaRays/s	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0

AccelerationStructure: 
Ray tracing acceleration structure building statistics: 
	Bottom Level Acceleration Structure: 
		Maximum [B]: 6208
		Minimum [B]: 6208
		Currently Allocated [B]: 6208
		Currently Created [num]: 1
		Current Average [B]: 6208
		Maximum Triangles [num]: 12
		Minimum Triangles [num]: 12
		Total Triangles [num]: 12
		Maximum Meshes [num]: 1
		Minimum Meshes [num]: 1
		Total Meshes [num]: 1
	Top Level Acceleration Structure: 
		Maximum [B]: 64
		Minimum [B]: 64
		Currently Allocated [B]: 64
		Total Allocated [B]: 64
		Total Created [num]: 1
		Average Allocated [B]: 64
		Maximum Instances [num]: 1
		Minimum Instances [num]: 1
		Total Instances [num]: 1
	Scratch Buffer: 
		Maximum [B]: 3584
		Minimum [B]: 3584
		Currently Allocated [B]: 3584
		Total Allocated [B]: 3584
		Total Created [num]: 1
		Average Allocated [B]: 3584
ProfilingStack: 
[PROF] Profiling statistics: 
	Scopes entered : 1867683
	Scopes exited : 1867682
	Overhead per scope [ticks] : 1029.9
	Threads entered : 3
	Threads exited : 0
	============================
		         Sum[Mt]   Parent[%]  Samples[num]       Avg[Mt]   Last/Curr[Mt]        Self[Mt]     Self[%] 	 Name

	main:

		   11022.276 Mt   100.000 %         1 x    11022.278 Mt    11022.275 Mt        1.048 Mt     0.010 % 	 /

		   11021.285 Mt    99.991 %         1 x    11021.287 Mt    11021.285 Mt     4612.129 Mt    41.847 % 	 ./Main application loop

		       2.850 Mt     0.026 %         1 x        2.850 Mt        0.000 Mt        2.850 Mt   100.000 % 	 ../TexturedRLInitialization

		       6.458 Mt     0.059 %         1 x        6.458 Mt        0.000 Mt        6.458 Mt   100.000 % 	 ../DeferredRLInitialization

		      51.016 Mt     0.463 %         1 x       51.016 Mt        0.000 Mt        1.736 Mt     3.404 % 	 ../RayTracingRLInitialization

		      49.280 Mt    96.596 %         1 x       49.280 Mt        0.000 Mt       49.280 Mt   100.000 % 	 .../PipelineBuild

		      11.062 Mt     0.100 %         1 x       11.062 Mt        0.000 Mt       11.062 Mt   100.000 % 	 ../ResolveRLInitialization

		     111.070 Mt     1.008 %         1 x      111.070 Mt        0.000 Mt        2.390 Mt     2.152 % 	 ../Initialization

		     108.680 Mt    97.848 %         1 x      108.680 Mt        0.000 Mt        1.190 Mt     1.094 % 	 .../ScenePrep

		      78.916 Mt    72.613 %         1 x       78.916 Mt        0.000 Mt       13.481 Mt    17.083 % 	 ..../SceneLoad

		      58.200 Mt    73.750 %         1 x       58.200 Mt        0.000 Mt       13.205 Mt    22.689 % 	 ...../glTF-LoadScene

		      44.995 Mt    77.311 %         2 x       22.498 Mt        0.000 Mt       44.995 Mt   100.000 % 	 ....../glTF-LoadImageData

		       3.935 Mt     4.987 %         1 x        3.935 Mt        0.000 Mt        3.935 Mt   100.000 % 	 ...../glTF-CreateScene

		       3.299 Mt     4.181 %         1 x        3.299 Mt        0.000 Mt        3.299 Mt   100.000 % 	 ...../SceneDesc-CreateScene

		       2.060 Mt     1.895 %         1 x        2.060 Mt        0.000 Mt        0.005 Mt     0.238 % 	 ..../TexturedRLPrep

		       2.055 Mt    99.762 %         1 x        2.055 Mt        0.000 Mt        0.395 Mt    19.234 % 	 ...../PrepareForRendering

		       1.354 Mt    65.907 %         1 x        1.354 Mt        0.000 Mt        1.354 Mt   100.000 % 	 ....../PrepareMaterials

		       0.305 Mt    14.859 %         1 x        0.305 Mt        0.000 Mt        0.305 Mt   100.000 % 	 ....../PrepareDrawBundle

		       2.675 Mt     2.461 %         1 x        2.675 Mt        0.000 Mt        0.005 Mt     0.176 % 	 ..../DeferredRLPrep

		       2.670 Mt    99.824 %         1 x        2.670 Mt        0.000 Mt        1.746 Mt    65.372 % 	 ...../PrepareForRendering

		       0.017 Mt     0.652 %         1 x        0.017 Mt        0.000 Mt        0.017 Mt   100.000 % 	 ....../PrepareMaterials

		       0.626 Mt    23.431 %         1 x        0.626 Mt        0.000 Mt        0.626 Mt   100.000 % 	 ....../BuildMaterialCache

		       0.282 Mt    10.545 %         1 x        0.282 Mt        0.000 Mt        0.282 Mt   100.000 % 	 ....../PrepareDrawBundle

		      12.940 Mt    11.906 %         1 x       12.940 Mt        0.000 Mt        4.015 Mt    31.030 % 	 ..../RayTracingRLPrep

		       0.239 Mt     1.847 %         1 x        0.239 Mt        0.000 Mt        0.239 Mt   100.000 % 	 ...../PrepareAccelerationStructures

		       0.025 Mt     0.195 %         1 x        0.025 Mt        0.000 Mt        0.025 Mt   100.000 % 	 ...../PrepareCache

		       0.488 Mt     3.769 %         1 x        0.488 Mt        0.000 Mt        0.488 Mt   100.000 % 	 ...../BuildCache

		       6.174 Mt    47.711 %         1 x        6.174 Mt        0.000 Mt        0.007 Mt     0.112 % 	 ...../BuildAccelerationStructures

		       6.167 Mt    99.888 %         1 x        6.167 Mt        0.000 Mt        2.989 Mt    48.471 % 	 ....../AccelerationStructureBuild

		       1.933 Mt    31.345 %         1 x        1.933 Mt        0.000 Mt        1.933 Mt   100.000 % 	 ......./BuildBottomLevelAS

		       1.245 Mt    20.184 %         1 x        1.245 Mt        0.000 Mt        1.245 Mt   100.000 % 	 ......./BuildTopLevelAS

		       1.999 Mt    15.449 %         1 x        1.999 Mt        0.000 Mt        1.999 Mt   100.000 % 	 ...../BuildShaderTables

		      10.900 Mt    10.030 %         1 x       10.900 Mt        0.000 Mt       10.900 Mt   100.000 % 	 ..../GpuSidePrep

		    6226.700 Mt    56.497 %   1864824 x        0.003 Mt       16.212 Mt     3135.495 Mt    50.356 % 	 ../MainLoop

		    1346.981 Mt    21.632 %       602 x        2.238 Mt        2.549 Mt     1346.981 Mt   100.000 % 	 .../Update

		    1744.224 Mt    28.012 %       546 x        3.195 Mt       10.956 Mt     1666.540 Mt    95.546 % 	 .../Render

		      77.684 Mt     4.454 %       546 x        0.142 Mt        0.005 Mt        1.849 Mt     2.380 % 	 ..../TexturedRLPrep

		      75.835 Mt    97.620 %        12 x        6.320 Mt        0.000 Mt        4.990 Mt     6.580 % 	 ...../PrepareForRendering

		       8.484 Mt    11.188 %        12 x        0.707 Mt        0.000 Mt        8.484 Mt   100.000 % 	 ....../PrepareMaterials

		      62.361 Mt    82.232 %        12 x        5.197 Mt        0.000 Mt       62.361 Mt   100.000 % 	 ....../PrepareDrawBundle

	WindowThread:

		   11010.592 Mt   100.000 %         1 x    11010.593 Mt    11010.591 Mt    11010.592 Mt   100.000 % 	 /

	GpuThread:

		     148.447 Mt   100.000 %       546 x        0.272 Mt        0.179 Mt      100.871 Mt    67.951 % 	 /

		       0.193 Mt     0.130 %         1 x        0.193 Mt        0.000 Mt        0.005 Mt     2.454 % 	 ./ScenePrep

		       0.188 Mt    97.546 %         1 x        0.188 Mt        0.000 Mt        0.001 Mt     0.340 % 	 ../AccelerationStructureBuild

		       0.127 Mt    67.330 %         1 x        0.127 Mt        0.000 Mt        0.127 Mt   100.000 % 	 .../BuildBottomLevelASGpu

		       0.061 Mt    32.330 %         1 x        0.061 Mt        0.000 Mt        0.061 Mt   100.000 % 	 .../BuildTopLevelASGpu

		      41.917 Mt    28.237 %       546 x        0.077 Mt        0.160 Mt       41.917 Mt   100.000 % 	 ./Textured

		       5.467 Mt     3.683 %       546 x        0.010 Mt        0.019 Mt        5.467 Mt   100.000 % 	 ./Present


	============================


