Configuration: 
Command line parameters  = E:\programming\FIT-DIP\Masters-DXR-Main-git\build\Measurement\x64\Debug\Measurement.exe --scene Cube/Cube.gltf --profile-track Cube/Cube.trk --rt-mode --width 1024 --height 768 --profile-output TrackCube768Rt 
Window width             = 1024
Window height            = 768
Window title             = Direct3D App
Back-buffer count        = 3
Target FPS               = 0
Target UPS               = 60
Tearing                  = enabled
VSync                    = disabled
GUI rendering            = disabled
Debug layer              = enabled
GPU validation           = disabled
GPU profiling            = enabled
Render mode              = Ray Tracing
Loaded scene file        = Cube/Cube.gltf
Using testing scene      = disabled
Duplication              = 1
Duplication in cube      = disabled
Duplication offset       = 10
BLAS duplication         = disabled
Rays per pixel           = 9

ProfilingBuildAggregator: 
BuildBottomLevelAS	,	1.349
BuildBottomLevelASGpu	,	0.174976
BuildTopLevelAS	,	1.2294
BuildTopLevelASGpu	,	0.086848

ProfilingAggregator: 
Render	,	3.4112	,	5.7498	,	3.8247	,	6.794	,	5.8443	,	4.4311	,	4.6262	,	6.0316	,	4.2642	,	3.8419	,	6.1359	,	6.6343	,	6.3409	,	6.3145	,	4.5413	,	4.1513	,	4.6595	,	7.7215	,	7.0172	,	4.0531	,	3.9455	,	5.3935	,	3.8792	,	3.884	,	3.9984	,	6.0479	,	4.2722	,	4.4855	,	6.0087
Update	,	0.4766	,	0.6571	,	0.5583	,	0.6155	,	0.7799	,	0.2738	,	0.2711	,	0.8068	,	0.2823	,	0.4118	,	0.5653	,	0.8323	,	0.2891	,	0.6147	,	0.7563	,	0.2771	,	0.8181	,	0.3005	,	0.2762	,	0.2736	,	0.274	,	0.7633	,	0.7502	,	0.2784	,	0.8037	,	0.5113	,	0.2898	,	0.8124	,	0.5505
RayTracing	,	1.23082	,	1.3809	,	1.5719	,	1.77802	,	1.90883	,	1.97885	,	1.75491	,	1.83741	,	1.9199	,	1.63622	,	1.56061	,	1.73584	,	1.64563	,	1.61098	,	1.85398	,	1.9719	,	2.07363	,	1.89178	,	2.05274	,	1.76624	,	1.62058	,	1.45712	,	1.42454	,	1.43168	,	1.42035	,	1.77635	,	1.78163	,	1.39722	,	1.43235
RayTracingRL	,	0.340288	,	0.452064	,	0.533504	,	0.653568	,	0.825344	,	0.91552	,	0.795488	,	0.850112	,	0.877536	,	0.752704	,	0.723136	,	0.80992	,	0.726368	,	0.677664	,	0.834272	,	0.920128	,	0.95504	,	0.871008	,	0.839744	,	0.818848	,	0.710368	,	0.540416	,	0.506656	,	0.486912	,	0.441408	,	0.370592	,	0.294528	,	0.24736	,	0.22928

FpsAggregator: 
FPS	,	209	,	207	,	205	,	187	,	177	,	176	,	182	,	196	,	185	,	193	,	198	,	194	,	184	,	200	,	190	,	184	,	168	,	175	,	185	,	192	,	193	,	200	,	207	,	205	,	201	,	201	,	206	,	206	,	210
UPS	,	59	,	60	,	61	,	60	,	60	,	60	,	60	,	60	,	61	,	60	,	60	,	60	,	60	,	61	,	60	,	60	,	60	,	60	,	60	,	61	,	60	,	60	,	60	,	60	,	61	,	60	,	60	,	60	,	60
FrameTime	,	4.79935	,	4.84343	,	4.89359	,	5.34786	,	5.65426	,	5.68745	,	5.51887	,	5.12884	,	5.41256	,	5.1975	,	5.07979	,	5.18213	,	5.43811	,	5.02582	,	5.28388	,	5.44829	,	5.95652	,	5.74074	,	5.41658	,	5.22241	,	5.18443	,	5.01759	,	4.84916	,	4.88706	,	4.99591	,	4.99181	,	4.86108	,	4.86397	,	4.77631
GigaRays/s	,	1.47476	,	1.46134	,	1.44636	,	1.3235	,	1.25178	,	1.24447	,	1.28249	,	1.38002	,	1.30768	,	1.36179	,	1.39334	,	1.36583	,	1.30153	,	1.4083	,	1.33952	,	1.2991	,	1.18826	,	1.23292	,	1.30671	,	1.35529	,	1.36522	,	1.41061	,	1.45961	,	1.44829	,	1.41674	,	1.4179	,	1.45603	,	1.45517	,	1.48187

AccelerationStructure: 
Ray tracing acceleration structure building statistics: 
	Bottom Level Acceleration Structure: 
		Maximum [B]: 6208
		Minimum [B]: 6208
		Currently Allocated [B]: 6208
		Currently Created [num]: 1
		Current Average [B]: 6208
		Maximum Triangles [num]: 12
		Minimum Triangles [num]: 12
		Total Triangles [num]: 12
		Maximum Meshes [num]: 1
		Minimum Meshes [num]: 1
		Total Meshes [num]: 1
	Top Level Acceleration Structure: 
		Maximum [B]: 64
		Minimum [B]: 64
		Currently Allocated [B]: 64
		Total Allocated [B]: 64
		Total Created [num]: 1
		Average Allocated [B]: 64
		Maximum Instances [num]: 1
		Minimum Instances [num]: 1
		Total Instances [num]: 1
	Scratch Buffer: 
		Maximum [B]: 3584
		Minimum [B]: 3584
		Currently Allocated [B]: 3584
		Total Allocated [B]: 3584
		Total Created [num]: 1
		Average Allocated [B]: 3584
ProfilingStack: 
[PROF] Profiling statistics: 
	Scopes entered : 53655
	Scopes exited : 53654
	Overhead per scope [ticks] : 997.2
	Threads entered : 3
	Threads exited : 0
	============================
		         Sum[Mt]   Parent[%]  Samples[num]       Avg[Mt]   Last/Curr[Mt]        Self[Mt]     Self[%] 	 Name

	main:

		   29698.965 Mt   100.000 %         1 x    29698.967 Mt    29698.963 Mt        0.720 Mt     0.002 % 	 /

		   29698.338 Mt    99.998 %         1 x    29698.342 Mt    29698.337 Mt      388.040 Mt     1.307 % 	 ./Main application loop

		       3.647 Mt     0.012 %         1 x        3.647 Mt        0.000 Mt        3.647 Mt   100.000 % 	 ../TexturedRLInitialization

		       8.345 Mt     0.028 %         1 x        8.345 Mt        0.000 Mt        8.345 Mt   100.000 % 	 ../DeferredRLInitialization

		      55.048 Mt     0.185 %         1 x       55.048 Mt        0.000 Mt        2.031 Mt     3.690 % 	 ../RayTracingRLInitialization

		      53.016 Mt    96.310 %         1 x       53.016 Mt        0.000 Mt       53.016 Mt   100.000 % 	 .../PipelineBuild

		      11.954 Mt     0.040 %         1 x       11.954 Mt        0.000 Mt       11.954 Mt   100.000 % 	 ../ResolveRLInitialization

		     145.750 Mt     0.491 %         1 x      145.750 Mt        0.000 Mt        3.841 Mt     2.635 % 	 ../Initialization

		     141.909 Mt    97.365 %         1 x      141.909 Mt        0.000 Mt        2.880 Mt     2.029 % 	 .../ScenePrep

		     101.382 Mt    71.441 %         1 x      101.382 Mt        0.000 Mt       14.481 Mt    14.283 % 	 ..../SceneLoad

		      57.768 Mt    56.981 %         1 x       57.768 Mt        0.000 Mt       13.416 Mt    23.224 % 	 ...../glTF-LoadScene

		      44.352 Mt    76.776 %         2 x       22.176 Mt        0.000 Mt       44.352 Mt   100.000 % 	 ....../glTF-LoadImageData

		      14.101 Mt    13.909 %         1 x       14.101 Mt        0.000 Mt       14.101 Mt   100.000 % 	 ...../glTF-CreateScene

		      15.031 Mt    14.827 %         1 x       15.031 Mt        0.000 Mt       15.031 Mt   100.000 % 	 ...../SceneDesc-CreateScene

		       2.330 Mt     1.642 %         1 x        2.330 Mt        0.000 Mt        0.005 Mt     0.219 % 	 ..../TexturedRLPrep

		       2.325 Mt    99.781 %         1 x        2.325 Mt        0.000 Mt        0.882 Mt    37.941 % 	 ...../PrepareForRendering

		       1.160 Mt    49.880 %         1 x        1.160 Mt        0.000 Mt        1.160 Mt   100.000 % 	 ....../PrepareMaterials

		       0.283 Mt    12.180 %         1 x        0.283 Mt        0.000 Mt        0.283 Mt   100.000 % 	 ....../PrepareDrawBundle

		       3.206 Mt     2.259 %         1 x        3.206 Mt        0.000 Mt        0.004 Mt     0.137 % 	 ..../DeferredRLPrep

		       3.201 Mt    99.863 %         1 x        3.201 Mt        0.000 Mt        2.101 Mt    65.623 % 	 ...../PrepareForRendering

		       0.016 Mt     0.506 %         1 x        0.016 Mt        0.000 Mt        0.016 Mt   100.000 % 	 ....../PrepareMaterials

		       0.824 Mt    25.724 %         1 x        0.824 Mt        0.000 Mt        0.824 Mt   100.000 % 	 ....../BuildMaterialCache

		       0.261 Mt     8.147 %         1 x        0.261 Mt        0.000 Mt        0.261 Mt   100.000 % 	 ....../PrepareDrawBundle

		      19.479 Mt    13.726 %         1 x       19.479 Mt        0.000 Mt        8.396 Mt    43.102 % 	 ..../RayTracingRLPrep

		       0.238 Mt     1.221 %         1 x        0.238 Mt        0.000 Mt        0.238 Mt   100.000 % 	 ...../PrepareAccelerationStructures

		       0.023 Mt     0.119 %         1 x        0.023 Mt        0.000 Mt        0.023 Mt   100.000 % 	 ...../PrepareCache

		       2.150 Mt    11.035 %         1 x        2.150 Mt        0.000 Mt        2.150 Mt   100.000 % 	 ...../BuildCache

		       6.318 Mt    32.436 %         1 x        6.318 Mt        0.000 Mt        0.007 Mt     0.104 % 	 ...../BuildAccelerationStructures

		       6.311 Mt    99.896 %         1 x        6.311 Mt        0.000 Mt        3.733 Mt    59.148 % 	 ....../AccelerationStructureBuild

		       1.349 Mt    21.374 %         1 x        1.349 Mt        0.000 Mt        1.349 Mt   100.000 % 	 ......./BuildBottomLevelAS

		       1.229 Mt    19.479 %         1 x        1.229 Mt        0.000 Mt        1.229 Mt   100.000 % 	 ......./BuildTopLevelAS

		       2.354 Mt    12.087 %         1 x        2.354 Mt        0.000 Mt        2.354 Mt   100.000 % 	 ...../BuildShaderTables

		      12.633 Mt     8.902 %         1 x       12.633 Mt        0.000 Mt       12.633 Mt   100.000 % 	 ..../GpuSidePrep

		   29085.553 Mt    97.937 %      6927 x        4.199 Mt        7.356 Mt     1060.170 Mt     3.645 % 	 ../MainLoop

		    1027.973 Mt     3.534 %      1747 x        0.588 Mt        0.346 Mt     1027.973 Mt   100.000 % 	 .../Update

		   26997.410 Mt    92.821 %      5618 x        4.806 Mt        6.058 Mt    26945.873 Mt    99.809 % 	 .../Render

		      30.787 Mt     0.114 %      5618 x        0.005 Mt        0.006 Mt       30.787 Mt   100.000 % 	 ..../DeferredRLPrep

		      20.749 Mt     0.077 %      5618 x        0.004 Mt        0.006 Mt       20.749 Mt   100.000 % 	 ..../RayTracingRLPrep

	WindowThread:

		   29696.272 Mt   100.000 %         1 x    29696.273 Mt    29696.271 Mt    29696.272 Mt   100.000 % 	 /

	GpuThread:

		    9538.486 Mt   100.000 %      5618 x        1.698 Mt        1.467 Mt      145.858 Mt     1.529 % 	 /

		       0.269 Mt     0.003 %         1 x        0.269 Mt        0.000 Mt        0.006 Mt     2.365 % 	 ./ScenePrep

		       0.263 Mt    97.635 %         1 x        0.263 Mt        0.000 Mt        0.001 Mt     0.426 % 	 ../AccelerationStructureBuild

		       0.175 Mt    66.545 %         1 x        0.175 Mt        0.000 Mt        0.175 Mt   100.000 % 	 .../BuildBottomLevelASGpu

		       0.087 Mt    33.029 %         1 x        0.087 Mt        0.000 Mt        0.087 Mt   100.000 % 	 .../BuildTopLevelASGpu

		    9388.428 Mt    98.427 %      5618 x        1.671 Mt        1.466 Mt       10.313 Mt     0.110 % 	 ./RayTracing

		     498.151 Mt     5.306 %      5618 x        0.089 Mt        0.035 Mt      498.151 Mt   100.000 % 	 ../DeferredRL

		    3644.468 Mt    38.819 %      5618 x        0.649 Mt        0.240 Mt     3644.468 Mt   100.000 % 	 ../RayTracingRL

		    5235.496 Mt    55.765 %      5618 x        0.932 Mt        1.187 Mt     5235.496 Mt   100.000 % 	 ../ResolveRL

		       3.931 Mt     0.041 %      5618 x        0.001 Mt        0.001 Mt        3.931 Mt   100.000 % 	 ./Present


	============================


