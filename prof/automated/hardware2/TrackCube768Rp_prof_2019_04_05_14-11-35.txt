Configuration: 
Command line parameters  = E:\programming\FIT-DIP\Masters-DXR-Main-git\build\Measurement\x64\Debug\Measurement.exe --scene Cube/Cube.gltf --profile-track Cube/Cube.trk --rt-mode-pure --width 1024 --height 768 --profile-output TrackCube768Rp 
Window width             = 1024
Window height            = 768
Window title             = Direct3D App
Back-buffer count        = 3
Target FPS               = 0
Target UPS               = 60
Tearing                  = enabled
VSync                    = disabled
GUI rendering            = disabled
Debug layer              = enabled
GPU validation           = disabled
GPU profiling            = enabled
Render mode              = Ray Tracing
Loaded scene file        = Cube/Cube.gltf
Using testing scene      = disabled
Duplication              = 1
Duplication in cube      = disabled
Duplication offset       = 10
BLAS duplication         = disabled
Rays per pixel           = 10

ProfilingBuildAggregator: 
BuildBottomLevelAS	,	1.8185
BuildBottomLevelASGpu	,	0.127776
BuildTopLevelAS	,	1.1485
BuildTopLevelASGpu	,	0.064416

ProfilingAggregator: 
Render	,	2.9273	,	3.1731	,	3.1727	,	4.2048	,	3.9784	,	3.7765	,	3.7257	,	3.8526	,	4.0001	,	3.7042	,	4.2065	,	3.0565	,	2.7917	,	4.3524	,	3.534	,	4.3637	,	4.1965	,	4.0982	,	3.0488	,	3.0281	,	2.9851	,	2.5439	,	3.127	,	2.7918	,	2.5784	,	3.3213	,	2.2436	,	2.8639	,	2.9689
Update	,	0.4588	,	0.6406	,	0.7492	,	0.5374	,	0.6552	,	0.801	,	0.3494	,	0.7217	,	0.5473	,	0.4183	,	0.3086	,	0.4982	,	0.5571	,	0.5894	,	0.4818	,	0.7694	,	0.5265	,	0.838	,	0.8444	,	0.8493	,	0.3243	,	0.5881	,	0.4061	,	0.3949	,	0.3032	,	0.7942	,	0.4828	,	0.4977	,	0.4216
RayTracing	,	1.0209	,	1.17392	,	1.24726	,	1.35523	,	1.37354	,	1.448	,	1.35658	,	1.51382	,	1.32976	,	1.31789	,	1.30326	,	1.33699	,	1.21683	,	1.21472	,	1.33626	,	1.47853	,	1.45213	,	1.44826	,	1.38934	,	1.39398	,	1.28282	,	1.10701	,	1.15392	,	1.1152	,	1.06637	,	0.931904	,	0.869024	,	0.953824	,	0.997472
RayTracingRL	,	0.347552	,	0.47056	,	0.512256	,	0.600096	,	0.676416	,	0.78288	,	0.707648	,	0.7008	,	0.732608	,	0.72048	,	0.704	,	0.7288	,	0.612192	,	0.58432	,	0.68688	,	0.8152	,	0.82704	,	0.825024	,	0.791456	,	0.794816	,	0.681728	,	0.50672	,	0.492896	,	0.440352	,	0.384864	,	0.286944	,	0.223872	,	0.212064	,	0.198848

FpsAggregator: 
FPS	,	310	,	287	,	266	,	263	,	258	,	246	,	255	,	304	,	270	,	264	,	268	,	267	,	266	,	290	,	265	,	249	,	240	,	245	,	257	,	250	,	257	,	286	,	282	,	299	,	311	,	344	,	339	,	321	,	311
UPS	,	59	,	61	,	60	,	60	,	60	,	60	,	60	,	61	,	60	,	60	,	60	,	60	,	61	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	61	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60
FrameTime	,	3.23397	,	3.48949	,	3.774	,	3.8035	,	3.89093	,	4.07741	,	3.93493	,	3.30286	,	3.7166	,	3.80135	,	3.74524	,	3.75624	,	3.77032	,	3.45352	,	3.78691	,	4.02268	,	4.16751	,	4.09837	,	3.89308	,	4.00715	,	3.89439	,	3.50402	,	3.55058	,	3.34499	,	3.21554	,	2.91469	,	2.95095	,	3.11793	,	3.22326
GigaRays/s	,	2.43178	,	2.25372	,	2.08382	,	2.06766	,	2.02119	,	1.92875	,	1.99859	,	2.38106	,	2.116	,	2.06882	,	2.09982	,	2.09367	,	2.08585	,	2.27719	,	2.07671	,	1.955	,	1.88706	,	1.91889	,	2.02008	,	1.96257	,	2.0194	,	2.24437	,	2.21494	,	2.35107	,	2.44572	,	2.69816	,	2.66502	,	2.52229	,	2.43986

AccelerationStructure: 
Ray tracing acceleration structure building statistics: 
	Bottom Level Acceleration Structure: 
		Maximum [B]: 6208
		Minimum [B]: 6208
		Currently Allocated [B]: 6208
		Currently Created [num]: 1
		Current Average [B]: 6208
		Maximum Triangles [num]: 12
		Minimum Triangles [num]: 12
		Total Triangles [num]: 12
		Maximum Meshes [num]: 1
		Minimum Meshes [num]: 1
		Total Meshes [num]: 1
	Top Level Acceleration Structure: 
		Maximum [B]: 64
		Minimum [B]: 64
		Currently Allocated [B]: 64
		Total Allocated [B]: 64
		Total Created [num]: 1
		Average Allocated [B]: 64
		Maximum Instances [num]: 1
		Minimum Instances [num]: 1
		Total Instances [num]: 1
	Scratch Buffer: 
		Maximum [B]: 3584
		Minimum [B]: 3584
		Currently Allocated [B]: 3584
		Total Allocated [B]: 3584
		Total Created [num]: 1
		Average Allocated [B]: 3584
ProfilingStack: 
[PROF] Profiling statistics: 
	Scopes entered : 62884
	Scopes exited : 62883
	Overhead per scope [ticks] : 1017.1
	Threads entered : 3
	Threads exited : 0
	============================
		         Sum[Mt]   Parent[%]  Samples[num]       Avg[Mt]   Last/Curr[Mt]        Self[Mt]     Self[%] 	 Name

	main:

		   29607.638 Mt   100.000 %         1 x    29607.640 Mt    29607.638 Mt        0.707 Mt     0.002 % 	 /

		   29606.976 Mt    99.998 %         1 x    29606.978 Mt    29606.976 Mt      361.803 Mt     1.222 % 	 ./Main application loop

		       2.706 Mt     0.009 %         1 x        2.706 Mt        0.000 Mt        2.706 Mt   100.000 % 	 ../TexturedRLInitialization

		       6.644 Mt     0.022 %         1 x        6.644 Mt        0.000 Mt        6.644 Mt   100.000 % 	 ../DeferredRLInitialization

		      50.411 Mt     0.170 %         1 x       50.411 Mt        0.000 Mt        2.226 Mt     4.415 % 	 ../RayTracingRLInitialization

		      48.186 Mt    95.585 %         1 x       48.186 Mt        0.000 Mt       48.186 Mt   100.000 % 	 .../PipelineBuild

		      13.631 Mt     0.046 %         1 x       13.631 Mt        0.000 Mt       13.631 Mt   100.000 % 	 ../ResolveRLInitialization

		     150.904 Mt     0.510 %         1 x      150.904 Mt        0.000 Mt        2.733 Mt     1.811 % 	 ../Initialization

		     148.171 Mt    98.189 %         1 x      148.171 Mt        0.000 Mt        1.852 Mt     1.250 % 	 .../ScenePrep

		     110.704 Mt    74.714 %         1 x      110.704 Mt        0.000 Mt       16.952 Mt    15.313 % 	 ..../SceneLoad

		      78.141 Mt    70.585 %         1 x       78.141 Mt        0.000 Mt       30.158 Mt    38.595 % 	 ...../glTF-LoadScene

		      47.983 Mt    61.405 %         2 x       23.991 Mt        0.000 Mt       47.983 Mt   100.000 % 	 ....../glTF-LoadImageData

		       6.972 Mt     6.298 %         1 x        6.972 Mt        0.000 Mt        6.972 Mt   100.000 % 	 ...../glTF-CreateScene

		       8.640 Mt     7.804 %         1 x        8.640 Mt        0.000 Mt        8.640 Mt   100.000 % 	 ...../SceneDesc-CreateScene

		       2.504 Mt     1.690 %         1 x        2.504 Mt        0.000 Mt        0.015 Mt     0.607 % 	 ..../TexturedRLPrep

		       2.489 Mt    99.393 %         1 x        2.489 Mt        0.000 Mt        1.023 Mt    41.110 % 	 ...../PrepareForRendering

		       0.756 Mt    30.385 %         1 x        0.756 Mt        0.000 Mt        0.756 Mt   100.000 % 	 ....../PrepareMaterials

		       0.709 Mt    28.505 %         1 x        0.709 Mt        0.000 Mt        0.709 Mt   100.000 % 	 ....../PrepareDrawBundle

		       3.776 Mt     2.548 %         1 x        3.776 Mt        0.000 Mt        0.011 Mt     0.294 % 	 ..../DeferredRLPrep

		       3.765 Mt    99.706 %         1 x        3.765 Mt        0.000 Mt        2.485 Mt    66.010 % 	 ...../PrepareForRendering

		       0.039 Mt     1.031 %         1 x        0.039 Mt        0.000 Mt        0.039 Mt   100.000 % 	 ....../PrepareMaterials

		       0.584 Mt    15.505 %         1 x        0.584 Mt        0.000 Mt        0.584 Mt   100.000 % 	 ....../BuildMaterialCache

		       0.657 Mt    17.455 %         1 x        0.657 Mt        0.000 Mt        0.657 Mt   100.000 % 	 ....../PrepareDrawBundle

		      18.772 Mt    12.669 %         1 x       18.772 Mt        0.000 Mt        5.813 Mt    30.967 % 	 ..../RayTracingRLPrep

		       0.543 Mt     2.894 %         1 x        0.543 Mt        0.000 Mt        0.543 Mt   100.000 % 	 ...../PrepareAccelerationStructures

		       0.056 Mt     0.297 %         1 x        0.056 Mt        0.000 Mt        0.056 Mt   100.000 % 	 ...../PrepareCache

		       0.591 Mt     3.150 %         1 x        0.591 Mt        0.000 Mt        0.591 Mt   100.000 % 	 ...../BuildCache

		       6.172 Mt    32.879 %         1 x        6.172 Mt        0.000 Mt        0.010 Mt     0.169 % 	 ...../BuildAccelerationStructures

		       6.162 Mt    99.831 %         1 x        6.162 Mt        0.000 Mt        3.195 Mt    51.848 % 	 ....../AccelerationStructureBuild

		       1.819 Mt    29.513 %         1 x        1.819 Mt        0.000 Mt        1.819 Mt   100.000 % 	 ......./BuildBottomLevelAS

		       1.149 Mt    18.639 %         1 x        1.149 Mt        0.000 Mt        1.149 Mt   100.000 % 	 ......./BuildTopLevelAS

		       5.597 Mt    29.813 %         1 x        5.597 Mt        0.000 Mt        5.597 Mt   100.000 % 	 ...../BuildShaderTables

		      10.562 Mt     7.128 %         1 x       10.562 Mt        0.000 Mt       10.562 Mt   100.000 % 	 ..../GpuSidePrep

		   29020.876 Mt    98.020 %     12671 x        2.290 Mt        3.132 Mt      344.316 Mt     1.186 % 	 ../MainLoop

		    1019.596 Mt     3.513 %      1744 x        0.585 Mt        0.287 Mt     1019.596 Mt   100.000 % 	 .../Update

		   27656.965 Mt    95.300 %      8072 x        3.426 Mt        2.837 Mt    27607.729 Mt    99.822 % 	 .../Render

		      49.236 Mt     0.178 %      8072 x        0.006 Mt        0.004 Mt       49.236 Mt   100.000 % 	 ..../RayTracingRLPrep

	WindowThread:

		   29603.310 Mt   100.000 %         1 x    29603.311 Mt    29603.310 Mt    29603.310 Mt   100.000 % 	 /

	GpuThread:

		   10133.562 Mt   100.000 %      8072 x        1.255 Mt        1.034 Mt      124.581 Mt     1.229 % 	 /

		       0.198 Mt     0.002 %         1 x        0.198 Mt        0.000 Mt        0.005 Mt     2.410 % 	 ./ScenePrep

		       0.193 Mt    97.590 %         1 x        0.193 Mt        0.000 Mt        0.001 Mt     0.464 % 	 ../AccelerationStructureBuild

		       0.128 Mt    66.175 %         1 x        0.128 Mt        0.000 Mt        0.128 Mt   100.000 % 	 .../BuildBottomLevelASGpu

		       0.064 Mt    33.361 %         1 x        0.064 Mt        0.000 Mt        0.064 Mt   100.000 % 	 .../BuildTopLevelASGpu

		   10004.282 Mt    98.724 %      8072 x        1.239 Mt        1.033 Mt        8.896 Mt     0.089 % 	 ./RayTracing

		    4614.519 Mt    46.125 %      8072 x        0.572 Mt        0.210 Mt     4614.519 Mt   100.000 % 	 ../RayTracingRL

		    5380.867 Mt    53.786 %      8072 x        0.667 Mt        0.821 Mt     5380.867 Mt   100.000 % 	 ../ResolveRL

		       4.501 Mt     0.044 %      8072 x        0.001 Mt        0.001 Mt        4.501 Mt   100.000 % 	 ./Present


	============================


