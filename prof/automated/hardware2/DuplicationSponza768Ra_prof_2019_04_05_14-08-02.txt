Configuration: 
Command line parameters  = E:\programming\FIT-DIP\Masters-DXR-Main-git\build\Measurement\x64\Debug\Measurement.exe --scene Sponza/Sponza.gltf --duplication-cube --profile-duplication 7 --width 1024 --height 768 --profile-output DuplicationSponza768Ra 
Window width             = 1024
Window height            = 768
Window title             = Direct3D App
Back-buffer count        = 3
Target FPS               = 60
Target UPS               = 60
Tearing                  = disabled
VSync                    = disabled
GUI rendering            = disabled
Debug layer              = enabled
GPU validation           = disabled
GPU profiling            = enabled
Render mode              = Rasterization
Loaded scene file        = Sponza/Sponza.gltf
Using testing scene      = disabled
Duplication              = 7
Duplication in cube      = enabled
Duplication offset       = 10
BLAS duplication         = enabled
Rays per pixel           = 9

ProfilingBuildAggregator: 
BuildBottomLevelAS	,	5.3165
BuildBottomLevelASGpu	,	24.6363
BuildTopLevelAS	,	2.192
BuildTopLevelASGpu	,	0.62432

ProfilingAggregator: 
Render	,	1.8514	,	8.9948	,	23.1908	,	60.1649	,	109.133	,	180.557	,	1097.01
Update	,	0.2314	,	0.2351	,	0.3067	,	0.4201	,	0.4535	,	0.6757	,	0.5239

FpsAggregator: 
FPS	,	55	,	59	,	35	,	16	,	6	,	2	,	1
UPS	,	59	,	60	,	60	,	60	,	62	,	58	,	14
FrameTime	,	18.1818	,	17.0913	,	28.8172	,	65.2631	,	180.013	,	539.822	,	1238.53
GigaRays/s	,	0	,	0	,	0	,	0	,	0	,	0	,	0

AccelerationStructure: 
Ray tracing acceleration structure building statistics: 
	Bottom Level Acceleration Structure: 
		Maximum [B]: 18391808
		Minimum [B]: 18391808
		Currently Allocated [B]: 18391808
		Currently Created [num]: 1
		Current Average [B]: 18391808
		Maximum Triangles [num]: 262267
		Minimum Triangles [num]: 262267
		Total Triangles [num]: 262267
		Maximum Meshes [num]: 103
		Minimum Meshes [num]: 103
		Total Meshes [num]: 103
	Top Level Acceleration Structure: 
		Maximum [B]: 64
		Minimum [B]: 64
		Currently Allocated [B]: 64
		Total Allocated [B]: 64
		Total Created [num]: 1
		Average Allocated [B]: 64
		Maximum Instances [num]: 1
		Minimum Instances [num]: 1
		Total Instances [num]: 1
	Scratch Buffer: 
		Maximum [B]: 14998528
		Minimum [B]: 14998528
		Currently Allocated [B]: 14998528
		Total Allocated [B]: 14998528
		Total Created [num]: 1
		Average Allocated [B]: 14998528
ProfilingStack: 
[PROF] Profiling statistics: 
	Scopes entered : 345983
	Scopes exited : 345982
	Overhead per scope [ticks] : 1010.3
	Threads entered : 3
	Threads exited : 0
	============================
		         Sum[Mt]   Parent[%]  Samples[num]       Avg[Mt]   Last/Curr[Mt]        Self[Mt]     Self[%] 	 Name

	main:

		   17661.022 Mt   100.000 %         1 x    17661.025 Mt    17661.020 Mt        0.773 Mt     0.004 % 	 /

		   17660.392 Mt    99.996 %         1 x    17660.397 Mt    17660.391 Mt     1137.146 Mt     6.439 % 	 ./Main application loop

		       2.805 Mt     0.016 %         1 x        2.805 Mt        0.000 Mt        2.805 Mt   100.000 % 	 ../TexturedRLInitialization

		       5.609 Mt     0.032 %         1 x        5.609 Mt        0.000 Mt        5.609 Mt   100.000 % 	 ../DeferredRLInitialization

		      52.320 Mt     0.296 %         1 x       52.320 Mt        0.000 Mt        2.057 Mt     3.931 % 	 ../RayTracingRLInitialization

		      50.264 Mt    96.069 %         1 x       50.264 Mt        0.000 Mt       50.264 Mt   100.000 % 	 .../PipelineBuild

		      10.663 Mt     0.060 %         1 x       10.663 Mt        0.000 Mt       10.663 Mt   100.000 % 	 ../ResolveRLInitialization

		    9337.282 Mt    52.871 %         1 x     9337.282 Mt        0.000 Mt        8.349 Mt     0.089 % 	 ../Initialization

		    9328.933 Mt    99.911 %         1 x     9328.933 Mt        0.000 Mt        3.441 Mt     0.037 % 	 .../ScenePrep

		    9202.165 Mt    98.641 %         1 x     9202.165 Mt        0.000 Mt      348.584 Mt     3.788 % 	 ..../SceneLoad

		    8336.963 Mt    90.598 %         1 x     8336.963 Mt        0.000 Mt     1111.429 Mt    13.331 % 	 ...../glTF-LoadScene

		    7225.534 Mt    86.669 %        69 x      104.718 Mt        0.000 Mt     7225.534 Mt   100.000 % 	 ....../glTF-LoadImageData

		     371.855 Mt     4.041 %         1 x      371.855 Mt        0.000 Mt      371.855 Mt   100.000 % 	 ...../glTF-CreateScene

		     144.762 Mt     1.573 %         1 x      144.762 Mt        0.000 Mt      144.762 Mt   100.000 % 	 ...../SceneDesc-CreateScene

		       7.228 Mt     0.077 %         1 x        7.228 Mt        0.000 Mt        0.005 Mt     0.072 % 	 ..../TexturedRLPrep

		       7.222 Mt    99.928 %         1 x        7.222 Mt        0.000 Mt        2.265 Mt    31.358 % 	 ...../PrepareForRendering

		       2.678 Mt    37.074 %         1 x        2.678 Mt        0.000 Mt        2.678 Mt   100.000 % 	 ....../PrepareMaterials

		       2.280 Mt    31.568 %         1 x        2.280 Mt        0.000 Mt        2.280 Mt   100.000 % 	 ....../PrepareDrawBundle

		      11.839 Mt     0.127 %         1 x       11.839 Mt        0.000 Mt        0.006 Mt     0.053 % 	 ..../DeferredRLPrep

		      11.833 Mt    99.947 %         1 x       11.833 Mt        0.000 Mt        4.246 Mt    35.887 % 	 ...../PrepareForRendering

		       0.021 Mt     0.174 %         1 x        0.021 Mt        0.000 Mt        0.021 Mt   100.000 % 	 ....../PrepareMaterials

		       5.102 Mt    43.115 %         1 x        5.102 Mt        0.000 Mt        5.102 Mt   100.000 % 	 ....../BuildMaterialCache

		       2.464 Mt    20.824 %         1 x        2.464 Mt        0.000 Mt        2.464 Mt   100.000 % 	 ....../PrepareDrawBundle

		      32.515 Mt     0.349 %         1 x       32.515 Mt        0.000 Mt        6.470 Mt    19.898 % 	 ..../RayTracingRLPrep

		       0.992 Mt     3.052 %         1 x        0.992 Mt        0.000 Mt        0.992 Mt   100.000 % 	 ...../PrepareAccelerationStructures

		       0.025 Mt     0.077 %         1 x        0.025 Mt        0.000 Mt        0.025 Mt   100.000 % 	 ...../PrepareCache

		       4.771 Mt    14.673 %         1 x        4.771 Mt        0.000 Mt        4.771 Mt   100.000 % 	 ...../BuildCache

		      12.758 Mt    39.237 %         1 x       12.758 Mt        0.000 Mt        0.007 Mt     0.053 % 	 ...../BuildAccelerationStructures

		      12.751 Mt    99.947 %         1 x       12.751 Mt        0.000 Mt        5.242 Mt    41.114 % 	 ....../AccelerationStructureBuild

		       5.316 Mt    41.695 %         1 x        5.316 Mt        0.000 Mt        5.316 Mt   100.000 % 	 ......./BuildBottomLevelAS

		       2.192 Mt    17.191 %         1 x        2.192 Mt        0.000 Mt        2.192 Mt   100.000 % 	 ......./BuildTopLevelAS

		       7.499 Mt    23.064 %         1 x        7.499 Mt        0.000 Mt        7.499 Mt   100.000 % 	 ...../BuildShaderTables

		      71.745 Mt     0.769 %         1 x       71.745 Mt        0.000 Mt       71.745 Mt   100.000 % 	 ..../GpuSidePrep

		    7114.567 Mt    40.285 %    344714 x        0.021 Mt      396.522 Mt      638.993 Mt     8.981 % 	 ../MainLoop

		     594.279 Mt     8.353 %       447 x        1.329 Mt        0.511 Mt      594.279 Mt   100.000 % 	 .../Update

		    5881.295 Mt    82.666 %       175 x       33.607 Mt      314.301 Mt     4008.122 Mt    68.150 % 	 .../Render

		    1873.173 Mt    31.850 %       175 x       10.704 Mt        0.004 Mt        0.616 Mt     0.033 % 	 ..../TexturedRLPrep

		    1872.557 Mt    99.967 %         6 x      312.093 Mt        0.000 Mt       30.465 Mt     1.627 % 	 ...../PrepareForRendering

		       2.139 Mt     0.114 %         6 x        0.356 Mt        0.000 Mt        2.139 Mt   100.000 % 	 ....../PrepareMaterials

		    1839.954 Mt    98.259 %         6 x      306.659 Mt        0.000 Mt     1839.954 Mt   100.000 % 	 ....../PrepareDrawBundle

	WindowThread:

		   17660.389 Mt   100.000 %         1 x    17660.391 Mt    17660.388 Mt    17660.389 Mt   100.000 % 	 /

	GpuThread:

		     749.794 Mt   100.000 %       175 x        4.285 Mt       75.622 Mt      162.927 Mt    21.730 % 	 /

		      25.314 Mt     3.376 %         1 x       25.314 Mt        0.000 Mt        0.051 Mt     0.200 % 	 ./ScenePrep

		      25.264 Mt    99.800 %         1 x       25.264 Mt        0.000 Mt        0.003 Mt     0.012 % 	 ../AccelerationStructureBuild

		      24.636 Mt    97.517 %         1 x       24.636 Mt        0.000 Mt       24.636 Mt   100.000 % 	 .../BuildBottomLevelASGpu

		       0.624 Mt     2.471 %         1 x        0.624 Mt        0.000 Mt        0.624 Mt   100.000 % 	 .../BuildTopLevelASGpu

		     560.927 Mt    74.811 %       175 x        3.205 Mt       75.602 Mt      560.927 Mt   100.000 % 	 ./Textured

		       0.625 Mt     0.083 %       175 x        0.004 Mt        0.018 Mt        0.625 Mt   100.000 % 	 ./Present


	============================


