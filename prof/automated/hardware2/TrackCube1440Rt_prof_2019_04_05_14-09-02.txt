Configuration: 
Command line parameters  = E:\programming\FIT-DIP\Masters-DXR-Main-git\build\Measurement\x64\Debug\Measurement.exe --scene Cube/Cube.gltf --profile-track Cube/Cube.trk --rt-mode --width 2560 --height 1440 --profile-output TrackCube1440Rt 
Window width             = 2560
Window height            = 1421
Window title             = Direct3D App
Back-buffer count        = 3
Target FPS               = 0
Target UPS               = 60
Tearing                  = enabled
VSync                    = disabled
GUI rendering            = disabled
Debug layer              = enabled
GPU validation           = disabled
GPU profiling            = enabled
Render mode              = Ray Tracing
Loaded scene file        = Cube/Cube.gltf
Using testing scene      = disabled
Duplication              = 1
Duplication in cube      = disabled
Duplication offset       = 10
BLAS duplication         = disabled
Rays per pixel           = 9

ProfilingBuildAggregator: 
BuildBottomLevelAS	,	1.9964
BuildBottomLevelASGpu	,	0.173952
BuildTopLevelAS	,	1.0972
BuildTopLevelASGpu	,	0.084608

ProfilingAggregator: 
Render	,	9.3815	,	9.3371	,	7.9932	,	9.5526	,	9.1147	,	8.0128	,	7.7804	,	8.9648	,	8.9789	,	11.2377	,	10.2342	,	9.7092	,	10.8334	,	10.5605	,	10.5969	,	8.4307	,	11.2351	,	10.2668	,	12.4348	,	12.9102	,	10.4435	,	7.8883	,	10.3241	,	9.8397	,	9.3814	,	7.477	,	7.3542	,	10.4206	,	8.3454
Update	,	0.6354	,	0.5879	,	0.4644	,	0.6654	,	0.6334	,	0.4985	,	0.3299	,	0.4271	,	0.3459	,	0.672	,	0.7885	,	0.4869	,	0.764	,	0.7056	,	0.7896	,	0.4382	,	0.8127	,	0.5772	,	0.6091	,	0.6838	,	0.8673	,	0.376	,	0.4865	,	0.6443	,	0.6475	,	0.5838	,	0.8383	,	0.2869	,	0.6329
RayTracing	,	3.98416	,	4.04771	,	4.12496	,	4.29286	,	4.956	,	5.22205	,	4.98723	,	5.40518	,	6.03622	,	5.55235	,	5.15203	,	5.72067	,	5.21283	,	4.76083	,	4.83798	,	5.28998	,	5.92176	,	7.084	,	7.14426	,	7.38435	,	6.13802	,	5.02269	,	4.95478	,	4.51539	,	3.73005	,	3.43242	,	3.20118	,	3.38742	,	3.01222
RayTracingRL	,	0.768768	,	1.03664	,	1.0992	,	1.25862	,	1.64563	,	2.05325	,	1.87965	,	2.16397	,	2.52864	,	2.26214	,	2.02301	,	2.18701	,	1.9768	,	1.76506	,	1.7927	,	2.11933	,	2.51648	,	3.29386	,	3.11117	,	3.40166	,	2.5983	,	1.78624	,	1.68464	,	1.37696	,	0.889984	,	0.667456	,	0.494112	,	0.402784	,	0.346752

FpsAggregator: 
FPS	,	94	,	106	,	102	,	104	,	98	,	92	,	91	,	93	,	87	,	85	,	93	,	87	,	94	,	95	,	93	,	90	,	90	,	86	,	75	,	72	,	76	,	93	,	94	,	99	,	107	,	113	,	127	,	164	,	126
UPS	,	59	,	61	,	60	,	60	,	60	,	61	,	60	,	60	,	60	,	60	,	60	,	61	,	60	,	60	,	60	,	61	,	59	,	61	,	61	,	60	,	60	,	61	,	60	,	60	,	61	,	60	,	60	,	60	,	60
FrameTime	,	10.6846	,	9.49609	,	9.80709	,	9.68043	,	10.2711	,	10.9081	,	11.0036	,	10.7592	,	11.5212	,	11.8083	,	10.8022	,	11.5364	,	10.6699	,	10.543	,	10.8052	,	11.1725	,	11.1305	,	11.7323	,	13.4904	,	13.9639	,	13.2043	,	10.8411	,	10.6961	,	10.1107	,	9.40421	,	8.85452	,	7.89693	,	6.13863	,	7.95424
GigaRays/s	,	3.06422	,	3.44772	,	3.33839	,	3.38206	,	3.18756	,	3.00142	,	2.97537	,	3.04295	,	2.8417	,	2.77261	,	3.03084	,	2.83795	,	3.06842	,	3.10537	,	3.03	,	2.93041	,	2.94144	,	2.79057	,	2.4269	,	2.34461	,	2.47949	,	3.01998	,	3.06092	,	3.23814	,	3.4814	,	3.69753	,	4.1459	,	5.33341	,	4.11602

AccelerationStructure: 
Ray tracing acceleration structure building statistics: 
	Bottom Level Acceleration Structure: 
		Maximum [B]: 6208
		Minimum [B]: 6208
		Currently Allocated [B]: 6208
		Currently Created [num]: 1
		Current Average [B]: 6208
		Maximum Triangles [num]: 12
		Minimum Triangles [num]: 12
		Total Triangles [num]: 12
		Maximum Meshes [num]: 1
		Minimum Meshes [num]: 1
		Total Meshes [num]: 1
	Top Level Acceleration Structure: 
		Maximum [B]: 64
		Minimum [B]: 64
		Currently Allocated [B]: 64
		Total Allocated [B]: 64
		Total Created [num]: 1
		Average Allocated [B]: 64
		Maximum Instances [num]: 1
		Minimum Instances [num]: 1
		Total Instances [num]: 1
	Scratch Buffer: 
		Maximum [B]: 3584
		Minimum [B]: 3584
		Currently Allocated [B]: 3584
		Total Allocated [B]: 3584
		Total Created [num]: 1
		Average Allocated [B]: 3584
ProfilingStack: 
[PROF] Profiling statistics: 
	Scopes entered : 28492
	Scopes exited : 28491
	Overhead per scope [ticks] : 997
	Threads entered : 3
	Threads exited : 0
	============================
		         Sum[Mt]   Parent[%]  Samples[num]       Avg[Mt]   Last/Curr[Mt]        Self[Mt]     Self[%] 	 Name

	main:

		   29776.053 Mt   100.000 %         1 x    29776.056 Mt    29776.052 Mt        0.705 Mt     0.002 % 	 /

		   29775.464 Mt    99.998 %         1 x    29775.469 Mt    29775.463 Mt      353.690 Mt     1.188 % 	 ./Main application loop

		       2.852 Mt     0.010 %         1 x        2.852 Mt        0.000 Mt        2.852 Mt   100.000 % 	 ../TexturedRLInitialization

		       6.476 Mt     0.022 %         1 x        6.476 Mt        0.000 Mt        6.476 Mt   100.000 % 	 ../DeferredRLInitialization

		      72.216 Mt     0.243 %         1 x       72.216 Mt        0.000 Mt        2.179 Mt     3.017 % 	 ../RayTracingRLInitialization

		      70.037 Mt    96.983 %         1 x       70.037 Mt        0.000 Mt       70.037 Mt   100.000 % 	 .../PipelineBuild

		      11.566 Mt     0.039 %         1 x       11.566 Mt        0.000 Mt       11.566 Mt   100.000 % 	 ../ResolveRLInitialization

		     181.054 Mt     0.608 %         1 x      181.054 Mt        0.000 Mt        7.703 Mt     4.255 % 	 ../Initialization

		     173.351 Mt    95.745 %         1 x      173.351 Mt        0.000 Mt        2.199 Mt     1.269 % 	 .../ScenePrep

		     128.298 Mt    74.011 %         1 x      128.298 Mt        0.000 Mt       19.069 Mt    14.863 % 	 ..../SceneLoad

		      81.008 Mt    63.140 %         1 x       81.008 Mt        0.000 Mt       32.252 Mt    39.813 % 	 ...../glTF-LoadScene

		      48.756 Mt    60.187 %         2 x       24.378 Mt        0.000 Mt       48.756 Mt   100.000 % 	 ....../glTF-LoadImageData

		      13.965 Mt    10.885 %         1 x       13.965 Mt        0.000 Mt       13.965 Mt   100.000 % 	 ...../glTF-CreateScene

		      14.257 Mt    11.112 %         1 x       14.257 Mt        0.000 Mt       14.257 Mt   100.000 % 	 ...../SceneDesc-CreateScene

		       2.945 Mt     1.699 %         1 x        2.945 Mt        0.000 Mt        0.015 Mt     0.526 % 	 ..../TexturedRLPrep

		       2.930 Mt    99.474 %         1 x        2.930 Mt        0.000 Mt        1.001 Mt    34.153 % 	 ...../PrepareForRendering

		       0.993 Mt    33.897 %         1 x        0.993 Mt        0.000 Mt        0.993 Mt   100.000 % 	 ....../PrepareMaterials

		       0.936 Mt    31.951 %         1 x        0.936 Mt        0.000 Mt        0.936 Mt   100.000 % 	 ....../PrepareDrawBundle

		       4.908 Mt     2.831 %         1 x        4.908 Mt        0.000 Mt        0.015 Mt     0.306 % 	 ..../DeferredRLPrep

		       4.893 Mt    99.694 %         1 x        4.893 Mt        0.000 Mt        2.959 Mt    60.466 % 	 ...../PrepareForRendering

		       0.050 Mt     1.028 %         1 x        0.050 Mt        0.000 Mt        0.050 Mt   100.000 % 	 ....../PrepareMaterials

		       1.102 Mt    22.527 %         1 x        1.102 Mt        0.000 Mt        1.102 Mt   100.000 % 	 ....../BuildMaterialCache

		       0.782 Mt    15.979 %         1 x        0.782 Mt        0.000 Mt        0.782 Mt   100.000 % 	 ....../PrepareDrawBundle

		      22.434 Mt    12.941 %         1 x       22.434 Mt        0.000 Mt        7.421 Mt    33.078 % 	 ..../RayTracingRLPrep

		       0.631 Mt     2.812 %         1 x        0.631 Mt        0.000 Mt        0.631 Mt   100.000 % 	 ...../PrepareAccelerationStructures

		       0.072 Mt     0.319 %         1 x        0.072 Mt        0.000 Mt        0.072 Mt   100.000 % 	 ...../PrepareCache

		       2.158 Mt     9.618 %         1 x        2.158 Mt        0.000 Mt        2.158 Mt   100.000 % 	 ...../BuildCache

		       6.045 Mt    26.945 %         1 x        6.045 Mt        0.000 Mt        0.015 Mt     0.253 % 	 ...../BuildAccelerationStructures

		       6.029 Mt    99.747 %         1 x        6.029 Mt        0.000 Mt        2.936 Mt    48.692 % 	 ....../AccelerationStructureBuild

		       1.996 Mt    33.111 %         1 x        1.996 Mt        0.000 Mt        1.996 Mt   100.000 % 	 ......./BuildBottomLevelAS

		       1.097 Mt    18.197 %         1 x        1.097 Mt        0.000 Mt        1.097 Mt   100.000 % 	 ......./BuildTopLevelAS

		       6.108 Mt    27.227 %         1 x        6.108 Mt        0.000 Mt        6.108 Mt   100.000 % 	 ...../BuildShaderTables

		      12.566 Mt     7.249 %         1 x       12.566 Mt        0.000 Mt       12.566 Mt   100.000 % 	 ..../GpuSidePrep

		   29147.610 Mt    97.891 %      4092 x        7.123 Mt       54.105 Mt     1136.977 Mt     3.901 % 	 ../MainLoop

		    1215.363 Mt     4.170 %      1747 x        0.696 Mt        0.906 Mt     1215.363 Mt   100.000 % 	 .../Update

		   26795.270 Mt    91.930 %      2827 x        9.478 Mt       10.222 Mt    26759.211 Mt    99.865 % 	 .../Render

		      19.732 Mt     0.074 %      2827 x        0.007 Mt        0.011 Mt       19.732 Mt   100.000 % 	 ..../DeferredRLPrep

		      16.328 Mt     0.061 %      2827 x        0.006 Mt        0.010 Mt       16.328 Mt   100.000 % 	 ..../RayTracingRLPrep

	WindowThread:

		   29766.820 Mt   100.000 %         1 x    29766.821 Mt    29766.819 Mt    29766.820 Mt   100.000 % 	 /

	GpuThread:

		   13725.703 Mt   100.000 %      2827 x        4.855 Mt        3.024 Mt      164.814 Mt     1.201 % 	 /

		       0.268 Mt     0.002 %         1 x        0.268 Mt        0.000 Mt        0.008 Mt     2.959 % 	 ./ScenePrep

		       0.260 Mt    97.041 %         1 x        0.260 Mt        0.000 Mt        0.002 Mt     0.664 % 	 ../AccelerationStructureBuild

		       0.174 Mt    66.831 %         1 x        0.174 Mt        0.000 Mt        0.174 Mt   100.000 % 	 .../BuildBottomLevelASGpu

		       0.085 Mt    32.506 %         1 x        0.085 Mt        0.000 Mt        0.085 Mt   100.000 % 	 .../BuildTopLevelASGpu

		   13559.125 Mt    98.786 %      2827 x        4.796 Mt        3.023 Mt        3.622 Mt     0.027 % 	 ./RayTracing

		     635.799 Mt     4.689 %      2827 x        0.225 Mt        0.055 Mt      635.799 Mt   100.000 % 	 ../DeferredRL

		    4592.133 Mt    33.867 %      2827 x        1.624 Mt        0.356 Mt     4592.133 Mt   100.000 % 	 ../RayTracingRL

		    8327.571 Mt    61.417 %      2827 x        2.946 Mt        2.610 Mt     8327.571 Mt   100.000 % 	 ../ResolveRL

		       1.497 Mt     0.011 %      2827 x        0.001 Mt        0.000 Mt        1.497 Mt   100.000 % 	 ./Present


	============================


