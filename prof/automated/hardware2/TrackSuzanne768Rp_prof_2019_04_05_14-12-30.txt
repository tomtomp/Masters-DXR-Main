Configuration: 
Command line parameters  = E:\programming\FIT-DIP\Masters-DXR-Main-git\build\Measurement\x64\Debug\Measurement.exe --scene Suzanne/Suzanne.gltf --profile-track Suzanne/Suzanne.trk --rt-mode-pure --width 1024 --height 768 --profile-output TrackSuzanne768Rp 
Window width             = 1024
Window height            = 768
Window title             = Direct3D App
Back-buffer count        = 3
Target FPS               = 0
Target UPS               = 60
Tearing                  = enabled
VSync                    = disabled
GUI rendering            = disabled
Debug layer              = enabled
GPU validation           = disabled
GPU profiling            = enabled
Render mode              = Ray Tracing
Loaded scene file        = Suzanne/Suzanne.gltf
Using testing scene      = disabled
Duplication              = 1
Duplication in cube      = disabled
Duplication offset       = 10
BLAS duplication         = disabled
Rays per pixel           = 10

ProfilingBuildAggregator: 
BuildBottomLevelAS	,	1.4727
BuildBottomLevelASGpu	,	0.445152
BuildTopLevelAS	,	0.9655
BuildTopLevelASGpu	,	0.061376

ProfilingAggregator: 
Render	,	2.4192	,	3.9755	,	2.777	,	2.8705	,	2.5593	,	3.2624	,	3.7272	,	3.4568	,	4.0589	,	2.6191	,	5.1591	,	3.9544	,	5.8182	,	5.6509	,	2.9748	,	3.4754	,	3.3905	,	3.8347	,	6.386	,	3.6605	,	2.668	,	3.7541	,	3.1445	,	2.8578
Update	,	0.3027	,	0.5449	,	0.304	,	0.3983	,	0.2816	,	0.512	,	0.7408	,	0.6591	,	0.2571	,	0.8199	,	0.5768	,	0.2704	,	0.5029	,	0.8057	,	0.6028	,	0.2745	,	0.8229	,	0.6755	,	0.7469	,	0.5344	,	0.6532	,	0.5726	,	0.6146	,	0.5832
RayTracing	,	0.924544	,	1.08739	,	1.18342	,	1.1399	,	1.06179	,	0.978144	,	0.942496	,	0.926368	,	0.964896	,	1.11306	,	1.53018	,	1.19069	,	1.10541	,	1.15766	,	1.14765	,	1.29901	,	1.34573	,	1.33632	,	1.69418	,	1.53094	,	1.18989	,	0.98448	,	1.0112	,	1.05043
RayTracingRL	,	0.248992	,	0.364736	,	0.43424	,	0.376704	,	0.270144	,	0.186336	,	0.150784	,	0.135392	,	0.172768	,	0.265024	,	0.336224	,	0.258784	,	0.178688	,	0.186912	,	0.250944	,	0.397888	,	0.442656	,	0.55232	,	0.908896	,	0.72688	,	0.459008	,	0.255872	,	0.19168	,	0.18592

FpsAggregator: 
FPS	,	330	,	305	,	285	,	294	,	291	,	323	,	331	,	358	,	331	,	309	,	269	,	275	,	290	,	293	,	292	,	281	,	265	,	259	,	228	,	215	,	268	,	294	,	313	,	310
UPS	,	59	,	61	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	61	,	60	,	60	,	60	,	60	,	60	,	60	,	61	,	60	,	60	,	60
FrameTime	,	3.03225	,	3.29255	,	3.51838	,	3.40252	,	3.437	,	3.09991	,	3.02747	,	2.80048	,	3.02936	,	3.23683	,	3.72819	,	3.63907	,	3.45102	,	3.43496	,	3.42482	,	3.56131	,	3.77754	,	3.86937	,	4.39354	,	4.65527	,	3.74312	,	3.40377	,	3.20076	,	3.22813
GigaRays/s	,	2.59356	,	2.38852	,	2.23521	,	2.31132	,	2.28813	,	2.53695	,	2.59766	,	2.8082	,	2.59603	,	2.42964	,	2.10942	,	2.16108	,	2.27884	,	2.2895	,	2.29627	,	2.20827	,	2.08186	,	2.03245	,	1.78997	,	1.68934	,	2.10101	,	2.31048	,	2.45701	,	2.43618

AccelerationStructure: 
Ray tracing acceleration structure building statistics: 
	Bottom Level Acceleration Structure: 
		Maximum [B]: 283264
		Minimum [B]: 283264
		Currently Allocated [B]: 283264
		Currently Created [num]: 1
		Current Average [B]: 283264
		Maximum Triangles [num]: 3936
		Minimum Triangles [num]: 3936
		Total Triangles [num]: 3936
		Maximum Meshes [num]: 1
		Minimum Meshes [num]: 1
		Total Meshes [num]: 1
	Top Level Acceleration Structure: 
		Maximum [B]: 64
		Minimum [B]: 64
		Currently Allocated [B]: 64
		Total Allocated [B]: 64
		Total Created [num]: 1
		Average Allocated [B]: 64
		Maximum Instances [num]: 1
		Minimum Instances [num]: 1
		Total Instances [num]: 1
	Scratch Buffer: 
		Maximum [B]: 83968
		Minimum [B]: 83968
		Currently Allocated [B]: 83968
		Total Allocated [B]: 83968
		Total Created [num]: 1
		Average Allocated [B]: 83968
ProfilingStack: 
[PROF] Profiling statistics: 
	Scopes entered : 51830
	Scopes exited : 51829
	Overhead per scope [ticks] : 1000.2
	Threads entered : 3
	Threads exited : 0
	============================
		         Sum[Mt]   Parent[%]  Samples[num]       Avg[Mt]   Last/Curr[Mt]        Self[Mt]     Self[%] 	 Name

	main:

		   24694.801 Mt   100.000 %         1 x    24694.805 Mt    24694.799 Mt        0.750 Mt     0.003 % 	 /

		   24694.196 Mt    99.998 %         1 x    24694.201 Mt    24694.196 Mt      391.435 Mt     1.585 % 	 ./Main application loop

		       2.816 Mt     0.011 %         1 x        2.816 Mt        0.000 Mt        2.816 Mt   100.000 % 	 ../TexturedRLInitialization

		       5.775 Mt     0.023 %         1 x        5.775 Mt        0.000 Mt        5.775 Mt   100.000 % 	 ../DeferredRLInitialization

		      49.511 Mt     0.200 %         1 x       49.511 Mt        0.000 Mt        2.605 Mt     5.262 % 	 ../RayTracingRLInitialization

		      46.906 Mt    94.738 %         1 x       46.906 Mt        0.000 Mt       46.906 Mt   100.000 % 	 .../PipelineBuild

		      10.675 Mt     0.043 %         1 x       10.675 Mt        0.000 Mt       10.675 Mt   100.000 % 	 ../ResolveRLInitialization

		     229.146 Mt     0.928 %         1 x      229.146 Mt        0.000 Mt        2.387 Mt     1.042 % 	 ../Initialization

		     226.758 Mt    98.958 %         1 x      226.758 Mt        0.000 Mt        1.291 Mt     0.569 % 	 .../ScenePrep

		     194.263 Mt    85.670 %         1 x      194.263 Mt        0.000 Mt       16.364 Mt     8.424 % 	 ..../SceneLoad

		     163.999 Mt    84.421 %         1 x      163.999 Mt        0.000 Mt       19.601 Mt    11.952 % 	 ...../glTF-LoadScene

		     144.398 Mt    88.048 %         2 x       72.199 Mt        0.000 Mt      144.398 Mt   100.000 % 	 ....../glTF-LoadImageData

		       7.543 Mt     3.883 %         1 x        7.543 Mt        0.000 Mt        7.543 Mt   100.000 % 	 ...../glTF-CreateScene

		       6.356 Mt     3.272 %         1 x        6.356 Mt        0.000 Mt        6.356 Mt   100.000 % 	 ...../SceneDesc-CreateScene

		       1.172 Mt     0.517 %         1 x        1.172 Mt        0.000 Mt        0.005 Mt     0.401 % 	 ..../TexturedRLPrep

		       1.167 Mt    99.599 %         1 x        1.167 Mt        0.000 Mt        0.407 Mt    34.844 % 	 ...../PrepareForRendering

		       0.477 Mt    40.831 %         1 x        0.477 Mt        0.000 Mt        0.477 Mt   100.000 % 	 ....../PrepareMaterials

		       0.284 Mt    24.325 %         1 x        0.284 Mt        0.000 Mt        0.284 Mt   100.000 % 	 ....../PrepareDrawBundle

		       3.925 Mt     1.731 %         1 x        3.925 Mt        0.000 Mt        0.004 Mt     0.110 % 	 ..../DeferredRLPrep

		       3.921 Mt    99.890 %         1 x        3.921 Mt        0.000 Mt        1.540 Mt    39.288 % 	 ...../PrepareForRendering

		       0.015 Mt     0.375 %         1 x        0.015 Mt        0.000 Mt        0.015 Mt   100.000 % 	 ....../PrepareMaterials

		       2.095 Mt    53.423 %         1 x        2.095 Mt        0.000 Mt        2.095 Mt   100.000 % 	 ....../BuildMaterialCache

		       0.271 Mt     6.914 %         1 x        0.271 Mt        0.000 Mt        0.271 Mt   100.000 % 	 ....../PrepareDrawBundle

		      15.569 Mt     6.866 %         1 x       15.569 Mt        0.000 Mt        3.914 Mt    25.139 % 	 ..../RayTracingRLPrep

		       0.239 Mt     1.537 %         1 x        0.239 Mt        0.000 Mt        0.239 Mt   100.000 % 	 ...../PrepareAccelerationStructures

		       0.023 Mt     0.149 %         1 x        0.023 Mt        0.000 Mt        0.023 Mt   100.000 % 	 ...../PrepareCache

		       0.475 Mt     3.053 %         1 x        0.475 Mt        0.000 Mt        0.475 Mt   100.000 % 	 ...../BuildCache

		       9.177 Mt    58.947 %         1 x        9.177 Mt        0.000 Mt        0.006 Mt     0.066 % 	 ...../BuildAccelerationStructures

		       9.171 Mt    99.934 %         1 x        9.171 Mt        0.000 Mt        6.733 Mt    73.414 % 	 ....../AccelerationStructureBuild

		       1.473 Mt    16.058 %         1 x        1.473 Mt        0.000 Mt        1.473 Mt   100.000 % 	 ......./BuildBottomLevelAS

		       0.966 Mt    10.528 %         1 x        0.966 Mt        0.000 Mt        0.966 Mt   100.000 % 	 ......./BuildTopLevelAS

		       1.740 Mt    11.175 %         1 x        1.740 Mt        0.000 Mt        1.740 Mt   100.000 % 	 ...../BuildShaderTables

		      10.538 Mt     4.647 %         1 x       10.538 Mt        0.000 Mt       10.538 Mt   100.000 % 	 ..../GpuSidePrep

		   24004.839 Mt    97.208 %      8284 x        2.898 Mt        3.743 Mt      294.944 Mt     1.229 % 	 ../MainLoop

		     842.965 Mt     3.512 %      1443 x        0.584 Mt        0.379 Mt      842.965 Mt   100.000 % 	 .../Update

		   22866.929 Mt    95.260 %      7011 x        3.262 Mt        3.352 Mt    22825.236 Mt    99.818 % 	 .../Render

		      41.694 Mt     0.182 %      7011 x        0.006 Mt        0.005 Mt       41.694 Mt   100.000 % 	 ..../RayTracingRLPrep

	WindowThread:

		   24693.727 Mt   100.000 %         1 x    24693.729 Mt    24693.726 Mt    24693.727 Mt   100.000 % 	 /

	GpuThread:

		    8123.023 Mt   100.000 %      7011 x        1.159 Mt        1.055 Mt      118.655 Mt     1.461 % 	 /

		       0.512 Mt     0.006 %         1 x        0.512 Mt        0.000 Mt        0.005 Mt     0.931 % 	 ./ScenePrep

		       0.507 Mt    99.069 %         1 x        0.507 Mt        0.000 Mt        0.001 Mt     0.145 % 	 ../AccelerationStructureBuild

		       0.445 Mt    87.755 %         1 x        0.445 Mt        0.000 Mt        0.445 Mt   100.000 % 	 .../BuildBottomLevelASGpu

		       0.061 Mt    12.099 %         1 x        0.061 Mt        0.000 Mt        0.061 Mt   100.000 % 	 .../BuildTopLevelASGpu

		    7998.043 Mt    98.461 %      7011 x        1.141 Mt        1.053 Mt        9.840 Mt     0.123 % 	 ./RayTracing

		    2188.450 Mt    27.362 %      7011 x        0.312 Mt        0.186 Mt     2188.450 Mt   100.000 % 	 ../RayTracingRL

		    5799.752 Mt    72.515 %      7011 x        0.827 Mt        0.865 Mt     5799.752 Mt   100.000 % 	 ../ResolveRL

		       5.814 Mt     0.072 %      7011 x        0.001 Mt        0.001 Mt        5.814 Mt   100.000 % 	 ./Present


	============================


