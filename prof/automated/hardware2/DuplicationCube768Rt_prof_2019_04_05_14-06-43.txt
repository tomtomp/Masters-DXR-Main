Configuration: 
Command line parameters  = E:\programming\FIT-DIP\Masters-DXR-Main-git\build\Measurement\x64\Debug\Measurement.exe --scene Cube/Cube.gltf --duplication-cube --profile-duplication 10 --rt-mode --width 1024 --height 768 --profile-output DuplicationCube768Rt 
Window width             = 1024
Window height            = 768
Window title             = Direct3D App
Back-buffer count        = 3
Target FPS               = 60
Target UPS               = 60
Tearing                  = disabled
VSync                    = disabled
GUI rendering            = disabled
Debug layer              = enabled
GPU validation           = disabled
GPU profiling            = enabled
Render mode              = Ray Tracing
Loaded scene file        = Cube/Cube.gltf
Using testing scene      = disabled
Duplication              = 10
Duplication in cube      = enabled
Duplication offset       = 10
BLAS duplication         = enabled
Rays per pixel           = 9

ProfilingBuildAggregator: 
BuildBottomLevelAS	,	2.3111	,	37.8201	,	85.662	,	209.328	,	300.173	,	408.112	,	605.684	,	726.98	,	1258.86
BuildBottomLevelASGpu	,	0.174592	,	12.0188	,	41.6963	,	59.8791	,	93.6523	,	56.6315	,	159.14	,	156.352	,	299.884
BuildTopLevelAS	,	1.412	,	1.0899	,	1.587	,	1.3832	,	1.2197	,	2.2651	,	1.0553	,	0.9801	,	1.7492
BuildTopLevelASGpu	,	0.088224	,	0.709888	,	0.557024	,	0.59408	,	0.59504	,	0.220576	,	0.400768	,	0.26496	,	0.30224

ProfilingAggregator: 
Render	,	3.7466	,	5.6201	,	9.8823	,	9.5301	,	13.2564	,	4.8237	,	6.2789	,	977.61	,	1185.73	,	1949.2
Update	,	0.2433	,	0.569	,	0.5877	,	0.67	,	1.2158	,	0.4693	,	0.795	,	0.7363	,	0.8835	,	1.2236
RayTracing	,	0.918816	,	2.82144	,	4.87382	,	4.6545	,	5.44934	,	0.991488	,	1.00045	,	162.276	,	157.905	,	302.014
RayTracingRL	,	0.093216	,	0.500032	,	1.60586	,	1.67386	,	2.46742	,	0.167904	,	0.171264	,	0.78528	,	0.207776	,	0.22592

FpsAggregator: 
FPS	,	55	,	55	,	53	,	44	,	32	,	20	,	12	,	1	,	1	,	1
UPS	,	59	,	60	,	61	,	60	,	60	,	61	,	60	,	3	,	68	,	94
FrameTime	,	18.1819	,	18.2249	,	19.0289	,	22.8917	,	31.4404	,	50.2273	,	83.3336	,	1185.36	,	1567.69	,	2466.1
GigaRays/s	,	0.389283	,	0.388363	,	0.371954	,	0.30919	,	0.22512	,	0.140917	,	0.0849344	,	0.0059711	,	0.00451484	,	0.00287008

AccelerationStructure: 
Ray tracing acceleration structure building statistics: 
	Bottom Level Acceleration Structure: 
		Maximum [B]: 6208
		Minimum [B]: 6208
		Currently Allocated [B]: 6208000
		Currently Created [num]: 1000
		Current Average [B]: 6208
		Maximum Triangles [num]: 12
		Minimum Triangles [num]: 12
		Total Triangles [num]: 12000
		Maximum Meshes [num]: 1
		Minimum Meshes [num]: 1
		Total Meshes [num]: 1000
	Top Level Acceleration Structure: 
		Maximum [B]: 64000
		Minimum [B]: 64000
		Currently Allocated [B]: 64000
		Total Allocated [B]: 64000
		Total Created [num]: 1
		Average Allocated [B]: 64000
		Maximum Instances [num]: 1000
		Minimum Instances [num]: 1000
		Total Instances [num]: 1000
	Scratch Buffer: 
		Maximum [B]: 79360
		Minimum [B]: 79360
		Currently Allocated [B]: 79360
		Total Allocated [B]: 79360
		Total Created [num]: 1
		Average Allocated [B]: 79360
ProfilingStack: 
[PROF] Profiling statistics: 
	Scopes entered : 514432
	Scopes exited : 514431
	Overhead per scope [ticks] : 994.8
	Threads entered : 3
	Threads exited : 0
	============================
		         Sum[Mt]   Parent[%]  Samples[num]       Avg[Mt]   Last/Curr[Mt]        Self[Mt]     Self[%] 	 Name

	main:

		   13223.299 Mt   100.000 %         1 x    13223.300 Mt    13223.298 Mt        0.746 Mt     0.006 % 	 /

		   13222.595 Mt    99.995 %         1 x    13222.597 Mt    13222.595 Mt     1688.183 Mt    12.767 % 	 ./Main application loop

		       3.008 Mt     0.023 %         1 x        3.008 Mt        0.000 Mt        3.008 Mt   100.000 % 	 ../TexturedRLInitialization

		       5.859 Mt     0.044 %         1 x        5.859 Mt        0.000 Mt        5.859 Mt   100.000 % 	 ../DeferredRLInitialization

		      65.412 Mt     0.495 %         1 x       65.412 Mt        0.000 Mt        2.428 Mt     3.712 % 	 ../RayTracingRLInitialization

		      62.984 Mt    96.288 %         1 x       62.984 Mt        0.000 Mt       62.984 Mt   100.000 % 	 .../PipelineBuild

		      11.547 Mt     0.087 %         1 x       11.547 Mt        0.000 Mt       11.547 Mt   100.000 % 	 ../ResolveRLInitialization

		     176.554 Mt     1.335 %         1 x      176.554 Mt        0.000 Mt        9.093 Mt     5.150 % 	 ../Initialization

		     167.461 Mt    94.850 %         1 x      167.461 Mt        0.000 Mt        2.301 Mt     1.374 % 	 .../ScenePrep

		     120.118 Mt    71.729 %         1 x      120.118 Mt        0.000 Mt       19.658 Mt    16.366 % 	 ..../SceneLoad

		      82.842 Mt    68.967 %         1 x       82.842 Mt        0.000 Mt       17.253 Mt    20.827 % 	 ...../glTF-LoadScene

		      65.589 Mt    79.173 %         2 x       32.794 Mt        0.000 Mt       65.589 Mt   100.000 % 	 ....../glTF-LoadImageData

		       8.768 Mt     7.300 %         1 x        8.768 Mt        0.000 Mt        8.768 Mt   100.000 % 	 ...../glTF-CreateScene

		       8.849 Mt     7.367 %         1 x        8.849 Mt        0.000 Mt        8.849 Mt   100.000 % 	 ...../SceneDesc-CreateScene

		       2.665 Mt     1.592 %         1 x        2.665 Mt        0.000 Mt        0.015 Mt     0.578 % 	 ..../TexturedRLPrep

		       2.650 Mt    99.422 %         1 x        2.650 Mt        0.000 Mt        0.818 Mt    30.865 % 	 ...../PrepareForRendering

		       0.896 Mt    33.820 %         1 x        0.896 Mt        0.000 Mt        0.896 Mt   100.000 % 	 ....../PrepareMaterials

		       0.936 Mt    35.315 %         1 x        0.936 Mt        0.000 Mt        0.936 Mt   100.000 % 	 ....../PrepareDrawBundle

		       6.208 Mt     3.707 %         1 x        6.208 Mt        0.000 Mt        0.015 Mt     0.242 % 	 ..../DeferredRLPrep

		       6.193 Mt    99.758 %         1 x        6.193 Mt        0.000 Mt        3.408 Mt    55.034 % 	 ...../PrepareForRendering

		       0.050 Mt     0.812 %         1 x        0.050 Mt        0.000 Mt        0.050 Mt   100.000 % 	 ....../PrepareMaterials

		       1.804 Mt    29.123 %         1 x        1.804 Mt        0.000 Mt        1.804 Mt   100.000 % 	 ....../BuildMaterialCache

		       0.931 Mt    15.031 %         1 x        0.931 Mt        0.000 Mt        0.931 Mt   100.000 % 	 ....../PrepareDrawBundle

		      22.771 Mt    13.598 %         1 x       22.771 Mt        0.000 Mt        7.206 Mt    31.645 % 	 ..../RayTracingRLPrep

		       0.730 Mt     3.207 %         1 x        0.730 Mt        0.000 Mt        0.730 Mt   100.000 % 	 ...../PrepareAccelerationStructures

		       0.078 Mt     0.343 %         1 x        0.078 Mt        0.000 Mt        0.078 Mt   100.000 % 	 ...../PrepareCache

		       1.334 Mt     5.860 %         1 x        1.334 Mt        0.000 Mt        1.334 Mt   100.000 % 	 ...../BuildCache

		       7.557 Mt    33.188 %         1 x        7.557 Mt        0.000 Mt        0.016 Mt     0.205 % 	 ...../BuildAccelerationStructures

		       7.542 Mt    99.795 %         1 x        7.542 Mt        0.000 Mt        3.818 Mt    50.632 % 	 ....../AccelerationStructureBuild

		       2.311 Mt    30.645 %         1 x        2.311 Mt        0.000 Mt        2.311 Mt   100.000 % 	 ......./BuildBottomLevelAS

		       1.412 Mt    18.723 %         1 x        1.412 Mt        0.000 Mt        1.412 Mt   100.000 % 	 ......./BuildTopLevelAS

		       5.865 Mt    25.759 %         1 x        5.865 Mt        0.000 Mt        5.865 Mt   100.000 % 	 ...../BuildShaderTables

		      13.399 Mt     8.001 %         1 x       13.399 Mt        0.000 Mt       13.399 Mt   100.000 % 	 ..../GpuSidePrep

		   11272.032 Mt    85.248 %    511326 x        0.022 Mt      240.478 Mt     1499.641 Mt    13.304 % 	 ../MainLoop

		    1826.569 Mt    16.204 %       734 x        2.489 Mt        1.193 Mt     1826.569 Mt   100.000 % 	 .../Update

		    7945.822 Mt    70.491 %       275 x       28.894 Mt       11.498 Mt     2973.637 Mt    37.424 % 	 .../Render

		      86.418 Mt     1.088 %       275 x        0.314 Mt        0.004 Mt        1.318 Mt     1.525 % 	 ..../DeferredRLPrep

		      85.100 Mt    98.475 %         9 x        9.456 Mt        0.000 Mt        6.310 Mt     7.415 % 	 ...../PrepareForRendering

		       8.241 Mt     9.683 %         9 x        0.916 Mt        0.000 Mt        8.241 Mt   100.000 % 	 ....../PrepareMaterials

		       0.009 Mt     0.010 %         9 x        0.001 Mt        0.000 Mt        0.009 Mt   100.000 % 	 ....../BuildMaterialCache

		      70.541 Mt    82.891 %         9 x        7.838 Mt        0.000 Mt       70.541 Mt   100.000 % 	 ....../PrepareDrawBundle

		    4885.766 Mt    61.488 %       275 x       17.766 Mt        0.004 Mt       34.824 Mt     0.713 % 	 ..../RayTracingRLPrep

		    1025.616 Mt    20.992 %         9 x      113.957 Mt        0.000 Mt     1025.616 Mt   100.000 % 	 ...../PrepareAccelerationStructures

		       8.137 Mt     0.167 %         9 x        0.904 Mt        0.000 Mt        8.137 Mt   100.000 % 	 ...../PrepareCache

		       0.008 Mt     0.000 %         9 x        0.001 Mt        0.000 Mt        0.008 Mt   100.000 % 	 ...../BuildCache

		    3721.800 Mt    76.176 %         9 x      413.533 Mt        0.000 Mt        0.033 Mt     0.001 % 	 ...../BuildAccelerationStructures

		    3721.767 Mt    99.999 %         9 x      413.530 Mt        0.000 Mt       64.436 Mt     1.731 % 	 ....../AccelerationStructureBuild

		    3644.792 Mt    97.932 %         9 x      404.977 Mt        0.000 Mt     3644.792 Mt   100.000 % 	 ......./BuildBottomLevelAS

		      12.539 Mt     0.337 %         9 x        1.393 Mt        0.000 Mt       12.539 Mt   100.000 % 	 ......./BuildTopLevelAS

		      95.380 Mt     1.952 %         9 x       10.598 Mt        0.000 Mt       95.380 Mt   100.000 % 	 ...../BuildShaderTables

	WindowThread:

		   13219.343 Mt   100.000 %         1 x    13219.344 Mt    13219.343 Mt    13219.343 Mt   100.000 % 	 /

	GpuThread:

		    1845.900 Mt   100.000 %       275 x        6.712 Mt        0.787 Mt      142.186 Mt     7.703 % 	 /

		       0.270 Mt     0.015 %         1 x        0.270 Mt        0.000 Mt        0.006 Mt     2.274 % 	 ./ScenePrep

		       0.264 Mt    97.726 %         1 x        0.264 Mt        0.000 Mt        0.001 Mt     0.485 % 	 ../AccelerationStructureBuild

		       0.175 Mt    66.109 %         1 x        0.175 Mt        0.000 Mt        0.175 Mt   100.000 % 	 .../BuildBottomLevelASGpu

		       0.088 Mt    33.406 %         1 x        0.088 Mt        0.000 Mt        0.088 Mt   100.000 % 	 .../BuildTopLevelASGpu

		    1702.977 Mt    92.257 %       275 x        6.193 Mt        0.786 Mt        2.676 Mt     0.157 % 	 ./RayTracing

		      47.464 Mt     2.787 %       275 x        0.173 Mt        0.033 Mt       47.464 Mt   100.000 % 	 ../DeferredRL

		     238.532 Mt    14.007 %       275 x        0.867 Mt        0.160 Mt      238.532 Mt   100.000 % 	 ../RayTracingRL

		     530.013 Mt    31.123 %       275 x        1.927 Mt        0.588 Mt      530.013 Mt   100.000 % 	 ../ResolveRL

		     884.291 Mt    51.926 %         9 x       98.255 Mt        0.000 Mt        0.021 Mt     0.002 % 	 ../AccelerationStructureBuild

		     880.522 Mt    99.574 %         9 x       97.836 Mt        0.000 Mt      880.522 Mt   100.000 % 	 .../BuildBottomLevelASGpu

		       3.748 Mt     0.424 %         9 x        0.416 Mt        0.000 Mt        3.748 Mt   100.000 % 	 .../BuildTopLevelASGpu

		       0.466 Mt     0.025 %       275 x        0.002 Mt        0.001 Mt        0.466 Mt   100.000 % 	 ./Present


	============================


