Configuration: 
Command line parameters  = E:\programming\FIT-DIP\Masters-DXR-Main-git\build\Measurement\x64\Debug\Measurement.exe --scene Cube/Cube.gltf --profile-track Cube/Cube.trk --rt-mode-pure --width 2560 --height 1440 --profile-output TrackCube1440Rp 
Window width             = 2560
Window height            = 1421
Window title             = Direct3D App
Back-buffer count        = 3
Target FPS               = 0
Target UPS               = 60
Tearing                  = enabled
VSync                    = disabled
GUI rendering            = disabled
Debug layer              = enabled
GPU validation           = disabled
GPU profiling            = enabled
Render mode              = Ray Tracing
Loaded scene file        = Cube/Cube.gltf
Using testing scene      = disabled
Duplication              = 1
Duplication in cube      = disabled
Duplication offset       = 10
BLAS duplication         = disabled
Rays per pixel           = 10

ProfilingBuildAggregator: 
BuildBottomLevelAS	,	1.4593
BuildBottomLevelASGpu	,	0.181376
BuildTopLevelAS	,	1.0663
BuildTopLevelASGpu	,	0.084896

ProfilingAggregator: 
Render	,	8.9201	,	7.8359	,	6.6471	,	9.4547	,	7.0233	,	10.5621	,	9.4153	,	8.4933	,	10.0387	,	10.1485	,	10.1431	,	10.3052	,	8.5569	,	9.8231	,	9.0309	,	9.7914	,	9.4609	,	9.726	,	10.6735	,	9.9317	,	9.4746	,	9.4044	,	9.7044	,	7.4148	,	8.9407	,	8.8562	,	8.4307	,	6.7096	,	8.1008
Update	,	0.8671	,	0.7981	,	0.3946	,	0.8144	,	0.665	,	0.9143	,	0.7876	,	0.5734	,	0.833	,	0.8471	,	0.8932	,	0.9146	,	0.6361	,	0.9236	,	0.7384	,	0.7871	,	0.741	,	0.598	,	0.7807	,	0.5294	,	0.5958	,	0.6805	,	0.8963	,	0.8181	,	0.7357	,	0.8652	,	0.8668	,	0.7108	,	0.8458
RayTracing	,	3.81363	,	4.18474	,	4.5024	,	4.34611	,	4.68064	,	5.39123	,	5.45334	,	5.2512	,	5.59459	,	5.30333	,	5.14986	,	5.32448	,	5.33658	,	4.6536	,	4.91059	,	5.55837	,	5.69043	,	6.21232	,	6.33072	,	6.87965	,	5.75094	,	4.85891	,	4.7009	,	4.09405	,	3.8968	,	3.93936	,	3.44387	,	3.34064	,	3.27606
RayTracingRL	,	1.08723	,	1.42851	,	1.52074	,	1.60077	,	1.95542	,	2.37251	,	2.47277	,	2.50483	,	2.87069	,	2.56099	,	2.39024	,	2.56557	,	2.23133	,	1.8865	,	2.13878	,	2.50182	,	2.93949	,	3.44269	,	3.5824	,	3.89562	,	2.99562	,	2.09738	,	1.66659	,	1.32051	,	1.1223	,	0.884832	,	0.691392	,	0.585408	,	0.52368

FpsAggregator: 
FPS	,	103	,	113	,	117	,	118	,	106	,	101	,	99	,	102	,	92	,	91	,	100	,	90	,	98	,	108	,	102	,	98	,	93	,	91	,	86	,	87	,	90	,	98	,	107	,	115	,	120	,	122	,	126	,	135	,	134
UPS	,	59	,	60	,	61	,	60	,	60	,	61	,	60	,	61	,	60	,	60	,	60	,	60	,	60	,	61	,	60	,	60	,	61	,	60	,	60	,	61	,	60	,	60	,	61	,	60	,	60	,	61	,	60	,	60	,	61
FrameTime	,	9.73723	,	8.87671	,	8.59787	,	8.54921	,	9.45547	,	9.99425	,	10.1333	,	9.88602	,	10.8769	,	10.9993	,	10.0343	,	11.1335	,	10.2633	,	9.31779	,	9.84721	,	10.2596	,	10.8182	,	11.0598	,	11.6611	,	11.5762	,	11.1537	,	10.268	,	9.4053	,	8.74612	,	8.36845	,	8.269	,	7.97517	,	7.42336	,	7.52621
GigaRays/s	,	3.73593	,	4.09809	,	4.231	,	4.25508	,	3.84726	,	3.63985	,	3.58992	,	3.6797	,	3.34449	,	3.30728	,	3.62531	,	3.26739	,	3.54444	,	3.9041	,	3.6942	,	3.5457	,	3.36261	,	3.28917	,	3.11957	,	3.14246	,	3.26147	,	3.54282	,	3.86777	,	4.15928	,	4.34699	,	4.39927	,	4.56136	,	4.90042	,	4.83346

AccelerationStructure: 
Ray tracing acceleration structure building statistics: 
	Bottom Level Acceleration Structure: 
		Maximum [B]: 6208
		Minimum [B]: 6208
		Currently Allocated [B]: 6208
		Currently Created [num]: 1
		Current Average [B]: 6208
		Maximum Triangles [num]: 12
		Minimum Triangles [num]: 12
		Total Triangles [num]: 12
		Maximum Meshes [num]: 1
		Minimum Meshes [num]: 1
		Total Meshes [num]: 1
	Top Level Acceleration Structure: 
		Maximum [B]: 64
		Minimum [B]: 64
		Currently Allocated [B]: 64
		Total Allocated [B]: 64
		Total Created [num]: 1
		Average Allocated [B]: 64
		Maximum Instances [num]: 1
		Minimum Instances [num]: 1
		Total Instances [num]: 1
	Scratch Buffer: 
		Maximum [B]: 3584
		Minimum [B]: 3584
		Currently Allocated [B]: 3584
		Total Allocated [B]: 3584
		Total Created [num]: 1
		Average Allocated [B]: 3584
ProfilingStack: 
[PROF] Profiling statistics: 
	Scopes entered : 26621
	Scopes exited : 26620
	Overhead per scope [ticks] : 1002.5
	Threads entered : 3
	Threads exited : 0
	============================
		         Sum[Mt]   Parent[%]  Samples[num]       Avg[Mt]   Last/Curr[Mt]        Self[Mt]     Self[%] 	 Name

	main:

		   29687.660 Mt   100.000 %         1 x    29687.664 Mt    29687.658 Mt        1.006 Mt     0.003 % 	 /

		   29686.812 Mt    99.997 %         1 x    29686.819 Mt    29686.811 Mt      354.029 Mt     1.193 % 	 ./Main application loop

		       2.782 Mt     0.009 %         1 x        2.782 Mt        0.000 Mt        2.782 Mt   100.000 % 	 ../TexturedRLInitialization

		       6.778 Mt     0.023 %         1 x        6.778 Mt        0.000 Mt        6.778 Mt   100.000 % 	 ../DeferredRLInitialization

		      49.039 Mt     0.165 %         1 x       49.039 Mt        0.000 Mt        2.468 Mt     5.034 % 	 ../RayTracingRLInitialization

		      46.571 Mt    94.966 %         1 x       46.571 Mt        0.000 Mt       46.571 Mt   100.000 % 	 .../PipelineBuild

		      11.080 Mt     0.037 %         1 x       11.080 Mt        0.000 Mt       11.080 Mt   100.000 % 	 ../ResolveRLInitialization

		     112.415 Mt     0.379 %         1 x      112.415 Mt        0.000 Mt        2.405 Mt     2.140 % 	 ../Initialization

		     110.010 Mt    97.860 %         1 x      110.010 Mt        0.000 Mt        1.104 Mt     1.003 % 	 .../ScenePrep

		      77.603 Mt    70.542 %         1 x       77.603 Mt        0.000 Mt       12.478 Mt    16.080 % 	 ..../SceneLoad

		      57.784 Mt    74.460 %         1 x       57.784 Mt        0.000 Mt       13.008 Mt    22.512 % 	 ...../glTF-LoadScene

		      44.775 Mt    77.488 %         2 x       22.388 Mt        0.000 Mt       44.775 Mt   100.000 % 	 ....../glTF-LoadImageData

		       4.128 Mt     5.319 %         1 x        4.128 Mt        0.000 Mt        4.128 Mt   100.000 % 	 ...../glTF-CreateScene

		       3.214 Mt     4.142 %         1 x        3.214 Mt        0.000 Mt        3.214 Mt   100.000 % 	 ...../SceneDesc-CreateScene

		       1.141 Mt     1.037 %         1 x        1.141 Mt        0.000 Mt        0.005 Mt     0.421 % 	 ..../TexturedRLPrep

		       1.136 Mt    99.579 %         1 x        1.136 Mt        0.000 Mt        0.383 Mt    33.709 % 	 ...../PrepareForRendering

		       0.468 Mt    41.181 %         1 x        0.468 Mt        0.000 Mt        0.468 Mt   100.000 % 	 ....../PrepareMaterials

		       0.285 Mt    25.110 %         1 x        0.285 Mt        0.000 Mt        0.285 Mt   100.000 % 	 ....../PrepareDrawBundle

		       2.285 Mt     2.077 %         1 x        2.285 Mt        0.000 Mt        0.004 Mt     0.179 % 	 ..../DeferredRLPrep

		       2.281 Mt    99.821 %         1 x        2.281 Mt        0.000 Mt        1.584 Mt    69.436 % 	 ...../PrepareForRendering

		       0.015 Mt     0.644 %         1 x        0.015 Mt        0.000 Mt        0.015 Mt   100.000 % 	 ....../PrepareMaterials

		       0.423 Mt    18.557 %         1 x        0.423 Mt        0.000 Mt        0.423 Mt   100.000 % 	 ....../BuildMaterialCache

		       0.259 Mt    11.363 %         1 x        0.259 Mt        0.000 Mt        0.259 Mt   100.000 % 	 ....../PrepareDrawBundle

		      12.633 Mt    11.483 %         1 x       12.633 Mt        0.000 Mt        3.922 Mt    31.045 % 	 ..../RayTracingRLPrep

		       0.221 Mt     1.747 %         1 x        0.221 Mt        0.000 Mt        0.221 Mt   100.000 % 	 ...../PrepareAccelerationStructures

		       0.023 Mt     0.179 %         1 x        0.023 Mt        0.000 Mt        0.023 Mt   100.000 % 	 ...../PrepareCache

		       0.616 Mt     4.877 %         1 x        0.616 Mt        0.000 Mt        0.616 Mt   100.000 % 	 ...../BuildCache

		       5.792 Mt    45.845 %         1 x        5.792 Mt        0.000 Mt        0.019 Mt     0.326 % 	 ...../BuildAccelerationStructures

		       5.773 Mt    99.674 %         1 x        5.773 Mt        0.000 Mt        3.247 Mt    56.248 % 	 ....../AccelerationStructureBuild

		       1.459 Mt    25.280 %         1 x        1.459 Mt        0.000 Mt        1.459 Mt   100.000 % 	 ......./BuildBottomLevelAS

		       1.066 Mt    18.472 %         1 x        1.066 Mt        0.000 Mt        1.066 Mt   100.000 % 	 ......./BuildTopLevelAS

		       2.060 Mt    16.307 %         1 x        2.060 Mt        0.000 Mt        2.060 Mt   100.000 % 	 ...../BuildShaderTables

		      15.243 Mt    13.856 %         1 x       15.243 Mt        0.000 Mt       15.243 Mt   100.000 % 	 ..../GpuSidePrep

		   29150.690 Mt    98.194 %      6570 x        4.437 Mt       10.260 Mt      397.895 Mt     1.365 % 	 ../MainLoop

		    1469.114 Mt     5.040 %      1750 x        0.839 Mt        0.697 Mt     1469.114 Mt   100.000 % 	 .../Update

		   27283.680 Mt    93.595 %      3044 x        8.963 Mt        8.669 Mt    27255.959 Mt    99.898 % 	 .../Render

		      27.721 Mt     0.102 %      3044 x        0.009 Mt        0.009 Mt       27.721 Mt   100.000 % 	 ..../RayTracingRLPrep

	WindowThread:

		   29683.830 Mt   100.000 %         1 x    29683.832 Mt    29683.829 Mt    29683.830 Mt   100.000 % 	 /

	GpuThread:

		   14766.055 Mt   100.000 %      3044 x        4.851 Mt        3.280 Mt      152.906 Mt     1.036 % 	 /

		       0.274 Mt     0.002 %         1 x        0.274 Mt        0.000 Mt        0.006 Mt     2.370 % 	 ./ScenePrep

		       0.268 Mt    97.630 %         1 x        0.268 Mt        0.000 Mt        0.001 Mt     0.478 % 	 ../AccelerationStructureBuild

		       0.181 Mt    67.791 %         1 x        0.181 Mt        0.000 Mt        0.181 Mt   100.000 % 	 .../BuildBottomLevelASGpu

		       0.085 Mt    31.731 %         1 x        0.085 Mt        0.000 Mt        0.085 Mt   100.000 % 	 .../BuildTopLevelASGpu

		   14611.319 Mt    98.952 %      3044 x        4.800 Mt        3.279 Mt        3.082 Mt     0.021 % 	 ./RayTracing

		    6005.459 Mt    41.101 %      3044 x        1.973 Mt        0.526 Mt     6005.459 Mt   100.000 % 	 ../RayTracingRL

		    8602.778 Mt    58.877 %      3044 x        2.826 Mt        2.753 Mt     8602.778 Mt   100.000 % 	 ../ResolveRL

		       1.556 Mt     0.011 %      3044 x        0.001 Mt        0.000 Mt        1.556 Mt   100.000 % 	 ./Present


	============================


