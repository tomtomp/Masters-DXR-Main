Configuration: 
Command line parameters  = E:\programming\FIT-DIP\Masters-DXR-Main-git\build\Measurement\x64\Debug\Measurement.exe --scene Suzanne/Suzanne.gltf --profile-track Suzanne/Suzanne.trk --width 1024 --height 768 --profile-output TrackSuzanne768Ra 
Window width             = 1024
Window height            = 768
Window title             = Direct3D App
Back-buffer count        = 3
Target FPS               = 0
Target UPS               = 60
Tearing                  = enabled
VSync                    = disabled
GUI rendering            = disabled
Debug layer              = enabled
GPU validation           = disabled
GPU profiling            = enabled
Render mode              = Rasterization
Loaded scene file        = Suzanne/Suzanne.gltf
Using testing scene      = disabled
Duplication              = 1
Duplication in cube      = disabled
Duplication offset       = 10
BLAS duplication         = disabled
Rays per pixel           = 9

ProfilingBuildAggregator: 
BuildBottomLevelAS	,	1.3781
BuildBottomLevelASGpu	,	0.613728
BuildTopLevelAS	,	0.8887
BuildTopLevelASGpu	,	0.08432

ProfilingAggregator: 
Render	,	0.6812	,	0.6883	,	2.0839	,	0.6857	,	0.7984	,	0.8926	,	1.3818	,	1.5553	,	2.1539	,	0.8308	,	0.8179	,	0.7905	,	1.5131	,	1.3565	,	1.0466	,	1.5681	,	1.5895	,	0.8214	,	1.0716	,	1.3957	,	2.1151	,	0.8571	,	2.0526	,	0.7805
Update	,	0.2834	,	0.2727	,	0.2888	,	0.2811	,	0.2977	,	0.2755	,	0.8476	,	0.6921	,	0.8273	,	0.3227	,	0.4849	,	0.3098	,	0.7917	,	0.7056	,	0.7805	,	0.2824	,	0.2791	,	0.2664	,	0.2715	,	0.3373	,	0.8849	,	0.354	,	0.2743	,	0.7808

FpsAggregator: 
FPS	,	1202	,	1328	,	1221	,	1073	,	1075	,	839	,	829	,	799	,	838	,	810	,	819	,	887	,	848	,	890	,	819	,	813	,	832	,	780	,	749	,	698	,	682	,	664	,	739	,	771
UPS	,	59	,	61	,	59	,	61	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60
FrameTime	,	0.832498	,	0.753525	,	0.819012	,	0.932095	,	0.930972	,	1.19197	,	1.20647	,	1.25263	,	1.19362	,	1.23543	,	1.22138	,	1.12791	,	1.18006	,	1.12393	,	1.22205	,	1.23182	,	1.20247	,	1.28248	,	1.33628	,	1.4339	,	1.46773	,	1.50662	,	1.35342	,	1.29787
GigaRays/s	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0

AccelerationStructure: 
Ray tracing acceleration structure building statistics: 
	Bottom Level Acceleration Structure: 
		Maximum [B]: 283264
		Minimum [B]: 283264
		Currently Allocated [B]: 283264
		Currently Created [num]: 1
		Current Average [B]: 283264
		Maximum Triangles [num]: 3936
		Minimum Triangles [num]: 3936
		Total Triangles [num]: 3936
		Maximum Meshes [num]: 1
		Minimum Meshes [num]: 1
		Total Meshes [num]: 1
	Top Level Acceleration Structure: 
		Maximum [B]: 64
		Minimum [B]: 64
		Currently Allocated [B]: 64
		Total Allocated [B]: 64
		Total Created [num]: 1
		Average Allocated [B]: 64
		Maximum Instances [num]: 1
		Minimum Instances [num]: 1
		Total Instances [num]: 1
	Scratch Buffer: 
		Maximum [B]: 83968
		Minimum [B]: 83968
		Currently Allocated [B]: 83968
		Total Allocated [B]: 83968
		Total Created [num]: 1
		Average Allocated [B]: 83968
ProfilingStack: 
[PROF] Profiling statistics: 
	Scopes entered : 107911
	Scopes exited : 107910
	Overhead per scope [ticks] : 998.4
	Threads entered : 3
	Threads exited : 0
	============================
		         Sum[Mt]   Parent[%]  Samples[num]       Avg[Mt]   Last/Curr[Mt]        Self[Mt]     Self[%] 	 Name

	main:

		   24693.712 Mt   100.000 %         1 x    24693.713 Mt    24693.711 Mt        1.082 Mt     0.004 % 	 /

		   24692.674 Mt    99.996 %         1 x    24692.675 Mt    24692.673 Mt      449.889 Mt     1.822 % 	 ./Main application loop

		       3.189 Mt     0.013 %         1 x        3.189 Mt        0.000 Mt        3.189 Mt   100.000 % 	 ../TexturedRLInitialization

		       6.670 Mt     0.027 %         1 x        6.670 Mt        0.000 Mt        6.670 Mt   100.000 % 	 ../DeferredRLInitialization

		      50.762 Mt     0.206 %         1 x       50.762 Mt        0.000 Mt        2.168 Mt     4.272 % 	 ../RayTracingRLInitialization

		      48.593 Mt    95.728 %         1 x       48.593 Mt        0.000 Mt       48.593 Mt   100.000 % 	 .../PipelineBuild

		      10.915 Mt     0.044 %         1 x       10.915 Mt        0.000 Mt       10.915 Mt   100.000 % 	 ../ResolveRLInitialization

		     239.350 Mt     0.969 %         1 x      239.350 Mt        0.000 Mt        2.766 Mt     1.156 % 	 ../Initialization

		     236.584 Mt    98.844 %         1 x      236.584 Mt        0.000 Mt        1.246 Mt     0.527 % 	 .../ScenePrep

		     203.035 Mt    85.819 %         1 x      203.035 Mt        0.000 Mt       18.120 Mt     8.925 % 	 ..../SceneLoad

		     164.671 Mt    81.105 %         1 x      164.671 Mt        0.000 Mt       20.156 Mt    12.240 % 	 ...../glTF-LoadScene

		     144.515 Mt    87.760 %         2 x       72.258 Mt        0.000 Mt      144.515 Mt   100.000 % 	 ....../glTF-LoadImageData

		      13.731 Mt     6.763 %         1 x       13.731 Mt        0.000 Mt       13.731 Mt   100.000 % 	 ...../glTF-CreateScene

		       6.513 Mt     3.208 %         1 x        6.513 Mt        0.000 Mt        6.513 Mt   100.000 % 	 ...../SceneDesc-CreateScene

		       1.143 Mt     0.483 %         1 x        1.143 Mt        0.000 Mt        0.005 Mt     0.420 % 	 ..../TexturedRLPrep

		       1.139 Mt    99.580 %         1 x        1.139 Mt        0.000 Mt        0.390 Mt    34.226 % 	 ...../PrepareForRendering

		       0.462 Mt    40.602 %         1 x        0.462 Mt        0.000 Mt        0.462 Mt   100.000 % 	 ....../PrepareMaterials

		       0.287 Mt    25.171 %         1 x        0.287 Mt        0.000 Mt        0.287 Mt   100.000 % 	 ....../PrepareDrawBundle

		       4.463 Mt     1.886 %         1 x        4.463 Mt        0.000 Mt        0.004 Mt     0.096 % 	 ..../DeferredRLPrep

		       4.458 Mt    99.904 %         1 x        4.458 Mt        0.000 Mt        3.771 Mt    84.597 % 	 ...../PrepareForRendering

		       0.015 Mt     0.343 %         1 x        0.015 Mt        0.000 Mt        0.015 Mt   100.000 % 	 ....../PrepareMaterials

		       0.408 Mt     9.154 %         1 x        0.408 Mt        0.000 Mt        0.408 Mt   100.000 % 	 ....../BuildMaterialCache

		       0.263 Mt     5.906 %         1 x        0.263 Mt        0.000 Mt        0.263 Mt   100.000 % 	 ....../PrepareDrawBundle

		      14.339 Mt     6.061 %         1 x       14.339 Mt        0.000 Mt        3.716 Mt    25.912 % 	 ..../RayTracingRLPrep

		       0.252 Mt     1.759 %         1 x        0.252 Mt        0.000 Mt        0.252 Mt   100.000 % 	 ...../PrepareAccelerationStructures

		       0.024 Mt     0.165 %         1 x        0.024 Mt        0.000 Mt        0.024 Mt   100.000 % 	 ...../PrepareCache

		       0.442 Mt     3.083 %         1 x        0.442 Mt        0.000 Mt        0.442 Mt   100.000 % 	 ...../BuildCache

		       5.113 Mt    35.657 %         1 x        5.113 Mt        0.000 Mt        0.006 Mt     0.121 % 	 ...../BuildAccelerationStructures

		       5.107 Mt    99.879 %         1 x        5.107 Mt        0.000 Mt        2.840 Mt    55.612 % 	 ....../AccelerationStructureBuild

		       1.378 Mt    26.986 %         1 x        1.378 Mt        0.000 Mt        1.378 Mt   100.000 % 	 ......./BuildBottomLevelAS

		       0.889 Mt    17.402 %         1 x        0.889 Mt        0.000 Mt        0.889 Mt   100.000 % 	 ......./BuildTopLevelAS

		       4.793 Mt    33.423 %         1 x        4.793 Mt        0.000 Mt        4.793 Mt   100.000 % 	 ...../BuildShaderTables

		      12.358 Mt     5.224 %         1 x       12.358 Mt        0.000 Mt       12.358 Mt   100.000 % 	 ..../GpuSidePrep

		   23931.898 Mt    96.919 %     22405 x        1.068 Mt        3.157 Mt      325.775 Mt     1.361 % 	 ../MainLoop

		     761.761 Mt     3.183 %      1441 x        0.529 Mt        0.827 Mt      761.761 Mt   100.000 % 	 .../Update

		   22844.363 Mt    95.456 %     21007 x        1.087 Mt        2.310 Mt    22746.622 Mt    99.572 % 	 .../Render

		      97.740 Mt     0.428 %     21007 x        0.005 Mt        0.014 Mt       97.740 Mt   100.000 % 	 ..../TexturedRLPrep

	WindowThread:

		   24680.320 Mt   100.000 %         1 x    24680.321 Mt    24680.320 Mt    24680.320 Mt   100.000 % 	 /

	GpuThread:

		    1374.101 Mt   100.000 %     21007 x        0.065 Mt        0.050 Mt      152.274 Mt    11.082 % 	 /

		       0.706 Mt     0.051 %         1 x        0.706 Mt        0.000 Mt        0.006 Mt     0.912 % 	 ./ScenePrep

		       0.699 Mt    99.088 %         1 x        0.699 Mt        0.000 Mt        0.001 Mt     0.156 % 	 ../AccelerationStructureBuild

		       0.614 Mt    87.784 %         1 x        0.614 Mt        0.000 Mt        0.614 Mt   100.000 % 	 .../BuildBottomLevelASGpu

		       0.084 Mt    12.061 %         1 x        0.084 Mt        0.000 Mt        0.084 Mt   100.000 % 	 .../BuildTopLevelASGpu

		    1012.416 Mt    73.678 %     21007 x        0.048 Mt        0.045 Mt     1012.416 Mt   100.000 % 	 ./Textured

		     208.705 Mt    15.188 %     21007 x        0.010 Mt        0.004 Mt      208.705 Mt   100.000 % 	 ./Present


	============================


