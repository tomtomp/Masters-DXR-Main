Configuration: 
Command line parameters  = E:\programming\FIT-DIP\Masters-DXR-Main-git\build\Measurement\x64\Debug\Measurement.exe --scene Sponza/Sponza.gltf --profile-track Sponza/Sponza.trk --rt-mode-pure --width 1024 --height 768 --profile-output TrackSponza768Rp 
Window width             = 1024
Window height            = 768
Window title             = Direct3D App
Back-buffer count        = 3
Target FPS               = 0
Target UPS               = 60
Tearing                  = enabled
VSync                    = disabled
GUI rendering            = disabled
Debug layer              = enabled
GPU validation           = disabled
GPU profiling            = enabled
Render mode              = Ray Tracing
Loaded scene file        = Sponza/Sponza.gltf
Using testing scene      = disabled
Duplication              = 1
Duplication in cube      = disabled
Duplication offset       = 10
BLAS duplication         = disabled
Rays per pixel           = 10

ProfilingBuildAggregator: 
BuildBottomLevelAS	,	5.7829
BuildBottomLevelASGpu	,	25.4311
BuildTopLevelAS	,	2.1263
BuildTopLevelASGpu	,	0.656352

ProfilingAggregator: 
Render	,	3.9751	,	3.8041	,	5.5407	,	6.0643	,	7.9857	,	4.6494	,	6.1511	,	4.0394	,	3.6855	,	5.6206	,	4.2914	,	4.6125	,	5.4864	,	5.9833	,	3.675	,	3.846	,	5.9394	,	4.0819	,	5.9883	,	3.7162	,	5.8804	,	4.7821	,	6.3189	,	3.6827	,	5.4754	,	3.9529
Update	,	0.3906	,	0.4592	,	0.5474	,	0.6181	,	0.5564	,	0.3313	,	0.4424	,	0.4809	,	0.2934	,	0.6724	,	0.7323	,	0.5645	,	0.5761	,	0.6551	,	0.5545	,	0.4477	,	0.5974	,	0.5858	,	0.3997	,	0.474	,	0.8288	,	0.6873	,	0.4322	,	0.3702	,	0.5895	,	0.7398
RayTracing	,	1.94576	,	1.82461	,	1.84563	,	1.86314	,	1.81654	,	1.79274	,	1.94842	,	1.6105	,	1.69174	,	1.74058	,	1.72003	,	1.80048	,	1.80333	,	1.82614	,	1.72685	,	1.74906	,	1.7615	,	1.68214	,	1.75347	,	1.75322	,	1.80659	,	2.06902	,	1.80688	,	1.76141	,	1.76413	,	1.82381
RayTracingRL	,	1.2857	,	1.21837	,	1.24307	,	1.26163	,	1.19648	,	1.17232	,	1.34624	,	1.0057	,	1.0903	,	1.13875	,	1.11917	,	1.1984	,	1.20234	,	1.20797	,	1.12483	,	1.14669	,	1.15555	,	1.07674	,	1.1304	,	1.13043	,	1.18154	,	1.44339	,	1.20534	,	1.15933	,	1.16272	,	1.22224

FpsAggregator: 
FPS	,	132	,	194	,	199	,	200	,	203	,	194	,	205	,	207	,	229	,	213	,	210	,	208	,	200	,	196	,	211	,	198	,	200	,	202	,	208	,	206	,	197	,	202	,	202	,	202	,	201	,	203
UPS	,	59	,	61	,	60	,	60	,	60	,	60	,	60	,	61	,	60	,	60	,	60	,	60	,	61	,	60	,	60	,	60	,	60	,	60	,	60	,	61	,	60	,	60	,	60	,	61	,	60	,	60
FrameTime	,	7.60511	,	5.1766	,	5.03371	,	5.00576	,	4.96506	,	5.15755	,	4.89028	,	4.84682	,	4.37365	,	4.70778	,	4.76961	,	4.82764	,	5.02773	,	5.11958	,	4.74271	,	5.05067	,	5.00776	,	4.95064	,	4.83656	,	4.86721	,	5.10652	,	4.96767	,	4.96521	,	4.97021	,	4.99366	,	4.93561
GigaRays/s	,	1.03408	,	1.51921	,	1.56233	,	1.57105	,	1.58393	,	1.52482	,	1.60815	,	1.62257	,	1.79812	,	1.67049	,	1.64884	,	1.62902	,	1.56419	,	1.53613	,	1.65819	,	1.55708	,	1.57043	,	1.58854	,	1.62601	,	1.61578	,	1.54005	,	1.5831	,	1.58389	,	1.58229	,	1.57486	,	1.59338

AccelerationStructure: 
Ray tracing acceleration structure building statistics: 
	Bottom Level Acceleration Structure: 
		Maximum [B]: 18391808
		Minimum [B]: 18391808
		Currently Allocated [B]: 18391808
		Currently Created [num]: 1
		Current Average [B]: 18391808
		Maximum Triangles [num]: 262267
		Minimum Triangles [num]: 262267
		Total Triangles [num]: 262267
		Maximum Meshes [num]: 103
		Minimum Meshes [num]: 103
		Total Meshes [num]: 103
	Top Level Acceleration Structure: 
		Maximum [B]: 64
		Minimum [B]: 64
		Currently Allocated [B]: 64
		Total Allocated [B]: 64
		Total Created [num]: 1
		Average Allocated [B]: 64
		Maximum Instances [num]: 1
		Minimum Instances [num]: 1
		Total Instances [num]: 1
	Scratch Buffer: 
		Maximum [B]: 14998528
		Minimum [B]: 14998528
		Currently Allocated [B]: 14998528
		Total Allocated [B]: 14998528
		Total Created [num]: 1
		Average Allocated [B]: 14998528
ProfilingStack: 
[PROF] Profiling statistics: 
	Scopes entered : 40738
	Scopes exited : 40737
	Overhead per scope [ticks] : 1010
	Threads entered : 3
	Threads exited : 0
	============================
		         Sum[Mt]   Parent[%]  Samples[num]       Avg[Mt]   Last/Curr[Mt]        Self[Mt]     Self[%] 	 Name

	main:

		   35796.284 Mt   100.000 %         1 x    35796.285 Mt    35796.283 Mt        0.698 Mt     0.002 % 	 /

		   35795.631 Mt    99.998 %         1 x    35795.633 Mt    35795.630 Mt      375.105 Mt     1.048 % 	 ./Main application loop

		       3.566 Mt     0.010 %         1 x        3.566 Mt        0.000 Mt        3.566 Mt   100.000 % 	 ../TexturedRLInitialization

		      10.035 Mt     0.028 %         1 x       10.035 Mt        0.000 Mt       10.035 Mt   100.000 % 	 ../DeferredRLInitialization

		      49.440 Mt     0.138 %         1 x       49.440 Mt        0.000 Mt        2.058 Mt     4.162 % 	 ../RayTracingRLInitialization

		      47.382 Mt    95.838 %         1 x       47.382 Mt        0.000 Mt       47.382 Mt   100.000 % 	 .../PipelineBuild

		      10.741 Mt     0.030 %         1 x       10.741 Mt        0.000 Mt       10.741 Mt   100.000 % 	 ../ResolveRLInitialization

		    9287.969 Mt    25.947 %         1 x     9287.969 Mt        0.000 Mt        8.334 Mt     0.090 % 	 ../Initialization

		    9279.635 Mt    99.910 %         1 x     9279.635 Mt        0.000 Mt        3.209 Mt     0.035 % 	 .../ScenePrep

		    9153.262 Mt    98.638 %         1 x     9153.262 Mt        0.000 Mt      383.629 Mt     4.191 % 	 ..../SceneLoad

		    8230.417 Mt    89.918 %         1 x     8230.417 Mt        0.000 Mt     1079.879 Mt    13.121 % 	 ...../glTF-LoadScene

		    7150.538 Mt    86.879 %        69 x      103.631 Mt        0.000 Mt     7150.538 Mt   100.000 % 	 ....../glTF-LoadImageData

		     326.824 Mt     3.571 %         1 x      326.824 Mt        0.000 Mt      326.824 Mt   100.000 % 	 ...../glTF-CreateScene

		     212.392 Mt     2.320 %         1 x      212.392 Mt        0.000 Mt      212.392 Mt   100.000 % 	 ...../SceneDesc-CreateScene

		       7.087 Mt     0.076 %         1 x        7.087 Mt        0.000 Mt        0.005 Mt     0.072 % 	 ..../TexturedRLPrep

		       7.081 Mt    99.928 %         1 x        7.081 Mt        0.000 Mt        2.170 Mt    30.641 % 	 ...../PrepareForRendering

		       2.562 Mt    36.185 %         1 x        2.562 Mt        0.000 Mt        2.562 Mt   100.000 % 	 ....../PrepareMaterials

		       2.349 Mt    33.174 %         1 x        2.349 Mt        0.000 Mt        2.349 Mt   100.000 % 	 ....../PrepareDrawBundle

		       8.951 Mt     0.096 %         1 x        8.951 Mt        0.000 Mt        0.005 Mt     0.057 % 	 ..../DeferredRLPrep

		       8.946 Mt    99.943 %         1 x        8.946 Mt        0.000 Mt        4.338 Mt    48.494 % 	 ...../PrepareForRendering

		       0.022 Mt     0.244 %         1 x        0.022 Mt        0.000 Mt        0.022 Mt   100.000 % 	 ....../PrepareMaterials

		       2.168 Mt    24.229 %         1 x        2.168 Mt        0.000 Mt        2.168 Mt   100.000 % 	 ....../BuildMaterialCache

		       2.418 Mt    27.033 %         1 x        2.418 Mt        0.000 Mt        2.418 Mt   100.000 % 	 ....../PrepareDrawBundle

		      30.679 Mt     0.331 %         1 x       30.679 Mt        0.000 Mt        7.044 Mt    22.961 % 	 ..../RayTracingRLPrep

		       0.583 Mt     1.901 %         1 x        0.583 Mt        0.000 Mt        0.583 Mt   100.000 % 	 ...../PrepareAccelerationStructures

		       0.024 Mt     0.079 %         1 x        0.024 Mt        0.000 Mt        0.024 Mt   100.000 % 	 ...../PrepareCache

		       4.694 Mt    15.302 %         1 x        4.694 Mt        0.000 Mt        4.694 Mt   100.000 % 	 ...../BuildCache

		      13.227 Mt    43.114 %         1 x       13.227 Mt        0.000 Mt        0.007 Mt     0.051 % 	 ...../BuildAccelerationStructures

		      13.220 Mt    99.949 %         1 x       13.220 Mt        0.000 Mt        5.311 Mt    40.172 % 	 ....../AccelerationStructureBuild

		       5.783 Mt    43.744 %         1 x        5.783 Mt        0.000 Mt        5.783 Mt   100.000 % 	 ......./BuildBottomLevelAS

		       2.126 Mt    16.084 %         1 x        2.126 Mt        0.000 Mt        2.126 Mt   100.000 % 	 ......./BuildTopLevelAS

		       5.106 Mt    16.644 %         1 x        5.106 Mt        0.000 Mt        5.106 Mt   100.000 % 	 ...../BuildShaderTables

		      76.447 Mt     0.824 %         1 x       76.447 Mt        0.000 Mt       76.447 Mt   100.000 % 	 ..../GpuSidePrep

		   26058.775 Mt    72.799 %      7725 x        3.373 Mt        9.330 Mt      295.559 Mt     1.134 % 	 ../MainLoop

		     979.288 Mt     3.758 %      1565 x        0.626 Mt        0.889 Mt      979.288 Mt   100.000 % 	 .../Update

		   24783.929 Mt    95.108 %      5224 x        4.744 Mt        8.420 Mt    24753.218 Mt    99.876 % 	 .../Render

		      30.711 Mt     0.124 %      5224 x        0.006 Mt        0.009 Mt       30.711 Mt   100.000 % 	 ..../RayTracingRLPrep

	WindowThread:

		   35792.014 Mt   100.000 %         1 x    35792.015 Mt    35792.014 Mt    35792.014 Mt   100.000 % 	 /

	GpuThread:

		    9804.437 Mt   100.000 %      5224 x        1.877 Mt        1.842 Mt      221.857 Mt     2.263 % 	 /

		      26.142 Mt     0.267 %         1 x       26.142 Mt        0.000 Mt        0.051 Mt     0.194 % 	 ./ScenePrep

		      26.091 Mt    99.806 %         1 x       26.091 Mt        0.000 Mt        0.004 Mt     0.013 % 	 ../AccelerationStructureBuild

		      25.431 Mt    97.471 %         1 x       25.431 Mt        0.000 Mt       25.431 Mt   100.000 % 	 .../BuildBottomLevelASGpu

		       0.656 Mt     2.516 %         1 x        0.656 Mt        0.000 Mt        0.656 Mt   100.000 % 	 .../BuildTopLevelASGpu

		    9553.951 Mt    97.445 %      5224 x        1.829 Mt        1.841 Mt        5.349 Mt     0.056 % 	 ./RayTracing

		    6330.053 Mt    66.256 %      5224 x        1.212 Mt        1.236 Mt     6330.053 Mt   100.000 % 	 ../RayTracingRL

		    3218.549 Mt    33.688 %      5224 x        0.616 Mt        0.603 Mt     3218.549 Mt   100.000 % 	 ../ResolveRL

		       2.487 Mt     0.025 %      5224 x        0.000 Mt        0.000 Mt        2.487 Mt   100.000 % 	 ./Present


	============================


