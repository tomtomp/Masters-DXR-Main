Configuration: 
Command line parameters  = E:\programming\FIT-DIP\Masters-DXR-Main-git\build\Measurement\x64\Debug\Measurement.exe --scene Suzanne/Suzanne.gltf --profile-track Suzanne/Suzanne.trk --rt-mode --width 1024 --height 768 --profile-output TrackSuzanne768Rt 
Window width             = 1024
Window height            = 768
Window title             = Direct3D App
Back-buffer count        = 3
Target FPS               = 0
Target UPS               = 60
Tearing                  = enabled
VSync                    = disabled
GUI rendering            = disabled
Debug layer              = enabled
GPU validation           = disabled
GPU profiling            = enabled
Render mode              = Ray Tracing
Loaded scene file        = Suzanne/Suzanne.gltf
Using testing scene      = disabled
Duplication              = 1
Duplication in cube      = disabled
Duplication offset       = 10
BLAS duplication         = disabled
Rays per pixel           = 9

ProfilingBuildAggregator: 
BuildBottomLevelAS	,	3.4712
BuildBottomLevelASGpu	,	0.43264
BuildTopLevelAS	,	1.3121
BuildTopLevelASGpu	,	0.0648

ProfilingAggregator: 
Render	,	3.2026	,	5.3335	,	4.7917	,	3.8068	,	8.8202	,	5.7705	,	4.8699	,	9.1149	,	4.42	,	7.9031	,	4.8792	,	3.8047	,	5.4058	,	3.997	,	5.5483	,	3.4682	,	4.07	,	9.3032	,	4.4983	,	4.0378	,	6.634	,	5.3163	,	4.1035	,	4.4744
Update	,	0.2973	,	0.6292	,	0.3845	,	0.289	,	0.2679	,	0.5615	,	0.7557	,	0.5387	,	0.8115	,	0.8297	,	0.2614	,	0.5639	,	0.5375	,	0.3526	,	0.3242	,	0.8121	,	0.3718	,	0.8431	,	0.3961	,	0.383	,	0.5024	,	0.7607	,	0.5188	,	0.8023
RayTracing	,	1.08378	,	1.21005	,	1.57123	,	1.58166	,	1.81888	,	1.71421	,	2.65194	,	1.70589	,	1.77098	,	1.01117	,	1.05331	,	1.2799	,	1.29798	,	1.75366	,	1.14397	,	1.29379	,	1.65856	,	1.96874	,	2.12106	,	1.78874	,	1.55562	,	1.35667	,	1.33386	,	1.4071
RayTracingRL	,	0.239008	,	0.348608	,	0.507904	,	0.446336	,	0.292544	,	0.187008	,	0.57232	,	0.408512	,	0.500128	,	0.194816	,	0.23312	,	0.21968	,	0.155392	,	0.465408	,	0.196352	,	0.321824	,	0.450688	,	0.716576	,	0.95168	,	0.729792	,	0.5112	,	0.286496	,	0.200896	,	0.188544

FpsAggregator: 
FPS	,	257	,	218	,	204	,	180	,	201	,	208	,	201	,	173	,	175	,	258	,	232	,	219	,	222	,	211	,	229	,	212	,	189	,	184	,	170	,	165	,	197	,	200	,	205	,	198
UPS	,	59	,	60	,	61	,	60	,	60	,	60	,	60	,	60	,	61	,	60	,	60	,	61	,	60	,	60	,	60	,	60	,	60	,	61	,	60	,	60	,	60	,	60	,	60	,	60
FrameTime	,	3.89222	,	4.59409	,	4.91857	,	5.57064	,	5.0128	,	4.80943	,	4.98173	,	5.79979	,	5.72514	,	3.90389	,	4.33094	,	4.58312	,	4.53094	,	4.75358	,	4.37938	,	4.71976	,	5.30058	,	5.4858	,	5.88376	,	6.0734	,	5.0781	,	5.00728	,	4.88383	,	5.06799
GigaRays/s	,	1.81847	,	1.54065	,	1.43901	,	1.27057	,	1.41196	,	1.47167	,	1.42077	,	1.22037	,	1.23628	,	1.81304	,	1.63426	,	1.54434	,	1.56212	,	1.48896	,	1.61619	,	1.49963	,	1.3353	,	1.29022	,	1.20295	,	1.16539	,	1.39381	,	1.41352	,	1.44925	,	1.39659

AccelerationStructure: 
Ray tracing acceleration structure building statistics: 
	Bottom Level Acceleration Structure: 
		Maximum [B]: 283264
		Minimum [B]: 283264
		Currently Allocated [B]: 283264
		Currently Created [num]: 1
		Current Average [B]: 283264
		Maximum Triangles [num]: 3936
		Minimum Triangles [num]: 3936
		Total Triangles [num]: 3936
		Maximum Meshes [num]: 1
		Minimum Meshes [num]: 1
		Total Meshes [num]: 1
	Top Level Acceleration Structure: 
		Maximum [B]: 64
		Minimum [B]: 64
		Currently Allocated [B]: 64
		Total Allocated [B]: 64
		Total Created [num]: 1
		Average Allocated [B]: 64
		Maximum Instances [num]: 1
		Minimum Instances [num]: 1
		Total Instances [num]: 1
	Scratch Buffer: 
		Maximum [B]: 83968
		Minimum [B]: 83968
		Currently Allocated [B]: 83968
		Total Allocated [B]: 83968
		Total Created [num]: 1
		Average Allocated [B]: 83968
ProfilingStack: 
[PROF] Profiling statistics: 
	Scopes entered : 50202
	Scopes exited : 50201
	Overhead per scope [ticks] : 1018.9
	Threads entered : 3
	Threads exited : 0
	============================
		         Sum[Mt]   Parent[%]  Samples[num]       Avg[Mt]   Last/Curr[Mt]        Self[Mt]     Self[%] 	 Name

	main:

		   24850.833 Mt   100.000 %         1 x    24850.836 Mt    24850.832 Mt        1.080 Mt     0.004 % 	 /

		   24849.886 Mt    99.996 %         1 x    24849.891 Mt    24849.886 Mt      387.496 Mt     1.559 % 	 ./Main application loop

		       2.881 Mt     0.012 %         1 x        2.881 Mt        0.000 Mt        2.881 Mt   100.000 % 	 ../TexturedRLInitialization

		       5.628 Mt     0.023 %         1 x        5.628 Mt        0.000 Mt        5.628 Mt   100.000 % 	 ../DeferredRLInitialization

		      58.521 Mt     0.235 %         1 x       58.521 Mt        0.000 Mt        1.973 Mt     3.371 % 	 ../RayTracingRLInitialization

		      56.548 Mt    96.629 %         1 x       56.548 Mt        0.000 Mt       56.548 Mt   100.000 % 	 .../PipelineBuild

		      26.938 Mt     0.108 %         1 x       26.938 Mt        0.000 Mt       26.938 Mt   100.000 % 	 ../ResolveRLInitialization

		     293.578 Mt     1.181 %         1 x      293.578 Mt        0.000 Mt        2.813 Mt     0.958 % 	 ../Initialization

		     290.765 Mt    99.042 %         1 x      290.765 Mt        0.000 Mt        2.233 Mt     0.768 % 	 .../ScenePrep

		     254.662 Mt    87.584 %         1 x      254.662 Mt        0.000 Mt       24.617 Mt     9.666 % 	 ..../SceneLoad

		     191.856 Mt    75.338 %         1 x      191.856 Mt        0.000 Mt       42.892 Mt    22.356 % 	 ...../glTF-LoadScene

		     148.965 Mt    77.644 %         2 x       74.482 Mt        0.000 Mt      148.965 Mt   100.000 % 	 ....../glTF-LoadImageData

		      23.157 Mt     9.093 %         1 x       23.157 Mt        0.000 Mt       23.157 Mt   100.000 % 	 ...../glTF-CreateScene

		      15.032 Mt     5.903 %         1 x       15.032 Mt        0.000 Mt       15.032 Mt   100.000 % 	 ...../SceneDesc-CreateScene

		       2.486 Mt     0.855 %         1 x        2.486 Mt        0.000 Mt        0.009 Mt     0.354 % 	 ..../TexturedRLPrep

		       2.477 Mt    99.646 %         1 x        2.477 Mt        0.000 Mt        0.861 Mt    34.742 % 	 ...../PrepareForRendering

		       1.097 Mt    44.268 %         1 x        1.097 Mt        0.000 Mt        1.097 Mt   100.000 % 	 ....../PrepareMaterials

		       0.520 Mt    20.990 %         1 x        0.520 Mt        0.000 Mt        0.520 Mt   100.000 % 	 ....../PrepareDrawBundle

		       3.868 Mt     1.330 %         1 x        3.868 Mt        0.000 Mt        0.008 Mt     0.204 % 	 ..../DeferredRLPrep

		       3.860 Mt    99.796 %         1 x        3.860 Mt        0.000 Mt        2.461 Mt    63.746 % 	 ...../PrepareForRendering

		       0.028 Mt     0.736 %         1 x        0.028 Mt        0.000 Mt        0.028 Mt   100.000 % 	 ....../PrepareMaterials

		       0.992 Mt    25.699 %         1 x        0.992 Mt        0.000 Mt        0.992 Mt   100.000 % 	 ....../BuildMaterialCache

		       0.379 Mt     9.819 %         1 x        0.379 Mt        0.000 Mt        0.379 Mt   100.000 % 	 ....../PrepareDrawBundle

		      17.127 Mt     5.890 %         1 x       17.127 Mt        0.000 Mt        4.906 Mt    28.645 % 	 ..../RayTracingRLPrep

		       0.348 Mt     2.033 %         1 x        0.348 Mt        0.000 Mt        0.348 Mt   100.000 % 	 ...../PrepareAccelerationStructures

		       0.034 Mt     0.197 %         1 x        0.034 Mt        0.000 Mt        0.034 Mt   100.000 % 	 ...../PrepareCache

		       0.833 Mt     4.861 %         1 x        0.833 Mt        0.000 Mt        0.833 Mt   100.000 % 	 ...../BuildCache

		       8.721 Mt    50.920 %         1 x        8.721 Mt        0.000 Mt        0.009 Mt     0.101 % 	 ...../BuildAccelerationStructures

		       8.712 Mt    99.899 %         1 x        8.712 Mt        0.000 Mt        3.929 Mt    45.097 % 	 ....../AccelerationStructureBuild

		       3.471 Mt    39.843 %         1 x        3.471 Mt        0.000 Mt        3.471 Mt   100.000 % 	 ......./BuildBottomLevelAS

		       1.312 Mt    15.060 %         1 x        1.312 Mt        0.000 Mt        1.312 Mt   100.000 % 	 ......./BuildTopLevelAS

		       2.285 Mt    13.344 %         1 x        2.285 Mt        0.000 Mt        2.285 Mt   100.000 % 	 ...../BuildShaderTables

		      10.389 Mt     3.573 %         1 x       10.389 Mt        0.000 Mt       10.389 Mt   100.000 % 	 ..../GpuSidePrep

		   24074.844 Mt    96.881 %      9449 x        2.548 Mt       46.232 Mt      862.712 Mt     3.583 % 	 ../MainLoop

		     807.228 Mt     3.353 %      1444 x        0.559 Mt        0.434 Mt      807.228 Mt   100.000 % 	 .../Update

		   22404.905 Mt    93.064 %      4909 x        4.564 Mt        5.867 Mt    22361.597 Mt    99.807 % 	 .../Render

		      25.804 Mt     0.115 %      4909 x        0.005 Mt        0.005 Mt       25.804 Mt   100.000 % 	 ..../DeferredRLPrep

		      17.504 Mt     0.078 %      4909 x        0.004 Mt        0.005 Mt       17.504 Mt   100.000 % 	 ..../RayTracingRLPrep

	WindowThread:

		   24832.892 Mt   100.000 %         1 x    24832.893 Mt    24832.890 Mt    24832.892 Mt   100.000 % 	 /

	GpuThread:

		    7285.822 Mt   100.000 %      4909 x        1.484 Mt        1.421 Mt      110.885 Mt     1.522 % 	 /

		       0.504 Mt     0.007 %         1 x        0.504 Mt        0.000 Mt        0.006 Mt     1.161 % 	 ./ScenePrep

		       0.499 Mt    98.839 %         1 x        0.499 Mt        0.000 Mt        0.001 Mt     0.237 % 	 ../AccelerationStructureBuild

		       0.433 Mt    86.767 %         1 x        0.433 Mt        0.000 Mt        0.433 Mt   100.000 % 	 .../BuildBottomLevelASGpu

		       0.065 Mt    12.996 %         1 x        0.065 Mt        0.000 Mt        0.065 Mt   100.000 % 	 .../BuildTopLevelASGpu

		    7170.238 Mt    98.414 %      4909 x        1.461 Mt        1.419 Mt       12.119 Mt     0.169 % 	 ./RayTracing

		     272.193 Mt     3.796 %      4909 x        0.055 Mt        0.032 Mt      272.193 Mt   100.000 % 	 ../DeferredRL

		    1745.399 Mt    24.342 %      4909 x        0.356 Mt        0.197 Mt     1745.399 Mt   100.000 % 	 ../RayTracingRL

		    5140.526 Mt    71.693 %      4909 x        1.047 Mt        1.186 Mt     5140.526 Mt   100.000 % 	 ../ResolveRL

		       4.194 Mt     0.058 %      4909 x        0.001 Mt        0.001 Mt        4.194 Mt   100.000 % 	 ./Present


	============================


