Configuration: 
Command line parameters  = E:\programming\FIT-DIP\Masters-DXR-Main-git\build\Measurement\x64\Debug\Measurement.exe --scene Cube/Cube.gltf --profile-track Cube/Cube.trk --rt-mode-pure --width 2560 --height 1440 --profile-output TrackCube1440Rp 
Window width             = 2560
Window height            = 1421
Window title             = Direct3D App
Back-buffer count        = 3
Target FPS               = 0
Target UPS               = 60
Tearing                  = enabled
VSync                    = disabled
GUI rendering            = disabled
Debug layer              = enabled
GPU validation           = disabled
GPU profiling            = enabled
Render mode              = Ray Tracing
Loaded scene file        = Cube/Cube.gltf
Using testing scene      = disabled
Duplication              = 1
Duplication in cube      = disabled
Duplication offset       = 10
BLAS duplication         = disabled
Rays per pixel           = 10

ProfilingBuildAggregator: 
BuildBottomLevelAS	,	2.2223
BuildBottomLevelASGpu	,	0.15632
BuildTopLevelAS	,	1.8396
BuildTopLevelASGpu	,	0.074528

ProfilingAggregator: 
Render	,	5.5556	,	6.3544	,	9.0729	,	8.1968	,	8.35	,	8.0337	,	10.1385	,	10.6859	,	10.9065	,	7.1901	,	10.8123	,	8.193	,	8.5588	,	7.3234	,	9.6396	,	12.2442	,	10.5788	,	8.293	,	8.4399	,	9.8442	,	9.8897	,	9.5182	,	9.3992	,	9.2445	,	5.5618	,	5.501	,	5.3294	,	8.2681	,	5.1543
Update	,	0.5548	,	0.3032	,	0.8619	,	0.3694	,	0.5828	,	0.5079	,	0.8476	,	0.8798	,	0.8799	,	0.3586	,	0.8508	,	0.467	,	0.7193	,	0.4806	,	0.2978	,	0.5977	,	0.8682	,	0.3071	,	0.3243	,	0.5307	,	0.7095	,	0.5876	,	1.2646	,	0.8824	,	0.872	,	0.6337	,	0.2954	,	0.8536	,	0.2974
RayTracing	,	3.84874	,	4.34784	,	4.24672	,	4.34509	,	4.69981	,	5.22611	,	5.15882	,	5.70576	,	5.91837	,	5.60787	,	5.13914	,	5.52432	,	4.79488	,	4.8225	,	4.89005	,	5.23686	,	5.65376	,	6.66592	,	6.80922	,	6.73453	,	5.73565	,	4.82976	,	4.71818	,	4.07299	,	3.85853	,	3.83206	,	3.72512	,	3.54378	,	3.26195
RayTracingRL	,	1.09744	,	1.41286	,	1.4961	,	1.59693	,	1.95878	,	2.39264	,	2.40835	,	2.72467	,	2.89018	,	2.57533	,	2.39507	,	2.56374	,	2.04854	,	2.07856	,	2.1393	,	2.49072	,	2.91402	,	3.74048	,	3.848	,	3.7793	,	2.98058	,	2.08547	,	1.65936	,	1.31875	,	1.1145	,	0.87536	,	0.690592	,	0.602432	,	0.519328

FpsAggregator: 
FPS	,	114	,	127	,	127	,	125	,	123	,	106	,	109	,	113	,	102	,	102	,	116	,	115	,	108	,	118	,	116	,	112	,	108	,	97	,	94	,	94	,	97	,	110	,	117	,	123	,	133	,	135	,	142	,	144	,	150
UPS	,	59	,	60	,	61	,	60	,	60	,	61	,	60	,	60	,	61	,	60	,	60	,	60	,	61	,	60	,	60	,	60	,	61	,	60	,	60	,	61	,	60	,	60	,	60	,	61	,	60	,	60	,	61	,	60	,	60
FrameTime	,	8.77492	,	7.91244	,	7.94497	,	8.02093	,	8.18404	,	9.45972	,	9.22354	,	8.89296	,	9.89118	,	9.80699	,	8.65759	,	8.73268	,	9.32157	,	8.49543	,	8.70139	,	8.98006	,	9.27188	,	10.3845	,	10.6492	,	10.7109	,	10.3265	,	9.14515	,	8.56586	,	8.21222	,	7.52494	,	7.44381	,	7.07696	,	6.94834	,	6.70038
GigaRays/s	,	4.14563	,	4.59752	,	4.5787	,	4.53534	,	4.44494	,	3.84553	,	3.944	,	4.09061	,	3.67778	,	3.70935	,	4.20181	,	4.16569	,	3.90252	,	4.28202	,	4.18066	,	4.05093	,	3.92343	,	3.50306	,	3.41598	,	3.39633	,	3.52274	,	3.9778	,	4.24681	,	4.42969	,	4.83427	,	4.88696	,	5.14028	,	5.23544	,	5.42918

AccelerationStructure: 
Ray tracing acceleration structure building statistics: 
	Bottom Level Acceleration Structure: 
		Maximum [B]: 6208
		Minimum [B]: 6208
		Currently Allocated [B]: 6208
		Currently Created [num]: 1
		Current Average [B]: 6208
		Maximum Triangles [num]: 12
		Minimum Triangles [num]: 12
		Total Triangles [num]: 12
		Maximum Meshes [num]: 1
		Minimum Meshes [num]: 1
		Total Meshes [num]: 1
	Top Level Acceleration Structure: 
		Maximum [B]: 64
		Minimum [B]: 64
		Currently Allocated [B]: 64
		Total Allocated [B]: 64
		Total Created [num]: 1
		Average Allocated [B]: 64
		Maximum Instances [num]: 1
		Minimum Instances [num]: 1
		Total Instances [num]: 1
	Scratch Buffer: 
		Maximum [B]: 3584
		Minimum [B]: 3584
		Currently Allocated [B]: 3584
		Total Allocated [B]: 3584
		Total Created [num]: 1
		Average Allocated [B]: 3584
ProfilingStack: 
[PROF] Profiling statistics: 
	Scopes entered : 29692
	Scopes exited : 29691
	Overhead per scope [ticks] : 1018.3
	Threads entered : 3
	Threads exited : 0
	============================
		         Sum[Mt]   Parent[%]  Samples[num]       Avg[Mt]   Last/Curr[Mt]        Self[Mt]     Self[%] 	 Name

	main:

		   29697.354 Mt   100.000 %         1 x    29697.357 Mt    29697.352 Mt        1.057 Mt     0.004 % 	 /

		   29696.424 Mt    99.997 %         1 x    29696.428 Mt    29696.423 Mt      381.229 Mt     1.284 % 	 ./Main application loop

		       3.191 Mt     0.011 %         1 x        3.191 Mt        0.000 Mt        3.191 Mt   100.000 % 	 ../TexturedRLInitialization

		       6.069 Mt     0.020 %         1 x        6.069 Mt        0.000 Mt        6.069 Mt   100.000 % 	 ../DeferredRLInitialization

		      57.695 Mt     0.194 %         1 x       57.695 Mt        0.000 Mt        3.811 Mt     6.605 % 	 ../RayTracingRLInitialization

		      53.884 Mt    93.395 %         1 x       53.884 Mt        0.000 Mt       53.884 Mt   100.000 % 	 .../PipelineBuild

		      11.571 Mt     0.039 %         1 x       11.571 Mt        0.000 Mt       11.571 Mt   100.000 % 	 ../ResolveRLInitialization

		     108.289 Mt     0.365 %         1 x      108.289 Mt        0.000 Mt        2.413 Mt     2.228 % 	 ../Initialization

		     105.876 Mt    97.772 %         1 x      105.876 Mt        0.000 Mt        1.134 Mt     1.071 % 	 .../ScenePrep

		      79.152 Mt    74.759 %         1 x       79.152 Mt        0.000 Mt       11.543 Mt    14.583 % 	 ..../SceneLoad

		      58.029 Mt    73.313 %         1 x       58.029 Mt        0.000 Mt       13.157 Mt    22.672 % 	 ...../glTF-LoadScene

		      44.872 Mt    77.328 %         2 x       22.436 Mt        0.000 Mt       44.872 Mt   100.000 % 	 ....../glTF-LoadImageData

		       4.102 Mt     5.182 %         1 x        4.102 Mt        0.000 Mt        4.102 Mt   100.000 % 	 ...../glTF-CreateScene

		       5.479 Mt     6.922 %         1 x        5.479 Mt        0.000 Mt        5.479 Mt   100.000 % 	 ...../SceneDesc-CreateScene

		       2.810 Mt     2.654 %         1 x        2.810 Mt        0.000 Mt        0.005 Mt     0.167 % 	 ..../TexturedRLPrep

		       2.805 Mt    99.833 %         1 x        2.805 Mt        0.000 Mt        0.440 Mt    15.683 % 	 ...../PrepareForRendering

		       2.065 Mt    73.591 %         1 x        2.065 Mt        0.000 Mt        2.065 Mt   100.000 % 	 ....../PrepareMaterials

		       0.301 Mt    10.725 %         1 x        0.301 Mt        0.000 Mt        0.301 Mt   100.000 % 	 ....../PrepareDrawBundle

		       2.089 Mt     1.973 %         1 x        2.089 Mt        0.000 Mt        0.004 Mt     0.201 % 	 ..../DeferredRLPrep

		       2.085 Mt    99.799 %         1 x        2.085 Mt        0.000 Mt        1.362 Mt    65.346 % 	 ...../PrepareForRendering

		       0.016 Mt     0.763 %         1 x        0.016 Mt        0.000 Mt        0.016 Mt   100.000 % 	 ....../PrepareMaterials

		       0.441 Mt    21.136 %         1 x        0.441 Mt        0.000 Mt        0.441 Mt   100.000 % 	 ....../BuildMaterialCache

		       0.266 Mt    12.755 %         1 x        0.266 Mt        0.000 Mt        0.266 Mt   100.000 % 	 ....../PrepareDrawBundle

		      12.373 Mt    11.687 %         1 x       12.373 Mt        0.000 Mt        3.600 Mt    29.098 % 	 ..../RayTracingRLPrep

		       0.232 Mt     1.877 %         1 x        0.232 Mt        0.000 Mt        0.232 Mt   100.000 % 	 ...../PrepareAccelerationStructures

		       0.024 Mt     0.193 %         1 x        0.024 Mt        0.000 Mt        0.024 Mt   100.000 % 	 ...../PrepareCache

		       0.472 Mt     3.815 %         1 x        0.472 Mt        0.000 Mt        0.472 Mt   100.000 % 	 ...../BuildCache

		       6.463 Mt    52.237 %         1 x        6.463 Mt        0.000 Mt        0.008 Mt     0.128 % 	 ...../BuildAccelerationStructures

		       6.455 Mt    99.872 %         1 x        6.455 Mt        0.000 Mt        2.393 Mt    37.075 % 	 ....../AccelerationStructureBuild

		       2.222 Mt    34.427 %         1 x        2.222 Mt        0.000 Mt        2.222 Mt   100.000 % 	 ......./BuildBottomLevelAS

		       1.840 Mt    28.498 %         1 x        1.840 Mt        0.000 Mt        1.840 Mt   100.000 % 	 ......./BuildTopLevelAS

		       1.581 Mt    12.780 %         1 x        1.581 Mt        0.000 Mt        1.581 Mt   100.000 % 	 ...../BuildShaderTables

		       8.317 Mt     7.856 %         1 x        8.317 Mt        0.000 Mt        8.317 Mt   100.000 % 	 ..../GpuSidePrep

		   29128.380 Mt    98.087 %      7632 x        3.817 Mt       10.967 Mt      389.984 Mt     1.339 % 	 ../MainLoop

		    1215.805 Mt     4.174 %      1749 x        0.695 Mt        0.743 Mt     1215.805 Mt   100.000 % 	 .../Update

		   27522.591 Mt    94.487 %      3379 x        8.145 Mt        9.268 Mt    27497.704 Mt    99.910 % 	 .../Render

		      24.887 Mt     0.090 %      3379 x        0.007 Mt        0.009 Mt       24.887 Mt   100.000 % 	 ..../RayTracingRLPrep

	WindowThread:

		   29687.203 Mt   100.000 %         1 x    29687.204 Mt    29687.202 Mt    29687.203 Mt   100.000 % 	 /

	GpuThread:

		   16572.477 Mt   100.000 %      3379 x        4.905 Mt        3.266 Mt      146.165 Mt     0.882 % 	 /

		       0.238 Mt     0.001 %         1 x        0.238 Mt        0.000 Mt        0.006 Mt     2.437 % 	 ./ScenePrep

		       0.232 Mt    97.563 %         1 x        0.232 Mt        0.000 Mt        0.001 Mt     0.428 % 	 ../AccelerationStructureBuild

		       0.156 Mt    67.426 %         1 x        0.156 Mt        0.000 Mt        0.156 Mt   100.000 % 	 .../BuildBottomLevelASGpu

		       0.075 Mt    32.146 %         1 x        0.075 Mt        0.000 Mt        0.075 Mt   100.000 % 	 .../BuildTopLevelASGpu

		   16424.445 Mt    99.107 %      3379 x        4.861 Mt        3.265 Mt        3.430 Mt     0.021 % 	 ./RayTracing

		    6741.228 Mt    41.044 %      3379 x        1.995 Mt        0.522 Mt     6741.228 Mt   100.000 % 	 ../RayTracingRL

		    9679.787 Mt    58.935 %      3379 x        2.865 Mt        2.741 Mt     9679.787 Mt   100.000 % 	 ../ResolveRL

		       1.629 Mt     0.010 %      3379 x        0.000 Mt        0.000 Mt        1.629 Mt   100.000 % 	 ./Present


	============================


