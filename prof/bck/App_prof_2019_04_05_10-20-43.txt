Configuration: 
Command line parameters = 000001607FBFEAC8 000001607FBFEB7A 000001607FBFEB8A 000001607FBFEBB4 
Window width = 1024
Window height = 768
Window title = Direct3D App
Back-buffer count = 3
Target FPS = 0
Target UPS = 60
Tearing = enabled
VSync = disabled
GUI rendering = enabled
Debug layer = enabled
GPU validation = disabled
GPU profiling = enabled
Render mode = Ray Tracing
Loaded scene file = Suzanne/Suzanne.gltf
Using testing scene = disabled
Duplication = 1
Duplication in cube = enabled
Duplication offset = 10

ProfilingBuildAggregator: 
BuildBottomLevelAS , 0 , 
BuildBottomLevelASGpu , 0 , 
BuildTopLevelAS , 0 , 
BuildTopLevelASGpu , 0 , 

ProfilingAggregator: 
Render , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 
Update , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 
RayTracing , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 
RayTracingRL , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 

FpsAggregator: 
FPS , 60 , 60 , 60 , 60 , 60 , 60 , 185 , 185 , 203 , 217 , 192 , 200 , 223 , 222 , 228 , 226 , 220 , 223 , 
UPS , 60 , 60 , 60 , 60 , 60 , 60 , 59 , 61 , 60 , 60 , 60 , 60 , 60 , 61 , 60 , 60 , 60 , 60 , 
FrameTime , 16.6667 , 16.6667 , 16.6667 , 16.6667 , 16.6667 , 16.6667 , 5.43576 , 5.43651 , 4.92618 , 4.62394 , 5.21526 , 5.0057 , 4.49054 , 4.51657 , 4.39979 , 4.43584 , 4.55893 , 4.49474 , 
GigaRays/s , 0.471859 , 0.471858 , 0.471859 , 0.471859 , 0.471858 , 0.471858 , 1.44678 , 1.44657 , 1.59643 , 1.70079 , 1.50794 , 1.57107 , 1.75131 , 1.74122 , 1.78743 , 1.77291 , 1.72504 , 1.74967 , 
AccelerationStructure: 
Ray tracing acceleration structure building statistics: 
	Bottom Level Acceleration Structure: 
		Maximum [B]: 283264
		Minimum [B]: 283264
		Currently Allocated [B]: 283264
		Currently Created [num]: 1
		Current Average [B]: 283264
		Maximum Triangles [num]: 3936
		Minimum Triangles [num]: 3936
		Total Triangles [num]: 3936
	Top Level Acceleration Structure: 
		Maximum [B]: 3584
		Minimum [B]: 3584
		Currently Allocated [B]: 3584
		Total Allocated [B]: 3584
		Total Created [num]: 1
		Average Allocated [B]: 3584
	Scratch Buffer: 
		Maximum [B]: 83968
		Minimum [B]: 83968
		Currently Allocated [B]: 83968
		Total Allocated [B]: 83968
		Total Created [num]: 1
		Average Allocated [B]: 83968
ProfilingStack: 
[PROF] Profiling statistics: 
	Scopes entered : 2047430
	Scopes exited : 2047425
	Overhead per scope [ticks] : 1021.8
	Threads entered : 3
	Threads exited : 0
	============================
		         Sum[Mt]   Parent[%]  Samples[num]       Avg[Mt]   Last/Curr[Mt]        Self[Mt]     Self[%] 	 Name

	main:

		   25493.373 Mt   100.000 %         1 x    25493.375 Mt    25493.372 Mt      363.272 Mt     1.425 % 	 /

		   25130.145 Mt    98.575 %         1 x    25130.147 Mt    25130.145 Mt     5418.251 Mt    21.561 % 	 ./Main application loop

		       2.596 Mt     0.010 %         1 x        2.596 Mt        0.000 Mt        2.596 Mt   100.000 % 	 ../TexturedRLInitialization

		       8.692 Mt     0.035 %         1 x        8.692 Mt        0.000 Mt        8.692 Mt   100.000 % 	 ../DeferredRLInitialization

		     154.267 Mt     0.614 %         1 x      154.267 Mt        0.000 Mt        2.221 Mt     1.440 % 	 ../RayTracingRLInitialization

		     152.046 Mt    98.560 %         1 x      152.046 Mt        0.000 Mt      152.046 Mt   100.000 % 	 .../PipelineBuild

		      12.259 Mt     0.049 %         1 x       12.259 Mt        0.000 Mt       12.259 Mt   100.000 % 	 ../ResolveRLInitialization

		     318.466 Mt     1.267 %         1 x      318.466 Mt        0.000 Mt        3.027 Mt     0.950 % 	 ../Initialization

		     315.440 Mt    99.050 %         1 x      315.440 Mt        0.000 Mt        2.229 Mt     0.707 % 	 .../ScenePrep

		     286.594 Mt    90.855 %         1 x      286.594 Mt        0.000 Mt       84.580 Mt    29.512 % 	 ..../SceneLoad

		     192.284 Mt    67.093 %         1 x      192.284 Mt        0.000 Mt       26.181 Mt    13.616 % 	 ...../glTF-LoadScene

		     166.103 Mt    86.384 %         2 x       83.051 Mt        0.000 Mt      166.103 Mt   100.000 % 	 ....../glTF-LoadImageData

		       6.915 Mt     2.413 %         1 x        6.915 Mt        0.000 Mt        6.915 Mt   100.000 % 	 ...../glTF-CreateScene

		       2.814 Mt     0.982 %         1 x        2.814 Mt        0.000 Mt        2.814 Mt   100.000 % 	 ...../SceneDesc-CreateScene

		       0.939 Mt     0.298 %         1 x        0.939 Mt        0.000 Mt        0.006 Mt     0.618 % 	 ..../TexturedRLPrep

		       0.933 Mt    99.382 %         1 x        0.933 Mt        0.000 Mt        0.334 Mt    35.766 % 	 ...../PrepareForRendering

		       0.284 Mt    30.429 %         1 x        0.284 Mt        0.000 Mt        0.284 Mt   100.000 % 	 ....../PrepareMaterials

		       0.315 Mt    33.805 %         1 x        0.315 Mt        0.000 Mt        0.315 Mt   100.000 % 	 ....../PrepareDrawBundle

		       1.745 Mt     0.553 %         1 x        1.745 Mt        0.000 Mt        0.004 Mt     0.252 % 	 ..../DeferredRLPrep

		       1.741 Mt    99.748 %         1 x        1.741 Mt        0.000 Mt        1.225 Mt    70.393 % 	 ...../PrepareForRendering

		       0.018 Mt     1.023 %         1 x        0.018 Mt        0.000 Mt        0.018 Mt   100.000 % 	 ....../PrepareMaterials

		       0.218 Mt    12.546 %         1 x        0.218 Mt        0.000 Mt        0.218 Mt   100.000 % 	 ....../BuildMaterialCache

		       0.279 Mt    16.039 %         1 x        0.279 Mt        0.000 Mt        0.279 Mt   100.000 % 	 ....../PrepareDrawBundle

		       5.332 Mt     1.690 %         1 x        5.332 Mt        0.000 Mt        2.383 Mt    44.692 % 	 ..../RayTracingRLPrep

		       0.079 Mt     1.474 %         1 x        0.079 Mt        0.000 Mt        0.079 Mt   100.000 % 	 ...../PrepareAccelerationStructures

		       0.024 Mt     0.458 %         1 x        0.024 Mt        0.000 Mt        0.024 Mt   100.000 % 	 ...../PrepareCache

		       0.230 Mt     4.308 %         1 x        0.230 Mt        0.000 Mt        0.230 Mt   100.000 % 	 ...../BuildCache

		       2.032 Mt    38.098 %         1 x        2.032 Mt        0.000 Mt        0.004 Mt     0.212 % 	 ...../BuildAccelerationStructures

		       2.027 Mt    99.788 %         1 x        2.027 Mt        0.000 Mt        0.678 Mt    33.434 % 	 ....../AccelerationStructureBuild

		       0.789 Mt    38.938 %         1 x        0.789 Mt        0.000 Mt        0.789 Mt   100.000 % 	 ......./BuildBottomLevelAS

		       0.560 Mt    27.628 %         1 x        0.560 Mt        0.000 Mt        0.560 Mt   100.000 % 	 ......./BuildTopLevelAS

		       0.585 Mt    10.970 %         1 x        0.585 Mt        0.000 Mt        0.585 Mt   100.000 % 	 ...../BuildShaderTables

		      18.600 Mt     5.897 %         1 x       18.600 Mt        0.000 Mt       18.600 Mt   100.000 % 	 ..../GpuSidePrep

		   19216.763 Mt    76.469 %   2013826 x        0.010 Mt        5.526 Mt     3621.240 Mt    18.844 % 	 ../MainLoop

		     565.652 Mt     2.944 %      1416 x        0.399 Mt        0.292 Mt      565.652 Mt   100.000 % 	 .../Update

		   15029.945 Mt    78.213 %      3310 x        4.541 Mt        5.302 Mt    12988.011 Mt    86.414 % 	 .../Render

		       1.750 Mt     0.012 %       237 x        0.007 Mt        0.000 Mt        1.750 Mt   100.000 % 	 ..../TexturedRLPrep

		    2011.484 Mt    13.383 %      3310 x        0.608 Mt        3.730 Mt     2011.484 Mt   100.000 % 	 ..../GuiModelApply

		      17.152 Mt     0.114 %      3073 x        0.006 Mt        0.003 Mt       17.152 Mt   100.000 % 	 ..../DeferredRLPrep

		      11.621 Mt     0.077 %      3073 x        0.004 Mt        0.004 Mt       11.621 Mt   100.000 % 	 ..../RayTracingRLPrep

	WindowThread:

		   25127.735 Mt   100.000 %         1 x    25127.735 Mt    25127.734 Mt    25127.735 Mt   100.000 % 	 /

	GpuThread:

		    2884.183 Mt   100.000 %      3309 x        0.872 Mt        0.640 Mt      138.677 Mt     4.808 % 	 /

		       0.507 Mt     0.018 %         1 x        0.507 Mt        0.000 Mt        0.006 Mt     1.131 % 	 ./ScenePrep

		       0.501 Mt    98.869 %         1 x        0.501 Mt        0.000 Mt        0.001 Mt     0.185 % 	 ../AccelerationStructureBuild

		       0.439 Mt    87.663 %         1 x        0.439 Mt        0.000 Mt        0.439 Mt   100.000 % 	 .../BuildBottomLevelASGpu

		       0.061 Mt    12.152 %         1 x        0.061 Mt        0.000 Mt        0.061 Mt   100.000 % 	 .../BuildTopLevelASGpu

		       1.224 Mt     0.042 %       237 x        0.005 Mt        0.000 Mt        1.224 Mt   100.000 % 	 ./Textured

		      31.789 Mt     1.102 %      3309 x        0.010 Mt        0.010 Mt       31.789 Mt   100.000 % 	 ./ImGui

		       5.206 Mt     0.181 %      3309 x        0.002 Mt        0.001 Mt        5.206 Mt   100.000 % 	 ./Present

		    2706.780 Mt    93.849 %      3072 x        0.881 Mt        0.627 Mt        4.691 Mt     0.173 % 	 ./RayTracing

		     106.325 Mt     3.928 %      3072 x        0.035 Mt        0.009 Mt      106.325 Mt   100.000 % 	 ../DeferredRL

		     558.601 Mt    20.637 %      3072 x        0.182 Mt        0.053 Mt      558.601 Mt   100.000 % 	 ../RayTracingRL

		    2037.163 Mt    75.262 %      3072 x        0.663 Mt        0.563 Mt     2037.163 Mt   100.000 % 	 ../ResolveRL


	============================


