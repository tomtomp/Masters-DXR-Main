Configuration: 
Command line parameters  = E:\programming\FIT-DIP\Masters-DXR-Main-git\\build\Measurement\x64\Debug\Measurement.exe --scene Cube/Cube.gltf 
Window width             = 1024
Window height            = 768
Window title             = Direct3D App
Back-buffer count        = 3
Target FPS               = 60
Target UPS               = 60
Tearing                  = disabled
VSync                    = disabled
GUI rendering            = enabled
Debug layer              = enabled
GPU validation           = disabled
GPU profiling            = enabled
Render mode              = Rasterization
Loaded scene file        = Cube/Cube.gltf
Using testing scene      = disabled
Duplication              = 1
Duplication in cube      = disabled
Duplication offset       = 10
BLAS duplication         = disabled
Rays per pixel           = 9

ProfilingBuildAggregator: 
BuildBottomLevelAS	,	1.0274
BuildBottomLevelASGpu	,	0.178848
BuildTopLevelAS	,	0.6467
BuildTopLevelASGpu	,	0.083616

ProfilingAggregator: 
Render	,	1.5571
Update	,	0.287

FpsAggregator: 
FPS	,	56
UPS	,	59
FrameTime	,	17.8572
GigaRays/s	,	0

AccelerationStructure: 
Ray tracing acceleration structure building statistics: 
	Bottom Level Acceleration Structure: 
		Maximum [B]: 6208
		Minimum [B]: 6208
		Currently Allocated [B]: 6208
		Currently Created [num]: 1
		Current Average [B]: 6208
		Maximum Triangles [num]: 12
		Minimum Triangles [num]: 12
		Total Triangles [num]: 12
		Maximum Meshes [num]: 1
		Minimum Meshes [num]: 1
		Total Meshes [num]: 1
	Top Level Acceleration Structure: 
		Maximum [B]: 64
		Minimum [B]: 64
		Currently Allocated [B]: 64
		Total Allocated [B]: 64
		Total Created [num]: 1
		Average Allocated [B]: 64
		Maximum Instances [num]: 1
		Minimum Instances [num]: 1
		Total Instances [num]: 1
	Scratch Buffer: 
		Maximum [B]: 3584
		Minimum [B]: 3584
		Currently Allocated [B]: 3584
		Total Allocated [B]: 3584
		Total Created [num]: 1
		Average Allocated [B]: 3584
ProfilingStack: 
[PROF] Profiling statistics: 
	Scopes entered : 391650
	Scopes exited : 391649
	Overhead per scope [ticks] : 1017.7
	Threads entered : 3
	Threads exited : 0
	============================
		         Sum[Mt]   Parent[%]  Samples[num]       Avg[Mt]   Last/Curr[Mt]        Self[Mt]     Self[%] 	 Name

	main:

		    2947.928 Mt   100.000 %         1 x     2947.930 Mt     2947.928 Mt      463.839 Mt    15.734 % 	 /

		    2484.133 Mt    84.267 %         1 x     2484.134 Mt     2484.132 Mt     1347.151 Mt    54.230 % 	 ./Main application loop

		       4.123 Mt     0.166 %         1 x        4.123 Mt        0.000 Mt        4.123 Mt   100.000 % 	 ../TexturedRLInitialization

		      10.107 Mt     0.407 %         1 x       10.107 Mt        0.000 Mt       10.107 Mt   100.000 % 	 ../DeferredRLInitialization

		      71.614 Mt     2.883 %         1 x       71.614 Mt        0.000 Mt        1.708 Mt     2.385 % 	 ../RayTracingRLInitialization

		      69.906 Mt    97.615 %         1 x       69.906 Mt        0.000 Mt       69.906 Mt   100.000 % 	 .../PipelineBuild

		      10.368 Mt     0.417 %         1 x       10.368 Mt        0.000 Mt       10.368 Mt   100.000 % 	 ../ResolveRLInitialization

		      89.820 Mt     3.616 %         1 x       89.820 Mt        0.000 Mt        2.431 Mt     2.706 % 	 ../Initialization

		      87.389 Mt    97.294 %         1 x       87.389 Mt        0.000 Mt        0.850 Mt     0.973 % 	 .../ScenePrep

		      70.012 Mt    80.115 %         1 x       70.012 Mt        0.000 Mt        6.713 Mt     9.588 % 	 ..../SceneLoad

		      58.621 Mt    83.730 %         1 x       58.621 Mt        0.000 Mt       13.279 Mt    22.651 % 	 ...../glTF-LoadScene

		      45.342 Mt    77.349 %         2 x       22.671 Mt        0.000 Mt       45.342 Mt   100.000 % 	 ....../glTF-LoadImageData

		       2.569 Mt     3.669 %         1 x        2.569 Mt        0.000 Mt        2.569 Mt   100.000 % 	 ...../glTF-CreateScene

		       2.109 Mt     3.012 %         1 x        2.109 Mt        0.000 Mt        2.109 Mt   100.000 % 	 ...../SceneDesc-CreateScene

		       0.782 Mt     0.895 %         1 x        0.782 Mt        0.000 Mt        0.006 Mt     0.818 % 	 ..../TexturedRLPrep

		       0.776 Mt    99.182 %         1 x        0.776 Mt        0.000 Mt        0.254 Mt    32.809 % 	 ...../PrepareForRendering

		       0.221 Mt    28.490 %         1 x        0.221 Mt        0.000 Mt        0.221 Mt   100.000 % 	 ....../PrepareMaterials

		       0.300 Mt    38.701 %         1 x        0.300 Mt        0.000 Mt        0.300 Mt   100.000 % 	 ....../PrepareDrawBundle

		       1.629 Mt     1.864 %         1 x        1.629 Mt        0.000 Mt        0.004 Mt     0.276 % 	 ..../DeferredRLPrep

		       1.625 Mt    99.724 %         1 x        1.625 Mt        0.000 Mt        1.141 Mt    70.212 % 	 ...../PrepareForRendering

		       0.016 Mt     1.003 %         1 x        0.016 Mt        0.000 Mt        0.016 Mt   100.000 % 	 ....../PrepareMaterials

		       0.194 Mt    11.911 %         1 x        0.194 Mt        0.000 Mt        0.194 Mt   100.000 % 	 ....../BuildMaterialCache

		       0.274 Mt    16.873 %         1 x        0.274 Mt        0.000 Mt        0.274 Mt   100.000 % 	 ....../PrepareDrawBundle

		       6.054 Mt     6.927 %         1 x        6.054 Mt        0.000 Mt        2.295 Mt    37.910 % 	 ..../RayTracingRLPrep

		       0.240 Mt     3.963 %         1 x        0.240 Mt        0.000 Mt        0.240 Mt   100.000 % 	 ...../PrepareAccelerationStructures

		       0.025 Mt     0.418 %         1 x        0.025 Mt        0.000 Mt        0.025 Mt   100.000 % 	 ...../PrepareCache

		       0.264 Mt     4.356 %         1 x        0.264 Mt        0.000 Mt        0.264 Mt   100.000 % 	 ...../BuildCache

		       2.490 Mt    41.129 %         1 x        2.490 Mt        0.000 Mt        0.009 Mt     0.365 % 	 ...../BuildAccelerationStructures

		       2.481 Mt    99.635 %         1 x        2.481 Mt        0.000 Mt        0.807 Mt    32.515 % 	 ....../AccelerationStructureBuild

		       1.027 Mt    41.416 %         1 x        1.027 Mt        0.000 Mt        1.027 Mt   100.000 % 	 ......./BuildBottomLevelAS

		       0.647 Mt    26.069 %         1 x        0.647 Mt        0.000 Mt        0.647 Mt   100.000 % 	 ......./BuildTopLevelAS

		       0.740 Mt    12.224 %         1 x        0.740 Mt        0.000 Mt        0.740 Mt   100.000 % 	 ...../BuildShaderTables

		       8.063 Mt     9.226 %         1 x        8.063 Mt        0.000 Mt        8.063 Mt   100.000 % 	 ..../GpuSidePrep

		     950.949 Mt    38.281 %    390902 x        0.002 Mt        7.017 Mt      655.996 Mt    68.983 % 	 ../MainLoop

		     103.446 Mt    10.878 %       105 x        0.985 Mt        7.005 Mt      103.446 Mt   100.000 % 	 .../Update

		     191.507 Mt    20.139 %       101 x        1.896 Mt        7.976 Mt      137.220 Mt    71.652 % 	 .../Render

		       0.420 Mt     0.219 %       101 x        0.004 Mt        0.003 Mt        0.420 Mt   100.000 % 	 ..../TexturedRLPrep

		      53.868 Mt    28.128 %       101 x        0.533 Mt        0.481 Mt       53.868 Mt   100.000 % 	 ..../GuiModelApply

	WindowThread:

		    2480.874 Mt   100.000 %         1 x     2480.874 Mt     2480.874 Mt     2480.874 Mt   100.000 % 	 /

	GpuThread:

		     110.216 Mt   100.000 %       101 x        1.091 Mt        0.043 Mt      108.554 Mt    98.492 % 	 /

		       0.272 Mt     0.246 %         1 x        0.272 Mt        0.000 Mt        0.007 Mt     2.498 % 	 ./ScenePrep

		       0.265 Mt    97.502 %         1 x        0.265 Mt        0.000 Mt        0.002 Mt     0.894 % 	 ../AccelerationStructureBuild

		       0.179 Mt    67.533 %         1 x        0.179 Mt        0.000 Mt        0.179 Mt   100.000 % 	 .../BuildBottomLevelASGpu

		       0.084 Mt    31.573 %         1 x        0.084 Mt        0.000 Mt        0.084 Mt   100.000 % 	 .../BuildTopLevelASGpu

		       0.709 Mt     0.643 %       101 x        0.007 Mt        0.007 Mt        0.709 Mt   100.000 % 	 ./Textured

		       0.499 Mt     0.453 %       101 x        0.005 Mt        0.030 Mt        0.499 Mt   100.000 % 	 ./ImGui

		       0.182 Mt     0.166 %       101 x        0.002 Mt        0.004 Mt        0.182 Mt   100.000 % 	 ./Present


	============================


