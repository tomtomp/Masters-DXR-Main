Configuration: 
Command line parameters  = E:\programming\FIT-DIP\Masters-DXR-Main-git\build\Measurement\x64\Debug\Measurement.exe --scene Sponza/Sponza.gltf --duplication-cube --profile-duplication 7 --width 1024 --height 768 --profile-output DuplicationSponza768Ra 
Window width             = 1024
Window height            = 768
Window title             = Direct3D App
Back-buffer count        = 3
Target FPS               = 60
Target UPS               = 60
Tearing                  = disabled
VSync                    = disabled
GUI rendering            = disabled
Debug layer              = enabled
GPU validation           = disabled
GPU profiling            = enabled
Render mode              = Rasterization
Loaded scene file        = Sponza/Sponza.gltf
Using testing scene      = disabled
Duplication              = 7
Duplication in cube      = enabled
Duplication offset       = 10
BLAS duplication         = enabled
Rays per pixel           = 9

ProfilingBuildAggregator: 
BuildBottomLevelAS	,	4.8673
BuildBottomLevelASGpu	,	2.40416
BuildTopLevelAS	,	1.7618
BuildTopLevelASGpu	,	0.090912

ProfilingAggregator: 
Render	,	2.2679	,	23.4448	,	44.8507	,	68.4185	,	129.015	,	217.26	,	1014.59
Update	,	0.2893	,	0.8308	,	0.7914	,	0.5786	,	0.3227	,	1.0468	,	0.5251

FpsAggregator: 
FPS	,	56	,	58	,	31	,	14	,	6	,	3	,	1
UPS	,	59	,	60	,	60	,	59	,	63	,	66	,	14
FrameTime	,	17.8572	,	17.4796	,	32.8996	,	72.6805	,	183.009	,	402.974	,	1148.7
GigaRays/s	,	0	,	0	,	0	,	0	,	0	,	0	,	0

AccelerationStructure: 
Ray tracing acceleration structure building statistics: 
	Bottom Level Acceleration Structure: 
		Maximum [B]: 18391808
		Minimum [B]: 18391808
		Currently Allocated [B]: 18391808
		Currently Created [num]: 1
		Current Average [B]: 18391808
		Maximum Triangles [num]: 262267
		Minimum Triangles [num]: 262267
		Total Triangles [num]: 262267
		Maximum Meshes [num]: 103
		Minimum Meshes [num]: 103
		Total Meshes [num]: 103
	Top Level Acceleration Structure: 
		Maximum [B]: 64
		Minimum [B]: 64
		Currently Allocated [B]: 64
		Total Allocated [B]: 64
		Total Created [num]: 1
		Average Allocated [B]: 64
		Maximum Instances [num]: 1
		Minimum Instances [num]: 1
		Total Instances [num]: 1
	Scratch Buffer: 
		Maximum [B]: 14998528
		Minimum [B]: 14998528
		Currently Allocated [B]: 14998528
		Total Allocated [B]: 14998528
		Total Created [num]: 1
		Average Allocated [B]: 14998528
ProfilingStack: 
[PROF] Profiling statistics: 
	Scopes entered : 318569
	Scopes exited : 318568
	Overhead per scope [ticks] : 1018.8
	Threads entered : 3
	Threads exited : 0
	============================
		         Sum[Mt]   Parent[%]  Samples[num]       Avg[Mt]   Last/Curr[Mt]        Self[Mt]     Self[%] 	 Name

	main:

		   17565.074 Mt   100.000 %         1 x    17565.078 Mt    17565.073 Mt        0.847 Mt     0.005 % 	 /

		   17564.369 Mt    99.996 %         1 x    17564.375 Mt    17564.368 Mt     1110.037 Mt     6.320 % 	 ./Main application loop

		       2.771 Mt     0.016 %         1 x        2.771 Mt        0.000 Mt        2.771 Mt   100.000 % 	 ../TexturedRLInitialization

		       6.987 Mt     0.040 %         1 x        6.987 Mt        0.000 Mt        6.987 Mt   100.000 % 	 ../DeferredRLInitialization

		      54.226 Mt     0.309 %         1 x       54.226 Mt        0.000 Mt        2.071 Mt     3.819 % 	 ../RayTracingRLInitialization

		      52.155 Mt    96.181 %         1 x       52.155 Mt        0.000 Mt       52.155 Mt   100.000 % 	 .../PipelineBuild

		      11.104 Mt     0.063 %         1 x       11.104 Mt        0.000 Mt       11.104 Mt   100.000 % 	 ../ResolveRLInitialization

		    9160.663 Mt    52.155 %         1 x     9160.663 Mt        0.000 Mt        2.746 Mt     0.030 % 	 ../Initialization

		    9157.917 Mt    99.970 %         1 x     9157.917 Mt        0.000 Mt        2.943 Mt     0.032 % 	 .../ScenePrep

		    9049.522 Mt    98.816 %         1 x     9049.522 Mt        0.000 Mt      296.762 Mt     3.279 % 	 ..../SceneLoad

		    8245.461 Mt    91.115 %         1 x     8245.461 Mt        0.000 Mt     1138.161 Mt    13.803 % 	 ...../glTF-LoadScene

		    7107.300 Mt    86.197 %        69 x      103.004 Mt        0.000 Mt     7107.300 Mt   100.000 % 	 ....../glTF-LoadImageData

		     356.420 Mt     3.939 %         1 x      356.420 Mt        0.000 Mt      356.420 Mt   100.000 % 	 ...../glTF-CreateScene

		     150.878 Mt     1.667 %         1 x      150.878 Mt        0.000 Mt      150.878 Mt   100.000 % 	 ...../SceneDesc-CreateScene

		      12.898 Mt     0.141 %         1 x       12.898 Mt        0.000 Mt        0.010 Mt     0.080 % 	 ..../TexturedRLPrep

		      12.887 Mt    99.920 %         1 x       12.887 Mt        0.000 Mt        2.478 Mt    19.225 % 	 ...../PrepareForRendering

		       5.454 Mt    42.320 %         1 x        5.454 Mt        0.000 Mt        5.454 Mt   100.000 % 	 ....../PrepareMaterials

		       4.956 Mt    38.455 %         1 x        4.956 Mt        0.000 Mt        4.956 Mt   100.000 % 	 ....../PrepareDrawBundle

		      16.103 Mt     0.176 %         1 x       16.103 Mt        0.000 Mt        0.011 Mt     0.069 % 	 ..../DeferredRLPrep

		      16.092 Mt    99.931 %         1 x       16.092 Mt        0.000 Mt        5.247 Mt    32.606 % 	 ...../PrepareForRendering

		       0.039 Mt     0.244 %         1 x        0.039 Mt        0.000 Mt        0.039 Mt   100.000 % 	 ....../PrepareMaterials

		       6.563 Mt    40.783 %         1 x        6.563 Mt        0.000 Mt        6.563 Mt   100.000 % 	 ....../BuildMaterialCache

		       4.243 Mt    26.368 %         1 x        4.243 Mt        0.000 Mt        4.243 Mt   100.000 % 	 ....../PrepareDrawBundle

		      43.708 Mt     0.477 %         1 x       43.708 Mt        0.000 Mt       11.664 Mt    26.687 % 	 ..../RayTracingRLPrep

		       2.059 Mt     4.712 %         1 x        2.059 Mt        0.000 Mt        2.059 Mt   100.000 % 	 ...../PrepareAccelerationStructures

		       0.081 Mt     0.186 %         1 x        0.081 Mt        0.000 Mt        0.081 Mt   100.000 % 	 ...../PrepareCache

		      10.376 Mt    23.739 %         1 x       10.376 Mt        0.000 Mt       10.376 Mt   100.000 % 	 ...../BuildCache

		      12.333 Mt    28.217 %         1 x       12.333 Mt        0.000 Mt        0.011 Mt     0.088 % 	 ...../BuildAccelerationStructures

		      12.322 Mt    99.912 %         1 x       12.322 Mt        0.000 Mt        5.693 Mt    46.203 % 	 ....../AccelerationStructureBuild

		       4.867 Mt    39.500 %         1 x        4.867 Mt        0.000 Mt        4.867 Mt   100.000 % 	 ......./BuildBottomLevelAS

		       1.762 Mt    14.298 %         1 x        1.762 Mt        0.000 Mt        1.762 Mt   100.000 % 	 ......./BuildTopLevelAS

		       7.194 Mt    16.459 %         1 x        7.194 Mt        0.000 Mt        7.194 Mt   100.000 % 	 ...../BuildShaderTables

		      32.744 Mt     0.358 %         1 x       32.744 Mt        0.000 Mt       32.744 Mt   100.000 % 	 ..../GpuSidePrep

		    7218.582 Mt    41.098 %    317314 x        0.023 Mt      418.292 Mt      631.434 Mt     8.747 % 	 ../MainLoop

		     571.524 Mt     7.917 %       450 x        1.270 Mt        0.532 Mt      571.524 Mt   100.000 % 	 .../Update

		    6015.624 Mt    83.335 %       170 x       35.386 Mt      338.334 Mt     4449.974 Mt    73.974 % 	 .../Render

		    1565.650 Mt    26.026 %       170 x        9.210 Mt        0.005 Mt        0.705 Mt     0.045 % 	 ..../TexturedRLPrep

		    1564.945 Mt    99.955 %         7 x      223.564 Mt        0.000 Mt       36.331 Mt     2.322 % 	 ...../PrepareForRendering

		       2.437 Mt     0.156 %         7 x        0.348 Mt        0.000 Mt        2.437 Mt   100.000 % 	 ....../PrepareMaterials

		    1526.177 Mt    97.523 %         7 x      218.025 Mt        0.000 Mt     1526.177 Mt   100.000 % 	 ....../PrepareDrawBundle

	WindowThread:

		   17563.965 Mt   100.000 %         1 x    17563.967 Mt    17563.964 Mt    17563.965 Mt   100.000 % 	 /

	GpuThread:

		    1015.075 Mt   100.000 %       170 x        5.971 Mt       91.250 Mt      127.563 Mt    12.567 % 	 /

		       2.504 Mt     0.247 %         1 x        2.504 Mt        0.000 Mt        0.007 Mt     0.289 % 	 ./ScenePrep

		       2.496 Mt    99.711 %         1 x        2.496 Mt        0.000 Mt        0.001 Mt     0.055 % 	 ../AccelerationStructureBuild

		       2.404 Mt    96.303 %         1 x        2.404 Mt        0.000 Mt        2.404 Mt   100.000 % 	 .../BuildBottomLevelASGpu

		       0.091 Mt     3.642 %         1 x        0.091 Mt        0.000 Mt        0.091 Mt   100.000 % 	 .../BuildTopLevelASGpu

		     884.484 Mt    87.135 %       170 x        5.203 Mt       91.245 Mt      884.484 Mt   100.000 % 	 ./Textured

		       0.524 Mt     0.052 %       170 x        0.003 Mt        0.002 Mt        0.524 Mt   100.000 % 	 ./Present


	============================


