Configuration: 
Command line parameters  = E:\programming\FIT-DIP\Masters-DXR-Main-git\build\Measurement\x64\Debug\Measurement.exe --scene Cube/Cube.gltf --profile-track Cube/Cube.trk --width 1024 --height 768 --profile-output TrackCube768Ra 
Window width             = 1024
Window height            = 768
Window title             = Direct3D App
Back-buffer count        = 3
Target FPS               = 0
Target UPS               = 60
Tearing                  = enabled
VSync                    = disabled
GUI rendering            = disabled
Debug layer              = enabled
GPU validation           = disabled
GPU profiling            = enabled
Render mode              = Rasterization
Loaded scene file        = Cube/Cube.gltf
Using testing scene      = disabled
Duplication              = 1
Duplication in cube      = disabled
Duplication offset       = 10
BLAS duplication         = disabled
Rays per pixel           = 9

ProfilingBuildAggregator: 
BuildBottomLevelAS	,	1.2247
BuildBottomLevelASGpu	,	0.127136
BuildTopLevelAS	,	0.7409
BuildTopLevelASGpu	,	0.060864

ProfilingAggregator: 
Render	,	0.7992	,	0.8055	,	0.6679	,	0.6535	,	0.6448	,	0.7065	,	0.7018	,	0.6815	,	0.6962	,	0.7081	,	0.6711	,	0.6919	,	0.6545	,	0.7501	,	0.6553	,	0.6706	,	0.6908	,	0.7063	,	0.7151	,	1.1523	,	0.7219	,	0.7532	,	0.6709	,	0.6684	,	0.7963	,	0.6521	,	0.64	,	0.7615	,	0.6554	,	0.6458
Update	,	0.2901	,	0.3023	,	0.2958	,	0.2982	,	0.2943	,	0.2906	,	0.2975	,	0.2932	,	0.2908	,	0.2925	,	0.2936	,	0.2944	,	0.2893	,	0.2939	,	0.2973	,	0.2953	,	0.2935	,	0.2933	,	0.2942	,	0.4819	,	0.3056	,	0.2987	,	0.2989	,	0.2964	,	0.292	,	0.2966	,	0.2936	,	0.8459	,	0.2924	,	0.8234

FpsAggregator: 
FPS	,	1267	,	1278	,	1307	,	1237	,	1211	,	1112	,	1074	,	1078	,	1060	,	913	,	1140	,	1082	,	1148	,	1045	,	1112	,	1075	,	1153	,	1084	,	1121	,	1069	,	1139	,	1081	,	1043	,	1143	,	888	,	1062	,	1130	,	933	,	990	,	1152
UPS	,	59	,	61	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60	,	60
FrameTime	,	0.789589	,	0.783332	,	0.765122	,	0.80888	,	0.825871	,	0.89966	,	0.93121	,	0.928205	,	0.943866	,	1.09606	,	0.877534	,	0.924802	,	0.871113	,	0.9571	,	0.899818	,	0.930342	,	0.86778	,	0.922733	,	0.892422	,	0.936327	,	0.87813	,	0.925139	,	0.958813	,	0.875024	,	1.12631	,	0.94221	,	0.885201	,	1.07211	,	1.01038	,	0.8684
GigaRays/s	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0	,	0

AccelerationStructure: 
Ray tracing acceleration structure building statistics: 
	Bottom Level Acceleration Structure: 
		Maximum [B]: 6208
		Minimum [B]: 6208
		Currently Allocated [B]: 6208
		Currently Created [num]: 1
		Current Average [B]: 6208
		Maximum Triangles [num]: 12
		Minimum Triangles [num]: 12
		Total Triangles [num]: 12
		Maximum Meshes [num]: 1
		Minimum Meshes [num]: 1
		Total Meshes [num]: 1
	Top Level Acceleration Structure: 
		Maximum [B]: 64
		Minimum [B]: 64
		Currently Allocated [B]: 64
		Total Allocated [B]: 64
		Total Created [num]: 1
		Average Allocated [B]: 64
		Maximum Instances [num]: 1
		Minimum Instances [num]: 1
		Total Instances [num]: 1
	Scratch Buffer: 
		Maximum [B]: 3584
		Minimum [B]: 3584
		Currently Allocated [B]: 3584
		Total Allocated [B]: 3584
		Total Created [num]: 1
		Average Allocated [B]: 3584
ProfilingStack: 
[PROF] Profiling statistics: 
	Scopes entered : 171926
	Scopes exited : 171925
	Overhead per scope [ticks] : 1012.9
	Threads entered : 3
	Threads exited : 0
	============================
		         Sum[Mt]   Parent[%]  Samples[num]       Avg[Mt]   Last/Curr[Mt]        Self[Mt]     Self[%] 	 Name

	main:

		   30568.018 Mt   100.000 %         1 x    30568.020 Mt    30568.018 Mt        0.622 Mt     0.002 % 	 /

		   30567.461 Mt    99.998 %         1 x    30567.463 Mt    30567.461 Mt      428.145 Mt     1.401 % 	 ./Main application loop

		       2.928 Mt     0.010 %         1 x        2.928 Mt        0.000 Mt        2.928 Mt   100.000 % 	 ../TexturedRLInitialization

		       5.979 Mt     0.020 %         1 x        5.979 Mt        0.000 Mt        5.979 Mt   100.000 % 	 ../DeferredRLInitialization

		      76.999 Mt     0.252 %         1 x       76.999 Mt        0.000 Mt        2.694 Mt     3.499 % 	 ../RayTracingRLInitialization

		      74.305 Mt    96.501 %         1 x       74.305 Mt        0.000 Mt       74.305 Mt   100.000 % 	 .../PipelineBuild

		      15.332 Mt     0.050 %         1 x       15.332 Mt        0.000 Mt       15.332 Mt   100.000 % 	 ../ResolveRLInitialization

		     118.207 Mt     0.387 %         1 x      118.207 Mt        0.000 Mt        2.410 Mt     2.039 % 	 ../Initialization

		     115.797 Mt    97.961 %         1 x      115.797 Mt        0.000 Mt        0.955 Mt     0.824 % 	 .../ScenePrep

		      91.040 Mt    78.620 %         1 x       91.040 Mt        0.000 Mt       17.163 Mt    18.852 % 	 ..../SceneLoad

		      64.588 Mt    70.944 %         1 x       64.588 Mt        0.000 Mt       18.320 Mt    28.365 % 	 ...../glTF-LoadScene

		      46.267 Mt    71.635 %         2 x       23.134 Mt        0.000 Mt       46.267 Mt   100.000 % 	 ....../glTF-LoadImageData

		       6.336 Mt     6.959 %         1 x        6.336 Mt        0.000 Mt        6.336 Mt   100.000 % 	 ...../glTF-CreateScene

		       2.953 Mt     3.244 %         1 x        2.953 Mt        0.000 Mt        2.953 Mt   100.000 % 	 ...../SceneDesc-CreateScene

		       0.826 Mt     0.713 %         1 x        0.826 Mt        0.000 Mt        0.005 Mt     0.569 % 	 ..../TexturedRLPrep

		       0.821 Mt    99.431 %         1 x        0.821 Mt        0.000 Mt        0.243 Mt    29.534 % 	 ...../PrepareForRendering

		       0.277 Mt    33.772 %         1 x        0.277 Mt        0.000 Mt        0.277 Mt   100.000 % 	 ....../PrepareMaterials

		       0.301 Mt    36.695 %         1 x        0.301 Mt        0.000 Mt        0.301 Mt   100.000 % 	 ....../PrepareDrawBundle

		       2.196 Mt     1.896 %         1 x        2.196 Mt        0.000 Mt        0.005 Mt     0.214 % 	 ..../DeferredRLPrep

		       2.191 Mt    99.786 %         1 x        2.191 Mt        0.000 Mt        1.521 Mt    69.428 % 	 ...../PrepareForRendering

		       0.015 Mt     0.707 %         1 x        0.015 Mt        0.000 Mt        0.015 Mt   100.000 % 	 ....../PrepareMaterials

		       0.339 Mt    15.491 %         1 x        0.339 Mt        0.000 Mt        0.339 Mt   100.000 % 	 ....../BuildMaterialCache

		       0.315 Mt    14.373 %         1 x        0.315 Mt        0.000 Mt        0.315 Mt   100.000 % 	 ....../PrepareDrawBundle

		      10.057 Mt     8.685 %         1 x       10.057 Mt        0.000 Mt        3.238 Mt    32.199 % 	 ..../RayTracingRLPrep

		       0.273 Mt     2.717 %         1 x        0.273 Mt        0.000 Mt        0.273 Mt   100.000 % 	 ...../PrepareAccelerationStructures

		       0.028 Mt     0.279 %         1 x        0.028 Mt        0.000 Mt        0.028 Mt   100.000 % 	 ...../PrepareCache

		       2.003 Mt    19.917 %         1 x        2.003 Mt        0.000 Mt        2.003 Mt   100.000 % 	 ...../BuildCache

		       3.514 Mt    34.935 %         1 x        3.514 Mt        0.000 Mt        0.008 Mt     0.222 % 	 ...../BuildAccelerationStructures

		       3.506 Mt    99.778 %         1 x        3.506 Mt        0.000 Mt        1.540 Mt    43.931 % 	 ....../AccelerationStructureBuild

		       1.225 Mt    34.935 %         1 x        1.225 Mt        0.000 Mt        1.225 Mt   100.000 % 	 ......./BuildBottomLevelAS

		       0.741 Mt    21.134 %         1 x        0.741 Mt        0.000 Mt        0.741 Mt   100.000 % 	 ......./BuildTopLevelAS

		       1.001 Mt     9.952 %         1 x        1.001 Mt        0.000 Mt        1.001 Mt   100.000 % 	 ...../BuildShaderTables

		      10.724 Mt     9.261 %         1 x       10.724 Mt        0.000 Mt       10.724 Mt   100.000 % 	 ..../GpuSidePrep

		   29919.871 Mt    97.881 %     37572 x        0.796 Mt        1.505 Mt      393.296 Mt     1.314 % 	 ../MainLoop

		     702.319 Mt     2.347 %      1801 x        0.390 Mt        0.297 Mt      702.319 Mt   100.000 % 	 .../Update

		   28824.256 Mt    96.338 %     33129 x        0.870 Mt        1.200 Mt    28706.415 Mt    99.591 % 	 .../Render

		     117.841 Mt     0.409 %     33129 x        0.004 Mt        0.003 Mt      117.841 Mt   100.000 % 	 ..../TexturedRLPrep

	WindowThread:

		   30564.389 Mt   100.000 %         1 x    30564.390 Mt    30564.389 Mt    30564.389 Mt   100.000 % 	 /

	GpuThread:

		    1567.504 Mt   100.000 %     33129 x        0.047 Mt        0.032 Mt      133.446 Mt     8.513 % 	 /

		       0.195 Mt     0.012 %         1 x        0.195 Mt        0.000 Mt        0.006 Mt     3.089 % 	 ./ScenePrep

		       0.189 Mt    96.911 %         1 x        0.189 Mt        0.000 Mt        0.001 Mt     0.390 % 	 ../AccelerationStructureBuild

		       0.127 Mt    67.362 %         1 x        0.127 Mt        0.000 Mt        0.127 Mt   100.000 % 	 .../BuildBottomLevelASGpu

		       0.061 Mt    32.248 %         1 x        0.061 Mt        0.000 Mt        0.061 Mt   100.000 % 	 .../BuildTopLevelASGpu

		    1296.169 Mt    82.690 %     33129 x        0.039 Mt        0.028 Mt     1296.169 Mt   100.000 % 	 ./Textured

		     137.694 Mt     8.784 %     33129 x        0.004 Mt        0.003 Mt      137.694 Mt   100.000 % 	 ./Present


	============================


