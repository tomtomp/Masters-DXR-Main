Configuration: 
Command line parameters  = E:\programming\FIT-DIP\Masters-DXR-Main-git\\build\Measurement\x64\Debug\Measurement.exe --scene Sponza/Sponza.gltf --duplication-cube --rt-mode --profile-track Sponza/Sponza.trk 
Window width             = 1024
Window height            = 768
Window title             = Direct3D App
Back-buffer count        = 3
Target FPS               = 60
Target UPS               = 60
Tearing                  = disabled
VSync                    = disabled
GUI rendering            = disabled
Debug layer              = enabled
GPU validation           = disabled
GPU profiling            = enabled
Render mode              = Ray Tracing
Loaded scene file        = Sponza/Sponza.gltf
Using testing scene      = disabled
Duplication              = 1
Duplication in cube      = enabled
Duplication offset       = 10
BLAS duplication         = enabled
Rays per pixel           = 10

ProfilingBuildAggregator: 
BuildBottomLevelAS	,	6.4151
BuildBottomLevelASGpu	,	23.6295
BuildTopLevelAS	,	1.7182
BuildTopLevelASGpu	,	0.642976

ProfilingAggregator: 
Render	,	7.2464	,	9.2701	,	9.3381	,	17.6973	,	10.5257	,	9.7503	,	9.3108	,	14.5562	,	10.1499	,	14.9922	,	13.1272	,	13.6612	,	10.5691	,	18.3907	,	10.9092	,	18.161	,	9.588	,	14.5474	,	15.6894	,	10.2746	,	11.9491	,	12.4985	,	9.5504	,	10.4287	,	13.0708	,	9.4451
Update	,	0.3023	,	0.3361	,	0.2929	,	0.8653	,	0.3051	,	0.3346	,	0.286	,	0.8365	,	0.8408	,	0.7094	,	0.5465	,	0.6697	,	0.3354	,	0.856	,	0.3457	,	0.8616	,	0.2876	,	0.664	,	0.7058	,	0.3081	,	0.3908	,	0.4769	,	0.3046	,	0.8577	,	0.4292	,	0.2811
RayTracing	,	2.87056	,	4.9503	,	4.884	,	5.18643	,	6.04733	,	5.39539	,	4.87606	,	4.69146	,	5.75434	,	6.08723	,	5.36733	,	5.2665	,	6.28723	,	6.01491	,	6.00768	,	5.42195	,	5.34435	,	5.13498	,	5.39347	,	5.83949	,	6.7391	,	5.18192	,	5.22451	,	5.96413	,	5.51808	,	5.34998
RayTracingRL	,	1.30432	,	2.13533	,	2.38349	,	2.57453	,	2.9919	,	2.452	,	2.43254	,	2.30822	,	3.15514	,	3.07082	,	2.43642	,	2.64307	,	3.16102	,	2.9599	,	2.90634	,	2.65069	,	2.62675	,	2.5561	,	2.67123	,	2.74141	,	3.13661	,	2.32342	,	2.42477	,	2.88128	,	2.45094	,	2.64266

FpsAggregator: 
FPS	,	56	,	60	,	59	,	59	,	60	,	59	,	59	,	60	,	60	,	59	,	60	,	59	,	60	,	59	,	59	,	59	,	59	,	59	,	59	,	58	,	59	,	59	,	59	,	59	,	60	,	58
UPS	,	59	,	60	,	61	,	60	,	61	,	60	,	61	,	60	,	60	,	61	,	60	,	60	,	61	,	61	,	60	,	61	,	61	,	60	,	60	,	60	,	60	,	60	,	60	,	61	,	60	,	61
FrameTime	,	17.8572	,	16.7517	,	16.9492	,	17.178	,	16.8263	,	17.1126	,	16.9493	,	16.8541	,	16.7492	,	16.9492	,	16.8848	,	17.1068	,	16.806	,	17.2562	,	17.0679	,	17.105	,	16.9492	,	17.1711	,	16.9643	,	17.2415	,	16.9755	,	17.0169	,	16.9955	,	16.9492	,	16.8601	,	17.2415
GigaRays/s	,	0.4404	,	0.469463	,	0.463993	,	0.457815	,	0.467382	,	0.459564	,	0.463991	,	0.466612	,	0.469533	,	0.463995	,	0.465764	,	0.45972	,	0.467947	,	0.455738	,	0.460765	,	0.459768	,	0.463994	,	0.457998	,	0.46358	,	0.456128	,	0.463276	,	0.462147	,	0.46273	,	0.463995	,	0.466445	,	0.456126

AccelerationStructure: 
Ray tracing acceleration structure building statistics: 
	Bottom Level Acceleration Structure: 
		Maximum [B]: 18391808
		Minimum [B]: 18391808
		Currently Allocated [B]: 18391808
		Currently Created [num]: 1
		Current Average [B]: 18391808
		Maximum Triangles [num]: 262267
		Minimum Triangles [num]: 262267
		Total Triangles [num]: 262267
		Maximum Meshes [num]: 103
		Minimum Meshes [num]: 103
		Total Meshes [num]: 103
	Top Level Acceleration Structure: 
		Maximum [B]: 64
		Minimum [B]: 64
		Currently Allocated [B]: 64
		Total Allocated [B]: 64
		Total Created [num]: 1
		Average Allocated [B]: 64
		Maximum Instances [num]: 1
		Minimum Instances [num]: 1
		Total Instances [num]: 1
	Scratch Buffer: 
		Maximum [B]: 14998528
		Minimum [B]: 14998528
		Currently Allocated [B]: 14998528
		Total Allocated [B]: 14998528
		Total Created [num]: 1
		Average Allocated [B]: 14998528
ProfilingStack: 
[PROF] Profiling statistics: 
	Scopes entered : 1608204
	Scopes exited : 1608203
	Overhead per scope [ticks] : 1019.4
	Threads entered : 3
	Threads exited : 0
	============================
		         Sum[Mt]   Parent[%]  Samples[num]       Avg[Mt]   Last/Curr[Mt]        Self[Mt]     Self[%] 	 Name

	main:

		   37063.721 Mt   100.000 %         1 x    37063.725 Mt    37063.720 Mt      565.952 Mt     1.527 % 	 /

		   36497.893 Mt    98.473 %         1 x    36497.898 Mt    36497.893 Mt     4898.714 Mt    13.422 % 	 ./Main application loop

		       3.382 Mt     0.009 %         1 x        3.382 Mt        0.000 Mt        3.382 Mt   100.000 % 	 ../TexturedRLInitialization

		       7.950 Mt     0.022 %         1 x        7.950 Mt        0.000 Mt        7.950 Mt   100.000 % 	 ../DeferredRLInitialization

		      58.375 Mt     0.160 %         1 x       58.375 Mt        0.000 Mt        1.760 Mt     3.016 % 	 ../RayTracingRLInitialization

		      56.615 Mt    96.984 %         1 x       56.615 Mt        0.000 Mt       56.615 Mt   100.000 % 	 .../PipelineBuild

		      11.138 Mt     0.031 %         1 x       11.138 Mt        0.000 Mt       11.138 Mt   100.000 % 	 ../ResolveRLInitialization

		    9313.949 Mt    25.519 %         1 x     9313.949 Mt        0.000 Mt        7.545 Mt     0.081 % 	 ../Initialization

		    9306.404 Mt    99.919 %         1 x     9306.404 Mt        0.000 Mt        2.318 Mt     0.025 % 	 .../ScenePrep

		    9181.394 Mt    98.657 %         1 x     9181.394 Mt        0.000 Mt      285.138 Mt     3.106 % 	 ..../SceneLoad

		    8564.868 Mt    93.285 %         1 x     8564.868 Mt        0.000 Mt     1176.164 Mt    13.732 % 	 ...../glTF-LoadScene

		    7388.703 Mt    86.268 %        69 x      107.083 Mt        0.000 Mt     7388.703 Mt   100.000 % 	 ....../glTF-LoadImageData

		     245.488 Mt     2.674 %         1 x      245.488 Mt        0.000 Mt      245.488 Mt   100.000 % 	 ...../glTF-CreateScene

		      85.900 Mt     0.936 %         1 x       85.900 Mt        0.000 Mt       85.900 Mt   100.000 % 	 ...../SceneDesc-CreateScene

		      10.552 Mt     0.113 %         1 x       10.552 Mt        0.000 Mt        0.009 Mt     0.086 % 	 ..../TexturedRLPrep

		      10.543 Mt    99.914 %         1 x       10.543 Mt        0.000 Mt        2.298 Mt    21.796 % 	 ...../PrepareForRendering

		       4.157 Mt    39.428 %         1 x        4.157 Mt        0.000 Mt        4.157 Mt   100.000 % 	 ....../PrepareMaterials

		       4.088 Mt    38.776 %         1 x        4.088 Mt        0.000 Mt        4.088 Mt   100.000 % 	 ....../PrepareDrawBundle

		      10.966 Mt     0.118 %         1 x       10.966 Mt        0.000 Mt        0.008 Mt     0.077 % 	 ..../DeferredRLPrep

		      10.957 Mt    99.923 %         1 x       10.957 Mt        0.000 Mt        4.800 Mt    43.807 % 	 ...../PrepareForRendering

		       0.042 Mt     0.380 %         1 x        0.042 Mt        0.000 Mt        0.042 Mt   100.000 % 	 ....../PrepareMaterials

		       3.501 Mt    31.953 %         1 x        3.501 Mt        0.000 Mt        3.501 Mt   100.000 % 	 ....../BuildMaterialCache

		       2.615 Mt    23.860 %         1 x        2.615 Mt        0.000 Mt        2.615 Mt   100.000 % 	 ....../PrepareDrawBundle

		      30.542 Mt     0.328 %         1 x       30.542 Mt        0.000 Mt        6.532 Mt    21.388 % 	 ..../RayTracingRLPrep

		       0.705 Mt     2.307 %         1 x        0.705 Mt        0.000 Mt        0.705 Mt   100.000 % 	 ...../PrepareAccelerationStructures

		       0.035 Mt     0.114 %         1 x        0.035 Mt        0.000 Mt        0.035 Mt   100.000 % 	 ...../PrepareCache

		       8.837 Mt    28.934 %         1 x        8.837 Mt        0.000 Mt        8.837 Mt   100.000 % 	 ...../BuildCache

		      11.410 Mt    37.359 %         1 x       11.410 Mt        0.000 Mt        0.023 Mt     0.200 % 	 ...../BuildAccelerationStructures

		      11.387 Mt    99.800 %         1 x       11.387 Mt        0.000 Mt        3.254 Mt    28.577 % 	 ....../AccelerationStructureBuild

		       6.415 Mt    56.335 %         1 x        6.415 Mt        0.000 Mt        6.415 Mt   100.000 % 	 ......./BuildBottomLevelAS

		       1.718 Mt    15.088 %         1 x        1.718 Mt        0.000 Mt        1.718 Mt   100.000 % 	 ......./BuildTopLevelAS

		       3.023 Mt     9.898 %         1 x        3.023 Mt        0.000 Mt        3.023 Mt   100.000 % 	 ...../BuildShaderTables

		      70.633 Mt     0.759 %         1 x       70.633 Mt        0.000 Mt       70.633 Mt   100.000 % 	 ..../GpuSidePrep

		   22204.385 Mt    60.837 %   1594234 x        0.014 Mt        0.735 Mt     3279.745 Mt    14.771 % 	 ../MainLoop

		     811.276 Mt     3.654 %      1570 x        0.517 Mt        0.721 Mt      811.276 Mt   100.000 % 	 .../Update

		   18113.364 Mt    81.576 %      1537 x       11.785 Mt       10.450 Mt    18096.549 Mt    99.907 % 	 .../Render

		       8.368 Mt     0.046 %      1537 x        0.005 Mt        0.006 Mt        8.368 Mt   100.000 % 	 ..../DeferredRLPrep

		       8.447 Mt     0.047 %      1537 x        0.005 Mt        0.003 Mt        8.447 Mt   100.000 % 	 ..../RayTracingRLPrep

	WindowThread:

		   36487.595 Mt   100.000 %         1 x    36487.596 Mt    36487.594 Mt    36487.595 Mt   100.000 % 	 /

	GpuThread:

		    8366.659 Mt   100.000 %      1537 x        5.443 Mt        5.900 Mt      159.005 Mt     1.900 % 	 /

		      24.326 Mt     0.291 %         1 x       24.326 Mt        0.000 Mt        0.051 Mt     0.208 % 	 ./ScenePrep

		      24.275 Mt    99.792 %         1 x       24.275 Mt        0.000 Mt        0.003 Mt     0.011 % 	 ../AccelerationStructureBuild

		      23.630 Mt    97.341 %         1 x       23.630 Mt        0.000 Mt       23.630 Mt   100.000 % 	 .../BuildBottomLevelASGpu

		       0.643 Mt     2.649 %         1 x        0.643 Mt        0.000 Mt        0.643 Mt   100.000 % 	 .../BuildTopLevelASGpu

		    8181.788 Mt    97.790 %      1537 x        5.323 Mt        5.898 Mt        3.826 Mt     0.047 % 	 ./RayTracing

		    1009.997 Mt    12.344 %      1537 x        0.657 Mt        0.635 Mt     1009.997 Mt   100.000 % 	 ../DeferredRL

		    3957.819 Mt    48.374 %      1537 x        2.575 Mt        2.637 Mt     3957.819 Mt   100.000 % 	 ../RayTracingRL

		    3210.147 Mt    39.235 %      1537 x        2.089 Mt        2.623 Mt     3210.147 Mt   100.000 % 	 ../ResolveRL

		       1.540 Mt     0.018 %      1537 x        0.001 Mt        0.001 Mt        1.540 Mt   100.000 % 	 ./Present


	============================


