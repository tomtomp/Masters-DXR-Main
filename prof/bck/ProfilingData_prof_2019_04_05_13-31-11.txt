Configuration: 
Command line parameters  = E:\programming\FIT-DIP\Masters-DXR-Main-git\\build\Measurement\x64\Debug\Measurement.exe --scene Cube/Cube.gltf --rt-mode --width 1024 --height 768 
Window width             = 1024
Window height            = 768
Window title             = Direct3D App
Back-buffer count        = 3
Target FPS               = 60
Target UPS               = 60
Tearing                  = disabled
VSync                    = disabled
GUI rendering            = enabled
Debug layer              = enabled
GPU validation           = disabled
GPU profiling            = enabled
Render mode              = Ray Tracing
Loaded scene file        = Cube/Cube.gltf
Using testing scene      = disabled
Duplication              = 1
Duplication in cube      = disabled
Duplication offset       = 10
BLAS duplication         = disabled
Rays per pixel           = 10

ProfilingBuildAggregator: 
BuildBottomLevelAS	,	0.9963
BuildBottomLevelASGpu	,	0.178048
BuildTopLevelAS	,	0.5953
BuildTopLevelASGpu	,	0.084672

ProfilingAggregator: 
Render	,	4.3352	,	6.9194
Update	,	0.287	,	0.2615
RayTracing	,	0.921344	,	3.63555
RayTracingRL	,	0.098368	,	0.554624

FpsAggregator: 
FPS	,	57	,	60
UPS	,	59	,	60
FrameTime	,	17.5628	,	16.7118
GigaRays/s	,	0.447782	,	0.470584

AccelerationStructure: 
Ray tracing acceleration structure building statistics: 
	Bottom Level Acceleration Structure: 
		Maximum [B]: 6208
		Minimum [B]: 6208
		Currently Allocated [B]: 6208
		Currently Created [num]: 1
		Current Average [B]: 6208
		Maximum Triangles [num]: 12
		Minimum Triangles [num]: 12
		Total Triangles [num]: 12
		Maximum Meshes [num]: 1
		Minimum Meshes [num]: 1
		Total Meshes [num]: 1
	Top Level Acceleration Structure: 
		Maximum [B]: 64
		Minimum [B]: 64
		Currently Allocated [B]: 64
		Total Allocated [B]: 64
		Total Created [num]: 1
		Average Allocated [B]: 64
		Maximum Instances [num]: 1
		Minimum Instances [num]: 1
		Total Instances [num]: 1
	Scratch Buffer: 
		Maximum [B]: 3584
		Minimum [B]: 3584
		Currently Allocated [B]: 3584
		Total Allocated [B]: 3584
		Total Created [num]: 1
		Average Allocated [B]: 3584
ProfilingStack: 
[PROF] Profiling statistics: 
	Scopes entered : 433363
	Scopes exited : 433362
	Overhead per scope [ticks] : 1019.2
	Threads entered : 3
	Threads exited : 0
	============================
		         Sum[Mt]   Parent[%]  Samples[num]       Avg[Mt]   Last/Curr[Mt]        Self[Mt]     Self[%] 	 Name

	main:

		    3567.562 Mt   100.000 %         1 x     3567.564 Mt     3567.561 Mt      362.293 Mt    10.155 % 	 /

		    3205.312 Mt    89.846 %         1 x     3205.313 Mt     3205.311 Mt     1463.260 Mt    45.651 % 	 ./Main application loop

		       2.615 Mt     0.082 %         1 x        2.615 Mt        0.000 Mt        2.615 Mt   100.000 % 	 ../TexturedRLInitialization

		       7.944 Mt     0.248 %         1 x        7.944 Mt        0.000 Mt        7.944 Mt   100.000 % 	 ../DeferredRLInitialization

		      60.273 Mt     1.880 %         1 x       60.273 Mt        0.000 Mt        1.601 Mt     2.657 % 	 ../RayTracingRLInitialization

		      58.671 Mt    97.343 %         1 x       58.671 Mt        0.000 Mt       58.671 Mt   100.000 % 	 .../PipelineBuild

		      10.519 Mt     0.328 %         1 x       10.519 Mt        0.000 Mt       10.519 Mt   100.000 % 	 ../ResolveRLInitialization

		      88.361 Mt     2.757 %         1 x       88.361 Mt        0.000 Mt        2.419 Mt     2.738 % 	 ../Initialization

		      85.942 Mt    97.262 %         1 x       85.942 Mt        0.000 Mt        0.825 Mt     0.960 % 	 .../ScenePrep

		      68.995 Mt    80.281 %         1 x       68.995 Mt        0.000 Mt        6.679 Mt     9.681 % 	 ..../SceneLoad

		      57.792 Mt    83.763 %         1 x       57.792 Mt        0.000 Mt       12.991 Mt    22.478 % 	 ...../glTF-LoadScene

		      44.801 Mt    77.522 %         2 x       22.401 Mt        0.000 Mt       44.801 Mt   100.000 % 	 ....../glTF-LoadImageData

		       2.520 Mt     3.652 %         1 x        2.520 Mt        0.000 Mt        2.520 Mt   100.000 % 	 ...../glTF-CreateScene

		       2.004 Mt     2.904 %         1 x        2.004 Mt        0.000 Mt        2.004 Mt   100.000 % 	 ...../SceneDesc-CreateScene

		       0.804 Mt     0.936 %         1 x        0.804 Mt        0.000 Mt        0.005 Mt     0.622 % 	 ..../TexturedRLPrep

		       0.799 Mt    99.378 %         1 x        0.799 Mt        0.000 Mt        0.239 Mt    29.947 % 	 ...../PrepareForRendering

		       0.251 Mt    31.436 %         1 x        0.251 Mt        0.000 Mt        0.251 Mt   100.000 % 	 ....../PrepareMaterials

		       0.309 Mt    38.616 %         1 x        0.309 Mt        0.000 Mt        0.309 Mt   100.000 % 	 ....../PrepareDrawBundle

		       1.573 Mt     1.831 %         1 x        1.573 Mt        0.000 Mt        0.004 Mt     0.273 % 	 ..../DeferredRLPrep

		       1.569 Mt    99.727 %         1 x        1.569 Mt        0.000 Mt        1.088 Mt    69.331 % 	 ...../PrepareForRendering

		       0.016 Mt     1.013 %         1 x        0.016 Mt        0.000 Mt        0.016 Mt   100.000 % 	 ....../PrepareMaterials

		       0.190 Mt    12.103 %         1 x        0.190 Mt        0.000 Mt        0.190 Mt   100.000 % 	 ....../BuildMaterialCache

		       0.275 Mt    17.553 %         1 x        0.275 Mt        0.000 Mt        0.275 Mt   100.000 % 	 ....../PrepareDrawBundle

		       5.974 Mt     6.952 %         1 x        5.974 Mt        0.000 Mt        2.371 Mt    39.690 % 	 ..../RayTracingRLPrep

		       0.330 Mt     5.523 %         1 x        0.330 Mt        0.000 Mt        0.330 Mt   100.000 % 	 ...../PrepareAccelerationStructures

		       0.028 Mt     0.462 %         1 x        0.028 Mt        0.000 Mt        0.028 Mt   100.000 % 	 ...../PrepareCache

		       0.256 Mt     4.287 %         1 x        0.256 Mt        0.000 Mt        0.256 Mt   100.000 % 	 ...../BuildCache

		       2.351 Mt    39.351 %         1 x        2.351 Mt        0.000 Mt        0.008 Mt     0.340 % 	 ...../BuildAccelerationStructures

		       2.343 Mt    99.660 %         1 x        2.343 Mt        0.000 Mt        0.751 Mt    32.070 % 	 ....../AccelerationStructureBuild

		       0.996 Mt    42.522 %         1 x        0.996 Mt        0.000 Mt        0.996 Mt   100.000 % 	 ......./BuildBottomLevelAS

		       0.595 Mt    25.408 %         1 x        0.595 Mt        0.000 Mt        0.595 Mt   100.000 % 	 ......./BuildTopLevelAS

		       0.638 Mt    10.687 %         1 x        0.638 Mt        0.000 Mt        0.638 Mt   100.000 % 	 ...../BuildShaderTables

		       7.769 Mt     9.040 %         1 x        7.769 Mt        0.000 Mt        7.769 Mt   100.000 % 	 ..../GpuSidePrep

		    1572.340 Mt    49.054 %    431739 x        0.004 Mt        7.636 Mt      718.009 Mt    45.665 % 	 ../MainLoop

		     112.418 Mt     7.150 %       147 x        0.765 Mt        7.629 Mt      112.418 Mt   100.000 % 	 .../Update

		     741.914 Mt    47.185 %       144 x        5.152 Mt        7.293 Mt      665.297 Mt    89.673 % 	 .../Render

		       0.627 Mt     0.085 %       144 x        0.004 Mt        0.003 Mt        0.627 Mt   100.000 % 	 ..../DeferredRLPrep

		       0.458 Mt     0.062 %       144 x        0.003 Mt        0.003 Mt        0.458 Mt   100.000 % 	 ..../RayTracingRLPrep

		      75.531 Mt    10.181 %       144 x        0.525 Mt        0.476 Mt       75.531 Mt   100.000 % 	 ..../GuiModelApply

	WindowThread:

		    3194.206 Mt   100.000 %         1 x     3194.206 Mt     3194.206 Mt     3194.206 Mt   100.000 % 	 /

	GpuThread:

		     319.406 Mt   100.000 %       144 x        2.218 Mt        3.738 Mt      103.212 Mt    32.314 % 	 /

		       0.272 Mt     0.085 %         1 x        0.272 Mt        0.000 Mt        0.008 Mt     2.789 % 	 ./ScenePrep

		       0.264 Mt    97.211 %         1 x        0.264 Mt        0.000 Mt        0.002 Mt     0.617 % 	 ../AccelerationStructureBuild

		       0.178 Mt    67.353 %         1 x        0.178 Mt        0.000 Mt        0.178 Mt   100.000 % 	 .../BuildBottomLevelASGpu

		       0.085 Mt    32.030 %         1 x        0.085 Mt        0.000 Mt        0.085 Mt   100.000 % 	 .../BuildTopLevelASGpu

		     213.387 Mt    66.807 %       144 x        1.482 Mt        3.700 Mt        0.369 Mt     0.173 % 	 ./RayTracing

		       5.759 Mt     2.699 %       144 x        0.040 Mt        0.096 Mt        5.759 Mt   100.000 % 	 ../DeferredRL

		      28.431 Mt    13.324 %       144 x        0.197 Mt        0.427 Mt       28.431 Mt   100.000 % 	 ../RayTracingRL

		     178.827 Mt    83.804 %       144 x        1.242 Mt        3.172 Mt      178.827 Mt   100.000 % 	 ../ResolveRL

		       2.193 Mt     0.687 %       144 x        0.015 Mt        0.032 Mt        2.193 Mt   100.000 % 	 ./ImGui

		       0.342 Mt     0.107 %       144 x        0.002 Mt        0.004 Mt        0.342 Mt   100.000 % 	 ./Present


	============================


