Configuration: 
Command line parameters  = E:\programming\FIT-DIP\Masters-DXR-Main-git\build\Measurement\x64\Debug\Measurement.exe --scene Cube/Cube.gltf --duplication-cube --profile-duplication 10 --rt-mode --width 1024 --height 768 --profile-output DuplicationCube768Rt 
Window width             = 1024
Window height            = 768
Window title             = Direct3D App
Back-buffer count        = 3
Target FPS               = 60
Target UPS               = 60
Tearing                  = disabled
VSync                    = disabled
GUI rendering            = disabled
Debug layer              = enabled
GPU validation           = disabled
GPU profiling            = enabled
Render mode              = Ray Tracing
Loaded scene file        = Cube/Cube.gltf
Using testing scene      = disabled
Duplication              = 10
Duplication in cube      = enabled
Duplication offset       = 10
BLAS duplication         = enabled
Rays per pixel           = 10

ProfilingBuildAggregator: 
BuildBottomLevelAS	,	3.3736	,	40.9229	,	111.628	,	189.98	,	349.959	,	510.301	,	833.591	,	974.689	,	1411.57
BuildBottomLevelASGpu	,	0.179648	,	4.17075	,	38.778	,	67.0947	,	90.6287	,	114.925	,	240.352	,	114.621	,	289.838
BuildTopLevelAS	,	1.5677	,	2.6403	,	2.8976	,	0.9498	,	2.2618	,	1.5344	,	3.5985	,	1.4077	,	1.0944
BuildTopLevelASGpu	,	0.086912	,	0.129312	,	0.39664	,	0.659232	,	0.45376	,	0.397696	,	0.205376	,	0.256256	,	0.29008

ProfilingAggregator: 
Render	,	7.521	,	7.7482	,	5.7479	,	8.9249	,	9.6834	,	6.7111	,	7.2983	,	1264.26	,	1342.15	,	2020.98
Update	,	0.7692	,	0.7881	,	0.2915	,	0.3206	,	0.7216	,	0.4809	,	0.6211	,	0.7338	,	0.9194	,	1.2027
RayTracing	,	0.916992	,	0.926336	,	2.86147	,	5.60918	,	5.44058	,	2.59651	,	2.61862	,	242.579	,	115.957	,	291.433
RayTracingRL	,	0.094944	,	0.109312	,	0.563712	,	2.23088	,	2.4424	,	0.668352	,	0.692768	,	0.51936	,	0.198016	,	0.21136

FpsAggregator: 
FPS	,	56	,	54	,	52	,	44	,	35	,	18	,	3	,	1	,	1	,	1
UPS	,	59	,	60	,	61	,	60	,	60	,	60	,	61	,	3	,	87	,	103
FrameTime	,	17.8739	,	18.6132	,	19.3385	,	22.7365	,	28.715	,	55.6807	,	333.334	,	1497.79	,	1718.62	,	2563.16
GigaRays/s	,	0.439988	,	0.422513	,	0.406667	,	0.345889	,	0.273875	,	0.14124	,	0.0235929	,	0.00525063	,	0.00457596	,	0.00306821

AccelerationStructure: 
Ray tracing acceleration structure building statistics: 
	Bottom Level Acceleration Structure: 
		Maximum [B]: 6208
		Minimum [B]: 6208
		Currently Allocated [B]: 6208000
		Currently Created [num]: 1000
		Current Average [B]: 6208
		Maximum Triangles [num]: 12
		Minimum Triangles [num]: 12
		Total Triangles [num]: 12000
		Maximum Meshes [num]: 1
		Minimum Meshes [num]: 1
		Total Meshes [num]: 1000
	Top Level Acceleration Structure: 
		Maximum [B]: 64000
		Minimum [B]: 64000
		Currently Allocated [B]: 64000
		Total Allocated [B]: 64000
		Total Created [num]: 1
		Average Allocated [B]: 64000
		Maximum Instances [num]: 1000
		Minimum Instances [num]: 1000
		Total Instances [num]: 1000
	Scratch Buffer: 
		Maximum [B]: 79360
		Minimum [B]: 79360
		Currently Allocated [B]: 79360
		Total Allocated [B]: 79360
		Total Created [num]: 1
		Average Allocated [B]: 79360
ProfilingStack: 
[PROF] Profiling statistics: 
	Scopes entered : 672459
	Scopes exited : 672458
	Overhead per scope [ticks] : 1019.9
	Threads entered : 3
	Threads exited : 0
	============================
		         Sum[Mt]   Parent[%]  Samples[num]       Avg[Mt]   Last/Curr[Mt]        Self[Mt]     Self[%] 	 Name

	main:

		   13774.079 Mt   100.000 %         1 x    13774.081 Mt    13774.078 Mt        0.620 Mt     0.005 % 	 /

		   13773.503 Mt    99.996 %         1 x    13773.505 Mt    13773.503 Mt     1910.457 Mt    13.871 % 	 ./Main application loop

		       2.867 Mt     0.021 %         1 x        2.867 Mt        0.000 Mt        2.867 Mt   100.000 % 	 ../TexturedRLInitialization

		       5.965 Mt     0.043 %         1 x        5.965 Mt        0.000 Mt        5.965 Mt   100.000 % 	 ../DeferredRLInitialization

		      67.945 Mt     0.493 %         1 x       67.945 Mt        0.000 Mt        2.063 Mt     3.037 % 	 ../RayTracingRLInitialization

		      65.882 Mt    96.963 %         1 x       65.882 Mt        0.000 Mt       65.882 Mt   100.000 % 	 .../PipelineBuild

		      10.845 Mt     0.079 %         1 x       10.845 Mt        0.000 Mt       10.845 Mt   100.000 % 	 ../ResolveRLInitialization

		     162.818 Mt     1.182 %         1 x      162.818 Mt        0.000 Mt        8.411 Mt     5.166 % 	 ../Initialization

		     154.407 Mt    94.834 %         1 x      154.407 Mt        0.000 Mt        2.067 Mt     1.338 % 	 .../ScenePrep

		     104.277 Mt    67.534 %         1 x      104.277 Mt        0.000 Mt       16.991 Mt    16.294 % 	 ..../SceneLoad

		      58.569 Mt    56.166 %         1 x       58.569 Mt        0.000 Mt       13.866 Mt    23.675 % 	 ...../glTF-LoadScene

		      44.702 Mt    76.325 %         2 x       22.351 Mt        0.000 Mt       44.702 Mt   100.000 % 	 ....../glTF-LoadImageData

		      19.266 Mt    18.476 %         1 x       19.266 Mt        0.000 Mt       19.266 Mt   100.000 % 	 ...../glTF-CreateScene

		       9.451 Mt     9.063 %         1 x        9.451 Mt        0.000 Mt        9.451 Mt   100.000 % 	 ...../SceneDesc-CreateScene

		       6.725 Mt     4.356 %         1 x        6.725 Mt        0.000 Mt        0.006 Mt     0.088 % 	 ..../TexturedRLPrep

		       6.720 Mt    99.912 %         1 x        6.720 Mt        0.000 Mt        2.702 Mt    40.217 % 	 ...../PrepareForRendering

		       3.059 Mt    45.529 %         1 x        3.059 Mt        0.000 Mt        3.059 Mt   100.000 % 	 ....../PrepareMaterials

		       0.958 Mt    14.254 %         1 x        0.958 Mt        0.000 Mt        0.958 Mt   100.000 % 	 ....../PrepareDrawBundle

		       6.655 Mt     4.310 %         1 x        6.655 Mt        0.000 Mt        0.014 Mt     0.206 % 	 ..../DeferredRLPrep

		       6.641 Mt    99.794 %         1 x        6.641 Mt        0.000 Mt        4.827 Mt    72.684 % 	 ...../PrepareForRendering

		       0.053 Mt     0.795 %         1 x        0.053 Mt        0.000 Mt        0.053 Mt   100.000 % 	 ....../PrepareMaterials

		       1.478 Mt    22.256 %         1 x        1.478 Mt        0.000 Mt        1.478 Mt   100.000 % 	 ....../BuildMaterialCache

		       0.283 Mt     4.266 %         1 x        0.283 Mt        0.000 Mt        0.283 Mt   100.000 % 	 ....../PrepareDrawBundle

		      22.463 Mt    14.548 %         1 x       22.463 Mt        0.000 Mt        5.003 Mt    22.272 % 	 ..../RayTracingRLPrep

		       0.253 Mt     1.128 %         1 x        0.253 Mt        0.000 Mt        0.253 Mt   100.000 % 	 ...../PrepareAccelerationStructures

		       0.027 Mt     0.119 %         1 x        0.027 Mt        0.000 Mt        0.027 Mt   100.000 % 	 ...../PrepareCache

		       3.585 Mt    15.962 %         1 x        3.585 Mt        0.000 Mt        3.585 Mt   100.000 % 	 ...../BuildCache

		      11.082 Mt    49.333 %         1 x       11.082 Mt        0.000 Mt        0.021 Mt     0.192 % 	 ...../BuildAccelerationStructures

		      11.060 Mt    99.808 %         1 x       11.060 Mt        0.000 Mt        6.119 Mt    55.324 % 	 ....../AccelerationStructureBuild

		       3.374 Mt    30.502 %         1 x        3.374 Mt        0.000 Mt        3.374 Mt   100.000 % 	 ......./BuildBottomLevelAS

		       1.568 Mt    14.174 %         1 x        1.568 Mt        0.000 Mt        1.568 Mt   100.000 % 	 ......./BuildTopLevelAS

		       2.513 Mt    11.186 %         1 x        2.513 Mt        0.000 Mt        2.513 Mt   100.000 % 	 ...../BuildShaderTables

		      12.220 Mt     7.914 %         1 x       12.220 Mt        0.000 Mt       12.220 Mt   100.000 % 	 ..../GpuSidePrep

		   11612.604 Mt    84.311 %    669392 x        0.017 Mt      233.364 Mt     1681.081 Mt    14.476 % 	 ../MainLoop

		    1772.398 Mt    15.263 %       767 x        2.311 Mt        1.195 Mt     1772.398 Mt   100.000 % 	 .../Update

		    8159.125 Mt    70.261 %       266 x       30.673 Mt       11.942 Mt     2585.214 Mt    31.685 % 	 .../Render

		      85.168 Mt     1.044 %       266 x        0.320 Mt        0.004 Mt        0.899 Mt     1.056 % 	 ..../DeferredRLPrep

		      84.268 Mt    98.944 %         9 x        9.363 Mt        0.000 Mt        6.651 Mt     7.893 % 	 ...../PrepareForRendering

		       8.272 Mt     9.816 %         9 x        0.919 Mt        0.000 Mt        8.272 Mt   100.000 % 	 ....../PrepareMaterials

		       0.009 Mt     0.010 %         9 x        0.001 Mt        0.000 Mt        0.009 Mt   100.000 % 	 ....../BuildMaterialCache

		      69.337 Mt    82.281 %         9 x        7.704 Mt        0.000 Mt       69.337 Mt   100.000 % 	 ....../PrepareDrawBundle

		    5488.744 Mt    67.271 %       266 x       20.634 Mt        0.004 Mt       53.899 Mt     0.982 % 	 ..../RayTracingRLPrep

		     782.920 Mt    14.264 %         9 x       86.991 Mt        0.000 Mt      782.920 Mt   100.000 % 	 ...../PrepareAccelerationStructures

		       8.017 Mt     0.146 %         9 x        0.891 Mt        0.000 Mt        8.017 Mt   100.000 % 	 ...../PrepareCache

		       0.007 Mt     0.000 %         9 x        0.001 Mt        0.000 Mt        0.007 Mt   100.000 % 	 ...../BuildCache

		    4528.207 Mt    82.500 %         9 x      503.134 Mt        0.000 Mt        0.032 Mt     0.001 % 	 ...../BuildAccelerationStructures

		    4528.176 Mt    99.999 %         9 x      503.131 Mt        0.000 Mt       74.277 Mt     1.640 % 	 ....../AccelerationStructureBuild

		    4435.824 Mt    97.961 %         9 x      492.869 Mt        0.000 Mt     4435.824 Mt   100.000 % 	 ......./BuildBottomLevelAS

		      18.075 Mt     0.399 %         9 x        2.008 Mt        0.000 Mt       18.075 Mt   100.000 % 	 ......./BuildTopLevelAS

		     115.694 Mt     2.108 %         9 x       12.855 Mt        0.000 Mt      115.694 Mt   100.000 % 	 ...../BuildShaderTables

	WindowThread:

		   13770.582 Mt   100.000 %         1 x    13770.582 Mt    13770.581 Mt    13770.582 Mt   100.000 % 	 /

	GpuThread:

		    1783.018 Mt   100.000 %       266 x        6.703 Mt        0.966 Mt      135.215 Mt     7.583 % 	 /

		       0.274 Mt     0.015 %         1 x        0.274 Mt        0.000 Mt        0.006 Mt     2.276 % 	 ./ScenePrep

		       0.268 Mt    97.724 %         1 x        0.268 Mt        0.000 Mt        0.001 Mt     0.525 % 	 ../AccelerationStructureBuild

		       0.180 Mt    67.041 %         1 x        0.180 Mt        0.000 Mt        0.180 Mt   100.000 % 	 .../BuildBottomLevelASGpu

		       0.087 Mt    32.434 %         1 x        0.087 Mt        0.000 Mt        0.087 Mt   100.000 % 	 .../BuildTopLevelASGpu

		    1647.082 Mt    92.376 %       266 x        6.192 Mt        0.965 Mt        2.800 Mt     0.170 % 	 ./RayTracing

		      36.840 Mt     2.237 %       266 x        0.138 Mt        0.043 Mt       36.840 Mt   100.000 % 	 ../DeferredRL

		     182.782 Mt    11.097 %       266 x        0.687 Mt        0.193 Mt      182.782 Mt   100.000 % 	 ../RayTracingRL

		     460.095 Mt    27.934 %       266 x        1.730 Mt        0.726 Mt      460.095 Mt   100.000 % 	 ../ResolveRL

		     964.565 Mt    58.562 %         9 x      107.174 Mt        0.000 Mt        0.022 Mt     0.002 % 	 ../AccelerationStructureBuild

		     961.661 Mt    99.699 %         9 x      106.851 Mt        0.000 Mt      961.661 Mt   100.000 % 	 .../BuildBottomLevelASGpu

		       2.882 Mt     0.299 %         9 x        0.320 Mt        0.000 Mt        2.882 Mt   100.000 % 	 .../BuildTopLevelASGpu

		       0.448 Mt     0.025 %       266 x        0.002 Mt        0.001 Mt        0.448 Mt   100.000 % 	 ./Present


	============================


