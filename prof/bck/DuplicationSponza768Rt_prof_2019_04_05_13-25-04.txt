Configuration: 
Command line parameters  = E:\programming\FIT-DIP\Masters-DXR-Main-git\build\Measurement\x64\Debug\Measurement.exe --scene Sponza/Sponza.gltf --duplication-cube --profile-duplication 7 --rt-mode --width 1024 --height 768 --profile-output DuplicationSponza768Rt 
Window width             = 1024
Window height            = 768
Window title             = Direct3D App
Back-buffer count        = 3
Target FPS               = 60
Target UPS               = 60
Tearing                  = disabled
VSync                    = disabled
GUI rendering            = disabled
Debug layer              = enabled
GPU validation           = disabled
GPU profiling            = enabled
Render mode              = Ray Tracing
Loaded scene file        = Sponza/Sponza.gltf
Using testing scene      = disabled
Duplication              = 7
Duplication in cube      = enabled
Duplication offset       = 10
BLAS duplication         = enabled
Rays per pixel           = 10

ProfilingBuildAggregator: 
BuildBottomLevelAS	,	5.9332	,	73.6465	,	200.804	,	334.766	,	585.787	,	979.481
BuildBottomLevelASGpu	,	25.9882	,	97.0837	,	169.61	,	221.547	,	474.514	,	745.003
BuildTopLevelAS	,	2.9948	,	1.1685	,	2.1814	,	1.2361	,	1.7323	,	1.4324
BuildTopLevelASGpu	,	0.645856	,	0.199008	,	0.111808	,	0.139456	,	0.151328	,	0.156576

ProfilingAggregator: 
Render	,	6.8717	,	14.8267	,	30.2767	,	51.5063	,	1294.51	,	2209.51	,	3537.31
Update	,	0.2702	,	0.7958	,	0.3798	,	0.2501	,	0.3251	,	0.4128	,	0.5291
RayTracing	,	2.80013	,	6.04998	,	5.15229	,	3.6535	,	226.693	,	484.845	,	762.505
RayTracingRL	,	1.29859	,	3.12576	,	2.36179	,	1.3607	,	1.67738	,	1.36778	,	1.37536

FpsAggregator: 
FPS	,	56	,	47	,	20	,	4	,	1	,	1	,	1
UPS	,	59	,	60	,	61	,	60	,	3	,	81	,	140
FrameTime	,	17.8572	,	21.4574	,	51.2781	,	254.925	,	1348.54	,	2346.49	,	3767.83
GigaRays/s	,	0.4404	,	0.366509	,	0.153366	,	0.0308496	,	0.00583174	,	0.00335153	,	0.00208723

AccelerationStructure: 
Ray tracing acceleration structure building statistics: 
	Bottom Level Acceleration Structure: 
		Maximum [B]: 18391808
		Minimum [B]: 18391808
		Currently Allocated [B]: 6308390144
		Currently Created [num]: 343
		Current Average [B]: 18391808
		Maximum Triangles [num]: 262267
		Minimum Triangles [num]: 262267
		Total Triangles [num]: 89957581
		Maximum Meshes [num]: 103
		Minimum Meshes [num]: 103
		Total Meshes [num]: 35329
	Top Level Acceleration Structure: 
		Maximum [B]: 21952
		Minimum [B]: 21952
		Currently Allocated [B]: 21952
		Total Allocated [B]: 21952
		Total Created [num]: 1
		Average Allocated [B]: 21952
		Maximum Instances [num]: 343
		Minimum Instances [num]: 343
		Total Instances [num]: 343
	Scratch Buffer: 
		Maximum [B]: 14998528
		Minimum [B]: 14998528
		Currently Allocated [B]: 14998528
		Total Allocated [B]: 14998528
		Total Created [num]: 1
		Average Allocated [B]: 14998528
ProfilingStack: 
[PROF] Profiling statistics: 
	Scopes entered : 145876
	Scopes exited : 145875
	Overhead per scope [ticks] : 1053.2
	Threads entered : 3
	Threads exited : 0
	============================
		         Sum[Mt]   Parent[%]  Samples[num]       Avg[Mt]   Last/Curr[Mt]        Self[Mt]     Self[%] 	 Name

	main:

		   21474.980 Mt   100.000 %         1 x    21474.981 Mt    21474.979 Mt        0.867 Mt     0.004 % 	 /

		   21474.158 Mt    99.996 %         1 x    21474.160 Mt    21474.158 Mt      670.470 Mt     3.122 % 	 ./Main application loop

		       2.740 Mt     0.013 %         1 x        2.740 Mt        0.000 Mt        2.740 Mt   100.000 % 	 ../TexturedRLInitialization

		       7.244 Mt     0.034 %         1 x        7.244 Mt        0.000 Mt        7.244 Mt   100.000 % 	 ../DeferredRLInitialization

		      54.906 Mt     0.256 %         1 x       54.906 Mt        0.000 Mt        1.998 Mt     3.639 % 	 ../RayTracingRLInitialization

		      52.908 Mt    96.361 %         1 x       52.908 Mt        0.000 Mt       52.908 Mt   100.000 % 	 .../PipelineBuild

		      12.038 Mt     0.056 %         1 x       12.038 Mt        0.000 Mt       12.038 Mt   100.000 % 	 ../ResolveRLInitialization

		    9101.074 Mt    42.382 %         1 x     9101.074 Mt        0.000 Mt        8.340 Mt     0.092 % 	 ../Initialization

		    9092.734 Mt    99.908 %         1 x     9092.734 Mt        0.000 Mt        3.227 Mt     0.035 % 	 .../ScenePrep

		    8969.466 Mt    98.644 %         1 x     8969.466 Mt        0.000 Mt      326.414 Mt     3.639 % 	 ..../SceneLoad

		    8207.820 Mt    91.508 %         1 x     8207.820 Mt        0.000 Mt     1127.679 Mt    13.739 % 	 ...../glTF-LoadScene

		    7080.141 Mt    86.261 %        69 x      102.611 Mt        0.000 Mt     7080.141 Mt   100.000 % 	 ....../glTF-LoadImageData

		     266.708 Mt     2.974 %         1 x      266.708 Mt        0.000 Mt      266.708 Mt   100.000 % 	 ...../glTF-CreateScene

		     168.524 Mt     1.879 %         1 x      168.524 Mt        0.000 Mt      168.524 Mt   100.000 % 	 ...../SceneDesc-CreateScene

		       6.914 Mt     0.076 %         1 x        6.914 Mt        0.000 Mt        0.005 Mt     0.077 % 	 ..../TexturedRLPrep

		       6.909 Mt    99.923 %         1 x        6.909 Mt        0.000 Mt        1.992 Mt    28.833 % 	 ...../PrepareForRendering

		       2.586 Mt    37.426 %         1 x        2.586 Mt        0.000 Mt        2.586 Mt   100.000 % 	 ....../PrepareMaterials

		       2.331 Mt    33.741 %         1 x        2.331 Mt        0.000 Mt        2.331 Mt   100.000 % 	 ....../PrepareDrawBundle

		       8.909 Mt     0.098 %         1 x        8.909 Mt        0.000 Mt        0.005 Mt     0.054 % 	 ..../DeferredRLPrep

		       8.904 Mt    99.946 %         1 x        8.904 Mt        0.000 Mt        3.980 Mt    44.700 % 	 ...../PrepareForRendering

		       0.020 Mt     0.221 %         1 x        0.020 Mt        0.000 Mt        0.020 Mt   100.000 % 	 ....../PrepareMaterials

		       2.343 Mt    26.315 %         1 x        2.343 Mt        0.000 Mt        2.343 Mt   100.000 % 	 ....../BuildMaterialCache

		       2.561 Mt    28.764 %         1 x        2.561 Mt        0.000 Mt        2.561 Mt   100.000 % 	 ....../PrepareDrawBundle

		      28.025 Mt     0.308 %         1 x       28.025 Mt        0.000 Mt        5.589 Mt    19.945 % 	 ..../RayTracingRLPrep

		       0.588 Mt     2.099 %         1 x        0.588 Mt        0.000 Mt        0.588 Mt   100.000 % 	 ...../PrepareAccelerationStructures

		       0.025 Mt     0.088 %         1 x        0.025 Mt        0.000 Mt        0.025 Mt   100.000 % 	 ...../PrepareCache

		       4.944 Mt    17.640 %         1 x        4.944 Mt        0.000 Mt        4.944 Mt   100.000 % 	 ...../BuildCache

		      13.221 Mt    47.176 %         1 x       13.221 Mt        0.000 Mt        0.007 Mt     0.051 % 	 ...../BuildAccelerationStructures

		      13.214 Mt    99.949 %         1 x       13.214 Mt        0.000 Mt        4.286 Mt    32.437 % 	 ....../AccelerationStructureBuild

		       5.933 Mt    44.900 %         1 x        5.933 Mt        0.000 Mt        5.933 Mt   100.000 % 	 ......./BuildBottomLevelAS

		       2.995 Mt    22.663 %         1 x        2.995 Mt        0.000 Mt        2.995 Mt   100.000 % 	 ......./BuildTopLevelAS

		       3.658 Mt    13.052 %         1 x        3.658 Mt        0.000 Mt        3.658 Mt   100.000 % 	 ...../BuildShaderTables

		      76.192 Mt     0.838 %         1 x       76.192 Mt        0.000 Mt       76.192 Mt   100.000 % 	 ..../GpuSidePrep

		   11625.687 Mt    54.138 %    143944 x        0.081 Mt      443.081 Mt      458.344 Mt     3.943 % 	 ../MainLoop

		     697.705 Mt     6.001 %       690 x        1.011 Mt        0.539 Mt      697.705 Mt   100.000 % 	 .../Update

		   10469.637 Mt    90.056 %       131 x       79.921 Mt      256.271 Mt     4545.429 Mt    43.415 % 	 .../Render

		    1604.064 Mt    15.321 %       131 x       12.245 Mt        0.004 Mt        0.539 Mt     0.034 % 	 ..../DeferredRLPrep

		    1603.525 Mt    99.966 %         6 x      267.254 Mt        0.000 Mt       55.945 Mt     3.489 % 	 ...../PrepareForRendering

		       2.129 Mt     0.133 %         6 x        0.355 Mt        0.000 Mt        2.129 Mt   100.000 % 	 ....../PrepareMaterials

		       0.005 Mt     0.000 %         6 x        0.001 Mt        0.000 Mt        0.005 Mt   100.000 % 	 ....../BuildMaterialCache

		    1545.446 Mt    96.378 %         6 x      257.574 Mt        0.000 Mt     1545.446 Mt   100.000 % 	 ....../PrepareDrawBundle

		    4320.144 Mt    41.264 %       131 x       32.978 Mt        0.009 Mt       18.018 Mt     0.417 % 	 ..../RayTracingRLPrep

		     596.878 Mt    13.816 %         6 x       99.480 Mt        0.000 Mt      596.878 Mt   100.000 % 	 ...../PrepareAccelerationStructures

		       2.104 Mt     0.049 %         6 x        0.351 Mt        0.000 Mt        2.104 Mt   100.000 % 	 ...../PrepareCache

		       0.005 Mt     0.000 %         6 x        0.001 Mt        0.000 Mt        0.005 Mt   100.000 % 	 ...../BuildCache

		    2243.245 Mt    51.925 %         6 x      373.874 Mt        0.000 Mt        0.022 Mt     0.001 % 	 ...../BuildAccelerationStructures

		    2243.223 Mt    99.999 %         6 x      373.871 Mt        0.000 Mt       36.788 Mt     1.640 % 	 ....../AccelerationStructureBuild

		    2196.998 Mt    97.939 %         6 x      366.166 Mt        0.000 Mt     2196.998 Mt   100.000 % 	 ......./BuildBottomLevelAS

		       9.437 Mt     0.421 %         6 x        1.573 Mt        0.000 Mt        9.437 Mt   100.000 % 	 ......./BuildTopLevelAS

		    1459.894 Mt    33.793 %         6 x      243.316 Mt        0.000 Mt     1459.894 Mt   100.000 % 	 ...../BuildShaderTables

	WindowThread:

		   21470.722 Mt   100.000 %         1 x    21470.722 Mt    21470.721 Mt    21470.722 Mt   100.000 % 	 /

	GpuThread:

		    2430.452 Mt   100.000 %       131 x       18.553 Mt        8.242 Mt      155.125 Mt     6.383 % 	 /

		      26.687 Mt     1.098 %         1 x       26.687 Mt        0.000 Mt        0.050 Mt     0.188 % 	 ./ScenePrep

		      26.637 Mt    99.812 %         1 x       26.637 Mt        0.000 Mt        0.002 Mt     0.009 % 	 ../AccelerationStructureBuild

		      25.988 Mt    97.566 %         1 x       25.988 Mt        0.000 Mt       25.988 Mt   100.000 % 	 .../BuildBottomLevelASGpu

		       0.646 Mt     2.425 %         1 x        0.646 Mt        0.000 Mt        0.646 Mt   100.000 % 	 .../BuildTopLevelASGpu

		    2248.235 Mt    92.503 %       131 x       17.162 Mt        8.241 Mt        0.351 Mt     0.016 % 	 ./RayTracing

		     132.112 Mt     5.876 %       131 x        1.008 Mt        6.151 Mt      132.112 Mt   100.000 % 	 ../DeferredRL

		     235.354 Mt    10.468 %       131 x        1.797 Mt        1.375 Mt      235.354 Mt   100.000 % 	 ../RayTracingRL

		     152.004 Mt     6.761 %       131 x        1.160 Mt        0.714 Mt      152.004 Mt   100.000 % 	 ../ResolveRL

		    1728.414 Mt    76.879 %         6 x      288.069 Mt        0.000 Mt        0.010 Mt     0.001 % 	 ../AccelerationStructureBuild

		    1727.545 Mt    99.950 %         6 x      287.924 Mt        0.000 Mt     1727.545 Mt   100.000 % 	 .../BuildBottomLevelASGpu

		       0.860 Mt     0.050 %         6 x        0.143 Mt        0.000 Mt        0.860 Mt   100.000 % 	 .../BuildTopLevelASGpu

		       0.405 Mt     0.017 %       131 x        0.003 Mt        0.000 Mt        0.405 Mt   100.000 % 	 ./Present


	============================


