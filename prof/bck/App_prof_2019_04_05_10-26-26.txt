Configuration: 
Command line parameters = 00000269DA410268 00000269DA41031A 00000269DA41032A 00000269DA410354 
Window width = 1024
Window height = 768
Window title = Direct3D App
Back-buffer count = 3
Target FPS = 60
Target UPS = 60
Tearing = disabled
VSync = disabled
GUI rendering = enabled
Debug layer = enabled
GPU validation = disabled
GPU profiling = enabled
Render mode = Rasterization
Loaded scene file = Suzanne/Suzanne.gltf
Using testing scene = disabled
Duplication = 1
Duplication in cube = enabled
Duplication offset = 10

ProfilingBuildAggregator: 
BuildBottomLevelAS , 0 , 
BuildBottomLevelASGpu , 0 , 
BuildTopLevelAS , 0 , 
BuildTopLevelASGpu , 0 , 

ProfilingAggregator: 

FpsAggregator: 
FPS , 54 , 60 , 60 , 60 , 
UPS , 59 , 60 , 61 , 60 , 
FrameTime , 18.5186 , 16.6667 , 16.6728 , 16.6667 , 
GigaRays/s , 0 , 0 , 0 , 0 , 
AccelerationStructure: 
Ray tracing acceleration structure building statistics: 
	Bottom Level Acceleration Structure: 
		Maximum [B]: 283264
		Minimum [B]: 283264
		Currently Allocated [B]: 283264
		Currently Created [num]: 1
		Current Average [B]: 283264
		Maximum Triangles [num]: 3936
		Minimum Triangles [num]: 3936
		Total Triangles [num]: 3936
	Top Level Acceleration Structure: 
		Maximum [B]: 3584
		Minimum [B]: 3584
		Currently Allocated [B]: 3584
		Total Allocated [B]: 3584
		Total Created [num]: 1
		Average Allocated [B]: 3584
	Scratch Buffer: 
		Maximum [B]: 83968
		Minimum [B]: 83968
		Currently Allocated [B]: 83968
		Total Allocated [B]: 83968
		Total Created [num]: 1
		Average Allocated [B]: 83968
ProfilingStack: 
[PROF] Profiling statistics: 
	Scopes entered : 1005283
	Scopes exited : 1005278
	Overhead per scope [ticks] : 1148.2
	Threads entered : 3
	Threads exited : 0
	============================
		         Sum[Mt]   Parent[%]  Samples[num]       Avg[Mt]   Last/Curr[Mt]        Self[Mt]     Self[%] 	 Name

	main:

		    6044.257 Mt   100.000 %         1 x     6044.258 Mt     6044.256 Mt        0.620 Mt     0.010 % 	 /

		    6043.682 Mt    99.990 %         1 x     6043.684 Mt     6043.682 Mt     3028.949 Mt    50.118 % 	 ./Main application loop

		       3.656 Mt     0.060 %         1 x        3.656 Mt        0.000 Mt        3.656 Mt   100.000 % 	 ../TexturedRLInitialization

		       8.103 Mt     0.134 %         1 x        8.103 Mt        0.000 Mt        8.103 Mt   100.000 % 	 ../DeferredRLInitialization

		      70.178 Mt     1.161 %         1 x       70.178 Mt        0.000 Mt        2.147 Mt     3.059 % 	 ../RayTracingRLInitialization

		      68.032 Mt    96.941 %         1 x       68.032 Mt        0.000 Mt       68.032 Mt   100.000 % 	 .../PipelineBuild

		      13.287 Mt     0.220 %         1 x       13.287 Mt        0.000 Mt       13.287 Mt   100.000 % 	 ../ResolveRLInitialization

		     330.758 Mt     5.473 %         1 x      330.758 Mt        0.000 Mt        3.210 Mt     0.970 % 	 ../Initialization

		     327.548 Mt    99.030 %         1 x      327.548 Mt        0.000 Mt        2.245 Mt     0.685 % 	 .../ScenePrep

		     297.703 Mt    90.888 %         1 x      297.703 Mt        0.000 Mt       83.778 Mt    28.141 % 	 ..../SceneLoad

		     203.994 Mt    68.522 %         1 x      203.994 Mt        0.000 Mt       25.714 Mt    12.605 % 	 ...../glTF-LoadScene

		     178.280 Mt    87.395 %         2 x       89.140 Mt        0.000 Mt      178.280 Mt   100.000 % 	 ....../glTF-LoadImageData

		       7.149 Mt     2.401 %         1 x        7.149 Mt        0.000 Mt        7.149 Mt   100.000 % 	 ...../glTF-CreateScene

		       2.783 Mt     0.935 %         1 x        2.783 Mt        0.000 Mt        2.783 Mt   100.000 % 	 ...../SceneDesc-CreateScene

		       0.829 Mt     0.253 %         1 x        0.829 Mt        0.000 Mt        0.005 Mt     0.603 % 	 ..../TexturedRLPrep

		       0.824 Mt    99.397 %         1 x        0.824 Mt        0.000 Mt        0.255 Mt    30.930 % 	 ...../PrepareForRendering

		       0.278 Mt    33.746 %         1 x        0.278 Mt        0.000 Mt        0.278 Mt   100.000 % 	 ....../PrepareMaterials

		       0.291 Mt    35.324 %         1 x        0.291 Mt        0.000 Mt        0.291 Mt   100.000 % 	 ....../PrepareDrawBundle

		       1.595 Mt     0.487 %         1 x        1.595 Mt        0.000 Mt        0.004 Mt     0.276 % 	 ..../DeferredRLPrep

		       1.590 Mt    99.724 %         1 x        1.590 Mt        0.000 Mt        1.082 Mt    68.015 % 	 ...../PrepareForRendering

		       0.017 Mt     1.038 %         1 x        0.017 Mt        0.000 Mt        0.017 Mt   100.000 % 	 ....../PrepareMaterials

		       0.180 Mt    11.314 %         1 x        0.180 Mt        0.000 Mt        0.180 Mt   100.000 % 	 ....../BuildMaterialCache

		       0.312 Mt    19.634 %         1 x        0.312 Mt        0.000 Mt        0.312 Mt   100.000 % 	 ....../PrepareDrawBundle

		       6.417 Mt     1.959 %         1 x        6.417 Mt        0.000 Mt        2.456 Mt    38.266 % 	 ..../RayTracingRLPrep

		       0.099 Mt     1.540 %         1 x        0.099 Mt        0.000 Mt        0.099 Mt   100.000 % 	 ...../PrepareAccelerationStructures

		       0.024 Mt     0.376 %         1 x        0.024 Mt        0.000 Mt        0.024 Mt   100.000 % 	 ...../PrepareCache

		       0.296 Mt     4.616 %         1 x        0.296 Mt        0.000 Mt        0.296 Mt   100.000 % 	 ...../BuildCache

		       2.920 Mt    45.503 %         1 x        2.920 Mt        0.000 Mt        0.005 Mt     0.185 % 	 ...../BuildAccelerationStructures

		       2.915 Mt    99.815 %         1 x        2.915 Mt        0.000 Mt        0.819 Mt    28.092 % 	 ....../AccelerationStructureBuild

		       1.337 Mt    45.871 %         1 x        1.337 Mt        0.000 Mt        1.337 Mt   100.000 % 	 ......./BuildBottomLevelAS

		       0.759 Mt    26.037 %         1 x        0.759 Mt        0.000 Mt        0.759 Mt   100.000 % 	 ......./BuildTopLevelAS

		       0.623 Mt     9.700 %         1 x        0.623 Mt        0.000 Mt        0.623 Mt   100.000 % 	 ...../BuildShaderTables

		      18.760 Mt     5.727 %         1 x       18.760 Mt        0.000 Mt       18.760 Mt   100.000 % 	 ..../GpuSidePrep

		    2590.143 Mt    42.857 %   1003275 x        0.003 Mt        4.257 Mt     1782.720 Mt    68.827 % 	 ../MainLoop

		     192.841 Mt     7.445 %       286 x        0.674 Mt        0.327 Mt      192.841 Mt   100.000 % 	 .../Update

		     614.651 Mt    23.730 %       281 x        2.187 Mt        4.322 Mt      443.952 Mt    72.228 % 	 .../Render

		       1.788 Mt     0.291 %       281 x        0.006 Mt        0.007 Mt        1.788 Mt   100.000 % 	 ..../TexturedRLPrep

		     168.976 Mt    27.491 %       281 x        0.601 Mt        3.747 Mt      168.976 Mt   100.000 % 	 ..../GuiModelApply

	WindowThread:

		    6039.693 Mt   100.000 %         1 x     6039.694 Mt     6039.693 Mt     6039.693 Mt   100.000 % 	 /

	GpuThread:

		     153.962 Mt   100.000 %       280 x        0.550 Mt        0.011 Mt      150.504 Mt    97.754 % 	 /

		       0.511 Mt     0.332 %         1 x        0.511 Mt        0.000 Mt        0.005 Mt     0.971 % 	 ./ScenePrep

		       0.506 Mt    99.029 %         1 x        0.506 Mt        0.000 Mt        0.001 Mt     0.164 % 	 ../AccelerationStructureBuild

		       0.443 Mt    87.593 %         1 x        0.443 Mt        0.000 Mt        0.443 Mt   100.000 % 	 .../BuildBottomLevelASGpu

		       0.062 Mt    12.242 %         1 x        0.062 Mt        0.000 Mt        0.062 Mt   100.000 % 	 .../BuildTopLevelASGpu

		       1.422 Mt     0.924 %       280 x        0.005 Mt        0.005 Mt        1.422 Mt   100.000 % 	 ./Textured

		       1.077 Mt     0.700 %       280 x        0.004 Mt        0.004 Mt        1.077 Mt   100.000 % 	 ./ImGui

		       0.447 Mt     0.291 %       280 x        0.002 Mt        0.002 Mt        0.447 Mt   100.000 % 	 ./Present


	============================


