Configuration: 
Command line parameters  = E:\programming\FIT-DIP\Masters-DXR-Main-git\\build\Measurement\x64\Debug\Measurement.exe --scene Suzanne/Suzanne.gltf --duplication-cube --profile-duplication 10 --rt-mode --width 2560 --height 1440 
Window width             = 2560
Window height            = 1421
Window title             = Direct3D App
Back-buffer count        = 3
Target FPS               = 60
Target UPS               = 60
Tearing                  = disabled
VSync                    = disabled
GUI rendering            = disabled
Debug layer              = enabled
GPU validation           = disabled
GPU profiling            = enabled
Render mode              = Ray Tracing
Loaded scene file        = Suzanne/Suzanne.gltf
Using testing scene      = disabled
Duplication              = 10
Duplication in cube      = enabled
Duplication offset       = 10
BLAS duplication         = enabled
Rays per pixel           = 10

ProfilingBuildAggregator: 
BuildBottomLevelAS	,	1.2244	,	6.4007	,	17.7426	,	44.9321	,	96.7616	,	164.408	,	242.237	,	381.464	,	513.637	,	702.455
BuildBottomLevelASGpu	,	0.533632	,	4.28432	,	13.9259	,	34.9254	,	62.8075	,	115.294	,	179.195	,	271.213	,	388.15	,	532.452
BuildTopLevelAS	,	0.7004	,	0.8084	,	0.8282	,	0.6664	,	0.6608	,	1.0337	,	0.8348	,	0.9382	,	0.98	,	0.8335
BuildTopLevelASGpu	,	0.060512	,	0.069248	,	0.091424	,	0.100672	,	0.133024	,	0.159776	,	0.15744	,	0.136096	,	0.18864	,	0.308224

ProfilingAggregator: 
Render	,	7.4793	,	6.7892	,	7.5923	,	7.3941	,	8.5663	,	10.0529	,	8.8797	,	833.562	,	1157.08	,	1648.93
Update	,	0.3274	,	0.4438	,	0.3447	,	0.3775	,	0.7009	,	0.8022	,	0.951	,	0.6984	,	1.4162	,	2.281
RayTracing	,	3.12304	,	3.2417	,	3.15248	,	3.03098	,	3.07648	,	3.53264	,	3.67802	,	275.037	,	392.568	,	536.971
RayTracingRL	,	0.19552	,	0.229632	,	0.285952	,	0.325216	,	0.353056	,	0.412096	,	0.454144	,	0.520064	,	0.944544	,	0.762272

FpsAggregator: 
FPS	,	55	,	59	,	55	,	46	,	46	,	34	,	18	,	2	,	1	,	1
UPS	,	59	,	60	,	61	,	60	,	60	,	60	,	61	,	6	,	56	,	91
FrameTime	,	18.2887	,	16.9625	,	18.1818	,	21.7392	,	21.7987	,	29.6365	,	55.5875	,	520.826	,	1519.09	,	2155.19
GigaRays/s	,	1.98907	,	2.14459	,	2.00077	,	1.67336	,	1.6688	,	1.22746	,	0.654421	,	0.0698459	,	0.0239469	,	0.0168791

AccelerationStructure: 
Ray tracing acceleration structure building statistics: 
	Bottom Level Acceleration Structure: 
		Maximum [B]: 283264
		Minimum [B]: 283264
		Currently Allocated [B]: 283264000
		Currently Created [num]: 1000
		Current Average [B]: 283264
		Maximum Triangles [num]: 3936
		Minimum Triangles [num]: 3936
		Total Triangles [num]: 3936000
		Maximum Meshes [num]: 1
		Minimum Meshes [num]: 1
		Total Meshes [num]: 1000
	Top Level Acceleration Structure: 
		Maximum [B]: 64000
		Minimum [B]: 64000
		Currently Allocated [B]: 64000
		Total Allocated [B]: 64000
		Total Created [num]: 1
		Average Allocated [B]: 64000
		Maximum Instances [num]: 1000
		Minimum Instances [num]: 1000
		Total Instances [num]: 1000
	Scratch Buffer: 
		Maximum [B]: 83968
		Minimum [B]: 83968
		Currently Allocated [B]: 83968
		Total Allocated [B]: 83968
		Total Created [num]: 1
		Average Allocated [B]: 83968
ProfilingStack: 
[PROF] Profiling statistics: 
	Scopes entered : 579065
	Scopes exited : 579064
	Overhead per scope [ticks] : 1024.2
	Threads entered : 3
	Threads exited : 0
	============================
		         Sum[Mt]   Parent[%]  Samples[num]       Avg[Mt]   Last/Curr[Mt]        Self[Mt]     Self[%] 	 Name

	main:

		   13487.132 Mt   100.000 %         1 x    13487.134 Mt    13487.131 Mt      513.397 Mt     3.807 % 	 /

		   12973.811 Mt    96.194 %         1 x    12973.814 Mt    12973.811 Mt     1969.205 Mt    15.178 % 	 ./Main application loop

		       3.574 Mt     0.028 %         1 x        3.574 Mt        0.000 Mt        3.574 Mt   100.000 % 	 ../TexturedRLInitialization

		       9.270 Mt     0.071 %         1 x        9.270 Mt        0.000 Mt        9.270 Mt   100.000 % 	 ../DeferredRLInitialization

		      69.133 Mt     0.533 %         1 x       69.133 Mt        0.000 Mt        2.230 Mt     3.226 % 	 ../RayTracingRLInitialization

		      66.903 Mt    96.774 %         1 x       66.903 Mt        0.000 Mt       66.903 Mt   100.000 % 	 .../PipelineBuild

		      12.306 Mt     0.095 %         1 x       12.306 Mt        0.000 Mt       12.306 Mt   100.000 % 	 ../ResolveRLInitialization

		     245.765 Mt     1.894 %         1 x      245.765 Mt        0.000 Mt        4.160 Mt     1.693 % 	 ../Initialization

		     241.605 Mt    98.307 %         1 x      241.605 Mt        0.000 Mt        1.170 Mt     0.484 % 	 .../ScenePrep

		     222.893 Mt    92.255 %         1 x      222.893 Mt        0.000 Mt       12.464 Mt     5.592 % 	 ..../SceneLoad

		     200.145 Mt    89.795 %         1 x      200.145 Mt        0.000 Mt       24.216 Mt    12.099 % 	 ...../glTF-LoadScene

		     175.929 Mt    87.901 %         2 x       87.965 Mt        0.000 Mt      175.929 Mt   100.000 % 	 ....../glTF-LoadImageData

		       6.996 Mt     3.139 %         1 x        6.996 Mt        0.000 Mt        6.996 Mt   100.000 % 	 ...../glTF-CreateScene

		       3.287 Mt     1.475 %         1 x        3.287 Mt        0.000 Mt        3.287 Mt   100.000 % 	 ...../SceneDesc-CreateScene

		       0.863 Mt     0.357 %         1 x        0.863 Mt        0.000 Mt        0.005 Mt     0.626 % 	 ..../TexturedRLPrep

		       0.857 Mt    99.374 %         1 x        0.857 Mt        0.000 Mt        0.257 Mt    30.009 % 	 ...../PrepareForRendering

		       0.281 Mt    32.797 %         1 x        0.281 Mt        0.000 Mt        0.281 Mt   100.000 % 	 ....../PrepareMaterials

		       0.319 Mt    37.194 %         1 x        0.319 Mt        0.000 Mt        0.319 Mt   100.000 % 	 ....../PrepareDrawBundle

		       1.676 Mt     0.694 %         1 x        1.676 Mt        0.000 Mt        0.004 Mt     0.268 % 	 ..../DeferredRLPrep

		       1.672 Mt    99.732 %         1 x        1.672 Mt        0.000 Mt        1.150 Mt    68.792 % 	 ...../PrepareForRendering

		       0.016 Mt     0.969 %         1 x        0.016 Mt        0.000 Mt        0.016 Mt   100.000 % 	 ....../PrepareMaterials

		       0.215 Mt    12.873 %         1 x        0.215 Mt        0.000 Mt        0.215 Mt   100.000 % 	 ....../BuildMaterialCache

		       0.290 Mt    17.366 %         1 x        0.290 Mt        0.000 Mt        0.290 Mt   100.000 % 	 ....../PrepareDrawBundle

		       6.516 Mt     2.697 %         1 x        6.516 Mt        0.000 Mt        2.482 Mt    38.088 % 	 ..../RayTracingRLPrep

		       0.279 Mt     4.288 %         1 x        0.279 Mt        0.000 Mt        0.279 Mt   100.000 % 	 ...../PrepareAccelerationStructures

		       0.024 Mt     0.370 %         1 x        0.024 Mt        0.000 Mt        0.024 Mt   100.000 % 	 ...../PrepareCache

		       0.260 Mt     3.988 %         1 x        0.260 Mt        0.000 Mt        0.260 Mt   100.000 % 	 ...../BuildCache

		       2.711 Mt    41.604 %         1 x        2.711 Mt        0.000 Mt        0.015 Mt     0.542 % 	 ...../BuildAccelerationStructures

		       2.696 Mt    99.458 %         1 x        2.696 Mt        0.000 Mt        0.772 Mt    28.616 % 	 ....../AccelerationStructureBuild

		       1.224 Mt    45.409 %         1 x        1.224 Mt        0.000 Mt        1.224 Mt   100.000 % 	 ......./BuildBottomLevelAS

		       0.700 Mt    25.975 %         1 x        0.700 Mt        0.000 Mt        0.700 Mt   100.000 % 	 ......./BuildTopLevelAS

		       0.760 Mt    11.663 %         1 x        0.760 Mt        0.000 Mt        0.760 Mt   100.000 % 	 ...../BuildShaderTables

		       8.487 Mt     3.513 %         1 x        8.487 Mt        0.000 Mt        8.487 Mt   100.000 % 	 ..../GpuSidePrep

		   10664.559 Mt    82.201 %    575600 x        0.019 Mt      277.684 Mt     1149.553 Mt    10.779 % 	 ../MainLoop

		    1891.109 Mt    17.733 %       704 x        2.686 Mt        1.201 Mt     1891.109 Mt   100.000 % 	 .../Update

		    7623.897 Mt    71.488 %       318 x       23.975 Mt       19.506 Mt     4243.023 Mt    55.654 % 	 .../Render

		     138.430 Mt     1.816 %       318 x        0.435 Mt        0.004 Mt        2.403 Mt     1.736 % 	 ..../DeferredRLPrep

		     136.027 Mt    98.264 %        12 x       11.336 Mt        0.000 Mt        6.123 Mt     4.501 % 	 ...../PrepareForRendering

		      12.420 Mt     9.131 %        12 x        1.035 Mt        0.000 Mt       12.420 Mt   100.000 % 	 ....../PrepareMaterials

		       0.011 Mt     0.008 %        12 x        0.001 Mt        0.000 Mt        0.011 Mt   100.000 % 	 ....../BuildMaterialCache

		     117.473 Mt    86.360 %        12 x        9.789 Mt        0.000 Mt      117.473 Mt   100.000 % 	 ....../PrepareDrawBundle

		    3242.444 Mt    42.530 %       318 x       10.196 Mt        0.014 Mt       21.263 Mt     0.656 % 	 ..../RayTracingRLPrep

		     840.033 Mt    25.907 %        12 x       70.003 Mt        0.000 Mt      840.033 Mt   100.000 % 	 ...../PrepareAccelerationStructures

		      10.981 Mt     0.339 %        12 x        0.915 Mt        0.000 Mt       10.981 Mt   100.000 % 	 ...../PrepareCache

		       0.011 Mt     0.000 %        12 x        0.001 Mt        0.000 Mt        0.011 Mt   100.000 % 	 ...../BuildCache

		    2277.790 Mt    70.249 %        12 x      189.816 Mt        0.000 Mt        0.040 Mt     0.002 % 	 ...../BuildAccelerationStructures

		    2277.750 Mt    99.998 %        12 x      189.812 Mt        0.000 Mt       25.897 Mt     1.137 % 	 ....../AccelerationStructureBuild

		    2242.040 Mt    98.432 %        12 x      186.837 Mt        0.000 Mt     2242.040 Mt   100.000 % 	 ......./BuildBottomLevelAS

		       9.814 Mt     0.431 %        12 x        0.818 Mt        0.000 Mt        9.814 Mt   100.000 % 	 ......./BuildTopLevelAS

		      92.367 Mt     2.849 %        12 x        7.697 Mt        0.000 Mt       92.367 Mt   100.000 % 	 ...../BuildShaderTables

	WindowThread:

		   12971.095 Mt   100.000 %         1 x    12971.096 Mt    12971.094 Mt    12971.095 Mt   100.000 % 	 /

	GpuThread:

		    2872.194 Mt   100.000 %       318 x        9.032 Mt        5.311 Mt      147.680 Mt     5.142 % 	 /

		       0.600 Mt     0.021 %         1 x        0.600 Mt        0.000 Mt        0.005 Mt     0.817 % 	 ./ScenePrep

		       0.595 Mt    99.183 %         1 x        0.595 Mt        0.000 Mt        0.001 Mt     0.097 % 	 ../AccelerationStructureBuild

		       0.534 Mt    89.728 %         1 x        0.534 Mt        0.000 Mt        0.534 Mt   100.000 % 	 .../BuildBottomLevelASGpu

		       0.061 Mt    10.175 %         1 x        0.061 Mt        0.000 Mt        0.061 Mt   100.000 % 	 .../BuildTopLevelASGpu

		    2723.692 Mt    94.830 %       318 x        8.565 Mt        5.310 Mt        0.600 Mt     0.022 % 	 ./RayTracing

		      28.559 Mt     1.049 %       318 x        0.090 Mt        0.636 Mt       28.559 Mt   100.000 % 	 ../DeferredRL

		      99.601 Mt     3.657 %       318 x        0.313 Mt        1.727 Mt       99.601 Mt   100.000 % 	 ../RayTracingRL

		     935.912 Mt    34.362 %       318 x        2.943 Mt        2.945 Mt      935.912 Mt   100.000 % 	 ../ResolveRL

		    1659.020 Mt    60.911 %        12 x      138.252 Mt        0.000 Mt        0.019 Mt     0.001 % 	 ../AccelerationStructureBuild

		    1657.396 Mt    99.902 %        12 x      138.116 Mt        0.000 Mt     1657.396 Mt   100.000 % 	 .../BuildBottomLevelASGpu

		       1.605 Mt     0.097 %        12 x        0.134 Mt        0.000 Mt        1.605 Mt   100.000 % 	 .../BuildTopLevelASGpu

		       0.222 Mt     0.008 %       318 x        0.001 Mt        0.000 Mt        0.222 Mt   100.000 % 	 ./Present


	============================


