Configuration: 
Command line parameters  = E:\programming\FIT-DIP\Masters-DXR-Main-git\\build\Measurement\x64\Debug\Measurement.exe --scene Suzanne/Suzanne.gltf --duplication-cube 
Window width             = 1024
Window height            = 768
Window title             = Direct3D App
Back-buffer count        = 3
Target FPS               = 60
Target UPS               = 60
Tearing                  = disabled
VSync                    = disabled
GUI rendering            = enabled
Debug layer              = enabled
GPU validation           = disabled
GPU profiling            = enabled
Render mode              = Ray Tracing
Loaded scene file        = Suzanne/Suzanne.gltf
Using testing scene      = disabled
Duplication              = 1
Duplication in cube      = enabled
Duplication offset       = 10
BLAS duplication         = enabled
Rays per pixel           = 10

ProfilingBuildAggregator: 
BuildBottomLevelAS	,	1.2196
BuildBottomLevelASGpu	,	0.444992
BuildTopLevelAS	,	0.7388
BuildTopLevelASGpu	,	0.061632

ProfilingAggregator: 
Render	,	1.707	,	3.9171	,	4.0507	,	5.213	,	5.0163
Update	,	0.3777	,	0.2986	,	0.2815	,	0.2982	,	0.2986
RayTracing	,	0	,	0.641984	,	0.649312	,	0.648544	,	0.649856
RayTracingRL	,	0	,	0.0656	,	0.071328	,	0.070112	,	0.071872

FpsAggregator: 
FPS	,	56	,	60	,	60	,	60	,	60
UPS	,	59	,	60	,	60	,	60	,	61
FrameTime	,	17.8572	,	16.6908	,	16.6708	,	16.688	,	16.6707
GigaRays/s	,	0	,	0.471178	,	0.471743	,	0.471255	,	0.471746

AccelerationStructure: 
Ray tracing acceleration structure building statistics: 
	Bottom Level Acceleration Structure: 
		Maximum [B]: 283264
		Minimum [B]: 283264
		Currently Allocated [B]: 283264
		Currently Created [num]: 1
		Current Average [B]: 283264
		Maximum Triangles [num]: 3936
		Minimum Triangles [num]: 3936
		Total Triangles [num]: 3936
		Maximum Meshes [num]: 1
		Minimum Meshes [num]: 1
		Total Meshes [num]: 1
	Top Level Acceleration Structure: 
		Maximum [B]: 3584
		Minimum [B]: 3584
		Currently Allocated [B]: 3584
		Total Allocated [B]: 3584
		Total Created [num]: 1
		Average Allocated [B]: 3584
	Scratch Buffer: 
		Maximum [B]: 83968
		Minimum [B]: 83968
		Currently Allocated [B]: 83968
		Total Allocated [B]: 83968
		Total Created [num]: 1
		Average Allocated [B]: 83968
ProfilingStack: 
[PROF] Profiling statistics: 
	Scopes entered : 979812
	Scopes exited : 979811
	Overhead per scope [ticks] : 1020.8
	Threads entered : 3
	Threads exited : 0
	============================
		         Sum[Mt]   Parent[%]  Samples[num]       Avg[Mt]   Last/Curr[Mt]        Self[Mt]     Self[%] 	 Name

	main:

		    6961.091 Mt   100.000 %         1 x     6961.093 Mt     6961.090 Mt      616.309 Mt     8.854 % 	 /

		    6344.832 Mt    91.147 %         1 x     6344.834 Mt     6344.832 Mt     2913.734 Mt    45.923 % 	 ./Main application loop

		       4.418 Mt     0.070 %         1 x        4.418 Mt        0.000 Mt        4.418 Mt   100.000 % 	 ../TexturedRLInitialization

		       9.081 Mt     0.143 %         1 x        9.081 Mt        0.000 Mt        9.081 Mt   100.000 % 	 ../DeferredRLInitialization

		      62.424 Mt     0.984 %         1 x       62.424 Mt        0.000 Mt        2.003 Mt     3.208 % 	 ../RayTracingRLInitialization

		      60.421 Mt    96.792 %         1 x       60.421 Mt        0.000 Mt       60.421 Mt   100.000 % 	 .../PipelineBuild

		      13.767 Mt     0.217 %         1 x       13.767 Mt        0.000 Mt       13.767 Mt   100.000 % 	 ../ResolveRLInitialization

		     253.749 Mt     3.999 %         1 x      253.749 Mt        0.000 Mt        2.630 Mt     1.036 % 	 ../Initialization

		     251.120 Mt    98.964 %         1 x      251.120 Mt        0.000 Mt        1.583 Mt     0.630 % 	 .../ScenePrep

		     229.633 Mt    91.444 %         1 x      229.633 Mt        0.000 Mt       14.589 Mt     6.353 % 	 ..../SceneLoad

		     204.833 Mt    89.200 %         1 x      204.833 Mt        0.000 Mt       25.600 Mt    12.498 % 	 ...../glTF-LoadScene

		     179.233 Mt    87.502 %         2 x       89.617 Mt        0.000 Mt      179.233 Mt   100.000 % 	 ....../glTF-LoadImageData

		       7.720 Mt     3.362 %         1 x        7.720 Mt        0.000 Mt        7.720 Mt   100.000 % 	 ...../glTF-CreateScene

		       2.491 Mt     1.085 %         1 x        2.491 Mt        0.000 Mt        2.491 Mt   100.000 % 	 ...../SceneDesc-CreateScene

		       1.049 Mt     0.418 %         1 x        1.049 Mt        0.000 Mt        0.007 Mt     0.696 % 	 ..../TexturedRLPrep

		       1.041 Mt    99.304 %         1 x        1.041 Mt        0.000 Mt        0.353 Mt    33.871 % 	 ...../PrepareForRendering

		       0.364 Mt    34.927 %         1 x        0.364 Mt        0.000 Mt        0.364 Mt   100.000 % 	 ....../PrepareMaterials

		       0.325 Mt    31.201 %         1 x        0.325 Mt        0.000 Mt        0.325 Mt   100.000 % 	 ....../PrepareDrawBundle

		       2.010 Mt     0.801 %         1 x        2.010 Mt        0.000 Mt        0.005 Mt     0.244 % 	 ..../DeferredRLPrep

		       2.006 Mt    99.756 %         1 x        2.006 Mt        0.000 Mt        1.304 Mt    65.046 % 	 ...../PrepareForRendering

		       0.017 Mt     0.858 %         1 x        0.017 Mt        0.000 Mt        0.017 Mt   100.000 % 	 ....../PrepareMaterials

		       0.235 Mt    11.713 %         1 x        0.235 Mt        0.000 Mt        0.235 Mt   100.000 % 	 ....../BuildMaterialCache

		       0.449 Mt    22.383 %         1 x        0.449 Mt        0.000 Mt        0.449 Mt   100.000 % 	 ....../PrepareDrawBundle

		       7.009 Mt     2.791 %         1 x        7.009 Mt        0.000 Mt        2.897 Mt    41.339 % 	 ..../RayTracingRLPrep

		       0.358 Mt     5.102 %         1 x        0.358 Mt        0.000 Mt        0.358 Mt   100.000 % 	 ...../PrepareAccelerationStructures

		       0.038 Mt     0.538 %         1 x        0.038 Mt        0.000 Mt        0.038 Mt   100.000 % 	 ...../PrepareCache

		       0.260 Mt     3.712 %         1 x        0.260 Mt        0.000 Mt        0.260 Mt   100.000 % 	 ...../BuildCache

		       2.766 Mt    39.461 %         1 x        2.766 Mt        0.000 Mt        0.005 Mt     0.163 % 	 ...../BuildAccelerationStructures

		       2.761 Mt    99.837 %         1 x        2.761 Mt        0.000 Mt        0.803 Mt    29.077 % 	 ....../AccelerationStructureBuild

		       1.220 Mt    44.168 %         1 x        1.220 Mt        0.000 Mt        1.220 Mt   100.000 % 	 ......./BuildBottomLevelAS

		       0.739 Mt    26.756 %         1 x        0.739 Mt        0.000 Mt        0.739 Mt   100.000 % 	 ......./BuildTopLevelAS

		       0.690 Mt     9.847 %         1 x        0.690 Mt        0.000 Mt        0.690 Mt   100.000 % 	 ...../BuildShaderTables

		       9.836 Mt     3.917 %         1 x        9.836 Mt        0.000 Mt        9.836 Mt   100.000 % 	 ..../GpuSidePrep

		    3087.659 Mt    48.664 %    976776 x        0.003 Mt        8.589 Mt     1707.229 Mt    55.292 % 	 ../MainLoop

		     182.929 Mt     5.925 %       317 x        0.577 Mt        8.583 Mt      182.929 Mt   100.000 % 	 .../Update

		    1197.501 Mt    38.783 %       313 x        3.826 Mt        5.666 Mt     1015.409 Mt    84.794 % 	 .../Render

		       0.865 Mt     0.072 %       112 x        0.008 Mt        0.000 Mt        0.865 Mt   100.000 % 	 ..../TexturedRLPrep

		     178.905 Mt    14.940 %       313 x        0.572 Mt        0.491 Mt      178.905 Mt   100.000 % 	 ..../GuiModelApply

		       1.473 Mt     0.123 %       201 x        0.007 Mt        0.007 Mt        1.473 Mt   100.000 % 	 ..../DeferredRLPrep

		       0.850 Mt     0.071 %       201 x        0.004 Mt        0.003 Mt        0.850 Mt   100.000 % 	 ..../RayTracingRLPrep

	WindowThread:

		    6341.186 Mt   100.000 %         1 x     6341.187 Mt     6341.186 Mt     6341.186 Mt   100.000 % 	 /

	GpuThread:

		     270.180 Mt   100.000 %       313 x        0.863 Mt        0.663 Mt      136.080 Mt    50.366 % 	 /

		       0.513 Mt     0.190 %         1 x        0.513 Mt        0.000 Mt        0.006 Mt     1.098 % 	 ./ScenePrep

		       0.508 Mt    98.902 %         1 x        0.508 Mt        0.000 Mt        0.001 Mt     0.177 % 	 ../AccelerationStructureBuild

		       0.445 Mt    87.680 %         1 x        0.445 Mt        0.000 Mt        0.445 Mt   100.000 % 	 .../BuildBottomLevelASGpu

		       0.062 Mt    12.144 %         1 x        0.062 Mt        0.000 Mt        0.062 Mt   100.000 % 	 .../BuildTopLevelASGpu

		       0.632 Mt     0.234 %       112 x        0.006 Mt        0.000 Mt        0.632 Mt   100.000 % 	 ./Textured

		       2.159 Mt     0.799 %       313 x        0.007 Mt        0.009 Mt        2.159 Mt   100.000 % 	 ./ImGui

		       0.493 Mt     0.182 %       313 x        0.002 Mt        0.002 Mt        0.493 Mt   100.000 % 	 ./Present

		     130.304 Mt    48.229 %       201 x        0.648 Mt        0.652 Mt        0.368 Mt     0.282 % 	 ./RayTracing

		       2.506 Mt     1.923 %       201 x        0.012 Mt        0.013 Mt        2.506 Mt   100.000 % 	 ../DeferredRL

		      14.091 Mt    10.814 %       201 x        0.070 Mt        0.075 Mt       14.091 Mt   100.000 % 	 ../RayTracingRL

		     113.339 Mt    86.981 %       201 x        0.564 Mt        0.563 Mt      113.339 Mt   100.000 % 	 ../ResolveRL


	============================


