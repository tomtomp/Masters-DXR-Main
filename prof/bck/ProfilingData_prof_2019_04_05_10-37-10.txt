Configuration: 
Command line parameters = E:\programming\FIT-DIP\Masters-DXR-Main-git\\build\Measurement\x64\Debug\Measurement.exe --scene Suzanne/Suzanne.gltf --duplication-cube 
Window width = 1024
Window height = 768
Window title = Direct3D App
Back-buffer count = 3
Target FPS = 60
Target UPS = 60
Tearing = disabled
VSync = disabled
GUI rendering = enabled
Debug layer = enabled
GPU validation = disabled
GPU profiling = enabled
Render mode = Rasterization
Loaded scene file = Suzanne/Suzanne.gltf
Using testing scene = disabled
Duplication = 1
Duplication in cube = enabled
Duplication offset = 10

ProfilingBuildAggregator: 
BuildBottomLevelAS , 0.9056 , 
BuildBottomLevelASGpu , 0.437376 , 
BuildTopLevelAS , 0.615 , 
BuildTopLevelASGpu , 0.061632 , 

ProfilingAggregator: 
Render , 1.6937 , 0 , 1.9133 , 1.8685 , 2.8613 , 1.7922 , 1.6459 , 1.8951 , 1.9003 , 2.2727 , 1.7511 , 1.6612 , 1.7783 , 2.1112 , 2.3971 , 2.8722 , 2.0631 , 1.7261 , 3.1249 , 1.9628 , 1.74 , 2.0044 , 1.6819 , 1.7995 , 2.3076 , 1.7927 , 2.2728 , 2.1243 , 1.7704 , 1.9489 , 2.1247 , 2.3833 , 2.3818 , 1.5987 , 1.729 , 1.7273 , 1.9807 , 1.6036 , 2.6126 , 1.8585 , 3.1605 , 2.5411 , 
Update , 0.2833 , 0.3183 , 0.3243 , 0.3144 , 0.3868 , 0.3371 , 0.3098 , 0.3418 , 0.3654 , 0.3473 , 0.3877 , 0.2908 , 0.3526 , 0.2987 , 0.3431 , 0.297 , 0.3051 , 0.2986 , 0.2936 , 0.2821 , 0.2882 , 0.3011 , 0.2873 , 0.2929 , 0.297 , 0.2957 , 0.3228 , 0.345 , 0.3689 , 0.433 , 0.4543 , 0.483 , 0.3332 , 0.3323 , 0.3251 , 0.3165 , 0.3014 , 0.3246 , 0.3385 , 0.3286 , 0.3224 , 0.3002 , 

FpsAggregator: 
FPS , 55 , 0 , 55 , 60 , 60 , 60 , 60 , 60 , 60 , 60 , 60 , 60 , 60 , 60 , 60 , 60 , 60 , 60 , 60 , 60 , 60 , 60 , 60 , 60 , 60 , 60 , 60 , 60 , 60 , 60 , 60 , 60 , 60 , 60 , 60 , 60 , 60 , 60 , 60 , 60 , 60 , 59 , 
UPS , 59 , 1 , 506 , 60 , 60 , 60 , 60 , 60 , 60 , 60 , 60 , 60 , 60 , 60 , 60 , 60 , 60 , 60 , 60 , 60 , 60 , 60 , 60 , 60 , 60 , 60 , 60 , 60 , 60 , 60 , 60 , 60 , 60 , 60 , 60 , 60 , 60 , 60 , 60 , 60 , 60 , 60 , 
FrameTime , 18.1818 , inf , 18.2867 , 16.667 , 16.6667 , 16.6667 , 16.6667 , 16.6667 , 16.6667 , 16.6667 , 16.6667 , 16.6667 , 16.6667 , 16.6667 , 16.6667 , 16.6667 , 16.6667 , 16.6667 , 16.6667 , 16.6667 , 16.6667 , 16.6667 , 16.6667 , 16.6667 , 16.6667 , 16.6667 , 16.6667 , 16.6667 , 16.6667 , 16.6667 , 16.6667 , 16.6667 , 16.6667 , 16.6667 , 16.6667 , 16.6667 , 16.6667 , 16.6667 , 16.6667 , 16.6667 , 16.6758 , 16.9492 , 
GigaRays/s , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 
AccelerationStructure: 
Ray tracing acceleration structure building statistics: 
	Bottom Level Acceleration Structure: 
		Maximum [B]: 283264
		Minimum [B]: 283264
		Currently Allocated [B]: 283264
		Currently Created [num]: 1
		Current Average [B]: 283264
		Maximum Triangles [num]: 3936
		Minimum Triangles [num]: 3936
		Total Triangles [num]: 3936
	Top Level Acceleration Structure: 
		Maximum [B]: 3584
		Minimum [B]: 3584
		Currently Allocated [B]: 3584
		Total Allocated [B]: 3584
		Total Created [num]: 1
		Average Allocated [B]: 3584
	Scratch Buffer: 
		Maximum [B]: 83968
		Minimum [B]: 83968
		Currently Allocated [B]: 83968
		Total Allocated [B]: 83968
		Total Created [num]: 1
		Average Allocated [B]: 83968
ProfilingStack: 
[PROF] Profiling statistics: 
	Scopes entered : 8803856
	Scopes exited : 8803855
	Overhead per scope [ticks] : 1126.7
	Threads entered : 3
	Threads exited : 0
	============================
		         Sum[Mt]   Parent[%]  Samples[num]       Avg[Mt]   Last/Curr[Mt]        Self[Mt]     Self[%] 	 Name

	main:

		   51760.178 Mt   100.000 %         1 x    51760.180 Mt    51760.177 Mt       54.631 Mt     0.106 % 	 /

		   51705.594 Mt    99.895 %         1 x    51705.596 Mt    51705.594 Mt    21836.179 Mt    42.232 % 	 ./Main application loop

		       3.218 Mt     0.006 %         1 x        3.218 Mt        0.000 Mt        3.218 Mt   100.000 % 	 ../TexturedRLInitialization

		       8.985 Mt     0.017 %         1 x        8.985 Mt        0.000 Mt        8.985 Mt   100.000 % 	 ../DeferredRLInitialization

		      67.289 Mt     0.130 %         1 x       67.289 Mt        0.000 Mt        2.188 Mt     3.252 % 	 ../RayTracingRLInitialization

		      65.101 Mt    96.748 %         1 x       65.101 Mt        0.000 Mt       65.101 Mt   100.000 % 	 .../PipelineBuild

		      12.262 Mt     0.024 %         1 x       12.262 Mt        0.000 Mt       12.262 Mt   100.000 % 	 ../ResolveRLInitialization

		     329.527 Mt     0.637 %         1 x      329.527 Mt        0.000 Mt        2.972 Mt     0.902 % 	 ../Initialization

		     326.556 Mt    99.098 %         1 x      326.556 Mt        0.000 Mt        2.536 Mt     0.777 % 	 .../ScenePrep

		     296.098 Mt    90.673 %         1 x      296.098 Mt        0.000 Mt       85.903 Mt    29.012 % 	 ..../SceneLoad

		     199.297 Mt    67.308 %         1 x      199.297 Mt        0.000 Mt       23.812 Mt    11.948 % 	 ...../glTF-LoadScene

		     175.484 Mt    88.052 %         2 x       87.742 Mt        0.000 Mt      175.484 Mt   100.000 % 	 ....../glTF-LoadImageData

		       7.499 Mt     2.533 %         1 x        7.499 Mt        0.000 Mt        7.499 Mt   100.000 % 	 ...../glTF-CreateScene

		       3.399 Mt     1.148 %         1 x        3.399 Mt        0.000 Mt        3.399 Mt   100.000 % 	 ...../SceneDesc-CreateScene

		       1.337 Mt     0.409 %         1 x        1.337 Mt        0.000 Mt        0.005 Mt     0.404 % 	 ..../TexturedRLPrep

		       1.332 Mt    99.596 %         1 x        1.332 Mt        0.000 Mt        0.485 Mt    36.460 % 	 ...../PrepareForRendering

		       0.346 Mt    26.006 %         1 x        0.346 Mt        0.000 Mt        0.346 Mt   100.000 % 	 ....../PrepareMaterials

		       0.500 Mt    37.534 %         1 x        0.500 Mt        0.000 Mt        0.500 Mt   100.000 % 	 ....../PrepareDrawBundle

		       1.735 Mt     0.531 %         1 x        1.735 Mt        0.000 Mt        0.005 Mt     0.265 % 	 ..../DeferredRLPrep

		       1.731 Mt    99.735 %         1 x        1.731 Mt        0.000 Mt        1.180 Mt    68.177 % 	 ...../PrepareForRendering

		       0.022 Mt     1.248 %         1 x        0.022 Mt        0.000 Mt        0.022 Mt   100.000 % 	 ....../PrepareMaterials

		       0.243 Mt    14.034 %         1 x        0.243 Mt        0.000 Mt        0.243 Mt   100.000 % 	 ....../BuildMaterialCache

		       0.286 Mt    16.541 %         1 x        0.286 Mt        0.000 Mt        0.286 Mt   100.000 % 	 ....../PrepareDrawBundle

		       5.822 Mt     1.783 %         1 x        5.822 Mt        0.000 Mt        2.403 Mt    41.285 % 	 ..../RayTracingRLPrep

		       0.102 Mt     1.749 %         1 x        0.102 Mt        0.000 Mt        0.102 Mt   100.000 % 	 ...../PrepareAccelerationStructures

		       0.028 Mt     0.481 %         1 x        0.028 Mt        0.000 Mt        0.028 Mt   100.000 % 	 ...../PrepareCache

		       0.304 Mt     5.227 %         1 x        0.304 Mt        0.000 Mt        0.304 Mt   100.000 % 	 ...../BuildCache

		       2.362 Mt    40.572 %         1 x        2.362 Mt        0.000 Mt        0.004 Mt     0.178 % 	 ...../BuildAccelerationStructures

		       2.358 Mt    99.822 %         1 x        2.358 Mt        0.000 Mt        0.837 Mt    35.505 % 	 ....../AccelerationStructureBuild

		       0.906 Mt    38.410 %         1 x        0.906 Mt        0.000 Mt        0.906 Mt   100.000 % 	 ......./BuildBottomLevelAS

		       0.615 Mt    26.085 %         1 x        0.615 Mt        0.000 Mt        0.615 Mt   100.000 % 	 ......./BuildTopLevelAS

		       0.622 Mt    10.686 %         1 x        0.622 Mt        0.000 Mt        0.622 Mt   100.000 % 	 ...../BuildShaderTables

		      19.028 Mt     5.827 %         1 x       19.028 Mt        0.000 Mt       19.028 Mt   100.000 % 	 ..../GpuSidePrep

		   29448.133 Mt    56.953 %   8786156 x        0.003 Mt        0.311 Mt    23057.829 Mt    78.300 % 	 ../MainLoop

		    1055.956 Mt     3.586 %      2915 x        0.362 Mt        0.308 Mt     1055.956 Mt   100.000 % 	 .../Update

		    5334.348 Mt    18.114 %      2458 x        2.170 Mt        2.530 Mt     3894.926 Mt    73.016 % 	 .../Render

		      17.393 Mt     0.326 %      2458 x        0.007 Mt        0.007 Mt       17.393 Mt   100.000 % 	 ..../TexturedRLPrep

		    1422.029 Mt    26.658 %      2458 x        0.579 Mt        0.651 Mt     1422.029 Mt   100.000 % 	 ..../GuiModelApply

	WindowThread:

		   51702.808 Mt   100.000 %         1 x    51702.808 Mt    51702.807 Mt    51702.808 Mt   100.000 % 	 /

	GpuThread:

		     156.336 Mt   100.000 %      2458 x        0.064 Mt        0.011 Mt      129.546 Mt    82.864 % 	 /

		       0.505 Mt     0.323 %         1 x        0.505 Mt        0.000 Mt        0.005 Mt     1.014 % 	 ./ScenePrep

		       0.500 Mt    98.986 %         1 x        0.500 Mt        0.000 Mt        0.001 Mt     0.147 % 	 ../AccelerationStructureBuild

		       0.437 Mt    87.520 %         1 x        0.437 Mt        0.000 Mt        0.437 Mt   100.000 % 	 .../BuildBottomLevelASGpu

		       0.062 Mt    12.333 %         1 x        0.062 Mt        0.000 Mt        0.062 Mt   100.000 % 	 .../BuildTopLevelASGpu

		      12.949 Mt     8.283 %      2458 x        0.005 Mt        0.005 Mt       12.949 Mt   100.000 % 	 ./Textured

		       9.367 Mt     5.992 %      2458 x        0.004 Mt        0.004 Mt        9.367 Mt   100.000 % 	 ./ImGui

		       3.969 Mt     2.539 %      2458 x        0.002 Mt        0.001 Mt        3.969 Mt   100.000 % 	 ./Present


	============================


