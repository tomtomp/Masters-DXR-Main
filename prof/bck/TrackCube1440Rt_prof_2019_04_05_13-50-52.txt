Configuration: 
Command line parameters  = E:\programming\FIT-DIP\Masters-DXR-Main-git\build\Measurement\x64\Debug\Measurement.exe --scene Cube/Cube.gltf --profile-track Cube/Cube.trk --rt-mode --width 2560 --height 1440 --profile-output TrackCube1440Rt 
Window width             = 2560
Window height            = 1421
Window title             = Direct3D App
Back-buffer count        = 3
Target FPS               = 0
Target UPS               = 60
Tearing                  = enabled
VSync                    = disabled
GUI rendering            = disabled
Debug layer              = enabled
GPU validation           = disabled
GPU profiling            = enabled
Render mode              = Ray Tracing
Loaded scene file        = Cube/Cube.gltf
Using testing scene      = disabled
Duplication              = 1
Duplication in cube      = disabled
Duplication offset       = 10
BLAS duplication         = disabled
Rays per pixel           = 9

ProfilingBuildAggregator: 
BuildBottomLevelAS	,	1.5383
BuildBottomLevelASGpu	,	0.174816
BuildTopLevelAS	,	2.303
BuildTopLevelASGpu	,	0.08512

ProfilingAggregator: 
Render	,	10.9478	,	8.1995	,	6.8236	,	10.67	,	11.9917	,	11.0476	,	10.6013	,	11.9381	,	9.9107	,	8.3602	,	10.5209	,	8.6047	,	8.0343	,	7.5874	,	7.4512	,	7.9411	,	9.0325	,	12.9807	,	13.2833	,	15.1296	,	13.6575	,	8.7194	,	8.0597	,	6.8543	,	11.2251	,	10.0009	,	6.8689	,	8.2406	,	5.6204
Update	,	0.4393	,	0.5716	,	0.6309	,	0.6369	,	0.3234	,	0.7115	,	0.4138	,	0.7899	,	0.5289	,	0.3584	,	0.697	,	0.3751	,	0.3692	,	0.3069	,	0.3074	,	0.3852	,	0.3131	,	0.7454	,	0.5596	,	0.9136	,	0.8571	,	0.4193	,	0.5067	,	0.3127	,	0.5439	,	0.7834	,	0.4747	,	0.6597	,	0.2928
RayTracing	,	3.6975	,	4.57434	,	4.13645	,	4.30989	,	4.67219	,	5.3225	,	5.20842	,	5.66048	,	6.51952	,	6.06045	,	5.17165	,	5.41738	,	4.93718	,	4.92614	,	4.83136	,	5.31443	,	6.38982	,	6.51459	,	7.29782	,	7.38406	,	6.3175	,	5.3255	,	4.94701	,	4.40848	,	3.93318	,	3.42656	,	3.1976	,	3.08701	,	3.10067
RayTracingRL	,	0.7736	,	1.3264	,	1.1007	,	1.25482	,	1.6479	,	2.05238	,	1.89299	,	2.16618	,	2.80646	,	2.55501	,	2.02413	,	2.17488	,	1.71699	,	1.64854	,	1.79018	,	2.12464	,	2.81862	,	2.97805	,	3.33171	,	3.41712	,	2.58301	,	1.78643	,	1.67312	,	1.28608	,	0.886304	,	0.660896	,	0.49248	,	0.396672	,	0.345216

FpsAggregator: 
FPS	,	112	,	122	,	117	,	110	,	106	,	103	,	98	,	102	,	91	,	93	,	102	,	94	,	99	,	112	,	112	,	98	,	95	,	88	,	81	,	76	,	82	,	96	,	102	,	117	,	121	,	128	,	140	,	138	,	143
UPS	,	59	,	60	,	60	,	60	,	60	,	61	,	60	,	61	,	60	,	60	,	61	,	60	,	61	,	60	,	60	,	60	,	61	,	60	,	60	,	61	,	60	,	61	,	60	,	60	,	60	,	61	,	60	,	61	,	60
FrameTime	,	8.94159	,	8.20512	,	8.5591	,	9.11551	,	9.45513	,	9.79149	,	10.2916	,	9.90266	,	11.0377	,	10.7658	,	9.88604	,	10.6584	,	10.1685	,	8.94236	,	8.99222	,	10.2601	,	10.5979	,	11.4027	,	12.4525	,	13.288	,	12.2797	,	10.4873	,	9.82481	,	8.55994	,	8.27723	,	7.89497	,	7.1859	,	7.30754	,	7.0304
GigaRays/s	,	3.66152	,	3.99017	,	3.82515	,	3.59166	,	3.46265	,	3.3437	,	3.18122	,	3.30617	,	2.96619	,	3.04109	,	3.31172	,	3.07174	,	3.21972	,	3.66121	,	3.64091	,	3.191	,	3.08927	,	2.87123	,	2.62918	,	2.46387	,	2.66618	,	3.12187	,	3.33236	,	3.82478	,	3.95541	,	4.14692	,	4.55612	,	4.48028	,	4.6569

AccelerationStructure: 
Ray tracing acceleration structure building statistics: 
	Bottom Level Acceleration Structure: 
		Maximum [B]: 6208
		Minimum [B]: 6208
		Currently Allocated [B]: 6208
		Currently Created [num]: 1
		Current Average [B]: 6208
		Maximum Triangles [num]: 12
		Minimum Triangles [num]: 12
		Total Triangles [num]: 12
		Maximum Meshes [num]: 1
		Minimum Meshes [num]: 1
		Total Meshes [num]: 1
	Top Level Acceleration Structure: 
		Maximum [B]: 64
		Minimum [B]: 64
		Currently Allocated [B]: 64
		Total Allocated [B]: 64
		Total Created [num]: 1
		Average Allocated [B]: 64
		Maximum Instances [num]: 1
		Minimum Instances [num]: 1
		Total Instances [num]: 1
	Scratch Buffer: 
		Maximum [B]: 3584
		Minimum [B]: 3584
		Currently Allocated [B]: 3584
		Total Allocated [B]: 3584
		Total Created [num]: 1
		Average Allocated [B]: 3584
ProfilingStack: 
[PROF] Profiling statistics: 
	Scopes entered : 32170
	Scopes exited : 32169
	Overhead per scope [ticks] : 1028
	Threads entered : 3
	Threads exited : 0
	============================
		         Sum[Mt]   Parent[%]  Samples[num]       Avg[Mt]   Last/Curr[Mt]        Self[Mt]     Self[%] 	 Name

	main:

		   29846.285 Mt   100.000 %         1 x    29846.288 Mt    29846.283 Mt        0.812 Mt     0.003 % 	 /

		   29845.615 Mt    99.998 %         1 x    29845.620 Mt    29845.614 Mt      405.102 Mt     1.357 % 	 ./Main application loop

		       2.898 Mt     0.010 %         1 x        2.898 Mt        0.000 Mt        2.898 Mt   100.000 % 	 ../TexturedRLInitialization

		       5.695 Mt     0.019 %         1 x        5.695 Mt        0.000 Mt        5.695 Mt   100.000 % 	 ../DeferredRLInitialization

		      68.270 Mt     0.229 %         1 x       68.270 Mt        0.000 Mt        2.732 Mt     4.002 % 	 ../RayTracingRLInitialization

		      65.537 Mt    95.998 %         1 x       65.537 Mt        0.000 Mt       65.537 Mt   100.000 % 	 .../PipelineBuild

		      11.790 Mt     0.040 %         1 x       11.790 Mt        0.000 Mt       11.790 Mt   100.000 % 	 ../ResolveRLInitialization

		     178.918 Mt     0.599 %         1 x      178.918 Mt        0.000 Mt        6.348 Mt     3.548 % 	 ../Initialization

		     172.570 Mt    96.452 %         1 x      172.570 Mt        0.000 Mt        2.554 Mt     1.480 % 	 .../ScenePrep

		     129.537 Mt    75.063 %         1 x      129.537 Mt        0.000 Mt       21.229 Mt    16.388 % 	 ..../SceneLoad

		      79.338 Mt    61.248 %         1 x       79.338 Mt        0.000 Mt       31.392 Mt    39.567 % 	 ...../glTF-LoadScene

		      47.947 Mt    60.433 %         2 x       23.973 Mt        0.000 Mt       47.947 Mt   100.000 % 	 ....../glTF-LoadImageData

		      12.659 Mt     9.772 %         1 x       12.659 Mt        0.000 Mt       12.659 Mt   100.000 % 	 ...../glTF-CreateScene

		      16.311 Mt    12.592 %         1 x       16.311 Mt        0.000 Mt       16.311 Mt   100.000 % 	 ...../SceneDesc-CreateScene

		       4.546 Mt     2.634 %         1 x        4.546 Mt        0.000 Mt        0.006 Mt     0.130 % 	 ..../TexturedRLPrep

		       4.540 Mt    99.870 %         1 x        4.540 Mt        0.000 Mt        1.611 Mt    35.473 % 	 ...../PrepareForRendering

		       2.626 Mt    57.830 %         1 x        2.626 Mt        0.000 Mt        2.626 Mt   100.000 % 	 ....../PrepareMaterials

		       0.304 Mt     6.698 %         1 x        0.304 Mt        0.000 Mt        0.304 Mt   100.000 % 	 ....../PrepareDrawBundle

		       7.764 Mt     4.499 %         1 x        7.764 Mt        0.000 Mt        0.005 Mt     0.058 % 	 ..../DeferredRLPrep

		       7.760 Mt    99.942 %         1 x        7.760 Mt        0.000 Mt        5.357 Mt    69.032 % 	 ...../PrepareForRendering

		       0.021 Mt     0.269 %         1 x        0.021 Mt        0.000 Mt        0.021 Mt   100.000 % 	 ....../PrepareMaterials

		       2.096 Mt    27.006 %         1 x        2.096 Mt        0.000 Mt        2.096 Mt   100.000 % 	 ....../BuildMaterialCache

		       0.286 Mt     3.692 %         1 x        0.286 Mt        0.000 Mt        0.286 Mt   100.000 % 	 ....../PrepareDrawBundle

		      15.627 Mt     9.055 %         1 x       15.627 Mt        0.000 Mt        5.213 Mt    33.357 % 	 ..../RayTracingRLPrep

		       0.246 Mt     1.577 %         1 x        0.246 Mt        0.000 Mt        0.246 Mt   100.000 % 	 ...../PrepareAccelerationStructures

		       0.028 Mt     0.176 %         1 x        0.028 Mt        0.000 Mt        0.028 Mt   100.000 % 	 ...../PrepareCache

		       0.874 Mt     5.590 %         1 x        0.874 Mt        0.000 Mt        0.874 Mt   100.000 % 	 ...../BuildCache

		       6.745 Mt    43.165 %         1 x        6.745 Mt        0.000 Mt        0.007 Mt     0.110 % 	 ...../BuildAccelerationStructures

		       6.738 Mt    99.890 %         1 x        6.738 Mt        0.000 Mt        2.897 Mt    42.991 % 	 ....../AccelerationStructureBuild

		       1.538 Mt    22.830 %         1 x        1.538 Mt        0.000 Mt        1.538 Mt   100.000 % 	 ......./BuildBottomLevelAS

		       2.303 Mt    34.179 %         1 x        2.303 Mt        0.000 Mt        2.303 Mt   100.000 % 	 ......./BuildTopLevelAS

		       2.522 Mt    16.135 %         1 x        2.522 Mt        0.000 Mt        2.522 Mt   100.000 % 	 ...../BuildShaderTables

		      12.541 Mt     7.267 %         1 x       12.541 Mt        0.000 Mt       12.541 Mt   100.000 % 	 ..../GpuSidePrep

		   29172.943 Mt    97.746 %      5752 x        5.072 Mt       50.219 Mt      952.991 Mt     3.267 % 	 ../MainLoop

		    1009.910 Mt     3.462 %      1749 x        0.577 Mt        0.306 Mt     1009.910 Mt   100.000 % 	 .../Update

		   27210.042 Mt    93.272 %      3079 x        8.837 Mt        6.144 Mt    27176.745 Mt    99.878 % 	 .../Render

		      18.480 Mt     0.068 %      3079 x        0.006 Mt        0.004 Mt       18.480 Mt   100.000 % 	 ..../DeferredRLPrep

		      14.817 Mt     0.054 %      3079 x        0.005 Mt        0.003 Mt       14.817 Mt   100.000 % 	 ..../RayTracingRLPrep

	WindowThread:

		   29835.607 Mt   100.000 %         1 x    29835.609 Mt    29835.607 Mt    29835.607 Mt   100.000 % 	 /

	GpuThread:

		   15081.484 Mt   100.000 %      3079 x        4.898 Mt        3.266 Mt      158.821 Mt     1.053 % 	 /

		       0.268 Mt     0.002 %         1 x        0.268 Mt        0.000 Mt        0.007 Mt     2.522 % 	 ./ScenePrep

		       0.261 Mt    97.478 %         1 x        0.261 Mt        0.000 Mt        0.001 Mt     0.417 % 	 ../AccelerationStructureBuild

		       0.175 Mt    66.973 %         1 x        0.175 Mt        0.000 Mt        0.175 Mt   100.000 % 	 .../BuildBottomLevelASGpu

		       0.085 Mt    32.610 %         1 x        0.085 Mt        0.000 Mt        0.085 Mt   100.000 % 	 .../BuildTopLevelASGpu

		   14920.805 Mt    98.935 %      3079 x        4.846 Mt        3.266 Mt        4.027 Mt     0.027 % 	 ./RayTracing

		     691.269 Mt     4.633 %      3079 x        0.225 Mt        0.054 Mt      691.269 Mt   100.000 % 	 ../DeferredRL

		    5042.698 Mt    33.796 %      3079 x        1.638 Mt        0.343 Mt     5042.698 Mt   100.000 % 	 ../RayTracingRL

		    9182.811 Mt    61.544 %      3079 x        2.982 Mt        2.867 Mt     9182.811 Mt   100.000 % 	 ../ResolveRL

		       1.590 Mt     0.011 %      3079 x        0.001 Mt        0.000 Mt        1.590 Mt   100.000 % 	 ./Present


	============================


