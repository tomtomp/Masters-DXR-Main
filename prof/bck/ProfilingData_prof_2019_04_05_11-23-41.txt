Configuration: 
Command line parameters  = E:\programming\FIT-DIP\Masters-DXR-Main-git\\build\Measurement\x64\Debug\Measurement.exe --scene Sponza/Sponza.gltf --duplication-cube 
Window width             = 1024
Window height            = 768
Window title             = Direct3D App
Back-buffer count        = 3
Target FPS               = 60
Target UPS               = 60
Tearing                  = disabled
VSync                    = disabled
GUI rendering            = enabled
Debug layer              = enabled
GPU validation           = disabled
GPU profiling            = enabled
Render mode              = Ray Tracing
Loaded scene file        = Sponza/Sponza.gltf
Using testing scene      = disabled
Duplication              = 1
Duplication in cube      = enabled
Duplication offset       = 10
BLAS duplication         = enabled
Rays per pixel           = 10

ProfilingBuildAggregator: 
BuildBottomLevelAS	,	2.8498
BuildBottomLevelASGpu	,	2.96154
BuildTopLevelAS	,	0.8144
BuildTopLevelASGpu	,	0.103808

ProfilingAggregator: 
Render	,	4.0485	,	10.6272	,	8.7259	,	7.6836	,	7.1129	,	7.0665
Update	,	0.2943	,	0.335	,	0.366	,	0.337	,	0.2903	,	0.2909
RayTracing	,	0	,	3.17395	,	2.37712	,	2.34749	,	2.10723	,	2.10406
RayTracingRL	,	0	,	1.99597	,	1.18749	,	1.1649	,	0.928832	,	0.926048

FpsAggregator: 
FPS	,	55	,	60	,	60	,	60	,	60	,	60
UPS	,	59	,	60	,	61	,	60	,	60	,	60
FrameTime	,	18.1818	,	16.753	,	16.6667	,	16.6667	,	16.6667	,	16.6667
GigaRays/s	,	0	,	0.469427	,	0.471858	,	0.471858	,	0.471858	,	0.471859

AccelerationStructure: 
Ray tracing acceleration structure building statistics: 
	Bottom Level Acceleration Structure: 
		Maximum [B]: 18391808
		Minimum [B]: 18391808
		Currently Allocated [B]: 18391808
		Currently Created [num]: 1
		Current Average [B]: 18391808
		Maximum Triangles [num]: 262267
		Minimum Triangles [num]: 262267
		Total Triangles [num]: 262267
		Maximum Meshes [num]: 103
		Minimum Meshes [num]: 103
		Total Meshes [num]: 103
	Top Level Acceleration Structure: 
		Maximum [B]: 3584
		Minimum [B]: 3584
		Currently Allocated [B]: 3584
		Total Allocated [B]: 3584
		Total Created [num]: 1
		Average Allocated [B]: 3584
	Scratch Buffer: 
		Maximum [B]: 14998528
		Minimum [B]: 14998528
		Currently Allocated [B]: 14998528
		Total Allocated [B]: 14998528
		Total Created [num]: 1
		Average Allocated [B]: 14998528
ProfilingStack: 
[PROF] Profiling statistics: 
	Scopes entered : 817446
	Scopes exited : 817445
	Overhead per scope [ticks] : 1033.9
	Threads entered : 3
	Threads exited : 0
	============================
		         Sum[Mt]   Parent[%]  Samples[num]       Avg[Mt]   Last/Curr[Mt]        Self[Mt]     Self[%] 	 Name

	main:

		   20673.493 Mt   100.000 %         1 x    20673.494 Mt    20673.492 Mt      567.754 Mt     2.746 % 	 /

		   20105.784 Mt    97.254 %         1 x    20105.786 Mt    20105.783 Mt     2575.628 Mt    12.810 % 	 ./Main application loop

		       3.726 Mt     0.019 %         1 x        3.726 Mt        0.000 Mt        3.726 Mt   100.000 % 	 ../TexturedRLInitialization

		      10.829 Mt     0.054 %         1 x       10.829 Mt        0.000 Mt       10.829 Mt   100.000 % 	 ../DeferredRLInitialization

		      79.150 Mt     0.394 %         1 x       79.150 Mt        0.000 Mt        2.428 Mt     3.068 % 	 ../RayTracingRLInitialization

		      76.721 Mt    96.932 %         1 x       76.721 Mt        0.000 Mt       76.721 Mt   100.000 % 	 .../PipelineBuild

		      15.306 Mt     0.076 %         1 x       15.306 Mt        0.000 Mt       15.306 Mt   100.000 % 	 ../ResolveRLInitialization

		   12517.673 Mt    62.259 %         1 x    12517.673 Mt        0.000 Mt        2.730 Mt     0.022 % 	 ../Initialization

		   12514.943 Mt    99.978 %         1 x    12514.943 Mt        0.000 Mt        2.613 Mt     0.021 % 	 .../ScenePrep

		   12434.476 Mt    99.357 %         1 x    12434.476 Mt        0.000 Mt     2323.404 Mt    18.685 % 	 ..../SceneLoad

		    9790.822 Mt    78.739 %         1 x     9790.822 Mt        0.000 Mt     1456.544 Mt    14.877 % 	 ...../glTF-LoadScene

		    8334.278 Mt    85.123 %        69 x      120.787 Mt        0.000 Mt     8334.278 Mt   100.000 % 	 ....../glTF-LoadImageData

		     245.153 Mt     1.972 %         1 x      245.153 Mt        0.000 Mt      245.153 Mt   100.000 % 	 ...../glTF-CreateScene

		      75.097 Mt     0.604 %         1 x       75.097 Mt        0.000 Mt       75.097 Mt   100.000 % 	 ...../SceneDesc-CreateScene

		       7.986 Mt     0.064 %         1 x        7.986 Mt        0.000 Mt        0.009 Mt     0.109 % 	 ..../TexturedRLPrep

		       7.977 Mt    99.891 %         1 x        7.977 Mt        0.000 Mt        1.246 Mt    15.623 % 	 ...../PrepareForRendering

		       3.227 Mt    40.447 %         1 x        3.227 Mt        0.000 Mt        3.227 Mt   100.000 % 	 ....../PrepareMaterials

		       3.504 Mt    43.930 %         1 x        3.504 Mt        0.000 Mt        3.504 Mt   100.000 % 	 ....../PrepareDrawBundle

		       9.480 Mt     0.076 %         1 x        9.480 Mt        0.000 Mt        0.006 Mt     0.060 % 	 ..../DeferredRLPrep

		       9.474 Mt    99.940 %         1 x        9.474 Mt        0.000 Mt        2.451 Mt    25.874 % 	 ...../PrepareForRendering

		       0.032 Mt     0.340 %         1 x        0.032 Mt        0.000 Mt        0.032 Mt   100.000 % 	 ....../PrepareMaterials

		       2.371 Mt    25.029 %         1 x        2.371 Mt        0.000 Mt        2.371 Mt   100.000 % 	 ....../BuildMaterialCache

		       4.619 Mt    48.757 %         1 x        4.619 Mt        0.000 Mt        4.619 Mt   100.000 % 	 ....../PrepareDrawBundle

		      22.452 Mt     0.179 %         1 x       22.452 Mt        0.000 Mt        4.668 Mt    20.792 % 	 ..../RayTracingRLPrep

		       0.798 Mt     3.554 %         1 x        0.798 Mt        0.000 Mt        0.798 Mt   100.000 % 	 ...../PrepareAccelerationStructures

		       0.045 Mt     0.201 %         1 x        0.045 Mt        0.000 Mt        0.045 Mt   100.000 % 	 ...../PrepareCache

		       8.161 Mt    36.350 %         1 x        8.161 Mt        0.000 Mt        8.161 Mt   100.000 % 	 ...../BuildCache

		       5.651 Mt    25.169 %         1 x        5.651 Mt        0.000 Mt        0.006 Mt     0.110 % 	 ...../BuildAccelerationStructures

		       5.645 Mt    99.890 %         1 x        5.645 Mt        0.000 Mt        1.981 Mt    35.087 % 	 ....../AccelerationStructureBuild

		       2.850 Mt    50.485 %         1 x        2.850 Mt        0.000 Mt        2.850 Mt   100.000 % 	 ......./BuildBottomLevelAS

		       0.814 Mt    14.427 %         1 x        0.814 Mt        0.000 Mt        0.814 Mt   100.000 % 	 ......./BuildTopLevelAS

		       3.128 Mt    13.934 %         1 x        3.128 Mt        0.000 Mt        3.128 Mt   100.000 % 	 ...../BuildShaderTables

		      37.936 Mt     0.303 %         1 x       37.936 Mt        0.000 Mt       37.936 Mt   100.000 % 	 ..../GpuSidePrep

		    4903.473 Mt    24.388 %    813281 x        0.006 Mt        7.831 Mt     1489.117 Mt    30.369 % 	 ../MainLoop

		     233.724 Mt     4.766 %       405 x        0.577 Mt        7.823 Mt      233.724 Mt   100.000 % 	 .../Update

		    3180.633 Mt    64.865 %       400 x        7.952 Mt        9.681 Mt     2931.128 Mt    92.155 % 	 .../Render

		       0.628 Mt     0.020 %        86 x        0.007 Mt        0.000 Mt        0.628 Mt   100.000 % 	 ..../TexturedRLPrep

		     244.220 Mt     7.678 %       400 x        0.611 Mt        0.494 Mt      244.220 Mt   100.000 % 	 ..../GuiModelApply

		       2.356 Mt     0.074 %       314 x        0.008 Mt        0.007 Mt        2.356 Mt   100.000 % 	 ..../DeferredRLPrep

		       2.300 Mt     0.072 %       314 x        0.007 Mt        0.008 Mt        2.300 Mt   100.000 % 	 ..../RayTracingRLPrep

	WindowThread:

		   20101.502 Mt   100.000 %         1 x    20101.502 Mt    20101.501 Mt    20101.502 Mt   100.000 % 	 /

	GpuThread:

		     953.438 Mt   100.000 %       400 x        2.384 Mt        2.458 Mt      154.293 Mt    16.183 % 	 /

		       3.075 Mt     0.322 %         1 x        3.075 Mt        0.000 Mt        0.007 Mt     0.230 % 	 ./ScenePrep

		       3.067 Mt    99.770 %         1 x        3.067 Mt        0.000 Mt        0.002 Mt     0.070 % 	 ../AccelerationStructureBuild

		       2.962 Mt    96.546 %         1 x        2.962 Mt        0.000 Mt        2.962 Mt   100.000 % 	 .../BuildBottomLevelASGpu

		       0.104 Mt     3.384 %         1 x        0.104 Mt        0.000 Mt        0.104 Mt   100.000 % 	 .../BuildTopLevelASGpu

		      11.764 Mt     1.234 %        86 x        0.137 Mt        0.000 Mt       11.764 Mt   100.000 % 	 ./Textured

		       3.377 Mt     0.354 %       400 x        0.008 Mt        0.010 Mt        3.377 Mt   100.000 % 	 ./ImGui

		       0.631 Mt     0.066 %       400 x        0.002 Mt        0.001 Mt        0.631 Mt   100.000 % 	 ./Present

		     780.298 Mt    81.841 %       314 x        2.485 Mt        2.445 Mt        0.701 Mt     0.090 % 	 ./RayTracing

		     142.157 Mt    18.218 %       314 x        0.453 Mt        0.453 Mt      142.157 Mt   100.000 % 	 ../DeferredRL

		     401.975 Mt    51.516 %       314 x        1.280 Mt        1.262 Mt      401.975 Mt   100.000 % 	 ../RayTracingRL

		     235.465 Mt    30.176 %       314 x        0.750 Mt        0.727 Mt      235.465 Mt   100.000 % 	 ../ResolveRL


	============================


