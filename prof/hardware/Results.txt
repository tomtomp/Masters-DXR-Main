Hardware accelerated: 
			width	height	fps	rpp	rpp_correct	rps	rps_correct	build_bottom_cpu	build_bottom_gpu	build_top_cpu	build_top_gpu	fps_raster
Box 768			1024	768	300.37	66	32.2		15.591	7.601		0.836			0.182			0.631		0.083		3523.13
Box 1440		2560	1377	141.49	66	25.7		32.919	12.818		0.973			0.182			0.634		0.084		3239.85
Suzanne 768		1028	768	274.67	66	16.6		14.257	3.586		1.338			0.628			0.881		0.084		3457.03
Suzanne 1440	    	2560	1377	122.89	66	12.7		28.591	5.502		0.919			0.627			0.590		0.084		3179.71
Sponza 768		1024	768	107.42	66	66.0		5.576	5.576		3.414			8.602			0.993		0.090		671.48
Sponza 1440		2560	1377	30.46	66	66.0		7.087	7.087		4.741			25.032			2.143		0.620		537.34
Sponza Miss		2560	1377	55.48	64	64.0		12.517	12.517		4.553			25.493			1.681		0.618		537.34
Sponza Cast		2560	1377	27.68	64	64.0		6.245	6.245		5.228			23.685			2.287		0.218		537.34

This data has been measures on computer WITH hardware support for ray tracing, through the use of DirectX Ray Tracing API.
Hardware is following: 
CPU : Intel Xeon W 2135 6 core
GPU : RTX 2080Ti

The name for each line corresponds to the used scene configuration, for which a screenshot can be found in the 
/prof/ directory - e.g. "Box 1440" camera configuration can be found in the "Box_1440.png" file.

Legend:
	width/height : Resolution of the render target.
	fps : Frames per second, calculated from average per-frame time.
	rpp : Rays per pixel, for which the primary ray hits geometry.
	rpp_correct : Rays per pixel, corrected by weighing the percentage of pixels for which the primary ray hits
		rpp_correct = rays_when_hit * hit_ratio + rays_when_miss * miss_ratio
		hit_ratio and miss_ratio taken from the table below.
	rps : Rays per second, for the uncorrected rays per pixel
		rps = width * height * fps * rpp
	rps_correct : Corrected rays per second.
		rps_correct = width * height * fps * rpp_correct
	build_bottom/top_cpu : Build time of the bottom level acceleration structure, on the CPU. Value in milliseconds.
	build_bottom/top_gpu : Build time of the bottom level acceleration structure, on the GPU. Value in milliseconds.
	fps_raster : Frames per second when rendering using plain textured rasterization pass.
	
			miss	hit	triangles
Box 768			0.52	0.48	12
Box 1440		0.62	0.38	12
Suzanne 768		0.76	0.24	3936
Suzanne 1440		0.82	0.18	3936
Sponza 768		0.00	1.00	262267
Sponza 1440		0.00	1.00	262267

Legend: 
	miss : Percentage of pixels which contain miss for the primary ray.
	hit : Percentage of pixels which contain hit for the primary ray.
	triangles : Number of triangles in the target scene.