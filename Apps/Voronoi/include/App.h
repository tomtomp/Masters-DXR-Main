/**
* @file App.h
* @author Tomas Polasek
* @brief Main application header.
*/

#pragma once

#include "Engine.h"
#include "engine/application/Application.h"
#include "engine/application/ApplicationGui.h"

#include "engine/renderer/imGuiRL/ImGuiRL.h"
#include "engine/renderer/voronoiRL/VoronoiRL.h"

#include "engine/input/KeyBinding.h"

#include "engine/scene/systems/CameraSys.h"
#include "engine/scene/logic/FreeLookController.h"
#include "engine/scene/logic/ScriptedCameraController.h"

/// Namespace containing the project.
namespace dxr
{

/// Application specific options should be added here.
class AppSpecificConfig : public quark::EngineRuntimeConfig
{
public:
    AppSpecificConfig();

    // Start of options section

    /// Periodically print out profiler information.
    bool specificPrintProfiler{ false };
    /// Should the periodic profiler skip inactive scopes?
    bool specificPrintProfilerSkipInactive{ true };
    /// GlTF model for the primary scene.
    std::string glTFSceneFile{ "" };

    // End of options section
}; // class AppSpecificRuntimeConfig

/// Main application class.
class App : public quark::app::Application<App, AppSpecificConfig>
{
public:
    /// Constructor called by the engine.
    App(ConfigT &cfg, quark::Engine &engine);

    /// Wait for all GPU command queues and free any resources.
    virtual ~App();

    virtual void initialize() override final;
    //virtual void destroy() override final;
    virtual int32_t run() override final;
    virtual void update(Milliseconds delta) override final;
    virtual void render() override final;
    //virtual void processEvents() override final;

    void onEvent(const KeyboardEvent &event) const;
    void onEvent(const MouseButtonEvent &event) const;
    void onEvent(const MouseMoveEvent &event) const;
    void onEvent(const MouseScrollEvent &event) const;
    void onEvent(const ResizeEvent &event);
    void onEvent(const ExitEvent &event);

    static void registerComponents();
private:
    /// Default scene file loaded when none is provided.
    static constexpr const char *DEFAULT_SCENE_FILE{ "Cube/Cube.gltf" };
    /// Default path which will be searched for provided models.
    static constexpr const char *DEFAULT_SCENE_SEARCH_PATH_PRIMARY{ "models/" };
    /// Backup scene path, which works when the executable is in the build hierarchy.
    static constexpr const char *DEFAULT_SCENE_SEARCH_PATH_SECONDARY{ "../../../../models/" };

    /// Default FOV of the camera.
    static constexpr float DEFAULT_CAMERA_FOV{ 80.0f };
    /// Default width of the viewport, before window size changes.
    static constexpr float DEFAULT_VIEWPORT_WIDTH{ 1024.0f };
    /// Default height of the viewport, before window size changes.
    static constexpr float DEFAULT_VIEWPORT_HEIGHT{ 768.0f };

    /// Mouse sensitivity for camera rotation control.
    static constexpr float DEFAULT_MOUSE_SENSITIVITY_MULT{ 0.01f };
    /// How many units does the camera move per one time unit?
    static constexpr float DEFAULT_MOVEMENT_UNITS{ 0.005f };
    /// Unit multiplier when sprinting.
    static constexpr float DEFAULT_MOVEMENT_SPRINT_MULTIPLIER{ 5.0f };

    /// Default camera distance from origin
    static constexpr float DEFAULT_CAMERA_DISTANCE{ 5.0f };

    /**
     * Load the default empty scene.
     * @param filename File which contains the scene.
     * @return Returns handle to the created scene.
     */
    quark::res::scene::SceneHandle createPrimaryScene(const std::string &filename);

    /**
     * Prepare the default empty scene.
     * @param filename File which contains the scene.
     * @return Returns handle to the created scene.
     */
    quark::res::scene::SceneHandle preparePrimaryScene(const std::string &filename);

    /**
     * Prepare for rendering of the user selected scene, specified within 
     * the runtime configuration.
     */
    void prepareSceneRendering();

    /// Prepare the camera for scene display.
    void prepareCamera();

    /// Setup key bindings for controlling camera etc.
    void setupInputBindings();

    /// Setup common GUI elements, base GUI context and initialize render layer GUIs.
    void setupGui();

    /// Common GUI elements and main window.
    void setupCommonGui();

    /**
     * Check whether a camera has bee already selected, selecting 
     * the first one if it was not.
     */
    void checkChooseCamera();

    /**
     * Update the current state of the GUI variables and reflect 
     * any changes.
     * @param delta Delta time from the last call.
     */
    void updateGui(Milliseconds delta);

    /// Update the common GUI variables.
    void updateCommonGui();

    /// Convert rotational quaternion to yaw pitch roll.
    quark::dxtk::math::Vector3 quaternionToYawPitchRoll(const quark::dxtk::math::Quaternion &rot);
    /// Convert yaw pitch roll to rotational quaternion.
    quark::dxtk::math::Quaternion yawPitchRollToQuaternion(const quark::dxtk::math::Vector3 &ypr);

    /// Main binding scheme.
    quark::input::KeyBinding mBinding;

    /// Timer for counting fps/ups.
    quark::app::FpsUpdateTimer mFpsTimer;
    /// Once per how many ms should update run?
    Milliseconds mUpdateDeltaMs{ 0.0f };
    /// Once per how many ms should render run?
    Milliseconds mRenderDeltaMs{ 0.0f };

    /// Wrapper for common GUI variables.
    quark::app::ApplicationGui mAppGui;

    /// ImGUI rendering layer.
    quark::rndr::ImGuiRL::PtrT mImGuiRl;
    /// Rendering layer for G-Buffer generation.
    quark::rndr::DeferredRL::PtrT mDeferredRl;
    /// Voronoi rendering layer.
    quark::rndr::VoronoiRL::PtrT mVoronoiRl;
    /// Shadow map rendering layer.
    quark::rndr::ShadowMapRL::PtrT mShadowMapRl;
    /// Ambient occlusion rendering layer.
    quark::rndr::AmbientOcclusionRL::PtrT mAmbientOcclusionRl;

    /// Camera management system.
    quark::scene::sys::CameraSys *mCameraSys{ nullptr };
    /// Controller for chosen camera.
    quark::scene::logic::FreeLookController mCameraController{ };

    /// Container for rendering configuration.
    struct Configuration
    {
        /// Current width of the main window.
        float windowWidth{ DEFAULT_VIEWPORT_WIDTH };
        /// Current height of the main window.
        float windowHeight{ DEFAULT_VIEWPORT_HEIGHT };
    }; // struct Configuration

    /// Configuration of the current scene rendering.
    Configuration mC;
protected:
}; // class App
    
} // namespace App
