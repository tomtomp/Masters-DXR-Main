/**
* @file App.cpp
* @author Tomas Polasek
* @brief Main application header.
*/

#include "stdafx.h"

#include "App.h"
#include "scene/loaders/GlTFLoader.h"

namespace dxr
{

AppSpecificConfig::AppSpecificConfig()
{
    mParser.addOption<bool>(L"--prof", [&] (const bool &val)
    {
        specificPrintProfiler = val;
    }, L"Enable/disable periodic profiler information printing.");
    mParser.addOption<bool>(L"--prof-skip-inactive", [&] (const bool &val)
    {
        specificPrintProfilerSkipInactive = val;
    }, L"Enable/disable skipping of inactive scopes for the periodic profiler.");
    mParser.addOption<std::wstring>(L"--scene", [&] (const std::wstring &val)
    {
        glTFSceneFile = util::toString(val);
    }, L"Choose which glTF scene should be displayed, default "
       L"value loads a cube scene. Provided path should be "
       L"relative to the models/ folder!");
}

App::App(ConfigT &cfg, quark::Engine &engine) :
    Application(cfg, engine),
    mFpsTimer(false), 
    mAppGui(cfg), 
    mImGuiRl{ quark::rndr::ImGuiRL::create(engine.renderer()) },
    mDeferredRl{ quark::rndr::DeferredRL::create(engine.renderer()) }, 
    mVoronoiRl{ quark::rndr::VoronoiRL::create(engine.renderer()) }, 
    mShadowMapRl{ quark::rndr::ShadowMapRL::create(engine.renderer()) },
    mAmbientOcclusionRl{ quark::rndr::AmbientOcclusionRL::create(engine.renderer()) }
{ }

App::~App()
{ mEngine.renderer().flushAll(); }

void App::initialize()
{
    PROF_SCOPE("Initialization");

    // Prepare the user-selected scene for rendering.
    prepareSceneRendering();

    // Prepare display camera of the scene.
    prepareCamera();

    // Input binding to control the camera etc.
    setupInputBindings();

    // Create common GUI elements, base GUI context and initialize render layer GUIs.
    setupGui();
}

int32_t App::run()
{
    // Profiling printer.
    util::prof::PrintCrawler pc(mCfg.specificPrintProfilerSkipInactive);

    // Target number of milliseconds between updates.
    mUpdateDeltaMs = updateDelta();
    util::DeltaTimer updateTimer;

    // Target number of milliseconds between renders.
    mRenderDeltaMs = renderDelta();
    util::DeltaTimer renderTimer;

    mFpsTimer.reset();

    util::TickTimer debugPrintTimer(Seconds(1u), [&] ()
    {
        if (mCfg.specificPrintProfiler)
        {
            PROF_DUMP(pc);
        }
        mFpsTimer.printInfo();
    });

    util::TimerTicker ticker;
    ticker.addTimer(updateTimer);
    ticker.addTimer(renderTimer);
    ticker.addTimer(mFpsTimer);
    ticker.addTimer(debugPrintTimer);

    // Start the main loop.
    mRunning = true;

    while (mRunning)
    { // Main application loop
        PROF_SCOPE("MainLoop");

        ticker.tick();

        while (updateTimer.tryTake(mUpdateDeltaMs))
        { // While there is updating to do, 
            PROF_SCOPE("Update");
            update(mUpdateDeltaMs);
            mFpsTimer.update();
        }

        if (renderTimer.tryTake(mRenderDeltaMs))
        { // Target time between renders exceeded.
            PROF_SCOPE("Render");
            render();
            mFpsTimer.frame();
            renderTimer.takeAll();
        }
    }

    return EXIT_SUCCESS;
}

void App::update(Milliseconds delta)
{
    // Process any events gathered on the message bus.
    processEvents(delta);

    // Make sure entities within the scene are ready.
    mEngine.scene().refresh();

    // Perform update logic, running any scene systems: 

    mCameraSys->refresh();
    checkChooseCamera();
    mCameraController.update(delta.count());

    // End of update logic.

    // Update GUI variables.
    updateGui(delta);
}

void App::render()
{
    auto cmdList{ clear(quark::dxtk::math::Color{0.0f, 0.0f, 0.0f}) };

    auto &swapChain{ mEngine.renderer().swapChain() };

    { PROF_SCOPE("Rasterization"); GPU_PROF_SCOPE(*cmdList, "RasterizationGpu");
        mDeferredRl->prepare();
        mDeferredRl->render(
            mCameraSys->camera(), 
            *cmdList);
            
        mShadowMapRl->setEnabled(true);
        mShadowMapRl->lights()
            .clear()
            .pushLight(mVoronoiRl->getLightDirection());
        mShadowMapRl->prepare();
        mShadowMapRl->render(
            mCameraSys->camera(), 
            *cmdList);

        mAmbientOcclusionRl->setEnabled(true);
        mAmbientOcclusionRl->setDeferredBuffers(
            mDeferredRl->getDeferredBuffers());
        mAmbientOcclusionRl->render(
            mCameraSys->camera(),
            *cmdList);

        mVoronoiRl->prepare();
        mVoronoiRl->setDeferredBuffers(
            mDeferredRl->getDeferredBuffers());
        mVoronoiRl->setShadowMapBuffers(
            mShadowMapRl->shadowMapBuffers(), 
            mShadowMapRl->lights());
        mVoronoiRl->setAmbientOcclusionBuffers(
            mAmbientOcclusionRl->getAmbientOcclusionBuffers());
        mVoronoiRl->render(
            mCameraSys->camera(), 
            swapChain.currentBackbufferRtv(), 
            *cmdList);
    }

    if (gui().enabled())
    { GPU_PROF_SCOPE(*cmdList, "ImGui");
        mImGuiRl->render(
            swapChain.currentBackbufferRtv(), 
            gui().model(), 
            *cmdList);
    }

    present(cmdList);
}

void App::onEvent(const KeyboardEvent &event) const
{ mBinding.processEvent(event); }

void App::onEvent(const MouseButtonEvent &event) const
{ mBinding.processEvent(event); }

void App::onEvent(const MouseMoveEvent &event) const
{ mBinding.processEvent(event); }

void App::onEvent(const MouseScrollEvent &event) const
{ mBinding.processEvent(event); }

void App::onEvent(const ResizeEvent &event)
{
    log<Info>() << "Resizing to: " << event.newWidth << "x" << event.newHeight << std::endl;

    mAppGui.updateResolution(event);

    if (event.newWidth == 0u || event.newHeight == 0u)
    {
        log<Info>() << "Skipping resize, since resolution is 0!" << std::endl;
        return;
    }

    { // Before resizing the swap chain: 
        mImGuiRl->resizeBeforeSwapChain(event.newWidth, event.newHeight);
    }

    mEngine.renderer().resize(event.newWidth, event.newHeight);

    { // After resizing the swap chain: 
        mImGuiRl->resizeAfterSwapChain();
        mDeferredRl->resize(event.newWidth, event.newHeight);;
        mAmbientOcclusionRl->resize(event.newWidth, event.newHeight);
        mVoronoiRl->resize(event.newWidth, event.newHeight);

        mCameraController.setViewportSize(
            static_cast<float>(event.newWidth), static_cast<float>(event.newHeight));
    }

    mC.windowWidth = static_cast<float>(event.newWidth);
    mC.windowHeight = static_cast<float>(event.newHeight);
}

void App::onEvent(const quark::app::events::ExitEvent &event)
{ mRunning = false; }

void App::registerComponents()
{ /* Unused */ }

quark::res::scene::SceneHandle App::createPrimaryScene(const std::string &sceneFile)
{
    try
    {
        quark::res::File sceneFilePath;
        if (!sceneFile.empty())
        { sceneFilePath = quark::res::File(sceneFile, { DEFAULT_SCENE_SEARCH_PATH_PRIMARY, DEFAULT_SCENE_SEARCH_PATH_SECONDARY }); }
        else
        { sceneFilePath = quark::res::File(DEFAULT_SCENE_FILE, { DEFAULT_SCENE_SEARCH_PATH_PRIMARY, DEFAULT_SCENE_SEARCH_PATH_SECONDARY }); }
        return mEngine.scene().loadGlTFScene(sceneFilePath);
    } catch (const quark::res::File::FileNotFoundException &e)
    {
        log<Error>() << "Unable to find scene file \"" << 
            (sceneFile.empty() ? DEFAULT_SCENE_FILE : sceneFile) << 
            "\"! Make sure it is in the \"" << DEFAULT_SCENE_SEARCH_PATH_PRIMARY << 
            "\" folder!: " << e.what() << std::endl;
    } catch (const quark::scene::loaders::BaseSceneLoader::SceneLoadException &e)
    {
        log<Error>() << "Unable to load the scene!: \n" << e.what() << std::endl;
    }

    return mEngine.scene().loadEmptyScene();
}

quark::res::scene::SceneHandle App::preparePrimaryScene(const std::string &sceneFile)
{
    PROF_SCOPE("SceneLoad");

    auto scene{ createPrimaryScene(sceneFile) };

    mEngine.scene().prepareScene(scene);
    mEngine.scene().setCurrentScene(scene);
    mEngine.scene().refresh();

    return scene;
}

void App::prepareSceneRendering()
{ 
    PROF_SCOPE("ScenePrep");

    auto scene{ preparePrimaryScene(mCfg.glTFSceneFile) };
    const auto toDraw{ mEngine.scene().getToDraw() };
    auto directCmdList{ mEngine.renderer().directCmdQueue().getCommandList() };

    { GPU_PROF_SCOPE(*directCmdList, "ScenePrep");
        mDeferredRl->setDrawablesList(toDraw);
        mDeferredRl->prepare();

        mVoronoiRl->prepare();

        mShadowMapRl->setDrawablesList(toDraw);
        mShadowMapRl->prepare();
    }

    { PROF_SCOPE("GpuSidePrep");
        log<Info>() << "Executing preparation command list!" << std::endl;
        directCmdList->executeNoWait();
        mEngine.renderer().flushCopyCommands();
    }

    log<Info>() << "Finished preparation!" << std::endl;
}

void App::prepareCamera()
{
    mCameraSys = mEngine.scene().addGetSystem<quark::scene::sys::CameraSys>();
    if (!mCameraSys)
    { throw std::runtime_error("Failed to create the camera management system!"); }

    mCameraController.setCameraSystem(*mCameraSys);
    mCameraController.setFov(DEFAULT_CAMERA_FOV);
    mCameraController.setViewportSize(DEFAULT_VIEWPORT_WIDTH, DEFAULT_VIEWPORT_HEIGHT);
}

void App::setupInputBindings()
{
    // Escape to exit the program.
    mBinding.kb().setAction(
        Key::Escape,
        KeyMods::None,
        KeyAction::Press,
        [&]()
    {
        exit();
    });

    // Mouse movement to control the camera rotation.
    mBinding.mouse().setMoveAction(
        [&] (const MouseMoveEvent &event)
    {
        // Remember last position, for delta computation.
        static auto lastPos{ event.pos };

        // Calculate the new camera rotation.
        if(event.buttonPressed(MouseButton::LButton))
        {
            mCameraController.setRotation(
                mCameraController.rotationFromMouseDelta(
                    static_cast<float>(event.pos.x - lastPos.x) * DEFAULT_MOUSE_SENSITIVITY_MULT, 
                    static_cast<float>(event.pos.y - lastPos.y) * DEFAULT_MOUSE_SENSITIVITY_MULT));
        }

        lastPos = event.pos;
    });

    // Sprint when holding down b
    mBinding.kb().setAction(
        Key::B, 
        KeyMods::None, 
        KeyAction::Press, 
        [&] ()
    {
        mCameraController.speedModifier(DEFAULT_MOVEMENT_SPRINT_MULTIPLIER);
    });
    mBinding.kb().setAction(
        Key::B, 
        KeyMods::None, 
        KeyAction::Release, 
        [&] ()
    {
        mCameraController.speedModifier(1.0f);
    });

    // Move the camera forward.
    mBinding.kb().setAction(
        Key::W, 
        KeyMods::None, 
        KeyAction::Press, 
        [&] ()
    {
        mCameraController.forward(DEFAULT_MOVEMENT_UNITS);
    });
    mBinding.kb().setAction(
        Key::W, 
        KeyMods::None, 
        KeyAction::Release, 
        [&] ()
    {
        mCameraController.forward(0.0f);
    });

    // Move the camera backward.
    mBinding.kb().setAction(
        Key::S, 
        KeyMods::None, 
        KeyAction::Press, 
        [&] ()
    {
        mCameraController.backward(DEFAULT_MOVEMENT_UNITS);
    });
    mBinding.kb().setAction(
        Key::S, 
        KeyMods::None, 
        KeyAction::Release, 
        [&] ()
    {
        mCameraController.backward(0.0f);
    });

    // Move the camera left.
    mBinding.kb().setAction(
        Key::A, 
        KeyMods::None, 
        KeyAction::Press, 
        [&] ()
    {
        mCameraController.left(DEFAULT_MOVEMENT_UNITS);
    });
    mBinding.kb().setAction(
        Key::A, 
        KeyMods::None, 
        KeyAction::Release, 
        [&] ()
    {
        mCameraController.left(0.0f);
    });

    // Move the camera right.
    mBinding.kb().setAction(
        Key::D, 
        KeyMods::None, 
        KeyAction::Press, 
        [&] ()
    {
        mCameraController.right(DEFAULT_MOVEMENT_UNITS);
    });
    mBinding.kb().setAction(
        Key::D, 
        KeyMods::None, 
        KeyAction::Release, 
        [&] ()
    {
        mCameraController.right(0.0f);
    });

    // Move the camera up.
    mBinding.kb().setAction(
        Key::Space, 
        KeyMods::None, 
        KeyAction::Press, 
        [&] ()
    {
        mCameraController.up(DEFAULT_MOVEMENT_UNITS);
    });
    mBinding.kb().setAction(
        Key::Space, 
        KeyMods::None, 
        KeyAction::Release, 
        [&] ()
    {
        mCameraController.up(0.0f);
    });

    // Move the camera down.
    mBinding.kb().setAction(
        Key::C, 
        KeyMods::None, 
        KeyAction::Press, 
        [&] ()
    {
        mCameraController.down(DEFAULT_MOVEMENT_UNITS);
    });
    mBinding.kb().setAction(
        Key::C, 
        KeyMods::None, 
        KeyAction::Release, 
        [&] ()
    {
        mCameraController.down(0.0f);
    });

    // Switch GUI rendering on/off.
    mBinding.kb().setAction(
        Key::G, 
        KeyMods::None, 
        KeyAction::Press, 
        [&]()
    { 
        mCfg.guiEnable = !mCfg.guiEnable;
        gui().setEnabled(mCfg.guiEnable); 
    });
}

void App::setupGui()
{
    mAppGui.prepareGui(gui());
    setupCommonGui();
    mDeferredRl->prepareGui(gui());
    mShadowMapRl->prepareGui(gui());
    mAmbientOcclusionRl->prepareGui(gui());
    mVoronoiRl->prepareGui(gui());
}

void App::setupCommonGui()
{
    gui().addGuiContext("/Common/").setType(quark::gui::GuiContextType::Category).setLabel("Common Settings");

    gui().addGuiVariable<quark::gui::InputInt>("TargetFps", "/Common/", { mCfg.appTargetFps })
        .setLabel("Target FPS")
        .cfg().setMin(0);
    gui().addGuiVariable<quark::gui::InputInt>("TargetUps", "/Common/", { mCfg.appTargetUps })
        .setLabel("Target UPS")
        .cfg().setMin(1);

    gui().addGuiContext("/Camera/").setType(quark::gui::GuiContextType::Category).setLabel("Camera Settings");

    gui().addGuiVariable<quark::gui::Position3>("CameraPosition", "/Camera/", { })
        .setLabel("Position");
    gui().addGuiVariable<quark::gui::Position3>("CameraRotation", "/Camera/", { })
        .setLabel("Rotation");
}

void App::checkChooseCamera()
{
    if (!mCameraSys->cameraChosen())
    {
        log<Info>() << "No camera has been chosen!" << std::endl;
        const auto availableCameras{ mCameraSys->numAvailableCameras() };
        if (availableCameras == 0u)
        {
            log<Warning>() << "No cameras are available in the current scene!" << std::endl;
            return;
        }

        log<Info>() << "Choosing the first camera from " << availableCameras << " total cameras in the current scene." << std::endl;
        mCameraSys->chooseCamera(0u);

        mCameraController.position(quark::dxtk::math::Vector3(0.0f, 0.0f, DEFAULT_CAMERA_DISTANCE));
    }
}

void App::updateGui(Milliseconds delta)
{
    gui().setEnabled(mCfg.guiEnable);

    mAppGui.updateFpsTimer(mFpsTimer);
    mAppGui.updateGui(gui());

    updateCommonGui();

    mDeferredRl->updateGui(gui());
    mShadowMapRl->updateGui(gui());
    mAmbientOcclusionRl->updateGui(gui());
    mVoronoiRl->updateGui(gui());
}

void App::updateCommonGui()
{
    auto &targetFpsVar{ gui().getVariable<quark::gui::InputInt>("TargetFps") };
    if (targetFpsVar.valueChanged)
    {
        mCfg.appTargetFps = targetFpsVar.value;
        mCfg.appUseVSync = mCfg.appTargetFps != 0u;
        mCfg.appUseTearing = mCfg.appTargetFps == 0u;

        mRenderDeltaMs = renderDelta();
    }
    auto &targetUpsVar{ gui().getVariable<quark::gui::InputInt>("TargetUps") };
    if (targetUpsVar.valueChanged)
    {
        mCfg.appTargetUps = targetUpsVar.value;

        mUpdateDeltaMs = updateDelta();
    }

    if (mCameraSys->cameraChosen())
    {
        auto &transform{ mCameraSys->chosenCameraTransform() };
        auto translation{ transform.localTransform.translate() };
        auto rotation{ quaternionToYawPitchRoll(transform.localTransform.rotate()) };

        auto &translationVar{ gui().getVariable<quark::gui::Position3>("CameraPosition") };
        auto &rotationVar{ gui().getVariable<quark::gui::Position3>("CameraRotation") };

        if (quark::gui::synchronizeVariables(translationVar, translation))
        {
            transform.localTransform.setTranslate(translation);
            mCameraSys->chosenCameraTransformChanged();
        }

        if (rotationVar.valueChanged)
        {
            rotation = static_cast<quark::dxtk::math::Vector3>(rotationVar);
            transform.localTransform.setRotate(yawPitchRollToQuaternion(rotation));
            mCameraSys->chosenCameraTransformChanged();
        }
        else
        { rotationVar.value = static_cast<quark::gui::Position3>(rotation); }
    }
}

quark::dxtk::math::Vector3 App::quaternionToYawPitchRoll(
    const quark::dxtk::math::Quaternion &rot)
{
    // Source: https://en.wikipedia.org/wiki/Conversion_between_quaternions_and_Euler_angles
    const auto sinrCosp{ 2.0f * (rot.w * rot.x + rot.y * rot.z) };
    const auto cosrCosp{ 1.0f - 2.0f * (rot.x * rot.x + rot.y * rot.y) };
    const auto roll = atan2(sinrCosp, cosrCosp);

    const auto sinp{ 2.0f * (rot.w * rot.y - rot.z * rot.x) };
    auto pitch{ 0.0f };
    if (fabs(sinp) >= 1.0f)
    { pitch = copysign(M_FPI / 2.0f, sinp); }
    else
    { pitch = asin(sinp); }

    const auto sinyCosp{ 2.0f * (rot.w * rot.z + rot.x * rot.y) };
    const auto cosyCosp{ 1.0f - 2.0f * (rot.y * rot.y + rot.z * rot.z) };
    const auto yaw{ atan2(sinyCosp, cosyCosp) };

    return { yaw, pitch, roll };
}

quark::dxtk::math::Quaternion App::yawPitchRollToQuaternion(
    const quark::dxtk::math::Vector3 &ypr)
//{ return quark::dxtk::math::Quaternion::CreateFromYawPitchRoll(ypr.x, ypr.y, ypr.z); }
{
    const auto q{ quark::dxtk::math::Quaternion::CreateFromYawPitchRoll(ypr.x, ypr.y, ypr.z) };
    return { q.z, q.x, q.y, q.w };
}

}
