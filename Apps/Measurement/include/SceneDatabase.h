/**
* @file SceneDatabase.h
* @author Tomas Polasek
* @brief Helper class used for management 
* of available scene files.
*/

#pragma once

#include "engine/util/Types.h"

/// Namespace containing the project.
namespace dxr
{

/**
 * Helper class used for management of available scene files.
 */
class SceneDatabase
{
public:
    /// Exception thrown when scene does not exist in the DB.
    struct NoSceneException : public std::exception
    {
        NoSceneException(const char *msg) : 
            std::exception(msg)
        { }
    }; // struct NoSceneExecption

    /// Information about a scene.
    struct SceneInfo
    {
        /// Human readable name.
        std::string name{ };
        /// Relative path.
        std::string path{ };
    }; // struct SceneInfo

    /// Create empty scene database.
    SceneDatabase();
    /// Free the resources.
    ~SceneDatabase();

    /**
     * Manually specify a scene for the database.
     * @param name Human readable name of the scene.
     * @param path Path to the scene file.
     */
    void registerScene(const std::string &name, const std::string &path);

    // TODO - Search for scene files automatically.

    /// Get number of scenes registered.
    std::size_t size() const;

    /// Access scene information, index must be < size().
    const SceneInfo &operator[](std::size_t index) const;

    /**
     * Access the currently selected scene.
     * @return Returns reference to the currently 
     * selected scene.
     * @throws NoSceneException Thrown when no scene 
     * is currently selected.
     */
    const SceneInfo &currentScene() const;

    /**
     * Get index of the currently selected scene.
     * @return Returns index to the currently selected 
     * scene.
     * @throws NoSceneException Thrown when no scene 
     * is currently selected.
     */
    std::size_t currentSceneIndex() const;

    /**
     * Select scene by index.
     * @param index Index of the selected scene.
     * @throws NoSceneException Thrown when the scene 
     * index is out of range.
     */
    void selectScene(std::size_t index);

    /**
     * Select scene by its path.
     * @param path Path to the scene file.
     * @return Returns true if a scene with given path 
     * has been successfuly found.
     */
    bool selectSceneByPath(const std::string &path);

    /// Is a scene currently selected?
    bool sceneSelected() const;
private:
    /// List of scenes within the database.
    std::vector<SceneInfo> mScenes{ };

    /// Index of the currently selected scene.
    std::size_t mCurrentSceneIndex{ 0u };
    /// Is a scene selected?
    bool mCurrentSceneSelected{ false };
protected:
}; // class SceneDatabase

} // namespace dxr

