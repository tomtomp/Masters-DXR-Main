/**
* @file IncrementalAccelerationBuilder.h
* @author Tomas Polasek
* @brief Helper class used for incremental building 
* of acceleration structures of given meshes. Used 
* for profiling purposes.
*/

#pragma once

#include "engine/helpers/d3d12/D3D12RTAccelerationBuilder.h"

// Using Engine namespace for ease of access.

/// Namespace containing the engine code.
namespace quark
{

/// Namespace containing helper classes and methods.
namespace helpers
{

/// Direct3D 12 resource management.
namespace d3d12
{

/**
 * Helper class used for incremental building 
 * of acceleration structures of given meshes. Used 
 * for profiling purposes.
 */
class IncrementalAccelerationBuilder : public util::PointerType<IncrementalAccelerationBuilder>
{
public:
    /// Using geometry handles from the builder.
    using GeometryHandleT = D3D12RTAccelerationBuilder::GeometryHandleT;

    /// Initialize without starting a block.
    IncrementalAccelerationBuilder();

    /// Free any currently used resources.
    ~IncrementalAccelerationBuilder();

    // No copying, or moving.
    IncrementalAccelerationBuilder(const IncrementalAccelerationBuilder &other) = delete;
    IncrementalAccelerationBuilder &operator=(const IncrementalAccelerationBuilder &other) = delete;
    IncrementalAccelerationBuilder(IncrementalAccelerationBuilder &&other) = default;
    IncrementalAccelerationBuilder &operator=(IncrementalAccelerationBuilder &&other) = default;

    /**
     * Add new geometry to the acceleration structure.
     * @param primitive Primitive representing the geometry.
     * @param name Name of the primitive for debug purposes.
     * @return Returns geometry handle which represents the 
     * primitive. Returns NullGeometryHandle if the primitive 
     * is invalid.
     */
    GeometryHandleT addGeometryNoTransform(const res::rndr::Primitive &primitive, 
        const std::wstring &name = L"");

    /**
     * Add new geometry to the acceleration structure.
     * @param meshHandle Mesh representing the geometry. All 
     * primitives within this mesh will be added under a single 
     * instantiable handle.
     * @param name Name of the primitive for debug purposes.
     * @param noDuplication Don't duplicate already added meshes.
     * @return Returns geometry handle, representing the mesh as 
     * a whole, which can be made of multiple primitives. Returns 
     * NullGeometryHandle if there are not valid primitives.
     */
    GeometryHandleT addGeometryNoTransform(const res::rndr::MeshHandle &meshHandle, 
        const std::wstring &name = L"", bool noDuplication = true);

    /**
     * Add instance of given geometry to the scene.
     * @param geometry Handle specifying which geometry will be used 
     * by this instance.
     * @param transform Transform of the instance within the scene. 
     * Only 3x4 part of the matrix will be used - no projection.
     * @param shaderTableOffset Offset of this instance when choosing 
     * shaders within the bound shader table.
     * @param instanceID Value accessible from ray tracing shaders, 
     * allowing specialization of per-instance data.
     * @param instanceMask 8 bit mask used for ray tracing only some 
     * objects. Set to zero to never include the instance.
     * @param flags Flags used for this instance.
     */
    void addInstance(const GeometryHandleT &geometry, const helpers::math::Transform &transform, 
        ::UINT shaderTableOffset = 0u, ::UINT instanceID = 0u, ::UINT instanceMask = 1u, 
        ::D3D12_RAYTRACING_INSTANCE_FLAGS flags = ::D3D12_RAYTRACING_INSTANCE_FLAG_NONE);

    /**
     * Reset and initialize all necessary classes, to begin incremental 
     * building.
     * @param device Target device, where ray tracing would occur.
     * @param memoryMgr Memory management system which should be 
     * used for GPU memory allocation for the acceleration structure.
     */
     void beginBuilderBlock(res::d3d12::D3D12RayTracingDevice &device, 
        res::rndr::GpuMemoryMgr &memoryMgr);

    /**
     * Build current configuration of acceleration structure.
     * @param device Device used in the building process.
     * @param directCmdList Command list used to record the building 
     * commands.
     */
    void buildAccelerationStructure(res::d3d12::D3D12Device &device, 
        res::d3d12::D3D12CommandList &directCmdList);

    /// Get current building statistics.
    const D3D12RTAccelerationBuilder::Statistics &statistics() const;

    /// Focus the build on fast AS.
    void preferFastRayTracing();

    /// Focus the build on finishing as fast as possible.
    void preferFastBuild();
private:
    /// Builder used for actually performing the operations.
    util::PtrT<D3D12RTAccelerationBuilder> mBuilder{ };
protected:
}; // class D3D12RTAccelerationBuilder

} // namespace d3d12

} // namespace helpers

} // namespace quark

