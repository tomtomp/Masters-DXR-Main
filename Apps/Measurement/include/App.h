/**
* @file App.h
* @author Tomas Polasek
* @brief Main application header.
*/

#pragma once

#define ENGINE_ENABLE_RAY_TRACING
#include "Engine.h"
#include "engine/application/Application.h"
#include "engine/application/ApplicationGui.h"

#include "engine/application/ProfilingCollector.h"
#include "engine/application/FpsCollector.h"
#include "engine/application/ManualCollector.h"
#include "engine/application/DataCollectorMgr.h"

#include "engine/renderer/imGuiRL/ImGuiRL.h"
#include "engine/renderer/basicTexturedRL/BasicTexturedRL.h"
#include "engine/renderer/shadowMapRL/ShadowMapRL.h"
#include "engine/renderer/ambientOcclusionRL/AmbientOcclusionRL.h"
#include "engine/renderer/rasterResolveRL/RasterResolveRL.h"
#include "engine/renderer/deferredRL/DeferredRL.h"
#include "engine/renderer/basicRayTracingRL/BasicRayTracingRL.h"
#include "engine/renderer/resolveRL/ResolveRL.h"

#include "engine/input/KeyBinding.h"

#include "engine/scene/systems/CameraSys.h"
#include "engine/scene/logic/FreeLookController.h"
#include "engine/scene/logic/ScriptedCameraController.h"

#include "IncrementalAccelerationBuilder.h"
#include "SceneDatabase.h"

/// Namespace containing the project.
namespace dxr
{

/// Application specific options should be added here.
class AppSpecificConfig : public quark::EngineRuntimeConfig
{
public:
    /// Quality presets for the application.
    enum class QualityPreset
    {
        /// All effects disabled.
        NoEffects,

        // Enable only specified effect.
        HardShadows,
        AmbientOcclusion,
        AmbientOcclusion16,
        Reflections, 
        SoftShadows,
        SoftShadows8,

        /// Hard shadows, 4 samples of AO, no reflections.
        Low,
        /// 4 samples of soft shadows, 4 samples of AO, no reflections.
        Medium,
        /// 4 samples of soft shadows, 4 samples of AO, reflections enabled.
        High,
        /// 8 samples of soft shadows, 16 samples of AO, reflections enabled.
        Ultra,

        /// Quality is specified manually.
        Custom, 

        Count
    }; // enum class QualityPreset

    /// Convert quality preset to readable string.
    static const char *qualityPresetName(QualityPreset mode);

    /// Convert quality preset to readable string.
    static QualityPreset nameToQualityPreset(const std::string &name);

    AppSpecificConfig();

    // Start of options section

    /// Periodically print out profiler information.
    bool specificPrintProfiler{ false };
    /// Should the periodic profiler skip inactive scopes?
    bool specificPrintProfilerSkipInactive{ true };
    /// GlTF model for the primary scene.
    std::string glTFSceneFile{ "" };
    /// Offset of duplicated instances in world space.
    float duplicationOffset{ 10.0f };
    /// Should the duplication create a 2D grid (false) or 3D cube (true).
    bool duplicationCube{ false };

    /// Should the testing scene be loaded instead?
    bool useTestingScene{ false };

    /// Should the bottom level acceleration structures be duplicated for same meshes?
    bool duplicateBlas{ false };

    /// Overwrite the default mode to hybrid ray tracing.
    bool rtMode{ false };
    /// Overwrite the default mode to pure ray tracing.
    bool rtModePure{ false };

    /// Target duplication for profiling.
    uint32_t profileMaxDuplication{ 1u };
    /// Track used for profiling camera movement.
    std::string profileCameraTrack{ "" };

    /// Should the duplication profiling be executed?
    bool profileDuplication{ false };
    /// Should the automated profiling be executed?
    bool profileAutomation{ false };
    /// Should the build profiling be executed?
    bool profileBuilds{ false };
    /// Should the triangle profiling be executed?
    bool profileTriangles{ false };

    /// Prefix of the final profiling file.
    std::string profileFilePrefix{ "ProfilingData" };

    /// Quality preset used by the application.
    QualityPreset qualityPreset{ QualityPreset::High };

    // End of options section
}; // class AppSpecificRuntimeConfig

// Forward declaration.
class App;

/// Base interface for automated profiling.
class AutomatedProfiler
{
public:
    virtual ~AutomatedProfiler() = default;

    /// Called once before starting the main loop.
    virtual void start(App &app) = 0;

    /// Called once per update.
    virtual void update(float milliseconds) = 0;
    /// Called once per debugPringTimer, at the beginning.
    virtual void updateDebugStart() = 0;
    /// Called once per debugPringTimer, at the end.
    virtual void updateDebugEnd() = 0;

    /// Allow profiler to set data streams.
    virtual void processCustomAggregators(
        quark::app::DataCollectorMgr &collectorMgr);
private:
protected:
}; // class AutomatedProfiler

/// Profiling acceleration structures.
class DuplicationProfiler : public AutomatedProfiler
{
public:
    virtual ~DuplicationProfiler();

    /// Called once before starting the main loop.
    virtual void start(App &app);

    /// Called once per update.
    virtual void update(float milliseconds);
    /// Called once per debugPringTimer, at the beginning.
    virtual void updateDebugStart();
    /// Called once per debugPringTimer, at the end.
    virtual void updateDebugEnd();
private:
    App *mApp{ };
    /// Target duplication to run to.
    std::size_t mDuplicationTarget{ 0u };
    /// Index of the current debug update.
    std::size_t mUpdateIndex{ 0u };
protected:
}; // class DuplicationProfiler

/// Profiling with camera movement.
class CameraProfiler : public AutomatedProfiler
{
public:
    virtual ~CameraProfiler();

    /// Called once before starting the main loop.
    virtual void start(App &app);

    /// Called once per update.
    virtual void update(float milliseconds);
    /// Called once per debugPringTimer, at the beginning.
    virtual void updateDebugStart();
    /// Called once per debugPringTimer, at the end.
    virtual void updateDebugEnd();
private:
    App *mApp{ };
protected:
}; // class CameraProfiler

/// Profiling acceleration structure building with multiple models.
class BuildProfiler : public AutomatedProfiler
{
public:
    virtual ~BuildProfiler();

    /// Called once before starting the main loop.
    virtual void start(App &app);

    /// Called once per update.
    virtual void update(float milliseconds);
    /// Called once per debugPringTimer, at the beginning.
    virtual void updateDebugStart();
    /// Called once per debugPringTimer, at the end.
    virtual void updateDebugEnd();

    /// Allow profiler to set data streams.
    virtual void processCustomAggregators(
        quark::app::DataCollectorMgr &collectorMgr);
private:
    App *mApp{ };

    /// Profiling helper.
    quark::helpers::d3d12::IncrementalAccelerationBuilder mBuilder{ };
protected:
}; // class BuildProfiler

/// Profiling with changing number of triangles.
class TriangleProfiler : public AutomatedProfiler
{
public:
    virtual ~TriangleProfiler();

    /// Called once before starting the main loop.
    virtual void start(App &app);

    /// Called once per update.
    virtual void update(float milliseconds);
    /// Called once per debugPringTimer, at the beginning.
    virtual void updateDebugStart();
    /// Called once per debugPringTimer, at the end.
    virtual void updateDebugEnd();

    /// Allow profiler to set data streams.
    virtual void processCustomAggregators(
        quark::app::DataCollectorMgr &collectorMgr);
private:
    // States used in the profiling process.
    enum class ProfState
    {
        /// Incrementing number of models.
        Increment = 0, 
        /// Prepare for profiling
        Prepare, 
        /// Waiting for reload to finish.
        Wait, 
        /// Run the profiling.
        Profile, 
    }; // enum class ProfState

    /// How many debug cycles should we wait before profiling?
    static constexpr std::size_t WAIT_CYCLES{ 2u };

    App *mApp{ };
    ProfState mState{ };

    std::size_t mDuplicationTarget{ 0u };
    std::size_t mWaitCounter{ 0u };
protected:
}; // class TriangleProfiler

/// Main application class.
class App : public quark::app::Application<App, AppSpecificConfig>
{
public:
    /// Constructor called by the engine.
    App(ConfigT &cfg, quark::Engine &engine);

    /// Wait for all GPU command queues and free any resources.
    virtual ~App();

    virtual void initialize() override final;
    //virtual void destroy() override final;
    virtual int32_t run() override final;
    virtual void update(Milliseconds delta) override final;
    virtual void render() override final;
    //virtual void processEvents() override final;

    void onEvent(const KeyboardEvent &event) const;
    void onEvent(const MouseButtonEvent &event) const;
    void onEvent(const MouseMoveEvent &event) const;
    void onEvent(const MouseScrollEvent &event) const;
    void onEvent(const ResizeEvent &event);
    void onEvent(const ExitEvent &event);

    /// Register the app specific components.
    static void registerComponents();
private:
    /// Component used to store app specific information.
    struct AppInfoComponent
    {
        /// Has this entity been duplicated?
        bool duplicated{ false };
    }; // struct AppInfoComponent

    /// Enumeration of possible rendering modes.
    enum class RenderMode
    {
        Rasterization,
        RayTracing,
        Textured, 
        Last
    }; // enum class RenderMode

    /// Default scene file loaded when none is provided.
    static constexpr const char *DEFAULT_SCENE_FILE{ "Cube/Cube.gltf" };
    /// Default path which will be searched for provided models.
    static constexpr const char *DEFAULT_SCENE_SEARCH_PATH_PRIMARY{ "models/" };
    /// Backup scene path, which works when the executable is in the build hierarchy.
    static constexpr const char *DEFAULT_SCENE_SEARCH_PATH_SECONDARY{ "../../../../models/" };

    /// Default FOV of the camera.
    static constexpr float DEFAULT_CAMERA_FOV{ 80.0f };
    /// Default width of the viewport, before window size changes.
    static constexpr float DEFAULT_VIEWPORT_WIDTH{ 1024.0f };
    /// Default height of the viewport, before window size changes.
    static constexpr float DEFAULT_VIEWPORT_HEIGHT{ 768.0f };

    /// Mouse sensitivity for camera rotation control.
    static constexpr float DEFAULT_MOUSE_SENSITIVITY_MULT{ 0.01f };
    /// How many units does the camera move per one time unit?
    static constexpr float DEFAULT_MOVEMENT_UNITS{ 0.005f };
    /// Unit multiplier when sprinting.
    static constexpr float DEFAULT_MOVEMENT_SPRINT_MULTIPLIER{ 5.0f };

    /// Default camera distance from origin
    static constexpr float DEFAULT_CAMERA_DISTANCE{ 5.0f };

    /// How many seconds should we wait for other resize events.
    static constexpr float DEFAULT_RESIZE_AGGREGATION_TIME{ 0.5f };

    /**
     * Load provided scene file and return a handle to the loaded scene.
     * If provided scene file is empty or non-existent, a default scene 
     * will be loaded instead. Default scene is a cube, or empty if cube 
     * is unavailable.
     * @param sceneFile Scene file which should be loaded.
     * @return Returns handle to the created scene.
     */
    quark::res::scene::SceneHandle createPrimaryScene(const std::string &sceneFile);

    /**
     * Load provided scene file and return a handle to the loaded scene.
     * If provided scene file is empty or non-existent, a default scene 
     * will be loaded instead. Default scene is a cube, or empty if cube 
     * is unavailable.
     * Also prepares the scene for rendering.
     * @param sceneFile Scene file which should be loaded.
     * @return Returns handle to the created scene.
     */
    quark::res::scene::SceneHandle preparePrimaryScene(const std::string &sceneFile);

    /// Initialize the available scenes database.
    void prepareSceneDatabase();

    /**
     * Prepare for rendering of the user selected scene, specified within 
     * the runtime configuration.
     */
    void prepareSceneRendering();

    /// Prepare the camera for scene display.
    void prepareCamera();

    /// Setup key bindings for controlling camera etc.
    void setupInputBindings();

    /// Setup common GUI elements, base GUI context and initialize render layer GUIs.
    void setupGui();

    /// Setup profiling collectors.
    void setupProfiling();

    /// Set requested quality settings.
    void setupQualitySettings();

    /// Setup automatic profiling modes.
    void setupAutomaticProfiling();

    /// Common GUI elements and main window.
    void setupCommonGui();

    /**
     * Check whether a camera has bee already selected, selecting 
     * the first one if it was not.
     */
    void checkChooseCamera();

    /**
     * Change the duplication by given amount, setting the changed 
     * flag if necessary.
     * Final value is automatically set to zero if the result is 
     * negative.
     * @param offset Offset to change the duplication by.
     */
    void changeDuplicationBy(int32_t offset);

    /**
     * Check if scene reload is required and perform it if necessary.
     */
    void updateLoadedScene();

    /**
     * Update the scene state to reflect the current duplication.
     * Actions will be taken only if duplicationChanged flag is true.
     */
    void updateDuplication();

    /**
     * Update the current state of the GUI variables and reflect 
     * any changes.
     * @param delta Delta time from the last call.
     */
    void updateGui(Milliseconds delta);

    /// Update the common GUI variables.
    void updateCommonGui();

    /**
     * Set current scene from the scene database.
     * @param sceneIndex Index of the scene within the scene 
     * database.
     */
    void setCurrentScene(std::size_t sceneIndex);

    /**
     * Convert location of a pixel on the screen to a direction 
     * in world space.
     * @param x Coordinate of the pixel on the x-axis.
     * @param y Coordinate of the pixel on the y-axis.
     */
    quark::dxtk::math::Vector3 screenSpaceToDirection(uint32_t x, uint32_t y);

    /// Convert rotational quaternion to yaw pitch roll.
    quark::dxtk::math::Vector3 quaternionToYawPitchRoll(const quark::dxtk::math::Quaternion &rot);
    /// Convert yaw pitch roll to rotational quaternion.
    quark::dxtk::math::Quaternion yawPitchRollToQuaternion(const quark::dxtk::math::Vector3 &ypr);

    /// Convert render mode enum to readable string.
    static const char *renderModeToStr(RenderMode mode);

    /// Generate string for current configuration of the application.
    std::string generateConfigurationString();

    /// Generate string describing the selected quality settings.
    std::string generateQualityConfigurationString() const;

    /// Load quality settings from a file.
    void loadQualityConfigurationString(quark::res::File &settings) const;
    /// Save quality settings to a file.
    void saveQualityConfigurationString(quark::res::File &settings) const;

    /**
     * Save profiling data into 1 file called: 
     *   filePrefix+"_prof_"+dateTime+".txt"
     * @param filePrefix Prefix used for the file.
     */ 
    void saveProfilingData(const std::string &filePrefix = "ProfilingData");

    /**
     * Perform the resizing operations according 
     * to the provided resize event.
     * @param event Event describing the resize 
     * operation.
     */
    void performResize(ResizeEvent event);

    /// Render the scene using pure rasterization.
    void renderRasterization(quark::res::d3d12::D3D12CommandList &cmdList);

    /// Render the scene using hybrid ray tracing.
    void renderRayTracing(quark::res::d3d12::D3D12CommandList &cmdList);

    /// Render the scene using simple textured pass.
    void renderTextured(quark::res::d3d12::D3D12CommandList &cmdList);

    /// Render the GUI.
    void renderGui(quark::res::d3d12::D3D12CommandList &cmdList);

    /// Main binding scheme.
    quark::input::KeyBinding mBinding;

    /// ImGUI testing window.
    bool mShowDemoWindow{ true };

    /// Timer for counting fps/ups.
    quark::app::FpsUpdateTimer mFpsTimer;
    /// Once per how many ms should update run?
    Milliseconds mUpdateDeltaMs{ 0.0f };
    /// Once per how many ms should render run?
    Milliseconds mRenderDeltaMs{ 0.0f };

    /// Helper for aggregation of resize events.
    util::DelayedExecutor mResizeExecutor{ };

    /// Wrapper for common GUI variables.
    quark::app::ApplicationGui mAppGui;

    /// Helper used for collection of profiling statistics.
    quark::app::DataCollectorMgr mCollectorMgr{ };

    /// ImGUI rendering layer.
    quark::rndr::ImGuiRL::PtrT mImGuiRl;
    /// Textured entities rendering layer.
    quark::rndr::BasicTexturedRL::PtrT mTexturedRl;
    /// Shadow map rendering layer.
    quark::rndr::ShadowMapRL::PtrT mShadowMapRl;
    /// Ambient occlusion rendering layer.
    quark::rndr::AmbientOcclusionRL::PtrT mAmbientOcclusionRl;
    /// Rasterization resolve rendering layer.
    quark::rndr::RasterResolveRL::PtrT mRasterResolveRl;
    /// Rendering layer for G-Buffer generation.
    quark::rndr::DeferredRL::PtrT mDeferredRl;
    /// Ray tracing rendering layer.
    quark::rndr::BasicRayTracingRL::PtrT mRayTracingRl;
    /// Resolve pass for hybrid ray tracing.
    quark::rndr::ResolveRL::PtrT mResolveRl;

    /// Camera management system.
    quark::scene::sys::CameraSys *mCameraSys{ nullptr };
    /// Controller for chosen camera.
    quark::scene::logic::FreeLookController mCameraController{ };
    /// Automated controller for chosen camera.
    quark::scene::logic::ScriptedCameraController mScriptedController{ };
    /// Helper for creating camera automation tracks.
    quark::scene::logic::ScriptedCameraTrackMaker mScriptedTrackMaker{ };

    /// Slot for automated profiling controller.
    util::PtrT<AutomatedProfiler> mAutomatedProfiler{ };
    friend class DuplicationProfiler;
    friend class CameraProfiler;
    friend class TriangleProfiler;
    friend class BuildProfiler;

    /// Database of available scenes.
    SceneDatabase mSceneDatabase{ };

    /// Container for rendering configuration.
    struct Configuration
    {
        /// Current mode of rendering.
        RenderMode renderMode{ RenderMode::Rasterization };

        /// What is the dimension of the duplication grid?
        uint32_t duplication{ 1u };
        /// Has the duplication changed since last update?
        bool duplicationChanged{ false };

        /// Current width of the main window.
        float windowWidth{ DEFAULT_VIEWPORT_WIDTH };
        /// Current height of the main window.
        float windowHeight{ DEFAULT_VIEWPORT_HEIGHT };

        /// Current per-frame time in milliseconds.
        float frameTime{ 0.0f };
        /// Current frames per second.
        float fps{ 0.0f };

        /// Should the glTF scene file be reloaded?
        bool sceneReloadRequired{ false };
    }; // struct Configuration

    /// Configuration of the current scene rendering.
    Configuration mC;
protected:
}; // class App
    
} // namespace App
