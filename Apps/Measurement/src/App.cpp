/**
* @file App.cpp
* @author Tomas Polasek
* @brief Main application header.
*/

#include "stdafx.h"

#include "App.h"
#include "scene/loaders/GlTFLoader.h"

namespace dxr
{

const char *AppSpecificConfig::qualityPresetName(QualityPreset mode)
{
    switch (mode)
    {
        case QualityPreset::NoEffects:
        { return "NoEffects"; }
        case QualityPreset::HardShadows: 
        { return "HardShadows"; }
        case QualityPreset::AmbientOcclusion: 
        { return "AmbientOcclusion"; }
        case QualityPreset::AmbientOcclusion16: 
        { return "AmbientOcclusion16"; }
        case QualityPreset::Reflections: 
        { return "Reflections"; }
        case QualityPreset::SoftShadows: 
        { return "SoftShadows"; }
        case QualityPreset::SoftShadows8: 
        { return "SoftShadows8"; }
        case QualityPreset::Low: 
        { return "Low"; }
        case QualityPreset::Medium: 
        { return "Medium"; }
        case QualityPreset::High: 
        { return "High"; }
        case QualityPreset::Ultra: 
        { return "Ultra"; }
        case QualityPreset::Custom: 
        { return "Custom"; }
        default: 
        { return "Unknown"; }
    }
}

AppSpecificConfig::QualityPreset AppSpecificConfig::nameToQualityPreset(
    const std::string &name)
{
    const auto lastPreset{ static_cast<std::size_t>(QualityPreset::Count) };

    for (std::size_t iii = 0u; iii < lastPreset; ++iii)
    {
        const auto enumValue{ static_cast<QualityPreset>(iii) };
        if (name == qualityPresetName(enumValue))
        { return enumValue; }
    }

    // TODO - Default value?
    return QualityPreset::Count;
}

AppSpecificConfig::AppSpecificConfig()
{
    mParser.addOption<bool>(L"--prof", [&] (const bool &val)
    {
        specificPrintProfiler = val;
    }, L"Enable/disable periodic profiler information printing.");
    mParser.addOption<bool>(L"--prof-skip-inactive", [&] (const bool &val)
    {
        specificPrintProfilerSkipInactive = val;
    }, L"Enable/disable skipping of inactive scopes for the periodic profiler.");
    mParser.addOption<std::wstring>(L"--scene", [&] (const std::wstring &val)
    {
        glTFSceneFile = util::toString(val);
    }, L"Choose which glTF scene should be displayed, default "
       L"value loads a cube scene. Provided path should be "
       L"relative to the models/ folder!");
    mParser.addOption<float>(L"--duplication-offset", [&] (const float &val)
    {
        duplicationOffset = val;
    }, L"Offset of the duplicated entities in world-space.");
    mParser.addOption(L"--duplication-cube", [&] ()
    {
        duplicationCube = true;
    }, L"Create a 3D cube of duplication instead of 2D grid.");
    mParser.addOption(L"--testing-scene", [&] ()
    {
        useTestingScene = true;
    }, L"Overwrite the target scene with a testing scene.");
    mParser.addOption(L"--duplicate-blas", [&]()
    {
        duplicateBlas = true;
    }, L"Enables bottom level acceleration structure duplication, "
       L"when multiple instances of a single mesh are used");

    mParser.addOption(L"--rt-mode", [&] ()
    {
        rtMode = true;
    }, L"Overwrite the startup mode from rasterization to hybrid ray tracing.");
    mParser.addOption(L"--rt-mode-pure", [&] ()
    {
        rtModePure = true;
    }, L"Overwrite the startup mode from rasterization to pure ray tracing.");

    mParser.addOption<uint32_t>(L"--profile-max-duplication", [&] (const uint32_t &val)
    {
        profileMaxDuplication = val;
    }, L"Specify maximum duplication for profiling runs.");
    mParser.addOption<std::wstring>(L"--profile-camera-track", [&] (const std::wstring &val)
    {
        profileCameraTrack = util::toString(val);
    }, L"Specify camera track for profiling runs.");

    mParser.addOption(L"--profile-duplication", [&] ()
    {
        profileDuplication = true;
    }, L"When set, the application performs automatic profiling "
       L"of acceleration structure building. Uses max-duplication.");
    mParser.addOption(L"--profile-automation", [&] ()
    {
        profileAutomation = true;
    }, L"When set, the application performs automatic profiling "
       L"with automated camera. Uses camera-track.");
    mParser.addOption(L"--profile-builds", [&] ()
    {
        profileBuilds = true;
    }, L"When set, the application performs automatic profiling "
       L"of singular mesh/primitive builds.");
    mParser.addOption(L"--profile-triangles", [&] ()
    {
        profileTriangles = true;
    }, L"When set, the application performs automatic profiling "
       L"of triangle ray tracing. Uses camera-track and max-duplication.");

    mParser.addOption<std::wstring>(L"--profile-output", [&] (const std::wstring &val)
    {
        profileFilePrefix = util::toString(val);
    }, L"Set prefix of the final profiling file.");

    mParser.addOption<std::wstring>(L"--quality-preset", [&](const std::wstring &val)
    {
        const auto presetName{ util::toString(val) };
        const auto presetValue{ nameToQualityPreset(presetName) };

        if (presetValue == QualityPreset::Count)
        { throw std::runtime_error("Provided quality preset string is not valid!"); }

        qualityPreset = presetValue;
    }, L"Use provided quality preset when running the application.\n"
       L"Available values: \n"
       L"  NoEffects : All effects are disabled, only lighting."
       L"  HardShadows : Only hard shadows (Low) and lighting."
       L"  AmbientOcclusion : Only ambient occlusion (Low) and lighting."
       L"  AmbientOcclusion16 : Only ambient occlusion (Ultra) and lighting."
       L"  Reflections : Only reflections (High) and lighting."
       L"  SoftShadows : Only soft shadows (Medium) and lighting."
       L"  SoftShadows8 : Only soft shadows (Ultra) and lighting."
       L"  Low : Hard shadows (2048px), ambient occlusion (4s), no reflections."
       L"  Medium : Soft shadows (4096px, 4s), ambient occlusion (4s), no reflections."
       L"  High : Soft shadows (4096px, 4s), ambient occlusion (4s), reflections enabled."
       L"  Ultra : Soft shadows (8192px, 8s), ambient occlusion (16s), reflections enabled."
    );
}

App::App(ConfigT &cfg, quark::Engine &engine) :
    Application(cfg, engine),
    mFpsTimer(false), 
    mAppGui(cfg), 
    mImGuiRl{ quark::rndr::ImGuiRL::create(engine.renderer()) },
    mTexturedRl{ quark::rndr::BasicTexturedRL::create(engine.renderer()) },
    mShadowMapRl{ quark::rndr::ShadowMapRL::create(engine.renderer()) },
    mAmbientOcclusionRl{ quark::rndr::AmbientOcclusionRL::create(engine.renderer()) },
    mRasterResolveRl{ quark::rndr::RasterResolveRL::create(engine.renderer()) },
    mDeferredRl{ quark::rndr::DeferredRL::create(engine.renderer()) }, 
    mRayTracingRl{ quark::rndr::BasicRayTracingRL::create(engine.renderer()) }, 
    mResolveRl{ quark::rndr::ResolveRL::create(engine.renderer()) }
{ }

App::~App()
{ 
    saveProfilingData(mCfg.profileFilePrefix);
    mEngine.renderer().flushAll(); 
}

void App::initialize()
{
    PROF_SCOPE("Initialization");

    // Overwrite the startup mode.
    if (mCfg.rtMode || mCfg.rtModePure)
    { mC.renderMode = RenderMode::RayTracing; }
    if (mCfg.rtModePure)
    { mRayTracingRl->setDebugMode(quark::rndr::BasicRayTracingRL::DebugMode::FinalColorWithPrimary); }

    // Prepare the database of usable scenes.
    prepareSceneDatabase();

    // Prepare the user-selected scene for rendering.
    prepareSceneRendering();

    // Prepare display camera of the scene.
    prepareCamera();

    // Input binding to control the camera etc.
    setupInputBindings();

    // Create common GUI elements, base GUI context and initialize render layer GUIs.
    setupGui();

    // Setup automatic profiling modes.
    setupAutomaticProfiling();

    // Setup profiling collectors.
    setupProfiling();

    // Set the requested quality settings.
    setupQualitySettings();
}

int32_t App::run()
{
    // Profiling printer.
    util::prof::PrintCrawler pc(mCfg.specificPrintProfilerSkipInactive);

    // Target number of milliseconds between updates.
    mUpdateDeltaMs = updateDelta();
    util::DeltaTimer updateTimer;

    // Target number of milliseconds between renders.
    mRenderDeltaMs = renderDelta();
    util::DeltaTimer renderTimer;

    mFpsTimer.reset();

    util::TickTimer debugPrintTimer(Seconds(1u), [&] ()
    {
        if (mAutomatedProfiler)
        { mAutomatedProfiler->updateDebugStart(); }
        else
        { mCollectorMgr.collectData(); }

        if (mCfg.specificPrintProfiler)
        {
            PROF_DUMP(pc);
        }
        mFpsTimer.printInfo();

        if (mC.renderMode == RenderMode::RayTracing)
        {
            log<Info>() << "Ray tracing rendering mode: " << mResolveRl->currentDisplayModeName() << std::endl;
            log<Info>() << "GigaRays/s : " << mFpsTimer.grps() << std::endl;
            log<Info>() << "Deferred buffers " << (mRayTracingRl->deferredBuffersRequired() ? "ARE" : "ARE NOT") << " generated!" << std::endl;
        }
        else if (mC.renderMode == RenderMode::Rasterization)
        {
            log<Info>() << "Rasterization rendering mode: " << mRasterResolveRl->currentDisplayModeName() << std::endl;
        }

        log<Info>() << "Current size of duplication grid: " << mC.duplication << std::endl;

        if (mAutomatedProfiler)
        { mAutomatedProfiler->updateDebugEnd(); }
    });

    util::TimerTicker ticker;
    ticker.addTimer(updateTimer);
    ticker.addTimer(renderTimer);
    ticker.addTimer(mFpsTimer);
    ticker.addTimer(debugPrintTimer);
    ticker.addTimer(mResizeExecutor);

    // Start the main loop.
    mRunning = true;

    // Startup any automation.
    if (mAutomatedProfiler)
    { mAutomatedProfiler->start(*this); }

    while (mRunning)
    { // Main application loop
        PROF_SCOPE("MainLoop");

        ticker.tick();

        while (updateTimer.tryTake(mUpdateDeltaMs))
        { // While there is updating to do, 
            PROF_SCOPE("Update");
            update(mUpdateDeltaMs);
            mFpsTimer.update();
        }

        if (renderTimer.tryTake(mRenderDeltaMs))
        { // Target time between renders exceeded.
            PROF_SCOPE("Render");
            render();
            mFpsTimer.frame();
            renderTimer.takeAll();
        }
    }

    return EXIT_SUCCESS;
}

void App::update(Milliseconds delta)
{
    // Process any events gathered on the message bus.
    processEvents(delta);

    // Check if a new scene should be loaded.
    updateLoadedScene();

    // Reflect the duplication parameter.
    updateDuplication();

    // Make sure entities within the scene are ready.
    mEngine.scene().refresh();

    // Perform update logic, running any scene systems: 

    mCameraSys->refresh();
    checkChooseCamera();
    mCameraController.update(delta.count());
    mScriptedController.update(delta.count());

    // End of update logic.

    // Update GUI variables.
    updateGui(delta);

    // Update automated profiling.
    if (mAutomatedProfiler)
    { mAutomatedProfiler->update(delta.count()); }
    mRenderDeltaMs = renderDelta();
}

void App::render()
{
    auto cmdList{ clear(quark::dxtk::math::Color{0.0f, 0.0f, 0.0f}) };

    switch (mC.renderMode)
    {
        case RenderMode::Rasterization:
        { // Render using pure rasterization.
            renderRasterization(*cmdList);
            break;
        }
        case RenderMode::RayTracing:
        { // Render using hybrid ray tracing.
            renderRayTracing(*cmdList);
            break;
        }
        case RenderMode::Textured:
        { // Render using simple textured pass.
            renderTextured(*cmdList);
            break;
        }
        default:
        { // No rendering.
            break;
        }
    }

    if (gui().enabled())
    { renderGui(*cmdList); }

    present(cmdList);
}

void App::onEvent(const KeyboardEvent &event) const
{ mBinding.processEvent(event); }

void App::onEvent(const MouseButtonEvent &event) const
{ mBinding.processEvent(event); }

void App::onEvent(const MouseMoveEvent &event) const
{ mBinding.processEvent(event); }

void App::onEvent(const MouseScrollEvent &event) const
{ mBinding.processEvent(event); }

void App::onEvent(const ResizeEvent &event)
{
    static bool firstResize{ true };
    if (firstResize)
    {
        performResize(event);
        firstResize = false;
    }

    /// Aggregate resize events and execute only latest.
    mResizeExecutor.delayExecution(Milliseconds(DEFAULT_RESIZE_AGGREGATION_TIME * 1000.0f), 
        [this, event]() { performResize(event); });
}

void App::onEvent(const quark::app::events::ExitEvent &event)
{ mRunning = false; }

void App::registerComponents()
{
    quark::scene::ComponentRegister::registerComponent<AppInfoComponent>();
}

quark::res::scene::SceneHandle App::createPrimaryScene(const std::string &sceneFile)
{
    if (mCfg.useTestingScene)
    { return mEngine.scene().loadTestingScene(); }

    try
    {
        quark::res::File sceneFilePath;
        if (!sceneFile.empty())
        { sceneFilePath = quark::res::File(sceneFile, { DEFAULT_SCENE_SEARCH_PATH_PRIMARY, DEFAULT_SCENE_SEARCH_PATH_SECONDARY }); }
        else
        { sceneFilePath = quark::res::File(DEFAULT_SCENE_FILE, { DEFAULT_SCENE_SEARCH_PATH_PRIMARY, DEFAULT_SCENE_SEARCH_PATH_SECONDARY }); }
        return mEngine.scene().loadGlTFScene(sceneFilePath);
    } catch (const quark::res::File::FileNotFoundException &e)
    {
        log<Error>() << "Unable to find scene file \"" << 
            (sceneFile.empty() ? DEFAULT_SCENE_FILE : sceneFile) << 
            "\"! Make sure it is in the \"" << DEFAULT_SCENE_SEARCH_PATH_PRIMARY << 
            "\" folder!: " << e.what() << std::endl;
    } catch (const quark::scene::loaders::BaseSceneLoader::SceneLoadException &e)
    {
        log<Error>() << "Unable to load the scene!: \n" << e.what() << std::endl;
    }

    return mEngine.scene().loadEmptyScene();
}

quark::res::scene::SceneHandle App::preparePrimaryScene(const std::string &sceneFile)
{
    PROF_SCOPE("SceneLoad");

    auto scene{ createPrimaryScene(sceneFile) };

    mEngine.scene().prepareScene(scene);
    mEngine.scene().setCurrentScene(scene);
    mEngine.scene().refresh();

    return scene;
}

void App::prepareSceneDatabase()
{
    mSceneDatabase.registerScene("Cube", "Cube/Cube.gltf");
    mSceneDatabase.registerScene("CubePlane", "CubePlane/CubePlane.gltf");
    mSceneDatabase.registerScene("Diorama", "Diorama/Diorama.glb");
    mSceneDatabase.registerScene("Dragon", "Dragon/Dragon.glb");
    mSceneDatabase.registerScene("Sponza", "Sponza/Sponza.gltf");
    mSceneDatabase.registerScene("Suzanne", "Suzanne/Suzanne.gltf");
    mSceneDatabase.registerScene("SuzannePlane", "SuzannePlane/SuzannePlane.gltf");

    mSceneDatabase.selectSceneByPath(mCfg.glTFSceneFile);
}

void App::prepareSceneRendering()
{ 
    PROF_SCOPE("ScenePrep");

    auto scene{ preparePrimaryScene(mCfg.glTFSceneFile) };
    const auto toDraw{ mEngine.scene().getToDraw() };
    auto directCmdList{ mEngine.renderer().directCmdQueue().getCommandList() };

    { GPU_PROF_SCOPE(*directCmdList, "ScenePrep");
        mShadowMapRl->setDrawablesList(toDraw);
        mShadowMapRl->prepare();

        // Specify true since, a new scene is being loaded.
        const auto loadingNewScene{ true };

        mTexturedRl->setDrawablesList(toDraw);
        mTexturedRl->prepare(loadingNewScene);

        mDeferredRl->setDrawablesList(toDraw);
        mDeferredRl->prepare(loadingNewScene);

        mRayTracingRl->setBlasDuplication(mCfg.duplicateBlas);
        mRayTracingRl->setDrawablesList(toDraw);
        mRayTracingRl->prepare(*directCmdList, loadingNewScene);
    }

    { PROF_SCOPE("GpuSidePrep");
        log<Info>() << "Executing preparation command list, this includes building the AS!" << std::endl;
        directCmdList->executeNoWait();
        mEngine.renderer().flushCopyCommands();
    }

    log<Info>() << "Finished preparation!" << std::endl;
}

void App::prepareCamera()
{
    mCameraSys = mEngine.scene().addGetSystem<quark::scene::sys::CameraSys>();
    if (!mCameraSys)
    { throw std::runtime_error("Failed to create the camera management system!"); }

    mCameraController.setCameraSystem(*mCameraSys);
    mCameraController.setFov(DEFAULT_CAMERA_FOV);
    mCameraController.setViewportSize(DEFAULT_VIEWPORT_WIDTH, DEFAULT_VIEWPORT_HEIGHT);

    mScriptedController.setCameraSystem(*mCameraSys);
}

void App::setupInputBindings()
{
    // Escape to exit the program.
    mBinding.kb().setAction(
        Key::Escape,
        KeyMods::None,
        KeyAction::Press,
        [&]()
    {
        exit();
    });

    // Mouse movement to control the camera rotation.
    mBinding.mouse().setMoveAction(
        [&] (const MouseMoveEvent &event)
    {
        // Remember last position, for delta computation.
        static auto lastPos{ event.pos };

        // Calculate the new camera rotation.
        if(event.buttonPressed(MouseButton::LButton))
        {
            if (event.modPressed(MouseMods::Control))
            {
                mRasterResolveRl->setLightDirection(-screenSpaceToDirection(event.pos.x, event.pos.y));
                mRayTracingRl->setLightDirection(-screenSpaceToDirection(event.pos.x, event.pos.y));
            }
            else if (event.modPressed(MouseMods::Shift))
            {
                mRasterResolveRl->setLightDirection(screenSpaceToDirection(event.pos.x, event.pos.y));
                mRayTracingRl->setLightDirection(screenSpaceToDirection(event.pos.x, event.pos.y));
            }
            else
            {
                mCameraController.setRotation(
                    mCameraController.rotationFromMouseDelta(
                        static_cast<float>(event.pos.x - lastPos.x) * DEFAULT_MOUSE_SENSITIVITY_MULT, 
                        static_cast<float>(event.pos.y - lastPos.y) * DEFAULT_MOUSE_SENSITIVITY_MULT));
            }
        }

        lastPos = event.pos;
    });

    // Mouse scrolling for changing instance duplication.
    mBinding.mouse().setScrollAction(
        [&] (const MouseScrollEvent &event)
    {
        if (event.delta > 0)
        { changeDuplicationBy(1); }
        else if (event.delta < 0)
        { changeDuplicationBy(-1); }
    });

    // Sprint when holding down b
    mBinding.kb().setAction(
        Key::B, 
        KeyMods::None, 
        KeyAction::Press, 
        [&] ()
    {
        mCameraController.speedModifier(DEFAULT_MOVEMENT_SPRINT_MULTIPLIER);
    });
    mBinding.kb().setAction(
        Key::B, 
        KeyMods::None, 
        KeyAction::Release, 
        [&] ()
    {
        mCameraController.speedModifier(1.0f);
    });

    // Move the camera forward.
    mBinding.kb().setAction(
        Key::W, 
        KeyMods::None, 
        KeyAction::Press, 
        [&] ()
    {
        mCameraController.forward(DEFAULT_MOVEMENT_UNITS);
    });
    mBinding.kb().setAction(
        Key::W, 
        KeyMods::None, 
        KeyAction::Release, 
        [&] ()
    {
        mCameraController.forward(0.0f);
    });

    // Move the camera backward.
    mBinding.kb().setAction(
        Key::S, 
        KeyMods::None, 
        KeyAction::Press, 
        [&] ()
    {
        mCameraController.backward(DEFAULT_MOVEMENT_UNITS);
    });
    mBinding.kb().setAction(
        Key::S, 
        KeyMods::None, 
        KeyAction::Release, 
        [&] ()
    {
        mCameraController.backward(0.0f);
    });

    // Move the camera left.
    mBinding.kb().setAction(
        Key::A, 
        KeyMods::None, 
        KeyAction::Press, 
        [&] ()
    {
        mCameraController.left(DEFAULT_MOVEMENT_UNITS);
    });
    mBinding.kb().setAction(
        Key::A, 
        KeyMods::None, 
        KeyAction::Release, 
        [&] ()
    {
        mCameraController.left(0.0f);
    });

    // Move the camera right.
    mBinding.kb().setAction(
        Key::D, 
        KeyMods::None, 
        KeyAction::Press, 
        [&] ()
    {
        mCameraController.right(DEFAULT_MOVEMENT_UNITS);
    });
    mBinding.kb().setAction(
        Key::D, 
        KeyMods::None, 
        KeyAction::Release, 
        [&] ()
    {
        mCameraController.right(0.0f);
    });

    // Move the camera up.
    mBinding.kb().setAction(
        Key::Space, 
        KeyMods::None, 
        KeyAction::Press, 
        [&] ()
    {
        mCameraController.up(DEFAULT_MOVEMENT_UNITS);
    });
    mBinding.kb().setAction(
        Key::Space, 
        KeyMods::None, 
        KeyAction::Release, 
        [&] ()
    {
        mCameraController.up(0.0f);
    });

    // Move the camera down.
    mBinding.kb().setAction(
        Key::C, 
        KeyMods::None, 
        KeyAction::Press, 
        [&] ()
    {
        mCameraController.down(DEFAULT_MOVEMENT_UNITS);
    });
    mBinding.kb().setAction(
        Key::C, 
        KeyMods::None, 
        KeyAction::Release, 
        [&] ()
    {
        mCameraController.down(0.0f);
    });

    // Switch rendering mode.
    mBinding.kb().setAction(
        Key::R, 
        KeyMods::None, 
        KeyAction::Press, 
        [&]()
    {
        auto nextMode{static_cast<std::size_t>(mC.renderMode) + 1u};
        if (nextMode >= static_cast<std::size_t>(RenderMode::Last))
        { nextMode = 0u; }

        mC.renderMode = static_cast<RenderMode>(nextMode);
    });

    // Switch ray tracing rendering mode forward.
    mBinding.kb().setAction(
        Key::N, 
        KeyMods::None, 
        KeyAction::Press, 
        [&]()
    { mResolveRl->prevDisplayMode(); });

    // Switch ray tracing rendering mode back.
    mBinding.kb().setAction(
        Key::M, 
        KeyMods::None, 
        KeyAction::Press, 
        [&]()
    { mResolveRl->nextDisplayMode(); });

    // Switch rasterization rendering mode forward.
    mBinding.kb().setAction(
        Key::J, 
        KeyMods::None, 
        KeyAction::Press, 
        [&]()
    { mRasterResolveRl->prevDisplayMode(); });

    // Switch rasterization rendering mode back.
    mBinding.kb().setAction(
        Key::K, 
        KeyMods::None, 
        KeyAction::Press, 
        [&]()
    { mRasterResolveRl->nextDisplayMode(); });

    // Switch GUI rendering on/off.
    mBinding.kb().setAction(
        Key::G, 
        KeyMods::None, 
        KeyAction::Press, 
        [&]()
    { 
        mCfg.guiEnable = !mCfg.guiEnable;
        gui().setEnabled(mCfg.guiEnable); 
    });

    // Toggle automatic camera.
    mBinding.kb().setAction(
        Key::X, 
        KeyMods::None, 
        KeyAction::Press, 
        [&]()
    { 
        if (!mScriptedController.running())
        { mScriptedController.startTrack(); }
        else
        { mScriptedController.stopTrack(); }
    });
}

void App::setupGui()
{
    mAppGui.prepareGui(gui());

    setupCommonGui();

    mTexturedRl->prepareGui(gui());

    mDeferredRl->prepareGui(gui());
    mRayTracingRl->prepareGui(gui());
    mResolveRl->prepareGui(gui());

    mShadowMapRl->prepareGui(gui());
    mAmbientOcclusionRl->prepareGui(gui());
    mRasterResolveRl->prepareGui(gui());
}

void App::setupProfiling()
{
    mCollectorMgr.reset();

    { auto &profilingAggregator{ mCollectorMgr.pushAggregation("ProfilingAggregator") };
        profilingAggregator.setAutoIndexing(true);
        profilingAggregator.addCollector<quark::app::ProfilingCollector>()
            .setNoDuplication(false)
            .addStream("Render")
            .addStream("Update")
            .addStream("Textured")
            .addStream("TexturedGpu")
            .addStream("TexturedRLGpu")
            .addStream("RayTracing")
            .addStream("RayTracingGpu")
            .addStream("DeferredRLGpu")
            .addStream("RayTracingRLGpu")
            .addStream("ResolveRLGpu");
    }

    { auto &profilingBuildAggregator{ mCollectorMgr.pushAggregation("ProfilingBuildAggregator") };
        profilingBuildAggregator.setAutoIndexing(true);
        profilingBuildAggregator.addCollector<quark::app::ProfilingCollector>()
            .setNoDuplication(true)
            .addStream("BuildBottomLevelAS", 2u)
            .addStream("BuildBottomLevelASGpu", 2u)
            .addStream("BuildTopLevelAS", 2u)
            .addStream("BuildTopLevelASGpu", 2u);
        auto &modelCollector{ profilingBuildAggregator.addCollector<quark::app::ManualCollector>() };
        modelCollector.setCollectInSync(true);
        modelCollector.setNoCollection(true);
        modelCollector.addDataStream<std::size_t>("Duplication", [&]() {
            return mC.duplication;
        });
        modelCollector.addDataStream<std::size_t>("Triangles", [&]() {
            return mRayTracingRl->accelerationStructureStatistics().totalBlasTriangles;
        });
        modelCollector.addDataStream<std::size_t>("Meshes", [&]() {
            return mRayTracingRl->accelerationStructureStatistics().totalBlasMeshes;
        });
        modelCollector.addDataStream<std::size_t>("BottomLevels", [&]() {
            return mRayTracingRl->accelerationStructureStatistics().numBlasCreated;
        });
        modelCollector.addDataStream<std::size_t>("TopLevelSize", [&]() {
            return mRayTracingRl->accelerationStructureStatistics().totalTlasSizes;
        });
        modelCollector.addDataStream<std::size_t>("BottomLevelsSize", [&]() {
            return mRayTracingRl->accelerationStructureStatistics().totalBlasSizes;
        });
    }

    { auto &fpsAggregator{ mCollectorMgr.pushAggregation("FpsAggregator") };
        fpsAggregator.setAutoIndexing(true);
        fpsAggregator.addCollector<quark::app::FpsCollector>(mFpsTimer);
    }

    if (mAutomatedProfiler)
    { mAutomatedProfiler->processCustomAggregators(mCollectorMgr); }
}

void App::setupQualitySettings()
{
    /// Don't overwrite, when using custom settings.
    if (mCfg.qualityPreset == AppSpecificConfig::QualityPreset::Custom)
    { return; }

    bool enableShadows{ false };
    uint32_t shadowMapResolution{ 1u };
    uint32_t softShadowSamples{ 0u };
    bool enableAmbientOcclusion{ false };
    uint32_t ambientOcclusionSize{ 0u };
    bool enableReflections{ false };

    switch (mCfg.qualityPreset)
    {
        case AppSpecificConfig::QualityPreset::NoEffects:
        {
            enableShadows           = false;
            shadowMapResolution     = 1u;
            softShadowSamples       = 0u;
            enableAmbientOcclusion  = false;
            ambientOcclusionSize    = 0u;
            enableReflections       = false;
            break;
        }
        case AppSpecificConfig::QualityPreset::HardShadows: 
        {
            enableShadows           = true;
            shadowMapResolution     = 2048u;
            softShadowSamples       = 0u;
            enableAmbientOcclusion  = false;
            ambientOcclusionSize    = 0u;
            enableReflections       = false;
            break;
        }
        case AppSpecificConfig::QualityPreset::AmbientOcclusion: 
        {
            enableShadows           = false;
            shadowMapResolution     = 1u;
            softShadowSamples       = 0u;
            enableAmbientOcclusion  = true;
            ambientOcclusionSize    = 4u;
            enableReflections       = false;
            break;
        }
        case AppSpecificConfig::QualityPreset::AmbientOcclusion16: 
        {
            enableShadows           = false;
            shadowMapResolution     = 1u;
            softShadowSamples       = 0u;
            enableAmbientOcclusion  = true;
            ambientOcclusionSize    = 16u;
            enableReflections       = false;
            break;
        }
        case AppSpecificConfig::QualityPreset::Reflections: 
        {
            enableShadows           = false;
            shadowMapResolution     = 1u;
            softShadowSamples       = 0u;
            enableAmbientOcclusion  = false;
            ambientOcclusionSize    = 0u;
            enableReflections       = true;
            break;
        }
        case AppSpecificConfig::QualityPreset::SoftShadows: 
        {
            enableShadows           = true;
            shadowMapResolution     = 4096u;
            softShadowSamples       = 4u;
            enableAmbientOcclusion  = false;
            ambientOcclusionSize    = 0u;
            enableReflections       = false;
            break;
        }
        case AppSpecificConfig::QualityPreset::SoftShadows8: 
        {
            enableShadows           = true;
            shadowMapResolution     = 8192u;
            softShadowSamples       = 8u;
            enableAmbientOcclusion  = false;
            ambientOcclusionSize    = 0u;
            enableReflections       = false;
            break;
        }
        case AppSpecificConfig::QualityPreset::Low: 
        {
            enableShadows           = true;
            shadowMapResolution     = 2048u;
            softShadowSamples       = 0u;
            enableAmbientOcclusion  = true;
            ambientOcclusionSize    = 4u;
            enableReflections       = false;
            break;
        }
        default: 
        case AppSpecificConfig::QualityPreset::Medium: 
        {
            enableShadows           = true;
            shadowMapResolution     = 4096u;
            softShadowSamples       = 4u;
            enableAmbientOcclusion  = true;
            ambientOcclusionSize    = 4u;
            enableReflections       = false;
            break;
        }
        case AppSpecificConfig::QualityPreset::High: 
        {
            enableShadows           = true;
            shadowMapResolution     = 4096u;
            softShadowSamples       = 4u;
            enableAmbientOcclusion  = true;
            ambientOcclusionSize    = 4u;
            enableReflections       = true;
            break;
        }
        case AppSpecificConfig::QualityPreset::Ultra: 
        {
            enableShadows           = true;
            shadowMapResolution     = 8192u;
            softShadowSamples       = 8u;
            enableAmbientOcclusion  = true;
            ambientOcclusionSize    = 16u;
            enableReflections       = true;
            break;
        }
    }

    gui().getVariable<quark::gui::InputInt>("AmbientOcclusionRlKernelSize").setValue(
        ambientOcclusionSize ? ambientOcclusionSize : 1u);
    gui().getVariable<quark::gui::InputInt>("ShadowMapRlResolution").setValue(shadowMapResolution);
    gui().getVariable<quark::gui::InputBool>("RasterResolveRlEnableShadows").setValue(enableShadows);
    gui().getVariable<quark::gui::InputInt>("RasterResolveRlPcfKernelHalfSize").setValue(softShadowSamples);
    gui().getVariable<quark::gui::InputBool>("RasterResolveRlEnableAO").setValue(enableAmbientOcclusion);
    gui().getVariable<quark::gui::InputBool>("RasterResolveRlEnableReflections").setValue(enableReflections);

    gui().getVariable<quark::gui::InputBool>("ResolveRlEnableShadows").setValue(enableShadows);
    gui().getVariable<quark::gui::InputInt>("ResolveRlKernelHalfSize").setValue(std::max(softShadowSamples, 4u));
    gui().getVariable<quark::gui::InputBool>("ResolveRlEnableAO").setValue(enableAmbientOcclusion);
    gui().getVariable<quark::gui::InputBool>("ResolveRlEnableReflections").setValue(enableReflections);
    gui().getVariable<quark::gui::InputInt>("RayTracingRlShadowRays").setValue(
        enableShadows && softShadowSamples == 0u ? 1u : softShadowSamples);
    gui().getVariable<quark::gui::InputInt>("RayTracingRlAoRays").setValue(ambientOcclusionSize);
    gui().getVariable<quark::gui::InputBool>("RayTracingRlEnableReflections").setValue(enableReflections);
}

void App::setupAutomaticProfiling()
{
    if (mCfg.profileDuplication)
    { mAutomatedProfiler = std::make_shared<DuplicationProfiler>(); }
    else if (mCfg.profileAutomation)
    { mAutomatedProfiler = std::make_shared<CameraProfiler>(); }
    else if (mCfg.profileBuilds)
    { mAutomatedProfiler = std::make_shared<BuildProfiler>(); }
    else if (mCfg.profileTriangles)
    { mAutomatedProfiler = std::make_shared<TriangleProfiler>(); }
}

void App::setupCommonGui()
{
    gui().addGuiContext("/Common/").setType(quark::gui::GuiContextType::Category).setLabel("Common Settings");

    const auto startingSceneIndex{ mSceneDatabase.sceneSelected() ? mSceneDatabase.currentSceneIndex() + 1 : 0 };
    gui().addGuiVariable<quark::gui::Enumeration>("Scene", "/Common/", quark::gui::Enumeration{ startingSceneIndex })
        .setLabel("Scene")
        .cfg().setItemGetter(mSceneDatabase.size() + 1u, 
            [&] (int index, const char **out)
    {
        if (index <= 0)
        { *out = "Custom"; }
        else
        { *out = mSceneDatabase[index - 1].name.c_str(); }

        return true;
    });

    gui().addGuiVariable<quark::gui::Enumeration>("RenderMode", "/Common/", { })
        .setLabel("Rendering mode")
        .cfg().setItemGetter(static_cast<std::size_t>(RenderMode::Last), 
            [] (int index, const char **out)
    {
        *out = renderModeToStr(static_cast<RenderMode>(index));
        return true;
    });
    gui().addGuiVariable<quark::gui::InputFloat>("DuplicationOffset", "/Common/", { mCfg.duplicationOffset })
        .setLabel("Dup. Offset");
    gui().addGuiVariable<quark::gui::InputInt>("DuplicationValue", "/Common/", { mC.duplication })
        .setLabel("Duplication")
        .cfg().setMin(1);
    gui().addGuiVariable<quark::gui::InputBool>("DuplicationCubic", "/Common/", { })
        .setLabel("Duplicate in Cube");
    gui().addGuiVariable<quark::gui::InputInt>("TargetFps", "/Common/", { mCfg.appTargetFps })
        .setLabel("Target FPS")
        .cfg().setMin(0);
    gui().addGuiVariable<quark::gui::InputInt>("TargetUps", "/Common/", { mCfg.appTargetUps })
        .setLabel("Target UPS")
        .cfg().setMin(1);
    gui().addGuiVariable<quark::gui::InputBool>("FastBuildAS", "/Common/", { mCfg.renderRayTracingFastBuildAS })
        .setLabel("Fast Build AS")
        .cfg();
    gui().addGuiVariable<quark::gui::InputBool>("DuplicateMeshes", "/Common/", { mCfg.duplicateBlas })
        .setLabel("Duplicate BLAS")
        .cfg();
    gui().addGuiVariable<quark::gui::Enumeration>("QualityPreset", "/Common/", { })
        .setLabel("Quality Preset")
        .cfg().setItemGetter(static_cast<std::size_t>(AppSpecificConfig::QualityPreset::Count), 
            [] (int index, const char **out) 
    {
        *out = AppSpecificConfig::qualityPresetName(static_cast<AppSpecificConfig::QualityPreset>(index));
        return out != nullptr;
    });

    gui().addGuiContext("/Camera/").setType(quark::gui::GuiContextType::Category).setLabel("Camera Settings");

    gui().addGuiVariable<quark::gui::Position3>("CameraPosition", "/Camera/", { })
        .setLabel("Position");
    gui().addGuiVariable<quark::gui::Position3>("CameraRotation", "/Camera/", { })
        .setLabel("Rotation");

    gui().addGuiVariable<quark::gui::InputBool>("CameraAuto", "/Camera/", { false })
        .setLabel("Auto Camera");
    gui().addGuiVariable<quark::gui::Button>("CameraClearTrack", "/Camera/", { [&] ()
    {
        mScriptedTrackMaker.finishPoints(mScriptedController.track());
        mScriptedController.track().clearKeyPoints();
    } }).setLabel("Clear Track");
    gui().addGuiVariable<quark::gui::Button>("CameraRecordPoint", "/Camera/", { [&] ()
    {
        mScriptedTrackMaker.pushCameraPoint(*mCameraSys, mScriptedController.track());
    } }).setLabel("Add Track Point");
    gui().addGuiVariable<quark::gui::InputString>("CameraTrackName", "/Camera/", { "Track" })
        .setLabel("Track Name");
    gui().addGuiVariable<quark::gui::Button>("CameraTrackLoad", "/Camera/", { [&] () 
    {
        const auto &trackName{ gui().getVariable<quark::gui::InputString>("CameraTrackName") };
        mScriptedController.track().loadFromFile(quark::res::File(trackName.value, 
            { "./", App::DEFAULT_SCENE_SEARCH_PATH_PRIMARY, App::DEFAULT_SCENE_SEARCH_PATH_SECONDARY }));
    } }).setLabel("Load Track");
    gui().addGuiVariable<quark::gui::Button>("CameraTrackSave", "/Camera/", { [&]() 
    {
        const auto &trackName{ gui().getVariable<quark::gui::InputString>("CameraTrackName") };
        mScriptedController.track().saveToFile(quark::res::File(trackName.value));
    } }).setLabel("Save Track");

    gui().addGuiContext("/Profiling/").setType(quark::gui::GuiContextType::Category).setLabel("Profiling");
    gui().addGuiVariable<quark::gui::InputString>("ProfilingFileName", "/Profiling/", { "App" })
        .setLabel("Filename");
    gui().addGuiVariable<quark::gui::Button>("ProfilingBegin", "/Profiling/", { [&] ()
    {
        setupProfiling();
    } }).setLabel("Begin Profiling");
    gui().addGuiVariable<quark::gui::Button>("ProfilingEnd", "/Profiling/", { [&] ()
    {
        const auto &fileName{ gui().getVariable<quark::gui::InputString>("ProfilingFileName") };
        saveProfilingData(fileName);
    } }).setLabel("End Profiling");
}

void App::checkChooseCamera()
{
    if (!mCameraSys->cameraChosen())
    {
        log<Info>() << "No camera has been chosen!" << std::endl;
        const auto availableCameras{ mCameraSys->numAvailableCameras() };
        if (availableCameras == 0u)
        {
            log<Warning>() << "No cameras are available in the current scene!" << std::endl;
            return;
        }

        log<Info>() << "Choosing the first camera from " << availableCameras << " total cameras in the current scene." << std::endl;
        mCameraSys->chooseCamera(0u);

        mCameraController.position(quark::dxtk::math::Vector3(0.0f, 0.0f, DEFAULT_CAMERA_DISTANCE));
    }
}

void App::changeDuplicationBy(int32_t offset)
{
    int64_t newDuplication{ static_cast<int64_t>(mC.duplication) + offset };

    if (newDuplication < 1)
    { newDuplication = 1; }

    if (newDuplication != mC.duplication)
    {
        mC.duplication = static_cast<uint32_t>(newDuplication);
        mC.duplicationChanged = true;
    }
}

void App::updateLoadedScene()
{
    if (!mC.sceneReloadRequired)
    { return; }

    PROF_SCOPE("UpdateLoadedScene");

    // Prepare the user-selected scene for rendering.
    prepareSceneRendering();

    // Done.
    mC.sceneReloadRequired = false;
}

void App::updateDuplication()
{
    if (!mC.duplicationChanged)
    { return; }

    for (auto &entity : mEngine.scene().foreachSceneEntity())
    { // Cleanup the duplicated entities.
        if (entity.has<AppInfoComponent>() && entity.get<AppInfoComponent>()->duplicated)
        { mEngine.scene().destroyEntity(entity); }
    }

    if (mEngine.scene().foreachSceneEntity().size() == 0u)
    { // No source entity available.
        return;
    }

    // Source entity is always the first one for now.
    auto srcEntity{ *mEngine.scene().foreachSceneEntity().begin() };

    // Factor of duplication if we are duplicating in a cube.
    const auto cubeDuplicates{ mCfg.duplicationCube ? mC.duplication : 1u };
    // Number of duplicates to create.
    const auto duplicates{ mC.duplication * mC.duplication * cubeDuplicates };

    // Create the grid of entities.
    for (std::size_t iii = 1u; iii < duplicates; ++iii)
    { // Skip the first one, which would overlap the original.
        const auto pX{ iii % mC.duplication };
        const auto pY{ (iii / mC.duplication) % mC.duplication };
        const auto pZ{ mCfg.duplicationCube ? iii / (mC.duplication * mC.duplication) : 0.0f };

        auto duplicateEntity{ mEngine.scene().duplicateSceneEntity<quark::scene::comp::Renderable, quark::scene::comp::Transform>(srcEntity) };

        // Offset the location.
        auto transformComp{ duplicateEntity.get<quark::scene::comp::Transform>() };
        transformComp->localTransform.setTranslate({ mCfg.duplicationOffset * pX, mCfg.duplicationOffset * pZ, mCfg.duplicationOffset * pY });
        transformComp->dirty = true;

        // Mark the entity as duplicated.
        auto appInfoComp{ duplicateEntity.add<AppInfoComponent>() };
        appInfoComp->duplicated = true;
    }

    // Duplication has been updated.
    mC.duplicationChanged = false;
}

void App::updateGui(Milliseconds delta)
{
    gui().setEnabled(mCfg.guiEnable);

    mAppGui.updateFpsTimer(mFpsTimer);
    mAppGui.updateGui(gui());

    updateCommonGui();

    mTexturedRl->updateGui(gui());
    mDeferredRl->updateGui(gui());
    mRayTracingRl->updateGui(gui());
    mResolveRl->updateGui(gui());

    mShadowMapRl->updateGui(gui());
    mAmbientOcclusionRl->updateGui(gui());
    mRasterResolveRl->updateGui(gui());
}

void App::updateCommonGui()
{
    auto &renderModeVar{ gui().getVariable<quark::gui::Enumeration>("RenderMode") };
    quark::gui::synchronizeVariables(renderModeVar, mC.renderMode);

    auto &sceneVar{ gui().getVariable<quark::gui::Enumeration>("Scene") };
    if (sceneVar.valueChanged && sceneVar.value > 0)
    { setCurrentScene(static_cast<std::size_t>(sceneVar.value) - 1u); }

    auto &duplicationOffsetVar{ gui().getVariable<quark::gui::InputFloat>("DuplicationOffset") };
    mC.duplicationChanged |= quark::gui::synchronizeVariables(duplicationOffsetVar, mCfg.duplicationOffset);

    auto &duplicationVar{ gui().getVariable<quark::gui::InputInt>("DuplicationValue") };
    mC.duplicationChanged |= quark::gui::synchronizeVariables(duplicationVar, mC.duplication);

    auto &duplicationCubeVar{ gui().getVariable<quark::gui::InputBool>("DuplicationCubic") };
    mC.duplicationChanged |= quark::gui::synchronizeVariables(duplicationCubeVar, mCfg.duplicationCube);

    auto &targetFpsVar{ gui().getVariable<quark::gui::InputInt>("TargetFps") };
    if (targetFpsVar.valueChanged)
    {
        mCfg.appTargetFps = targetFpsVar.value;
        mCfg.appUseVSync = mCfg.appTargetFps != 0u;
        mCfg.appUseTearing = mCfg.appTargetFps == 0u;

        mRenderDeltaMs = renderDelta();
    }
    auto &targetUpsVar{ gui().getVariable<quark::gui::InputInt>("TargetUps") };
    if (targetUpsVar.valueChanged)
    {
        mCfg.appTargetUps = targetUpsVar.value;

        mUpdateDeltaMs = updateDelta();
    }

    auto &fastBuildAsVar{ gui().getVariable<quark::gui::InputBool>("FastBuildAS") };
    quark::gui::synchronizeVariables(fastBuildAsVar, mCfg.renderRayTracingFastBuildAS);
    if (mRayTracingRl)
    { mRayTracingRl->setBuildOptimized(!mCfg.renderRayTracingFastBuildAS); }
    auto &duplicateMeshesVar{ gui().getVariable<quark::gui::InputBool>("DuplicateMeshes") };
    quark::gui::synchronizeVariables(duplicateMeshesVar, mCfg.duplicateBlas);
    if (mRayTracingRl)
    { mRayTracingRl->setBlasDuplication(mCfg.duplicateBlas); }

    auto &qualityPresetVar{ gui().getVariable<quark::gui::Enumeration>("QualityPreset") };
    if (quark::gui::synchronizeVariables(qualityPresetVar, mCfg.qualityPreset))
    { setupQualitySettings(); }

    if (mCameraSys->cameraChosen())
    {
        auto &transform{ mCameraSys->chosenCameraTransform() };
        auto translation{ transform.localTransform.translate() };
        auto rotation{ quaternionToYawPitchRoll(transform.localTransform.rotate()) };

        auto &translationVar{ gui().getVariable<quark::gui::Position3>("CameraPosition") };
        auto &rotationVar{ gui().getVariable<quark::gui::Position3>("CameraRotation") };

        if (quark::gui::synchronizeVariables(translationVar, translation))
        {
            transform.localTransform.setTranslate(translation);
            mCameraSys->chosenCameraTransformChanged();
        }

        if (rotationVar.valueChanged)
        {
            rotation = static_cast<quark::dxtk::math::Vector3>(rotationVar);
            transform.localTransform.setRotate(yawPitchRollToQuaternion(rotation));
            mCameraSys->chosenCameraTransformChanged();
        }
        else
        { rotationVar.value = static_cast<quark::gui::Position3>(rotation); }
    }

    const auto cameraAuto{ gui().getVariable<quark::gui::InputBool>("CameraAuto") };
    if (cameraAuto.valueChanged)
    {
        mScriptedTrackMaker.finishPoints(mScriptedController.track());
        if (cameraAuto.value)
        { mScriptedController.startTrack(); }
        else
        { mScriptedController.stopTrack(); }
    }
}

void App::setCurrentScene(std::size_t sceneIndex)
{
    // Check validity of the scene index.
    if (sceneIndex >= mSceneDatabase.size())
    { return; }

    const auto &newScenePath = mSceneDatabase[sceneIndex].path;

    if (newScenePath != mCfg.glTFSceneFile)
    {
        mCfg.glTFSceneFile = newScenePath;
        mC.sceneReloadRequired = true;
    }
}

quark::dxtk::math::Vector3 App::screenSpaceToDirection(uint32_t x, uint32_t y)
{
    // Use ray tracing matrix, which transforms from screen-space to world.
    const auto screenToWorld{ mCameraSys->camera().rayTracingVP().Transpose() };

    // Normalized device coordinates <-1.0f, 1.0f>, y-axis is flipped.
    const auto screenSpace{ 
        quark::dxtk::math::Vector4 {
            2.0f * x / mC.windowWidth - 1.0f,
            -(2.0f * y / mC.windowHeight - 1.0f),
            0.0f, 1.0f } };

    // Position of the pixel in world-space
    quark::dxtk::math::Vector4 worldSpace{ XMVector4Transform(screenSpace, screenToWorld) };
    worldSpace.x /= worldSpace.w;
    worldSpace.y /= worldSpace.w;
    worldSpace.z /= worldSpace.w;

    // Position of the camera in world-space.
    const auto cameraPos{ mCameraSys->camera().position() };

    quark::dxtk::math::Vector3 result{ 
        worldSpace.x - cameraPos.x, 
        worldSpace.y - cameraPos.y, 
        worldSpace.z - cameraPos.z };
    result.Normalize();

    // Calculate a vector from camera to the pixel.
    return result;
}

quark::dxtk::math::Vector3 App::quaternionToYawPitchRoll(
    const quark::dxtk::math::Quaternion &rot)
{
    // Source: https://en.wikipedia.org/wiki/Conversion_between_quaternions_and_Euler_angles
    const auto sinrCosp{ 2.0f * (rot.w * rot.x + rot.y * rot.z) };
    const auto cosrCosp{ 1.0f - 2.0f * (rot.x * rot.x + rot.y * rot.y) };
    const auto roll = atan2(sinrCosp, cosrCosp);

    const auto sinp{ 2.0f * (rot.w * rot.y - rot.z * rot.x) };
    auto pitch{ 0.0f };
    if (fabs(sinp) >= 1.0f)
    { pitch = copysign(M_FPI / 2.0f, sinp); }
    else
    { pitch = asin(sinp); }

    const auto sinyCosp{ 2.0f * (rot.w * rot.z + rot.x * rot.y) };
    const auto cosyCosp{ 1.0f - 2.0f * (rot.y * rot.y + rot.z * rot.z) };
    const auto yaw{ atan2(sinyCosp, cosyCosp) };

    return { yaw, pitch, roll };
}

quark::dxtk::math::Quaternion App::yawPitchRollToQuaternion(
    const quark::dxtk::math::Vector3 &ypr)
//{ return quark::dxtk::math::Quaternion::CreateFromYawPitchRoll(ypr.x, ypr.y, ypr.z); }
{
    const auto q{ quark::dxtk::math::Quaternion::CreateFromYawPitchRoll(ypr.x, ypr.y, ypr.z) };
    return { q.z, q.x, q.y, q.w };
}

const char *App::renderModeToStr(RenderMode mode)
{
    switch (mode)
    {
        case RenderMode::Rasterization:
        { return "Rasterization"; }
        case RenderMode::RayTracing: 
        { return "Ray Tracing"; }
        case RenderMode::Textured: 
        { return "Textured"; }
        default: 
        { return "Unknown"; }
    }
}

std::string App::generateConfigurationString() 
{
    std::stringstream ss;

    ss << "Command line parameters  = " << mCfg.commandLineParameters << "\n";
#ifdef NDEBUG
    ss << "Build type               = Release\n";
#else // NDEBUG
    ss << "Build type               = Debug\n";
#endif // NDEBUG
    ss << "Device name              = " << util::toString(renderer().adapter().deviceName()) << "\n";
    ss << "Device memory MB         = " << renderer().adapter().deviceMemory() << "\n";
    ss << "Ray Tracing backend      = " << renderer().rayTracing().rayTracingBackendStr() << "\n";
    ss << "Window width             = " << mC.windowWidth << "\n";
    ss << "Window height            = " << mC.windowHeight << "\n";
    ss << "Window title             = " << util::toString(mCfg.windowTitle) << "\n";
    ss << "Back-buffer count        = " << mCfg.swapChainNumBackBuffers << "\n";
    ss << "Fill Triangles           = " << (mCfg.renderRasterizerFill ? "enabled" : "disabled") << "\n";
    ss << "Triangles Font Clock-Wise= " << (mCfg.renderRasterizerFrontClockwise ? "enabled" : "disabled") << "\n";
    ss << "Fast Build AS            = " << (mCfg.renderRayTracingFastBuildAS ? "enabled" : "disabled") << "\n";
    ss << "Target FPS               = " << mCfg.appTargetFps << "\n";
    ss << "Target UPS               = " << mCfg.appTargetUps << "\n";
    ss << "Tearing                  = " << (mCfg.appUseTearing ? "enabled" : "disabled") << "\n";
    ss << "VSync                    = " << (mCfg.appUseVSync ? "enabled" : "disabled") << "\n";
    ss << "GUI rendering            = " << (mCfg.guiEnable ? "enabled" : "disabled") << "\n";
    ss << "Debug layer              = " << (mCfg.debugEnableLayer ? "enabled" : "disabled") << "\n";
    ss << "GPU validation           = " << (mCfg.debugGpuValidation ? "enabled" : "disabled") << "\n";
    ss << "GPU profiling            = " << (mCfg.debugGpuProfiling ? "enabled" : "disabled") << "\n";
    ss << "Render mode              = " << renderModeToStr(mC.renderMode) << "\n";
    ss << "Loaded scene file        = " << mCfg.glTFSceneFile << "\n";
    ss << "Using testing scene      = " << (mCfg.useTestingScene ? "enabled" : "disabled") << "\n";
    ss << "Duplication              = " << mC.duplication << "\n";
    ss << "Duplication in cube      = " << (mCfg.duplicationCube ? "enabled" : "disabled") << "\n";
    ss << "Duplication offset       = " << mCfg.duplicationOffset << "\n";
    ss << "BLAS duplication         = " << (mCfg.duplicationCube ? "enabled" : "disabled") << "\n";
    ss << "Rays per pixel           = " << mRayTracingRl->raysPerPixel() << "\n";
    ss << "Quality preset           = " << AppSpecificConfig::qualityPresetName(mCfg.qualityPreset) << "\n";

    ss << "Rasterization Quality    : " << "\n";
    ss << "  AO                     = " << (gui().getVariable<quark::gui::InputBool>("RasterResolveRlEnableAO").value ? "enabled" : "disabled") << "\n";
    ss << "  AO kernel size         = " << gui().getVariable<quark::gui::InputInt>("AmbientOcclusionRlKernelSize").value << "\n";
    ss << "  Shadows                = " << (gui().getVariable<quark::gui::InputBool>("RasterResolveRlEnableShadows").value ? "enabled" : "disabled") << "\n";
    ss << "  Shadow map resolution  = " << gui().getVariable<quark::gui::InputInt>("ShadowMapRlResolution").value << "\n";
    ss << "  Shadow PCF kernel size = " << gui().getVariable<quark::gui::InputInt>("RasterResolveRlPcfKernelHalfSize").value << "\n";
    ss << "  Reflections            = " << (gui().getVariable<quark::gui::InputBool>("RasterResolveRlEnableReflections").value ? "enabled" : "disabled") << "\n";

    ss << "Ray Tracing Quality      : " << "\n";
    ss << "  AO                     = " << (gui().getVariable<quark::gui::InputBool>("ResolveRlEnableAO").value ? "enabled" : "disabled") << "\n";
    ss << "  AO rays                = " << gui().getVariable<quark::gui::InputInt>("RayTracingRlAoRays").value << "\n";
    ss << "  AO kernel size         = " << gui().getVariable<quark::gui::InputInt>("ResolveRlKernelHalfSize").value << "\n";
    ss << "  Shadows                = " << (gui().getVariable<quark::gui::InputBool>("ResolveRlEnableShadows").value ? "enabled" : "disabled") << "\n";
    ss << "  Shadow rays            = " << gui().getVariable<quark::gui::InputInt>("RayTracingRlShadowRays").value << "\n";
    ss << "  Shadow kernel size     = " << gui().getVariable<quark::gui::InputInt>("ResolveRlKernelHalfSize").value << "\n";
    ss << "  Reflections            = " << (gui().getVariable<quark::gui::InputBool>("ResolveRlEnableReflections").value ? "enabled" : "disabled") << "\n";

    return ss.str();
}

std::string App::generateQualityConfigurationString() const
{
    // TODO - Implement
    return "";
}

void App::loadQualityConfigurationString(quark::res::File &settings) const
{
    // TODO - Implement
    UNUSED(settings);
}

void App::saveQualityConfigurationString(quark::res::File &settings) const
{
    // TODO - Implement
    UNUSED(settings);
}

void App::saveProfilingData(const std::string &filePrefix)
{
    const auto dateTime{ util::generateDateTimeString() };
    const auto profFileName{ filePrefix + "_prof_" + dateTime + ".txt" };

    std::stringstream ss;

    ss << "Configuration: \n";
    ss << generateConfigurationString();
    ss << "\n";

    const auto profilingTables{ mCollectorMgr.generateOutputString() };
    ss << profilingTables << "\n";

    ss << "AccelerationStructure: \n";
    ss << mRayTracingRl->accelerationStructureStatisticsString();
    ss << "\n";

    util::StringBuffer<util::syncers::LambdaSyncer<std::string>> tmpBuffer([&](const std::string &str) {
        ss << str << std::endl;
    });

    auto oldBuffer{ log<Prof>().rdbuf(&tmpBuffer) };
    {
        ss << "ProfilingStack: \n";
        util::prof::PrintCrawler pc(false);
        PROF_DUMP(pc);
        ss << "\n";
    }
    log<Prof>().rdbuf(oldBuffer);

    std::ofstream out(profFileName);
    out << ss.str();
}

void App::performResize(ResizeEvent event)
{
    log<Info>() << "Resizing to: " << event.newWidth << "x" << event.newHeight << std::endl;

    mAppGui.updateResolution(event);

    if (event.newWidth == 0u || event.newHeight == 0u)
    {
        log<Info>() << "Skipping resize, since resolution is 0!" << std::endl;
        return;
    }

    { // Before resizing the swap chain: 
        mImGuiRl->resizeBeforeSwapChain(event.newWidth, event.newHeight);
    }

    mEngine.renderer().resize(event.newWidth, event.newHeight);

    { // After resizing the swap chain: 
        mImGuiRl->resizeAfterSwapChain();
        mTexturedRl->resize(event.newWidth, event.newHeight);
        mAmbientOcclusionRl->resize(event.newWidth, event.newHeight);
        mRasterResolveRl->resize(event.newWidth, event.newHeight);
        mDeferredRl->resize(event.newWidth, event.newHeight);;
        mRayTracingRl->resize(event.newWidth, event.newHeight);
        mResolveRl->resize(event.newWidth, event.newHeight);

        mCameraController.setViewportSize(
            static_cast<float>(event.newWidth), static_cast<float>(event.newHeight));
    }

    mC.windowWidth = static_cast<float>(event.newWidth);
    mC.windowHeight = static_cast<float>(event.newHeight);
}

void App::renderRasterization(quark::res::d3d12::D3D12CommandList &cmdList)
{
    PROF_SCOPE("Rasterization"); 
    GPU_PROF_SCOPE(cmdList, "RasterizationGpu");

    mDeferredRl->prepare();
    mDeferredRl->render(
        mCameraSys->camera(), 
        cmdList);
        
    mShadowMapRl->setEnabled(
        mRasterResolveRl->requireShadows());
    mShadowMapRl->lights()
        .clear()
        .pushLight(mRasterResolveRl->getLightDirection());
    mShadowMapRl->prepare();
    mShadowMapRl->render(
        mCameraSys->camera(), 
        cmdList);

    mAmbientOcclusionRl->setEnabled(
        mRasterResolveRl->requireAmbientOcclusion());
    mAmbientOcclusionRl->setDeferredBuffers(
        mDeferredRl->getDeferredBuffers());
    mAmbientOcclusionRl->render(
        mCameraSys->camera(),
        cmdList);

    mRasterResolveRl->setDeferredBuffers(
        mDeferredRl->getDeferredBuffers());
    mRasterResolveRl->setShadowMapBuffers(
        mShadowMapRl->shadowMapBuffers(), 
        mShadowMapRl->lights());
    mRasterResolveRl->setAmbientOcclusionBuffers(
        mAmbientOcclusionRl->getAmbientOcclusionBuffers());
    mFpsTimer.setRaysPerFrame(0);
    mRasterResolveRl->render(
        mCameraSys->camera(), 
        renderer().swapChain().currentBackbuffer(), 
        renderer().swapChain().currentBackbufferRtv(), 
        cmdList);
}

void App::renderRayTracing(quark::res::d3d12::D3D12CommandList & cmdList)
{
    PROF_SCOPE("RayTracing"); 
    GPU_PROF_SCOPE(cmdList, "RayTracingGpu");

    if (mRayTracingRl->deferredBuffersRequired())
    { // Skip generation of deferred buffers, when using pure ray tracing.
        PROF_SCOPE("Deferred");

        mDeferredRl->prepare();
        mDeferredRl->render(
            mCameraSys->camera(), 
            cmdList);
    }

    PROF_BLOCK("RayCasting");
    mRayTracingRl->setBlasDuplication(
        mCfg.duplicateBlas);
    mRayTracingRl->prepare(
        cmdList);
    mRayTracingRl->setDeferredBuffers(
        mDeferredRl->getDeferredBuffers());
    mRayTracingRl->render(
        mCameraSys->camera(), 
        renderer().swapChain().currentBackbuffer(), 
        cmdList);
    mFpsTimer.setRaysPerFrame(
        mRayTracingRl->raysPerFrame());
    PROF_BLOCK_END();

    if (mRayTracingRl->resolveRequired())
    { // Skip resolve when using pure ray tracing.
        PROF_SCOPE("Resolve");

        mResolveRl->setDeferredBuffers(
            mDeferredRl->getDeferredBuffers());
        mResolveRl->setRayTracingBuffers(
            mRayTracingRl->getRayTracingBuffers());
        mResolveRl->setLightingInformation(
            mRayTracingRl->getLightDirection(),
            mRayTracingRl->getLightColor(),
            mRayTracingRl->getBackgroundColor());
        mResolveRl->render(
            mCameraSys->camera(), 
            renderer().swapChain().currentBackbuffer(), 
            renderer().swapChain().currentBackbufferRtv(), 
            cmdList);
    }
   
}

void App::renderTextured(quark::res::d3d12::D3D12CommandList & cmdList)
{
    PROF_SCOPE("Textured"); 
    GPU_PROF_SCOPE(cmdList, "TexturedGpu");

    mTexturedRl->prepare();
    mFpsTimer.setRaysPerFrame(0);
    mTexturedRl->render(
        mCameraSys->camera(), 
        renderer().swapChain().currentBackbufferRtv(), 
        cmdList);
}

void App::renderGui(quark::res::d3d12::D3D12CommandList & cmdList)
{
    PROF_SCOPE("ImGui"); 
    GPU_PROF_SCOPE(cmdList, "ImGuiGpu");

    mImGuiRl->render(
        renderer().swapChain().currentBackbufferRtv(), 
        gui().model(), 
        cmdList);
}

void AutomatedProfiler::processCustomAggregators(
    quark::app::DataCollectorMgr &collectorMgr)
{ UNUSED(collectorMgr); }

DuplicationProfiler::~DuplicationProfiler()
{ /* Automatic */ }

void DuplicationProfiler::start(App &app)
{
    mApp = &app;
    mDuplicationTarget = mApp->mCfg.profileMaxDuplication;
    mApp->mCfg.guiEnable = false;
    mApp->mC.duplication = 1u; 
    mApp->mCfg.duplicateBlas = true;
}

void DuplicationProfiler::update(float milliseconds)
{ UNUSED(milliseconds); }

void DuplicationProfiler::updateDebugStart()
{ mApp->mCollectorMgr.collectData(); }

void DuplicationProfiler::updateDebugEnd()
{
    if (mUpdateIndex % 2)
    {
        if (mApp->mC.duplication < mDuplicationTarget)
        { 
            mApp->mC.duplication++; 
            mApp->mC.duplicationChanged = true;
        }
        else
        { mApp->exit(); }
    }

    mUpdateIndex++;
}

CameraProfiler::~CameraProfiler()
{ /* Automatic */ }

void CameraProfiler::start(App &app)
{
    mApp = &app;
    mApp->mScriptedController.track().loadFromFile(quark::res::File(mApp->mCfg.profileCameraTrack, 
        { App::DEFAULT_SCENE_SEARCH_PATH_PRIMARY, App::DEFAULT_SCENE_SEARCH_PATH_SECONDARY }));
    mApp->mScriptedController.startTrack();
    mApp->mCfg.guiEnable = false;
    mApp->mCfg.appTargetFps = 0u;
    mApp->mCfg.appUseVSync = false;
    mApp->mCfg.appUseTearing = true;
}

void CameraProfiler::update(float milliseconds)
{ UNUSED(milliseconds); }

void CameraProfiler::updateDebugStart()
{ mApp->mCollectorMgr.collectData(); }

void CameraProfiler::updateDebugEnd()
{ 
    if (!mApp->mScriptedController.running())
    { mApp->exit(); }
}

TriangleProfiler::~TriangleProfiler()
{ /* Automatic */ }

void TriangleProfiler::start(App &app) 
{ 
    mApp = &app;

    if (mApp->mCfg.profileCameraTrack.empty())
    {
        log<Error>() << "Triangle profiler requires a camera track to be set using --profile-camera-track" << std::endl;
        mApp->exit();
        return;
    }

    mApp->mCfg.guiEnable = false;
    mApp->mCfg.appTargetFps = 0u;
    mApp->mCfg.appUseVSync = false;
    mApp->mCfg.appUseTearing = true;
    mApp->mCfg.duplicateBlas = true;

    mState = ProfState::Prepare;
    mDuplicationTarget = mApp->mCfg.profileMaxDuplication;
}

void TriangleProfiler::update(float milliseconds)
{ UNUSED(milliseconds); }

void TriangleProfiler::updateDebugStart()
{ 
    switch (mState)
    {
        case ProfState::Increment:
        { break; }
        default:
        case ProfState::Prepare:
        { break; }
        case ProfState::Wait:
        { break; }
        case ProfState::Profile:
        {
            mApp->mCollectorMgr.collectData(); 

            if (!mApp->mScriptedController.running())
            { mState = ProfState::Increment; }

            break;
        }
    }
}

void TriangleProfiler::updateDebugEnd()
{
    switch (mState)
    {
        case ProfState::Increment:
        { 
            if (mApp->mC.duplication < mDuplicationTarget)
            {
                mApp->mC.duplication++; 
                mApp->mC.duplicationChanged = true;
                mState = ProfState::Prepare;
            }
            else
            { 
                mApp->exit(); 
                mState = ProfState::Increment;
            }

            break;
        }
        default:
        case ProfState::Prepare:
        { 
            mApp->mScriptedController.track().loadFromFile(quark::res::File(mApp->mCfg.profileCameraTrack, 
                { App::DEFAULT_SCENE_SEARCH_PATH_PRIMARY, App::DEFAULT_SCENE_SEARCH_PATH_SECONDARY }));
            mApp->mScriptedController.track().transformKeyPoints([&](auto &keyPoint) {
                const auto offset{ mApp->mCfg.duplicationOffset };
                const auto multiplier{ offset > 0.0f ? static_cast<float>(mApp->mC.duplication) : 1.0f };
                const auto posOffset{ ((multiplier - 1.0f) * offset) / 2.0f };

                if (mApp->mCfg.duplicationCube)
                { // Move in all axes.
                    keyPoint.translation *= multiplier;
                    keyPoint.translation += quark::dxtk::math::Vector3{ -posOffset };
                }
                else
                { // Move only in the x-z 2D grid.
                    keyPoint.translation.x *= multiplier;
                    keyPoint.translation.z *= multiplier;
                    keyPoint.translation.x += -posOffset;
                    keyPoint.translation.z += -posOffset;
                }
            });

            mApp->mScriptedController.startTrack();
            mState = ProfState::Wait;
            mWaitCounter = 0u;

            break; 
        }
        case ProfState::Wait:
        {
            if (++mWaitCounter >= WAIT_CYCLES)
            { mState = ProfState::Profile; }
            break; 
        }
        case ProfState::Profile:
        {
            mApp->mCollectorMgr.collectData(); 

            if (!mApp->mScriptedController.running())
            { mState = ProfState::Increment; }

            break;
        }
    }
}

void TriangleProfiler::processCustomAggregators(
    quark::app::DataCollectorMgr &collectorMgr)
{
    auto &aggregator{ collectorMgr.getAggregation("FpsAggregator") };

    /// Allow to differentiate between duplications.
    auto &categoryCollector{ aggregator.addCollector<quark::app::ManualCollector>() };
    categoryCollector.addDataStream<std::size_t>("Duplication", [&]() {
        return mApp->mC.duplication;
    });
}

BuildProfiler::~BuildProfiler()
{ /* Automatic */ }

void BuildProfiler::start(App &app)
{ 
    mApp = &app;

    auto &device{ mApp->mEngine.renderer().device() };
    auto &rtDevice{ mApp->mEngine.renderer().rayTracing().device() };
    auto &deviceMemoryMgr{ mApp->mEngine.renderer().memoryMgr() };
    const auto drawables{ mApp->mEngine.scene().getToDraw().get() };

    { // Warm-up
        const auto drawable{ *drawables.begin() };

        auto cmdList{ mApp->mEngine.renderer().directCmdQueue().getCommandList() };
        mBuilder.beginBuilderBlock(rtDevice, deviceMemoryMgr);

        const auto geometry{ mBuilder.addGeometryNoTransform(drawable.meshHandle,
            util::toWString(drawable.meshHandle.getHolder().id()), true) };
        mBuilder.addInstance(geometry, drawable.modelMatrix);

        mBuilder.buildAccelerationStructure(device, *cmdList);
        cmdList->executeAndWait();
    }

    for (const auto &drawable : drawables)
    { // Whole meshes.
        auto cmdList{ mApp->mEngine.renderer().directCmdQueue().getCommandList() };
        mBuilder.beginBuilderBlock(rtDevice, deviceMemoryMgr);

        const auto geometry{ mBuilder.addGeometryNoTransform(drawable.meshHandle,
            util::toWString(drawable.meshHandle.getHolder().id()), true) };
        mBuilder.addInstance(geometry, drawable.modelMatrix);

        mBuilder.buildAccelerationStructure(device, *cmdList);
        GPU_PROF_RESOLVE(*cmdList);
        cmdList->executeAndWait();
        GPU_PROF_FINALIZE();

        mApp->mCollectorMgr.collectData();
    }

    for (const auto &drawable : drawables)
    { // Each primitive.
        const auto &primitives{ drawable.meshHandle->primitives };
        for (const auto &primitive : primitives)
        {
            auto cmdList{ mApp->mEngine.renderer().directCmdQueue().getCommandList() };
            mBuilder.beginBuilderBlock(rtDevice, deviceMemoryMgr);

            const auto geometry{ mBuilder.addGeometryNoTransform(primitive) };
            mBuilder.addInstance(geometry, drawable.modelMatrix);

            mBuilder.buildAccelerationStructure(device, *cmdList);
            GPU_PROF_RESOLVE(*cmdList);
            cmdList->executeAndWait();
            GPU_PROF_FINALIZE();

            mApp->mCollectorMgr.collectData();
        }
    }

    // Immediate cessation of the application.
    mApp->mRunning = false;
    mApp->exit();
}

void BuildProfiler::update(float milliseconds)
{ UNUSED(milliseconds); }

void BuildProfiler::updateDebugStart()
{ }

void BuildProfiler::updateDebugEnd()
{ }

void BuildProfiler::processCustomAggregators(
    quark::app::DataCollectorMgr &collectorMgr)
{
    // We need to re-create the build aggregator to use our statistics...
    { auto &profilingBuildAggregator{ collectorMgr.getAggregation("ProfilingBuildAggregator") };
        profilingBuildAggregator.clear();
        profilingBuildAggregator.setAutoIndexing(true);
        profilingBuildAggregator.addCollector<quark::app::ProfilingCollector>()
            .setNoDuplication(true)
            .addStream("BuildBottomLevelAS", 2u)
            .addStream("BuildBottomLevelASGpu", 2u)
            .addStream("BuildTopLevelAS", 2u)
            .addStream("BuildTopLevelASGpu", 2u);
        auto &modelCollector{ profilingBuildAggregator.addCollector<quark::app::ManualCollector>() };
        modelCollector.setCollectInSync(true);
        modelCollector.setNoCollection(true);
        modelCollector.addDataStream<std::size_t>("Duplication", [&]() {
            return mApp->mC.duplication;
        });
        modelCollector.addDataStream<std::size_t>("Triangles", [&]() {
            return mBuilder.statistics().totalBlasTriangles;
        });
        modelCollector.addDataStream<std::size_t>("Meshes", [&]() {
            return mBuilder.statistics().totalBlasMeshes;
        });
        modelCollector.addDataStream<std::size_t>("BottomLevels", [&]() {
            return mBuilder.statistics().numBlasCreated;
        });
        modelCollector.addDataStream<std::size_t>("TopLevelSize", [&]() {
            return mBuilder.statistics().totalTlasSizes;
        });
        modelCollector.addDataStream<std::size_t>("BottomLevelsSize", [&]() {
            return mBuilder.statistics().totalBlasSizes;
        });
    }
}

}
