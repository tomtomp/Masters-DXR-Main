/**
* @file SceneDatabase.cpp
* @author Tomas Polasek
* @brief Helper class used for management 
* of available scene files.
*/

#include "stdafx.h"

#include "SceneDatabase.h"

namespace dxr
{

SceneDatabase::SceneDatabase()
{ /* Automatic */ }

SceneDatabase::~SceneDatabase()
{ /* Automatic */ }

void SceneDatabase::registerScene(
    const std::string& name, const std::string& path)
{ mScenes.emplace_back(SceneInfo{ name, path }); }

std::size_t SceneDatabase::size() const
{ return mScenes.size(); }

const SceneDatabase::SceneInfo &SceneDatabase::operator[](std::size_t index) const
{ return mScenes[index]; }

const SceneDatabase::SceneInfo &SceneDatabase::currentScene() const
{
    if (!mCurrentSceneSelected)
    { throw NoSceneException("Unable to get current scene: No scene is selected!"); }

    return mScenes[mCurrentSceneIndex];
}

std::size_t SceneDatabase::currentSceneIndex() const
{
    if (!mCurrentSceneSelected)
    { throw NoSceneException("Unable to set current scene: Index is out of range!"); }

    return mCurrentSceneIndex;
}

void SceneDatabase::selectScene(std::size_t index)
{
    if (index >= size())
    { throw NoSceneException("Unable to set current scene: Index is out of range!"); }

    mCurrentSceneIndex = index;
    mCurrentSceneSelected = true;
}

bool SceneDatabase::selectSceneByPath(const std::string& path)
{
    for (std::size_t iii = 0u; iii < mScenes.size(); ++iii)
    {
        if (mScenes[iii].path == path)
        {
            selectScene(iii);
            return true;
        }
    }

    return false;
}

bool SceneDatabase::sceneSelected() const
{ return mCurrentSceneSelected; }

}

