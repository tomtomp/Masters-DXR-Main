/**
* @file IncrementalAccelerationBuilder.cpp
* @author Tomas Polasek
* @brief Helper class used for incremental building 
* of acceleration structures of given meshes. Used 
* for profiling purposes.
*/

#include "stdafx.h"

#include "IncrementalAccelerationBuilder.h"

#include "engine/helpers/d3d12/D3D12DescTable.h"

namespace quark
{

namespace helpers
{

namespace d3d12
{

IncrementalAccelerationBuilder::IncrementalAccelerationBuilder()
{ /* Automatic */ }

IncrementalAccelerationBuilder::~IncrementalAccelerationBuilder()
{ /* Automatic */ }

IncrementalAccelerationBuilder::GeometryHandleT IncrementalAccelerationBuilder::addGeometryNoTransform(
    const res::rndr::Primitive &primitive, const std::wstring &name)
{ return mBuilder->addGeometryNoTransform(primitive, name); }

IncrementalAccelerationBuilder::GeometryHandleT IncrementalAccelerationBuilder::addGeometryNoTransform(
    const res::rndr::MeshHandle &meshHandle, const std::wstring &name, bool noDuplication)
{ return mBuilder->addGeometryNoTransform(meshHandle, name, noDuplication); }

void IncrementalAccelerationBuilder::addInstance(const GeometryHandleT &geometry, 
    const helpers::math::Transform &transform, ::UINT shaderTableOffset, 
    ::UINT instanceID, ::UINT instanceMask, ::D3D12_RAYTRACING_INSTANCE_FLAGS flags)
{ mBuilder->addInstance(geometry, transform, shaderTableOffset, instanceID, instanceMask, flags); }

void IncrementalAccelerationBuilder::beginBuilderBlock(
    res::d3d12::D3D12RayTracingDevice &device, res::rndr::GpuMemoryMgr &memoryMgr)
{ mBuilder = D3D12RTAccelerationBuilder::create(device, memoryMgr); }
void IncrementalAccelerationBuilder::buildAccelerationStructure(res::d3d12::D3D12Device &device, 
    res::d3d12::D3D12CommandList &directCmdList)
{ 
    const auto requiredDescriptors{ mBuilder->requiredDescriptors() };
    auto descHeap{ res::d3d12::D3D12DescHeap::create(device,
        ::D3D12_DESCRIPTOR_HEAP_TYPE_CBV_SRV_UAV,
        static_cast<::uint32_t>(requiredDescriptors ? requiredDescriptors : 1u),
        ::D3D12_DESCRIPTOR_HEAP_FLAG_SHADER_VISIBLE) };

    D3D12DescTable buildTable(*descHeap, descHeap->allocateTable(requiredDescriptors));

    mBuilder->buildAccelerationStructure(directCmdList, buildTable); 
}

const D3D12RTAccelerationBuilder::Statistics &IncrementalAccelerationBuilder::statistics() const
{ return mBuilder->statistics(); }

void IncrementalAccelerationBuilder::preferFastRayTracing()
{ mBuilder->preferFastRayTracing(); }

void IncrementalAccelerationBuilder::preferFastBuild()
{ mBuilder->preferFastBuild(); }

}

}

}
